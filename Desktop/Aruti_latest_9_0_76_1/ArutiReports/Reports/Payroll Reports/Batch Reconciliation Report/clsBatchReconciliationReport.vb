﻿'Class Name : clsBatchReconciliationReport.vb
'Purpose    :
'Date       : 13-June-2023
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsBatchReconciliationReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsBatchReconciliationReport"
    Private mstrReportId As String = enArutiReport.Batch_Reconciliation_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintPeriodUnkid As Integer = 0
    Private mstrPeriodName As String = ""
    Private mstrBatchName As String = ""
    Private mintStatusUnkid As Integer = 0
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodUnkid() As Integer
        Set(ByVal value As Integer)
            mintPeriodUnkid = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _BatchName() As String
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    Public WriteOnly Property _StatusUnkid() As Integer
        Set(ByVal value As Integer)
            mintStatusUnkid = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintPeriodUnkid = 0
            mstrPeriodName = ""
            mstrBatchName = ""
            mintStatusUnkid = 0
            mintStatusId = 0
            mstrStatusName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "Employee"
            OrderByQuery = "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintPeriodUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkid)
                Me._FilterQuery &= " AND prpayment_reconciliation_tran.periodunkid = @periodunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Period :") & " " & mstrPeriodName & " "
            End If

            If mstrBatchName.Trim.Length > 0 Then
                objDataOperation.AddParameter("@BatchName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName)
                Me._FilterQuery &= " AND prpayment_reconciliation_tran.batchname = @BatchName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Batch :") & " " & mstrBatchName & " "
            End If

            
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'If mintStatusUnkid > 0 Then
            '    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusUnkid)
            '    Me._FilterQuery &= " AND prpayment_reconciliation_tran.statusunkid = @statusunkid "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Status :") & " " & mstrStatusName & " "
            'End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@status", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStatusName)
                Me._FilterQuery &= " AND prpayment_reconciliation_tran.status = @status "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Status :") & " " & mstrStatusName & " "
            End If
            'Hemant (21 Jul 2023) -- End


            Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As Date, _
                                     ByVal xPeriodEnd As Date, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xExportReportPath As String, _
                                     ByVal xOpenReportAfterExport As Boolean, _
                                     Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                     Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                     Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                     )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Reconciliation")

            dtCol = New DataColumn("BatchName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Batch ID")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Employee", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Employee Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmpBankName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Employee Bank Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmpBranchName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Employee Branch Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmpAccountNo", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Employee Account No")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmpPaidCurrency", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Paid Currency")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("PaidAmount", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Paid Amount")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            If mintReportId = 1 Then
                dtCol = New DataColumn("Status", System.Type.GetType("System.String"))
                dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Status")
                dtCol.DefaultValue = ""
                dtFinalTable.Columns.Add(dtCol)

                dtCol = New DataColumn("message", System.Type.GetType("System.String"))
                dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Message")
                dtCol.DefaultValue = ""
                dtFinalTable.Columns.Add(dtCol)
            End If

            Dim dsStatus As DataSet = (New clspaymentbatchposting_master).getStatusComboList("List", True)

            StrQ &= " SELECT " & _
                    "  batchname " & _
                    ", prpayment_reconciliation_tran.employeeunkid " & _
                    ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                    ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                    ", ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname,'') AS EmpBankName " & _
                    ", ISNULL(hrmsconfiguration..cfbankbranch_master.branchname,'') AS EmpBranchName " & _
                    ", ISNULL(prpayment_reconciliation_tran.accountno, '') as  EmpAccountNo " & _
                    ", cfexchange_rate.currency_sign AS EmpPaidCurrency " & _
                    ", ISNULL(prpayment_reconciliation_tran.amount, 0) AS PaidAmount " & _
                    ", ISNULL(prpayment_reconciliation_tran.statusunkid, 0) as statusunkid "

            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'StrQ &= ", CASE ISNULL(prpayment_reconciliation_tran.statusunkid, 0) "
            'For Each dsRow As DataRow In dsStatus.Tables(0).Rows
            '    StrQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
            'Next
            'StrQ &= " END AS Status "
            StrQ &= ", ISNULL(prpayment_reconciliation_tran.Status, 0)  AS Status "
            'Hemant (21 Jul 2023) -- End            

            StrQ &= ", ISNULL(prpayment_reconciliation_tran.message, '') as message "

            StrQ &= " FROM prpayment_reconciliation_tran " & _
                    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_reconciliation_tran.employeeunkid AND hremployee_master.isactive = 1 " & _
                    " JOIN hrmsconfiguration..cfbankbranch_master ON prpayment_reconciliation_tran.branchunkid = hrmsconfiguration..cfbankbranch_master.branchunkid  AND hrmsconfiguration..cfbankbranch_master.isactive = 1 " & _
                    " JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                    " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_reconciliation_tran.paidcurrencyid "

            StrQ &= " WHERE prpayment_reconciliation_tran.isvoid = 0 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("BatchName") = drRow.Item("BatchName")
                rpt_Row.Item("EmployeeCode") = drRow.Item("EmployeeCode")
                rpt_Row.Item("Employee") = drRow.Item("Employee")
                rpt_Row.Item("EmpBankName") = drRow.Item("EmpBankName")
                rpt_Row.Item("EmpBranchName") = drRow.Item("EmpBranchName")
                rpt_Row.Item("EmpAccountNo") = drRow.Item("EmpAccountNo")
                rpt_Row.Item("EmpPaidCurrency") = drRow.Item("EmpPaidCurrency")
                rpt_Row.Item("PaidAmount") = Format(drRow.Item("PaidAmount"), GUI.fmtCurrency)
                If mintReportId = 1 Then
                    rpt_Row.Item("Status") = drRow.Item("Status")
                    rpt_Row.Item("message") = drRow.Item("message")
                End If
                dtFinalTable.Rows.Add(rpt_Row)

            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = {100, 100, 200, 200, 200, 200, 100, 100, 100, 100}
            'ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            'For i As Integer = 0 To intArrayColumnWidth.Length - 1
            '    intArrayColumnWidth(i) = 125
            'Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_DetailReport", mstrModuleName)
        End Try
    End Sub
    
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Batch ID")
			Language.setMessage(mstrModuleName, 2, "Employee Code")
			Language.setMessage(mstrModuleName, 3, "Employee Name")
			Language.setMessage(mstrModuleName, 4, "Employee Bank Name")
			Language.setMessage(mstrModuleName, 5, "Employee Branch Name")
			Language.setMessage(mstrModuleName, 6, "Employee Account No")
			Language.setMessage(mstrModuleName, 7, "Paid Currency")
			Language.setMessage(mstrModuleName, 8, "Paid Amount")
			Language.setMessage(mstrModuleName, 9, "Status")
			Language.setMessage(mstrModuleName, 10, "Message")
			Language.setMessage(mstrModuleName, 11, "Period :")
			Language.setMessage(mstrModuleName, 12, "Batch :")
			Language.setMessage(mstrModuleName, 13, "Status :")
			Language.setMessage(mstrModuleName, 14, "Prepared By :")
			Language.setMessage(mstrModuleName, 15, "Checked By :")
			Language.setMessage(mstrModuleName, 16, "Approved By")
			Language.setMessage(mstrModuleName, 17, "Received By :")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
