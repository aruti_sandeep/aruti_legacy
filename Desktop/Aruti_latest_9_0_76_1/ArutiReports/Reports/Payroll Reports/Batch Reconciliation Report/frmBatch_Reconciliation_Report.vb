﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmBatch_Reconciliation_Report
    Private ReadOnly mstrModuleName As String = "frmBatch_Reconciliation_Report"
    Private objBatchReconciliationReport As clsBatchReconciliationReport
    Private dtStatus As New DataTable

#Region " Private Function "
    Private Sub FillCombo()
        Dim objCountry As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim dsCombo As New DataSet
        Try
            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Batch Posted Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Batch Reconciliation Report"))
                .SelectedIndex = 0
            End With

            Dim intFirstOpenPeriod As Integer = objCountry.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = intFirstOpenPeriod
            End With


            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'dsCombo = objPaymentBatchPostingMaster.getStatusComboList("List", True)
            'With cboStatus
            '    .DisplayMember = "Name"
            '    .ValueMember = "Id"
            '    .DataSource = dsCombo.Tables(0)
            '    .SelectedValue = "-1"
            '    .SelectedIndex = 0
            'End With
            FillStatusCombo(dtStatus)
            'Hemant (21 Jul 2023) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCountry = Nothing
            objPeriod = Nothing
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Sub

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private Sub FillStatusCombo(ByVal dtTable As DataTable)
        Try
            If Not dtStatus.Columns.Contains("Status") Then
                Dim dCol As DataColumn

                dCol = New DataColumn("Status")
                dCol.Caption = "Status"
                dCol.DataType = System.Type.GetType("System.String")
                dtStatus.Columns.Add(dCol)
            End If

            With cboStatus
                .Items.Clear()
                .Items.Add("Select")
                For Each dr As DataRow In dtTable.Rows
                    .Items.Add(dr.Item("Status").ToString)
                Next
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillStatusCombo", mstrModuleName)
        End Try


    End Sub
    'Hemant (21 Jul 2023) -- End

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboPeriod.SelectedIndex = 0
            cboBatch.SelectedIndex = 0
            cboStatus.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            If CInt(cboBatch.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Batch."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Function
            End If

            objBatchReconciliationReport.SetDefaultValue()

            objBatchReconciliationReport._ReportId = cboReportType.SelectedIndex
            objBatchReconciliationReport._ReportTypeName = cboReportType.Text
            objBatchReconciliationReport._PeriodUnkid = cboPeriod.SelectedValue
            objBatchReconciliationReport._PeriodName = cboPeriod.Text
            objBatchReconciliationReport._BatchName = cboBatch.Text
            objBatchReconciliationReport._StatusUnkid = cboStatus.SelectedValue
            objBatchReconciliationReport._StatusName = cboStatus.Text

            objBatchReconciliationReport.setDefaultOrderBy(0)
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Constructor "
    Public Sub New()
        objBatchReconciliationReport = New clsBatchReconciliationReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objBatchReconciliationReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmBatch_Reconciliation_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBatchReconciliationReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatch_Reconciliation_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatch_Reconciliation_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            'Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatch_Reconciliation_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatch_Reconciliation_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatch_Reconciliation_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatch_Reconciliation_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatch_Reconciliation_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBatchReconciliationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchReconciliationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objBatchReconciliationReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                           User._Object._Userunkid, _
                                                                           FinancialYear._Object._YearUnkid, _
                                                                           Company._Object._Companyunkid, _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                                           True, False, ConfigParameter._Object._ExportReportPath, _
                                                                           ConfigParameter._Object._OpenAfterExport, _
                                                                           0, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            dsCombos = objPaymentBatchPostingMaster.GetBatchNameList("List", CInt(cboPeriod.SelectedValue), clspaymentbatchposting_master.enApprovalStatus.Approved, True, True, , FinancialYear._Object._DatabaseName)
            With cboBatch
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) = 1 Then
                cboStatus.SelectedValue = -1
                cboStatus.Enabled = True
            Else
                cboStatus.SelectedValue = -1
                cboStatus.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private Sub cboBatch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBatch.SelectedIndexChanged
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            If CInt(cboBatch.SelectedValue) <> 0 Then
                Dim dsRenciliationData As DataSet = (New clspaymentbatchreconciliation_tran).GetList("List", cboBatch.Text)
                dtStatus = dsRenciliationData.Tables(0).DefaultView.ToTable(True, "status")
                Call FillStatusCombo(dtStatus)
            Else
                dtStatus = New DataTable
                Call FillStatusCombo(dtStatus)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatch_SelectedIndexChanged", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Sub
    'Hemant (21 Jul 2023) -- End

#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblBatch.Text = Language._Object.getCaption(Me.lblBatch.Name, Me.lblBatch.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Batch Posted Report")
			Language.setMessage(mstrModuleName, 2, "Batch Reconciliation Report")
			Language.setMessage(mstrModuleName, 3, "Please Select Period.")
			Language.setMessage(mstrModuleName, 4, "Please Select Batch.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class