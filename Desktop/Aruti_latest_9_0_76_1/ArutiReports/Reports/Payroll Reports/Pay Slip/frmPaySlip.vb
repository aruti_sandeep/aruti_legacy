#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region


Public Class frmPaySlip

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmPaySlip"
    Private objEmpPaySlip As clsPaySlip
    'Vimal (15 Dec 2010) -- Start 
    Private mstrEmpIds As String = ""
    Private mstrPeriodIds As String = ""
    Private mblnPaySlip As Boolean = False
    'Vimal (15 Dec 2010) -- End
    Dim strEmployeeId As String = ""
    Dim strPeriodId As String = ""
    Dim strMembershipID As String = ""
    Private mstrMembershipId As String = ""


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private StartDate As Date = Nothing
    Private EndDate As Date = Nothing
    'Sohail (21 Aug 2015) -- End

    'Sohail (11 May 2017) -- Start
    'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
    Private dvEmployee As DataView
    Private dvPeriod As DataView
    Private mstrSearchEmpText As String = ""
    Private mstrSearchPeriodText As String = ""
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    'Sohail (11 May 2017) -- End
    Private mstrAdvanceFilter As String = "" 'Varsha (29-Sep-2017)

#End Region

    'Vimal (15 Dec 2010) -- Start 
#Region "Properties"
    Public WriteOnly Property _EmpIds() As String
        Set(ByVal value As String)
            mstrEmpIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PaySlip() As Boolean
        Set(ByVal value As Boolean)
            mblnPaySlip = value
        End Set
    End Property

    Public WriteOnly Property _MembershipID() As String
        Set(ByVal value As String)
            mstrMembershipId = value
        End Set
    End Property

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

#End Region
    'Vimal (15 Dec 2010) -- End

#Region "Constructor"
    Public Sub New()
        objEmpPaySlip = New clsPaySlip(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpPaySlip.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Private Function"
    Public Sub FillCombo()
        'Sohail (14 Jun 2013) -- Start
        'TRA - ENHANCEMENT
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        'Sohail (14 Jun 2013) -- End
        Dim objTranHead As New clsTransactionHead 'Sohail (09 Jan 2014)
        Dim objExRate As New clsExchangeRate 'Sohail (11 Nov 2014)

        Try
            '**************************************************************************************************************
            '********  PLEASE ADD REQUIRED CHANGES REGARDING NEW COMBOBOX IN cboOT1Hours_Validating() EVENT  WITHOUT FAIL
            '**************************************************************************************************************

            'cboMembership.DataSource = (New clsmembership_master).getListForCombo(, False).Tables(0)
            cboMembership.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True).Tables(0)
            cboMembership.ValueMember = "masterunkid"
            cboMembership.DisplayMember = "name"
            'Vimal (30 Nov 2010) -- Start 
            cboLeaveType.DataSource = (New clsleavetype_master).getListForCombo(, True, 1).Tables(0)
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "name"
            'Vimal (30 Nov 2010) -- End

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            dsCombo = objMaster.GetEAllocation_Notification("Allocation", , True)
            'Sohail (29 Mar 2016) -- Start
            'Enhancement - 58.1 - Allow to sort employees on alphabetical order.
            Dim dr As DataRow = dsCombo.Tables("Allocation").NewRow
            dr.Item("Id") = 0
            dr.Item("Name") = Language.getMessage(mstrModuleName, 12, " Select")
            dsCombo.Tables("Allocation").Rows.InsertAt(dr, 0)
            'Sohail (29 Mar 2016) -- End
            'Sohail (11 Oct 2017) -- Start
            'SUMATRA Enhancement - 70.1 - Currently in pay slip report an option for sorting is by allocation and names (both ascending and descending), client needs to have also sorting option based on employee code..
            dr = dsCombo.Tables("Allocation").NewRow
            dr.Item("Id") = -1
            dr.Item("Name") = Language.getMessage(mstrModuleName, 13, " Employee Code")
            dsCombo.Tables("Allocation").Rows.InsertAt(dr, 1)
            'Sohail (11 Oct 2017) -- End
            With cboEmpSortBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Allocation")
                .SelectedValue = enAllocation.DEPARTMENT
            End With
            'Sohail (14 Jun 2013) -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            'Sohail (21 Aug 2015) -- End
            With cboOT1Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT2Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT3Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT4Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", True, enTranHeadType.EarningForEmployees, , , , , " typeof_id NOT IN (" & enTypeOf.Salary & ")")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.EarningForEmployees, , , , , " typeof_id NOT IN (" & enTypeOf.Salary & ")")
            'Sohail (21 Aug 2015) -- End
            With cboOT1Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT2Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT3Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT4Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With
            'Sohail (09 Jan 2014) -- End

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            dsCombo = objExRate.getComboList("ExRate", True)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombo.Tables("ExRate")
                .SelectedValue = 0
            End With
            'Sohail (11 Nov 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            objMaster = Nothing
            'Sohail (14 Jun 2013) -- End
            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            objTranHead = Nothing
            objExRate = Nothing
            'Sohail (11 Nov 2014) -- End
        End Try
    End Sub

    Public Sub FillList()
        Try
            Dim objPeriod As New clscommom_period_Tran
            'Dim objEmployee As New clsEmployee_Master 'Sohail (28 Jun 2011)
            Dim objMembership As New clsmembership_master

            'Sohail (28 Jun 2011) -- Start
            'lvEmployee.Items.Clear()

            'Changes : Moved to FillEmpList to refresh employee when IncludeInActiveEmployee checked changed
            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            ''Dim dsList As DataSet = objEmployee.GetList("Employee")
            'Dim dsList As DataSet = objEmployee.GetEmployeeList("Employee", False, False)
            'Dim lvItem As ListViewItem
            'For Each dtRow As DataRow In dsList.Tables("Employee").Rows
            '    lvItem = New ListViewItem
            '    'lvItem.Text = dtRow.Item("name").ToString
            '    lvItem.Text = dtRow.Item("employeename").ToString
            '    lvItem.Tag = CInt(dtRow.Item("employeeunkid").ToString)

            '    lvEmployee.Items.Add(lvItem)
            'Next
            'lvEmployee.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            ''Pinkal (24-Jun-2011) -- End
            'Sohail (28 Jun 2011) -- End

            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'lvPeriod.Items.Clear() 'Sohail (11 May 2017)
            dgPeriod.DataSource = Nothing
            RemoveHandler txtSearchPeriod.TextChanged, AddressOf txtSearchPeriod_TextChanged
            Call SetDefaultSearchPeriodText()
            AddHandler txtSearchPeriod.TextChanged, AddressOf txtSearchPeriod_TextChanged
            'Sohail (11 May 2017) -- End

            'Vimal (27 Nov 2010) -- Start 
            'Dim dsPeriodList As DataSet = objPeriod.GetList("Period", 1, True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsPeriodList As DataSet = objPeriod.GetList("Period", enModuleReference.Payroll, True, , False) 'Sohail (28 Dec 2010)
            Dim dsPeriodList As DataSet = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, , False)
            'Sohail (21 Aug 2015) -- End
            'Vimal (27 Nov 2010) -- End
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'Dim lvItemPeriod As ListViewItem 
            'For Each dtRow As DataRow In dsPeriodList.Tables("Period").Rows
            '    lvItemPeriod = New ListViewItem
            '    lvItemPeriod.Text = dtRow.Item("period_name").ToString
            '    lvItemPeriod.Tag = CInt(dtRow.Item("periodunkid").ToString)

            '    'Sohail (17 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    lvItemPeriod.SubItems.Add(dtRow.Item("start_date").ToString)
            '    lvItemPeriod.SubItems.Add(dtRow.Item("end_date").ToString)
            '    'Sohail (17 Apr 2012) -- End

            '    RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked 'Sohail (26 Nov 2011)
            '    lvPeriod.Items.Add(lvItemPeriod)
            '    AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked 'Sohail (26 Nov 2011)
            'Next
            'lvPeriod.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            Dim dtTable As DataTable = New DataView(dsPeriodList.Tables("Period")).ToTable
            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvPeriod = dtTable.DefaultView
            dgPeriod.AutoGenerateColumns = False
            objdgperiodcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhPeriodunkid.DataPropertyName = "periodunkid"
            dgColhPeriodCode.DataPropertyName = "period_code"
            dgColhPeriod.DataPropertyName = "period_name"
            dgcolhPeriodStart.DataPropertyName = "start_date"
            dgcolhPeriodEnd.DataPropertyName = "end_date"

            dgPeriod.DataSource = dvPeriod
            dvPeriod.Sort = "IsChecked DESC, end_date "
            'Sohail (11 May 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    'Sohail (28 Jun 2011) -- Start
    Public Sub FillEmpList()
        Dim objEmployee As New clsEmployee_Master
        Try
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'lvEmployee.Items.Clear()
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged

            objlblEmpCount.Text = "( 0 / 0 )"
            mintCount = 0
            'Sohail (11 May 2017) -- End

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objEmployee.GetEmployeeList("Employee", False, Not chkInactiveemp.Checked)
            

            'Nilay (18-Nov-2015) -- Start
            'Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, chkInactiveemp.Checked, "Employee", True)


            'Varsha Rana (29-Sept-2017) -- Start
            'Enhancement - 70.001 - add analysis by option on salary slip reports [Allan Wesonga] - (RefNo: 66)
            'Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, chkInactiveemp.Checked, "Employee", False)
            Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                         True, chkInactiveemp.Checked, "Employee", False, , , , , , , , , , , , , , , mstrAdvanceFilter)
            'Varsha Rana (29-Sept-2017) -- End
           
            'Nilay (18-Nov-2015) -- End

            'Anjan [10 June 2015] -- End



            'Sohail (21 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            Dim dtTable As DataTable = New DataView(dsList.Tables("Employee"), "", "employeename", DataViewRowState.CurrentRows).ToTable
            'Sohail (21 Feb 2012) -- End
            'Dim lvItem As ListViewItem 'Sohail (11 May 2017)
            'Sohail (20 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'For Each dtRow As DataRow In dtTable.Rows
            '    'For Each dtRow As DataRow In dsList.Tables("Employee").Rows
            '    'Sohail (20 Feb 2012) -- End
            '    lvItem = New ListViewItem
            '    'Sohail (29 Mar 2016) -- Start
            '    'Enhancement - 58.1 - Show emplyee code in employee list and allow to search empoyees on employee code.
            '    'lvItem.Text = dtRow.Item("employeename").ToString
            '    lvItem.Text = dtRow.Item("employeecode").ToString
            '    lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
            '    'Sohail (29 Mar 2016) -- End
            '    lvItem.Tag = CInt(dtRow.Item("employeeunkid").ToString)

            '    RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked 'Sohail (26 Nov 2011)
            '    lvEmployee.Items.Add(lvItem)
            '    AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked 'Sohail (26 Nov 2011)
            'Next
            ''Sohail (29 Mar 2016) -- Start
            ''Enhancement - 58.1 - PShow emplyee code in list and allow to search empoyees on employee code.
            ''lvEmployee.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            'lvEmployee.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            ''Sohail (29 Mar 2016) -- End
            mintTotalEmployee = dtTable.Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"
            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvEmployee = dtTable.DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, employeename "
            'Sohail (11 May 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmpList", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub
    'Sohail (28 Jun 2011) -- End

    Public Sub ResetValue()
        Try
            rabClass.Checked = False
            rabCostCenter.Checked = False
            rabDept.Checked = False
            rabEmployee.Checked = True
            rabGrade.Checked = False
            rabJob.Checked = False
            rabPayPoint.Checked = False
            rabSection.Checked = False

            'Vimal (30 Nov 2010) -- Start 
            cboLeaveType.SelectedValue = 0
            'Vimal (30 Nov 2010) -- End

            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'If lvEmployee.CheckedItems.Count > 0 Then
            '    For i As Integer = 0 To lvEmployee.Items.Count - 1
            '        If lvEmployee.Items(i).Checked = True Then
            '            lvEmployee.Items(i).Checked = False
            '        End If
            '    Next
            'End If

            'If lvPeriod.CheckedItems.Count > 0 Then
            '    For i As Integer = 0 To lvPeriod.Items.Count - 1
            '        If lvPeriod.Items(i).Checked = True Then
            '            lvPeriod.Items(i).Checked = False
            '        End If
            '    Next
            'End If
            Call CheckAllEmployee(False)
            Call CheckAllPeriod(False)
            Call SetDefaultSearchEmpText()
            Call SetDefaultSearchPeriodText()
            'Sohail (11 May 2017) -- End

            'Vimal (10 Dec 2010) -- Start 
            objEmpPaySlip.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmpPaySlip.OrderByDisplay
            'Vimal (10 Dec 2010) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


            'Anjan (30 Aug 2011)-Start
            'Issue : Including logo setting.
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'radLogoCompanyInfo.Checked = True
            'radLogo.Checked = False
            'radCompanyDetails.Checked = False

            'chkIgnoreZeroHead.Checked = True
            'Sohail (16 May 2012) -- End
            'Sohail (13 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'chkShowLoanBalance.Checked = True
            'chkShowSavingBalance.Checked = True
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'chkShowEmployerContrib.Checked = False
            'Select Case ConfigParameter._Object._PayslipTemplate
            '    Case 1 'Default Payslip
            '        chkShowLoanBalance.Checked = True
            '        chkShowSavingBalance.Checked = True
            '    Case 2 ' TRA Format
            '        chkShowLoanBalance.Checked = False
            '        chkShowSavingBalance.Checked = False
            'End Select
            'Sohail (16 May 2012) -- End
            'Sohail (13 Apr 2012) -- End

            'Anjan (30 Aug 2011)-End 

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            chkIgnoreZeroHead.Checked = ConfigParameter._Object._IgnoreZeroValueHeadsOnPayslip
            chkShowLoanBalance.Checked = ConfigParameter._Object._ShowLoanBalanceOnPayslip
            chkShowSavingBalance.Checked = ConfigParameter._Object._ShowSavingBalanceOnPayslip
            chkShowEmployerContrib.Checked = ConfigParameter._Object._ShowEmployerContributionOnPayslip
            chkShowCumulativeAccrual.Checked = ConfigParameter._Object._ShowCumulativeAccrualOnPayslip 'Sohail (11 Sep 2012)
            chkShowAllHeads.Checked = ConfigParameter._Object._ShowAllHeadsOnPayslip
            chkShowEachPayslipOnNewPage.Checked = True 'Sohail (25 Feb 2013)
            chkShowCategory.Checked = ConfigParameter._Object._ShowCategoryOnPayslip 'Sohail (31 Aug 2013)
            'Sohail (19 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            chkShowInformationalHeads.Checked = ConfigParameter._Object._ShowInformationalHeadsOnPayslip
            chkShowTwoPayslipPerPage.Checked = False
            'Sohail (19 Sep 2013) -- End
            'Sohail (23 Dec 2013) -- Start
            'Enhancement - Oman
            chkDOB.Checked = ConfigParameter._Object._ShowBirthDateOnPayslip
            chkAge.Checked = ConfigParameter._Object._ShowAgeOnPayslip
            chkDependents.Checked = ConfigParameter._Object._ShowNoOfDependantsOnPayslip
            chkBasicSalary.Checked = ConfigParameter._Object._ShowMonthlySalaryOnPayslip
            'Sohail (23 Dec 2013) -- End


            'Anjan [01 September 2016] -- Start
            'ENHANCEMENT : Including Payslip settings on configuration.
            chkShowBankAccountNo.Checked = ConfigParameter._Object._ShowBankAccountNoOnPayslip
            'Anjan [01 Sepetember 2016] -- End

            Select Case ConfigParameter._Object._LogoOnPayslip
                Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                    radLogoCompanyInfo.Checked = True
                Case enLogoOnPayslip.COMPANY_DETAILS
                    radCompanyDetails.Checked = True
                Case enLogoOnPayslip.LOGO
                    radLogo.Checked = True
            End Select
            'Sohail (16 May 2012) -- End
            cboLeaveType.SelectedValue = ConfigParameter._Object._LeaveTypeOnPayslip 'Sohail (31 Aug 2012)

            'Hemant (23 Dec 2020) -- Start
            'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
            chkShowRemainingNoofLoanInstallments.Checked = ConfigParameter._Object._ShowRemainingNoOfLoanInstallmentsOnPayslip
            'Hemant (23 Dec 2020) -- End

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            cboEmpSortBy.SelectedValue = enAllocation.DEPARTMENT
            radAscending.Checked = True
            'Sohail (14 Jun 2013) -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            cboOT1Hours.SelectedValue = ConfigParameter._Object._OT1HourHeadId
            cboOT1Amount.SelectedValue = ConfigParameter._Object._OT1AmountHeadId
            cboOT2Hours.SelectedValue = ConfigParameter._Object._OT2HourHeadId
            cboOT2Amount.SelectedValue = ConfigParameter._Object._OT2AmountHeadId
            cboOT3Hours.SelectedValue = ConfigParameter._Object._OT3HourHeadId
            cboOT3Amount.SelectedValue = ConfigParameter._Object._OT3AmountHeadId
            cboOT4Hours.SelectedValue = ConfigParameter._Object._OT4HourHeadId
            cboOT4Amount.SelectedValue = ConfigParameter._Object._OT4AmountHeadId
            'Sohail (09 Jan 2014) -- End

            'Anjan [07 January 2016] -- Start
            'ENHANCEMENT : setting account no. option on payslip on Rutta's Request.
            'chkShowBankAccountNo.Checked = False
            'Anjan [07 January 2016] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub


    Public Function SetFilter() As Boolean
        'Sohail (11 Nov 2014) -- Start
        'AUMSG Enhancement - Currency option on Payslip Report.
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'Dim StartDate As Date = Nothing
        'Dim EndDate As Date = Nothing
        StartDate = Nothing
        EndDate = Nothing
        'Sohail (21 Aug 2015) -- End
        strEmployeeId = ""
        strPeriodId = ""
        'Sohail (11 Nov 2014) -- End
        Try
            '------------------------------------------------------------------------------------------------
            'For all Configuration related properties changes, please do same changes on '...\eZeePersonnel\Forms\Common Forms\Sending Mail\frmSendMail.vb' - > Export_EPayslip method
            '------------------------------------------------------------------------------------------------

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'If lvEmployee.CheckedItems.Count = 0 Then
            If dvEmployee.Table.Select("IsChecked = 1 ").Length = 0 Then
                'Sohail (11 May 2017) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one Employee."), enMsgBoxStyle.Information)
                Exit Function
            End If

            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'If lvPeriod.CheckedItems.Count = 0 Then
            If dvPeriod.Table.Select("IsChecked = 1 ").Length = 0 Then
                'Sohail (11 May 2017) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Period."), enMsgBoxStyle.Information)
                Exit Function
            End If
            'Sohail (11 Nov 2014) -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            If CInt(cboOT1Hours.SelectedValue) > 0 AndAlso CInt(cboOT1Amount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Head for OT1 Amount."), enMsgBoxStyle.Information)
                cboOT1Amount.Focus()
                Exit Function
            ElseIf CInt(cboOT2Hours.SelectedValue) > 0 AndAlso CInt(cboOT2Amount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Head for OT2 Amount."), enMsgBoxStyle.Information)
                cboOT2Amount.Focus()
                Exit Function
            ElseIf CInt(cboOT3Hours.SelectedValue) > 0 AndAlso CInt(cboOT3Amount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Head for OT3 Amount."), enMsgBoxStyle.Information)
                cboOT3Amount.Focus()
                Exit Function
            ElseIf CInt(cboOT4Hours.SelectedValue) > 0 AndAlso CInt(cboOT4Amount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Head for OT4 Amount."), enMsgBoxStyle.Information)
                cboOT4Amount.Focus()
                Exit Function
            ElseIf CInt(cboOT1Amount.SelectedValue) > 0 AndAlso CInt(cboOT1Hours.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Head for OT1 Hours."), enMsgBoxStyle.Information)
                cboOT1Hours.Focus()
                Exit Function
            ElseIf CInt(cboOT2Amount.SelectedValue) > 0 AndAlso CInt(cboOT2Hours.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Head for OT2 Hours."), enMsgBoxStyle.Information)
                cboOT2Hours.Focus()
                Exit Function
            ElseIf CInt(cboOT3Amount.SelectedValue) > 0 AndAlso CInt(cboOT3Hours.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Head for OT3 Hours."), enMsgBoxStyle.Information)
                cboOT3Hours.Focus()
                Exit Function
            ElseIf CInt(cboOT4Amount.SelectedValue) > 0 AndAlso CInt(cboOT4Hours.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Head for OT4 Hours."), enMsgBoxStyle.Information)
                cboOT4Hours.Focus()
                Exit Function
            End If
            'Sohail (09 Jan 2014) -- End


            objEmpPaySlip.SetDefaultValue()

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'Dim allEmp As List(Of String) = (From lv In lvEmployee.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
            Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
            'Sohail (11 May 2017) -- End
            strEmployeeId = String.Join(",", allEmp.ToArray)

            'Sohail (21 May 2016) -- Start
            'Enhancement - 60.1 - Show Period wise allocations on Payslip Report.
            Dim dicPeriod As New Dictionary(Of Integer, String)
            'Sohail (21 May 2016) -- End

            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'For Each lvItem As ListViewItem In lvPeriod.CheckedItems
            '    strPeriodId &= "" & lvItem.Tag.ToString & ","
            '    If StartDate = Nothing Then StartDate = eZeeDate.convertDate(lvItem.SubItems(colhStart.Index).Text)
            '    EndDate = eZeeDate.convertDate(lvItem.SubItems(colhEnd.Index).Text)
            '    dicPeriod.Add(CInt(lvItem.Tag), lvItem.SubItems(colhEnd.Index).Text) 'Sohail (21 May 2016)
            'Next
            'strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
            Dim allPeriod As List(Of String) = (From p In dvPeriod.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("periodunkid").ToString)).ToList
            strPeriodId = String.Join(",", allPeriod.ToArray)
            For Each dtRow As DataRow In dvPeriod.Table.Select("IsChecked = 1 ")
                If StartDate = Nothing Then StartDate = eZeeDate.convertDate(dtRow.Item("start_date").ToString)
                EndDate = eZeeDate.convertDate(dtRow.Item("end_date").ToString)
                dicPeriod.Add(CInt(dtRow.Item("periodunkid")), dtRow.Item("end_date").ToString)
            Next
            'Sohail (11 May 2017) -- End

            'Sohail (21 May 2016) -- Start
            'Enhancement - 60.1 - Show Period wise allocations on Payslip Report.
            objEmpPaySlip._Dic_Period = dicPeriod
            'Sohail (21 May 2016) -- End

            objEmpPaySlip._EmployeeIds = strEmployeeId
            objEmpPaySlip._PeriodIds = strPeriodId

            objEmpPaySlip._PeriodStart = StartDate
            objEmpPaySlip._PeriodEnd = EndDate
            'Sohail (11 Nov 2014) -- End

            objEmpPaySlip._MembershipId = cboMembership.SelectedValue
            objEmpPaySlip._MembershipName = cboMembership.Text

            'Vimal (30 Nov 2010) -- Start 
            objEmpPaySlip._LeaveTypeId = cboLeaveType.SelectedValue
            objEmpPaySlip._LeaveTypeName = cboLeaveType.Text
            'Vimal (30 Nov 2010) -- End

            'Vimal (27 Nov 2010) -- Start 
            


            If rabEmployee.Checked = True Then
                'objEmpPaySlip._Employee = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.Employee
            ElseIf rabDept.Checked = True Then
                'objEmpPaySlip._Department = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.DeptInDeptGroup
            ElseIf rabClass.Checked = True Then
                'objEmpPaySlip._Class = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.ClassInClassGroup
            ElseIf rabCostCenter.Checked = True Then
                'objEmpPaySlip._CostCenter = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.CostCenter
            ElseIf rabGrade.Checked = True Then
                'objEmpPaySlip._Grade = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.Grade
            ElseIf rabJob.Checked = True Then
                'objEmpPaySlip._Job = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.JobInJobGroup
            ElseIf rabPayPoint.Checked = True Then
                'objEmpPaySlip._PayPoint = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.PayPoint
            ElseIf rabSection.Checked = True Then
                'objEmpPaySlip._Section = True
                objEmpPaySlip._AnalysisRefId = enAnalysisRef.SectionInDepartment
            End If

            'Vimal (27 Nov 2010) -- End


            'Sandeep [ 07 APRIL 2011 ] -- Start
            objEmpPaySlip._IgnoreZeroValueHead = CBool(chkIgnoreZeroHead.CheckState)
            'Sandeep [ 07 APRIL 2011 ] -- End 

            'Sohail (23 Apr 2011) -- Start
            objEmpPaySlip._ShowLoanBalance = chkShowLoanBalance.Checked
            objEmpPaySlip._ShowSavingBalance = chkShowSavingBalance.Checked
            'Sohail (23 Apr 2011) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objEmpPaySlip._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            objEmpPaySlip._ShowAllHeads = chkShowAllHeads.Checked 'Sohail (12 Oct 2011)

            'Anjan (30 Aug 2011)-Start
            'Issue : Including logo setting.

            objEmpPaySlip._IsLogoCompanyInfo = radLogoCompanyInfo.Checked
            objEmpPaySlip._IsOnlyLogo = radLogo.Checked
            objEmpPaySlip._IsOnlyCompanyInfo = radCompanyDetails.Checked

            'Anjan (30 Aug 2011)-End
            objEmpPaySlip._ShowEmployerContribution = chkShowEmployerContrib.Checked
            objEmpPaySlip._ShowCumulativeAccrual = chkShowCumulativeAccrual.Checked 'Sohail (11 Sep 2012)
            objEmpPaySlip._ShowEachPayslipOnNewPage = chkShowEachPayslipOnNewPage.Checked 'Sohail (25 Feb 2013)
            objEmpPaySlip._ShowCategory = chkShowCategory.Checked  'Sohail (31 Aug 2013)
            'Sohail (19 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            objEmpPaySlip._ShowInformationalHeads = chkShowInformationalHeads.Checked
            objEmpPaySlip._ShowTwoPayslipsPerPage = chkShowTwoPayslipPerPage.Checked
            'Sohail (19 Sep 2013) -- End

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            objEmpPaySlip._EmpSortById = CInt(cboEmpSortBy.SelectedValue)
            If radAscending.Checked = True Then
                objEmpPaySlip._EmpSortByDESC = False
            Else
                objEmpPaySlip._EmpSortByDESC = True
            End If
            'Sohail (14 Jun 2013) -- End


            'Anjan [17 Dev 2013 ] -- Start
            'ENHANCEMENT : Requested by Rutta
            objEmpPaySlip._ShowAge = chkAge.Checked
            objEmpPaySlip._ShowBirthDate = chkDOB.Checked
            objEmpPaySlip._ShowMonthlySalary = chkBasicSalary.Checked
            objEmpPaySlip._ShowNo_of_Dependents = chkDependents.Checked
            'Anjan [17 Dec 2013 ] -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            objEmpPaySlip._OT1HourHeadId = CInt(cboOT1Hours.SelectedValue)
            objEmpPaySlip._OT1HourHeadName = cboOT1Hours.Text

            objEmpPaySlip._OT1AmountHeadId = CInt(cboOT1Amount.SelectedValue)
            objEmpPaySlip._OT1AmountHeadName = cboOT1Amount.Text

            objEmpPaySlip._OT2HourHeadId = CInt(cboOT2Hours.SelectedValue)
            objEmpPaySlip._OT2HourHeadName = cboOT2Hours.Text

            objEmpPaySlip._OT2AmountHeadId = CInt(cboOT2Amount.SelectedValue)
            objEmpPaySlip._OT2AmountHeadName = cboOT2Amount.Text

            objEmpPaySlip._OT3HourHeadId = CInt(cboOT3Hours.SelectedValue)
            objEmpPaySlip._OT3HourHeadName = cboOT3Hours.Text

            objEmpPaySlip._OT3AmountHeadId = CInt(cboOT3Amount.SelectedValue)
            objEmpPaySlip._OT3AmountHeadName = cboOT3Amount.Text

            objEmpPaySlip._OT4HourHeadId = CInt(cboOT4Hours.SelectedValue)
            objEmpPaySlip._OT4HourHeadName = cboOT4Hours.Text

            objEmpPaySlip._OT4AmountHeadId = CInt(cboOT4Amount.SelectedValue)
            objEmpPaySlip._OT4AmountHeadName = cboOT4Amount.Text
            'Sohail (09 Jan 2014) -- End
            objEmpPaySlip._ShowSalaryOnHold = ConfigParameter._Object._ShowSalaryOnHoldOnPayslip 'Sohail (24 Feb 2014)

            objEmpPaySlip._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            objEmpPaySlip._CurrencyCountryId = CInt(cboCurrency.SelectedValue)
            objEmpPaySlip._CurrencyName = cboCurrency.Text
            'Sohail (11 Nov 2014) -- End


            'Anjan [07 January 2016] -- Start
            'ENHANCEMENT : setting account no. option on payslip on Rutta's Request.
            objEmpPaySlip._ShowBankAccountNo = chkShowBankAccountNo.Checked
            'Anjan [07 January 2016] -- End

            'Sohail (03 Oct 2017) -- Start
            'Enhancement - 70.1 - Show loan received on payslip as information, When loan is received (paid cash or by deposited) the amount received is not reflected on Payslip. - ((RefNo: 43))
            objEmpPaySlip._ShowLoanReceived = chkShowLoanReceived.Checked
            'Sohail (03 Oct 2017) -- End

            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            objEmpPaySlip._ApplyUserAccessFilter = True
            'Sohail ((13 Feb 2016) -- End


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objEmpPaySlip._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objEmpPaySlip._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (18-Nov-2016) -- End

            'Hemant (23 Dec 2020) -- Start
            'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
            objEmpPaySlip._ShowRemainingNoofLoanInstallments = chkShowRemainingNoofLoanInstallments.Checked
            'Hemant (23 Dec 2020) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'Sohail (11 May 2017) -- Start
    'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchPeriodText()
        Try
            mstrSearchPeriodText = lblSearchPeriod.Text
            With txtSearchPeriod
                .ForeColor = Color.Gray
                .Text = mstrSearchPeriodText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchPeriodText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            'Sohail (15 Jan 2019) -- Start
            'SIC Enhancement - 76.1 - New Payslip Template 18 same as Template 11 with Calibri fonts and Size 11 and 2 payslip per page option.
            dvEmployee.Table.AcceptChanges()
            'Sohail (15 Jan 2019) -- End

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetPeriodCheckBoxValue()
        Try

            RemoveHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgPeriod.CurrentRow.Cells(objdgperiodcolhCheck.Index).Value)

            Dim intCount As Integer = dvPeriod.Table.Select("IsChecked = 1").Length

            If intCount <= 0 Then
                objchkPeriodSelectAll.CheckState = CheckState.Unchecked
            ElseIf intCount < dgPeriod.Rows.Count Then
                objchkPeriodSelectAll.CheckState = CheckState.Indeterminate
            ElseIf intCount = dgPeriod.Rows.Count Then
                objchkPeriodSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub
    'Sohail (11 May 2017) -- End

    'Sohail (15 May 2020) -- Start
    'NMB Enhancement # : Option Don't Show Payslip On ESS If Payment is Not Done on aruti configuration.
    Private Sub ESSLanguage()
        Dim strTmpMsg As String
        Try
            strTmpMsg = Language.getMessage(mstrModuleName, 14, "Please select Period.")
            strTmpMsg = Language.getMessage(mstrModuleName, 15, "Sorry! Payment is not done for the selected period.")
            strTmpMsg = Language.getMessage(mstrModuleName, 16, "Sorry! Payment is not Authorized for the selected period.")
            strTmpMsg = Language.getMessage(mstrModuleName, 17, "You can not see your payslip now, Reason: As per the Company Policy, you will able to see your payslip on")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ESSLanguage", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 May 2020) -- End

#End Region


#Region "Form's Events"

    Private Sub frmEmployeePaySlip_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpPaySlip = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeePaySlip_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objEmpPaySlip._ReportName
            Me._Message = objEmpPaySlip._ReportDesc

            Call FillCombo()
            Call FillList()
            'Call ResetValue() 'Sohail (11 Sep 2014)
            'Sohail (28 Jun 2011) -- Start
            Call FillEmpList()
            'Sohail (28 Jun 2011) -- End

            

            'Sohail (31 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._PayslipTemplate = enPayslipTemplate.TAMA_6 OrElse ConfigParameter._Object._PayslipTemplate = enPayslipTemplate.VOLTAMP_7 Then
                chkShowCategory.Visible = True
            Else
                chkShowCategory.Visible = False
            End If
            'Sohail (31 Aug 2013) -- End

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            If ConfigParameter._Object._PayslipTemplate = enPayslipTemplate.FINCA_4 OrElse ConfigParameter._Object._PayslipTemplate = enPayslipTemplate.FINCA_DRC_10 OrElse ConfigParameter._Object._PayslipTemplate = enPayslipTemplate.TRA_2 Then
                lblCurrency.Visible = False
                cboCurrency.Visible = False
            Else
                lblCurrency.Visible = True
                cboCurrency.Visible = True
            End If
            'Sohail (11 Nov 2014) -- End

            'Sohail (23 Dec 2013) -- Start
            'Enhancement - Oman
            chkDOB.Visible = False
            chkAge.Visible = False
            chkBasicSalary.Visible = False
            chkDependents.Visible = False
            chkDOB.Checked = False
            chkAge.Checked = False
            chkBasicSalary.Checked = False
            chkDependents.Checked = False
            'Sohail (23 Dec 2013) -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            lblOT1Hours.Visible = False
            lblOT2Hours.Visible = False
            lblOT3Hours.Visible = False
            lblOT4Hours.Visible = False
            lblOT1Amount.Visible = False
            lblOT2Amount.Visible = False
            lblOT3Amount.Visible = False
            lblOT4Amount.Visible = False
            cboOT1Hours.Visible = False
            cboOT2Hours.Visible = False
            cboOT3Hours.Visible = False
            cboOT4Hours.Visible = False
            cboOT1Amount.Visible = False
            cboOT2Amount.Visible = False
            cboOT3Amount.Visible = False
            cboOT4Amount.Visible = False
            'Sohail (09 Jan 2014) -- End

            'Sohail (19 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            Select Case ConfigParameter._Object._PayslipTemplate
                Case enPayslipTemplate.TAMA_6, enPayslipTemplate.GHANA_9, enPayslipTemplate.IBA_16, enPayslipTemplate.INDONESIA_11, enPayslipTemplate.SIC_18, enPayslipTemplate.EKO_SUPREME_20 'Sohail (11 Sep 2015) - [enPayslipTemplate.GHANA_9]
                    'Hemant (24 May 2024) -- [enPayslipTemplate.EKO_SUPREME_20]
                    'Sohail (15 Jan 2019) - [INDONESIA_11, SIC_18]
                    'Nilay (03-Oct-2016) -- [enPayslipTemplate.IBA_16]
                    chkShowTwoPayslipPerPage.Visible = True
                Case enPayslipTemplate.VOLTAMP_7
                    chkShowTwoPayslipPerPage.Visible = True
                    chkShowTwoPayslipPerPage.Checked = True
                    'Sohail (09 Jan 2014) -- Start
                    'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
                    'Sohail (19 Jul 2014) -- Start
                    'Enhancement - New Payslip Template 9 for GHANA.
                    'lblOT1Hours.Visible = True
                    'lblOT2Hours.Visible = True
                    'lblOT3Hours.Visible = True
                    'lblOT4Hours.Visible = True
                    'lblOT1Amount.Visible = True
                    'lblOT2Amount.Visible = True
                    'lblOT3Amount.Visible = True
                    'lblOT4Amount.Visible = True
                    'cboOT1Hours.Visible = True
                    'cboOT2Hours.Visible = True
                    'cboOT3Hours.Visible = True
                    'cboOT4Hours.Visible = True
                    'cboOT1Amount.Visible = True
                    'cboOT2Amount.Visible = True
                    'cboOT3Amount.Visible = True
                    'cboOT4Amount.Visible = True
                    'Sohail (19 Jul 2014) -- End
                    'Sohail (09 Jan 2014) -- End
                    'Sohail (23 Dec 2013) -- Start
                    'Enhancement - Oman
                    'Sohail (11 Sep 2014) -- Start
                    'Enhancement - New Payslip Template 10 for FINCA DRC.
                Case enPayslipTemplate.FINCA_4, enPayslipTemplate.FINCA_DRC_10
                    'Case enPayslipTemplate.FINCA_4
                    'Sohail (11 Sep 2014) -- End
                    chkDOB.Visible = True
                    chkAge.Visible = True
                    chkBasicSalary.Visible = True
                    chkDependents.Visible = True
                    'Sohail (23 Dec 2013) -- End
                Case Else
                    chkShowTwoPayslipPerPage.Visible = False
                    chkShowTwoPayslipPerPage.Checked = False
            End Select
            'Sohail (19 Sep 2013) -- End

            'Hemant (23 Dec 2020) -- Start
            'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
            If ConfigParameter._Object._PayslipTemplate = enPayslipTemplate.ONE_SIDED_KBC_13 Then
                chkShowRemainingNoofLoanInstallments.Visible = True
                chkShowRemainingNoofLoanInstallments.Visible = True
            Else
                chkShowRemainingNoofLoanInstallments.Visible = False
                chkShowRemainingNoofLoanInstallments.Visible = False
            End If
            'Hemant (23 Dec 2020) -- End

            Call ResetValue()  'Sohail (11 Sep 2014)

            'Vimal (15 Dec 2010) -- Start /Sohail -Anjan (31 Oct 2014)- Start
            If mblnPaySlip = True Then
                Dim arrEmp As String()
                Dim arrPeriod As String()

                arrEmp = mstrEmpIds.Split(",")
                'Sohail (11 May 2017) -- Start
                'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
                'For m As Integer = 0 To lvEmployee.Items.Count - 1
                '    For n As Integer = 0 To arrEmp.Length - 1
                '        If CInt(lvEmployee.Items(m).Tag) = CInt(arrEmp(n)) Then
                '            lvEmployee.Items(m).Checked = True
                '        End If
                '    Next
                'Next
                Dim lstEmp As List(Of DataRow) = (From p In dvEmployee.Table Where (arrEmp.Contains(p.Item("employeeunkid").ToString)) Select (p)).ToList
                For Each dtRow As DataRow In lstEmp
                    dtRow.Item("IsChecked") = True
                Next
                dvEmployee.Table.AcceptChanges()
                If dgEmployee.RowCount > 0 Then dgEmployee.CurrentCell = dgEmployee.Rows(0).Cells(0)
                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
                'Sohail (11 May 2017) -- End

                arrPeriod = mstrPeriodIds.Split(",")
                'Sohail (11 May 2017) -- Start
                'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
                'For p As Integer = 0 To lvPeriod.Items.Count - 1
                '    For q As Integer = 0 To arrPeriod.Length - 1
                '        If CInt(lvPeriod.Items(p).Tag) = CInt(arrPeriod(q)) Then
                '            lvPeriod.Items(p).Checked = True
                '        End If
                '    Next
                'Next
                Dim lstPeriod As List(Of DataRow) = (From p In dvPeriod.Table Where (arrPeriod.Contains(p.Item("periodunkid").ToString)) Select (p)).ToList
                For Each dtRow As DataRow In lstPeriod
                    dtRow.Item("IsChecked") = True
                Next
                dvPeriod.Table.AcceptChanges()
                If dgPeriod.RowCount > 0 Then dgPeriod.CurrentCell = dgPeriod.Rows(0).Cells(0)
                'Sohail (11 May 2017) -- End

                arrEmp = Nothing
                arrPeriod = Nothing
            End If

            'Vimal (15 Dec 2010) -- End /Sohail -Anjan (31 Oct 2014)- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_Load", mstrModuleName)
        End Try
    End Sub


    Private Sub frmEmployeePaySlip_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeePaySlip_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeePaySlip_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_KeyPress", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "Buttons"
    Private Sub frmEmployeePaySlip_Report_Click(ByVal sender As System.Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles MyBase.Report_Click
        'Sohail (11 Nov 2014) -- Start
        'AUMSG Enhancement - Currency option on Payslip Report.
        'strEmployeeId = ""
        'strPeriodId = ""
        ''Sohail (17 Apr 2012) -- Start
        ''TRA - ENHANCEMENT
        'Dim StartDate As Date = Nothing
        'Dim EndDate As Date = Nothing
        ''Sohail (17 Apr 2012) -- End
        'Sohail (11 Nov 2014) -- End
        Try
            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            'If lvEmployee.CheckedItems.Count = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one Employee."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'If lvPeriod.CheckedItems.Count = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Period."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Sohail (11 Nov 2014) -- End


            'Sohail (23 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'If lvEmployee.CheckedItems.Count > 0 Then
            '    For i As Integer = 0 To lvEmployee.CheckedItems.Count - 1
            '        strEmployeeId &= "" & lvEmployee.CheckedItems(i).Tag.ToString & ","
            '    Next
            '    strEmployeeId = strEmployeeId.Substring(0, strEmployeeId.Length - 1)
            'End If

            ''Vimal (30 Nov 2010) -- Start 
            'If lvPeriod.CheckedItems.Count > 0 Then
            '    For j As Integer = 0 To lvPeriod.CheckedItems.Count - 1
            '        strPeriodId &= "" & lvPeriod.CheckedItems(j).Tag.ToString & ","
            '        'Sohail (17 Apr 2012) -- Start
            '        'TRA - ENHANCEMENT
            '        If StartDate = Nothing Then StartDate = eZeeDate.convertDate(lvPeriod.CheckedItems(j).SubItems(colhStart.Index).Text)
            '        EndDate = eZeeDate.convertDate(lvPeriod.CheckedItems(j).SubItems(colhEnd.Index).Text)
            '        'Sohail (17 Apr 2012) -- End
            '    Next
            '    strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
            'End If
            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            'For Each lvItem As ListViewItem In lvEmployee.CheckedItems
            '    strEmployeeId &= "" & lvItem.Tag.ToString & ","
            'Next
            'strEmployeeId = strEmployeeId.Substring(0, strEmployeeId.Length - 1)

            'For Each lvItem As ListViewItem In lvPeriod.CheckedItems
            '    strPeriodId &= "" & lvItem.Tag.ToString & ","
            '    If StartDate = Nothing Then StartDate = eZeeDate.convertDate(lvItem.SubItems(colhStart.Index).Text)
            '    EndDate = eZeeDate.convertDate(lvItem.SubItems(colhEnd.Index).Text)
            '    Next
            '    strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
            ''Sohail (23 Jun 2012) -- End

            ''Issue: Get PeriodEndDate of only one period selected for Leave Details.
            ''If lvPeriod.CheckedItems.Count > 1 Then
            ''    For j As Integer = 0 To lvPeriod.CheckedItems.Count - 1
            ''        strPeriodId &= "" & lvPeriod.CheckedItems(j).Tag.ToString & ","
            ''    Next
            ''    strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
            ''ElseIf lvPeriod.CheckedItems.Count = 1 Then
            ''    strPeriodId = lvPeriod.CheckedItems(0).Tag.ToString

            ''    Dim objPeriod As New clscommom_period_Tran
            ''    objPeriod._Periodunkid = CInt(lvPeriod.CheckedItems(0).Tag)
            ''    objEmpPaySlip._PeriodEndDate = objPeriod._End_Date
            ''End If
            ''Vimal (30 Nov 2010) -- End

            'objEmpPaySlip._EmployeeIds = strEmployeeId
            'objEmpPaySlip._PeriodIds = strPeriodId

            ''Sohail (17 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            'objEmpPaySlip._PeriodStart = StartDate
            'objEmpPaySlip._PeriodEnd = EndDate
            ''Sohail (17 Apr 2012) -- End
            'Sohail (11 Nov 2014) -- End

            If SetFilter() = False Then Exit Sub

            'Vimal (27 Nov 2010) -- Start 
            'objEmpPaySlip.generateReport(Aruti.Data.enReportType.Standard, Aruti.Data.enPrintAction.Preview, Aruti.Data.enExportAction.None)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmpPaySlip.generateReport(0, e.Type, enExportAction.None)
            objEmpPaySlip.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, StartDate, EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
            'Vimal (27 Nov 2010) -- End

            'Sohail (07 Nov 2014) -- Start
            'Enhancement - Message handling on payslip.
            If objEmpPaySlip._Message <> "" Then
                eZeeMsgBox.Show(objEmpPaySlip._Message, enMsgBoxStyle.Information)
            End If
            'Sohail (07 Nov 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeePaySlip_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        'Sohail (11 Nov 2014) -- Start
        'AUMSG Enhancement - Currency option on Payslip Report.
        'strEmployeeId = ""
        'strPeriodId = ""
        ''Sohail (17 Apr 2012) -- Start
        ''TRA - ENHANCEMENT
        'Dim StartDate As Date = Nothing
        'Dim EndDate As Date = Nothing
        ''Sohail (17 Apr 2012) -- End
        'Sohail (11 Nov 2014) -- End
        Try
            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            'If lvEmployee.CheckedItems.Count = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one Employee."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'If lvPeriod.CheckedItems.Count = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Period."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Sohail (11 Nov 2014) -- End

            'If lvEmployee.CheckedItems.Count > 0 Then
            '    For i As Integer = 0 To lvEmployee.CheckedItems.Count - 1
            '        strEmployeeId &= "" & lvEmployee.CheckedItems(i).Tag.ToString & ","
            '    Next
            '    strEmployeeId = strEmployeeId.Substring(0, strEmployeeId.Length - 1)
            'End If


            'Vimal (30 Nov 2010) -- Start 
            'If lvPeriod.CheckedItems.Count > 0 Then
            '    For j As Integer = 0 To lvPeriod.CheckedItems.Count - 1
            '        strPeriodId &= "" & lvPeriod.CheckedItems(j).Tag.ToString & ","
            '    Next
            '    strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
            'End If

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            'For Each lvItem As ListViewItem In lvEmployee.CheckedItems
            '    strEmployeeId &= "" & lvItem.Tag.ToString & ","
            'Next
            'strEmployeeId = strEmployeeId.Substring(0, strEmployeeId.Length - 1)

            'For Each lvItem As ListViewItem In lvPeriod.CheckedItems
            '    strPeriodId &= "" & lvItem.Tag.ToString & ","
            '    If StartDate = Nothing Then StartDate = eZeeDate.convertDate(lvItem.SubItems(colhStart.Index).Text)
            '    EndDate = eZeeDate.convertDate(lvItem.SubItems(colhEnd.Index).Text)
            'Next
            'strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)

            'objEmpPaySlip._EmployeeIds = strEmployeeId
            'objEmpPaySlip._PeriodIds = strPeriodId

            ''Sohail (17 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            'objEmpPaySlip._PeriodStart = StartDate
            'objEmpPaySlip._PeriodEnd = EndDate
            ''Sohail (17 Apr 2012) -- End
            'Sohail (11 Nov 2014) -- End

            If SetFilter() = False Then Exit Sub
            'Vimal (27 Nov 2010) -- Start 
            'objEmpPaySlip.generateReport(Aruti.Data.enReportType.Standard, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmpPaySlip.generateReport(0, enPrintAction.None, e.Type)
            objEmpPaySlip.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, StartDate, EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
            'Vimal (27 Nov 2010) -- End

            'Sohail (07 Nov 2014) -- Start
            'Enhancement - Message handling on payslip.
            If objEmpPaySlip._Message <> "" Then
                eZeeMsgBox.Show(objEmpPaySlip._Message, enMsgBoxStyle.Information)
            End If
            'Sohail (07 Nov 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeePaySlip_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeePaySlip_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeePaySlip_Cancel_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmEmployeePaySlip_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPaySlip.SetMessages()
            objfrm._Other_ModuleNames = "clsPaySlip"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeePaySlip_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


#End Region

#Region "Controls"

    'Sohail (11 May 2017) -- Start
    'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
    'Private Sub chkSelectallEmployeeList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Call CheckAllEmployee(objchkSelectallEmployeeList.Checked)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkSelectallEmployeeList_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (11 May 2017) -- End

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'For Each lvItem As ListViewItem In lvEmployee.Items
            '    RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            'Next
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvEmployee.ToTable.AcceptChanges()
            End If

            Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
            mintCount = drRow.Length
            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            'Sohail (11 May 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'For Each lvItem As ListViewItem In lvPeriod.Items
            '    RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
            'Next
            If dvPeriod IsNot Nothing Then
                For Each dr As DataRowView In dvPeriod
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvPeriod.ToTable.AcceptChanges()
            End If
            'Sohail (11 May 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllPeriod", mstrModuleName)
        End Try
    End Sub

    'Sohail (11 May 2017) -- Start
    'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
    'Private Sub chkSelectallPeriodList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Call CheckAllPeriod(chkSelectallPeriodList.Checked)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkSelectallPeriodList_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (11 May 2017) -- End

    'Sohail (19 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkShowEachPayslipOnNewPage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowEachPayslipOnNewPage.CheckedChanged
        Try
            If chkShowEachPayslipOnNewPage.Checked = True Then chkShowTwoPayslipPerPage.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowEachPayslipOnNewPage_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowTwoPayslipPerPage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowTwoPayslipPerPage.CheckedChanged
        Try
            If chkShowTwoPayslipPerPage.Checked = True Then chkShowEachPayslipOnNewPage.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowTwoPayslipPerPage_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Sep 2013) -- End

    'Sohail (11 May 2017) -- Start
    'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
    'Private Sub lvEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        If lvEmployee.CheckedItems.Count <= 0 Then
    '            RemoveHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
    '            chkSelectallEmployeeList.CheckState = CheckState.Unchecked
    '            AddHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
    '        ElseIf lvEmployee.CheckedItems.Count < lvEmployee.Items.Count Then
    '            RemoveHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
    '            chkSelectallEmployeeList.CheckState = CheckState.Indeterminate
    '            AddHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
    '        ElseIf lvEmployee.CheckedItems.Count = lvEmployee.Items.Count Then
    '            RemoveHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
    '            chkSelectallEmployeeList.CheckState = CheckState.Checked
    '            AddHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployee_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        If lvPeriod.CheckedItems.Count <= 0 Then
    '            RemoveHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
    '            chkSelectallPeriodList.CheckState = CheckState.Unchecked
    '            AddHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
    '        ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
    '            RemoveHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
    '            chkSelectallPeriodList.CheckState = CheckState.Indeterminate
    '            AddHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
    '        ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
    '            RemoveHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
    '            chkSelectallPeriodList.CheckState = CheckState.Checked
    '            AddHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (11 May 2017) -- End

    'Vimal (10 Dec 2010) -- Start 
    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpPaySlip.setOrderBy(0)
            txtOrderBy.Text = objEmpPaySlip.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
    'Vimal (10 Dec 2010) -- End

    'Sohail (28 Jun 2011) -- Start
    Private Sub chkInactiveemp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInactiveemp.CheckedChanged
        Try
            Call FillEmpList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkInactiveemp_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (28 Jun 2011) -- End


    'Sohail (11 May 2017) -- Start
    'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.GotFocus
        Try
            With txtSearchPeriod
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchPeriodText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.Leave
        Try
            If txtSearchPeriod.Text.Trim = "" Then
                Call SetDefaultSearchPeriodText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.TextChanged
        Try
            If txtSearchPeriod.Text.Trim = mstrSearchPeriodText Then Exit Sub
            If dvPeriod IsNot Nothing Then
                dvPeriod.RowFilter = "period_code LIKE '%" & txtSearchPeriod.Text.Replace("'", "''") & "%'  OR period_name LIKE '%" & txtSearchPeriod.Text.Replace("'", "''") & "%'"
                dgPeriod.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 May 2017) -- End

    'Sohail (21 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            'Sohail (11 May 2017) -- Start
            'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
            'If lvEmployee.Items.Count <= 0 Then Exit Sub 'Sohail (29 Mar 2016)
            'lvEmployee.SelectedIndices.Clear()
            ''Nilay (18-Nov-2015) -- Start
            ''Dim lvFoundItem As ListViewItem = lvEmployee.FindItemWithText(txtSearchEmp.Text, False, 0, True)
            'Dim lvFoundItem As ListViewItem = lvEmployee.FindItemWithText(txtSearchEmp.Text, True, 0, True)
            ''Nilay (18-Nov-2015) -- End
            'If lvFoundItem IsNot Nothing Then
            '    lvEmployee.TopItem = lvFoundItem
            '    lvFoundItem.Selected = True
            'End If
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If
            'Sohail (11 May 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Feb 2012) -- End

#End Region

    'Sohail (09 Jan 2014) -- Start
    'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
#Region " Combobox's Events "
    Private Sub cboOT1Hours_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboOT1Hours.Validating, cboOT1Amount.Validating, _
                                                                                                                         cboOT2Hours.Validating, cboOT2Amount.Validating, _
                                                                                                                         cboOT3Hours.Validating, cboOT3Amount.Validating, _
                                                                                                                         cboOT4Hours.Validating, cboOT4Amount.Validating

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteriaMembership.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboMembership.Name AndAlso t.Name <> cboLeaveType.Name AndAlso t.Name <> cboEmpSortBy.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOT1Hours_Validating", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (09 Jan 2014) -- End

    'Sohail (11 May 2017) -- Start
    'Enhancement - 66.1 - replacing list view with data grid view on payslip report screen.
#Region " CheckBox's Events "
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkPeriodSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkPeriodSelectAll.CheckedChanged
        Try
            Call CheckAllPeriod(objchkPeriodSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " GridView Events "

    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPeriod_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgPeriod.CurrentCellDirtyStateChanged
        Try
            If dgPeriod.IsCurrentCellDirty Then
                dgPeriod.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPeriod_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPeriod_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgPeriod.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgperiodcolhCheck.Index Then
                SetPeriodCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPeriod_CellValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (11 May 2017) -- End

    'Varsha Rana (29-Sept-2017) -- Start
    'Enhancement - 70.001 - add analysis by option on salary slip reports [Allan Wesonga] - (RefNo: 66)
#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillEmpList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub EZeeSearchResetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EZeeSearchResetbtn.Click
        Try
            mstrAdvanceFilter = ""
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillEmpList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_LinkClicked", mstrModuleName)
        End Try
    End Sub
#End Region
    'Varsha Rana (29-Sept-2017) -- End

    'Messages
    '1 "Please Select atleast one Employee."
    '2 "Please Select atleast one Period."
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteriaMembership.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteriaMembership.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteriaEmpPeriod.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteriaEmpPeriod.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteriaMessage.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteriaMessage.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmpSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmpSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.rabPayPoint.Text = Language._Object.getCaption(Me.rabPayPoint.Name, Me.rabPayPoint.Text)
			Me.rabCostCenter.Text = Language._Object.getCaption(Me.rabCostCenter.Name, Me.rabCostCenter.Text)
			Me.rabClass.Text = Language._Object.getCaption(Me.rabClass.Name, Me.rabClass.Text)
			Me.rabGrade.Text = Language._Object.getCaption(Me.rabGrade.Name, Me.rabGrade.Text)
			Me.rabJob.Text = Language._Object.getCaption(Me.rabJob.Name, Me.rabJob.Text)
			Me.rabEmployee.Text = Language._Object.getCaption(Me.rabEmployee.Name, Me.rabEmployee.Text)
			Me.rabDept.Text = Language._Object.getCaption(Me.rabDept.Name, Me.rabDept.Text)
			Me.rabSection.Text = Language._Object.getCaption(Me.rabSection.Name, Me.rabSection.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteriaMembership.Text = Language._Object.getCaption(Me.gbFilterCriteriaMembership.Name, Me.gbFilterCriteriaMembership.Text)
			Me.gbFilterCriteriaEmpPeriod.Text = Language._Object.getCaption(Me.gbFilterCriteriaEmpPeriod.Name, Me.gbFilterCriteriaEmpPeriod.Text)
			Me.gbFilterCriteriaMessage.Text = Language._Object.getCaption(Me.gbFilterCriteriaMessage.Name, Me.gbFilterCriteriaMessage.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.chkIgnoreZeroHead.Text = Language._Object.getCaption(Me.chkIgnoreZeroHead.Name, Me.chkIgnoreZeroHead.Text)
			Me.chkShowSavingBalance.Text = Language._Object.getCaption(Me.chkShowSavingBalance.Name, Me.chkShowSavingBalance.Text)
			Me.chkShowLoanBalance.Text = Language._Object.getCaption(Me.chkShowLoanBalance.Name, Me.chkShowLoanBalance.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.radLogoCompanyInfo.Text = Language._Object.getCaption(Me.radLogoCompanyInfo.Name, Me.radLogoCompanyInfo.Text)
			Me.radLogo.Text = Language._Object.getCaption(Me.radLogo.Name, Me.radLogo.Text)
			Me.radCompanyDetails.Text = Language._Object.getCaption(Me.radCompanyDetails.Name, Me.radCompanyDetails.Text)
			Me.chkShowAllHeads.Text = Language._Object.getCaption(Me.chkShowAllHeads.Name, Me.chkShowAllHeads.Text)
			Me.chkShowEmployerContrib.Text = Language._Object.getCaption(Me.chkShowEmployerContrib.Name, Me.chkShowEmployerContrib.Text)
			Me.chkShowCumulativeAccrual.Text = Language._Object.getCaption(Me.chkShowCumulativeAccrual.Name, Me.chkShowCumulativeAccrual.Text)
			Me.chkShowEachPayslipOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachPayslipOnNewPage.Name, Me.chkShowEachPayslipOnNewPage.Text)
			Me.gbEmpSortBy.Text = Language._Object.getCaption(Me.gbEmpSortBy.Name, Me.gbEmpSortBy.Text)
			Me.lblEmpSortBy.Text = Language._Object.getCaption(Me.lblEmpSortBy.Name, Me.lblEmpSortBy.Text)
			Me.radDescending.Text = Language._Object.getCaption(Me.radDescending.Name, Me.radDescending.Text)
			Me.radAscending.Text = Language._Object.getCaption(Me.radAscending.Name, Me.radAscending.Text)
			Me.chkShowCategory.Text = Language._Object.getCaption(Me.chkShowCategory.Name, Me.chkShowCategory.Text)
			Me.chkShowInformationalHeads.Text = Language._Object.getCaption(Me.chkShowInformationalHeads.Name, Me.chkShowInformationalHeads.Text)
			Me.chkShowTwoPayslipPerPage.Text = Language._Object.getCaption(Me.chkShowTwoPayslipPerPage.Name, Me.chkShowTwoPayslipPerPage.Text)
			Me.chkBasicSalary.Text = Language._Object.getCaption(Me.chkBasicSalary.Name, Me.chkBasicSalary.Text)
			Me.chkAge.Text = Language._Object.getCaption(Me.chkAge.Name, Me.chkAge.Text)
			Me.chkDependents.Text = Language._Object.getCaption(Me.chkDependents.Name, Me.chkDependents.Text)
			Me.chkDOB.Text = Language._Object.getCaption(Me.chkDOB.Name, Me.chkDOB.Text)
			Me.lblOT4Amount.Text = Language._Object.getCaption(Me.lblOT4Amount.Name, Me.lblOT4Amount.Text)
			Me.lblOT3Amount.Text = Language._Object.getCaption(Me.lblOT3Amount.Name, Me.lblOT3Amount.Text)
			Me.lblOT2Amount.Text = Language._Object.getCaption(Me.lblOT2Amount.Name, Me.lblOT2Amount.Text)
			Me.lblOT1Amount.Text = Language._Object.getCaption(Me.lblOT1Amount.Name, Me.lblOT1Amount.Text)
			Me.lblOT4Hours.Text = Language._Object.getCaption(Me.lblOT4Hours.Name, Me.lblOT4Hours.Text)
			Me.lblOT3Hours.Text = Language._Object.getCaption(Me.lblOT3Hours.Name, Me.lblOT3Hours.Text)
			Me.lblOT2Hours.Text = Language._Object.getCaption(Me.lblOT2Hours.Name, Me.lblOT2Hours.Text)
			Me.lblOT1Hours.Text = Language._Object.getCaption(Me.lblOT1Hours.Name, Me.lblOT1Hours.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.chkShowBankAccountNo.Text = Language._Object.getCaption(Me.chkShowBankAccountNo.Name, Me.chkShowBankAccountNo.Text)
			Me.lblSearchPeriod.Text = Language._Object.getCaption(Me.lblSearchPeriod.Name, Me.lblSearchPeriod.Text)
			Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgColhPeriodCode.HeaderText = Language._Object.getCaption(Me.dgColhPeriodCode.Name, Me.dgColhPeriodCode.HeaderText)
			Me.dgColhPeriod.HeaderText = Language._Object.getCaption(Me.dgColhPeriod.Name, Me.dgColhPeriod.HeaderText)
			Me.dgcolhPeriodStart.HeaderText = Language._Object.getCaption(Me.dgcolhPeriodStart.Name, Me.dgcolhPeriodStart.HeaderText)
			Me.dgcolhPeriodEnd.HeaderText = Language._Object.getCaption(Me.dgcolhPeriodEnd.Name, Me.dgcolhPeriodEnd.HeaderText)
			Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
			Me.EZeeSearchResetbtn.Text = Language._Object.getCaption(Me.EZeeSearchResetbtn.Name, Me.EZeeSearchResetbtn.Text)
			Me.chkShowLoanReceived.Text = Language._Object.getCaption(Me.chkShowLoanReceived.Name, Me.chkShowLoanReceived.Text)
            Me.chkShowRemainingNoofLoanInstallments.Text = Language._Object.getCaption(Me.chkShowRemainingNoofLoanInstallments.Name, Me.chkShowRemainingNoofLoanInstallments.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select atleast one Employee.")
			Language.setMessage(mstrModuleName, 2, "Please Select atleast one Period.")
			Language.setMessage(mstrModuleName, 3, "Please select Head for OT1 Amount.")
			Language.setMessage(mstrModuleName, 4, "Please select Head for OT2 Amount.")
			Language.setMessage(mstrModuleName, 5, "Please select Head for OT3 Amount.")
			Language.setMessage(mstrModuleName, 6, "Please select Head for OT4 Amount.")
			Language.setMessage(mstrModuleName, 7, "Please select Head for OT1 Hours.")
			Language.setMessage(mstrModuleName, 8, "Please select Head for OT2 Hours.")
			Language.setMessage(mstrModuleName, 9, "Please select Head for OT3 Hours.")
			Language.setMessage(mstrModuleName, 10, "Please select Head for OT4 Hours.")
			Language.setMessage(mstrModuleName, 11, "Sorry, This transaction head is already mapped.")
			Language.setMessage(mstrModuleName, 12, " Select")
			Language.setMessage(mstrModuleName, 13, " Employee Code")
			Language.setMessage(mstrModuleName, 14, "Please select Period.")
			Language.setMessage(mstrModuleName, 15, "Sorry! Payment is not done for the selected period.")
			Language.setMessage(mstrModuleName, 16, "Sorry! Payment is not Authorized for the selected period.")
			Language.setMessage(mstrModuleName, 17, "You can not see your payslip now, Reason: As per the Company Policy, you will able to see your payslip on")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
