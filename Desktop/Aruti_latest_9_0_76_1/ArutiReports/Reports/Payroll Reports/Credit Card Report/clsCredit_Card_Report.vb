'************************************************************************************************************************************
'Class Name : clsCredit_Card_Report.vb
'Purpose    :
'Date       : 28/09/2024
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsCredit_Card_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCredit_Card_Report"
    Private mstrReportId As String = enArutiReport.Credit_Card_Report  '323
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintLoanSchemeId As Integer = 0
    Private mstrLoanScheme As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLoanApplicationId As Integer = 0
    Private mstrLoanApplicationNo As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "

    Public WriteOnly Property _LoanSchemeId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _LoanScheme() As String
        Set(ByVal value As String)
            mstrLoanScheme = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanApplicationId() As Integer
        Set(ByVal value As Integer)
            mintLoanApplicationId = value
        End Set
    End Property

    Public WriteOnly Property _LoanApplicationNo() As String
        Set(ByVal value As String)
            mstrLoanApplicationNo = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintLoanSchemeId = 0
            mstrLoanScheme = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintLoanApplicationId = 0
            mstrLoanApplicationNo = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Try
            If mintLoanSchemeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Loan Scheme: ") & " " & mstrLoanScheme & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 36, "Employee: ") & " " & mstrEmployeeName & " "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(formno, '')", Language.getMessage(mstrModuleName, 1, "Form No")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(lvleavetype_master.leavename, '')", Language.getMessage(mstrModuleName, 2, "Leave Type")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 3, "Employee Code")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 4, "Employee")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(appr.employeecode, '')", Language.getMessage(mstrModuleName, 6, "Approver Code")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(appr.firstname, '') + ' ' + ISNULL(appr.othername, '') + ' ' + ISNULL(appr.surname, '')", Language.getMessage(mstrModuleName, 7, "Approver")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(lvapproverlevel_master.levelname, '')", Language.getMessage(mstrModuleName, 8, "Level")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objDataOperation = New clsDataOperation


            Dim dtEmployee As DataTable = GetEmployeeDetails(mintEmployeeId, False)


            objDataOperation.ClearParameters()

            StrQ = " SELECT employeeunkid, application_date	,application_no, loan_amount,(loan_amount * 30)/100 AS cash_limit,lnloan_process_pending_loan.loanschemeunkid,ISNULL(ln.name,'') AS LoanScheme  " & _
                       " FROM lnloan_process_pending_loan  " & _
                       " LEFT JOIN lnloan_scheme_master ln ON ln.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                       " WHERE isvoid = 0 AND loan_statusunkid not in (5,6)  AND employeeunkid = @employeeunkid "

            If mintLoanApplicationId > 0 Then
                StrQ &= " AND processpendingloanunkid = @processpendingloanunkid"
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanApplicationId)
            End If

            If mintLoanSchemeId > 0 Then
                StrQ &= " AND lnloan_process_pending_loan.loanschemeunkid = @loanschemeunkid"
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeId)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            Dim dsEmpLoanDetails As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If dsEmpLoanDetails IsNot Nothing AndAlso dsEmpLoanDetails.Tables(0).Rows.Count > 0 Then
                Dim objEmpBank As New clsEmployeeBanks
                Dim dsEmpBank As DataSet = Nothing
                Dim objMaster As New clsMasterData
                Dim objReportingTo As New clsReportingToEmployee

                Dim strarrGroupColumns As String() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell
                Dim intArrayColumnWidth As Integer() = Nothing

                FilterTitleAndFilterQuery()

                Dim mdtTableExcel As New DataTable
                mdtTableExcel.Columns.Add("SrNo", Type.GetType("System.Int32")).Caption = Language.getMessage(mstrModuleName, 1, "Sr No")
                mdtTableExcel.Columns.Add("C1", Type.GetType("System.String")).Caption = ""
                mdtTableExcel.Columns.Add("C2", Type.GetType("System.String")).Caption = ""
                mdtTableExcel.Columns.Add("C3", Type.GetType("System.String")).Caption = ""
                mdtTableExcel.Columns.Add("C4", Type.GetType("System.String")).Caption = ""

                For Each dr As DataRow In dsEmpLoanDetails.Tables(0).Rows
                    dsEmpBank = objEmpBank.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, CDate(dr("application_date")).Date, CDate(dr("application_date")).Date, xUserModeSetting, xOnlyApproved, False, "List", IIf(xUserUnkid > 0, True, False), "", mintEmployeeId.ToString(), CDate(dr("application_date")).Date, "end_date DESC")
                    objReportingTo._EmployeeUnkid(CDate(dr("application_date")).Date, Nothing, False) = mintEmployeeId
                    Dim dtReportingTo As DataTable = objReportingTo._RDataTable

                    If dtReportingTo IsNot Nothing AndAlso dtReportingTo.Rows.Count > 0 Then
                        Dim dtEmpReporting As DataTable = GetEmployeeDetails(CInt(dtReportingTo.Rows(0)("reporttoemployeeunkid")), True)

                        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

                        'SET EXCEL CELL WIDTH  

                        ReDim intArrayColumnWidth(4)
                        For i As Integer = 0 To intArrayColumnWidth.Length - 1
                            If i = 0 Then
                                intArrayColumnWidth(i) = 50
                            Else
                                intArrayColumnWidth(i) = 200
                            End If
                        Next
                        'SET EXCEL CELL WIDTH


                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayHeader.Add(row)


                        row = New WorksheetRow()
                        If Me._FilterTitle.ToString.Length > 0 Then
                            wcell = New WorksheetCell(Me._FilterTitle.ToString().Trim() & " " & Language.getMessage(mstrModuleName, 37, "Loan Application No : " & dr("application_no").ToString()), "s9bw")
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                            row.Cells.Add(wcell)
                        End If
                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                        ' START CREDIT LIMIT APPLIED
                        Dim mintSrNo As Integer = 1

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Credit Limit Applied"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(CDec(dr("loan_amount")).ToString(GUI.fmtCurrency), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)
                        ' END CREDIT LIMIT APPLIED


                        ' START CASH LIMIT
                        mintSrNo += 1
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "Cash Limit"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(CDec(dr("cash_limit")).ToString(GUI.fmtCurrency), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)
                        ' END CASH LIMIT


                        ' START LOAN SCHEME
                        Dim objCCLoanScheme As New clsLoan_Scheme
                        Dim dsLoanScheme As DataSet = objCCLoanScheme.getComboList(False, "List", -1, " ISNULL(loanschemecategory_id,0) = " & enLoanSchemeCategories.CREDIT_CARD, False, False, "")

                        Dim cnt As Integer = 0
                        For Each drRow As DataRow In dsLoanScheme.Tables(0).Rows

                            row = New WorksheetRow()
                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            If cnt = 0 Then
                                wcell = New WorksheetCell(Space(10) & Language.getMessage(mstrModuleName, 4, "Card Type"), "s9w")
                            Else
                                wcell = New WorksheetCell("", "s9w")
                            End If
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(drRow("name").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            If CInt(drRow("loanschemeunkid")) = CInt(dr("loanschemeunkid")) Then
                                wcell = New WorksheetCell("√", "s9w")
                            Else
                                wcell = New WorksheetCell("", "s9w")
                            End If

                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            cnt += 1
                        Next

                        objCCLoanScheme = Nothing

                        ' END LOAN SCHEME


                        ' START PERSONAL DETAILS
                        mintSrNo += 1
                        row = New WorksheetRow()

                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Personal Details"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Title"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("Title").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "First Name"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("FirstName").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Middle Name"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("MiddleName").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Last Name"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("LastName").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        ' END PERSONAL DETAILS


                        ' START GENDER
                        mintSrNo += 1
                        row = New WorksheetRow()

                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Gender"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 31, "Male"), "s9w")
                        row.Cells.Add(wcell)

                        If CInt(dtEmployee.Rows(0)("GenderId")) = 1 Then  'Male
                            wcell = New WorksheetCell("√", "s9w")
                        Else
                            wcell = New WorksheetCell("", "s9w")
                        End If
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 32, "Female"), "s9w")
                        row.Cells.Add(wcell)

                        If CInt(dtEmployee.Rows(0)("GenderId")) = 2 Then  'Female
                            wcell = New WorksheetCell("√", "s9w")
                        Else
                            wcell = New WorksheetCell("", "s9w")
                        End If
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        ' END GENDER


                        ' START MARITAL STATUS

                        Dim objCommon As New clsCommon_Master
                        Dim dsMaritalStatus As DataSet = objCommon.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, False, "List", -1, False, False, 0, False, False)

                        Dim cntMS As Integer = 0
                        For Each drRow As DataRow In dsMaritalStatus.Tables(0).Rows

                            row = New WorksheetRow()
                            If cntMS = 0 Then
                                mintSrNo += 1
                                wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                                row.Cells.Add(wcell)

                                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Marital Status"), "s9w")
                                row.Cells.Add(wcell)

                            Else
                                wcell = New WorksheetCell("", "s9w")
                                row.Cells.Add(wcell)

                                wcell = New WorksheetCell("", "s9w")
                                row.Cells.Add(wcell)
                            End If

                            wcell = New WorksheetCell(drRow("name").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            If CInt(dtEmployee.Rows(0)("MaritalStatusId")) = CInt(drRow("masterunkid")) Then
                                wcell = New WorksheetCell("√", "s9w")
                            Else
                                wcell = New WorksheetCell("", "s9w")
                            End If
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            cntMS += 1
                        Next
                        ' END MARITAL STATUS

                        ' START DOB
                        mintSrNo += 1

                        row = New WorksheetRow()

                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Date of Birth"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(CDate(dtEmployee.Rows(0)("birthdate")).ToShortDateString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)
                        ' END DOB


                        ' START RESIDENTIAL ADDRESS
                        mintSrNo += 1

                        row = New WorksheetRow()

                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Residentail Address"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Region"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._City_Name, "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "District"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._State_Name, "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "City/Town"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._City_Name, "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Country"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Country_Name, "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Street"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Address1, "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "Email Address"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Email, "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Mobile Number"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Phone1, "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        ' END RESIDENTIAL ADDRESS


                        ' START NATIONAL ID NUMBER

                        mintSrNo += 1

                        row = New WorksheetRow()

                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "National ID Number"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("NIDA").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        ' END NATIONAL ID NUMBER

                        ' START RECOVERY ACCOUNT NUMBER
                        mintSrNo += 1

                        row = New WorksheetRow()

                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Recovery Account Number"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Account No"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dsEmpBank.Tables(0).Rows(0)("accountno").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Account Type"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dsEmpBank.Tables(0).Rows(0)("AccName").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "Account Names"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dsEmpBank.Tables(0).Rows(0)("EmpName").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        ' END RECOVERY ACCOUNT NUMBER



                        ' START REFEREES
                        mintSrNo += 1

                        row = New WorksheetRow()

                        wcell = New WorksheetCell(mintSrNo.ToString(), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Referees"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Referee 1 [relative]"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 29, "Spouse/Father/Mother/Son/Daughter/Other"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "First Name"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("Emergency_FirstName").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Middle Name"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Last Name"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("Emergency_LastName").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 30, "Postal Address"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("Emergency_Address").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "City/Town"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("City").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Region"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("Region").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "District"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("City").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Mobile Number"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("MobileNo").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "Email Address"), "s9w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dtEmployee.Rows(0)("Email").ToString(), "s9w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)


                        If dtEmpReporting IsNot Nothing AndAlso dtReportingTo.Rows.Count > 0 Then

                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 34, "Referee 2 [Friend/Colleague]"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "First Name"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("FirstName").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)


                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Middle Name"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("MiddleName").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)


                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Last Name"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("LastName").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)



                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 30, "Postal Address"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("Address").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)



                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "City/Town"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("City").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)


                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Region"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("Region").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)


                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "District"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("District").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)


                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Mobile Number"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("MobileNo").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)


                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "Email Address"), "s9w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(dtEmpReporting.Rows(0)("Email").ToString(), "s9w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                        End If
                        ' END REFEREES

                        row = New WorksheetRow()
                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        rowsArrayFooter.Add(row)

                    End If  'If dtReportingTo IsNot Nothing AndAlso dtReportingTo.Rows.Count > 0 Then

                Next

                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

                objReportingTo = Nothing
                objMaster = Nothing
                objEmpBank = Nothing

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function GetEmployeeDetails(ByVal xEmployeeId As Integer, ByVal mblnIsReportingToEmployee As Boolean) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = ""
        Dim objDataOperation As clsDataOperation = Nothing
        Dim exForce As Exception = Nothing
        Try

            objDataOperation = New clsDataOperation

            StrQ = " SELECT ISNULL(hremployee_master.firstname, '') AS FirstName " & _
                       ",ISNULL(hremployee_master.othername, '') AS MiddleName " & _
                       ",ISNULL(hremployee_master.surname, '') AS LastName "

            If mblnIsReportingToEmployee = False Then

                StrQ &= ", ISNULL(tl.name, '') AS Title " & _
                             ", hremployee_master.gender AS GenderId " & _
                             ",CASE WHEN gender = 1 THEN  @Male " & _
                             "          WHEN gender = 2 THEN  @Female " & _
                             "          WHEN gender = 3  THEN @Other " & _
                             " ELSE '' END AS Gender " & _
                             ", ms.masterunkid  AS MaritalStatusId " & _
                             ",ISNULL(ms.name, '') AS MaritalStatus " & _
                             ",birthdate " & _
                             ",ISNULL(hid.identity_no, '') AS NIDA " & _
                             ",ISNULL(hremployee_master.emer_con_firstname, '') AS Emergency_FirstName " & _
                             ",ISNULL(hremployee_master.emer_con_lastname, '') AS Emergency_LastName " & _
                             ",ISNULL(hremployee_master.emer_con_address, '') AS Emergency_Address " & _
                             ",ISNULL(ct.name, '') AS City " & _
                             ",ISNULL(st.name, '') AS Region " & _
                             ",ISNULL(ct.name, '') AS District " & _
                             ",ISNULL(hremployee_master.emer_con_mobile, hremployee_master.emer_con_alternateno) AS MobileNo " & _
                             ",ISNULL(hremployee_master.emer_con_email, '') AS Email " & _
                             ",ISNULL(hid.identity_no,'') AS NationalID "
            Else

                StrQ &= " , ISNULL(hremployee_master.present_address1, '') AS Address " & _
                             ",ISNULL(rct.name, '') AS City " & _
                             ",ISNULL(rst.name, '') AS Region " & _
                             ",ISNULL(rct.name, '') AS District " & _
                             ",ISNULL(hremployee_master.present_mobile, hremployee_master.present_alternateno) AS MobileNo " & _
                             ",ISNULL(hremployee_master.present_email, '') AS Email "

            End If

            StrQ &= " FROM hremployee_master "

            If mblnIsReportingToEmployee = False Then
                StrQ &= " LEFT JOIN cfcommon_master tl ON tl.masterunkid = hremployee_master.titleunkid AND tl.mastertype = " & clsCommon_Master.enCommonMaster.TITLE & _
                             " LEFT JOIN cfcommon_master ms ON ms.masterunkid = hremployee_master.maritalstatusunkid AND ms.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                             " LEFT JOIN hremployee_idinfo_tran hid ON hid.employeeunkid = hremployee_master.employeeunkid AND isdefault = 1 " & _
                             " LEFT JOIN cfcommon_master im ON im.masterunkid = hid.idtypeunkid AND ms.mastertype = " & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & _
                             " LEFT JOIN hrmsConfiguration..cfstate_master st ON st.stateunkid = hremployee_master.emer_con_state " & _
                             " LEFT JOIN hrmsConfiguration..cfcity_master ct ON ct.cityunkid = hremployee_master.emer_con_post_townunkid "
            Else
                StrQ &= " LEFT JOIN hrmsConfiguration..cfstate_master rst ON rst.stateunkid = hremployee_master.present_stateunkid " & _
                             " LEFT JOIN hrmsConfiguration..cfcity_master rct ON rct.cityunkid = hremployee_master.present_post_townunkid "
            End If

            StrQ &= " WHERE hremployee_master.employeeunkid = @employeeunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Female"))
            objDataOperation.AddParameter("@Other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "Other"))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            Dim dsEmployee As DataSet = objDataOperation.ExecQuery(StrQ, "EmpList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsEmployee.Tables(0).Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeDetails; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function



#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sr No")
            Language.setMessage(mstrModuleName, 2, "Credit Limit Applied")
            Language.setMessage(mstrModuleName, 3, "Cash Limit")
            Language.setMessage(mstrModuleName, 4, "Card Type")
            Language.setMessage(mstrModuleName, 5, "Personal Details")
            Language.setMessage(mstrModuleName, 6, "Title")
            Language.setMessage(mstrModuleName, 7, "First Name")
            Language.setMessage(mstrModuleName, 9, "Middle Name")
            Language.setMessage(mstrModuleName, 10, "Last Name")
            Language.setMessage(mstrModuleName, 11, "Gender")
            Language.setMessage(mstrModuleName, 12, "Marital Status")
            Language.setMessage(mstrModuleName, 13, "Date of Birth")
            Language.setMessage(mstrModuleName, 14, "Residentail Address")
            Language.setMessage(mstrModuleName, 15, "Region")
            Language.setMessage(mstrModuleName, 16, "District")
            Language.setMessage(mstrModuleName, 17, "City/Town")
            Language.setMessage(mstrModuleName, 18, "Country")
            Language.setMessage(mstrModuleName, 19, "Street")
            Language.setMessage(mstrModuleName, 20, "Email Address")
            Language.setMessage(mstrModuleName, 21, "Mobile Number")
            Language.setMessage(mstrModuleName, 22, "National ID Number")
            Language.setMessage(mstrModuleName, 23, "Recovery Account Number")
            Language.setMessage(mstrModuleName, 24, "Account No")
            Language.setMessage(mstrModuleName, 25, "Account Type")
            Language.setMessage(mstrModuleName, 26, "Account Names")
            Language.setMessage(mstrModuleName, 27, "Referees")
            Language.setMessage(mstrModuleName, 28, "Referee 1 [relative]")
            Language.setMessage(mstrModuleName, 29, "Spouse/Father/Mother/Son/Daughter/Other")
            Language.setMessage(mstrModuleName, 30, "Postal Address")
            Language.setMessage(mstrModuleName, 31, "Male")
            Language.setMessage(mstrModuleName, 32, "Female")
            Language.setMessage(mstrModuleName, 33, "Other")
            Language.setMessage(mstrModuleName, 34, "Referee 2 [Friend/Colleague]")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
