Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmCredit_Card_Report

#Region "Private Variables"

    Private mstrModuleName As String = "frmCredit_Card_Report"
    Dim objCreditCard As clsCredit_Card_Report
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objCreditCard = New clsCredit_Card_Report(User._Object._Languageunkid, Company._Object._Companyunkid)
        objCreditCard.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            Dim objLoanScheme As New clsLoan_Scheme
            dsList = objLoanScheme.getComboList(True, "List", -1, " ISNULL(loanschemecategory_id,0) = " & enLoanSchemeCategories.CREDIT_CARD, False)
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objLoanScheme = Nothing

            Dim objEmp As New clsEmployee_Master
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboLoanScheme.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboLoanApplicationNo.SelectedIndex = 0
            mstrAdvanceFilter = String.Empty
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objCreditCard.SetDefaultValue()
            objCreditCard._LoanSchemeId = CInt(cboLoanScheme.SelectedValue)
            objCreditCard._LoanScheme = cboLoanScheme.Text
            objCreditCard._EmployeeID = CInt(cboEmployee.SelectedValue)
            objCreditCard._EmployeeName = cboEmployee.Text
            objCreditCard._LoanApplicationId = CInt(cboLoanApplicationNo.SelectedValue)
            objCreditCard._LoanApplicationNo = cboLoanApplicationNo.Text
            objCreditCard._Advance_Filter = mstrAdvanceFilter
            objCreditCard._ViewByIds = mstrViewByIds
            objCreditCard._ViewIndex = mintViewIndex
            objCreditCard._ViewByName = mstrViewByName
            objCreditCard._Analysis_Fields = mstrAnalysis_Fields
            objCreditCard._Analysis_Join = mstrAnalysis_Join
            objCreditCard._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCreditCard._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objCreditCard._Report_GroupName = mstrReport_GroupName
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmCredit_Card_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCreditCard = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCredit_Card_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmCredit_Card_Report_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objCreditCard._ReportName
            eZeeHeader.Message = objCreditCard._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCredit_Card_Report_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCredit_Card_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCredit_Card_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCredit_Card_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCredit_Card_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCredit_Card_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsCredit_Card_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Scheme is compulsory information.Please Select Loan Scheme."), enMsgBoxStyle.Information)
                cboLoanScheme.Select()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            End If

            Dim dtTable As DataTable = CType(cboLoanApplicationNo.DataSource, DataTable)
            Dim dRow = dtTable.Select("processpendingloanunkid > 0")
            If dRow Is Nothing OrElse dRow.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no Loan Application(s) to export Report."), enMsgBoxStyle.Information)
                cboLoanApplicationNo.Select()
                Exit Sub
            End If

            If SetFilter() = False Then Exit Sub
            objCreditCard.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                      , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate.ToString, ConfigParameter._Object._UserAccessModeSetting, True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchLoanScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLoanScheme.Click
        Dim objfrm As New frmCommonSearch
        Dim dtLoanScheme As DataTable
        Try
            dtLoanScheme = CType(cboLoanScheme.DataSource, DataTable)
            With cboLoanScheme
                objfrm.DataSource = dtLoanScheme
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "Code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLoanScheme_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchLoanApplicationNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLoanApplicationNo.Click
        Dim objfrm As New frmCommonSearch
        Dim dtLoanApplicationNo As DataTable
        Try
            dtLoanApplicationNo = CType(cboLoanApplicationNo.DataSource, DataTable)
            With cboLoanApplicationNo
                objfrm.DataSource = dtLoanApplicationNo
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLoanApplicationNo_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboLoanScheme.SelectedIndexChanged
        Try
            Dim objLoan As New clsProcess_pending_loan
            Dim dsList As DataSet = objLoan.GetEmployeeLoanApplicationNoList(CInt(cboEmployee.SelectedValue), CInt(cboLoanScheme.SelectedValue), True, "List", -1, "loan_statusunkid IN (" & enLoanApplicationStatus.PENDING & "," & enLoanApplicationStatus.APPROVED & ")")
            With cboLoanApplicationNo
                .ValueMember = "processpendingloanunkid"
                .DisplayMember = "application_no"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objLoan = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


        
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblLoanScheme.Text = Language._Object.getCaption(Me.LblLoanScheme.Name, Me.LblLoanScheme.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.LblLeaveFormNo.Text = Language._Object.getCaption(Me.LblLeaveFormNo.Name, Me.LblLeaveFormNo.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Loan Scheme is compulsory information.Please Select Loan Scheme.")
            Language.setMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 3, "There is no Loan Application(s) to export Report.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
