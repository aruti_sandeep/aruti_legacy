'************************************************************************************************************************************
'Class Name : frmEDDetailPeriodWiseReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEDDetailPeriodWiseReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEDDetailPeriodWiseReport"
    Private objEDDetail As clsEDDetailPeriodWiseReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private dsPeriod As DataSet = Nothing

    Private mstrAdvanceFilter As String = String.Empty

    Private mstrTranDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0
    Private mdicYearDBName As New Dictionary(Of Integer, String) 'Sohail (21 Aug 2015)
#End Region

#Region " Constructor "

    Public Sub New()
        objEDDetail = New clsEDDetailPeriodWiseReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEDDetail.SetDefaultValue()
        InitializeComponent()


    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim dsList As New DataSet
            Dim objUserdefReport As New clsUserDef_ReportMode
            Dim objMaster As New clsMasterData


            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'dsList = objEmp.GetEmployeeList("Emp", True, False)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (25 Mar 2013)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (25 Mar 2013)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables(0).Copy
                .SelectedValue = mintFirstOpenPeriod
            End With
            objperiod = Nothing

            dsList = objMaster.getComboListForHeadType("List")
            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'Dim dr As DataRow = dsList.Tables(0).NewRow
            'dr.Item("Id") = 99999
            'dr.Item("Name") = Language.getMessage(mstrModuleName, 7, "PAY PER ACTIVITY")
            'dsList.Tables(0).Rows.Add(dr)
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            Dim dr As DataRow = dsList.Tables(0).NewRow
            dr.Item("Id") = 99998
            dr.Item("Name") = Language.getMessage(mstrModuleName, 8, "Claim Request Expense")
            dsList.Tables(0).Rows.Add(dr)
            'Sohail (23 Jun 2015) -- End
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables(0)
            End With

            Dim objMember As New clsmembership_master
            dsList = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0

        Try

            objEDDetail.SetDefaultValue()

            objEDDetail._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEDDetail._EmployeeName = cboEmployee.Text

            objEDDetail._ModeId = CInt(cboMode.SelectedValue)
            objEDDetail._ModeName = cboMode.Text

            objEDDetail._HeadId = CInt(cboHeades.SelectedValue)
            objEDDetail._HeadName = cboHeades.Text

            If cboHeades.DataSource IsNot Nothing Then
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                'Dim dtRow() As DataRow = CType(cboHeades.DataSource, DataTable).Select("tranheadunkid = '" & CInt(cboHeades.SelectedValue) & "'")
                'If dtRow.Length > 0 Then
                '    objEDDetail._HeadCode = dtRow(0)("code").ToString
                'End If
                'If CInt(cboMode.SelectedValue) = 99999 Then
                '    objEDDetail._HeadCode = CType(cboHeades.SelectedItem, DataRowView).Item("code").ToString
                'Else
                    objEDDetail._HeadCode = CType(cboHeades.SelectedItem, DataRowView).Item("code").ToString
                'End If
                'Sohail (10 Sep 2014) -- End
            End If

            objEDDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objEDDetail._PeriodName = cboPeriod.Text

            objEDDetail._ViewByIds = mstrStringIds
            objEDDetail._ViewIndex = mintViewIdx
            objEDDetail._ViewByName = mstrStringName
            objEDDetail._Analysis_Fields = mstrAnalysis_Fields
            objEDDetail._Analysis_Join = mstrAnalysis_Join
            objEDDetail._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEDDetail._Report_GroupName = mstrReport_GroupName

            objEDDetail._IsActive = chkInactiveemp.Checked
            objEDDetail._IncludeNegativeHeads = chkIncludeNegativeHeads.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEDDetail._PeriodStartDate = objPeriod._Start_Date

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboToPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEDDetail._PeriodEndDate = objPeriod._End_Date

            objEDDetail._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            objEDDetail._ToPeriodName = cboToPeriod.Text

            mdicYearDBName.Clear() 'Sohail (21 Aug 2015)
            For Each dsRow As DataRow In dsPeriod.Tables(0).Rows
                If i >= cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            End If
                            'Sohail (21 Aug 2015) -- End
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            If arrDatabaseName.Count <= 0 Then Return False

            objEDDetail._mstrPeriodID = strPeriodIDs
            objEDDetail._Arr_DatabaseName = arrDatabaseName

            objPeriod = Nothing

            objEDDetail._Advance_Filter = mstrAdvanceFilter


            objEDDetail._MembershipId = CInt(cboMembership.SelectedValue)
            objEDDetail._MemberShipName = cboMembership.Text


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objEDDetail.setDefaultOrderBy(0)
            txtOrderBy.Text = objEDDetail.OrderByDisplay
            cboEmployee.SelectedIndex = 0
            cboMode.SelectedIndex = 0
            cboHeades.SelectedIndex = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboToPeriod.SelectedValue = mintFirstOpenPeriod
            chkInactiveemp.Checked = False
            chkIncludeNegativeHeads.Checked = True
            cboMembership.SelectedValue = 0

            mstrStringIds = ""
            mstrStringName = ""

            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEDDetailReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEDDetail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEDDetailReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboHeades
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try


            If CInt(cboMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Mode."), enMsgBoxStyle.Information)
                cboMode.Select()
                Exit Sub
            ElseIf CInt(cboHeades.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Head."), enMsgBoxStyle.Information)
                cboHeades.Select()
                Exit Sub
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub

            ElseIf CInt(cboPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Sub

            End If

            If cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, " To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Sub
            End If

            If SetFilter() = False Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEDDetail.Generate_DetailReport()
            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._Start_Date
            objPeriod = New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            dtPeriodEnd = objPeriod._End_Date
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objEDDetail._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(dtPeriodEnd))
            'Sohail (03 Aug 2019) -- End
            objEDDetail.Generate_DetailReport(mdicYearDBName, User._Object._Userunkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, User._Object._Username, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'Sohail (21 Aug 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClosel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub


    Private Sub objbtnLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEDDetailPeriodWiseReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEDDetailPeriodWiseReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboMembership
                objfrm.DataSource = cboMembership.DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEDDetail.setOrderBy(0)
            txtOrderBy.Text = objEDDetail.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "ComboBox Event"

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Dim dsFill As DataSet = Nothing
        Try
            'Dim objTransactionhead As New clsTransactionHead 'Sohail (10 Sep 2014)

            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue))
            'dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue), , , , , , , , True)
            'Sohail (17 Sep 2014) -- End
            'If CInt(cboMode.SelectedValue) = 99999 Then
            '    Dim objActivity As New clsActivity_Master
            '    dsFill = objActivity.getComboList("List", True)
            'Else
            '    Dim objTransactionhead As New clsTransactionHead
            '    dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue))
            'End If
            'cboHeades.DataSource = Nothing
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            If CInt(cboMode.SelectedValue) = 99998 Then
                Dim objExpense As New clsExpense_Master
                dsFill = objExpense.getComboList(0, True, "List")
            Else
                Dim objTransactionhead As New clsTransactionHead
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue), , , , , , , , True)
                dsFill = objTransactionhead.getComboList(FinancialYear._Object._DatabaseName, "List", True, CInt(cboMode.SelectedValue), , , , , , , True)
                'Sohail (21 Aug 2015) -- End
            End If
            cboHeades.DataSource = Nothing
            'Sohail (23 Jun 2015) -- End
            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'cboHeades.ValueMember = "tranheadunkid"
            'If CInt(cboMode.SelectedValue) = 99999 Then
            '    cboHeades.ValueMember = "activityunkid"
            'Else
            'cboHeades.ValueMember = "tranheadunkid"
            'End If
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            If CInt(cboMode.SelectedValue) = 99998 Then
                cboHeades.ValueMember = "Id"
            Else
                cboHeades.ValueMember = "tranheadunkid"
            End If
            'Sohail (23 Jun 2015) -- End
            cboHeades.DisplayMember = "Name"
            cboHeades.DataSource = dsFill.Tables("List")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)
			Me.chkIncludeNegativeHeads.Text = Language._Object.getCaption(Me.chkIncludeNegativeHeads.Name, Me.chkIncludeNegativeHeads.Text)
			Me.LblTo.Text = Language._Object.getCaption(Me.LblTo.Name, Me.LblTo.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblHeads.Text = Language._Object.getCaption(Me.lblHeads.Name, Me.lblHeads.Text)
			Me.lblSelectMode.Text = Language._Object.getCaption(Me.lblSelectMode.Name, Me.lblSelectMode.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Mode.")
			Language.setMessage(mstrModuleName, 2, "Please Select Head.")
			Language.setMessage(mstrModuleName, 3, "Please Select Period.")
			Language.setMessage(mstrModuleName, 4, "Please Select From Period.")
			Language.setMessage(mstrModuleName, 5, "Please Select To Period.")
			Language.setMessage(mstrModuleName, 6, " To Period cannot be less than From Period")
			Language.setMessage(mstrModuleName, 7, "PAY PER ACTIVITY")
            Language.setMessage(mstrModuleName, 8, "Claim Request Expense")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
