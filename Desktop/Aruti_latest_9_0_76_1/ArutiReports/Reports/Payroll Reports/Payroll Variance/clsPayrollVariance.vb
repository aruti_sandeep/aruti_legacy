'************************************************************************************************************************************
'Class Name : clsPayrollReconciliation.vb
'Purpose    :
'Date       :25/11/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsPayrollVariance
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPayrollReconciliation"
    Private mstrReportId As String = enArutiReport.PayrollVariance
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport() 'Sohail (15 Apr 2014)
    End Sub

#End Region

#Region " Private Variables "

    Private mintFromPeriodId As Integer = -1
    Private mstrFromPeriodName As String = String.Empty
    Private mintNextPeriodId As Integer = -1
    Private mstrNextPeriodName As String = ""
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    Private mintEmpId As Integer = -1
    Private mstrEmpName As String = String.Empty
    Private dsEmployees As New DataSet
    Private dsEarnTotal As New DataSet
    Private dsDeducTotal As New DataSet
    Private dtFilterTable As DataTable = Nothing
    Private dtPeriod1Table As DataTable = Nothing
    Private dtPeriod2Table As DataTable = Nothing
    Private dtFinalTable As DataTable = Nothing
    Private mdicColumns As New Dictionary(Of Integer, String)
    Dim dblColTot As Decimal()

    'Sandeep [ 09 MARCH 2011 ] -- Start
    Dim StrFinalPath As String = String.Empty
    'Sandeep [ 09 MARCH 2011 ] -- End 


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (17 Apr 2012) -- End

    'Sohail (12 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintTranHeadTypeId As Integer = 0
    Private mstrTranHeadTypeName As String = ""
    Private mblnIgnoreZeroVariance As Boolean = False

    Private marrEarningIDs As New ArrayList
    Private marrDeductionIDs As New ArrayList
    Private marrStatutoryDeductionIDs As New ArrayList
    'Sohail (12 Jul 2012) -- End

    'Sohail (20 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintTranHeadUnkId As Integer = 0
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    'Sohail (20 Jul 2012) -- End
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mintFromYearUnkId As Integer
    Private mintToYearUnkId As Integer
    'Sohail (21 Aug 2015) -- End

    'Pinkal (15-Jan-2013) -- Start
    'Enhancement : TRA Changes
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    'Pinkal (15-Jan-2013) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintBaseCurrencyId As Integer = 0
    'Pinkal (24-May-2013) -- End

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname 'Sohail (15 Apr 2014)
    'Sohail (23 Feb 2017) -- Start
    'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
    Private mblnShowEmployerContribution As Boolean = False
    Private mblnShowInformationalHeads As Boolean = False

    Private marrEmployerContributionIDs As New ArrayList
    Private marrInformationalIDs As New ArrayList
    'Sohail (23 Feb 2017) -- End
    'Hemant (22 Jan 2019) -- Start
    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
    Private mblnShowVariancePercentageColumns As Boolean = False
    'Hemant (22 Jan 2019) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _FromPeriodId() As String
        Set(ByVal value As String)
            mintFromPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _NextPeriodId() As Integer
        Set(ByVal value As Integer)
            mintNextPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _NextPeriodName() As String
        Set(ByVal value As String)
            mstrNextPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _BranchName() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End

    'Sohail (12 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _TranHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadTypeId = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadTypeName() As String
        Set(ByVal value As String)
            mstrTranHeadTypeName = value
        End Set
    End Property

    Public WriteOnly Property _IgnoreZeroVariance() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZeroVariance = value
        End Set
    End Property
    'Sohail (12 Jul 2012) -- End

    'Sohail (20 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _TranHeadUnkId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadUnkId = value
        End Set
    End Property

    Public WriteOnly Property _FromDatabaseName() As String
        Set(ByVal value As String)
            mstrFromDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ToDatabaseName() As String
        Set(ByVal value As String)
            mstrToDatabaseName = value
        End Set
    End Property
    'Sohail (20 Jul 2012) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public WriteOnly Property _FromYearUnkId() As Integer
        Set(ByVal value As Integer)
            mintFromYearUnkId = value
        End Set
    End Property

    Public WriteOnly Property _ToYearUnkId() As Integer
        Set(ByVal value As Integer)
            mintToYearUnkId = value
        End Set
    End Property
    'Sohail (21 Aug 2015) -- End

    'Pinkal (15-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Pinkal (15-Jan-2013) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrencyId = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- End

    'Sohail (15 Apr 2014) -- Start
    'Enhancement - Sorting on Payroll Variance Report
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'Sohail (15 Apr 2014) -- End
    'Sohail (23 Feb 2017) -- Start
    'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
    Public WriteOnly Property _ShowEmployerContribution() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmployerContribution = value
        End Set
    End Property

    Public WriteOnly Property _ShowInformationalHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnShowInformationalHeads = value
        End Set
    End Property
    'Sohail (23 Feb 2017) -- End
    'Hemant (22 Jan 2019) -- Start
    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
    Public WriteOnly Property _ShowVariancePercentageColumns() As Boolean
        Set(ByVal value As Boolean)
            mblnShowVariancePercentageColumns = value
        End Set
    End Property
    'Hemant (22 Jan 2019) -- End

#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintFromPeriodId = -1
            mintNextPeriodId = -1
            mstrFromPeriodName = ""
            mstrNextPeriodName = ""
            mintBranchId = -1
            mstrBranchName = ""
            mintEmpId = -1
            mstrEmpName = ""

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            mintTranHeadTypeId = 0
            mstrTranHeadTypeName = ""
            mblnIgnoreZeroVariance = True
            'Sohail (12 Jul 2012) -- End

            'Sohail (20 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            mintTranHeadUnkId = 0
            mstrFromDatabaseName = FinancialYear._Object._DatabaseName
            mstrToDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (20 Jul 2012) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mintFromYearUnkId = 0
            mintToYearUnkId = 0
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            mblnShowEmployerContribution = False
            mblnShowInformationalHeads = False
            'Sohail (23 Feb 2017) -- End

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'Pinkal (15-Jan-2013) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

            mdicColumns.Clear() 'Sohail (12 Nov 2014)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Private Sub GetNextMonth(ByVal intPeriodId As Integer)
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As DataSet = Nothing
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '                    "	 periodunkid AS Pid " & _
    '                    "   ,period_name AS PName " & _
    '                 "FROM cfcommon_period_tran " & _
    '                 "WHERE periodunkid = @PeriodId "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromPeriodId + 1)

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            mintNextPeriodId = dsList.Tables("List").Rows(0).Item("Pid")
    '            mstrNextPeriodName = dsList.Tables("List").Rows(0).Item("PName")
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetNextMonth; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@FromPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromPeriodId)
            objDataOperation.AddParameter("@ToPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextPeriodId)

            Me._FilterTitle &= mstrFromPeriodName & " - " & mstrNextPeriodName

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(1).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(1).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Sohail (16 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    'Private Sub SetEmployee(ByRef dsDataSet As DataSet, ByVal intPeriodId As Integer)
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '                    "	 cfcommon_period_tran.period_name AS PeriodName " & _
    '                    "	,(ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '                    "	,hremployee_master.employeeunkid AS EmpId " & _
    '                    "	,cfcommon_period_tran.periodunkid AS PeriodId " & _
    '                    "   ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '                    "FROM  prpayrollprocess_tran " & _
    '                    "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "	AND cfcommon_period_tran.isactive = 1 " & _
    '                    "	AND (cfcommon_period_tran.periodunkid = @PeriodId "
    '        'Sohail (17 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        StrQ &= " OR cfcommon_period_tran.periodunkid = @ToPeriodId )  "
    '        'Sohail (17 Apr 2012) -- End

    '        If mintEmpId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId  "
    '        End If


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "	AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "GROUP BY " & _
    '                    "	cfcommon_period_tran.period_name " & _
    '                    "	,(ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '                    "	,hremployee_master.employeeunkid " & _
    '                    "	,cfcommon_period_tran.periodunkid " & _
    '                    "  ,ISNULL(hremployee_master.employeecode,'') " & _
    '                    "ORDER BY  " & _
    '                    "	hremployee_master.employeeunkid "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromPeriodId)
    '        objDataOperation.AddParameter("@ToPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextPeriodId) 'Sohail (17 Apr 2012)

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 


    '        dsDataSet = objDataOperation.ExecQuery(StrQ, "EMP")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub GetEDTotals(ByRef dsEarningTot As DataSet, ByRef dsDeductionTot As DataSet, ByVal intPeriodid As Integer)
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation


    '        'Sandeep [ 25 MARCH 2011 ] -- Start
    '        'Issue : Period Opening Balance
    '        'StrQ = "SELECT " & _
    '        '                "	 hremployee_master.employeeunkid AS EmpId " & _
    '        '                "	,prtranhead_master.tranheadunkid AS TranId " & _
    '        '                "	,SUM(amount) AS Amount " & _
    '        '                "FROM  prpayrollprocess_tran " & _
    '        '                "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '                "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '                "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '                "	AND hremployee_master.isactive = 1 " & _
    '        '                "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '                "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '                "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '        '                "	AND prtnaleave_tran.payperiodunkid = @PeriodId "

    'StrQ = "SELECT " & _
    '                "	 hremployee_master.employeeunkid AS EmpId " & _
    '                "	,prtranhead_master.tranheadunkid AS TranId " & _
    '                "	,SUM(amount) AS Amount " & _
    '               "    ,SUM(prtnaleave_tran.openingbalance) AS OpBal " & _
    '                "FROM  prpayrollprocess_tran " & _
    '                "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '                "	AND prtnaleave_tran.payperiodunkid = @PeriodId "
    '        'Sandeep [ 25 MARCH 2011 ] -- End 

    '        If mintEmpId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '        End If

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            StrQ &= "	AND hremployee_master.isactive = 1 "
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "GROUP BY  hremployee_master.employeeunkid " & _
    '                        ",prtranhead_master.tranheadunkid " & _
    '                        "ORDER BY  " & _
    '                        "hremployee_master.employeeunkid " & _
    '                        ",prtranhead_master.tranheadunkid "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodid)

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        dsEarningTot = objDataOperation.ExecQuery(StrQ, "ETotal")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT " & _
    '                    "	  EmpId AS EmpId " & _
    '                    "	, TranId AS TranId " & _
    '                    "	, Amount AS Amount " & _
    '                    "	, Mid AS Mid " & _
    '                    "   , 0 AS OpBal " & _
    '                    "FROM " & _
    '                    "	( " & _
    '                    "		SELECT " & _
    '                    "			 hremployee_master.employeeunkid AS EmpId " & _
    '                    "			,prtranhead_master.tranheadunkid AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,0 AS Mid " & _
    '                    "		FROM  prpayrollprocess_tran " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                    "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "			AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '                    "			AND prtnaleave_tran.payperiodunkid = @PeriodId "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            StrQ &= "	AND hremployee_master.isactive = 1 "
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '                    "			,prtranhead_master.tranheadunkid " & _
    '                    "	UNION ALL " & _
    '                    "		SELECT " & _
    '                    "			  hremployee_master.employeeunkid AS EmpId " & _
    '                    "			,-1 AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,1 AS Mid " & _
    '                    "		FROM  prpayrollprocess_tran " & _
    '                    "			JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "			AND lnloan_advance_tran.isloan = 1 " & _
    '                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "			AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '                    "			AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                    "			AND prtnaleave_tran.payperiodunkid = @PeriodId "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            StrQ &= "	 AND hremployee_master.isactive = 1 "
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '                    "	UNION ALL " & _
    '                    "		SELECT " & _
    '                    "			 hremployee_master.employeeunkid AS EmpId " & _
    '                    "			,-1 AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,2 AS Mid " & _
    '                    "		FROM  prpayrollprocess_tran " & _
    '                    "			JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "			AND lnloan_advance_tran.isloan = 0 " & _
    '                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "			AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '                    "			AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                    "			AND prtnaleave_tran.payperiodunkid = @PeriodId "

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            StrQ &= "		AND hremployee_master.isactive = 1 "
    '        End If

    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '                    "	UNION ALL " & _
    '                    "		SELECT " & _
    '                    "			 hremployee_master.employeeunkid AS EmpId " & _
    '                    "			,-1 AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,3 AS Mid " & _
    '                    "		FROM  prpayrollprocess_tran " & _
    '                    "			JOIN svsaving_tran ON prpayrollprocess_tran.savingtranunkid = svsaving_tran.savingtranunkid " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '                    "			AND prpayrollprocess_tran.add_deduct IN (2)  " & _
    '                    "			AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                    "			AND prtnaleave_tran.payperiodunkid = @PeriodId "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            StrQ &= "	AND hremployee_master.isactive = 1 "
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '                    ") AS Deduction "

    '        If mintEmpId > 0 Then
    '            StrQ &= "	WHERE EmpId = @EmpId "
    '        End If
    '        StrQ &= "ORDER BY EmpId,TranId "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodid)

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        dsDeductionTot = objDataOperation.ExecQuery(StrQ, "Deduction")


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEDTotals; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub SetDataTableValue(ByVal dsEmp As DataSet, ByVal dsEarnTot As DataSet, ByVal dsDeducTot As DataSet, ByRef dtTable As DataTable)
    '    Try
    '        Dim mdicEmployee As New Dictionary(Of Integer, Integer)
    '        Dim intCnt As Integer = 1
    '        For Each drRow As DataRow In dsEmp.Tables(0).Rows
    '            If mdicEmployee.ContainsKey(drRow.Item("EmpId")) Then Continue For
    '            mdicEmployee.Add(drRow.Item("EmpId"), drRow.Item("EmpId"))

    '            dtFilterTable = New DataView(dsEarnTot.Tables(0), "EmpId = " & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable

    '            Dim rpt_Row As DataRow
    '            rpt_Row = dtTable.NewRow

    '            rpt_Row.Item("EmpId") = drRow.Item("EmpId")
    '            rpt_Row.Item("SrNo") = intCnt
    '            rpt_Row.Item("EmpName") = drRow.Item("EmpName")

    '            For Each dtRow As DataRow In dtFilterTable.Rows
    '                rpt_Row.Item("Column" & dtRow.Item("TranId")) = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '                rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(dtRow.Item("Amount")), GUI.fmtCurrency)

    '                'Sandeep [ 25 MARCH 2011 ] -- Start
    '                'Issue : Period Opening Balance
    '                rpt_Row.Item("NetPayBF") = Format(CDec(dtRow.Item("OpBal")), GUI.fmtCurrency)
    '                'Sandeep [ 25 MARCH 2011 ] -- End
    '            Next

    '            dtFilterTable = New DataView(dsDeducTot.Tables(0), "EmpId = " & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable

    '            For Each dtDRow As DataRow In dtFilterTable.Rows
    '                If dtDRow.Item("Mid").ToString = "1" Then   'LOAN
    '                    rpt_Row.Item("Loan") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                ElseIf dtDRow.Item("Mid").ToString = "2" Then 'ADVANCE
    '                    rpt_Row.Item("Advance") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                ElseIf dtDRow.Item("Mid").ToString = "3" Then 'SAVINGS
    '                    rpt_Row.Item("Savings") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column" & dtDRow.Item("TranId")) = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                End If
    '                rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '            Next
    '            rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")), GUI.fmtCurrency) - Format(CDec(rpt_Row("TDD")), GUI.fmtCurrency)

    '            'Anjan (29 Dec 2010)-Start
    '            'rpt_Row.Item("NetPay") = Format(cdec(rpt_Row.Item("NetPay")), GUI.fmtCurrency)
    '            rpt_Row.Item("NetPay") = Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type)
    '            'Anjan (29 Dec 2010)-End

    '            'Sandeep [ 25 MARCH 2011 ] -- Start
    '            'Issue : Period Opening Balance
    '            rpt_Row.Item("TotalNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("NetPayBF")), GUI.fmtCurrency)

    '            rpt_Row.Item("TotalNetPay") = Rounding.BRound(CDec(rpt_Row.Item("TotalNetPay")), ConfigParameter._Object._RoundOff_Type)
    '            'Sandeep [ 25 MARCH 2011 ] -- End 


    '            dtTable.Rows.Add(rpt_Row)
    '            intCnt += 1
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SetDataTableValue; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (16 Jun 2012) -- End

    Private Sub SetDataColumns(ByVal dtTable As DataTable)
        Dim objExpense As New clsExpense_Master 'Sohail (12 Nov 2014)
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim intCnt As Integer = 0
        Try
            objDataOperation = New clsDataOperation
            Dim dCol As DataColumn

            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            marrEarningIDs.Clear()
            marrDeductionIDs.Clear()
            marrStatutoryDeductionIDs.Clear()
            'Sohail (12 Jul 2012) -- End
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            marrEmployerContributionIDs.Clear()
            marrInformationalIDs.Clear()
            'Sohail (23 Feb 2017) -- End

            dCol = New DataColumn("EmpId")
            dCol.Caption = "Id"
            dCol.DataType = System.Type.GetType("System.Int32")
            dtTable.Columns.Add(dCol)

            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("SrNo")
            dCol.Caption = "No."

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DataType = System.Type.GetType("System.String")
            'Pinkal (15-Jan-2013) -- End

            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            'Pinkal (24-Sep-2012) -- Start
            'Enhancement : TRA Changes
            dCol = New DataColumn("Code")
            dCol.Caption = "Employee Code"
            dCol.DataType = System.Type.GetType("System.String")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Pinkal (24-Sep-2012) -- End

            dCol = New DataColumn("EmpName")
            dCol.Caption = "Employee Name"
            dCol.DataType = System.Type.GetType("System.String")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("Id")
            dCol.Caption = "Id"
            dCol.DataType = System.Type.GetType("System.Int32")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("GName")
            dCol.Caption = "GName"
            dCol.DataType = System.Type.GetType("System.String")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Pinkal (15-Jan-2013) -- End

            StrQ = "SELECT " & _
                       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                       "FROM prtranhead_master " & _
                       "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
                       "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "

            Dim dsList As New DataSet
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    dCol = New DataColumn("FPColumn" & dtRow.Item("tranheadunkid")) 'From Period
                    dCol.DefaultValue = 0
                    dCol.Caption = mstrFromPeriodName
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.ExtendedProperties.Add("Caption", dtRow.Item("Tname"))
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If

                    dCol = New DataColumn("TPColumn" & dtRow.Item("tranheadunkid")) 'To Period
                    dCol.DefaultValue = 0
                    dCol.Caption = mstrNextPeriodName
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    'Sohail (17 Feb 2017) -- End

                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                    dCol.DefaultValue = 0
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    'dCol.Caption = dtRow.Item("Tname")
                    dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
                    'Sohail (17 Feb 2017) -- End
                    dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (10 Aug 2012)
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    marrEarningIDs.Add("Column" & dtRow.Item("tranheadunkid")) 'Sohail (12 Jul 2012)

                    'Hemant (25 Dec 2018) -- Start
                    'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                    'Hemant (22 Jan 2019) -- Start
                    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                    If mblnShowVariancePercentageColumns = True Then
                        'Hemant (22 Jan 2019) -- End
                        dCol = New DataColumn("PerColumn" & dtRow.Item("tranheadunkid"))
                        dCol.DefaultValue = 0
                        dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                        dCol.DataType = System.Type.GetType("System.String")
                        dCol.ExtendedProperties.Add("style", "s8r")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If
                    End If
                    'Hemant (25 Dec 2018) -- End
                Next
            End If

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (29 Mar 2017) -- Start
            'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
            'dsList = objExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.EarningForEmployees)
            dsList = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.EarningForEmployees)
            'Sohail (29 Mar 2017) -- End
            If dsList.Tables("Expense").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Expense").Rows
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    dCol = New DataColumn("FPColumnCR" & dtRow.Item("Id")) 'From Period
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = mstrFromPeriodName
                    dCol.ExtendedProperties.Add("Caption", dtRow.Item("name"))
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If

                    dCol = New DataColumn("TPColumnCR" & dtRow.Item("Id")) 'From Period
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = mstrNextPeriodName
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    'Sohail (17 Feb 2017) -- End

                    dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    'dCol.Caption = dtRow.Item("name")
                    dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
                    'Sohail (17 Feb 2017) -- End
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    marrEarningIDs.Add("ColumnCR" & dtRow.Item("Id"))
                    'Hemant (25 Dec 2018) -- Start
                    'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                    'Hemant (22 Jan 2019) -- Start
                    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                    If mblnShowVariancePercentageColumns = True Then
                        'Hemant (22 Jan 2019) -- End
                        dCol = New DataColumn("PerColumnCR" & dtRow.Item("Id"))
                        dCol.DataType = System.Type.GetType("System.String")
                        dCol.DefaultValue = 0
                        dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                        dCol.ExtendedProperties.Add("style", "s8r")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If
                    End If
                    'Hemant (25 Dec 2018) -- End

                Next
            End If
            'Sohail (12 Nov 2014) -- End

            '------------------ Add Gross Pay Column
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPTGP")
            dCol.Caption = mstrFromPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Gross Pay")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPTGP")
            dCol.Caption = mstrNextPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            dCol = New DataColumn("TGP")
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Gross Pay"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End
            dCol.DefaultValue = 0

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Pinkal (15-Jan-2013) -- End


            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerTGP")
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End


            '------------------- Deduction

            StrQ = "SELECT " & _
                       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                       ",ISNULL(prtranhead_master.trnheadtype_id, 0) As trnheadtype_id " & _
                       "FROM prtranhead_master " & _
                       "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
                       "AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & ") ORDER BY prtranhead_master.tranheadunkid " 'Sohail (12 Jul 2012) - [trnheadtype_id]

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    dCol = New DataColumn("FPColumn" & dtRow.Item("tranheadunkid"))
                    dCol.Caption = mstrFromPeriodName
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.ExtendedProperties.Add("Caption", dtRow.Item("Tname"))
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If

                    dCol = New DataColumn("TPColumn" & dtRow.Item("tranheadunkid"))
                    dCol.Caption = mstrNextPeriodName
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    'Sohail (17 Feb 2017) -- End

                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    'dCol.Caption = dtRow.Item("Tname")
                    dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
                    'Sohail (17 Feb 2017) -- End
                    dCol.DefaultValue = 0
                    'dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (10 Aug 2012)
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    'Sohail (12 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If CInt(dtRow.Item("trnheadtype_id")) = enTranHeadType.DeductionForEmployee Then
                        marrDeductionIDs.Add("Column" & dtRow.Item("tranheadunkid"))
                    ElseIf CInt(dtRow.Item("trnheadtype_id")) = enTranHeadType.EmployeesStatutoryDeductions Then
                        marrStatutoryDeductionIDs.Add("Column" & dtRow.Item("tranheadunkid"))
                    End If
                    'Sohail (12 Jul 2012) -- End

                    'Hemant (25 Dec 2018) -- Start
                    'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                    'Hemant (22 Jan 2019) -- Start
                    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                    If mblnShowVariancePercentageColumns = True Then
                        'Hemant (22 Jan 2019) -- End
                        dCol = New DataColumn("PerColumn" & dtRow.Item("tranheadunkid"))
                        dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                        dCol.DefaultValue = 0
                        dCol.DataType = System.Type.GetType("System.String")
                        dCol.ExtendedProperties.Add("style", "s8r")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If
                    End If
                    'Hemant (25 Dec 2018) -- End

                Next
            End If

            '------------ Loan
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPLoan")
            dCol.Caption = mstrFromPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Loan")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPLoan")
            dCol.Caption = mstrNextPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            dCol = New DataColumn("Loan")
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Loan"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End
            dCol.DefaultValue = 0
            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Sohail (10 Aug 2012) -- End
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            marrDeductionIDs.Add("Loan") 'Sohail (12 Jul 2012)

            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerLoan")
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End


            '------------ Advance
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPAdvance")
            dCol.Caption = mstrFromPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Advance")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPAdvance")
            dCol.Caption = mstrNextPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            dCol = New DataColumn("Advance")
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Advance"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End
            dCol.DefaultValue = 0
            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Sohail (10 Aug 2012) -- End
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            marrDeductionIDs.Add("Advance") 'Sohail (12 Jul 2012)

            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerAdvance")
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End


            '------------ Savings
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPSavings")
            dCol.Caption = mstrFromPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Savings")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPSavings")
            dCol.Caption = mstrNextPeriodName
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            dCol = New DataColumn("Savings")
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Savings"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End
            dCol.DefaultValue = 0
            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Sohail (10 Aug 2012) -- End
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            marrDeductionIDs.Add("Savings") 'Sohail (12 Jul 2012)

            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerSavings")
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End


            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (29 Mar 2017) -- Start
            'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
            'dsList = objExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
            dsList = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
            'Sohail (29 Mar 2017) -- End
            If dsList.Tables("Expense").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Expense").Rows
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    dCol = New DataColumn("FPColumnCR" & dtRow.Item("Id"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = mstrFromPeriodName
                    dCol.ExtendedProperties.Add("Caption", dtRow.Item("name"))
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If

                    dCol = New DataColumn("TPColumnCR" & dtRow.Item("Id"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = mstrNextPeriodName
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    'Sohail (17 Feb 2017) -- End

                    dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    'Sohail (17 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                    'dCol.Caption = dtRow.Item("name")
                    dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
                    'Sohail (17 Feb 2017) -- End
                    dtTable.Columns.Add(dCol)
                    If mdicColumns.ContainsKey(intCnt) = False Then
                        mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                        intCnt += 1
                    End If
                    marrDeductionIDs.Add("ColumnCR" & dtRow.Item("Id"))
                    'Hemant (25 Dec 2018) -- Start
                    'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                    'Hemant (22 Jan 2019) -- Start
                    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                    If mblnShowVariancePercentageColumns = True Then
                        'Hemant (22 Jan 2019) -- End
                        dCol = New DataColumn("PerColumnCR" & dtRow.Item("Id"))
                        dCol.DataType = System.Type.GetType("System.String")
                        dCol.DefaultValue = 0
                        dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                        dCol.ExtendedProperties.Add("style", "s8r")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If
                    End If
                    'Hemant (25 Dec 2018) -- End

                Next
            End If
            'Sohail (12 Nov 2014) -- End

            '------------------ Add Total Deduction Column
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPTDD")
            dCol.DefaultValue = 0
            dCol.Caption = mstrFromPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Total Deduction")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPTDD")
            dCol.DefaultValue = 0
            dCol.Caption = mstrNextPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            dCol = New DataColumn("TDD")
            dCol.DefaultValue = 0
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Total Deduction"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")

            'Pinkal (15-Jan-2013) -- End

            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerTDD")
                dCol.DefaultValue = 0
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End


            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPNetPayBF")
            dCol.DefaultValue = 0
            dCol.Caption = mstrFromPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Net Pay B/F")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPNetPayBF")
            dCol.DefaultValue = 0
            dCol.Caption = mstrNextPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            'Sandeep [ 25 MARCH 2011 ] -- Start
            'Issue : Period Opening Balance
            dCol = New DataColumn("NetPayBF")
            dCol.DefaultValue = 0
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Net Pay B/F"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Pinkal (15-Jan-2013) -- End


            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sandeep [ 25 MARCH 2011 ] -- End 

            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerNetPayBF")
                dCol.DefaultValue = 0
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End

            '------------------ Add Net Pay Column
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPNetPay")
            dCol.DefaultValue = 0
            dCol.Caption = mstrFromPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Net Pay")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPNetPay")
            dCol.DefaultValue = 0
            dCol.Caption = mstrNextPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            dCol = New DataColumn("NetPay")
            dCol.DefaultValue = 0
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Net Pay"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")

            'Pinkal (15-Jan-2013) -- End

            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerNetPay")
                dCol.DefaultValue = 0
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End


            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            dCol = New DataColumn("FPTotalNetPay")
            dCol.DefaultValue = 0
            dCol.Caption = mstrFromPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.ExtendedProperties.Add("Caption", "Total Net Pay")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If

            dCol = New DataColumn("TPTotalNetPay")
            dCol.DefaultValue = 0
            dCol.Caption = mstrNextPeriodName
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sohail (17 Feb 2017) -- End

            'Sandeep [ 25 MARCH 2011 ] -- Start
            'Issue : Period Opening Balance
            dCol = New DataColumn("TotalNetPay")
            dCol.DefaultValue = 0
            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            'dCol.Caption = "Total Net Pay"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
            'Sohail (17 Feb 2017) -- End

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Pinkal (15-Jan-2013) -- End


            dtTable.Columns.Add(dCol)
            If mdicColumns.ContainsKey(intCnt) = False Then
                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                intCnt += 1
            End If
            'Sandeep [ 25 MARCH 2011 ] -- End 

            'Hemant (25 Dec 2018) -- Start
            'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                'Hemant (22 Jan 2019) -- End
                dCol = New DataColumn("PerTotalNetPay")
                dCol.DefaultValue = 0
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                dCol.DataType = System.Type.GetType("System.String")
                dCol.ExtendedProperties.Add("style", "s8r")
                dtTable.Columns.Add(dCol)
                If mdicColumns.ContainsKey(intCnt) = False Then
                    mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                    intCnt += 1
                End If
            End If
            'Hemant (25 Dec 2018) -- End

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            If mblnShowEmployerContribution = True Then
                StrQ = "SELECT  ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                             ", ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                      "FROM prtranhead_master " & _
                      "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
                              "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployersStatutoryContributions & " ORDER BY prtranhead_master.trnheadname "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        dCol = New DataColumn("FPColumn" & dtRow.Item("tranheadunkid")) 'From Period
                        dCol.DefaultValue = 0
                        dCol.Caption = mstrFromPeriodName
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.ExtendedProperties.Add("Caption", dtRow.Item("Tname"))
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If

                        dCol = New DataColumn("TPColumn" & dtRow.Item("tranheadunkid")) 'To Period
                        dCol.DefaultValue = 0
                        dCol.Caption = mstrNextPeriodName
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If

                        dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                        dCol.DefaultValue = 0
                        dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If
                        marrEmployerContributionIDs.Add("Column" & dtRow.Item("tranheadunkid"))
                        'Hemant (25 Dec 2018) -- Start
                        'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                        'Hemant (22 Jan 2019) -- Start
                        'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                        If mblnShowVariancePercentageColumns = True Then
                            'Hemant (22 Jan 2019) -- End
                            dCol = New DataColumn("PerColumn" & dtRow.Item("tranheadunkid"))
                            dCol.DefaultValue = 0
                            dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                            dCol.DataType = System.Type.GetType("System.String")
                            dCol.ExtendedProperties.Add("style", "s8r")
                            dtTable.Columns.Add(dCol)
                            If mdicColumns.ContainsKey(intCnt) = False Then
                                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                                intCnt += 1
                            End If
                        End If
                        'Hemant (25 Dec 2018) -- End
                    Next
                End If
            End If

            If mblnShowInformationalHeads = True Then
                StrQ = "SELECT  ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                             ", ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                      "FROM prtranhead_master " & _
                      "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
                      "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " ORDER BY prtranhead_master.trnheadname "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        dCol = New DataColumn("FPColumn" & dtRow.Item("tranheadunkid")) 'From Period
                        dCol.DefaultValue = 0
                        dCol.Caption = mstrFromPeriodName
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.ExtendedProperties.Add("Caption", dtRow.Item("Tname"))
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If

                        dCol = New DataColumn("TPColumn" & dtRow.Item("tranheadunkid")) 'To Period
                        dCol.DefaultValue = 0
                        dCol.Caption = mstrNextPeriodName
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If

                        dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                        dCol.DefaultValue = 0
                        dCol.Caption = Language.getMessage(mstrModuleName, 9, "Variance")
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dtTable.Columns.Add(dCol)
                        If mdicColumns.ContainsKey(intCnt) = False Then
                            mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                            intCnt += 1
                        End If
                        marrInformationalIDs.Add("Column" & dtRow.Item("tranheadunkid"))
                        'Hemant (25 Dec 2018) -- Start
                        'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                        'Hemant (22 Jan 2019) -- Start
                        'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                        If mblnShowVariancePercentageColumns = True Then
                            'Hemant (22 Jan 2019) -- End
                            dCol = New DataColumn("PerColumn" & dtRow.Item("tranheadunkid"))
                            dCol.DefaultValue = 0
                            dCol.Caption = Language.getMessage(mstrModuleName, 10, "Variance(%)")
                            dCol.DataType = System.Type.GetType("System.String")
                            dCol.ExtendedProperties.Add("style", "s8r")
                            dtTable.Columns.Add(dCol)
                            If mdicColumns.ContainsKey(intCnt) = False Then
                                mdicColumns.Add(intCnt, dCol.ColumnName.ToString)
                                intCnt += 1
                            End If
                        End If
                        'Hemant (25 Dec 2018) -- End

                    Next
                End If
            End If
            'Sohail (23 Feb 2017) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDataColums; Module Name: " & mstrModuleName)
            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
        Finally
            objExpense = Nothing
            'Sohail (12 Nov 2014) -- End
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            'HEADER PART
            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 2, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 3, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            'strBuilder.Append(" <TR> " & vbCrLf)
            'strBuilder.Append(" <TD width='60%' colspan=18.5 align='center' > " & vbCrLf)
            'strBuilder.Append(" <FONT SIZE=3><b> " & Language.getMessage(mstrModuleName, 16, "Branch :") & " " & mstrBranchName & " " & "</B></FONT> " & vbCrLf)
            'strBuilder.Append(" </TD> " & vbCrLf)
            If mintBranchId > 0 Then
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD width='60%' colspan= 22 align='center' > " & vbCrLf)
                strBuilder.Append(" <FONT SIZE=3><b> " & Language.getMessage(mstrModuleName, 6, "Branch :") & " " & mstrBranchName & " " & "</B></FONT> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            Dim arrZeroColIndx As New ArrayList 'Sohail (12 Jul 2012)

            'Report Column Caption
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                'Sohail (12 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                If mblnIgnoreZeroVariance = True Then
                    Debug.Print(objDataReader.Columns(j).ColumnName)
                    If objDataReader.Columns(j).ColumnName = "Loan" OrElse _
                       objDataReader.Columns(j).ColumnName = "Advance" OrElse _
                       objDataReader.Columns(j).ColumnName = "Savings" OrElse _
                       objDataReader.Columns(j).ColumnName.StartsWith("Column") = True Then

                        Dim drRow As DataRow() = Nothing
                        'Sohail (10 Aug 2012) -- Start
                        'TRA - ENHANCEMENT
                        If objDataReader.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                            'If objDataReader.Columns(j).DataType Is Type.GetType("System.String") Then
                            'drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = '0' OR " & objDataReader.Columns(j).ColumnName & " = '0.00'")
                            drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = 0 ")
                            'Sohail (10 Aug 2012) -- End
                            If drRow.Length = objDataReader.Rows.Count Then
                                arrZeroColIndx.Add(j)
                                Continue For
                            End If
                        End If

                    End If
                End If
                'Sohail (12 Jul 2012) -- End
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1

                    'Sohail (12 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If mblnIgnoreZeroVariance = True Then
                        If arrZeroColIndx.Contains(k) = True Then
                            Continue For
                        End If
                    End If
                    'Sohail (12 Jul 2012) -- End

                    'S.SANDEEP [ 17 AUG 2011 ] -- START
                    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                    'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Select Case k

                        'Pinkal (24-Sep-2012) -- Start
                        'Enhancement : TRA Changes
                        'Case 0, 1
                        Case 0, 1, 2
                            'Pinkal (24-Sep-2012) -- End

                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                        Case Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
                    End Select
                    'S.SANDEEP [ 17 AUG 2011 ] -- END 
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" <TR> " & vbCrLf)
            For k As Integer = 0 To objDataReader.Columns.Count - 1

                'Sohail (12 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                If mblnIgnoreZeroVariance = True Then
                    If arrZeroColIndx.Contains(k) = True Then
                        Continue For
                    End If
                End If
                'Sohail (12 Jul 2012) -- End

                If k = 0 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 1, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)

                    'Pinkal (24-Sep-2012) -- Start
                    'Enhancement : TRA Changes
                    'ElseIf k <= 1 Then
                ElseIf k <= 2 Then
                    'Pinkal (24-Sep-2012) -- End

                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B> &nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                End If
            Next
            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            'Sandeep [ 09 MARCH 2011 ] -- Start
            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If
            'Sandeep [ 09 MARCH 2011 ] -- End 

            'Anjan (14 Apr 2011)-Start
            'Issue : To remove "\" from path which comes at end in path.
            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If
            'Anjan (14 Apr 2011)-End

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                'Sandeep [ 09 MARCH 2011 ] -- Start
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                'Sandeep [ 09 MARCH 2011 ] -- End 
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function



    'Pinkal (15-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Private Function IgnoreZeroHead(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        Try

            Dim intZeroColumn As Integer = 0
            Dim arrZeroColIndx As New ArrayList

            'Report Column Caption
SetColumnCount:
            intColCount = objDataReader.Columns.Count
            For j As Integer = 0 To intColCount - 1
                If mblnIgnoreZeroVariance Then

                    If objDataReader.Columns(j).ColumnName = "Loan" OrElse objDataReader.Columns(j).ColumnName = "Advance" OrElse objDataReader.Columns(j).ColumnName = "Savings" _
                           OrElse objDataReader.Columns(j).ColumnName.StartsWith("Column") = True Then

                        Dim drRow As DataRow() = Nothing
                        If objDataReader.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                            drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = 0 OR " & objDataReader.Columns(j).ColumnName & " = 0.00")
                            If drRow.Length = objDataReader.Rows.Count Then
                                'Sohail (17 Feb 2017) -- Start
                                'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                                'objDataReader.Columns.RemoveAt(j)
                                objDataReader.Columns.RemoveAt(j - 2)
                                objDataReader.Columns.RemoveAt(j - 2)
                                objDataReader.Columns.RemoveAt(j - 2)
                                'Hemant (25 Dec 2018) -- Start
                                'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                                'Hemant (22 Jan 2019) -- Start
                                'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                                If mblnShowVariancePercentageColumns = True Then
                                    'Hemant (22 Jan 2019) -- End
                                    objDataReader.Columns.RemoveAt(j - 2)
                                End If
                                'Hemant (25 Dec 2018) -- End
                                'Sohail (17 Feb 2017) -- End
                                GoTo SetColumnCount
                            End If
                        End If

                    End If
                End If

            Next

            'Sohail (09 May 2017) -- Start
            'KBC Enhancement - 66.1 - Don't show employee if he has all heads variance with Zero amount and if Ignore Zero Variance is ticked on Payroll Variance Report.
            Dim decCols() As String = (From p In objDataReader.Columns.Cast(Of DataColumn)() Where (p.DataType Is Type.GetType("System.Decimal") AndAlso Not (p.ColumnName.StartsWith("FP") OrElse p.ColumnName.StartsWith("TP"))) Select (p.ColumnName.ToString & " = 0 AND ")).ToArray
            If decCols.Length > 0 Then
                Dim s As String = String.Join("", decCols)
                Dim r() As DataRow = objDataReader.Select(s.Substring(0, s.Length - 4))
                For Each dr As DataRow In r
                    objDataReader.Rows.Remove(dr)
                Next
            End If
            'Sohail (09 May 2017) -- End

            Return objDataReader

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IgnoreZeroHead; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (15-Jan-2013) -- End


#End Region

#Region " Report Generation "

    'Sohail (15 Apr 2014) -- Start
    'Enhancement - Sorting on Payroll Variance Report
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 7, "Employee Code")))
            If mblnFirstNamethenSurname = True Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Apr 2014) -- End

    Public Sub Export_Recociliaton_Report(ByVal xUserUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strfmtCurrency As String _
                                           , ByVal intBase_CurrencyId As Integer _
                                           , ByVal xExportReportPath As String _
                                           , ByVal xOpenAfterExport As Boolean _
                                           )
        'Sohail (21 Aug 2015) - [xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, intBase_CurrencyId, xExportReportPath, xOpenAfterExport]

        'Sohail (16 Jun 2012) -- Start
        'TRA - ENHANCEMENT
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dt1, dt2 As DataTable
        'Sohail (16 Jun 2012) -- End


        'Pinkal (15-Jan-2013) -- Start
        'Enhancement : TRA Changes
        Dim mstrField As String = ""
        'Pinkal (15-Jan-2013) -- End


        Try


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If ConfigParameter._Object._ExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'Nilay (18-Mar-2015) -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'If mstrExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Nilay (18-Mar-2015) -- End

            'Pinkal (24-May-2013) -- End



            'Sohail (17 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            mintBaseCurrencyId = intBase_CurrencyId 'Sohail (21 Aug 2015)
            If mintBaseCurrencyId <= 0 Then
                mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
            End If
            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId

            'Pinkal (24-May-2013) -- End


            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (17 Aug 2012) -- End

            dtPeriod1Table = New DataTable("PreviosPeriod")
            dtPeriod2Table = New DataTable("NextPeriod")
            dtFinalTable = New DataTable("Reconciliation")

            '/***************************** SETTING COLUMNS TO DATATABLE *******************************/ -- START
            Call SetDataColumns(dtPeriod1Table)   'Period1 Table
            Call SetDataColumns(dtPeriod2Table)   'Period2 Table
            Call SetDataColumns(dtFinalTable)   'Final Table
            '/***************************** SETTING COLUMNS TO DATATABLE *******************************/ -- END

            'Sohail (16 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            ''/***************************** THIS IS FOR FIRST PERIOD SELECTED BY USER *****************************/ -- START
            ''SETTING EMPLOYEE DATA
            'Call SetEmployee(dsEmployees, mintFromPeriodId)

            ''SETTING EMPLOYEE TOTAL EARNING & DEDUCTION
            'Call GetEDTotals(dsEarnTotal, dsDeducTotal, mintFromPeriodId)

            ''SETTING DATA TO DATATABLE
            'Call SetDataTableValue(dsEmployees, dsEarnTotal, dsDeducTotal, dtPeriod1Table)
            ''/***************************** THIS IS FOR FIRST PERIOD SELECTED BY USER *****************************/ -- END


            ''/***************************** THIS IS FOR SECOND PERIOD SELECTED BY USER *****************************/ -- START
            ''SETTING EMPLOYEE DATA
            'Call SetEmployee(dsEmployees, mintNextPeriodId)

            ''SETTING EMPLOYEE TOTAL EARNING & DEDUCTION
            'Call GetEDTotals(dsEarnTotal, dsDeducTotal, mintNextPeriodId)

            ''SETTING DATA TO DATATABLE
            'Call SetDataTableValue(dsEmployees, dsEarnTotal, dsDeducTotal, dtPeriod2Table)
            ''/***************************** THIS IS FOR SECOND PERIOD SELECTED BY USER *****************************/ -- END

            ''/***************************** DEDUCTION FROM DATATABLE(2) TO DATATABLE(1)  *****************************/ -- START
            'For Each dtRow As DataRow In dtPeriod2Table.Rows
            '    Dim rpt_row As DataRow
            '    rpt_row = dtFinalTable.NewRow
            '    Dim intCnt As Integer = 0

            '    rpt_row.Item(mdicColumns(intCnt)) = dtRow.Item(mdicColumns(intCnt))
            '    intCnt += 1

            '    rpt_row.Item(mdicColumns(intCnt)) = dtRow.Item(mdicColumns(intCnt))
            '    intCnt += 1

            '    rpt_row.Item(mdicColumns(intCnt)) = dtRow.Item(mdicColumns(intCnt))
            '    intCnt += 1

            '    Dim drRow() As DataRow = dtPeriod1Table.Select("EmpId ='" & dtRow.Item("EmpId") & "'")

            '    If drRow.Length > 0 Then
            '        For i As Integer = intCnt To drRow(0).ItemArray.Length - 1
            '            rpt_row.Item(mdicColumns(i)) = Format(CDec(dtRow.Item(mdicColumns(i))) - CDec(drRow(0).Item(mdicColumns(i))), GUI.fmtCurrency)
            '            'intCnt += 1
            '        Next
            '        dtFinalTable.Rows.Add(rpt_row)
            '    End If
            'Next

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, mintFromYearUnkId, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
            'Sohail (21 Aug 2015) -- End


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAnalysis_Fields.Trim.Length > 0 Then
                mstrField = mstrAnalysis_Fields.Trim.Replace("AS Id", "").Replace("AS GName", "").Substring(1) & ","
            End If
            'Pinkal (15-Jan-2013) -- End



            StrQ = "SELECT  ISNULL(PeriodId, 0) AS PeriodId " & _
                         ", ISNULL(period_name, '') AS PeriodName " & _
                         ", hremployee_master.employeeunkid AS EmpId "

            'Sohail (15 Apr 2014) -- Start
            'Enhancement - Sorting on Payroll Variance Report
            '", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ') AS EmpName "
            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            End If
            'Sohail (15 Apr 2014) -- End

            StrQ &= ", hremployee_master.employeecode AS Code " & _
                         ", ISNULL(TranId, -99) AS TranId " & _
                         ", ISNULL(Amount, 0) AS Amount " & _
                         ", ISNULL(Mid, -1) AS Mid " & _
                         ", ISNULL(CAST(prtnaleave_tran.openingbalance AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS OpenBal "


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            StrQ &= mstrAnalysis_Fields
            'Pinkal (15-Jan-2013) -- End



            StrQ &= " FROM    ( SELECT DISTINCT " & _
                                        "payperiodunkid AS PeriodId " & _
                                      ", prtnaleave_tran.employeeunkid AS EmpId " & _
                                      ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                      ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", 10 AS Mid "
            'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            StrQ &= mstrAnalysis_Fields
            'Pinkal (15-Jan-2013) -- End


            StrQ &= "FROM     " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                  "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "


            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            StrQ &= mstrAnalysis_Join
            'Pinkal (15-Jan-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            ''Pinkal (31-Mar-2023) -- Start
            ''TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            ''Pinkal (31-Mar-2023) -- End

            'Sohail (21 Aug 2015) -- End

            StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                        "AND payperiodunkid = @PeriodId "

            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= "	AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            'End If
            'If mintTranHeadUnkId > 0 Then
            '    StrQ &= "	AND prtranhead_master.tranheadunkid = @tranheadunkid "
            'End If
            'Sohail (23 Feb 2017) -- End
            'Sohail (10 Aug 2012) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                StrQ &= " AND T.stationunkid = @BranchId"
                'Sohail (21 Aug 2015) -- End
            End If


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes




            StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                            ", prtnaleave_tran.employeeunkid " & _
                            ", prpayrollprocess_tran.tranheadunkid " & _
                    "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                          ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 0 AS Mid "
            'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

            StrQ &= mstrAnalysis_Fields
            'Pinkal (15-Jan-2013) -- End


            StrQ &= "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                      "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "


            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            StrQ &= mstrAnalysis_Join
            'Pinkal (15-Jan-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Pinkal (31-Mar-2023) -- End
         
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & " ) " & _
                            "AND payperiodunkid = @PeriodId "


            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= "	AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            'End If
            'If mintTranHeadUnkId > 0 Then
            '    StrQ &= "	AND prtranhead_master.tranheadunkid = @tranheadunkid "
            'End If
            'Sohail (23 Feb 2017) -- End
            'Sohail (10 Aug 2012) -- End


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End


            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                StrQ &= " AND T.stationunkid = @BranchId"
                'Sohail (21 Aug 2015) -- End
            End If


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                          ", hremployee_master.employeeunkid " & _
                          ", prpayrollprocess_tran.tranheadunkid "

            'Pinkal (15-Jan-2013) -- End

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            StrQ &= "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                            ", hremployee_master.employeeunkid AS EmpId " & _
                            ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                            ", SUM(CAST(prpayrollprocess_tran.Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                            ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN 1 THEN 7 WHEN 2 THEN 8 END ELSE CASE prpayrollprocess_tran.add_deduct WHEN 1 THEN 7 ELSE 8 END END AS Mid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (27 Apr 2021) - [SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(prpayrollprocess_tran.amount)]

            StrQ &= mstrAnalysis_Fields

            StrQ &= "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                       "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                       "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                       "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                       "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                       "LEFT JOIN " & mstrFromDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                       "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                       "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]


            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= mstrAnalysis_Join

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Pinkal (31-Mar-2023) -- End

            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                            "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                            "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                            "AND payperiodunkid = @PeriodId "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'If mintTranHeadUnkId > 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= " AND cmexpense_master.trnheadtype_id = @trnheadtype_id "
            'Else
            '    StrQ &= " AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") "
            'End If
            StrQ &= " AND 1 = CASE WHEN prpayrollprocess_tran.crprocesstranunkid > 0 AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") THEN 1 WHEN prpayrollprocess_tran.crretirementprocessunkid > 0 THEN 1 ELSE 0 END "
            'Sohail (23 Feb 2017) -- End

            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                StrQ &= " AND T.stationunkid = @BranchId"
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= "GROUP BY  " & mstrField & "  payperiodunkid " & _
                      ", hremployee_master.employeeunkid " & _
                      ", cmexpense_master.expenseunkid " & _
                      ", cmexpense_master.trnheadtype_id " & _
                      ", crretireexpense.expenseunkid " & _
                      ", prpayrollprocess_tran.add_deduct "
            'Sohail (09 Jun 2021) - [crretireexpense]
            'Sohail (12 Nov 2014) -- End

            ' *** Show loan advance saving when either no filter selected OR only employee deduction type selected.
            If Not (mintTranHeadTypeId = enTranHeadType.EarningForEmployees _
                OrElse mintTranHeadTypeId = enTranHeadType.EmployeesStatutoryDeductions _
                OrElse mintTranHeadTypeId = enTranHeadType.EmployersStatutoryContributions _
                OrElse mintTranHeadTypeId = enTranHeadType.Informational _
                OrElse mintTranHeadUnkId > 0) Then 'Sohail (10 Aug 2012)

                StrQ &= "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", -1 AS TranId " & _
                              ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 1 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                StrQ &= mstrAnalysis_Fields
                'Pinkal (15-Jan-2013) -- End


                StrQ &= "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                StrQ &= mstrAnalysis_Join
                'Pinkal (15-Jan-2013) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                            "AND lnloan_advance_tran.isloan = 1 " & _
                            "AND payperiodunkid = @PeriodId "


                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                'Pinkal (27-Feb-2013) -- Start
                'Enhancement : TRA Changes
                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If
                'Pinkal (27-Feb-2013) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End
                'Pinkal (24-May-2013) -- End


                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                    StrQ &= " AND T.stationunkid = @BranchId"
                    'Sohail (21 Aug 2015) -- End
                End If


                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes

                StrQ &= "GROUP BY  " & mstrField & "  payperiodunkid " & _
                          ", hremployee_master.employeeunkid " & _
                    "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", -1 AS TranId " & _
                              ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 2 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                StrQ &= mstrAnalysis_Fields
                'Pinkal (15-Jan-2013) -- End

                StrQ &= "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End


                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                StrQ &= mstrAnalysis_Join
                'Pinkal (15-Jan-2013) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End


                'Sohail (21 Aug 2015) -- End

                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                            "AND lnloan_advance_tran.isloan = 0 " & _
                            "AND payperiodunkid = @PeriodId "

                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                'Pinkal (27-Feb-2013) -- Start
                'Enhancement : TRA Changes
                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If
                'Pinkal (27-Feb-2013) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End
                'Pinkal (24-May-2013) -- End


                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                    StrQ &= " AND T.stationunkid = @BranchId"
                    'Sohail (21 Aug 2015) -- End
                End If


                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes

                StrQ &= " GROUP BY " & mstrField & _
                            " payperiodunkid, hremployee_master.employeeunkid " & _
                        "	UNION ALL " & _
                        "SELECT  payperiodunkid AS PeriodId " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", -1 AS TranId " & _
                                  ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                              ", 3 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields

                'Pinkal (15-Jan-2013) -- End


                StrQ &= "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                StrQ &= mstrAnalysis_Join
                'Pinkal (15-Jan-2013) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                                "AND payperiodunkid = @PeriodId "

                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                'Pinkal (27-Feb-2013) -- Start
                'Enhancement : TRA Changes
                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If
                'Pinkal (27-Feb-2013) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End
                'Pinkal (24-May-2013) -- End


                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                    StrQ &= " AND T.stationunkid = @BranchId"
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                          ", hremployee_master.employeeunkid "
            End If 'Sohail (10 Aug 2012)

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            If mblnShowEmployerContribution = True Then

                StrQ &= "UNION ALL " & _
                        "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                          ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 11 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields

                StrQ &= "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                          "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= mstrAnalysis_Join

                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployersStatutoryContributions & " " & _
                                "AND payperiodunkid = @PeriodId "


                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId"
                End If

                StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                              ", hremployee_master.employeeunkid " & _
                              ", prpayrollprocess_tran.tranheadunkid "
            End If

            If mblnShowInformationalHeads = True Then

                StrQ &= "UNION ALL " & _
                        "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                          ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 12 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields

                StrQ &= "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                          "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= mstrAnalysis_Join

                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " " & _
                                "AND payperiodunkid = @PeriodId "


                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId"
                End If

                StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                              ", hremployee_master.employeeunkid " & _
                              ", prpayrollprocess_tran.tranheadunkid "

            End If
            'Sohail (23 Feb 2017) -- End

            StrQ &= ") AS payroll " & _
                    "RIGHT JOIN " & mstrFromDatabaseName & "..hremployee_master ON payroll.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid " & _
                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                    "LEFT JOIN " & mstrFromDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = payroll.PeriodId " & _
                                                 "AND cfcommon_period_tran.isactive = 1 " & _
                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "


            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            StrQ &= mstrAnalysis_Join
            'Pinkal (15-Jan-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Pinkal (31-Mar-2023) -- End

            'Sohail (21 Aug 2015) -- End

            StrQ &= "	WHERE 1 = 1 "

            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End


            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                StrQ &= " AND T.stationunkid = @BranchId"
                'Sohail (21 Aug 2015) -- End
            End If


            'Pinkal (24-Sep-2012) -- Start
            'Enhancement : TRA Changes

            'StrQ &= "ORDER BY hremployee_master.employeeunkid " & _
            '                ", TranId "
            'Sohail (15 Apr 2014) -- Start
            'Enhancement - Sorting on Payroll Variance Report
            'StrQ &= "ORDER BY  hremployee_master.employeecode "
            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY  " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    StrQ &= "ORDER BY " & mstrAnalysis_OrderBy & ",  hremployee_master.employeecode "
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY  " & Me.OrderByQuery
                Else
                    StrQ &= "ORDER BY  hremployee_master.employeecode "
                End If
            End If
            'Sohail (15 Apr 2014) -- End
            'Pinkal (24-Sep-2012) -- End


            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromPeriodId)

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            If mintTranHeadTypeId > 0 Then
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadTypeId)
            End If
            If mintTranHeadUnkId > 0 Then
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadUnkId)
            End If
            'Sohail (10 Aug 2012) -- End

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            End If

            dt1 = objDataOperation.ExecQuery(StrQ, "payroll").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            '**********************************************************
            objDataOperation.ClearParameters()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrToDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrToDatabaseName, xUserUnkid, xCompanyUnkid, mintToYearUnkId, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrToDatabaseName)

            StrQ = "SELECT  ISNULL(PeriodId, 0) AS PeriodId " & _
                         ", ISNULL(period_name, '') AS PeriodName " & _
                         ", hremployee_master.employeeunkid AS EmpId "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            End If

            StrQ &= ", hremployee_master.employeecode AS Code " & _
                         ", ISNULL(TranId, -99) AS TranId " & _
                         ", ISNULL(Amount, 0) AS Amount " & _
                         ", ISNULL(Mid, -1) AS Mid " & _
                         ", ISNULL(CAST(prtnaleave_tran.openingbalance AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS OpenBal "

            StrQ &= mstrAnalysis_Fields

            StrQ &= " FROM    ( SELECT DISTINCT " & _
                                        "payperiodunkid AS PeriodId " & _
                                      ", prtnaleave_tran.employeeunkid AS EmpId " & _
                                      ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                      ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", 10 AS Mid "
            'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

            StrQ &= mstrAnalysis_Fields

            StrQ &= "FROM     " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "JOIN " & mstrToDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "JOIN " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                  "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= mstrAnalysis_Join

            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                        "AND payperiodunkid = @PeriodId "

            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= "	AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            'End If
            'If mintTranHeadUnkId > 0 Then
            '    StrQ &= "	AND prtranhead_master.tranheadunkid = @tranheadunkid "
            'End If
            'Sohail (23 Feb 2017) -- End

            If mintBranchId > 0 Then
                StrQ &= " AND T.stationunkid = @BranchId"
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                            ", prtnaleave_tran.employeeunkid " & _
                            ", prpayrollprocess_tran.tranheadunkid " & _
                    "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                          ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 0 AS Mid "
            'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

            StrQ &= mstrAnalysis_Fields

            StrQ &= "FROM   " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrToDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                      "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= mstrAnalysis_Join

            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & " ) " & _
                            "AND payperiodunkid = @PeriodId "


            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= "	AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            'End If
            'If mintTranHeadUnkId > 0 Then
            '    StrQ &= "	AND prtranhead_master.tranheadunkid = @tranheadunkid "
            'End If
            'Sohail (23 Feb 2017) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mintBranchId > 0 Then
                StrQ &= " AND T.stationunkid = @BranchId"
            End If

            StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                          ", hremployee_master.employeeunkid " & _
                          ", prpayrollprocess_tran.tranheadunkid "

            StrQ &= "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                            ", hremployee_master.employeeunkid AS EmpId " & _
                            ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                            ", SUM(CAST(prpayrollprocess_tran.Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                            ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN 1 THEN 7 WHEN 2 THEN 8 END ELSE CASE prpayrollprocess_tran.add_deduct WHEN 1 THEN 7 ELSE 8 END END AS Mid "
            'Sohail (27 Apr 2021) - [SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(prpayrollprocess_tran.amount)]

            StrQ &= mstrAnalysis_Fields

            StrQ &= "FROM   " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                       "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                       "JOIN " & mstrToDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & mstrToDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                       "LEFT JOIN " & mstrToDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                       "LEFT JOIN " & mstrToDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                       "LEFT JOIN " & mstrToDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                       "LEFT JOIN " & mstrToDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                       "LEFT JOIN " & mstrToDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]


            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= mstrAnalysis_Join

            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                            "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                            "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                            "AND payperiodunkid = @PeriodId "

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'If mintTranHeadUnkId > 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= " AND cmexpense_master.trnheadtype_id = @trnheadtype_id "
            'Else
            '    StrQ &= " AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") "
            'End If
            StrQ &= " AND 1 = CASE WHEN prpayrollprocess_tran.crprocesstranunkid > 0 AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") THEN 1 WHEN prpayrollprocess_tran.crretirementprocessunkid > 0 THEN 1 ELSE 0 END "
            'Sohail (23 Feb 2017) -- End

            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If mintBranchId > 0 Then
                StrQ &= " AND T.stationunkid = @BranchId"
            End If

            StrQ &= "GROUP BY  " & mstrField & "  payperiodunkid " & _
                      ", hremployee_master.employeeunkid " & _
                      ", cmexpense_master.expenseunkid " & _
                      ", cmexpense_master.trnheadtype_id " & _
                      ", crretireexpense.expenseunkid " & _
                      ", prpayrollprocess_tran.add_deduct "
            'Sohail (09 Jun 2021) - [crretireexpense]

            ' *** Show loan advance saving when either no filter selected OR only employee deduction type selected.
            If Not (mintTranHeadTypeId = enTranHeadType.EarningForEmployees _
                OrElse mintTranHeadTypeId = enTranHeadType.EmployeesStatutoryDeductions _
                OrElse mintTranHeadTypeId = enTranHeadType.EmployersStatutoryContributions _
                OrElse mintTranHeadTypeId = enTranHeadType.Informational _
                OrElse mintTranHeadUnkId > 0) Then

                StrQ &= "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", -1 AS TranId " & _
                              ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 1 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields


                StrQ &= "FROM   " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrToDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrToDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End


                StrQ &= mstrAnalysis_Join

                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                            "AND lnloan_advance_tran.isloan = 1 " & _
                            "AND payperiodunkid = @PeriodId "


                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId"
                End If

                StrQ &= "GROUP BY  " & mstrField & "  payperiodunkid " & _
                          ", hremployee_master.employeeunkid " & _
                    "UNION ALL " & _
                    "SELECT  payperiodunkid AS PeriodId " & _
                          ", hremployee_master.employeeunkid AS EmpId " & _
                          ", -1 AS TranId " & _
                              ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                          ", 2 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields

                StrQ &= "FROM   " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrToDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrToDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= mstrAnalysis_Join

                StrQ &= "            LEFT JOIN " & _
                                   "( " & _
                                   "    SELECT " & _
                                   "         stationunkid " & _
                                   "        ,deptgroupunkid " & _
                                   "        ,departmentunkid " & _
                                   "        ,sectiongroupunkid " & _
                                   "        ,sectionunkid " & _
                                   "        ,unitgroupunkid " & _
                                   "        ,unitunkid " & _
                                   "        ,teamunkid " & _
                                   "        ,classgroupunkid " & _
                                   "        ,classunkid " & _
                                   "        ,employeeunkid " & _
                                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                   "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                   ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                            "AND lnloan_advance_tran.isloan = 0 " & _
                            "AND payperiodunkid = @PeriodId "

                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId"
                End If

                StrQ &= " GROUP BY " & mstrField & _
                            " payperiodunkid, hremployee_master.employeeunkid " & _
                        "	UNION ALL " & _
                        "SELECT  payperiodunkid AS PeriodId " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", -1 AS TranId " & _
                                  ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                              ", 3 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields

                StrQ &= "FROM   " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "JOIN " & mstrToDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= mstrAnalysis_Join

                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                                "AND payperiodunkid = @PeriodId "

                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId"
                End If

                StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                          ", hremployee_master.employeeunkid "
            End If

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            If mblnShowEmployerContribution = True Then

                StrQ &= "UNION ALL " & _
                        "SELECT  payperiodunkid AS PeriodId " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                              ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                              ", 11 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields

                StrQ &= "FROM   " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "JOIN " & mstrToDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                          "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= mstrAnalysis_Join

                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployersStatutoryContributions & " " & _
                                "AND payperiodunkid = @PeriodId "


                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId"
                End If

                StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                              ", hremployee_master.employeeunkid " & _
                              ", prpayrollprocess_tran.tranheadunkid "

            End If

            If mblnShowInformationalHeads = True Then

                StrQ &= "UNION ALL " & _
                        "SELECT  payperiodunkid AS PeriodId " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                              ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                              ", 12 AS Mid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                StrQ &= mstrAnalysis_Fields

                StrQ &= "FROM   " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "JOIN " & mstrToDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                          "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= mstrAnalysis_Join

                StrQ &= "            LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                    "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                'Pinkal (31-Mar-2023) -- Start
                'TRA Payroll Report Performance Issue.
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Pinkal (31-Mar-2023) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " " & _
                                "AND payperiodunkid = @PeriodId "


                If mintEmpId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId"
                End If

                StrQ &= "GROUP BY " & mstrField & " payperiodunkid " & _
                              ", hremployee_master.employeeunkid " & _
                              ", prpayrollprocess_tran.tranheadunkid "

            End If
            'Sohail (23 Feb 2017) -- End

            StrQ &= ") AS payroll " & _
                    "RIGHT JOIN " & mstrToDatabaseName & "..hremployee_master ON payroll.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid " & _
                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                    "LEFT JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = payroll.PeriodId " & _
                                                 "AND cfcommon_period_tran.isactive = 1 " & _
                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= mstrAnalysis_Join

            StrQ &= "            LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (31-Mar-2023) -- Start
            'TRA Payroll Report Performance Issue.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Pinkal (31-Mar-2023) -- End

            StrQ &= "	WHERE 1 = 1 "

            If mintEmpId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mintBranchId > 0 Then
                StrQ &= " AND T.stationunkid = @BranchId"
            End If

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY  " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    StrQ &= "ORDER BY " & mstrAnalysis_OrderBy & ",  hremployee_master.employeecode "
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY  " & Me.OrderByQuery
                Else
                    StrQ &= "ORDER BY  hremployee_master.employeecode "
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextPeriodId)

            StrQ = StrQ.Replace(mstrFromDatabaseName, mstrToDatabaseName) 'Sohail (20 Jul 2012)

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            If mintTranHeadTypeId > 0 Then
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadTypeId)
            End If
            If mintTranHeadUnkId > 0 Then
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadUnkId)
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            End If

            dt2 = objDataOperation.ExecQuery(StrQ, "payroll").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strKey As String = ""
            Dim strPrevKey As String = ""
            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dt1.Rows.Count
            For i As Integer = 0 To intRowCount - 1
                drRow = dt1.Rows(i)

                strKey = drRow.Item("PeriodId").ToString & "_" & drRow.Item("EmpId")

                If strPrevKey <> strKey Then
                    rpt_Row = dtPeriod1Table.NewRow


                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes
                    If dt1.Columns.Contains("Id") Then
                        rpt_Row.Item("Id") = drRow.Item("Id")
                    End If

                    If dt1.Columns.Contains("GName") Then
                        rpt_Row.Item("GName") = drRow.Item("GName")
                    End If

                    'Pinkal (15-Jan-2013) -- End


                    rpt_Row.Item("EmpId") = drRow.Item("EmpId")

                    'Pinkal (24-Sep-2012) -- Start
                    'Enhancement : TRA Changes
                    rpt_Row.Item("Code") = drRow.Item("Code")
                    'Pinkal (24-Sep-2012) -- End

                    rpt_Row.Item("EmpName") = drRow.Item("EmpName")



                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes
                    'rpt_Row.Item("NetPayBF") = Format(CDec(drRow.Item("OpenBal")), GUI.fmtCurrency)
                    'rpt_Row.Item("NetPayBF") = CDec(drRow.Item("OpenBal"))
                    rpt_Row.Item("NetPayBF") = Format(CDec(drRow.Item("OpenBal")), strfmtCurrency)
                    'Pinkal (15-Jan-2013) -- End


                Else
                    rpt_Row = dtPeriod1Table.Rows(dtPeriod1Table.Rows.Count - 1)
                End If


                If drRow.Item("TranId").ToString = "-99" OrElse drRow.Item("Mid").ToString = "-1" Then
                    ' Do nothing

                    'Sohail (23 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
                ElseIf drRow.Item("Mid").ToString = "11" Then 'Employer Contribution
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))

                ElseIf drRow.Item("Mid").ToString = "12" Then 'Informational
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (23 Feb 2017) -- End

                ElseIf drRow.Item("Mid").ToString = "10" Then 'Earning
                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                    'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)

                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                    'rpt_Row.Item("TGP") = (CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))).ToString
                    'rpt_Row.Item("Column" & drRow.Item("TranId")) = (CDec(drRow.Item("Amount"))).ToString

                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))

                    'Pinkal (15-Jan-2013) -- End
                    'Sohail (21 Jul 2012) -- End

                    'Sohail (12 Nov 2014) -- Start
                    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                ElseIf drRow.Item("Mid").ToString = "7" Then 'Claim & Request Earning
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (12 Nov 2014) -- End

                Else
                    If drRow.Item("Mid").ToString = "1" Then   'LOAN
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Loan") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)

                        'Pinkal (15-Jan-2013) -- Start
                        'Enhancement : TRA Changes
                        'rpt_Row.Item("Loan") = (CDec(drRow.Item("Amount"))).ToString
                        rpt_Row.Item("Loan") = CDec(drRow.Item("Amount"))
                        'Pinkal (15-Jan-2013) -- End


                        'Sohail (21 Jul 2012) -- End
                    ElseIf drRow.Item("Mid").ToString = "2" Then 'ADVANCE
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Advance") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)

                        'Pinkal (15-Jan-2013) -- Start
                        'Enhancement : TRA Changes
                        'rpt_Row.Item("Advance") = (CDec(drRow.Item("Amount"))).ToString
                        rpt_Row.Item("Advance") = CDec(drRow.Item("Amount"))
                        'Pinkal (15-Jan-2013) -- End


                        'Sohail (21 Jul 2012) -- End
                    ElseIf drRow.Item("Mid").ToString = "3" Then 'SAVINGS
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Savings") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)

                        'Pinkal (15-Jan-2013) -- Start
                        'Enhancement : TRA Changes
                        'rpt_Row.Item("Savings") = (CDec(drRow.Item("Amount"))).ToString
                        rpt_Row.Item("Savings") = CDec(drRow.Item("Amount"))
                        'Pinkal (15-Jan-2013) -- End


                        'Sohail (21 Jul 2012) -- End
                    ElseIf drRow.Item("Mid").ToString = "0" Then 'Deduction
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)


                        'Pinkal (15-Jan-2013) -- Start
                        'Enhancement : TRA Changes
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = (CDec(drRow.Item("Amount"))).ToString
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Pinkal (15-Jan-2013) -- End
                        'Sohail (21 Jul 2012) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                    ElseIf drRow.Item("Mid").ToString = "8" Then 'Claim & Request Deduction
                        rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (12 Nov 2014) -- End

                    End If
                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)

                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes
                    'rpt_Row.Item("TDD") = (CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))).ToString
                    rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                    'Pinkal (15-Jan-2013) -- End


                    'Sohail (21 Jul 2012) -- End
                End If

                'Sohail (21 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                'rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), GUI.fmtCurrency)

                'rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)

                'rpt_Row.Item("TotalNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("NetPayBF")), GUI.fmtCurrency)
                'rpt_Row.Item("TotalNetPay") = Rounding.BRound(CDec(rpt_Row.Item("TotalNetPay")), ConfigParameter._Object._RoundOff_Type)

                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                'rpt_Row.Item("NetPay") = (CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD"))).ToString
                rpt_Row.Item("NetPay") = CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD"))
                'Pinkal (15-Jan-2013) -- End

                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                'rpt_Row.Item("TotalNetPay") = (CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("NetPayBF"))).ToString
                rpt_Row.Item("TotalNetPay") = CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("NetPayBF"))
                'Pinkal (15-Jan-2013) -- End


                'Sohail (21 Jul 2012) -- End

                If strPrevKey <> strKey Then
                    dtPeriod1Table.Rows.Add(rpt_Row)
                End If
                strPrevKey = strKey
            Next

            strKey = ""
            strPrevKey = ""
            rpt_Row = Nothing
            intRowCount = dt2.Rows.Count
            For i As Integer = 0 To intRowCount - 1
                drRow = dt2.Rows(i)

                strKey = drRow.Item("PeriodId").ToString & "_" & drRow.Item("EmpId")

                If strPrevKey <> strKey Then
                    rpt_Row = dtPeriod2Table.NewRow


                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes
                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes
                    If dt2.Columns.Contains("Id") Then
                        rpt_Row.Item("Id") = drRow.Item("Id")
                    End If

                    If dt2.Columns.Contains("GName") Then
                        rpt_Row.Item("GName") = drRow.Item("GName")
                    End If

                    'Pinkal (15-Jan-2013) -- End
                    'Pinkal (15-Jan-2013) -- End


                    rpt_Row.Item("EmpId") = drRow.Item("EmpId")

                    'Pinkal (24-Sep-2012) -- Start
                    'Enhancement : TRA Changes
                    rpt_Row.Item("Code") = drRow.Item("Code")
                    'Pinkal (24-Sep-2012) -- End
                    rpt_Row.Item("EmpName") = drRow.Item("EmpName")


                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes
                    'rpt_Row.Item("NetPayBF") = Format(CDec(drRow.Item("OpenBal")), GUI.fmtCurrency)
                    'rpt_Row.Item("NetPayBF") = CDec(drRow.Item("OpenBal"))
                    rpt_Row.Item("NetPayBF") = Format(CDec(drRow.Item("OpenBal")), strfmtCurrency)
                    'Pinkal (15-Jan-2013) -- End


                Else
                    rpt_Row = dtPeriod2Table.Rows(dtPeriod2Table.Rows.Count - 1)
                End If


                If drRow.Item("TranId").ToString = "-99" OrElse drRow.Item("Mid").ToString = "-1" Then
                    ' Do nothing

                    'Sohail (23 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
                ElseIf drRow.Item("Mid").ToString = "11" Then 'Employer Contribution
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))

                ElseIf drRow.Item("Mid").ToString = "12" Then 'Informational
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (23 Feb 2017) -- End

                ElseIf drRow.Item("Mid").ToString = "10" Then 'Earning
                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                    'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)


                    rpt_Row.Item("Column" & drRow.Item("TranId")) = drRow.Item("Amount")

                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                    'rpt_Row.Item("TGP") = (CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))).ToString
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))

                    'Pinkal (15-Jan-2013) -- End
                    'Sohail (21 Jul 2012) -- End

                    'Sohail (12 Nov 2014) -- Start
                    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                ElseIf drRow.Item("Mid").ToString = "7" Then 'Claim & Request Earning
                    rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    'Sohail (12 Nov 2014) -- End

                Else
                    If drRow.Item("Mid").ToString = "1" Then   'LOAN
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Loan") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Loan") = drRow.Item("Amount")
                        'Sohail (21 Jul 2012) -- End
                    ElseIf drRow.Item("Mid").ToString = "2" Then 'ADVANCE
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Advance") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Advance") = drRow.Item("Amount")
                        'Sohail (21 Jul 2012) -- End
                    ElseIf drRow.Item("Mid").ToString = "3" Then 'SAVINGS
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Savings") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Savings") = drRow.Item("Amount")
                        'Sohail (21 Jul 2012) -- End
                    ElseIf drRow.Item("Mid").ToString = "0" Then 'Deduction
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = drRow.Item("Amount")
                        'Sohail (21 Jul 2012) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                    ElseIf drRow.Item("Mid").ToString = "8" Then 'Claim & Request Deduction
                        rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (12 Nov 2014) -- End

                    End If
                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)


                    'Pinkal (15-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                    'rpt_Row.Item("TDD") = (CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))).ToString
                    rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))

                    'Pinkal (15-Jan-2013) -- End


                    'Sohail (21 Jul 2012) -- End
                End If

                'Sohail (21 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                'rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), GUI.fmtCurrency)

                'rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)

                'rpt_Row.Item("TotalNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("NetPayBF")), GUI.fmtCurrency)
                'rpt_Row.Item("TotalNetPay") = Rounding.BRound(CDec(rpt_Row.Item("TotalNetPay")), ConfigParameter._Object._RoundOff_Type)


                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes

                'rpt_Row.Item("NetPay") = (CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD"))).ToString
                'rpt_Row.Item("TotalNetPay") = (CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("NetPayBF"))).ToString

                rpt_Row.Item("NetPay") = CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD"))
                rpt_Row.Item("TotalNetPay") = CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("NetPayBF"))

                'Pinkal (15-Jan-2013) -- End


                'Sohail (21 Jul 2012) -- End

                If strPrevKey <> strKey Then
                    dtPeriod2Table.Rows.Add(rpt_Row)
                End If
                strPrevKey = strKey
            Next

            intRowCount = dtPeriod2Table.Rows.Count
            Dim intColIdx As Integer
            Dim intcount As Integer = 1
            Dim dtRow As DataRow
            Dim dRow() As DataRow
            For i As Integer = 0 To intRowCount - 1
                dtRow = dtPeriod2Table.Rows(i)

                rpt_Row = dtFinalTable.NewRow
                intColIdx = 0

                rpt_Row.Item(mdicColumns(intColIdx)) = dtRow.Item(mdicColumns(intColIdx))
                intColIdx += 1

                'Sohail (12 Jul 2012) -- Start
                'TRA - ENHANCEMENT - SrNo.
                'rpt_Row.Item(mdicColumns(intColIdx)) = dtRow.Item(mdicColumns(intColIdx))
                'rpt_Row.Item(mdicColumns(intColIdx)) = i + 1
                rpt_Row.Item(mdicColumns(intColIdx)) = intcount
                'Sohail (12 Jul 2012) -- End
                intColIdx += 1


                'Pinkal (24-Sep-2012) -- Start
                'Enhancement : TRA Changes ---EMP CODE
                rpt_Row.Item(mdicColumns(intColIdx)) = dtRow.Item(mdicColumns(intColIdx))
                intColIdx += 1
                'Pinkal (24-Sep-2012) -- End

                rpt_Row.Item(mdicColumns(intColIdx)) = dtRow.Item(mdicColumns(intColIdx))
                intColIdx += 1


                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes

                rpt_Row.Item(mdicColumns(intColIdx)) = dtRow.Item(mdicColumns(intColIdx))
                intColIdx += 1

                rpt_Row.Item(mdicColumns(intColIdx)) = dtRow.Item(mdicColumns(intColIdx))
                intColIdx += 1

                'Pinkal (15-Jan-2013) -- End



                dRow = dtPeriod1Table.Select("EmpId ='" & dtRow.Item("EmpId") & "'")


                'Pinkal (24-Sep-2012) -- Start
                'Enhancement : TRA Changes
                'Sohail (10 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                'Dim mblnSkip As Boolean = False
                Dim mblnSkip As Boolean = True
                'Sohail (10 Nov 2012) -- End
                'Pinkal (24-Sep-2012) -- End


                If dRow.Length > 0 Then
                    For j As Integer = intColIdx To dRow(0).ItemArray.Length - 1
                        'Sohail (17 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                        rpt_Row.Item(mdicColumns(j)) = CDec(dRow(0).Item(mdicColumns(j + 2)))

                        rpt_Row.Item(mdicColumns(j + 1)) = CDec(dtRow.Item(mdicColumns(j + 2)))
                        j += 2
                        'Sohail (17 Feb 2017) -- End

                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item(mdicColumns(j)) = Format(CDec(dtRow.Item(mdicColumns(j))) - CDec(dRow(0).Item(mdicColumns(j))), GUI.fmtCurrency)

                        'Pinkal (24-Sep-2012) -- Start
                        'Enhancement : TRA Changes

                        rpt_Row.Item(mdicColumns(j)) = (CDec(dtRow.Item(mdicColumns(j))) - CDec(dRow(0).Item(mdicColumns(j)))).ToString

                        If mblnIgnoreZeroVariance Then

                            'Sohail (10 Nov 2012) -- Start
                            'TRA - ENHANCEMENT - Issue : Report doesnot match with Payroll Total Variance and Payroll Summary.
                            'If CDec(rpt_Row.Item(mdicColumns(j))) = 0 Then
                            '    mblnSkip = True
                            'Else
                            '    mblnSkip = False
                            'End If
                            If CDec(rpt_Row.Item(mdicColumns(j))) <> 0 Then
                                mblnSkip = False
                            End If
                            'Sohail (10 Nov 2012) -- End

                            'Pinkal (24-May-2013) -- Start
                            'Enhancement : TRA Changes

                        Else
                            mblnSkip = False
                        End If

                        'Pinkal (24-May-2013) -- End

                        'Pinkal (24-Sep-2012) -- End

                        'Sohail (21 Jul 2012) -- End

                        'Hemant (25 Dec 2018) -- Start
                        'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                        'Hemant (22 Jan 2019) -- Start
                        'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                        If mblnShowVariancePercentageColumns = True Then
                            'Hemant (22 Jan 2019) -- End
                            If CDec(dRow(0).Item(mdicColumns(j))) > 0 Then
                                rpt_Row.Item(mdicColumns(j + 1)) = ((CDec(dtRow.Item(mdicColumns(j))) - CDec(dRow(0).Item(mdicColumns(j)))) * 100 / CDec(dRow(0).Item(mdicColumns(j)))).ToString("0.00")
                            Else
                                rpt_Row.Item(mdicColumns(j + 1)) = "0.00"
                            End If
                            j += 1
                        End If
                        'Hemant (25 Dec 2018) -- End

                    Next


                    'Pinkal (24-Sep-2012) -- Start
                    'Enhancement : TRA Changes
                    If mblnSkip = False Then
                        dtFinalTable.Rows.Add(rpt_Row)
                        intcount += 1
                    End If
                    'Pinkal (24-Sep-2012) -- End


                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                Else
                    For j As Integer = intColIdx To dtRow.ItemArray.Length - 1
                        'Sohail (22 Jan 2018) -- Start
                        'Issue: SUPPORT CCBRT#1896 | Basic salary showing zero in Variance Report for newly hired employee in first period of new year db in 70.1.
                        'rpt_Row.Item(mdicColumns(j)) = dtRow.Item(mdicColumns(j))
                        rpt_Row.Item(mdicColumns(j)) = 0

                        rpt_Row.Item(mdicColumns(j + 1)) = CDec(dtRow.Item(mdicColumns(j + 2)))
                        j += 2

                        rpt_Row.Item(mdicColumns(j)) = (CDec(dtRow.Item(mdicColumns(j))) - 0).ToString

                        If mblnIgnoreZeroVariance Then

                            If CDec(rpt_Row.Item(mdicColumns(j))) <> 0 Then
                                mblnSkip = False
                            End If

                        Else
                            mblnSkip = False
                        End If
                        'Sohail (22 Jan 2018) -- End
                        'Hemant (22 Jan 2019) -- Start
                        'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                        If mblnShowVariancePercentageColumns = True Then
                            'Hemant (22 Jan 2019) -- End                            
                            rpt_Row.Item(mdicColumns(j + 1)) = "0.00"
                            j += 1
                        End If
                        'Hemant (22 Jan 2019) -- End

                    Next
                    dtFinalTable.Rows.Add(rpt_Row)
                    'Sohail (21 Jul 2012) -- End
                End If
            Next
            'Sohail (16 Jun 2012) -- End

            dtFinalTable.Columns.Remove("EmpId")

            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mintTranHeadTypeId > 0 Then
                Select Case mintTranHeadTypeId
                    Case enTranHeadType.EarningForEmployees
                        For i As Integer = 0 To marrDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrDeductionIDs.Item(i).ToString)
                            End If
                            'Sohail (17 Feb 2017) -- Start
                            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                            If dtFinalTable.Columns.Contains("FP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                            'Sohail (17 Feb 2017) -- End
                        Next
                        For i As Integer = 0 To marrStatutoryDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Sohail (17 Feb 2017) -- Start
                            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                            If dtFinalTable.Columns.Contains("FP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                            'Sohail (17 Feb 2017) -- End
                        Next
                        'Sohail (23 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
                        For i As Integer = 0 To marrEmployerContributionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrInformationalIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        'Sohail (23 Feb 2017) -- End

                    Case enTranHeadType.DeductionForEmployee
                        For i As Integer = 0 To marrEarningIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEarningIDs.Item(i).ToString)
                            End If
                            'Sohail (17 Feb 2017) -- Start
                            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                            If dtFinalTable.Columns.Contains("FP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEarningIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                            'Sohail (17 Feb 2017) -- End
                        Next
                        For i As Integer = 0 To marrStatutoryDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Sohail (17 Feb 2017) -- Start
                            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                            If dtFinalTable.Columns.Contains("FP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                            'Sohail (17 Feb 2017) -- End
                        Next
                        'Sohail (23 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
                        For i As Integer = 0 To marrEmployerContributionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrInformationalIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        'Sohail (23 Feb 2017) -- End

                    Case enTranHeadType.EmployeesStatutoryDeductions
                        For i As Integer = 0 To marrEarningIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEarningIDs.Item(i).ToString)
                            End If
                            'Sohail (17 Feb 2017) -- Start
                            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                            If dtFinalTable.Columns.Contains("FP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEarningIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                            'Sohail (17 Feb 2017) -- End
                        Next
                        For i As Integer = 0 To marrDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrDeductionIDs.Item(i).ToString)
                            End If
                            'Sohail (17 Feb 2017) -- Start
                            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                            If dtFinalTable.Columns.Contains("FP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                            'Sohail (17 Feb 2017) -- End
                        Next
                        'Sohail (23 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
                        For i As Integer = 0 To marrEmployerContributionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrInformationalIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next

                    Case enTranHeadType.EmployersStatutoryContributions
                        For i As Integer = 0 To marrEarningIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEarningIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEarningIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrStatutoryDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrInformationalIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrInformationalIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrInformationalIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next

                    Case enTranHeadType.Informational
                        For i As Integer = 0 To marrEarningIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEarningIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEarningIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEarningIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEarningIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrStatutoryDeductionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrStatutoryDeductionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrStatutoryDeductionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End
                        Next
                        For i As Integer = 0 To marrEmployerContributionIDs.Count - 1
                            If dtFinalTable.Columns.Contains(marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove(marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("FP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("FP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            If dtFinalTable.Columns.Contains("TP" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("TP" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- Start
                            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                            If dtFinalTable.Columns.Contains("Per" & marrEmployerContributionIDs.Item(i).ToString) = True Then
                                dtFinalTable.Columns.Remove("Per" & marrEmployerContributionIDs.Item(i).ToString)
                            End If
                            'Hemant (22 Jan 2019) -- End 
                        Next
                        'Sohail (23 Feb 2017) -- End

                End Select
            End If
            'Sohail (12 Jul 2012) -- End

            'Sohail (20 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mintTranHeadUnkId > 0 Then
                If mintTranHeadTypeId > 0 Then
                    Select Case mintTranHeadTypeId
                        Case enTranHeadType.EarningForEmployees
                            For i As Integer = 0 To marrEarningIDs.Count - 1
                                If dtFinalTable.Columns.Contains(marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove(marrEarningIDs.Item(i).ToString)
                                End If
                                'Sohail (17 Feb 2017) -- Start
                                'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                                If dtFinalTable.Columns.Contains("FP" & marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("FP" & marrEarningIDs.Item(i).ToString)
                                End If
                                If dtFinalTable.Columns.Contains("TP" & marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("TP" & marrEarningIDs.Item(i).ToString)
                                End If
                                'Sohail (17 Feb 2017) -- End
                                'Hemant (01 Mar 2019) -- Start
                                'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                                If dtFinalTable.Columns.Contains("Per" & marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("Per" & marrEarningIDs.Item(i).ToString)
                                End If
                                'Hemant (01 Mar 2019) -- End
                            Next

                        Case enTranHeadType.DeductionForEmployee
                            For i As Integer = 0 To marrDeductionIDs.Count - 1
                                If dtFinalTable.Columns.Contains(marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove(marrDeductionIDs.Item(i).ToString)
                                End If
                                'Sohail (17 Feb 2017) -- Start
                                'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                                If dtFinalTable.Columns.Contains("FP" & marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("FP" & marrDeductionIDs.Item(i).ToString)
                                End If
                                If dtFinalTable.Columns.Contains("TP" & marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("TP" & marrDeductionIDs.Item(i).ToString)
                                End If
                                'Sohail (17 Feb 2017) -- End
                                'Hemant (01 Mar 2019) -- Start
                                'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                                If dtFinalTable.Columns.Contains("Per" & marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("Per" & marrDeductionIDs.Item(i).ToString)
                                End If
                                'Hemant (01 Mar 2019) -- End
                            Next

                        Case enTranHeadType.EmployeesStatutoryDeductions
                            For i As Integer = 0 To marrStatutoryDeductionIDs.Count - 1
                                If dtFinalTable.Columns.Contains(marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove(marrStatutoryDeductionIDs.Item(i).ToString)
                                End If
                                'Sohail (17 Feb 2017) -- Start
                                'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                                If dtFinalTable.Columns.Contains("FP" & marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("FP" & marrStatutoryDeductionIDs.Item(i).ToString)
                                End If
                                If dtFinalTable.Columns.Contains("TP" & marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("TP" & marrStatutoryDeductionIDs.Item(i).ToString)
                                End If
                                'Sohail (17 Feb 2017) -- End
                                'Hemant (01 Mar 2019) -- Start
                                'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                                If dtFinalTable.Columns.Contains("Per" & marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("Per" & marrStatutoryDeductionIDs.Item(i).ToString)
                                End If
                                'Hemant (01 Mar 2019) -- End
                            Next

                            'Sohail (23 Feb 2017) -- Start
                            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
                        Case enTranHeadType.EmployersStatutoryContributions
                            For i As Integer = 0 To marrEmployerContributionIDs.Count - 1
                                If dtFinalTable.Columns.Contains(marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove(marrEmployerContributionIDs.Item(i).ToString)
                                End If
                                If dtFinalTable.Columns.Contains("FP" & marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("FP" & marrEmployerContributionIDs.Item(i).ToString)
                                End If
                                If dtFinalTable.Columns.Contains("TP" & marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("TP" & marrEmployerContributionIDs.Item(i).ToString)
                                End If
                                'Hemant (01 Mar 2019) -- Start
                                'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                                If dtFinalTable.Columns.Contains("Per" & marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("Per" & marrEmployerContributionIDs.Item(i).ToString)
                                End If
                                'Hemant (01 Mar 2019) -- End
                            Next

                        Case enTranHeadType.Informational
                            For i As Integer = 0 To marrInformationalIDs.Count - 1
                                If dtFinalTable.Columns.Contains(marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove(marrInformationalIDs.Item(i).ToString)
                                End If
                                If dtFinalTable.Columns.Contains("FP" & marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("FP" & marrInformationalIDs.Item(i).ToString)
                                End If
                                If dtFinalTable.Columns.Contains("TP" & marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("TP" & marrInformationalIDs.Item(i).ToString)
                                End If
                                'Hemant (01 Mar 2019) -- Start
                                'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                                If dtFinalTable.Columns.Contains("Per" & marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                                    dtFinalTable.Columns.Remove("Per" & marrInformationalIDs.Item(i).ToString)
                                End If
                                'Hemant (01 Mar 2019) -- End
                            Next
                            'Sohail (23 Feb 2017) -- End

                    End Select
                Else
                    For i As Integer = 0 To marrEarningIDs.Count - 1
                        If dtFinalTable.Columns.Contains(marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove(marrEarningIDs.Item(i).ToString)
                        End If
                        'Sohail (17 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                        If dtFinalTable.Columns.Contains("FP" & marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("FP" & marrEarningIDs.Item(i).ToString)
                        End If
                        If dtFinalTable.Columns.Contains("TP" & marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("TP" & marrEarningIDs.Item(i).ToString)
                        End If
                        'Sohail (17 Feb 2017) -- End
                        'Hemant (01 Mar 2019) -- Start
                        'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                        If dtFinalTable.Columns.Contains("Per" & marrEarningIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEarningIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("Per" & marrEarningIDs.Item(i).ToString)
                        End If
                        'Hemant (01 Mar 2019) -- End
                    Next
                    For i As Integer = 0 To marrDeductionIDs.Count - 1
                        If dtFinalTable.Columns.Contains(marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove(marrDeductionIDs.Item(i).ToString)
                        End If
                        'Sohail (17 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                        If dtFinalTable.Columns.Contains("FP" & marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("FP" & marrDeductionIDs.Item(i).ToString)
                        End If
                        If dtFinalTable.Columns.Contains("TP" & marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("TP" & marrDeductionIDs.Item(i).ToString)
                        End If
                        'Sohail (17 Feb 2017) -- End
                        'Hemant (01 Mar 2019) -- Start
                        'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                        If dtFinalTable.Columns.Contains("Per" & marrDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("Per" & marrDeductionIDs.Item(i).ToString)
                        End If
                        'Hemant (01 Mar 2019) -- End
                    Next
                    For i As Integer = 0 To marrStatutoryDeductionIDs.Count - 1
                        If dtFinalTable.Columns.Contains(marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove(marrStatutoryDeductionIDs.Item(i).ToString)
                        End If
                        'Sohail (17 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                        If dtFinalTable.Columns.Contains("FP" & marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("FP" & marrStatutoryDeductionIDs.Item(i).ToString)
                        End If
                        If dtFinalTable.Columns.Contains("TP" & marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("TP" & marrStatutoryDeductionIDs.Item(i).ToString)
                        End If
                        'Sohail (17 Feb 2017) -- End
                        'Hemant (01 Mar 2019) -- Start
                        'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                        If dtFinalTable.Columns.Contains("Per" & marrStatutoryDeductionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrStatutoryDeductionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("Per" & marrStatutoryDeductionIDs.Item(i).ToString)
                        End If
                        'Hemant (01 Mar 2019) -- End
                    Next
                    'Sohail (23 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
                    For i As Integer = 0 To marrEmployerContributionIDs.Count - 1
                        If dtFinalTable.Columns.Contains(marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove(marrEmployerContributionIDs.Item(i).ToString)
                        End If
                        If dtFinalTable.Columns.Contains("FP" & marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("FP" & marrEmployerContributionIDs.Item(i).ToString)
                        End If
                        If dtFinalTable.Columns.Contains("TP" & marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("TP" & marrEmployerContributionIDs.Item(i).ToString)
                        End If
                        'Hemant (01 Mar 2019) -- Start
                        'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                        If dtFinalTable.Columns.Contains("Per" & marrEmployerContributionIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrEmployerContributionIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("Per" & marrEmployerContributionIDs.Item(i).ToString)
                        End If
                        'Hemant (01 Mar 2019) -- End
                    Next
                    For i As Integer = 0 To marrInformationalIDs.Count - 1
                        If dtFinalTable.Columns.Contains(marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove(marrInformationalIDs.Item(i).ToString)
                        End If
                        If dtFinalTable.Columns.Contains("FP" & marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("FP" & marrInformationalIDs.Item(i).ToString)
                        End If
                        If dtFinalTable.Columns.Contains("TP" & marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("TP" & marrInformationalIDs.Item(i).ToString)
                        End If
                        'Hemant (01 Mar 2019) -- Start
                        'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                        If dtFinalTable.Columns.Contains("Per" & marrInformationalIDs.Item(i).ToString) = True AndAlso ("Column" & mintTranHeadUnkId.ToString) <> marrInformationalIDs.Item(i).ToString Then
                            dtFinalTable.Columns.Remove("Per" & marrInformationalIDs.Item(i).ToString)
                        End If
                        'Hemant (01 Mar 2019) -- End
                    Next
                    'Sohail (23 Feb 2017) -- End
                End If
            End If
            'Sohail (20 Jul 2012) -- End

            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT - Show Only Selected Heads
            'Sohail (14 Mar 2014) -- Start
            'Enhancement - Show Gross Pay column if head type Earning is selected and show Total Deuction column if head type Deduction is selected.
            'If mintTranHeadTypeId > 0 OrElse mintTranHeadUnkId > 0 Then
            If mintTranHeadTypeId > 0 AndAlso mintTranHeadUnkId > 0 Then
                'Sohail (14 Mar 2014) -- End
                dtFinalTable.Columns.Remove("TGP")
                dtFinalTable.Columns.Remove("TDD")
                dtFinalTable.Columns.Remove("NetPayBF")
                dtFinalTable.Columns.Remove("NetPay")
                dtFinalTable.Columns.Remove("TotalNetPay")
                'Sohail (17 Feb 2017) -- Start
                'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                dtFinalTable.Columns.Remove("FPTGP")
                dtFinalTable.Columns.Remove("TPTGP")
                dtFinalTable.Columns.Remove("FPTDD")
                dtFinalTable.Columns.Remove("TPTDD")
                dtFinalTable.Columns.Remove("FPNetPayBF")
                dtFinalTable.Columns.Remove("TPNetPayBF")
                dtFinalTable.Columns.Remove("FPNetPay")
                dtFinalTable.Columns.Remove("TPNetPay")
                dtFinalTable.Columns.Remove("FPTotalNetPay")
                dtFinalTable.Columns.Remove("TPTotalNetPay")
                'Sohail (17 Feb 2017) -- End
                'Hemant (22 Jan 2019) -- Start
                'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                'Hemant (01 Mar 2019) -- Start
                'Issue # 0003538: Error on producing payroll variance report for informational heads (Total Earning).
                If mblnShowVariancePercentageColumns = True Then
                    'Hemant (01 Mar 2019) -- End
                    dtFinalTable.Columns.Remove("PerTGP")
                    dtFinalTable.Columns.Remove("PerTDD")
                    dtFinalTable.Columns.Remove("PerNetPayBF")
                    dtFinalTable.Columns.Remove("PerNetPay")
                    dtFinalTable.Columns.Remove("PerTotalNetPay")
                End If
                'Hemant (22 Jan 2019) -- End
                'Sohail (14 Mar 2014) -- Start
                'Enhancement - Show Gross Pay column if head type Earning is selected and show Total Deuction column if head type Deduction is selected.
            ElseIf mintTranHeadTypeId > 0 AndAlso mintTranHeadTypeId = enTranHeadType.EarningForEmployees Then
                dtFinalTable.Columns.Remove("TDD")
                dtFinalTable.Columns.Remove("NetPayBF")
                dtFinalTable.Columns.Remove("NetPay")
                dtFinalTable.Columns.Remove("TotalNetPay")
                'Sohail (17 Feb 2017) -- Start
                'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                dtFinalTable.Columns.Remove("FPTDD")
                dtFinalTable.Columns.Remove("TPTDD")
                dtFinalTable.Columns.Remove("FPNetPayBF")
                dtFinalTable.Columns.Remove("TPNetPayBF")
                dtFinalTable.Columns.Remove("FPNetPay")
                dtFinalTable.Columns.Remove("TPNetPay")
                dtFinalTable.Columns.Remove("FPTotalNetPay")
                dtFinalTable.Columns.Remove("TPTotalNetPay")
                'Sohail (17 Feb 2017) -- End
                'Hemant (22 Jan 2019) -- Start
                'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                If mblnShowVariancePercentageColumns = True Then
                    dtFinalTable.Columns.Remove("PerTDD")
                    dtFinalTable.Columns.Remove("PerNetPayBF")
                    dtFinalTable.Columns.Remove("PerNetPay")
                    dtFinalTable.Columns.Remove("PerTotalNetPay")
                End If
                'Hemant (22 Jan 2019) -- End
            ElseIf mintTranHeadTypeId > 0 AndAlso (mintTranHeadTypeId = enTranHeadType.DeductionForEmployee OrElse mintTranHeadTypeId = enTranHeadType.EmployeesStatutoryDeductions) Then
                dtFinalTable.Columns.Remove("TGP")
                dtFinalTable.Columns.Remove("NetPayBF")
                dtFinalTable.Columns.Remove("NetPay")
                dtFinalTable.Columns.Remove("TotalNetPay")
                'Sohail (14 Mar 2014) -- End
                'Sohail (17 Feb 2017) -- Start
                'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
                dtFinalTable.Columns.Remove("FPTGP")
                dtFinalTable.Columns.Remove("TPTGP")
                dtFinalTable.Columns.Remove("FPNetPayBF")
                dtFinalTable.Columns.Remove("TPNetPayBF")
                dtFinalTable.Columns.Remove("FPNetPay")
                dtFinalTable.Columns.Remove("TPNetPay")
                dtFinalTable.Columns.Remove("FPTotalNetPay")
                dtFinalTable.Columns.Remove("TPTotalNetPay")
                'Sohail (17 Feb 2017) -- End
                'Hemant (22 Jan 2019) -- Start
                'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                If mblnShowVariancePercentageColumns = True Then
                    dtFinalTable.Columns.Remove("PerTGP")
                    dtFinalTable.Columns.Remove("PerNetPayBF")
                    dtFinalTable.Columns.Remove("PerNetPay")
                    dtFinalTable.Columns.Remove("PerTotalNetPay")
                End If
                'Hemant (22 Jan 2019) -- End
                'Sohail (23 Feb 2017) -- Start
                'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            ElseIf mintTranHeadTypeId = mintTranHeadTypeId = enTranHeadType.EmployersStatutoryContributions Then
                dtFinalTable.Columns.Remove("TGP")
                dtFinalTable.Columns.Remove("TDD")
                dtFinalTable.Columns.Remove("NetPayBF")
                dtFinalTable.Columns.Remove("NetPay")
                dtFinalTable.Columns.Remove("TotalNetPay")
                dtFinalTable.Columns.Remove("FPTGP")
                dtFinalTable.Columns.Remove("TPTGP")
                dtFinalTable.Columns.Remove("FPTDD")
                dtFinalTable.Columns.Remove("TPTDD")
                dtFinalTable.Columns.Remove("FPNetPayBF")
                dtFinalTable.Columns.Remove("TPNetPayBF")
                dtFinalTable.Columns.Remove("FPNetPay")
                dtFinalTable.Columns.Remove("TPNetPay")
                dtFinalTable.Columns.Remove("FPTotalNetPay")
                dtFinalTable.Columns.Remove("TPTotalNetPay")
                'Hemant (22 Jan 2019) -- Start
                'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                If mblnShowVariancePercentageColumns = True Then
                    dtFinalTable.Columns.Remove("PerTGP")
                    dtFinalTable.Columns.Remove("PerTDD")
                    dtFinalTable.Columns.Remove("PerNetPayBF")
                    dtFinalTable.Columns.Remove("PerNetPay")
                    dtFinalTable.Columns.Remove("PerTotalNetPay")
                End If
                'Hemant (22 Jan 2019) -- End
            ElseIf mintTranHeadTypeId = mintTranHeadTypeId = enTranHeadType.Informational Then
                dtFinalTable.Columns.Remove("TGP")
                dtFinalTable.Columns.Remove("TDD")
                dtFinalTable.Columns.Remove("NetPayBF")
                dtFinalTable.Columns.Remove("NetPay")
                dtFinalTable.Columns.Remove("TotalNetPay")
                dtFinalTable.Columns.Remove("FPTGP")
                dtFinalTable.Columns.Remove("TPTGP")
                dtFinalTable.Columns.Remove("FPTDD")
                dtFinalTable.Columns.Remove("TPTDD")
                dtFinalTable.Columns.Remove("FPNetPayBF")
                dtFinalTable.Columns.Remove("TPNetPayBF")
                dtFinalTable.Columns.Remove("FPNetPay")
                dtFinalTable.Columns.Remove("TPNetPay")
                dtFinalTable.Columns.Remove("FPTotalNetPay")
                dtFinalTable.Columns.Remove("TPTotalNetPay")
                'Sohail (23 Feb 2017) -- End
                'Hemant (22 Jan 2019) -- Start
                'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                If mblnShowVariancePercentageColumns = True Then
                    dtFinalTable.Columns.Remove("PerTGP")
                    dtFinalTable.Columns.Remove("PerTDD")
                    dtFinalTable.Columns.Remove("PerNetPayBF")
                    dtFinalTable.Columns.Remove("PerNetPay")
                    dtFinalTable.Columns.Remove("PerTotalNetPay")
                End If
                'Hemant (22 Jan 2019) -- End

            End If
            'Sohail (10 Aug 2012) -- End

            dtFinalTable.AcceptChanges()

            Call FilterTitleAndFilterQuery()


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            'Dim dblTotal As Decimal = 0

            'Pinkal (24-Sep-2012) -- Start
            'Enhancement : TRA Changes
            'Dim m As Integer = 2
            'Dim n As Integer = 2

            'Dim m As Integer = 3
            'Dim n As Integer = 3
            'Pinkal (24-Sep-2012) -- End


            'dblColTot = New Decimal(dtFinalTable.Columns.Count) {}

            'While m < dtFinalTable.Columns.Count
            '    For t As Integer = 0 To dtFinalTable.Rows.Count - 1
            '        dblTotal += CDec(dtFinalTable.Rows(t)(m))
            '    Next
            '    dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
            '    dblTotal = 0
            '    m += 1
            '    n += 1
            'End While



            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
            '    'Sandeep [ 09 MARCH 2011 ] -- Start
            '    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
            '    'Sandeep [ 09 MARCH 2011 ] -- End 
            'End If


            'Sohail (15 Apr 2014) -- Start
            'Enhancement - Sorting on Payroll Variance Report
            'dtFinalTable = New DataView(dtFinalTable, "", "GName,code", DataViewRowState.CurrentRows).ToTable
            dtFinalTable = New DataView(dtFinalTable, "", "GName", DataViewRowState.CurrentRows).ToTable
            'Sohail (15 Apr 2014) -- End

            If mintViewIndex > 0 Then
                For i As Integer = 0 To dtFinalTable.Rows.Count - 1
                    dtFinalTable.Rows(i)("SrNo") = i + 1
                Next
                dtFinalTable.AcceptChanges()
            End If

            Dim strGTotal As String = Language.getMessage(mstrModuleName, 1, "Grand Total :")
            Dim strSubTotal As String = Language.getMessage(mstrModuleName, 4, "Sub Total :")
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            If mblnIgnoreZeroVariance Then
                mdtTableExcel = IgnoreZeroHead(dtFinalTable)
            Else
                mdtTableExcel = dtFinalTable
            End If


            'START TO REMOVE GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

            If mintViewIndex <= 0 Then
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns.Remove("GName")
            Else
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If

            'END TO REMOVE GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 


            row = New WorksheetRow()
            wcell = New WorksheetCell(mstrFromPeriodName & " - " & mstrNextPeriodName, "s10bw")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            'Sohail (17 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show columns for From Period Value, To Period Value and Variance Amount for each in Payroll Variance Report.
            row = New WorksheetRow()
            For Each col As DataColumn In mdtTableExcel.Columns
                If col.ColumnName.StartsWith("FP") = True Then
                    wcell = New WorksheetCell(col.ExtendedProperties("Caption").ToString, "HeaderStyle")
                    'Hemant (25 Dec 2018) -- Start
                    'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                    'wcell.MergeAcross = 2
                    'Hemant (22 Jan 2019) -- Start
                    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                    'wcell.MergeAcross = 3
                    If mblnShowVariancePercentageColumns = True Then
                        wcell.MergeAcross = 3
                    Else
                        wcell.MergeAcross = 2
                    End If
                    'Hemant (22 Jan 2019) -- End
                    'Hemant (25 Dec 2018) -- End
                    row.Cells.Add(wcell)
                ElseIf col.ColumnName.StartsWith("TP") = True Then

                ElseIf col.DataType Is System.Type.GetType("System.Decimal") Then
                    'Hemant (25 Dec 2018) -- Start
                    'Enhancement - On payroll variance report, provide a column that shows the percentage variance in 76.1.
                ElseIf col.ColumnName.StartsWith("Per") = True Then
                    'Hemant (25 Dec 2018) -- End
                Else
                    wcell = New WorksheetCell("", "HeaderStyle")
                    row.Cells.Add(wcell)
                End If

            Next
            rowsArrayHeader.Add(row)
            'Sohail (17 Feb 2017) -- End

            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, strGTotal, True, rowsArrayHeader)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, strGTotal, True, rowsArrayHeader)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, strGTotal, True, rowsArrayHeader)
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-May-2013) -- End

            'Pinkal (14-Dec-2012) -- End


            'Pinkal (15-Jan-2013) -- End


            '/***************************** DEDUCTION FROM DATATABLE(2) TO DATATABLE(1) *****************************/ -- END



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Recociliaton_Report; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Grand Total :")
            Language.setMessage(mstrModuleName, 2, "Prepared By :")
            Language.setMessage(mstrModuleName, 3, "Date :")
            Language.setMessage(mstrModuleName, 4, "Sub Total :")
            Language.setMessage(mstrModuleName, 5, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 6, "Branch :")
            Language.setMessage(mstrModuleName, 7, "Employee Code")
            Language.setMessage(mstrModuleName, 8, "Employee Name")
            Language.setMessage(mstrModuleName, 9, "Variance")
            Language.setMessage(mstrModuleName, 10, "Variance(%)")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
