'************************************************************************************************************************************
'Class Name : clsSalaryReconciliationReport.vb
'Purpose    :
'Date       :07 Oct 2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsSalaryReconciliationReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsSalaryReconciliationReport"
    Private mstrReportId As String = enArutiReport.Salary_Reconciliation_Report
    Dim objDataOperation As clsDataOperation

#Region "Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_DetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrEmpName As String = String.Empty
    Private mintEmployeeunkid As Integer = -1
    Private mdecCurrentScaleFrom As Decimal = 0
    Private mdecCurrentScaleTo As Decimal = 0
    Private mdecIncrementFrom As Decimal = 0
    Private mdecIncrementTo As Decimal = 0
    Private mdecNewScaleFrom As Decimal = 0
    Private mdecNewScaleTo As Decimal = 0
    Private mblnIncludeInactiveEmp As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mdtIncrementDateFrom As Date = Nothing
    Private mdtIncrementDateTo As Date = Nothing



    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Sohail (30 Dec 2015) -- Start
    'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
    Private mdtTableDetail As DataTable
    Private mdtTableFooter As DataTable
    Private mintFirstPeriodHeadCount As Integer = 0
    Private mintExitTotal As Integer = 0
    Private mintAppointedTotal As Integer = 0
    'Sohail (30 Dec 2015) -- End

    Private mstrAdvance_Filter As String = String.Empty


    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mintFromPeriodUnkId As Integer = 0
    Private mstrFromPeriodName As String = ""
    Private mintToPeriodUnkId As Integer = 0
    Private mstrToPeriodName As String = ""
    Private mdtFromPeriodStartDate As Date
    Private mdtFromPeriodEndDate As Date
    Private mdtToPeriodStartDate As Date
    Private mdtToPeriodEndDate As Date
    Private mstrPeriodIDs As String = ""
    Private mstrFromDatabaseName As String = FinancialYear._Object._DatabaseName
    Private mstrToDatabaseName As String = FinancialYear._Object._DatabaseName
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mintFromYearUnkId As Integer = 0
    Private mintToYearUnkId As Integer = 0
    'Sohail (21 Aug 2015) -- End

    Private mintReasonId As Integer = 0
    Private mstrReason As String = ""


    Private marrDatabaseName As New ArrayList



#End Region

#Region " Properties "

    Public WriteOnly Property _Emp_Name() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _Employeeunkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodUnkID() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodUnkID() As Integer
        Set(ByVal value As Integer)
            mintToPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtFromPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtFromPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtToPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtToPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _Current_ScaleFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecCurrentScaleFrom = value
        End Set
    End Property

    Public WriteOnly Property _Current_ScaleTo() As Decimal
        Set(ByVal value As Decimal)
            mdecCurrentScaleTo = value
        End Set
    End Property

    Public WriteOnly Property _Increment_From() As Decimal
        Set(ByVal value As Decimal)
            mdecIncrementFrom = value
        End Set
    End Property

    Public WriteOnly Property _Increment_To() As Decimal
        Set(ByVal value As Decimal)
            mdecIncrementTo = value
        End Set
    End Property

    Public WriteOnly Property _NewScale_From() As Decimal
        Set(ByVal value As Decimal)
            mdecNewScaleFrom = value
        End Set
    End Property

    Public WriteOnly Property _NewScale_To() As Decimal
        Set(ByVal value As Decimal)
            mdecNewScaleTo = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IncrementDateFrom() As Date
        Set(ByVal value As Date)
            mdtIncrementDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _IncrementDateTo() As Date
        Set(ByVal value As Date)
            mdtIncrementDateTo = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property


    Public WriteOnly Property _ReasonId() As Integer
        Set(ByVal value As Integer)
            mintReasonId = value
        End Set
    End Property

    Public WriteOnly Property _Reason() As String
        Set(ByVal value As String)
            mstrReason = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _FromDatabaseName() As String
        Set(ByVal value As String)
            mstrFromDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ToDatabaseName() As String
        Set(ByVal value As String)
            mstrToDatabaseName = value
        End Set
    End Property

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private WriteOnly Property _FromYearUnkId() As Integer
        Set(ByVal value As Integer)
            mintFromYearUnkId = 0
        End Set
    End Property

    Private WriteOnly Property _ToYearUnkId() As Integer
        Set(ByVal value As Integer)
            mintToYearUnkId = 0
        End Set
    End Property
    'Sohail (21 Aug 2015) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmpName = String.Empty
            mintEmployeeunkid = -1
            mdecCurrentScaleFrom = 0
            mdecCurrentScaleTo = 0
            mdecIncrementFrom = 0
            mdecIncrementTo = 0
            mdecNewScaleFrom = 0
            mdecNewScaleTo = 0
            mblnIncludeInactiveEmp = True
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mdtIncrementDateFrom = Nothing
            mdtIncrementDateTo = Nothing

            mstrAdvance_Filter = ""

            mintReasonId = 0
            mstrReason = ""

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mintFromYearUnkId = 0
            mintToYearUnkId = 0
            'Sohail (21 Aug 2015) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@fromperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromPeriodUnkId)
            objDataOperation.AddParameter("@toperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToPeriodUnkId)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Period From :") & " " & mstrFromPeriodName & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "To :") & " " & mstrToPeriodName & " "

            objDataOperation.AddParameter("@fromperiodstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodStartDate).ToString)
            objDataOperation.AddParameter("@fromperiodenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodEndDate).ToString)
            objDataOperation.AddParameter("@toperiodstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodStartDate).ToString)
            objDataOperation.AddParameter("@toperiodenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodEndDate).ToString)
            objDataOperation.AddParameter("@fromperiodenddatenextdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodEndDate.AddDays(1)).ToString)
            objDataOperation.AddParameter("@toperiodenddatenextdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodEndDate.AddDays(1)).ToString) 'Sohail (30 Dec 2016)

            objDataOperation.AddParameter("@Appointed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Appointed"))
            objDataOperation.AddParameter("@TransferIn", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Transfer In"))
            objDataOperation.AddParameter("@TransferOut", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Transfer Out"))
            objDataOperation.AddParameter("@Reinstated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Reinstated"))
            objDataOperation.AddParameter("@Exit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Exit"))


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If mintEmployeeunkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 88, "Employee : ") & " " & mstrEmpName & " "
            End If
            'Sohail (21 Aug 2015) -- End

            If mintReasonId > 0 Then
                objDataOperation.AddParameter("@reasonid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Reason : ") & " " & mstrReason & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Order By :") & " " & Me.OrderByDisplay
                Me._FilterQuery &= " ORDER BY GName, " & Me.OrderByQuery
            Else
                Me._FilterQuery &= " ORDER BY GName "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt


        '    If Not IsNothing(objRpt) Then


        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDic As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 10, "Grand Total :")
        '        Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total :")
        '        'Dim row As WorksheetRow
        '        'Dim wcell As WorksheetCell

        '        If mdtTableExcel IsNot Nothing Then


        '            '    If mintViewIndex > 0 Then
        '            '        Dim strGrpCols As String() = {"column10"}
        '            '        strarrGroupColumns = strGrpCols
        '            '    End If


        '            '    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            '    For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '            '        intArrayColumnWidth(i) = 125
        '            '    Next


        '            '    '*******   REPORT FOOTER   ******
        '            '    row = New WorksheetRow()
        '            '    wcell = New WorksheetCell("", "s8w")
        '            '    row.Cells.Add(wcell)
        '            '    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            '    rowsArrayFooter.Add(row)
        '            '    '----------------------


        '            '    If ConfigParameter._Object._IsShowCheckedBy = True Then
        '            '        row = New WorksheetRow()
        '            '        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
        '            '        row.Cells.Add(wcell)

        '            '        wcell = New WorksheetCell("", "s8w")
        '            '        row.Cells.Add(wcell)
        '            '        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '            '        rowsArrayFooter.Add(row)

        '            '        row = New WorksheetRow()
        '            '        wcell = New WorksheetCell("", "s8w")
        '            '        row.Cells.Add(wcell)
        '            '        rowsArrayFooter.Add(row)

        '            '    End If

        '            '    If ConfigParameter._Object._IsShowApprovedBy = True Then

        '            '        row = New WorksheetRow()
        '            '        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By :"), "s8bw")
        '            '        row.Cells.Add(wcell)

        '            '        wcell = New WorksheetCell("", "s8w")
        '            '        row.Cells.Add(wcell)
        '            '        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '            '        rowsArrayFooter.Add(row)

        '            '        row = New WorksheetRow()
        '            '        wcell = New WorksheetCell("", "s8w")
        '            '        row.Cells.Add(wcell)
        '            '        rowsArrayFooter.Add(row)

        '            '    End If

        '            '    If ConfigParameter._Object._IsShowReceivedBy = True Then
        '            '        row = New WorksheetRow()
        '            '        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
        '            '        row.Cells.Add(wcell)

        '            '        wcell = New WorksheetCell("", "s8w")
        '            '        row.Cells.Add(wcell)
        '            '        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '            '        rowsArrayFooter.Add(row)

        '            '        row = New WorksheetRow()
        '            '        wcell = New WorksheetCell("", "s8w")
        '            '        row.Cells.Add(wcell)
        '            '        rowsArrayFooter.Add(row)
        '            '    End If

        '        End If

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions

        Try

            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objConfig._Companyunkid = xCompanyUnkid

            objRpt = Generate_DetailReport(xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIncludeInactiveEmp, True, objConfig._CurrencyFormat)
            Rpt = objRpt


            If Not IsNothing(objRpt) Then


                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDic As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strarrGroupColumnsTableB As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 10, "Grand Total :")
                Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total :")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                If mdtTableExcel IsNot Nothing Then


                    If mintViewIndex > 0 Then
                        Dim strGrpCols As String() = {"column10"}
                        strarrGroupColumns = strGrpCols

                        Dim strGrpColsTableB As String() = {"column10, Column5"}
                        strarrGroupColumnsTableB = strGrpColsTableB
                    End If


                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count + mdtTableDetail.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next


                    '    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Payroll Local Staff for") & " " & mstrFromPeriodName & " " & Language.getMessage(mstrModuleName, 20, "to") & " " & mstrToPeriodName & " " & Language.getMessage(mstrModuleName, 24, "period."), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count + mdtTableDetail.Columns.Count - 2
                    rowsArrayFooter.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8b")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Month"), "s8bc")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Gross Salary"), "s8bc")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Net Salary"), "s8bc")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Head Count"), "s8bc")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(mintFirstPeriodHeadCount.ToString, "count8b")
                    row.Cells.Add(wcell)

                    rowsArrayFooter.Add(row)
                    '----------------------

                    Dim j As Integer = -1
                    Dim decTotGrossSalary As Decimal = 0
                    Dim decTotNetSalary As Decimal = 0
                    Dim intTotHeadCount As Integer = 0
                    For Each dsRow As DataRow In mdtTableFooter.Rows
                        j += 1

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dsRow.Item("Column2").ToString, "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dsRow.Item("Column3").ToString, "n8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dsRow.Item("Column4").ToString, "n8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(dsRow.Item("Column6").ToString, "n8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)

                        If j = 0 Then

                            wcell = New WorksheetCell((mintExitTotal * -1).ToString, "count8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Exit"), "s8bw")
                            row.Cells.Add(wcell)

                            decTotGrossSalary = CDec(dsRow.Item("Column3").ToString) * -1
                            decTotNetSalary = CDec(dsRow.Item("Column4").ToString) * -1
                            intTotHeadCount = CInt(dsRow.Item("Column6").ToString) * -1
                        ElseIf j = 1 Then

                            wcell = New WorksheetCell(mintAppointedTotal.ToString, "count8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "Appointed"), "s8bw")
                            row.Cells.Add(wcell)

                            decTotGrossSalary += CDec(dsRow.Item("Column3").ToString)
                            decTotNetSalary += CDec(dsRow.Item("Column4").ToString)
                            intTotHeadCount += CInt(dsRow.Item("Column6").ToString)
                        End If


                        rowsArrayFooter.Add(row)
                        '----------------------

                    Next

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8b")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Variance"), "s8b")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(decTotGrossSalary.ToString, "n8b")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(decTotNetSalary.ToString, "n8b")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(intTotHeadCount.ToString, "n8b")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell((mintFirstPeriodHeadCount - mintExitTotal + mintAppointedTotal).ToString, "count8b")
                    row.Cells.Add(wcell)

                    rowsArrayFooter.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    rowsArrayFooter.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Approved by :  Chief Accountant"), "s8bc")
                    wcell.MergeAcross = 1
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "Approved by : HR Manager"), "s8bc")
                    wcell.MergeAcross = 1
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Approved by : Chief Finance Officer"), "s8bc")
                    wcell.MergeAcross = 2
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Approved By : Chief Executive Officer"), "s8bc")
                    wcell.MergeAcross = 1
                    row.Cells.Add(wcell)

                    rowsArrayFooter.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8")
                    wcell.MergeAcross = 1
                    wcell.MergeDown = 1
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8")
                    wcell.MergeAcross = 1
                    wcell.MergeDown = 1
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8")
                    wcell.MergeAcross = 2
                    wcell.MergeDown = 1
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8")
                    wcell.MergeAcross = 1
                    wcell.MergeDown = 1
                    row.Cells.Add(wcell)

                    rowsArrayFooter.Add(row)
                    '----------------------


                End If

                'Sohail (10 May 2015) -- Start
                'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
                'Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)
                'Hemant (14 Mar 2019) -- Start
                'Support Issue Id # 3576 : Excel advance format for Salary Reconciliation report isn’t showing the reconciliation summary.
                'Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, mdtTableDetail, intArrayColumnWidth, True, True, False, strarrGroupColumns, strarrGroupColumnsTableB, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic, , , True)
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, mdtTableDetail, intArrayColumnWidth, True, True, True, strarrGroupColumns, strarrGroupColumnsTableB, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic, , , True)
                'Hemant (14 Mar 2019) -- End
                'Sohail (10 May 2015) -- End

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_Detail.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_Detail.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

        Try
            Call OrderByExecute(iColumn_Detail)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_Detail As New IColumnCollection
    Public Property Field_DetailReport() As IColumnCollection
        Get
            Return iColumn_Detail
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_Detail = value
        End Set
    End Property

    Private Sub Create_DetailReport()
        Try
            iColumn_Detail.Clear()
            'iColumn_Detail.Add(New IColumn("incrementdate", Language.getMessage(mstrModuleName, 9, "Increment Date")))
            iColumn_Detail.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 12, "Employee Code")))
            iColumn_Detail.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 13, "Employee Name")))
            'iColumn_Detail.Add(New IColumn("Current_Scale", Language.getMessage(mstrModuleName, 12, "Current Scale")))
            'iColumn_Detail.Add(New IColumn("Increment", Language.getMessage(mstrModuleName, 13, "Increment Amount")))
            'iColumn_Detail.Add(New IColumn("New_Scale", Language.getMessage(mstrModuleName, 14, "New Scale")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xUserUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strfmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim dsSummary As DataSet
        Dim dsFooter As DataSet
        Dim exForce As Exception
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Dim strAllocationJoinFromPeriod As String = ""
        Dim strAllocationJoinToPeriod As String = ""
        'Sohail (30 Dec 2015) -- Start
        'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
        'Dim mintFirstPeriodHeadCount As Integer = 0
        mintFirstPeriodHeadCount = 0
        Dim strActiveEmpFromPeriodFilter As String = ""
        Dim strActiveEmpToPeriodFilter As String = ""
        'Sohail (30 Dec 2015) -- End
        'Sohail (10 May 2015) -- Start
        'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
        Dim strAllocationIdField As String = "stationunkid"
        Dim strInnerQ As String = ""
        'Sohail (10 May 2015) -- End
        objDataOperation = New clsDataOperation
        Try

            '*******************************************************************************************************************
            '       TO INCLUDE REINSTATED EMPLOYEES PLEASE REMOVE '/*' AND '*/' CHARACTERS FROM QUERY
            '*******************************************************************************************************************

            If mintViewIndex > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strAllocationJoin = mstrAnalysis_Join.Replace(Split(mstrAnalysis_Join, "=")(1).Split("AND ")(0), " hrallocation_tracking.allocationunkid ")
                'Sohail (10 May 2015) -- Start
                'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
                'strAllocationJoin = mstrAnalysis_Join.Replace(Split(mstrAnalysis_Join, "AS VB_TRF")(1).Split("=")(2).Split("AND ")(0), " hrallocation_tracking.allocationunkid ") 
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
                'strAllocationJoinFromPeriod = mstrAnalysis_Join.Replace("@toperiodenddate", "@fromperiodenddate")
                mstrAnalysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtToPeriodEndDate))
                strAllocationJoinFromPeriod = mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtToPeriodEndDate), eZeeDate.convertDate(mdtFromPeriodEndDate))
                'Sohail (14 Nov 2017) -- End
                strAllocationJoinToPeriod = mstrAnalysis_Join
                'Sohail (10 May 2015) -- End
                'Sohail (21 Aug 2015) -- End
            End If

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            Dim strTransferTable As String = "hremployee_transfer_tran"
            Dim strTransferUnkIdFld As String = "transferunkid"
            Dim strEffDateFld As String = "effectivedate"
            'Sohail (14 Nov 2017) -- End

            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            Select Case mintViewIndex
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
                'Case enAllocation.BRANCH
                Case enAnalysisReport.Branch
                    'Sohail (14 Nov 2017) -- End
                    strAllocationIdField = "stationunkid"

                    'Sohail (14 Nov 2017) -- Start
                    'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
                Case enAnalysisReport.Department
                    strAllocationIdField = "departmentunkid"

                Case enAnalysisReport.Section
                    strAllocationIdField = "sectionunkid"

                Case enAnalysisReport.Unit
                    strAllocationIdField = "unitunkid"

                Case enAnalysisReport.Job
                    strAllocationIdField = "jobunkid"
                    strTransferTable = "hremployee_categorization_tran"
                    strTransferUnkIdFld = "categorizationtranunkid"

                Case enAnalysisReport.CostCenter
                    strAllocationIdField = "cctranheadvalueid"
                    strTransferTable = "hremployee_cctranhead_tran"
                    strTransferUnkIdFld = "cctranheadunkid"

                Case enAnalysisReport.UnitGroup
                    strAllocationIdField = "unitgroupunkid"

                Case enAnalysisReport.Team
                    strAllocationIdField = "teamunkid"

                Case enAnalysisReport.DepartmentGroup
                    strAllocationIdField = "deptgroupunkid"

                Case enAnalysisReport.JobGroup
                    strAllocationIdField = "jobgroupunkid"
                    strTransferTable = "hremployee_categorization_tran"
                    strTransferUnkIdFld = "categorizationtranunkid"

                Case enAnalysisReport.ClassGroup
                    strAllocationIdField = "classgroupunkid"

                Case enAnalysisReport.Classs
                    strAllocationIdField = "classunkid"

                Case enAnalysisReport.GradeGroup
                    strAllocationIdField = "gradegroupunkid"
                    strTransferTable = "prsalaryincrement_tran"
                    strTransferUnkIdFld = "salaryincrementtranunkid"
                    strEffDateFld = "incrementdate"

                Case enAnalysisReport.Grade
                    strAllocationIdField = "gradeunkid"
                    strTransferTable = "prsalaryincrement_tran"
                    strTransferUnkIdFld = "salaryincrementtranunkid"
                    strEffDateFld = "incrementdate"

                Case enAnalysisReport.GradeLevel
                    strAllocationIdField = "gradelevelunkid"
                    strTransferTable = "prsalaryincrement_tran"
                    strTransferUnkIdFld = "salaryincrementtranunkid"
                    strEffDateFld = "incrementdate"
                    'Sohail (14 Nov 2017) -- End

            End Select
            'Sohail (10 May 2015) -- End

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            If strAllocationIdField = "" Then
                strAllocationIdField = "stationunkid"
                mintViewIndex = 1
            End If
            'Sohail (14 Nov 2017) -- End

            'Sohail (30 Dec 2015) -- Start
            'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
            If mblnIncludeInactiveEmp = False Then
                strActiveEmpFromPeriodFilter &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @fromperiodenddate " & _
                                                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112), @fromperiodstartdate) >= @fromperiodstartdate " & _
                                                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112), @fromperiodstartdate) >= @fromperiodstartdate " & _
                                                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @fromperiodstartdate) >= @fromperiodstartdate "

                strActiveEmpFromPeriodFilter &= " AND ( ( ISNULL(isexclude_payroll, 0) = 0 ) OR ( ISNULL(CONVERT(CHAR(8),termination_from_date,112), @fromperiodenddatenextdate) > @fromperiodenddate " & _
                                                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112), @fromperiodenddatenextdate) > @fromperiodenddate " & _
                                                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @fromperiodenddatenextdate) > @fromperiodenddate AND isexclude_payroll = 1 ) ) "

                strActiveEmpToPeriodFilter &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @toperiodenddate " & _
                                              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112), @toperiodstartdate) >= @toperiodstartdate " & _
                                              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112), @toperiodstartdate) >= @toperiodstartdate " & _
                                              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @toperiodstartdate) >= @toperiodstartdate "

                strActiveEmpToPeriodFilter &= " AND ( ( ISNULL(isexclude_payroll, 0) = 0 ) OR ( ISNULL(CONVERT(CHAR(8),termination_from_date,112), @toperiodenddatenextdate) > @toperiodenddate " & _
                                                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112), @toperiodenddatenextdate) > @toperiodenddate " & _
                                                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @toperiodenddatenextdate) > @toperiodenddate AND isexclude_payroll = 1 ) ) "
            End If
            'Sohail (30 Dec 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQryFromPeriod, xDateFilterQryFromPeriod, xUACQryFromPeriod, xUACFiltrQryFromPeriod, xAdvanceJoinQryFromPeriod As String
            xDateJoinQryFromPeriod = "" : xDateFilterQryFromPeriod = "" : xUACQryFromPeriod = "" : xUACFiltrQryFromPeriod = "" : xAdvanceJoinQryFromPeriod = ""
            Call GetDatesFilterString(xDateJoinQryFromPeriod, xDateFilterQryFromPeriod, mdtFromPeriodStartDate, mdtFromPeriodEndDate, , True, mstrFromDatabaseName)
            Call NewAccessLevelFilterString(xUACQryFromPeriod, xUACFiltrQryFromPeriod, mdtFromPeriodEndDate, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, mintFromYearUnkId, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQryFromPeriod, mdtFromPeriodEndDate, mstrFromDatabaseName)

            Dim xDateJoinQryToPeriod, xDateFilterQryToPeriod, xUACQryToPeriod, xUACFiltrQryToPeriod, xAdvanceJoinQryToPeriod As String
            xDateJoinQryToPeriod = "" : xDateFilterQryToPeriod = "" : xUACQryToPeriod = "" : xUACFiltrQryToPeriod = "" : xAdvanceJoinQryToPeriod = ""
            Call GetDatesFilterString(xDateJoinQryToPeriod, xDateFilterQryToPeriod, mdtToPeriodStartDate, mdtToPeriodEndDate, , True, mstrToDatabaseName)
            Call NewAccessLevelFilterString(xUACQryToPeriod, xUACFiltrQryToPeriod, mdtToPeriodEndDate, xOnlyApproved, mstrToDatabaseName, xUserUnkid, xCompanyUnkid, mintToYearUnkId, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQryToPeriod, mdtToPeriodEndDate, mstrToDatabaseName)

            Dim xDateJoinQryFromPeriodWExPayroll, xDateFilterQryFromPeriodWExPayroll As String
            xDateJoinQryFromPeriodWExPayroll = "" : xDateFilterQryFromPeriodWExPayroll = ""
            Call GetDatesFilterString(xDateJoinQryFromPeriodWExPayroll, xDateFilterQryFromPeriodWExPayroll, mdtToPeriodStartDate, mdtToPeriodEndDate, , False, mstrToDatabaseName)

            Dim xDateJoinQryToPeriodWExPayroll, xDateFilterQryToPeriodWExPayroll As String
            xDateJoinQryToPeriodWExPayroll = "" : xDateFilterQryToPeriodWExPayroll = ""
            Call GetDatesFilterString(xDateJoinQryToPeriodWExPayroll, xDateFilterQryToPeriodWExPayroll, mdtToPeriodStartDate, mdtToPeriodEndDate, , False, mstrToDatabaseName)
            'Sohail (21 Aug 2015) -- End

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            'strQ = "CREATE TABLE #emptransfer " & _
            '            "( " & _
            '              "transferunkid INT  " & _
            '            ", employeeunkid INT " & _
            '            ", effectivedate DATETIME " & _
            '            ", rehiretranunkid INT " & _
            '            ", stationunkid INT " & _
            '            ", deptgroupunkid INT " & _
            '            ", departmentunkid INT " & _
            '            ", sectiongroupunkid INT " & _
            '            ", sectionunkid INT " & _
            '            ", unitgroupunkid INT " & _
            '            ", unitunkid INT " & _
            '            ", teamunkid INT " & _
            '            ", classgroupunkid INT " & _
            '            ", classunkid INT " & _
            '            ", changereasonunkid INT " & _
            '            ", isfromemployee BIT " & _
            '            ", userunkid INT " & _
            '            ", statusunkid INT " & _
            '            ", isvoid BIT " & _
            '            ", voiduserunkid INT " & _
            '            ", voiddatetime DATETIME " & _
            '            ", voidreason VARCHAR(MAX) " & _
            '            ") " & _
            '        "DECLARE @transferunkid INT  " & _
            '          ", @empunkid INT " & _
            '          ", @effectivedate DATETIME " & _
            '          ", @rehiretranunkid INT " & _
            '          ", @stationunkid INT " & _
            '          ", @deptgroupunkid INT " & _
            '          ", @departmentunkid INT " & _
            '          ", @sectiongroupunkid INT " & _
            '          ", @sectionunkid INT " & _
            '          ", @unitgroupunkid INT " & _
            '          ", @unitunkid INT " & _
            '          ", @teamunkid INT " & _
            '          ", @classgroupunkid INT " & _
            '          ", @classunkid INT " & _
            '          ", @changereasonunkid INT " & _
            '          ", @isfromemployee BIT " & _
            '          ", @userunkid INT " & _
            '          ", @statusunkid INT " & _
            '          ", @isvoid BIT " & _
            '          ", @voiduserunkid INT " & _
            '          ", @voiddatetime DATETIME " & _
            '          ", @voidreason cDescription " & _
            '        " " & _
            '        "DECLARE @prevempunkid INT " & _
            '        "DECLARE @prevstationunkid INT " & _
            '        "SET @prevempunkid = 0 " & _
            '        "SET @prevstationunkid = 0 " & _
            '        " " & _
            '        "DECLARE c CURSOR " & _
            '        "FOR "

            'strInnerQ = " SELECT  hremployee_transfer_tran.transferunkid  " & _
            '                  ", hremployee_transfer_tran.employeeunkid " & _
            '                  ", hremployee_transfer_tran.effectivedate " & _
            '                  ", hremployee_transfer_tran.rehiretranunkid " & _
            '                  ", hremployee_transfer_tran.stationunkid " & _
            '                  ", hremployee_transfer_tran.deptgroupunkid " & _
            '                  ", hremployee_transfer_tran.departmentunkid " & _
            '                  ", hremployee_transfer_tran.sectiongroupunkid " & _
            '                  ", hremployee_transfer_tran.sectionunkid " & _
            '                  ", hremployee_transfer_tran.unitgroupunkid " & _
            '                  ", hremployee_transfer_tran.unitunkid " & _
            '                  ", hremployee_transfer_tran.teamunkid " & _
            '                  ", hremployee_transfer_tran.classgroupunkid " & _
            '                  ", hremployee_transfer_tran.classunkid " & _
            '                  ", hremployee_transfer_tran.changereasonunkid " & _
            '                  ", hremployee_transfer_tran.isfromemployee " & _
            '                  ", hremployee_transfer_tran.userunkid " & _
            '                  ", hremployee_transfer_tran.statusunkid " & _
            '                  ", hremployee_transfer_tran.isvoid " & _
            '                  ", hremployee_transfer_tran.voiduserunkid " & _
            '                  ", hremployee_transfer_tran.voiddatetime " & _
            '                  ", hremployee_transfer_tran.voidreason " & _
            '            "FROM    " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
            '            "WHERE   hremployee_transfer_tran.isvoid = 0 "
            strQ = "CREATE TABLE #emptransfer " & _
                        "( " & _
                          "" & strTransferUnkIdFld & " INT  " & _
                        ", employeeunkid INT " & _
                        ", " & strEffDateFld & " DATETIME " & _
                        ", " & strAllocationIdField & " INT " & _
                        ") " & _
                    "DECLARE @" & strTransferUnkIdFld & " INT  " & _
                      ", @empunkid INT " & _
                      ", @" & strEffDateFld & " DATETIME " & _
                      ", @" & strAllocationIdField & " INT " & _
                    " " & _
                    "DECLARE @prevempunkid INT " & _
                    "DECLARE @prev" & strAllocationIdField & " INT " & _
                    "SET @prevempunkid = 0 " & _
                    "SET @prev" & strAllocationIdField & " = 0 " & _
                    " " & _
                    "DECLARE c CURSOR " & _
                    "FOR "

            strInnerQ = " SELECT  " & strTransferTable & "." & strTransferUnkIdFld & "  " & _
                              ", " & strTransferTable & ".employeeunkid " & _
                              ", " & strTransferTable & "." & strEffDateFld & " " & _
                              ", " & strTransferTable & "." & strAllocationIdField & " " & _
                        "FROM    " & mstrFromDatabaseName & ".." & strTransferTable & " " & _
                        "WHERE   " & strTransferTable & ".isvoid = 0 "
            'Sohail (14 Nov 2017) -- End

            If mintEmployeeunkid > 0 Then
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
                'strInnerQ &= " AND hremployee_transfer_tran.employeeunkid = " & mintEmployeeunkid & " "
                strInnerQ &= " AND " & strTransferTable & ".employeeunkid = " & mintEmployeeunkid & " "
                'Sohail (14 Nov 2017) -- End
            End If

            If mstrFromDatabaseName <> mstrToDatabaseName Then
                strInnerQ &= " UNION ALL " & _
                               strInnerQ.Replace(mstrFromDatabaseName, mstrToDatabaseName)
            End If

            strQ &= strInnerQ

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            'strQ &= "    ORDER BY hremployee_transfer_tran.employeeunkid  " & _
            '            ", hremployee_transfer_tran.effectivedate " & _
            '        " " & _
            '        "OPEN c " & _
            '        " " & _
            '        "FETCH NEXT FROM c INTO @transferunkid, @empunkid, @effectivedate " & _
            '          ", @rehiretranunkid, @stationunkid, @deptgroupunkid, @departmentunkid " & _
            '          ", @sectiongroupunkid, @sectionunkid, @unitgroupunkid, @unitunkid, @teamunkid " & _
            '          ", @classgroupunkid, @classunkid, @changereasonunkid, @isfromemployee " & _
            '          ", @userunkid, @statusunkid, @isvoid, @voiduserunkid, @voiddatetime " & _
            '          ", @voidreason " & _
            '        " " & _
            '        "WHILE @@FETCH_STATUS = 0 " & _
            '            "BEGIN " & _
            '                "IF @prevempunkid <> @empunkid OR @prev" & strAllocationIdField & " <> @" & strAllocationIdField & " " & _
            '                    "BEGIN " & _
            '                        "INSERT  INTO #emptransfer " & _
            '                        "VALUES  ( @transferunkid, @empunkid, @effectivedate " & _
            '                                ", @rehiretranunkid, @stationunkid, @deptgroupunkid " & _
            '                                ", @departmentunkid, @sectiongroupunkid, @sectionunkid " & _
            '                                ", @unitgroupunkid, @unitunkid, @teamunkid " & _
            '                                ", @classgroupunkid, @classunkid, @changereasonunkid " & _
            '                                ", @isfromemployee, @userunkid, @statusunkid, @isvoid " & _
            '                                ", @voiduserunkid, @voiddatetime, @voidreason ) " & _
            '                    "END " & _
            '                "SET @prevempunkid = @empunkid " & _
            '                "SET @prev" & strAllocationIdField & " = @" & strAllocationIdField & " " & _
            '                "FETCH NEXT FROM c INTO @transferunkid, @empunkid, @effectivedate " & _
            '                  ", @rehiretranunkid, @stationunkid, @deptgroupunkid, @departmentunkid " & _
            '                  ", @sectiongroupunkid, @sectionunkid, @unitgroupunkid, @unitunkid " & _
            '                  ", @teamunkid, @classgroupunkid, @classunkid, @changereasonunkid " & _
            '                  ", @isfromemployee, @userunkid, @statusunkid, @isvoid, @voiduserunkid " & _
            '                  ", @voiddatetime, @voidreason " & _
            '            "END " & _
            '        "CLOSE c " & _
            '        "DEALLOCATE c; "
            strQ &= "    ORDER BY " & strTransferTable & ".employeeunkid  " & _
                        ", " & strTransferTable & "." & strEffDateFld & " " & _
                    " " & _
                    "OPEN c " & _
                    " " & _
                    "FETCH NEXT FROM c INTO @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                    " " & _
                    "WHILE @@FETCH_STATUS = 0 " & _
                        "BEGIN " & _
                            "IF @prevempunkid <> @empunkid OR @prev" & strAllocationIdField & " <> @" & strAllocationIdField & " " & _
                                "BEGIN " & _
                                    "INSERT  INTO #emptransfer " & _
                                    "VALUES  ( @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                                            ") " & _
                                "END " & _
                            "SET @prevempunkid = @empunkid " & _
                            "SET @prev" & strAllocationIdField & " = @" & strAllocationIdField & " " & _
                            "FETCH NEXT FROM c INTO @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                        "END " & _
                    "CLOSE c " & _
                    "DEALLOCATE c; "
            'Sohail (14 Nov 2017) -- End            

            strQ &= "WITH    CTE " & _
                              "AS ( SELECT   B." & strTransferUnkIdFld & "  " & _
                                          ", B.employeeunkid " & _
                                          ", B.employeecode " & _
                                          ", B.employeename " & _
                                          ", B.allocationunkid " & _
                                          ", B.start_date " & _
                                          ", B.reason_id " & _
                                          ", B.reason " & _
                                          ", B.Id " & _
                                          ", B.GName " & _
                                   "FROM     ( SELECT    A." & strTransferUnkIdFld & "  " & _
                                                      ", A.employeeunkid " & _
                                                      ", A.employeecode " & _
                                                      ", A.employeename " & _
                                                      ", A.allocationunkid " & _
                                                      ", A.start_date " & _
                                                      ", A.reason_id " & _
                                                      ", A.reason " & _
                                                      ", allocationunkid AS Id " & _
                                                      ", A.GName " & _
                                                      ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A.start_date DESC ) AS ROW_NO " & _
                                              "FROM      ( SELECT    #emptransfer." & strTransferUnkIdFld & "  " & _
                                                                  ", #emptransfer.employeeunkid " & _
                                                                  ", hremployee_master.employeecode " & _
                                                                  ", #emptransfer." & strAllocationIdField & " AS allocationunkid " & _
                                                                  ", CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) AS start_date " & _
                                                                  ", CASE WHEN CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) = CONVERT(CHAR(8), hremployee_master.appointeddate, 112) " & _
                                                                         "THEN -3 " & _
                                                                         "ELSE -1 " & _
                                                                    "END AS reason_id  " & _
                                                                  ", CASE WHEN CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) = CONVERT(CHAR(8), hremployee_master.appointeddate, 112) " & _
                                                                         "THEN @Appointed " & _
                                                                         "ELSE @TransferIn " & _
                                                                    "END AS reason  " & _
                                                                  ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO "
            'Sohail (14 Nov 2017) - [transferunkid = " & strTransferUnkIdFld & "]

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                      FROM      #emptransfer " & _
                                                    "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinToPeriod.Replace("hremployee_master", "#emptransfer")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryToPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryToPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryToPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryToPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryToPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryToPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                      WHERE     #emptransfer." & strAllocationIdField & " > 0 " & _
                                                    "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @toperiodenddate "

            If mintEmployeeunkid > 0 Then
                strQ &= " AND #emptransfer.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryToPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryToPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryToPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryToPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    ) AS A " & _
                               "WHERE    A.ROWNO = 1 " & _
                                         "AND CONVERT(CHAR(8), A.start_date, 112) BETWEEN @fromperiodenddatenextdate AND @toperiodenddate " & _
                             ") AS B " & _
                            "LEFT JOIN ( SELECT  * " & _
                                        "FROM    ( SELECT    #emptransfer." & strTransferUnkIdFld & "  " & _
                                                          ", #emptransfer.employeeunkid " & _
                                                          ", CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) AS start_date " & _
                                                          ", #emptransfer." & strAllocationIdField & " AS allocationunkid " & _
                                                          ", CASE WHEN CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) = CONVERT(CHAR(8), hremployee_master.appointeddate, 112) " & _
                                                                 "THEN -3 " & _
                                                                 "ELSE -1 " & _
                                                            "END AS reason_id  " & _
                                                          ", CASE WHEN CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) = CONVERT(CHAR(8), hremployee_master.appointeddate, 112) " & _
                                                                 "THEN @Appointed " & _
                                                                 "ELSE @TransferIn " & _
                                                            "END AS reason  "
            'Sohail (14 Nov 2017) - [transferunkid = " & strTransferUnkIdFld & "], [Removed - hrstation_master.name AS GName]

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If
            'Sohail (14 Nov 2017) -- End

            strQ &= "                                       , DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                  "FROM      #emptransfer " & _
                                                            "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid "
            'Sohail (14 Nov 2017) - [transferunkid = " & strTransferUnkIdFld & "]

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinFromPeriod.Replace("hremployee_master", "#emptransfer")
            End If

            If xDateJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryFromPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryFromPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryFromPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryFromPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryFromPeriod
            End If

            strQ &= "                               WHERE     #emptransfer." & strAllocationIdField & " > 0 " & _
                                                            "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @fromperiodenddate "
            'Sohail (14 Nov 2017) - [effectivedate = " & strTransferUnkIdFld & "]

            If mintEmployeeunkid > 0 Then
                strQ &= " AND #emptransfer.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryFromPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryFromPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryFromPeriod
                End If
            End If

            strQ &= "                           ) AS D " & _
                                        "WHERE   D.ROWNO = 1 " & _
                                      ") AS Prev ON B.employeeunkid = Prev.employeeunkid " & _
                                                   "AND B.allocationunkid = Prev.allocationunkid " & _
                   "WHERE    B.ROW_NO = 1 " & _
                            "AND Prev.employeeunkid IS NULL " & _
                 ") " & _
                      "/*, TBL " & _
                          "AS ( SELECT   employeeunkid  " & _
                                      ", employeecode " & _
                                      ", employeename " & _
                                      ", reinstatement_date " & _
                                      ", MIN(mindate) AS terminatedate " & _
                               "FROM     ( SELECT    athremployee_master.employeeunkid  " & _
                                                  ", employeecode " & _
                                                  ", appointeddate " & _
                                                  ", termination_from_date " & _
                                                  ", empl_enddate " & _
                                                  ", termination_to_date " & _
                                                  ", reinstatement_date " & _
                                                  ", auditdatetime " & _
                                                  ", DENSE_RANK() OVER ( PARTITION BY athremployee_master.employeeunkid ORDER BY auditdatetime DESC ) AS ROWNO "

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                      FROM      " & mstrToDatabaseName & "..athremployee_master "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join.Replace("hremployee_master", "athremployee_master")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryToPeriod.Replace("hremployee_master", "athremployee_master")
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryToPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryToPeriod.Replace("hremployee_master", "athremployee_master")
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryToPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryToPeriod.Replace("hremployee_master", "athremployee_master")
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END


            If xAdvanceJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryToPeriod.Replace("hremployee_master", "athremployee_master")
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                      WHERE     CONVERT(CHAR(8), reinstatement_date, 112) BETWEEN @fromperiodenddatenextdate AND @toperiodenddate "

            If mintEmployeeunkid > 0 Then
                strQ &= " AND athremployee_master.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter.Replace("hremployee_master", "athremployee_master")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter.Replace("hremployee_master", "athremployee_master")
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryToPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryToPeriod.Replace("hremployee_master", "athremployee_master")
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryToPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryToPeriod.Replace("hremployee_master", "athremployee_master")
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    ) AS A UNPIVOT ( mindate FOR d IN ( termination_from_date " & _
                                                                          ", termination_to_date " & _
                                                                          ", empl_enddate ) ) unpvt " & _
                               "WHERE    ROWNO = 1 " & _
                               "GROUP BY employeeunkid  " & _
                                      ", employeecode " & _
                                      ", employeename " & _
                                      ", reinstatement_date " & _
                               "HAVING   reinstatement_date > MIN(mindate) " & _
                                        "OR CONVERT(CHAR(8), MIN(mindate), 112) > @toperiodenddate " & _
                             ")*/ " & _
                    "SELECT  ISNULL(A.employeeunkid, 0) AS employeeunkid  " & _
                          ", ISNULL(A.employeecode, '') AS employeecode " & _
                          ", ISNULL(A.employeename, '') AS employeename " & _
                          ", ISNULL(A.reason_id, '') AS reason_id " & _
                          ", ISNULL(A.reason, '') AS reason " & _
                          ", CASE A.ROW_NO " & _
                              "WHEN 1 THEN ISNULL(A.newscale, 0) - ISNULL(B.newscale, 0) " & _
                              "ELSE ISNULL(A.newscale, 0) - ( A.lastscale ) " & _
                            "END AS incrementamount  " & _
                          ", ISNULL(A.GName, '') AS GName " & _
                    "FROM    ( SELECT    AA.periodunkid  " & _
                                      ", AA.period_name " & _
                                      ", AA.employeeunkid " & _
                                      ", AA.employeecode " & _
                                      ", AA.employeename " & _
                                      ", AA.reason_id " & _
                                      ", AA.reason " & _
                                      ", AA.incrementdate " & _
                                      ", AA.newscale " & _
                                      ", AA.mode " & _
                                      ", AA.GName " & _
                                      ", ISNULL(BB.newscale, 0) AS lastscale " & _
                                      ", AA.ROW_NO " & _
                              "FROM      ( SELECT    c.periodunkid  " & _
                                                  ", c.period_name " & _
                                                  ", c.employeeunkid " & _
                                                  ", c.employeecode " & _
                                                  ", c.employeename " & _
                                                  ", c.reason_id " & _
                                                  ", c.reason " & _
                                                  ", c.incrementdate " & _
                                                  ", c.newscale " & _
                                                  ", c.mode " & _
                                                  ", c.GName " & _
                                                  ", ROW_NUMBER() OVER ( PARTITION BY C.employeeunkid ORDER BY C.incrementdate ASC ) AS ROW_NO " & _
                                          "FROM      ( SELECT    prsalaryincrement_tran.periodunkid  " & _
                                                              ", period_name " & _
                                                              ", prsalaryincrement_tran.employeeunkid " & _
                                                              ", employeecode " & _
                                                              ", reason_id " & _
                                                              ", cfcommon_master.name AS reason " & _
                                                              ", CONVERT(CHAR(8), incrementdate, 112) AS incrementdate " & _
                                                              ", newscale " & _
                                                              ", CASE WHEN increment >= 0 " & _
                                                                     "THEN 'P' " & _
                                                                     "ELSE 'N' " & _
                                                                "END AS mode  " & _
                                                              ", DENSE_RANK() OVER ( PARTITION BY prsalaryincrement_tran.employeeunkid , prsalaryincrement_tran.reason_id ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC ) AS ROWNO "

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                                  FROM      " & mstrToDatabaseName & "..prsalaryincrement_tran " & _
                                                               "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                                                               "LEFT JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prsalaryincrement_tran.periodunkid " & _
                                                               "LEFT JOIN " & mstrToDatabaseName & "..cfcommon_master ON masterunkid = reason_id AND mastertype = " & clsCommon_Master.enCommonMaster.SALINC_REASON & " " & _
                                                               "LEFT JOIN ( SELECT " & _
                                                                                 "A.employeeunkid  " & _
                                                                               ", A." & strAllocationIdField & " " & _
                                                                               ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A." & strEffDateFld & " DESC ) AS ROW_NO " & _
                                                                           "FROM " & _
                                                                             "( SELECT " & _
                                                                                     "#emptransfer.employeeunkid  " & _
                                                                                   ", #emptransfer." & strEffDateFld & " " & _
                                                                                   ", #emptransfer." & strAllocationIdField & " " & _
                                                                                   ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                                             "FROM    #emptransfer " & _
                                                                                     "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid " & _
                                                                             "WHERE   #emptransfer." & strAllocationIdField & " > 0 " & _
                                                                                     "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @toperiodenddate " & _
                                                                             ") AS A " & _
                                                                           "WHERE   A.ROWNO = 1 " & _
                                                                         ") AS Ab ON prsalaryincrement_tran.employeeunkid = Ab.employeeunkid "
            'Sohail (14 Nov 2017) - [, A.stationunkid] = [, A." & strAllocationIdField & "]
            '                       [, #emptransfer.stationunkid] = [, #emptransfer." & strAllocationIdField & "]
            '                       [AND #emptransfer.stationunkid] = [AND #emptransfer." & strAllocationIdField & " > 0]

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinToPeriod.Replace("hremployee_master", "Ab")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryToPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryToPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryToPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryToPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryToPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryToPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                                  WHERE     isvoid = 0 " & _
                                                                "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                                                "AND reason_id > 0 " & _
                                                                "AND Ab.ROW_NO = 1 " & _
                                                                "AND cfcommon_period_tran.end_date BETWEEN @fromperiodenddatenextdate AND @toperiodenddate "

            If mintEmployeeunkid > 0 Then
                strQ &= " AND prsalaryincrement_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryToPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryToPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryToPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryToPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                                ) AS C " & _
                                          "WHERE     C.ROWNO = 1 " & _
                                        ") AS AA " & _
                                        "LEFT JOIN ( SELECT  c.periodunkid  " & _
                                                          ", c.period_name " & _
                                                          ", c.employeeunkid " & _
                                                          ", c.employeecode " & _
                                                          ", c.employeename " & _
                                                          ", c.reason_id " & _
                                                          ", c.reason " & _
                                                          ", c.incrementdate " & _
                                                          ", c.newscale " & _
                                                          ", c.mode " & _
                                                          ", c.GName " & _
                                                          ", ROW_NUMBER() OVER ( PARTITION BY C.employeeunkid ORDER BY C.incrementdate ASC ) AS ROW_NO " & _
                                                    "FROM    ( SELECT    prsalaryincrement_tran.periodunkid  " & _
                                                                      ", period_name " & _
                                                                      ", prsalaryincrement_tran.employeeunkid " & _
                                                                      ", employeecode " & _
                                                                      ", reason_id " & _
                                                                      ", cfcommon_master.name AS reason " & _
                                                                      ", CONVERT(CHAR(8), incrementdate, 112) AS incrementdate " & _
                                                                      ", newscale " & _
                                                                      ", CASE WHEN increment >= 0 " & _
                                                                             "THEN 'P' " & _
                                                                             "ELSE 'N' " & _
                                                                        "END AS mode  " & _
                                                                      ", DENSE_RANK() OVER ( PARTITION BY prsalaryincrement_tran.employeeunkid , prsalaryincrement_tran.reason_id ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC ) AS ROWNO "

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                                  FROM      " & mstrToDatabaseName & "..prsalaryincrement_tran " & _
                                                              "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                                                              "LEFT JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prsalaryincrement_tran.periodunkid " & _
                                                              "LEFT JOIN " & mstrToDatabaseName & "..cfcommon_master ON masterunkid = reason_id AND mastertype = " & clsCommon_Master.enCommonMaster.SALINC_REASON & " " & _
                                                              "LEFT JOIN ( SELECT " & _
                                                                                "A.employeeunkid  " & _
                                                                              ", A." & strAllocationIdField & " " & _
                                                                              ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A." & strEffDateFld & " DESC ) AS ROW_NO " & _
                                                                          "FROM " & _
                                                                            "( SELECT " & _
                                                                                    "#emptransfer.employeeunkid  " & _
                                                                                  ", #emptransfer." & strEffDateFld & " " & _
                                                                                  ", #emptransfer." & strAllocationIdField & " " & _
                                                                                  ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                                            "FROM    #emptransfer " & _
                                                                                    "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid " & _
                                                                            "WHERE   #emptransfer." & strAllocationIdField & " > 0 " & _
                                                                                    "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @toperiodenddate " & _
                                                                            ") AS A " & _
                                                                          "WHERE   A.ROWNO = 1 " & _
                                                                        ") AS Ab ON prsalaryincrement_tran.employeeunkid = Ab.employeeunkid "
            'Sohail (14 Nov 2017) - [ A.stationunkid] = [A." & strAllocationIdField & "]
            '                       [emptransfer.stationunkid] = [#emptransfer." & strAllocationIdField & "]
            '                       [AND #emptransfer.stationunkid > 0] = [AND #emptransfer." & strAllocationIdField & " > 0]

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinToPeriod.Replace("hremployee_master", "Ab")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryToPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryToPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryToPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryToPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryToPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryToPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                                  WHERE     isvoid = 0 " & _
                                                                "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                                                "AND reason_id > 0 " & _
                                                                "AND Ab.ROW_NO = 1 " & _
                                                                "AND cfcommon_period_tran.end_date BETWEEN @fromperiodenddatenextdate AND @toperiodenddate "

            If mintEmployeeunkid > 0 Then
                strQ &= " AND prsalaryincrement_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryToPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryToPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryToPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryToPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                                ) AS C " & _
                                          "WHERE     C.ROWNO = 1 " & _
                                                  ") AS BB ON aa.periodunkid = bb.periodunkid " & _
                                                             "AND aa.employeeunkid = bb.employeeunkid " & _
                                                             "AND bb.ROW_NO = aa.ROW_NO - 1 " & _
                            ") AS A "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, mintFromYearUnkId, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
            'Sohail (21 Aug 2015) -- End

            strQ &= "LEFT JOIN ( SELECT  * " & _
                                        "FROM    ( SELECT    prsalaryincrement_tran.periodunkid  " & _
                                                          ", period_name " & _
                                                          ", prsalaryincrement_tran.employeeunkid " & _
                                                          ", employeecode " & _
                                                          ", reason_id " & _
                                                          ", cfcommon_master.name AS reason " & _
                                                          ", CONVERT(CHAR(8), incrementdate, 112) AS incrementdate " & _
                                                          ", newscale " & _
                                                          ", CASE WHEN increment >= 0 THEN 'P' " & _
                                                                 "ELSE 'N' " & _
                                                            "END AS mode  " & _
                                                          ", DENSE_RANK() OVER ( PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS ROWNO "

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                                  FROM      " & mstrFromDatabaseName & "..prsalaryincrement_tran " & _
                                                              "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                                                              "LEFT JOIN " & mstrFromDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prsalaryincrement_tran.periodunkid " & _
                                                              "LEFT JOIN " & mstrFromDatabaseName & "..cfcommon_master ON masterunkid = reason_id AND mastertype = " & clsCommon_Master.enCommonMaster.SALINC_REASON & " " & _
                                                              "LEFT JOIN ( SELECT " & _
                                                                                "A.employeeunkid  " & _
                                                                              ", A." & strAllocationIdField & " " & _
                                                                              ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A." & strEffDateFld & " DESC ) AS ROW_NO " & _
                                                                          "FROM " & _
                                                                            "( SELECT " & _
                                                                                    "#emptransfer.employeeunkid  " & _
                                                                                  ", #emptransfer." & strEffDateFld & " " & _
                                                                                  ", #emptransfer." & strAllocationIdField & " " & _
                                                                                  ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                                            "FROM    #emptransfer " & _
                                                                                    "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid " & _
                                                                            "WHERE   #emptransfer." & strAllocationIdField & " > 0 " & _
                                                                                    "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @fromperiodenddate " & _
                                                                            ") AS A " & _
                                                                          "WHERE   A.ROWNO = 1 " & _
                                                                        ") AS Ab ON prsalaryincrement_tran.employeeunkid = Ab.employeeunkid "
            'Sohail (14 Nov 2017) - [ A.stationunkid] = [A." & strAllocationIdField & "]
            '                       [emptransfer.stationunkid] = [#emptransfer." & strAllocationIdField & "]
            '                       [AND #emptransfer.stationunkid > 0] = [AND #emptransfer." & strAllocationIdField & " > 0]

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinFromPeriod.Replace("hremployee_master", "Ab")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryFromPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryFromPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryFromPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryFromPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryFromPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                              WHERE     isvoid = 0 " & _
                                                            "AND ISNULL(cfcommon_period_tran.modulerefid, " & enModuleReference.Payroll & ") = " & enModuleReference.Payroll & " " & _
                                                            "AND ISNULL(cfcommon_period_tran.isactive, 1) = 1 " & _
                                                            "AND Ab.ROW_NO = 1 " & _
                                                            "AND ISNULL(cfcommon_period_tran.end_date, @fromperiodenddate) <= @fromperiodenddate "

            If mintEmployeeunkid > 0 Then
                strQ &= " AND prsalaryincrement_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryFromPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryFromPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryFromPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "               ) AS D " & _
                                        "WHERE   D.ROWNO = 1 " & _
                                      ") AS B ON A.employeeunkid = B.employeeunkid " & _
                    "UNION ALL " & _
                    "SELECT  Main.employeeunkid  " & _
                          ", Main.employeecode " & _
                          ", Main.employeename " & _
                          ", Main.reason_id " & _
                          ", Main.reason " & _
                          ", CASE reason_id WHEN -2 THEN SUM(ISNULL(amount, 0)) * -1 WHEN -5 THEN SUM(ISNULL(amount, 0)) * -1 ELSE SUM(ISNULL(amount, 0)) END AS incrementamount " & _
                          ", Main.GName " & _
                    "FROM    ( SELECT    employeeunkid  " & _
                                      ", employeecode " & _
                                      ", employeename " & _
                                      ", reason_id " & _
                                      ", reason " & _
                                      ", GName " & _
                              "FROM      CTE " & _
                              "UNION ALL " & _
                              "SELECT    B.employeeunkid  " & _
                                      ", B.employeecode " & _
                                      ", B.employeename " & _
                                      ", -2 AS reason_id " & _
                                      ", @TransferOut AS reason " & _
                                      ", B.GName " & _
                              "FROM      ( SELECT    C." & strTransferUnkIdFld & "  " & _
                                                  ", C.employeeunkid " & _
                                                  ", C.employeecode " & _
                                                  ", C.employeename " & _
                                                  ", C.allocationunkid " & _
                                                  ", C.start_date " & _
                                                  ", C.Id " & _
                                                  ", C.GName " & _
                                          "FROM      ( SELECT    A." & strTransferUnkIdFld & "  " & _
                                                              ", A.employeeunkid " & _
                                                              ", A.employeecode " & _
                                                              ", A.employeename " & _
                                                              ", A.allocationunkid " & _
                                                              ", A.start_date " & _
                                                              ", A.allocationunkid AS Id " & _
                                                              ", A.GName " & _
                                                              ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A.start_date DESC ) AS ROW_NO " & _
                                                      "FROM      ( SELECT    #emptransfer." & strTransferUnkIdFld & "  " & _
                                                                          ", #emptransfer.employeeunkid " & _
                                                                          ", hremployee_master.employeecode " & _
                                                                          ", #emptransfer." & strAllocationIdField & " AS allocationunkid " & _
                                                                          ", #emptransfer." & strEffDateFld & " AS start_date " & _
                                                                          ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO "
            'Sohail (14 Nov 2017) - [emptransfer.stationunkid AS allocationunkid] = [emptransfer." & strAllocationIdField & " AS allocationunkid]

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                      FROM      #emptransfer " & _
                                                    "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinFromPeriod.Replace("hremployee_master", "#emptransfer")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryFromPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryFromPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryFromPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryFromPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryFromPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                      WHERE     #emptransfer." & strAllocationIdField & " > 0 " & _
                                                    "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @fromperiodenddate "
            'Sohail (14 Nov 2017) - [AND #emptransfer.stationunkid > 0] = [AND #emptransfer." & strAllocationIdField & " > 0]

            If mintEmployeeunkid > 0 Then
                strQ &= " AND #emptransfer.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryFromPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryFromPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryFromPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= ") AS A " & _
                                      "WHERE     A.ROWNO = 1 " & _
                                    ") AS C " & _
                          "WHERE     C.ROW_NO = 1 " & _
                        ") AS B " & _
                        "RIGHT JOIN CTE ON CTE.employeeunkid = B.employeeunkid "

            strQ &= "       /* UNION ALL " & _
                              "SELECT    A.employeeunkid  " & _
                                      ", employeecode " & _
                                      ", employeename  " & _
                                      ", -4 AS reason_id " & _
                                      ", @Reinstated AS reason " & _
                                      ", GName " & _
                              "FROM      ( SELECT    athremployee_master.employeeunkid  " & _
                                                  ", athremployee_master.employeecode " & _
                                                  ", athremployee_master.firstname + ' ' + ISNULL(athremployee_master.othername, '') + ' ' + athremployee_master.surname AS employeename  " & _
                                                  ", TBL.employeeunkid AS empid " & _
                                                  ", athremployee_master.stationunkid AS st " & _
                                                  ", hrstation_master.stationunkid AS Id " & _
                                                  ", hrstation_master.name AS GName " & _
                                                  ", DENSE_RANK() OVER ( PARTITION BY athremployee_master.employeeunkid ORDER BY auditdatetime DESC ) AS ROWNO "

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(athremployee_master.surname,'')+' '+ISNULL(athremployee_master.firstname,'')+' '+ISNULL(athremployee_master.othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(athremployee_master.firstname,'')+' '+ISNULL(athremployee_master.othername,'')+' '+ISNULL(athremployee_master.surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                      FROM      " & mstrToDatabaseName & "..athremployee_master " & _
                                                    "RIGHT JOIN TBL ON athremployee_master.employeeunkid = TBL.employeeunkid "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join.Replace("hremployee_master", "athremployee_master")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryFromPeriod.Replace("hremployee_master", "athremployee_master")
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryFromPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryFromPeriod.Replace("hremployee_master", "athremployee_master")
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryFromPeriod.Replace("hremployee_master", "athremployee_master")
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryFromPeriod.Replace("hremployee_master", "athremployee_master")
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "WHERE     1 = 1 " & _
                                                    "AND ( termination_from_date BETWEEN @fromperiodstartdate AND @fromperiodenddate " & _
                                                          "OR termination_to_date BETWEEN @fromperiodstartdate AND @fromperiodenddate " & _
                                                          "OR empl_enddate BETWEEN @fromperiodstartdate AND @fromperiodenddate " & _
                                                        ") "

            If mintEmployeeunkid > 0 Then
                strQ &= " AND athremployee_master.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter.Replace("hremployee_master", "athremployee_master")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter.Replace("hremployee_master", "athremployee_master")
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryFromPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryFromPeriod.Replace("hremployee_master", "athremployee_master")
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryFromPeriod.Replace("hremployee_master", "athremployee_master")
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    ) AS a " & _
                              "WHERE     a.ROWNO = 1 " & _
                                        "AND a.empid IS NULL */ " & _
                            ") AS Main " & _
                            "LEFT JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON Main.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            "LEFT JOIN " & mstrToDatabaseName & "..prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN " & mstrToDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " " & _
                            "AND payperiodunkid = @toperiodunkid " & _
                    "GROUP BY Main.employeeunkid  " & _
                          ", Main.employeecode " & _
                          ", Main.employeename " & _
                          ", Main.reason_id " & _
                          ", Main.reason " & _
                          ", Main.GName "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrToDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrToDatabaseName, xUserUnkid, xCompanyUnkid, mintToYearUnkId, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrToDatabaseName)
            'Sohail (21 Aug 2015) -- End

            strQ &= "UNION ALL " & _
                    "SELECT  TblExit.employeeunkid  " & _
                                  ", TblExit.employeecode " & _
                                  ", TblExit.employeename " & _
                                  ", TblExit.reason_id " & _
                                  ", TblExit.reason " & _
                                  ", CASE reason_id WHEN -2 THEN SUM(ISNULL(amount, 0)) * -1 WHEN -5 THEN SUM(ISNULL(amount, 0)) * -1 ELSE SUM(ISNULL(amount, 0)) END AS incrementamount " & _
                                  ", TblExit.GName " & _
                            "FROM    ( SELECT    employeeunkid  " & _
                                              ", employeecode " & _
                                              ", employeename " & _
                                              ", -5 AS reason_id " & _
                                              ", @Exit AS reason " & _
                                              ", GName " & _
                                      "FROM      ( SELECT    hremployee_master.employeeunkid  " & _
                                                          ", hremployee_master.employeecode " & _
                                                          ", hremployee_dates_tran.effectivedate " & _
                                                          ", hremployee_dates_tran.date1 " & _
                                                          ", hremployee_dates_tran.date2 " & _
                                                          ", DENSE_RANK() OVER ( PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC, hremployee_dates_tran.datestranunkid DESC ) AS ROWNO "

            If mblnFirstNamethenSurname = False Then
                strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS employeename "
            Else
                strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                              FROM      " & mstrToDatabaseName & "..hremployee_dates_tran " & _
                                                        "LEFT JOIN hremployee_master ON hremployee_dates_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "LEFT JOIN ( SELECT  Trf.TrfEmpId  " & _
                                                                  ", ISNULL(Trf." & strAllocationIdField & ", 0) AS " & strAllocationIdField & " " & _
                                                            "FROM    ( SELECT " & _
                                                                          "ETT.employeeunkid AS TrfEmpId  " & _
                                                                        ", ISNULL(ETT." & strAllocationIdField & ", 0) AS " & strAllocationIdField & " " & _
                                                                        ", CONVERT(CHAR(8), ETT." & strEffDateFld & ", 112) AS EfDt " & _
                                                                        ", ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT." & strEffDateFld & " DESC ) AS Rno " & _
                                                                      "FROM #emptransfer AS ETT " & _
                                                                      "WHERE CONVERT(CHAR(8), " & strEffDateFld & ", 112) <= @toperiodenddate " & _
                                                                    ") AS Trf " & _
                                                            "WHERE   Trf.Rno = 1 " & _
                                                          ") AS ETRF ON ETRF.TrfEmpId = hremployee_dates_tran.employeeunkid "
            'Sohail (14 Nov 2017) - [stationunkid = " & strAllocationIdField & "]

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryToPeriodWExPayroll.Trim.Length > 0 Then
                strQ &= xDateJoinQryToPeriodWExPayroll
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryToPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryToPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryToPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryToPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryToPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                              WHERE hremployee_dates_tran.isvoid = 0 " & _
                                                        "AND hremployee_dates_tran.datetypeunkid IN ( " & enEmp_Dates_Transaction.DT_TERMINATION & ", " & enEmp_Dates_Transaction.DT_RETIREMENT & " ) "

            If mintEmployeeunkid > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strQ &= " AND hrallocation_tracking.employeeunkid = @employeeunkid "
                strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                'Sohail (21 Aug 2015) -- End
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryToPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryToPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryToPeriodWExPayroll.Trim.Length > 0 Then
                    strQ &= xDateFilterQryToPeriodWExPayroll
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                              ) AS A UNPIVOT ( mindate FOR d IN ( date1, date2 ) ) unpvt " & _
                                      "GROUP BY  employeeunkid  " & _
                                              ", employeecode " & _
                                              ", employeename " & _
                                              ", GName " & _
                                      "HAVING    CONVERT(CHAR(8), MIN(mindate), 112) BETWEEN @fromperiodenddatenextdate AND @toperiodenddate " & _
                                    ") AS TblExit " & _
                                    "LEFT JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON TblExit.employeeunkid = prtnaleave_tran.employeeunkid " & _
                                    "LEFT JOIN " & mstrToDatabaseName & "..prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & mstrToDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " " & _
                                    "AND payperiodunkid = @fromperiodunkid " & _
                            "GROUP BY TblExit.employeeunkid  " & _
                                  ", TblExit.employeecode " & _
                                  ", TblExit.employeename " & _
                                  ", TblExit.reason_id " & _
                                  ", TblExit.reason " & _
                                  ", TblExit.GName "



            Call FilterTitleAndFilterQuery()

            strQ &= Me._FilterQuery

            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            strQ &= " DROP TABLE #emptransfer "
            'Sohail (10 May 2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, mintFromYearUnkId, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
            'Sohail (21 Aug 2015) -- End

            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            'strQ = "CREATE TABLE #emptransfer " & _
            '            "( " & _
            '              "transferunkid INT  " & _
            '            ", employeeunkid INT " & _
            '            ", effectivedate DATETIME " & _
            '            ", rehiretranunkid INT " & _
            '            ", stationunkid INT " & _
            '            ", deptgroupunkid INT " & _
            '            ", departmentunkid INT " & _
            '            ", sectiongroupunkid INT " & _
            '            ", sectionunkid INT " & _
            '            ", unitgroupunkid INT " & _
            '            ", unitunkid INT " & _
            '            ", teamunkid INT " & _
            '            ", classgroupunkid INT " & _
            '            ", classunkid INT " & _
            '            ", changereasonunkid INT " & _
            '            ", isfromemployee BIT " & _
            '            ", userunkid INT " & _
            '            ", statusunkid INT " & _
            '            ", isvoid BIT " & _
            '            ", voiduserunkid INT " & _
            '            ", voiddatetime DATETIME " & _
            '            ", voidreason VARCHAR(MAX) " & _
            '            ") " & _
            '        "DECLARE @transferunkid INT  " & _
            '          ", @empunkid INT " & _
            '          ", @effectivedate DATETIME " & _
            '          ", @rehiretranunkid INT " & _
            '          ", @stationunkid INT " & _
            '          ", @deptgroupunkid INT " & _
            '          ", @departmentunkid INT " & _
            '          ", @sectiongroupunkid INT " & _
            '          ", @sectionunkid INT " & _
            '          ", @unitgroupunkid INT " & _
            '          ", @unitunkid INT " & _
            '          ", @teamunkid INT " & _
            '          ", @classgroupunkid INT " & _
            '          ", @classunkid INT " & _
            '          ", @changereasonunkid INT " & _
            '          ", @isfromemployee BIT " & _
            '          ", @userunkid INT " & _
            '          ", @statusunkid INT " & _
            '          ", @isvoid BIT " & _
            '          ", @voiduserunkid INT " & _
            '          ", @voiddatetime DATETIME " & _
            '          ", @voidreason cDescription " & _
            '        " " & _
            '        "DECLARE @prevempunkid INT " & _
            '        "DECLARE @prevstationunkid INT " & _
            '        "SET @prevempunkid = 0 " & _
            '        "SET @prevstationunkid = 0 " & _
            '        " " & _
            '        "DECLARE c CURSOR " & _
            '        "FOR "

            'strInnerQ = " SELECT  hremployee_transfer_tran.transferunkid  " & _
            '                  ", hremployee_transfer_tran.employeeunkid " & _
            '                  ", hremployee_transfer_tran.effectivedate " & _
            '                  ", hremployee_transfer_tran.rehiretranunkid " & _
            '                  ", hremployee_transfer_tran.stationunkid " & _
            '                  ", hremployee_transfer_tran.deptgroupunkid " & _
            '                  ", hremployee_transfer_tran.departmentunkid " & _
            '                  ", hremployee_transfer_tran.sectiongroupunkid " & _
            '                  ", hremployee_transfer_tran.sectionunkid " & _
            '                  ", hremployee_transfer_tran.unitgroupunkid " & _
            '                  ", hremployee_transfer_tran.unitunkid " & _
            '                  ", hremployee_transfer_tran.teamunkid " & _
            '                  ", hremployee_transfer_tran.classgroupunkid " & _
            '                  ", hremployee_transfer_tran.classunkid " & _
            '                  ", hremployee_transfer_tran.changereasonunkid " & _
            '                  ", hremployee_transfer_tran.isfromemployee " & _
            '                  ", hremployee_transfer_tran.userunkid " & _
            '                  ", hremployee_transfer_tran.statusunkid " & _
            '                  ", hremployee_transfer_tran.isvoid " & _
            '                  ", hremployee_transfer_tran.voiduserunkid " & _
            '                  ", hremployee_transfer_tran.voiddatetime " & _
            '                  ", hremployee_transfer_tran.voidreason " & _
            '            "FROM    " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
            '            "WHERE   hremployee_transfer_tran.isvoid = 0 "
            strQ = "CREATE TABLE #emptransfer " & _
                       "( " & _
                         "" & strTransferUnkIdFld & " INT  " & _
                       ", employeeunkid INT " & _
                       ", " & strEffDateFld & " DATETIME " & _
                       ", " & strAllocationIdField & " INT " & _
                       ") " & _
                   "DECLARE @" & strTransferUnkIdFld & " INT  " & _
                     ", @empunkid INT " & _
                     ", @" & strEffDateFld & " DATETIME " & _
                     ", @" & strAllocationIdField & " INT " & _
                   " " & _
                   "DECLARE @prevempunkid INT " & _
                   "DECLARE @prev" & strAllocationIdField & " INT " & _
                   "SET @prevempunkid = 0 " & _
                   "SET @prev" & strAllocationIdField & " = 0 " & _
                   " " & _
                   "DECLARE c CURSOR " & _
                   "FOR "
            'Sohail (14 Nov 2017) -- End

            If mintEmployeeunkid > 0 Then
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
                'strInnerQ &= " AND hremployee_transfer_tran.employeeunkid = " & mintEmployeeunkid & " "
                strInnerQ &= " AND " & strTransferTable & ".employeeunkid = " & mintEmployeeunkid & " "
                'Sohail (14 Nov 2017) -- End
            End If

            If mstrFromDatabaseName <> mstrToDatabaseName Then
                strInnerQ &= " UNION ALL " & _
                               strInnerQ.Replace(mstrFromDatabaseName, mstrToDatabaseName)
            End If

            strQ &= strInnerQ

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            'strQ &= "    ORDER BY hremployee_transfer_tran.employeeunkid  " & _
            '            ", hremployee_transfer_tran.effectivedate " & _
            '        " " & _
            '        "OPEN c " & _
            '        " " & _
            '        "FETCH NEXT FROM c INTO @transferunkid, @empunkid, @effectivedate " & _
            '          ", @rehiretranunkid, @stationunkid, @deptgroupunkid, @departmentunkid " & _
            '          ", @sectiongroupunkid, @sectionunkid, @unitgroupunkid, @unitunkid, @teamunkid " & _
            '          ", @classgroupunkid, @classunkid, @changereasonunkid, @isfromemployee " & _
            '          ", @userunkid, @statusunkid, @isvoid, @voiduserunkid, @voiddatetime " & _
            '          ", @voidreason " & _
            '        " " & _
            '        "WHILE @@FETCH_STATUS = 0 " & _
            '            "BEGIN " & _
            '                "IF @prevempunkid <> @empunkid OR @prev" & strAllocationIdField & " <> @" & strAllocationIdField & " " & _
            '                    "BEGIN " & _
            '                        "INSERT  INTO #emptransfer " & _
            '                        "VALUES  ( @transferunkid, @empunkid, @effectivedate " & _
            '                                ", @rehiretranunkid, @stationunkid, @deptgroupunkid " & _
            '                                ", @departmentunkid, @sectiongroupunkid, @sectionunkid " & _
            '                                ", @unitgroupunkid, @unitunkid, @teamunkid " & _
            '                                ", @classgroupunkid, @classunkid, @changereasonunkid " & _
            '                                ", @isfromemployee, @userunkid, @statusunkid, @isvoid " & _
            '                                ", @voiduserunkid, @voiddatetime, @voidreason ) " & _
            '                    "END " & _
            '                "SET @prevempunkid = @empunkid " & _
            '                "SET @prev" & strAllocationIdField & " = @" & strAllocationIdField & " " & _
            '                "FETCH NEXT FROM c INTO @transferunkid, @empunkid, @effectivedate " & _
            '                  ", @rehiretranunkid, @stationunkid, @deptgroupunkid, @departmentunkid " & _
            '                  ", @sectiongroupunkid, @sectionunkid, @unitgroupunkid, @unitunkid " & _
            '                  ", @teamunkid, @classgroupunkid, @classunkid, @changereasonunkid " & _
            '                  ", @isfromemployee, @userunkid, @statusunkid, @isvoid, @voiduserunkid " & _
            '                  ", @voiddatetime, @voidreason " & _
            '            "END " & _
            '        "CLOSE c " & _
            '        "DEALLOCATE c; "
            strQ &= "    ORDER BY " & strTransferTable & ".employeeunkid  " & _
                        ", " & strTransferTable & "." & strEffDateFld & " " & _
                    " " & _
                    "OPEN c " & _
                    " " & _
                    "FETCH NEXT FROM c INTO @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                    " " & _
                    "WHILE @@FETCH_STATUS = 0 " & _
                        "BEGIN " & _
                            "IF @prevempunkid <> @empunkid OR @prev" & strAllocationIdField & " <> @" & strAllocationIdField & " " & _
                                "BEGIN " & _
                                    "INSERT  INTO #emptransfer " & _
                                    "VALUES  ( @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                                            ") " & _
                                "END " & _
                            "SET @prevempunkid = @empunkid " & _
                            "SET @prev" & strAllocationIdField & " = @" & strAllocationIdField & " " & _
                            "FETCH NEXT FROM c INTO @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                        "END " & _
                    "CLOSE c " & _
                    "DEALLOCATE c; "
            'Sohail (14 Nov 2017) -- End            
            'Sohail (10 May 2015) -- End

            strQ &= "SELECT  periodunkid  " & _
                          ", period_name " & _
                          ", ISNULL(A.GrossSalary, 0) AS GrossSalary " & _
                          ", ISNULL(A.NetSalary, 0) AS NetSalary " & _
                          ", ISNULL(A.HeadCount, 0) AS HeadCount " & _
                          ", ISNULL(A.GName, '') AS GName " & _
                    "FROM    " & mstrToDatabaseName & "..cfcommon_period_tran " & _
                            "JOIN ( " & _
                            "       SELECT B.payperiodunkid   " & _
                            "       , SUM(B.GrossSalary) AS GrossSalary  " & _
                            "       , SUM(B.NetSalary) AS NetSalary  " & _
                            "       , SUM(B.COUNT ) AS HeadCount  " & _
                            "       , B.GName  " & _
                            "       FROM (  " & _
                                              " SELECT  payperiodunkid  " & _
                                              ", SUM(amount) AS GrossSalary " & _
                                              ", total_amount + openingbalance AS NetSalary " & _
                                              ", prpayrollprocess_tran.employeeunkid  AS employeeunkid " & _
                                              ", 1 AS COUNT "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                    FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master  ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master on prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "LEFT JOIN ( SELECT " & _
                                                                    "A.employeeunkid  " & _
                                                                  ", A." & strAllocationIdField & " " & _
                                                                  ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A." & strEffDateFld & " DESC ) AS ROW_NO " & _
                                                              "FROM " & _
                                                                "( SELECT " & _
                                                                        "#emptransfer.employeeunkid  " & _
                                                                      ", #emptransfer." & strEffDateFld & " " & _
                                                                      ", #emptransfer." & strAllocationIdField & " " & _
                                                                      ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                                "FROM    #emptransfer " & _
                                                                        "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "WHERE   #emptransfer." & strAllocationIdField & " > 0 " & _
                                                                        "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @fromperiodenddate " & _
                                                                ") AS A " & _
                                                              "WHERE   A.ROWNO = 1 " & _
                                                ") AS A ON prpayrollprocess_tran.employeeunkid = A.employeeunkid "
            'Sohail (14 Nov 2017) - [, A.stationunkid] = [, A." & strAllocationIdField & "]
            '                       [, #emptransfer.stationunkid] = [, #emptransfer." & strAllocationIdField & "]
            '                       [AND #emptransfer.stationunkid > 0] = [AND #emptransfer." & strAllocationIdField & " > 0]

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinFromPeriod.Replace("hremployee_master", "A")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryFromPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryFromPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryFromPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryFromPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryFromPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND A.ROW_NO = 1 " & _
                                                "AND payperiodunkid = @fromperiodunkid " & _
                                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " "
            'Hemant (06 Dec 2024) -- [typeof_id = " & enTypeOf.Salary & " --> prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "]

            If mintEmployeeunkid > 0 Then
                strQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryFromPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryFromPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryFromPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    GROUP BY payperiodunkid "

            'Sohail (14 Nov 2017) - [Revoved = , hrstation_master.stationunkid] 

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If
            'Sohail (14 Nov 2017) -- End

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If

            'Hemant (06 Dec 2024) -- Start
            'ISSUE(IHI): the Gross is giving an incorrect amount on the salary reconciliation report 
            strQ &= " , prpayrollprocess_tran.employeeunkid , total_amount, openingbalance " & _
                    " ) AS B  " & _
                    " group by B.payperiodunkid, B.GName "
            'Hemant (06 Dec 2024) -- End


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrToDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrToDatabaseName, xUserUnkid, xCompanyUnkid, mintToYearUnkId, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrToDatabaseName)
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    UNION ALL " & _
                                        "       SELECT C.payperiodunkid   " & _
                                        "       , SUM(C.GrossSalary) AS GrossSalary  " & _
                                        "       , SUM(C.NetSalary) AS NetSalary  " & _
                                        "       , SUM(C.COUNT ) AS HeadCount  " & _
                                        "       , C.GName " & _
                                        "       FROM (  " & _
                                        "SELECT  payperiodunkid  " & _
                                              ", SUM(amount) AS GrossSalary " & _
                                                              ", total_amount + openingbalance AS NetSalary " & _
                                                              ", prpayrollprocess_tran.employeeunkid  AS employeeunkid " & _
                                                              ", 1 AS COUNT "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "                    FROM    " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                                "LEFT JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master  ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                "LEFT JOIN " & mstrToDatabaseName & "..prtranhead_master on prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "LEFT JOIN ( SELECT " & _
                                                                    "A.employeeunkid  " & _
                                                                  ", A." & strAllocationIdField & " " & _
                                                                  ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A." & strEffDateFld & " DESC ) AS ROW_NO " & _
                                                              "FROM " & _
                                                                "( SELECT " & _
                                                                        "#emptransfer.employeeunkid  " & _
                                                                      ", #emptransfer." & strEffDateFld & " " & _
                                                                      ", #emptransfer." & strAllocationIdField & " " & _
                                                                      ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                                "FROM    #emptransfer " & _
                                                                        "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "WHERE   #emptransfer." & strAllocationIdField & " > 0 " & _
                                                                        "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @toperiodenddate " & _
                                                                ") AS A " & _
                                                              "WHERE   A.ROWNO = 1 " & _
                                                ") AS A ON prpayrollprocess_tran.employeeunkid = A.employeeunkid "
            'Sohail (14 Nov 2017) - [A.stationunkid] = [A." & strAllocationIdField & "]
            '                       [#emptransfer.stationunkid] = [#emptransfer." & strAllocationIdField & "]
            '                       [AND #emptransfer.stationunkid > 0] = [AND #emptransfer." & strAllocationIdField & " > 0]

            If mintViewIndex > 0 Then
                strQ &= strAllocationJoinToPeriod.Replace("hremployee_master", "A")
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryToPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryToPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryToPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryToPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryToPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryToPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND A.ROW_NO = 1 " & _
                                                "AND payperiodunkid = @toperiodunkid " & _
                                                "AND hremployee_master.isexclude_payroll = 0" & _
                                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " "

            'Hemant (06 Dec 2024) -- [typeof_id = " & enTypeOf.Salary & " --> prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "]

            If mintEmployeeunkid > 0 Then
                strQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryToPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryToPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryToPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryToPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    GROUP BY payperiodunkid "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If

            'Hemant (06 Dec 2024) -- Start
            'ISSUE(IHI): the Gross is giving an incorrect amount on the salary reconciliation report 
            strQ &= " , prpayrollprocess_tran.employeeunkid , total_amount, openingbalance " & _
                    " ) AS C  " & _
                    " group by C.payperiodunkid, C.GName "
            'Hemant (06 Dec 2024) -- End


            strQ &= "                  ) AS A ON A.payperiodunkid = periodunkid " & _
                    "WHERE   cfcommon_period_tran.isactive = 1 " & _
                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "


            'Call FilterTitleAndFilterQuery()

            'strQ &= Me._FilterQuery

            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            strQ &= " DROP TABLE #emptransfer "
            'Sohail (10 May 2015) -- End

            '*** IF YOU CHANGE DSSUMMARY DATASET QUERY, PLEASE DO SAME CHANGES IN DSFOOTER DATASET QUERY
            dsSummary = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            Dim a() As String = dsSummary.Tables(0).AsEnumerable().GroupBy(Function(x) x.Item("GName").ToString).Where(Function(x) x.Count = 1).Select(Function(x) x.Key).ToArray
            Dim b = (From p In dsSummary.Tables(0) Where (a.Contains(p.Item("GName").ToString)) Select (p)).ToList

            If b.Count > 0 Then
                For Each r In b

                    Dim rw As DataRow = dsSummary.Tables(0).NewRow

                    If CInt(r.Item("periodunkid")) = mintFromPeriodUnkId Then
                        rw.Item("periodunkid") = mintToPeriodUnkId
                        rw.Item("period_name") = mstrToPeriodName
                    Else
                        rw.Item("periodunkid") = mintFromPeriodUnkId
                        rw.Item("period_name") = mstrFromPeriodName
                    End If

                    rw.Item("GrossSalary") = 0
                    rw.Item("NetSalary") = 0
                    rw.Item("HeadCount") = 0
                    rw.Item("GName") = r.Item("GName")

                    dsSummary.Tables(0).Rows.Add(rw)
                Next
            End If
            'Sohail (10 May 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, mintFromYearUnkId, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
            'Sohail (21 Aug 2015) -- End

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            'strQ = "CREATE TABLE #emptransfer " & _
            '            "( " & _
            '              "transferunkid INT  " & _
            '            ", employeeunkid INT " & _
            '            ", effectivedate DATETIME " & _
            '            ", rehiretranunkid INT " & _
            '            ", stationunkid INT " & _
            '            ", deptgroupunkid INT " & _
            '            ", departmentunkid INT " & _
            '            ", sectiongroupunkid INT " & _
            '            ", sectionunkid INT " & _
            '            ", unitgroupunkid INT " & _
            '            ", unitunkid INT " & _
            '            ", teamunkid INT " & _
            '            ", classgroupunkid INT " & _
            '            ", classunkid INT " & _
            '            ", changereasonunkid INT " & _
            '            ", isfromemployee BIT " & _
            '            ", userunkid INT " & _
            '            ", statusunkid INT " & _
            '            ", isvoid BIT " & _
            '            ", voiduserunkid INT " & _
            '            ", voiddatetime DATETIME " & _
            '            ", voidreason VARCHAR(MAX) " & _
            '            ") " & _
            '        "DECLARE @transferunkid INT  " & _
            '          ", @empunkid INT " & _
            '          ", @effectivedate DATETIME " & _
            '          ", @rehiretranunkid INT " & _
            '          ", @stationunkid INT " & _
            '          ", @deptgroupunkid INT " & _
            '          ", @departmentunkid INT " & _
            '          ", @sectiongroupunkid INT " & _
            '          ", @sectionunkid INT " & _
            '          ", @unitgroupunkid INT " & _
            '          ", @unitunkid INT " & _
            '          ", @teamunkid INT " & _
            '          ", @classgroupunkid INT " & _
            '          ", @classunkid INT " & _
            '          ", @changereasonunkid INT " & _
            '          ", @isfromemployee BIT " & _
            '          ", @userunkid INT " & _
            '          ", @statusunkid INT " & _
            '          ", @isvoid BIT " & _
            '          ", @voiduserunkid INT " & _
            '          ", @voiddatetime DATETIME " & _
            '          ", @voidreason cDescription " & _
            '        " " & _
            '        "DECLARE @prevempunkid INT " & _
            '        "DECLARE @prevstationunkid INT " & _
            '        "SET @prevempunkid = 0 " & _
            '        "SET @prevstationunkid = 0 " & _
            '        " " & _
            '        "DECLARE c CURSOR " & _
            '        "FOR "

            'strInnerQ = " SELECT  hremployee_transfer_tran.transferunkid  " & _
            '                  ", hremployee_transfer_tran.employeeunkid " & _
            '                  ", hremployee_transfer_tran.effectivedate " & _
            '                  ", hremployee_transfer_tran.rehiretranunkid " & _
            '                  ", hremployee_transfer_tran.stationunkid " & _
            '                  ", hremployee_transfer_tran.deptgroupunkid " & _
            '                  ", hremployee_transfer_tran.departmentunkid " & _
            '                  ", hremployee_transfer_tran.sectiongroupunkid " & _
            '                  ", hremployee_transfer_tran.sectionunkid " & _
            '                  ", hremployee_transfer_tran.unitgroupunkid " & _
            '                  ", hremployee_transfer_tran.unitunkid " & _
            '                  ", hremployee_transfer_tran.teamunkid " & _
            '                  ", hremployee_transfer_tran.classgroupunkid " & _
            '                  ", hremployee_transfer_tran.classunkid " & _
            '                  ", hremployee_transfer_tran.changereasonunkid " & _
            '                  ", hremployee_transfer_tran.isfromemployee " & _
            '                  ", hremployee_transfer_tran.userunkid " & _
            '                  ", hremployee_transfer_tran.statusunkid " & _
            '                  ", hremployee_transfer_tran.isvoid " & _
            '                  ", hremployee_transfer_tran.voiduserunkid " & _
            '                  ", hremployee_transfer_tran.voiddatetime " & _
            '                  ", hremployee_transfer_tran.voidreason " & _
            '            "FROM    " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
            '            "WHERE   hremployee_transfer_tran.isvoid = 0 "
            strQ = "CREATE TABLE #emptransfer " & _
                       "( " & _
                         "" & strTransferUnkIdFld & " INT  " & _
                       ", employeeunkid INT " & _
                       ", " & strEffDateFld & " DATETIME " & _
                       ", " & strAllocationIdField & " INT " & _
                       ") " & _
                   "DECLARE @" & strTransferUnkIdFld & " INT  " & _
                     ", @empunkid INT " & _
                     ", @" & strEffDateFld & " DATETIME " & _
                     ", @" & strAllocationIdField & " INT " & _
                   " " & _
                   "DECLARE @prevempunkid INT " & _
                   "DECLARE @prev" & strAllocationIdField & " INT " & _
                   "SET @prevempunkid = 0 " & _
                   "SET @prev" & strAllocationIdField & " = 0 " & _
                   " " & _
                   "DECLARE c CURSOR " & _
                   "FOR "

            strInnerQ = " SELECT  " & strTransferTable & "." & strTransferUnkIdFld & "  " & _
                              ", " & strTransferTable & ".employeeunkid " & _
                              ", " & strTransferTable & "." & strEffDateFld & " " & _
                              ", " & strTransferTable & "." & strAllocationIdField & " " & _
                        "FROM    " & mstrFromDatabaseName & ".." & strTransferTable & " " & _
                        "WHERE   " & strTransferTable & ".isvoid = 0 "
            'Sohail (14 Nov 2017) -- End

            If mintEmployeeunkid > 0 Then
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
                'strInnerQ &= " AND hremployee_transfer_tran.employeeunkid = " & mintEmployeeunkid & " "
                strInnerQ &= " AND " & strTransferTable & ".employeeunkid = " & mintEmployeeunkid & " "
                'Sohail (14 Nov 2017) -- End
            End If

            If mstrFromDatabaseName <> mstrToDatabaseName Then
                strInnerQ &= " UNION ALL " & _
                               strInnerQ.Replace(mstrFromDatabaseName, mstrToDatabaseName)
            End If

            strQ &= strInnerQ

            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            'strQ &= "    ORDER BY hremployee_transfer_tran.employeeunkid  " & _
            '            ", hremployee_transfer_tran.effectivedate " & _
            '        " " & _
            '        "OPEN c " & _
            '        " " & _
            '        "FETCH NEXT FROM c INTO @transferunkid, @empunkid, @effectivedate " & _
            '          ", @rehiretranunkid, @stationunkid, @deptgroupunkid, @departmentunkid " & _
            '          ", @sectiongroupunkid, @sectionunkid, @unitgroupunkid, @unitunkid, @teamunkid " & _
            '          ", @classgroupunkid, @classunkid, @changereasonunkid, @isfromemployee " & _
            '          ", @userunkid, @statusunkid, @isvoid, @voiduserunkid, @voiddatetime " & _
            '          ", @voidreason " & _
            '        " " & _
            '        "WHILE @@FETCH_STATUS = 0 " & _
            '            "BEGIN " & _
            '                "IF @prevempunkid <> @empunkid OR @prev" & strAllocationIdField & " <> @" & strAllocationIdField & " " & _
            '                    "BEGIN " & _
            '                        "INSERT  INTO #emptransfer " & _
            '                        "VALUES  ( @transferunkid, @empunkid, @effectivedate " & _
            '                                ", @rehiretranunkid, @stationunkid, @deptgroupunkid " & _
            '                                ", @departmentunkid, @sectiongroupunkid, @sectionunkid " & _
            '                                ", @unitgroupunkid, @unitunkid, @teamunkid " & _
            '                                ", @classgroupunkid, @classunkid, @changereasonunkid " & _
            '                                ", @isfromemployee, @userunkid, @statusunkid, @isvoid " & _
            '                                ", @voiduserunkid, @voiddatetime, @voidreason ) " & _
            '                    "END " & _
            '                "SET @prevempunkid = @empunkid " & _
            '                "SET @prev" & strAllocationIdField & " = @" & strAllocationIdField & " " & _
            '                "FETCH NEXT FROM c INTO @transferunkid, @empunkid, @effectivedate " & _
            '                  ", @rehiretranunkid, @stationunkid, @deptgroupunkid, @departmentunkid " & _
            '                  ", @sectiongroupunkid, @sectionunkid, @unitgroupunkid, @unitunkid " & _
            '                  ", @teamunkid, @classgroupunkid, @classunkid, @changereasonunkid " & _
            '                  ", @isfromemployee, @userunkid, @statusunkid, @isvoid, @voiduserunkid " & _
            '                  ", @voiddatetime, @voidreason " & _
            '            "END " & _
            '        "CLOSE c " & _
            '        "DEALLOCATE c; "
            strQ &= "    ORDER BY " & strTransferTable & ".employeeunkid  " & _
                        ", " & strTransferTable & "." & strEffDateFld & " " & _
                    " " & _
                    "OPEN c " & _
                    " " & _
                    "FETCH NEXT FROM c INTO @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                    " " & _
                    "WHILE @@FETCH_STATUS = 0 " & _
                        "BEGIN " & _
                            "IF @prevempunkid <> @empunkid OR @prev" & strAllocationIdField & " <> @" & strAllocationIdField & " " & _
                                "BEGIN " & _
                                    "INSERT  INTO #emptransfer " & _
                                    "VALUES  ( @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                                            ") " & _
                                "END " & _
                            "SET @prevempunkid = @empunkid " & _
                            "SET @prev" & strAllocationIdField & " = @" & strAllocationIdField & " " & _
                            "FETCH NEXT FROM c INTO @" & strTransferUnkIdFld & ", @empunkid, @" & strEffDateFld & ", @" & strAllocationIdField & " " & _
                        "END " & _
                    "CLOSE c " & _
                    "DEALLOCATE c; "
            'Sohail (14 Nov 2017) -- End            

            strQ &= "SELECT  periodunkid  " & _
                          ", period_name " & _
                          ", ISNULL(A.GrossSalary, 0) AS GrossSalary " & _
                          ", ISNULL(A.NetSalary, 0) AS NetSalary " & _
                          ", ISNULL(A.HeadCount, 0) AS HeadCount " & _
                    "FROM    " & mstrToDatabaseName & "..cfcommon_period_tran " & _
                            "JOIN (  " & _
                            "       SELECT D.payperiodunkid   " & _
                            "       , SUM(D.GrossSalary) AS GrossSalary  " & _
                            "       , SUM(D.NetSalary) AS NetSalary  " & _
                            "       , SUM(D.COUNT ) AS HeadCount  " & _
                            "       FROM (  " & _
                            "               SELECT  payperiodunkid  " & _
                                              ", SUM(amount) AS GrossSalary " & _
                                              ", total_amount + openingbalance AS NetSalary " & _
                                              ", prpayrollprocess_tran.employeeunkid  AS employeeunkid " & _
                                              ", 1 AS COUNT "


            strQ &= "                    FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master  ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master on prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "LEFT JOIN ( SELECT " & _
                                                                    "A.employeeunkid  " & _
                                                                  ", A." & strAllocationIdField & " " & _
                                                                  ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A." & strEffDateFld & " DESC ) AS ROW_NO " & _
                                                              "FROM " & _
                                                                "( SELECT " & _
                                                                        "#emptransfer.employeeunkid  " & _
                                                                      ", #emptransfer." & strEffDateFld & " " & _
                                                                      ", #emptransfer." & strAllocationIdField & " " & _
                                                                      ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                                "FROM    #emptransfer " & _
                                                                        "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "WHERE   #emptransfer." & strAllocationIdField & " > 0 " & _
                                                                        "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @fromperiodenddate " & _
                                                                ") AS A " & _
                                                              "WHERE   A.ROWNO = 1 " & _
                                                ") AS A ON prpayrollprocess_tran.employeeunkid = A.employeeunkid "
            'Sohail (14 Nov 2017) - [A.stationunkid] = [A." & strAllocationIdField & "]
            '                       [#emptransfer.stationunkid] = [#emptransfer." & strAllocationIdField & "]
            '                       [AND #emptransfer.stationunkid > 0] = [AND #emptransfer." & strAllocationIdField & " > 0]

            If mintViewIndex > 0 Then

            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryFromPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryFromPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryFromPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryFromPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryFromPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryFromPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND A.ROW_NO = 1 " & _
                                                "AND payperiodunkid = @fromperiodunkid " & _
                                                "AND  prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " "
            'Hemant (06 Dec 2024) -- [typeof_id = " & enTypeOf.Salary & " --> prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "]

            If mintEmployeeunkid > 0 Then
                strQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryFromPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryFromPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryFromPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryFromPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    GROUP BY payperiodunkid "

            'Hemant (06 Dec 2024) -- Start
            'ISSUE(IHI): the Gross is giving an incorrect amount on the salary reconciliation report 
            strQ &= " , prpayrollprocess_tran.employeeunkid , total_amount, openingbalance " & _
                    " ) AS D  " & _
                    " group by D.payperiodunkid "
            'Hemant (06 Dec 2024) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrToDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrToDatabaseName, xUserUnkid, xCompanyUnkid, mintToYearUnkId, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrToDatabaseName)
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    UNION ALL " & _
                                 "       SELECT E.payperiodunkid   " & _
                                 "       , SUM(E.GrossSalary) AS GrossSalary  " & _
                                 "       , SUM(E.NetSalary) AS NetSalary  " & _
                                 "       , SUM(E.COUNT ) AS HeadCount  " & _
                                 "       FROM (  " & _
                                        "SELECT  payperiodunkid  " & _
                                              ", SUM(amount) AS GrossSalary " & _
                                              ", total_amount + openingbalance AS NetSalary " & _
                                              ", prpayrollprocess_tran.employeeunkid  AS employeeunkid " & _
                                              ", 1 AS COUNT "


            strQ &= "                    FROM    " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                                "LEFT JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master  ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                "LEFT JOIN " & mstrToDatabaseName & "..prtranhead_master on prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "LEFT JOIN ( SELECT " & _
                                                                    "A.employeeunkid  " & _
                                                                  ", A." & strAllocationIdField & " " & _
                                                                  ", DENSE_RANK() OVER ( PARTITION BY A.employeeunkid ORDER BY A." & strEffDateFld & " DESC ) AS ROW_NO " & _
                                                              "FROM " & _
                                                                "( SELECT " & _
                                                                        "#emptransfer.employeeunkid  " & _
                                                                      ", #emptransfer." & strEffDateFld & " " & _
                                                                      ", #emptransfer." & strAllocationIdField & " " & _
                                                                      ", DENSE_RANK() OVER ( PARTITION BY #emptransfer.employeeunkid ORDER BY #emptransfer." & strEffDateFld & " DESC, #emptransfer." & strTransferUnkIdFld & " DESC ) AS ROWNO " & _
                                                                "FROM    #emptransfer " & _
                                                                        "LEFT JOIN " & mstrToDatabaseName & "..hremployee_master ON #emptransfer.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "WHERE   #emptransfer." & strAllocationIdField & " > 0 " & _
                                                                        "AND CONVERT(CHAR(8), #emptransfer." & strEffDateFld & ", 112) <= @toperiodenddate " & _
                                                                ") AS A " & _
                                                              "WHERE   A.ROWNO = 1 " & _
                                                ") AS A ON prpayrollprocess_tran.employeeunkid = A.employeeunkid "
            'Sohail (14 Nov 2017) - [A.stationunkid] = [A." & strAllocationIdField & "]
            '                       [#emptransfer.stationunkid] = [#emptransfer." & strAllocationIdField & "]
            '                       [AND #emptransfer.stationunkid > 0] = [AND #emptransfer." & strAllocationIdField & " > 0]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xDateJoinQryToPeriod
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQryToPeriod.Trim.Length > 0 Then
            '    StrQ &= xUACQryToPeriod
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQryToPeriod.Trim.Length > 0 Then
                    strQ &= xUACQryToPeriod
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQryToPeriod.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQryToPeriod
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND A.ROW_NO = 1 " & _
                                                "AND payperiodunkid = @toperiodunkid " & _
                                                "AND hremployee_master.isexclude_payroll = 0" & _
                                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " "
            'Hemant (06 Dec 2024) -- [typeof_id = " & enTypeOf.Salary & " --> prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "]

            If mintEmployeeunkid > 0 Then
                strQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQryToPeriod.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQryToPeriod
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQryToPeriod.Trim.Length > 0 Then
                    strQ &= xDateFilterQryToPeriod
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "                    GROUP BY payperiodunkid "
            strQ &= " , prpayrollprocess_tran.employeeunkid , total_amount, openingbalance " & _
                   " ) AS E  " & _
                   " group by E.payperiodunkid "
            'Hemant (06 Dec 2024) -- End
            strQ &= "                  ) AS A ON A.payperiodunkid = periodunkid " & _
                    "WHERE   cfcommon_period_tran.isactive = 1 " & _
                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "

            'Call FilterTitleAndFilterQuery()

            'strQ &= Me._FilterQuery

            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            strQ &= " DROP TABLE #emptransfer "
            'Sohail (10 May 2015) -- End

            '*** IF YOU CHANGE DSFOOTER DATASET QUERY, PLEASE DO SAME CHANGES IN DSSUMMARY DATASET QUERY
            dsFooter = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim lstTransferIn As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("reason_id")) = -1 OrElse CInt(p.Item("reason_id")) = -2) Select p).ToList 'Transfer In OR Transfer Out
            Dim decTotal As Decimal = 0
            Dim intEmpId As Integer = 0

            If lstTransferIn.Count > 0 Then
                For Each dRow As DataRow In lstTransferIn
                    intEmpId = CInt(dRow.Item("employeeunkid"))

                    decTotal = (From p In dsList.Tables(0) Where (CInt(p.Item("reason_id")) > 0 AndAlso CInt(p.Item("employeeunkid")) = intEmpId) Select CDec(p.Item("incrementamount"))).DefaultIfEmpty().Sum
                    If CDec(dRow.Item("incrementamount")) < 0 Then
                        dRow.Item("incrementamount") = CDec(dRow.Item("incrementamount")) + decTotal
                    Else
                        dRow.Item("incrementamount") = CDec(dRow.Item("incrementamount")) - decTotal
                    End If
                    dRow.AcceptChanges()
                Next
            End If



            Dim ds1 As New ArutiReport.Designer.dsArutiReport
            Dim ds2 As New ArutiReport.Designer.dsArutiReport
            Dim ds3 As New ArutiReport.Designer.dsArutiReport



            Dim StrGrpName As String = ""
            Dim mdecTotalDifference As Decimal = 0
            Dim mdecIncTotal As Decimal = 0
            'Sohail (30 Dec 2015) -- Start
            'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
            'Dim mintExitTotal As Integer = 0
            'Dim mintAppointedTotal As Integer = 0
            mintExitTotal = 0
            mintAppointedTotal = 0
            'Sohail (30 Dec 2015) -- End
            Dim rpt_Row As DataRow

            mdecTotalDifference = (From p In dsList.Tables(0) Select CDec(p.Item("incrementamount"))).DefaultIfEmpty().Sum
            mintExitTotal = (From p In dsList.Tables(0) Where (CInt(p.Item("reason_id")) = -5) Select (p)).Count
            mintAppointedTotal = (From p In dsList.Tables(0) Where (CInt(p.Item("reason_id")) = -3) Select (p)).Count

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                rpt_Row = ds1.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employeename")
                rpt_Row.Item("Column4") = dtRow.Item("reason_id")
                rpt_Row.Item("Column5") = dtRow.Item("reason")
                'rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("newscalefrom")), GUI.fmtCurrency)
                'rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("newscaleto")), GUI.fmtCurrency)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("incrementamount")), GUI.fmtCurrency)
                'rpt_Row.Item("Column81") = CDec(Format(CDec(dtRow.Item("incrementamount")), GUI.fmtCurrency))
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("incrementamount")), strfmtCurrency)
                rpt_Row.Item("Column81") = CDec(Format(CDec(dtRow.Item("incrementamount")), strfmtCurrency))
                'Sohail (21 Aug 2015) -- End
                rpt_Row.Item("Column10") = dtRow.Item("GName")

                ds1.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            For Each dtRow As DataRow In dsSummary.Tables("DataTable").Rows
                rpt_Row = ds2.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("periodunkid")
                rpt_Row.Item("Column2") = dtRow.Item("period_name")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossSalary")), GUI.fmtCurrency)
                'rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
                'rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossSalary")), strfmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("NetSalary")), strfmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("NetSalary")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Row.Item("Column6") = dtRow.Item("HeadCount")
                'Sohail (30 Dec 2015) -- Start
                'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
                rpt_Row.Item("Column81") = CDec(Format(CDec(dtRow.Item("GrossSalary")), GUI.fmtCurrency))
                rpt_Row.Item("Column82") = CDec(Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency))
                rpt_Row.Item("Column83") = CDec(Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency))
                rpt_Row.Item("Column84") = CInt(dtRow.Item("HeadCount"))
                'Sohail (30 Dec 2015) -- End
                rpt_Row.Item("Column10") = dtRow.Item("GName")

                ds2.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            For Each dtRow As DataRow In dsFooter.Tables("DataTable").Rows
                rpt_Row = ds3.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("periodunkid")
                rpt_Row.Item("Column2") = dtRow.Item("period_name")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossSalary")), GUI.fmtCurrency)
                'rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
                'rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossSalary")), strfmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("NetSalary")), strfmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("NetSalary")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Row.Item("Column6") = dtRow.Item("HeadCount")
                'Sohail (30 Dec 2015) -- Start
                'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
                rpt_Row.Item("Column81") = CDec(Format(CDec(dtRow.Item("GrossSalary")), GUI.fmtCurrency))
                rpt_Row.Item("Column82") = CDec(Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency))
                rpt_Row.Item("Column83") = CDec(Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency))
                rpt_Row.Item("Column84") = CInt(dtRow.Item("HeadCount"))
                'Sohail (30 Dec 2015) -- End
                rpt_Row.Item("Column10") = ""

                ds3.Tables("ArutiTable").Rows.Add(rpt_Row)

                If CInt(dtRow.Item("periodunkid")) = mintFromPeriodUnkId Then
                    mintFirstPeriodHeadCount = CInt(dtRow.Item("HeadCount"))
                End If
            Next

            objRpt = New ArutiReport.Designer.rptSalaryReconciliationReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = ds2.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            ds2.Tables("ArutiImage").Rows.Add(arrImageRow)

            If ds2.Tables("ArutiTable").Rows.Count <= 0 Then
                ds2.Tables("ArutiTable").Rows.Add("")
            End If

            'If ConfigParameter._Object._IsShowPreparedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 15, "Prepared By :"))
            '    Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            'End If

            'If ConfigParameter._Object._IsShowCheckedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 16, "Checked By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            'End If

            'If ConfigParameter._Object._IsShowApprovedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 17, "Approved By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            'End If

            'If ConfigParameter._Object._IsShowReceivedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 18, "Received By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            'End If

            objRpt.SetDataSource(ds2)
            objRpt.Subreports("Detail").SetDataSource(ds2)
            objRpt.Subreports("Summary").SetDataSource(ds1)
            objRpt.Subreports("Footer").SetDataSource(ds3)

            Call ReportFunction.TextChange(objRpt.Subreports("Detail"), "txtMonth", Language.getMessage(mstrModuleName, 14, "Month"))
            Call ReportFunction.TextChange(objRpt.Subreports("Detail"), "txtGrossSalary", Language.getMessage(mstrModuleName, 15, "Gross Salary"))
            Call ReportFunction.TextChange(objRpt.Subreports("Detail"), "txtNetSalary", Language.getMessage(mstrModuleName, 16, "Net Salary"))
            Call ReportFunction.TextChange(objRpt.Subreports("Detail"), "txtHeadCount", Language.getMessage(mstrModuleName, 17, "Head Count"))
            Call ReportFunction.TextChange(objRpt.Subreports("Footer"), "txtMonth", Language.getMessage(mstrModuleName, 18, "Month"))
            Call ReportFunction.TextChange(objRpt.Subreports("Footer"), "txtGrossSalary", Language.getMessage(mstrModuleName, 15, "Gross Salary"))
            Call ReportFunction.TextChange(objRpt.Subreports("Footer"), "txtNetSalary", Language.getMessage(mstrModuleName, 16, "Net Salary"))
            Call ReportFunction.TextChange(objRpt.Subreports("Footer"), "txtHeadCount", Language.getMessage(mstrModuleName, 17, "Head Count"))
            Call ReportFunction.TextChange(objRpt.Subreports("Detail"), "txtGroupName", mstrReport_GroupName.Replace(":", ""))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 22, "Total"))
            Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtTotal", Language.getMessage(mstrModuleName, 22, "Total"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt, "txtTotalDiff", Format(mdecTotalDifference, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotalDiff", Format(mdecTotalDifference, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            Call ReportFunction.TextChange(objRpt.Subreports("Detail"), "txtVariance", Language.getMessage(mstrModuleName, 23, "Variance"))
            Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtAmountUSD", Language.getMessage(mstrModuleName, 21, "Amount USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Footer"), "txtAsofStaff", Language.getMessage(mstrModuleName, 19, "Payroll Local Staff for") & " " & mstrFromPeriodName & " " & Language.getMessage(mstrModuleName, 20, "to") & " " & mstrToPeriodName & " " & Language.getMessage(mstrModuleName, 24, "period."))
            Call ReportFunction.TextChange(objRpt, "txtApprovedByCA", Language.getMessage(mstrModuleName, 25, "Approved by :  Chief Accountant"))
            Call ReportFunction.TextChange(objRpt, "txtApprovedByHRM", Language.getMessage(mstrModuleName, 26, "Approved by : HR Manager"))
            Call ReportFunction.TextChange(objRpt, "txtApprovedByCFO", Language.getMessage(mstrModuleName, 27, "Approved by : Chief Finance Officer"))
            Call ReportFunction.TextChange(objRpt, "txtApprovedByCEO", Language.getMessage(mstrModuleName, 28, "Approved By : Chief Executive Officer"))
            Call ReportFunction.TextChange(objRpt.Subreports("Footer"), "txtVariance", Language.getMessage(mstrModuleName, 23, "Variance"))

            Call ReportFunction.TextChange(objRpt, "txtFirstCount", mintFirstPeriodHeadCount)
            'Sohail (30 Dec 2015) -- Start
            'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
            'Call ReportFunction.TextChange(objRpt, "txtExitCount", mintExitTotal)
            Call ReportFunction.TextChange(objRpt, "txtExitCount", mintExitTotal * -1)
            'Sohail (30 Dec 2015) -- End
            Call ReportFunction.TextChange(objRpt, "txtAppointedCount", mintAppointedTotal)
            Call ReportFunction.TextChange(objRpt, "txtLastCount", mintFirstPeriodHeadCount - mintExitTotal + mintAppointedTotal)

            Call ReportFunction.TextChange(objRpt, "lblExit", Language.getMessage(mstrModuleName, 7, "Exit"))
            Call ReportFunction.TextChange(objRpt, "lblAppointed", Language.getMessage(mstrModuleName, 3, "Appointed"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)



            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = New DataView(ds2.Tables(0), "", "Column10", DataViewRowState.CurrentRows).ToTable

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column10").Caption = mstrReport_GroupName.Replace(":", "")
                mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 14, "Month")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 15, "Gross Salary")
                mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 16, "Net Salary")
                mdtTableExcel.Columns("Column82").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 17, "Head Count")
                mdtTableExcel.Columns("Column84").SetOrdinal(mintColumn)
                mintColumn += 1

                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next



                mdtTableDetail = New DataView(ds1.Tables(0), "", "Column10, Column5", DataViewRowState.CurrentRows).ToTable

                mintColumn = 0

                mdtTableDetail.Columns("Column10").Caption = ""
                mdtTableDetail.Columns("Column10").SetOrdinal(mintColumn)
                mintColumn += 1


                mdtTableDetail.Columns("Column5").Caption = ""
                mdtTableDetail.Columns("Column5").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableDetail.Columns("Column3").Caption = ""
                mdtTableDetail.Columns("Column3").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableDetail.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 21, "Amount USD")
                mdtTableDetail.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                For i = mintColumn To mdtTableDetail.Columns.Count - 1
                    mdtTableDetail.Columns.RemoveAt(mintColumn)
                Next

                mdtTableFooter = ds3.Tables(0)
            End If


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period From :")
            Language.setMessage(mstrModuleName, 2, "To :")
            Language.setMessage(mstrModuleName, 3, "Appointed")
            Language.setMessage(mstrModuleName, 4, "Transfer In")
            Language.setMessage(mstrModuleName, 5, "Transfer Out")
            Language.setMessage(mstrModuleName, 6, "Reinstated")
            Language.setMessage(mstrModuleName, 7, "Exit")
            Language.setMessage(mstrModuleName, 8, "Reason :")
            Language.setMessage(mstrModuleName, 9, "Order By :")
            Language.setMessage(mstrModuleName, 10, "Grand Total :")
            Language.setMessage(mstrModuleName, 11, "Sub Total :")
            Language.setMessage(mstrModuleName, 12, "Employee Code")
            Language.setMessage(mstrModuleName, 13, "Employee Name")
            Language.setMessage(mstrModuleName, 14, "Month")
            Language.setMessage(mstrModuleName, 15, "Gross Salary")
            Language.setMessage(mstrModuleName, 16, "Net Salary")
            Language.setMessage(mstrModuleName, 17, "Head Count")
            Language.setMessage(mstrModuleName, 18, "Month")
            Language.setMessage(mstrModuleName, 19, "Payroll Local Staff for")
            Language.setMessage(mstrModuleName, 20, "to")
            Language.setMessage(mstrModuleName, 21, "Amount USD")
            Language.setMessage(mstrModuleName, 22, "Total")
            Language.setMessage(mstrModuleName, 23, "Variance")
            Language.setMessage(mstrModuleName, 24, "period.")
            Language.setMessage(mstrModuleName, 25, "Approved by :  Chief Accountant")
            Language.setMessage(mstrModuleName, 26, "Approved by : HR Manager")
            Language.setMessage(mstrModuleName, 27, "Approved by : Chief Finance Officer")
            Language.setMessage(mstrModuleName, 28, "Approved By : Chief Executive Officer")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
