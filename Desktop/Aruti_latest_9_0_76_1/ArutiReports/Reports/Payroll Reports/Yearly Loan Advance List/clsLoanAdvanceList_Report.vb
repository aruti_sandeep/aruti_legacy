#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsLoanAdvanceList_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLoanAdvanceList_Report"
    Private mstrReportId As String = enArutiReport.LoanAdvanceListReport  '109
    Dim objDataOperation As clsDataOperation

#Region "Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintFrmYear As Integer = 0
    Private mintToYear As Integer = 0
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintLoanSchemeId As Integer = 0
    Private mstrLoanSchemeName As String = String.Empty
    Private mintLATypeId As Integer = 0
    Private mstrLATypeName As String = String.Empty
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    Private mintCompanyId As Integer = 0
    Private iCnt As Integer = 1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintViewIndex As Integer = -1
    Private mblnIsDataPresent As Boolean = True

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _From_Year() As Integer
        Set(ByVal value As Integer)
            mintFrmYear = value
        End Set
    End Property

    Public WriteOnly Property _To_Year() As Integer
        Set(ByVal value As Integer)
            mintToYear = value
        End Set
    End Property

    Public WriteOnly Property _Employee_Id() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _Employee_Name() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeName() As String
        Set(ByVal value As String)
            mstrLoanSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _LATypeId() As Integer
        Set(ByVal value As Integer)
            mintLATypeId = value
        End Set
    End Property

    Public WriteOnly Property _LATypeName() As String
        Set(ByVal value As String)
            mstrLATypeName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _CompanyId() As Integer
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public ReadOnly Property _IsDataPresent() As Boolean
        Get
            Return mblnIsDataPresent
        End Get
    End Property

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintFrmYear = CInt(Now.Year)
            mintToYear = CInt(Now.Year)
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintLoanSchemeId = 0
            mstrLoanSchemeName = String.Empty
            mintLATypeId = 0
            mstrLATypeName = String.Empty
            mintStatusId = 0
            mstrStatusName = String.Empty
            mintCompanyId = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            iCnt = 1
            Me._FilterTitle = ""
            Me._FilterQuery = ""
            mblnIsDataPresent = True

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Advance"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Completed"))

            'Nilay (09-Dec-2015) -- Start
            objDataOperation.AddParameter("@FutureLoan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Future Loan"))
            'Nilay (09-Dec-2015) -- End

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND A" & iCnt & ".EmpId = @EmpId "
            End If

            If mintLoanSchemeId > 0 Then
                objDataOperation.AddParameter("@LSchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeId)
                Me._FilterQuery &= " AND A" & iCnt & ".LSchId = @LSchId "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND A" & iCnt & ".StId = @StId "
            End If

            If mintLATypeId > 0 Then
                objDataOperation.AddParameter("@LATyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLATypeId)
                Me._FilterQuery &= " AND A" & iCnt & ".LATyId = @LATyId "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        Try

            'objRpt = Generate_DetailReport()

            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'End If

            'If objRpt Is Nothing Then
            '    mblnIsDataPresent = False
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                           Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

            If objRpt Is Nothing Then
                mblnIsDataPresent = False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Nilay (10-Oct-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("Ecode", Language.getMessage(mstrModuleName, 12, "Code")))
            iColumn_DetailReport.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 13, "Employee")))
            iColumn_DetailReport.Add(New IColumn("LCode", Language.getMessage(mstrModuleName, 14, "Loan Code")))
            iColumn_DetailReport.Add(New IColumn("LScheme", Language.getMessage(mstrModuleName, 15, "Loan Scheme")))
            iColumn_DetailReport.Add(New IColumn("LAType", Language.getMessage(mstrModuleName, 16, "Loan/Advance")))
            iColumn_DetailReport.Add(New IColumn("Installment", Language.getMessage(mstrModuleName, 17, "Installment")))
            iColumn_DetailReport.Add(New IColumn("Amount", Language.getMessage(mstrModuleName, 18, "Amount")))
            iColumn_DetailReport.Add(New IColumn("Period", Language.getMessage(mstrModuleName, 19, "Period")))
            iColumn_DetailReport.Add(New IColumn("LStatus", Language.getMessage(mstrModuleName, 20, "Status")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Dim StrDBName As String = String.Empty
        Dim StrDBYear As String = String.Empty
        Dim blnUnion As Boolean = False
        Dim dtDbEndDate As DateTime
        Try
            objDataOperation = New clsDataOperation

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String



            StrQ = "SELECT " & _
                   " YEAR(start_date) AS DYear " & _
                   " ,database_name AS dbname " & _
                   " ,yearunkid AS YId " & _
                   " ,CONVERT(CHAR(8),end_date,112) AS EndDate " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE companyunkid = @CompanyId " & _
                   " AND YEAR(start_date) >= @FYear " & _
                   " AND YEAR(start_date) <= @TYear "

            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyId)
            objDataOperation.AddParameter("@FYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFrmYear)
            objDataOperation.AddParameter("@TYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "From Year : ") & " " & mintFrmYear.ToString & " " & _
                               Language.getMessage(mstrModuleName, 33, "To Year : ") & " " & mintToYear.ToString & " "

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintLoanSchemeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Loan Scheme :") & " " & mstrLoanSchemeName & " "
            End If

            If mintStatusId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Loan Status :") & " " & mstrStatusName & " "
            End If

            If mintLATypeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Loan/Advance :") & " " & mstrLATypeName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Order By :") & " " & Me.OrderByDisplay & " "
            End If

            If dsList.Tables(0).Rows.Count > 0 Then blnUnion = True

            StrQ = ""

            For Each dRow As DataRow In dsList.Tables(0).Rows

                StrDBName = dRow.Item("dbname").ToString
                StrDBYear = dRow.Item("DYear").ToString
                dtDbEndDate = eZeeDate.convertDate(dRow.Item("EndDate"))

                xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtDbEndDate, xOnlyApproved, StrDBName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, dtDbEndDate, StrDBName)

                StrQ &= "SELECT " & _
                                 " A" & iCnt & ".VocNo AS VocNo " & _
                                 ",A" & iCnt & ".Ecode AS Ecode " & _
                                 ",A" & iCnt & ".EName AS EName " & _
                                 ",A" & iCnt & ".LCode AS LCode " & _
                                 ",A" & iCnt & ".LScheme AS LScheme " & _
                                 ",A" & iCnt & ".LAType AS LAType " & _
                                 ",A" & iCnt & ".Installment AS Installment " & _
                                 ",A" & iCnt & ".Amount AS Amount " & _
                                 ",A" & iCnt & ".Period AS Period " & _
                                 ",CASE WHEN ISNULL(A" & iCnt & ".LSPrId,0) =0 THEN @FutureLoan ELSE A" & iCnt & ".LStatus END AS LStatus " & _
                                 ",A" & iCnt & ".Id AS Id " & _
                                 ",A" & iCnt & ".GName AS  GName " & _
                                 ",A" & iCnt & ".StId AS StId" & _
                                 ",A" & iCnt & ".EmpId AS EmpId " & _
                                 ",A" & iCnt & ".LSchId AS LSchId " & _
                                 ",A" & iCnt & ".LATyId AS LATyId " & _
                                 ",'" & StrDBYear & "' AS DYear " & _
                            "FROM " & _
                            "( " & _
                                 "SELECT " & _
                                   "lnloan_advance_tran.loanvoucher_no AS VocNo " & _
                                 ",hremployee_master.employeecode AS Ecode "

                'Nilay (09-Dec-2015) -- Start
                '",A" & iCnt & ".LStatus AS LStatus " & _  -- REMOVED
                '",CASE WHEN ISNULL(A" & iCnt & ".LSPrId,0) =0 THEN @FutureLoan ELSE A" & iCnt & ".LStatus END AS LStatus " & _  -- ADDED
                'Nilay (09-Dec-2015) -- End

                If mblnFirstNamethenSurname = False Then
                    StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                Else
                    StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                End If

                StrQ &= ",ISNULL(lnloan_scheme_master.code,'') AS LCode " & _
                                 ",CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                                       "THEN lnloan_scheme_master.name " & _
                                       "ELSE @Advance " & _
                                  "END AS LScheme " & _
                                 ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
                                       "ELSE @Advance " & _
                                  "END AS LAType " & _
                                 ",CASE WHEN lnloan_advance_tran.emi_tenure <= 0 THEN '' ELSE CAST(lnloan_advance_tran.emi_tenure AS NVARCHAR(MAX)) END AS Installment " & _
                                 ",CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                                       "THEN lnloan_advance_tran.net_amount " & _
                                       "ELSE lnloan_advance_tran.advance_amount " & _
                                  "END AS Amount " & _
                                 ",cfcommon_period_tran.period_name AS Period " & _
                                 ",CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
                                       "WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
                                       "WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
                                       "WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
                                   "END AS LStatus " & _
                                 ",CASE WHEN ISNULL(LSP.periodunkid,0) = 0 THEN -999 ELSE lnloan_advance_tran.loan_statusunkid END AS StId " & _
                                 ",hremployee_master.employeeunkid AS EmpId " & _
                                 ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS LSchId " & _
                                 ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS LATyId " & _
                                 ",LSP.periodunkid AS LSPrId "

                'Nilay (09-Dec-2015) -- Start
                '",lnloan_advance_tran.loan_statusunkid AS StId " & _ -- REMOVED
                '",CASE WHEN ISNULL(LSP.periodunkid,0) = 0 THEN -999 ELSE lnloan_advance_tran.loan_statusunkid END AS StId " & _  -- ADDED
                '",LSP.periodunkid AS LSPrId "  -- ADDED
                'Nilay (09-Dec-2015) -- End

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                'Nilay (09-Dec-2015) -- Start
                'StrQ &= "FROM " & StrDBName & "..lnloan_advance_tran " & _
                '                 "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
                '                                       ", lnloan_status_tran.loanadvancetranunkid " & _
                '                                       ", lnloan_status_tran.status_date " & _
                '                                       ", lnloan_status_tran.statusunkid " & _
                '                                       ", lnloan_status_tran.remark " & _
                '                                "FROM " & StrDBName & "..lnloan_status_tran " & _
                '                                          "JOIN ( SELECT   MAX(lnloan_status_tran.loanstatustranunkid) AS loanstatustranunkid " & _
                '                                                           ", lnloan_status_tran.loanadvancetranunkid " & _
                '                                "FROM " & StrDBName & "..lnloan_status_tran " & _
                '                                          "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
                '                                                                ", lnloan_status_tran.loanadvancetranunkid " & _
                '                                                          "FROM " & StrDBName & "..lnloan_status_tran " & _
                '                                                          "WHERE  ISNULL(isvoid, 0) = 0 " & _
                '                                                          "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
                '                                                                           ") AS Loan_Status ON lnloan_status_tran.loanadvancetranunkid = Loan_Status.loanadvancetranunkid " & _
                '                                                                                    "AND lnloan_status_tran.status_date = Loan_Status.status_date " & _
                '                                "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
                '                                                  "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
                '                                                ") AS A ON A.loanstatustranunkid = lnloan_status_tran.loanstatustranunkid " & _
                '                             ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
                '                 "LEFT JOIN  " & StrDBName & "..hremployee_master ON  " & StrDBName & "..lnloan_advance_tran.employeeunkid =  " & StrDBName & "..hremployee_master.employeeunkid " & _
                '                 "JOIN  " & StrDBName & "..cfcommon_period_tran ON  " & StrDBName & "..cfcommon_period_tran.periodunkid =  " & StrDBName & "..lnloan_advance_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " AND " & StrDBName & "..cfcommon_period_tran.yearunkid = '" & dRow.Item("YId") & "'  " & _
                '                 "LEFT JOIN  " & StrDBName & "..lnloan_scheme_master ON  " & StrDBName & "..lnloan_scheme_master.loanschemeunkid =  " & StrDBName & "..lnloan_advance_tran.loanschemeunkid "

                Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, dRow.Item("YId"), 1, , True)
                Dim dtAsONDate As DateTime = Nothing
                If intFirstOpenPeriodID <= 0 Then
                    dtAsONDate = Now
                Else
                    Dim objPrd As New clscommom_period_Tran
                    objPrd._Periodunkid(xDatabaseName) = intFirstOpenPeriodID
                    dtAsONDate = objPrd._End_Date
                    objPrd = Nothing
                End If


                'Varsha Rana (16-Oct-2017) -- Start
                'Issue - On Loan/Advance List Report duplicate entry resolve

                'StrQ &= "FROM " & StrDBName & "..lnloan_advance_tran " & _
                '       " LEFT JOIN " & StrDBName & "..lnloan_status_tran ON lnloan_status_tran.loanadvancetranunkid = " & StrDBName & "..lnloan_advance_tran.loanadvancetranunkid " & _
                '       " LEFT JOIN " & StrDBName & "..cfcommon_period_tran AS LSP ON lnloan_status_tran.periodunkid = LSP.periodunkid " & _
                '               " AND LSP.modulerefid = 1 AND CONVERT(CHAR(8),LSP.start_date,112) <= '" & eZeeDate.convertDate(dtAsONDate) & "' " & _
                '                "LEFT JOIN  " & StrDBName & "..hremployee_master ON  " & StrDBName & "..lnloan_advance_tran.employeeunkid =  " & StrDBName & "..hremployee_master.employeeunkid " & _
                '       "JOIN  " & StrDBName & "..cfcommon_period_tran ON  " & StrDBName & "..cfcommon_period_tran.periodunkid =  " & StrDBName & "..lnloan_advance_tran.periodunkid " & _
                '               " AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " AND " & StrDBName & "..cfcommon_period_tran.yearunkid = '" & dRow.Item("YId") & "'  " & _
                '                "LEFT JOIN  " & StrDBName & "..lnloan_scheme_master ON  " & StrDBName & "..lnloan_scheme_master.loanschemeunkid =  " & StrDBName & "..lnloan_advance_tran.loanschemeunkid "


                StrQ &= "FROM " & StrDBName & "..lnloan_advance_tran " & _
                        " LEFT JOIN ( SELECT * FROM	 ( SELECT *, dense_rank() OVER ( PARTITION BY loanadvancetranunkid ORDER BY lnloan_status_tran.status_date DESC, lnloan_status_tran.loanstatustranunkid DESC	) AS ROWNO " & _
                                 " FROM " & StrDBName & "..lnloan_status_tran Where isvoid = 0 ) AS A WHERE A.ROWNO	 = 1) AS loanstatus ON lnloan_advance_tran.loanadvancetranunkid = loanstatus.loanadvancetranunkid " & _
                        " LEFT JOIN " & StrDBName & "..cfcommon_period_tran AS LSP ON loanstatus.periodunkid = LSP.periodunkid " & _
                                " AND LSP.modulerefid = 1 AND CONVERT(CHAR(8),LSP.start_date,112) <= '" & eZeeDate.convertDate(dtAsONDate) & "' " & _
                                 "LEFT JOIN  " & StrDBName & "..hremployee_master ON  " & StrDBName & "..lnloan_advance_tran.employeeunkid =  " & StrDBName & "..hremployee_master.employeeunkid " & _
                        "JOIN  " & StrDBName & "..cfcommon_period_tran ON  " & StrDBName & "..cfcommon_period_tran.periodunkid =  " & StrDBName & "..lnloan_advance_tran.periodunkid " & _
                                " AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " AND " & StrDBName & "..cfcommon_period_tran.yearunkid = '" & dRow.Item("YId") & "'  " & _
                                 "LEFT JOIN  " & StrDBName & "..lnloan_scheme_master ON  " & StrDBName & "..lnloan_scheme_master.loanschemeunkid =  " & StrDBName & "..lnloan_advance_tran.loanschemeunkid "

                'Varsha Rana (16-Oct-2017) -- End


                'Nilay (09-Dec-2015) -- End

                StrQ &= mstrAnalysis_Join

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                StrQ &= ") AS A" & iCnt.ToString & " WHERE 1 = 1 "

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                If blnUnion = True Then
                    If iCnt < dsList.Tables(0).Rows.Count Then StrQ &= " UNION ALL "
                End If

                iCnt += 1
            Next

            If StrQ.Trim.Length <= 0 Then Return Nothing

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dTable As DataTable
            dTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dTable.Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                rpt_Rows.Item("Column3") = dtRow.Item("EName")
                rpt_Rows.Item("Column4") = dtRow.Item("LCode")
                rpt_Rows.Item("Column5") = dtRow.Item("LScheme")
                rpt_Rows.Item("Column6") = dtRow.Item("LAType")
                rpt_Rows.Item("Column7") = dtRow.Item("Installment")
                rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column82") = Format(CDec(dTable.Compute("SUM(Amount)", "Ecode = '" & dtRow.Item("Ecode") & "'")), GUI.fmtCurrency)
                rpt_Rows.Item("Column83") = Format(CDec(dTable.Compute("SUM(Amount)", "GName = '" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = dtRow.Item("Period")
                rpt_Rows.Item("Column10") = dtRow.Item("LStatus")
                rpt_Rows.Item("Column11") = dtRow.Item("VocNo")
                rpt_Rows.Item("Column12") = dtRow.Item("DYear")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptYearlyLoanAdvanceList

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 25, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 26, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 27, "Page :"))
            Call ReportFunction.TextChange(objRpt, "txtVoc", Language.getMessage(mstrModuleName, 28, "Voc. No."))
            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 12, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 13, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtLCode", Language.getMessage(mstrModuleName, 14, "Loan Code"))
            Call ReportFunction.TextChange(objRpt, "txtLScheme", Language.getMessage(mstrModuleName, 15, "Loan Scheme"))
            Call ReportFunction.TextChange(objRpt, "txtLAType", Language.getMessage(mstrModuleName, 16, "Loan/Advance"))
            Call ReportFunction.TextChange(objRpt, "txtInstallation", Language.getMessage(mstrModuleName, 17, "Installment"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 18, "Amount"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 19, "Period"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 20, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            If mstrReport_GroupName <> "" Then
                Call ReportFunction.TextChange(objRpt, "lblTotal1", Language.getMessage(mstrModuleName, 29, "Sub Total : "))
            Else
                Call ReportFunction.TextChange(objRpt, "lblTotal1", Language.getMessage(mstrModuleName, 30, "Group Total : "))
            End If
            Call ReportFunction.TextChange(objRpt, "lblTotal2", Language.getMessage(mstrModuleName, 30, "Group Total : "))
            Call ReportFunction.TextChange(objRpt, "lblTotal3", Language.getMessage(mstrModuleName, 31, "Grand Total : "))
            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                If IsDBNull(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column81")) = False Then
                    Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
                End If

            End If

            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 34, "Year : "))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim exForce As Exception
    '    Dim StrDBName As String = String.Empty
    '    Dim StrDBYear As String = String.Empty
    '    Dim blnUnion As Boolean = False
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '               " YEAR(start_date) AS DYear " & _
    '               " ,database_name AS dbname " & _
    '               " ,yearunkid AS YId " & _
    '               "FROM hrmsConfiguration..cffinancial_year_tran " & _
    '               "WHERE companyunkid = @CompanyId " & _
    '               " AND YEAR(start_date) >= @FYear " & _
    '               " AND YEAR(start_date) <= @TYear "

    '        objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyId)
    '        objDataOperation.AddParameter("@FYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFrmYear)
    '        objDataOperation.AddParameter("@TYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "From Year : ") & " " & mintFrmYear.ToString & " " & _
    '                           Language.getMessage(mstrModuleName, 33, "To Year : ") & " " & mintToYear.ToString & " "

    '        If mintEmployeeId > 0 Then
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Employee :") & " " & mstrEmployeeName & " "
    '        End If

    '        If mintLoanSchemeId > 0 Then
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Loan Scheme :") & " " & mstrLoanSchemeName & " "
    '        End If

    '        If mintStatusId > 0 Then
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Loan Status :") & " " & mstrStatusName & " "
    '        End If

    '        If mintLATypeId > 0 Then
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Loan/Advance :") & " " & mstrLATypeName & " "
    '        End If

    '        If Me.OrderByQuery <> "" Then
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Order By :") & " " & Me.OrderByDisplay & " "
    '        End If

    '        If dsList.Tables(0).Rows.Count > 0 Then blnUnion = True

    '        StrQ = ""

    '        For Each dRow As DataRow In dsList.Tables(0).Rows
    '            StrDBName = dRow.Item("dbname").ToString
    '            StrDBYear = dRow.Item("DYear").ToString
    '            StrQ &= "SELECT " & _
    '                             " A" & iCnt & ".VocNo AS VocNo " & _
    '                             ",A" & iCnt & ".Ecode AS Ecode " & _
    '                             ",A" & iCnt & ".EName AS EName " & _
    '                             ",A" & iCnt & ".LCode AS LCode " & _
    '                             ",A" & iCnt & ".LScheme AS LScheme " & _
    '                             ",A" & iCnt & ".LAType AS LAType " & _
    '                             ",A" & iCnt & ".Installment AS Installment " & _
    '                             ",A" & iCnt & ".Amount AS Amount " & _
    '                             ",A" & iCnt & ".Period AS Period " & _
    '                             ",A" & iCnt & ".LStatus AS LStatus " & _
    '                             ",A" & iCnt & ".Id AS Id " & _
    '                             ",A" & iCnt & ".GName AS  GName " & _
    '                             ",A" & iCnt & ".StId AS StId" & _
    '                             ",A" & iCnt & ".EmpId AS EmpId " & _
    '                             ",A" & iCnt & ".LSchId AS LSchId " & _
    '                             ",A" & iCnt & ".LATyId AS LATyId " & _
    '                             ",'" & StrDBYear & "' AS DYear " & _
    '                        "FROM " & _
    '                        "( " & _
    '                             "SELECT " & _
    '                               "lnloan_advance_tran.loanvoucher_no AS VocNo " & _
    '                             ",hremployee_master.employeecode AS Ecode "
    '            'S.SANDEEP [ 26 MAR 2014 ] -- START
    '            '"",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName " 
    '            If mblnFirstNamethenSurname = False Then
    '                StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
    '            Else
    '                StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
    '            End If
    '            'S.SANDEEP [ 26 MAR 2014 ] -- END


    '            StrQ &= ",ISNULL(lnloan_scheme_master.code,'') AS LCode " & _
    '                             ",CASE WHEN lnloan_advance_tran.isloan = 1 " & _
    '                                   "THEN lnloan_scheme_master.name " & _
    '                                   "ELSE @Advance " & _
    '                              "END AS LScheme " & _
    '                             ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
    '                                   "ELSE @Advance " & _
    '                              "END AS LAType " & _
    '                             ",CASE WHEN lnloan_advance_tran.emi_tenure <= 0 THEN '' ELSE CAST(lnloan_advance_tran.emi_tenure AS NVARCHAR(MAX)) END AS Installment " & _
    '                             ",CASE WHEN lnloan_advance_tran.isloan = 1 " & _
    '                                   "THEN lnloan_advance_tran.net_amount " & _
    '                                   "ELSE lnloan_advance_tran.advance_amount " & _
    '                              "END AS Amount " & _
    '                             ",cfcommon_period_tran.period_name AS Period " & _
    '                             ",CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
    '                                   "WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
    '                                   "WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
    '                                   "WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
    '                               "END AS LStatus " & _
    '                             ",lnloan_advance_tran.loan_statusunkid AS StId " & _
    '                             ",hremployee_master.employeeunkid AS EmpId " & _
    '                             ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS LSchId " & _
    '                             ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS LATyId "
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If
    '            StrQ &= "FROM " & StrDBName & "..lnloan_advance_tran " & _
    '                             "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
    '                                                   ", lnloan_status_tran.loanadvancetranunkid " & _
    '                                                   ", lnloan_status_tran.status_date " & _
    '                                                   ", lnloan_status_tran.statusunkid " & _
    '                                                   ", lnloan_status_tran.remark " & _
    '                                            "FROM " & StrDBName & "..lnloan_status_tran " & _
    '                                                      "JOIN ( SELECT   MAX(lnloan_status_tran.loanstatustranunkid) AS loanstatustranunkid " & _
    '                                                                       ", lnloan_status_tran.loanadvancetranunkid " & _
    '                                            "FROM " & StrDBName & "..lnloan_status_tran " & _
    '                                                      "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
    '                                                                            ", lnloan_status_tran.loanadvancetranunkid " & _
    '                                                                      "FROM " & StrDBName & "..lnloan_status_tran " & _
    '                                                                      "WHERE  ISNULL(isvoid, 0) = 0 " & _
    '                                                                      "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
    '                                                                                       ") AS Loan_Status ON lnloan_status_tran.loanadvancetranunkid = Loan_Status.loanadvancetranunkid " & _
    '                                                                                                "AND lnloan_status_tran.status_date = Loan_Status.status_date " & _
    '                                            "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
    '                                                              "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
    '                                                            ") AS A ON A.loanstatustranunkid = lnloan_status_tran.loanstatustranunkid " & _
    '                                         ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
    '                             "LEFT JOIN  " & StrDBName & "..hremployee_master ON  " & StrDBName & "..lnloan_advance_tran.employeeunkid =  " & StrDBName & "..hremployee_master.employeeunkid " & _
    '                             "JOIN  " & StrDBName & "..cfcommon_period_tran ON  " & StrDBName & "..cfcommon_period_tran.periodunkid =  " & StrDBName & "..lnloan_advance_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " AND " & StrDBName & "..cfcommon_period_tran.yearunkid = '" & dRow.Item("YId") & "'  " & _
    '                             "LEFT JOIN  " & StrDBName & "..lnloan_scheme_master ON  " & StrDBName & "..lnloan_scheme_master.loanschemeunkid =  " & StrDBName & "..lnloan_advance_tran.loanschemeunkid "

    '            StrQ &= mstrAnalysis_Join
    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If


    '            StrQ &= "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

    '            'Pinkal (27-Feb-2013) -- Start
    '            'Enhancement : TRA Changes

    '            If mstrAdvance_Filter.Trim.Length > 0 Then
    '                StrQ &= " AND " & mstrAdvance_Filter
    '            End If

    '            StrQ &= ") AS A" & iCnt.ToString & " WHERE 1 = 1 "

    '            'Pinkal (27-Feb-2013) -- End

    '            Call FilterTitleAndFilterQuery()

    '            StrQ &= Me._FilterQuery

    '            If blnUnion = True Then
    '                If iCnt < dsList.Tables(0).Rows.Count Then StrQ &= " UNION ALL "
    '            End If

    '            iCnt += 1
    '        Next

    '        If StrQ.Trim.Length <= 0 Then Return Nothing

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dTable As DataTable
    '        dTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dTable.Rows
    '            Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("GName")
    '            rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
    '            rpt_Rows.Item("Column3") = dtRow.Item("EName")
    '            rpt_Rows.Item("Column4") = dtRow.Item("LCode")
    '            rpt_Rows.Item("Column5") = dtRow.Item("LScheme")
    '            rpt_Rows.Item("Column6") = dtRow.Item("LAType")
    '            rpt_Rows.Item("Column7") = dtRow.Item("Installment")
    '            rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column82") = Format(CDec(dTable.Compute("SUM(Amount)", "Ecode = '" & dtRow.Item("Ecode") & "'")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column83") = Format(CDec(dTable.Compute("SUM(Amount)", "GName = '" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column9") = dtRow.Item("Period")
    '            rpt_Rows.Item("Column10") = dtRow.Item("LStatus")
    '            rpt_Rows.Item("Column11") = dtRow.Item("VocNo")
    '            rpt_Rows.Item("Column12") = dtRow.Item("DYear")

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptYearlyLoanAdvanceList

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex <= 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 25, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 26, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 27, "Page :"))
    '        Call ReportFunction.TextChange(objRpt, "txtVoc", Language.getMessage(mstrModuleName, 28, "Voc. No."))
    '        Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 12, "Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 13, "Employee"))
    '        Call ReportFunction.TextChange(objRpt, "txtLCode", Language.getMessage(mstrModuleName, 14, "Loan Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtLScheme", Language.getMessage(mstrModuleName, 15, "Loan Scheme"))
    '        Call ReportFunction.TextChange(objRpt, "txtLAType", Language.getMessage(mstrModuleName, 16, "Loan/Advance"))
    '        Call ReportFunction.TextChange(objRpt, "txtInstallation", Language.getMessage(mstrModuleName, 17, "Installment"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 18, "Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 19, "Period"))
    '        Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 20, "Status"))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        If mstrReport_GroupName <> "" Then
    '            Call ReportFunction.TextChange(objRpt, "lblTotal1", Language.getMessage(mstrModuleName, 29, "Sub Total : "))
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "lblTotal1", Language.getMessage(mstrModuleName, 30, "Group Total : "))
    '        End If
    '        Call ReportFunction.TextChange(objRpt, "lblTotal2", Language.getMessage(mstrModuleName, 30, "Group Total : "))
    '        Call ReportFunction.TextChange(objRpt, "lblTotal3", Language.getMessage(mstrModuleName, 31, "Grand Total : "))
    '        If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
    '            If IsDBNull(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column81")) = False Then
    '                Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
    '            End If

    '        End If

    '        Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 34, "Year : "))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
    'Nilay (10-Oct-2015) -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Advance")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "In Progress")
            Language.setMessage(mstrModuleName, 4, "On Hold")
            Language.setMessage(mstrModuleName, 5, "Written Off")
            Language.setMessage(mstrModuleName, 6, "Completed")
            Language.setMessage(mstrModuleName, 7, "Employee :")
            Language.setMessage(mstrModuleName, 8, "Loan Scheme :")
            Language.setMessage(mstrModuleName, 9, "Loan Status :")
            Language.setMessage(mstrModuleName, 10, "Loan/Advance :")
            Language.setMessage(mstrModuleName, 11, "Order By :")
            Language.setMessage(mstrModuleName, 12, "Code")
            Language.setMessage(mstrModuleName, 13, "Employee")
            Language.setMessage(mstrModuleName, 14, "Loan Code")
            Language.setMessage(mstrModuleName, 15, "Loan Scheme")
            Language.setMessage(mstrModuleName, 16, "Loan/Advance")
            Language.setMessage(mstrModuleName, 17, "Installment")
            Language.setMessage(mstrModuleName, 18, "Amount")
            Language.setMessage(mstrModuleName, 19, "Period")
            Language.setMessage(mstrModuleName, 20, "Status")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Checked By :")
            Language.setMessage(mstrModuleName, 23, "Approved By :")
            Language.setMessage(mstrModuleName, 24, "Received By :")
            Language.setMessage(mstrModuleName, 25, "Printed By :")
            Language.setMessage(mstrModuleName, 26, "Printed Date :")
            Language.setMessage(mstrModuleName, 27, "Page :")
            Language.setMessage(mstrModuleName, 28, "Voc. No.")
            Language.setMessage(mstrModuleName, 29, "Sub Total :")
            Language.setMessage(mstrModuleName, 30, "Group Total :")
            Language.setMessage(mstrModuleName, 31, "Grand Total :")
            Language.setMessage(mstrModuleName, 32, "From Year :")
            Language.setMessage(mstrModuleName, 33, "To Year :")
            Language.setMessage(mstrModuleName, 34, "Year :")
            Language.setMessage(mstrModuleName, 35, "Future Loan")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
