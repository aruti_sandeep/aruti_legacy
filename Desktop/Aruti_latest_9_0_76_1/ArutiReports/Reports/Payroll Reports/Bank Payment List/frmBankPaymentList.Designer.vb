﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBankPaymentList
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBankPaymentList))
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkEFT_NBC = New System.Windows.Forms.LinkLabel
        Me.lnkEFTPaymentBatchReconciliation = New System.Windows.Forms.LinkLabel
        Me.lnkEFTPaymentBatchApproval = New System.Windows.Forms.LinkLabel
        Me.lnkEFT_NCBA = New System.Windows.Forms.LinkLabel
        Me.lnkEFTBankOfKigali = New System.Windows.Forms.LinkLabel
        Me.lnkEFTGenericCSVPostWeb = New System.Windows.Forms.LinkLabel
        Me.lnkEFTGenericCSV = New System.Windows.Forms.LinkLabel
        Me.lnkDynamicNavisionPaymentPostDB = New System.Windows.Forms.LinkLabel
        Me.lnkDynamicNavisionExport = New System.Windows.Forms.LinkLabel
        Me.lnkEFTStandardCharteredBank_TZ = New System.Windows.Forms.LinkLabel
        Me.lnkEFTCitiBankKenya = New System.Windows.Forms.LinkLabel
        Me.lnkEFTNationalBankKenya = New System.Windows.Forms.LinkLabel
        Me.lnkEFTEquityBankKenya = New System.Windows.Forms.LinkLabel
        Me.lblMembershipRepo = New System.Windows.Forms.Label
        Me.lnkEFTNationalBankMalawiXLSX = New System.Windows.Forms.LinkLabel
        Me.lnkEFT_ABSA_Bank = New System.Windows.Forms.LinkLabel
        Me.lnkEFTStandardCharteredBank_S2B = New System.Windows.Forms.LinkLabel
        Me.lnkMobileMoneyEFTMPesaExport = New System.Windows.Forms.LinkLabel
        Me.lnkEFT_FNB_Bank_Export = New System.Windows.Forms.LinkLabel
        Me.chkShowCompanyLogoOnReport = New System.Windows.Forms.CheckBox
        Me.chkShowCompanyGrpInfo = New System.Windows.Forms.CheckBox
        Me.chkShowSelectedBankInfo = New System.Windows.Forms.CheckBox
        Me.lnkEFTCityBank = New System.Windows.Forms.LinkLabel
        Me.lnkEFTNationalBankMalawi = New System.Windows.Forms.LinkLabel
        Me.pnlOtherSettings = New System.Windows.Forms.Panel
        Me.chkSaveAsTXT_WPS = New System.Windows.Forms.CheckBox
        Me.txtCustomCurrFormat = New System.Windows.Forms.TextBox
        Me.lblCustomCurrFormat = New System.Windows.Forms.Label
        Me.lblAbsentDays = New System.Windows.Forms.Label
        Me.cboAbsentDays = New System.Windows.Forms.ComboBox
        Me.txtPaymentType = New System.Windows.Forms.TextBox
        Me.lblPaymentType = New System.Windows.Forms.Label
        Me.txtIdentityType = New System.Windows.Forms.TextBox
        Me.lblIdentityType = New System.Windows.Forms.Label
        Me.lblExtraIncome = New System.Windows.Forms.Label
        Me.cboExtraIncome = New System.Windows.Forms.ComboBox
        Me.lblSocialSecurity = New System.Windows.Forms.Label
        Me.cboSocialSecurity = New System.Windows.Forms.ComboBox
        Me.lblBasicSalary = New System.Windows.Forms.Label
        Me.cboBasicSalary = New System.Windows.Forms.ComboBox
        Me.cboPresentDays = New System.Windows.Forms.ComboBox
        Me.lblPresentDays = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkOtherSetting = New System.Windows.Forms.LinkLabel
        Me.lnkEFTBarclaysBankExport = New System.Windows.Forms.LinkLabel
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.chkAddresstoEmployeeBank = New System.Windows.Forms.CheckBox
        Me.pnl_ID_Passcode = New System.Windows.Forms.Panel
        Me.lblLink = New System.Windows.Forms.Label
        Me.txtLink = New System.Windows.Forms.TextBox
        Me.objbtnSearchLoanheads = New eZee.Common.eZeeGradientButton
        Me.cboHeads = New System.Windows.Forms.ComboBox
        Me.lblLoanHeads = New System.Windows.Forms.Label
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtPasscode = New System.Windows.Forms.TextBox
        Me.txtID = New System.Windows.Forms.TextBox
        Me.lblPasscode = New System.Windows.Forms.Label
        Me.lblID = New System.Windows.Forms.Label
        Me.chkDefinedSignatory3 = New System.Windows.Forms.CheckBox
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.chkShowBankCode = New System.Windows.Forms.CheckBox
        Me.cboEmployeeName = New System.Windows.Forms.ComboBox
        Me.chkShowBranchCode = New System.Windows.Forms.CheckBox
        Me.chkLetterhead = New System.Windows.Forms.CheckBox
        Me.lblChequeNo = New System.Windows.Forms.Label
        Me.lblCountryName = New System.Windows.Forms.Label
        Me.chkDefinedSignatory2 = New System.Windows.Forms.CheckBox
        Me.lblBankName = New System.Windows.Forms.Label
        Me.cboCompanyAccountNo = New System.Windows.Forms.ComboBox
        Me.lnkBankPaymentLetter = New System.Windows.Forms.LinkLabel
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblPostingDate = New System.Windows.Forms.Label
        Me.cboChequeNo = New System.Windows.Forms.ComboBox
        Me.cboBankName = New System.Windows.Forms.ComboBox
        Me.chkShowSortCode = New System.Windows.Forms.CheckBox
        Me.dtpPostingDate = New System.Windows.Forms.DateTimePicker
        Me.lblCompanyAccountNo = New System.Windows.Forms.Label
        Me.lnkEFT_T24_Web = New System.Windows.Forms.LinkLabel
        Me.chkShowReportHeader = New System.Windows.Forms.CheckBox
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.cboCompanyBranchName = New System.Windows.Forms.ComboBox
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.chkShowPayrollPeriod = New System.Windows.Forms.CheckBox
        Me.lnkEFT_ECO_Bank = New System.Windows.Forms.LinkLabel
        Me.lblCompanyBranchName = New System.Windows.Forms.Label
        Me.lblBranchName = New System.Windows.Forms.Label
        Me.chkShowAccountType = New System.Windows.Forms.CheckBox
        Me.chkSignatory1 = New System.Windows.Forms.CheckBox
        Me.cboCompanyBankName = New System.Windows.Forms.ComboBox
        Me.lnkEFT_FlexCubeRetailGEFU_Export = New System.Windows.Forms.LinkLabel
        Me.chkDefinedSignatory = New System.Windows.Forms.CheckBox
        Me.cboBranchName = New System.Windows.Forms.ComboBox
        Me.lblCompanyBankName = New System.Windows.Forms.Label
        Me.chkEmployeeSign = New System.Windows.Forms.CheckBox
        Me.chkShowEmployeeCode = New System.Windows.Forms.CheckBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.cboReportMode = New System.Windows.Forms.ComboBox
        Me.lnkEFT_Custom_XLS_Export = New System.Windows.Forms.LinkLabel
        Me.lnkEFTCityBankExport = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblReportMode = New System.Windows.Forms.Label
        Me.chkSignatory2 = New System.Windows.Forms.CheckBox
        Me.lnkEFT_CBA_Export = New System.Windows.Forms.LinkLabel
        Me.txtAmountTo = New System.Windows.Forms.TextBox
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lnkEFT_Custom_CSV_Export = New System.Windows.Forms.LinkLabel
        Me.chkShowFNameSeparately = New System.Windows.Forms.CheckBox
        Me.lblAmountTo = New System.Windows.Forms.Label
        Me.lblCutOffAmount = New System.Windows.Forms.Label
        Me.chkSignatory3 = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lnkEFT_EXIM_Export = New System.Windows.Forms.LinkLabel
        Me.chkShowGroupByBankBranch = New System.Windows.Forms.CheckBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.txtCutOffAmount = New eZee.TextBox.NumericTextBox
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.lnkEFT_T24_Export = New System.Windows.Forms.LinkLabel
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboMembershipRepo = New System.Windows.Forms.ComboBox
        Me.lblEmpCode = New System.Windows.Forms.Label
        Me.txtEmpcode = New System.Windows.Forms.TextBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.pnlGenericPanel = New System.Windows.Forms.Panel
        Me.dgBatch = New System.Windows.Forms.DataGridView
        Me.dgColhBatch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPostedData = New System.Windows.Forms.DataGridViewLinkColumn
        Me.dgColhReconciliation = New System.Windows.Forms.DataGridViewLinkColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkEFT_DTB = New System.Windows.Forms.LinkLabel
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.pnlOtherSettings.SuspendLayout()
        Me.pnl_ID_Passcode.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.pnlGenericPanel.SuspendLayout()
        CType(Me.dgBatch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 560)
        Me.NavPanel.Size = New System.Drawing.Size(1234, 55)
        Me.NavPanel.TabIndex = 3
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.AllowDrop = True
        Me.EZeeCollapsibleContainer1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EZeeCollapsibleContainer1.AutoScroll = True
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_DTB)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_NBC)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTPaymentBatchReconciliation)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTPaymentBatchApproval)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_NCBA)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTBankOfKigali)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTGenericCSVPostWeb)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTGenericCSV)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkDynamicNavisionPaymentPostDB)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkDynamicNavisionExport)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTStandardCharteredBank_TZ)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTCitiBankKenya)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTNationalBankKenya)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTEquityBankKenya)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblMembershipRepo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTNationalBankMalawiXLSX)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_ABSA_Bank)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTStandardCharteredBank_S2B)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkMobileMoneyEFTMPesaExport)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_FNB_Bank_Export)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowCompanyLogoOnReport)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowCompanyGrpInfo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowSelectedBankInfo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTCityBank)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTNationalBankMalawi)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.pnlOtherSettings)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkOtherSetting)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTBarclaysBankExport)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblReportType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCondition)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkAnalysisBy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkAddresstoEmployeeBank)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.pnl_ID_Passcode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkDefinedSignatory3)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowBankCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowBranchCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkLetterhead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblChequeNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCountryName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkDefinedSignatory2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCompanyAccountNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkBankPaymentLetter)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.btnSaveSelection)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblPostingDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboChequeNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowSortCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpPostingDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCompanyAccountNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_T24_Web)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowReportHeader)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCountry)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCompanyBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowPayrollPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_ECO_Bank)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCompanyBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowAccountType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkSignatory1)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCompanyBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_FlexCubeRetailGEFU_Export)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkDefinedSignatory)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCompanyBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkEmployeeSign)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowEmployeeCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboReportMode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_Custom_XLS_Export)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFTCityBankExport)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblReportMode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkSignatory2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_CBA_Export)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtAmountTo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboReportType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_Custom_CSV_Export)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowFNameSeparately)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblAmountTo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCutOffAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkSignatory3)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_EXIM_Export)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowGroupByBankBranch)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtCutOffAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkEFT_T24_Export)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboMembershipRepo)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(627, 420)
        Me.EZeeCollapsibleContainer1.TabIndex = 0
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFT_NBC
        '
        Me.lnkEFT_NBC.AccessibleDescription = ""
        Me.lnkEFT_NBC.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_NBC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_NBC.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_NBC.Location = New System.Drawing.Point(29, 475)
        Me.lnkEFT_NBC.Name = "lnkEFT_NBC"
        Me.lnkEFT_NBC.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFT_NBC.TabIndex = 339
        Me.lnkEFT_NBC.TabStop = True
        Me.lnkEFT_NBC.Text = "EFT NBC"
        Me.lnkEFT_NBC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTPaymentBatchReconciliation
        '
        Me.lnkEFTPaymentBatchReconciliation.AccessibleDescription = ""
        Me.lnkEFTPaymentBatchReconciliation.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTPaymentBatchReconciliation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTPaymentBatchReconciliation.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTPaymentBatchReconciliation.Location = New System.Drawing.Point(61, 465)
        Me.lnkEFTPaymentBatchReconciliation.Name = "lnkEFTPaymentBatchReconciliation"
        Me.lnkEFTPaymentBatchReconciliation.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFTPaymentBatchReconciliation.TabIndex = 337
        Me.lnkEFTPaymentBatchReconciliation.TabStop = True
        Me.lnkEFTPaymentBatchReconciliation.Text = "EFT Payment Batch Reconciliation"
        Me.lnkEFTPaymentBatchReconciliation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTPaymentBatchApproval
        '
        Me.lnkEFTPaymentBatchApproval.AccessibleDescription = ""
        Me.lnkEFTPaymentBatchApproval.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTPaymentBatchApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTPaymentBatchApproval.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTPaymentBatchApproval.Location = New System.Drawing.Point(39, 468)
        Me.lnkEFTPaymentBatchApproval.Name = "lnkEFTPaymentBatchApproval"
        Me.lnkEFTPaymentBatchApproval.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFTPaymentBatchApproval.TabIndex = 336
        Me.lnkEFTPaymentBatchApproval.TabStop = True
        Me.lnkEFTPaymentBatchApproval.Text = "EFT Payment Batch Approval"
        Me.lnkEFTPaymentBatchApproval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFT_NCBA
        '
        Me.lnkEFT_NCBA.AccessibleDescription = ""
        Me.lnkEFT_NCBA.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_NCBA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_NCBA.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_NCBA.Location = New System.Drawing.Point(21, 476)
        Me.lnkEFT_NCBA.Name = "lnkEFT_NCBA"
        Me.lnkEFT_NCBA.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFT_NCBA.TabIndex = 334
        Me.lnkEFT_NCBA.TabStop = True
        Me.lnkEFT_NCBA.Text = "EFT NCBA"
        Me.lnkEFT_NCBA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTBankOfKigali
        '
        Me.lnkEFTBankOfKigali.AccessibleDescription = ""
        Me.lnkEFTBankOfKigali.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTBankOfKigali.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTBankOfKigali.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTBankOfKigali.Location = New System.Drawing.Point(21, 460)
        Me.lnkEFTBankOfKigali.Name = "lnkEFTBankOfKigali"
        Me.lnkEFTBankOfKigali.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFTBankOfKigali.TabIndex = 332
        Me.lnkEFTBankOfKigali.TabStop = True
        Me.lnkEFTBankOfKigali.Text = "EFT Bank Of Kigali"
        Me.lnkEFTBankOfKigali.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTGenericCSVPostWeb
        '
        Me.lnkEFTGenericCSVPostWeb.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTGenericCSVPostWeb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTGenericCSVPostWeb.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTGenericCSVPostWeb.Location = New System.Drawing.Point(50, 466)
        Me.lnkEFTGenericCSVPostWeb.Name = "lnkEFTGenericCSVPostWeb"
        Me.lnkEFTGenericCSVPostWeb.Size = New System.Drawing.Size(151, 16)
        Me.lnkEFTGenericCSVPostWeb.TabIndex = 330
        Me.lnkEFTGenericCSVPostWeb.TabStop = True
        Me.lnkEFTGenericCSVPostWeb.Text = "EFT Generic CSV Post To Web"
        Me.lnkEFTGenericCSVPostWeb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTGenericCSV
        '
        Me.lnkEFTGenericCSV.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTGenericCSV.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTGenericCSV.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTGenericCSV.Location = New System.Drawing.Point(50, 450)
        Me.lnkEFTGenericCSV.Name = "lnkEFTGenericCSV"
        Me.lnkEFTGenericCSV.Size = New System.Drawing.Size(140, 16)
        Me.lnkEFTGenericCSV.TabIndex = 328
        Me.lnkEFTGenericCSV.TabStop = True
        Me.lnkEFTGenericCSV.Text = "EFT Generic CSV"
        Me.lnkEFTGenericCSV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkDynamicNavisionPaymentPostDB
        '
        Me.lnkDynamicNavisionPaymentPostDB.BackColor = System.Drawing.Color.Transparent
        Me.lnkDynamicNavisionPaymentPostDB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkDynamicNavisionPaymentPostDB.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkDynamicNavisionPaymentPostDB.Location = New System.Drawing.Point(10, 479)
        Me.lnkDynamicNavisionPaymentPostDB.Name = "lnkDynamicNavisionPaymentPostDB"
        Me.lnkDynamicNavisionPaymentPostDB.Size = New System.Drawing.Size(200, 16)
        Me.lnkDynamicNavisionPaymentPostDB.TabIndex = 326
        Me.lnkDynamicNavisionPaymentPostDB.TabStop = True
        Me.lnkDynamicNavisionPaymentPostDB.Text = "&Dynamic Nav Payment Post to DB..."
        Me.lnkDynamicNavisionPaymentPostDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkDynamicNavisionExport
        '
        Me.lnkDynamicNavisionExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkDynamicNavisionExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkDynamicNavisionExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkDynamicNavisionExport.Location = New System.Drawing.Point(10, 466)
        Me.lnkDynamicNavisionExport.Name = "lnkDynamicNavisionExport"
        Me.lnkDynamicNavisionExport.Size = New System.Drawing.Size(180, 16)
        Me.lnkDynamicNavisionExport.TabIndex = 325
        Me.lnkDynamicNavisionExport.TabStop = True
        Me.lnkDynamicNavisionExport.Text = "&Dynamic Nav Export..."
        Me.lnkDynamicNavisionExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTStandardCharteredBank_TZ
        '
        Me.lnkEFTStandardCharteredBank_TZ.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTStandardCharteredBank_TZ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTStandardCharteredBank_TZ.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTStandardCharteredBank_TZ.Location = New System.Drawing.Point(164, 447)
        Me.lnkEFTStandardCharteredBank_TZ.Name = "lnkEFTStandardCharteredBank_TZ"
        Me.lnkEFTStandardCharteredBank_TZ.Size = New System.Drawing.Size(63, 16)
        Me.lnkEFTStandardCharteredBank_TZ.TabIndex = 323
        Me.lnkEFTStandardCharteredBank_TZ.TabStop = True
        Me.lnkEFTStandardCharteredBank_TZ.Text = "EFT Standard Chartered Bank (TZ)"
        Me.lnkEFTStandardCharteredBank_TZ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTCitiBankKenya
        '
        Me.lnkEFTCitiBankKenya.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTCitiBankKenya.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTCitiBankKenya.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTCitiBankKenya.Location = New System.Drawing.Point(156, 466)
        Me.lnkEFTCitiBankKenya.Name = "lnkEFTCitiBankKenya"
        Me.lnkEFTCitiBankKenya.Size = New System.Drawing.Size(63, 10)
        Me.lnkEFTCitiBankKenya.TabIndex = 321
        Me.lnkEFTCitiBankKenya.TabStop = True
        Me.lnkEFTCitiBankKenya.Text = "EFT Citi Bank Kenya"
        Me.lnkEFTCitiBankKenya.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTNationalBankKenya
        '
        Me.lnkEFTNationalBankKenya.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTNationalBankKenya.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTNationalBankKenya.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTNationalBankKenya.Location = New System.Drawing.Point(173, 449)
        Me.lnkEFTNationalBankKenya.Name = "lnkEFTNationalBankKenya"
        Me.lnkEFTNationalBankKenya.Size = New System.Drawing.Size(63, 10)
        Me.lnkEFTNationalBankKenya.TabIndex = 319
        Me.lnkEFTNationalBankKenya.TabStop = True
        Me.lnkEFTNationalBankKenya.Text = "EFT National Bank Kenya"
        Me.lnkEFTNationalBankKenya.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTEquityBankKenya
        '
        Me.lnkEFTEquityBankKenya.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTEquityBankKenya.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTEquityBankKenya.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTEquityBankKenya.Location = New System.Drawing.Point(156, 431)
        Me.lnkEFTEquityBankKenya.Name = "lnkEFTEquityBankKenya"
        Me.lnkEFTEquityBankKenya.Size = New System.Drawing.Size(63, 10)
        Me.lnkEFTEquityBankKenya.TabIndex = 318
        Me.lnkEFTEquityBankKenya.TabStop = True
        Me.lnkEFTEquityBankKenya.Text = "EFT Equity Bank Kenya"
        Me.lnkEFTEquityBankKenya.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembershipRepo
        '
        Me.lblMembershipRepo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipRepo.Location = New System.Drawing.Point(8, 276)
        Me.lblMembershipRepo.Name = "lblMembershipRepo"
        Me.lblMembershipRepo.Size = New System.Drawing.Size(115, 13)
        Me.lblMembershipRepo.TabIndex = 316
        Me.lblMembershipRepo.Text = "Membership"
        '
        'lnkEFTNationalBankMalawiXLSX
        '
        Me.lnkEFTNationalBankMalawiXLSX.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTNationalBankMalawiXLSX.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTNationalBankMalawiXLSX.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTNationalBankMalawiXLSX.Location = New System.Drawing.Point(413, 436)
        Me.lnkEFTNationalBankMalawiXLSX.Name = "lnkEFTNationalBankMalawiXLSX"
        Me.lnkEFTNationalBankMalawiXLSX.Size = New System.Drawing.Size(154, 16)
        Me.lnkEFTNationalBankMalawiXLSX.TabIndex = 314
        Me.lnkEFTNationalBankMalawiXLSX.TabStop = True
        Me.lnkEFTNationalBankMalawiXLSX.Text = "EFT National Bank Malawi XLSX"
        Me.lnkEFTNationalBankMalawiXLSX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFT_ABSA_Bank
        '
        Me.lnkEFT_ABSA_Bank.AccessibleDescription = ""
        Me.lnkEFT_ABSA_Bank.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_ABSA_Bank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_ABSA_Bank.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_ABSA_Bank.Location = New System.Drawing.Point(411, 425)
        Me.lnkEFT_ABSA_Bank.Name = "lnkEFT_ABSA_Bank"
        Me.lnkEFT_ABSA_Bank.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFT_ABSA_Bank.TabIndex = 312
        Me.lnkEFT_ABSA_Bank.TabStop = True
        Me.lnkEFT_ABSA_Bank.Text = "EFT ABSA Bank"
        Me.lnkEFT_ABSA_Bank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTStandardCharteredBank_S2B
        '
        Me.lnkEFTStandardCharteredBank_S2B.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTStandardCharteredBank_S2B.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTStandardCharteredBank_S2B.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTStandardCharteredBank_S2B.Location = New System.Drawing.Point(146, 461)
        Me.lnkEFTStandardCharteredBank_S2B.Name = "lnkEFTStandardCharteredBank_S2B"
        Me.lnkEFTStandardCharteredBank_S2B.Size = New System.Drawing.Size(63, 16)
        Me.lnkEFTStandardCharteredBank_S2B.TabIndex = 310
        Me.lnkEFTStandardCharteredBank_S2B.TabStop = True
        Me.lnkEFTStandardCharteredBank_S2B.Text = "EFT Standard Chartered Bank(S2B)"
        Me.lnkEFTStandardCharteredBank_S2B.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkMobileMoneyEFTMPesaExport
        '
        Me.lnkMobileMoneyEFTMPesaExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkMobileMoneyEFTMPesaExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkMobileMoneyEFTMPesaExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkMobileMoneyEFTMPesaExport.Location = New System.Drawing.Point(12, 475)
        Me.lnkMobileMoneyEFTMPesaExport.Name = "lnkMobileMoneyEFTMPesaExport"
        Me.lnkMobileMoneyEFTMPesaExport.Size = New System.Drawing.Size(180, 16)
        Me.lnkMobileMoneyEFTMPesaExport.TabIndex = 266
        Me.lnkMobileMoneyEFTMPesaExport.TabStop = True
        Me.lnkMobileMoneyEFTMPesaExport.Text = "EFT &MPesa Export..."
        Me.lnkMobileMoneyEFTMPesaExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFT_FNB_Bank_Export
        '
        Me.lnkEFT_FNB_Bank_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_FNB_Bank_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_FNB_Bank_Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_FNB_Bank_Export.Location = New System.Drawing.Point(70, 475)
        Me.lnkEFT_FNB_Bank_Export.Name = "lnkEFT_FNB_Bank_Export"
        Me.lnkEFT_FNB_Bank_Export.Size = New System.Drawing.Size(58, 16)
        Me.lnkEFT_FNB_Bank_Export.TabIndex = 308
        Me.lnkEFT_FNB_Bank_Export.TabStop = True
        Me.lnkEFT_FNB_Bank_Export.Text = "EFT FNB Bank Export..."
        Me.lnkEFT_FNB_Bank_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowCompanyLogoOnReport
        '
        Me.chkShowCompanyLogoOnReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowCompanyLogoOnReport.Location = New System.Drawing.Point(196, 429)
        Me.chkShowCompanyLogoOnReport.Name = "chkShowCompanyLogoOnReport"
        Me.chkShowCompanyLogoOnReport.Size = New System.Drawing.Size(167, 17)
        Me.chkShowCompanyLogoOnReport.TabIndex = 306
        Me.chkShowCompanyLogoOnReport.Text = "Show Company Logo"
        Me.chkShowCompanyLogoOnReport.UseVisualStyleBackColor = True
        '
        'chkShowCompanyGrpInfo
        '
        Me.chkShowCompanyGrpInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowCompanyGrpInfo.Location = New System.Drawing.Point(196, 450)
        Me.chkShowCompanyGrpInfo.Name = "chkShowCompanyGrpInfo"
        Me.chkShowCompanyGrpInfo.Size = New System.Drawing.Size(167, 17)
        Me.chkShowCompanyGrpInfo.TabIndex = 304
        Me.chkShowCompanyGrpInfo.Text = "Show Company Group Info"
        Me.chkShowCompanyGrpInfo.UseVisualStyleBackColor = True
        '
        'chkShowSelectedBankInfo
        '
        Me.chkShowSelectedBankInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSelectedBankInfo.Location = New System.Drawing.Point(196, 469)
        Me.chkShowSelectedBankInfo.Name = "chkShowSelectedBankInfo"
        Me.chkShowSelectedBankInfo.Size = New System.Drawing.Size(159, 17)
        Me.chkShowSelectedBankInfo.TabIndex = 302
        Me.chkShowSelectedBankInfo.Text = "Show Selected Bank Info"
        Me.chkShowSelectedBankInfo.UseVisualStyleBackColor = True
        '
        'lnkEFTCityBank
        '
        Me.lnkEFTCityBank.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTCityBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTCityBank.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTCityBank.Location = New System.Drawing.Point(160, 449)
        Me.lnkEFTCityBank.Name = "lnkEFTCityBank"
        Me.lnkEFTCityBank.Size = New System.Drawing.Size(140, 16)
        Me.lnkEFTCityBank.TabIndex = 299
        Me.lnkEFTCityBank.TabStop = True
        Me.lnkEFTCityBank.Text = "EFT City Bank"
        Me.lnkEFTCityBank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTNationalBankMalawi
        '
        Me.lnkEFTNationalBankMalawi.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTNationalBankMalawi.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTNationalBankMalawi.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTNationalBankMalawi.Location = New System.Drawing.Point(126, 431)
        Me.lnkEFTNationalBankMalawi.Name = "lnkEFTNationalBankMalawi"
        Me.lnkEFTNationalBankMalawi.Size = New System.Drawing.Size(140, 16)
        Me.lnkEFTNationalBankMalawi.TabIndex = 297
        Me.lnkEFTNationalBankMalawi.TabStop = True
        Me.lnkEFTNationalBankMalawi.Text = "EFT National Bank Malawi"
        Me.lnkEFTNationalBankMalawi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlOtherSettings
        '
        Me.pnlOtherSettings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOtherSettings.Controls.Add(Me.chkSaveAsTXT_WPS)
        Me.pnlOtherSettings.Controls.Add(Me.txtCustomCurrFormat)
        Me.pnlOtherSettings.Controls.Add(Me.lblCustomCurrFormat)
        Me.pnlOtherSettings.Controls.Add(Me.lblAbsentDays)
        Me.pnlOtherSettings.Controls.Add(Me.cboAbsentDays)
        Me.pnlOtherSettings.Controls.Add(Me.txtPaymentType)
        Me.pnlOtherSettings.Controls.Add(Me.lblPaymentType)
        Me.pnlOtherSettings.Controls.Add(Me.txtIdentityType)
        Me.pnlOtherSettings.Controls.Add(Me.lblIdentityType)
        Me.pnlOtherSettings.Controls.Add(Me.lblExtraIncome)
        Me.pnlOtherSettings.Controls.Add(Me.cboExtraIncome)
        Me.pnlOtherSettings.Controls.Add(Me.lblSocialSecurity)
        Me.pnlOtherSettings.Controls.Add(Me.cboSocialSecurity)
        Me.pnlOtherSettings.Controls.Add(Me.lblBasicSalary)
        Me.pnlOtherSettings.Controls.Add(Me.cboBasicSalary)
        Me.pnlOtherSettings.Controls.Add(Me.cboPresentDays)
        Me.pnlOtherSettings.Controls.Add(Me.lblPresentDays)
        Me.pnlOtherSettings.Controls.Add(Me.btnClose)
        Me.pnlOtherSettings.Location = New System.Drawing.Point(159, 105)
        Me.pnlOtherSettings.Name = "pnlOtherSettings"
        Me.pnlOtherSettings.Size = New System.Drawing.Size(309, 283)
        Me.pnlOtherSettings.TabIndex = 295
        Me.pnlOtherSettings.Visible = False
        '
        'chkSaveAsTXT_WPS
        '
        Me.chkSaveAsTXT_WPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSaveAsTXT_WPS.Location = New System.Drawing.Point(122, 219)
        Me.chkSaveAsTXT_WPS.Name = "chkSaveAsTXT_WPS"
        Me.chkSaveAsTXT_WPS.Size = New System.Drawing.Size(167, 17)
        Me.chkSaveAsTXT_WPS.TabIndex = 307
        Me.chkSaveAsTXT_WPS.Text = "Save As TXT"
        Me.chkSaveAsTXT_WPS.UseVisualStyleBackColor = True
        '
        'txtCustomCurrFormat
        '
        Me.txtCustomCurrFormat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomCurrFormat.Location = New System.Drawing.Point(121, 192)
        Me.txtCustomCurrFormat.Name = "txtCustomCurrFormat"
        Me.txtCustomCurrFormat.Size = New System.Drawing.Size(172, 20)
        Me.txtCustomCurrFormat.TabIndex = 234
        '
        'lblCustomCurrFormat
        '
        Me.lblCustomCurrFormat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomCurrFormat.Location = New System.Drawing.Point(2, 196)
        Me.lblCustomCurrFormat.Name = "lblCustomCurrFormat"
        Me.lblCustomCurrFormat.Size = New System.Drawing.Size(113, 13)
        Me.lblCustomCurrFormat.TabIndex = 236
        Me.lblCustomCurrFormat.Text = "Custom Curr. Format"
        '
        'lblAbsentDays
        '
        Me.lblAbsentDays.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbsentDays.Location = New System.Drawing.Point(1, 118)
        Me.lblAbsentDays.Name = "lblAbsentDays"
        Me.lblAbsentDays.Size = New System.Drawing.Size(113, 13)
        Me.lblAbsentDays.TabIndex = 235
        Me.lblAbsentDays.Text = "Absent Deduction"
        '
        'cboAbsentDays
        '
        Me.cboAbsentDays.DropDownWidth = 230
        Me.cboAbsentDays.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAbsentDays.FormattingEnabled = True
        Me.cboAbsentDays.Location = New System.Drawing.Point(121, 114)
        Me.cboAbsentDays.Name = "cboAbsentDays"
        Me.cboAbsentDays.Size = New System.Drawing.Size(172, 21)
        Me.cboAbsentDays.TabIndex = 229
        '
        'txtPaymentType
        '
        Me.txtPaymentType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaymentType.Location = New System.Drawing.Point(121, 166)
        Me.txtPaymentType.Name = "txtPaymentType"
        Me.txtPaymentType.Size = New System.Drawing.Size(172, 20)
        Me.txtPaymentType.TabIndex = 233
        '
        'lblPaymentType
        '
        Me.lblPaymentType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentType.Location = New System.Drawing.Point(2, 170)
        Me.lblPaymentType.Name = "lblPaymentType"
        Me.lblPaymentType.Size = New System.Drawing.Size(113, 13)
        Me.lblPaymentType.TabIndex = 232
        Me.lblPaymentType.Text = "Payment Type"
        '
        'txtIdentityType
        '
        Me.txtIdentityType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdentityType.Location = New System.Drawing.Point(121, 141)
        Me.txtIdentityType.Name = "txtIdentityType"
        Me.txtIdentityType.Size = New System.Drawing.Size(172, 20)
        Me.txtIdentityType.TabIndex = 231
        '
        'lblIdentityType
        '
        Me.lblIdentityType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentityType.Location = New System.Drawing.Point(2, 145)
        Me.lblIdentityType.Name = "lblIdentityType"
        Me.lblIdentityType.Size = New System.Drawing.Size(113, 13)
        Me.lblIdentityType.TabIndex = 230
        Me.lblIdentityType.Text = "Identity Type"
        '
        'lblExtraIncome
        '
        Me.lblExtraIncome.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExtraIncome.Location = New System.Drawing.Point(1, 91)
        Me.lblExtraIncome.Name = "lblExtraIncome"
        Me.lblExtraIncome.Size = New System.Drawing.Size(113, 13)
        Me.lblExtraIncome.TabIndex = 229
        Me.lblExtraIncome.Text = "Extra Income"
        '
        'cboExtraIncome
        '
        Me.cboExtraIncome.DropDownWidth = 230
        Me.cboExtraIncome.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExtraIncome.FormattingEnabled = True
        Me.cboExtraIncome.Location = New System.Drawing.Point(121, 87)
        Me.cboExtraIncome.Name = "cboExtraIncome"
        Me.cboExtraIncome.Size = New System.Drawing.Size(172, 21)
        Me.cboExtraIncome.TabIndex = 228
        '
        'lblSocialSecurity
        '
        Me.lblSocialSecurity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSocialSecurity.Location = New System.Drawing.Point(1, 64)
        Me.lblSocialSecurity.Name = "lblSocialSecurity"
        Me.lblSocialSecurity.Size = New System.Drawing.Size(113, 13)
        Me.lblSocialSecurity.TabIndex = 227
        Me.lblSocialSecurity.Text = "Social Security"
        '
        'cboSocialSecurity
        '
        Me.cboSocialSecurity.DropDownWidth = 230
        Me.cboSocialSecurity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSocialSecurity.FormattingEnabled = True
        Me.cboSocialSecurity.Location = New System.Drawing.Point(121, 60)
        Me.cboSocialSecurity.Name = "cboSocialSecurity"
        Me.cboSocialSecurity.Size = New System.Drawing.Size(172, 21)
        Me.cboSocialSecurity.TabIndex = 2
        '
        'lblBasicSalary
        '
        Me.lblBasicSalary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasicSalary.Location = New System.Drawing.Point(2, 37)
        Me.lblBasicSalary.Name = "lblBasicSalary"
        Me.lblBasicSalary.Size = New System.Drawing.Size(113, 13)
        Me.lblBasicSalary.TabIndex = 225
        Me.lblBasicSalary.Text = "Basic Salary"
        '
        'cboBasicSalary
        '
        Me.cboBasicSalary.DropDownWidth = 230
        Me.cboBasicSalary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBasicSalary.FormattingEnabled = True
        Me.cboBasicSalary.Location = New System.Drawing.Point(121, 33)
        Me.cboBasicSalary.Name = "cboBasicSalary"
        Me.cboBasicSalary.Size = New System.Drawing.Size(172, 21)
        Me.cboBasicSalary.TabIndex = 1
        '
        'cboPresentDays
        '
        Me.cboPresentDays.DropDownWidth = 230
        Me.cboPresentDays.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPresentDays.FormattingEnabled = True
        Me.cboPresentDays.Location = New System.Drawing.Point(121, 6)
        Me.cboPresentDays.Name = "cboPresentDays"
        Me.cboPresentDays.Size = New System.Drawing.Size(172, 21)
        Me.cboPresentDays.TabIndex = 0
        '
        'lblPresentDays
        '
        Me.lblPresentDays.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPresentDays.Location = New System.Drawing.Point(2, 10)
        Me.lblPresentDays.Name = "lblPresentDays"
        Me.lblPresentDays.Size = New System.Drawing.Size(113, 13)
        Me.lblPresentDays.TabIndex = 220
        Me.lblPresentDays.Text = "Present Days"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(182, 243)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(111, 28)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "C&lose"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkOtherSetting
        '
        Me.lnkOtherSetting.BackColor = System.Drawing.Color.Transparent
        Me.lnkOtherSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkOtherSetting.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkOtherSetting.Location = New System.Drawing.Point(290, 433)
        Me.lnkOtherSetting.Name = "lnkOtherSetting"
        Me.lnkOtherSetting.Size = New System.Drawing.Size(115, 16)
        Me.lnkOtherSetting.TabIndex = 294
        Me.lnkOtherSetting.TabStop = True
        Me.lnkOtherSetting.Text = "Other Settings..."
        Me.lnkOtherSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkOtherSetting.Visible = False
        '
        'lnkEFTBarclaysBankExport
        '
        Me.lnkEFTBarclaysBankExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTBarclaysBankExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTBarclaysBankExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTBarclaysBankExport.Location = New System.Drawing.Point(289, 465)
        Me.lnkEFTBarclaysBankExport.Name = "lnkEFTBarclaysBankExport"
        Me.lnkEFTBarclaysBankExport.Size = New System.Drawing.Size(45, 16)
        Me.lnkEFTBarclaysBankExport.TabIndex = 292
        Me.lnkEFTBarclaysBankExport.TabStop = True
        Me.lnkEFTBarclaysBankExport.Text = "EFT Barclays Bank Export..."
        Me.lnkEFTBarclaysBankExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 35)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(115, 13)
        Me.lblReportType.TabIndex = 220
        Me.lblReportType.Text = "Report Type"
        '
        'cboCondition
        '
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.DropDownWidth = 150
        Me.cboCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Location = New System.Drawing.Point(129, 166)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(46, 21)
        Me.cboCondition.TabIndex = 8
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(473, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(94, 17)
        Me.lnkAnalysisBy.TabIndex = 233
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkAddresstoEmployeeBank
        '
        Me.chkAddresstoEmployeeBank.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAddresstoEmployeeBank.Location = New System.Drawing.Point(416, 434)
        Me.chkAddresstoEmployeeBank.Name = "chkAddresstoEmployeeBank"
        Me.chkAddresstoEmployeeBank.Size = New System.Drawing.Size(159, 17)
        Me.chkAddresstoEmployeeBank.TabIndex = 290
        Me.chkAddresstoEmployeeBank.Text = "Address to Employee Bank"
        Me.chkAddresstoEmployeeBank.UseVisualStyleBackColor = True
        '
        'pnl_ID_Passcode
        '
        Me.pnl_ID_Passcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_ID_Passcode.Controls.Add(Me.lblLink)
        Me.pnl_ID_Passcode.Controls.Add(Me.txtLink)
        Me.pnl_ID_Passcode.Controls.Add(Me.objbtnSearchLoanheads)
        Me.pnl_ID_Passcode.Controls.Add(Me.cboHeads)
        Me.pnl_ID_Passcode.Controls.Add(Me.lblLoanHeads)
        Me.pnl_ID_Passcode.Controls.Add(Me.btnOk)
        Me.pnl_ID_Passcode.Controls.Add(Me.btnCancel)
        Me.pnl_ID_Passcode.Controls.Add(Me.txtPasscode)
        Me.pnl_ID_Passcode.Controls.Add(Me.txtID)
        Me.pnl_ID_Passcode.Controls.Add(Me.lblPasscode)
        Me.pnl_ID_Passcode.Controls.Add(Me.lblID)
        Me.pnl_ID_Passcode.Location = New System.Drawing.Point(55, 262)
        Me.pnl_ID_Passcode.Name = "pnl_ID_Passcode"
        Me.pnl_ID_Passcode.Size = New System.Drawing.Size(417, 128)
        Me.pnl_ID_Passcode.TabIndex = 7
        Me.pnl_ID_Passcode.Visible = False
        '
        'lblLink
        '
        Me.lblLink.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLink.Location = New System.Drawing.Point(5, 33)
        Me.lblLink.Name = "lblLink"
        Me.lblLink.Size = New System.Drawing.Size(113, 20)
        Me.lblLink.TabIndex = 223
        Me.lblLink.Text = "T24 Link"
        '
        'txtLink
        '
        Me.txtLink.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLink.Location = New System.Drawing.Point(120, 33)
        Me.txtLink.Name = "txtLink"
        Me.txtLink.Size = New System.Drawing.Size(283, 21)
        Me.txtLink.TabIndex = 1
        '
        'objbtnSearchLoanheads
        '
        Me.objbtnSearchLoanheads.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanheads.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanheads.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanheads.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLoanheads.BorderSelected = False
        Me.objbtnSearchLoanheads.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLoanheads.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLoanheads.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLoanheads.Location = New System.Drawing.Point(249, 6)
        Me.objbtnSearchLoanheads.Name = "objbtnSearchLoanheads"
        Me.objbtnSearchLoanheads.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLoanheads.TabIndex = 221
        '
        'cboHeads
        '
        Me.cboHeads.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeads.DropDownWidth = 230
        Me.cboHeads.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeads.FormattingEnabled = True
        Me.cboHeads.Location = New System.Drawing.Point(121, 6)
        Me.cboHeads.Name = "cboHeads"
        Me.cboHeads.Size = New System.Drawing.Size(123, 21)
        Me.cboHeads.TabIndex = 0
        '
        'lblLoanHeads
        '
        Me.lblLoanHeads.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanHeads.Location = New System.Drawing.Point(2, 10)
        Me.lblLoanHeads.Name = "lblLoanHeads"
        Me.lblLoanHeads.Size = New System.Drawing.Size(103, 13)
        Me.lblLoanHeads.TabIndex = 220
        Me.lblLoanHeads.Text = "Loan Head"
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(285, 60)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(124, 24)
        Me.btnOk.TabIndex = 4
        Me.btnOk.Text = "&OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(285, 90)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(124, 24)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'txtPasscode
        '
        Me.txtPasscode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasscode.Location = New System.Drawing.Point(121, 86)
        Me.txtPasscode.Name = "txtPasscode"
        Me.txtPasscode.Size = New System.Drawing.Size(150, 21)
        Me.txtPasscode.TabIndex = 3
        Me.txtPasscode.UseSystemPasswordChar = True
        '
        'txtID
        '
        Me.txtID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtID.Location = New System.Drawing.Point(121, 60)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(150, 21)
        Me.txtID.TabIndex = 2
        '
        'lblPasscode
        '
        Me.lblPasscode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPasscode.Location = New System.Drawing.Point(2, 86)
        Me.lblPasscode.Name = "lblPasscode"
        Me.lblPasscode.Size = New System.Drawing.Size(113, 20)
        Me.lblPasscode.TabIndex = 1
        Me.lblPasscode.Text = "Pass Code"
        '
        'lblID
        '
        Me.lblID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblID.Location = New System.Drawing.Point(2, 63)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(113, 20)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID"
        '
        'chkDefinedSignatory3
        '
        Me.chkDefinedSignatory3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDefinedSignatory3.Location = New System.Drawing.Point(416, 345)
        Me.chkDefinedSignatory3.Name = "chkDefinedSignatory3"
        Me.chkDefinedSignatory3.Size = New System.Drawing.Size(159, 17)
        Me.chkDefinedSignatory3.TabIndex = 25
        Me.chkDefinedSignatory3.Text = "Show Defined Signatory 3"
        Me.chkDefinedSignatory3.UseVisualStyleBackColor = True
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 62)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(115, 13)
        Me.lblEmployeeName.TabIndex = 0
        Me.lblEmployeeName.Text = "Emp. Name"
        '
        'chkShowBankCode
        '
        Me.chkShowBankCode.Checked = True
        Me.chkShowBankCode.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowBankCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBankCode.Location = New System.Drawing.Point(416, 366)
        Me.chkShowBankCode.Name = "chkShowBankCode"
        Me.chkShowBankCode.Size = New System.Drawing.Size(159, 17)
        Me.chkShowBankCode.TabIndex = 28
        Me.chkShowBankCode.Text = "Show Bank Code"
        Me.chkShowBankCode.UseVisualStyleBackColor = True
        '
        'cboEmployeeName
        '
        Me.cboEmployeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeName.DropDownWidth = 150
        Me.cboEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeName.FormattingEnabled = True
        Me.cboEmployeeName.Location = New System.Drawing.Point(129, 58)
        Me.cboEmployeeName.Name = "cboEmployeeName"
        Me.cboEmployeeName.Size = New System.Drawing.Size(411, 21)
        Me.cboEmployeeName.TabIndex = 1
        '
        'chkShowBranchCode
        '
        Me.chkShowBranchCode.Checked = True
        Me.chkShowBranchCode.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowBranchCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBranchCode.Location = New System.Drawing.Point(416, 389)
        Me.chkShowBranchCode.Name = "chkShowBranchCode"
        Me.chkShowBranchCode.Size = New System.Drawing.Size(159, 17)
        Me.chkShowBranchCode.TabIndex = 31
        Me.chkShowBranchCode.Text = "Show Branch Code"
        Me.chkShowBranchCode.UseVisualStyleBackColor = True
        '
        'chkLetterhead
        '
        Me.chkLetterhead.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLetterhead.Location = New System.Drawing.Point(11, 434)
        Me.chkLetterhead.Name = "chkLetterhead"
        Me.chkLetterhead.Size = New System.Drawing.Size(272, 17)
        Me.chkLetterhead.TabIndex = 289
        Me.chkLetterhead.Text = "Letterhead"
        Me.chkLetterhead.UseVisualStyleBackColor = True
        '
        'lblChequeNo
        '
        Me.lblChequeNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChequeNo.Location = New System.Drawing.Point(289, 143)
        Me.lblChequeNo.Name = "lblChequeNo"
        Me.lblChequeNo.Size = New System.Drawing.Size(117, 13)
        Me.lblChequeNo.TabIndex = 241
        Me.lblChequeNo.Text = "Cheque No."
        '
        'lblCountryName
        '
        Me.lblCountryName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountryName.Location = New System.Drawing.Point(8, 197)
        Me.lblCountryName.Name = "lblCountryName"
        Me.lblCountryName.Size = New System.Drawing.Size(115, 13)
        Me.lblCountryName.TabIndex = 5
        Me.lblCountryName.Text = "Country Name"
        '
        'chkDefinedSignatory2
        '
        Me.chkDefinedSignatory2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDefinedSignatory2.Location = New System.Drawing.Point(416, 322)
        Me.chkDefinedSignatory2.Name = "chkDefinedSignatory2"
        Me.chkDefinedSignatory2.Size = New System.Drawing.Size(159, 17)
        Me.chkDefinedSignatory2.TabIndex = 22
        Me.chkDefinedSignatory2.Text = "Show Defined Signatory 2"
        Me.chkDefinedSignatory2.UseVisualStyleBackColor = True
        '
        'lblBankName
        '
        Me.lblBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankName.Location = New System.Drawing.Point(8, 170)
        Me.lblBankName.Name = "lblBankName"
        Me.lblBankName.Size = New System.Drawing.Size(115, 13)
        Me.lblBankName.TabIndex = 7
        Me.lblBankName.Text = "Emp. Bank Name"
        '
        'cboCompanyAccountNo
        '
        Me.cboCompanyAccountNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyAccountNo.DropDownWidth = 230
        Me.cboCompanyAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyAccountNo.FormattingEnabled = True
        Me.cboCompanyAccountNo.Location = New System.Drawing.Point(129, 112)
        Me.cboCompanyAccountNo.Name = "cboCompanyAccountNo"
        Me.cboCompanyAccountNo.Size = New System.Drawing.Size(154, 21)
        Me.cboCompanyAccountNo.TabIndex = 4
        '
        'lnkBankPaymentLetter
        '
        Me.lnkBankPaymentLetter.AutoSize = True
        Me.lnkBankPaymentLetter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkBankPaymentLetter.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkBankPaymentLetter.Location = New System.Drawing.Point(314, 475)
        Me.lnkBankPaymentLetter.Name = "lnkBankPaymentLetter"
        Me.lnkBankPaymentLetter.Size = New System.Drawing.Size(107, 13)
        Me.lnkBankPaymentLetter.TabIndex = 287
        Me.lnkBankPaymentLetter.TabStop = True
        Me.lnkBankPaymentLetter.Text = "Bank Payment Letter"
        Me.lnkBankPaymentLetter.Visible = False
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(392, 455)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 253
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lblPostingDate
        '
        Me.lblPostingDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostingDate.Location = New System.Drawing.Point(289, 249)
        Me.lblPostingDate.Name = "lblPostingDate"
        Me.lblPostingDate.Size = New System.Drawing.Size(117, 13)
        Me.lblPostingDate.TabIndex = 279
        Me.lblPostingDate.Text = "Posting Date"
        '
        'cboChequeNo
        '
        Me.cboChequeNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChequeNo.DropDownWidth = 230
        Me.cboChequeNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChequeNo.FormattingEnabled = True
        Me.cboChequeNo.Location = New System.Drawing.Point(412, 139)
        Me.cboChequeNo.Name = "cboChequeNo"
        Me.cboChequeNo.Size = New System.Drawing.Size(128, 21)
        Me.cboChequeNo.TabIndex = 7
        '
        'cboBankName
        '
        Me.cboBankName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankName.DropDownWidth = 230
        Me.cboBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankName.FormattingEnabled = True
        Me.cboBankName.Location = New System.Drawing.Point(181, 166)
        Me.cboBankName.Name = "cboBankName"
        Me.cboBankName.Size = New System.Drawing.Size(102, 21)
        Me.cboBankName.TabIndex = 9
        '
        'chkShowSortCode
        '
        Me.chkShowSortCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSortCode.Location = New System.Drawing.Point(196, 389)
        Me.chkShowSortCode.Name = "chkShowSortCode"
        Me.chkShowSortCode.Size = New System.Drawing.Size(214, 16)
        Me.chkShowSortCode.TabIndex = 30
        Me.chkShowSortCode.Text = "Show Sort Code"
        Me.chkShowSortCode.UseVisualStyleBackColor = True
        '
        'dtpPostingDate
        '
        Me.dtpPostingDate.Checked = False
        Me.dtpPostingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPostingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPostingDate.Location = New System.Drawing.Point(412, 245)
        Me.dtpPostingDate.Name = "dtpPostingDate"
        Me.dtpPostingDate.ShowCheckBox = True
        Me.dtpPostingDate.Size = New System.Drawing.Size(126, 21)
        Me.dtpPostingDate.TabIndex = 16
        '
        'lblCompanyAccountNo
        '
        Me.lblCompanyAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyAccountNo.Location = New System.Drawing.Point(8, 116)
        Me.lblCompanyAccountNo.Name = "lblCompanyAccountNo"
        Me.lblCompanyAccountNo.Size = New System.Drawing.Size(115, 13)
        Me.lblCompanyAccountNo.TabIndex = 231
        Me.lblCompanyAccountNo.Text = "Company Account No."
        '
        'lnkEFT_T24_Web
        '
        Me.lnkEFT_T24_Web.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_T24_Web.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_T24_Web.Location = New System.Drawing.Point(316, 464)
        Me.lnkEFT_T24_Web.Name = "lnkEFT_T24_Web"
        Me.lnkEFT_T24_Web.Size = New System.Drawing.Size(75, 13)
        Me.lnkEFT_T24_Web.TabIndex = 285
        Me.lnkEFT_T24_Web.TabStop = True
        Me.lnkEFT_T24_Web.Text = "EFT T24 WEB"
        Me.lnkEFT_T24_Web.Visible = False
        '
        'chkShowReportHeader
        '
        Me.chkShowReportHeader.Checked = True
        Me.chkShowReportHeader.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowReportHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReportHeader.Location = New System.Drawing.Point(416, 411)
        Me.chkShowReportHeader.Name = "chkShowReportHeader"
        Me.chkShowReportHeader.Size = New System.Drawing.Size(159, 17)
        Me.chkShowReportHeader.TabIndex = 258
        Me.chkShowReportHeader.Text = "Show Report Header"
        Me.chkShowReportHeader.UseVisualStyleBackColor = True
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.DropDownWidth = 230
        Me.cboCountry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(129, 193)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(154, 21)
        Me.cboCountry.TabIndex = 11
        '
        'cboCompanyBranchName
        '
        Me.cboCompanyBranchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyBranchName.DropDownWidth = 230
        Me.cboCompanyBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyBranchName.FormattingEnabled = True
        Me.cboCompanyBranchName.Location = New System.Drawing.Point(412, 85)
        Me.cboCompanyBranchName.Name = "cboCompanyBranchName"
        Me.cboCompanyBranchName.Size = New System.Drawing.Size(128, 21)
        Me.cboCompanyBranchName.TabIndex = 3
        '
        'txtAmount
        '
        Me.txtAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(129, 220)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(154, 20)
        Me.txtAmount.TabIndex = 13
        '
        'chkShowPayrollPeriod
        '
        Me.chkShowPayrollPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPayrollPeriod.Location = New System.Drawing.Point(11, 389)
        Me.chkShowPayrollPeriod.Name = "chkShowPayrollPeriod"
        Me.chkShowPayrollPeriod.Size = New System.Drawing.Size(128, 17)
        Me.chkShowPayrollPeriod.TabIndex = 29
        Me.chkShowPayrollPeriod.Text = "Show Payroll Period"
        Me.chkShowPayrollPeriod.UseVisualStyleBackColor = True
        '
        'lnkEFT_ECO_Bank
        '
        Me.lnkEFT_ECO_Bank.AutoSize = True
        Me.lnkEFT_ECO_Bank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_ECO_Bank.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_ECO_Bank.Location = New System.Drawing.Point(289, 479)
        Me.lnkEFT_ECO_Bank.Name = "lnkEFT_ECO_Bank"
        Me.lnkEFT_ECO_Bank.Size = New System.Drawing.Size(75, 13)
        Me.lnkEFT_ECO_Bank.TabIndex = 281
        Me.lnkEFT_ECO_Bank.TabStop = True
        Me.lnkEFT_ECO_Bank.Text = "EFT ECO Bank"
        Me.lnkEFT_ECO_Bank.Visible = False
        '
        'lblCompanyBranchName
        '
        Me.lblCompanyBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyBranchName.Location = New System.Drawing.Point(289, 89)
        Me.lblCompanyBranchName.Name = "lblCompanyBranchName"
        Me.lblCompanyBranchName.Size = New System.Drawing.Size(117, 13)
        Me.lblCompanyBranchName.TabIndex = 229
        Me.lblCompanyBranchName.Text = "Company Bank Branch"
        '
        'lblBranchName
        '
        Me.lblBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranchName.Location = New System.Drawing.Point(289, 170)
        Me.lblBranchName.Name = "lblBranchName"
        Me.lblBranchName.Size = New System.Drawing.Size(117, 13)
        Me.lblBranchName.TabIndex = 9
        Me.lblBranchName.Text = "Branch Name"
        '
        'chkShowAccountType
        '
        Me.chkShowAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowAccountType.Location = New System.Drawing.Point(196, 366)
        Me.chkShowAccountType.Name = "chkShowAccountType"
        Me.chkShowAccountType.Size = New System.Drawing.Size(209, 16)
        Me.chkShowAccountType.TabIndex = 27
        Me.chkShowAccountType.Text = "Show Account Type"
        Me.chkShowAccountType.UseVisualStyleBackColor = True
        '
        'chkSignatory1
        '
        Me.chkSignatory1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSignatory1.Location = New System.Drawing.Point(11, 300)
        Me.chkSignatory1.Name = "chkSignatory1"
        Me.chkSignatory1.Size = New System.Drawing.Size(128, 17)
        Me.chkSignatory1.TabIndex = 17
        Me.chkSignatory1.Text = "Show Signatory 1"
        Me.chkSignatory1.UseVisualStyleBackColor = True
        '
        'cboCompanyBankName
        '
        Me.cboCompanyBankName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyBankName.DropDownWidth = 230
        Me.cboCompanyBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyBankName.FormattingEnabled = True
        Me.cboCompanyBankName.Location = New System.Drawing.Point(129, 85)
        Me.cboCompanyBankName.Name = "cboCompanyBankName"
        Me.cboCompanyBankName.Size = New System.Drawing.Size(154, 21)
        Me.cboCompanyBankName.TabIndex = 2
        '
        'lnkEFT_FlexCubeRetailGEFU_Export
        '
        Me.lnkEFT_FlexCubeRetailGEFU_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_FlexCubeRetailGEFU_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_FlexCubeRetailGEFU_Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_FlexCubeRetailGEFU_Export.Location = New System.Drawing.Point(225, 476)
        Me.lnkEFT_FlexCubeRetailGEFU_Export.Name = "lnkEFT_FlexCubeRetailGEFU_Export"
        Me.lnkEFT_FlexCubeRetailGEFU_Export.Size = New System.Drawing.Size(58, 16)
        Me.lnkEFT_FlexCubeRetailGEFU_Export.TabIndex = 276
        Me.lnkEFT_FlexCubeRetailGEFU_Export.TabStop = True
        Me.lnkEFT_FlexCubeRetailGEFU_Export.Text = "EFT Flex Cube Retail - GEFU Export..."
        Me.lnkEFT_FlexCubeRetailGEFU_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDefinedSignatory
        '
        Me.chkDefinedSignatory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDefinedSignatory.Location = New System.Drawing.Point(416, 300)
        Me.chkDefinedSignatory.Name = "chkDefinedSignatory"
        Me.chkDefinedSignatory.Size = New System.Drawing.Size(159, 17)
        Me.chkDefinedSignatory.TabIndex = 19
        Me.chkDefinedSignatory.Text = "Show Defined Signatory"
        Me.chkDefinedSignatory.UseVisualStyleBackColor = True
        '
        'cboBranchName
        '
        Me.cboBranchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranchName.DropDownWidth = 230
        Me.cboBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranchName.FormattingEnabled = True
        Me.cboBranchName.Location = New System.Drawing.Point(412, 166)
        Me.cboBranchName.Name = "cboBranchName"
        Me.cboBranchName.Size = New System.Drawing.Size(128, 21)
        Me.cboBranchName.TabIndex = 10
        '
        'lblCompanyBankName
        '
        Me.lblCompanyBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyBankName.Location = New System.Drawing.Point(8, 89)
        Me.lblCompanyBankName.Name = "lblCompanyBankName"
        Me.lblCompanyBankName.Size = New System.Drawing.Size(115, 13)
        Me.lblCompanyBankName.TabIndex = 226
        Me.lblCompanyBankName.Text = "Company Bank Name"
        '
        'chkEmployeeSign
        '
        Me.chkEmployeeSign.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmployeeSign.Location = New System.Drawing.Point(196, 322)
        Me.chkEmployeeSign.Name = "chkEmployeeSign"
        Me.chkEmployeeSign.Size = New System.Drawing.Size(214, 17)
        Me.chkEmployeeSign.TabIndex = 21
        Me.chkEmployeeSign.Text = "Show Employee Sign"
        Me.chkEmployeeSign.UseVisualStyleBackColor = True
        '
        'chkShowEmployeeCode
        '
        Me.chkShowEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmployeeCode.Location = New System.Drawing.Point(11, 369)
        Me.chkShowEmployeeCode.Name = "chkShowEmployeeCode"
        Me.chkShowEmployeeCode.Size = New System.Drawing.Size(181, 16)
        Me.chkShowEmployeeCode.TabIndex = 26
        Me.chkShowEmployeeCode.Text = "Show Employee Code"
        Me.chkShowEmployeeCode.UseVisualStyleBackColor = True
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(8, 224)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(115, 13)
        Me.lblAmount.TabIndex = 11
        Me.lblAmount.Text = "Amount"
        '
        'cboReportMode
        '
        Me.cboReportMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportMode.DropDownWidth = 230
        Me.cboReportMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportMode.FormattingEnabled = True
        Me.cboReportMode.Location = New System.Drawing.Point(412, 112)
        Me.cboReportMode.Name = "cboReportMode"
        Me.cboReportMode.Size = New System.Drawing.Size(128, 21)
        Me.cboReportMode.TabIndex = 5
        '
        'lnkEFT_Custom_XLS_Export
        '
        Me.lnkEFT_Custom_XLS_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_Custom_XLS_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_Custom_XLS_Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_Custom_XLS_Export.Location = New System.Drawing.Point(191, 476)
        Me.lnkEFT_Custom_XLS_Export.Name = "lnkEFT_Custom_XLS_Export"
        Me.lnkEFT_Custom_XLS_Export.Size = New System.Drawing.Size(45, 16)
        Me.lnkEFT_Custom_XLS_Export.TabIndex = 274
        Me.lnkEFT_Custom_XLS_Export.TabStop = True
        Me.lnkEFT_Custom_XLS_Export.Text = "EFT Custom XLS Export..."
        Me.lnkEFT_Custom_XLS_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFTCityBankExport
        '
        Me.lnkEFTCityBankExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFTCityBankExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFTCityBankExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFTCityBankExport.Location = New System.Drawing.Point(12, 456)
        Me.lnkEFTCityBankExport.Name = "lnkEFTCityBankExport"
        Me.lnkEFTCityBankExport.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFTCityBankExport.TabIndex = 237
        Me.lnkEFTCityBankExport.TabStop = True
        Me.lnkEFTCityBankExport.Text = "&EFT City Direct Export..."
        Me.lnkEFTCityBankExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(546, 58)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'lblReportMode
        '
        Me.lblReportMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportMode.Location = New System.Drawing.Point(289, 116)
        Me.lblReportMode.Name = "lblReportMode"
        Me.lblReportMode.Size = New System.Drawing.Size(117, 13)
        Me.lblReportMode.TabIndex = 223
        Me.lblReportMode.Text = "Report Mode"
        '
        'chkSignatory2
        '
        Me.chkSignatory2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSignatory2.Location = New System.Drawing.Point(11, 323)
        Me.chkSignatory2.Name = "chkSignatory2"
        Me.chkSignatory2.Size = New System.Drawing.Size(128, 17)
        Me.chkSignatory2.TabIndex = 20
        Me.chkSignatory2.Text = "Show Signatory 2"
        Me.chkSignatory2.UseVisualStyleBackColor = True
        '
        'lnkEFT_CBA_Export
        '
        Me.lnkEFT_CBA_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_CBA_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_CBA_Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_CBA_Export.Location = New System.Drawing.Point(17, 476)
        Me.lnkEFT_CBA_Export.Name = "lnkEFT_CBA_Export"
        Me.lnkEFT_CBA_Export.Size = New System.Drawing.Size(58, 16)
        Me.lnkEFT_CBA_Export.TabIndex = 264
        Me.lnkEFT_CBA_Export.TabStop = True
        Me.lnkEFT_CBA_Export.Text = "EFT CBA Export..."
        Me.lnkEFT_CBA_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAmountTo
        '
        Me.txtAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountTo.Location = New System.Drawing.Point(412, 220)
        Me.txtAmountTo.Name = "txtAmountTo"
        Me.txtAmountTo.Size = New System.Drawing.Size(128, 20)
        Me.txtAmountTo.TabIndex = 14
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 150
        Me.cboReportType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(129, 31)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(411, 21)
        Me.cboReportType.TabIndex = 0
        '
        'lnkEFT_Custom_CSV_Export
        '
        Me.lnkEFT_Custom_CSV_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_Custom_CSV_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_Custom_CSV_Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_Custom_CSV_Export.Location = New System.Drawing.Point(164, 476)
        Me.lnkEFT_Custom_CSV_Export.Name = "lnkEFT_Custom_CSV_Export"
        Me.lnkEFT_Custom_CSV_Export.Size = New System.Drawing.Size(45, 16)
        Me.lnkEFT_Custom_CSV_Export.TabIndex = 272
        Me.lnkEFT_Custom_CSV_Export.TabStop = True
        Me.lnkEFT_Custom_CSV_Export.Text = "EFT Custom CSV Export..."
        Me.lnkEFT_Custom_CSV_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowFNameSeparately
        '
        Me.chkShowFNameSeparately.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowFNameSeparately.Location = New System.Drawing.Point(11, 411)
        Me.chkShowFNameSeparately.Name = "chkShowFNameSeparately"
        Me.chkShowFNameSeparately.Size = New System.Drawing.Size(344, 17)
        Me.chkShowFNameSeparately.TabIndex = 32
        Me.chkShowFNameSeparately.Text = "Show First Name, Other Name and Surname in Separate Columns"
        Me.chkShowFNameSeparately.UseVisualStyleBackColor = True
        '
        'lblAmountTo
        '
        Me.lblAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountTo.Location = New System.Drawing.Point(289, 224)
        Me.lblAmountTo.Name = "lblAmountTo"
        Me.lblAmountTo.Size = New System.Drawing.Size(117, 13)
        Me.lblAmountTo.TabIndex = 13
        Me.lblAmountTo.Text = "To"
        '
        'lblCutOffAmount
        '
        Me.lblCutOffAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCutOffAmount.Location = New System.Drawing.Point(8, 249)
        Me.lblCutOffAmount.Name = "lblCutOffAmount"
        Me.lblCutOffAmount.Size = New System.Drawing.Size(115, 13)
        Me.lblCutOffAmount.TabIndex = 256
        Me.lblCutOffAmount.Text = "Cut Off Amount"
        '
        'chkSignatory3
        '
        Me.chkSignatory3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSignatory3.Location = New System.Drawing.Point(11, 346)
        Me.chkSignatory3.Name = "chkSignatory3"
        Me.chkSignatory3.Size = New System.Drawing.Size(128, 17)
        Me.chkSignatory3.TabIndex = 23
        Me.chkSignatory3.Text = "Show Signatory 3"
        Me.chkSignatory3.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 143)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(115, 13)
        Me.lblPeriod.TabIndex = 5
        Me.lblPeriod.Text = "Period"
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(412, 193)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(128, 21)
        Me.cboCurrency.TabIndex = 12
        '
        'lnkEFT_EXIM_Export
        '
        Me.lnkEFT_EXIM_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_EXIM_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_EXIM_Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_EXIM_Export.Location = New System.Drawing.Point(134, 479)
        Me.lnkEFT_EXIM_Export.Name = "lnkEFT_EXIM_Export"
        Me.lnkEFT_EXIM_Export.Size = New System.Drawing.Size(58, 16)
        Me.lnkEFT_EXIM_Export.TabIndex = 270
        Me.lnkEFT_EXIM_Export.TabStop = True
        Me.lnkEFT_EXIM_Export.Text = "EFT EXIM Export..."
        Me.lnkEFT_EXIM_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowGroupByBankBranch
        '
        Me.chkShowGroupByBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowGroupByBankBranch.Location = New System.Drawing.Point(196, 300)
        Me.chkShowGroupByBankBranch.Name = "chkShowGroupByBankBranch"
        Me.chkShowGroupByBankBranch.Size = New System.Drawing.Size(214, 16)
        Me.chkShowGroupByBankBranch.TabIndex = 18
        Me.chkShowGroupByBankBranch.Text = "Show Group By Bank / Branch"
        Me.chkShowGroupByBankBranch.UseVisualStyleBackColor = True
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(129, 139)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(154, 21)
        Me.cboPeriod.TabIndex = 6
        '
        'txtCutOffAmount
        '
        Me.txtCutOffAmount.AllowNegative = True
        Me.txtCutOffAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCutOffAmount.DigitsInGroup = 0
        Me.txtCutOffAmount.Flags = 0
        Me.txtCutOffAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCutOffAmount.Location = New System.Drawing.Point(129, 245)
        Me.txtCutOffAmount.MaxDecimalPlaces = 4
        Me.txtCutOffAmount.MaxWholeDigits = 9
        Me.txtCutOffAmount.Name = "txtCutOffAmount"
        Me.txtCutOffAmount.Prefix = ""
        Me.txtCutOffAmount.RangeMax = 1.7976931348623157E+308
        Me.txtCutOffAmount.RangeMin = -1.7976931348623157E+308
        Me.txtCutOffAmount.Size = New System.Drawing.Size(154, 21)
        Me.txtCutOffAmount.TabIndex = 15
        Me.txtCutOffAmount.Text = "0"
        Me.txtCutOffAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Checked = True
        Me.chkIncludeInactiveEmp.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(196, 345)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(214, 17)
        Me.chkIncludeInactiveEmp.TabIndex = 24
        Me.chkIncludeInactiveEmp.Text = "Include inactive employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        Me.chkIncludeInactiveEmp.Visible = False
        '
        'lnkEFT_T24_Export
        '
        Me.lnkEFT_T24_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_T24_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_T24_Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_T24_Export.Location = New System.Drawing.Point(81, 476)
        Me.lnkEFT_T24_Export.Name = "lnkEFT_T24_Export"
        Me.lnkEFT_T24_Export.Size = New System.Drawing.Size(58, 16)
        Me.lnkEFT_T24_Export.TabIndex = 268
        Me.lnkEFT_T24_Export.TabStop = True
        Me.lnkEFT_T24_Export.Text = "EFT T24 Export..."
        Me.lnkEFT_T24_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(289, 197)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(117, 13)
        Me.lblCurrency.TabIndex = 218
        Me.lblCurrency.Text = "Paid Currency"
        '
        'cboMembershipRepo
        '
        Me.cboMembershipRepo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembershipRepo.DropDownWidth = 230
        Me.cboMembershipRepo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembershipRepo.FormattingEnabled = True
        Me.cboMembershipRepo.Location = New System.Drawing.Point(129, 272)
        Me.cboMembershipRepo.Name = "cboMembershipRepo"
        Me.cboMembershipRepo.Size = New System.Drawing.Size(154, 21)
        Me.cboMembershipRepo.TabIndex = 317
        '
        'lblEmpCode
        '
        Me.lblEmpCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCode.Location = New System.Drawing.Point(645, 346)
        Me.lblEmpCode.Name = "lblEmpCode"
        Me.lblEmpCode.Size = New System.Drawing.Size(78, 13)
        Me.lblEmpCode.TabIndex = 3
        Me.lblEmpCode.Text = "Emp.  Code"
        Me.lblEmpCode.Visible = False
        '
        'txtEmpcode
        '
        Me.txtEmpcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpcode.Location = New System.Drawing.Point(646, 366)
        Me.txtEmpcode.Name = "txtEmpcode"
        Me.txtEmpcode.Size = New System.Drawing.Size(67, 20)
        Me.txtEmpcode.TabIndex = 6
        Me.txtEmpcode.Visible = False
        '
        'gbSortBy
        '
        Me.gbSortBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 489)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 61
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(627, 68)
        Me.gbSortBy.TabIndex = 2
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(94, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(446, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(546, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(80, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'pnlGenericPanel
        '
        Me.pnlGenericPanel.AutoScroll = True
        Me.pnlGenericPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlGenericPanel.Controls.Add(Me.dgBatch)
        Me.pnlGenericPanel.Location = New System.Drawing.Point(648, 66)
        Me.pnlGenericPanel.Name = "pnlGenericPanel"
        Me.pnlGenericPanel.Size = New System.Drawing.Size(574, 294)
        Me.pnlGenericPanel.TabIndex = 333
        Me.pnlGenericPanel.Visible = False
        '
        'dgBatch
        '
        Me.dgBatch.AllowUserToAddRows = False
        Me.dgBatch.AllowUserToDeleteRows = False
        Me.dgBatch.AllowUserToResizeRows = False
        Me.dgBatch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgBatch.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgBatch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgBatch.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgBatch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgBatch.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgColhBatch, Me.dgColhStatus, Me.dgColhPostedData, Me.dgColhReconciliation})
        Me.dgBatch.Location = New System.Drawing.Point(3, 7)
        Me.dgBatch.Name = "dgBatch"
        Me.dgBatch.RowHeadersVisible = False
        Me.dgBatch.RowHeadersWidth = 5
        Me.dgBatch.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgBatch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgBatch.Size = New System.Drawing.Size(566, 278)
        Me.dgBatch.TabIndex = 224
        '
        'dgColhBatch
        '
        Me.dgColhBatch.HeaderText = "Batch"
        Me.dgColhBatch.Name = "dgColhBatch"
        Me.dgColhBatch.ReadOnly = True
        Me.dgColhBatch.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgColhStatus
        '
        Me.dgColhStatus.HeaderText = "Status"
        Me.dgColhStatus.Name = "dgColhStatus"
        Me.dgColhStatus.ReadOnly = True
        Me.dgColhStatus.Width = 200
        '
        'dgColhPostedData
        '
        Me.dgColhPostedData.HeaderText = "Posted Data"
        Me.dgColhPostedData.Name = "dgColhPostedData"
        Me.dgColhPostedData.ReadOnly = True
        Me.dgColhPostedData.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgColhPostedData.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgColhReconciliation
        '
        Me.dgColhReconciliation.HeaderText = "Reconciliation"
        Me.dgColhReconciliation.Name = "dgColhReconciliation"
        Me.dgColhReconciliation.ReadOnly = True
        Me.dgColhReconciliation.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgColhReconciliation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgColhReconciliation.Width = 150
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "employeeunkid"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Emp. Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Bank Group"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Bank Branch"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Account No."
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "EmpBankTranUnkId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'lnkEFT_DTB
        '
        Me.lnkEFT_DTB.AccessibleDescription = ""
        Me.lnkEFT_DTB.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_DTB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_DTB.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkEFT_DTB.Location = New System.Drawing.Point(29, 463)
        Me.lnkEFT_DTB.Name = "lnkEFT_DTB"
        Me.lnkEFT_DTB.Size = New System.Drawing.Size(180, 16)
        Me.lnkEFT_DTB.TabIndex = 341
        Me.lnkEFT_DTB.TabStop = True
        Me.lnkEFT_DTB.Text = "EFT DTB"
        Me.lnkEFT_DTB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmBankPaymentList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1234, 615)
        Me.Controls.Add(Me.pnlGenericPanel)
        Me.Controls.Add(Me.lblEmpCode)
        Me.Controls.Add(Me.txtEmpcode)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmBankPaymentList"
        Me.Text = "Bank Payment List"
        Me.Controls.SetChildIndex(Me.EZeeCollapsibleContainer1, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.txtEmpcode, 0)
        Me.Controls.SetChildIndex(Me.lblEmpCode, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.pnlGenericPanel, 0)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.pnlOtherSettings.ResumeLayout(False)
        Me.pnlOtherSettings.PerformLayout()
        Me.pnl_ID_Passcode.ResumeLayout(False)
        Me.pnl_ID_Passcode.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.pnlGenericPanel.ResumeLayout(False)
        CType(Me.dgBatch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtEmpcode As System.Windows.Forms.TextBox
    Friend WithEvents lblEmpCode As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountryName As System.Windows.Forms.Label
    Friend WithEvents cboBranchName As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranchName As System.Windows.Forms.Label
    Friend WithEvents cboBankName As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankName As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAmountTo As System.Windows.Forms.Label
    Friend WithEvents txtAmountTo As System.Windows.Forms.TextBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents chkEmployeeSign As System.Windows.Forms.CheckBox
    Friend WithEvents chkSignatory3 As System.Windows.Forms.CheckBox
    Friend WithEvents chkSignatory2 As System.Windows.Forms.CheckBox
    Friend WithEvents chkSignatory1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowGroupByBankBranch As System.Windows.Forms.CheckBox
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents cboReportMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportMode As System.Windows.Forms.Label
    Friend WithEvents lblCompanyBankName As System.Windows.Forms.Label
    Friend WithEvents cboCompanyBranchName As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyBranchName As System.Windows.Forms.Label
    Friend WithEvents cboCompanyBankName As System.Windows.Forms.ComboBox
    Friend WithEvents cboCompanyAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyAccountNo As System.Windows.Forms.Label
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShowFNameSeparately As System.Windows.Forms.CheckBox
    Friend WithEvents lnkEFTCityBankExport As System.Windows.Forms.LinkLabel
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents chkDefinedSignatory As System.Windows.Forms.CheckBox
    Friend WithEvents cboChequeNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblChequeNo As System.Windows.Forms.Label
    Friend WithEvents chkShowSortCode As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowPayrollPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents chkDefinedSignatory3 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDefinedSignatory2 As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowBranchCode As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowBankCode As System.Windows.Forms.CheckBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents lblCutOffAmount As System.Windows.Forms.Label
    Public WithEvents txtCutOffAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents chkShowAccountType As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowEmployeeCode As System.Windows.Forms.CheckBox
    Friend WithEvents lnkEFT_CBA_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMobileMoneyEFTMPesaExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_T24_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_EXIM_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_Custom_CSV_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_Custom_XLS_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_FlexCubeRetailGEFU_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents dtpPostingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPostingDate As System.Windows.Forms.Label
    Friend WithEvents lnkEFT_ECO_Bank As System.Windows.Forms.LinkLabel
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lnkEFT_T24_Web As System.Windows.Forms.LinkLabel
    Friend WithEvents pnl_ID_Passcode As System.Windows.Forms.Panel
    Friend WithEvents lblPasscode As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtPasscode As System.Windows.Forms.TextBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents cboHeads As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanHeads As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchLoanheads As eZee.Common.eZeeGradientButton
    Friend WithEvents lblLink As System.Windows.Forms.Label
    Friend WithEvents txtLink As System.Windows.Forms.TextBox
    Friend WithEvents lnkBankPaymentLetter As System.Windows.Forms.LinkLabel
    Friend WithEvents chkLetterhead As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowReportHeader As System.Windows.Forms.CheckBox
    Friend WithEvents chkAddresstoEmployeeBank As System.Windows.Forms.CheckBox
    Friend WithEvents lnkEFTBarclaysBankExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkOtherSetting As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlOtherSettings As System.Windows.Forms.Panel
    Friend WithEvents cboBasicSalary As System.Windows.Forms.ComboBox
    Friend WithEvents cboPresentDays As System.Windows.Forms.ComboBox
    Friend WithEvents lblPresentDays As System.Windows.Forms.Label
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblBasicSalary As System.Windows.Forms.Label
    Friend WithEvents lblSocialSecurity As System.Windows.Forms.Label
    Friend WithEvents cboSocialSecurity As System.Windows.Forms.ComboBox
    Friend WithEvents lblExtraIncome As System.Windows.Forms.Label
    Friend WithEvents cboExtraIncome As System.Windows.Forms.ComboBox
    Friend WithEvents txtIdentityType As System.Windows.Forms.TextBox
    Friend WithEvents lblIdentityType As System.Windows.Forms.Label
    Friend WithEvents txtPaymentType As System.Windows.Forms.TextBox
    Friend WithEvents lblPaymentType As System.Windows.Forms.Label
    Friend WithEvents lblAbsentDays As System.Windows.Forms.Label
    Friend WithEvents cboAbsentDays As System.Windows.Forms.ComboBox
    Friend WithEvents txtCustomCurrFormat As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomCurrFormat As System.Windows.Forms.Label
    Friend WithEvents lnkEFTNationalBankMalawi As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFTCityBank As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShowSelectedBankInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowCompanyGrpInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowCompanyLogoOnReport As System.Windows.Forms.CheckBox
    Friend WithEvents lnkEFT_FNB_Bank_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFTStandardCharteredBank_S2B As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_ABSA_Bank As System.Windows.Forms.LinkLabel
    Friend WithEvents chkSaveAsTXT_WPS As System.Windows.Forms.CheckBox
    Friend WithEvents lnkEFTNationalBankMalawiXLSX As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMembershipRepo As System.Windows.Forms.Label
    Friend WithEvents cboMembershipRepo As System.Windows.Forms.ComboBox
    Friend WithEvents lnkEFTEquityBankKenya As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFTNationalBankKenya As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFTCitiBankKenya As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFTStandardCharteredBank_TZ As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkDynamicNavisionPaymentPostDB As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkDynamicNavisionExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFTGenericCSV As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlGenericPanel As System.Windows.Forms.Panel
    Friend WithEvents lnkEFTGenericCSVPostWeb As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkEFTBankOfKigali As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_NCBA As System.Windows.Forms.LinkLabel
    Friend WithEvents dgBatch As System.Windows.Forms.DataGridView
    Friend WithEvents dgColhBatch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPostedData As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents dgColhReconciliation As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents lnkEFTPaymentBatchApproval As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFTPaymentBatchReconciliation As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_NBC As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEFT_DTB As System.Windows.Forms.LinkLabel
End Class
