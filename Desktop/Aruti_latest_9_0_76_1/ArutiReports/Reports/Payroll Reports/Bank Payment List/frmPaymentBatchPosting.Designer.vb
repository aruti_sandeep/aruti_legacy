﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaymentBatchPosting
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPaymentBatchPosting))
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSubmitForApproval = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboBatch = New System.Windows.Forms.ComboBox
        Me.lblBatch = New System.Windows.Forms.Label
        Me.btnReconciliation = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.gbBankInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchBankGroup = New eZee.Common.eZeeGradientButton
        Me.lblBankBranch = New System.Windows.Forms.Label
        Me.cboBankBranch = New System.Windows.Forms.ComboBox
        Me.cboBankGroup = New System.Windows.Forms.ComboBox
        Me.lblBankGroup = New System.Windows.Forms.Label
        Me.btnApply = New eZee.Common.eZeeLightButton(Me.components)
        Me.radApplyToAll = New System.Windows.Forms.RadioButton
        Me.radApplyToChecked = New System.Windows.Forms.RadioButton
        Me.objbtnSearchBankBranch = New eZee.Common.eZeeGradientButton
        Me.dgvDataList = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhBatchId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBankGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBranch = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objdgcolhBranchName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccountNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBankGroupUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBranchUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDpndtBeneficeTranUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBankInfo.SuspendLayout()
        CType(Me.dgvDataList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.btnExport)
        Me.objFooter.Controls.Add(Me.btnSubmitForApproval)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 462)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(749, 54)
        Me.objFooter.TabIndex = 2
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(12, 12)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "E&xport"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnSubmitForApproval
        '
        Me.btnSubmitForApproval.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSubmitForApproval.BackColor = System.Drawing.Color.White
        Me.btnSubmitForApproval.BackgroundImage = CType(resources.GetObject("btnSubmitForApproval.BackgroundImage"), System.Drawing.Image)
        Me.btnSubmitForApproval.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSubmitForApproval.BorderColor = System.Drawing.Color.Empty
        Me.btnSubmitForApproval.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSubmitForApproval.FlatAppearance.BorderSize = 0
        Me.btnSubmitForApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmitForApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmitForApproval.ForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSubmitForApproval.GradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForApproval.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.Location = New System.Drawing.Point(496, 13)
        Me.btnSubmitForApproval.Name = "btnSubmitForApproval"
        Me.btnSubmitForApproval.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForApproval.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.Size = New System.Drawing.Size(136, 30)
        Me.btnSubmitForApproval.TabIndex = 0
        Me.btnSubmitForApproval.Text = "&Submit For Approval"
        Me.btnSubmitForApproval.UseVisualStyleBackColor = True
        Me.btnSubmitForApproval.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(640, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboBatch)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatch)
        Me.gbFilterCriteria.Controls.Add(Me.btnReconciliation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(4, 1)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(318, 171)
        Me.gbFilterCriteria.TabIndex = 3
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 40)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(73, 17)
        Me.lblPeriod.TabIndex = 313
        Me.lblPeriod.Text = "Pay Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(90, 38)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(200, 21)
        Me.cboPeriod.TabIndex = 312
        '
        'cboBatch
        '
        Me.cboBatch.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatch.FormattingEnabled = True
        Me.cboBatch.Location = New System.Drawing.Point(90, 65)
        Me.cboBatch.Name = "cboBatch"
        Me.cboBatch.Size = New System.Drawing.Size(200, 21)
        Me.cboBatch.TabIndex = 310
        '
        'lblBatch
        '
        Me.lblBatch.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBatch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatch.Location = New System.Drawing.Point(10, 67)
        Me.lblBatch.Name = "lblBatch"
        Me.lblBatch.Size = New System.Drawing.Size(74, 15)
        Me.lblBatch.TabIndex = 311
        Me.lblBatch.Text = "Batch"
        Me.lblBatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnReconciliation
        '
        Me.btnReconciliation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReconciliation.BackColor = System.Drawing.Color.White
        Me.btnReconciliation.BackgroundImage = CType(resources.GetObject("btnReconciliation.BackgroundImage"), System.Drawing.Image)
        Me.btnReconciliation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReconciliation.BorderColor = System.Drawing.Color.Empty
        Me.btnReconciliation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReconciliation.FlatAppearance.BorderSize = 0
        Me.btnReconciliation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReconciliation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReconciliation.ForeColor = System.Drawing.Color.Black
        Me.btnReconciliation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReconciliation.GradientForeColor = System.Drawing.Color.Black
        Me.btnReconciliation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReconciliation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReconciliation.Location = New System.Drawing.Point(185, 127)
        Me.btnReconciliation.Name = "btnReconciliation"
        Me.btnReconciliation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReconciliation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReconciliation.Size = New System.Drawing.Size(105, 30)
        Me.btnReconciliation.TabIndex = 10
        Me.btnReconciliation.Text = "Reconciliation"
        Me.btnReconciliation.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(291, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 23)
        Me.objbtnReset.TabIndex = 4
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(266, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 23)
        Me.objbtnSearch.TabIndex = 3
        Me.objbtnSearch.TabStop = False
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 200
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(90, 92)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(200, 21)
        Me.cboStatus.TabIndex = 0
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.Transparent
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 95)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(76, 15)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBankInfo
        '
        Me.gbBankInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBankInfo.Checked = False
        Me.gbBankInfo.CollapseAllExceptThis = False
        Me.gbBankInfo.CollapsedHoverImage = Nothing
        Me.gbBankInfo.CollapsedNormalImage = Nothing
        Me.gbBankInfo.CollapsedPressedImage = Nothing
        Me.gbBankInfo.CollapseOnLoad = False
        Me.gbBankInfo.Controls.Add(Me.objbtnSearchBankGroup)
        Me.gbBankInfo.Controls.Add(Me.lblBankBranch)
        Me.gbBankInfo.Controls.Add(Me.cboBankBranch)
        Me.gbBankInfo.Controls.Add(Me.cboBankGroup)
        Me.gbBankInfo.Controls.Add(Me.lblBankGroup)
        Me.gbBankInfo.Controls.Add(Me.btnApply)
        Me.gbBankInfo.Controls.Add(Me.radApplyToAll)
        Me.gbBankInfo.Controls.Add(Me.radApplyToChecked)
        Me.gbBankInfo.Controls.Add(Me.objbtnSearchBankBranch)
        Me.gbBankInfo.ExpandedHoverImage = Nothing
        Me.gbBankInfo.ExpandedNormalImage = Nothing
        Me.gbBankInfo.ExpandedPressedImage = Nothing
        Me.gbBankInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBankInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBankInfo.HeaderHeight = 25
        Me.gbBankInfo.HeaderMessage = ""
        Me.gbBankInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBankInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBankInfo.HeightOnCollapse = 0
        Me.gbBankInfo.LeftTextSpace = 0
        Me.gbBankInfo.Location = New System.Drawing.Point(326, 2)
        Me.gbBankInfo.Name = "gbBankInfo"
        Me.gbBankInfo.OpenHeight = 300
        Me.gbBankInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBankInfo.ShowBorder = True
        Me.gbBankInfo.ShowCheckBox = False
        Me.gbBankInfo.ShowCollapseButton = False
        Me.gbBankInfo.ShowDefaultBorderColor = True
        Me.gbBankInfo.ShowDownButton = False
        Me.gbBankInfo.ShowHeader = True
        Me.gbBankInfo.Size = New System.Drawing.Size(420, 170)
        Me.gbBankInfo.TabIndex = 5
        Me.gbBankInfo.Temp = 0
        Me.gbBankInfo.Text = "Bank Information"
        Me.gbBankInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchBankGroup
        '
        Me.objbtnSearchBankGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankGroup.BorderSelected = False
        Me.objbtnSearchBankGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankGroup.Location = New System.Drawing.Point(335, 37)
        Me.objbtnSearchBankGroup.Name = "objbtnSearchBankGroup"
        Me.objbtnSearchBankGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankGroup.TabIndex = 251
        '
        'lblBankBranch
        '
        Me.lblBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankBranch.Location = New System.Drawing.Point(26, 67)
        Me.lblBankBranch.Name = "lblBankBranch"
        Me.lblBankBranch.Size = New System.Drawing.Size(82, 15)
        Me.lblBankBranch.TabIndex = 250
        Me.lblBankBranch.Text = "Branch"
        '
        'cboBankBranch
        '
        Me.cboBankBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankBranch.FormattingEnabled = True
        Me.cboBankBranch.Location = New System.Drawing.Point(118, 64)
        Me.cboBankBranch.Name = "cboBankBranch"
        Me.cboBankBranch.Size = New System.Drawing.Size(202, 21)
        Me.cboBankBranch.TabIndex = 248
        '
        'cboBankGroup
        '
        Me.cboBankGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankGroup.FormattingEnabled = True
        Me.cboBankGroup.Location = New System.Drawing.Point(118, 37)
        Me.cboBankGroup.Name = "cboBankGroup"
        Me.cboBankGroup.Size = New System.Drawing.Size(202, 21)
        Me.cboBankGroup.TabIndex = 247
        '
        'lblBankGroup
        '
        Me.lblBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankGroup.Location = New System.Drawing.Point(26, 37)
        Me.lblBankGroup.Name = "lblBankGroup"
        Me.lblBankGroup.Size = New System.Drawing.Size(82, 15)
        Me.lblBankGroup.TabIndex = 249
        Me.lblBankGroup.Text = "Bank Group"
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.BackColor = System.Drawing.Color.White
        Me.btnApply.BackgroundImage = CType(resources.GetObject("btnApply.BackgroundImage"), System.Drawing.Image)
        Me.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApply.BorderColor = System.Drawing.Color.Empty
        Me.btnApply.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApply.FlatAppearance.BorderSize = 0
        Me.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApply.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.ForeColor = System.Drawing.Color.Black
        Me.btnApply.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApply.GradientForeColor = System.Drawing.Color.Black
        Me.btnApply.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Location = New System.Drawing.Point(322, 95)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Size = New System.Drawing.Size(89, 30)
        Me.btnApply.TabIndex = 9
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'radApplyToAll
        '
        Me.radApplyToAll.BackColor = System.Drawing.Color.Transparent
        Me.radApplyToAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplyToAll.Location = New System.Drawing.Point(152, 95)
        Me.radApplyToAll.Name = "radApplyToAll"
        Me.radApplyToAll.Size = New System.Drawing.Size(93, 17)
        Me.radApplyToAll.TabIndex = 5
        Me.radApplyToAll.TabStop = True
        Me.radApplyToAll.Text = "Apply To All"
        Me.radApplyToAll.UseVisualStyleBackColor = False
        '
        'radApplyToChecked
        '
        Me.radApplyToChecked.BackColor = System.Drawing.Color.Transparent
        Me.radApplyToChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplyToChecked.Location = New System.Drawing.Point(29, 95)
        Me.radApplyToChecked.Name = "radApplyToChecked"
        Me.radApplyToChecked.Size = New System.Drawing.Size(116, 17)
        Me.radApplyToChecked.TabIndex = 4
        Me.radApplyToChecked.TabStop = True
        Me.radApplyToChecked.Text = "Apply To Checked"
        Me.radApplyToChecked.UseVisualStyleBackColor = False
        '
        'objbtnSearchBankBranch
        '
        Me.objbtnSearchBankBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankBranch.BorderSelected = False
        Me.objbtnSearchBankBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankBranch.Location = New System.Drawing.Point(335, 64)
        Me.objbtnSearchBankBranch.Name = "objbtnSearchBankBranch"
        Me.objbtnSearchBankBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankBranch.TabIndex = 252
        '
        'dgvDataList
        '
        Me.dgvDataList.AllowUserToAddRows = False
        Me.dgvDataList.AllowUserToDeleteRows = False
        Me.dgvDataList.AllowUserToResizeRows = False
        Me.dgvDataList.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvDataList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDataList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDataList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSelect, Me.dgcolhBatchId, Me.dgcolhEmployeeCode, Me.dgcolhBankGroup, Me.dgcolhBranch, Me.objdgcolhBranchName, Me.dgcolhAccountNo, Me.objdgcolhBankGroupUnkid, Me.objdgcolhBranchUnkid, Me.objdgcolhDpndtBeneficeTranUnkid, Me.dgcolhAmount, Me.dgcolhStatus})
        Me.dgvDataList.Location = New System.Drawing.Point(1, 178)
        Me.dgvDataList.Name = "dgvDataList"
        Me.dgvDataList.RowHeadersVisible = False
        Me.dgvDataList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDataList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDataList.Size = New System.Drawing.Size(745, 281)
        Me.dgvDataList.TabIndex = 4
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "App. Voc #"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 180
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 110
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 110
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Effective Date"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 85
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle7.Format = "F2"
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn6.HeaderText = "Rate %"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 50
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "F0"
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn7.HeaderText = "No. Of Instl."
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 75
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Installment Amount"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 110
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhInstallmentAmtTag"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhIsEx"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhPendingunkid"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhApprId"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhDeductionPeriodId"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objdgcolhCalcTypeId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "objdgMatchInstallmentAmt"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "objdgcolhInterestAmt"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "objdgcolhNetAmount"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "objdgcolhIntCalcTypeId"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "objdgcolhMappedHeadUnkId"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.Frozen = True
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhSelect.Width = 25
        '
        'dgcolhBatchId
        '
        Me.dgcolhBatchId.Frozen = True
        Me.dgcolhBatchId.HeaderText = "Batch ID"
        Me.dgcolhBatchId.Name = "dgcolhBatchId"
        Me.dgcolhBatchId.ReadOnly = True
        Me.dgcolhBatchId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBatchId.Width = 80
        '
        'dgcolhEmployeeCode
        '
        Me.dgcolhEmployeeCode.Frozen = True
        Me.dgcolhEmployeeCode.HeaderText = "Employee Code"
        Me.dgcolhEmployeeCode.Name = "dgcolhEmployeeCode"
        Me.dgcolhEmployeeCode.ReadOnly = True
        Me.dgcolhEmployeeCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployeeCode.Width = 120
        '
        'dgcolhBankGroup
        '
        Me.dgcolhBankGroup.HeaderText = "Bank Group"
        Me.dgcolhBankGroup.Name = "dgcolhBankGroup"
        Me.dgcolhBankGroup.ReadOnly = True
        Me.dgcolhBankGroup.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhBankGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBankGroup.Width = 150
        '
        'dgcolhBranch
        '
        Me.dgcolhBranch.DropDownWidth = 200
        Me.dgcolhBranch.HeaderText = "Branch"
        Me.dgcolhBranch.Name = "dgcolhBranch"
        Me.dgcolhBranch.Width = 150
        '
        'objdgcolhBranchName
        '
        Me.objdgcolhBranchName.HeaderText = "objdgcolhBranchName"
        Me.objdgcolhBranchName.Name = "objdgcolhBranchName"
        Me.objdgcolhBranchName.Visible = False
        '
        'dgcolhAccountNo
        '
        Me.dgcolhAccountNo.HeaderText = "Account No"
        Me.dgcolhAccountNo.Name = "dgcolhAccountNo"
        Me.dgcolhAccountNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAccountNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAccountNo.Width = 150
        '
        'objdgcolhBankGroupUnkid
        '
        Me.objdgcolhBankGroupUnkid.HeaderText = "objdgcolhBankGroupUnkid"
        Me.objdgcolhBankGroupUnkid.Name = "objdgcolhBankGroupUnkid"
        Me.objdgcolhBankGroupUnkid.Visible = False
        '
        'objdgcolhBranchUnkid
        '
        Me.objdgcolhBranchUnkid.HeaderText = "objdgcolhBranchUnkid"
        Me.objdgcolhBranchUnkid.Name = "objdgcolhBranchUnkid"
        Me.objdgcolhBranchUnkid.Visible = False
        '
        'objdgcolhDpndtBeneficeTranUnkid
        '
        Me.objdgcolhDpndtBeneficeTranUnkid.HeaderText = "objdgcolhDpndtBeneficeTranUnkid"
        Me.objdgcolhDpndtBeneficeTranUnkid.Name = "objdgcolhDpndtBeneficeTranUnkid"
        Me.objdgcolhDpndtBeneficeTranUnkid.Visible = False
        '
        'dgcolhAmount
        '
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(173, 21)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(300, 13)
        Me.objlblProgress.TabIndex = 16
        '
        'frmPaymentBatchPosting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(749, 516)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbBankInfo)
        Me.Controls.Add(Me.dgvDataList)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPaymentBatchPosting"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payment Batch Posting"
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBankInfo.ResumeLayout(False)
        CType(Me.dgvDataList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSubmitForApproval As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents gbBankInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnApply As eZee.Common.eZeeLightButton
    Friend WithEvents radApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents radApplyToChecked As System.Windows.Forms.RadioButton
    Friend WithEvents dgvDataList As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchBankBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBankGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblBankBranch As System.Windows.Forms.Label
    Friend WithEvents cboBankBranch As System.Windows.Forms.ComboBox
    Friend WithEvents cboBankGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankGroup As System.Windows.Forms.Label
    Friend WithEvents btnReconciliation As eZee.Common.eZeeLightButton
    Friend WithEvents cboBatch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBatch As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhBatchId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBankGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBranch As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objdgcolhBranchName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccountNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBankGroupUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBranchUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDpndtBeneficeTranUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
End Class
