﻿
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Security.Cryptography
Imports Newtonsoft.Json
Imports System.IO
Imports Newtonsoft.Json.Linq

Public Class frmPaymentBatchPosting

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPaymentBatchPosting"
    Private objPaymentBatchPostingMaster As clspaymentbatchposting_master
    Private mblnCancel As Boolean = True
    Private mstrVoucherNo As String
    Private mstrBatchName As String
    Private mintPeriodUnkid As Integer
    Private mdtReconciliationData As New DataTable
    Private mblnIsFromGrid As Boolean
    Private mintReportModeId As Integer
    Private mintBankId As Integer
    Private mintBranchId As Integer
    Private mintCompanyBranchId As Integer
    Private mstrCompanyBankAccountNo As String
    Private mintCurrencyId As Integer = 0
    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private mstrCurrency_Sign As String
    Private dtStatus As New DataTable
    Private mstrOldBatchName As String
    'Hemant (21 Jul 2023) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal strVoucherNo As String, ByVal intPeriodUnkid As Integer, ByVal blnIsFromGrid As Boolean, ByVal intReportModeId As Integer, ByVal intBankId As Integer, ByVal intBranchId As Integer, ByVal intCompanyBranchId As Integer, ByVal strCompanyBankAccountNo As String, ByVal intCurrencyId As Integer, ByVal strCurrency_Sign As String) As Boolean
        'Hemant (21 Jul 2023) -- [strCurrency_Sign]
        Try
            mstrVoucherNo = strVoucherNo
            mstrBatchName = strVoucherNo
            mintPeriodUnkid = intPeriodUnkid
            mblnIsFromGrid = blnIsFromGrid
            mintReportModeId = intReportModeId
            mintBankId = intBankId
            mintBranchId = intBranchId
            mintCompanyBranchId = intCompanyBranchId
            mstrCompanyBankAccountNo = strCompanyBankAccountNo
            mintCurrencyId = intCurrencyId
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            mstrCurrency_Sign = strCurrency_Sign
            'Hemant (21 Jul 2023) -- End
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objCountry As New clsMasterData
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim objPeriod As New clscommom_period_Tran
        Dim objBank As New clspayrollgroup_master
        Dim dsCombo As New DataSet
        Try
            Dim intFirstOpenPeriod As Integer = objCountry.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                If mblnIsFromGrid = True Then
                    .SelectedValue = mintPeriodUnkid
                Else
                    .SelectedValue = intFirstOpenPeriod
                End If
            End With


            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'dsCombo = objPaymentBatchPostingMaster.getStatusComboList("List", True)
            'With cboStatus
            '    .DisplayMember = "Name"
            '    .ValueMember = "Id"
            '    .DataSource = dsCombo.Tables(0)
            '    .SelectedValue = "-1"
            '    .SelectedIndex = 0
            'End With
            FillStatusCombo(dtStatus)
            'Hemant (21 Jul 2023) -- End



            dsCombo = objBank.getListForCombo(enPayrollGroupType.Bank, "Banks", True)
            With cboBankGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Banks")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCountry = Nothing
            objPeriod = Nothing
            objPaymentBatchPostingMaster = Nothing
            objBank = Nothing
        End Try
    End Sub

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private Sub FillStatusCombo(ByVal dtTable As DataTable)
        Try
            If Not dtStatus.Columns.Contains("status") Then
                Dim dCol As DataColumn

                dCol = New DataColumn("status")
                dCol.Caption = "status"
                dCol.DataType = System.Type.GetType("System.String")
                dtStatus.Columns.Add(dCol)
            End If

            With cboStatus
                .Items.Clear()
                .Items.Add("Select")
                For Each dr As DataRow In dtTable.Rows
                    .Items.Add(dr.Item("status").ToString)
                Next
                .SelectedIndex = 0
            End With
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillStatusCombo", mstrModuleName)
        End Try


    End Sub
    'Hemant (21 Jul 2023) -- End

    Private Sub FillList()
        Dim drRow() As DataRow
        Dim strFilter As String = String.Empty
        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
        Try
            If CInt(cboBatch.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please select Batch."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Sub
            End If
            Dim dtTable As DataTable
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            strFilter = "1 = 1 "
            If CInt(cboPeriod.SelectedValue) > 0 Then
                strFilter &= "AND periodunkID = " & CInt(cboPeriod.SelectedValue) & " "
            End If

            'Hemant (04 Aug 2023) -- Start
            'If CInt(cboBatch.SelectedValue) <> 0 Then
            '    strFilter &= "AND batchname = '" & CStr(cboBatch.Text) & "' "
            'End If
            'Hemant (04 Aug 2023) -- End

            If CInt(cboStatus.SelectedIndex) > 0 Then
                strFilter &= "AND status = '" & CStr(cboStatus.Text) & "' "
            End If
            'If CInt(cboStatus.SelectedValue) <= 0 Then
            '    dtTable = mdtReconciliationData.Copy
            'Else
            '    drRow = mdtReconciliationData.Select("status = '" & cboStatus.Text & "'")
            '    If drRow.Length > 0 Then
            '        dtTable = drRow.CopyToDataTable()

            '    End If
            'End If
            'Hemant (04 Aug 2023) -- Start
            Dim dsList As DataSet = objPaymentBatchReconciliationTran.GetList("List", IIf(CInt(cboBatch.SelectedValue) <> 0, cboBatch.Text, ""))
            If dsList.Tables(0).Columns.Contains("ischeck") = False Then
                Dim dCol As New DataColumn
                With dCol
                    .ColumnName = "ischeck"
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = False
                End With
                dsList.Tables(0).Columns.Add(dCol)
            End If

            dtStatus = dsList.Tables(0).DefaultView.ToTable(True, "status")
            If mstrOldBatchName <> cboBatch.Text Then
                FillStatusCombo(dtStatus)
            End If
            drRow = dsList.Tables(0).Select(strFilter)
                If drRow.Length > 0 Then
                    dtTable = drRow.CopyToDataTable()
            End If
            Call SetGridDataSource(dtTable)
            mstrOldBatchName = cboBatch.Text
            'Hemant (21 Jul 2023) -- End
            'Hemant (04 Aug 2023) -- End

            If cboStatus.Text.Trim.ToUpper = "FAILED" Then
                btnSubmitForApproval.Visible = True
            Else
                btnSubmitForApproval.Visible = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Hemant (04 Aug 2023) -- Start
        Finally
            objPaymentBatchReconciliationTran = Nothing
            'Hemant (04 Aug 2023) -- End
        End Try
    End Sub

    Private Sub SetGridDataSource(ByVal dtTable As DataTable)
        Try
            dgvDataList.AutoGenerateColumns = False
            objdgcolhSelect.DataPropertyName = "IsCheck"
            dgcolhBatchId.DataPropertyName = "batchname"
            dgcolhEmployeeCode.DataPropertyName = "employeeCode"
            dgcolhAccountNo.DataPropertyName = "accountno"
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            dgcolhBankGroup.DataPropertyName = "bankname"
            'Hemant (21 Jul 2023) -- End
            Dim objBranch As New clsbankbranch_master
            Dim dsBranch As DataSet = objBranch.getListForCombo("Branch", True)
            With dgcolhBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsBranch.Tables("Branch")
                .DataPropertyName = "branchunkid"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            End With
            objdgcolhBranchUnkid.DataPropertyName = "branchunkid"
            objBranch = Nothing

            objdgcolhDpndtBeneficeTranUnkid.DataPropertyName = "dpndtbeneficetranunkid"
            'Hemant (04 Aug 2023) -- Start
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhStatus.DataPropertyName = "status"
            objdgcolhBranchName.DataPropertyName = "branchname"
            'Hemant (04 Aug 2023) -- End
            dgvDataList.DataSource = dtTable
            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridDataSource", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue(ByVal objPaymentBatchPostingMaster As clspaymentbatchposting_master)
        Try
            If mblnIsFromGrid = True Then
                objPaymentBatchPostingMaster._BatchName = mstrVoucherNo & "-1"
            Else
                objPaymentBatchPostingMaster._BatchName = cboBatch.Text & "-1"
            End If
            objPaymentBatchPostingMaster._Periodunkid = mintPeriodUnkid
            objPaymentBatchPostingMaster._VoucherNo = mstrVoucherNo
            objPaymentBatchPostingMaster._Statusunkid = clspaymentbatchposting_master.enApprovalStatus.SubmitForApproval
            objPaymentBatchPostingMaster._Userunkid = User._Object._Userunkid
            objPaymentBatchPostingMaster._Refno = objPaymentBatchPostingMaster.getNextRefNo(mstrVoucherNo).ToString
            objPaymentBatchPostingMaster._Isvoid = False
            objPaymentBatchPostingMaster._Voiduserunkid = -1
            objPaymentBatchPostingMaster._Voiddatetime = Nothing
            objPaymentBatchPostingMaster._Voidreason = ""
            objPaymentBatchPostingMaster._ClientIP = getIP()
            objPaymentBatchPostingMaster._HostName = getHostName()
            objPaymentBatchPostingMaster._FormName = mstrModuleName
            objPaymentBatchPostingMaster._AuditUserId = User._Object._Userunkid
            objPaymentBatchPostingMaster._Isweb = False
            objPaymentBatchPostingMaster._BatchPostingEmpTran = CType(dgvDataList.DataSource, DataTable)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            If dgvDataList.Rows.Cast(Of DataGridViewRow).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Atleast one Employee in the list is required for Approval."), enMsgBoxStyle.Information)
                dgvDataList.Focus()
                Return False
            ElseIf objPaymentBatchPostingMaster.IsBatchPostingPendingForApproval(mstrVoucherNo) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You can not submit for Approval. Because Already Batch is submitted for Approval Process"), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Function

    Private Sub SetCalculation(ByVal blnIsApplyToAll As Boolean)
        Try
            Dim dtmp As DataRow() = Nothing
            If blnIsApplyToAll = True Then
                dtmp = CType(dgvDataList.DataSource, DataTable).Select("1=1")
            Else
                dtmp = CType(dgvDataList.DataSource, DataTable).Select("IsCheck = true")
            End If
            If dtmp.Length > 0 Then
                For Each dRow As DataRow In dtmp
                    dgvDataList.CurrentCell = dgvDataList.Rows(CType(dgvDataList.DataSource, DataTable).Rows.IndexOf(dRow)).Cells(dgcolhBranch.Index)
                    If blnIsApplyToAll = True Then
                    End If
                    dRow.Item("branchunkid") = CInt(cboBankBranch.SelectedValue)

                    Call Combo_SelectionChangeCommitted(cboBankBranch, New EventArgs)

                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCalculation", mstrModuleName)
        End Try
    End Sub


    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    'Private Function GenarateJSONReconciliation(ByVal drRow As DataRow, _
    '                                            ByVal intPaymentBatchPostingId As Integer _
    '                                           ) As String
    '    Dim strJSON As String = String.Empty
    '    Try
    '        strJSON = "{"
    '        If intPaymentBatchPostingId < 0 Then
    '            strJSON &= """batchId"":""" & drRow.Item("voucherno") & """"
    '            strJSON &= ",""employeeCode"":""" & drRow.Item("empcode") & """"
    '            strJSON &= ",""accountNumber"":""" & drRow.Item("emp_accountno") & """"
    '        Else
    '            strJSON &= """batchId"":""" & drRow.Item("batchname") & """"
    '            strJSON &= ",""employeeCode"":""" & drRow.Item("employeecode") & """"
    '            strJSON &= ",""accountNumber"":""" & drRow.Item("accountno") & """"
    '        End If
    '        strJSON &= ",""timestamp"":""" & CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyy-MM-dd hh:mm:ss") & """"
    '        strJSON &= "}"
    '        Return strJSON
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GenarateJSONReconciliation", mstrModuleName)
    '    End Try
    'End Function
    Private Function GenarateJSONReconciliation(ByVal strBatchId As String _
                                                , ByVal intBatchCount As Integer _
                                                , ByVal strOrganizationCode As String _
                                                , ByVal strbatchTimestamp As String _
                                                , ByVal strDebitAccountNumber As String _
                                                , ByVal strDebitCurrency As String _
                                                , ByVal decTotalAmount As Decimal _
                                                ) As String
        Dim strJSON As String = String.Empty
        Try
            strJSON = "{"
            strJSON &= """batchId"":""" & strBatchId & """"
            strJSON &= ",""organizationCodeName"":""" & strOrganizationCode & """"
            strJSON &= ",""batchTimestamp"":""" & strbatchTimestamp & """"
            strJSON &= ",""debitAccount"":""" & strDebitAccountNumber & """"
            strJSON &= ",""debitCurrency"":""" & strDebitCurrency & """"
            strJSON &= ",""debitAccountName"":""TRA Salary Account"""
            strJSON &= ",""totalAmount"":""" & Format(decTotalAmount, "#########################0.#0") & """"
            strJSON &= ",""batchCount"":""" & intBatchCount & """"
            strJSON &= ",""accountingType"":""SDMC"""
            strJSON &= ",""batchNarration"":""TRA batch"""
            strJSON &= "}"
            Return strJSON
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenarateJSONReconciliation", mstrModuleName)
        End Try
    End Function
    'Hemant (21 Jul 2023) -- End



    Public Function SignData(ByVal StringToSign As String) As String
        Try

            Dim rsa As RSACryptoServiceProvider = New RSACryptoServiceProvider()
            rsa.FromXmlString("<RSAKeyValue><Modulus>7W+lAXsQXX4bIITjBLeGGzKIaOIWKLyMItGNYs3ozTvemyBigsTRKJToBhwsx5p+fDsHOGmFPyL1z8P2k3lPUM6Yg28qKl1h9WxOz1W8t4/WMBTCEu9h04ldspJSgMOynovjeaerj7bpge/ffpS8/LRA0j0ZvRaMs7DkfvzCsGby7aaeXm3uglvGhA2p0Gl/TNxfFNo+9eWnjN3Dg/7qXNwYjlYbFVMA/SZ5xJDY+F5YzjVDpKq6XVpB+9H5iD5lv04VLveWk+EkJct8oq+WmgNEqZzIWzmadgv++B0VtxiPnkpzeqRmWoZAtAT1IeevyX0y/KKXKKJYam7CFwETCQ==</Modulus><Exponent>AQAB</Exponent><P>+ZpHDGtbuc03Nl5Dop96bi8M6VMiR4n7wye97cTn76tQ6iNbioUnlmmdUXllognllUmk6h+sD92wuQbMpbkgh5WZmodcrY/N2acafKBrrAS5JwgN2WioxoHyIqJa9Z9B7GfxTLNTXfoz7CEoxDUJD1QHsoWFDjDUqzyMFxdZ7e0=</P><Q>84WJ27U7gNLTpFFTLwMKidHuroWx7PkyWVwyx2qxki1mmGYHf+13jlSCdT7Jz7d8h3XAykrDsHElTus6jRV2PSFfs4g4u1rsrqmgHYa2jrAGvX8U9LfnjoYs9v6alu74Z90HCLTBdJ5Hea8yumEMMiKkDxp/prpiItBplMRANg0=</Q><DP>TLJ1ZoGOu/ctIg2xJsVub3ERvJiJDgZ+UCdkGy3IP0MbJ/cZZ+Umlvd5GdH9wt7bpxXsEO0OiAmNBi3qsHnEXyU+/9bcSZDIpjrMzsLUkxUYd7/n0YhxZB4F81KENLltHmGKKhFoapY5YjOGPVQ2pnkhrF+O1R94Ge4O9gF85rk=</DP><DQ>me8Y1LQ8F9OtCxqJPZdrivEUMme6r/RaGliIlLvh4WgniUA9j2U5hNPw31JAWbg/1JTfuEAIcTkkfz18doBRjJTTHPaH/g6cvE/nMaLdNVcZ+6EgSw0RJ2uzcrJAYBZRGb6C2sL/4srGnancpCoCfpKdKBr1BByfOiiKBQsFF+U=</DQ><InverseQ>ywk/7sgD8R/8QWqnUF2fmXLgEUnSWh2Y6bKFbiDffnH1+AZdp3x8s4tocyu+RXFUt+IXHWKP5sxTNyjYSJ0YGxGeUSwjhn87t0G6L2/KGOe6ihtfXEOgvlngVbUVTCtdJYrXD50+/pvBxOoWB9Yxsve1NYJfhr5V3YLZJg97jL4=</InverseQ><D>aV6Of6W5kYQRTdErXkCDxzYZy1HqO5HRLvKIKDzw/4N+OqGYlif6GmRaw7tlM/+f+knH3oUVmPtO0zFIEBJZ3KaSkGGY+MwQWPYD04ddBKlUiGnt5rFNXK8tYb4F1xcCAdJa1PZP8Ktf3UYyjN49MHhd++8ZqQyEzInIHYLWc6mhdcNN6QuLMPhwcA19yoAMeuP6Bu03XcmObhulrFaEl4AVA4VK8KmZKG80rl3bjE9L59a83T6nEiIkD4xpvKJjmGrnI0pSsSptJZUkrWgCTDAmTaaMIdRU93+WF6N3/ycAd5c1uU72Z7qhgT8HWO2+AArqoAuBB3jeIrVF5H69wQ==</D></RSAKeyValue>")
            Dim originalString As String = StringToSign
            Dim signedData As Byte() = rsa.SignData(System.Text.Encoding.UTF8.GetBytes(originalString), CryptoConfig.MapNameToOID("SHA256"))
            Return Convert.ToBase64String(signedData)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SignData", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Form's Events "

    Private Sub frmPaymentBatchPosting_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentBatchPosting_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSubmitForApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            SetValue(objPaymentBatchPostingMaster)
            blnFlag = objPaymentBatchPostingMaster.Insert()

            If blnFlag = False And objPaymentBatchPostingMaster._Message <> "" Then
                eZeeMsgBox.Show(objPaymentBatchPostingMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag = True Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSubmitForApproval_Click", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Sub

    Private Sub btnReconciliation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReconciliation.Click
        Dim objBankPaymentList As New clsBankPaymentList(User._Object._Languageunkid, Company._Object._Companyunkid)
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
        Dim strError As String = ""
        Dim strPostedData As String = ""
        Try
            If CInt(cboBatch.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select atleast one Batch for Reconciliation process."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Sub
            End If

            Dim dsRenciliationData As DataSet = objPaymentBatchReconciliationTran.GetList("List", cboBatch.Text)
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            If dsRenciliationData.Tables(0).Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Selected Batch is not Posted yet."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Sub
            End If
            'Hemant (21 Jul 2023) -- End

            'Hemant (04 Aug 2023) -- Start
            Dim drRecon() As DataRow = dsRenciliationData.Tables(0).Select("reconciliation_date IS NOT NULL OR STATUS <> ''")
            If drRecon.Length > 0 Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Reconciliation has been already done for Selected Batch, Are you sure you want to Reconciliation again for Selected Batch?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
                    Exit Try
                End If
            End If
            'Hemant (04 Aug 2023) -- End

            'Hemant (04 Aug 2023) -- Start
            'Dim dsBatchData As DataSet
            'mdtReconciliationData = objPaymentBatchPostingTran._TranDataTable
            'If CInt(cboBatch.SelectedValue) < 0 Then
            '    Dim intEFTMembershipUnkId As Integer = 0
            '    Dim objPeriod As New clscommom_period_Tran
            '    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            '    objBankPaymentList._PeriodId = CStr(cboPeriod.SelectedValue)
            '    objBankPaymentList._BankId = mintBankId
            '    objBankPaymentList._BranchId = mintBranchId
            '    objBankPaymentList._CompanyBranchId = mintCompanyBranchId
            '    objBankPaymentList._CompanyBankAccountNo = mstrCompanyBankAccountNo
            '    objBankPaymentList._CurrencyId = mintCurrencyId
            '    objBankPaymentList._ReportModeId = mintReportModeId
            '    dsBatchData = objBankPaymentList.Get_EFT_Generic_CSV_Post_Data(FinancialYear._Object._DatabaseName, _
            '                                                                   User._Object._Userunkid, _
            '                                                                   FinancialYear._Object._YearUnkid, _
            '                                                                   Company._Object._Companyunkid, _
            '                                                                   objPeriod._Start_Date, _
            '                                                                   objPeriod._End_Date, _
            '                                                                   ConfigParameter._Object._UserAccessModeSetting, _
            '                                                                   True, True, _
            '                                                                   True, _
            '                                                                   ConfigParameter._Object._CurrencyFormat, _
            '                                                                   intEFTMembershipUnkId, _
            '                                                                   "", _
            '                                                                   " AND (prpayment_tran.voucherno = '" & cboBatch.Text & "' OR prglobalvoc_master.globalvocno = '" & cboBatch.Text & "') " _
            '                                                                   )
            '    'Hemant (21 Jul 2023) -- Start
            '    'ENHANCEMENT : A1X-1107 - Payment Gateway - Pass the total net pay going through mobile in one tranche on the gateway payload
            '    Dim decTotalAmount As Decimal = (From p In dsBatchData.Tables(0) Where p.Item("voucherno") = CStr(cboBatch.Text) And p.Item("payment_typeid") = CInt(enBankPaymentType.MobileNumber) Select (CDec(p.Item("Amount")))).Sum()
            '    If decTotalAmount > 0 AndAlso CInt(ConfigParameter._Object._EFTMobileMoneyBank) > 0 AndAlso CInt(ConfigParameter._Object._EFTMobileMoneyBranch) > 0 AndAlso ConfigParameter._Object._EFTMobileMoneyAccountName.Trim.Length > 0 AndAlso ConfigParameter._Object._EFTMobileMoneyAccountNo.Trim.Length > 0 Then
            '        Dim objPayrollGroupMaster As New clspayrollgroup_master
            '        Dim objBankbBranchMaster As New clsbankbranch_master
            '        objPayrollGroupMaster._Groupmasterunkid = CInt(ConfigParameter._Object._EFTMobileMoneyBank)
            '        objBankbBranchMaster._Branchunkid = CInt(ConfigParameter._Object._EFTMobileMoneyBranch)
            '        Dim drMobileMoney As DataRow = dsBatchData.Tables(0).NewRow
            '        drMobileMoney.Item("employeeunkid") = "-1"
            '        drMobileMoney.Item("EmpCode") = "000000"
            '        drMobileMoney.Item("EmpName") = ConfigParameter._Object._EFTMobileMoneyAccountName.ToString
            '        drMobileMoney.Item("EMP_BGCode") = objPayrollGroupMaster._Groupcode
            '        drMobileMoney.Item("EMP_BankName") = objPayrollGroupMaster._Groupname
            '        drMobileMoney.Item("EMP_BRCode") = objBankbBranchMaster._Branchcode
            '        drMobileMoney.Item("EMP_SwiftCode") = objPayrollGroupMaster._Swiftcode
            '        drMobileMoney.Item("EMP_BranchName") = objBankbBranchMaster._Branchname
            '        drMobileMoney.Item("EMP_SortCode") = objBankbBranchMaster._Sortcode
            '        drMobileMoney.Item("EMP_AccountNo") = ConfigParameter._Object._EFTMobileMoneyAccountNo.ToString
            '        drMobileMoney.Item("EMP_BGID") = CInt(ConfigParameter._Object._EFTMobileMoneyBank)
            '        drMobileMoney.Item("EMP_BRID") = CInt(ConfigParameter._Object._EFTMobileMoneyBranch)
            '        drMobileMoney.Item("currency_sign") = dsBatchData.Tables(0).Rows(0).Item("currency_sign")
            '        drMobileMoney.Item("Amount") = Format(decTotalAmount, ConfigParameter._Object._CurrencyFormat).Replace(",", "")
            '        drMobileMoney.Item("CMP_BGID") = dsBatchData.Tables(0).Rows(0).Item("CMP_BGID")
            '        drMobileMoney.Item("CMP_BRID") = dsBatchData.Tables(0).Rows(0).Item("CMP_BRID")
            '        drMobileMoney.Item("CMP_BGCODE") = dsBatchData.Tables(0).Rows(0).Item("CMP_BGCODE")
            '        drMobileMoney.Item("CMP_BankName") = dsBatchData.Tables(0).Rows(0).Item("CMP_BankName")
            '        drMobileMoney.Item("CMP_BRCode") = dsBatchData.Tables(0).Rows(0).Item("CMP_BRCode")
            '        drMobileMoney.Item("CMP_BranchName") = dsBatchData.Tables(0).Rows(0).Item("CMP_BranchName")
            '        drMobileMoney.Item("CMP_Account") = dsBatchData.Tables(0).Rows(0).Item("CMP_Account")
            '        drMobileMoney.Item("CMP_SortCode") = dsBatchData.Tables(0).Rows(0).Item("CMP_SortCode")
            '        drMobileMoney.Item("CMP_SwiftCode") = dsBatchData.Tables(0).Rows(0).Item("CMP_SwiftCode")
            '        drMobileMoney.Item("voucherno") = cboBatch.Text
            '        drMobileMoney.Item("periodunkid") = dsBatchData.Tables(0).Rows(0).Item("periodunkid")
            '        drMobileMoney.Item("paidcurrencyid") = dsBatchData.Tables(0).Rows(0).Item("paidcurrencyid")
            '        drMobileMoney.Item("payment_typeid") = CInt(enBankPaymentType.BankAccount)
            '        drMobileMoney.Item("dpndtbeneficetranunkid") = 0
            '        dsBatchData.Tables(0).Rows.Add(drMobileMoney)
            '        dsBatchData.Tables(0).AcceptChanges()
            '        objPayrollGroupMaster = Nothing
            '        objBankbBranchMaster = Nothing
            '    End If
            '    Dim dtBatch As DataTable = dsBatchData.Tables(0).Select("voucherno = '" & cboBatch.Text & "' AND payment_typeid = " & CInt(enBankPaymentType.BankAccount) & " ").CopyToDataTable
            '    dsBatchData.Tables.RemoveAt(0)
            '    dsBatchData.Tables.Add(dtBatch)
            '    'Hemant (21 Jul 2023) -- End
            'Else
            '    dsBatchData = objPaymentBatchPostingTran.GetList("Batch", CInt(cboBatch.SelectedValue))
            'End If
            'Hemant (04 Aug 2023) -- End


            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'Dim dsStatus As DataSet = (New clspaymentbatchposting_master).getStatusComboList("List", True)
            'For Each drEmployeeData As DataRow In dsBatchData.Tables(0).Rows
            '    Dim strJSON As String = GenarateJSONReconciliation(drEmployeeData, CInt(cboBatch.SelectedValue))
            '    Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(ConfigParameter._Object._EFTRequestClientID & ":" & ConfigParameter._Object._EFTRequestPassword)
            '    Dim strBase64Credential As String = Convert.ToBase64String(byt)

            '    Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(ConfigParameter._Object._EFTReconciliationURL, strBase64Credential, strJSON, strError, strPostedData, True, SignData(strJSON))

            '    If strMessage IsNot Nothing Then
            '        eZeeMsgBox.Show(" Request Response : " & strMessage("statusCode").ToString() & " - " & strMessage("message").ToString(), enMsgBoxStyle.Information)
            '    End If
            '    'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":{""batchId"":""PV13496"",""accountNumber"":""0321101809097"",""amount"":""306500.00"",""status"":""FAILED"",""reference"":null,""timestamp"":""2023-06-06 10:21:46""}}"
            '    Dim ResultJSON = JsonConvert.DeserializeObject(strMessage)
            '    Dim objObject As Object = ResultJSON
            '    Dim strPayload = objObject("payload").ToString
            '    Dim PayloadJSON = JsonConvert.DeserializeObject(strPayload)
            '    Dim strStatus = PayloadJSON("status").ToString()

            '    Dim drRow As DataRow = mdtReconciliationData.NewRow
            '    drRow.Item("IsCheck") = "False"
            '    drRow.Item("employeeunkid") = drEmployeeData.Item("employeeunkid").ToString
            '    If cboBatch.SelectedValue < 0 Then
            '        drRow.Item("batchname") = drEmployeeData.Item("voucherno").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("empcode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("EMP_BRID").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("EMP_accountNo").ToString
            '    Else
            '        drRow.Item("batchname") = drEmployeeData.Item("batchname").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("employeecode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("branchunkid").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("accountNo").ToString
            '    End If

            '    drRow.Item("status") = strStatus.ToString
            '    Dim drReconciliation() As DataRow = dsRenciliationData.Tables(0).Select("employeeunkid = " & CInt(drEmployeeData.Item("employeeunkid").ToString) & " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND batchname = '" & cboBatch.Text & "' AND accountno = '" & drRow.Item("accountNo") & "' ")
            '    If drReconciliation.Length > 0 Then
            '        Dim drStatus() As DataRow = dsStatus.Tables(0).Select("name = '" & strStatus.ToString & "'")
            '        If drStatus.Length > 0 Then
            '            drReconciliation(0).Item("statusunkid") = CInt(drStatus(0).Item("Id"))
            '        End If
            '        drReconciliation(0).Item("reconciliation_date") = CDate(ConfigParameter._Object._CurrentDateAndTime)
            '        dsRenciliationData.Tables(0).AcceptChanges()
            '    End If

            '    drRow.Item("dpndtbeneficetranunkid") = drEmployeeData.Item("dpndtbeneficetranunkid").ToString

            '    mdtReconciliationData.Rows.Add(drRow)
            'Next
            Dim decTotalBatchAmount As Decimal = (From p In dsRenciliationData.Tables(0) Select (CDec(p.Item("Amount")))).Sum()
            Dim strJSON As String = GenarateJSONReconciliation(cboBatch.Text, dsRenciliationData.Tables(0).Rows.Count, Company._Object._Code, CDate(dsRenciliationData.Tables(0).Rows(0).Item("batchposted_date")).ToString("yyyyMMdd"), mstrCompanyBankAccountNo, mstrCurrency_Sign, decTotalBatchAmount)
                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(ConfigParameter._Object._EFTRequestClientID & ":" & ConfigParameter._Object._EFTRequestPassword)
                Dim strBase64Credential As String = Convert.ToBase64String(byt)

                Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(ConfigParameter._Object._EFTReconciliationURL, strBase64Credential, strJSON, strError, strPostedData, True, SignData(strJSON))

                If strMessage IsNot Nothing Then
                    eZeeMsgBox.Show(" Request Response : " & strMessage("statusCode").ToString() & " - " & strMessage("message").ToString(), enMsgBoxStyle.Information)
                End If

                'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":{""batchId"":""PV13496"",""accountNumber"":""0321101809097"",""amount"":""306500.00"",""status"":""FAILED"",""reference"":null,""timestamp"":""2023-06-06 10:21:46""}}"
            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":[{""clientReference"":""2106611021000025000001"",""status"":""success""},{""clientReference"":""2106611011000009000002"",""status"":""success""},{""clientReference"":""2106611011000025000003"",""status"":""failed""},{""clientReference"":""2106611011000584000004"",""status"":""success""},{""clientReference"":""210661101000000000005"",""status"":""success""}]}"
            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":[{""clientReference"": ""2106620021000425000001"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""P"",""bankRef"": null,""paymentStatusDesc"":""Failed"",""resv1"":null,""resv2"":null}, " & _
            '                                                                           "{""clientReference"": ""2106620021000580000002"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""S"",""bankRef"": null,""paymentStatusDesc"":""Failed"",""resv1"":null,""resv2"":null}, " & _
            '                                                                           "{""clientReference"": ""210662002000000000003"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""U"",""bankRef"": null,""paymentStatusDesc"":""Failed"",""resv1"":null,""resv2"":null}," & _
            '                                                                           "{""clientReference"": ""2106618011000584000004"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""S"",""bankRef"": null,""paymentStatusDesc"":""Submitted"",""resv1"":null,""resv2"":null}, " & _
            '                                                                            "{""clientReference"": ""210661801000000000005"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""F"",""bankRef"": null,""paymentStatusDesc"":""Failed"",""resv1"":null,""resv2"":null}]}"

            'Dim ResultJSON = JsonConvert.DeserializeObject(strMessage)
            'Dim objObject As Object = ResultJSON
            'Dim strPayload = objObject("payload").ToString
            Dim strPayload = strMessage("payload").ToString
            Dim PayloadJSON = JsonConvert.DeserializeObject(strPayload.Trim)
            Dim dtResponse As DataTable = JsonConvert.DeserializeObject(Of DataTable)(PayloadJSON.ToString())
            'Hemant (04 Aug 2023) -- Start
            'dtStatus = dtResponse.DefaultView.ToTable(True, "PaymentStatusDesc")
            'Hemant (04 Aug 2023) -- End
            'Hemant (04 Aug 2023) -- Start
            'Dim intCount As Integer = 0
            'Dim intTotalCount As Integer = dsBatchData.Tables(0).Rows.Count
            'For Each drEmployeeData As DataRow In dsBatchData.Tables(0).Rows
            '    Dim drRow As DataRow = mdtReconciliationData.NewRow
            '    drRow.Item("IsCheck") = "False"
            '    drRow.Item("employeeunkid") = drEmployeeData.Item("employeeunkid").ToString
            '    If cboBatch.SelectedValue < 0 Then
            '        drRow.Item("batchname") = drEmployeeData.Item("voucherno").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("empcode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("EMP_BRID").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("EMP_accountNo").ToString
            '        drRow.Item("bankname") = drEmployeeData.Item("EMP_BankName").ToString
            '        drRow.Item("periodunkid") = drEmployeeData.Item("periodunkid").ToString
            '        drRow.Item("branchname") = drEmployeeData.Item("EMP_branchname").ToString
            '    Else
            '        drRow.Item("batchname") = drEmployeeData.Item("batchname").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("employeecode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("branchunkid").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("accountNo").ToString
            '        drRow.Item("bankname") = drEmployeeData.Item("bankname").ToString
            '        drRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            '        drRow.Item("branchname") = drEmployeeData.Item("branchname").ToString
            '    End If
            '    'Hemant (04 Aug 2023) -- Start
            '    drRow.Item("amount") = drEmployeeData.Item("amount")
            '    'Hemant (04 Aug 2023) -- End
            '    Dim drReconciliation() As DataRow = dsRenciliationData.Tables(0).Select("employeeunkid = " & CInt(drEmployeeData.Item("employeeunkid").ToString) & " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND batchname = '" & cboBatch.Text & "' AND accountno = '" & drRow.Item("accountNo") & "' ")
            '    If drReconciliation.Length > 0 Then
            '        If drReconciliation(0).Item("clientReference").ToString.Trim.Length > 0 Then
            '            Dim drResponse() As DataRow = dtResponse.Select("clientReference = '" & drReconciliation(0).Item("clientReference") & "'")
            '            If drResponse.Length > 0 Then
            '                'Dim drStatus() As DataRow = dsStatus.Tables(0).Select("name = '" & drResponse(0).Item("PaymentStatusDesc").ToString & "'")
            '                'If drStatus.Length > 0 Then
            '                '    drReconciliation(0).Item("statusunkid") = CInt(drStatus(0).Item("Id"))
            '                '    drRow.Item("statusunkid") = CInt(drStatus(0).Item("Id"))
            '                '    drRow.Item("status") = drResponse(0).Item("status").ToString
            '                'End If
            '                drReconciliation(0).Item("status") = drResponse(0).Item("PaymentStatusDesc").ToString
            '                drRow.Item("status") = drResponse(0).Item("PaymentStatusDesc").ToString
            '                drReconciliation(0).Item("reconciliation_date") = CDate(ConfigParameter._Object._CurrentDateAndTime)
            '                dsRenciliationData.Tables(0).AcceptChanges()
            '            End If
            '        End If
            '    End If

            '    drRow.Item("dpndtbeneficetranunkid") = drEmployeeData.Item("dpndtbeneficetranunkid").ToString

            '    mdtReconciliationData.Rows.Add(drRow)
            '    intCount = intCount + 1
            '    objlblProgress.Text = "Fetching Data : " & "[ " & intCount.ToString & " / " & intTotalCount.ToString & " ]"
            '    objlblProgress.Refresh()
            'Next
            Dim intCount As Integer = 0
            Dim intTotalCount As Integer = dsRenciliationData.Tables(0).Rows.Count
            For Each drEmployeeData As DataRow In dsRenciliationData.Tables(0).Rows
                Dim drResponse() As DataRow = dtResponse.Select("clientReference = '" & drEmployeeData.Item("clientReference") & "'")
                        If drResponse.Length > 0 Then
                    drEmployeeData.Item("status") = drResponse(0).Item("PaymentStatusDesc").ToString
                    drEmployeeData.Item("reconciliation_date") = CDate(ConfigParameter._Object._CurrentDateAndTime)
                    dsRenciliationData.Tables(0).AcceptChanges()
                End If
                intCount = intCount + 1
                objlblProgress.Text = "Fetching Data : " & "[ " & intCount.ToString & " / " & intTotalCount.ToString & " ]"
                objlblProgress.Refresh()
            Next
            'Hemant (04 Aug 2023) -- End

            'Hemant (21 Jul 2023) -- End
            objPaymentBatchReconciliationTran._TranDataTable = dsRenciliationData.Tables(0)
            If objPaymentBatchReconciliationTran.UpdateAll() = False And objPaymentBatchReconciliationTran._Message <> "" Then
                eZeeMsgBox.Show(objPaymentBatchReconciliationTran._Message, enMsgBoxStyle.Information)
            End If

            btnReconciliation.Enabled = False
            Call FillList()

            'Hemant (04 Aug 2023) -- Start
            'Call FillStatusCombo(dtStatus)
            'Hemant (04 Aug 2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReconciliation_Click", mstrModuleName)
        Finally
            objBankPaymentList = Nothing
            objPaymentBatchPostingTran = Nothing
            objPaymentBatchReconciliationTran = Nothing
        End Try
    End Sub

    'Hemant (04 Aug 2023) -- Start
    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If dgvDataList.Rows.Cast(Of DataGridViewRow).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no Employee to export."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgvDataList.Focus()
                Exit Sub
            End If

            Dim dTab As DataTable
            dTab = CType(dgvDataList.DataSource, DataTable).Copy

            If dTab IsNot Nothing Then
                If dTab.Columns.Contains("ischeck") Then
                    dTab.Columns.Remove("ischeck")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("periodunkid") Then
                    dTab.Columns.Remove("periodunkid")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("employeeunkid") Then
                    dTab.Columns.Remove("employeeunkid")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("branchunkid") Then
                    dTab.Columns.Remove("branchunkid")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("bankgroupunkid") Then
                    dTab.Columns.Remove("bankgroupunkid")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("statusunkid") Then
                    dTab.Columns.Remove("statusunkid")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("isvoid") Then
                    dTab.Columns.Remove("isvoid")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("voiduserunkid") Then
                    dTab.Columns.Remove("voiduserunkid")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("voiddatetime") Then
                    dTab.Columns.Remove("voiddatetime")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("voidreason") Then
                    dTab.Columns.Remove("voidreason")
                    dTab.AcceptChanges()
                End If
                If dTab.Columns.Contains("dpndtbeneficetranunkid") Then
                    dTab.Columns.Remove("dpndtbeneficetranunkid")
                    dTab.AcceptChanges()
                End If
            End If

            Dim dtReader As DataTable
           
            dtReader = dTab

            If dtReader IsNot Nothing Then
                Dim strBuilder As New System.Text.StringBuilder
                strBuilder.Append(" <HTML> " & vbCrLf)
                strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)

                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='2' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Reconciliation Report") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Period : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboPeriod.SelectedValue) > 0, cboPeriod.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Batch : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboBatch.SelectedValue) <> 0, cboBatch.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                If cboStatus.SelectedIndex > 0 Then
                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 14, "Status : ") & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboStatus.SelectedIndex) > 0, cboStatus.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)
                End If

                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" <BR/> " & vbCrLf)
                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                'strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                'strBuilder.Append(" <TD BORDER = 1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtReader.Columns.Count & "' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & cboBatch.Text & "</B></FONT></TD>" & vbCrLf)
                'strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For i As Integer = 0 To dgvDataList.Columns.Count - 1
                    If dgvDataList.Columns(i).Name = "objdgcolhSelect" OrElse dgvDataList.Columns(i).Name = "objdgcolhBankGroupUnkid" _
                       OrElse dgvDataList.Columns(i).Name = "objdgcolhBranchUnkid" OrElse dgvDataList.Columns(i).Name = "objdgcolhDpndtBeneficeTranUnkid" _
                      OrElse dgvDataList.Columns(i).Name = "dgcolhBranch" Then Continue For
                    If dgvDataList.Columns(i).Name = "objdgcolhBranchName" Then
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & dgcolhBranch.HeaderText & "</B></FONT></TD> " & vbCrLf)
                    Else
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & dgvDataList.Columns(i).HeaderText & "</B></FONT></TD> " & vbCrLf)

                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For j As Integer = 0 To dgvDataList.Rows.Count - 1
                    For i As Integer = 0 To dgvDataList.Columns.Count - 1
                        If dgvDataList.Columns(i).Name = "objdgcolhSelect" OrElse dgvDataList.Columns(i).Name = "objdgcolhBankGroupUnkid" _
                       OrElse dgvDataList.Columns(i).Name = "objdgcolhBranchUnkid" OrElse dgvDataList.Columns(i).Name = "objdgcolhDpndtBeneficeTranUnkid" _
                      OrElse dgvDataList.Columns(i).Name = "dgcolhBranch" Then Continue For
                        If dgvDataList.Columns(i).Name = "dgcolhAmount" Then
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & Format(CDbl(dgvDataList.Rows(j).Cells(i).Value), "###########################0.#0").ToString & "</FONT></TD> " & vbCrLf)
                        Else
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dgvDataList.Rows(j).Cells(i).Value & "</FONT></TD> " & vbCrLf)
                        End If

                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next
                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" </BODY> " & vbCrLf)
                strBuilder.Append(" </HTML> " & vbCrLf)

                Dim svDialog As New SaveFileDialog
                svDialog.Filter = "Excel files (*.xls)|*.xls"
                If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim strWriter As New IO.StreamWriter(fsFile)
                    With strWriter
                        .BaseStream.Seek(0, IO.SeekOrigin.End)
                        .WriteLine(strBuilder)
                        .Close()
                    End With
                    Diagnostics.Process.Start(svDialog.FileName)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (04 Aug 2023) -- End

#End Region

#Region "Combobox Event"

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objBranch As New clsbankbranch_master
        Try
            dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            With cboBankBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub Combo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cbo As ComboBox = TryCast(sender, ComboBox)
            If cbo IsNot Nothing Then
                If dgvDataList.CurrentCell.ColumnIndex = dgcolhBranch.Index Then

                    Dim intBankGroupid As Integer = -1
                    Dim intBranchUnkid As Integer = CInt(cbo.SelectedValue)
                    Dim objBranch As New clsbankbranch_master
                    Dim objBankGroup As New clspayrollgroup_master
                    objBranch._Branchunkid = intBranchUnkid
                    intBankGroupid = CInt(objBranch._Bankgroupunkid)
                    objBankGroup._Groupmasterunkid = intBankGroupid
                    dgvDataList.CurrentRow.Cells(dgcolhBankGroup.Index).Value = objBankGroup._Groupname


                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combo_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            dsCombos = objPaymentBatchPostingMaster.GetBatchNameList("List", CInt(cboPeriod.SelectedValue), clspaymentbatchposting_master.enApprovalStatus.Approved, True, True, , FinancialYear._Object._DatabaseName)
            With cboBatch
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                If mblnIsFromGrid = True Then
                    Dim drVoucher() As DataRow = dsCombos.Tables(0).Select("Name = '" & mstrVoucherNo & "'")
                    If drVoucher.Length > 0 Then
                        .SelectedValue = CInt(drVoucher(0).Item("id"))
                    End If
                Else
                    .SelectedValue = 0
                End If
            End With

            dgvDataList.DataSource = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboBatch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBatch.SelectedIndexChanged
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            If CInt(cboBatch.SelectedValue) > 0 Then
                objPaymentBatchPostingMaster._PaymentBatchPostingunkid = CInt(cboBatch.SelectedValue)
                mstrVoucherNo = objPaymentBatchPostingMaster._VoucherNo
                btnReconciliation.Enabled = True
                dgvDataList.DataSource = Nothing
                dtStatus = New DataTable
                Call FillStatusCombo(dtStatus)

            ElseIf CInt(cboBatch.SelectedValue) < 0 Then
                mstrVoucherNo = cboBatch.Text
                btnReconciliation.Enabled = True
                dgvDataList.DataSource = Nothing
                dtStatus = New DataTable
                Call FillStatusCombo(dtStatus)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatch_SelectedIndexChanged", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Sub

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) > 0 Then
                btnSubmitForApproval.Visible = False
                dgvDataList.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (21 Jul 2023) -- End

#End Region

#Region " DataGrid Events "

    Private Sub dgvDataList_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDataList.EditingControlShowing
        Try
            Select Case dgvDataList.CurrentCell.ColumnIndex
                Case dgcolhBranch.Index
                    If e.Control IsNot Nothing Then
                        Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                        'RemoveHandler cbo.SelectionChangeCommitted, AddressOf Combo_SelectionChangeCommitted
                        AddHandler cbo.SelectionChangeCommitted, AddressOf Combo_SelectionChangeCommitted
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvDataList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvDataList.DataError

    End Sub

    Private Sub dgvDataList_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case dgcolhBranch.Index
                    SendKeys.Send("{F2}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If dgvDataList.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "No Data records found."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If radApplyToAll.Checked = False AndAlso radApplyToChecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please either tick Apply To Checked OR Apply To All for further process."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If radApplyToChecked.Checked = True Then
                Dim dR() As DataRow = Nothing
                dR = CType(dgvDataList.DataSource, DataTable).Select("IsCheck = true")
                If dR.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one record from the list for further process."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Exit Sub
                End If
            End If

            If CInt(cboBankGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select atleast one Bank Group for further process."), enMsgBoxStyle.Information)
                cboBankGroup.Focus()
                Exit Sub
            End If

            If CInt(cboBankBranch.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select atleast one Bank Branch for further process."), enMsgBoxStyle.Information)
                cboBankBranch.Focus()
                Exit Sub
            End If

            SetCalculation(radApplyToAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_EditingControlShowing", mstrModuleName)
        End Try
    End Sub





   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbBankInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbBankInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.btnSubmitForApproval.GradientBackColor = GUI._ButttonBackColor
			Me.btnSubmitForApproval.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnApply.GradientBackColor = GUI._ButttonBackColor
			Me.btnApply.GradientForeColor = GUI._ButttonFontColor

			Me.btnReconciliation.GradientBackColor = GUI._ButttonBackColor
			Me.btnReconciliation.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSubmitForApproval.Text = Language._Object.getCaption(Me.btnSubmitForApproval.Name, Me.btnSubmitForApproval.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.gbBankInfo.Text = Language._Object.getCaption(Me.gbBankInfo.Name, Me.gbBankInfo.Text)
			Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.Name, Me.btnApply.Text)
			Me.radApplyToAll.Text = Language._Object.getCaption(Me.radApplyToAll.Name, Me.radApplyToAll.Text)
			Me.radApplyToChecked.Text = Language._Object.getCaption(Me.radApplyToChecked.Name, Me.radApplyToChecked.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
			Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
			Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
			Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
			Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
			Me.lblBankBranch.Text = Language._Object.getCaption(Me.lblBankBranch.Name, Me.lblBankBranch.Text)
			Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
			Me.btnReconciliation.Text = Language._Object.getCaption(Me.btnReconciliation.Name, Me.btnReconciliation.Text)
			Me.lblBatch.Text = Language._Object.getCaption(Me.lblBatch.Name, Me.lblBatch.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.dgcolhBatchId.HeaderText = Language._Object.getCaption(Me.dgcolhBatchId.Name, Me.dgcolhBatchId.HeaderText)
			Me.dgcolhEmployeeCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmployeeCode.Name, Me.dgcolhEmployeeCode.HeaderText)
			Me.dgcolhBankGroup.HeaderText = Language._Object.getCaption(Me.dgcolhBankGroup.Name, Me.dgcolhBankGroup.HeaderText)
			Me.dgcolhBranch.HeaderText = Language._Object.getCaption(Me.dgcolhBranch.Name, Me.dgcolhBranch.HeaderText)
			Me.dgcolhAccountNo.HeaderText = Language._Object.getCaption(Me.dgcolhAccountNo.Name, Me.dgcolhAccountNo.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Atleast one Employee in the list is required for Approval.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You can not submit for Approval. Because Already Batch is submitted for Approval Process")
			Language.setMessage(mstrModuleName, 3, "No Data records found.")
			Language.setMessage(mstrModuleName, 4, "Please either tick Apply To Checked OR Apply To All for further process.")
			Language.setMessage(mstrModuleName, 5, "Please check atleast one record from the list for further process.")
			Language.setMessage(mstrModuleName, 6, "Please select atleast one Bank Group for further process.")
			Language.setMessage(mstrModuleName, 7, "Please select atleast one Bank Branch for further process.")
			Language.setMessage(mstrModuleName, 8, "Please select atleast one Batch for Reconciliation process.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Selected Batch is not Posted yet.")
			Language.setMessage(mstrModuleName, 10, "There is no Employee to export.")
			Language.setMessage(mstrModuleName, 11, "Reconciliation Report")
			Language.setMessage(mstrModuleName, 12, "Period :")
			Language.setMessage(mstrModuleName, 13, "Batch :")
			Language.setMessage(mstrModuleName, 14, "Status :")
			Language.setMessage(mstrModuleName, 15, "Please select Batch.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class