#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSunAccountProjectJVReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSunAccountProjectJVReport"
    Private objSunAccount As clsSunAccountProjectJVReport

    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String


    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderByGName As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrSearchText As String = ""
#End Region

#Region " Constructor "

    Public Sub New()
        objSunAccount = New clsSunAccountProjectJVReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objSunAccount.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objBudget As New clsBudget_MasterNew
        Dim objPeriod As New clscommom_period_Tran
        Dim objHead As New clsTransactionHead
        Dim dsCombo As DataSet
        Try
            'Sohail (02 Aug 2017) -- Start
            'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
            'dsCombo = objBudget.GetComboList("Budget", False, True)
            dsCombo = objBudget.GetComboList("Budget", False, False)
            Dim intDefaultBudget As Integer = objBudget.GetDefaultBudgetID()
            'Sohail (02 Aug 2017) -- End
            With cboBudget
                .ValueMember = "budgetunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Budget")
                'Sohail (02 Aug 2017) -- Start
                'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
                'If .Items.Count > 0 Then .SelectedIndex = 0
                If .Items.Count > 0 Then
                    If intDefaultBudget > 0 Then
                        .SelectedValue = 0
                    Else
                        .SelectedIndex = 0
                    End If
                End If
                'Sohail (02 Aug 2017) -- End
            End With

            dsCombo = objHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True)
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Head")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objBudget = Nothing
            objPeriod = Nothing
            objHead = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            objSunAccount.setDefaultOrderBy(0)
            txtOrderBy.Text = objSunAccount.OrderByDisplay
            If cboBudget.Items.Count > 0 Then cboBudget.SelectedIndex = 0

            cboTranHead.SelectedValue = 0
            Call SetDefaultSearchText()

            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderByGName = ""
            mstrReport_GroupName = ""

            mstrAdvanceFilter = ""
            'Hemant (22 Mar 2019) -- Start
            'ISSUE#3620 : Add the "ignore zero rows" in the SUN JV account report for MST (Marie Stopes Tanzania).
            chkIgnoreZero.Checked = True
            'Hemant (22 Mar 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText()
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 5, "Type to Search")
            With cboTranHead
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmSalaryBudgetBreakdownt_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSunAccount = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryBudgetBreakdownt_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryBudgetBreakdownt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            eZeeHeader.Title = objSunAccount._ReportName
            eZeeHeader.Message = objSunAccount._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryBudgetBreakdownt_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBudget.SelectedIndexChanged
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim objBudget As New clsBudget_MasterNew
        Dim dsCombo As DataSet
        Try
            dsCombo = objBudgetCodes.GetComboListPeriod("Period", True, CInt(cboBudget.SelectedValue))
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With

            objBudget._Budgetunkid = CInt(cboBudget.SelectedValue)

            If objBudget._Viewbyid = enBudgetViewBy.Employee Then
                lnkAnalysisBy.Visible = True
            Else
                lnkAnalysisBy.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBudget_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.GotFocus
        Try
            With cboTranHead
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.Leave
        Try
            If CInt(cboTranHead.SelectedValue) <= 0 Then
                Call SetDefaultSearchText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTranHead.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboTranHead.ValueMember
                    .DisplayMember = cboTranHead.DisplayMember
                    .DataSource = CType(cboTranHead.DataSource, DataTable)
                    .CodeMember = "code"

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cboTranHead.SelectedValue = frm.SelectedValue
                    cboTranHead.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboTranHead.Text = ""
                    cboTranHead.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If cboBudget.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, There is no Default budget. Please set Default budget from Budget List screen"), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf cboBudget.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Budget is compulsory infomation. Please select Budget to continue."), enMsgBoxStyle.Information)
                cboBudget.Focus()
                Exit Sub
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory infomation. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            ElseIf CInt(cboTranHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Transaction head. Transaction head is mandatory information."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Exit Try
            End If


            objSunAccount.SetDefaultValue()

            objSunAccount._BudgetId = CInt(cboBudget.SelectedValue)
            objSunAccount._BudgetName = cboBudget.Text

            objSunAccount._PeriodId = CInt(cboPeriod.SelectedValue)
            objSunAccount._PeriodName = cboPeriod.Text

            'Sohail (12 Sep 2017) -- Start
            'Issue - 69.1 - Selected head value was not coming on sun account project jv report.
            objSunAccount._TranHeadId = CInt(cboTranHead.SelectedValue)
            objSunAccount._TranHeadName = cboTranHead.Text
            'Sohail (12 Sep 2017) -- End

            'Hemant (22 Mar 2019) -- Start
            'ISSUE#3620 : Add the "ignore zero rows" in the SUN JV account report for MST (Marie Stopes Tanzania).
            objSunAccount._IgnoreZero = chkIgnoreZero.Checked
            'Hemant (22 Mar 2019) -- End

            objSunAccount._ViewByIds = mstrStringIds
            objSunAccount._ViewIndex = mintViewIdx
            objSunAccount._ViewByName = mstrStringName
            objSunAccount._Analysis_Fields = mstrAnalysis_Fields
            objSunAccount._Analysis_Join = mstrAnalysis_Join
            objSunAccount._Analysis_OrderBy = mstrAnalysis_OrderBy
            objSunAccount._Analysis_OrderByGName = mstrAnalysis_OrderByGName
            objSunAccount._Report_GroupName = mstrReport_GroupName


            'objReconciliation._Advance_Filter = mstrAdvanceFilter
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            If objSunAccount.Export_Report(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, True, GUI.fmtCurrency, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._Base_CurrencyId) = False Then
                If objSunAccount._Message.Trim <> "" Then
                    eZeeMsgBox.Show(objSunAccount._Message, enMsgBoxStyle.Information)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollVariance.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollVariance"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objSunAccount.setOrderBy(0)
            txtOrderBy.Text = objSunAccount.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control Event "

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderByGName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblBudget.Text = Language._Object.getCaption(Me.lblBudget.Name, Me.lblBudget.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.Name, Me.chkIgnoreZero.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, There is no Default budget. Please set Default budget from Budget List screen")
			Language.setMessage(mstrModuleName, 2, "Budget is compulsory infomation. Please select Budget to continue.")
			Language.setMessage(mstrModuleName, 3, "Period is compulsory infomation. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 4, "Please Select Transaction head. Transaction head is mandatory information.")
			Language.setMessage(mstrModuleName, 5, "Type to Search")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
