Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsEDDetailReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEDDetailReport"
    Private mstrReportId As String = enArutiReport.EDDetail_Report
    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass 'Sohail (25 Mar 2013)

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintModeId As Integer = -1
    Private mstrModeName As String = ""
    Private mintHeadID As Integer = -1
    Private mstrHeadName As String = ""
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private mstrOrderByQuery As String = ""

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnIsActive As Boolean = True
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (20 Apr 2012) -- End

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrHeadCode As String = String.Empty
    'S.SANDEEP [ 12 MAY 2012 ] -- END


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes
    Private mintToPeriodId As Integer = -1
    Private mstrToPeriodName As String = ""
    Private mstrPeriodID As String = ""
    'Pinkal (03-Sep-2012) -- End

    Private mblnIncludeNegativeHeads As Boolean = True 'Sohail (19 Dec 2012)


    'Pinkal (09-Jan-2013) -- Start
    'Enhancement : TRA Changes
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Pinkal (09-Jan-2013) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'Sohail (25 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mintUserUnkid As Integer = User._Object._Userunkid
    Private mintCompanyUnkid As Integer = Company._Object._Companyunkid
    Private mstrTranDatabaseName As String = FinancialYear._Object._DatabaseName
    'Sohail (25 Mar 2013) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdicYearDBName As New Dictionary(Of Integer, String)
    'Sohail (21 Aug 2015) -- End


    'Pinkal (08-Jun-2013) -- Start
    'Enhancement : TRA Changes
    Private mintMembershipId As Integer = -1
    Private mstrMembershipName As String = ""
    'Pinkal (08-Jun-2013) -- End

    Private marrDatabaseName As New ArrayList 'Sohail (03 Dec 2013)

    Private mstrUserAccessFilter As String = "" 'Sohail (31 Jan 2014)
    'Sohail (09 Feb 2016) -- Start
    'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
    Private mblnIsLogo As Boolean = True
    Private mblnIsCompanyInfo As Boolean = False
    Private mblnIsLogoCompanyInfo As Boolean = False
    Private mintPayslipTemplate As Integer = 1
    'Sohail (09 Feb 2016) -- End
    Private mblnApplyUserAccessFilter As Boolean = True 'Sohail (13 Feb 2016) 
    'Sohail (04 Aug 2016) -- Start
    'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'Sohail (04 Aug 2016) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ModeId() As Integer
        Set(ByVal value As Integer)
            mintModeId = value
        End Set
    End Property

    Public WriteOnly Property _ModeName() As String
        Set(ByVal value As String)
            mstrModeName = value
        End Set
    End Property

    Public WriteOnly Property _HeadId() As Integer
        Set(ByVal value As Integer)
            mintHeadID = value
        End Set
    End Property

    Public WriteOnly Property _HeadName() As String
        Set(ByVal value As String)
            mstrHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (20 Apr 2012) -- End

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _HeadCode() As String
        Set(ByVal value As String)
            mstrHeadCode = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END



    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _mstrPeriodID() As String
        Set(ByVal value As String)
            mstrPeriodID = value
        End Set
    End Property

    'Pinkal (03-Sep-2012) -- End

    'Sohail (19 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _IncludeNegativeHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeNegativeHeads = value
        End Set
    End Property
    'Sohail (19 Dec 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'Sohail (25 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TranDatabaseName() As String
        Set(ByVal value As String)
            mstrTranDatabaseName = value
        End Set
    End Property
    'Sohail (25 Mar 2013) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public WriteOnly Property _mdicYearDBName() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicYearDBName = value
        End Set
    End Property
    'Sohail (21 Aug 2015) -- End


    'Pinkal (08-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MemberShipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property
    'Pinkal (08-Jun-2013) -- End

    'Sohail (03 Dec 2013) -- Start
    'Enhancement - TBC
    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property
    'Sohail (03 Dec 2013) -- End

    'Sohail (31 Jan 2014) -- Start
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'Sohail (31 Jan 2014) -- End

    'Sohail (09 Feb 2016) -- Start
    'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
    Public WriteOnly Property _IsOnlyLogo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLogo = value
        End Set
    End Property
    Public WriteOnly Property _IsOnlyCompanyInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCompanyInfo = value
        End Set
    End Property
    Public WriteOnly Property _IsLogoCompanyInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLogoCompanyInfo = value
        End Set
    End Property

    Public WriteOnly Property _PayslipTemplate() As Integer
        Set(ByVal value As Integer)
            mintPayslipTemplate = value
        End Set
    End Property
    'Sohail (09 Feb 2016) -- End

    'Sohail (13 Feb 2016) -- Start
    'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property
    'Sohail ((13 Feb 2016) -- End

    'Sohail (04 Aug 2016) -- Start
    'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'Sohail (04 Aug 2016) -- End
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintModeId = -1
            mstrModeName = ""
            mintHeadID = -1
            mstrHeadName = ""
            mintPeriodId = -1
            mstrPeriodName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End


            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            mintToPeriodId = -1
            mstrToPeriodName = ""
            'Pinkal (03-Sep-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End


            'Pinkal (08-Jun-2013) -- Start
            'Enhancement : TRA Changes
            mintMembershipId = -1
            mstrMembershipName = ""
            'Pinkal (08-Jun-2013) -- End
            mblnApplyUserAccessFilter = True 'Sohail (13 Feb 2016) 

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (08-Jun-2013) -- Start
    'Enhancement : TRA Changes

    'Private Sub FilterTitleAndFilterQuery()
    '    Me._FilterQuery = ""
    '    Me._FilterTitle = ""
    '    Try

    '        If mintEmployeeId > 0 Then
    '            Me._FilterQuery &= " AND vwPayroll.employeeunkId = @employeeunkId "
    '            objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
    '        End If

    '        If mintModeId > 0 Then
    '            'Sohail (04 Feb 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'Me._FilterQuery &= " AND prearningdeduction_master.trnheadtype_id = @tranheadunkid "
    '            'objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeId)
    '            Me._FilterQuery &= " AND vwPayroll.trnheadtype_id = @trnheadtype_id "
    '            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeId)
    '            'Sohail (04 Feb 2012) -- End
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Mode : ") & " " & mstrModeName & " "
    '        End If

    '        If mintHeadID > 0 Then
    '            'Sohail (04 Feb 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'Me._FilterQuery &= " AND prearningdeduction_master.tranheadunkid = @headunkid "
    '            'objDataOperation.AddParameter("@headunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHeadID)
    '            Me._FilterQuery &= " AND vwPayroll.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHeadID)
    '            'Sohail (04 Feb 2012) -- End
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Head : ") & " " & mstrHeadName & " "
    '        End If



    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If mintPeriodId > 0 Then
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "From Period : ") & " " & mstrPeriodName & " "
    '        End If

    '        If mintToPeriodId > 0 Then
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "To Period : ") & " " & mstrToPeriodName & " "
    '        End If

    '        Me._FilterQuery &= " AND vwPayroll.payperiodunkid IN (" & mstrPeriodID & " ) "

    '        Me._FilterQuery &= "  ) AS A WHERE  ROWNO = 1 "

    '        'Pinkal (03-Sep-2012) -- End

    '        If mblnIsActive = False Then
    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '        End If



    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'If Me.OrderByQuery <> "" Then
    '        '    Me._FilterTitle &= ""
    '        '    If mintViewIndex > 0 Then
    '        '        If Me.OrderByQuery.Trim = "" Then
    '        '            Me._FilterQuery &= "ORDER BY GNAME"
    '        '        Else
    '        '            Me._FilterQuery &= "ORDER BY GNAME, " & Me.OrderByQuery
    '        '        End If

    '        '    Else
    '        '        Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
    '        '    End If
    '        'End If


    '        If Me.OrderByQuery <> "" Then
    '            Me._FilterTitle &= ""
    '            If mintViewIndex > 0 Then
    '                If Me.OrderByQuery.Trim = "" Then
    '                    Me._FilterQuery &= "ORDER BY payperiodunkid,GNAME"
    '                Else
    '                    Me._FilterQuery &= "ORDER BY payperiodunkid,GNAME, " & Me.OrderByQuery
    '                End If

    '            Else
    '                Me._FilterQuery &= "ORDER BY payperiodunkid," & Me.OrderByQuery
    '            End If
    '        End If

    '        'Pinkal (03-Sep-2012) -- End


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""
        Try

            Dim mstrheadID As String = ""
            Dim mstrModeID As String = ""

            If mintMembershipId > 0 Then
                Dim objMemberShip As New clsmembership_master
                objMemberShip._Membershipunkid = mintMembershipId
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Membership : ") & " " & mstrMembershipName & " "
                mstrheadID = "," & objMemberShip._ETransHead_Id.ToString() & "," & objMemberShip._CTransHead_Id.ToString()
                mstrModeID = "," & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions
            End If

            If mintEmployeeId > 0 Then
                'Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId " 'Sohail (15 Oct 2020)
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintModeId > 0 Then
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                'Me._FilterQuery &= " AND prtranhead_master.trnheadtype_id IN (" & mintModeId & mstrModeID & ")"
                If mintModeId = 99999 Then
                    Me._FilterQuery &= " AND prpayrollprocess_tran.activityunkid > 0 "
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    'Sohail (07 Jun 2021) -- Start
                    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                    'Me._FilterQuery &= " AND prpayrollprocess_tran.crprocesstranunkid > 0 "
                    Me._FilterQuery &= " AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) "
                    'Sohail (07 JUn 2021) -- End
                    'Sohail (23 Jun 2015) -- End
                Else
                    Me._FilterQuery &= " AND prtranhead_master.trnheadtype_id IN (" & mintModeId & mstrModeID & ")"
                End If
                'Sohail (10 Sep 2014) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Mode : ") & " " & mstrModeName & " "
            End If

            If mintHeadID > 0 Then
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                'Me._FilterQuery &= " AND prtranhead_master.tranheadunkid IN  ( " & mintHeadID & mstrheadID & ")"
                If mintModeId = 99999 Then
                    Me._FilterQuery &= " AND prpayrollprocess_tran.activityunkid IN  ( " & mintHeadID & mstrheadID & ")"
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    'Sohail (07 Jun 2021) -- Start
                    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                    'Me._FilterQuery &= " AND cmexpense_master.expenseunkid IN  ( " & mintHeadID & mstrheadID & ")"
                    Me._FilterQuery &= " AND (cmexpense_master.expenseunkid IN  ( " & mintHeadID & mstrheadID & ") OR crretireexpense.expenseunkid IN  ( " & mintHeadID & mstrheadID & ")) "
                    'Sohail (07 JUn 2021) -- End
                    'Sohail (23 Jun 2015) -- End
                Else
                    Me._FilterQuery &= " AND prtranhead_master.tranheadunkid IN  ( " & mintHeadID & mstrheadID & ")"
                End If
                'Sohail (10 Sep 2014) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Head : ") & " " & mstrHeadName & " "
            End If

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "From Period : ") & " " & mstrPeriodName & " "
            End If

            If mintToPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "To Period : ") & " " & mstrToPeriodName & " "
            End If

            Me._FilterQuery &= " AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodID & " ) "

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC
            'Me._FilterQuery &= "  ) AS A WHERE  ROWNO = 1 "
            'Sohail (03 Dec 2013) -- End

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                'Sohail (03 Dec 2013) -- Start
                'Enhancement - TBC
                'If mintViewIndex > 0 Then
                '    If Me.OrderByQuery.Trim = "" Then
                '        Me._FilterQuery &= "ORDER BY payperiodunkid,GNAME"
                '    Else
                '        Me._FilterQuery &= "ORDER BY payperiodunkid,GNAME, " & Me.OrderByQuery
                '    End If

                'Else
                '    Me._FilterQuery &= "ORDER BY payperiodunkid," & Me.OrderByQuery
                'End If
                If mintViewIndex > 0 Then
                    If Me.OrderByQuery.Trim = "" Then
                        mstrOrderByQuery &= "ORDER BY payperiodunkid,GNAME"
                    Else
                        mstrOrderByQuery &= "ORDER BY payperiodunkid,GNAME, " & Me.OrderByQuery
                    End If

                Else
                    mstrOrderByQuery &= "ORDER BY payperiodunkid," & Me.OrderByQuery
                End If
                'Sohail (03 Dec 2013) -- End
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (08-Jun-2013) -- End

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Sohail (25 Mar 2013) -- Start
        '    'TRA - ENHANCEMENT
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid
        '    User._Object._Userunkid = mintUserUnkid
        '    'Sohail (25 Mar 2013) -- End

        '    'Pinkal (09-Jan-2013) -- Start
        '    'Enhancement : TRA Changes

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    'Pinkal (09-Jan-2013) -- End


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt 'Sohail (25 Mar 2013)

        '    If Not IsNothing(objRpt) Then

        '        'Pinkal (09-Jan-2013) -- Start
        '        'Enhancement : TRA Changes

        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")
        '        'Sohail (24 Jan 2013) -- Start
        '        'TRA - ENHANCEMENT
        '        'Dim strSubTotal As String = Language.getMessage(mstrModuleName, 27, "Period Count:")
        '        'Dim strSubGroupTotal As String = Language.getMessage(mstrModuleName, 27, "Period Count:")
        '        'Sohail (24 Jan 2013) -- End
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell
        '        'Sohail (24 Jan 2013) -- Start
        '        'TRA - ENHANCEMENT
        '        Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '        Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing
        '        'Sohail (24 Jan 2013) -- End


        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intGroupColumn As Integer = 0

        '            If mintViewIndex > 0 Then
        '                'Sohail (18 Jul 2013) -- Start
        '                'TRA - ENHANCEMENT
        '                'intGroupColumn = mdtTableExcel.Columns.Count - 5
        '                If mintMembershipId > 0 Then
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 6
        '                Else
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 5
        '                End If
        '                'Sohail (18 Jul 2013) -- End
        '                Dim strGrpCols As String() = {"column1", "column10"}
        '                strarrGroupColumns = strGrpCols
        '                'Sohail (24 Jan 2013) -- Start
        '                'TRA - ENHANCEMENT
        '                'strSubTotal = Language.getMessage(mstrModuleName, 20, "Sub Count :")
        '                Dim arr() As String = {Language.getMessage(mstrModuleName, 20, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '                arrGroupCount = arr
        '                'Sohail (24 Jan 2013) -- End

        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 20, "Sub Total:"))
        '                objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                'Sohail (24 Jan 2013) -- Start
        '                'TRA - ENHANCEMENT
        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
        '                arrDicGroupExtraInfo = arrDic
        '                'Sohail (24 Jan 2013) -- End
        '            Else
        '                'Sohail (18 Jul 2013) -- Start
        '                'TRA - ENHANCEMENT
        '                'intGroupColumn = mdtTableExcel.Columns.Count - 4
        '                If mintMembershipId > 0 Then
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 5
        '                Else
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 4
        '                End If
        '                'Sohail (18 Jul 2013) -- End
        '                Dim strGrpCols As String() = {"column10"}
        '                strarrGroupColumns = strGrpCols
        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                'Sohail (24 Jan 2013) -- Start
        '                'TRA - ENHANCEMENT
        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
        '                arrDicGroupExtraInfo = arrDic
        '                'Sohail (24 Jan 2013) -- End
        '            End If

        '            objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

        '    End If

        '    'Pinkal (09-Jan-2013) -- End

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Dim objUser As New clsUserAddEdit
        Dim objCompany As New clsCompany_Master 'Sohail (09 Feb 2016)

        Try

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            User._Object._Userunkid = mintUserUnkid

            objConfig._Companyunkid = xCompanyUnkid
            objUser._Userunkid = xUserUnkid
            objCompany._Companyunkid = xCompanyUnkid 'Sohail (09 Feb 2016)

            menExportAction = ExportAction
            mdtTableExcel = Nothing

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'objRpt = Generate_DetailReport(xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objUser._Username, objConfig._Base_CurrencyId)
            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            'objRpt = Generate_DetailReport(xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objUser._Username, objConfig._Base_CurrencyId, objCompany._Name)
            objRpt = Generate_DetailReport(xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, mblnApplyUserAccessFilter, objConfig._CurrencyFormat, objUser._Username, objConfig._Base_CurrencyId, objCompany._Name)
            'Sohail ((13 Feb 2016) -- End
            'Sohail (09 Feb 2016) -- End
            Rpt = objRpt

            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
                Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing


                If mdtTableExcel IsNot Nothing Then

                    Dim intGroupColumn As Integer = 0

                    If mintViewIndex > 0 Then
                        If mintMembershipId > 0 Then
                            intGroupColumn = mdtTableExcel.Columns.Count - 6
                        Else
                            intGroupColumn = mdtTableExcel.Columns.Count - 5
                        End If

                        Dim strGrpCols As String() = {"column1", "column10"}
                        strarrGroupColumns = strGrpCols

                        Dim arr() As String = {Language.getMessage(mstrModuleName, 20, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
                        arrGroupCount = arr

                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 20, "Sub Total:"))
                        objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
                        arrDicGroupExtraInfo = arrDic
                    Else
                        If mintMembershipId > 0 Then
                            intGroupColumn = mdtTableExcel.Columns.Count - 5
                        Else
                            intGroupColumn = mdtTableExcel.Columns.Count - 4
                        End If
                        Dim strGrpCols As String() = {"column10"}
                        strarrGroupColumns = strGrpCols
                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
                        arrDicGroupExtraInfo = arrDic
                    End If

                    objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------


                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 9, "Emp No")))
            'Sohail (04 Aug 2016) -- Start
            'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
            'iColumn_DetailReport.Add(New IColumn("surname", Language.getMessage(mstrModuleName, 10, "Surname")))
            'iColumn_DetailReport.Add(New IColumn("firstname", Language.getMessage(mstrModuleName, 11, "First Name")))
            'iColumn_DetailReport.Add(New IColumn("amount", Language.getMessage(mstrModuleName, 13, "Amount")))
            iColumn_DetailReport.Add(New IColumn("A.EmpName", Language.getMessage(mstrModuleName, 19, "Employee Name")))
            'Sohail (04 Aug 2016) -- End
            iColumn_DetailReport.Add(New IColumn("referenceno", Language.getMessage(mstrModuleName, 14, "Reference No.")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (08-Jun-2013) -- Start
    'Enhancement : TRA Changes

    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        'Sohail (25 Mar 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
    '        'Sohail (25 Mar 2013) -- End
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        Dim objPeriod As New clscommom_period_Tran

    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'objPeriod._Periodunkid = mintPeriodId
    '        'Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)

    '        objPeriod._Periodunkid = mintToPeriodId
    'Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)

    '        'Pinkal (03-Sep-2012) -- End


    '        objPeriod = Nothing


    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'StrQ &= "SELECT ISNULL(vwPayroll.employeeunkid, 0) employeeunkid " & _
    '        '            ", ISNULL(vwPayroll.employeecode, '') employeecode " & _
    '        '            ", ISNULL(hremployee_master.surname, '') surname " & _
    '        '            ", ISNULL(hremployee_master.firstname, '') firstname " & _
    '        '            ", ISNULL(hremployee_master.othername, '') initial " & _
    '        '            ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) amount " & _
    '        '            ", ISNULL(prearningdeduction_master.medicalrefno, '') referenceno " & _
    '        '          ", ISNULL(prearningdeduction_master.periodunkid, 0) AS EDPeriodUnkId " & _
    '        '            ", " & strEndDate & " AS end_date "

    '        'If mintViewIndex > 0 Then
    '        '    StrQ &= mstrAnalysis_Fields
    '        'Else
    '        '    StrQ &= ", 0 AS Id, '' AS GName "
    '        'End If


    '        'StrQ &= " FROM  vwPayroll " & _
    '        '            " JOIN prearningdeduction_master ON vwPayroll.employeeunkid = prearningdeduction_master.employeeunkid   " & _
    '        '            " AND vwPayroll.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '            " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
    '        '                                                    "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " AND cfcommon_period_tran.isactive = 1 " 'Sohail (08 May 2012) - [LEFT JOIN cfcommon_period_tran]


    '        StrQ &= " SELECT  A.employeeunkid  " & _
    '                     " , A.employeecode " & _
    '                     " , A.surname " & _
    '                     " , A.firstname " & _
    '                     " , A.initial " & _
    '                     " , A.amount " & _
    '                     " , A.referenceno " & _
    '                     " , A.Id " & _
    '                     " , A.GName " & _
    '                     " , A.payperiodunkid " & _
    '                     " , A.period_name " & _
    '                     " , A.ROWNO " & _
    '                     "  FROM    ( SELECT    ISNULL(vwPayroll.employeeunkid, 0) employeeunkid " & _
    '            ", ISNULL(vwPayroll.employeecode, '') employeecode " & _
    '            ", ISNULL(hremployee_master.surname, '') surname " & _
    '            ", ISNULL(hremployee_master.firstname, '') firstname " & _
    '            ", ISNULL(hremployee_master.othername, '') initial " & _
    '            ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) amount " & _
    '            ", ISNULL(prearningdeduction_master.medicalrefno, '') referenceno " & _
    '                                      ", vwPayroll.period_name " & _
    '                                      ", vwPayroll.payperiodunkid " & _
    '                                      ", DENSE_RANK() OVER ( PARTITION BY vwPayroll.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO "



    'If mintViewIndex > 0 Then
    '    StrQ &= mstrAnalysis_Fields
    'Else
    '    StrQ &= ", 0 AS Id, '' AS GName "
    'End If

    '        StrQ &= " FROM  " & mstrTranDatabaseName & "..vwPayroll " & _
    '                    " JOIN " & mstrTranDatabaseName & "..prearningdeduction_master ON vwPayroll.employeeunkid = prearningdeduction_master.employeeunkid   " & _
    '                      " AND " & mstrTranDatabaseName & "..vwPayroll.tranheadunkid = prearningdeduction_master.tranheadunkid  AND prearningdeduction_master.isvoid = 0 " & _
    '                    " JOIN " & mstrTranDatabaseName & "..hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                     "LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
    '                      " AND cfcommon_period_tran.modulerefid =" & enModuleReference.Payroll & "  AND cfcommon_period_tran.isactive = 1  "
    '        'Sohail (25 Mar 2013) - [mstrTranDatabaseName]
    '        'Pinkal (03-Sep-2012) -- End


    '        StrQ &= mstrAnalysis_Join


    '        'Sohail (19 Dec 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= " WHERE ISNULL(vwPayroll.amount, 0) > 0  AND prearningdeduction_master.isvoid = 0"
    '        StrQ &= " WHERE prearningdeduction_master.isvoid = 0 "
    '        If mblnIncludeNegativeHeads = True Then
    '            StrQ &= " AND ISNULL(vwPayroll.amount, 0) <> 0 "
    '        Else
    '            StrQ &= " AND ISNULL(vwPayroll.amount, 0) > 0 "
    '        End If
    '        'Sohail (19 Dec 2012) -- End
    '        StrQ &= " AND   CONVERT(CHAR(8),ISNULL(cfcommon_period_tran.end_date, '" & strEndDate & "'),112) <= '" & strEndDate & "' "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        If mblnIsActive = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Sohail (20 Apr 2012) -- End


    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery



    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If


    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes
    '        Dim dtList As DataTable = New DataView(dsList.Tables(0), "", "Gname", DataViewRowState.CurrentRows).ToTable()
    '        'Pinkal (03-Sep-2012) -- End



    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        Dim objED As New clsEarningDeduction 'Sohail (04 Feb 2012)
    '        Dim strPrevGroup As String = ""
    '        Dim decSubTotal As Decimal = 0


    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes

    '        Dim strPrevPeriod As String = ""
    '        Dim decPeriodTotal As Decimal = 0

    '        'Pinkal (03-Sep-2012) -- End



    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'For Each dtRow As DataRow In dsList.Tables(0).Rows
    '        For Each dtRow As DataRow In dtList.Rows
    '            'Pinkal (03-Sep-2012) -- End


    '            'Pinkal (03-Sep-2012) -- Start
    '            'Enhancement : TRA Changes

    '            'If objED.GetCurrentSlabEDPeriodUnkID(CInt(dtRow.Item("employeeunkid")), eZeeDate.convertDate(dtRow.Item("end_date").ToString)) <> CInt(dtRow.Item("EDPeriodUnkId")) Then
    '            '    Continue For
    '            'End If

    '            'Pinkal (03-Sep-2012) -- End

    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '            rpt_Row.Item("Column1") = dtRow.Item("GName")
    '            rpt_Row.Item("Column2") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column3") = dtRow.Item("surname")
    '            rpt_Row.Item("Column4") = dtRow.Item("firstname")
    '            rpt_Row.Item("Column5") = dtRow.Item("initial")
    '            'Sohail (25 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("amount")), mstrfmtCurrency)
    '            'Sohail (25 Mar 2013) -- End
    '            rpt_Row.Item("Column7") = dtRow.Item("referenceno")
    '            rpt_Row.Item("Column81") = CDec(dtRow.Item("amount"))

    '            If mintViewIndex > 0 Then
    '                If strPrevGroup <> dtRow.Item("GName").ToString Then
    '                    'Sohail (25 Mar 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'decSubTotal = CDec(Format(dtRow.Item("amount"), GUI.fmtCurrency))
    '                    decSubTotal = CDec(Format(dtRow.Item("amount"), mstrfmtCurrency))
    '                    'Sohail (25 Mar 2013) -- End
    '                Else
    '                    'Sohail (25 Mar 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'decSubTotal += CDec(Format(dtRow.Item("amount"), GUI.fmtCurrency))
    '                    decSubTotal += CDec(Format(dtRow.Item("amount"), mstrfmtCurrency))
    '                    'Sohail (25 Mar 2013) -- End
    '                End If
    '                strPrevGroup = dtRow.Item("GName").ToString
    '            End If
    '            'Sohail (25 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'rpt_Row.Item("Column8") = Format(decSubTotal, GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(decSubTotal, mstrfmtCurrency)
    '            'Sohail (25 Mar 2013) -- End


    '            'Pinkal (03-Sep-2012) -- Start
    '            'Enhancement : TRA Changes
    '            rpt_Row.Item("Column10") = dtRow.Item("period_name")

    '            If strPrevPeriod <> dtRow.Item("period_name").ToString Then
    '                decPeriodTotal = 0
    '                'Sohail (25 Mar 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'decPeriodTotal = CDec(Format(dtRow.Item("amount"), GUI.fmtCurrency))
    '                decPeriodTotal = CDec(Format(dtRow.Item("amount"), mstrfmtCurrency))
    '                'Sohail (25 Mar 2013) -- End
    '            Else
    '                'Sohail (25 Mar 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'decPeriodTotal += CDec(Format(dtRow.Item("amount"), GUI.fmtCurrency))
    '                decPeriodTotal += CDec(Format(dtRow.Item("amount"), mstrfmtCurrency))
    '                'Sohail (25 Mar 2013) -- End
    '            End If
    '            strPrevPeriod = dtRow.Item("period_name").ToString
    '            'Sohail (25 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'rpt_Row.Item("Column11") = Format(decPeriodTotal, GUI.fmtCurrency)
    '            rpt_Row.Item("Column11") = Format(decPeriodTotal, mstrfmtCurrency)
    '            'Sohail (25 Mar 2013) -- End

    '            'Pinkal (03-Sep-2012) -- End


    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '        Next


    '        objRpt = New ArutiReport.Designer.rptEDDetail


    '        If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
    '            'Sohail (25 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
    '            ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), mstrfmtCurrency))
    '            'Sohail (25 Mar 2013) -- End
    '        Else
    '            'Sohail (25 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(0, GUI.fmtCurrency))
    '            ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(0, mstrfmtCurrency))
    '            'Sohail (25 Mar 2013) -- End
    '        End If

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If



    '        'Pinkal (06-Feb-2012) -- Start
    '        'Enhancement : TRA Changes
    '        If mintViewIndex < 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If
    '        'Pinkal (06-Feb-2012) -- End


    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 5, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 6, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 7, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
    '        Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 10, "Surname"))
    '        Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "First Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtInitial", Language.getMessage(mstrModuleName, 12, "Initial"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 13, "Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtReferenceNo", Language.getMessage(mstrModuleName, 14, "Reference No."))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 18, "Sub Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 20, "Sub Count :"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotalCount", Language.getMessage(mstrModuleName, 21, "Total Count :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)



    '        'Pinkal (03-Sep-2012) -- Start
    '        'Enhancement : TRA Changes
    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 25, "Period :"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriodTotal", Language.getMessage(mstrModuleName, 26, "Period Total:"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriodCount", Language.getMessage(mstrModuleName, 27, "Period Count:"))
    '        'Pinkal (03-Sep-2012) -- End


    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & "  [ " & "(" & mstrHeadCode & "), " & mstrHeadName & " ] ")
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        'Pinkal (09-Jan-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If menExportAction = enExportAction.ExcelExtra Then
    '            mdtTableExcel = rpt_Data.Tables(0)

    '            Dim mintColumn As Integer = 0

    '            mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
    '            mdtTableExcel.Columns("Column2").SetOrdinal(0)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
    '            mdtTableExcel.Columns("Column3").SetOrdinal(1)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
    '            mdtTableExcel.Columns("Column4").SetOrdinal(2)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
    '            mdtTableExcel.Columns("Column5").SetOrdinal(3)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 13, "Amount")
    '            mdtTableExcel.Columns("Column81").SetOrdinal(4)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "Reference No.")
    '            mdtTableExcel.Columns("Column7").SetOrdinal(5)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 4, "Period :")
    '            mdtTableExcel.Columns("Column10").SetOrdinal(6)
    '            mintColumn += 1

    '            If mintViewIndex > 0 Then
    '                mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
    '                mdtTableExcel.Columns("Column1").SetOrdinal(7)
    '                mintColumn += 1
    '            End If


    '            For i = mintColumn To mdtTableExcel.Columns.Count - 1
    '                mdtTableExcel.Columns.RemoveAt(mintColumn)
    '            Next


    '        End If

    '        'Pinkal (09-Jan-2013) -- End



    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try

    'End Function

    'Pinkal (08-Jun-2013) -- End


    Public Function Generate_DetailReport(ByVal xUserUnkid As Integer _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As Boolean _
                                          , ByVal strfmtCurrency As String _
                                          , ByVal strUserName As String _
                                          , ByVal intBase_CurrencyId As Integer _
                                          , ByVal strCompanyName As String _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (09 Feb 2016) - [strCompanyName]
        'Sohail (21 Aug 2015) - [xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, strUserName, intBase_CurrencyId]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrInnerQ As String = ""
        Dim strDatabaseName As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mstrfmtCurrency = strfmtCurrency
            mintBase_CurrencyId = intBase_CurrencyId
            'Sohail (21 Aug 2015) -- End

            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            Dim objPeriod As New clscommom_period_Tran

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = mintToPeriodId
            objPeriod._Periodunkid(strDatabaseName) = mintToPeriodId
            'Sohail (21 Aug 2015) -- End
            Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)

            objPeriod = Nothing

            'Sohail (31 Jan 2014) -- Start
            If mstrUserAccessFilter.Trim = "" Then
                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            End If
            'Sohail (31 Jan 2014) -- End

            'Sohail (15 Oct 2020) -- Start
            'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
            Dim mstrheadID As String = ""
            Dim mstrModeID As String = ""

            If mintMembershipId > 0 Then
                Dim objMemberShip As New clsmembership_master
                objMemberShip._Membershipunkid = mintMembershipId
                mstrheadID = "," & objMemberShip._ETransHead_Id.ToString() & "," & objMemberShip._CTransHead_Id.ToString()
                mstrModeID = "," & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions
            End If

            StrQ = ""
            For Each key In mdicYearDBName
                strDatabaseName = key.Value

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , key.Value)
                If blnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, key.Value, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                'Hemant (13 Oct 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA): A1X-1402 - E&D Detailed report performance optimization: Earning for Employee
                If mstrAdvance_Filter.Trim.Length > 0 Then
                    'Hemant (13 Oct 2023) -- End
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, key.Value)
                End If 'Hemant (13 Oct 2023)

                StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                                ", ISNULL(hremployee_master.employeecode, '') AS employeecode "

                If mblnFirstNamethenSurname = True Then
                    StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                Else
                    StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
                End If

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "INTO #" & strDatabaseName & " "

                StrQ &= "FROM  " & strDatabaseName & "..hremployee_master "

                StrQ &= mstrAnalysis_Join

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE 1 = 1 "

                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkId = @employeeunkId "
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If mintModeId = 99999 Then
                ElseIf mintModeId = 99998 Then
                Else
                    StrQ &= "; SELECT DISTINCT * " & _
                                "INTO #" & strDatabaseName & "ED " & _
                                "FROM ( " & _
                                    "SELECT prpayrollprocess_tran.employeeunkid " & _
                                        ", prpayrollprocess_tran.tranheadunkid " & _
                                        ", prtnaleave_tran.payperiodunkid " & _
                                        "/*, prtranhead_master.trnheadtype_id*/ " & _
                                        ", ISNULL(prearningdeduction_master.medicalrefno, '') AS medicalrefno " & _
                                        ", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY ISNULL(edperiod.end_date, '" & strEndDate & "') DESC ) AS ROWNO " & _
                                    "FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN #" & strDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & strDatabaseName & ".employeeunkid " & _
                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
                                    "/*LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid*/ " & _
                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                    " LEFT JOIN " & strDatabaseName & "..prearningdeduction_master ON prpayrollprocess_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
                                    " AND prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                                    " LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = prearningdeduction_master.periodunkid "
                    'Sohail (27 Apr 2021) -[; SELECT *] = [; SELECT DISTINCT *]

                    StrQ &= " WHERE  prpayrollprocess_tran.isvoid = 0 " & _
                            " AND prtnaleave_tran.isvoid = 0 " & _
                            " AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodID & " ) " & _
                            " AND prearningdeduction_master.isvoid = 0 " & _
                             " AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                             " AND edperiod.end_date <=  cfcommon_period_tran.end_date "

                    If mblnIncludeNegativeHeads = True Then
                        StrQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0) <> 0 "
                    Else
                        StrQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0)  > 0 "
                    End If

                    StrQ &= " AND   CONVERT(CHAR(8), edperiod.end_date, 112) <= '" & strEndDate & "' "

                    StrQ &= " ) AS ED " & _
                            "WHERE ED.ROWNO = 1 "

                    If mintHeadID > 0 Then
                        StrQ &= " AND ED.tranheadunkid IN  ( " & mintHeadID & mstrheadID & ")" & _
                                " /*AND ED.trnheadtype_id IN (" & mintModeId & mstrModeID & ")*/"
                    End If
                End If

            Next
            'Sohail (15 Oct 2020) -- End

            StrQ &= " SELECT  A.employeeunkid  " & _
                         " , A.employeecode " & _
                         " , A.EmpName " & _
                            ", SUM(A.amount) AS amount " & _
                         " , A.referenceno " & _
                         " , A.Id " & _
                         " , A.GName " & _
                         " , A.payperiodunkid " & _
                         " , A.period_name " & _
                         " , A.ROWNO " & _
                         " , A.tranheadunkid  "
            'Sohail (04 Aug 2016) -- Start
            'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
            '" , A.surname " & _
            '" , A.firstname " & _
            '" , A.initial " & _
            'Sohail (04 Aug 2016) -- End

            If mintMembershipId > 0 Then
                StrQ &= " , A.membershipunkid " & _
                             " , A.membershipno "
            End If
            StrQ &= " FROM ( "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'For i = 0 To marrDatabaseName.Count - 1
            '    strDatabaseName = marrDatabaseName(i)
            Dim i As Integer = -1
            For Each key In mdicYearDBName
                strDatabaseName = key.Value
                i += 1
                'Sohail (21 Aug 2015) -- End

                'Sohail (15 Oct 2020) -- Start
                'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , strDatabaseName)
                ''Sohail (13 Feb 2016) -- Start
                ''Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
                ''Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, strDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                'If blnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, strDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                ''Sohail ((13 Feb 2016) -- End
                'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, strDatabaseName)
                ''Sohail (21 Aug 2015) -- End
                'Sohail (15 Oct 2020) -- End

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= "SELECT    ISNULL(#" & strDatabaseName & ".employeeunkid, 0) AS employeeunkid " & _
                     "             , ISNULL(#" & strDatabaseName & ".employeecode, '') AS employeecode " & _
                     "             , CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) amount " & _
                     "             , cfcommon_period_tran.period_name " & _
                     "             , prtnaleave_tran.payperiodunkid "
                'Sohail (27 Apr 2021) - [CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))] = [prpayrollprocess_tran.amount AS amount]
                'Sohail (04 Aug 2016) -- Start
                'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
                '", ISNULL(hremployee_master.surname, '') surname " & _
                '", ISNULL(hremployee_master.firstname, '') firstname " & _
                '", ISNULL(hremployee_master.othername, '') initial " & _
                'Sohail (15 Oct 2020) -- Start
                'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                'If mblnFirstNamethenSurname = True Then
                '    StrInnerQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                'Else
                '    StrInnerQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
                'End If
                StrInnerQ &= ", #" & strDatabaseName & ".EmpName "
                'Sohail (15 Oct 2020) -- End
                'Sohail (04 Aug 2016) -- End

                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                '", ISNULL(prearningdeduction_master.medicalrefno, '') referenceno " & _
                '", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY edperiod.end_date DESC ) AS ROWNO " & _
                '"             , ISNULL(prtranhead_master.tranheadunkid,0) tranheadunkid "
                If mintModeId = 99999 Then
                    StrInnerQ &= " , '' referenceno  " & _
                                 ", 1 AS ROWNO " & _
                                 ", prpayrollprocess_tran.activityunkid AS tranheadunkid "
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= " , '' referenceno  " & _
                                ", 1 AS ROWNO " & _
                                ", ISNULL(cmexpense_master.expenseunkid, ISNULL(crretireexpense.expenseunkid, -1)) AS tranheadunkid "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    StrInnerQ &= " , ISNULL(#" & strDatabaseName & "ED.medicalrefno, '') referenceno " & _
                                 ", 1 AS ROWNO " & _
                                 ", ISNULL(prpayrollprocess_tran.tranheadunkid,0) AS tranheadunkid "
                    'Sohail (15 Oct 2020) - [, DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY ISNULL(edperiod.end_date, '" & strEndDate & "') DESC ) AS ROWNO] = [1 AS ROWNO]
                End If
                'Sohail (10 Sep 2014) -- End

                If mintMembershipId > 0 Then
                    StrInnerQ &= "   , ISNULL(hremployee_meminfo_tran.membershipunkid,0) membershipunkid " & _
                             "              , ISNULL(hremployee_meminfo_tran.membershipno,'') membershipno "

                End If

                'Sohail (15 Oct 2020) -- Start
                'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                'If mintViewIndex > 0 Then
                '    StrInnerQ &= mstrAnalysis_Fields
                'Else
                '    StrInnerQ &= ", 0 AS Id, '' AS GName "
                StrInnerQ &= ", #" & strDatabaseName & ".Id, #" & strDatabaseName & ".GName "
                'End If
                'Sohail (15 Oct 2020) -- End

                StrInnerQ &= "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN #" & strDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & strDatabaseName & ".employeeunkid " & _
                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   "
                'Sohail (15 Oct 2020) - [LEFT JOIN " & strDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid] = [JOIN #" & strDatabaseName & "]

                If mintMembershipId > 0 Then

                    StrInnerQ &= "      LEFT JOIN " & strDatabaseName & "..hremployee_Meminfo_tran ON prpayrollprocess_tran.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
                             " AND hremployee_Meminfo_tran.membershipunkid = " & mintMembershipId & _
                             " AND hremployee_Meminfo_tran.isactive = 1 "

                End If

                StrInnerQ &= "          LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                '" JOIN " & mstrTranDatabaseName & "..prearningdeduction_master ON prpayrollprocess_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
                '" AND prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                '" LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = prearningdeduction_master.periodunkid " & _
                '" AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                '                    "AND edperiod.end_date <=  cfcommon_period_tran.end_date " & _
                '" LEFT JOIN " & mstrTranDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                If mintModeId = 99999 Then
                    'Do Nothing
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= "LEFT JOIN " & strDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                        "AND prpayrollprocess_tran.crprocesstranunkid > 0 " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid " & _
                                        "AND prpayrollprocess_tran.crretirementprocessunkid > 0 "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    'Sohail (15 Oct 2020) -- Start
                    'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                    'StrInnerQ &= " LEFT JOIN " & strDatabaseName & "..prearningdeduction_master ON prpayrollprocess_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
                    '        " AND prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                    '        " LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = prearningdeduction_master.periodunkid "
                    StrInnerQ &= " LEFT JOIN #" & strDatabaseName & "ED ON prpayrollprocess_tran.employeeunkid = #" & strDatabaseName & "ED.employeeunkid " & _
                                    " AND prpayrollprocess_tran.tranheadunkid = #" & strDatabaseName & "ED.tranheadunkid " & _
                                    " AND prtnaleave_tran.payperiodunkid = #" & strDatabaseName & "ED.payperiodunkid "
                    'Sohail (15 Oct 2020) -- End
                    StrInnerQ &= " LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                    'Sohail (12 Jan 2015) -- Start
                    'Issue : Amount is coming multiple times to real amount due to filter in join statement.
                    ' "AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                    '"AND edperiod.end_date <=  cfcommon_period_tran.end_date " & _
                    'Sohail (12 Jan 2015) -- End
                End If
                'Sohail (10 Sep 2014) -- End


                'Sohail (15 Oct 2020) -- Start
                'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                'StrInnerQ &= mstrAnalysis_Join

                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrInnerQ &= xDateJoinQry
                'End If

                ''S.SANDEEP [15 NOV 2016] -- START
                ''If xUACQry.Trim.Length > 0 Then
                ''    StrInnerQ &= xUACQry
                ''End If
                'If blnApplyUserAccessFilter = True Then
                '    If xUACQry.Trim.Length > 0 Then
                '        StrInnerQ &= xUACQry
                '    End If
                'End If
                ''S.SANDEEP [15 NOV 2016] -- END

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrInnerQ &= xAdvanceJoinQry
                'End If
                ''Sohail (21 Aug 2015) -- End
                'Sohail (15 Oct 2020) -- End

                StrInnerQ &= " WHERE  prtnaleave_tran.isvoid = 0 " & _
                             " AND prpayrollprocess_tran.isvoid = 0 "
                'Sohail (23 Jun 2015) -- Removed [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
                'Sohail (12 Jan 2015) -- [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                '" AND prearningdeduction_master.isvoid = 0 " & _
                '" AND ISNULL(prtranhead_master.isvoid, 0) = 0 "
                If mintModeId = 99999 Then
                    'Do Nothing
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                                 "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                                 "AND ISNULL(crretireexpense.isactive, 1) = 1 "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    'Sohail (15 Oct 2020) -- Start
                    'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                    'StrInnerQ &= " AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                    '         " AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    '         " AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                    '         " AND edperiod.end_date <=  cfcommon_period_tran.end_date "
                    'Sohail (15 Oct 2020) -- End
                    'Sohail (23 Jun 2015) - [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
                End If
                'Sohail (10 Sep 2014) -- End

                If mblnIncludeNegativeHeads = True Then
                    StrInnerQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0) <> 0 "
                Else
                    StrInnerQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0)  > 0 "
                End If

                'Sohail (20 Oct 2014) -- Start
                'Issue : Diffrence with head amount on payslip.
                'StrInnerQ &= " AND   CONVERT(CHAR(8),ISNULL(cfcommon_period_tran.end_date, '" & strEndDate & "'),112) <= '" & strEndDate & "' "
                'Sohail (23 Jun 2015) -- Start
                'Enhancement - Claim Request Expense on E&D Detail Reports.
                'StrInnerQ &= " AND   CONVERT(CHAR(8), edperiod.end_date, 112) <= '" & strEndDate & "' "
                If mintModeId = 99999 Then
                    'Do Nothing
                ElseIf mintModeId = 99998 Then
                    'Do Nothing
                Else
                    'StrInnerQ &= " AND   CONVERT(CHAR(8), edperiod.end_date, 112) <= '" & strEndDate & "' " 'Sohail (15 Oct 2020) 
                End If
                'Sohail (23 Jun 2015) -- End

                'Sohail (20 Oct 2014) -- End


                'Sohail (15 Oct 2020) -- Start
                'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                'Sohail (15 Oct 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                'Sohail (15 Oct 2020) -- Start
                'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                'Sohail (15 Oct 2020) -- End
                'Sohail (21 Aug 2015) -- End

                'Sohail (31 Jan 2014) -- Start
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End
                'Sohail (31 Jan 2014) -- End

                If i = 0 Then
                    Call FilterTitleAndFilterQuery()
                End If

                StrInnerQ &= Me._FilterQuery

            Next

            StrQ &= StrInnerQ

            StrQ &= "  ) AS A WHERE  ROWNO = 1 " & _
                    "GROUP BY A.employeeunkid " & _
                          ", A.employeecode " & _
                          ", A.EmpName " & _
                          ", A.referenceno " & _
                          ", A.Id " & _
                          ", A.GName " & _
                          ", A.payperiodunkid " & _
                          ", A.period_name " & _
                          ", A.ROWNO " & _
                          ", A.tranheadunkid "
            'Sohail (04 Aug 2016) -- Start
            'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
            '", A.surname " & _
            '", A.firstname " & _
            '", A.initial " & _
            'Sohail (04 Aug 2016) -- End

            If mintMembershipId > 0 Then
                StrQ &= ", A.membershipunkid " & _
                        ", A.membershipno "
            End If

            StrQ &= mstrOrderByQuery

            'Sohail (15 Oct 2020) -- Start
            'Sheer Logic performance issue # OLD-183 : - E&D report does not Export when many employees are selected.
            For Each key In mdicYearDBName
                StrQ &= " DROP TABLE #" & key.Value & " "

                If mintModeId = 99999 Then
                ElseIf mintModeId = 99998 Then
                Else
                    StrQ &= " DROP TABLE #" & key.Value & "ED "
                End If
            Next
            'Sohail (15 Oct 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dtList As DataTable = New DataView(dsList.Tables(0), "", "Gname", DataViewRowState.CurrentRows).ToTable()

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim objED As New clsEarningDeduction
            Dim strPrevGroup As String = ""
            Dim decSubTotal As Decimal = 0
            Dim strPrevPeriod As String = ""
            Dim decPeriodTotal As Decimal = 0

            For Each dtRow As DataRow In dtList.Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                'Sohail (04 Aug 2016) -- Start
                'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
                'rpt_Row.Item("Column3") = dtRow.Item("surname")
                'rpt_Row.Item("Column4") = dtRow.Item("firstname")
                'rpt_Row.Item("Column5") = dtRow.Item("initial")
                rpt_Row.Item("Column3") = dtRow.Item("EmpName")
                'Sohail (04 Aug 2016) -- End
                rpt_Row.Item("Column7") = dtRow.Item("referenceno")
                rpt_Row.Item("Column10") = dtRow.Item("period_name")


                If CInt(dtRow.Item("tranheadunkid")) <> mintHeadID Then Continue For

                rpt_Row.Item("Column6") = Format(CDec(dtRow("amount")), mstrfmtCurrency)
                rpt_Row.Item("Column81") = CDec(dtRow("amount"))

                If mintViewIndex > 0 Then
                    If strPrevGroup <> dtRow.Item("GName").ToString Then
                        decSubTotal = CDec(Format(dtRow("amount"), mstrfmtCurrency))
                    Else
                        decSubTotal += CDec(Format(dtRow("amount"), mstrfmtCurrency))
                    End If
                    strPrevGroup = dtRow.Item("GName").ToString
                End If

                rpt_Row.Item("Column8") = Format(decSubTotal, mstrfmtCurrency)


                If strPrevPeriod <> dtRow.Item("period_name").ToString Then
                    decPeriodTotal = 0
                    decPeriodTotal = CDec(Format(dtRow("amount"), mstrfmtCurrency))
                Else
                    decPeriodTotal += CDec(Format(dtRow("amount"), mstrfmtCurrency))
                End If

                strPrevPeriod = dtRow.Item("period_name").ToString
                rpt_Row.Item("Column11") = Format(decPeriodTotal, mstrfmtCurrency)

                If mintMembershipId > 0 Then
                    Dim drMemberShip() As DataRow = dtList.Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND membershipunkid = " & mintMembershipId)
                    If drMemberShip.Length > 0 Then
                        rpt_Row.Item("Column12") = drMemberShip(0)("membershipno").ToString()
                    Else
                        rpt_Row.Item("Column12") = ""
                    End If
                Else
                    rpt_Row.Item("Column12") = ""
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next


            objRpt = New ArutiReport.Designer.rptEDDetail


            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), mstrfmtCurrency))
            Else
                ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(0, mstrfmtCurrency))
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'ReportFunction.Logo_Display(objRpt, _
            '                            ConfigParameter._Object._IsDisplayLogo, _
            '                            ConfigParameter._Object._ShowLogoRightSide, _
            '                            "arutiLogo1", _
            '                            "arutiLogo2", _
            '                            arrImageRow, _
            '                            "txtCompanyName", _
            '                            "txtReportName", _
            '                            "txtFilterDescription", _
            '                            ConfigParameter._Object._GetLeftMargin, _
            '                            ConfigParameter._Object._GetRightMargin)
            If mblnIsLogoCompanyInfo = True Then

                ReportFunction.Logo_Display(objRpt, _
                                            ConfigParameter._Object._IsDisplayLogo, _
                                            ConfigParameter._Object._ShowLogoRightSide, _
                                            "arutiLogo1", _
                                            "arutiLogo2", _
                                            arrImageRow, _
                                            "txtCompanyName", _
                                            "txtReportName", _
                                            "txtFilterDescription", _
                                            ConfigParameter._Object._GetLeftMargin, _
                                            ConfigParameter._Object._GetRightMargin)

            ElseIf mblnIsLogo = True Then

                ReportFunction.Logo_Display(objRpt, _
                                           ConfigParameter._Object._IsDisplayLogo, _
                                           False, _
                                           "arutiLogo1", _
                                           "arutiLogo2", _
                                           arrImageRow, _
                                           "txtCompanyName", _
                                           "txtReportName", _
                                           "txtFilterDescription", _
                                           ConfigParameter._Object._GetLeftMargin, _
                                           ConfigParameter._Object._GetRightMargin)

            End If
            'Sohail (09 Feb 2016) -- End

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            If mblnIsCompanyInfo = True Then
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = False
                Call ReportFunction.TextChange(objRpt, "txtCompanyName1", strCompanyName)
                Call ReportFunction.TextChange(objRpt, "txtReportName1", Me._ReportName)
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = True 'Sohail (07 Mar 2016)

            ElseIf mblnIsLogo = True Then
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = False 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtReportName2", mstrReportTypeName) 'Sohail (07 Mar 2016)
                If mintPayslipTemplate = enPayslipTemplate.ONE_SIDED_KBC_13 Then
                    objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = False
                    Call ReportFunction.TextChange(objRpt, "txtCompanyName2", strCompanyName)
                End If

            ElseIf mblnIsLogoCompanyInfo = True Then
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = True
                Call ReportFunction.TextChange(objRpt, "txtCompanyName1", strCompanyName)
                Call ReportFunction.TextChange(objRpt, "txtReportName1", Me._ReportName)
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = True 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtCompanyName", strCompanyName) 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName) 'Sohail (07 Mar 2016)
            End If
            'Sohail (09 Feb 2016) -- End

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 5, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 6, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 7, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mintMembershipId <= 0 Then
                Call ReportFunction.EnableSuppress(objRpt, "txtMembershipNo", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column121", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtMembershipNo", Language.getMessage(mstrModuleName, 28, "Membership No"))
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
            'Sohail (04 Aug 2016) -- Start
            'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
            'Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 10, "Surname"))
            'Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "First Name"))
            'Call ReportFunction.TextChange(objRpt, "txtInitial", Language.getMessage(mstrModuleName, 12, "Initial"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 19, "Employee Name"))
            'Sohail (04 Aug 2016) -- End
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 13, "Amount"))
            Call ReportFunction.TextChange(objRpt, "txtReferenceNo", Language.getMessage(mstrModuleName, 14, "Reference No."))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 18, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 20, "Sub Count :"))
            Call ReportFunction.TextChange(objRpt, "txtTotalCount", Language.getMessage(mstrModuleName, 21, "Total Count :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 25, "Period :"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodTotal", Language.getMessage(mstrModuleName, 26, "Period Total:"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodCount", Language.getMessage(mstrModuleName, 27, "Period Count:"))


            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", strUserName)
            'Sohail (09 Feb 2016) -- End
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & "  [ " & "(" & mstrHeadCode & "), " & mstrHeadName & " ] ")
            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", strCompanyName)
            'Sohail (09 Feb 2016) -- End
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription1", Me._FilterTitle) 'Sohail (09 Feb 2016)

            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                'Sohail (04 Aug 2016) -- Start
                'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
                'mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
                'mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
                'mintColumn += 1

                'mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
                'mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
                'mintColumn += 1

                'mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
                'mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
                'mintColumn += 1
                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 19, "Employee Name")
                mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
                mintColumn += 1
                'Sohail (04 Aug 2016) -- End

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 13, "Amount")
                mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "Reference No.")
                mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
                mintColumn += 1

                If mintMembershipId > 0 Then
                    mdtTableExcel.Columns("Column12").Caption = Language.getMessage(mstrModuleName, 28, "Membership No.")
                    mdtTableExcel.Columns("Column12").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If

                mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 4, "Period :")
                mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If


                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next


            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function

#Region " Generate_DetailReport - Before giving Closed FY Periods on 03 Dec 2013 "
    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0

    '        objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        Dim objPeriod As New clscommom_period_Tran

    '        objPeriod._Periodunkid = mintToPeriodId
    '        Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)

    '        objPeriod = Nothing


    '        StrQ &= " SELECT  A.employeeunkid  " & _
    '                     " , A.employeecode " & _
    '                     " , A.surname " & _
    '                     " , A.firstname " & _
    '                     " , A.initial " & _
    '                     " , A.amount " & _
    '                     " , A.referenceno " & _
    '                     " , A.Id " & _
    '                     " , A.GName " & _
    '                     " , A.payperiodunkid " & _
    '                     " , A.period_name " & _
    '                     " , A.ROWNO " & _
    '                     " , A.tranheadunkid  "

    '        If mintMembershipId > 0 Then
    '            StrQ &= " , A.membershipunkid " & _
    '                         " , A.membershipno "
    '        End If

    '        StrQ &= " FROM ( SELECT    ISNULL(hremployee_master.employeeunkid, 0) employeeunkid " & _
    '                 "             , ISNULL(hremployee_master.employeecode, '') employeecode " & _
    '                    ", ISNULL(hremployee_master.surname, '') surname " & _
    '                    ", ISNULL(hremployee_master.firstname, '') firstname " & _
    '                    ", ISNULL(hremployee_master.othername, '') initial " & _
    '                 "             , CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) amount " & _
    '                    ", ISNULL(prearningdeduction_master.medicalrefno, '') referenceno " & _
    '                 "             , cfcommon_period_tran.period_name " & _
    '                 "             , prtnaleave_tran.payperiodunkid " & _
    '                 "             , DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid ORDER BY edperiod.end_date DESC ) AS ROWNO " & _
    '                 "             , ISNULL(prtranhead_master.tranheadunkid,0) tranheadunkid "


    '        If mintMembershipId > 0 Then
    '            StrQ &= "             , ISNULL(hremployee_meminfo_tran.membershipunkid,0) membershipunkid " & _
    '                         "              , ISNULL(hremployee_meminfo_tran.membershipno,'') membershipno "

    '        End If


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        StrQ &= " FROM  " & mstrTranDatabaseName & "..prpayrollprocess_tran " & _
    '                    " JOIN " & mstrTranDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
    '                    " LEFT JOIN " & mstrTranDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "

    '        If mintMembershipId > 0 Then

    '            StrQ &= " LEFT JOIN " & mstrTranDatabaseName & "..hremployee_Meminfo_tran ON prpayrollprocess_tran.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
    '                         " AND hremployee_Meminfo_tran.membershipunkid = " & mintMembershipId & _
    '                         " AND hremployee_Meminfo_tran.isactive = 1 "
    '            'Sohail (18 Jul 2013) - [To include Non-Payroll Membership] " AND prpayrollprocess_tran.membershiptranunkid = hremployee_Meminfo_tran.membershiptranunkid AND hremployee_Meminfo_tran.membershipunkid = " & mintMembershipId & _

    '        End If

    '        StrQ &= " LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
    '                        " JOIN " & mstrTranDatabaseName & "..prearningdeduction_master ON prpayrollprocess_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                        " AND prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                        " LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = prearningdeduction_master.periodunkid " & _
    '                        " AND edperiod.modulerefid = " & enModuleReference.Payroll & _
    '                        " LEFT JOIN " & mstrTranDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE  prtnaleave_tran.isvoid = 0 " & _
    '                         " AND prpayrollprocess_tran.isvoid = 0 " & _
    '                         " AND prearningdeduction_master.isvoid = 0 " & _
    '                         " AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

    '        If mblnIncludeNegativeHeads = True Then
    '            StrQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0) <> 0 "
    '        Else
    '            StrQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0)  > 0 "
    '        End If

    '        StrQ &= " AND   CONVERT(CHAR(8),ISNULL(cfcommon_period_tran.end_date, '" & strEndDate & "'),112) <= '" & strEndDate & "' "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If mblnIsActive = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        Dim dtList As DataTable = New DataView(dsList.Tables(0), "", "Gname", DataViewRowState.CurrentRows).ToTable()

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        Dim objED As New clsEarningDeduction
    '        Dim strPrevGroup As String = ""
    '        Dim decSubTotal As Decimal = 0
    '        Dim strPrevPeriod As String = ""
    '        Dim decPeriodTotal As Decimal = 0
    '        Dim dicEmpAmount As New Dictionary(Of Integer, Decimal)

    '        For Each dtRow As DataRow In dtList.Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("GName")
    '            rpt_Row.Item("Column2") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column3") = dtRow.Item("surname")
    '            rpt_Row.Item("Column4") = dtRow.Item("firstname")
    '            rpt_Row.Item("Column5") = dtRow.Item("initial")
    '            rpt_Row.Item("Column7") = dtRow.Item("referenceno")
    '            rpt_Row.Item("Column10") = dtRow.Item("period_name")




    '            Dim dr() As DataRow = dtList.Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND tranheadunkid = " & mintHeadID)

    '            If dr.Length > 0 Then

    '                If dicEmpAmount.ContainsKey(CInt(dtRow.Item("employeeunkid"))) = True Then
    '                    Continue For
    '                Else
    '                    dicEmpAmount.Add(CInt(dtRow.Item("employeeunkid")), CDec(dr(0)("amount")))
    '                End If


    '                rpt_Row.Item("Column6") = Format(CDec(dr(0)("amount")), mstrfmtCurrency)
    '                rpt_Row.Item("Column81") = CDec(dr(0)("amount"))

    '                If mintViewIndex > 0 Then
    '                    If strPrevGroup <> dtRow.Item("GName").ToString Then
    '                        decSubTotal = CDec(Format(dr(0)("amount"), mstrfmtCurrency))
    '                    Else
    '                        decSubTotal += CDec(Format(dr(0)("amount"), mstrfmtCurrency))
    '                    End If
    '                    strPrevGroup = dtRow.Item("GName").ToString
    '                End If

    '                rpt_Row.Item("Column8") = Format(decSubTotal, mstrfmtCurrency)


    '                If strPrevPeriod <> dtRow.Item("period_name").ToString Then
    '                    decPeriodTotal = 0
    '                    decPeriodTotal = CDec(Format(dr(0)("amount"), mstrfmtCurrency))
    '                Else
    '                    decPeriodTotal += CDec(Format(dr(0)("amount"), mstrfmtCurrency))
    '                End If

    '                strPrevPeriod = dtRow.Item("period_name").ToString
    '                rpt_Row.Item("Column11") = Format(decPeriodTotal, mstrfmtCurrency)

    '                If mintMembershipId > 0 Then
    '                    Dim drMemberShip() As DataRow = dtList.Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND membershipunkid = " & mintMembershipId)
    '                    If drMemberShip.Length > 0 Then
    '                        rpt_Row.Item("Column12") = drMemberShip(0)("membershipno").ToString()
    '                    Else
    '                        rpt_Row.Item("Column12") = ""
    '                    End If
    '                Else
    '                    rpt_Row.Item("Column12") = ""
    '                End If

    '            End If

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '        Next


    '        objRpt = New ArutiReport.Designer.rptEDDetail


    '        If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
    '            ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), mstrfmtCurrency))
    '        Else
    '            ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(0, mstrfmtCurrency))
    '        End If

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex < 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 5, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 6, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 7, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        If mintMembershipId <= 0 Then
    '            Call ReportFunction.EnableSuppress(objRpt, "txtMembershipNo", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column121", True)
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtMembershipNo", Language.getMessage(mstrModuleName, 28, "Membership No"))
    '        End If


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
    '        Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 10, "Surname"))
    '        Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "First Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtInitial", Language.getMessage(mstrModuleName, 12, "Initial"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 13, "Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtReferenceNo", Language.getMessage(mstrModuleName, 14, "Reference No."))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 18, "Sub Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 20, "Sub Count :"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotalCount", Language.getMessage(mstrModuleName, 21, "Total Count :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 25, "Period :"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriodTotal", Language.getMessage(mstrModuleName, 26, "Period Total:"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriodCount", Language.getMessage(mstrModuleName, 27, "Period Count:"))



    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & "  [ " & "(" & mstrHeadCode & "), " & mstrHeadName & " ] ")
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        If menExportAction = enExportAction.ExcelExtra Then
    '            mdtTableExcel = rpt_Data.Tables(0)

    '            Dim mintColumn As Integer = 0

    '            mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
    '            mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
    '            mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
    '            mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
    '            mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 13, "Amount")
    '            mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "Reference No.")
    '            mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            'Sohail (18 Jul 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintMembershipId > 0 Then
    '                mdtTableExcel.Columns("Column12").Caption = Language.getMessage(mstrModuleName, 28, "Membership No.")
    '                mdtTableExcel.Columns("Column12").SetOrdinal(mintColumn)
    '                mintColumn += 1
    '            End If
    '            'Sohail (18 Jul 2013) -- End

    '            mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 4, "Period :")
    '            mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            If mintViewIndex > 0 Then
    '                mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
    '                mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
    '                mintColumn += 1
    '            End If


    '            For i = mintColumn To mdtTableExcel.Columns.Count - 1
    '                mdtTableExcel.Columns.RemoveAt(mintColumn)
    '            Next


    '        End If

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try

    'End Function
#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Mode :")
            Language.setMessage(mstrModuleName, 3, "Head :")
            Language.setMessage(mstrModuleName, 4, "Period :")
            Language.setMessage(mstrModuleName, 5, "Prepared By :")
            Language.setMessage(mstrModuleName, 6, "Checked By :")
            Language.setMessage(mstrModuleName, 7, "Approved By :")
            Language.setMessage(mstrModuleName, 8, "Received By :")
            Language.setMessage(mstrModuleName, 9, "Emp No")
            Language.setMessage(mstrModuleName, 10, "Surname")
            Language.setMessage(mstrModuleName, 11, "First Name")
            Language.setMessage(mstrModuleName, 12, "Initial")
            Language.setMessage(mstrModuleName, 13, "Amount")
            Language.setMessage(mstrModuleName, 14, "Reference No.")
            Language.setMessage(mstrModuleName, 15, "Printed By :")
            Language.setMessage(mstrModuleName, 16, "Printed Date :")
            Language.setMessage(mstrModuleName, 17, "Page :")
            Language.setMessage(mstrModuleName, 18, "Sub Total :")
            Language.setMessage(mstrModuleName, 19, "Grand Total :")
            Language.setMessage(mstrModuleName, 20, "Sub Count :")
            Language.setMessage(mstrModuleName, 21, "Total Count :")
            Language.setMessage(mstrModuleName, 22, "Emp No.")
            Language.setMessage(mstrModuleName, 23, "From Period :")
            Language.setMessage(mstrModuleName, 24, "To Period :")
            Language.setMessage(mstrModuleName, 25, "Period :")
            Language.setMessage(mstrModuleName, 26, "Period Total:")
            Language.setMessage(mstrModuleName, 27, "Period Count:")


        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class
