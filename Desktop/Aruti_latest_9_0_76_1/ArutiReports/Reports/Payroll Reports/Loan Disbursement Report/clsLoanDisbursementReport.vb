﻿'Class Name : clsLoanDisbursementReport.vb
'Purpose    :
'Date       : 19-Mar-2024
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsLoanDisbursementReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsLoanDisbursementReport"
    Private mstrReportId As String = enArutiReport.Loan_Disbursement_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mdtApprovalDateFrom As Date = Nothing
    Private mdtApprovalDateTo As Date = Nothing
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLoanSchemeUnkid As Integer = 0
    Private mstrLoanSchemeName As String = ""
    Private mstrExportedFileName As String = String.Empty
#End Region

#Region " Properties "

    Public WriteOnly Property _ApprovalDateFrom() As Date
        Set(ByVal value As Date)
            mdtApprovalDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ApprovalDateTo() As Date
        Set(ByVal value As Date)
            mdtApprovalDateTo = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeName() As String
        Set(ByVal value As String)
            mstrLoanSchemeName = value
        End Set
    End Property

    Public ReadOnly Property _ExportedFileName() As String
        Get
            Return mstrExportedFileName
        End Get
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtApprovalDateFrom = Nothing
            mdtApprovalDateTo = Nothing
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mintLoanSchemeUnkid = 0
            mstrLoanSchemeName = ""


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtApprovalDateFrom <> Nothing Then
                objDataOperation.AddParameter("@ApprovalDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtApprovalDateFrom))
                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8),A.approvaldate,112),'') >= @ApprovalDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Date From :") & " " & mdtApprovalDateFrom.Date & " "
            End If

            If mdtApprovalDateTo <> Nothing Then
                objDataOperation.AddParameter("@ApprovalDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtApprovalDateTo))
                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8),A.approvaldate,112),'') <= @ApprovalDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Date To") & " " & mdtApprovalDateTo.Date & " "
            End If

            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintLoanSchemeUnkid > 0 Then
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeUnkid)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.loanschemeunkid = @loanschemeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Loan Type :") & " " & mstrLoanSchemeName & " "
            End If

            Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "Employee"
            OrderByQuery = " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As Date, _
                                    ByVal xPeriodEnd As Date, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal xExportReportPath As String, _
                                    ByVal xOpenReportAfterExport As Boolean)
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Loan")

            dtCol = New DataColumn("SrNo", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "SN")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "EmployeeName")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("LoanScheme", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Loan Type")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("LoanAmount", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Loan Amount")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("InterestRate", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Interest Rate")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("NoOfInstallment", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Tenure")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("RepaymentDays", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Repayment Type")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            StrQ &= "SELECT " & _
                    "   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '' ) AS Employee " & _
                    ",  ISNULL(lnloan_scheme_master.name, '')  AS LoanScheme " & _
                    ",  ISNULL(lnloan_process_pending_loan.loan_amount, 0)  AS LoanAmount " & _
                    ",  ISNULL(lnloan_process_pending_loan.interest_rate, 0)  AS InterestRate " & _
                    ",  ISNULL(lnloan_process_pending_loan.noofinstallment, 0)  AS noofinstallment " & _
                    ",  ISNULL(lnloan_process_pending_loan.repaymentdays, 0)  AS repaymentdays " & _
                    ",  lnloan_process_pending_loan.processpendingloanunkid " & _
                    ",  A.approvaldate " & _
                    " FROM lnloan_process_pending_loan  " & _
                    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                    " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid AND lnloan_scheme_master.isactive = 1 " & _
                    " LEFT JOIN ( select " & _
                    "               processpendingloanunkid " & _
                    "           ,   approvaldate " & _
                    "           ,   ROW_NUMBER() OVER (PARTITION BY processpendingloanunkid ORDER BY priority DESC) AS Rno " & _
                    "             FROM lnloanapproval_process_tran " & _
                    "             WHERE isvoid = 0 AND statusunkid = " & enLoanApplicationStatus.APPROVED & " " & _
                    "       UNION                   " & _
                    "             select " & _
                    "               processpendingloanunkid " & _
                    "           ,   approvaldate " & _
                    "           ,   ROW_NUMBER() OVER (PARTITION BY processpendingloanunkid ORDER BY priority DESC) AS Rno " & _
                    "             FROM lnroleloanapproval_process_tran " & _
                    "             WHERE isvoid = 0 AND statusunkid = " & enLoanApplicationStatus.APPROVED & " " & _
                    "           ) AS A on A.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid AND Rno = 1 " & _
                    " WHERE lnloan_process_pending_loan.isvoid = 0 " & _
                    " AND lnloan_process_pending_loan.loan_statusunkid IN ( " & enLoanApplicationStatus.APPROVED & "," & enLoanApplicationStatus.ASSIGNED & ") "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                Dim drBlankRow As DataRow = dsList.Tables(0).NewRow
                drBlankRow.Item("LoanAmount") = Format(0, GUI.fmtCurrency)
                drBlankRow.Item("InterestRate") = Format(0, GUI.fmtCurrency)
                dsList.Tables(0).Rows.Add(drBlankRow)
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intCount As Integer = 1
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("SrNo") = intCount
                rpt_Row.Item("EmployeeName") = drRow.Item("Employee")
                rpt_Row.Item("LoanScheme") = drRow.Item("LoanScheme")
                rpt_Row.Item("LoanAmount") = Format(CDec(drRow.Item("LoanAmount")), GUI.fmtCurrency)
                rpt_Row.Item("InterestRate") = Format(CDec(drRow.Item("InterestRate")), GUI.fmtCurrency)
                rpt_Row.Item("NoOfInstallment") = drRow.Item("noofinstallment")
                rpt_Row.Item("RepaymentDays") = drRow.Item("RepaymentDays")
                dtFinalTable.Rows.Add(rpt_Row)

                intCount = intCount + 1
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Using package As New OfficeOpenXml.ExcelPackage()
                Dim worksheet As OfficeOpenXml.ExcelWorksheet = package.Workbook.Worksheets.Add("Data")
                Dim xTable As DataTable = mdtTableExcel.Copy
                For Each iCol As DataColumn In xTable.Columns
                    If iCol.Ordinal <> xTable.Columns.Count - 1 Then
                        xTable.Columns(iCol.Ordinal).ColumnName = iCol.Caption.Trim()
                    End If
                Next

                worksheet.Cells("A1").LoadFromDataTable(xTable, True)
                worksheet.Cells("A1:XFD").AutoFilter = False
                worksheet.Cells(2, 1, xTable.Rows.Count + 1, xTable.Columns.Count - 2).Style.Numberformat.Format = "@"
                worksheet.Cells(2, xTable.Columns.Count - 1, xTable.Rows.Count + 1, xTable.Columns.Count - 1).Style.Numberformat.Format = "0"
                worksheet.Cells(2, xTable.Columns.Count, xTable.Rows.Count + 1, xTable.Columns.Count).Style.Numberformat.Format = "0"

                worksheet.Cells.AutoFitColumns(0)

                If Me._ReportName.Contains("/") = True Then
                    Me._ReportName = Me._ReportName.Replace("/", "_")
                End If

                If Me._ReportName.Contains("\") Then
                    Me._ReportName = Me._ReportName.Replace("\", "_")
                End If

                If Me._ReportName.Contains(":") Then
                    Me._ReportName = Me._ReportName.Replace(":", "_")
                End If
                Dim strExportFileName As String = ""
                strExportFileName = Me._ReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")

                xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsx"

                Dim fi As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                package.SaveAs(fi)

                mstrExportedFileName = strExportFileName & ".xlsx"

            End Using

            If IO.File.Exists(xExportReportPath) Then
                If xOpenReportAfterExport Then
                    Process.Start(xExportReportPath)
                End If
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "SN")
			Language.setMessage(mstrModuleName, 2, "EmployeeName")
			Language.setMessage(mstrModuleName, 3, "Loan Type")
			Language.setMessage(mstrModuleName, 4, "Loan Amount")
			Language.setMessage(mstrModuleName, 5, "Interest Rate")
			Language.setMessage(mstrModuleName, 6, "Tenure")
			Language.setMessage(mstrModuleName, 7, "Repayment Type")
			Language.setMessage(mstrModuleName, 8, "Date From :")
			Language.setMessage(mstrModuleName, 9, "Date To")
			Language.setMessage(mstrModuleName, 10, "Employee :")
			Language.setMessage(mstrModuleName, 11, "Loan Type :")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
