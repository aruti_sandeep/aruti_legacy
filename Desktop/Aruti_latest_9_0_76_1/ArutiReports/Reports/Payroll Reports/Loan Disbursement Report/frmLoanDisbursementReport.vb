﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmLoanDisbursementReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLoanDisbursementReport"
    Private objLoanDisbursementReport As clsLoanDisbursementReport

#End Region

#Region " Constructor "

    Public Sub New()
        objLoanDisbursementReport = New clsLoanDisbursementReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objLoanDisbursementReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmLoanDisbursementReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLoanDisbursementReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanDisbursementReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanDisbursementReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanDisbursementReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanDisbursementReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanDisbursementReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanDisbursementReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanDisbursementReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoanDisbursementReport.SetMessages()
            objfrm._Other_ModuleNames = "clsLoanDisbursementReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Try

            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     ConfigParameter._Object._UserAccessModeSetting, _
                                    True, True, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombo = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)

            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("LoanScheme")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objLoanScheme = Nothing
            objEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpApprovalDateFrom.Checked = False
            dtpApprovalDateTo.Checked = False
            cboEmployee.SelectedIndex = 0
            cboLoanScheme.SelectedIndex = 0
            objLoanDisbursementReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If dtpApprovalDateFrom.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set Approval Start Date. Approval Start Date is mandatory information."), enMsgBoxStyle.Information)
                dtpApprovalDateFrom.Focus()
                Exit Function
            End If

            If dtpApprovalDateTo.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please set Approval To Date. Approval To Date is mandatory information."), enMsgBoxStyle.Information)
                dtpApprovalDateFrom.Focus()
                Exit Function
            End If

            objLoanDisbursementReport.SetDefaultValue()

            If dtpApprovalDateFrom.Checked = True Then
                objLoanDisbursementReport._ApprovalDateFrom = dtpApprovalDateFrom.Value.Date
            End If
            If dtpApprovalDateTo.Checked = True Then
                objLoanDisbursementReport._ApprovalDateTo = dtpApprovalDateTo.Value.Date
            End If
            objLoanDisbursementReport._EmployeeUnkid = cboEmployee.SelectedValue
            objLoanDisbursementReport._EmployeeName = cboEmployee.Text
            objLoanDisbursementReport._LoanSchemeUnkid = cboLoanScheme.SelectedValue
            objLoanDisbursementReport._LoanSchemeName = cboLoanScheme.Text

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub


            objLoanDisbursementReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                  User._Object._Userunkid, _
                                                                  FinancialYear._Object._YearUnkid, _
                                                                  Company._Object._Companyunkid, _
                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                                  True, False, _
                                                                  ConfigParameter._Object._ExportReportPath, _
                                                                  ConfigParameter._Object._OpenAfterExport)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblApprovalDateTo.Text = Language._Object.getCaption(Me.lblApprovalDateTo.Name, Me.lblApprovalDateTo.Text)
            Me.lblApprovalDateFrom.Text = Language._Object.getCaption(Me.lblApprovalDateFrom.Name, Me.lblApprovalDateFrom.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class