﻿'************************************************************************************************************************************
'Class Name : clsFlexcubeLoanFormReport.vb
'Purpose    :
'Date       : 25/11/2022
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System

Public Class clsFlexcubeLoanFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsFlexcubeLoanFormReport"
    Private mstrReportId As String = enArutiReport.Flexcube_Loan_Form_Report
    Dim objDataOperation As clsDataOperation

#Region "Enum"
    Enum enLoanType
        General_Loan = 1
        Car_Loan = 2
        Refinancing_Mortgage_Loan = 3
        Purchase_Mortgage = 4
        Construction_Mortgage = 5
        'Hemant (17 Jan 2025) -- Start
        'ISSUE/ENHANCEMENT(NMB): A1X - 2968 :  Semi finished mortgage loan changes
        Semi_Finish_Mortgage = 6
        'Hemant (17 Jan 2025) -- End
    End Enum
#End Region

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintLoanTypeId As Integer = 0
    Private mstrLoanTypeName As String = ""

    Private mintEmployeeUnkId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLoanSchemeUnkId As Integer = 0
    Private mstrLoanSchemeName As String = ""
    Private mintProcessPendingLoanUnkid As Integer = 0
    Private mstrApplicationNo As String = ""
    Private mintIdentityUnkId As Integer = 0
    Private mstrIdentityName As String = ""

    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
#End Region

#Region " Properties "

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _LoanTypeId() As Integer
        Set(ByVal value As Integer)
            mintLoanTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LoanTypeName() As String
        Set(ByVal value As String)
            mstrLoanTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeUnkId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeName() As String
        Set(ByVal value As String)
            mstrLoanSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPendingLoanUnkid() As Integer
        Set(ByVal value As Integer)
            mintProcessPendingLoanUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ApplicationNo() As String
        Set(ByVal value As String)
            mstrApplicationNo = value
        End Set
    End Property

    Public WriteOnly Property _IdentityUnkId() As Integer
        Set(ByVal value As Integer)
            mintIdentityUnkId = value
        End Set
    End Property

    Public WriteOnly Property _IdentityName() As String
        Set(ByVal value As String)
            mstrIdentityName = value
        End Set
    End Property

    'Hemant (29 Mar 2024) -- Start
    'ENHANCEMENT(TADB): New loan application form
    Private mstrOracleHostName As String = ""
    Public WriteOnly Property _OracleHostName() As String
        Set(ByVal value As String)
            mstrOracleHostName = value
        End Set
    End Property

    Private mstrOraclePortNo As String = ""
    Public WriteOnly Property _OraclePortNo() As String
        Set(ByVal value As String)
            mstrOraclePortNo = value
        End Set
    End Property

    Private mstrOracleServiceName As String = ""
    Public WriteOnly Property OracleServiceName() As String
        Set(ByVal value As String)
            mstrOracleServiceName = value
        End Set
    End Property

    Private mstrOracleUserName As String = ""
    Public WriteOnly Property _OracleUserName() As String
        Set(ByVal value As String)
            mstrOracleUserName = value
        End Set
    End Property

    Private mstrOracleUserPassword As String = ""
    Public WriteOnly Property _OracleUserPassword() As String
        Set(ByVal value As String)
            mstrOracleUserPassword = value
        End Set
    End Property
    'Hemant (29 Mar 2024) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintLoanTypeId = 0
            mstrLoanTypeName = ""

            mintEmployeeUnkId = 0
            mstrEmployeeName = ""
            mintLoanSchemeUnkId = 0
            mstrLoanSchemeName = ""
            mintProcessPendingLoanUnkid = 0
            mstrApplicationNo = ""

            mintIdentityUnkId = 0
            mstrIdentityName = ""

            mstrOrderByQuery = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""

        Try

            If mintEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintLoanSchemeUnkId > 0 Then
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeUnkId)
                Me._FilterQuery &= " AND  lnloan_process_pending_loan.loanschemeunkid = @loanschemeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Loan Scheme :") & " " & mstrLoanSchemeName & " "
            End If

            If mintProcessPendingLoanUnkid > 0 Then
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Application No :") & " " & mstrApplicationNo & " "
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function NumberToWords(ByVal doubleNumber As Double) As String
        Dim beforeFloatingPoint = CInt(Math.Floor(doubleNumber))
        Dim beforeFloatingPointWord = String.Format("{0}", NumberToString(beforeFloatingPoint))
        Dim afterFloatingPointWord = String.Format("{0} cents ", GroupToWords(CInt(((doubleNumber - beforeFloatingPoint) * 100))))

        If CInt(((doubleNumber - beforeFloatingPoint) * 100)) > 0 Then
            Return String.Format("{0} and {1}", beforeFloatingPointWord, afterFloatingPointWord)
        Else
            Return String.Format("{0}", beforeFloatingPointWord)
        End If
    End Function

    Public Function NumberToString(ByVal num_str As String) As String ', Optional ByVal use_us_group_names As Boolean = True
        ' Get the appropiate group names.
        Dim groups() As String
        'If use_us_group_names Then
        '    groups = New String() {"", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion", "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "quattuordecillion", "quindecillion", "sexdecillion", "septendecillion", "octodecillion", "novemdecillion", "vigintillion"}
        'Else
        groups = New String() {"", "Thousand", "Million", "Milliard", "Billion", "1000 Billion", "Trillion", "1000 Trillion", "Quadrillion", "1000 Quadrillion", "Quintillion", "1000 Quintillion", "Sextillion", "1000 Sextillion", "Septillion", "1000 Septillion", "Octillion", "1000 Octillion", "Nonillion", "1000 Nonillion", "Decillion", "1000 Decillion"}
        'End If

        ' Clean the string a bit.
        ' Remove "$", ",", leading zeros, and
        ' anything after a decimal point.
        Const CURRENCY As String = "$"
        Const SEPARATOR As String = ","
        Const DECIMAL_POINT As String = "."
        num_str = num_str.Replace(CURRENCY, "").Replace(SEPARATOR, "")
        num_str = num_str.TrimStart(New Char() {"0"c})
        Dim pos As Integer = num_str.IndexOf(DECIMAL_POINT)
        If pos = 0 Then
            Return "zero"
        ElseIf pos > 0 Then
            num_str = num_str.Substring(0, pos - 1)
        End If

        ' See how many groups there will be.
        Dim num_groups As Integer = (num_str.Length + 2) \ 3

        ' Pad so length is a multiple of 3.
        num_str = num_str.PadLeft(num_groups * 3, " "c)

        ' Process the groups, largest first.
        Dim result As String = ""
        Dim group_num As Integer
        For group_num = num_groups - 1 To 0 Step -1
            ' Get the next three digits.
            Dim group_str As String = num_str.Substring(0, 3)
            num_str = num_str.Substring(3)
            Dim group_value As Integer = CInt(group_str)

            ' Convert the group into words.
            If group_value > 0 Then
                If group_num >= groups.Length Then
                    result &= GroupToWords(group_value) & _
                        " ?, "
                Else
                    result &= GroupToWords(group_value) & _
                        " " & groups(group_num) & ", "
                End If
            End If
        Next group_num

        ' Remove the trailing ", ".
        If result.EndsWith(", ") Then
            result = result.Substring(0, result.Length - 2)
        End If

        Return result.Trim()
    End Function

    Private Function GroupToWords(ByVal num As Integer) As String
        Static one_to_nineteen() As String = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen"}
        Static multiples_of_ten() As String = {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"}

        ' If the number is 0, return an empty string.
        If num = 0 Then Return ""

        ' Handle the hundreds digit.
        Dim digit As Integer
        Dim result As String = ""
        If num > 99 Then
            digit = num \ 100
            num = num Mod 100
            result = one_to_nineteen(digit) & " hundred"
        End If

        ' If num = 0, we have hundreds only.
        If num = 0 Then Return result.Trim()

        ' See if the rest is less than 20.
        If num < 20 Then
            ' Look up the correct name.
            result &= " " & one_to_nineteen(num)
        Else
            ' Handle the tens digit.
            digit = num \ 10
            num = num Mod 10
            result &= " " & multiples_of_ten(digit - 2)

            ' Handle the final digit.
            If num > 0 Then
                result &= " " & one_to_nineteen(num)
            End If
        End If

        Return result.Trim()
    End Function

    'Hemant (29 Mar 2024) -- Start
    'ENHANCEMENT(TADB): New loan application form
    Private Function GetFlexcubeLoanDetails(ByVal strBankAccountNo As String, ByVal strEmployeecode As String, ByVal strLoanSchemeCode As String) As DataSet
        Dim dsList As New DataSet()
        Dim objLoanAdvance As New clsLoan_Advance
        Try
            dsList = (New clsLoan_Advance).GetFlexcubeLoanData(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, strBankAccountNo, strEmployeecode, strLoanSchemeCode)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFlexcubeLoanDetails; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Hemant (29 Mar 2024) -- End


#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As Date _
                                     , ByVal xPeriodEnd As Date _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_LoanApprovers As ArutiReport.Designer.dsArutiReport
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try


            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            'Company._Object._Companyunkid = xCompanyUnkid
            'ConfigParameter._Object._Companyunkid = xCompanyUnkid

            'User._Object._Userunkid = xUserUnkid

            'Dim dsModule As DataSet = (New clsMasterData).GetModuleReference("List", Nothing, False)
            Dim objGrpMstr As New clsGroup_Master
            objGrpMstr._Groupunkid = 1
            Dim strGroupName = objGrpMstr._Groupname
            objGrpMstr = Nothing
            'Hemant (29 Mar 2024) -- End



            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()


            StrQ = "SELECT " & _
                    "   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' +ISNULL(hremployee_master.surname, '') AS EmployeeFullName " & _
                    "   ,hremployee_master.birthdate " & _
                    "   ,lnloan_process_pending_loan.loanschemeunkid " & _
                    "   ,lnloan_scheme_master.loanschemecategory_id " & _
                    "   ,hremployee_idinfo_tran.identity_no AS NationalID " & _
                    "   ,DefualtID.identity_no AS DefualtIDNo " & _
                    "   ,hremployee_master.appointeddate " & _
                    "   ,hremployee_master.employeecode " & _
                    "   ,CnfDt.date1 AS Confirmation_Date " & _
                    "   ,Bank.accountno AS accountno " & _
                    "   ,ISNULL(lnloan_process_pending_loan.loan_amount,0) AS loan_amount " & _
                    "   ,ISNULL(lnloan_process_pending_loan.noofinstallment,0) AS noofinstallment " & _
                    "   ,ISNULL(lnloan_process_pending_loan.interest_rate,0) AS interest_rate " & _
                    "   ,ISNULL(lnloan_process_pending_loan.istopup,0) AS istopup " & _
                    "   ,ISNULL(lnloan_process_pending_loan.emp_remark, 0) AS Purpose " & _
                    "   ,ISNULL(lnloan_scheme_master.code, '') AS LoanSchemeCode " & _
                    "   ,ISNULL(lnloan_scheme_master.name, '') AS LoanScheme " & _
                    "   ,ISNULL(lnloan_scheme_master.loanschemecategory_id, 0) AS loanschemecategoryid " & _
                    "   ,ISNULL(lnloan_process_pending_loan.plot_no, '') AS plot_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.block_no, '') AS block_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.street, '') AS street " & _
                    "   ,ISNULL(lnloan_process_pending_loan.town_city, '') AS town_city " & _
                    "   ,ISNULL(lnloan_process_pending_loan.ct_no, '') AS ct_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.lo_no, '') AS lo_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.market_value, 0) AS market_value " & _
                    "   ,ISNULL(lnloan_process_pending_loan.fsv, 0) AS fsv " & _
                    "   ,ISNULL(lnloan_process_pending_loan.model, '') AS model " & _
                    "   ,ISNULL(lnloan_process_pending_loan.yom, '') AS yom " & _
                    "   ,ISNULL(lnloan_process_pending_loan.chassis_no, '') AS chassis_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.colour, '') AS colour " & _
                    "   ,ISNULL(lnloan_process_pending_loan.supplier, '') AS supplier " & _
                    "   ,ISNULL(lnloan_process_pending_loan.total_cif, 0) AS total_cif " & _
                    "   ,ISNULL(lnloan_process_pending_loan.estimated_tax, 0) AS estimated_tax " & _
                    "   ,ISNULL(lnloan_process_pending_loan.invoice_value, 0) AS invoice_value " & _
                    "   ,ISNULL(lnloan_process_pending_loan.model_no, '') AS model_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.engine_no, '') AS engine_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.engine_capacity, 0) AS engine_capacity " & _
                    "   ,lnloan_process_pending_loan.employeeunkid " & _
                    "   ,lnloan_process_pending_loan.application_date " & _
                    "   ,lnloan_process_pending_loan.installmentamt " & _
                    "   ,ISNULL(lnloan_process_pending_loan.isliquidate, 0) AS isliquidate " & _
                    " FROM lnloan_process_pending_loan " & _
                    " JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                    " LEFT JOIN lnloan_scheme_master     ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                    " LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid	AND hremployee_idinfo_tran.idtypeunkid =  " & mintIdentityUnkId & " " & _
                    " LEFT JOIN hremployee_idinfo_tran DefualtID ON DefualtID.employeeunkid = hremployee_master.employeeunkid  AND DefualtID.isdefault = 1 " & _
                    " LEFT JOIN " & _
                    " ( " & _
                    "    SELECT " & _
                    "         date1 " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodStart) & "' " & _
                    " )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                    " LEFT JOIN  " & _
                    " ( " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "           ,accountno " & _
                    "           ,DENSE_RANK() OVER (PARTITION BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                    "       FROM premployee_bank_tran " & _
                    "       LEFT JOIN cfcommon_period_tran     ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "       WHERE ISNULL(isvoid, 0) = 0 " & _
                    "       AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    " ) AS Bank ON Bank.employeeunkid = hremployee_master.employeeunkid AND Bank.ROWNO = 1 " & _
                    " WHERE lnloan_process_pending_loan.isvoid = 0 "

            'Hemant (22 Nov 2024) -- [isliquidate]
            'Hemant (26 Apr 2024) -- [employeeunkid,application_date,installmentamt]
            'Hemant (29 Mar 2024) -- [DefualtIDNo,noofinstallment,interest_rate,LoanScheme,total_cif,estimated_tax,invoice_value,model_no,engine_no,engine_capacity,LoanSchemeCode]

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_LoanApprovers = New ArutiReport.Designer.dsArutiReport
            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                Dim strBankAccountNo As String = ""
                Dim strEmployeeCode As String = ""
                Dim strLoanSchemeCode As String = ""
                'Hemant (29 Mar 2024) -- End
                'Hemant (26 Apr 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
                Dim intEmployeeunkid As Integer = -1
                Dim dtApplicationDate As Date = Nothing
                Dim dtPeriodStartDate As Date = Nothing
                Dim dtPeriodEndDate As Date = Nothing
                Dim strLoanSchemeName As String = ""
                Dim intLoanSchemeCategory As Integer
                'Hemant (26 Apr 2024) -- End


                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("EmployeeFullName")
                rpt_Row.Item("Column2") = CDate(dtRow.Item("birthdate")).ToShortDateString
                rpt_Row.Item("Column3") = dtRow.Item("NationalID")
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                rpt_Row.Item("Column35") = dtRow.Item("DefualtIDNo")
                strEmployeeCode = dtRow.Item("employeecode")
                strBankAccountNo = dtRow.Item("accountno")
                strLoanSchemeCode = dtRow.Item("loanschemecode")
                'Hemant (29 Mar 2024) -- End
                'Hemant (26 Apr 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
                intEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                dtApplicationDate = CDate(dtRow.Item("application_date"))
                dtPeriodStartDate = New Date(dtApplicationDate.Year, dtApplicationDate.Month, 1)
                dtPeriodEndDate = dtPeriodStartDate.AddMonths(1)
                dtPeriodEndDate = dtPeriodEndDate.AddDays(-(dtPeriodEndDate.Day))
                strLoanSchemeName = dtRow.Item("loanscheme")
                intLoanSchemeCategory = CInt(dtRow.Item("loanschemecategoryid"))
                'Hemant (26 Apr 2024) -- End

                rpt_Row.Item("Column4") = CDate(dtRow.Item("appointeddate")).ToShortDateString()
                rpt_Row.Item("Column5") = dtRow.Item("employeecode")
                rpt_Row.Item("Column6") = CDate(dtRow.Item("Confirmation_Date")).ToShortDateString()
                rpt_Row.Item("Column11") = dtRow.Item("accountno")
                rpt_Row.Item("Column13") = CDec(dtRow.Item("loan_amount")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")) & " /="
                rpt_Row.Item("Column14") = NumberToWords(CDec(dtRow.Item("loan_amount"))) & " Only"
                rpt_Row.Item("Column15") = CBool(dtRow.Item("istopup"))
                rpt_Row.Item("Column16") = dtRow.Item("purpose")

                If mintLoanTypeId = enLoanType.General_Loan Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 1, "Unsecured General Loan")
                ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 2, "Car Loan")
                ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 3, "Refinancing Mortgage")
                ElseIf mintLoanTypeId = enLoanType.Purchase_Mortgage Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 4, "Purchase Mortgage")
                ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 5, "Construction Mortgage")
                    'Hemant (17 Jan 2025) -- Start
                    'ISSUE/ENHANCEMENT(NMB): A1X - 2968 :  Semi finished mortgage loan changes
                ElseIf mintLoanTypeId = enLoanType.Semi_Finish_Mortgage Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 6, "Semi-Finish Mortgage")
                    'Hemant (17 Jan 2025) -- End
                End If

                If CInt(dtRow.Item("loanschemecategoryid")) = enLoanSchemeCategories.SECURED Then
                    rpt_Row.Item("Column17") = dtRow.Item("EmployeeFullName")
                Else
                    rpt_Row.Item("Column17") = ""
                End If

                rpt_Row.Item("Column19") = dtRow.Item("plot_no")
                rpt_Row.Item("Column20") = dtRow.Item("block_no")
                rpt_Row.Item("Column21") = dtRow.Item("street")
                rpt_Row.Item("Column22") = dtRow.Item("town_city")
                rpt_Row.Item("Column23") = dtRow.Item("ct_no")
                rpt_Row.Item("Column24") = dtRow.Item("lo_no")
                rpt_Row.Item("Column25") = CDec(dtRow.Item("market_value")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                rpt_Row.Item("Column26") = CDec(dtRow.Item("fsv")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))

                rpt_Row.Item("Column27") = dtRow.Item("model")
                rpt_Row.Item("Column28") = dtRow.Item("yom")
                rpt_Row.Item("Column29") = dtRow.Item("chassis_no")
                rpt_Row.Item("Column30") = dtRow.Item("colour")
                rpt_Row.Item("Column31") = dtRow.Item("supplier")
                rpt_Row.Item("Column32") = dtRow.Item("loanschemecategoryid")
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                rpt_Row.Item("Column36") = dtRow.Item("NoOfInstallment")
                rpt_Row.Item("Column37") = CDec(dtRow.Item("interest_rate")).ToString("###.##")
                rpt_Row.Item("Column38") = dtRow.Item("LoanScheme")
                rpt_Row.Item("Column46") = CDec(dtRow.Item("total_cif")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                rpt_Row.Item("Column47") = CDec(dtRow.Item("estimated_tax")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                rpt_Row.Item("Column48") = CDec(dtRow.Item("invoice_value")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                rpt_Row.Item("Column49") = dtRow.Item("model_no")
                rpt_Row.Item("Column50") = dtRow.Item("engine_no")
                rpt_Row.Item("Column51") = CDec(dtRow.Item("engine_capacity")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                'Hemant (29 Mar 2024) -- End               
                'Hemant (26 Apr 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
                rpt_Row.Item("Column60") = CDec(dtRow.Item("installmentamt")).ToString("#,##0.00", New System.Globalization.CultureInfo("en-US")) & " /="
                'Hemant (26 Apr 2024) -- End

                '---------------------- Employee History Data -- Start--------------

                Dim dsEmployeeHistory As New DataSet
                objDataOperation.ClearParameters()

                StrQ = "SELECT " & _
                       "    athremployee_master.employeecode " & _
                       ",   ISNULL(athremployee_master.firstname, '') + ' ' + ISNULL(athremployee_master.othername, '') + ' ' + ISNULL(athremployee_master.surname, '') AS EmployeeName " & _
                       ",   isapplicant " & _
                       ",   iscentralized " & _
                       ",   isfinalapprover " & _
                       ",   empsignature " & _
                       ",   ISNULL(hrjob_master.job_name, '') as Job " & _
                       ",   ISNULL(hrclasses_master.name, '') as Class " & _
                       ",   ISNULL(hrsectiongroup_master.name, '') as SectionGroup " & _
                       ",   ISNULL(hrdepartment_master.name, '') as Department " & _
                       ",   ISNULL(athremployee_master.present_address1, '') AS present_address1 " & _
                       ",   ISNULL(athremployee_master.present_mobile, '') As present_mobile " & _
                       ",   ISNULL(athremployee_master.email, '') As Email " & _
                        "FROM lnloan_history_tran " & _
                        "LEFT JOIN athremployee_master ON athremployee_master.atemployeeunkid = lnloan_history_tran.atemployeeunkid " & _
                        "LEFT JOIN hremployee_categorization_tran ON hremployee_categorization_tran.categorizationtranunkid = lnloan_history_tran.categorizationtranunkid AND hremployee_categorization_tran.isvoid = 0 " & _
                        "LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                        "LEFT JOIN hremployee_transfer_tran ON hremployee_transfer_tran.transferunkid = lnloan_history_tran.transferunkid AND hremployee_transfer_tran.isvoid = 0 " & _
                        "LEFT JOIN hrclasses_master on hrclasses_master.classesunkid = hremployee_transfer_tran.classunkid AND hrclasses_master.isactive = 1 " & _
                        "LEFT JOIN hrsectiongroup_master on hrsectiongroup_master.sectiongroupunkid = hremployee_transfer_tran.sectiongroupunkid AND hrsectiongroup_master.isactive = 1 " & _
                        "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_transfer_tran.departmentunkid AND hrdepartment_master.isactive = 1 " & _
                        "WHERE lnloan_history_tran.isvoid = 0 "
                'Hemant (29 Mar 2024) -- [Department,email]

                If mintProcessPendingLoanUnkid > 0 Then
                    objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                    StrQ &= " AND lnloan_history_tran.processpendingloanunkid = @processpendingloanunkid "
                End If

                dsEmployeeHistory = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                Dim drEmployeeHistory() As DataRow = dsEmployeeHistory.Tables(0).Select("isapplicant = 1")
                If drEmployeeHistory.Length > 0 Then
                    rpt_Row.Item("Column7") = drEmployeeHistory(0).Item("Class")
                    rpt_Row.Item("Column8") = drEmployeeHistory(0).Item("SectionGroup")
                    rpt_Row.Item("Column9") = drEmployeeHistory(0).Item("Job")
                    rpt_Row.Item("Column10") = drEmployeeHistory(0).Item("present_address1")
                    rpt_Row.Item("Column12") = drEmployeeHistory(0).Item("present_mobile")
                    rpt_Row.Item("sign1") = drEmployeeHistory(0).Item("empsignature")
                    'Hemant (29 Mar 2024) -- Start
                    'ENHANCEMENT(TADB): New loan application form
                    rpt_Row.Item("Column33") = drEmployeeHistory(0).Item("Department")
                    rpt_Row.Item("Column34") = drEmployeeHistory(0).Item("Email")
                    'Hemant (29 Mar 2024) -- End

                End If

                Dim drCentralizedApproverHistory() As DataRow = dsEmployeeHistory.Tables(0).Select("iscentralized = 1")
                If drCentralizedApproverHistory.Length > 0 Then
                    rpt_Row.Item("sign2") = drCentralizedApproverHistory(0).Item("empsignature")
                End If


                '---------------------- Employee History Data -- End--------------

                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                If strGroupName.ToUpper.Trim = "TADB" Then
                    Dim strTermCondition1 As String = "Following your request for a #LoanSchemName# Facility, Tanzania Agricultural Development Bank (hereinafter called ""the Employer/Bank"") is pleased to confirm (Subject to the terms and conditions set herein and upon your representations and warranties as set out herein) its willingness to make available to you (hereinafter called ''the Employee"") a #LoanSchemName# Facility (""the Facility"") on the terms and conditions set out herein."
                    Dim strTermCondition2 As String = "IT IS IMPORTANT THAT YOU READ AND UNDERSTAND THESE TERMS AND CONDITIONS. ONCE ACCEPTED, THESE TERMS AND CONDITIONS CONSTITUTE A BINDING AGREEMENT BETWEEN THE EMPLOYEE AND THE BANK."
                    strTermCondition1 = strTermCondition1.Replace("#LoanSchemName#", dtRow.Item("LoanScheme"))
                    rpt_Row.Item("Column39") = strTermCondition1
                    rpt_Row.Item("Column40") = strTermCondition2

                    Dim strExpiryDateDesc As String = "The Facility shall be for a duration of #NoOfInstallment# months from the date of drawdown."
                    strExpiryDateDesc = strExpiryDateDesc.Replace("#NoOfInstallment#", CDec(dtRow.Item("NoOfInstallment")))
                    rpt_Row.Item("Column41") = strExpiryDateDesc

                    Dim strRepaymentDesc As String = "The Facility, inclusive of interest thereon, shall be repaid in agreed monthly installments. The first installment is due at the end of the month in which the drawdown was effected."
                    rpt_Row.Item("Column42") = strRepaymentDesc

                    Dim strInterestDesc As String = "The Facility shall be charged an interest rate of #Percentage#% per annum, accruing daily but charged monthly and calculated on the remaining Facility balance."
                    strInterestDesc = strInterestDesc.Replace("#Percentage#", CDec(dtRow.Item("interest_rate")).ToString("###.##"))
                    rpt_Row.Item("Column43") = strInterestDesc

                    Dim strAcceptanceDesc2 As String = "I, #EmployeeFullName# (Name of the employee), hereby accept to be bound by these terms and conditions."
                    strAcceptanceDesc2 = strAcceptanceDesc2.Replace("#EmployeeFullName#", dtRow.Item("EmployeeFullName").ToString())
                    rpt_Row.Item("Column44") = strAcceptanceDesc2

                    Dim strDeclarationFormDesc As String = " I #EmployeeFullName# (Applicant’s full name ) after having received independent legal advice and having understood the implications, do hereby voluntarily assign to the Tanzania Agricultural Development Bank all the rights relating to my terminal benefits or gratuity or pension from either NSSF or PSSSF or any other relevant pension fund or scheme, for the purposes of satisfying any outstanding liability and interest accrued upon cessation of my employment at TADB by whatever means, such to include but not to be limited to my resignation, abscondment, dismissal, retrenchment or termination."
                    strDeclarationFormDesc = strDeclarationFormDesc.Replace("#EmployeeFullName#", dtRow.Item("EmployeeFullName").ToString())
                    rpt_Row.Item("Column45") = strDeclarationFormDesc

                    '---------------------- Flexcube Loan Data -- Start--------------
                    Dim dsFlexcubeloan As DataSet = GetFlexcubeLoanDetails(strBankAccountNo, strEmployeeCode, strLoanSchemeCode)
                    Dim decOutstandingPrincipal As Decimal = 0
                    Dim decOutstandingInterest As Decimal = 0

                    Dim decTotalExposure As Decimal = CDec(dtRow.Item("loan_amount"))
                    rpt_Row.Item("Column54") = decTotalExposure.ToString("#,##0.00", New System.Globalization.CultureInfo("en-US"))

                    If CBool(dtRow.Item("istopup")) = True Then
                        decOutstandingPrincipal = CDec(dsFlexcubeloan.Tables(0).Compute("SUM(PRINCIPALOUTSTANDING)", ""))
                        decOutstandingInterest = CDec(dsFlexcubeloan.Tables(0).Compute("SUM(CURRENT_OUTSTANDING_INTEREST)", ""))

                        Dim decExistingLoanBalance As Decimal = decOutstandingPrincipal + decOutstandingInterest
                        rpt_Row.Item("Column52") = decExistingLoanBalance.ToString("#,##0.00", New System.Globalization.CultureInfo("en-US"))
                        rpt_Row.Item("Column53") = (decTotalExposure - decExistingLoanBalance).ToString("#,##0.00", New System.Globalization.CultureInfo("en-US"))
                    End If

                    '---------------------- Flexcube Loan Data -- End--------------

                    'Hemant (26 Apr 2024) -- Start
                    'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
                    Dim strYesNoDesc As String = "YES/NO            [       #ANS#      ]"
                    Dim decSalaryAdvanceOutstandingPrincipal As Decimal = 0
                    Dim dsSalaryAdvanceFlexcubeloan As DataSet = GetFlexcubeLoanDetails(strBankAccountNo, strEmployeeCode, "58")
                    If dsSalaryAdvanceFlexcubeloan.Tables(0).Rows.Count > 0 Then
                        decSalaryAdvanceOutstandingPrincipal = CDec(dsSalaryAdvanceFlexcubeloan.Tables(0).Compute("SUM(PRINCIPALOUTSTANDING)", ""))
                    End If
                    If decSalaryAdvanceOutstandingPrincipal > 0 Then
                        rpt_Row.Item("Column55") = strYesNoDesc.Replace("#ANS#", "YES")
                    Else
                        rpt_Row.Item("Column55") = strYesNoDesc.Replace("#ANS#", "NO ")
                    End If

                    Dim intPrevPeriod As Integer = (New clsMasterData).getCurrentPeriodID(CInt(enModuleReference.Payroll), dtPeriodStartDate.AddDays(-1), 0, 0, True, False, Nothing, False)
                    Dim dtStart As Date = dtPeriodStartDate
                    Dim dtEnd As Date = dtPeriodEndDate
                    Dim strDBName As String = xDatabaseName
                    Dim intYearId As Integer = xYearUnkid
                    Dim objPeriod As New clscommom_period_Tran
                    Dim strHeadName As String = strLoanSchemeName & " " & "Insurance"
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                    dtStart = objPeriod._Start_Date
                    dtEnd = objPeriod._End_Date
                    If intYearId <> objPeriod._Yearunkid Then
                        intYearId = objPeriod._Yearunkid
                        strDBName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, xCompanyUnkid)
                    End If
                    Dim dsPayrollData As DataSet = (New clsPayrollProcessTran).GetList(strDBName, xUserUnkid, intYearId, xCompanyUnkid, dtStart, dtEnd, xUserModeSetting, True, False, "List", intEmployeeunkid, intPrevPeriod)
                    Dim drInsurancePremium() As DataRow = dsPayrollData.Tables(0).Select(" trnheadname = '" & strHeadName & "' ")
                    If drInsurancePremium.Length > 0 Then
                        rpt_Row.Item("Column56") = strYesNoDesc.Replace("#ANS#", "YES")
                    Else
                        rpt_Row.Item("Column56") = strYesNoDesc.Replace("#ANS#", "NO ")
                    End If

                    If intLoanSchemeCategory = enLoanSchemeCategories.UNSECURED Then
                        Dim decTotalUnsecureLoanAmount As Decimal = 0
                        decTotalUnsecureLoanAmount = CDec(dtRow.Item("loan_amount")) - decSalaryAdvanceOutstandingPrincipal
                        rpt_Row.Item("Column57") = decTotalUnsecureLoanAmount.ToString("#,##0.00", New System.Globalization.CultureInfo("en-US")) & " /="
                    End If


                    Dim decGrossPay As Decimal = 0

                    'Hemant (11 Oct 2024) -- Start
                    'Dim drGrossPay() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '003' ")
                    Dim drGrossPay() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '061' ")
                    'Hemant (11 Oct 2024) -- End

                    If drGrossPay.Length > 0 Then
                        decGrossPay = CDec(drGrossPay.CopyToDataTable.Compute("SUM(amount)", ""))
                    End If
                    rpt_Row.Item("Column58") = decGrossPay.ToString("#,##0.00", New System.Globalization.CultureInfo("en-US")) & " /="

                    Dim decNetpay As Decimal = 0

                    'Hemant (11 Oct 2024) -- Start
                    'Dim drNetPay() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '061' ")
                    Dim drNetPay() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '003' ")
                    'Hemant (11 Oct 2024) -- End

                    If drNetPay.Length > 0 Then
                        decNetpay = CDec(drNetPay.CopyToDataTable.Compute("SUM(amount)", ""))
                    End If
                    rpt_Row.Item("Column59") = decNetpay.ToString("#,##0.00", New System.Globalization.CultureInfo("en-US")) & " /="

                    Dim decTotalDeduction As Decimal = 0
                    Dim drTotalDeduction() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '060' ")
                    If drTotalDeduction.Length > 0 Then
                        decTotalDeduction = CDec(drTotalDeduction.CopyToDataTable.Compute("SUM(amount)", ""))
                    End If
                    'Hemant (11 Oct 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                    'Hemant (22 Nov 2024) -- Start
                    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                    Dim decExcludeDeduction As Decimal = 0
                    If strLoanSchemeName = "Personal Loan" AndAlso CBool(dtRow.Item("istopup")) = True Then
                        Dim decPersonalLoanInstallmentAmt As Decimal = 0
                        Dim drPersonalLoan() As DataRow = dsPayrollData.Tables(0).Select(" trnheadname = 'Personal Loan INSTALLMENT' ")
                        If drPersonalLoan.Length > 0 Then
                            decPersonalLoanInstallmentAmt = CDec(drPersonalLoan.CopyToDataTable.Compute("SUM(amount)", ""))
                        End If
                        decExcludeDeduction = decExcludeDeduction + decPersonalLoanInstallmentAmt

                        If CBool(dtRow.Item("isliquidate")) = True Then
                            Dim decSalaryAdvanceInstallmentAmt As Decimal = 0
                            Dim drSalaryAdvance() As DataRow = dsPayrollData.Tables(0).Select(" trnheadname = 'Salary Advance INSTALLMENT' ")
                            If drSalaryAdvance.Length > 0 Then
                                decSalaryAdvanceInstallmentAmt = CDec(drSalaryAdvance.CopyToDataTable.Compute("SUM(amount)", ""))
                            End If
                            decExcludeDeduction = decExcludeDeduction + decSalaryAdvanceInstallmentAmt
                        End If

                    End If
                    'Hemant (22 Nov 2024) -- End
                    decTotalDeduction = decTotalDeduction + CDec(dtRow.Item("installmentamt")) - decExcludeDeduction
                    'Hemant (11 Oct 2024) -- End
                    rpt_Row.Item("Column61") = decTotalDeduction.ToString("#,##0.00", New System.Globalization.CultureInfo("en-US")) & " /="

                    Dim decMonthlyDedPerc As Decimal = 0
                    If decGrossPay > 0 Then
                        decMonthlyDedPerc = decTotalDeduction * 100 / decGrossPay
                    End If
                    rpt_Row.Item("Column62") = Format(decMonthlyDedPerc, "#,##0.00")

                    Dim decExpectedNetPay As Decimal = 0

                    'Hemant (11 Oct 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                    'decExpectedNetPay = decNetpay - CDec(dtRow.Item("installmentamt"))
                    decExpectedNetPay = decGrossPay - decTotalDeduction
                    'Hemant (11 Oct 2024) -- End
                    rpt_Row.Item("Column63") = decExpectedNetPay.ToString("#,##0.00", New System.Globalization.CultureInfo("en-US")) & " /="
                    'Hemant (26 Apr 2024) -- End

                    'Hemant (02 Aug 2024) -- Start
                    'Dim dectest As Decimal = decTotalDeduction * 100 / 0
                    'Hemant (02 Aug 2024) -- End


                End If
                'Hemant (29 Mar 2024) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)



            Next

            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "SELECT lnloan_process_pending_loan.processpendingloanunkid " & _
                         ",    #mappingunkid# AS mappingunkid  " & _
                         ",    #lnlevelunkid# AS lnlevelunkid " & _
                         ",    #lnlevelname# AS lnlevelname " & _
                         ",    #priority# AS priority " & _
                         "    ,#APPVR_NAME# approvername " & _
                         "    ,#APPR_JOB_NAME# AS approvertitle " & _
                         ",    lnroleloanapproval_process_tran.approvaldate " & _
                         ",    lnroleloanapproval_process_tran.statusunkid " & _
                         ",    CASE WHEN lnroleloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " THEN  @PENDING " & _
                         "          WHEN lnroleloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN  @APPROVED " & _
                         "          WHEN lnroleloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN  @REJECTED " & _
                         "     END status	 " & _
                         ",    ISNULL(lnroleloanapproval_process_tran.loan_amount, 0) AS approvedamount " & _
                         ",    lnroleloanapproval_process_tran.remark " & _
                         ",    athremployee_master.empsignature  " & _
                         ",    athremployee_master.displayname  " & _
                         " FROM lnroleloanapproval_process_tran " & _
                         " JOIN lnloan_process_pending_loan ON lnroleloanapproval_process_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                         " #ROLEMAPPING_JOIN# " & _
                             " #COMM_JOIN# "
            'Hemant (26 Apr 2024) -- [displayname]

            StrConditionQry = " WHERE lnroleloanapproval_process_tran.isvoid = 0 AND lnroleloanapproval_process_tran.priority #PriorityCond# " & _
                              " AND lnroleloanapproval_process_tran.processpendingloanunkid = @processpendingloanunkid "


            StrQ = StrInnerQry
            StrQ &= StrConditionQry

            StrQ = StrQ.Replace("#ROLEMAPPING_JOIN#", " JOIN lnloanscheme_role_mapping ON lnroleloanapproval_process_tran.mappingunkid = lnloanscheme_role_mapping.mappingunkid " & _
                                                    " JOIN lnapproverlevel_master ON lnloanscheme_role_mapping.levelunkid = lnapproverlevel_master.lnlevelunkid ")

            StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lnroleloanapproval_process_tran.mapuserunkid " & _
                                                       " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid " & _
                                               " LEFT JOIN lnloan_history_tran ON lnloan_history_tran.processpendingloanunkid = lnroleloanapproval_process_tran.processpendingloanunkid AND lnloan_history_tran.isvoid = 0 AND lnloan_history_tran.pendingloanaprovalunkid = lnroleloanapproval_process_tran.pendingloanaprovalunkid  " & _
                                               " LEFT JOIN hremployee_categorization_tran on hremployee_categorization_tran.categorizationtranunkid = lnloan_history_tran.categorizationtranunkid  " & _
                                               " LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                                               " LEFT JOIN athremployee_master on athremployee_master.atemployeeunkid = lnloan_history_tran.atemployeeunkid ")



            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#mappingunkid#", "lnloanscheme_role_mapping.mappingunkid")
            StrQ = StrQ.Replace("#lnlevelunkid#", "lnapproverlevel_master.lnlevelunkid")
            StrQ = StrQ.Replace("#lnlevelname#", "lnapproverlevel_master.lnlevelname")
            StrQ = StrQ.Replace("#priority#", "lnapproverlevel_master.priority")
            StrQ = StrQ.Replace("#PriorityCond#", ">= 0")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")

            StrQ &= " ORDER BY lnapproverlevel_master.priority "
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))
            objDataOperation.AddParameter("@ReportingTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Reporting To"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim dsReportingToApproval As New DataSet
            StrQ = StrInnerQry
            StrQ &= StrConditionQry

           
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lnroleloanapproval_process_tran.mapuserunkid = hremployee_master.employeeunkid  " & _
                                               " LEFT JOIN lnloan_history_tran ON lnloan_history_tran.processpendingloanunkid = lnroleloanapproval_process_tran.processpendingloanunkid AND lnloan_history_tran.isvoid = 0 AND lnloan_history_tran.pendingloanaprovalunkid = lnroleloanapproval_process_tran.pendingloanaprovalunkid  " & _
                                               " LEFT JOIN hremployee_categorization_tran on hremployee_categorization_tran.categorizationtranunkid = lnloan_history_tran.categorizationtranunkid  " & _
                                               " LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                                               " LEFT JOIN athremployee_master on athremployee_master.atemployeeunkid = lnloan_history_tran.atemployeeunkid ")

            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ROLEMAPPING_JOIN#", "")
            StrQ = StrQ.Replace("#mappingunkid#", "-1")
            StrQ = StrQ.Replace("#lnlevelunkid#", "-1")
            StrQ = StrQ.Replace("#lnlevelname#", "@ReportingTo")
            StrQ = StrQ.Replace("#priority#", "-1")
            StrQ = StrQ.Replace("#PriorityCond#", "= -1")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            dsReportingToApproval = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables.Count <= 0 Then
                dsList.Tables.Add(dsReportingToApproval.Tables(0).Copy)
            Else
                dsList.Tables(0).Merge(dsReportingToApproval.Tables(0), True)
            End If

            If dsList.Tables.Count > 0 Then
                dsList.Tables(0).DefaultView.Sort = "Priority ASC"
                Dim dtTempTable As DataTable = dsList.Tables(0).DefaultView.ToTable
                dsList.Tables.Remove(dsList.Tables(0))
                dsList.Tables.Add(dtTempTable)
            End If
            Dim lstProcessPendingLoanUnkIds As IEnumerable(Of Int32) = dsList.Tables(0).AsEnumerable(). _
                                                         Select(Function(row) row.Field(Of Int32)("processpendingloanunkid")). _
                                                         Distinct()

            Dim arrProcessPendingLoanIds As Integer() = lstProcessPendingLoanUnkIds.ToArray

            For Each intProcessPendingLoanID As Integer In arrProcessPendingLoanIds
                Dim drList() As DataRow = dsList.Tables(0).Select("processpendingloanunkid = " & intProcessPendingLoanID)
                If drList.Length > 0 Then
                    Dim dt As New DataTable
                    dt = drList.CopyToDataTable()

                    Dim mintPriorty As Integer = -1

                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
Recalculate:
                        Dim mintPrioriry As Integer = 0
                        For i As Integer = 0 To dt.Rows.Count - 1

                            If mintPrioriry <> CInt(dt.Rows(i)("priority")) Then
                                mintPrioriry = CInt(dt.Rows(i)("priority"))
                                Dim drRow() As DataRow = dt.Select("statusunkid <> 1 AND priority  = " & mintPrioriry)
                                If drRow.Length > 0 Then
                                    Dim drPending() As DataRow = dt.Select("statusunkid = 1 AND priority  = " & mintPrioriry)
                                    If drPending.Length > 0 Then
                                        Dim drPendingList() As DataRow = dsList.Tables(0).Select("processpendingloanunkid = " & intProcessPendingLoanID & " AND statusunkid = 1 AND priority  = " & mintPrioriry)
                                        If drPendingList.Length > 0 Then
                                            For Each dRowList As DataRow In drPendingList
                                                dsList.Tables(0).Rows.Remove(dRowList)
                                            Next
                                            dsList.Tables(0).AcceptChanges()
                                        End If
                                        For Each dRow As DataRow In drPending
                                            dt.Rows.Remove(dRow)
                                            GoTo Recalculate
                                        Next
                                        dt.AcceptChanges()
                                    End If
                                End If


                            End If

                        Next
                    End If
                End If
            Next

            Dim intPriority As Integer = -2
            'Hemant (26 Apr 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
            Dim intCount As Integer = 1
            If strGroupName.ToUpper.Trim = "TADB" Then
                intCount = 16
            End If
            'Hemant (26 Apr 2024) -- End
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                If intPriority = CInt(dtRow.Item("priority")) Then Continue For
                Dim rpt_Row As DataRow
                rpt_Row = rpt_LoanApprovers.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("lnlevelname")
                'Hemant (26 Apr 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
                rpt_Row.Item("Column9") = intCount.ToString()
                intCount = intCount + 1
                'Hemant (26 Apr 2024) -- End
                If CInt(dtRow.Item("statusunkid")) <> enLoanApplicationStatus.PENDING Then
                    rpt_Row.Item("Column2") = dtRow.Item("approvername")
                    rpt_Row.Item("Column3") = dtRow.Item("approvertitle")
                    rpt_Row.Item("Column4") = dtRow.Item("status")
                    rpt_Row.Item("Column5") = CDec(dtRow.Item("approvedamount")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                    rpt_Row.Item("Column6") = CDate(dtRow.Item("approvaldate")).ToShortDateString
                    rpt_Row.Item("Column7") = dtRow.Item("remark")
                    rpt_Row.Item("sign1") = dtRow.Item("empsignature")
                    'Hemant (26 Apr 2024) -- Start
                    'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
                    rpt_Row.Item("Column8") = dtRow.Item("displayname")
                    'Hemant (26 Apr 2024) -- End
                End If
                rpt_LoanApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
                intPriority = CInt(dtRow.Item("priority"))
            Next


            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            If strGroupName.ToUpper.Trim = "NMB PLC" Then
            objRpt = New ArutiReport.Designer.rptFlexcubeLoanForm
            ElseIf strGroupName.ToUpper.Trim = "TADB" Then
                objRpt = New ArutiReport.Designer.rptFlexcubeLoanForm_TADB
            Else
                'Hemant (29 Mar 2024) -- End
                objRpt = New ArutiReport.Designer.rptFlexcubeLoanForm
            End If  'Hemant (29 Mar 2024)


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                       True, _
                                       False, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       , _
                                       , _
                                       , _
                                       ConfigParameter._Object._GetLeftMargin, _
                                       ConfigParameter._Object._GetRightMargin, _
                                       ConfigParameter._Object._TOPPageMarging)




            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 11, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 12, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 13, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 14, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            If strGroupName.ToUpper.Trim = "NMB PLC" Then
                'Hemant (29 Mar 2024) -- End

            Call ReportFunction.TextChange(objRpt, "lblReportName", Language.getMessage(mstrModuleName, 1, "STANDARD STAFF LOAN APPLICATION FORM CR-HR-2022"))
            Call ReportFunction.TextChange(objRpt, "lblone", Language.getMessage(mstrModuleName, 2, "1.0"))
            Call ReportFunction.TextChange(objRpt, "txtone", Language.getMessage(mstrModuleName, 3, "PERSONAL BRIEF EMPLOYMENT PARTICULARS"))

            Call ReportFunction.TextChange(objRpt, "lblone_one", Language.getMessage(mstrModuleName, 2, "1.1"))
                Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 103, "Employee’s Full Name :"))

            Call ReportFunction.TextChange(objRpt, "lblone_two", Language.getMessage(mstrModuleName, 2, "1.2"))
                Call ReportFunction.TextChange(objRpt, "txtBirthDate", Language.getMessage(mstrModuleName, 104, "Date of Birth :"))

            Call ReportFunction.TextChange(objRpt, "lblone_three", Language.getMessage(mstrModuleName, 2, "1.3"))
            Call ReportFunction.TextChange(objRpt, "txtEmployementDate", Language.getMessage(mstrModuleName, 3, "Date of Employment :"))

            Call ReportFunction.TextChange(objRpt, "lblone_four", Language.getMessage(mstrModuleName, 2, "1.4"))
                Call ReportFunction.TextChange(objRpt, "txtConfirmationDate", Language.getMessage(mstrModuleName, 106, "Date of Confirmation :"))

            Call ReportFunction.TextChange(objRpt, "lblone_five", Language.getMessage(mstrModuleName, 2, "1.5"))
                Call ReportFunction.TextChange(objRpt, "txtStationOfDuty", Language.getMessage(mstrModuleName, 107, "Station of Duty :"))

            Call ReportFunction.TextChange(objRpt, "lblone_six", Language.getMessage(mstrModuleName, 2, "1.6"))
            Call ReportFunction.TextChange(objRpt, "txtCurrentDesignation", Language.getMessage(mstrModuleName, 3, "Current Designation :"))

            Call ReportFunction.TextChange(objRpt, "lblone_seven", Language.getMessage(mstrModuleName, 2, "1.7"))
            Call ReportFunction.TextChange(objRpt, "txtAccountNumber", Language.getMessage(mstrModuleName, 3, "Account Number :"))


            Call ReportFunction.TextChange(objRpt, "txtNIDAIdNo", Language.getMessage(mstrModuleName, 2, "NIDA ID No :"))
            Call ReportFunction.TextChange(objRpt, "txtStaffIdNo", Language.getMessage(mstrModuleName, 6, "Staff ID No :"))
            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 8, "Branch"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 9, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtResidence", Language.getMessage(mstrModuleName, 15, "Residence :"))
            Call ReportFunction.TextChange(objRpt, "txtMobileNo", Language.getMessage(mstrModuleName, 16, "Mobile No :"))

            Call ReportFunction.TextChange(objRpt, "lbltwo", Language.getMessage(mstrModuleName, 2, "2.0"))
            Call ReportFunction.TextChange(objRpt, "txttwo", Language.getMessage(mstrModuleName, 2, "CREDIT PARTICULARS"))

            Call ReportFunction.TextChange(objRpt, "lbltwo_one", Language.getMessage(mstrModuleName, 2, "2.0.1"))
            Call ReportFunction.TextChange(objRpt, "txttwo_one", Language.getMessage(mstrModuleName, 2, "Credit Amount Requested (TZS) :"))

            Call ReportFunction.TextChange(objRpt, "lbltwo_two", Language.getMessage(mstrModuleName, 2, "2.0.2"))
            Call ReportFunction.TextChange(objRpt, "txttwo_two", Language.getMessage(mstrModuleName, 2, "Type of Credit Accommodation Applied for: (Please give ""X"" as appropriate) "))

            Call ReportFunction.TextChange(objRpt, "lbltwo_twoI", Language.getMessage(mstrModuleName, 2, "i)."))
            Call ReportFunction.TextChange(objRpt, "lblNew", Language.getMessage(mstrModuleName, 2, "New"))
            Call ReportFunction.TextChange(objRpt, "lblTopUp", Language.getMessage(mstrModuleName, 2, "Top-Up"))
                Call ReportFunction.TextChange(objRpt, "lblNewX", Language.getMessage(mstrModuleName, 122, "X"))
                Call ReportFunction.TextChange(objRpt, "lblTopUpX", Language.getMessage(mstrModuleName, 122, "X"))

            Call ReportFunction.TextChange(objRpt, "lbltwo_three", Language.getMessage(mstrModuleName, 2, "2.0.3"))
            Call ReportFunction.TextChange(objRpt, "txttwo_three", Language.getMessage(mstrModuleName, 2, "Purpose of the Credit Facility: "))

            Call ReportFunction.TextChange(objRpt, "lblthree", Language.getMessage(mstrModuleName, 2, "3.0"))
            Call ReportFunction.TextChange(objRpt, "txtthree", Language.getMessage(mstrModuleName, 2, "PROTECTION (FOR SECURED CREDIT REQUESTS ONLY)"))
            Call ReportFunction.TextChange(objRpt, "lblthree_one", Language.getMessage(mstrModuleName, 2, "3.1"))
            Call ReportFunction.TextChange(objRpt, "txtthree_one", Language.getMessage(mstrModuleName, 2, "Ownership:"))
            Call ReportFunction.TextChange(objRpt, "lblthree_oneA", Language.getMessage(mstrModuleName, 2, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_oneA", Language.getMessage(mstrModuleName, 2, "Own (Full Name) :"))
            Call ReportFunction.TextChange(objRpt, "lblthree_oneB", Language.getMessage(mstrModuleName, 2, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_oneB", Language.getMessage(mstrModuleName, 2, "Guarantor’s	Husband’s/Wife/Child’s (strike through not applicable)"))
            Call ReportFunction.TextChange(objRpt, "lblthree_oneC", Language.getMessage(mstrModuleName, 2, "(c)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_oneC", Language.getMessage(mstrModuleName, 2, "Third Party	Please indicate__________________________________"))
            Call ReportFunction.TextChange(objRpt, "lblthree_two", Language.getMessage(mstrModuleName, 2, "3.2"))
            Call ReportFunction.TextChange(objRpt, "txtthree_two", Language.getMessage(mstrModuleName, 2, "Security Description:"))


            Call ReportFunction.TextChange(objRpt, "txtSecurityType", Language.getMessage(mstrModuleName, 2, "Security Type"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 2, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtRegistration", Language.getMessage(mstrModuleName, 2, "Registration"))
            Call ReportFunction.TextChange(objRpt, "txtValue", Language.getMessage(mstrModuleName, 2, "Value (TZS ""000"")"))
            Call ReportFunction.TextChange(objRpt, "txtMarket", Language.getMessage(mstrModuleName, 2, "Market"))
            Call ReportFunction.TextChange(objRpt, "txtFSV", Language.getMessage(mstrModuleName, 2, "FSV"))
            Call ReportFunction.TextChange(objRpt, "lblthree_twoA", Language.getMessage(mstrModuleName, 2, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoA", Language.getMessage(mstrModuleName, 2, "Residential House"))
            Call ReportFunction.TextChange(objRpt, "txtPlotNo", Language.getMessage(mstrModuleName, 2, "Plot No:"))
            Call ReportFunction.TextChange(objRpt, "txtBlockNo", Language.getMessage(mstrModuleName, 2, "Block No:"))
            Call ReportFunction.TextChange(objRpt, "txtStreet", Language.getMessage(mstrModuleName, 2, "Street :"))
            Call ReportFunction.TextChange(objRpt, "txtTown", Language.getMessage(mstrModuleName, 2, "Town/City :"))
            Call ReportFunction.TextChange(objRpt, "txtCTNo", Language.getMessage(mstrModuleName, 2, "CT No :"))
            Call ReportFunction.TextChange(objRpt, "txtLONo", Language.getMessage(mstrModuleName, 2, "LO No :"))

            Call ReportFunction.TextChange(objRpt, "txtModel", Language.getMessage(mstrModuleName, 2, "Model :"))
            Call ReportFunction.TextChange(objRpt, "txtYOM", Language.getMessage(mstrModuleName, 2, "YOM :"))
            Call ReportFunction.TextChange(objRpt, "txtChassisNo", Language.getMessage(mstrModuleName, 2, "Chassis No :"))
            Call ReportFunction.TextChange(objRpt, "txtColor", Language.getMessage(mstrModuleName, 2, "Color :"))
            Call ReportFunction.TextChange(objRpt, "txtSupplier", Language.getMessage(mstrModuleName, 2, "Supplier :"))


            Call ReportFunction.TextChange(objRpt, "lblthree_twoB", Language.getMessage(mstrModuleName, 2, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoB", Language.getMessage(mstrModuleName, 2, "Others (**indicate) "))

            Call ReportFunction.TextChange(objRpt, "txtSecurityType2", Language.getMessage(mstrModuleName, 2, "Security Type"))
            Call ReportFunction.TextChange(objRpt, "txtLocation2", Language.getMessage(mstrModuleName, 2, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtRegistration2", Language.getMessage(mstrModuleName, 2, "Registration"))
            Call ReportFunction.TextChange(objRpt, "txtValue2", Language.getMessage(mstrModuleName, 2, "Value (TZS ""000"")"))
            Call ReportFunction.TextChange(objRpt, "txtMarket2", Language.getMessage(mstrModuleName, 2, "Market"))
            Call ReportFunction.TextChange(objRpt, "txtFSV2", Language.getMessage(mstrModuleName, 2, "FSV"))
            Call ReportFunction.TextChange(objRpt, "lblthree_twoA2", Language.getMessage(mstrModuleName, 2, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoA2", Language.getMessage(mstrModuleName, 2, "Motor Vehicle"))
            Call ReportFunction.TextChange(objRpt, "lblthree_twoB2", Language.getMessage(mstrModuleName, 2, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoB2", Language.getMessage(mstrModuleName, 2, "Others (**indicate) "))

            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection3", True)
                If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage OrElse mintLoanTypeId = enLoanType.Construction_Mortgage _
                   OrElse mintLoanTypeId = enLoanType.Semi_Finish_Mortgage Then
                    'Hemant (17 Jan 2025) -- [OrElse mintLoanTypeId = enLoanType.Semi_Finish_Mortgage]
                Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", False)
            End If
            If mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection3", False)
            End If

            Call ReportFunction.TextChange(objRpt, "lblfour", Language.getMessage(mstrModuleName, 2, "4.0"))
            Call ReportFunction.TextChange(objRpt, "txtfour", Language.getMessage(mstrModuleName, 2, "ASSIGNMENT OF TERMINAL BENEFITS TO LIQUIDATE OUTSTANDING ASSET(S)"))

            Call ReportFunction.TextChange(objRpt, "lblfive", Language.getMessage(mstrModuleName, 2, "5.0"))
            Call ReportFunction.TextChange(objRpt, "txtfive", Language.getMessage(mstrModuleName, 2, "APPROVALS"))

            objRpt.Subreports("rptLoanApproverDetailReport").SetDataSource(rpt_LoanApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtLevelName", Language.getMessage(mstrModuleName, 17, "Level Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApprover", Language.getMessage(mstrModuleName, 17, "Approver"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtJobTitle", Language.getMessage(mstrModuleName, 17, "Job Title"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtStatus", Language.getMessage(mstrModuleName, 17, "Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApporvedAmount", Language.getMessage(mstrModuleName, 17, "Approved Amount"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApprovalDate", Language.getMessage(mstrModuleName, 17, "Approval Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtRemarks", Language.getMessage(mstrModuleName, 17, "Remarks"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtSignature", Language.getMessage(mstrModuleName, 17, "Signature"))

            Call ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 102, "Bank"))
            Call ReportFunction.TextChange(objRpt, "lblBorrower2", Language.getMessage(mstrModuleName, 103, "Borrower"))

                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
            ElseIf strGroupName.ToUpper.Trim = "TADB" Then
                Call ReportFunction.TextChange(objRpt, "lblReportName", Language.getMessage(mstrModuleName, 101, "STAFF LOAN APPLICATION FORM"))

                Call ReportFunction.TextChange(objRpt, "lblone", Language.getMessage(mstrModuleName, 102, "1."))
                Call ReportFunction.TextChange(objRpt, "txtone", Language.getMessage(mstrModuleName, 103, "PERSONAL EMPLOYMENT PARTICULARS:"))
                Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 104, "Employee’s Full Name :"))
                Call ReportFunction.TextChange(objRpt, "txtBirthDate", Language.getMessage(mstrModuleName, 105, "Date of Birth :"))
                Call ReportFunction.TextChange(objRpt, "txtEmployementDate", Language.getMessage(mstrModuleName, 106, "Date of joining TADB :"))
                Call ReportFunction.TextChange(objRpt, "txtConfirmationDate", Language.getMessage(mstrModuleName, 107, "Date of Confirmation :"))
                Call ReportFunction.TextChange(objRpt, "txtStationOfDuty", Language.getMessage(mstrModuleName, 108, "Station of Duty :"))
                Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 109, "Department :"))
                Call ReportFunction.TextChange(objRpt, "txtCurrentDesignation", Language.getMessage(mstrModuleName, 110, "Current Designation :"))
                Call ReportFunction.TextChange(objRpt, "txtEmailAddress", Language.getMessage(mstrModuleName, 111, "Email Address :"))
                Call ReportFunction.TextChange(objRpt, "txtDefaultIdNo", Language.getMessage(mstrModuleName, 112, "National ID/ Passport Number :"))
                Call ReportFunction.TextChange(objRpt, "txtMobileNo", Language.getMessage(mstrModuleName, 113, "Cellphone Number :"))
                Call ReportFunction.TextChange(objRpt, "txtResidence", Language.getMessage(mstrModuleName, 114, "Residential Address :"))

                Call ReportFunction.TextChange(objRpt, "lbltwo", Language.getMessage(mstrModuleName, 115, "2."))
                Call ReportFunction.TextChange(objRpt, "txttwo", Language.getMessage(mstrModuleName, 116, "LOAN PARTICULARS:"))
                Call ReportFunction.TextChange(objRpt, "txtLoanAmountRequested", Language.getMessage(mstrModuleName, 117, "Loan amount requested "))
                Call ReportFunction.TextChange(objRpt, "txtNoOfInstallment", Language.getMessage(mstrModuleName, 118, "Tenure :"))
                Call ReportFunction.TextChange(objRpt, "txtInterestRate", Language.getMessage(mstrModuleName, 119, "Interest Rate :"))
                Call ReportFunction.TextChange(objRpt, "txtLoanScheme", Language.getMessage(mstrModuleName, 120, "Loan Type :"))
                Call ReportFunction.TextChange(objRpt, "txtLoanPurpose", Language.getMessage(mstrModuleName, 121, "Purpose of the loan :"))
                Call ReportFunction.TextChange(objRpt, "lblNew", Language.getMessage(mstrModuleName, 122, "New Loan :"))
                Call ReportFunction.TextChange(objRpt, "lblTopUp", Language.getMessage(mstrModuleName, 123, "Top-Up Loan :"))
                Call ReportFunction.TextChange(objRpt, "txtOtherLenders", Language.getMessage(mstrModuleName, 124, "Take-over from other lenders :"))
                Call ReportFunction.TextChange(objRpt, "lblNewX", Language.getMessage(mstrModuleName, 125, "X"))
                Call ReportFunction.TextChange(objRpt, "lblTopUpX", Language.getMessage(mstrModuleName, 125, "X"))

                Call ReportFunction.TextChange(objRpt, "lblthree", Language.getMessage(mstrModuleName, 126, "3."))
                Call ReportFunction.TextChange(objRpt, "txtthree", Language.getMessage(mstrModuleName, 127, "TOTAL CREDIT EXPOSURE :"))

                Call ReportFunction.TextChange(objRpt, "lblfour", Language.getMessage(mstrModuleName, 128, "4."))
                Call ReportFunction.TextChange(objRpt, "txtfour", Language.getMessage(mstrModuleName, 129, "PROTECTION/COLLATERAL (FOR SECURED STAFF LOAN):"))

                Call ReportFunction.TextChange(objRpt, "lblfive", Language.getMessage(mstrModuleName, 130, "5."))
                Call ReportFunction.TextChange(objRpt, "txtfive", Language.getMessage(mstrModuleName, 131, "TERMS AND CONDITIONS OF THE PERSONAL/CAR LOAN FACILITY"))
                Call ReportFunction.TextChange(objRpt, "lblfiveone", Language.getMessage(mstrModuleName, 132, "5.1"))
                Call ReportFunction.TextChange(objRpt, "txtfiveone", Language.getMessage(mstrModuleName, 133, "EXPIRY DATE"))
                Call ReportFunction.TextChange(objRpt, "lblfivetwo", Language.getMessage(mstrModuleName, 134, "5.2"))
                Call ReportFunction.TextChange(objRpt, "txtfivetwo", Language.getMessage(mstrModuleName, 135, "REPAYMENT"))
                Call ReportFunction.TextChange(objRpt, "lblfivethree", Language.getMessage(mstrModuleName, 136, "5.3"))
                Call ReportFunction.TextChange(objRpt, "txtfivethree", Language.getMessage(mstrModuleName, 137, "INTEREST"))
                Call ReportFunction.TextChange(objRpt, "lblfivefour", Language.getMessage(mstrModuleName, 138, "5.4"))
                Call ReportFunction.TextChange(objRpt, "txtfivefour", Language.getMessage(mstrModuleName, 139, "SECURITY"))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourDesc", Language.getMessage(mstrModuleName, 140, "The Employee shall simultaneous with this Agreement, execute and deliver the following security for this facility and any other securities to be added or substituted therefore are hereinafter to be called (""the security"") namely:"))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourDesc1", Language.getMessage(mstrModuleName, 141, "The Employee’s current and future pension contributions held by Pensions Fund will be the principal security for the loan."))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourDesc2", Language.getMessage(mstrModuleName, 142, "Without prejudice to the security mentioned above and other means of loan recovery which may be applied by the Employer against the Employee, monthly salaries and terminal benefits of the Employee will also be security for the loan."))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourDesc3", Language.getMessage(mstrModuleName, 143, "Any loan balance and/or other monies payable under this Contract shall be payable by way of direct deductions from the monthly salaries and/or the Employee’s pension contributions benefits held by the Pensions Fund and terminal benefits payable to the Employee at the time of termination of employment."))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourDesc4", Language.getMessage(mstrModuleName, 144, "In case of a Car loan, the employee shall pledge the Car purchased through the loan whose details are provided in para 4 herein as a security for such loan. In view of this, an employee shall submit a Motor Vehicle Registration Card jointly registered with the Employer as a title holder."))
                Call ReportFunction.TextChange(objRpt, "lblfivefourOne", Language.getMessage(mstrModuleName, 138, "5.4.1"))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourOneDesc", Language.getMessage(mstrModuleName, 145, "The Employee hereby assigns all of his/her pension contributions benefits to the Employer for liquidation of outstanding Staff loan and interest thereon, if such a loan in full or portion thereon remains unpaid under the conditions specified herein."))
                Call ReportFunction.TextChange(objRpt, "lblfivefourTwo", Language.getMessage(mstrModuleName, 138, "5.4.2"))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourTwoDesc", Language.getMessage(mstrModuleName, 146, "The Employee hereby authorizes the Employer to use the Employee’s pension contributions benefits held by the pension fund to repay the outstanding loan and interest, in situations where the Employee’s employment with the Employer is terminated by either the Employer or the Employee or by absconding or any other reason."))
                Call ReportFunction.TextChange(objRpt, "lblfivefourThree", Language.getMessage(mstrModuleName, 138, "5.4.3"))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourThreeDesc", Language.getMessage(mstrModuleName, 147, "The Employee hereby authorizes the Employer to communicate with Pension Fund to arrange for the pension fund to remit the Employee’s pension contributions benefits to the Employer where the Employee’s employment with the Employer is terminated and/or the loan is in default."))
                Call ReportFunction.TextChange(objRpt, "lblfivefourFour", Language.getMessage(mstrModuleName, 138, "5.4.4"))
                Call ReportFunction.TextChange(objRpt, "txtFiveFourFourDesc", Language.getMessage(mstrModuleName, 148, " The Employer shall have such right to communicate and follow up recovery of the outstanding loan and interest from the pension fund to which the Employee is a member."))

                Call ReportFunction.TextChange(objRpt, "lblSix", Language.getMessage(mstrModuleName, 130, "6."))
                Call ReportFunction.TextChange(objRpt, "txtSixDesc", Language.getMessage(mstrModuleName, 131, "EVENTS OF DEFAULTS"))
                Call ReportFunction.TextChange(objRpt, "lblSixone", Language.getMessage(mstrModuleName, 132, "6.1"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneDesc", Language.getMessage(mstrModuleName, 133, "There shall be an event of default if any of the following occurs: - "))
                Call ReportFunction.TextChange(objRpt, "lblSixOneOne", Language.getMessage(mstrModuleName, 132, "6.1.1"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneOneDesc", Language.getMessage(mstrModuleName, 133, "The Employee terminates his/her employment with the Bank before full repayment of the loan or interest or any other monies payable to the Employer under this Agreement;"))
                Call ReportFunction.TextChange(objRpt, "lblSixOneTwo", Language.getMessage(mstrModuleName, 132, "6.1.2"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneTwoDesc", Language.getMessage(mstrModuleName, 133, "The Employer terminates the Employees’ employment with the Bank before full repayment of the loan or interest or any other monies payable to the Employer under this Agreement;"))
                Call ReportFunction.TextChange(objRpt, "lblSixOneThree", Language.getMessage(mstrModuleName, 132, "6.1.3"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneThreeDesc", Language.getMessage(mstrModuleName, 133, "The Employer demands payment of any money secured under this Agreement and repayable on demand and if not paid immediately; "))
                Call ReportFunction.TextChange(objRpt, "lblSixOneFour", Language.getMessage(mstrModuleName, 132, "6.1.4"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneFourDesc", Language.getMessage(mstrModuleName, 133, "The Employee fails to comply with any of his/her obligations under this Agreement;"))
                Call ReportFunction.TextChange(objRpt, "lblSixOneFive", Language.getMessage(mstrModuleName, 132, "6.1.5"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneFiveDesc", Language.getMessage(mstrModuleName, 133, "The Employee fails to pay any sum payable by him/her under this Agreement when due;"))
                Call ReportFunction.TextChange(objRpt, "lblSixOneSix", Language.getMessage(mstrModuleName, 132, "6.1.6"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneSixDesc", Language.getMessage(mstrModuleName, 133, "Any representation or warranty made or represented by the Employee in or pursuant to this Agreement or any document delivered under this Agreement is or proves to have been incorrect in any material respect; or"))
                Call ReportFunction.TextChange(objRpt, "lblSixOneSeven", Language.getMessage(mstrModuleName, 132, "6.1.7"))
                Call ReportFunction.TextChange(objRpt, "txtSixOneSevenDesc", Language.getMessage(mstrModuleName, 133, "The Employer becomes bankrupt, has a receiving order made against his/her, makes any arrangement with her/his creditors generally or takes or suffers any similar action as a result of debt."))

                Call ReportFunction.TextChange(objRpt, "lblSeven", Language.getMessage(mstrModuleName, 130, "7."))
                Call ReportFunction.TextChange(objRpt, "txtSevenDesc", Language.getMessage(mstrModuleName, 131, "EMPLOYEE’S DEFAULT AND REMEDY OF THE EMPLOYER"))
                Call ReportFunction.TextChange(objRpt, "lblSevenOne", Language.getMessage(mstrModuleName, 132, "7.1"))
                Call ReportFunction.TextChange(objRpt, "txtSevenOneDesc", Language.getMessage(mstrModuleName, 133, "If the Employee defaults in the payment of any amount of the loan then the whole sum of the loan plus any other money due under this Agreement shall immediately become payable."))
                Call ReportFunction.TextChange(objRpt, "lblSevenTwo", Language.getMessage(mstrModuleName, 132, "7.2"))
                Call ReportFunction.TextChange(objRpt, "txtSevenTwoDesc", Language.getMessage(mstrModuleName, 133, "Where the default is caused by the termination of employment, any outstanding amount of the loan at the time the default occurs shall become a commercial loan and attract commercial interest at the rate that will be applicable to commercial loans granted by the Bank. The outstanding loan amount will become payable immediately, and such outstanding loan amount shall be deductible from the Employee pensions contribution held by the pension fund and any terminal benefits payable to the Employee."))
                Call ReportFunction.TextChange(objRpt, "lblSevenThree", Language.getMessage(mstrModuleName, 132, "7.3"))
                Call ReportFunction.TextChange(objRpt, "txtSevenThreeDesc", Language.getMessage(mstrModuleName, 133, "The Employer shall have such right to communicate and follow up recovery of the outstanding loans from Employees’ pension contributions held by the pension fund to which the Employee is a member."))
                Call ReportFunction.TextChange(objRpt, "lblSevenFour", Language.getMessage(mstrModuleName, 132, "7.4"))
                Call ReportFunction.TextChange(objRpt, "txtSevenFourDesc", Language.getMessage(mstrModuleName, 133, "The Employer may without prejudice to above-stated rights or any of its other rights, at any time after the happening of an Event of Default by notice to the Employee declare the facility and all other sums payable under this Agreement due and payable immediately, and upon such declaration, the facility and all other sums shall become due and payable immediately, anything contrary in this Agreement notwithstanding."))
                Call ReportFunction.TextChange(objRpt, "lblSevenFive", Language.getMessage(mstrModuleName, 132, "7.5"))
                Call ReportFunction.TextChange(objRpt, "txtSevenFiveDesc", Language.getMessage(mstrModuleName, 133, "The Employer may sell assign transfer call up collect or otherwise dispose of or realize the said securities or any part thereof in such manner and on such terms as may appear to the Employer most expedient."))
                Call ReportFunction.TextChange(objRpt, "lblSevenSix", Language.getMessage(mstrModuleName, 132, "7.6"))
                Call ReportFunction.TextChange(objRpt, "txtSevenSixDesc", Language.getMessage(mstrModuleName, 133, "The Employer may institute any legal proceedings which the Employer may deem necessary in connection herewith."))
                Call ReportFunction.TextChange(objRpt, "lblSevenSeven", Language.getMessage(mstrModuleName, 132, "7.7"))
                Call ReportFunction.TextChange(objRpt, "txtSevenSevenDesc", Language.getMessage(mstrModuleName, 133, "In the event of the proceeds of the said securities being insufficient to pay the money hereby secured and such costs and expenses as aforesaid the Employee undertakes to pay the shortfall on demand."))
                Call ReportFunction.TextChange(objRpt, "lblSevenEight", Language.getMessage(mstrModuleName, 132, "7.8"))
                Call ReportFunction.TextChange(objRpt, "txtSevenEightDesc", Language.getMessage(mstrModuleName, 133, "It shall always be lawful for the Employer either before or during or after the realization of the said securities to sue for the recovery of the money then remaining due or portion thereof and on obtaining a judgment to attach any other property or effects belonging to the Employee just as if this security had not been given."))

                Call ReportFunction.TextChange(objRpt, "lblEight", Language.getMessage(mstrModuleName, 130, "8."))
                Call ReportFunction.TextChange(objRpt, "txtEightDesc", Language.getMessage(mstrModuleName, 131, "EMPLOYER’S OTHER RIGHTS"))
                Call ReportFunction.TextChange(objRpt, "lblEightOne", Language.getMessage(mstrModuleName, 132, "8.1"))
                Call ReportFunction.TextChange(objRpt, "txtEightOneDesc", Language.getMessage(mstrModuleName, 133, "The Employer shall have the right to communicate and follow up recovery of the loan or any outstanding loan balance from any subsequent Employer of the Employee in the event that the Employee terminates his/her Employment with the Bank and joins another Employer."))
                Call ReportFunction.TextChange(objRpt, "lblEightTwo", Language.getMessage(mstrModuleName, 132, "8.2"))
                Call ReportFunction.TextChange(objRpt, "txtEightTwoDesc", Language.getMessage(mstrModuleName, 133, "The Employee hereby authorizes the Employer to communicate with and follow up recovery of the loan or any outstanding loan balance from any subsequent Employer of the Employee in the event that the Employee terminates his/her Employment with the Bank and joins another Employer"))
                Call ReportFunction.TextChange(objRpt, "lblEightThree", Language.getMessage(mstrModuleName, 132, "8.3"))
                Call ReportFunction.TextChange(objRpt, "txtEightThreeDesc", Language.getMessage(mstrModuleName, 133, "The Employer reserves the right to disclose information of the Employee to other banks, financial institutions or Credit Information Reference Bureau, should it be requested or deem important to do so."))
                Call ReportFunction.TextChange(objRpt, "lblEightFour", Language.getMessage(mstrModuleName, 132, "8.4"))
                Call ReportFunction.TextChange(objRpt, "txtEightFourDesc", Language.getMessage(mstrModuleName, 133, "The Employer consents that in the event a default occurs in the repayment of the loan and such default persists, the Employer may, unless the Employee takes steps to remedy the default, publicize the default by issuing notices in the media calling relatives and associates of the Employee to remedy the default before the Employer embarks into other legal recovery measures. For clarity purpose, the Employer reserves the right to embark on legal recovery measures prior to calling on the Employee’s relatives and associates to remedy the default."))

                Call ReportFunction.TextChange(objRpt, "lblNine", Language.getMessage(mstrModuleName, 130, "9."))
                Call ReportFunction.TextChange(objRpt, "txtNineDesc", Language.getMessage(mstrModuleName, 131, "WARRANTIES AND REPRESENTATIONS"))
                Call ReportFunction.TextChange(objRpt, "lblNineOne", Language.getMessage(mstrModuleName, 132, "9.1"))
                Call ReportFunction.TextChange(objRpt, "txtNineOneDesc", Language.getMessage(mstrModuleName, 133, "The Bank has entered into these terms and conditions in reliance on the following representations, and the Employee represents and warrants to the Bank that:"))
                Call ReportFunction.TextChange(objRpt, "lblNineOneI", Language.getMessage(mstrModuleName, 132, "i)"))
                Call ReportFunction.TextChange(objRpt, "txtNineOneIDesc", Language.getMessage(mstrModuleName, 133, "These Terms and Conditions constitutes the legal, valid and binding obligations of the Employee enforceable in accordance with their terms."))
                Call ReportFunction.TextChange(objRpt, "lblNineOneII", Language.getMessage(mstrModuleName, 132, "ii)"))
                Call ReportFunction.TextChange(objRpt, "txtNineOneIIDesc", Language.getMessage(mstrModuleName, 133, "All information supplied by the Employee or any person on [his] [her] behalf to the Bank in connection with these terms and conditions was and remains true and complete in all respects and there is no other material fact or circumstance relating to the affairs of the Employee which has not been disclosed to the Bank."))

                Call ReportFunction.TextChange(objRpt, "lblTen", Language.getMessage(mstrModuleName, 130, "10."))
                Call ReportFunction.TextChange(objRpt, "txtTen", Language.getMessage(mstrModuleName, 131, "COVENANTS"))
                Call ReportFunction.TextChange(objRpt, "txtTenDesc", Language.getMessage(mstrModuleName, 133, "The Employee agrees to:"))
                Call ReportFunction.TextChange(objRpt, "lblTenI", Language.getMessage(mstrModuleName, 132, "i)"))
                Call ReportFunction.TextChange(objRpt, "txtTenIDesc", Language.getMessage(mstrModuleName, 133, "Ensure that this Facility shall rank at least equally with all of the Employee's present and future unsecured and un subordinated liabilities other than those which are mandatory preferred by law."))
                Call ReportFunction.TextChange(objRpt, "lblTenII", Language.getMessage(mstrModuleName, 132, "ii)"))
                Call ReportFunction.TextChange(objRpt, "txtTenIIDesc", Language.getMessage(mstrModuleName, 133, "The Employee agrees and acknowledges that the Bank shall have the right to retain and use any securities/collaterals securing other indebtedness of the Employee, for securing the indebtedness of the Employee under these terms and conditions."))
                Call ReportFunction.TextChange(objRpt, "lblTenIII", Language.getMessage(mstrModuleName, 132, "iii)"))
                Call ReportFunction.TextChange(objRpt, "txtTenIIIDesc", Language.getMessage(mstrModuleName, 133, "Voluntarily assign to the Tanzania Agricultural Development Bank all the rights relating to my terminal benefits or gratuity or pension from either NSSF or PSSSF or any other relevant pension fund or scheme, for the purposes of satisfying any outstanding liability and interest accrued upon cessation of my employment at TADB by whatever means, such to include but not to be limited to my resignation, abscondment, dismissal, retrenchment or termination."))

                Call ReportFunction.TextChange(objRpt, "lblEleven", Language.getMessage(mstrModuleName, 130, "11."))
                Call ReportFunction.TextChange(objRpt, "txtEleven", Language.getMessage(mstrModuleName, 131, "GENERAL INDEMNITY"))
                Call ReportFunction.TextChange(objRpt, "txtElevenDesc", Language.getMessage(mstrModuleName, 133, "The Employee shall indemnify the Bank against all losses and expenses which the Bank incurs as a result of the occurrence of:"))
                Call ReportFunction.TextChange(objRpt, "lblElevenI", Language.getMessage(mstrModuleName, 132, "i)"))
                Call ReportFunction.TextChange(objRpt, "txtElevenIDesc", Language.getMessage(mstrModuleName, 133, "an Event of Default; and or"))
                Call ReportFunction.TextChange(objRpt, "lblElevenII", Language.getMessage(mstrModuleName, 132, "ii)"))
                Call ReportFunction.TextChange(objRpt, "txtElevenIIDesc", Language.getMessage(mstrModuleName, 133, "The failure of the Employee to pay any amount due under this Facility on the due date"))

                Call ReportFunction.TextChange(objRpt, "lblTwelve", Language.getMessage(mstrModuleName, 130, "12."))
                Call ReportFunction.TextChange(objRpt, "txtTwelve", Language.getMessage(mstrModuleName, 131, "LAW AND DISPUTE RESOLUTION:"))
                Call ReportFunction.TextChange(objRpt, "txtTwelveDesc", Language.getMessage(mstrModuleName, 133, "This agreement shall be construed in accordance with the laws of Tanzania. The parties to the agreement submit themselves to the Courts of competent jurisdiction for adjudication of any dispute arising from or in connection with this agreement."))

                Call ReportFunction.TextChange(objRpt, "lblThirteen", Language.getMessage(mstrModuleName, 130, "13."))
                Call ReportFunction.TextChange(objRpt, "txtThirteen", Language.getMessage(mstrModuleName, 131, "ACCEPTANCE:"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenDesc1", Language.getMessage(mstrModuleName, 133, "These terms and conditions may be amended from time to time by supplementary terms and conditions to the effect and extent only as may be set out in such supplementary terms and conditions."))

                Call ReportFunction.TextChange(objRpt, "lblFourteen", Language.getMessage(mstrModuleName, 130, "14."))
                Call ReportFunction.TextChange(objRpt, "txtFourteen", Language.getMessage(mstrModuleName, 131, "HUMAN RESOURCES & ADMINISTRATION VERIFICATION:"))

                Call ReportFunction.TextChange(objRpt, "lblFifteen", Language.getMessage(mstrModuleName, 130, "15."))
                Call ReportFunction.TextChange(objRpt, "txtFifteen", Language.getMessage(mstrModuleName, 131, "HUMAN RESOURCES VERIFICATION:"))

                'Hemant (26 Apr 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2567 : Loan application form changes
                'Call ReportFunction.TextChange(objRpt, "lblSixteen", Language.getMessage(mstrModuleName, 130, "16."))
                'Call ReportFunction.TextChange(objRpt, "txtSixteen", Language.getMessage(mstrModuleName, 131, "RECOMMENDATIONS BY HEAD OF HUMAN RESOURCES AND ADMINISTRATION:"))

                'Call ReportFunction.TextChange(objRpt, "lblSeventeen", Language.getMessage(mstrModuleName, 130, "17."))
                'Call ReportFunction.TextChange(objRpt, "txtSeventeen", Language.getMessage(mstrModuleName, 131, "MANAGING DIRECTOR’S APPROVAL/DISAPPROVAL"))

                'Call ReportFunction.TextChange(objRpt, "lblEighteen", Language.getMessage(mstrModuleName, 130, "18."))
                'Call ReportFunction.TextChange(objRpt, "txtEighteen", Language.getMessage(mstrModuleName, 131, "HEAD OF LEGAL SERVICES:"))

                'Call ReportFunction.TextChange(objRpt, "txtDeclarationForm", Language.getMessage(mstrModuleName, 131, "DECLARATION FORM"))
                'Call ReportFunction.TextChange(objRpt, "lblNinteen", Language.getMessage(mstrModuleName, 130, "19."))
                'Call ReportFunction.TextChange(objRpt, "txtNinteen", Language.getMessage(mstrModuleName, 131, "ASSIGNMENT OF TERMINAL BENEFITS/GRATUITY TO LIQUIDATE OUTSTANDING LOAN(S):"))
                Call ReportFunction.TextChange(objRpt, "txtSixteen", Language.getMessage(mstrModuleName, 131, "LOAN VERIFICATION / APPROVAL:"))

                objRpt.Subreports("rptLoanApproverDetailReport").SetDataSource(rpt_LoanApprovers)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtNo", Language.getMessage(mstrModuleName, 17, "No."))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtLevelName", Language.getMessage(mstrModuleName, 17, "Level"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApprover", Language.getMessage(mstrModuleName, 17, "Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtSignature", Language.getMessage(mstrModuleName, 17, "Signature"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApprovalDate", Language.getMessage(mstrModuleName, 17, "Date"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtRemarks", Language.getMessage(mstrModuleName, 17, "Remarks"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtStatus", Language.getMessage(mstrModuleName, 17, "Status"))
                'Hemant (26 Apr 2024) -- End

                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                Call ReportFunction.TextChange(objRpt, "lblNinteen", Language.getMessage(mstrModuleName, 130, "19."))
                Call ReportFunction.TextChange(objRpt, "txtNinteen", Language.getMessage(mstrModuleName, 131, "SALARY ADVANCE SETTLEMENT [CAD]:"))
                'Hemant (22 Nov 2024) -- End




            End If
            'Hemant (29 Mar 2024) -- End



            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function



#End Region


	
End Class

