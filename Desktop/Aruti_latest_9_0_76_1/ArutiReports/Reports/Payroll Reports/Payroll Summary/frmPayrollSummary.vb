#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
#End Region

Public Class frmPayrollSummary
#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmPayrollSummary"
    Private objCompanyTotal As clsPayrollSummary
    Private intCurrPeriodID As Integer = 0

    'Sohail (12 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    'Sohail (12 May 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    'S.SANDEEP [ 11 SEP 2012 ] -- END

    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
    Private dvHeads As DataView
    Private mstrSearchEmpText As String = ""
    Private mstrInfoHeadIDs As String = ""
    'Sohail (30 Dec 2019) -- End

#End Region

#Region "Constructor"
    Public Sub New()
        objCompanyTotal = New clsPayrollSummary(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCompanyTotal.SetDefaultValue()
        InitializeComponent()


        'Pinkal (21-Jan-2013) -- Start
        'Enhancement : TRA Changes
        _Show_ExcelExtra_Menu = True
        'Pinkal (21-Jan-2013) -- End

    End Sub
#End Region

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
#Region " Private Enum "
    Private Enum enHeadTypeId
        IncludeInactiveEmployee = 1
        IgnoreZeroValueHeads = 2
        ShowEmployerContribution = 3
        ShowInformationalHeads = 4
        ShowAnalysisOnNewPage = 5
        LogoSettings = 6
        'Sohail (30 Dec 2019) -- Start
        'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
        InfoHeadIDs = 7
        'Sohail (30 Dec 2019) -- End
    End Enum
#End Region
    'Sohail (23 Sep 2016) -- End

#Region "Public Function"
    Public Sub FillCombo()
        Try
            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objExRate As New clsExchangeRate
            Dim dsList As New DataSet
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            'Vimal (27 Nov 2010) -- Start 

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True).Tables(0)
            cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True).Tables(0)
            'Sohail (21 Aug 2015) -- End
            'cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(1, , , True).Tables(0)
            'Vimal (27 Nov 2010) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objExRate.getComboList("ExRate", True)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 11 SEP 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
    Private Sub FillList()
        Dim objHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            RemoveHandler txtSearchHead.TextChanged, AddressOf txtSearchHead_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchHead.TextChanged, AddressOf txtSearchHead_TextChanged

            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, enTranHeadType.Informational)

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            If mstrInfoHeadIDs.Trim <> "" Then
                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (mstrInfoHeadIDs.Split(",").Contains(p.Item("tranheadunkid").ToString)) Select (p)).ToList
                For Each r As DataRow In lstRow
                    r.Item("IsChecked") = True
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            dvHeads = dsList.Tables(0).DefaultView
            dgHeads.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhTranheadunkid.DataPropertyName = "tranheadunkid"
            dgColhHeadCode.DataPropertyName = "code"
            dgColhHeadName.DataPropertyName = "name"

            dgHeads.DataSource = dvHeads
            dvHeads.Sort = "IsChecked DESC, name "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgHeads.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            Dim mintCount As Integer = dvHeads.Table.Select("IsChecked = 1 ").Length
            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dvHeads.Table.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dvHeads.Table.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = Language.getMessage(mstrModuleName, 5, "Search Heads")
            With txtSearchHead
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            If dvHeads IsNot Nothing Then
                For Each dr As DataRowView In dvHeads
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvHeads.ToTable.AcceptChanges()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHeads", mstrModuleName)
        End Try
    End Sub
    'Sohail (30 Dec 2019) -- End

    Public Sub ResetValue()
        Try

            'Vimal (10 Dec 2010) -- Start 

            Dim objMaster As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)
            'Sohail (29 Jan 2016) -- Start
            'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
            'intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date, FinancialYear._Object._YearUnkid)
            intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'Sohail (29 Jan 2016) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            cboPeriod.SelectedValue = intCurrPeriodID
            objMaster = Nothing


            'objCompanyTotal.setDefaultOrderBy(cboPeriod.SelectedIndex)
            objCompanyTotal.setDefaultOrderBy(0)
            'Vimal (10 Dec 2010) -- End

            txtOrderBy.Text = objCompanyTotal.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            chkIgnorezeroHead.Checked = True
            'Pinkal (22-Mar-2012) -- End

            'Sohail (23 Sep 2016) -- Start
            'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
            chkShowEmployersContribution.Checked = False
            chkShowInformational.Checked = False
            'Sohail (23 Sep 2016) -- End

            'Sohail (12 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_TableName = ""
            'Sohail (12 May 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            'Sohail (23 Sep 2016) -- Start
            'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
            'Select Case ConfigParameter._Object._LogoOnPayslip
            '    Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
            '        radLogoCompanyInfo.Checked = True
            '    Case enLogoOnPayslip.COMPANY_DETAILS
            '        radCompanyDetails.Checked = True
            '    Case enLogoOnPayslip.LOGO
            '        radLogo.Checked = True
            'End Select
            Call GetValue()
            'Sohail (23 Sep 2016) -- End
            'Sohail (21 Jan 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PayrollSummary)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.IncludeInactiveEmployee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IgnoreZeroValueHeads
                            chkIgnorezeroHead.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowEmployerContribution
                            chkShowEmployersContribution.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowInformationalHeads
                            chkShowInformational.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowAnalysisOnNewPage
                            chkShowEachAnalysisOnNewPage.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.LogoSettings

                            If CInt(radLogoCompanyInfo.Tag) = CInt(dsRow.Item("transactionheadid")) Then
                                radLogoCompanyInfo.Checked = True
                            ElseIf CInt(radCompanyDetails.Tag) = CInt(dsRow.Item("transactionheadid")) Then
                                radCompanyDetails.Checked = True
                            ElseIf CInt(radLogo.Tag) = CInt(dsRow.Item("transactionheadid")) Then
                                radLogo.Checked = True
                            Else
                                Select Case ConfigParameter._Object._LogoOnPayslip
                                    Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                                        radLogoCompanyInfo.Checked = True
                                    Case enLogoOnPayslip.COMPANY_DETAILS
                                        radCompanyDetails.Checked = True
                                    Case enLogoOnPayslip.LOGO
                                        radLogo.Checked = True
                                End Select
                            End If

                            'Sohail (30 Dec 2019) -- Start
                            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                        Case enHeadTypeId.InfoHeadIDs
                            mstrInfoHeadIDs = dsRow.Item("transactionheadid").ToString
                            'Sohail (30 Dec 2019) -- End

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Sep 2016) -- End

    Public Function SetFilter() As Boolean
        Try

            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            mstrInfoHeadIDs = ""
            'Sohail (30 Dec 2019) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, No exchange rate defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Function
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            objCompanyTotal.SetDefaultValue()

            objCompanyTotal._PeriodId = cboPeriod.SelectedValue
            objCompanyTotal._PeriodName = cboPeriod.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objCompanyTotal._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            objCompanyTotal._IgnoreZeroHeads = chkIgnorezeroHead.Checked
            'Pinkal (22-Mar-2012) -- End

            'Sohail (16 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objCompanyTotal._PeriodStartDate = objPeriod._Start_Date
            objCompanyTotal._PeriodEndDate = objPeriod._End_Date
            'Sohail (16 Apr 2012) -- End

            'Sohail (23 Sep 2016) -- Start
            'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
            objCompanyTotal._ShowEmployerContribution = chkShowEmployersContribution.Checked
            objCompanyTotal._ShowInformational = chkShowInformational.Checked
            'Sohail (23 Sep 2016) -- End

            'Sohail (12 May 2012) -- Start
            'TRA - ENHANCEMENT
            objCompanyTotal._ViewByIds = mstrStringIds
            objCompanyTotal._ViewIndex = mintViewIdx
            objCompanyTotal._ViewByName = mstrStringName
            objCompanyTotal._Analysis_Fields = mstrAnalysis_Fields
            objCompanyTotal._Analysis_Join = mstrAnalysis_Join
            objCompanyTotal._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCompanyTotal._Report_GroupName = mstrReport_GroupName
            objCompanyTotal._Analysis_TableName = mstrAnalysis_TableName
            'Sohail (12 May 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objCompanyTotal._Ex_Rate = mdecEx_Rate
            objCompanyTotal._Currency_Sign = mstrCurr_Sign
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            'Anjan [16 November 2015] -- Start
            'ENHANCEMENT : Include setting for Analysis by to be shown on each page. Requested by Rutta for VFT.
            objCompanyTotal._ShowAnalysisOnNewPage = chkShowEachAnalysisOnNewPage.Checked
            'Anjan [16 November 2015] -- End

            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            mstrInfoHeadIDs = String.Join(",", (From p In dvHeads.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("tranheadunkid").ToString)).ToArray)
            objCompanyTotal._InfoHeadIDs = mstrInfoHeadIDs
            'Sohail (30 Dec 2019) -- End

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            objCompanyTotal._IsLogoCompanyInfo = radLogoCompanyInfo.Checked
            objCompanyTotal._IsOnlyLogo = radLogo.Checked
            objCompanyTotal._IsOnlyCompanyInfo = radCompanyDetails.Checked
            objCompanyTotal._PayslipTemplate = ConfigParameter._Object._PayslipTemplate
            'Sohail (21 Jan 2016) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Form's Events"

    Private Sub frmPayrollSummary_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCompanyTotal = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollSummary_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objCompanyTotal._ReportName
            Me._Message = objCompanyTotal._ReportDesc

            Call FillCombo()
            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            gbHeads.Enabled = False
            'Sohail (30 Dec 2019) -- End

            Dim objMaster As New clsMasterData
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)
            intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date, FinancialYear._Object._YearUnkid)
            'S.SANDEEP [04 JUN 2015] -- END

            cboPeriod.SelectedValue = intCurrPeriodID
            objMaster = Nothing

            Call ResetValue()
            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            Call FillList()
            'Sohail (30 Dec 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollSummary_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmPayrollSummary_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollSummary_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_KeyPress", mstrModuleName)
        End Try
    End Sub



#End Region

#Region "Buttons"
    Private Sub frmPayrollSummary_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If CInt(cboPeriod.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            If SetFilter() = False Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objCompanyTotal.generateReport(0, e.Type, Aruti.Data.enExportAction.None)

            'Nilay (10-Feb-2016) -- Start
            'objCompanyTotal.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                 ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
            '                                 0, e.Type, Aruti.Data.enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer..
            objCompanyTotal._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objCompanyTotal.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              objPeriod._Start_Date, _
                                              objPeriod._End_Date, _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                              0, e.Type, Aruti.Data.enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Nilay (10-Feb-2016) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollSummary_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objCompanyTotal.generateReport(0, enPrintAction.None, e.Type)

            'Nilay (10-Feb-2016) -- Start
            'objCompanyTotal.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                  ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
            '                                  0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer..
            objCompanyTotal._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objCompanyTotal.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              objPeriod._Start_Date, _
                                              objPeriod._End_Date, _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                              0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Nilay (10-Feb-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollSummary_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollSummary_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollSummary_Cancel_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmPayrollSummary_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollSummary.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollSummary"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Pinkal (03-Sep-2012) -- End

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1
                objUserDefRMode._Reportunkid = enArutiReport.PayrollSummary
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.IncludeInactiveEmployee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkInactiveemp.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollSummary, 0, 0, intHeadType)

                    Case enHeadTypeId.IgnoreZeroValueHeads
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkIgnorezeroHead.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollSummary, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowEmployerContribution
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkShowEmployersContribution.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollSummary, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowInformationalHeads
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkShowInformational.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollSummary, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowAnalysisOnNewPage
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkShowEachAnalysisOnNewPage.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollSummary, 0, 0, intHeadType)

                    Case enHeadTypeId.LogoSettings
                        objUserDefRMode._Headtypeid = intHeadType
                        If radLogoCompanyInfo.Checked = True Then
                            objUserDefRMode._EarningTranHeadIds = radLogoCompanyInfo.Tag.ToString
                        ElseIf radCompanyDetails.Checked = True Then
                            objUserDefRMode._EarningTranHeadIds = radCompanyDetails.Tag.ToString
                        ElseIf radLogo.Checked = True Then
                            objUserDefRMode._EarningTranHeadIds = radLogo.Tag.ToString
                        End If

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollSummary, 0, 0, intHeadType)

                        'Sohail (30 Dec 2019) -- Start
                        'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                    Case enHeadTypeId.InfoHeadIDs
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrInfoHeadIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollSummary, 0, 0, intHeadType)
                        'Sohail (30 Dec 2019) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Sep 2016) -- End

#End Region

    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
#Region " GridView Events "

    Private Sub dgHeads_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgHeads.CurrentCellDirtyStateChanged, dgHeads.CurrentCellDirtyStateChanged
        Try
            If dgHeads.IsCurrentCellDirty Then
                dgHeads.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgHeads_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgHeads_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgHeads.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgHeads_CellValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBoxs Events "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllHeads(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowInformational_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInformational.CheckedChanged
        Try
            gbHeads.Enabled = chkShowInformational.Checked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowInformational_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Textbox Events "

    Private Sub txtSearchHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchHead.GotFocus
        Try
            With txtSearchHead
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchHead.Leave
        Try
            If txtSearchHead.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchHead_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchHead.TextChanged
        Try
            If txtSearchHead.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvHeads IsNot Nothing Then
                dvHeads.RowFilter = "code LIKE '%" & txtSearchHead.Text.Replace("'", "''") & "%'  OR name LIKE '%" & txtSearchHead.Text.Replace("'", "''") & "%'"
                dgHeads.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (30 Dec 2019) -- End

#Region " Controls "
    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            'Vimal (10 Dec 2010) -- Start 
            objCompanyTotal.setOrderBy(0)
            'Vimal (10 Dec 2010) -- End
            txtOrderBy.Text = objCompanyTotal.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_TableName = frm._Analysis_TableName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (12 May 2012) -- End


    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    Dim dsList As DataSet
                    Dim objPeriod As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 3, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub
    'S.SANDEEP [ 11 SEP 2012 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbHeads.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbHeads.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.chkIgnorezeroHead.Text = Language._Object.getCaption(Me.chkIgnorezeroHead.Name, Me.chkIgnorezeroHead.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.chkShowEachAnalysisOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachAnalysisOnNewPage.Name, Me.chkShowEachAnalysisOnNewPage.Text)
            Me.radLogo.Text = Language._Object.getCaption(Me.radLogo.Name, Me.radLogo.Text)
            Me.radCompanyDetails.Text = Language._Object.getCaption(Me.radCompanyDetails.Name, Me.radCompanyDetails.Text)
            Me.radLogoCompanyInfo.Text = Language._Object.getCaption(Me.radLogoCompanyInfo.Name, Me.radLogoCompanyInfo.Text)
            Me.chkShowInformational.Text = Language._Object.getCaption(Me.chkShowInformational.Name, Me.chkShowInformational.Text)
            Me.chkShowEmployersContribution.Text = Language._Object.getCaption(Me.chkShowEmployersContribution.Name, Me.chkShowEmployersContribution.Text)
            Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.gbHeads.Text = Language._Object.getCaption(Me.gbHeads.Name, Me.gbHeads.Text)
			Me.dgColhHeadCode.HeaderText = Language._Object.getCaption(Me.dgColhHeadCode.Name, Me.dgColhHeadCode.HeaderText)
			Me.dgColhHeadName.HeaderText = Language._Object.getCaption(Me.dgColhHeadName.Name, Me.dgColhHeadName.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Period.")
            Language.setMessage(mstrModuleName, 2, "Sorry, No exchange rate defined for this currency for the period selected.")
            Language.setMessage(mstrModuleName, 3, "Exchange Rate :")
            Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 5, "Search Heads")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
