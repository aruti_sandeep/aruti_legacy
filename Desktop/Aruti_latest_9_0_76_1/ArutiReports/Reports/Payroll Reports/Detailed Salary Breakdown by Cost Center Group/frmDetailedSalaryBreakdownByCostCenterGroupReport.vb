#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDetailedSalaryBreakdownByCostCenterGroupReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDetailedSalaryBreakdownByCostCenterGroupReport"
    Private objPayroll As clsDetailedSalaryBreakdownByCostCenterGroupReport

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""


    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty

    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty

    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrAdvanceFilter As String = String.Empty

    Private marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds As New ArrayList

#End Region

#Region " Constructor "

    Public Sub New()
        objPayroll = New clsDetailedSalaryBreakdownByCostCenterGroupReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPayroll.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

    'Sohail (10 Jun 2020) -- Start
    'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
#Region " Private Enum "
    Private Enum enHeadTypeId
        CostCenterGroup = 1
        CostCenter = 2
        Branch = 3
        IncludeInactiveEmployee = 4
        ShowGroupByCosCenterGroup = 5
    End Enum
#End Region
    'Sohail (10 Jun 2020) -- End

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objCCGroup As New clspayrollgroup_master
        Dim objCCenter As New clscostcenter_master
        Dim objBranch As New clsStation
        Dim objExRate As New clsExchangeRate
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Try


            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsList = objCCGroup.getListForCombo(enPayrollGroupType.CostCenter, "List", True)
            With cboCCenterGrp
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objCCenter.getComboList("List", True)
            With cboCCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If

            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With


            dsList = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("HeadType")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCCenter = Nothing
            objCCGroup = Nothing
            objBranch = Nothing
            objExRate = Nothing
            objMaster = Nothing
        End Try
    End Sub


    Private Sub ResetValue()
        Try

            txtOrderBy.Text = objPayroll.OrderByDisplay

            cboPeriod.SelectedValue = mintFirstOpenPeriod

            cboCCenter.SelectedValue = 0
            cboCCenterGrp.SelectedValue = 0
            cboBranch.SelectedValue = 0


            chkInactiveemp.Checked = False

            'Sohail (10 Jun 2020) -- Start
            'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
            chkGroupByCostCenterGroup.Checked = True
            'Sohail (10 Jun 2020) -- End

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""




            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            mstrCurrency_Rate = ""

            mstrAdvanceFilter = ""

            'Sohail (10 Jun 2020) -- Start
            'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
            Call GetValue()
            'Sohail (10 Jun 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jun 2020) -- Start
    'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))


                        Case enHeadTypeId.CostCenterGroup
                            cboCCenterGrp.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.CostCenter
                            cboCCenter.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Branch
                            cboBranch.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IncludeInactiveEmployee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowGroupByCosCenterGroup
                            chkGroupByCostCenterGroup.Checked = CBool(dsRow.Item("transactionheadid"))

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Jun 2020) -- End

    Private Sub FillCustomHeads()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            Dim dtTable As New DataTable
            Dim lvItem As ListViewItem
            lvCustomTranHead.Items.Clear()

            Dim lvArray As New List(Of ListViewItem)
            lvCustomTranHead.BeginUpdate()

            Dim ds As DataSet
            If marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.Count > 0 Then
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.ToArray(Type.GetType("System.String"))) & ")")
                ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.ToArray(Type.GetType("System.String"))) & ")")
            Else
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , )
                ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , )
            End If
            Dim a As Dictionary(Of Integer, DataRow) = (From p In ds.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("tranheadunkid")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW)

            dtTable.Merge(dsList.Tables(0), True)
            Dim intPos As Integer = 0
            For Each itm In marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds
                Dim dr As DataRow = dtTable.NewRow
                dr.ItemArray = a.Item(CInt(itm)).ItemArray
                dtTable.Rows.InsertAt(dr, intPos)
                intPos += 1
            Next

            For Each dsRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))

                lvItem.SubItems.Add(dsRow.Item("code").ToString)
                lvItem.SubItems.Add(dsRow.Item("name").ToString)

                If marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.Contains(dsRow.Item("tranheadunkid").ToString) = True Then
                    lvItem.Checked = True
                Else
                    lvItem.Checked = False
                End If
                lvArray.Add(lvItem)
            Next

            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
            lvCustomTranHead.Items.AddRange(lvArray.ToArray)
            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked

            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            If lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            End If
            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged

            If lvCustomTranHead.Items.Count > 15 Then
                colhCName.Width = 240 - 20
            Else
                colhCName.Width = 240
            End If
            lvArray = Nothing
            lvCustomTranHead.EndUpdate()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCustomHeads", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf lvCustomTranHead.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Please select atleast one transaction head in order to generate report."), enMsgBoxStyle.Information)
                Return False
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
            End If



            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmDetailedSalaryBreakdownByCostCenterGroupReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objPayroll = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDetailedSalaryBreakdownByCostCenterGroupReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDetailedSalaryBreakdownByCostCenterGroupReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.Clear()
            If ConfigParameter._Object._DetailedSalaryBreakdownReportByCCenterGroupHeadsIds.Trim <> "" Then
                marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.AddRange(ConfigParameter._Object._DetailedSalaryBreakdownReportByCCenterGroupHeadsIds.Split(","))
            End If

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDetailedSalaryBreakdownByCostCenterGroupReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim arrDatabaseName As New ArrayList
        Dim intPrevYearID As Integer = 0
        Try
            If IsValidate() = False Then Exit Sub


            objPayroll.SetDefaultValue()



            Dim i As Integer = 0
            Dim strList As String = cboPeriod.SelectedValue.ToString
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboPeriod.SelectedItem, DataRowView).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                intPrevYearID = CInt(CType(cboPeriod.SelectedItem, DataRowView).Item("yearunkid"))
            End If



            objPayroll._PeriodIdList = strList
            objPayroll._Arr_DatabaseName = arrDatabaseName


            objPayroll._PeriodId = cboPeriod.SelectedValue
            objPayroll._PeriodName = cboPeriod.Text

            objPayroll._IsActive = chkInactiveemp.Checked
            'Sohail (10 Jun 2020) -- Start
            'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
            objPayroll._ShowGroupByCostCenterGroup = chkGroupByCostCenterGroup.Checked
            'Sohail (10 Jun 2020) -- End

            objPayroll._CCenterId = cboCCenter.SelectedValue
            objPayroll._CCenter_Name = cboCCenter.Text
            objPayroll._CCGroupId = cboCCenterGrp.SelectedValue
            objPayroll._CCGroup_Name = cboCCenterGrp.Text
            objPayroll._BranchId = cboBranch.SelectedValue
            objPayroll._Branch_Name = cboBranch.Text

            objPayroll._ViewByIds = mstrStringIds
            objPayroll._ViewIndex = mintViewIdx
            objPayroll._ViewByName = mstrStringName
            objPayroll._Analysis_Fields = mstrAnalysis_Fields
            objPayroll._Analysis_Join = mstrAnalysis_Join
            objPayroll._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPayroll._Report_GroupName = mstrReport_GroupName



            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objPayroll._PeriodStartDate = objPeriod._Start_Date
            objPayroll._PeriodEndDate = objPeriod._End_Date


            objPayroll._Ex_Rate = mdecEx_Rate
            objPayroll._Currency_Sign = mstrCurr_Sign
            objPayroll._Currency_Rate = mstrCurrency_Rate

            objPayroll._Advance_Filter = mstrAdvanceFilter



            Dim xCSVHeads As String = String.Empty
            xCSVHeads = String.Join(",", lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)().[Select](Function(x) x.Tag.ToString()).ToArray())
            objPayroll._DetailedSalaryBreakdownReportByCCenterGroupHeadsIds = xCSVHeads
            Dim xAllocVal As New Dictionary(Of Integer, String)
            xAllocVal = lvCustomTranHead.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(x) x.SubItems(colhCName.Index).Text)

            objPayroll._DetailedSalaryBreakdown = xAllocVal

            objPayroll.Generate_DetailedSalaryBreakdownReport(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              ConfigParameter._Object._UserAccessModeSetting, True)




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'Hemant (02 May 2019) -- Start
            'ISSUE : Not able to Change Language
            'clsPayrollReport.SetMessages()
            'objfrm._Other_ModuleNames = "clsPayrollReport"
            clsDetailedSalaryBreakdownByCostCenterGroupReport.SetMessages()
            objfrm._Other_ModuleNames = "clsDetailedSalaryBreakdownByCostCenterGroupReport"
            'Hemant (02 May 2019) --) -- End
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If lvCustomTranHead.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvCustomTranHead.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvCustomTranHead.Items(SelIndex - 1)
                lvCustomTranHead.Items(SelIndex - 1) = lvCustomTranHead.Items(SelIndex).Clone
                lvCustomTranHead.Items(SelIndex) = tItem.Clone
                lvCustomTranHead.Items(SelIndex - 1).Selected = True
                lvCustomTranHead.Items(SelIndex - 1).EnsureVisible()
            End If
            lvCustomTranHead.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Try
            If lvCustomTranHead.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvCustomTranHead.SelectedIndices.Item(0)
                If SelIndex = lvCustomTranHead.Items.Count - 1 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvCustomTranHead.Items(SelIndex + 1)
                lvCustomTranHead.Items(SelIndex + 1) = lvCustomTranHead.Items(SelIndex).Clone
                lvCustomTranHead.Items(SelIndex) = tItem.Clone
                lvCustomTranHead.Items(SelIndex + 1).Selected = True
                lvCustomTranHead.Items(SelIndex + 1).EnsureVisible()
            End If
            lvCustomTranHead.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDown_Click", mstrModuleName)
        Finally
        End Try
    End Sub



#End Region

#Region " Listview Events "

    Private Sub lvCustomTranHead_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvCustomTranHead.ItemChecked
        Try
            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            If lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            End If

            marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.Clear()
            marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvCustomTranHead_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub objchkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkCheckAll.CheckedChanged
        Try
            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
            For Each lvItem As ListViewItem In lvCustomTranHead.Items
                lvItem.Checked = objchkCheckAll.CheckState
            Next

            marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.Clear()
            marrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate

                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 4, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                        mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " " 'Sohail (16 Mar 2013)
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                    mstrCurrency_Rate = ""
                End If
            End If

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            Call FillCustomHeads()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objPayroll.setOrderBy(0)
            txtOrderBy.Text = objPayroll.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchCHeads_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchCHeads.TextChanged
        Try
            If lvCustomTranHead.Items.Count <= 0 Then Exit Sub
            lvCustomTranHead.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvCustomTranHead.FindItemWithText(txtSearchCHeads.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvCustomTranHead.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchCHeads_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            If IsValidate() = False Then Exit Sub

            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid

            Dim xCheckedItems As String = String.Empty
            If lvCustomTranHead.CheckedItems.Count > 0 Then
                xCheckedItems = String.Join(",", lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)().[Select](Function(x) x.Tag.ToString()).ToArray())
                If xCheckedItems.Trim.Length > 0 Then
                    objConfig._DetailedSalaryBreakdownReportByCCenterGroupHeadsIds = xCheckedItems
                End If
            End If
            xCheckedItems = ""


            objConfig.updateParam()
            ConfigParameter._Object.Refresh()

            'Sohail (10 Jun 2020) -- Start
            'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
            Dim objUserDefRMode As New clsUserDef_ReportMode
            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1
                objUserDefRMode._Reportunkid = enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.CostCenterGroup
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboCCenterGrp.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP, 0, 0, intHeadType)

                    Case enHeadTypeId.CostCenter
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboCCenter.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP, 0, 0, intHeadType)

                    Case enHeadTypeId.Branch
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboBranch.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP, 0, 0, intHeadType)

                    Case enHeadTypeId.IncludeInactiveEmployee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkInactiveemp.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowGroupByCosCenterGroup
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkGroupByCostCenterGroup.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            'Sohail (10 Jun 2020) -- End

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Settings saved successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbCustomSetting.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCustomSetting.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnUp.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnUp.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnDown.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnDown.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbCustomSetting.Text = Language._Object.getCaption(Me.gbCustomSetting.Name, Me.gbCustomSetting.Text)
			Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
			Me.colhCCode.Text = Language._Object.getCaption(CStr(Me.colhCCode.Tag), Me.colhCCode.Text)
			Me.colhCName.Text = Language._Object.getCaption(CStr(Me.colhCName.Tag), Me.colhCName.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblCCenterGrp.Text = Language._Object.getCaption(Me.lblCCenterGrp.Name, Me.lblCCenterGrp.Text)
			Me.lblCCenter.Text = Language._Object.getCaption(Me.lblCCenter.Name, Me.lblCCenter.Text)
			Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.chkGroupByCostCenterGroup.Text = Language._Object.getCaption(Me.chkGroupByCostCenterGroup.Name, Me.chkGroupByCostCenterGroup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Please select atleast one transaction head in order to generate report.")
			Language.setMessage(mstrModuleName, 3, "Sorry, No exchange rate is defined for this currency for the period selected.")
			Language.setMessage(mstrModuleName, 4, "Exchange Rate :")
			Language.setMessage(mstrModuleName, 5, "Settings saved successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
