﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollTotalVariance
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayrollTotalVariance))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbHeads = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgHeads = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhTranheadunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhHeadCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhHeadName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchHead = New System.Windows.Forms.TextBox
        Me.chkShowVariancePercentageColumns = New System.Windows.Forms.CheckBox
        Me.chkIgnoreZeroVariance = New System.Windows.Forms.CheckBox
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblTrnHeadType = New System.Windows.Forms.Label
        Me.cboPeriodTo = New System.Windows.Forms.ComboBox
        Me.lblPeriodTo = New System.Windows.Forms.Label
        Me.lblPeriodFrom = New System.Windows.Forms.Label
        Me.cboPeriodFrom = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbHeads.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgHeads, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 547)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.gbHeads)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowVariancePercentageColumns)
        Me.gbFilterCriteria.Controls.Add(Me.chkIgnoreZeroVariance)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrnHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrnHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriodTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriodTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriodFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriodFrom)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(728, 404)
        Me.gbFilterCriteria.TabIndex = 12
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(105, 153)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 239
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'gbHeads
        '
        Me.gbHeads.BorderColor = System.Drawing.Color.Black
        Me.gbHeads.Checked = False
        Me.gbHeads.CollapseAllExceptThis = False
        Me.gbHeads.CollapsedHoverImage = Nothing
        Me.gbHeads.CollapsedNormalImage = Nothing
        Me.gbHeads.CollapsedPressedImage = Nothing
        Me.gbHeads.CollapseOnLoad = False
        Me.gbHeads.Controls.Add(Me.pnlEmployeeList)
        Me.gbHeads.ExpandedHoverImage = Nothing
        Me.gbHeads.ExpandedNormalImage = Nothing
        Me.gbHeads.ExpandedPressedImage = Nothing
        Me.gbHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHeads.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHeads.HeaderHeight = 25
        Me.gbHeads.HeaderMessage = ""
        Me.gbHeads.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHeads.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHeads.HeightOnCollapse = 0
        Me.gbHeads.LeftTextSpace = 0
        Me.gbHeads.Location = New System.Drawing.Point(358, 31)
        Me.gbHeads.Name = "gbHeads"
        Me.gbHeads.OpenHeight = 300
        Me.gbHeads.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHeads.ShowBorder = True
        Me.gbHeads.ShowCheckBox = False
        Me.gbHeads.ShowCollapseButton = False
        Me.gbHeads.ShowDefaultBorderColor = True
        Me.gbHeads.ShowDownButton = False
        Me.gbHeads.ShowHeader = True
        Me.gbHeads.Size = New System.Drawing.Size(353, 369)
        Me.gbHeads.TabIndex = 238
        Me.gbHeads.Temp = 0
        Me.gbHeads.Text = "Informational Heads"
        Me.gbHeads.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgHeads)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchHead)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(350, 340)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgHeads
        '
        Me.dgHeads.AllowUserToAddRows = False
        Me.dgHeads.AllowUserToDeleteRows = False
        Me.dgHeads.AllowUserToResizeRows = False
        Me.dgHeads.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgHeads.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgHeads.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgHeads.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgHeads.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgHeads.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhTranheadunkid, Me.dgColhHeadCode, Me.dgColhHeadName})
        Me.dgHeads.Location = New System.Drawing.Point(1, 28)
        Me.dgHeads.Name = "dgHeads"
        Me.dgHeads.RowHeadersVisible = False
        Me.dgHeads.RowHeadersWidth = 5
        Me.dgHeads.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgHeads.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgHeads.Size = New System.Drawing.Size(347, 312)
        Me.dgHeads.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhTranheadunkid
        '
        Me.objdgcolhTranheadunkid.HeaderText = "tranheadunkid"
        Me.objdgcolhTranheadunkid.Name = "objdgcolhTranheadunkid"
        Me.objdgcolhTranheadunkid.ReadOnly = True
        Me.objdgcolhTranheadunkid.Visible = False
        '
        'dgColhHeadCode
        '
        Me.dgColhHeadCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhHeadCode.HeaderText = "Code"
        Me.dgColhHeadCode.Name = "dgColhHeadCode"
        Me.dgColhHeadCode.ReadOnly = True
        Me.dgColhHeadCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgColhHeadName
        '
        Me.dgColhHeadName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhHeadName.HeaderText = "Head Name"
        Me.dgColhHeadName.Name = "dgColhHeadName"
        Me.dgColhHeadName.ReadOnly = True
        '
        'txtSearchHead
        '
        Me.txtSearchHead.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchHead.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchHead.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchHead.Name = "txtSearchHead"
        Me.txtSearchHead.Size = New System.Drawing.Size(346, 21)
        Me.txtSearchHead.TabIndex = 12
        '
        'chkShowVariancePercentageColumns
        '
        Me.chkShowVariancePercentageColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowVariancePercentageColumns.Location = New System.Drawing.Point(104, 107)
        Me.chkShowVariancePercentageColumns.Name = "chkShowVariancePercentageColumns"
        Me.chkShowVariancePercentageColumns.Size = New System.Drawing.Size(168, 18)
        Me.chkShowVariancePercentageColumns.TabIndex = 227
        Me.chkShowVariancePercentageColumns.Text = "Show Variance% Columns"
        Me.chkShowVariancePercentageColumns.UseVisualStyleBackColor = True
        '
        'chkIgnoreZeroVariance
        '
        Me.chkIgnoreZeroVariance.Checked = True
        Me.chkIgnoreZeroVariance.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIgnoreZeroVariance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZeroVariance.Location = New System.Drawing.Point(105, 85)
        Me.chkIgnoreZeroVariance.Name = "chkIgnoreZeroVariance"
        Me.chkIgnoreZeroVariance.Size = New System.Drawing.Size(186, 18)
        Me.chkIgnoreZeroVariance.TabIndex = 199
        Me.chkIgnoreZeroVariance.Text = "Ignore Zero Variance"
        Me.chkIgnoreZeroVariance.UseVisualStyleBackColor = True
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(632, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 197
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(105, 58)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(247, 21)
        Me.cboTrnHeadType.TabIndex = 159
        '
        'lblTrnHeadType
        '
        Me.lblTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHeadType.Location = New System.Drawing.Point(8, 61)
        Me.lblTrnHeadType.Name = "lblTrnHeadType"
        Me.lblTrnHeadType.Size = New System.Drawing.Size(98, 15)
        Me.lblTrnHeadType.TabIndex = 160
        Me.lblTrnHeadType.Text = "Tran. Head Type"
        '
        'cboPeriodTo
        '
        Me.cboPeriodTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodTo.DropDownWidth = 180
        Me.cboPeriodTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriodTo.FormattingEnabled = True
        Me.cboPeriodTo.Location = New System.Drawing.Point(247, 31)
        Me.cboPeriodTo.Name = "cboPeriodTo"
        Me.cboPeriodTo.Size = New System.Drawing.Size(105, 21)
        Me.cboPeriodTo.TabIndex = 152
        '
        'lblPeriodTo
        '
        Me.lblPeriodTo.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriodTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodTo.Location = New System.Drawing.Point(216, 35)
        Me.lblPeriodTo.Name = "lblPeriodTo"
        Me.lblPeriodTo.Size = New System.Drawing.Size(25, 13)
        Me.lblPeriodTo.TabIndex = 151
        Me.lblPeriodTo.Text = "To"
        '
        'lblPeriodFrom
        '
        Me.lblPeriodFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriodFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodFrom.Location = New System.Drawing.Point(8, 35)
        Me.lblPeriodFrom.Name = "lblPeriodFrom"
        Me.lblPeriodFrom.Size = New System.Drawing.Size(81, 13)
        Me.lblPeriodFrom.TabIndex = 110
        Me.lblPeriodFrom.Text = "Period From"
        '
        'cboPeriodFrom
        '
        Me.cboPeriodFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodFrom.DropDownWidth = 180
        Me.cboPeriodFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriodFrom.FormattingEnabled = True
        Me.cboPeriodFrom.Location = New System.Drawing.Point(105, 31)
        Me.cboPeriodFrom.Name = "cboPeriodFrom"
        Me.cboPeriodFrom.Size = New System.Drawing.Size(105, 21)
        Me.cboPeriodFrom.TabIndex = 111
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(9, 476)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(352, 63)
        Me.gbSortBy.TabIndex = 13
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(340, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(91, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(105, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(229, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(104, 131)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(168, 16)
        Me.chkInactiveemp.TabIndex = 241
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'frmPayrollTotalVariance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 602)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayrollTotalVariance"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payroll Total Variance Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbHeads.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgHeads, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents cboPeriodTo As System.Windows.Forms.ComboBox
    Private WithEvents lblPeriodTo As System.Windows.Forms.Label
    Private WithEvents lblPeriodFrom As System.Windows.Forms.Label
    Public WithEvents cboPeriodFrom As System.Windows.Forms.ComboBox
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHeadType As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents chkIgnoreZeroVariance As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowVariancePercentageColumns As System.Windows.Forms.CheckBox
    Friend WithEvents gbHeads As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgHeads As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhTranheadunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhHeadCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhHeadName As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents txtSearchHead As System.Windows.Forms.TextBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class
