﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrbitReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrbitReport))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeReportColumnHeader = New System.Windows.Forms.CheckBox
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.txtBatchName = New System.Windows.Forms.TextBox
        Me.cboCompanyAccountType = New System.Windows.Forms.ComboBox
        Me.lblCompanyAccountType = New System.Windows.Forms.Label
        Me.cboCompanyAccountNo = New System.Windows.Forms.ComboBox
        Me.lblCompanyAccountNo = New System.Windows.Forms.Label
        Me.cboCompanyBranchName = New System.Windows.Forms.ComboBox
        Me.lblCompanyBranchName = New System.Windows.Forms.Label
        Me.cboCompanyBankName = New System.Windows.Forms.ComboBox
        Me.lblCompanyBankName = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblAmountTo = New System.Windows.Forms.Label
        Me.txtAmountTo = New System.Windows.Forms.TextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblAmount = New System.Windows.Forms.Label
        Me.cboBranchName = New System.Windows.Forms.ComboBox
        Me.lblBranchName = New System.Windows.Forms.Label
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.cboBankName = New System.Windows.Forms.ComboBox
        Me.lblBankName = New System.Windows.Forms.Label
        Me.lblCountryName = New System.Windows.Forms.Label
        Me.lblEmpCode = New System.Windows.Forms.Label
        Me.cboEmployeeName = New System.Windows.Forms.ComboBox
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.txtEmpcode = New System.Windows.Forms.TextBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 478)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(683, 55)
        Me.EZeeFooter1.TabIndex = 4
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(390, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(486, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(582, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(683, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Orbit Report"
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.AllowDrop = True
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboReportType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblReportType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkIncludeReportColumnHeader)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBatchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtBatchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCompanyAccountType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCompanyAccountType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCompanyAccountNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCompanyAccountNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCompanyBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCompanyBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCompanyBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCompanyBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblAmountTo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtAmountTo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBranchName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCountry)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBankName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCountryName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmpCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtEmpcode)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(580, 295)
        Me.EZeeCollapsibleContainer1.TabIndex = 0
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeReportColumnHeader
        '
        Me.chkIncludeReportColumnHeader.Checked = True
        Me.chkIncludeReportColumnHeader.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeReportColumnHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeReportColumnHeader.Location = New System.Drawing.Point(141, 247)
        Me.chkIncludeReportColumnHeader.Name = "chkIncludeReportColumnHeader"
        Me.chkIncludeReportColumnHeader.Size = New System.Drawing.Size(214, 17)
        Me.chkIncludeReportColumnHeader.TabIndex = 13
        Me.chkIncludeReportColumnHeader.Text = "Include Report/Column Header"
        Me.chkIncludeReportColumnHeader.UseVisualStyleBackColor = True
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(8, 224)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(127, 13)
        Me.lblBatchName.TabIndex = 236
        Me.lblBatchName.Text = "Batch Name"
        '
        'txtBatchName
        '
        Me.txtBatchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchName.Location = New System.Drawing.Point(141, 220)
        Me.txtBatchName.Name = "txtBatchName"
        Me.txtBatchName.Size = New System.Drawing.Size(126, 20)
        Me.txtBatchName.TabIndex = 12
        '
        'cboCompanyAccountType
        '
        Me.cboCompanyAccountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyAccountType.DropDownWidth = 230
        Me.cboCompanyAccountType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyAccountType.FormattingEnabled = True
        Me.cboCompanyAccountType.Location = New System.Drawing.Point(412, 113)
        Me.cboCompanyAccountType.Name = "cboCompanyAccountType"
        Me.cboCompanyAccountType.Size = New System.Drawing.Size(128, 21)
        Me.cboCompanyAccountType.TabIndex = 5
        '
        'lblCompanyAccountType
        '
        Me.lblCompanyAccountType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyAccountType.Location = New System.Drawing.Point(279, 117)
        Me.lblCompanyAccountType.Name = "lblCompanyAccountType"
        Me.lblCompanyAccountType.Size = New System.Drawing.Size(127, 13)
        Me.lblCompanyAccountType.TabIndex = 234
        Me.lblCompanyAccountType.Text = "Company Account Type"
        '
        'cboCompanyAccountNo
        '
        Me.cboCompanyAccountNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyAccountNo.DropDownWidth = 230
        Me.cboCompanyAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyAccountNo.FormattingEnabled = True
        Me.cboCompanyAccountNo.Location = New System.Drawing.Point(141, 113)
        Me.cboCompanyAccountNo.Name = "cboCompanyAccountNo"
        Me.cboCompanyAccountNo.Size = New System.Drawing.Size(126, 21)
        Me.cboCompanyAccountNo.TabIndex = 4
        '
        'lblCompanyAccountNo
        '
        Me.lblCompanyAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyAccountNo.Location = New System.Drawing.Point(8, 117)
        Me.lblCompanyAccountNo.Name = "lblCompanyAccountNo"
        Me.lblCompanyAccountNo.Size = New System.Drawing.Size(127, 13)
        Me.lblCompanyAccountNo.TabIndex = 231
        Me.lblCompanyAccountNo.Text = "Company Account No."
        '
        'cboCompanyBranchName
        '
        Me.cboCompanyBranchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyBranchName.DropDownWidth = 230
        Me.cboCompanyBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyBranchName.FormattingEnabled = True
        Me.cboCompanyBranchName.Location = New System.Drawing.Point(412, 86)
        Me.cboCompanyBranchName.Name = "cboCompanyBranchName"
        Me.cboCompanyBranchName.Size = New System.Drawing.Size(128, 21)
        Me.cboCompanyBranchName.TabIndex = 3
        '
        'lblCompanyBranchName
        '
        Me.lblCompanyBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyBranchName.Location = New System.Drawing.Point(279, 90)
        Me.lblCompanyBranchName.Name = "lblCompanyBranchName"
        Me.lblCompanyBranchName.Size = New System.Drawing.Size(127, 13)
        Me.lblCompanyBranchName.TabIndex = 229
        Me.lblCompanyBranchName.Text = "Company Bank Branch"
        '
        'cboCompanyBankName
        '
        Me.cboCompanyBankName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyBankName.DropDownWidth = 230
        Me.cboCompanyBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyBankName.FormattingEnabled = True
        Me.cboCompanyBankName.Location = New System.Drawing.Point(141, 86)
        Me.cboCompanyBankName.Name = "cboCompanyBankName"
        Me.cboCompanyBankName.Size = New System.Drawing.Size(126, 21)
        Me.cboCompanyBankName.TabIndex = 2
        '
        'lblCompanyBankName
        '
        Me.lblCompanyBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyBankName.Location = New System.Drawing.Point(8, 90)
        Me.lblCompanyBankName.Name = "lblCompanyBankName"
        Me.lblCompanyBankName.Size = New System.Drawing.Size(127, 13)
        Me.lblCompanyBankName.TabIndex = 226
        Me.lblCompanyBankName.Text = "Company Bank Name"
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(412, 140)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(128, 21)
        Me.cboCurrency.TabIndex = 7
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(279, 144)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(127, 13)
        Me.lblCurrency.TabIndex = 218
        Me.lblCurrency.Text = "Paid Currency"
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Checked = True
        Me.chkIncludeInactiveEmp.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(141, 270)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(214, 17)
        Me.chkIncludeInactiveEmp.TabIndex = 14
        Me.chkIncludeInactiveEmp.Text = "Include inactive employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        Me.chkIncludeInactiveEmp.Visible = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(141, 140)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 6
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 144)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(127, 13)
        Me.lblPeriod.TabIndex = 5
        Me.lblPeriod.Text = "Period"
        '
        'lblAmountTo
        '
        Me.lblAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountTo.Location = New System.Drawing.Point(279, 198)
        Me.lblAmountTo.Name = "lblAmountTo"
        Me.lblAmountTo.Size = New System.Drawing.Size(127, 13)
        Me.lblAmountTo.TabIndex = 13
        Me.lblAmountTo.Text = "To"
        '
        'txtAmountTo
        '
        Me.txtAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountTo.Location = New System.Drawing.Point(412, 194)
        Me.txtAmountTo.Name = "txtAmountTo"
        Me.txtAmountTo.Size = New System.Drawing.Size(128, 20)
        Me.txtAmountTo.TabIndex = 11
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(546, 59)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(8, 198)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(127, 13)
        Me.lblAmount.TabIndex = 11
        Me.lblAmount.Text = "Amount"
        '
        'cboBranchName
        '
        Me.cboBranchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranchName.DropDownWidth = 230
        Me.cboBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranchName.FormattingEnabled = True
        Me.cboBranchName.Location = New System.Drawing.Point(412, 167)
        Me.cboBranchName.Name = "cboBranchName"
        Me.cboBranchName.Size = New System.Drawing.Size(128, 21)
        Me.cboBranchName.TabIndex = 9
        '
        'lblBranchName
        '
        Me.lblBranchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranchName.Location = New System.Drawing.Point(279, 171)
        Me.lblBranchName.Name = "lblBranchName"
        Me.lblBranchName.Size = New System.Drawing.Size(127, 13)
        Me.lblBranchName.TabIndex = 9
        Me.lblBranchName.Text = "Branch Name"
        '
        'txtAmount
        '
        Me.txtAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(141, 194)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(126, 20)
        Me.txtAmount.TabIndex = 10
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.DropDownWidth = 230
        Me.cboCountry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(464, 219)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(126, 21)
        Me.cboCountry.TabIndex = 12
        Me.cboCountry.Visible = False
        '
        'cboBankName
        '
        Me.cboBankName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankName.DropDownWidth = 230
        Me.cboBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankName.FormattingEnabled = True
        Me.cboBankName.Location = New System.Drawing.Point(141, 167)
        Me.cboBankName.Name = "cboBankName"
        Me.cboBankName.Size = New System.Drawing.Size(126, 21)
        Me.cboBankName.TabIndex = 8
        '
        'lblBankName
        '
        Me.lblBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankName.Location = New System.Drawing.Point(8, 171)
        Me.lblBankName.Name = "lblBankName"
        Me.lblBankName.Size = New System.Drawing.Size(127, 13)
        Me.lblBankName.TabIndex = 7
        Me.lblBankName.Text = "Emp. Bank Name"
        '
        'lblCountryName
        '
        Me.lblCountryName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountryName.Location = New System.Drawing.Point(338, 222)
        Me.lblCountryName.Name = "lblCountryName"
        Me.lblCountryName.Size = New System.Drawing.Size(127, 13)
        Me.lblCountryName.TabIndex = 5
        Me.lblCountryName.Text = "Country Name"
        Me.lblCountryName.Visible = False
        '
        'lblEmpCode
        '
        Me.lblEmpCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCode.Location = New System.Drawing.Point(358, 248)
        Me.lblEmpCode.Name = "lblEmpCode"
        Me.lblEmpCode.Size = New System.Drawing.Size(127, 13)
        Me.lblEmpCode.TabIndex = 3
        Me.lblEmpCode.Text = "Emp.  Code"
        Me.lblEmpCode.Visible = False
        '
        'cboEmployeeName
        '
        Me.cboEmployeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeName.DropDownWidth = 150
        Me.cboEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeName.FormattingEnabled = True
        Me.cboEmployeeName.Location = New System.Drawing.Point(141, 59)
        Me.cboEmployeeName.Name = "cboEmployeeName"
        Me.cboEmployeeName.Size = New System.Drawing.Size(399, 21)
        Me.cboEmployeeName.TabIndex = 1
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(127, 13)
        Me.lblEmployeeName.TabIndex = 0
        Me.lblEmployeeName.Text = "Emp. Name"
        '
        'txtEmpcode
        '
        Me.txtEmpcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpcode.Location = New System.Drawing.Point(491, 244)
        Me.txtEmpcode.Name = "txtEmpcode"
        Me.txtEmpcode.Size = New System.Drawing.Size(126, 20)
        Me.txtEmpcode.TabIndex = 6
        Me.txtEmpcode.Visible = False
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 367)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 61
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(580, 61)
        Me.gbSortBy.TabIndex = 1
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(94, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(446, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(546, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(80, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 230
        Me.cboReportType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(141, 32)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(126, 21)
        Me.cboReportType.TabIndex = 0
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(127, 13)
        Me.lblReportType.TabIndex = 239
        Me.lblReportType.Text = "Report Type"
        '
        'frmOrbitReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 533)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOrbitReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.EZeeFooter1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCompanyAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyAccountNo As System.Windows.Forms.Label
    Friend WithEvents cboCompanyBranchName As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyBranchName As System.Windows.Forms.Label
    Friend WithEvents cboCompanyBankName As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyBankName As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblAmountTo As System.Windows.Forms.Label
    Friend WithEvents txtAmountTo As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents cboBranchName As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranchName As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents cboBankName As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankName As System.Windows.Forms.Label
    Friend WithEvents lblCountryName As System.Windows.Forms.Label
    Friend WithEvents lblEmpCode As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents txtEmpcode As System.Windows.Forms.TextBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents cboCompanyAccountType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyAccountType As System.Windows.Forms.Label
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents txtBatchName As System.Windows.Forms.TextBox
    Friend WithEvents chkIncludeReportColumnHeader As System.Windows.Forms.CheckBox
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportType As System.Windows.Forms.Label
End Class
