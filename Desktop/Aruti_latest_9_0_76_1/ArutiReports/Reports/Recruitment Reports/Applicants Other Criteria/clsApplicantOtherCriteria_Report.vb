'************************************************************************************************************************************
'Class Name : clsApplicantOtherCriteria_Report.vb
'Purpose    :
'Date       : 09/04/2012
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsApplicantOtherCriteria_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsApplicantOtherCriteria_Report"
    Private mstrReportId As String = CStr(enArutiReport.Applicant_Other_Criteria_Report)  '84
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintApplicantId As Integer = 0
    Private mstrApplicantName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ApplicantId() As Integer
        Set(ByVal value As Integer)
            mintApplicantId = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantName() As String
        Set(ByVal value As String)
            mstrApplicantName = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintApplicantId = 0
            mstrApplicantName = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintApplicantId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrApplicantName & " "
                Me._FilterQuery &= "AND rcapplicant_master.applicantunkid = '" & mintApplicantId & "' "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            iColumn_DetailReport.Add(New IColumn("award_end_date", Language.getMessage(mstrModuleName, 5, "End Date")))
            'Pinkal (25-APR-2012) -- End

            iColumn_DetailReport.Add(New IColumn("applicant_code", Language.getMessage(mstrModuleName, 3, "Applicant Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 4, "Applicant Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End
        Try
            objDataOperation = New clsDataOperation

            objUser._Userunkid = intUserUnkid


            'Pinkal (20-Oct-2020) -- Start
            'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.

            'StrQ = "SELECT " & _
            '           " applicant_code AS A_CODE " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS A_NAME " & _
            '           ",ISNULL(OQ.other_qualificationgrp,'') AS OTHER_QUALIF_GROUP " & _
            '           ",ISNULL(OQ.other_qualification,'') AS OTHER_QUALIF " & _
            '           ",ISNULL(OQ.other_institute,'') AS OTHER_INSTITUTE " & _
            '           ",ISNULL(QS.other_skillcategory,'') AS OTHER_SKL_CATEGORY " & _
            '           ",ISNULL(QS.other_skill,'') AS OTHER_SKL " & _
            '           ",ISNULL(QId,0) AS QId " & _
            '           ",ISNULL(SId,0) AS SId " & _
            '         ",award_start_date " & _
            '         ",award_end_date " & _
            '       "FROM rcapplicant_master " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '           "SELECT " & _
            '               " applicantunkid " & _
            '               ",other_qualificationgrp " & _
            '               ",other_qualification " & _
            '               ",other_institute " & _
            '             ", ISNULL(CONVERT(CHAR(8),award_start_date,112),'') award_start_date " & _
            '             ", ISNULL(CONVERT(CHAR(8),award_end_date,112),'') award_end_date " & _
            '               ",1 AS QId " & _
            '           "FROM rcapplicantqualification_tran " & _
            '           "WHERE ISNULL(other_qualificationgrp,'') <> '' AND rcapplicantqualification_tran.isvoid = 0 " & _
            '       ")AS OQ ON rcapplicant_master.applicantunkid = OQ.applicantunkid " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '           "SELECT " & _
            '               " applicantunkid " & _
            '               ",other_skillcategory " & _
            '               ",other_skill " & _
            '               ",2 AS SId " & _
            '           "FROM rcapplicantskill_tran " & _
            '           "WHERE ISNULL(other_skillcategory,'') <> '' AND rcapplicantskill_tran.isvoid = 0 " & _
            '       ")AS QS ON rcapplicant_master.applicantunkid = QS.applicantunkid " & _
            '       "WHERE (OQ.QId > 0 OR QS.SId > 0) "


            StrQ = "SELECT " & _
                       " applicant_code AS A_CODE " & _
                       ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS A_NAME " & _
                       ",ISNULL(OQ.other_qualificationgrp,'') AS OTHER_QUALIF_GROUP " & _
                       ",ISNULL(OQ.other_qualification,'') AS OTHER_QUALIF " & _
                       ",ISNULL(OQ.other_institute,'') AS OTHER_INSTITUTE " & _
                       ",ISNULL(QS.other_skillcategory,'') AS OTHER_SKL_CATEGORY " & _
                       ",ISNULL(QS.other_skill,'') AS OTHER_SKL " & _
                       ",ISNULL(QId,0) AS QId " & _
                       ",ISNULL(SId,0) AS SId " & _
                     ",award_start_date " & _
                     ",award_end_date " & _
                      "  FROM rcapplicant_master " & _
                      "  LEFT JOIN " & _
                      " ( " & _
                      "   SELECT " & _
                           " applicantunkid " & _
                      "     ,other_qualificationgrp " & _
                      "     ,other_qualification " & _
                      "     ,other_institute " & _
                      "     , ISNULL(CONVERT(CHAR(8),award_start_date,112),'') award_start_date " & _
                      "     , ISNULL(CONVERT(CHAR(8),award_end_date,112),'') award_end_date " & _
                      "     ,1 AS QId " & _
                      "   FROM rcapplicantqualification_tran " & _
                      "   WHERE ISNULL(other_qualificationgrp,'') <> '' AND rcapplicantqualification_tran.isvoid = 0 " & _
                      "   UNION " & _
                      "   SELECT " & _
                      "       applicantunkid " & _
                      "      ,hremp_qualification_tran.other_qualificationgrp " & _
                      "      ,hremp_qualification_tran.other_qualification " & _
                      "      ,hremp_qualification_tran.other_institute " & _
                      "     ,ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112), '') award_start_date " & _
                      "     ,ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112), '') award_end_date " & _
                      "     ,1 AS QId " & _
                      "  FROM hremp_qualification_tran " & _
                      "  JOIN rcapplicant_master ON rcapplicant_master.employeeunkid= hremp_qualification_tran.employeeunkid " & _
                      "  WHERE ISNULL(hremp_qualification_tran.other_qualificationgrp, '') <> '' AND rcapplicant_master.employeeunkid > 0  " & _
                      "  AND hremp_qualification_tran.isvoid = 0 " & _
                      " )  AS OQ ON rcapplicant_master.applicantunkid = OQ.applicantunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "     SELECT " & _
                           " applicantunkid " & _
                      "         ,other_skillcategory " & _
                      "         ,other_skill " & _
                      "         ,2 AS SId " & _
                      "     FROM rcapplicantskill_tran " & _
                      "     WHERE ISNULL(other_skillcategory,'') <> '' AND rcapplicantskill_tran.isvoid = 0 " & _
                      "     UNION " & _
                      "     SELECT " & _
                      "          applicantunkid " & _
                      "         ,hremp_app_skills_tran.other_skillcategory " & _
                      "         ,hremp_app_skills_tran.other_skill " & _
                      "         ,2 AS SId " & _
                      "     FROM hremp_app_skills_tran " & _
                      "     JOIN rcapplicant_master ON rcapplicant_master.employeeunkid= hremp_app_skills_tran.emp_app_unkid " & _
                      "     WHERE ISNULL(hremp_app_skills_tran.other_skillcategory, '') <> '' AND rcapplicant_master.employeeunkid > 0 " & _
                      "     AND hremp_app_skills_tran.isvoid = 0 " & _
                      " ) AS QS ON rcapplicant_master.applicantunkid = QS.applicantunkid " & _
                      " WHERE (OQ.QId > 0 OR QS.SId > 0) "


            'Pinkal (20-Oct-2020) -- End

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            '-> AND rcapplicantskill_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END


            'Pinkal (25-APR-2012) -- End


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("A_CODE")
                rpt_Rows.Item("Column2") = dtRow.Item("A_NAME")
                rpt_Rows.Item("Column3") = dtRow.Item("OTHER_QUALIF_GROUP")
                rpt_Rows.Item("Column4") = dtRow.Item("OTHER_QUALIF")
                rpt_Rows.Item("Column5") = dtRow.Item("OTHER_INSTITUTE")
                rpt_Rows.Item("Column6") = dtRow.Item("OTHER_SKL_CATEGORY")
                rpt_Rows.Item("Column7") = dtRow.Item("OTHER_SKL")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptApp_Other_Criteria_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username.ToString)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtOtherQ_Grp", Language.getMessage(mstrModuleName, 10, "Other Qualification Group"))
            Call ReportFunction.TextChange(objRpt, "txtOther_Qualif", Language.getMessage(mstrModuleName, 11, "Other Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtOther_Institute", Language.getMessage(mstrModuleName, 12, "Other Institute"))
            Call ReportFunction.TextChange(objRpt, "txtOther_SKL_Category", Language.getMessage(mstrModuleName, 13, "Other Skill Category"))
            Call ReportFunction.TextChange(objRpt, "txtOther_SKL", Language.getMessage(mstrModuleName, 14, "Other Skill"))
            Call ReportFunction.TextChange(objRpt, "txtApplicantCode", Language.getMessage(mstrModuleName, 15, "Applicant Code :"))
            Call ReportFunction.TextChange(objRpt, "txtApplicant", Language.getMessage(mstrModuleName, 16, "Applicant Name :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser = Nothing
            'Shani(24-Aug-2015) -- End
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Order By :")
            Language.setMessage(mstrModuleName, 3, "Applicant Code")
            Language.setMessage(mstrModuleName, 4, "Applicant Name")
            Language.setMessage(mstrModuleName, 5, "End Date")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By :")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, "Other Qualification Group")
            Language.setMessage(mstrModuleName, 11, "Other Qualification")
            Language.setMessage(mstrModuleName, 12, "Other Institute")
            Language.setMessage(mstrModuleName, 13, "Other Skill Category")
            Language.setMessage(mstrModuleName, 14, "Other Skill")
            Language.setMessage(mstrModuleName, 15, "Applicant Code :")
            Language.setMessage(mstrModuleName, 16, "Applicant Name :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
