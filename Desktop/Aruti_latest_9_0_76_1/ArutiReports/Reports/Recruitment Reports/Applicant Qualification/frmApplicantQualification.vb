#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApplicantQualification

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApplicantQualification"
    Private objApplicantQualification As clsApplicantQualification

#End Region

#Region " Constructor "

    Public Sub New()
        objApplicantQualification = New clsApplicantQualification(User._Object._Languageunkid,Company._Object._Companyunkid)
        objApplicantQualification.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objVacancy As New clsVacancy
        Dim objApplicantList As New clsApplicant_master
        Try
            dsCombo = objApplicantList.GetApplicantList("list", True, , True)
            With cboApplicant
                .ValueMember = "applicantunkid"
                .DisplayMember = "applicantname"
                .DataSource = dsCombo.Tables("list")
                .SelectedValue = 0
            End With

            dsCombo = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-189  : Pick data from employee bio-data for internal applicant in Applicant Qualification Report.
            'dsCombo = objVacancy.getVacancyType
            dsCombo = objVacancy.getVacancyType(False)
            'Sohail (20 Oct 2020) -- End
            With cboApplicantType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Applicant Detailed Listing"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "Applicant Summary Listing"))
            cboReportType.SelectedIndex = 1

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objVacancy = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboApplicant.SelectedIndex = 0
            cboVacancyType.SelectedIndex = 0
            cboVacancy.SelectedIndex = 0
            cboApplicantType.SelectedIndex = 0
            txtReferenceNo.Text = ""
            cboReportType.SelectedIndex = 1

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmApplicantQualification_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objApplicantQualification = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantQualification_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantQualification_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            eZeeHeader.Title = objApplicantQualification._ReportName
            eZeeHeader.Message = objApplicantQualification._ReportDesc

            btnExport.Tag = btnExport.Text

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantQualification_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantQualification_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmApplicantQualification_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantQualification_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmApplicantQualification_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If cboReportType.SelectedIndex = 1 Then
                If cboVacancyType.SelectedValue <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Vacancy Type is mandatory information. Please select Vacancy type to continue."), enMsgBoxStyle.Information)
                    cboVacancyType.Focus()
                    Exit Sub
                End If
                If cboVacancy.SelectedValue <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Vacancy is mandatory information. Please select Vacancy to continue."), enMsgBoxStyle.Information)
                    cboVacancy.Focus()
                    Exit Sub
                End If
            End If
            'S.SANDEEP [ 04 MAY 2012 ] -- END


            objApplicantQualification.SetDefaultValue()

            objApplicantQualification._ApplicantID = cboApplicant.SelectedValue
            objApplicantQualification._ApplicantName = cboApplicant.Text

            objApplicantQualification._VacancyTypeID = cboVacancyType.SelectedValue
            objApplicantQualification._VacancyTypeName = cboVacancyType.Text

            objApplicantQualification._VacancyID = cboVacancy.SelectedValue
            objApplicantQualification._VacancyName = cboVacancy.Text

            objApplicantQualification._ApplicantTypeId = cboApplicantType.SelectedValue
            objApplicantQualification._ApplicantTypeName = cboApplicantType.Text

            objApplicantQualification._ReferenceNo = txtReferenceNo.Text.Trim

            objApplicantQualification._ReportTypeName = cboReportType.Text


            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objApplicantQualification._ReportTypeId = cboReportType.SelectedIndex
            'S.SANDEEP [ 04 MAY 2012 ] -- END



            If cboReportType.SelectedIndex = 0 Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objApplicantQualification.Generate_DetailReport()
                objApplicantQualification.Generate_DetailReport(User._Object._Userunkid, _
                                                                Company._Object._Companyunkid, _
                                                                ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport)
                'Shani(24-Aug-2015) -- End

            Else

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objApplicantQualification.Generate_SummaryReport()
                objApplicantQualification.Generate_SummaryReport(User._Object._Userunkid, _
                                                                 Company._Object._Companyunkid, _
                                                                 ConfigParameter._Object._ExportReportPath, _
                                                                 ConfigParameter._Object._OpenAfterExport)
                'Shani(24-Aug-2015) -- End

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboApplicant.DataSource
            frm.ValueMember = cboApplicant.ValueMember
            frm.DisplayMember = cboApplicant.DisplayMember
            frm.CodeMember = "applicant_code"
            If frm.DisplayDialog Then
                cboApplicant.SelectedValue = frm.SelectedValue
                cboApplicant.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub btnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsApplicantQualification.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicantQualification"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "btnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


#End Region

#Region "CombBox Event"

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsCombo As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objVacancy.getComboList(True, "List", -1, CInt(cboVacancyType.SelectedValue))
            dsCombo = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", -1, CInt(cboVacancyType.SelectedValue))
            'Shani(24-Aug-2015) -- End

            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 Then
                btnExport.Text = btnExport.Tag
                txtReferenceNo.Text = ""
                txtReferenceNo.Enabled = True
            Else
                btnExport.Text = Language.getMessage(mstrModuleName, 5, "Report")
                txtReferenceNo.Text = ""
                txtReferenceNo.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


 
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.btnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblApplicantType.Text = Language._Object.getCaption(Me.lblApplicantType.Name, Me.lblApplicantType.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
			Me.LalblVacancy.Text = Language._Object.getCaption(Me.LalblVacancy.Name, Me.LalblVacancy.Text)
			Me.lblOthername.Text = Language._Object.getCaption(Me.lblOthername.Name, Me.lblOthername.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnLanguage.Text = Language._Object.getCaption(Me.btnLanguage.Name, Me.btnLanguage.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Vacancy Type is mandatory information. Please select Vacancy type to continue.")
			Language.setMessage(mstrModuleName, 2, "Vacancy is mandatory information. Please select Vacancy to continue.")
			Language.setMessage(mstrModuleName, 3, "Applicant Detailed Listing")
			Language.setMessage(mstrModuleName, 4, "Applicant Summary Listing")
			Language.setMessage(mstrModuleName, 5, "Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
