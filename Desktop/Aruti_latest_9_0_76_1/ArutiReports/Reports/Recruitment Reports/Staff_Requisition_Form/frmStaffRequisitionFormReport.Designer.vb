﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStaffRequisitionFormReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblRequisitiontype = New System.Windows.Forms.Label
        Me.cboRequisitionType = New System.Windows.Forms.ComboBox
        Me.lblWorkStartDateFrom = New System.Windows.Forms.Label
        Me.lblPositionFrom = New System.Windows.Forms.Label
        Me.lblWorkStartDateTo = New System.Windows.Forms.Label
        Me.lblPositionTo = New System.Windows.Forms.Label
        Me.lblRequisitionBy = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.tlbAllocation = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchAllocation = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkAllocation = New System.Windows.Forms.CheckBox
        Me.dgvAllocation = New System.Windows.Forms.DataGridView
        Me.objdgcolhACheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhAllocation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAllocId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dtpWStartDateFrom = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.cboRequistionBy = New System.Windows.Forms.ComboBox
        Me.nudPositionTo = New System.Windows.Forms.NumericUpDown
        Me.dtpWStartDateTo = New System.Windows.Forms.DateTimePicker
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.nudPositionFrom = New System.Windows.Forms.NumericUpDown
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchFormNo = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkFormNo = New System.Windows.Forms.CheckBox
        Me.dgvFormNo = New System.Windows.Forms.DataGridView
        Me.objdgcolhFCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhFormNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFormId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.tlbAllocation.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvAllocation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPositionTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPositionFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvFormNo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 469)
        Me.NavPanel.Size = New System.Drawing.Size(792, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblRequisitiontype)
        Me.gbFilterCriteria.Controls.Add(Me.cboRequisitionType)
        Me.gbFilterCriteria.Controls.Add(Me.lblWorkStartDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblPositionFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblWorkStartDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblPositionTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblRequisitionBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.tlbAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.dtpWStartDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboRequistionBy)
        Me.gbFilterCriteria.Controls.Add(Me.nudPositionTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpWStartDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.nudPositionFrom)
        Me.gbFilterCriteria.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(726, 353)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRequisitiontype
        '
        Me.lblRequisitiontype.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequisitiontype.Location = New System.Drawing.Point(8, 36)
        Me.lblRequisitiontype.Name = "lblRequisitiontype"
        Me.lblRequisitiontype.Size = New System.Drawing.Size(94, 16)
        Me.lblRequisitiontype.TabIndex = 317
        Me.lblRequisitiontype.Text = "Requisition Type"
        '
        'cboRequisitionType
        '
        Me.cboRequisitionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRequisitionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRequisitionType.FormattingEnabled = True
        Me.cboRequisitionType.Location = New System.Drawing.Point(111, 34)
        Me.cboRequisitionType.Name = "cboRequisitionType"
        Me.cboRequisitionType.Size = New System.Drawing.Size(242, 21)
        Me.cboRequisitionType.TabIndex = 305
        '
        'lblWorkStartDateFrom
        '
        Me.lblWorkStartDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkStartDateFrom.Location = New System.Drawing.Point(8, 118)
        Me.lblWorkStartDateFrom.Name = "lblWorkStartDateFrom"
        Me.lblWorkStartDateFrom.Size = New System.Drawing.Size(94, 16)
        Me.lblWorkStartDateFrom.TabIndex = 322
        Me.lblWorkStartDateFrom.Text = "Work Date From"
        '
        'lblPositionFrom
        '
        Me.lblPositionFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPositionFrom.Location = New System.Drawing.Point(224, 118)
        Me.lblPositionFrom.Name = "lblPositionFrom"
        Me.lblPositionFrom.Size = New System.Drawing.Size(72, 16)
        Me.lblPositionFrom.TabIndex = 320
        Me.lblPositionFrom.Text = "Position From"
        '
        'lblWorkStartDateTo
        '
        Me.lblWorkStartDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkStartDateTo.Location = New System.Drawing.Point(8, 145)
        Me.lblWorkStartDateTo.Name = "lblWorkStartDateTo"
        Me.lblWorkStartDateTo.Size = New System.Drawing.Size(94, 16)
        Me.lblWorkStartDateTo.TabIndex = 323
        Me.lblWorkStartDateTo.Text = "To"
        '
        'lblPositionTo
        '
        Me.lblPositionTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPositionTo.Location = New System.Drawing.Point(221, 145)
        Me.lblPositionTo.Name = "lblPositionTo"
        Me.lblPositionTo.Size = New System.Drawing.Size(75, 16)
        Me.lblPositionTo.TabIndex = 321
        Me.lblPositionTo.Text = "To"
        '
        'lblRequisitionBy
        '
        Me.lblRequisitionBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequisitionBy.Location = New System.Drawing.Point(395, 36)
        Me.lblRequisitionBy.Name = "lblRequisitionBy"
        Me.lblRequisitionBy.Size = New System.Drawing.Size(77, 16)
        Me.lblRequisitionBy.TabIndex = 318
        Me.lblRequisitionBy.Text = "Requisition By"
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 63)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(94, 16)
        Me.lblJob.TabIndex = 319
        Me.lblJob.Text = "Job"
        '
        'tlbAllocation
        '
        Me.tlbAllocation.ColumnCount = 1
        Me.tlbAllocation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlbAllocation.Controls.Add(Me.txtSearchAllocation, 0, 0)
        Me.tlbAllocation.Controls.Add(Me.Panel1, 0, 1)
        Me.tlbAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlbAllocation.Location = New System.Drawing.Point(392, 63)
        Me.tlbAllocation.Name = "tlbAllocation"
        Me.tlbAllocation.RowCount = 2
        Me.tlbAllocation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tlbAllocation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlbAllocation.Size = New System.Drawing.Size(323, 279)
        Me.tlbAllocation.TabIndex = 307
        '
        'txtSearchAllocation
        '
        Me.txtSearchAllocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchAllocation.Flags = 0
        Me.txtSearchAllocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchAllocation.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchAllocation.Name = "txtSearchAllocation"
        Me.txtSearchAllocation.Size = New System.Drawing.Size(317, 21)
        Me.txtSearchAllocation.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkAllocation)
        Me.Panel1.Controls.Add(Me.dgvAllocation)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(317, 247)
        Me.Panel1.TabIndex = 107
        '
        'objchkAllocation
        '
        Me.objchkAllocation.AutoSize = True
        Me.objchkAllocation.Location = New System.Drawing.Point(7, 5)
        Me.objchkAllocation.Name = "objchkAllocation"
        Me.objchkAllocation.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllocation.TabIndex = 104
        Me.objchkAllocation.UseVisualStyleBackColor = True
        '
        'dgvAllocation
        '
        Me.dgvAllocation.AllowUserToAddRows = False
        Me.dgvAllocation.AllowUserToDeleteRows = False
        Me.dgvAllocation.AllowUserToResizeColumns = False
        Me.dgvAllocation.AllowUserToResizeRows = False
        Me.dgvAllocation.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAllocation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAllocation.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAllocation.ColumnHeadersHeight = 21
        Me.dgvAllocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAllocation.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhACheck, Me.objdgcolhAllocation, Me.objdgcolhAllocId})
        Me.dgvAllocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAllocation.Location = New System.Drawing.Point(0, 0)
        Me.dgvAllocation.MultiSelect = False
        Me.dgvAllocation.Name = "dgvAllocation"
        Me.dgvAllocation.RowHeadersVisible = False
        Me.dgvAllocation.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAllocation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAllocation.Size = New System.Drawing.Size(317, 247)
        Me.dgvAllocation.TabIndex = 105
        '
        'objdgcolhACheck
        '
        Me.objdgcolhACheck.HeaderText = ""
        Me.objdgcolhACheck.Name = "objdgcolhACheck"
        Me.objdgcolhACheck.Width = 25
        '
        'objdgcolhAllocation
        '
        Me.objdgcolhAllocation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.objdgcolhAllocation.HeaderText = ""
        Me.objdgcolhAllocation.Name = "objdgcolhAllocation"
        Me.objdgcolhAllocation.ReadOnly = True
        Me.objdgcolhAllocation.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAllocation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhAllocId
        '
        Me.objdgcolhAllocId.HeaderText = "objdgcolhAllocId"
        Me.objdgcolhAllocId.Name = "objdgcolhAllocId"
        Me.objdgcolhAllocId.Visible = False
        '
        'dtpWStartDateFrom
        '
        Me.dtpWStartDateFrom.Checked = False
        Me.dtpWStartDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpWStartDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpWStartDateFrom.Location = New System.Drawing.Point(111, 116)
        Me.dtpWStartDateFrom.Name = "dtpWStartDateFrom"
        Me.dtpWStartDateFrom.ShowCheckBox = True
        Me.dtpWStartDateFrom.Size = New System.Drawing.Size(104, 21)
        Me.dtpWStartDateFrom.TabIndex = 311
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(359, 61)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 314
        '
        'cboRequistionBy
        '
        Me.cboRequistionBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRequistionBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRequistionBy.FormattingEnabled = True
        Me.cboRequistionBy.Location = New System.Drawing.Point(478, 34)
        Me.cboRequistionBy.Name = "cboRequistionBy"
        Me.cboRequistionBy.Size = New System.Drawing.Size(234, 21)
        Me.cboRequistionBy.TabIndex = 306
        '
        'nudPositionTo
        '
        Me.nudPositionTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPositionTo.Location = New System.Drawing.Point(302, 143)
        Me.nudPositionTo.Name = "nudPositionTo"
        Me.nudPositionTo.Size = New System.Drawing.Size(51, 21)
        Me.nudPositionTo.TabIndex = 310
        Me.nudPositionTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtpWStartDateTo
        '
        Me.dtpWStartDateTo.Checked = False
        Me.dtpWStartDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpWStartDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpWStartDateTo.Location = New System.Drawing.Point(111, 143)
        Me.dtpWStartDateTo.Name = "dtpWStartDateTo"
        Me.dtpWStartDateTo.ShowCheckBox = True
        Me.dtpWStartDateTo.Size = New System.Drawing.Size(104, 21)
        Me.dtpWStartDateTo.TabIndex = 312
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(111, 61)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(242, 21)
        Me.cboJob.TabIndex = 308
        '
        'nudPositionFrom
        '
        Me.nudPositionFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPositionFrom.Location = New System.Drawing.Point(302, 116)
        Me.nudPositionFrom.Name = "nudPositionFrom"
        Me.nudPositionFrom.Size = New System.Drawing.Size(51, 21)
        Me.nudPositionFrom.TabIndex = 309
        Me.nudPositionFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchFormNo, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(108, 170)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(248, 172)
        Me.tblpAssessorEmployee.TabIndex = 304
        '
        'txtSearchFormNo
        '
        Me.txtSearchFormNo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchFormNo.Flags = 0
        Me.txtSearchFormNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchFormNo.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchFormNo.Name = "txtSearchFormNo"
        Me.txtSearchFormNo.Size = New System.Drawing.Size(242, 21)
        Me.txtSearchFormNo.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkFormNo)
        Me.objpnlEmp.Controls.Add(Me.dgvFormNo)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(242, 140)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkFormNo
        '
        Me.objchkFormNo.AutoSize = True
        Me.objchkFormNo.Location = New System.Drawing.Point(7, 5)
        Me.objchkFormNo.Name = "objchkFormNo"
        Me.objchkFormNo.Size = New System.Drawing.Size(15, 14)
        Me.objchkFormNo.TabIndex = 104
        Me.objchkFormNo.UseVisualStyleBackColor = True
        '
        'dgvFormNo
        '
        Me.dgvFormNo.AllowUserToAddRows = False
        Me.dgvFormNo.AllowUserToDeleteRows = False
        Me.dgvFormNo.AllowUserToResizeColumns = False
        Me.dgvFormNo.AllowUserToResizeRows = False
        Me.dgvFormNo.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvFormNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvFormNo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvFormNo.ColumnHeadersHeight = 21
        Me.dgvFormNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvFormNo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhFCheck, Me.dgcolhFormNo, Me.objdgcolhFormId})
        Me.dgvFormNo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFormNo.Location = New System.Drawing.Point(0, 0)
        Me.dgvFormNo.MultiSelect = False
        Me.dgvFormNo.Name = "dgvFormNo"
        Me.dgvFormNo.RowHeadersVisible = False
        Me.dgvFormNo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvFormNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFormNo.Size = New System.Drawing.Size(242, 140)
        Me.dgvFormNo.TabIndex = 105
        '
        'objdgcolhFCheck
        '
        Me.objdgcolhFCheck.HeaderText = ""
        Me.objdgcolhFCheck.Name = "objdgcolhFCheck"
        Me.objdgcolhFCheck.Width = 25
        '
        'dgcolhFormNo
        '
        Me.dgcolhFormNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhFormNo.HeaderText = "Staff Requisition Form No"
        Me.dgcolhFormNo.Name = "dgcolhFormNo"
        Me.dgcolhFormNo.ReadOnly = True
        Me.dgcolhFormNo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhFormNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhFormId
        '
        Me.objdgcolhFormId.HeaderText = "objdgcolhFormId"
        Me.objdgcolhFormId.Name = "objdgcolhFormId"
        Me.objdgcolhFormId.Visible = False
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 90)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(94, 16)
        Me.lblStatus.TabIndex = 326
        Me.lblStatus.Text = "Status"
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(111, 88)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(242, 21)
        Me.cboStatus.TabIndex = 325
        '
        'frmStaffRequisitionFormReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 524)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmStaffRequisitionFormReport"
        Me.Text = "frmStaffRequisitionFormReport"
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.tlbAllocation.ResumeLayout(False)
        Me.tlbAllocation.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvAllocation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPositionTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPositionFrom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvFormNo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchFormNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkFormNo As System.Windows.Forms.CheckBox
    Friend WithEvents dgvFormNo As System.Windows.Forms.DataGridView
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents tlbAllocation As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchAllocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkAllocation As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAllocation As System.Windows.Forms.DataGridView
    Friend WithEvents cboRequistionBy As System.Windows.Forms.ComboBox
    Friend WithEvents cboRequisitionType As System.Windows.Forms.ComboBox
    Friend WithEvents dtpWStartDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpWStartDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents nudPositionTo As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudPositionFrom As System.Windows.Forms.NumericUpDown
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents lblWorkStartDateTo As System.Windows.Forms.Label
    Friend WithEvents lblWorkStartDateFrom As System.Windows.Forms.Label
    Friend WithEvents lblPositionTo As System.Windows.Forms.Label
    Friend WithEvents lblPositionFrom As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblRequisitionBy As System.Windows.Forms.Label
    Friend WithEvents lblRequisitiontype As System.Windows.Forms.Label
    Friend WithEvents objdgcolhACheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhAllocation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAllocId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhFormNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFormId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
End Class
