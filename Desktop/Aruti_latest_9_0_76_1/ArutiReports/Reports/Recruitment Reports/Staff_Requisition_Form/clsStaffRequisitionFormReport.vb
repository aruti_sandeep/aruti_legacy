'************************************************************************************************************************************
'Class Name : clsStaffRequisitionFormReport.vb
'Purpose    : 
'Date       : 11-Oct-2017
'Written By : Sandeep J Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsStaffRequisitionFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsStaffRequisitionFormReport"
    Private mstrReportId As String = enArutiReport.StaffRequisitionFormReport   '196
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mstrRequisitionFormIds As String = String.Empty
    Private mstrRequisitionFormNos As String = String.Empty
    Private mintRequisitionTypeId As Integer = 0
    Private mstrRequisitionTypeName As String = String.Empty
    Private mintRequisitionById As Integer = 0
    Private mstrRequisitionByName As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    Private mstrAllocationNames As String = String.Empty
    Private mintJobId As Integer = 0
    Private mstrJobName As String = String.Empty
    Private mintPositionFrom As Integer = 0
    Private mintPositionTo As Integer = 0
    Private mdtWorkFromDate As Date = Nothing
    Private mdtWorkToDate As Date = Nothing
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Hemant (01 Nov 2021) -- Start
    'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    'Hemant (01 Nov 2021) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _RequisitionFormIds() As String
        Set(ByVal value As String)
            mstrRequisitionFormIds = value
        End Set
    End Property

    Public WriteOnly Property _RequisitionFormNos() As String
        Set(ByVal value As String)
            mstrRequisitionFormNos = value
        End Set
    End Property

    Public WriteOnly Property _RequisitionTypeId() As Integer
        Set(ByVal value As Integer)
            mintRequisitionTypeId = value
        End Set
    End Property

    Public WriteOnly Property _RequisitionTypeName() As String
        Set(ByVal value As String)
            mstrRequisitionTypeName = value
        End Set
    End Property

    Public WriteOnly Property _RequisitionById() As Integer
        Set(ByVal value As Integer)
            mintRequisitionById = value
        End Set
    End Property

    Public WriteOnly Property _RequisitionByName() As String
        Set(ByVal value As String)
            mstrRequisitionByName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _AllocationNames() As String
        Set(ByVal value As String)
            mstrAllocationNames = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _JobName() As String
        Set(ByVal value As String)
            mstrJobName = value
        End Set
    End Property

    Public WriteOnly Property _PositionFrom() As Integer
        Set(ByVal value As Integer)
            mintPositionFrom = value
        End Set
    End Property

    Public WriteOnly Property _PositionTo() As Integer
        Set(ByVal value As Integer)
            mintPositionTo = value
        End Set
    End Property

    Public WriteOnly Property _WorkFromDate() As Date
        Set(ByVal value As Date)
            mdtWorkFromDate = value
        End Set
    End Property

    Public WriteOnly Property _WorkToDate() As Date
        Set(ByVal value As Date)
            mdtWorkToDate = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Sohail (14 Sep 2021) -- Start
    'NMB Issue :  : Staff requisition report showing wrong signature (printed by).
    Private mstrUser_Name As String = String.Empty
    Public WriteOnly Property _User_Name() As String
        Set(ByVal value As String)
            mstrUser_Name = value
        End Set
    End Property
    'Sohail (14 Sep 2021) -- End

    'Hemant (01 Nov 2021) -- Start
    'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property
    'Hemant (01 Nov 2021) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrRequisitionFormIds = String.Empty
            mstrRequisitionFormNos = String.Empty
            mintRequisitionTypeId = 0
            mstrRequisitionTypeName = String.Empty
            mintRequisitionById = 0
            mstrRequisitionByName = String.Empty
            mstrAllocationIds = String.Empty
            mstrAllocationNames = String.Empty
            mintJobId = 0
            mstrJobName = String.Empty
            mintPositionFrom = 0
            mintPositionTo = 0
            mdtWorkFromDate = Nothing
            mdtWorkToDate = Nothing
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            mintStatusId = 0
            mstrStatusName = String.Empty
            'Hemant (01 Nov 2021) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@Branch", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DepartmentGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@Department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SectionGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@Section", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UnitGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@Unit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@Team", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objDataOperation.AddParameter("@JobGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objDataOperation.AddParameter("@Job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objDataOperation.AddParameter("@ClassGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objDataOperation.AddParameter("@Class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))
            objDataOperation.AddParameter("@CostCenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 586, "Cost Center"))
            objDataOperation.AddParameter("@Replacement", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 517, "Replacement"))
            objDataOperation.AddParameter("@Additional", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Additional"))
            objDataOperation.AddParameter("@ReplacementAdditional", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 660, "Replacement and Additional"))

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 263, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 262, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Rejected"))
            objDataOperation.AddParameter("@Cancelled", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "Cancelled"))
            objDataOperation.AddParameter("@Published", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Published"))

            If mintRequisitionTypeId > 0 Then
                objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequisitionTypeId)
                'Sohail (18 Aug 2021) -- Start
                'NMB Enhancement : OLD-431 : Show staff requisition form report on MSS. Staff Requisition Form No. list should be specific to the job selected. .
                'Me._FilterQuery &= " AND  rcstaffrequisition_tran.staffrequisitiontranunkid = @staffrequisitiontranunkid "
                Me._FilterQuery &= " AND  rcstaffrequisition_tran.staffrequisitiontypeid = @staffrequisitiontranunkid "
                'Sohail (18 Aug 2021) -- End
                'Sohail (12 Oct 2018) -- Start
                'Language number reordered in 75.1.
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Requisition Type :") & " " & mstrRequisitionTypeName & " "
                'Sohail (12 Oct 2018) -- End
            End If

            If mintRequisitionById > 0 Then
                objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequisitionById)
                Me._FilterQuery &= " AND rcstaffrequisition_tran.staffrequisitionbyid = @staffrequisitionbyid "
                'Sohail (12 Oct 2018) -- Start
                'Language number reordered in 75.1.
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Requisition By :") & " " & mstrRequisitionByName & " "
                'Sohail (12 Oct 2018) -- End
            End If

            If mintJobId > 0 Then
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterQuery &= " AND rcstaffrequisition_tran.jobunkid = @jobunkid "
                'Sohail (12 Oct 2018) -- Start
                'Language number reordered in 75.1.
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Job :") & " " & mstrJobName & " "
                'Sohail (12 Oct 2018) -- End
            End If

            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@form_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND rcstaffrequisition_tran.form_statusunkid = @form_statusunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Status :") & " " & mstrStatusName & " "
            End If
            'Hemant (01 Nov 2021) -- End

            If mintPositionFrom > 0 AndAlso mintPositionTo > 0 Then
                objDataOperation.AddParameter("@fnoofposition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPositionFrom)
                objDataOperation.AddParameter("@tnoofposition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPositionTo)
                Me._FilterQuery &= " AND rcstaffrequisition_tran.noofposition between @fnoofposition AND @tnoofposition "
                'Sohail (12 Oct 2018) -- Start
                'Language number reordered in 75.1.
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "No Of Position From :") & " " & mintPositionFrom.ToString & " " & _
                                   Language.getMessage(mstrModuleName, 5, "To : ") & " " & mintPositionTo.ToString & " "
                'Sohail (12 Oct 2018) -- End
            End If

            If mdtWorkFromDate <> Nothing AndAlso mdtWorkToDate <> Nothing Then
                objDataOperation.AddParameter("@fdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtWorkFromDate).ToString())
                objDataOperation.AddParameter("@tdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtWorkToDate).ToString())
                Me._FilterQuery &= " AND convert(nvarchar(8),workstartdate,112) between @fdate AND @tdate "
                'Sohail (12 Oct 2018) -- Start
                'Language number reordered in 75.1.
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Work Start Date From :") & " " & mdtWorkFromDate.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 7, "To : ") & " " & mdtWorkToDate.ToShortDateString & " "
                'Sohail (12 Oct 2018) -- End
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_Appr As ArutiReport.Designer.dsArutiReport
        Dim rpt_Empl As ArutiReport.Designer.dsArutiReport
        Try

            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Call FilterTitleAndFilterQuery()

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            Dim dsJobAdvert As DataSet = (New clsMasterData).getJobAdvert("List", True)
            'Sohail (29 Sep 2021) -- End

            StrQ = "select " & _
                   "     formno " & _
                   "    ,case when staffrequisitiontypeid = 1 then @Replacement " & _
                   "          when staffrequisitiontypeid = 2 then @Additional " & _
                   "          when staffrequisitiontypeid = 3 then @ReplacementAdditional " & _
                   "     end as requisitiontyp " & _
                   "    ,case when staffrequisitionbyid = 1 then @Branch " & _
                   "          when staffrequisitionbyid = 2 then @DepartmentGroup " & _
                   "          when staffrequisitionbyid = 3 then @Department " & _
                   "          when staffrequisitionbyid = 4 then @SectionGroup " & _
                   "          when staffrequisitionbyid = 5 then @Section " & _
                   "          when staffrequisitionbyid = 6 then @UnitGroup " & _
                   "          when staffrequisitionbyid = 7 then @Unit " & _
                   "          when staffrequisitionbyid = 8 then @Team " & _
                   "          when staffrequisitionbyid = 9 then @JobGroup " & _
                   "          when staffrequisitionbyid = 10 then @Job " & _
                   "          when staffrequisitionbyid = 13 then @ClassGroup " & _
                   "          when staffrequisitionbyid = 14 then @Class " & _
                   "          when staffrequisitionbyid = 15 then @CostCenter " & _
                   "     end as requisitionby " & _
                   "    ,additionalstaffreason as justification " & _
                   "    ,case when staffrequisitionbyid = 1 then isnull(ssm.name,'') " & _
                   "          when staffrequisitionbyid = 2 then isnull(sdg.name,'') " & _
                   "          when staffrequisitionbyid = 3 then isnull(sdm.name,'') " & _
                   "          when staffrequisitionbyid = 4 then isnull(ssg.name,'') " & _
                   "          when staffrequisitionbyid = 5 then isnull(ssc.name,'') " & _
                   "          when staffrequisitionbyid = 6 then isnull(sug.name,'') " & _
                   "          when staffrequisitionbyid = 7 then isnull(sut.name,'') " & _
                   "          when staffrequisitionbyid = 8 then isnull(stm.name,'') " & _
                   "          when staffrequisitionbyid = 9 then isnull(sjg.name,'') " & _
                   "          when staffrequisitionbyid = 10 then isnull(sjb.job_name,'') " & _
                   "          when staffrequisitionbyid = 13 then isnull(scg.name,'') " & _
                   "          when staffrequisitionbyid = 14 then isnull(scl.name,'') " & _
                   "          when staffrequisitionbyid = 15 then isnull(scc.costcentername,'') " & _
                   "     end as allocation " & _
                   "    ,noofposition " & _
                   "    ,convert(nvarchar(8),workstartdate,112) as workstartdate " & _
                   "    ,jobdescrription " & _
                   "    ,staffrequisitiontypeid " & _
                   "    ,staffrequisitionbyid " & _
                   "    ,allocationunkid " & _
                   "    ,rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                   "    ,rcj.job_name as rcjob " & _
                   ", ISNULL(rcstaffrequisition_tran.gradeunkid, 0) AS gradeunkid " & _
                   ", ISNULL(g.name, '') AS Grade " & _
                   ", ISNULL(rcstaffrequisition_tran.gradelevelunkid, 0) AS gradelevelunkid " & _
                   ", ISNULL(gl.name, '') AS GradeLevel " & _
                   ", ISNULL(rcstaffrequisition_tran.approved_headcount, 0) AS approved_headcount " & _
                   ", ISNULL(rcstaffrequisition_tran.actual_headcount, 0) AS actual_headcount " & _
                   ", ISNULL(rcstaffrequisition_tran.employmenttypeunkid, 0) AS employmenttypeunkid " & _
                   ", ISNULL(etype.name, '') AS employmenttype " & _
                   ", ISNULL(rcstaffrequisition_tran.contract_duration_months, 0) AS contract_duration_months " & _
                   ", ISNULL(rcstaffrequisition_tran.requisition_date, GETDATE()) AS requisition_date " & _
                   ", ISNULL(repojob.job_name, '') AS ReportToJob " & _
                   ", cg.name AS ClassGrp " & _
                   ", cl.name AS Class " & _
                   ", CASE ISNULL(rcstaffrequisition_tran.jobadvertid, 0) "

            'Pinkal (14-Apr-2023) -- (A1X-816) NMB As a user, I want to have allocations filled out on staff requisition on the staff requisition form report. [, cg.name AS ClassGrp , cl.name AS Class ]

            'Sohail (29 Sep 2021) - [ReportToJob, jobadvertid]

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            For Each dsRow As DataRow In dsJobAdvert.Tables(0).Rows
                If CInt(dsRow.Item("Id")) <= 0 Then
                    StrQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '' "
                Else
                    StrQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name") & "' "
                End If
            Next
            StrQ &= " END AS jobadvertname "
            'Sohail (29 Sep 2021) -- End

            StrQ &= "FROM rcstaffrequisition_tran " & _
                   "    left join hrstation_master as ssm on ssm.stationunkid = rcstaffrequisition_tran.allocationunkid and ssm.isactive = 1 " & _
                   "    left join hrdepartment_group_master as sdg on sdg.deptgroupunkid = rcstaffrequisition_tran.allocationunkid and sdg.isactive = 1 " & _
                   "    left join hrdepartment_master as sdm on sdm.departmentunkid = rcstaffrequisition_tran.allocationunkid and sdm.isactive = 1 " & _
                   "    left join hrsectiongroup_master as ssg on ssg.sectiongroupunkid = rcstaffrequisition_tran.allocationunkid and ssg.isactive = 1 " & _
                   "    left join hrsection_master as ssc on ssc.sectionunkid = rcstaffrequisition_tran.allocationunkid and ssc.isactive = 1 " & _
                   "    left join hrunitgroup_master as sug on sug.unitgroupunkid = rcstaffrequisition_tran.allocationunkid and sug.isactive = 1 " & _
                   "    left join hrunit_master as sut on sut.unitunkid = rcstaffrequisition_tran.allocationunkid and sut.isactive = 1 " & _
                   "    left join hrteam_master as stm on stm.teamunkid = rcstaffrequisition_tran.allocationunkid and stm.isactive = 1 " & _
                   "    left join hrjobgroup_master as sjg on sjg.jobgroupunkid = rcstaffrequisition_tran.allocationunkid and sjg.isactive = 1 " & _
                   "    left join hrjob_master as sjb on sjb.jobunkid = rcstaffrequisition_tran.allocationunkid and sjb.isactive = 1 " & _
                   "    left join hrclassgroup_master as scg on scg.classgroupunkid = rcstaffrequisition_tran.allocationunkid and scg.isactive = 1 " & _
                   "    left join hrclasses_master as scl on scl.classesunkid = rcstaffrequisition_tran.allocationunkid and scl.isactive = 1 " & _
                   "    left join prcostcenter_master as scc on scc.costcenterunkid = rcstaffrequisition_tran.allocationunkid and scc.isactive = 1 " & _
                   "    left join hrjob_master as rcj on rcj.jobunkid = rcstaffrequisition_tran.jobunkid " & _
               "    LEFT JOIN hrjob_master AS repojob ON repojob.jobunkid = rcstaffrequisition_tran.job_report_tounkid " & _
                   "LEFT JOIN hrgrade_master AS g ON g.gradeunkid = rcstaffrequisition_tran.gradeunkid " & _
                    "LEFT JOIN hrgradelevel_master AS gl ON gl.gradelevelunkid = rcstaffrequisition_tran.gradelevelunkid " & _
                    "LEFT JOIN cfcommon_master AS etype ON etype.masterunkid = rcstaffrequisition_tran.employmenttypeunkid AND etype.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & "' " & _
                       "   LEFT JOIN hrclassgroup_master AS cg ON cg.classgroupunkid = rcstaffrequisition_tran.classgroupunkid " & _
                       "   LEFT JOIN hrclasses_master AS cl ON cl.classesunkid = rcstaffrequisition_tran.classunkid " & _
                   "where rcstaffrequisition_tran.isvoid = 0 "

            'Pinkal (14-Apr-2023) -- (A1X-816) NMB As a user, I want to have allocations filled out on staff requisition on the staff requisition form report.[LEFT JOIN hrclassgroup_master AS cg ON cg.classgroupunkid = rcstaffrequisition_tran.classgroupunkid LEFT JOIN hrclasses_master AS cl ON cl.classesunkid = rcstaffrequisition_tran.classunkid]

            'Sohail (29 Sep 2021) - [LEFT JOIN hrjob_master]
            'Sohail (12 Oct 2018) - [gradeunkid, Grade, gradelevelunkid, GradeLevel, approved_headcount, actual_headcount, employmenttypeunkid, employmenttype, contract_duration_months, requisition_date]

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.allocationunkid IN (" & mstrAllocationIds & ") "
            End If

            If mstrRequisitionFormIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid IN (" & mstrRequisitionFormIds & ") "
            End If

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            Dim rtf As New RichTextBox
            'Sohail (12 Oct 2018) -- End

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_row As DataRow = rpt_Data.Tables("ArutiTable").NewRow()

                rpt_row("Column1") = dtRow("formno")
                rpt_row("Column2") = dtRow("requisitiontyp")
                rpt_row("Column3") = dtRow("requisitionby")
                rpt_row("Column4") = dtRow("allocation")
                rpt_row("Column5") = dtRow("noofposition")
                If dtRow("workstartdate").ToString.Length > 0 Then
                    rpt_row("Column6") = eZeeDate.convertDate(dtRow("workstartdate").ToString).ToShortDateString()
                Else
                    rpt_row("Column6") = ""
                End If
                rpt_row("Column7") = dtRow("rcjob")
                rpt_row("Column8") = dtRow("justification")
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                'rpt_row("Column9") = dtRow("jobdescrription")
                If dtRow("jobdescrription").ToString.StartsWith("{\rtf") = True Then
                    rtf.Rtf = dtRow.Item("jobdescrription").ToString
                Else
                    rtf.Text = dtRow.Item("jobdescrription").ToString
                End If
                rpt_row("Column9") = rtf.Text
                'Sohail (12 Oct 2018) -- End
                'Sohail (12 Oct 2018) -- Start
                'Language number reordered in 75.1.
                rpt_row("Column10") = Language.getMessage(mstrModuleName, 8, "Staff Requistion No") & " : " & dtRow("formno")
                'Sohail (12 Oct 2018) -- End

                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                rpt_row("Column11") = dtRow("Grade")
                rpt_row("Column12") = dtRow("GradeLevel")
                rpt_row("Column13") = dtRow("approved_headcount")
                rpt_row("Column14") = dtRow("actual_headcount")
                rpt_row("Column15") = dtRow("employmenttype")
                rpt_row("Column16") = Format(CInt(dtRow("contract_duration_months")), "0")
                'Sohail (12 Oct 2018) -- End

                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
                rpt_row("Column17") = dtRow("ReportToJob")
                rpt_row("Column18") = dtRow("jobadvertname")
                'Sohail (29 Sep 2021) -- End

                'Pinkal (14-Apr-2023) -- (A1X-816) NMB As a user, I want to have allocations filled out on staff requisition on the staff requisition form report.
                rpt_row("Column19") = dtRow("ClassGrp")
                rpt_row("Column20") = dtRow("Class")
                'Pinkal (14-Apr-2023) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_row)
            Next


            StrQ = "select " & _
                    "    rcstaffrequisition_tran.formno " & _
                    "   ,rcstaffrequisition_emp_tran.staffrequisitiontranunkid " & _
                    "   ,employeecode+' - '+firstname+' '+surname as employee " & _
                    "   ,ejb.job_name as jobtitle " & _
                    "from rcstaffrequisition_emp_tran " & _
                    "   join rcstaffrequisition_tran on rcstaffrequisition_tran.staffrequisitiontranunkid = rcstaffrequisition_emp_tran.staffrequisitiontranunkid " & _
                    "   join hremployee_master on hremployee_master.employeeunkid = rcstaffrequisition_emp_tran.employeeunkid " & _
                    "   left join " & _
                    "   ( " & _
                    "       select " & _
                    "            employeeunkid " & _
                    "           ,jobunkid " & _
                    "           ,row_number()over(partition by employeeunkid order by effectivedate desc) as rno " & _
                    "       from hremployee_categorization_tran " & _
                    "       where isvoid = 0 and convert(nvarchar(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) as ecat on ecat.employeeunkid = hremployee_master.employeeunkid and ecat.rno = 1 " & _
                    "   left join hrjob_master ejb on ecat.jobunkid = ejb.jobunkid and ejb.isactive = 1 " & _
                    "where rcstaffrequisition_emp_tran.isvoid = 0 and rcstaffrequisition_tran.isvoid = 0 "

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.allocationunkid IN (" & mstrAllocationIds & ") "
            End If

            If mstrRequisitionFormIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid IN (" & mstrRequisitionFormIds & ") "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Empl = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_row As DataRow = rpt_Empl.Tables("ArutiTable").NewRow()

                rpt_row("Column1") = dtRow("formno")
                rpt_row("Column2") = dtRow("employee")
                rpt_row("Column3") = dtRow("jobtitle")

                rpt_Empl.Tables("ArutiTable").Rows.Add(rpt_row)
            Next

            'Sohail (12 Oct 2018) -- Start
            'Issue : Pending records are coming even if other user of same level has approved the staff requisition in 75.1.
            'StrQ = "IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL BEGIN DROP TABLE #TempTable; END; " & _
            '       "create table #TempTable (userid int, uname nvarchar(max), title nvarchar(max)) " & _
            '       "declare @strq as nvarchar(max),@dname nvarchar(max),@edate nvarchar(8),@empid int, @userid int " & _
            '       "declare @finalstrq as nvarchar(max);set @finalstrq = '' " & _
            '       "DECLARE db_cursor CURSOR FOR " & _
            '       "    select distinct " & _
            '       "         cfuser_master.userunkid " & _
            '       "        ,employeeunkid " & _
            '       "        ,isnull([database_name],'') as dname " & _
            '       "        ,isnull(key_value,'') as edate " & _
            '       "    from rcstaffrequisition_approver_mapping " & _
            '       "        left join hrmsConfiguration..cfuser_master on hrmsConfiguration..cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
            '       "        left join hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cfuser_master.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid " & _
            '       "        left join hrmsConfiguration..cfconfiguration on cfconfiguration.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid AND UPPER([key_name]) = 'EMPLOYEEASONDATE' " & _
            '       "    where rcstaffrequisition_approver_mapping.isvoid = 0 " & _
            '       "OPEN db_cursor " & _
            '       "FETCH NEXT FROM db_cursor INTO @userid,@empid,@dname,@edate " & _
            '       "WHILE @@FETCH_STATUS = 0 " & _
            '       "BEGIN " & _
            '       "    SET @strq = '' " & _
            '       "    IF @empid > 0 " & _
            '       "        BEGIN " & _
            '       "            set @strq = 'insert into #TempTable (userid,uname,title) select ''' + cast(@userid as nvarchar(max)) + ''' ,firstname + '' '' + surname as ename " & _
            '       "                        ,jm.job_name as title from ' + @dname + '..hremployee_master " & _
            '       "                        left join " & _
            '       "                        ( " & _
            '       "                            select " & _
            '       "                                 employeeunkid " & _
            '       "                                ,jobunkid " & _
            '       "                                ,row_number()over(partition by employeeunkid order by effectivedate desc) as rno " & _
            '       "                            from ' + @dname + '..hremployee_categorization_tran " & _
            '       "                            where isvoid = 0 and employeeunkid = ' + cast(@empid as nvarchar(max)) + ' " & _
            '       "                            and convert(nvarchar(8),effectivedate,112) <= '''+ @edate + ''' " & _
            '       "                        ) as ej on ej.employeeunkid = hremployee_master.employeeunkid and ej.rno = 1 " & _
            '       "                        left join ' + @dname + '..hrjob_master as jm on ej.jobunkid = jm.jobunkid " & _
            '       "                        where hremployee_master.employeeunkid = ' + cast(@empid as nvarchar(max)) + ' ' " & _
            '       "        END " & _
            '       "    ELSE " & _
            '       "        BEGIN " & _
            '       "            SET @strq = 'insert into #TempTable (userid,uname,title) select userunkid,username,'''' as title from hrmsConfiguration..cfuser_master where userunkid = ' + cast(@userid as nvarchar(max)) + ';' " & _
            '       "        END " & _
            '       "    exec(@strq) " & _
            '       "    FETCH NEXT FROM db_cursor INTO @userid,@empid,@dname,@edate " & _
            '       "END " & _
            '       "CLOSE db_cursor " & _
            '       "DEALLOCATE db_cursor " & _
            '       "select " & _
            '       "     levelname " & _
            '       "    ,uname " & _
            '       "    ,title " & _
            '       "    ,isnull(convert(nvarchar(8),approval_date,112),'') as approval_date " & _
            '       "    ,isnull(approval_status,@Pending) as approval_status " & _
            '       "    ,isnull(remarks,'') as comments " & _
            '       "    ,rcstaffrequisition_tran.staffrequisitiontranunkid " & _
            '       "    ,rcstaffrequisition_tran.formno " & _
            '       "from rcstaffrequisition_tran " & _
            '       "    left join rcstaffrequisition_approver_mapping as sam on sam.allocationid = rcstaffrequisition_tran.staffrequisitionbyid " & _
            '       "        and sam.allocationunkid = rcstaffrequisition_tran.allocationunkid and sam.isvoid = 0 " & _
            '       "    join #TempTable on #TempTable.userid = sam.userapproverunkid " & _
            '       "    join rcstaffrequisitionlevel_master on sam.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
            '       "left join " & _
            '       "( " & _
            '       "    select " & _
            '       "         staffrequisitiontranunkid " & _
            '       "        ,userunkid " & _
            '       "        ,approval_date " & _
            '       "        ,case when statusunkid = 1 then @Pending " & _
            '       "              when statusunkid = 2 then @Approved " & _
            '       "              when statusunkid = 3 then @Rejected " & _
            '       "              when statusunkid = 4 then @Cancelled " & _
            '       "              when statusunkid = 5 then @Published " & _
            '       "         end as approval_status " & _
            '       "        ,remarks " & _
            '       "    from rcstaffrequisition_approval_tran " & _
            '       "    where isvoid = 0 " & _
            '       ") as rca on rca.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid and rca.userunkid = sam.userapproverunkid " & _
            '       "where rcstaffrequisition_tran.isvoid = 0 "
            StrQ = "IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL BEGIN DROP TABLE #TempTable; END; " & _
                   "create table #TempTable (userid int, uname nvarchar(max), title nvarchar(max)) " & _
                   "declare @strq as nvarchar(max),@dname nvarchar(max),@edate nvarchar(8),@empid int, @userid int " & _
                   "declare @finalstrq as nvarchar(max);set @finalstrq = '' " & _
                   "DECLARE db_cursor CURSOR FOR " & _
                   "    select distinct " & _
                   "         cfuser_master.userunkid " & _
                   "        ,employeeunkid " & _
                   "        ,isnull([database_name],'') as dname " & _
                   "        ,isnull(key_value,'') as edate " & _
                   "    from rcstaffrequisition_approver_mapping " & _
                   "        left join hrmsConfiguration..cfuser_master on hrmsConfiguration..cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                   "        left join hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cfuser_master.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid /* AND database_name = '" & strDatabaseName & "' */  " & _
                   "        left join hrmsConfiguration..cfconfiguration on cfconfiguration.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid AND UPPER([key_name]) = 'EMPLOYEEASONDATE' " & _
                   "    where 1 = 1 /*rcstaffrequisition_approver_mapping.isvoid = 0*/ " & _
                   "          AND database_name = '" & strDatabaseName & "' " & _
                   "    UNION " & _
                   "    select distinct " & _
                   "         cfuser_master.userunkid " & _
                   "        ,cfuser_master.employeeunkid " & _
                   "        ,isnull([database_name],'') as dname " & _
                   "        ,isnull(key_value,'') as edate " & _
                   "    from rcstaffrequisition_tran " & _
                   "        left join hrmsConfiguration..cfuser_master on hrmsConfiguration..cfuser_master.userunkid = rcstaffrequisition_tran.userunkid " & _
                   "        left join hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cfuser_master.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid /* AND database_name = '" & strDatabaseName & "' */ " & _
                   "        left join hrmsConfiguration..cfconfiguration on cfconfiguration.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid AND UPPER([key_name]) = 'EMPLOYEEASONDATE' " & _
                   "    where rcstaffrequisition_tran.isvoid = 0 " & _
                   "          AND database_name = '" & strDatabaseName & "' " & _
                   "OPEN db_cursor " & _
                   "FETCH NEXT FROM db_cursor INTO @userid,@empid,@dname,@edate " & _
                   "WHILE @@FETCH_STATUS = 0 " & _
                   "BEGIN " & _
                   "    SET @strq = '' " & _
                   "    IF @empid > 0 " & _
                   "        BEGIN " & _
                   "            set @strq = 'insert into #TempTable (userid,uname,title) select ''' + cast(@userid as nvarchar(max)) + ''' ,firstname + '' '' + surname as ename " & _
                   "                        ,jm.job_name as title from ' + @dname + '..hremployee_master " & _
                   "                        left join " & _
                   "                        ( " & _
                   "                            select " & _
                   "                                 employeeunkid " & _
                   "                                ,jobunkid " & _
                   "                                ,row_number()over(partition by employeeunkid order by effectivedate desc) as rno " & _
                   "                            from ' + @dname + '..hremployee_categorization_tran " & _
                   "                            where isvoid = 0 and employeeunkid = ' + cast(@empid as nvarchar(max)) + ' " & _
                   "                            and convert(nvarchar(8),effectivedate,112) <= '''+ @edate + ''' " & _
                   "                        ) as ej on ej.employeeunkid = hremployee_master.employeeunkid and ej.rno = 1 " & _
                   "                        left join ' + @dname + '..hrjob_master as jm on ej.jobunkid = jm.jobunkid " & _
                   "                        where hremployee_master.employeeunkid = ' + cast(@empid as nvarchar(max)) + ' ' " & _
                   "        END " & _
                   "    ELSE " & _
                   "        BEGIN " & _
                   "            SET @strq = 'insert into #TempTable (userid,uname,title) select userunkid,username,'''' as title from hrmsConfiguration..cfuser_master where userunkid = ' + cast(@userid as nvarchar(max)) + ';' " & _
                   "        END " & _
                   "    exec(@strq) " & _
                   "    FETCH NEXT FROM db_cursor INTO @userid,@empid,@dname,@edate " & _
                   "END " & _
                   "CLOSE db_cursor " & _
                   "DEALLOCATE db_cursor " & _
                   "SELECT * FROM ( " & _
                   "SELECT " & _
                   "     @RequestingManager AS levelname " & _
                   "    ,#TempTable.userid " & _
                   "    ,uname " & _
                   "    ,title " & _
                   "    ,ISNULL(convert(nvarchar(8), requisition_date,112),'') AS approval_date " & _
                   "    ,@Requested AS approval_status " & _
                   "    ,'' AS comments " & _
                   "    ,rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                   "    ,rcstaffrequisition_tran.formno " & _
                   "    ,0 AS priority " & _
                   "    , 0 as approvervoided " & _
                   "    , 1 AS ROWNO " & _
                   "    , 0 as approval_statusunkid " & _
                   "    , form_statusunkid " & _
                   "FROM rcstaffrequisition_tran " & _
                   "    JOIN #TempTable on #TempTable.userid = rcstaffrequisition_tran.userunkid " & _
                   "WHERE rcstaffrequisition_tran.isvoid = 0 "
            'Hemant (22 Dec 2023) -- [approval_statusunkid,form_statusunkid]
            'Sohail (08 Feb 2022) - [1 = 1 /*rcstaffrequisition_approver_mapping.isvoid = 0*/]
            'Sohail (24 Jan 2022) - [approvervoided]

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.allocationunkid IN (" & mstrAllocationIds & ") "
            End If

            If mstrRequisitionFormIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid IN (" & mstrRequisitionFormIds & ") "
            End If

            'Pinkal (26-Nov-2021)-- Start
            'NMB Staff Requisition Approval Bug . 
            'ADDED DISTINCT in Below Query
            'Pinkal (26-Nov-2021)-- End

            StrQ &= "UNION ALL " & _
                   " SELECT " & _
                   "     DISTINCT " & _
                   "     levelname " & _
                   "    ,#TempTable.userid " & _
                   "    ,uname " & _
                   "    ,title " & _
                   "    ,isnull(convert(nvarchar(8),approval_date,112),'') as approval_date " & _
                   "    ,isnull(approval_status,@Pending) as approval_status " & _
                   "    ,isnull(remarks,'') as comments " & _
                   "    ,rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                   "    ,rcstaffrequisition_tran.formno " & _
                   "    ,rcstaffrequisitionlevel_master.priority " & _
                   "    , ISNULL(sam.isvoid, 0) as approvervoided " & _
                   "    , DENSE_RANK() OVER (PARTITION BY priority,rcstaffrequisition_tran.staffrequisitiontranunkid ORDER BY rca.statusunkid DESC) AS ROWNO " & _
                   "    , ISNULL(rca.statusunkid, 1) as approval_statusunkid " & _
                   "    , form_statusunkid " & _
                   "FROM rcstaffrequisition_tran " & _
                   "    LEFT JOIN rcstaffrequisition_approver_mapping as sam on sam.allocationid = rcstaffrequisition_tran.staffrequisitionbyid " & _
                   "        and sam.allocationunkid = rcstaffrequisition_tran.allocationunkid "
            'Hemant (22 Dec 2023) -- [approval_statusunkid,form_statusunkid]
            'Sohail (24 Jan 2022) - [approvervoided]

            'Pinkal (02-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            StrQ &= " JOIN rcstaffrequisition_approver_jobmapping ON sam.allocationid = rcstaffrequisition_approver_jobmapping.allocationid " & _
                   " AND sam.levelunkid = rcstaffrequisition_approver_jobmapping.levelunkid " & _
                   " AND sam.userapproverunkid = rcstaffrequisition_approver_jobmapping.userapproverunkid " & _
                  " AND rcstaffrequisition_approver_jobmapping.jobunkid= rcstaffrequisition_tran.jobunkid "
            'Pinkal (02-Nov-2021) -- End


            StrQ &= "    JOIN #TempTable on #TempTable.userid = sam.userapproverunkid " & _
                   "    JOIN rcstaffrequisitionlevel_master on sam.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    select " & _
                   "         staffrequisitiontranunkid " & _
                   "        ,userunkid " & _
                   "        ,rcstaffrequisition_approval_tran.levelunkid " & _
                   "        ,approval_date " & _
                   "        ,statusunkid " & _
                   "        ,case when statusunkid = 1 then @Pending " & _
                   "              when statusunkid = 2 then @Approved " & _
                   "              when statusunkid = 3 then @Rejected " & _
                   "              when statusunkid = 4 then @Cancelled " & _
                   "              when statusunkid = 5 then @Published " & _
                   "         end as approval_status " & _
                   "        ,remarks " & _
                   "        ,rcstaffrequisition_approval_tran.priority as prioid " & _
                   "    FROM rcstaffrequisition_approval_tran " & _
                   "    WHERE isvoid = 0 "

            'Sohail (28 Jan 2022) -- Start
            'Enhancement :  OLD-547 : NMB - Creation of staff requisition approver migration.
            If mstrRequisitionFormIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_approval_tran.staffrequisitiontranunkid IN (" & mstrRequisitionFormIds & ") "
            End If
            'Sohail (28 Jan 2022) -- End

            StrQ &= ") as rca on rca.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid and rca.levelunkid = sam.levelunkid and rca.userunkid = sam.userapproverunkid " & _
                   "WHERE rcstaffrequisition_tran.isvoid = 0 "


            'Pinkal (26-Nov-2021)-- Start
            'NMB Staff Requisition Approval Bug . 
            '"AND sam.isvoid = 0 "  GETTING INACTIVE APROVER ALSO
            'Pinkal (26-Nov-2021) -- End




            'Pinkal (02-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            'StrQ &= " AND rcstaffrequisition_approver_jobmapping.isvoid = 0 " 'Sohail (31 Jan 2022) - [Commented To get approved approvals for voided users]
            'Pinkal (02-Nov-2021) -- End


            'Sohail (12 Oct 2018) -- End

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.allocationunkid IN (" & mstrAllocationIds & ") "
            End If

            If mstrRequisitionFormIds.Trim.Length > 0 Then
                StrQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid IN (" & mstrRequisitionFormIds & ") "
            End If

            'Sohail (12 Oct 2018) -- Start
            'Issue : Pending records are coming even if other user of same level has approved the staff requisition in 75.1.
            'StrQ &= "ORDER BY rcstaffrequisition_tran.staffrequisitiontranunkid,rcstaffrequisitionlevel_master.priority " & _
            '        "IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL BEGIN DROP TABLE #TempTable; END; "
            StrQ &= ") AS A WHERE A.ROWNO = 1  " & _
                    "ORDER BY staffrequisitiontranunkid, priority " & _
                    "IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL BEGIN DROP TABLE #TempTable; END; "
            'Sohail (12 Oct 2018) -- End

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            objDataOperation.AddParameter("@RequestingManager", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Requesting Manager"))
            objDataOperation.AddParameter("@Requested", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Requested"))
            'Sohail (12 Oct 2018) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            'Sohail (31 Jan 2022) -- Start
            'Issue :  : NMB - Users are coming twice on staff requisition report if he is voided and remapped at same level.
            Dim result = From p In dsList.Tables(0).AsEnumerable Group p By G = New With {Key .staffrequisitiontranunkid = CInt(p.Item("staffrequisitiontranunkid")), Key .userid = CInt(p.Item("userid")), Key .form_statusunkid = CInt(p.Item("form_statusunkid"))} _
                                                                                     Into grp = Group Where (grp.Count(Function(x) x.Item("userid")) > 1) Select New With {Key .staffrequisitiontranunkid = CInt(grp(0).Item("staffrequisitiontranunkid")), Key .userid = CInt(grp(0).Item("userid")), Key .form_statusunkid = CInt(grp(0).Item("form_statusunkid")), Key .usercount = grp.Count(Function(x) x.Item("userid"))}

            For Each itm In result
                'Hemant (22 Dec 2023) -- Start
                'ENHANCEMENT(NMB): A1X-1642 - Newly created approvers showing up on already approved forms provided they have access to particular department and job
                If itm.form_statusunkid = enApprovalStatus.PENDING Then
                    'Hemant (22 Dec 2023) -- End
                Dim r() As DataRow = dsList.Tables(0).Select("staffrequisitiontranunkid = " & itm.staffrequisitiontranunkid & " AND userid = " & itm.userid & " AND approvervoided = 1")
                If r.Length > 0 Then
                    For Each rr As DataRow In r
                        dsList.Tables(0).Rows.Remove(rr)
                    Next
                End If
                    'Hemant (22 Dec 2023) -- Start
                    'ENHANCEMENT(NMB): A1X-1642 - Newly created approvers showing up on already approved forms provided they have access to particular department and job
                Else
                    Dim drPendingRow() As DataRow = dsList.Tables(0).Select("staffrequisitiontranunkid = " & itm.staffrequisitiontranunkid & " AND userid = " & itm.userid & " AND approval_statusunkid = " & enApprovalStatus.PENDING & " ")
                    If drPendingRow.Length > 0 Then
                        For Each dPendingRow As DataRow In drPendingRow
                            dsList.Tables(0).Rows.Remove(dPendingRow)
                        Next
                    End If
                    Dim drMultiRow() As DataRow = dsList.Tables(0).Select("staffrequisitiontranunkid = " & itm.staffrequisitiontranunkid & " AND userid = " & itm.userid & "")
                    If drMultiRow.Length > 1 Then
                        Dim drVoidRow() As DataRow = dsList.Tables(0).Select("staffrequisitiontranunkid = " & itm.staffrequisitiontranunkid & " AND userid = " & itm.userid & " AND approvervoided = 1")
                        If drVoidRow.Length > 0 Then
                            For Each dVoidRow As DataRow In drVoidRow
                                dsList.Tables(0).Rows.Remove(dVoidRow)
                            Next
                        End If
                    End If
                End If
                'Hemant (22 Dec 2023) -- End
            Next
            dsList.Tables(0).AcceptChanges()
            'Sohail (31 Jan 2022) -- End

            rpt_Appr = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                'Sohail (24 Jan 2022) -- Start
                'NMB Issue :  : voided users are coming with pending status on staff requisition report even if nothing have been approved by them.
                If CBool(dtRow("approvervoided")) = True AndAlso dtRow("approval_date").ToString.Trim.Length <= 0 Then Continue For
                'Sohail ((24 Jan 2022) -- End
                Dim rpt_row As DataRow = rpt_Appr.Tables("ArutiTable").NewRow()

                rpt_row("Column1") = dtRow("formno")
                rpt_row("Column2") = dtRow("levelname")
                rpt_row("Column3") = dtRow("uname")
                rpt_row("Column4") = dtRow("title")
                If dtRow("approval_date").ToString.Trim.Length > 0 Then
                    rpt_row("Column5") = eZeeDate.convertDate(dtRow("approval_date").ToString()).ToShortDateString()
                End If
                rpt_row("Column6") = dtRow("approval_status")
                rpt_row("Column7") = dtRow("comments")

                rpt_Appr.Tables("ArutiTable").Rows.Add(rpt_row)
            Next

            objRpt = New ArutiReport.Designer.rptStaff_Requisition_Form

            Dim objReportFunction As New ReportFunction
            Call objReportFunction.GetReportSetting(CInt(mstrReportId), intCompanyUnkid)

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        objReportFunction._DisplayLogo, _
                                        objReportFunction._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        objReportFunction._LeftMargin, _
                                        objReportFunction._RightMargin, _
                                        objReportFunction._PageMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objReportFunction = Nothing

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptApprovals").SetDataSource(rpt_Appr)
            objRpt.Subreports("rptEmployeeData").SetDataSource(rpt_Empl)

            'Sohail (12 Oct 2018) -- Start
            'Language number reordered in 75.1.
            Call ReportFunction.TextChange(objRpt, "txtStaffRequistionNo", Language.getMessage(mstrModuleName, 8, "Staff Requistion No"))
            Call ReportFunction.TextChange(objRpt, "txtStaffRequisitionParticulars", Language.getMessage(mstrModuleName, 9, "Staff Requisition Particulars"))
            Call ReportFunction.TextChange(objRpt, "txtRequisitontype", Language.getMessage(mstrModuleName, 1, "Requisition Type :"))
            Call ReportFunction.TextChange(objRpt, "txtRequisitionBy", Language.getMessage(mstrModuleName, 2, "Requisition By :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job : "))
            Call ReportFunction.TextChange(objRpt, "txtJustification", Language.getMessage(mstrModuleName, 10, "Justification Reason :"))
            Call ReportFunction.TextChange(objRpt, "txtNoOfPosition", Language.getMessage(mstrModuleName, 11, "No Of Position :"))
            Call ReportFunction.TextChange(objRpt, "txtAllocation", Language.getMessage(mstrModuleName, 12, "Allocation :"))
            Call ReportFunction.TextChange(objRpt, "txtWorkStartDate", Language.getMessage(mstrModuleName, 13, "Work Start Date :"))
            Call ReportFunction.TextChange(objRpt, "txtJobDescription", Language.getMessage(mstrModuleName, 14, "Job Description"))

            'Pinkal (14-Apr-2023) -- Start
            '(A1X-816) NMB As a user, I want to have allocations filled out on staff requisition on the staff requisition form report.
            Call ReportFunction.TextChange(objRpt, "txtClassGrp", Language.getMessage(mstrModuleName, 36, "Class Group :"))
            Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 37, "Class :"))
            'Pinkal (14-Apr-2023) -- End


            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeData"), "txtEmployeeDetails", Language.getMessage(mstrModuleName, 15, "Employee Details"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeData"), "txtEmployee", Language.getMessage(mstrModuleName, 16, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeData"), "txtJobTitle", Language.getMessage(mstrModuleName, 17, "Job Title"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtStaffRequistionApprovals", Language.getMessage(mstrModuleName, 18, "Staff Requistion Approvals"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtLevelName", Language.getMessage(mstrModuleName, 19, "Level Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtApprover", Language.getMessage(mstrModuleName, 20, "Approver"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtTitle", Language.getMessage(mstrModuleName, 21, "Job Title"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtStatus", Language.getMessage(mstrModuleName, 22, "Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtDate", Language.getMessage(mstrModuleName, 23, "Approval Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtRemark", Language.getMessage(mstrModuleName, 24, "Remark/Status"))
            'Sohail (12 Oct 2018) -- End

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 25, "Grade :"))
            Call ReportFunction.TextChange(objRpt, "txtGradeLevel", Language.getMessage(mstrModuleName, 26, "Grade Level :"))
            Call ReportFunction.TextChange(objRpt, "txtApprovedHeadCount", Language.getMessage(mstrModuleName, 27, "Approved Head Count :"))
            Call ReportFunction.TextChange(objRpt, "txtActualHeadCount", Language.getMessage(mstrModuleName, 28, "Actual Head Count :"))
            Call ReportFunction.TextChange(objRpt, "txtContractType", Language.getMessage(mstrModuleName, 29, "Contract Type :"))
            Call ReportFunction.TextChange(objRpt, "txtContractPeriod", Language.getMessage(mstrModuleName, 30, "Contract Period (in months) :"))
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            Call ReportFunction.TextChange(objRpt, "txtReportingLine", Language.getMessage(mstrModuleName, 33, "Reporting Line :"))
            Call ReportFunction.TextChange(objRpt, "txtJobAdvert", Language.getMessage(mstrModuleName, 34, "Job Advert"))
            'Sohail (29 Sep 2021) -- End

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            'Sohail (14 Sep 2021) -- Start
            'NMB Issue :  : Staff requisition report showing wrong signature (printed by).
            'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", mstrUser_Name)
            'Sohail (14 Sep 2021) -- End

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Requisition Type :")
            Language.setMessage(mstrModuleName, 2, "Requisition By :")
            Language.setMessage(mstrModuleName, 3, "Job :")
            Language.setMessage(mstrModuleName, 4, "No Of Position From :")
            Language.setMessage(mstrModuleName, 5, "To :")
            Language.setMessage(mstrModuleName, 6, "Work Start Date From :")
            Language.setMessage(mstrModuleName, 7, "To :")
            Language.setMessage(mstrModuleName, 8, "Staff Requistion No")
            Language.setMessage(mstrModuleName, 9, "Staff Requisition Particulars")
            Language.setMessage(mstrModuleName, 10, "Justification Reason :")
            Language.setMessage(mstrModuleName, 11, "No Of Position :")
            Language.setMessage(mstrModuleName, 12, "Allocation :")
            Language.setMessage(mstrModuleName, 13, "Work Start Date :")
            Language.setMessage(mstrModuleName, 14, "Job Description")
            Language.setMessage(mstrModuleName, 15, "Employee Details")
            Language.setMessage(mstrModuleName, 16, "Employee")
            Language.setMessage(mstrModuleName, 17, "Job Title")
            Language.setMessage(mstrModuleName, 18, "Staff Requistion Approvals")
            Language.setMessage(mstrModuleName, 19, "Level Name")
            Language.setMessage(mstrModuleName, 20, "Approver")
            Language.setMessage(mstrModuleName, 21, "Job Title")
            Language.setMessage(mstrModuleName, 22, "Status")
            Language.setMessage(mstrModuleName, 23, "Approval Date")
            Language.setMessage(mstrModuleName, 24, "Remark/Status")
			Language.setMessage(mstrModuleName, 25, "Grade :")
			Language.setMessage(mstrModuleName, 26, "Grade Level :")
			Language.setMessage(mstrModuleName, 27, "Approved Head Count :")
			Language.setMessage(mstrModuleName, 28, "Actual Head Count :")
			Language.setMessage(mstrModuleName, 29, "Contract Type :")
			Language.setMessage(mstrModuleName, 30, "Contract Period (in months) :")
			Language.setMessage(mstrModuleName, 31, "Requesting Manager")
			Language.setMessage(mstrModuleName, 32, "Requested")
			Language.setMessage(mstrModuleName, 33, "Reporting Line :")
			Language.setMessage(mstrModuleName, 34, "Job Advert")
            Language.setMessage(mstrModuleName, 35, "Status :")
            Language.setMessage("clsMasterData", 262, "Approved")
            Language.setMessage("clsMasterData", 263, "Pending")
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 422, "Job Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage("clsMasterData", 517, "Replacement")
            Language.setMessage("clsMasterData", 518, "Additional")
            Language.setMessage("clsMasterData", 520, "Rejected")
            Language.setMessage("clsMasterData", 522, "Cancelled")
            Language.setMessage("clsMasterData", 524, "Published")
            Language.setMessage("clsMasterData", 586, "Cost Center")
            Language.setMessage("clsMasterData", 660, "Replacement and Additional")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
