'************************************************************************************************************************************
'Class Name : frmInterviewAnalysisReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApplicantCVReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApplicantCVReport"
    Private objAppCV As clsApplicantCVReport
    'Hemant (11 Apr 2022) -- Start            
    'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
    Private mdtVacancyEndDate As Date
    'Hemant (11 Apr 2022) -- End
#End Region

#Region " Contructor "

    Public Sub New()
        objAppCV = New clsApplicantCVReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAppCV.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objApplicant As New clsApplicant_master
        Dim dsCombo As New DataSet
        Try
           

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'dsCombo = objApplicant.GetApplicantList("List", True)
            'With cboApplicant
            '    .ValueMember = "applicantunkid"
            '    .DisplayMember = "applicantname"
            '    .DataSource = dsCombo.Tables("List")
            'End With

            Dim objVacancy As New clsVacancy
            dsCombo = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            Dim objApprovalStatus As New clsMasterData
            dsCombo = objApprovalStatus.GetComboforShortlistingApprovalStatus(True, "List")
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Applicant CV Report"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Short Listed Applicant CV Report"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "Final Short Listed Applicant CV Report"))
            cboReportType.SelectedIndex = 0

            'Varsha (03 Jan 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            cboReportType.SelectedIndex = 0
            cboVacancyType.SelectedIndex = 0
            cboVacancy.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            'Varsha (03 Jan 2018) -- End

            cboApplicant.SelectedValue = 0
            chkShowJobHistory.Checked = True
            chkShowOtherSkills.Checked = True
            chkShowProfessionalQualification.Checked = True
            chkShowReferences.Checked = True
            chkShowShortCourses.Checked = True
            chkShpwProfessionalSkills.Checked = True
            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            chkShowAttachments.Checked = True
            'Sohail (02 Jun 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAppCV.SetDefaultValue()

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.

            'If cboApplicant.SelectedValue <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Applicant is mandatory information. Please select Applicant to continue."), enMsgBoxStyle.Information)
            '    cboApplicant.Focus()
            '    Return False
            'End If


            If cboVacancyType.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Vacancy Type is mandatory information. Please select Vacancy Type to continue."), enMsgBoxStyle.Information)
                cboVacancyType.Focus()
                Return False
            ElseIf cboVacancy.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Vacancy is mandatory information. Please select Vacancy to continue."), enMsgBoxStyle.Information)
                cboVacancy.Focus()
                Return False
            End If
            'Varsha (03 Jan 2018) -- End


            objAppCV._ApplicantUnkid = cboApplicant.SelectedValue

            objAppCV._ShowJobHistory = chkShowJobHistory.Checked
            objAppCV._ShowOther_Qualification = chkShowShortCourses.Checked
            objAppCV._ShowProfessional_Qualification = chkShowProfessionalQualification.Checked
            objAppCV._ShowProfessional_Skills = chkShpwProfessionalSkills.Checked
            objAppCV._ShowReferences = chkShowReferences.Checked
            objAppCV._ShowOther_Skills = chkShowOtherSkills.Checked
            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            objAppCV._ShowAttachments = chkShowAttachments.Checked
            'Sohail (02 Jun 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objAppCV._DocumentPath = ConfigParameter._Object._Document_Path
            'Shani(24-Aug-2015) -- End


            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            objAppCV._ReportTypeId = cboReportType.SelectedIndex
            objAppCV._ReportTypeName = cboReportType.Text
            objAppCV._VacancyTypeId = cboVacancyType.SelectedValue
            objAppCV._VacancyTypeName = cboVacancyType.SelectedText
            objAppCV._VacancyId = cboVacancy.SelectedValue
            objAppCV._VacancyName = cboVacancy.SelectedText
            objAppCV._ApprovalStatusId = cboStatus.SelectedValue
            objAppCV._ApprovalStatusName = cboStatus.SelectedText
            'Varsha (03 Jan 2018) -- End
            'Hemant (11 Apr 2022) -- Start            
            'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
            objAppCV._VacancyEndDate = mdtVacancyEndDate
            'Hemant (11 Apr 2022) -- End
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function


    'Varsha (03 Jan 2018) -- Start
    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
    'This filter should allow user to report on all cv's shortlisted for that particular position. 
    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
    Private Sub FillApplicant()
        Try
            Dim objApplicant As New clsApplicant_master
            Dim dsCombo As New DataSet
            If cboReportType.SelectedIndex = 1 Then
                dsCombo = objApplicant.GetApplicantFromVacancy(CInt(cboVacancy.SelectedValue), "List", True, True, False)
            ElseIf cboReportType.SelectedIndex = 2 Then
                dsCombo = objApplicant.GetApplicantFromVacancy(CInt(cboVacancy.SelectedValue), "List", True, False, True)
            Else
                dsCombo = objApplicant.GetApplicantFromVacancy(CInt(cboVacancy.SelectedValue), "List", True, False, False)
            End If

            With cboApplicant
                .ValueMember = "applicantunkid"
                 .DisplayMember = "Applicant"
                 .DataSource = dsCombo.Tables("List")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicant", mstrModuleName)
        End Try
    End Sub

    'Varsha (03 Jan 2018) -- End
#End Region

#Region " Forms "

    Private Sub frmApplicantCVReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAppCV = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmApplicantCVReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantCVReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objAppCV._ReportName
            Me._Message = objAppCV._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantCVReport_Load", mstrModuleName)
        End Try
    End Sub


    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objAppCV.generateReport(0, e.Type, enExportAction.None)
            objAppCV.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                       ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, _
                                       0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objAppCV.generateReport(0, enPrintAction.None, e.Type)
            objAppCV.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                       ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, _
                                       0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                frm.ValueMember = cboApplicant.ValueMember
                frm.DisplayMember = cboApplicant.DisplayMember
                'Pinkal (05-Apr-2018) -- Start
                'Bug - (RefNo: 0002156)  Applicant CV report not applying the vacancy filter.
                'frm.CodeMember = "applicant_code"
                frm.CodeMember = "applicantcode"
             'Pinkal (05-Apr-2018) -- End
                frm.DataSource = cboApplicant.DataSource
            End With
            If frm.DisplayDialog Then
                cboApplicant.SelectedValue = frm.SelectedValue
                cboApplicant.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsApplicantCVReport.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicantCVReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


    'Varsha (03 Jan 2018) -- Start
    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
    'This filter should allow user to report on all cv's shortlisted for that particular position. 
    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = cboVacancy.DataSource
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Varsha (03 Jan 2018) -- End

#End Region


    'Varsha (03 Jan 2018) -- Start
    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
    'This filter should allow user to report on all cv's shortlisted for that particular position. 
    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
#Region " CombBox Event "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsCombo As New DataSet

            dsCombo = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", -1, CInt(cboVacancyType.SelectedValue))
            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Dim objVacancy As New clsVacancy     'Hemant (11 Apr 2022)
        Try
            'Hemant (11 Apr 2022) -- Start            
            'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
            objVacancy._Vacancyunkid = CInt(cboVacancy.SelectedValue)
            mdtVacancyEndDate = objVacancy._Closingdate
            'Hemant (11 Apr 2022) -- End
            FillApplicant()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrModuleName)
            'Hemant (11 Apr 2022) -- Start            
            'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
        Finally
            objVacancy = Nothing
            'Hemant (11 Apr 2022) -- End
        End Try

    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 2 Then 'Final Short Listed Applicant CV Report
                cboStatus.Enabled = True
            Else
                cboStatus.Enabled = False
                cboStatus.SelectedIndex = 0
            End If
            FillApplicant()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Varsha (03 Jan 2018) -- End



   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkShowReferences.Text = Language._Object.getCaption(Me.chkShowReferences.Name, Me.chkShowReferences.Text)
			Me.chkShowJobHistory.Text = Language._Object.getCaption(Me.chkShowJobHistory.Name, Me.chkShowJobHistory.Text)
			Me.chkShowShortCourses.Text = Language._Object.getCaption(Me.chkShowShortCourses.Name, Me.chkShowShortCourses.Text)
			Me.chkShowProfessionalQualification.Text = Language._Object.getCaption(Me.chkShowProfessionalQualification.Name, Me.chkShowProfessionalQualification.Text)
			Me.chkShowOtherSkills.Text = Language._Object.getCaption(Me.chkShowOtherSkills.Name, Me.chkShowOtherSkills.Text)
			Me.chkShpwProfessionalSkills.Text = Language._Object.getCaption(Me.chkShpwProfessionalSkills.Name, Me.chkShpwProfessionalSkills.Text)
			Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
			Me.chkShowAttachments.Text = Language._Object.getCaption(Me.chkShowAttachments.Name, Me.chkShowAttachments.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.LalblVacancy.Text = Language._Object.getCaption(Me.LalblVacancy.Name, Me.LalblVacancy.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Applicant CV Report")
			Language.setMessage(mstrModuleName, 3, "Short Listed Applicant CV Report")
			Language.setMessage(mstrModuleName, 4, "Final Short Listed Applicant CV Report")
			Language.setMessage(mstrModuleName, 5, "Vacancy Type is mandatory information. Please select Vacancy Type to continue.")
			Language.setMessage(mstrModuleName, 6, "Vacancy is mandatory information. Please select Vacancy to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
