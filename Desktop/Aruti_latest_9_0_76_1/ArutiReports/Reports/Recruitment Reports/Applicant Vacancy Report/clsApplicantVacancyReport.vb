#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsApplicantVacancyReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsApplicantVacancyReport"
    Private mstrReportId As String = enArutiReport.ApplicantVacancyReport   '96
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintApplicantId As Integer = -1
    Private mstrApplicantName As String = String.Empty
    Private mintVacancyTypeId As Integer = -1
    Private mstrVacancyTypeName As String = String.Empty
    Private mintVacancyId As Integer = -1
    Private mstrVacancyName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ApplicantID() As Integer
        Set(ByVal value As Integer)
            mintApplicantId = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantName() As String
        Set(ByVal value As String)
            mstrApplicantName = value
        End Set
    End Property

    Public WriteOnly Property _VacancyTypeID() As Integer
        Set(ByVal value As Integer)
            mintVacancyTypeId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyTypeName() As String
        Set(ByVal value As String)
            mstrVacancyTypeName = value
        End Set
    End Property

    Public WriteOnly Property _VacancyID() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property
#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintApplicantId = 0
            mstrApplicantName = ""
            mintVacancyTypeId = 0
            mstrVacancyTypeName = ""
            mintVacancyId = 0
            mstrVacancyName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintApplicantId > 0 Then
                Me._FilterQuery &= " AND applicant.applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Applicant : ") & " " & mstrApplicantName & "    "
            End If

            If mintVacancyTypeId > 0 Then
                'Sohail (18 Apr 2020) -- Start
                'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                'If mintVacancyTypeId = 1 Then
                '    Me._FilterQuery &= " AND applicant.isexternalvacancy = 1 "
                'ElseIf mintVacancyTypeId = 2 Then
                '    Me._FilterQuery &= " AND applicant.isexternalvacancy = 0 "
                'End If
                If mintVacancyTypeId = enVacancyType.EXTERNAL_VACANCY Then
                    Me._FilterQuery &= " AND (applicant.isexternalvacancy = 1 AND ISNULL(applicant.isbothintext, 0) = 0) "
                ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_VACANCY Then
                    Me._FilterQuery &= " AND (applicant.isexternalvacancy = 0 AND ISNULL(applicant.isbothintext, 0) = 0) "
                ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_AND_EXTERNAL Then
                    Me._FilterQuery &= " AND ISNULL(applicant.isbothintext, 0) = 1 "
                End If
                'Sohail (18 Apr 2020) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Vacancy Type : ") & "   " & mstrVacancyTypeName & "    "
            End If

            If mintVacancyId > 0 Then
                Me._FilterQuery &= " AND applicant.vacancyunkid = @vacancyunkid "
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Vacancy : ") & "   " & mstrVacancyName & "    "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

#End Region

#Region " Report Generation "


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsListMain As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim dsApplicant As New DataSet
        Dim dsPQualification As New DataSet
        Dim dsWorkExperience As New DataSet
        Dim dsOtherShortCourses As New DataSet

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = intUserUnkid
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT " & _
                   "     applicantunkid " & _
                   "    ,code " & _
                   "	,CASE WHEN age = '0' THEN '' ELSE age END AS age " & _
                   "    ,gender " & _
                   "    ,NAMEAdd " & _
                   "    ,vacancyunkid " & _
                   "    ,isexternalvacancy " & _
                   "    ,isbothintext " & _
                   "    ,employeecode " & _
                   "    ,vacancy " & _
                   "    ,ODate " & _
                   "    ,CDate " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         rcapplicant_master.applicantunkid " & _
                   "        ,rcapplicant_master.applicant_code AS code " & _
                   "        ,CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') = '' THEN '' " & _
                   "            ELSE CAST(ISNULL(DATEDIFF(yy,rcapplicant_master.birth_date,GETDATE()),'') AS NVARCHAR(50)) END AS age " & _
                   "        ,ISNULL(CASE WHEN gender=1 THEN @MALE ELSE @FEMALE END ,'') AS gender " & _
                   "		,rcapplicant_master.firstname + ' ' + rcapplicant_master.surname + '\r\n' + rcapplicant_master.present_address1 + '\r\n' " & _
                   "		+ rcapplicant_master.present_address2 + '\r\n'+ rcapplicant_master.present_mobileno AS NAMEAdd " & _
                   "		,ISNULL(Vac.vacancyunkid, 0) AS vacancyunkid " & _
                   "		,ISNULL(Vac.isexternalvacancy, 0) AS isexternalvacancy " & _
                   "		,ISNULL(Vac.isbothintext, 0) AS isbothintext " & _
                   "        ,ISNULL(employeecode, '')  AS employeecode " & _
                   "        ,Vac.vacancy AS vacancy " & _
                   "        ,Vac.ODate AS ODate " & _
                   "        ,Vac.CDate AS CDate " & _
                   "FROM rcapplicant_master " & _
                   "	LEFT JOIN " & _
                   "	( " & _
                   "		SELECT " & _
                   "			 rcapp_vacancy_mapping.applicantunkid " & _
                   "			,rcapp_vacancy_mapping.vacancyunkid " & _
                   "			,rcvacancy_master.isexternalvacancy " & _
                   "			,rcvacancy_master.isbothintext " & _
                   "			,cfcommon_master.name AS vacancy " & _
                   "			,CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                   "			,CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                   "        FROM  rcapp_vacancy_mapping " & _
                   "			JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcapp_vacancy_mapping.vacancyunkid " & _
                   "			JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "  " & _
                   "		WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                   "    ) AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
                   "	WHERE isimport = 0 " & _
                   ") AS applicant " & _
                   "WHERE 1 = 1 "
            'Sohail (09 Oct 2018) - [rcapp_vacancy_mapping.isactive = 1]=[ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
            'AND rcapp_vacancy_mapping.vacancyunkid = '" & mintVacancyId & "'" & " 

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsListMain = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrQ = ""
            StrQ = "SELECT " & _
                   "applicantunkid " & _
                   ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                   "hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') " & _
                   "else " & _
                   "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') END,'') AS qualification " & _
                   ",ISNULL(hrresult_master.resultname,'') AS classification " & _
                   ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                   ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                   ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                   "FROM rcapplicantqualification_tran " & _
                   "JOIN hrqualification_master ON hrqualification_master.qualificationunkid=rcapplicantqualification_tran.qualificationunkid " & _
                   "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=rcapplicantqualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                   "LEFT JOIN hrresult_master ON hrresult_master.resultunkid=rcapplicantqualification_tran.resultunkid " & _
                   " WHERE rcapplicantqualification_tran.qualificationunkid > 0 AND rcapplicantqualification_tran.isvoid = 0 "

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END


            'Pinkal (20-Oct-2020) -- Start
            'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
            StrQ &= " UNION " & _
                         " SELECT " & _
                         "       applicantunkid " & _
                         "      ,ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') ='' THEN " & _
                         "                                  hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') " & _
                         "                  ELSE " & _
                         "                  ISNULL(CAST(YEAR(hremp_qualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(hremp_qualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') END,'') AS qualification " & _
                         "      ,ISNULL(hrresult_master.resultname,'') AS classification " & _
                         "      ,CASE WHEN hremp_qualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(hremp_qualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                         "      , ISNULL(hremp_qualification_tran.award_start_date,'') AS startdate " & _
                         "      , ISNULL(hremp_qualification_tran.award_end_date,'') AS ENDdate " & _
                         " FROM hremp_qualification_tran " & _
                         " JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
                         " JOIN hrqualification_master ON hrqualification_master.qualificationunkid=hremp_qualification_tran.qualificationunkid " & _
                         " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=hremp_qualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                         " LEFT JOIN hrresult_master ON hrresult_master.resultunkid=hremp_qualification_tran.resultunkid " & _
                         " WHERE hremp_qualification_tran.qualificationunkid > 0 AND hremp_qualification_tran.isvoid = 0 AND rcapplicant_master.employeeunkid > 0 "

            'Pinkal (20-Oct-2020) -- End


            dsPQualification = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            StrQ = ""
            StrQ = "SELECT " & _
                   " applicantunkid " & _
                   " , ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcjobhistory.joiningdate ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcjobhistory.terminationdate,112),'') ='' THEN " & _
                   " ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') " & _
                   " else " & _
                   " ISNULL(CAST(YEAR(rcjobhistory.joiningdate) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(rcjobhistory.terminationdate) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') END,'') AS Experience " & _
                   " ,ISNULL(rcjobhistory.joiningdate,'')  AS startdate " & _
                   ",ISNULL(rcjobhistory.terminationdate,'') AS ENDdate " & _
                   "FROM rcjobhistory WHERE 1 = 1 AND rcjobhistory.isvoid = 0 "

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> WHERE 1 = 1 AND rcjobhistory.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            'Pinkal (20-Oct-2020) -- Start
            'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
            StrQ &= " UNION " & _
                         " SELECT " & _
                         "        applicantunkid " & _
                         "      , ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),hremp_experience_tran.start_date ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),hremp_experience_tran.end_date,112),'') ='' THEN " & _
                         "                                      ISNULL(hremp_experience_tran.company ,'') " & _
                         "                             ELSE " & _
                         "        ISNULL(CAST(YEAR(hremp_experience_tran.start_date) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(hremp_experience_tran.end_date) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(hremp_experience_tran.company ,'') +' - '+ ISNULL(hremp_experience_tran.old_job,'') END,'') AS Experience " & _
                         "       ,ISNULL(hremp_experience_tran.start_date,'')  AS startdate " & _
                         "      ,ISNULL(hremp_experience_tran.end_date,'') AS ENDdate " & _
                         "  FROM hremp_experience_tran " & _
                         "  JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_experience_tran.employeeunkid " & _
                         "  WHERE 1 = 1 AND hremp_experience_tran.isvoid = 0 and rcapplicant_master.employeeunkid > 0 "
            'Pinkal (20-Oct-2020) -- End

            dsWorkExperience = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            StrQ = ""
            StrQ = "SELECT " & _
                   " applicantunkid " & _
                   ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                   "ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') " & _
                   "else " & _
                   "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') END,'') AS qualification " & _
                   ",ISNULL(other_resultcode,'') AS classification " & _
                   ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                   ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                   ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                   "FROM rcapplicantqualification_tran " & _
                   "WHERE rcapplicantqualification_tran.qualificationunkid <= 0 AND rcapplicantqualification_tran.isvoid = 0 "

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            'Pinkal (20-Oct-2020) -- Start
            'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
            StrQ &= " UNION " & _
                         "  SELECT " & _
                         "      applicantunkid " & _
                         "      ,ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112), '') = '' AND " & _
                         "                          ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112), '') = '' THEN hremp_qualification_tran.other_qualification + ' - ' + ISNULL(hremp_qualification_tran.other_institute, '') " & _
                         "                  ELSE ISNULL(CAST(YEAR(hremp_qualification_tran.award_start_date) AS NVARCHAR(10)), '') + ' - ' + ISNULL(CAST(YEAR(hremp_qualification_tran.award_end_date) AS NVARCHAR(10)), '') + '\r\n' + hremp_qualification_tran.other_qualification + ' - ' + ISNULL(hremp_qualification_tran.other_institute, '') END, '') AS qualification " & _
                         "      ,ISNULL(hremp_qualification_tran.other_resultcode, '') AS classification " & _
                         "      ,CASE WHEN hremp_qualification_tran.gpacode = 0 THEN '' ELSE ISNULL(CAST(hremp_qualification_tran.gpacode AS NVARCHAR(50)), '') END AS GPA " & _
                         "      ,ISNULL(hremp_qualification_tran.award_start_date, '') AS startdate " & _
                         "      ,ISNULL(hremp_qualification_tran.award_end_date, '') AS ENDdate " & _
                         "  FROM hremp_qualification_tran " & _
                         "  JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
                         " WHERE hremp_qualification_tran.qualificationunkid <= 0 AND hremp_qualification_tran.isvoid = 0 " & _
                         " AND rcapplicant_master.employeeunkid > 0 "


            'Pinkal (20-Oct-2020) -- End


            dsOtherShortCourses = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Rows As DataRow = Nothing
            Dim dtPQFilter As DataTable
            Dim dtWEFilter As DataTable
            Dim dtOQFilter As DataTable
            Dim blnIsAdded As Boolean = False
            Dim iColCnt As Integer = 1

            For Each dtRow As DataRow In dsListMain.Tables("DataTable").Rows
                dtPQFilter = New DataView(dsPQualification.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable
                dtWEFilter = New DataView(dsWorkExperience.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "startdate DESC", DataViewRowState.CurrentRows).ToTable
                dtOQFilter = New DataView(dsOtherShortCourses.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable

                Dim iPQCnt As Integer = dtPQFilter.Rows.Count
                Dim iWECnt As Integer = dtWEFilter.Rows.Count
                Dim iOQCnt As Integer = dtOQFilter.Rows.Count

                blnIsAdded = False

                If iPQCnt <= 0 AndAlso iWECnt <= 0 AndAlso iOQCnt <= 0 Then 'This is for  when professional Qualification, Experience and other Qualification /Short courses is blank
                    If blnIsAdded = False Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        rpt_Rows.Item("Column1") = dtRow.Item("code")
                        rpt_Rows.Item("Column2") = dtRow.Item("age")
                        rpt_Rows.Item("Column3") = dtRow.Item("gender")
                        rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column5") = ""
                        rpt_Rows.Item("Column6") = ""
                        rpt_Rows.Item("Column7") = ""
                        rpt_Rows.Item("Column8") = ""
                        rpt_Rows.Item("Column9") = ""
                        rpt_Rows.Item("Column10") = iColCnt.ToString
                        'Sohail (01 Nov 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                        '                                                            eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        If IsDBNull(dtRow.Item("vacancy")) = True OrElse IsDBNull(dtRow.Item("ODate")) = True OrElse IsDBNull(dtRow.Item("CDate")) = True Then
                            rpt_Rows.Item("Column11") = ""
                        Else
                            rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                                                                                        eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        End If
                        rpt_Rows.Item("Column81") = 1
                        'Sohail (01 Nov 2012) -- End

                        iColCnt += 1
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                        blnIsAdded = True

                        Continue For
                    End If
                End If

                If iPQCnt >= iWECnt AndAlso iPQCnt >= iOQCnt Then 'This is for professional Qualification is greater than Experience and other Qualification /Short courses
                    For i As Integer = 0 To dtPQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            rpt_Rows.Item("Column81") = 1 'Sohail (01 Nov 2012)
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                            rpt_Rows.Item("Column81") = 0 'Sohail (01 Nov 2012)
                        End If


                        rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                        rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")

                        If i < iWECnt AndAlso iWECnt > 0 Then 'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If

                        'Sohail (01 Nov 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                        '                                                           eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        If IsDBNull(dtRow.Item("vacancy")) = True OrElse IsDBNull(dtRow.Item("ODate")) = True OrElse IsDBNull(dtRow.Item("CDate")) = True Then
                            rpt_Rows.Item("Column11") = ""
                        Else
                            rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                                                                                       eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        End If
                        'Sohail (01 Nov 2012) -- End

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iWECnt >= iPQCnt AndAlso iWECnt >= iOQCnt Then 'This is for Experience  is greater than professional Qualification and other Qualification /Short courses
                    For i As Integer = 0 To dtWEFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            rpt_Rows.Item("Column81") = 1 'Sohail (01 Nov 2012)
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                            rpt_Rows.Item("Column81") = 0 'Sohail (01 Nov 2012)
                        End If

                        rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If

                        'Sohail (01 Nov 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                        '                                                           eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        If IsDBNull(dtRow.Item("vacancy")) = True OrElse IsDBNull(dtRow.Item("ODate")) = True OrElse IsDBNull(dtRow.Item("CDate")) = True Then
                            rpt_Rows.Item("Column11") = ""
                        Else
                            rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                                                                                       eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        End If
                        'Sohail (01 Nov 2012) -- End

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iOQCnt >= iPQCnt AndAlso iOQCnt >= iWECnt Then 'This is for other Qualification /Short courses is greater than professional Qualification and Experience 
                    For i As Integer = 0 To dtOQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            rpt_Rows.Item("Column81") = 1 'Sohail (01 Nov 2012)
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                            rpt_Rows.Item("Column81") = 0 'Sohail (01 Nov 2012)
                        End If

                        rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row.
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iWECnt AndAlso iWECnt > 0 Then  'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If

                        'Sohail (01 Nov 2012) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                        '                                                                         eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        If IsDBNull(dtRow.Item("vacancy")) = True OrElse IsDBNull(dtRow.Item("ODate")) = True OrElse IsDBNull(dtRow.Item("CDate")) = True Then
                            rpt_Rows.Item("Column11") = ""
                        Else
                            rpt_Rows.Item("Column11") = dtRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                                                                                                     eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                        End If
                        'Sohail (01 Nov 2012) -- End

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                End If
            Next

            objRpt = New ArutiReport.Designer.rptApplicantVacancyReport


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 4, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 7, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 8, "Sr No."))
            Call ReportFunction.TextChange(objRpt, "txtAppcode", Language.getMessage(mstrModuleName, 9, "App. Code"))

            Call ReportFunction.TextChange(objRpt, "txtAppAge", Language.getMessage(mstrModuleName, 10, "Age"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 11, "Gender"))
            Call ReportFunction.TextChange(objRpt, "txtNameAddressMobile", Language.getMessage(mstrModuleName, 12, "Name /Address /Mobile"))

            Call ReportFunction.TextChange(objRpt, "txtQualification", Language.getMessage(mstrModuleName, 13, "Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtResultCode", Language.getMessage(mstrModuleName, 14, "Classification"))

            Call ReportFunction.TextChange(objRpt, "txtGPA", Language.getMessage(mstrModuleName, 15, "GPA"))
            Call ReportFunction.TextChange(objRpt, "txtExperience", Language.getMessage(mstrModuleName, 16, "Experience"))
            Call ReportFunction.TextChange(objRpt, "txtOtherShortcourses", Language.getMessage(mstrModuleName, 17, "Other Qualification/Short Courses"))
            Call ReportFunction.TextChange(objRpt, "txtVacancy", Language.getMessage(mstrModuleName, 18, "Vacancy :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Applicant :")
            Language.setMessage(mstrModuleName, 2, "Vacancy Type :")
            Language.setMessage(mstrModuleName, 3, "Vacancy :")
            Language.setMessage(mstrModuleName, 4, "Prepared By :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Sr No.")
            Language.setMessage(mstrModuleName, 9, "App. Code")
            Language.setMessage(mstrModuleName, 10, "Age")
            Language.setMessage(mstrModuleName, 11, "Gender")
            Language.setMessage(mstrModuleName, 12, "Name /Address /Mobile")
            Language.setMessage(mstrModuleName, 13, "Qualification")
            Language.setMessage(mstrModuleName, 14, "Classification")
            Language.setMessage(mstrModuleName, 15, "GPA")
            Language.setMessage(mstrModuleName, 16, "Experience")
            Language.setMessage(mstrModuleName, 17, "Other Qualification/Short Courses")
            Language.setMessage(mstrModuleName, 18, "Vacancy :")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
