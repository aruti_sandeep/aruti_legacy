#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApplicantVacancyReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApplicantVacancyReport"
    Private objApplicantVacancyReport As clsApplicantVacancyReport

#End Region

#Region " Constructor "

    Public Sub New()
        objApplicantVacancyReport = New clsApplicantVacancyReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objApplicantVacancyReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objVacancy As New clsVacancy
        Dim objApplicantList As New clsApplicant_master
        Try
            dsCombo = objApplicantList.GetApplicantList("list", True, , True)
            With cboApplicant
                .ValueMember = "applicantunkid"
                .DisplayMember = "applicantname"
                .DataSource = dsCombo.Tables("list")
                .SelectedValue = 0
            End With

            dsCombo = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objVacancy = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboApplicant.SelectedIndex = 0
            cboVacancyType.SelectedIndex = 0
            cboVacancy.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objApplicantVacancyReport.SetDefaultValue()

            objApplicantVacancyReport._ApplicantID = cboApplicant.SelectedValue
            objApplicantVacancyReport._ApplicantName = cboApplicant.Text

            objApplicantVacancyReport._VacancyID = cboVacancy.SelectedValue
            objApplicantVacancyReport._VacancyName = cboVacancy.Text

            objApplicantVacancyReport._VacancyTypeID = cboVacancyType.SelectedValue
            objApplicantVacancyReport._VacancyTypeName = cboVacancyType.Text

            
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmApplicantVacancyReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objApplicantVacancyReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantVacancyReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantVacancyReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objApplicantVacancyReport._ReportName
            Me._Message = objApplicantVacancyReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantVacancyReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantVacancyReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmApplicantVacancyReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantVacancyReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmApplicantVacancyReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objApplicantVacancyReport.generateReport(0, e.Type, enExportAction.None)
            objApplicantVacancyReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        ConfigParameter._Object._UserAccessModeSetting, True, _
                                                        ConfigParameter._Object._ExportReportPath, _
                                                        ConfigParameter._Object._OpenAfterExport, _
                                                        0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objApplicantVacancyReport.generateReport(0, enPrintAction.None, e.Type)
            objApplicantVacancyReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        ConfigParameter._Object._UserAccessModeSetting, True, _
                                                        ConfigParameter._Object._ExportReportPath, _
                                                        ConfigParameter._Object._OpenAfterExport, _
                                                        0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboApplicant.ValueMember
                .DisplayMember = cboApplicant.DisplayMember
                .CodeMember = "applicant_code"
                .DataSource = cboApplicant.DataSource
            End With
            If frm.DisplayDialog Then
                cboApplicant.SelectedValue = frm.SelectedValue
                cboApplicant.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = cboVacancy.DataSource
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub



    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsApplicantVacancyReport.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicantVacancyReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Pinkal (03-Sep-2012) -- End


#End Region

#Region " CombBox Event "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsCombo As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objVacancy.getComboList(True, "List", -1, CInt(cboVacancyType.SelectedValue))
            dsCombo = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", -1, CInt(cboVacancyType.SelectedValue))
            'Shani(24-Aug-2015) -- End

            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LalblVacancy.Text = Language._Object.getCaption(Me.LalblVacancy.Name, Me.LalblVacancy.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
