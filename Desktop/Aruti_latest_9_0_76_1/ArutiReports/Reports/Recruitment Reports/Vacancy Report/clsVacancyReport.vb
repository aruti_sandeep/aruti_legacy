'************************************************************************************************************************************
'Class Name : clsVacancyReport.vb
'Purpose    :
'Date       :21/04/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region


''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsVacancyReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsVacancyReport"
    Private mstrReportId As String = enArutiReport.Vacancy_Report     '51
    Dim objDataOperation As clsDataOperation
    Dim dtFinalTable As DataTable

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnListReport()
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintVacancyId As Integer = 0
    Private mstrVacancyName As String = String.Empty
    Private mintEmplTypeId As Integer = 0
    Private mstrEmplTypeName As String = String.Empty
    Private mintVacJobId As Integer = 0
    Private mstrVacJob_Name As String = String.Empty
    Private mstrJobSkillName As String = String.Empty
    Private mintReportType_Id As Integer = 0
    Private mstrReportType_Name As String = String.Empty
    Private mintVac_InterviewType_Id As Integer = 0
    Private mstrVac_Interview_Name As String = String.Empty
    'Hemant (02 Jul 2019) -- Start
    'ENHANCEMENT :  ZRA minimum Requirements
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    'Hemant (02 Jul 2019) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _VacancyId() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

    Public WriteOnly Property _EmplTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmplTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplTypeName() As String
        Set(ByVal value As String)
            mstrEmplTypeName = value
        End Set
    End Property

    Public WriteOnly Property _VacJobId() As Integer
        Set(ByVal value As Integer)
            mintVacJobId = value
        End Set
    End Property

    Public WriteOnly Property _VacJob_Name() As String
        Set(ByVal value As String)
            mstrVacJob_Name = value
        End Set
    End Property

    Public WriteOnly Property _ReportType_Id() As Integer
        Set(ByVal value As Integer)
            mintReportType_Id = value
        End Set
    End Property

    Public WriteOnly Property _ReportType_Name() As String
        Set(ByVal value As String)
            mstrReportType_Name = value
        End Set
    End Property

    Public WriteOnly Property _VacInterviewTypeId() As Integer
        Set(ByVal value As Integer)
            mintVac_InterviewType_Id = value
        End Set
    End Property

    Public WriteOnly Property _VacInterviewTypeName() As String
        Set(ByVal value As String)
            mstrVac_Interview_Name = value
        End Set
    End Property

    'Hemant (02 Jul 2019) -- Start
    'ENHANCEMENT :  ZRA minimum Requirements
    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property
    'Hemant (02 Jul 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintVacancyId = 0
            mstrVacancyName = ""
            mintEmplTypeId = 0
            mstrEmplTypeName = ""
            mintVacJobId = 0
            mstrVacJob_Name = ""
            mstrJobSkillName = ""
            mintReportType_Id = 0
            mstrReportType_Name = ""
            mintVac_InterviewType_Id = 0
            mstrVac_Interview_Name = ""
            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            mintStatusId = 0
            mstrStatusName = ""
            'Hemant (02 Jul 2019) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Sub FilterTitleAndFilterQuery()
    Private Sub FilterTitleAndFilterQuery(ByVal xCompanyUnkId As Integer)
        'Shani(24-Aug-2015) -- End

        Me._FilterQuery = ""
        Me._FilterTitle = ""

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objCompany As New clsCompany_Master

        'Shani(24-Aug-2015) -- End

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objCompany._Companyunkid = xCompanyUnkId
            'Shani(24-Aug-2015) -- End

            If mintVacancyId > 0 Then
                objDataOperation.AddParameter("@VacancyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)
                Me._FilterQuery &= " AND rcvacancy_master.vacancytitle = @VacancyId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Vacancy : ") & " " & mstrVacancyName & " "
            End If

            Select Case mintReportType_Id
                Case 0 'LIST
                    If mintEmplTypeId > 0 Then
                        objDataOperation.AddParameter("@EmplTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmplTypeId)
                        Me._FilterQuery &= " AND EmplType.masterunkid = @EmplTypeId "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employment : ") & " " & mstrEmplTypeName & " "
                    End If
                    If mintVacJobId > 0 Then
                        objDataOperation.AddParameter("@VacJobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacJobId)
                        Me._FilterQuery &= " AND hrjob_master.jobunkid = @VacJobId "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Job : ") & " " & mstrVacJob_Name & " "
                    End If
                    'Hemant (02 Jul 2019) -- Start
                    'ENHANCEMENT :  ZRA minimum Requirements
                    If mintStatusId > 0 Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "Vacancy Status : ") & " " & mstrStatusName & " "
                    End If
                    'Hemant (02 Jul 2019) -- End
                Case 1 'DETAIL
                    objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Level"))

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                    'objDataOperation.AddParameter("@CompanyName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Company._Object._Name)
                    objDataOperation.AddParameter("@CompanyName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objCompany._Name)
                    'Shani(24-Aug-2015) -- End

                    If mintVac_InterviewType_Id > 0 Then
                        objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVac_InterviewType_Id)
                        Me._FilterQuery &= " AND interviewtypeunkid = @interviewtypeunkid "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Interview Type : ") & " " & mstrVac_Interview_Name & " "
                    End If
            End Select

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    Select Case pintReportType
        '        Case 0
        '            objRpt = Generate_ListReport()
        '        Case 1
        '            objRpt = Generate_DetailReport()
        '    End Select

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            Select Case pintReportType
                Case 0
                    'Gajanan [11-SEP-2019] -- Start      
                    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                    'objRpt = Generate_ListReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                    objRpt = Generate_VacancyPreviewReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                    'Gajanan [11-SEP-2019] -- End
                Case 1
                    objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End Select

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            Select Case intReportType
                Case 0     'LIST
                    OrderByDisplay = iColumn_ListReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_ListReport.ColumnItem(0).Name
                Case 1     'DETAIL
                    OrderByDisplay = iColumn_DetailReoirt.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_DetailReoirt.ColumnItem(0).Name
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Select Case intReportType
                Case 0     'LIST
                    Call OrderByExecute(iColumn_ListReport)
                Case 1     'DETAILS
                    Call OrderByExecute(iColumn_DetailReoirt)
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function GetReport_TypeList(Optional ByVal StrList As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @List AS NAME " & _
            "UNION SELECT 1 AS Id, @Detail AS NAME "

            objDataOperation.AddParameter("@List", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "List"))
            objDataOperation.AddParameter("@Detail", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "Detail"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReport_TypeList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_ListReport As New IColumnCollection
    Dim iColumn_DetailReoirt As New IColumnCollection

    Public Property Field_OnListReport() As IColumnCollection
        Get
            Return iColumn_ListReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_ListReport = value
        End Set
    End Property

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReoirt
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReoirt = value
        End Set
    End Property


    Private Sub Create_OnListReport()
        Try
            iColumn_ListReport.Clear()
            iColumn_ListReport.Add(New IColumn("VacTitle.name", Language.getMessage(mstrModuleName, 5, "Vancancy Title")))
            iColumn_ListReport.Add(New IColumn("ISNULL(EmplType.name,'')", Language.getMessage(mstrModuleName, 9, "Employment Type")))
            iColumn_ListReport.Add(New IColumn("ISNULL(hrjob_master.job_name,'')", Language.getMessage(mstrModuleName, 10, "Job")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReoirt.Clear()
            iColumn_DetailReoirt.Add(New IColumn("VacTitle.name ", Language.getMessage(mstrModuleName, 5, "Vancancy Title")))
            iColumn_DetailReoirt.Add(New IColumn("CASE WHEN interviewerunkid < 0 THEN ISNULL(otherinterviewer_name,'') " & _
                                                 " WHEN interviewerunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') END", Language.getMessage(mstrModuleName, 22, "Interviewer")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub



    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Function Generate_ListReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_ListReport(ByVal strDatabaseName As String, _
                                         ByVal intUserUnkid As Integer, _
                                         ByVal intYearUnkid As Integer, _
                                         ByVal intCompanyUnkid As Integer, _
                                         ByVal dtPeriodStart As Date, _
                                         ByVal dtPeriodEnd As Date, _
                                         ByVal strUserModeSetting As String, _
                                         ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End
        Dim dtCol As DataColumn 'Hemant (02 Jul 2019) 
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = intUserUnkid
            'Shani(24-Aug-2015) -- End

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '               " vacancyunkid AS VacId " & _
            '               ",vacancytitle AS VacTitle " & _
            '               ",CONVERT(CHAR(8),openingdate,112) AS VacOpDate " & _
            '               ",CONVERT(CHAR(8),closingdate,112) AS VacClDate " & _
            '               ",CONVERT(CHAR(8),interview_startdate,112) AS VacStDate " & _
            '               ",CONVERT(CHAR(8),interview_closedate,112) AS VacEdDate " & _
            '               ",ISNULL(noofposition,0) AS VacPosition " & _
            '               ",ISNULL(EmplType.name,'') AS VacEmplType " & _
            '               ",ISNULL(hrjob_master.job_name,'') AS VacJob " & _
            '               ",EmplType.masterunkid AS VacEmplTypeId " & _
            '               ",hrjob_master.jobunkid AS VacJobId " & _
            '            "FROM rcvacancy_master " & _
            '               "JOIN hrjob_master ON rcvacancy_master.jobunkid = hrjob_master.jobunkid " & _
            '               "JOIN cfcommon_master AS EmplType ON EmplType.masterunkid  = rcvacancy_master.employeementtypeunkid AND EmplType.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
            '            "WHERE ISNULL(rcvacancy_master.isvoid,0) = 0 "
            StrQ = "SELECT " & _
                          " vacancyunkid AS VacId " & _
                          ",VacTitle.name AS VacTitle " & _
                          ",CONVERT(CHAR(8),openingdate,112) AS VacOpDate " & _
                          ",CONVERT(CHAR(8),closingdate,112) AS VacClDate " & _
                          ",CONVERT(CHAR(8),interview_startdate,112) AS VacStDate " & _
                          ",CONVERT(CHAR(8),interview_closedate,112) AS VacEdDate " & _
                          ",ISNULL(rcvacancy_master.noofposition,0) AS VacPosition " & _
                          ",ISNULL(EmplType.name,'') AS VacEmplType " & _
                          ",ISNULL(hrjob_master.job_name,'') AS VacJob " & _
                          ",EmplType.masterunkid AS VacEmplTypeId " & _
                          ",hrjob_master.jobunkid AS VacJobId " & _
                          ",ISNULL(rcstaffrequisition_tran.form_statusunkid,0) AS form_statusunkid " & _
                       "FROM rcvacancy_master " & _
                          "JOIN hrjob_master ON rcvacancy_master.jobunkid = hrjob_master.jobunkid " & _
                          "JOIN cfcommon_master AS EmplType ON EmplType.masterunkid  = rcvacancy_master.employeementtypeunkid AND EmplType.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
                          "JOIN cfcommon_master AS VacTitle ON VacTitle.masterunkid  = rcvacancy_master.vacancytitle AND VacTitle.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
                          "LEFT JOIN rcstaffrequisition_tran ON rcvacancy_master.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid AND rcstaffrequisition_tran.isvoid = 0 " & _
                       "WHERE ISNULL(rcvacancy_master.isvoid,0) = 0 "

            'Hemant (02 Jul 2019) -- [form_statusunkid,LEFT JOIN rcstaffrequisition_tran ON rcvacancy_master.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid AND rcstaffrequisition_tran.isvoid = 0]
            'Anjan (02 Mar 2012)-End 



            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'Call FilterTitleAndFilterQuery()
            Call FilterTitleAndFilterQuery(intCompanyUnkid)
            'Shani(24-Aug-2015) -- End


            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            dtCol = New DataColumn("StatusId", System.Type.GetType("System.Int32"))
            dtCol.Caption = "StatusId"
            dtCol.DefaultValue = 0
            dsList.Tables(0).Columns.Add(dtCol)

            Dim dsFinalShortListVacancy As New DataSet

            StrQ = "SELECT  " & _
                    " DISTINCT vacancyunkid " & _
                    "FROM rcshortlist_master " & _
                    "WHERE isvoid = 0 " & _
                    "AND statustypid = " & enShortListing_Status.SC_APPROVED & _
                    "AND appr_date IS NOT NULL "

            dsFinalShortListVacancy = objDataOperation.ExecQuery(StrQ, "FinalShortListVacancy")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim strFinalShortListVacancyIds As String = ""

            strFinalShortListVacancyIds = String.Join(",", dsFinalShortListVacancy.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("vacancyunkid").ToString).ToArray())

            If strFinalShortListVacancyIds.Trim <> "" Then
                For Each drRow As DataRow In dsList.Tables(0).Select("vacid IN ( " & strFinalShortListVacancyIds & ")")
                    drRow.Item("StatusId") = enVacancyStatus.FILLED
                Next
            End If

            For Each drRow As DataRow In dsList.Tables(0).Select(" StatusId = 0 AND VacClDate < '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' ")
                drRow.Item("StatusId") = enVacancyStatus.CLOSED
            Next

            For Each drRow As DataRow In dsList.Tables(0).Select(" StatusId = 0 AND VacOpDate <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' AND VacClDate >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' ")
                drRow.Item("StatusId") = enVacancyStatus.OPEN
            Next

            For Each drRow As DataRow In dsList.Tables(0).Select(" StatusId = 0 AND (form_statusunkid = " & enApprovalStatus.APPROVED & " OR VacOpDate > '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "'  )")
                drRow.Item("StatusId") = enVacancyStatus.NOT_PUBLISHED
            Next

            If mintStatusId > 0 Then
                For Each dr As DataRow In dsList.Tables(0).Select("StatusId <> " & mintStatusId & "")
                    dr.Delete()
                Next

            End If

            dsList.Tables(0).AcceptChanges()
            'Hemant (02 Jul 2019) -- End

            Dim dsSkills As New DataSet

            StrQ = "SELECT " & _
                        "  hrjob_master.jobunkid AS JobId " & _
                        " ,ISNULL(hrskill_master.skillname,'') AS JSkill " & _
                        "FROM hrjob_master " & _
                        "  LEFT JOIN hrjob_skill_tran ON hrjob_master.jobunkid = hrjob_skill_tran.jobunkid " & _
                        "  LEFT JOIN hrskill_master ON hrjob_skill_tran.skillunkid = hrskill_master.skillunkid " & _
                        "WHERE hrjob_master.isactive = 1 "

            If mintVacJobId > 0 Then
                StrQ &= " AND hrjob_master.jobunkid = " & mintVacJobId
            End If

            dsSkills = objDataOperation.ExecQuery(StrQ, "Skills")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("VacTitle")
                rpt_Row.Item("Column2") = eZeeDate.convertDate(dtRow.Item("VacOpDate").ToString).ToShortDateString & vbCrLf & eZeeDate.convertDate(dtRow.Item("VacClDate").ToString).ToShortDateString
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("VacStDate").ToString).ToShortDateString & vbCrLf & eZeeDate.convertDate(dtRow.Item("VacEdDate").ToString).ToShortDateString
                rpt_Row.Item("Column4") = dtRow.Item("VacPosition")
                rpt_Row.Item("Column5") = dtRow.Item("VacEmplType")
                rpt_Row.Item("Column6") = dtRow.Item("VacJob")

                Dim dtTemp() As DataRow = dsSkills.Tables("Skills").Select("JobId = '" & dtRow.Item("VacJobId") & "'")
                mstrJobSkillName = ""
                For i As Integer = 0 To dtTemp.Length - 1
                    mstrJobSkillName &= "," & dtTemp(i)("JSkill").ToString
                Next
                If mstrJobSkillName.Length > 0 Then
                    mstrJobSkillName = Mid(mstrJobSkillName, 2)
                End If

                rpt_Row.Item("Column7") = mstrJobSkillName

                'Hemant (02 Jul 2019) -- Start
                'ENHANCEMENT :  ZRA minimum Requirements
                Select Case CInt(dtRow.Item("StatusId"))
                    Case enVacancyStatus.NOT_PUBLISHED
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 910, "Not Published")
                    Case enVacancyStatus.OPEN
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 911, "Open")
                    Case enVacancyStatus.CLOSED
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 912, "Closed")
                    Case enVacancyStatus.FILLED
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 913, "Filled")
                End Select
                'Hemant (02 Jul 2019) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptVacancyListReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtVacancytitle", Language.getMessage(mstrModuleName, 5, "Vancancy Title"))
            Call ReportFunction.TextChange(objRpt, "txtvacancyDate", Language.getMessage(mstrModuleName, 6, "Vacancy Start & End Date"))
            Call ReportFunction.TextChange(objRpt, "txtInterviewDate", Language.getMessage(mstrModuleName, 7, "Interview Start & End Date"))
            Call ReportFunction.TextChange(objRpt, "txtPostion", Language.getMessage(mstrModuleName, 8, "Postions"))
            Call ReportFunction.TextChange(objRpt, "txtEmploymentType", Language.getMessage(mstrModuleName, 9, "Employment Type"))
            Call ReportFunction.TextChange(objRpt, "txtJobName", Language.getMessage(mstrModuleName, 10, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtJobSkills", Language.getMessage(mstrModuleName, 11, "Skill Required"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " -[ " & mstrReportType_Name & " ]")
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 34, "Vacancy Status :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 35, "Total :"))
            'Hemant (02 Jul 2019) -- End

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ListReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = intUserUnkid
            'Shani(24-Aug-2015) -- End

            'StrQ = "SELECT " & _
            '                     "rcvacancy_master.vacancyunkid AS VacId " & _
            '                    ",rcvacancy_master.vacancytitle AS VacTitle " & _
            '                    ",CASE WHEN interviewerunkid < 0 THEN ISNULL(otherinterviewer_name,'') " & _
            '                          "WHEN interviewerunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
            '                     "END AS Interviewer " & _
            '                    ",@Level+' '+CAST(ISNULL(interviewer_level,0) AS NVARCHAR(50)) AS ILevel " & _
            '                    ",CASE WHEN interviewerunkid < 0 THEN ISNULL(otherdepartment,'') " & _
            '                          "WHEN interviewerunkid > 0 THEN ISNULL(hrdepartment_master.name,'') " & _
            '                     "END AS Department " & _
            '                    ",CASE WHEN interviewerunkid < 0 THEN ISNULL(othercontact_no,'') " & _
            '                          "WHEN interviewerunkid > 0 THEN ISNULL(hremployee_master.present_tel_no,'') " & _
            '                     "END AS ContactNo " & _
            '                    ",CASE WHEN interviewerunkid < 0 THEN ISNULL(othercompany,'') " & _
            '                          "WHEN interviewerunkid > 0 THEN @CompanyName " & _
            '                     "END AS CompanyName " & _
            '                    ",CONVERT(CHAR(8),openingdate,112) AS VacOpDate " & _
            '                    ",CONVERT(CHAR(8),closingdate,112) AS VacClDate " & _
            '                    ",CONVERT(CHAR(8),interview_startdate,112) AS VacStDate " & _
            '                    ",CONVERT(CHAR(8),interview_closedate,112) AS VacEdDate " & _
            '                    ",interviewtypeunkid AS InterviewTypeId " & _
            '                    ",ISNULL(cfcommon_master.name,'') AS IntType " & _
            '            "FROM rcinterviewer_tran " & _
            '                "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcinterviewer_tran.interviewtypeunkid AND mastertype = " & clsCommon_Master.enCommonMaster.INTERVIEW_TYPE & "   " & _
            '                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcinterviewer_tran.interviewerunkid " & _
            '                "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
            '                "JOIN rcvacancy_master ON rcinterviewer_tran.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '            "WHERE ISNULL(rcinterviewer_tran.isvoid,0) = 0 AND ISNULL(rcvacancy_master.isvoid,0) = 0 "

            StrQ = "SELECT " & _
                                 "rcvacancy_master.vacancyunkid AS VacId " & _
                                ",VacTitle.name AS VacTitle " & _
                                ",CASE WHEN interviewerunkid < 0 THEN ISNULL(otherinterviewer_name,'') " & _
                                      "WHEN interviewerunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                                 "END AS Interviewer " & _
                                ",@Level+' '+CAST(ISNULL(interviewer_level,0) AS NVARCHAR(50)) AS ILevel " & _
                                ",CASE WHEN interviewerunkid < 0 THEN ISNULL(otherdepartment,'') " & _
                                      "WHEN interviewerunkid > 0 THEN ISNULL(hrdepartment_master.name,'') " & _
                                 "END AS Department " & _
                                ",CASE WHEN interviewerunkid < 0 THEN ISNULL(othercontact_no,'') " & _
                                      "WHEN interviewerunkid > 0 THEN ISNULL(hremployee_master.present_tel_no,'') " & _
                                 "END AS ContactNo " & _
                                ",CASE WHEN interviewerunkid < 0 THEN ISNULL(othercompany,'') " & _
                                      "WHEN interviewerunkid > 0 THEN @CompanyName " & _
                                 "END AS CompanyName " & _
                                ",CONVERT(CHAR(8),openingdate,112) AS VacOpDate " & _
                                ",CONVERT(CHAR(8),closingdate,112) AS VacClDate " & _
                                ",CONVERT(CHAR(8),interview_startdate,112) AS VacStDate " & _
                                ",CONVERT(CHAR(8),interview_closedate,112) AS VacEdDate " & _
                                ",interviewtypeunkid AS InterviewTypeId " & _
                                ",ISNULL(cfcommon_master.name,'') AS IntType " & _
                        "FROM rcbatchschedule_interviewer_tran " & _
                            "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_interviewer_tran.interviewtypeunkid AND mastertype = " & clsCommon_Master.enCommonMaster.INTERVIEW_TYPE & "   " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid " & _
                            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                            "JOIN rcvacancy_master ON rcbatchschedule_interviewer_tran.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                            "JOIN cfcommon_master AS VacTitle ON VacTitle.masterunkid = rcvacancy_master.vacancytitle  " & _
                        "WHERE ISNULL(rcbatchschedule_interviewer_tran.isvoid,0) = 0 AND ISNULL(rcvacancy_master.isvoid,0) = 0 "
            'Hemant (30 Oct 2019) -- [rcinterviewer_tran --> rcbatchschedule_interviewer_tran ]

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'Call FilterTitleAndFilterQuery()
            Call FilterTitleAndFilterQuery(intCompanyUnkid)
            'Shani(24-Aug-2015) -- End


            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("VacId")
                rpt_Row.Item("Column2") = dtRow.Item("VacTitle")
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("VacOpDate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dtRow.Item("VacClDate").ToString).ToShortDateString
                rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("VacStDate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dtRow.Item("VacEdDate").ToString).ToShortDateString
                rpt_Row.Item("Column5") = dtRow.Item("IntType")
                rpt_Row.Item("Column6") = dtRow.Item("Interviewer")
                rpt_Row.Item("Column7") = dtRow.Item("ILevel")
                rpt_Row.Item("Column8") = dtRow.Item("Department")
                rpt_Row.Item("Column9") = dtRow.Item("CompanyName")
                rpt_Row.Item("Column10") = dtRow.Item("ContactNo")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptVacancyDetailReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtVacancytitle", Language.getMessage(mstrModuleName, 23, "Vacancy Title :"))
            Call ReportFunction.TextChange(objRpt, "txtvacancyDate", Language.getMessage(mstrModuleName, 24, "Vacancy Start & End Date :"))
            Call ReportFunction.TextChange(objRpt, "txtInterviewDate", Language.getMessage(mstrModuleName, 25, "Interview Start & End Date :"))

            Call ReportFunction.TextChange(objRpt, "txtInterviewType", Language.getMessage(mstrModuleName, 26, "Interview Type"))
            Call ReportFunction.TextChange(objRpt, "txtInterviewer", Language.getMessage(mstrModuleName, 27, "Interviewer Name"))
            Call ReportFunction.TextChange(objRpt, "txtLevel", Language.getMessage(mstrModuleName, 28, "Level"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 29, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtCompany", Language.getMessage(mstrModuleName, 30, "Company Name"))
            Call ReportFunction.TextChange(objRpt, "txtContactNo", Language.getMessage(mstrModuleName, 31, "Contact No."))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " -[ " & mstrReportType_Name & " ]")
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Gajanan [11-SEP-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    Private Function Generate_VacancyPreviewReport(ByVal strDatabaseName As String, _
                                         ByVal intUserUnkid As Integer, _
                                         ByVal intYearUnkid As Integer, _
                                         ByVal intCompanyUnkid As Integer, _
                                         ByVal dtPeriodStart As Date, _
                                         ByVal dtPeriodEnd As Date, _
                                         ByVal strUserModeSetting As String, _
                                         ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass


        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim objUser As New clsUserAddEdit
        Dim dtCol As DataColumn
        Try
            objDataOperation = New clsDataOperation


            objUser._Userunkid = intUserUnkid


            StrQ &= "SELECT " & _
                    "t1.jobunkid " & _
                    ",STUFF((SELECT " & _
                          "', ' + hrskill_master.skillname " & _
                          "FROM hrjob_skill_tran t2 " & _
                          "JOIN hrskill_master " & _
                               "ON t2.skillunkid = hrskill_master.skillunkid " & _
                          "WHERE t1.jobunkid = t2.jobunkid "

            If mintVacJobId > 0 Then
                StrQ &= " AND t1.jobunkid = " & mintVacJobId
            End If

            StrQ &= "AND t2.isactive = 1 " & _
                          "FOR " & _
                          "XML PATH ('')) " & _
                    ", 1, 1, '') Skill INTO #Vac_Skill " & _
                    "FROM hrjob_skill_tran t1 " & _
                    "JOIN hrskill_master " & _
                         "ON t1.skillunkid = hrskill_master.skillunkid " & _
                    "WHERE t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid "

            StrQ &= "SELECT " & _
                    "t1.jobunkid " & _
                    ",STUFF((SELECT " & _
                              "', ' " & _
                              "+ hrqualification_master.qualificationname " & _
                              "FROM hrjob_qualification_tran t2 " & _
                              "JOIN hrqualification_master " & _
                                   "ON t2.qualificationunkid = hrqualification_master.qualificationunkid " & _
                              "WHERE t1.jobunkid = t2.jobunkid "

            If mintVacJobId > 0 Then
                StrQ &= " AND t1.jobunkid = " & mintVacJobId
            End If

            StrQ &= "AND t2.isactive = 1 " & _
                              "FOR " & _
                              "XML PATH ('')) " & _
                         ", 1, 1, '') Qualification INTO #Vac_Quali " & _
                    "FROM hrjob_qualification_tran t1 " & _
                    "JOIN hrqualification_master " & _
                         "ON t1.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    "WHERE t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid "

            StrQ &= "SELECT " & _
                          " vacancyunkid AS VacId " & _
                          ",VacTitle.name AS VacTitle " & _
                          ",CONVERT(CHAR(8),openingdate,112) AS VacOpDate " & _
                          ",CONVERT(CHAR(8),closingdate,112) AS VacClDate " & _
                          ",CONVERT(CHAR(8),interview_startdate,112) AS VacStDate " & _
                          ",CONVERT(CHAR(8),interview_closedate,112) AS VacEdDate " & _
                          ",ISNULL(rcvacancy_master.noofposition,0) AS VacPosition " & _
                          ",ISNULL(EmplType.name,'') AS VacEmplType " & _
                          ",ISNULL(hrjob_master.job_name,'') AS VacJob " & _
                          ",EmplType.masterunkid AS VacEmplTypeId " & _
                          ",hrjob_master.jobunkid AS VacJobId " & _
                          ",ISNULL(rcstaffrequisition_tran.form_statusunkid,0) AS form_statusunkid " & _
                          ",experience " & _
                          ",ISNULL(Vac_Skill.Skill, '') AS skill " & _
                          ",ISNULL(Vac_Quali.Qualification, '') AS Qualification " & _
                          ",remark AS JobDescription " & _
                          ",responsibilities_duties AS Responsibility " & _
                       "FROM rcvacancy_master " & _
                          "JOIN hrjob_master ON rcvacancy_master.jobunkid = hrjob_master.jobunkid " & _
                          "JOIN cfcommon_master AS EmplType ON EmplType.masterunkid  = rcvacancy_master.employeementtypeunkid AND EmplType.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
                          "JOIN cfcommon_master AS VacTitle ON VacTitle.masterunkid  = rcvacancy_master.vacancytitle AND VacTitle.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
                          "LEFT JOIN rcstaffrequisition_tran ON rcvacancy_master.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid AND rcstaffrequisition_tran.isvoid = 0 " & _
                          "LEFT JOIN #Vac_Skill AS Vac_Skill ON rcvacancy_master.jobunkid = Vac_Skill.jobunkid " & _
                          "LEFT JOIN #Vac_Quali AS Vac_Quali ON rcvacancy_master.jobunkid = Vac_Quali.jobunkid " & _
                       "WHERE ISNULL(rcvacancy_master.isvoid,0) = 0 "

            Call FilterTitleAndFilterQuery(intCompanyUnkid)

            StrQ &= Me._FilterQuery

            StrQ &= " DROP TABLE #Vac_Skill " & _
                    "DROP TABLE #Vac_Quali "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            dtCol = New DataColumn("StatusId", System.Type.GetType("System.Int32"))
            dtCol.Caption = "StatusId"
            dtCol.DefaultValue = 0
            dsList.Tables(0).Columns.Add(dtCol)

            Dim dsFinalShortListVacancy As New DataSet

            StrQ = "SELECT  " & _
                    " DISTINCT vacancyunkid " & _
                    "FROM rcshortlist_master " & _
                    "WHERE isvoid = 0 " & _
                    "AND statustypid = " & enShortListing_Status.SC_APPROVED & _
                    "AND appr_date IS NOT NULL "

            dsFinalShortListVacancy = objDataOperation.ExecQuery(StrQ, "FinalShortListVacancy")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim strFinalShortListVacancyIds As String = ""

            strFinalShortListVacancyIds = String.Join(",", dsFinalShortListVacancy.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("vacancyunkid").ToString).ToArray())

            If strFinalShortListVacancyIds.Trim <> "" Then
                For Each drRow As DataRow In dsList.Tables(0).Select("vacid IN ( " & strFinalShortListVacancyIds & ")")
                    drRow.Item("StatusId") = enVacancyStatus.FILLED
                Next
            End If

            For Each drRow As DataRow In dsList.Tables(0).Select(" StatusId = 0 AND VacClDate < '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' ")
                drRow.Item("StatusId") = enVacancyStatus.CLOSED
            Next

            For Each drRow As DataRow In dsList.Tables(0).Select(" StatusId = 0 AND VacOpDate <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' AND VacClDate >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' ")
                drRow.Item("StatusId") = enVacancyStatus.OPEN
            Next

            For Each drRow As DataRow In dsList.Tables(0).Select(" StatusId = 0 AND (form_statusunkid = " & enApprovalStatus.APPROVED & " OR VacOpDate > '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "'  )")
                drRow.Item("StatusId") = enVacancyStatus.NOT_PUBLISHED
            Next

            If mintStatusId > 0 Then
                For Each dr As DataRow In dsList.Tables(0).Select("StatusId <> " & mintStatusId & "")
                    dr.Delete()
                Next

            End If

            dsList.Tables(0).AcceptChanges()


            Dim dsSkills As New DataSet
            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column13") = dtRow.Item("VacTitle")
                rpt_Row.Item("Column11") = dtRow.Item("VacEmplType")
                rpt_Row.Item("Column12") = dtRow.Item("VacJob")
                rpt_Row.Item("Column2") = IIf(CInt(dtRow.Item("experience")) <= 0, Language.getMessage(mstrModuleName, 46, " Fresher Can Apply"), CInt(dtRow.Item("experience")))
                rpt_Row.Item("Column4") = dtRow.Item("Skill")
                rpt_Row.Item("Column5") = dtRow.Item("Qualification")
                rpt_Row.Item("Column6") = eZeeDate.convertDate(dtRow.Item("VacOpDate").ToString).ToShortDateString
                rpt_Row.Item("Column7") = eZeeDate.convertDate(dtRow.Item("VacClDate").ToString).ToShortDateString
                rpt_Row.Item("Column9") = dtRow.Item("JobDescription")
                rpt_Row.Item("Column3") = dtRow.Item("VacPosition")
                rpt_Row.Item("Column10") = dtRow.Item("Responsibility")

                Select Case CInt(dtRow.Item("StatusId"))
                    Case enVacancyStatus.NOT_PUBLISHED
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 910, "Not Published")
                    Case enVacancyStatus.OPEN
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 911, "Open")
                    Case enVacancyStatus.CLOSED
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 912, "Closed")
                    Case enVacancyStatus.FILLED
                        rpt_Row.Item("Column8") = Language.getMessage("clsMasterData", 913, "Filled")
                End Select

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptVacancyPreviewReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtEmploymentType", Language.getMessage(mstrModuleName, 44, "Employment Type :"))
            Call ReportFunction.TextChange(objRpt, "txtJobName", Language.getMessage(mstrModuleName, 45, "Job :"))
            Call ReportFunction.TextChange(objRpt, "txtExperience", Language.getMessage(mstrModuleName, 36, "Experience :"))
            Call ReportFunction.TextChange(objRpt, "txtNoOfPosition", Language.getMessage(mstrModuleName, 37, "No. of Position :"))
            Call ReportFunction.TextChange(objRpt, "txtSkill", Language.getMessage(mstrModuleName, 38, "Skill :"))
            Call ReportFunction.TextChange(objRpt, "txtQualificationRequired", Language.getMessage(mstrModuleName, 39, "Qualification Required :"))
            Call ReportFunction.TextChange(objRpt, "txtJobOpeningdate", Language.getMessage(mstrModuleName, 40, "Job Opening date :"))
            Call ReportFunction.TextChange(objRpt, "txtJobClosingdate", Language.getMessage(mstrModuleName, 41, "Job closing date :"))
            Call ReportFunction.TextChange(objRpt, "txtJobDescription", Language.getMessage(mstrModuleName, 42, "Job Description :"))
            Call ReportFunction.TextChange(objRpt, "txtResponsibility", Language.getMessage(mstrModuleName, 43, "Responsibility :"))
            Call ReportFunction.TextChange(objRpt, "txtVacancyType", Language.getMessage(mstrModuleName, 34, "Vacancy Status :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " -[ " & mstrReportType_Name & " ]")
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_VacancyPreviewReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Gajanan [11-SEP-2019] -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Vancancy Title")
            Language.setMessage(mstrModuleName, 6, "Vacancy Start & End Date")
            Language.setMessage(mstrModuleName, 7, "Interview Start & End Date")
            Language.setMessage(mstrModuleName, 8, "Postions")
            Language.setMessage(mstrModuleName, 9, "Employment Type")
            Language.setMessage(mstrModuleName, 10, "Job")
            Language.setMessage(mstrModuleName, 11, "Skill Required")
            Language.setMessage(mstrModuleName, 12, "Printed By :")
            Language.setMessage(mstrModuleName, 13, "Printed Date :")
            Language.setMessage(mstrModuleName, 14, "Page :")
            Language.setMessage(mstrModuleName, 15, "Vacancy :")
            Language.setMessage(mstrModuleName, 16, "Employment :")
            Language.setMessage(mstrModuleName, 17, "Job :")
            Language.setMessage(mstrModuleName, 18, "Order By :")
            Language.setMessage(mstrModuleName, 19, "Level")
            Language.setMessage(mstrModuleName, 20, "Interview Type :")
            Language.setMessage(mstrModuleName, 22, "Interviewer")
            Language.setMessage(mstrModuleName, 23, "Vacancy Title :")
            Language.setMessage(mstrModuleName, 24, "Vacancy Start & End Date :")
            Language.setMessage(mstrModuleName, 25, "Interview Start & End Date :")
            Language.setMessage(mstrModuleName, 26, "Interview Type")
            Language.setMessage(mstrModuleName, 27, "Interviewer Name")
            Language.setMessage(mstrModuleName, 28, "Level")
            Language.setMessage(mstrModuleName, 29, "Department")
            Language.setMessage(mstrModuleName, 30, "Company Name")
            Language.setMessage(mstrModuleName, 31, "Contact No.")
            Language.setMessage(mstrModuleName, 32, "List")
            Language.setMessage(mstrModuleName, 33, "Detail")
            Language.setMessage(mstrModuleName, 34, "Vacancy Status :")
            Language.setMessage(mstrModuleName, 35, "Total :")
            Language.setMessage(mstrModuleName, 36, "Experience :")
            Language.setMessage(mstrModuleName, 37, "No. of Position :")
            Language.setMessage(mstrModuleName, 38, "Skill :")
            Language.setMessage(mstrModuleName, 39, "Qualification Required :")
            Language.setMessage(mstrModuleName, 40, "Job Opening date :")
            Language.setMessage(mstrModuleName, 41, "Job closing date :")
            Language.setMessage(mstrModuleName, 42, "Job Description :")
            Language.setMessage(mstrModuleName, 43, "Responsibility :")
            Language.setMessage(mstrModuleName, 44, "Employment Type :")
            Language.setMessage(mstrModuleName, 45, "Job :")
            Language.setMessage("clsMasterData", 910, "Not Published")
            Language.setMessage("clsMasterData", 911, "Open")
            Language.setMessage("clsMasterData", 912, "Closed")
            Language.setMessage("clsMasterData", 913, "Filled")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
