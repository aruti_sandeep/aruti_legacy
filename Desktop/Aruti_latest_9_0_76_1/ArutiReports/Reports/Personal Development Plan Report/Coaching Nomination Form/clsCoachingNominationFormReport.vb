﻿'************************************************************************************************************************************
'Class Name : clsCoachingNominationFormReport.vb
'Purpose    :
'Date       : 09/10/2023
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsCoachingNominationFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCoachingNominationFormReport"
    Private mstrReportId As String = enArutiReport.Coaching_Nomination_Form_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintCompanyUnkid As Integer = -1
    Private mintFormTypeId As Integer
    Private mstrUserName As String = ""
    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _FormTypeId() As Integer
        Set(ByVal value As Integer)
            mintFormTypeId = value
        End Set
    End Property

    Public WriteOnly Property _User() As String
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Private mintCoachingNominationunkid As Integer
    Public Property _CoachingNominationunkid() As Integer
        Get
            Return mintCoachingNominationunkid
        End Get
        Set(ByVal value As Integer)
            mintCoachingNominationunkid = value
        End Set
    End Property
#End Region

#Region "Public Function & Procedures "

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCoachingNominationunkid > 0 Then
                objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid)
                Me._FilterQuery &= " AND pdpcoaching_nomination_form.coachingnominationunkid = @coachingnominationunkid "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid

            Dim mintCountryId As Integer = Company._Object._Countryunkid

            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                         , ByVal xUserUnkid As Integer _
                                         , ByVal xYearUnkid As Integer _
                                         , ByVal xCompanyUnkid As Integer _
                                         , ByVal mdtEmployeeAsonDate As Date _
                                         ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
      
        
        Try
            objDataOperation = New clsDataOperation

            If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                Dim dsCoachDetails As New DataSet
                Dim dsCoacheeDetails As New DataSet
                Dim dsApprovalDetails As New DataSet

                Dim rpt_Data As ArutiReport.Designer.dsArutiReport
                Dim rpt_CoachDetails As ArutiReport.Designer.dsArutiReport
                Dim rpt_CoacheeDetails As ArutiReport.Designer.dsArutiReport
                Dim rpt_ApprovalDetails As ArutiReport.Designer.dsArutiReport

                StrQ = " SELECT	coach_employeeunkid " & _
                       " ,      ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') coachname " & _
                       " ,      hrjob_master.job_name AS Designation " & _
                       " ,      hrclasses_master.name AS WorkStatation " & _
                       " ,      pdpcoaching_nomination_form.coach_work_process AS WorkProcess " & _
                       " ,      CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Gender " & _
                       " FROM pdpcoaching_nomination_form " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid " & _
                       " JOIN " & _
                       " ( " & _
                       "         SELECT " & _
                       "         jobunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_categorization_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                       " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                       " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                       " JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         employeeunkid " & _
                       "        ,ISNULL(classunkid, 0) AS classunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_transfer_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                       " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " LEFT JOIN hrclasses_master ON Alloc.classunkid = hrclasses_master.classesunkid " & _
                       " WHERE pdpcoaching_nomination_form.isvoid = 0 "

                objDataOperation.ClearParameters()

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= mstrOrderByQuery

                objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                dsCoachDetails = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                StrQ = " SELECT	coachee_employeeunkid " & _
                      " ,      ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') coacheename " & _
                      " ,      hrjob_master.job_name AS Designation " & _
                      " ,      hrdepartment_master.name AS Department " & _
                      " ,      hrclasses_master.name AS WorkStatation " & _
                      " ,      pdpcoaching_nomination_form.coachee_work_process AS WorkProcess " & _
                      " ,      CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Gender " & _
                      " FROM pdpcoaching_nomination_form " & _
                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pdpcoaching_nomination_form.coachee_employeeunkid " & _
                      " JOIN " & _
                      " ( " & _
                      "         SELECT " & _
                      "         jobunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                      " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                      " JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         employeeunkid " & _
                      "        ,ISNULL(departmentunkid, 0) AS departmentunkid " & _
                      "        ,ISNULL(classunkid, 0) AS classunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_transfer_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                      " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " LEFT JOIN hrdepartment_master ON Alloc.departmentunkid = hrdepartment_master.departmentunkid " & _
                      " LEFT JOIN hrclasses_master ON Alloc.classunkid = hrclasses_master.classesunkid " & _
                       " WHERE pdpcoaching_nomination_form.isvoid = 0 "

                objDataOperation.ClearParameters()

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= mstrOrderByQuery

                objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                dsCoacheeDetails = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                StrQ = " SELECT	pdpcoaching_approverlevel_master.levelunkid " & _
                      " ,      pdpcoaching_approverlevel_master.levelname " & _
                      " ,      pdpcoaching_approverlevel_master.priority " & _
                      " ,      pdpcoachingapproval_process_tran.mapuserunkid " & _
                      " ,      ISNULL(UEmp.firstname, '') + ' ' + ISNULL(UEmp.lastname, '') AS ApproverName " & _
                      " ,      pdpcoachingapproval_process_tran.remark " & _
                      " ,      pdpcoachingapproval_process_tran.statusunkid " & _
                      " ,      CASE WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.PENDING & " THEN  @PENDING " & _
                      "             WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.APPROVED & " THEN  @APPROVED " & _
                      "             WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.REJECTED & " THEN  @REJECTED " & _
                      "        END status	 " & _
                      " FROM pdpcoachingapproval_process_tran " & _
                      " JOIN pdpcoaching_nomination_form ON pdpcoaching_nomination_form.coachingnominationunkid = pdpcoachingapproval_process_tran.coachingnominationunkid	AND pdpcoaching_nomination_form.isvoid = 0 " & _
                      " JOIN pdpcoaching_role_mapping ON pdpcoachingapproval_process_tran.approvertranunkid = pdpcoaching_role_mapping.mappingunkid	AND pdpcoaching_role_mapping.isvoid = 0 " & _
                      " JOIN pdpcoaching_approverlevel_master ON pdpcoaching_role_mapping.levelunkid = pdpcoaching_approverlevel_master.levelunkid AND pdpcoaching_role_mapping.isvoid = 0 " & _
                      " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = pdpcoachingapproval_process_tran.mapuserunkid " & _
                       " WHERE pdpcoachingapproval_process_tran.isvoid = 0 "

                objDataOperation.ClearParameters()

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= mstrOrderByQuery

                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 2, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 3, "Approved"))
                objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 4, "Rejected"))

                dsApprovalDetails = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                Dim dt As New DataTable
                dt = dsApprovalDetails.Tables(0).Copy()

                Dim mintPriorty As Integer = -1

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
Recalculate:
                    Dim mintPrioriry As Integer = 0
                    For i As Integer = 0 To dt.Rows.Count - 1

                        If mintPrioriry <> CInt(dt.Rows(i)("priority")) Then
                            mintPrioriry = CInt(dt.Rows(i)("priority"))
                            Dim drRow() As DataRow = dt.Select("statusunkid <> 1 AND priority  = " & mintPrioriry)
                            If drRow.Length > 0 Then
                                Dim drPending() As DataRow = dt.Select("statusunkid = 1 AND priority  = " & mintPrioriry)
                                If drPending.Length > 0 Then
                                    Dim drPendingList() As DataRow = dsApprovalDetails.Tables(0).Select(" statusunkid = 1 AND priority  = " & mintPrioriry)
                                    If drPendingList.Length > 0 Then
                                        For Each dRowList As DataRow In drPendingList
                                            dsApprovalDetails.Tables(0).Rows.Remove(dRowList)
                                        Next
                                        dsApprovalDetails.Tables(0).AcceptChanges()
                                    End If
                                    For Each dRow As DataRow In drPending
                                        dt.Rows.Remove(dRow)
                                        GoTo Recalculate
                                    Next
                                    dt.AcceptChanges()
                                End If
                            End If


                        End If

                    Next
                End If

                rpt_Data = New ArutiReport.Designer.dsArutiReport
                rpt_CoachDetails = New ArutiReport.Designer.dsArutiReport
                rpt_CoacheeDetails = New ArutiReport.Designer.dsArutiReport
                rpt_ApprovalDetails = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsCoachDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_CoachDetails.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("coachname")
                    rpt_Row.Item("Column2") = dtRow.Item("Designation")
                    rpt_Row.Item("Column3") = dtRow.Item("WorkStatation")
                    rpt_Row.Item("Column4") = dtRow.Item("WorkProcess")
                    rpt_Row.Item("Column5") = dtRow.Item("Gender")
                    rpt_CoachDetails.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                For Each dtRow As DataRow In dsCoacheeDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_CoacheeDetails.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("coacheename")
                    rpt_Row.Item("Column2") = dtRow.Item("Designation")
                    rpt_Row.Item("Column3") = dtRow.Item("Department")
                    rpt_Row.Item("Column4") = dtRow.Item("WorkProcess")
                    rpt_Row.Item("Column5") = dtRow.Item("Gender")
                    rpt_CoacheeDetails.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                For Each dtRow As DataRow In dsApprovalDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_ApprovalDetails.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("levelname")
                    rpt_Row.Item("Column2") = dtRow.Item("ApproverName")
                    rpt_Row.Item("Column3") = dtRow.Item("remark")
                    rpt_Row.Item("Column4") = dtRow.Item("status")
                    rpt_ApprovalDetails.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next


                objRpt = New ArutiReport.Designer.rptCoachingNominationForm

                ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
                Dim arrImageRow As DataRow = Nothing
                arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

                ReportFunction.Logo_Display(objRpt, _
                                            ConfigParameter._Object._IsDisplayLogo, _
                                            ConfigParameter._Object._ShowLogoRightSide, _
                                            "arutiLogo1", _
                                            "arutiLogo2", _
                                            arrImageRow, _
                                            "txtCompanyName", _
                                            "txtReportName", _
                                            "", _
                                            ConfigParameter._Object._GetLeftMargin, _
                                            ConfigParameter._Object._GetRightMargin)

                rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

                If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                    rpt_Data.Tables("ArutiTable").Rows.Add("")
                End If

                objRpt.SetDataSource(rpt_Data)

                Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

                objRpt.Subreports("rptCoachDetails").SetDataSource(rpt_CoachDetails)
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtProposedCoachDetails", Language.getMessage(mstrModuleName, 1, "Details of the Proposed Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachName", Language.getMessage(mstrModuleName, 2, "Name of the Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachDesignation", Language.getMessage(mstrModuleName, 3, "Designation"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachWorkStation", Language.getMessage(mstrModuleName, 4, "Work Station of the Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachWorkProcess", Language.getMessage(mstrModuleName, 5, "Unit / work process"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachGender", Language.getMessage(mstrModuleName, 6, "Gender"))

                objRpt.Subreports("rptCoacheeDetails").SetDataSource(rpt_CoacheeDetails)
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoacheeDetails"), "txtProposedCoacheeDetails", Language.getMessage(mstrModuleName, 7, "Details of the Proposed coachee"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoacheeDetails"), "txtCoacheeName", Language.getMessage(mstrModuleName, 8, "Name of the Coachee"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoacheeDetails"), "txtCoacheeDesignation", Language.getMessage(mstrModuleName, 9, "Designation"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoacheeDetails"), "txtCoacheeDepartment", Language.getMessage(mstrModuleName, 10, "Department/ Region"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoacheeDetails"), "txtCoacheeWorkProcess", Language.getMessage(mstrModuleName, 11, "Unit / work process"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoacheeDetails"), "txtCoacheeGender", Language.getMessage(mstrModuleName, 12, "Gender"))

                objRpt.Subreports("rptApprovalDetails").SetDataSource(rpt_ApprovalDetails)
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtApprovals", Language.getMessage(mstrModuleName, 38, "APPROVALS"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtLevelName", Language.getMessage(mstrModuleName, 13, "Level Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtApproverName", Language.getMessage(mstrModuleName, 14, "Approver Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtRemark", Language.getMessage(mstrModuleName, 15, "Remark"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtStatus", Language.getMessage(mstrModuleName, 16, "Status"))

            ElseIf mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then

                Dim dsCurrentCoachDetails As New DataSet
                Dim dsNewCoachDetails As New DataSet
                Dim dsApprovalDetails As New DataSet

                Dim rpt_Data As ArutiReport.Designer.dsArutiReport
                Dim rpt_CurrentCoachDetails As ArutiReport.Designer.dsArutiReport
                Dim rpt_NewCoachDetails As ArutiReport.Designer.dsArutiReport
                Dim rpt_ApprovalDetails As ArutiReport.Designer.dsArutiReport

                StrQ = " SELECT	coach_employeeunkid " & _
                       " ,      ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') coachname " & _
                       " ,      hrclasses_master.name AS WorkStatation " & _
                       " ,      pdpcoaching_nomination_form.coach_work_process AS WorkProcess " & _
                       " ,      CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Gender " & _
                       " FROM pdpcoaching_nomination_form " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pdpcoaching_nomination_form.prev_coach_employeeunkid " & _
                       " JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         employeeunkid " & _
                       "        ,ISNULL(classunkid, 0) AS classunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_transfer_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                       " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " LEFT JOIN hrclasses_master ON Alloc.classunkid = hrclasses_master.classesunkid " & _
                       " WHERE pdpcoaching_nomination_form.isvoid = 0 "

                objDataOperation.ClearParameters()

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= mstrOrderByQuery

                objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                dsCurrentCoachDetails = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                StrQ = " SELECT	coach_employeeunkid " & _
                       " ,      ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') coachname " & _
                       " ,      hrclasses_master.name AS WorkStatation " & _
                       " ,      pdpcoaching_nomination_form.newcoach_work_process AS WorkProcess " & _
                       " ,      CASE WHEN hremployee_master.gender = 1 THEN @Male WHEN hremployee_master.gender = 2 THEN @Female ELSE '' END AS Gender " & _
                       " ,      ISNULL(coachee.firstname, '') + ' ' + ISNULL(coachee.othername, '') + ' ' + ISNULL(coachee.surname, '') coacheename " & _
                       " ,      ISNULL(pdpcoaching_nomination_form.reason_for_change, '') AS reason_for_change " & _
                       " FROM pdpcoaching_nomination_form " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid " & _
                       " LEFT JOIN hremployee_master coachee ON coachee.employeeunkid = pdpcoaching_nomination_form.coachee_employeeunkid " & _
                       " JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         employeeunkid " & _
                       "        ,ISNULL(classunkid, 0) AS classunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_transfer_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                       " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " LEFT JOIN hrclasses_master ON Alloc.classunkid = hrclasses_master.classesunkid " & _
                       " WHERE pdpcoaching_nomination_form.isvoid = 0 "

                objDataOperation.ClearParameters()

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= mstrOrderByQuery

                objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                dsNewCoachDetails = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                StrQ = " SELECT	pdpcoaching_approverlevel_master.levelunkid " & _
                      " ,      pdpcoaching_approverlevel_master.levelname " & _
                      " ,      pdpcoaching_approverlevel_master.priority " & _
                      " ,      pdpcoachingapproval_process_tran.mapuserunkid " & _
                      " ,      ISNULL(UEmp.firstname, '') + ' ' + ISNULL(UEmp.lastname, '') AS ApproverName " & _
                      " ,      pdpcoachingapproval_process_tran.remark " & _
                      " ,      pdpcoachingapproval_process_tran.statusunkid " & _
                      " ,      CASE WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.PENDING & " THEN  @PENDING " & _
                      "             WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.APPROVED & " THEN  @APPROVED " & _
                      "             WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.REJECTED & " THEN  @REJECTED " & _
                      "        END status	 " & _
                      " FROM pdpcoachingapproval_process_tran " & _
                      " JOIN pdpcoaching_nomination_form ON pdpcoaching_nomination_form.coachingnominationunkid = pdpcoachingapproval_process_tran.coachingnominationunkid	AND pdpcoaching_nomination_form.isvoid = 0 " & _
                      " JOIN pdpcoaching_role_mapping ON pdpcoachingapproval_process_tran.approvertranunkid = pdpcoaching_role_mapping.mappingunkid	AND pdpcoaching_role_mapping.isvoid = 0 " & _
                      " JOIN pdpcoaching_approverlevel_master ON pdpcoaching_role_mapping.levelunkid = pdpcoaching_approverlevel_master.levelunkid AND pdpcoaching_role_mapping.isvoid = 0 " & _
                      " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = pdpcoachingapproval_process_tran.mapuserunkid " & _
                       " WHERE pdpcoachingapproval_process_tran.isvoid = 0 "

                objDataOperation.ClearParameters()

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= mstrOrderByQuery

                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 2, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 3, "Approved"))
                objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 4, "Rejected"))

                dsApprovalDetails = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                Dim dt As New DataTable
                dt = dsApprovalDetails.Tables(0).Copy()

                Dim mintPriorty As Integer = -1

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
Recalculate1:
                    Dim mintPrioriry As Integer = 0
                    For i As Integer = 0 To dt.Rows.Count - 1

                        If mintPrioriry <> CInt(dt.Rows(i)("priority")) Then
                            mintPrioriry = CInt(dt.Rows(i)("priority"))
                            Dim drRow() As DataRow = dt.Select("statusunkid <> 1 AND priority  = " & mintPrioriry)
                            If drRow.Length > 0 Then
                                Dim drPending() As DataRow = dt.Select("statusunkid = 1 AND priority  = " & mintPrioriry)
                                If drPending.Length > 0 Then
                                    Dim drPendingList() As DataRow = dsApprovalDetails.Tables(0).Select(" statusunkid = 1 AND priority  = " & mintPrioriry)
                                    If drPendingList.Length > 0 Then
                                        For Each dRowList As DataRow In drPendingList
                                            dsApprovalDetails.Tables(0).Rows.Remove(dRowList)
                                        Next
                                        dsApprovalDetails.Tables(0).AcceptChanges()
                                    End If
                                    For Each dRow As DataRow In drPending
                                        dt.Rows.Remove(dRow)
                                        GoTo Recalculate1
                                    Next
                                    dt.AcceptChanges()
                                End If
                            End If


                        End If

                    Next
                End If

                rpt_Data = New ArutiReport.Designer.dsArutiReport
                rpt_CurrentCoachDetails = New ArutiReport.Designer.dsArutiReport
                rpt_NewCoachDetails = New ArutiReport.Designer.dsArutiReport
                rpt_ApprovalDetails = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsCurrentCoachDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_CurrentCoachDetails.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("coachname")
                    rpt_Row.Item("Column3") = dtRow.Item("WorkStatation")
                    rpt_Row.Item("Column4") = dtRow.Item("WorkProcess")
                    rpt_Row.Item("Column5") = dtRow.Item("Gender")
                    rpt_CurrentCoachDetails.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                For Each dtRow As DataRow In dsNewCoachDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_NewCoachDetails.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("coachname")
                    rpt_Row.Item("Column4") = dtRow.Item("WorkProcess")
                    rpt_Row.Item("Column5") = dtRow.Item("Gender")
                    rpt_Row.Item("Column2") = dtRow.Item("coacheename")
                    rpt_Row.Item("Column6") = dtRow.Item("reason_for_change")
                    rpt_NewCoachDetails.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                For Each dtRow As DataRow In dsApprovalDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_ApprovalDetails.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("levelname")
                    rpt_Row.Item("Column2") = dtRow.Item("ApproverName")
                    rpt_Row.Item("Column3") = dtRow.Item("remark")
                    rpt_Row.Item("Column4") = dtRow.Item("status")
                    rpt_ApprovalDetails.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                objRpt = New ArutiReport.Designer.rptCoachingReplacementForm

                ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
                Dim arrImageRow As DataRow = Nothing
                arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

                ReportFunction.Logo_Display(objRpt, _
                                            ConfigParameter._Object._IsDisplayLogo, _
                                            ConfigParameter._Object._ShowLogoRightSide, _
                                            "arutiLogo1", _
                                            "arutiLogo2", _
                                            arrImageRow, _
                                            "txtCompanyName", _
                                            "txtReportName", _
                                            "", _
                                            ConfigParameter._Object._GetLeftMargin, _
                                            ConfigParameter._Object._GetRightMargin)

                rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

                If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                    rpt_Data.Tables("ArutiTable").Rows.Add("")
                End If

                objRpt.SetDataSource(rpt_Data)

                Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

                objRpt.Subreports("rptCurrentCoachDetails").SetDataSource(rpt_CurrentCoachDetails)
                Call ReportFunction.TextChange(objRpt.Subreports("rptCurrentCoachDetails"), "txtCurrentCoachDetails", Language.getMessage(mstrModuleName, 17, "Details of the Current Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCurrentCoachDetails"), "txtCurrentCoachName", Language.getMessage(mstrModuleName, 18, "Name of the Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCurrentCoachDetails"), "txtCurrentCoachWorkStation", Language.getMessage(mstrModuleName, 19, "Work Station of the Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCurrentCoachDetails"), "txtCurrentCoachWorkProcess", Language.getMessage(mstrModuleName, 20, "Unit / work process"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCurrentCoachDetails"), "txtCoachGender", Language.getMessage(mstrModuleName, 21, "Gender"))

                objRpt.Subreports("rptCoachDetails").SetDataSource(rpt_NewCoachDetails)
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtProposedCoachDetails", Language.getMessage(mstrModuleName, 22, "Details of the Proposed Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachName", Language.getMessage(mstrModuleName, 23, "Name of the Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachWorkStation", Language.getMessage(mstrModuleName, 24, "Work Station of the Coach"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachWorkProcess", Language.getMessage(mstrModuleName, 25, "Unit / work process"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachGender", Language.getMessage(mstrModuleName, 26, "Gender"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoacheeName", Language.getMessage(mstrModuleName, 27, "Name of the Coachee"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCoachDetails"), "txtCoachReasonForChanging", Language.getMessage(mstrModuleName, 28, "Reasons for changing Coach"))

                objRpt.Subreports("rptApprovalDetails").SetDataSource(rpt_ApprovalDetails)
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtApprovals", Language.getMessage(mstrModuleName, 37, "APPROVALS"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtLevelName", Language.getMessage(mstrModuleName, 29, "Level Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtApproverName", Language.getMessage(mstrModuleName, 30, "Approver Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtRemark", Language.getMessage(mstrModuleName, 31, "Remark"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptApprovalDetails"), "txtStatus", Language.getMessage(mstrModuleName, 32, "Status"))

            End If

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 33, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 34, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", mstrUserName)

            If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 35, "Coaching Nomination Form Report"))
            ElseIf mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 36, "Coaching Replacement Form Report"))
            End If


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing

        End Try
    End Function
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsCoach_Nomination_Form", 2, "Pending")
			Language.setMessage("clsCoach_Nomination_Form", 3, "Approved")
			Language.setMessage("clsCoach_Nomination_Form", 4, "Rejected")
			Language.setMessage("clsMasterData", 73, "Male")
			Language.setMessage("clsMasterData", 74, "Female")
			Language.setMessage(mstrModuleName, 1, "Details of the Proposed Coach")
			Language.setMessage(mstrModuleName, 2, "Name of the Coach")
			Language.setMessage(mstrModuleName, 3, "Designation")
			Language.setMessage(mstrModuleName, 4, "Work Station of the Coach")
			Language.setMessage(mstrModuleName, 5, "Unit / work process")
			Language.setMessage(mstrModuleName, 6, "Gender")
			Language.setMessage(mstrModuleName, 7, "Details of the Proposed coachee")
			Language.setMessage(mstrModuleName, 8, "Name of the Coachee")
			Language.setMessage(mstrModuleName, 9, "Designation")
			Language.setMessage(mstrModuleName, 10, "Department/ Region")
			Language.setMessage(mstrModuleName, 11, "Unit / work process")
			Language.setMessage(mstrModuleName, 12, "Gender")
			Language.setMessage(mstrModuleName, 13, "Level Name")
			Language.setMessage(mstrModuleName, 14, "Approver Name")
			Language.setMessage(mstrModuleName, 15, "Remark")
			Language.setMessage(mstrModuleName, 16, "Status")
			Language.setMessage(mstrModuleName, 17, "Details of the Current Coach")
			Language.setMessage(mstrModuleName, 18, "Name of the Coach")
			Language.setMessage(mstrModuleName, 19, "Work Station of the Coach")
			Language.setMessage(mstrModuleName, 20, "Unit / work process")
			Language.setMessage(mstrModuleName, 21, "Gender")
			Language.setMessage(mstrModuleName, 22, "Details of the Proposed Coach")
			Language.setMessage(mstrModuleName, 23, "Name of the Coach")
			Language.setMessage(mstrModuleName, 24, "Work Station of the Coach")
			Language.setMessage(mstrModuleName, 25, "Unit / work process")
			Language.setMessage(mstrModuleName, 26, "Gender")
			Language.setMessage(mstrModuleName, 27, "Name of the Coachee")
			Language.setMessage(mstrModuleName, 28, "Reasons for changing Coach")
			Language.setMessage(mstrModuleName, 29, "Level Name")
			Language.setMessage(mstrModuleName, 30, "Approver Name")
			Language.setMessage(mstrModuleName, 31, "Remark")
			Language.setMessage(mstrModuleName, 32, "Status")
			Language.setMessage(mstrModuleName, 33, "Printed By :")
			Language.setMessage(mstrModuleName, 34, "Printed Date :")
			Language.setMessage(mstrModuleName, 35, "Coaching Nomination Form Report")
			Language.setMessage(mstrModuleName, 36, "Coaching Replacement Form Report")
			Language.setMessage(mstrModuleName, 37, "APPROVALS")
			Language.setMessage(mstrModuleName, 38, "APPROVALS")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
