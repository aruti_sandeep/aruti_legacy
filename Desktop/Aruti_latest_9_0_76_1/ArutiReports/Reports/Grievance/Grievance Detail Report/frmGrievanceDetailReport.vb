#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region



Public Class frmGrievanceDetailReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGrievanceDetailReport"

    'Gajanan [5-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance NMB Report
    'Private objGrievanceDetailReport As clsGrievanceDetailReport
    Private objGrievanceReport As clsGrievanceReport
    'Gajanan [5-July-2019] -- End

    Private objresolution_step_tran As New clsResolution_Step_Tran
    Dim objclsMasterData As New clsMasterData
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()

        'Gajanan [5-July-2019] -- Start      
        'ISSUE/ENHANCEMENT : Grievance NMB Report
        'objGrievanceDetailReport = New clsGrievanceDetailReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objGrievanceReport = New clsGrievanceReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        'Gajanan [5-July-2019] -- End
        objGrievanceReport.SetDefaultValue()
        InitializeComponent()

    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            
            With cboFromEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp").Copy()
                .SelectedValue = 0
            End With


            'Hemant (27 Oct 2023) -- Start
            'ISSUE/ENHANCEMENT(LHRC): A1X-1455 - Grievance detail report enhancement for company level grievances 
            If CInt(ConfigParameter._Object._GrievanceApprovalSetting) = enGrievanceApproval.ApproverEmpMapping Then
                Dim drEmployee() As DataRow = dsList.Tables(0).Select("employeeunkid = -999")
                If drEmployee.Length <= 0 Then
                    Dim drow As DataRow = dsList.Tables(0).NewRow()
                    drow.Item("employeeunkid") = -999
                    drow.Item("employeecode") = "000000"
                    drow.Item("employeename") = "Company"
                    drow.Item("EmpCodeName") = "000000 - Company "
                    dsList.Tables(0).Rows.Add(drow)
                End If
            End If
            'Hemant (27 Oct 2023) -- End

            With cboAgainstEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


            dsList = Nothing


            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report
            'dsList = objclsMasterData.getComboListGrievanceResponse()
            dsList = objresolution_step_tran.getEmployeeStatusFilter()
            'Gajanan [5-July-2019] -- End


            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtfromdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dttodate.Value = ConfigParameter._Object._CurrentDateAndTime
            cboFromEmployee.SelectedValue = 0
            cboAgainstEmployee.SelectedValue = 0
            txtRefno.Text = String.Empty
            chkshowcommiteemem.Checked = False
            cboStatus.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report


            'objGrievanceDetailReport.SetDefaultValue()

            'objGrievanceDetailReport._Approvalsetting = ConfigParameter._Object._GrievanceApprovalSetting
            'objGrievanceDetailReport._UserAccessFilter = ""
            'objGrievanceDetailReport._AdvanceFilter = ""
            'objGrievanceDetailReport._IncludeAccessFilterQry = True

            'If dtfromdate.Checked Then
            '    objGrievanceDetailReport._FromGreviceDate = dtfromdate.Value
            'End If

            'If dtfromdate.Checked Then
            '    objGrievanceDetailReport._ToGreviceDate = dttodate.Value
            'End If

            'objGrievanceDetailReport._FromEmployeeid = cboFromEmployee.SelectedValue
            'objGrievanceDetailReport._FromEmployeename = cboFromEmployee.Text
            'objGrievanceDetailReport._AginstEmployeeid = cboAgainstEmployee.SelectedValue
            'objGrievanceDetailReport._AginstEmployeename = cboAgainstEmployee.SelectedText
            'objGrievanceDetailReport._Refno = txtRefno.Text
            'objGrievanceDetailReport._ShowCommiteeMembers = chkshowcommiteemem.Checked
            'objGrievanceDetailReport._CommiteeMembers = chkshowcommiteemem.Text
            'objGrievanceDetailReport._GrievcaneStatusid = cboStatus.SelectedValue
            'objGrievanceDetailReport._GrievcaneStatus = cboStatus.SelectedText

            'objGrievanceDetailReport._ViewByIds = mstrStringIds
            'objGrievanceDetailReport._ViewIndex = mintViewIdx
            'objGrievanceDetailReport._ViewByName = mstrStringName
            'objGrievanceDetailReport._Analysis_Fields = mstrAnalysis_Fields
            'objGrievanceDetailReport._Analysis_Join = mstrAnalysis_Join
            'objGrievanceDetailReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objGrievanceDetailReport._Report_GroupName = mstrReport_GroupName

            objGrievanceReport.SetDefaultValue()
            objGrievanceReport._Approvalsetting = ConfigParameter._Object._GrievanceApprovalSetting
            objGrievanceReport._UserAccessFilter = ""
            objGrievanceReport._AdvanceFilter = ""
            objGrievanceReport._IncludeAccessFilterQry = True

            If dtfromdate.Checked Then
                objGrievanceReport._FromGreviceDate = dtfromdate.Value
            End If

            If dtfromdate.Checked Then
                objGrievanceReport._ToGreviceDate = dttodate.Value
            End If

            objGrievanceReport._FromEmployeeid = cboFromEmployee.SelectedValue
            objGrievanceReport._FromEmployeename = cboFromEmployee.Text
            objGrievanceReport._AginstEmployeeid = cboAgainstEmployee.SelectedValue
            objGrievanceReport._AginstEmployeename = cboAgainstEmployee.SelectedText
            objGrievanceReport._Refno = txtRefno.Text
            objGrievanceReport._ShowCommiteeMembers = chkshowcommiteemem.Checked
            objGrievanceReport._CommiteeMembers = chkshowcommiteemem.Text
            objGrievanceReport._GrievcaneStatusid = cboStatus.SelectedValue
            objGrievanceReport._GrievcaneStatus = cboStatus.SelectedText

            objGrievanceReport._ViewByIds = mstrStringIds
            objGrievanceReport._ViewIndex = mintViewIdx
            objGrievanceReport._ViewByName = mstrStringName
            objGrievanceReport._Analysis_Fields = mstrAnalysis_Fields
            objGrievanceReport._Analysis_Join = mstrAnalysis_Join
            objGrievanceReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objGrievanceReport._Report_GroupName = mstrReport_GroupName
            'Gajanan [5-July-2019] -- End
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReport_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report


            'objGrievanceDetailReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)


            objGrievanceReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Gajanan [5-July-2019] -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmGrievanceDetailReport_Report_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
           

            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report


            'objGrievanceDetailReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)

            objGrievanceReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Gajanan [5-July-2019] -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFromEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchfrmemp.Click
        Dim frm As New frmCommonSearch
        Try

            frm.DataSource = cboFromEmployee.DataSource
            frm.ValueMember = cboFromEmployee.ValueMember
            frm.DisplayMember = cboFromEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboFromEmployee.SelectedValue = frm.SelectedValue
                cboFromEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAgainstEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchagainst.Click
        Dim frm As New frmCommonSearch
        Try

            frm.DataSource = cboAgainstEmployee.DataSource
            frm.ValueMember = cboAgainstEmployee.ValueMember
            frm.DisplayMember = cboAgainstEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboAgainstEmployee.SelectedValue = frm.SelectedValue
                cboAgainstEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAgainstEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try

            frm.displayDialog("INI")
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

#Region " Forms "

    Private Sub frmGrievanceDetailReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me._Title = objGrievanceReport._ReportName
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAgeAnalysisReport_Load", mstrModuleName)
        End Try
    End Sub
#End Region
End Class
