﻿'Class Name : clsStaffTrainingsAttendedReport.vb
'Purpose    :
'Date       : 20-May-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsStaffTrainingsAttendedReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsStaffTrainingsAttendedReport"
    Private mstrReportId As String = enArutiReport.Staff_Trainings_Attended_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintCalendarUnkid As Integer = 0
    Private mstrCalendarName As String = ""
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mstrOrderByQuery As String = ""
    Private mblnExcludeWithNoTraining As Boolean = False
    'Hemant (25 Oct 2021) -- Start
    'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
    Private mblnIsActive As Boolean = True
    'Hemant (25 Oct 2021) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ExcludeWithNoTraining() As Boolean
        Set(ByVal value As Boolean)
            mblnExcludeWithNoTraining = value
        End Set
    End Property

    'Hemant (25 Oct 2021) -- Start
    'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Hemant (25 Oct 2021) -- End

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintCalendarUnkid = 0
            mstrCalendarName = ""
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mstrAdvance_Filter = ""
            mstrOrderByQuery = ""
            mblnExcludeWithNoTraining = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Calendar :") & " " & mstrCalendarName & " "
            End If

            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Employee :") & " " & mstrEmployeeName & " "
            End If

            'mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "startdate"
            OrderByQuery = "trdepartmentaltrainingneed_master.startdate"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub
#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("EmpCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Emp Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmpName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Emp Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Function", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Function")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Department", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Department")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Zone", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Zone")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Branch", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Branch")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Grade", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 15, "Grade")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Gender", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 17, "Gender")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TrainingName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 14, "Training(s) Attended")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("StartDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Start Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EndDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "End Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            StrQ &= "IF OBJECT_ID('tempdb..#Completed') IS NOT NULL DROP TABLE #Completed " & _
                        "SELECT " & _
                            "* INTO #Completed " & _
                        "FROM (SELECT " & _
                            "DISTINCT " & _
                                "trtraining_request_master.employeeunkid " & _
                               ",cfcommon_master.name AS TrainingName " & _
                               ",trtraining_request_master.start_date " & _
                               ",trtraining_request_master.end_date " & _
                            "FROM trtraining_request_master " & _
                            "LEFT JOIN cfcommon_master " & _
                                "ON cfcommon_master.masterunkid = trtraining_request_master.coursemasterunkid " & _
                                "AND mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                            "WHERE isvoid = 0 " & _
                            "AND periodunkid = @periodunkid " & _
                            "AND departmentaltrainingneedunkid > 0 " & _
                            "AND iscompleted_submit_approval = 1 " & _
                            "AND completed_statusunkid = " & enTrainingRequestStatus.APPROVED & " ) AS A "

            StrQ &= "SELECT  " & _
                       "  hremployee_master.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                       ", ISNULL(hd.name, '') AS Department " & _
                       ", ISNULL(hsg.name, '') AS SectionGrp " & _
                       ", ISNULL(hcg.name, '') AS ClassGrp " & _
                       ", ISNULL(hc.name, '') AS Class " & _
                       ", ISNULL(hgd.name, '') AS GradeGrp " & _
                       ", ISNULL(C.TrainingName, '') AS trainingcoursename " & _
                       ", CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Gender " & _
                       ", ISNULL(C.start_date, '') AS start_date " & _
                       ", ISNULL(C.end_date, '') AS end_date "
            StrQ &= "FROM  hremployee_master  " & _
                         " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             stationunkid " & _
                          "            ,deptgroupunkid " & _
                          "            ,departmentunkid " & _
                          "            ,sectiongroupunkid " & _
                          "            ,sectionunkid " & _
                          "            ,unitgroupunkid " & _
                          "            ,unitunkid " & _
                          "            ,teamunkid " & _
                          "            ,classgroupunkid " & _
                          "             ,classunkid " & _
                          "            ,employeeunkid " & _
                          "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "       FROM hremployee_transfer_tran " & _
                          "       WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                          " LEFT JOIN hrstation_master hs ON t.stationunkid = hs.stationunkid " & _
                          " LEFT JOIN hrdepartment_master hd ON t.departmentunkid = hd.departmentunkid " & _
                          " LEFT JOIN hrsectiongroup_master hsg ON t.sectiongroupunkid = hsg.sectiongroupunkid " & _
                          " LEFT JOIN hrclassgroup_master hcg ON t.classgroupunkid = hcg.classgroupunkid " & _
                          " LEFT JOIN hrclasses_master hc ON t.classunkid = hc.classesunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "             SELECT " & _
                          "                 jobgroupunkid " & _
                          "                ,jobunkid " & _
                          "                ,employeeunkid " & _
                          "                ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "             FROM hremployee_categorization_tran " & _
                          "             WHERE isvoid = 0	AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate  " & _
                          " ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                          " LEFT JOIN hrjob_master hjb ON hjb.jobunkid = j.jobunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "           gradegroupunkid " & _
                          "          ,gradeunkid " & _
                          "          ,gradelevelunkid " & _
                          "          ,employeeunkid " & _
                          "          ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                          "        FROM prsalaryincrement_tran " & _
                          "        WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS G ON G.employeeunkid = hremployee_master.employeeunkid	AND G.Rno = 1 " & _
                          " LEFT JOIN hrgradegroup_master hgd ON hgd.gradegroupunkid = G.gradegroupunkid " & _
                          " LEFT JOIN #Completed AS C ON C.employeeunkid = hremployee_master.employeeunkid	 "

            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'Hemant (25 Oct 2021) -- End

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  1 = 1 "

            If mblnExcludeWithNoTraining = True Then
                StrQ &= " AND C.TrainingName IS NOT NULL "
            End If
            'StrQ &= "WHERE trdepttrainingneed_employee_tran.isvoid = 0 " & _
            '          "AND trtraining_calendar_master.isactive = 1 " & _
            '          "AND trdepartmentaltrainingneed_master.statusunkid = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' "

            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Hemant (25 Oct 2021) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= mstrOrderByQuery

            If mintCalendarUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkid)
            End If

            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("EmpCode") = drRow.Item("employeecode")
                rpt_Row.Item("EmpName") = drRow.Item("employee")
                rpt_Row.Item("Function") = drRow.Item("Department")
                rpt_Row.Item("Department") = drRow.Item("SectionGrp")
                rpt_Row.Item("Zone") = drRow.Item("ClassGrp")
                rpt_Row.Item("Branch") = drRow.Item("Class")
                rpt_Row.Item("Grade") = drRow.Item("GradeGrp")
                rpt_Row.Item("Gender") = drRow.Item("Gender")
                rpt_Row.Item("TrainingName") = drRow.Item("trainingcoursename")

                If eZeeDate.convertDate(drRow.Item("start_date")) = "19000101" Then
                    rpt_Row.Item("startdate") = ""
                Else
                    rpt_Row.Item("startdate") = CDate(drRow.Item("start_date")).ToShortDateString
                End If

                If eZeeDate.convertDate(drRow.Item("end_date")) = "19000101" Then
                    rpt_Row.Item("enddate") = ""
                Else
                    rpt_Row.Item("enddate") = CDate(drRow.Item("end_date")).ToShortDateString
                End If

                dtFinalTable.Rows.Add(rpt_Row)

            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub

#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsMasterData", 73, "Male")
			Language.setMessage("clsMasterData", 74, "Female")
			Language.setMessage(mstrModuleName, 1, "Emp Code")
			Language.setMessage(mstrModuleName, 2, "Employee Names")
			Language.setMessage(mstrModuleName, 3, "Company")
			Language.setMessage(mstrModuleName, 4, "Function")
			Language.setMessage(mstrModuleName, 5, "Department")
			Language.setMessage(mstrModuleName, 6, "Zone")
			Language.setMessage(mstrModuleName, 7, "Branch")
			Language.setMessage(mstrModuleName, 8, "Prepared By :")
			Language.setMessage(mstrModuleName, 9, "Checked By :")
			Language.setMessage(mstrModuleName, 10, "Approved By :")
			Language.setMessage(mstrModuleName, 11, "Received By :")
			Language.setMessage(mstrModuleName, 12, "Calendar :")
			Language.setMessage(mstrModuleName, 13, "Employee :")
			Language.setMessage(mstrModuleName, 14, "Training Name")
			Language.setMessage(mstrModuleName, 15, "Grade")
			Language.setMessage(mstrModuleName, 16, "Job Title")
			Language.setMessage(mstrModuleName, 17, "Gender")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
