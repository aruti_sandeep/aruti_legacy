﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
#End Region

Public Class clsPIPFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPIPFormReport"
    Private mstrReportId As String = enArutiReport.PIP_Form_Report '319
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintPIPFormId As Integer = 0
    Private mstrPIPFormNo As String = ""
    Private mdtEmployeeAsOnDate As Date = Nothing
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

#End Region

#Region " Properties "


    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PIPFormId() As Integer
        Set(ByVal value As Integer)
            mintPIPFormId = value
        End Set
    End Property

    Public WriteOnly Property _PIPFormNo() As String
        Set(ByVal value As String)
            mstrPIPFormNo = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeAsOnDate() As Date
        Set(ByVal value As Date)
            mdtEmployeeAsOnDate = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mintPIPFormId = 0
            mstrPIPFormNo = ""
            mdtEmployeeAsOnDate = Now.Date
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Pending"))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsOnDate))

            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND pipform_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintPIPFormId > 0 Then
                objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormId)
                Me._FilterQuery &= " AND pipform_master.pipformunkid = @pipformunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "PIP Form No :") & " " & mstrPIPFormNo & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xExportReportPath, xOpenReportAfterExport, PrintAction, ExportAction, intBaseCurrencyUnkid)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rptpipChallange As New ArutiReport.Designer.dsArutiReport
        Dim rptpipMonitoring As New ArutiReport.Designer.dsArutiReport

        Dim objSetting As New clspipsettings_master
        Dim pipSetting As Dictionary(Of clspipsettings_master.enPIPConfiguration, String) = objSetting.GetSetting()

        Dim enProgressMapping As clspipsettings_master.enPIPProgressMappingId
        If pipSetting.Keys.Contains(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING) Then
            enProgressMapping = pipSetting(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING)
        End If

        Dim enOverAllAssessment As clspipsettings_master.enPIPProgressMappingId
        If pipSetting.Keys.Contains(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING) Then
            enOverAllAssessment = pipSetting(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING)
        End If


        objDataOperation = New clsDataOperation
        Try
            StrQ = " SELECT " & _
                      "    pipform_master.pipformunkid " & _
                      "   ,pipform_master.employeeunkid " & _
                      "   ,pipform_master.pipformno " & _
                      "   ,ISNULL(hremployee_master.employeecode, '') AS Code " & _
                      "   ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      "   ,ISNULL(RUCat.JobCode,'') AS JobCode " & _
                      "   ,ISNULL(RUCat.Job,'') AS Job " & _
                      "   ,ISNULL(RepTable.LineManager, '') AS LineManager " & _
                      "   ,ISNULL(A.coach, '') AS Coach " & _
                      "   ,ISNULL(piprating_master.optionvalue, @Pending) AS Status " & _
                      "   ,ISNULL(pipform_master.assessment_remark, '') AS assessment_remark " & _
                      " FROM pipform_master " & _
                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pipform_master.employeeunkid " & _
                      " LEFT JOIN (SELECT " & _
                      "				            ECT.employeeunkid AS CatEmpId " & _
                      "			               ,ECT.jobgroupunkid " & _
                      "			               ,ECT.jobunkid " & _
                      "			               ,CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                      "			               ,ECT.employeeunkid " & _
                      "			               ,ISNULL(hrjob_master.job_code ,'') AS JobCode " & _
                      "			               ,ISNULL(hrjob_master.job_name ,'') AS Job " & _
                      "			               ,ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                      "		            FROM hremployee_categorization_tran AS ECT " & _
                      "		            LEFT JOIN hrjob_master ON hrjob_master.jobunkid = ECT.jobunkid " & _
                      "		            WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <=  @effectivedate " & _
                      "		          ) AS RUCat ON hremployee_master.employeeunkid = RUCat.employeeunkid " & _
                      " LEFT JOIN (SELECT " & _
                      "				             hremployee_reportto.employeeunkid AS employeeunkid " & _
                      "		                    ,hremployee_reportto.employeeunkid AS reporttoemployeeunkid " & _
                      "			                ,ISNULL(u.userunkid, 0) AS LMUserId " & _
                      "			                ,ISNULL(R_Emp.firstname, '') + ' ' + ISNULL(R_Emp.surname, '') AS LineManager " & _
                      "			                ,ROW_NUMBER() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                      "		               FROM hremployee_reportto " & _
                      "		               LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid " & _
                      "		               LEFT JOIN hrmsconfiguration..cfuser_master AS u ON u.employeeunkid = R_Emp.employeeunkid AND u.isactive = 1 " & _
                      "		               WHERE ishierarchy = 1 AND hremployee_reportto.isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @effectivedate " & _
                      "		          ) AS RepTable ON RepTable.employeeunkid = hremployee_master.employeeunkid AND RepTable.Rno = 1 " & _
                      " LEFT JOIN (SELECT " & _
                      "				               coachee_employeeunkid " & _
                      "			                   ,coach_employeeunkid " & _
                      "			                   ,ISNULL(u.userunkid, 0) AS CoachUserId " & _
                      "			                   ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS coach " & _
                      "			                   ,ROW_NUMBER() OVER (PARTITION BY coachee_employeeunkid ORDER BY effective_date DESC) AS ROWNO " & _
                      "		               FROM pdpcoaching_nomination_form " & _
                      "		               LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid " & _
                      "    	               LEFT JOIN hrmsconfiguration..cfuser_master AS u ON u.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid AND u.isactive = 1 " & _
                      "		                WHERE pdpcoaching_nomination_form.isvoid = 0 " & _
                      "		            ) AS A ON A.coachee_employeeunkid = hremployee_master.employeeunkid AND A.ROWNO = 1 " & _
                      " LEFT JOIN piprating_master ON piprating_master.pipratingunkid = pipform_master.statusunkid AND piprating_master.ratingtypeunkid =  " & clspiprating_master.enPIPRatingType.Final_Assessment & _
                      " WHERE 1 = 1 "


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            '******************** Main Part ******************' START
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("pipformno")
                rpt_Rows.Item("Column2") = dtRow.Item("Code")
                rpt_Rows.Item("Column3") = dtRow.Item("Employee")
                rpt_Rows.Item("Column4") = dtRow.Item("Job")
                rpt_Rows.Item("Column5") = dtRow.Item("LineManager")
                rpt_Rows.Item("Column6") = dtRow.Item("Status")
                rpt_Rows.Item("Column7") = dtRow.Item("assessment_remark")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '******************** Main Part ******************' END

            '******************** Challange & Expectation ******************' START

            StrQ = " SELECT " & _
                       "    pipconcern_expectationtran.pipformunkid " & _
                       "	,pipconcern_expectationtran.concernunkid " & _
                       "	,pipconcern_expectationtran.parameterunkid " & _
                       "	,pipconcern_expectationtran.itemunkid " & _
                       "	,pipparameter_master.parameter " & _
                       "	,pipitem_master.item " & _
                       "	,pipconcern_expectationtran.concern_value " & _
                       " FROM pipconcern_expectationtran " & _
                       " JOIN pipform_master ON pipform_master.pipformunkid = pipconcern_expectationtran.pipformunkid AND pipform_master.isvoid = 0 " & _
                       " LEFT JOIN pipparameter_master ON pipparameter_master.parameterunkid = pipconcern_expectationtran.parameterunkid " & _
                       " LEFT JOIN pipitem_master ON pipitem_master.itemunkid = pipconcern_expectationtran.itemunkid " & _
                       " WHERE pipconcern_expectationtran.isvoid = 0 AND pipparameter_master.parameterunkid > 0 " & _
                       " AND pipconcern_expectationtran.pipformunkid = @pipformunkid " & _
                       " ORDER BY parameter "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormId)
            Dim dsExpectation As DataSet = objDataOperation.ExecQuery(StrQ, "Expectation")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dtRow As DataRow In dsExpectation.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rptpipChallange.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("parameter")
                rpt_Rows.Item("Column2") = dtRow.Item("concern_value")
                rptpipChallange.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '******************** Challange & Expectation ******************' END 


            '******************** Progres Monitoring ******************' START

            StrQ = " SELECT " & _
                        "	 pipmonitoring_tran.pipformunkid " & _
                        "	,pipmonitoring_tran.pipmoniteringunkid " & _
                        "    ,pipmonitoring_tran.parameterunkid " & _
                        "	,pipmonitoring_tran.itemunkid " & _
                        "	,pipparameter_master.parameter " & _
                        "	,pipitem_master.item " & _
                        "   ,pipimprovement_plantran.improvement_goal " & _
                        "   ,pipimprovement_plantran.improvement_value " & _
                        "   ,pipimprovement_plantran.duedate " & _
                        "	,pipmonitoring_tran.progressdate " & _
                        "	,pipmonitoring_tran.statusunkid " & _
                        "	,ISNULL(piprating_master.optionvalue,'') AS Status " & _
                        "	,pipmonitoring_tran.remark " & _
                        " FROM pipmonitoring_tran " & _
                        " JOIN pipimprovement_plantran ON pipimprovement_plantran.improvementplanunkid = pipmonitoring_tran.improvementplanunkid AND pipimprovement_plantran.isvoid = 0 " & _
                        " JOIN pipform_master ON pipform_master.pipformunkid = pipmonitoring_tran.pipformunkid AND pipform_master.isvoid = 0 " & _
                        " LEFT JOIN pipparameter_master ON pipparameter_master.parameterunkid = pipmonitoring_tran.parameterunkid " & _
                        " LEFT JOIN pipitem_master ON pipitem_master.itemunkid = pipmonitoring_tran.itemunkid AND pipitem_master.parameterunkid = pipmonitoring_tran.parameterunkid AND pipitem_master.isimprovementgoal = 1 " & _
                        " LEFT JOIN piprating_master ON piprating_master.pipratingunkid = pipmonitoring_tran.statusunkid AND piprating_master.ratingtypeunkid = " & clspiprating_master.enPIPRatingType.Progress_Monitoring & _
                        " WHERE pipmonitoring_tran.isvoid = 0 AND pipparameter_master.parameterunkid > 0 AND pipitem_master.itemunkid > 0 " & _
                        " AND pipmonitoring_tran.pipformunkid = @pipformunkid " & _
                        " ORDER BY item,progressdate "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormId)

            Dim dsMonitoring As DataSet = objDataOperation.ExecQuery(StrQ, "Monitoring")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dtRow As DataRow In dsMonitoring.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rptpipMonitoring.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("improvement_goal").ToString()
                rpt_Rows.Item("Column2") = dtRow.Item("improvement_value").ToString()
                rpt_Rows.Item("Column3") = CDate(dtRow.Item("duedate")).ToShortDateString()
                rpt_Rows.Item("Column4") = CDate(dtRow.Item("progressdate")).ToShortDateString()
                rpt_Rows.Item("Column5") = dtRow.Item("Status").ToString()
                rpt_Rows.Item("Column6") = dtRow.Item("remark").ToString()
                rptpipMonitoring.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '******************** Progres Monitoring ******************' END


            objRpt = New ArutiReport.Designer.rptPIPFormReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

            If Company._Object._Address1.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress1", True)
            End If

            Dim mstrAddress As String = String.Empty

            If Company._Object._Address2.ToString.Trim <> "" Then
                mstrAddress = Company._Object._Address2.ToString.Trim
            End If

            If Company._Object._Post_Code_No.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Post_Code_No.ToString.Trim
            End If

            If Company._Object._City_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._City_Name.ToString.Trim
            End If

            If Company._Object._State_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._State_Name.ToString.Trim
            End If

            If Company._Object._Country_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Country_Name.ToString.Trim
            End If

            If mstrAddress.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtAddress2", mstrAddress)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress2", True)
            End If

            ReportFunction.TextChange(objRpt, "txtPIPFormNo", Language.getMessage(mstrModuleName, 1, "PIP Form No"))
            ReportFunction.TextChange(objRpt, "txtEmpNumber", Language.getMessage(mstrModuleName, 2, "Employee Number"))
            ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 3, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 4, "Job Title"))

            If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER Then
                ReportFunction.TextChange(objRpt, "txtManagerName", Language.getMessage(mstrModuleName, 5, "Line Manager Name"))
            ElseIf enProgressMapping = clspipsettings_master.enPIPProgressMappingId.NOMINATED_COACH Then
                ReportFunction.TextChange(objRpt, "txtManagerName", Language.getMessage(mstrModuleName, 6, "Coach Name"))
            End If

            ReportFunction.TextChange(objRpt, "txtOverAssessment", Language.getMessage(mstrModuleName, 7, "Overall Assessment"))
            ReportFunction.TextChange(objRpt, "txtOverallRating", Language.getMessage(mstrModuleName, 8, "Overall Rating"))

            If enOverAllAssessment = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER Then
                ReportFunction.TextChange(objRpt, "txtOverallComments", Language.getMessage(mstrModuleName, 9, "Line Manager’s overall comments"))
                ReportFunction.TextChange(objRpt, "txtOverallEmpSign", Language.getMessage(mstrModuleName, 10, "Signature by Employee :"))
                ReportFunction.TextChange(objRpt, "txtOverallManagerSign", Language.getMessage(mstrModuleName, 11, "Signature by Line Manager :"))
            ElseIf enOverAllAssessment = clspipsettings_master.enPIPProgressMappingId.ROLE_BASED Then
                ReportFunction.TextChange(objRpt, "txtOverallComments", Language.getMessage(mstrModuleName, 12, "Coach’s overall comments"))
                ReportFunction.TextChange(objRpt, "txtOverallEmpSign", Language.getMessage(mstrModuleName, 13, "Signature by Coachee :"))
                ReportFunction.TextChange(objRpt, "txtOverallManagerSign", Language.getMessage(mstrModuleName, 14, "Signature by Coach :"))
            End If
            ReportFunction.TextChange(objRpt, "txtOverallEmpSignDate", Language.getMessage(mstrModuleName, 15, "Date :"))
            ReportFunction.TextChange(objRpt, "txtOverallManagerSignDate", Language.getMessage(mstrModuleName, 15, "Date :"))

            objRpt.Subreports("rptpipChallangeExpectation").SetDataSource(rptpipChallange)
            Call ReportFunction.TextChange(objRpt.Subreports("rptpipChallangeExpectation"), "txtEmpSign", Language.getMessage(mstrModuleName, 13, "Signature by Coachee :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptpipChallangeExpectation"), "txtManagerSign", Language.getMessage(mstrModuleName, 14, "Signature by Coach :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptpipChallangeExpectation"), "txtEmpSignDate", Language.getMessage(mstrModuleName, 15, "Date :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptpipChallangeExpectation"), "txtManagerSignDate", Language.getMessage(mstrModuleName, 15, "Date :"))

            objRpt.Subreports("rptMonitoring").SetDataSource(rptpipMonitoring)
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtMonitoringProgress", Language.getMessage(mstrModuleName, 16, "Monitoring / Regular Progress Review"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtPlannedGoal", Language.getMessage(mstrModuleName, 17, "Planned Goal"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtPlannedActivity", Language.getMessage(mstrModuleName, 18, "Planned Activities"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtTimeFrame", Language.getMessage(mstrModuleName, 19, "Agreed TimeFrame"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtProgressDate", Language.getMessage(mstrModuleName, 20, "Progress Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtStatus", Language.getMessage(mstrModuleName, 21, "Status"))

            If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER Then
                Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtRemark", Language.getMessage(mstrModuleName, 22, "Line Manager Remark"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtMonitoringEmpSign", Language.getMessage(mstrModuleName, 10, "Signature by Employee :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtMonitoringManagerSign", Language.getMessage(mstrModuleName, 11, "Signature by Line Manager :"))
            ElseIf enProgressMapping = clspipsettings_master.enPIPProgressMappingId.NOMINATED_COACH Then
                Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtRemark", Language.getMessage(mstrModuleName, 23, "Coach Remark"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtMonitoringEmpSign", Language.getMessage(mstrModuleName, 13, "Signature by Coachee :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtMonitoringManagerSign", Language.getMessage(mstrModuleName, 14, "Signature by Coach :"))
            End If
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtMonitoringEmpSignDate", Language.getMessage(mstrModuleName, 15, "Date :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptMonitoring"), "txtMonitoringManagerSignDate", Language.getMessage(mstrModuleName, 15, "Date :"))



            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 24, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 25, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "PIP Form No")
            Language.setMessage(mstrModuleName, 2, "Employee Number")
            Language.setMessage(mstrModuleName, 3, "Employee Name")
            Language.setMessage(mstrModuleName, 4, "Job Title")
            Language.setMessage(mstrModuleName, 5, "Line Manager Name")
            Language.setMessage(mstrModuleName, 6, "Coach Name")
            Language.setMessage(mstrModuleName, 7, "Overall Assessment")
            Language.setMessage(mstrModuleName, 8, "Overall Rating")
            Language.setMessage(mstrModuleName, 9, "Line Manager’s overall comments")
            Language.setMessage(mstrModuleName, 10, "Signature by Employee :")
            Language.setMessage(mstrModuleName, 11, "Signature by Line Manager :")
            Language.setMessage(mstrModuleName, 12, "Coach’s overall comments")
            Language.setMessage(mstrModuleName, 13, "Signature by Coachee :")
            Language.setMessage(mstrModuleName, 14, "Signature by Coach :")
            Language.setMessage(mstrModuleName, 15, "Date :")
            Language.setMessage(mstrModuleName, 16, "Monitoring / Regular Progress Review")
            Language.setMessage(mstrModuleName, 17, "Planned Goal")
            Language.setMessage(mstrModuleName, 18, "Planned Activities")
            Language.setMessage(mstrModuleName, 19, "Agreed TimeFrame")
            Language.setMessage(mstrModuleName, 20, "Progress Date")
            Language.setMessage(mstrModuleName, 21, "Status")
            Language.setMessage(mstrModuleName, 22, "Line Manager Remark")
            Language.setMessage(mstrModuleName, 23, "Coach Remark")
            Language.setMessage(mstrModuleName, 24, "Printed By :")
            Language.setMessage(mstrModuleName, 25, "Printed Date :")
            Language.setMessage(mstrModuleName, 26, "Pending")
            Language.setMessage(mstrModuleName, 27, "Employee :")
            Language.setMessage(mstrModuleName, 28, "PIP Form No :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
