#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib

#End Region


Public Class frmTrainingCostReport
#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmTrainingCostReport"
    Private objTrainingCost As clsTrainingCostReport
    Private objEmp As clsEmployee_Master
    Private objBankBranch As clsbankbranch_master
    Private objPeriod As clscommom_period_Tran
    Private mintCurrentPeriodId As Integer = 0
    Private objCourseType As clsCommon_Master
    Private objInstitute As clsinstitute_master
    Private objStatus As clsMasterData
    Private objDepartment As clsDepartment

#End Region

#Region " Constructor "
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        objTrainingCost = New clsTrainingCostReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTrainingCost.SetDefaultValue()
        ' Add any initialization after the InitializeComponent() call.
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            objEmp = New clsEmployee_Master
            objPeriod = New clscommom_period_Tran

            objCourseType = New clsCommon_Master
            objInstitute = New clsinstitute_master
            objStatus = New clsMasterData
            objDepartment = New clsDepartment



            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 15 MAY 2012 ] -- END

            'Pinkal (24-Jun-2011) -- End

            With cboEmployeeName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = objStatus.GetTraining_Status("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("Status")
                .SelectedValue = 0
            End With

            dsList = objInstitute.getListForCombo(False, "Institute", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Institute")
                .SelectedValue = 0
            End With

            dsList = objCourseType.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "Course")
            With cboCourseType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Course")
                .SelectedValue = 0
            End With

            dsList = objDepartment.getComboList("List", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            dsList = objCourseType.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "Training")
            With cboCourseMaster
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Training")
                .SelectedValue = 0
            End With
            'Anjan (10 Feb 2012)-End 



            cboReportType.Items.Clear()
            cboReportType.Items.Add("Department wise report")
            cboReportType.Items.Add("Employee Wise Report")
            cboReportType.SelectedIndex = 1

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            Dim objMData As New clsMasterData
            dsList = objMData.GetEAllocation_Notification("List")
            Dim row As DataRow = dsList.Tables("List").NewRow
            With row
                .Item("Id") = 0
                .Item("Name") = Language.getMessage(mstrModuleName, 100, "Select")
            End With

            dsList.Tables("List").Rows.InsertAt(row, 0)
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally

            dsList.Dispose()
            dsList = Nothing

            objEmp = Nothing
            objStatus = Nothing
            objPeriod = Nothing
            objInstitute = Nothing
            objCourseType = Nothing


        End Try

    End Sub

    Private Sub ResetValue()

        Try
            cboCourseType.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboEmployeeName.SelectedValue = 0
            cboInstitute.SelectedValue = mintCurrentPeriodId
            txtEmpcode.Text = ""
            txtCourseTitle.Text = ""
            cboInstitute.SelectedValue = 0
            cboStatus.SelectedValue = 0
            objTrainingCost.setDefaultOrderBy(2)
            txtOrderBy.Text = objTrainingCost.OrderByDisplay

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            cboCourseMaster.SelectedValue = 0
            'Anjan (10 Feb 2012)-End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            cboAllocations.SelectedValue = 0
            lvAllocation.Items.Clear()
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try

    End Sub

    Private Function SetFilter() As Boolean

        Try
            objTrainingCost.SetDefaultValue()

            If CInt(cboCourseType.SelectedValue) > 0 Then
                objTrainingCost._CourseTypeId = CInt(cboCourseType.SelectedValue)
                objTrainingCost._CourseType = cboCourseType.Text
            End If


            If CInt(cboInstitute.SelectedValue) > 0 Then
                objTrainingCost._Instituteid = CInt(cboInstitute.SelectedValue)
                objTrainingCost._InstituteName = cboInstitute.Text
            End If


            If CInt(cboStatus.SelectedValue) > 0 Then
                objTrainingCost._StatusId = CInt(cboStatus.SelectedValue)
                objTrainingCost._StatusName = cboStatus.Text
            End If

            If Trim(txtEmpcode.Text) <> "" Then
                objTrainingCost._EmpCode = txtEmpcode.Text
            End If

            If Trim(txtCourseTitle.Text) <> "" Then
                objTrainingCost._CourseTitle = txtCourseTitle.Text
            End If

            If CInt(cboEmployeeName.SelectedValue) > 0 Then
                objTrainingCost._EmployeeId = CInt(cboEmployeeName.SelectedValue)
                objTrainingCost._EmployeeName = cboEmployeeName.Text

            End If

            If CInt(cboDepartment.SelectedValue) > 0 Then
                objTrainingCost._DepartmentId = CInt(cboDepartment.SelectedValue)
                objTrainingCost._DepartmentName = cboDepartment.Text
            End If

            
            objTrainingCost._ReportTypeId = cboReportType.SelectedIndex
            objTrainingCost._ReportTypeName = cboReportType.Text


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If CInt(cboCourseMaster.SelectedValue) > 0 Then
                objTrainingCost._TrainingId = CInt(cboCourseMaster.SelectedValue)
                objTrainingCost._TrainingName = cboCourseMaster.Text
            End If
            'Anjan (10 Feb 2012)-End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objTrainingCost._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            objTrainingCost._AllocationTypeId = cboAllocations.SelectedValue
            objTrainingCost._AllocationTypeName = cboAllocations.Text
            If lvAllocation.CheckedItems.Count > 0 Then
                objTrainingCost._AllocationIds = String.Join(",", lvAllocation.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToArray)
                objTrainingCost._AllocationNames = String.Join(",", lvAllocation.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.SubItems(colhAllocations.Index).Text).ToArray)
            End If
            'S.SANDEEP [08-FEB-2017] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try

    End Function

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJGrp As New clsJobGroup
                    dList = objJGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.GetList("List")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 8 Then
                colhAllocations.Width = 285 - 20
            Else
                colhAllocations.Width = 285
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean)
        Try
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Form Events "
    Private Sub frmTrainingCost_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingCost = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_FormClosed", mstrModuleName)
        End Try
    End Sub
    Private Sub frmTrainingCost_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objTrainingCost._ReportName
            Me._Message = objTrainingCost._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_Load", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            'If cboPeriod.SelectedValue <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Period is mandatory information.Please select Period."), enMsgBoxStyle.Information)
            '    cboPeriod.Focus()
            '    Exit Sub
            'End If

            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTrainingCost.generateReport(CInt(cboReportType.SelectedIndex), e.Type, enExportAction.None)
            objTrainingCost.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              CInt(cboReportType.SelectedIndex), e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTrainingCost.generateReport(CInt(cboReportType.SelectedIndex), enPrintAction.None, e.Type)
            objTrainingCost.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              CInt(cboReportType.SelectedIndex), enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployeeName.DataSource
            frm.SelectedValue = cboEmployeeName.SelectedValue
            frm.ValueMember = cboEmployeeName.ValueMember
            frm.DisplayMember = cboEmployeeName.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployeeName.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingCostReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingCostReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click

        Try
            objTrainingCost.setOrderBy(0)
            txtOrderBy.Text = objTrainingCost.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try

    End Sub

#End Region

    

    'Private Sub cboBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
    '    objBankBranch = New clsbankbranch_master(User._Object._Languageunkid,Company._Object._Companyunkid)
    '    Dim dsList As New DataSet
    '    Try
    '        dsList = objBankBranch.getListForCombo("List", True, CInt(cboBankName.SelectedValue))
    '        With cboBranchName
    '            .ValueMember = "branchunkid"
    '            .DisplayMember = "name"
    '            .DataSource = dsList.Tables("List")
    '            .SelectedValue = 0

    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboBankName_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '        objBankBranch = Nothing
    '        dsList.Dispose()
    '        dsList = Nothing
    '    End Try

    'End Sub

    


    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
#Region " Combobox Event(s) "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged

        Try

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'If CInt(cboReportType.SelectedIndex) = 1 Then
            '    cboDepartment.Enabled = False
            '    cboDepartment.SelectedValue = 0
            'End If
            Select Case cboReportType.SelectedIndex
                Case 0
                    cboDepartment.Enabled = True
                    cboDepartment.SelectedValue = 0
                Case 1
                cboDepartment.Enabled = False
                cboDepartment.SelectedValue = 0
            End Select
            'S.SANDEEP [ 05 JULY 2011 ] -- END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Listview Events "

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState))
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [08-FEB-2017] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblEmpCode.Text = Language._Object.getCaption(Me.lblEmpCode.Name, Me.lblEmpCode.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblType.Text = Language._Object.getCaption(Me.lblType.Name, Me.lblType.Text)
			Me.lblCourseTitle.Text = Language._Object.getCaption(Me.lblCourseTitle.Name, Me.lblCourseTitle.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
