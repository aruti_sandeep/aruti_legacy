'************************************************************************************************************************************
'Class Name : frmEmployeeQualificationReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeQualificationReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeQualificationReport"
    Private objEQualifReport As clsEmployeeQualificationReport
    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    'Sandeep [ 12 MARCH 2011 ] -- End 

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Anjan (17 May 2012)-End 

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        objEQualifReport = New clsEmployeeQualificationReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEQualifReport.SetDefaultValue()
        InitializeComponent()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjCMaster As New clsCommon_Master
        Dim ObjQMaster As New clsqualification_master
        Dim ObjBMaster As New clsStation
        Dim ObjDMaster As New clsDepartment
        Dim ObjJMaster As New clsJobs
        Dim ObjIMaster As New clsinstitute_master
        Dim dsList As New DataSet
        Try


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = ObjEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = ObjEmp.GetEmployeeList("Emp", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 15 MAY 2012 ] -- END
            'Pinkal (24-Jun-2011) -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = ObjCMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QCAT")
            With cboQualifiyGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("QCAT")
                .SelectedValue = 0
            End With

            dsList = ObjQMaster.GetComboList("QMast", True)
            With cboQualifiy
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("QMast")
                .SelectedValue = 0
            End With

            'dsList = ObjBMaster.getComboList("Branch", True)
            'With cboBranch
            '    .ValueMember = "stationunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("Branch")
            '    .SelectedValue = 0
            'End With

            'dsList = ObjDMaster.getComboList("Dept", True)
            'With cboDepartment
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("Dept")
            '    .SelectedValue = 0
            'End With

            'dsList = ObjJMaster.getComboList("Job", True)
            'With cboJobs
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("Job")
            '    .SelectedValue = 0
            'End With

            dsList = ObjIMaster.getListForCombo(False, "Institute", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Institute")
                .SelectedValue = 0
            End With



            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Employee With Education Details"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Employee Without Education Details"))
            cboReportType.SelectedIndex = 0
            'Pinkal (22-Mar-2012) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing
            ObjCMaster = Nothing
            ObjQMaster = Nothing
            ObjBMaster = Nothing
            ObjDMaster = Nothing
            ObjJMaster = Nothing
            ObjIMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'cboBranch.SelectedValue = 0
            'cboDepartment.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboInstitute.SelectedValue = 0
            'cboJobs.SelectedValue = 0
            cboQualifiy.SelectedValue = 0
            cboQualifiyGroup.SelectedValue = 0

            objEQualifReport.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objEQualifReport.OrderByDisplay

            'Sandeep [ 12 MARCH 2011 ] -- Start
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            'Sandeep [ 12 MARCH 2011 ] -- End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            cboReportType.SelectedIndex = 0
            'Pinkal (22-Mar-2012) -- End


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Anjan (17 May 2012)-End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEQualifReport.SetDefaultValue()

            'objEQualifReport._BranchId = cboBranch.SelectedValue
            'objEQualifReport._BranchName = cboBranch.Text

            'objEQualifReport._DepartmentId = cboDepartment.SelectedValue
            'objEQualifReport._DepartmentName = cboDepartment.Text

            objEQualifReport._EmployeeId = cboEmployee.SelectedValue
            objEQualifReport._EmployeeName = cboEmployee.Text

            objEQualifReport._InstituteId = cboInstitute.SelectedValue
            objEQualifReport._InstituteName = cboInstitute.Text

            'objEQualifReport._JobId = cboJobs.SelectedValue
            'objEQualifReport._JobName = cboJobs.Text

            objEQualifReport._QualificationGrpId = cboQualifiyGroup.SelectedValue
            objEQualifReport._QualificationGrpName = cboQualifiyGroup.Text

            objEQualifReport._QualificationId = cboQualifiy.SelectedValue
            objEQualifReport._QualificationName = cboQualifiy.Text


            'Sandeep [ 12 MARCH 2011 ] -- Start
            objEQualifReport._ViewByIds = mstrStringIds
            objEQualifReport._ViewIndex = mintViewIdx
            objEQualifReport._ViewByName = mstrStringName
            'Sandeep [ 12 MARCH 2011 ] -- End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objEQualifReport._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End



            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            objEQualifReport._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objEQualifReport._ReportTypeName = cboReportType.Text
            'Pinkal (22-Mar-2012) -- End


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objEQualifReport._Analysis_Fields = mstrAnalysis_Fields
            objEQualifReport._Analysis_Join = mstrAnalysis_Join
            objEQualifReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEQualifReport._Report_GroupName = mstrReport_GroupName
            'Anjan (17 May 2012)-End 

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEQualifReport._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmployeeQualificationReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEQualifReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeQualificationReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeQualificationReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objEQualifReport._ReportName
            Me._Message = objEQualifReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
            cboReportType.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeQualificationReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEQualifReport.generateReport(0, e.Type, enExportAction.None)
            objEQualifReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEQualifReport.generateReport(0, enPrintAction.None, e.Type)
            objEQualifReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeQualificationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeQualificationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END


    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try

            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            objEQualifReport.setOrderBy(cboReportType.SelectedIndex)
            'Pinkal (22-Mar-2012) -- End

            txtOrderBy.Text = objEQualifReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Anjan (17 May 2012)-End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sandeep [ 12 MARCH 2011 ] -- End 

#End Region

    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

#Region "ComboBox's Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) = 0 Then
                cboQualifiy.Enabled = True
                cboQualifiyGroup.Enabled = True
                cboInstitute.Enabled = True
            Else
                cboQualifiy.SelectedIndex = 0
                cboInstitute.SelectedIndex = 0
                cboQualifiyGroup.SelectedIndex = 0
                cboQualifiy.Enabled = False
                cboQualifiyGroup.Enabled = False
                cboInstitute.Enabled = False
            End If
            objEQualifReport._ReportTypeId = cboReportType.SelectedIndex
            objEQualifReport.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objEQualifReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Pinkal (22-Mar-2012) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
			Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
			Me.lblQualifyGroup.Text = Language._Object.getCaption(Me.lblQualifyGroup.Name, Me.lblQualifyGroup.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee With Education Details")
			Language.setMessage(mstrModuleName, 2, "Employee Without Education Details")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
