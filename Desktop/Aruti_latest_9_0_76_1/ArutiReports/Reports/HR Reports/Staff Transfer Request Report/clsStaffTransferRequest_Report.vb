
Imports eZeeCommonLib
Imports Aruti.Data
Imports System


Public Class clsStaffTransferRequest_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsStaffTransferRequest_Report"
    Private mstrReportId As String = enArutiReport.Staff_Transfer_Request_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintTransferRequestId As Integer = 0
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mdtRequestDate As Date = Nothing
    Private mintFromClassGroupId As Integer = 0
    Private mstrFromClassGroup As String = ""
    Private mintDestincationClassGroupId As Integer = 0
    Private mstrDestinationClassGroup As String = ""
    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _TransferRequestId() As Integer
        Set(ByVal value As Integer)
            mintTransferRequestId = value
        End Set
    End Property


    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _RequestDate() As Date
        Set(ByVal value As Date)
            mdtRequestDate = value
        End Set
    End Property

    Public WriteOnly Property _FromClassGroupId() As Integer
        Set(ByVal value As Integer)
            mintFromClassGroupId = value
        End Set
    End Property

    Public WriteOnly Property _FromClassGroup() As String
        Set(ByVal value As String)
            mstrFromClassGroup = value
        End Set
    End Property

    Public WriteOnly Property _DestinationClassGroupId() As Integer
        Set(ByVal value As Integer)
            mintDestincationClassGroupId = value
        End Set
    End Property

    Public WriteOnly Property _DestincationClassGroup() As String
        Set(ByVal value As String)
            mstrDestinationClassGroup = value
        End Set
    End Property


    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintTransferRequestId = 0
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtRequestDate = Nothing
            mintFromClassGroupId = 0
            mstrFromClassGroup = ""
            mintDestincationClassGroupId = 0
            mstrDestinationClassGroup = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintTransferRequestId > 0 Then
                objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferRequestId)
                Me._FilterQuery &= " AND sttransfer_request_tran.transferrequestunkid  = @transferrequestunkid "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND sttransfer_request_tran.employeeunkid  = @EmployeeId "
            End If

            If mintFromClassGroupId > 0 Then
                objDataOperation.AddParameter("@FromClassGroupId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromClassGroupId)
                Me._FilterQuery &= " AND cgm.classgroupunkid = @FromClassGroupId "
            End If

            If mintDestincationClassGroupId > 0 Then
                objDataOperation.AddParameter("@DestinationClassGroupId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDestincationClassGroupId)
                Me._FilterQuery &= " AND sttransfer_request_tran.classgroupunkid = @DestinationClassGroupId "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                               , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date _
                                                               , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String _
                                                               , ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                               , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                               , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            Dim mintCountryId As Integer = Company._Object._Countryunkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_TransferApprovers As ArutiReport.Designer.dsArutiReport


        Try
            objDataOperation = New clsDataOperation


            StrQ = " SELECT " & _
                       "    sttransfer_request_tran.employeeunkid " & _
                       ",   ie.photo As EmpPhoto " & _
                       ",   sttransfer_request_tran.transferrequestunkid " & _
                       ",   sttransfer_request_tran.requestdate " & _
                       ",   ISNULL(hremployee_master.employeecode,'') AS Employeecode " & _
                       ",   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                       ",   ISNULL(hrdepartment_master.name,'') AS Department " & _
                       ",   ISNULL(hrjob_master.job_name,'') AS Job " & _
                       ",   ISNULL(hrsectiongroup_master.name,'') AS SectionGroup " & _
                       ",   ISNULL(cgm.name,'') AS ClassGroup " & _
                       ",   ISNULL(cm.name,'') AS Class " & _
                       ",   ISNULL(hrgradegroup_master.name,'') AS GradeGroup " & _
                       ",   ISNULL(rcgm.name,'') AS RequestedClassGrp " & _
                       ",   ISNULL(rcm.name,'') AS RequestedClass " & _
                       ",   ISNULL(sttransfer_request_tran.remark,'') AS Remark " & _
                       " FROM sttransfer_request_tran " & _
                       " JOIN hremployee_master ON sttransfer_request_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       " LEFT JOIN arutiimages..hremployee_images ie ON sttransfer_request_tran.employeeunkid = ie.employeeunkid AND ie.isvoid = 0" & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "        SELECT " & _
                       "             jobunkid " & _
                       "            ,employeeunkid " & _
                       "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "        FROM hremployee_categorization_tran " & _
                       "        WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmpAsOnDate " & _
                       " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                       " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "            SELECT " & _
                       "                gradeunkid " & _
                       "               ,gradegroupunkid " & _
                       "               ,employeeunkid " & _
                       "               ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                       "            FROM prsalaryincrement_tran " & _
                       "            WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= @EmpAsOnDate " & _
                       " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                       " LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = Grds.gradegroupunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "            SELECT " & _
                       "                departmentunkid " & _
                       "               ,sectiongroupunkid " & _
                       "               ,classgroupunkid " & _
                       "               ,classunkid " & _
                       "               ,employeeunkid " & _
                       "               ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "            FROM hremployee_transfer_tran " & _
                       "            WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmpAsOnDate " & _
                       " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                       " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid " & _
                       " LEFT JOIN hrclassgroup_master cgm ON cgm.classgroupunkid = Alloc.classgroupunkid " & _
                       " LEFT JOIN hrclasses_master cm ON cm.classesunkid = Alloc.classunkid " & _
                       " LEFT JOIN hrclassgroup_master rcgm ON rcgm.classgroupunkid = sttransfer_request_tran.classgroupunkid " & _
                       " LEFT JOIN hrclasses_master rcm ON rcm.classesunkid = sttransfer_request_tran.classunkid " & _
                       " WHERE sttransfer_request_tran.isvoid = 0 "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmpAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_TransferApprovers = New ArutiReport.Designer.dsArutiReport


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = CDate(dtRow.Item("requestdate")).ToShortDateString()
                rpt_Rows.Item("Column2") = dtRow.Item("Employeecode")
                rpt_Rows.Item("Column3") = dtRow.Item("Employee")
                rpt_Rows.Item("Column4") = dtRow.Item("Department")
                rpt_Rows.Item("Column5") = dtRow.Item("Job")
                rpt_Rows.Item("Column6") = dtRow.Item("SectionGroup")
                rpt_Rows.Item("Column7") = dtRow.Item("ClassGroup")
                rpt_Rows.Item("Column8") = dtRow.Item("Class")
                rpt_Rows.Item("Column9") = dtRow.Item("GradeGroup")
                rpt_Rows.Item("Column10") = dtRow.Item("RequestedClassGrp")
                rpt_Rows.Item("Column11") = dtRow.Item("RequestedClass")
                rpt_Rows.Item("Column12") = dtRow.Item("Remark")

                Dim mbytEmpPhoto As Byte() = Nothing
                If IsDBNull(dtRow.Item("EmpPhoto")) = False Then
                    Try
                        mbytEmpPhoto = clsSecurity.DecryptByte(CType(dtRow.Item("EmpPhoto"), Byte()), "ezee")
                    Catch ex As Exception
                        mbytEmpPhoto = CType(dtRow.Item("EmpPhoto"), Byte())
                    End Try
                    rpt_Rows.Item("sign1") = mbytEmpPhoto
                Else
                    Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    rpt_Rows.Item("sign1") = eZeeDataType.image2Data(imgBlank)
                End If
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then mintTransferRequestId = 0

            StrQ = " SELECT " & _
                      "       sttransfer_approval_tran.mapuserunkid " & _
                      "       ,sttransfer_approval_tran.levelunkid " & _
                      "       ,ISNULL(stapproverlevel_master.stlevelname ,'') AS Level " & _
                      "       ,ISNULL(cr.name ,'') AS Role " & _
                      "       ,CASE WHEN ISNULL(cu.firstname,'') <> '' THEN ISNULL(cu.firstname,'') + ' ' + ISNULL(cu.lastname,'') ELSE ISNULL(cu.username,'') END AS Approver " & _
                      "       ,stapproverlevel_master.priority " & _
                      "       ,sttransfer_approval_tran.approvaldate " & _
                      "       ,sttransfer_approval_tran.statusunkid " & _
                      "       ,CASE WHEN sttransfer_approval_tran.statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Pending & " THEN  @Pending " & _
                      "                 WHEN sttransfer_approval_tran.statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Approved & " THEN  @Approve END status " & _
                      "       ,sttransfer_approval_tran.approvalremark " & _
                      " FROM sttransfer_approval_tran " & _
                      " JOIN sttransfer_request_tran ON sttransfer_request_tran.transferrequestunkid = sttransfer_approval_tran.transferrequestunkid " & _
                      " AND sttransfer_approval_tran.employeeunkid = sttransfer_request_tran.employeeunkid AND sttransfer_request_tran.isvoid = 0 " & _
                      " LEFT JOIN stapproverlevel_role_mapping ON sttransfer_approval_tran.stmappingunkid = sttransfer_approval_tran.stmappingunkid " & _
                      " AND stapproverlevel_role_mapping.levelunkid = sttransfer_approval_tran.levelunkid AND stapproverlevel_role_mapping.isvoid = 0 " & _
                      " LEFT JOIN stapproverlevel_master ON stapproverlevel_master.stlevelunkid = sttransfer_approval_tran.levelunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfrole_master cr ON cr.roleunkid= sttransfer_approval_tran.roleunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master cu ON cu.userunkid = sttransfer_approval_tran.mapuserunkid " & _
                      " WHERE sttransfer_approval_tran.isvoid = 0 AND sttransfer_request_tran.transferrequestunkid = @transferrequestunkid " & _
                      " ORDER BY stapproverlevel_master.priority "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferRequestId)
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstransfer_request_tran", 2, "Pending"))
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstransfer_request_tran", 3, "Approved"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim mintPriorty As Integer = -1

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
Recalculate:
                Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> " & clstransfer_request_tran.enTransferRequestStatus.Pending & " AND priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Pending & " AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dsList.Tables(0).Rows.Remove(dRow)
                                    GoTo Recalculate
                                Next
                                dsList.AcceptChanges()
                            End If
                        End If
                    End If
                Next
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_TransferApprovers.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("Level")
                rpt_Row.Item("Column2") = dtRow.Item("Role")
                rpt_Row.Item("Column3") = dtRow.Item("Approver")

                If Not IsDBNull(dtRow.Item("approvaldate")) Then
                    rpt_Row.Item("Column4") = CDate(dtRow.Item("approvaldate")).ToShortDateString
                Else
                    rpt_Row.Item("Column4") = ""
                End If
                rpt_Row.Item("Column5") = dtRow.Item("status")
                rpt_Row.Item("Column6") = dtRow.Item("approvalremark")

                rpt_TransferApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            dsList = Nothing


            objRpt = New ArutiReport.Designer.rptStaffTransferRequestForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

            If Company._Object._Address1.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress1", True)
            End If

            Dim mstrAddress As String = String.Empty

            If Company._Object._Address2.ToString.Trim <> "" Then
                mstrAddress = Company._Object._Address2.ToString.Trim
            End If

            If Company._Object._Post_Code_No.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Post_Code_No.ToString.Trim
            End If

            If Company._Object._City_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._City_Name.ToString.Trim
            End If

            If Company._Object._State_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._State_Name.ToString.Trim
            End If

            If Company._Object._Country_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Country_Name.ToString.Trim
            End If

            If mstrAddress.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtAddress2", mstrAddress)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress2", True)
            End If


            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtRequestDate", Language.getMessage(mstrModuleName, 1, "Requested Date :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 2, "Employee Code :"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 3, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 4, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 5, "Job :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 6, "Department :"))
            Call ReportFunction.TextChange(objRpt, "txtSectionGrp", Language.getMessage(mstrModuleName, 7, "Section Group :"))
            Call ReportFunction.TextChange(objRpt, "txtClassGroup", Language.getMessage(mstrModuleName, 9, "Class Group :"))
            Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 10, "Class :"))
            Call ReportFunction.TextChange(objRpt, "txtGradeGroup", Language.getMessage(mstrModuleName, 11, "Grande Group:"))
            Call ReportFunction.TextChange(objRpt, "txtRequestDetail", Language.getMessage(mstrModuleName, 12, "REQUEST DETAILS"))
            Call ReportFunction.TextChange(objRpt, "txtRequestedClassGrp", Language.getMessage(mstrModuleName, 13, "Requested Class Group :"))
            Call ReportFunction.TextChange(objRpt, "txtRequestedClass", Language.getMessage(mstrModuleName, 14, "Requested Class :"))
            Call ReportFunction.TextChange(objRpt, "txtRemarks", Language.getMessage(mstrModuleName, 15, "Remark :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", Language.getMessage(mstrModuleName, 24, "EMPLOYEE PARTICULARS"))

            objRpt.Subreports("rptTransferApprovalDetailsReport").SetDataSource(rpt_TransferApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransferApprovalDetailsReport"), "txtLevel", Language.getMessage(mstrModuleName, 16, "Level"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransferApprovalDetailsReport"), "txtRole", Language.getMessage(mstrModuleName, 17, "Role"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransferApprovalDetailsReport"), "txtApprover", Language.getMessage(mstrModuleName, 18, "Approver"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransferApprovalDetailsReport"), "txtApprovalDate", Language.getMessage(mstrModuleName, 19, "Approval Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransferApprovalDetailsReport"), "txtStatus", Language.getMessage(mstrModuleName, 20, "Approval Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransferApprovalDetailsReport"), "txtRemarks", Language.getMessage(mstrModuleName, 21, "Approval Remark"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransferApprovalDetailsReport"), "txtApprovals", Language.getMessage(mstrModuleName, 25, "APPROVALS"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 22, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 23, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Sub Send_ETransferRequestForm(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String, ByVal StrFileName As String, ByVal StrFilePath As String)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            mintCompanyUnkid = xCompanyUnkid
            Company._Object._Companyunkid = mintCompanyUnkid
            mintUserUnkid = xUserUnkid
            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, xOnlyApproved, xUserModeSetting)

            If Not IsNothing(objRpt) Then
                Dim oStream As New IO.MemoryStream
                oStream = objRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
                Dim fs As New System.IO.FileStream(StrFilePath & "\" & StrFileName.Replace(" ", "_") & ".pdf", IO.FileMode.Create, IO.FileAccess.Write)
                Dim data As Byte() = oStream.ToArray()
                fs.Write(data, 0, data.Length)
                fs.Close()
                fs.Dispose()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_ETransferRequestForm; Module Name: " & mstrModuleName)
        Finally
            objRpt.Dispose()
        End Try
    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clstransfer_request_tran", 2, "Pending")
            Language.setMessage("clstransfer_request_tran", 3, "Approved")
            Language.setMessage(mstrModuleName, 1, "Requested Date :")
            Language.setMessage(mstrModuleName, 2, "Employee Code :")
            Language.setMessage(mstrModuleName, 3, "Employee :")
            Language.setMessage(mstrModuleName, 4, "Name :")
            Language.setMessage(mstrModuleName, 5, "Job :")
            Language.setMessage(mstrModuleName, 6, "Department :")
            Language.setMessage(mstrModuleName, 7, "Section Group :")
            Language.setMessage(mstrModuleName, 9, "Class Group :")
            Language.setMessage(mstrModuleName, 10, "Class :")
            Language.setMessage(mstrModuleName, 11, "Grande Group:")
            Language.setMessage(mstrModuleName, 12, "REQUEST DETAILS")
            Language.setMessage(mstrModuleName, 13, "Requested Class Group :")
            Language.setMessage(mstrModuleName, 14, "Requested Class :")
            Language.setMessage(mstrModuleName, 15, "Remark :")
            Language.setMessage(mstrModuleName, 16, "Level")
            Language.setMessage(mstrModuleName, 17, "Role")
            Language.setMessage(mstrModuleName, 18, "Approver")
            Language.setMessage(mstrModuleName, 19, "Approval Date")
            Language.setMessage(mstrModuleName, 20, "Approval Status")
            Language.setMessage(mstrModuleName, 21, "Approval Remark")
            Language.setMessage(mstrModuleName, 22, "Printed By :")
            Language.setMessage(mstrModuleName, 23, "Printed Date :")
            Language.setMessage(mstrModuleName, 24, "EMPLOYEE PARTICULARS")
            Language.setMessage(mstrModuleName, 25, "APPROVALS")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
