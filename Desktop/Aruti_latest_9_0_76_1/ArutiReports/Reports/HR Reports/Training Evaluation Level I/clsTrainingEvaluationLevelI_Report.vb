'************************************************************************************************************************************
'Class Name : clsTrainingEvaluationLevelI_Report.vb
'Purpose    : 
'Date       : 06/04/2012
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsTrainingEvaluationLevelI_Report
    Inherits IReportData
    Private Const mstrModuleName = "clsTrainingEvaluationLevelI_Report"
    Private mstrReportId As String = ""  '79
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintFeebackMasterId As Integer = -1

#End Region

#Region " Properties "

    Public WriteOnly Property _FeebackMasterId() As Integer
        Set(ByVal value As Integer)
            mintFeebackMasterId = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintFeebackMasterId = -1
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                    "	  cfcommon_master.name AS Title " & _
                    "	 ,CONVERT(CHAR(8),start_date,112) AS SDate " & _
                    "	 ,CONVERT(CHAR(5),start_time,108) AS STime " & _
                    "	 ,CONVERT(CHAR(8),end_date,112) AS EDate " & _
                    "	 ,CONVERT(CHAR(5),end_time,108) AS ETime " & _
                    "	 ,ISNULL(STUFF((SELECT ',' + CAST(CASE WHEN A.employeeunkid <=0 THEN other_name ELSE ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') END AS NVARCHAR(50)) " & _
                    "					FROM hrtraining_trainers_tran AS A LEFT JOIN hremployee_master ON A.employeeunkid = hremployee_master.employeeunkid " & _
                    "					WHERE hrtraining_scheduling.trainingschedulingunkid = A.trainingschedulingunkid ORDER BY A.trainingschedulingunkid FOR XML PATH('')),1,1,''),'') AS Trainers " & _
                    "	 ,ISNULL(hrinstitute_master.institute_name,'') AS Institution " & _
                    "	 ,ISNULL(FGroup,'') AS FGroup " & _
                    "	 ,ISNULL(FItem,'') AS FItem " & _
                    "	 ,ISNULL(FSubItem,'') AS FSubItem " & _
                    "	 ,ISNULL(Result,'') AS Result " & _
                    "	 ,ISNULL(Employee,'') AS Employee " & _
                    "FROM hrtraining_scheduling " & _
                    "	LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = hrtraining_scheduling.traininginstituteunkid " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "' " & _
                    "	JOIN " & _
                    "	( " & _
                    "		SELECT trainingschedulingunkid " & _
                    "		,FGroup,FItem,FSubItem,Result,Employee " & _
                    "		FROM " & _
                    "		( " & _
                    "			SELECT " & _
                    "			 trainingschedulingunkid " & _
                    "			,ISNULL(hrtnafdbk_group_master.name,'') AS FGroup " & _
                    "			,ISNULL(hrtnafdbk_item_master.name,'') AS FItem " & _
                    "			,ISNULL(hrtnafdbk_subitem_master.name,'') AS FSubItem " & _
                    "			,CASE WHEN hrtnafeedback_tran.resultunkid <= 0 THEN other_result  ELSE ISNULL(hrresult_master.resultname,'') END AS Result " & _
                    "			,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Employee " & _
                    "		FROM hrtnafeedback_master " & _
                    "			JOIN hremployee_master ON hrtnafeedback_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "			JOIN hrtnafeedback_tran ON hrtnafeedback_master.feedbackmasterunkid = hrtnafeedback_tran.feedbackmasterunkid " & _
                    "			LEFT JOIN hrresult_master ON hrtnafeedback_tran.resultunkid = hrresult_master.resultunkid " & _
                    "			LEFT JOIN hrtnafdbk_subitem_master ON hrtnafeedback_tran.fdbksubitemunkid = hrtnafdbk_subitem_master.fdbksubitemunkid " & _
                    "			JOIN hrtnafdbk_item_master ON hrtnafeedback_tran.fdbkitemunkid = hrtnafdbk_item_master.fdbkitemunkid AND fromimpact = 0 " & _
                    "			JOIN hrtnafdbk_group_master ON hrtnafeedback_tran.fdbkgroupunkid = hrtnafdbk_group_master.fdbkgroupunkid " & _
                    "		WHERE hrtnafeedback_master.isvoid = 0 AND hrtnafeedback_tran.isvoid = 0 AND hrtnafeedback_master.feedbackmasterunkid = '" & mintFeebackMasterId & "' " & _
                    "		) AS A " & _
                    "	)AS B ON hrtraining_scheduling.trainingschedulingunkid = B.trainingschedulingunkid " & _
                    "WHERE isvoid = 0 AND iscancel = 0 ORDER BY ISNULL(FGroup,'') "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim StrGrp As String = String.Empty

            'rpt_Rows.Item("Column7") = intCnt.ToString & ". " & dtRow.Item("FItem")
            'rpt_Rows.Item("Column2") = eZeeDate.convertDate(dtRow.Item("SDate").ToString).ToShortDateString & " " & dtRow.Item("STime")
            'rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("EDate").ToString).ToShortDateString & " " & dtRow.Item("ETime")

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("Title")
                rpt_Rows.Item("Column2") = eZeeDate.convertDate(dtRow.Item("SDate").ToString).ToShortDateString
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("EDate").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = dtRow.Item("Trainers")
                rpt_Rows.Item("Column5") = dtRow.Item("Institution")
                rpt_Rows.Item("Column6") = dtRow.Item("FGroup")
                rpt_Rows.Item("Column7") = dtRow.Item("FItem")
                rpt_Rows.Item("Column8") = dtRow.Item("FSubItem")
                rpt_Rows.Item("Column9") = dtRow.Item("Result")
                rpt_Rows.Item("Column10") = dtRow.Item("Employee")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEvaluationLevelIReport

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            Dim objImg(0) As Object
            If Company._Object._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            Else
                Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If
            arrImageRow.Item("arutiLogo") = objImg(0)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtAnswer", Language.getMessage(mstrModuleName, 1, "Answer :"))
            Call ReportFunction.TextChange(objRpt, "txtCaption1", Language.getMessage(mstrModuleName, 2, "F-HRD – 020- B"))
            Call ReportFunction.TextChange(objRpt, "txtCompany", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtCaption2", Language.getMessage(mstrModuleName, 3, "END OF COURSE EVALUATION FORM"))
            Call ReportFunction.TextChange(objRpt, "txtCourseTitle", Language.getMessage(mstrModuleName, 4, "1. Course/Seminar Title"))
            Call ReportFunction.TextChange(objRpt, "txtDurationFrom", Language.getMessage(mstrModuleName, 5, "2. Course/Seminar Duration: From"))
            Call ReportFunction.TextChange(objRpt, "txtTraininer", Language.getMessage(mstrModuleName, 6, "3. Facilitator’s/Trainer’s Name:"))
            Call ReportFunction.TextChange(objRpt, "txtInstitution", Language.getMessage(mstrModuleName, 7, "4. Institution:"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 8, "Name:"))
            Call ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 9, "Signature:"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 10, "Date:"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 12, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
End Class
