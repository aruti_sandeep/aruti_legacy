'************************************************************************************************************************************
'Class Name : frmAssessmentAuditReport.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessmentAuditReport

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentAuditReport"
    Private objAssessAudit As clsAssessmentAuditReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing

#End Region

#Region " Constructor "

    Public Sub New()
        objAssessAudit = New clsAssessmentAuditReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAssessAudit.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 1)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With


            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "BSC Planning Audit Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Assessment Audit Report"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            mstrAdvanceFilter = ""
            chkConsiderEmployeeTermination.Checked = False
            objAssessAudit.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objAssessAudit.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAssessAudit.SetDefaultValue()
            objAssessAudit._ReportTypeId = CInt(cboReportType.SelectedValue)
            objAssessAudit._ReportTypeName = cboReportType.Text
            objAssessAudit._PeriodId = CInt(cboPeriod.SelectedValue)
            objAssessAudit._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objAssessAudit._ReportTypeName = cboReportType.Text
            objAssessAudit._PeriodName = cboPeriod.Text
            objAssessAudit._ReportModeId = CInt(cboReportMode.SelectedIndex)
            objAssessAudit._ReportModeName = cboReportMode.Text
            If CInt(cboStatus.SelectedIndex) <= 0 Then
                objAssessAudit._ViewIndex = 1
            Else
                objAssessAudit._ViewIndex = 0
            End If

            Select Case cboReportType.SelectedIndex
                Case 1
                    'S.SANDEEP |20-APR-2019| -- START
                    'objAssessAudit._StatusId = CInt(cboStatus.SelectedIndex)
                    objAssessAudit._StatusId = CInt(CType(cboStatus.SelectedItem, ComboBoxValue).Value)
                    'S.SANDEEP |20-APR-2019| -- END
                Case 2
                    objAssessAudit._StatusId = -1
                    If CInt(cboStatus.SelectedIndex) = 1 Then
                        objAssessAudit._StatusId = 1
                    ElseIf CInt(cboStatus.SelectedIndex) = 2 Then
                        objAssessAudit._StatusId = 0
                    End If
            End Select

            objAssessAudit._StatusName = cboStatus.Text
            objAssessAudit._Report_GroupName = "Status"
            objAssessAudit._EmployeeId = CInt(cboEmployee.SelectedValue)
            objAssessAudit._EmployeeName = cboEmployee.Text
            objAssessAudit._AdvanceFilter = mstrAdvanceFilter
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            cboStatus.Items.Clear() : cboReportMode.Items.Clear()
            Select Case cboReportType.SelectedIndex
                Case 0  'SELECT
                    cboStatus.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 4, "Select"), 0))
                    cboReportMode.Items.Add(New ComboBoxValue(Language.getMessage("frmNotAssessedEmployeeReport", 4, "Select"), 0))
                    cboReportMode.Enabled = False : gbSortBy.Enabled = False
                    cboStatus.SelectedIndex = 0
                Case 1  'BSC Planning Audit Report
                    With cboStatus
                        .Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 4, "Select"), 0))
                        .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 1, "Submitted For Approval"), enObjective_Status.SUBMIT_APPROVAL))
                        'S.SANDEEP |20-APR-2019| -- START
                        '.Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 100, "Final Approved"), enObjective_Status.FINAL_SAVE))
                        .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 101, "Approved"), enObjective_Status.FINAL_SAVE))
                        'S.SANDEEP |20-APR-2019| -- END
                        .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 3, "Opened For Changes"), enObjective_Status.OPEN_CHANGES))
                        .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 44, "Not Submitted For Approval"), enObjective_Status.NOT_SUBMIT))
                        'S.SANDEEP |20-APR-2019| -- START
                        .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 51, "Periodic Review"), enObjective_Status.PERIODIC_REVIEW))
                        'S.SANDEEP |20-APR-2019| -- END
                        .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 5, "Not Planned"), 999))
                    End With
                    cboReportMode.Enabled = False
                    cboStatus.SelectedIndex = 0
                Case 2  'Assessment Audit Report
                    cboReportMode.Enabled = True
                    With cboStatus
                        .Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 4, "Select"), 0))
                        .Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 6, "Committed Assessment"), 1))
                        .Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 7, "Not Committed Assessment"), 1))
                    End With

                    With cboReportMode
                        .Items.Clear()
                        .Items.Add(New ComboBoxValue(Language.getMessage("frmNotAssessedEmployeeReport", 4, "Select"), 0))
                        .Items.Add(New ComboBoxValue(Language.getMessage("frmNotAssessedEmployeeReport", 55, "Self Assessment"), 1))
                        .Items.Add(New ComboBoxValue(Language.getMessage("frmNotAssessedEmployeeReport", 6, "Assessor Assessment"), 2))
                        .Items.Add(New ComboBoxValue(Language.getMessage("frmNotAssessedEmployeeReport", 7, "Reviewer Assessment"), 3))
                    End With

                    cboStatus.SelectedIndex = 0
                    cboReportMode.SelectedIndex = 0
            End Select
            If cboReportType.SelectedIndex > 0 Then
                gbSortBy.Enabled = True
                objAssessAudit.setDefaultOrderBy(cboReportType.SelectedIndex)
                txtOrderBy.Text = objAssessAudit.OrderByDisplay
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPrd._Start_Date
                mdtEndDate = objPrd._End_Date
                objPrd = Nothing
            Else
                mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

   

#End Region

#Region " Forms "

    Private Sub frmAssessmentAuditReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAssessAudit = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAssessmentAuditReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentAuditReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.EZeeHeader1.Title = objAssessAudit._ReportName
            Me.EZeeHeader1.Message = objAssessAudit._ReportDesc

            Call FillCombo()

            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentAuditReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            If CInt(cboReportType.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select Report Type."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            If CInt(cboPeriod.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If CInt(cboReportType.SelectedIndex) = 2 AndAlso CInt(cboReportMode.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Select Report Mode."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Sub
            End If

            'S.SANDEEP |25-APR-2019| -- START
            If chkConsiderEmployeeTermination.Checked = False Then
                mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date
                mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date
            End If
            'S.SANDEEP |25-APR-2019| -- END

            objAssessAudit.Export_Assessment_Report(CInt(cboReportType.SelectedIndex), _
                                                    FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    mdtStartDate, mdtEndDate, _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                    , enExportAction.Excel)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessmentAuditReport.SetMessages()
            clsBSC_Planning_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessmentAuditReport,clsBSC_Planning_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s)"
    Private Sub chkConsiderEmployeeTermination_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConsiderEmployeeTermination.CheckedChanged
        Try
            If chkConsiderEmployeeTermination.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPrd._Start_Date
                mdtEndDate = objPrd._End_Date
                objPrd = Nothing
            Else
                mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkConsiderEmployeeTermination_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objAssessAudit.setOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objAssessAudit.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkConsiderEmployeeTermination.Text = Language._Object.getCaption(Me.chkConsiderEmployeeTermination.Name, Me.chkConsiderEmployeeTermination.Text)
			Me.lblRType.Text = Language._Object.getCaption(Me.lblRType.Name, Me.lblRType.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblReportMode.Text = Language._Object.getCaption(Me.lblReportMode.Name, Me.lblReportMode.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
            Language.setMessage("clsBSC_Planning_Report", 1, "Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 3, "Opened For Changes")
            Language.setMessage("clsBSC_Planning_Report", 5, "Not Planned")
            Language.setMessage("clsBSC_Planning_Report", 44, "Not Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 51, "Periodic Review")
            Language.setMessage("clsBSC_Planning_Report", 101, "Approved")
            Language.setMessage("frmNotAssessedEmployeeReport", 4, "Select")
            Language.setMessage("frmNotAssessedEmployeeReport", 6, "Assessor Assessment")
            Language.setMessage("frmNotAssessedEmployeeReport", 7, "Reviewer Assessment")
            Language.setMessage("frmNotAssessedEmployeeReport", 55, "Self Assessment")
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "BSC Planning Audit Report")
			Language.setMessage(mstrModuleName, 3, "Assessment Audit Report")
			Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Please Select Report Type.")
			Language.setMessage(mstrModuleName, 6, "Committed Assessment")
			Language.setMessage(mstrModuleName, 7, "Not Committed Assessment")
			Language.setMessage(mstrModuleName, 8, "Please Select Report Mode.")
			Language.setMessage(mstrModuleName, 9, "Please Select Period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
