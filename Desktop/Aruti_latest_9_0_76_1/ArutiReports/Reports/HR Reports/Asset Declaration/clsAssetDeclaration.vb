'************************************************************************************************************************************
'Class Name : clsAssetDeclaration.vb
'Purpose    :
'Date       :03/04/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class
''' Developer: Sohail
''' </summary>
Public Class clsAssetDeclaration
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAssetDeclaration"
    Dim objDataOperation As clsDataOperation
    Dim mstrCompanyCode As String = Company._Object._Code
    Dim mstrCompanyName As String = Company._Object._Name

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass 'Sohail (11 Apr 2012)
#Region " Private variables "

    Private mintAssetDeclarationUnkid As Integer = -1
    Private mintEmployeeUnkid As Integer = -1
    Private mstrEmployeeCode As String = String.Empty
    Private mstrEmployeeName As String = String.Empty
    Private mstrWorkStation As String = String.Empty
    Private mstrTitle As String = String.Empty
    Private mstrNonOfficialDeclaration As String = String.Empty 'Sohail (29 Jan 2013)

    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private objCompany As New clsCompany_Master
    Private objConfig As New clsConfigOptions
    Private mintCompanyUnkid As Integer = -1
    'Sohail (23 Apr 2012) -- End


    'Anjan [ 22 Nov 2013 ] -- Start
    'ENHANCEMENT : Requested by Rutta
    Private mintLanguageid As Integer = 0
    'Anjan [22 Nov 2013 ] -- End
    Private mstrDatabaseName As String = "" 'Sohail (16 Apr 2015)

#End Region

#Region " Properties "
    Public WriteOnly Property _AssetDeclarationUnkid() As Integer
        Set(ByVal value As Integer)
            mintAssetDeclarationUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _WorkStation() As String
        Set(ByVal value As String)
            mstrWorkStation = value
        End Set
    End Property

    Public WriteOnly Property _Title() As String
        Set(ByVal value As String)
            mstrTitle = value
        End Set
    End Property

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _NonOfficialDeclaration() As String
        Set(ByVal value As String)
            mstrNonOfficialDeclaration = value
        End Set
    End Property
    'Sohail (29 Jan 2013) -- End


    'Sohail (11 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property
    'Sohail (11 Apr 2012) -- End

    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
    'Sohail (23 Apr 2012) -- End


    'Anjan [ 22 Nov 2013 ] -- Start
    'ENHANCEMENT : Requested by Rutta
    Public WriteOnly Property _LanguageId() As Integer
        Set(ByVal value As Integer)
            mintLanguageid = value
        End Set
    End Property
    'Anjan [22 Nov 2013 ] -- End

    'Sohail (16 Apr 2015) -- Start
    'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
    'Sohail (16 Apr 2015) -- End

#End Region

#Region " Report Generation "

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Sohail (23 Apr 2012) -- Start
        '    'TRA - ENHANCEMENT
        '    If mintCompanyUnkid <= 0 Then mintCompanyUnkid = Company._Object._Companyunkid
        '    objCompany._Companyunkid = mintCompanyUnkid
        '    objConfig._Companyunkid = mintCompanyUnkid
        '    mstrCompanyCode = objCompany._Code
        '    mstrCompanyName = objCompany._Name

        '    'Sohail (23 Apr 2012) -- End

        '    objRpt = Generate_DetailReport(FinancialYear._Object._DatabaseName)
        '    Rpt = objRpt 'Sohail (11 Apr 2012)
        '    If Not IsNothing(objRpt) Then
        '        'Sohail (11 Apr 2012) -- Start
        '        'TRA - ENHANCEMENT
        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '        'Sohail (23 Apr 2012) -- Start
        '        'TRA - ENHANCEMENT
        '        'Dim strPath As String = ConfigParameter._Object._ExportReportPath
        '        Dim strPath As String = objConfig._ExportReportPath
        '        'Sohail (23 Apr 2012) -- End
        '        If Right(strPath, 1) = "\" Then
        '            strPath = Left(strPath, Len(strPath) - 1)
        '        End If
        '        Me._ReportName = "Asset Declaration"
        '        'Sohail (23 Apr 2012) -- Start
        '        'TRA - ENHANCEMENT
        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, strPath, ConfigParameter._Object._OpenAfterExport)
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, strPath, objConfig._OpenAfterExport)
        '        'Sohail (23 Apr 2012) -- End
        '        'Sohail (11 Apr 2012) -- End
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then mintCompanyUnkid = xCompanyUnkid
            objCompany._Companyunkid = mintCompanyUnkid
            objConfig._Companyunkid = mintCompanyUnkid
            mstrCompanyCode = objCompany._Code
            mstrCompanyName = objCompany._Name

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt
            If Not IsNothing(objRpt) Then
                Dim strPath As String = xExportReportPath

                If Right(strPath, 1) = "\" Then
                    strPath = Left(strPath, Len(strPath) - 1)
                End If
                Me._ReportName = "Asset Declaration"

                Call ReportExecute(objRpt, PrintAction, ExportAction, strPath, xOpenReportAfterExport)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet

        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rptRow As DataRow

        Dim objAssetDeclaration As New clsAssetdeclaration_master
        Dim objAssetBank As New clsAsset_bank_tran
        Dim objAssetShareDividend As New clsAsset_sharedividend_tran
        Dim objAssetHouse As New clsAsset_housebuilding_tran
        Dim objAssetPark As New clsAsset_parkfarmmines_tran
        Dim objAssetVehicle As New clsAsset_vehicles_tran
        Dim objAssetMachinery As New clsAsset_machinery_tran
        Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran
        'Sohail (29 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        Dim objAssetResources As New clsAsset_otherresources_tran
        Dim objAssetDebts As New clsAsset_debts_tran
        'Sohail (29 Jan 2013) -- End

        Dim rpt_DataBank As DataTable
        Dim rpt_DataShareDividend As DataTable
        Dim rpt_DataHouse As DataTable
        Dim rpt_DataPark As DataTable
        Dim rpt_DataVehicle As DataTable
        Dim rpt_DataMachinery As DataTable
        Dim rpt_DataOtherBusiness As DataTable
        'Sohail (29 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        Dim rpt_DataResources As DataTable
        Dim rpt_DataDebt As DataTable
        'Sohail (29 Jan 2013) -- End


        Dim dtBank As DataTable
        Dim dtShareDividend As DataTable
        Dim dtHouse As DataTable
        Dim dtPark As DataTable
        Dim dtVehicle As DataTable
        Dim dtMachinery As DataTable
        Dim dtOtherBusiness As DataTable
        'Sohail (29 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        Dim dtResources As DataTable
        Dim dtDebt As DataTable
        'Sohail (29 Jan 2013) -- End

        Try
            objDataOperation = New clsDataOperation

            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim dicCurr As New Dictionary(Of Integer, String)
            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", False)
            For Each dsRow As DataRow In dsList.Tables("Currency").Rows
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'dicCurr.Add(CInt(dsRow.Item("exchangerateunkid")), dsRow.Item("currency_sign").ToString)
                dicCurr.Add(CInt(dsRow.Item("countryunkid")), dsRow.Item("currency_sign").ToString)
                'Sohail (29 Jan 2013) -- End
            Next
            objCurrency = Nothing
            'Sohail (06 Apr 2012) -- End

            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
            If mstrDatabaseName.Trim = "" Then mstrDatabaseName = strDatabaseName
            'Shani(24-Aug-2015) -- End

            objAssetDeclaration._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objAssetDeclaration._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            rptRow = rpt_Data.Tables("ArutiTable").NewRow

            rptRow.Item("column1") = mintEmployeeUnkid.ToString
            rptRow.Item("column2") = mstrEmployeeCode
            rptRow.Item("column3") = mstrEmployeeName
            rptRow.Item("column4") = mstrWorkStation
            rptRow.Item("column5") = mstrTitle


            rptRow.Item("column21") = objAssetDeclaration._Resources
            rptRow.Item("column22") = objAssetDeclaration._Debt
            rptRow.Item("column23") = mstrCompanyName

            rpt_Data.Tables("ArutiTable").Rows.Add(rptRow)


            'Bank
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetBank._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetBank._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetBank._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtBank = objAssetBank._Datasource
            rpt_DataBank = rpt_Data.Tables("ArutiTable").Clone
            For Each dtRow As DataRow In dtBank.Rows
                rptRow = rpt_DataBank.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("bank").ToString
                rptRow.Item("column3") = dtRow.Item("account").ToString
                rptRow.Item("column4") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'If dicCurr.ContainsKey(CInt(dtRow.Item("currencyunkid"))) = True Then
                '    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("currencyunkid"))).ToString
                'Else
                '    rptRow.Item("column8") = ""
                'End If
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column8") = ""
                End If
                'Sohail (29 Jan 2013) -- End
                'Sohail (06 Apr 2012) -- End

                'rptRow.Item("column21") = Format(objAssetDeclaration._Cash, GUI.fmtCurrency)
                'rptRow.Item("column22") = Format(objAssetDeclaration._CashExistingBank, GUI.fmtCurrency)

                rpt_DataBank.Rows.Add(rptRow)
            Next


            'Share Dividend
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetShareDividend._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objAssetShareDividend._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtShareDividend = objAssetShareDividend._Datasource
            rpt_DataShareDividend = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtShareDividend.Rows
                rptRow = rpt_DataShareDividend.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = Format(CDec(dtRow.Item("sharevalue")), GUI.fmtCurrency)
                rptRow.Item("column3") = dtRow.Item("location").ToString
                rptRow.Item("column4") = Format(CDec(dtRow.Item("dividendamount")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'If dicCurr.ContainsKey(CInt(dtRow.Item("currencyunkid"))) = True Then
                '    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("currencyunkid"))).ToString
                'Else
                '    rptRow.Item("column8") = ""
                'End If
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column8") = ""
                End If
                'Sohail (29 Jan 2013) -- End
                'Sohail (06 Apr 2012) -- End

                rpt_DataShareDividend.Rows.Add(rptRow)
            Next


            'House Building
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetHouse._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetHouse._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetHouse._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtHouse = objAssetHouse._Datasource
            rpt_DataHouse = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtHouse.Rows
                rptRow = rpt_DataHouse.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("homebuilding").ToString
                rptRow.Item("column3") = dtRow.Item("location").ToString
                rptRow.Item("column4") = Format(CDec(dtRow.Item("value")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'If dicCurr.ContainsKey(CInt(dtRow.Item("currencyunkid"))) = True Then
                '    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("currencyunkid"))).ToString
                'Else
                '    rptRow.Item("column8") = ""
                'End If
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column8") = ""
                End If
                'Sohail (29 Jan 2013) -- End
                'Sohail (06 Apr 2012) -- End

                rpt_DataHouse.Rows.Add(rptRow)
            Next


            'Park Farms
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetPark._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetPark._Assetdeclarationunkid() = mintAssetDeclarationUnkid
            objAssetPark._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtPark = objAssetPark._Datasource
            rpt_DataPark = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtPark.Rows
                rptRow = rpt_DataPark.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("parkfarmmines").ToString
                rptRow.Item("column3") = dtRow.Item("sizeofarea").ToString
                rptRow.Item("column4") = dtRow.Item("placeclad").ToString
                rptRow.Item("column5") = Format(CDec(dtRow.Item("value")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column8") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'If dicCurr.ContainsKey(CInt(dtRow.Item("currencyunkid"))) = True Then
                '    rptRow.Item("column9") = dicCurr.Item(CInt(dtRow.Item("currencyunkid"))).ToString
                'Else
                '    rptRow.Item("column9") = ""
                'End If
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column9") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column9") = ""
                End If
                'Sohail (29 Jan 2013) -- End
                'Sohail (06 Apr 2012) -- End

                rpt_DataPark.Rows.Add(rptRow)
            Next


            'Vehicles
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetVehicle._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetVehicle._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetVehicle._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtVehicle = objAssetVehicle._Datasource
            rpt_DataVehicle = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtVehicle.Rows
                rptRow = rpt_DataVehicle.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("vehicletype").ToString
                rptRow.Item("column3") = Format(CDec(dtRow.Item("value")), GUI.fmtCurrency)
                rptRow.Item("column4") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'If dicCurr.ContainsKey(CInt(dtRow.Item("currencyunkid"))) = True Then
                '    rptRow.Item("column7") = dicCurr.Item(CInt(dtRow.Item("currencyunkid"))).ToString
                'Else
                '    rptRow.Item("column7") = ""
                'End If
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column7") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column7") = ""
                End If
                'Sohail (29 Jan 2013) -- End
                'Sohail (06 Apr 2012) -- End

                rpt_DataVehicle.Rows.Add(rptRow)
            Next


            'Machinery Facory
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetMachinery._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetMachinery._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetMachinery._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtMachinery = objAssetMachinery._Datasource
            rpt_DataMachinery = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtMachinery.Rows
                rptRow = rpt_DataMachinery.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("machines").ToString
                rptRow.Item("column3") = dtRow.Item("location").ToString
                rptRow.Item("column4") = Format(CDec(dtRow.Item("value")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'If dicCurr.ContainsKey(CInt(dtRow.Item("currencyunkid"))) = True Then
                '    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("currencyunkid"))).ToString
                'Else
                '    rptRow.Item("column8") = ""
                'End If
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column8") = ""
                End If
                'Sohail (29 Jan 2013) -- End
                'Sohail (06 Apr 2012) -- End

                rpt_DataMachinery.Rows.Add(rptRow)
            Next


            'Other Business
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetOtherBusiness._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetOtherBusiness._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetOtherBusiness._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtOtherBusiness = objAssetOtherBusiness._Datasource
            rpt_DataOtherBusiness = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtOtherBusiness.Rows
                rptRow = rpt_DataOtherBusiness.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("businesstype").ToString
                rptRow.Item("column3") = dtRow.Item("placeclad").ToString
                rptRow.Item("column4") = Format(CDec(dtRow.Item("value")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'If dicCurr.ContainsKey(CInt(dtRow.Item("currencyunkid"))) = True Then
                '    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("currencyunkid"))).ToString
                'Else
                '    rptRow.Item("column8") = ""
                'End If
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column8") = ""
                End If
                'Sohail (29 Jan 2013) -- End
                'Sohail (06 Apr 2012) -- End

                rpt_DataOtherBusiness.Rows.Add(rptRow)
            Next

            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'Other Resources
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetResources._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetResources._Assetdeclarationunkid() = mintAssetDeclarationUnkid
            objAssetResources._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtResources = objAssetResources._Datasource
            rpt_DataResources = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtResources.Rows
                rptRow = rpt_DataResources.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("otherresources").ToString
                rptRow.Item("column3") = dtRow.Item("location").ToString
                rptRow.Item("column4") = Format(CDec(dtRow.Item("value")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column8") = ""
                End If

                rpt_DataResources.Rows.Add(rptRow)
            Next

            'Debt
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objAssetDebts._DatabaseName = mstrDatabaseName
            'Sohail (16 Apr 2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDebts._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetDebts._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtDebt = objAssetDebts._Datasource
            rpt_DataDebt = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtDebt.Rows
                rptRow = rpt_DataDebt.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("debts").ToString
                rptRow.Item("column3") = dtRow.Item("location").ToString
                rptRow.Item("column4") = Format(CDec(dtRow.Item("value")), GUI.fmtCurrency)
                rptRow.Item("column5") = Format(CDec(dtRow.Item("servant")), GUI.fmtCurrency)
                rptRow.Item("column6") = Format(CDec(dtRow.Item("wifehusband")), GUI.fmtCurrency)
                rptRow.Item("column7") = Format(CDec(dtRow.Item("children")), GUI.fmtCurrency)
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column8") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column8") = ""
                End If

                rpt_DataDebt.Rows.Add(rptRow)
            Next
            'Sohail (29 Jan 2013) -- End

            objRpt = New ArutiReport.Designer.rptAssetDeclaration

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'ReportFunction.Logo_Display(objRpt, _
            '                                   True, _
            '                                    False, _
            '                                    "arutiLogo1", _
            '                                    "arutiLogo2", _
            '                                    arrImageRow, _
            '                                    , _
            '                                    , _
            '                                    , _
            '                                    ConfigParameter._Object._GetLeftMargin, _
            '                                    ConfigParameter._Object._GetRightMargin, _
            '                                    ConfigParameter._Object._TOPPageMarging)
            ReportFunction.Logo_Display(objRpt, _
                                               True, _
                                                False, _
                                                "arutiLogo1", _
                                                "arutiLogo2", _
                                                arrImageRow, _
                                                , _
                                                , _
                                                , _
                                                objConfig._GetLeftMargin, _
                                                objConfig._GetRightMargin, _
                                                objConfig._TOPPageMarging)
            'Sohail (23 Apr 2012) -- End

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            objRpt.SetDataSource(rpt_Data)

            objRpt.Subreports("rptADBank").SetDataSource(rpt_DataBank)
            objRpt.Subreports("rptADShareDividend").SetDataSource(rpt_DataShareDividend)
            objRpt.Subreports("rptADHouse").SetDataSource(rpt_DataHouse)
            objRpt.Subreports("rptADPlotFarm").SetDataSource(rpt_DataPark)
            objRpt.Subreports("rptADVehicles").SetDataSource(rpt_DataVehicle)
            objRpt.Subreports("rptADFactory").SetDataSource(rpt_DataMachinery)
            objRpt.Subreports("rptADOtherBusiness").SetDataSource(rpt_DataOtherBusiness)
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            objRpt.Subreports("rptADResources").SetDataSource(rpt_DataResources)
            objRpt.Subreports("rptADDebt").SetDataSource(rpt_DataDebt)
            'Sohail (29 Jan 2013) -- End

            'Anjan [ 22 Nov 2013 ] -- Start
            'ENHANCEMENT : Requested by Rutta
            ' Dim iLang As New clsLocalization(gApplicationType)
            'Anjan [22 Nov 2013 ] -- End

            Call ReportFunction.TextChange(objRpt, "lblFormCaption", Language.getMessage(mstrModuleName, 1, "ASSET AND DEBT DECLARATION", ))
            Call ReportFunction.TextChange(objRpt, "lblRegulation", Language.getMessage(mstrModuleName, 2, "[According to 2009 ") & mstrCompanyCode & Language.getMessage(mstrModuleName, 3, " staff Regulation ordinance sections 44, 45, 46, 47 and 48 ]", ))
            Call ReportFunction.TextChange(objRpt, "lblImpInfo", Language.getMessage(mstrModuleName, 4, "Important Information", ))

            Call ReportFunction.TextChange(objRpt, "lblOne", Language.getMessage(mstrModuleName, 5, "1."))
            Call ReportFunction.TextChange(objRpt, "txtOne", Language.getMessage(mstrModuleName, 6, "Before filling this form, please read instructions provided below carefully. You are also advised to read 2009 ") & mstrCompanyCode & Language.getMessage(mstrModuleName, 7, " staff Regulations ordinance. The list should clearly indicate personal and commercial assets and possessions."))

            Call ReportFunction.TextChange(objRpt, "lblTwo", Language.getMessage(mstrModuleName, 8, "2."))
            Call ReportFunction.TextChange(objRpt, "txtTwo", Language.getMessage(mstrModuleName, 9, "List the assets you and your wife (spouse) own. In addition, you have to list assets and possessions belonging to unmarried children under/or 18 years of age. The assets falling under the  following categories must be listed:"))

            Call ReportFunction.TextChange(objRpt, "lblThree", Language.getMessage(mstrModuleName, 10, "3."))
            Call ReportFunction.TextChange(objRpt, "txtThree", Language.getMessage(mstrModuleName, 11, "List all debt you owe"))

            Call ReportFunction.TextChange(objRpt, "lblFour", Language.getMessage(mstrModuleName, 12, "4."))
            Call ReportFunction.TextChange(objRpt, "txtFour", Language.getMessage(mstrModuleName, 13, "This declaration must be submitted to Principal Commissioner in thirty (30) days soon after being employed. Thereafter, declaration must be submitted at the beginning of each financial year and upon termination of employment with ") & mstrCompanyCode & ".")

            Call ReportFunction.TextChange(objRpt, "lblFive", Language.getMessage(mstrModuleName, 14, "5."))
            Call ReportFunction.TextChange(objRpt, "txtFive", Language.getMessage(mstrModuleName, 15, "It is against the law and regulations if an employee fails to submit asset and debt declaration form within specified timeframe as described in 2009 ") & mstrCompanyCode & Language.getMessage(mstrModuleName, 16, " staff regulations ordinance. In addition, it is also against the law and it is a criminal offence for employee to provide or declare false information in regard to his/her assets and ownership."))

            Call ReportFunction.TextChange(objRpt, "lblA", Language.getMessage(mstrModuleName, 17, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtA", Language.getMessage(mstrModuleName, 18, "Cash money and deposits or security in a bank or any other financial institutions."))

            Call ReportFunction.TextChange(objRpt, "lblB", Language.getMessage(mstrModuleName, 19, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtB", Language.getMessage(mstrModuleName, 20, "Shares and dividend from the company stock or shares in any company or corporation"))

            Call ReportFunction.TextChange(objRpt, "lblC", Language.getMessage(mstrModuleName, 21, "(c)"))
            Call ReportFunction.TextChange(objRpt, "txtC", Language.getMessage(mstrModuleName, 22, "Fixed assets such as land, farms or estate, mine, house or houses whose construction is incomplete."))

            Call ReportFunction.TextChange(objRpt, "lblD", Language.getMessage(mstrModuleName, 23, "(d)"))
            Call ReportFunction.TextChange(objRpt, "txtD", Language.getMessage(mstrModuleName, 24, "Motor vehicle or any other transportation or movable equipment"))

            Call ReportFunction.TextChange(objRpt, "lblE", Language.getMessage(mstrModuleName, 25, "(e)"))
            Call ReportFunction.TextChange(objRpt, "txtE", Language.getMessage(mstrModuleName, 26, "Milling, factory and/or industrial equipment"))

            Call ReportFunction.TextChange(objRpt, "lblF", Language.getMessage(mstrModuleName, 27, "(f)"))
            Call ReportFunction.TextChange(objRpt, "txtF", Language.getMessage(mstrModuleName, 28, "Benefits in any commercial activity or equipment"))

            Call ReportFunction.TextChange(objRpt, "lblIam", Language.getMessage(mstrModuleName, 29, "I am"))
            Call ReportFunction.TextChange(objRpt, "lblEmploymentNo", Language.getMessage(mstrModuleName, 30, "Employment No."))
            Call ReportFunction.TextChange(objRpt, "lblWorkStation", Language.getMessage(mstrModuleName, 31, "Work Station"))
            Call ReportFunction.TextChange(objRpt, "lblEmployedBy", Language.getMessage(mstrModuleName, 32, "After being employed by ") & mstrCompanyName & " (" & mstrCompanyCode & "), " & Language.getMessage(mstrModuleName, 33, "with the"))
            Call ReportFunction.TextChange(objRpt, "lblTitle", Language.getMessage(mstrModuleName, 34, "title"))
            Call ReportFunction.TextChange(objRpt, "lblIdeclare", Language.getMessage(mstrModuleName, 35, "I truthfully and sincerely declare having the following"))
            Call ReportFunction.TextChange(objRpt, "lblAssetDebt", Language.getMessage(mstrModuleName, 36, "assets and debts:"))

            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'Call ReportFunction.TextChange(objRpt, "lblEight", Language.getMessage(mstrModuleName, 37, "8."))
            'Call ReportFunction.TextChange(objRpt, "txtEight", Language.getMessage(mstrModuleName, 38, "Assets or any other commercial share/benefits such as livestock, poultry etc"))

            'Call ReportFunction.TextChange(objRpt, "lblNine", Language.getMessage(mstrModuleName, 39, "9."))
            'Call ReportFunction.TextChange(objRpt, "txtNine", Language.getMessage(mstrModuleName, 40, "Debts"))
            Call ReportFunction.TextChange(objRpt, "lblNonOfficialDeclaration", mstrNonOfficialDeclaration)
            If mstrNonOfficialDeclaration.Trim = "" Then
                Call ReportFunction.EnableSuppressSection(objRpt, "PHNOD", True)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "PHNOD", False)
            End If
            'Sohail (29 Jan 2013) -- End





            Call ReportFunction.TextChange(objRpt, "lblBeforeMe", Language.getMessage(mstrModuleName, 41, "This declaration is made and signed before me (Name)"))
            Call ReportFunction.TextChange(objRpt, "txtBeforeMe", Language.getMessage(mstrModuleName, 42, ""))
            Call ReportFunction.TextChange(objRpt, "lblAnd", Language.getMessage(mstrModuleName, 43, "and"))
            Call ReportFunction.TextChange(objRpt, "lblDeclarantName", Language.getMessage(mstrModuleName, 44, "Declarant's Name"))
            Call ReportFunction.TextChange(objRpt, "txtDeclarantName", Language.getMessage(mstrModuleName, 45, ""))
            Call ReportFunction.TextChange(objRpt, "lblSignOf", Language.getMessage(mstrModuleName, 46, "and Signature of"))
            Call ReportFunction.TextChange(objRpt, "lblDeclarant", Language.getMessage(mstrModuleName, 47, "Declarant"))
            Call ReportFunction.TextChange(objRpt, "txtDeclarant", Language.getMessage(mstrModuleName, 48, ""))
            Call ReportFunction.TextChange(objRpt, "lblKnowntoMe", Language.getMessage(mstrModuleName, 49, "who is known to me"))
            Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 50, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 51, ""))
            Call ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 52, "Month"))
            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 53, ""))
            Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 54, "Year"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 55, ""))
            Call ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 56, "Signature:"))
            Call ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 57, ""))
            Call ReportFunction.TextChange(objRpt, "lblFTitle", Language.getMessage(mstrModuleName, 58, "Title:"))
            Call ReportFunction.TextChange(objRpt, "txtFTitle", Language.getMessage(mstrModuleName, 59, ""))
            Call ReportFunction.TextChange(objRpt, "lblAddress", Language.getMessage(mstrModuleName, 60, "Address:"))
            Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 61, ""))
            Call ReportFunction.TextChange(objRpt, "lblMadeBefore", Language.getMessage(mstrModuleName, 62, "(This declaration must be made before the Commissioner for Oath)"))
            Call ReportFunction.TextChange(objRpt, "lblTo", Language.getMessage(mstrModuleName, 63, "TO:"))
            Call ReportFunction.TextChange(objRpt, "lblPrincipal", Language.getMessage(mstrModuleName, 64, "Principal Commissioner"))
            Call ReportFunction.TextChange(objRpt, "lblHeadquarter", mstrCompanyName & " (" & mstrCompanyCode & ")" & Language.getMessage(mstrModuleName, 65, ", Headquarters"))
            Call ReportFunction.TextChange(objRpt, "lblPOBox", Language.getMessage(mstrModuleName, 66, "P.O.BOX 11491"))
            Call ReportFunction.TextChange(objRpt, "lblCIty", Language.getMessage(mstrModuleName, 67, "Dar es Salaam"))
            Call ReportFunction.TextChange(objRpt, "lblCC", Language.getMessage(mstrModuleName, 68, "CC:"))
            Call ReportFunction.TextChange(objRpt, "txtcc1", Language.getMessage(mstrModuleName, 69, "The Head of department/ITA College Principal/ Zanzibar Assistant Commissioner/Regional Manager"))
            Call ReportFunction.TextChange(objRpt, "txtcc2", Language.getMessage(mstrModuleName, 70, "Name (The Head of Department/ ITA Principal Commissioner/Zanzibar Assistant Commissioner/Manager)"))
            Call ReportFunction.TextChange(objRpt, "lblCCSignature", Language.getMessage(mstrModuleName, 71, "Signature"))
            Call ReportFunction.TextChange(objRpt, "txtCCSignature", Language.getMessage(mstrModuleName, 72, ""))
            Call ReportFunction.TextChange(objRpt, "lblCCDate", Language.getMessage(mstrModuleName, 50, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtCCDate", Language.getMessage(mstrModuleName, 73, ""))
            Call ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 74, "NOTE: Leave copy to the Head of Department. Two copies must be filled out."))
            Call ReportFunction.TextChange(objRpt, "lblNote1", Language.getMessage(mstrModuleName, 75, "(1)	Principal Commissioner (2) The Head of Department/ ITA College Principal/Zanzibar Assistant Commissioner/Regional Manager."))

            '*** Bank
            objRpt.Subreports("rptADBank").DataDefinition.FormulaFields("fmCash").Text = "'" & Format(objAssetDeclaration._Cash, GUI.fmtCurrency) & "'"
            objRpt.Subreports("rptADBank").DataDefinition.FormulaFields("fmCashInBank").Text = "'" & Format(objAssetDeclaration._CashExistingBank, GUI.fmtCurrency) & "'"

            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblOneBank", Language.getMessage(mstrModuleName, 5, "1."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "txtOneBank", Language.getMessage(mstrModuleName, 76, "Money at time of feeling this form"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblABank", Language.getMessage(mstrModuleName, 77, "a)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblCash", Language.getMessage(mstrModuleName, 78, "Cash money"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblBBank", Language.getMessage(mstrModuleName, 79, "b)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblOtherMoney", Language.getMessage(mstrModuleName, 80, "Money in a bank or any other financial"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblInstitute", Language.getMessage(mstrModuleName, 81, "institution"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblBank", Language.getMessage(mstrModuleName, 82, "Bank/ Financial Institution"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblAcccountNo", Language.getMessage(mstrModuleName, 83, "Account No."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblAmountBank", Language.getMessage(mstrModuleName, 84, "Amount"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblPercBank", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblEmpBank", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblHWBank", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblChildBank", Language.getMessage(mstrModuleName, 88, "Children"))

            '*** Share Dividend
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblTwoShare", Language.getMessage(mstrModuleName, 8, "2."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "txtTwoShare", Language.getMessage(mstrModuleName, 89, "Share and Dividend from stock"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblShareValue", Language.getMessage(mstrModuleName, 90, "Value of Share"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblSharePlace", Language.getMessage(mstrModuleName, 91, "A place where Shares are"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblDividendAmount", Language.getMessage(mstrModuleName, 92, "Dividend Amount"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblPercShare", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblEmpShare", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblHWShare", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShareDividend"), "lblChildShare", Language.getMessage(mstrModuleName, 88, "Children"))

            '*** House Building
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblThreeHouse", Language.getMessage(mstrModuleName, 10, "3."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "txtThreeHouse", Language.getMessage(mstrModuleName, 93, "Houses and Real Estates (plus houses whose constructions are incomplete)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblHouse", Language.getMessage(mstrModuleName, 94, "House/Building"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblLocationHouse", Language.getMessage(mstrModuleName, 95, "Location"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblMonetaryHouse", Language.getMessage(mstrModuleName, 96, "Monetary Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblPercHouse", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblEmpHouse", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblHWHouse", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADHouse"), "lblChildHouse", Language.getMessage(mstrModuleName, 88, "Children"))

            '*** Park Farm
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblFourFarm", Language.getMessage(mstrModuleName, 12, "4."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "txtFourFarm", Language.getMessage(mstrModuleName, 97, "Land, Plots, Farms, or Mines"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblPlotFarm", Language.getMessage(mstrModuleName, 98, "Plots, Farms, Mines"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblAreaSizeFarm", Language.getMessage(mstrModuleName, 99, "The size of the area"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblPlaceFarm", Language.getMessage(mstrModuleName, 100, "Name of the place where it is located"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblPercFarm", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblEmpFarm", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblHWFarm", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblChildFarm", Language.getMessage(mstrModuleName, 88, "Children"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADPlotFarm"), "lblMonetaryPark", Language.getMessage(mstrModuleName, 96, "Monetary Value")) 'Sohail (06 Apr 2012)

            '*** Vehicles
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "lblFiveVehicle", Language.getMessage(mstrModuleName, 14, "5."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "txtFiveVehicle", Language.getMessage(mstrModuleName, 101, "Motor Vehicle or any other transportation equipment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "lblVehicleType", Language.getMessage(mstrModuleName, 102, "Type of Vehicle/Motor cycle and plate No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "lblMonetaryVehicle", Language.getMessage(mstrModuleName, 96, "Monetary Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "lblPercVehicle", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "lblEmpVehicle", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "lblHWVehicle", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADVehicles"), "lblChildVehicle", Language.getMessage(mstrModuleName, 88, "Children"))

            '*** Factory
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblSixFactory", Language.getMessage(mstrModuleName, 103, "6."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "txtSixFactory", Language.getMessage(mstrModuleName, 104, "Milling, factory, and industrial equipments"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblFactory", Language.getMessage(mstrModuleName, 105, "Milling, factory or industrial equipment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblLocationFactory", Language.getMessage(mstrModuleName, 95, "Location"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblMonetaryFactory", Language.getMessage(mstrModuleName, 96, "Monetary Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblPercFactory", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblEmpFactory", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblHWFactory", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADFactory"), "lblChildFactory", Language.getMessage(mstrModuleName, 88, "Children"))

            '*** Other Business
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblSevenBusiness", Language.getMessage(mstrModuleName, 106, "7."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "txtSevenBusiness", Language.getMessage(mstrModuleName, 107, "Other businesses such as bars, shops, lodges, guest houses, hotels etc"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblBusinessType", Language.getMessage(mstrModuleName, 108, "Type of business equipment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblLocationBusiness", Language.getMessage(mstrModuleName, 95, "Location"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblMonetaryBusiness", Language.getMessage(mstrModuleName, 96, "Monetary Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblPercBusiness", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblEmpBusiness", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblHWBusiness", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADOtherBusiness"), "lblChildBusiness", Language.getMessage(mstrModuleName, 88, "Children"))


            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            '*** Resources
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblEightResources", Language.getMessage(mstrModuleName, 37, "8."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "txtEightResources", Language.getMessage(mstrModuleName, 38, "Assets or any other commercial share/benefits such as livestock, poultry etc"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblResources", Language.getMessage(mstrModuleName, 109, "Resources or Commercial Interest"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblLocationResources", Language.getMessage(mstrModuleName, 95, "Location"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblMonetaryResources", Language.getMessage(mstrModuleName, 96, "Monetary Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblPercResources", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblEmpResources", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblHWResources", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADResources"), "lblChildResources", Language.getMessage(mstrModuleName, 88, "Children"))

            '*** Debt
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblNineDebt", Language.getMessage(mstrModuleName, 39, "9."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "txtNineDebt", Language.getMessage(mstrModuleName, 40, "Debts"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblDebt", Language.getMessage(mstrModuleName, 110, "Debt"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblLocationDebt", Language.getMessage(mstrModuleName, 95, "Location"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblMonetaryDebt", Language.getMessage(mstrModuleName, 96, "Monetary Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblPercDebt", Language.getMessage(mstrModuleName, 85, "Shareholding  Percentage"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblEmpDebt", Language.getMessage(mstrModuleName, 86, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblHWDebt", Language.getMessage(mstrModuleName, 87, "Husband/Wife"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDebt"), "lblChildDebt", Language.getMessage(mstrModuleName, 88, "Children"))
            'Sohail (29 Jan 2013) -- End

            'Anjan [17 December 2015] -- Start
            'ENHANCEMENT : Included declaration and Financial Year Dates.
            Call ReportFunction.TextChange(objRpt, "lblTransactionDate", Language.getMessage(mstrModuleName, 111, "Declaration Date :"))
            Call ReportFunction.TextChange(objRpt, "txtTransactionDate", objAssetDeclaration._TransactionDate.ToShortDateString)
            Call ReportFunction.TextChange(objRpt, "lblFY", Language.getMessage(mstrModuleName, 112, "Financial Year :"))
            Dim objMaster As New clsMasterData
            'Sohail (04 Jul 2019) -- Start
            'TRA Issue - Support Issue Id # 0003897 - 70.1 - System allow us to select form of required year / period but exported report displays form of current year.
            'Dim dsYear As DataSet = objMaster.Get_Database_Year_List("DBYear", True, Company._Object._Companyunkid)
            Dim dsYear As DataSet = objMaster.Get_Database_Year_List("DBYear", True, intCompanyUnkid)
            'Sohail (04 Jul 2019) -- End
            Dim dRow() As DataRow = dsYear.Tables("DBYear").Select(" start_date <= '" & eZeeDate.convertDate(objAssetDeclaration._TransactionDate) & "'  AND  end_date >= '" & eZeeDate.convertDate(objAssetDeclaration._TransactionDate) & "' ")
            If dRow.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtFY", dRow(0).Item("financialyear_name").ToString)
            Else
                Call ReportFunction.TextChange(objRpt, "txtFY", "")
            End If
            'Anjan [17 December 2015] -- End


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "ASSET AND DEBT DECLARATION")
            Language.setMessage(mstrModuleName, 2, "[According to 2009")
            Language.setMessage(mstrModuleName, 3, " staff Regulation ordinance sections 44, 45, 46, 47 and 48 ]")
            Language.setMessage(mstrModuleName, 4, "Important Information")
            Language.setMessage(mstrModuleName, 5, "1.")
            Language.setMessage(mstrModuleName, 6, "Before filling this form, please read instructions provided below carefully. You are also advised to read 2009")
            Language.setMessage(mstrModuleName, 7, " staff Regulations ordinance. The list should clearly indicate personal and commercial assets and possessions.")
            Language.setMessage(mstrModuleName, 8, "2.")
            Language.setMessage(mstrModuleName, 9, "List the assets you and your wife (spouse) own. In addition, you have to list assets and possessions belonging to unmarried children under/or 18 years of age. The assets falling under the  following categories must be listed:")
            Language.setMessage(mstrModuleName, 10, "3.")
            Language.setMessage(mstrModuleName, 11, "List all debt you owe")
            Language.setMessage(mstrModuleName, 12, "4.")
            Language.setMessage(mstrModuleName, 13, "This declaration must be submitted to Principal Commissioner in thirty (30) days soon after being employed. Thereafter, declaration must be submitted at the beginning of each financial year and upon termination of employment with")
            Language.setMessage(mstrModuleName, 14, "5.")
            Language.setMessage(mstrModuleName, 15, "It is against the law and regulations if an employee fails to submit asset and debt declaration form within specified timeframe as described in 2009")
            Language.setMessage(mstrModuleName, 16, " staff regulations ordinance. In addition, it is also against the law and it is a criminal offence for employee to provide or declare false information in regard to his/her assets and ownership.")
            Language.setMessage(mstrModuleName, 17, "(a)")
            Language.setMessage(mstrModuleName, 18, "Cash money and deposits or security in a bank or any other financial institutions.")
            Language.setMessage(mstrModuleName, 19, "(b)")
            Language.setMessage(mstrModuleName, 20, "Shares and dividend from the company stock or shares in any company or corporation")
            Language.setMessage(mstrModuleName, 21, "(c)")
            Language.setMessage(mstrModuleName, 22, "Fixed assets such as land, farms or estate, mine, house or houses whose construction is incomplete.")
            Language.setMessage(mstrModuleName, 23, "(d)")
            Language.setMessage(mstrModuleName, 24, "Motor vehicle or any other transportation or movable equipment")
            Language.setMessage(mstrModuleName, 25, "(e)")
            Language.setMessage(mstrModuleName, 26, "Milling, factory and/or industrial equipment")
            Language.setMessage(mstrModuleName, 27, "(f)")
            Language.setMessage(mstrModuleName, 28, "Benefits in any commercial activity or equipment")
            Language.setMessage(mstrModuleName, 29, "I am")
            Language.setMessage(mstrModuleName, 30, "Employment No.")
            Language.setMessage(mstrModuleName, 31, "Work Station")
            Language.setMessage(mstrModuleName, 32, "After being employed by")
            Language.setMessage(mstrModuleName, 33, "with the")
            Language.setMessage(mstrModuleName, 34, "title")
            Language.setMessage(mstrModuleName, 35, "I truthfully and sincerely declare having the following")
            Language.setMessage(mstrModuleName, 36, "assets and debts:")
            Language.setMessage(mstrModuleName, 37, "8.")
            Language.setMessage(mstrModuleName, 38, "Assets or any other commercial share/benefits such as livestock, poultry etc")
            Language.setMessage(mstrModuleName, 39, "9.")
            Language.setMessage(mstrModuleName, 40, "Debts")
            Language.setMessage(mstrModuleName, 41, "This declaration is made and signed before me (Name)")
            Language.setMessage(mstrModuleName, 42, "")
            Language.setMessage(mstrModuleName, 43, "and")
            Language.setMessage(mstrModuleName, 44, "Declarant's Name")
            Language.setMessage(mstrModuleName, 45, "")
            Language.setMessage(mstrModuleName, 46, "and Signature of")
            Language.setMessage(mstrModuleName, 47, "Declarant")
            Language.setMessage(mstrModuleName, 48, "")
            Language.setMessage(mstrModuleName, 49, "who is known to me")
            Language.setMessage(mstrModuleName, 50, "Date")
            Language.setMessage(mstrModuleName, 51, "")
            Language.setMessage(mstrModuleName, 52, "Month")
            Language.setMessage(mstrModuleName, 53, "")
            Language.setMessage(mstrModuleName, 54, "Year")
            Language.setMessage(mstrModuleName, 55, "")
            Language.setMessage(mstrModuleName, 56, "Signature:")
            Language.setMessage(mstrModuleName, 57, "")
            Language.setMessage(mstrModuleName, 58, "Title:")
            Language.setMessage(mstrModuleName, 59, "")
            Language.setMessage(mstrModuleName, 60, "Address:")
            Language.setMessage(mstrModuleName, 61, "")
            Language.setMessage(mstrModuleName, 62, "(This declaration must be made before the Commissioner for Oath)")
            Language.setMessage(mstrModuleName, 63, "TO:")
            Language.setMessage(mstrModuleName, 64, "Principal Commissioner")
            Language.setMessage(mstrModuleName, 65, ", Headquarters")
            Language.setMessage(mstrModuleName, 66, "P.O.BOX 11491")
            Language.setMessage(mstrModuleName, 67, "Dar es Salaam")
            Language.setMessage(mstrModuleName, 68, "CC:")
            Language.setMessage(mstrModuleName, 69, "The Head of department/ITA College Principal/ Zanzibar Assistant Commissioner/Regional Manager")
            Language.setMessage(mstrModuleName, 70, "Name (The Head of Department/ ITA Principal Commissioner/Zanzibar Assistant Commissioner/Manager)")
            Language.setMessage(mstrModuleName, 71, "Signature")
            Language.setMessage(mstrModuleName, 72, "")
            Language.setMessage(mstrModuleName, 73, "")
            Language.setMessage(mstrModuleName, 74, "NOTE: Leave copy to the Head of Department. Two copies must be filled out.")
            Language.setMessage(mstrModuleName, 75, "(1)	Principal Commissioner (2) The Head of Department/ ITA College Principal/Zanzibar Assistant Commissioner/Regional Manager.")
            Language.setMessage(mstrModuleName, 76, "Money at time of feeling this form")
            Language.setMessage(mstrModuleName, 77, "a)")
            Language.setMessage(mstrModuleName, 78, "Cash money")
            Language.setMessage(mstrModuleName, 79, "b)")
            Language.setMessage(mstrModuleName, 80, "Money in a bank or any other financial")
            Language.setMessage(mstrModuleName, 81, "institution")
            Language.setMessage(mstrModuleName, 82, "Bank/ Financial Institution")
            Language.setMessage(mstrModuleName, 83, "Account No.")
            Language.setMessage(mstrModuleName, 84, "Amount")
            Language.setMessage(mstrModuleName, 85, "Shareholding  Percentage")
            Language.setMessage(mstrModuleName, 86, "Employee")
            Language.setMessage(mstrModuleName, 87, "Husband/Wife")
            Language.setMessage(mstrModuleName, 88, "Children")
            Language.setMessage(mstrModuleName, 89, "Share and Dividend from stock")
            Language.setMessage(mstrModuleName, 90, "Value of Share")
            Language.setMessage(mstrModuleName, 91, "A place where Shares are")
            Language.setMessage(mstrModuleName, 92, "Dividend Amount")
            Language.setMessage(mstrModuleName, 93, "Houses and Real Estates (plus houses whose constructions are incomplete)")
            Language.setMessage(mstrModuleName, 94, "House/Building")
            Language.setMessage(mstrModuleName, 95, "Location")
            Language.setMessage(mstrModuleName, 96, "Monetary Value")
            Language.setMessage(mstrModuleName, 97, "Land, Plots, Farms, or Mines")
            Language.setMessage(mstrModuleName, 98, "Plots, Farms, Mines")
            Language.setMessage(mstrModuleName, 99, "The size of the area")
            Language.setMessage(mstrModuleName, 100, "Name of the place where it is located")
            Language.setMessage(mstrModuleName, 101, "Motor Vehicle or any other transportation equipment")
            Language.setMessage(mstrModuleName, 102, "Type of Vehicle/Motor cycle and plate No")
            Language.setMessage(mstrModuleName, 103, "6.")
            Language.setMessage(mstrModuleName, 104, "Milling, factory, and industrial equipments")
            Language.setMessage(mstrModuleName, 105, "Milling, factory or industrial equipment")
            Language.setMessage(mstrModuleName, 106, "7.")
            Language.setMessage(mstrModuleName, 107, "Other businesses such as bars, shops, lodges, guest houses, hotels etc")
            Language.setMessage(mstrModuleName, 108, "Type of business equipment")
            Language.setMessage(mstrModuleName, 109, "Resources or Commercial Interest")
            Language.setMessage(mstrModuleName, 110, "Debt")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
