﻿Imports Aruti.Data
Imports eZeeCommonLib
Imports ExcelWriter

Public Class clsAttestationAcknowledgementReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsAttestationAcknowledgementReport"
    Private mstrReportId As String = enArutiReport.Attestation_Acknowledgement_Report
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mdtAsOnDate As DateTime = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnIncluderInactiveEmp As Boolean = False
    Private mdtAdOnDate As DateTime = Nothing
    Private mintDeclarationStatusID As Integer
    Private mstrDeclarationStatusName As String


    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvance_Filter As String = String.Empty


    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    Private mstrDeclareValueQuery As String = ""

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname



#End Region

#Region " Properties "
    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationStatusId() As Integer
        Set(ByVal value As Integer)
            mintDeclarationStatusID = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationStatusName() As String
        Set(ByVal value As String)
            mstrDeclarationStatusName = value
        End Set
    End Property

    Private mintYearUnkId As Integer = 0
    Public WriteOnly Property _FinancialYearId() As Integer
        Set(ByVal value As Integer)
            mintYearUnkId = value
        End Set
    End Property

    Private mstrYearName As String = ""
    Public WriteOnly Property _FinancialYearName() As String
        Set(ByVal value As String)
            mstrYearName = value
        End Set
    End Property


    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Private mdtFinStartDate As DateTime = Nothing
    Public WriteOnly Property _FinStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFinStartDate = value
        End Set
    End Property

    Private mdtFinEndDate As DateTime = Nothing
    Public WriteOnly Property _FinEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFinEndDate = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mdtAsOnDate = Nothing
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mblnIncluderInactiveEmp = False
            mintYearUnkId = 0
            mstrYearName = ""

            mdtAsOnDate = Nothing
            mstrUserAccessFilter = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Dim strDeclareValueQuery As String = ""
        Dim strLiabValueQuery As String = ""
        Dim strADCatQuery As String = ""
        Try

            If mdtFinStartDate <> Nothing Then
                objDataOperation.AddParameter("@finyear_start", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFinStartDate).ToString())
                Me._FilterQuery &= " AND CONVERT(CHAR(8),finyear_start,112)  >= @finyear_start"
            End If
            If mdtFinEndDate <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),finyear_end,112) <= @finyear_end"
                objDataOperation.AddParameter("@finyear_end", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFinEndDate).ToString())
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
           

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
           
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Function Generate_DetailReport(ByVal strDatabaseName As String _
                                          , ByVal intUserUnkid As Integer _
                                          , ByVal intYearUnkid As Integer _
                                          , ByVal intCompanyUnkid As Integer _
                                          , ByVal dtPeriodStart As Date _
                                          , ByVal dtPeriodEnd As Date _
                                          , ByVal strUserModeSetting As String _
                                          , ByVal blnIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal xExportReportPath As String _
                                          , ByVal xOpenReportAfterExport As Boolean _
                                          , ByVal xBaseCurrencyId As Integer _
                                          ) As Boolean
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet

        Try
            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = intUserUnkid
            Dim strUserName As String = objUser._Firstname & " " & objUser._Lastname
            If strUserName.Trim = "" Then
                strUserName = objUser._Username
            End If
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, True, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            If mstrAdvance_Filter.Trim.Length > 0 Then
                Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            End If


            Dim mdtExcel As New DataTable

            mdtExcel.Columns.Add("sno", System.Type.GetType("System.String")).DefaultValue = 0
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 1, "SNo")
            mdtExcel.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 2, "Employee Code")
            mdtExcel.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 3, "Employee Name")
            mdtExcel.Columns.Add("department", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 4, "Function")
            mdtExcel.Columns.Add("sectiongroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 5, "Department")
            mdtExcel.Columns.Add("classgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 6, "Zone")
            mdtExcel.Columns.Add("class", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 7, "Branch")
            mdtExcel.Columns.Add("job_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 18, "Job")
            mdtExcel.Columns.Add("gradegroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 8, "Designation Level")
            mdtExcel.Columns.Add("Email", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 9, "Email")
            mdtExcel.Columns.Add("Status", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 10, "Status")
            mdtExcel.Columns.Add("Acknowledgement_Date", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 11, "Acknowledgement Date")
            mdtExcel.Columns.Add("EmpActiveStatus", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 20, "Employee Status")

            mdtExcel.Columns.Add("GName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = mstrAnalysis_OrderBy_GroupName


            StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", ISNULL(hrdepartment_master.name, '') AS department " & _
                            ", ISNULL(hrsectiongroup_master.name, '') AS sectiongroup " & _
                            ", ISNULL(hrclassgroup_master.name, '') AS classgroup " & _
                            ", ISNULL(hrclasses_master.name, '') AS class " & _
                            ", ISNULL(hrgradegroup_master.name, '') AS gradegroup " & _
                            ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                            ", hremployee_master.email " & _
                            ", CASE WHEN CONVERT(CHAR(8),hremployee_master.appointeddate, 112) <= @enddate  " & _
                                "AND ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date, 112),@startdate) >= @startdate " & _
                                "AND ISNULL(CONVERT(CHAR(8),ERET.termination_to_date, 112),@startdate) >= @startdate " & _
                                "AND ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate, 112),@startdate) >= @startdate " & _
                                "THEN @Active ELSE @Inactive END AS EmpActiveStatus "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS employeename "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS employeename "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "INTO    #TableEmp " & _
                    "FROM  hremployee_master "

            StrQ &= mstrAnalysis_Join

            StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 gradegroupunkid " & _
                        "		,gradeunkid " & _
                        "		,gradelevelunkid " & _
                        "		,employeeunkid " & _
                        "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                        "	FROM prsalaryincrement_tran " & _
                        "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 TERM.TEEmpId " & _
                    "    		,TERM.empl_enddate " & _
                    "    		,TERM.termination_from_date " & _
                    "    		,TERM.TEfDt " & _
                    "    		,TERM.isexclude_payroll " & _
                    "    		,TERM.TR_REASON " & _
                    "    		,TERM.changereasonunkid " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             TRM.employeeunkid AS TEEmpId " & _
                    "            ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "            ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "            ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "            ,TRM.isexclude_payroll " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "            ,TRM.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS TRM " & _
                    "            LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "        WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 RET.REmpId " & _
                    "    	,RET.termination_to_date " & _
                    "    	,RET.REfDt " & _
                    "    	,RET.RET_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             RTD.employeeunkid AS REmpId " & _
                    "            ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "            ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "        FROM hremployee_dates_tran AS RTD " & _
                    "            LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "        WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid " & _
                    "LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = T.sectiongroupunkid " & _
                    "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = T.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = T.classunkid " & _
                    "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = G.gradegroupunkid " & _
                    "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = J.jobunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If




            If blnIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Hemant (27 Sep 2024) -- Start
            StrQ &= " select * into #FinalTable from ( "
            'Hemant (27 Sep 2024) -- End

            StrQ &= "SELECT " & _
                      "  hrassetdeclarationT2_master.assetdeclarationt2unkid " & _
                      ", hrassetdeclarationT2_master.employeeunkid " & _
                      ", #TableEmp.employeecode " & _
                      ", #TableEmp.employeename " & _
                      ", #TableEmp.department " & _
                      ", #TableEmp.sectiongroup " & _
                      ", #TableEmp.classgroup " & _
                      ", #TableEmp.class " & _
                      ", #TableEmp.gradegroup " & _
                      ", #TableEmp.job_name " & _
                      ", #TableEmp.email " & _
                      ", #TableEmp.EmpActiveStatus " & _
                      ", #TableEmp.GName " & _
                      ", CASE WHEN ISNULL(hrassetdeclarationT2_master.isattestation, 0) = 1  AND CONVERT(CHAR(8), hrassetdeclarationT2_master.attestation_date, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' THEN 1 ELSE 0 END AS isattestation " & _
                      ", CASE WHEN ISNULL(hrassetdeclarationT2_master.isattestation, 0) = 1  AND CONVERT(CHAR(8), hrassetdeclarationT2_master.attestation_date, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' THEN hrassetdeclarationT2_master.attestation_date ELSE NULL END AS attestation_date " & _
                      ", hrassetdeclarationT2_master.userunkid " & _
                      ", hrassetdeclarationT2_master.loginemployeeunkid " & _
                      ", hrassetdeclarationT2_master.isvoid " & _
                      ", hrassetdeclarationT2_master.voiduserunkid " & _
                      ", hrassetdeclarationT2_master.voidloginemployeeunkid " & _
                      ", hrassetdeclarationT2_master.voiddatetime " & _
                      ", hrassetdeclarationT2_master.voidreason " & _
                 "FROM #TableEmp " & _
                      "LEFT JOIN hrassetdeclarationT2_master ON #TableEmp.employeeunkid = hrassetdeclarationT2_master.employeeunkid " & _
                            "AND hrassetdeclarationT2_master.isvoid = 0 "

            'Hemant (27 Sep 2024) -- Start
            'StrQ &= "WHERE 1 = 1 "
            'Hemant (27 Sep 2024) -- End
         
            Call FilterTitleAndFilterQuery()

            'Hemant (27 Sep 2024) -- Start
            'If mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.NOT_SUBMITTED) Then
            '    StrQ &= " AND ( CASE WHEN ISNULL(hrassetdeclarationT2_master.isattestation, 0) = 1  AND CONVERT(CHAR(8), hrassetdeclarationT2_master.attestation_date, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' THEN 1 ELSE 0 END ) = 0  "
            'ElseIf mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.SUMBITTED) Then
            '    StrQ &= " AND ( CASE WHEN ISNULL(hrassetdeclarationT2_master.isattestation, 0) = 1  AND CONVERT(CHAR(8), hrassetdeclarationT2_master.attestation_date, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' THEN 1 ELSE 0 END ) = 1  "
            'End If

            'StrQ &= " ORDER BY #TableEmp.employeename "
            StrQ &= Me._FilterQuery

            StrQ &= " WHERE 1 = 1 "

            StrQ &= " ) AS A "

            StrQ &= " SELECT * FROM #FinalTable WHERE 1 = 1 "

            If mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.NOT_SUBMITTED) Then
                StrQ &= " AND ISNULL(isattestation, 0) = 0  "
            ElseIf mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.SUMBITTED) Then
                StrQ &= " AND ISNULL(isattestation, 0) = 1  "
            End If

            StrQ &= " ORDER BY #FinalTable.employeename "

            StrQ &= " DROP TABLE #FinalTable "
            'Hemant (27 Sep 2024) -- End           

            StrQ &= " DROP TABLE #TableEmp "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart).ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd).ToString)

            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Active"))
            objDataOperation.AddParameter("@Inactive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Inactive"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Dim strAcknowledged As String = Language.getMessage(mstrModuleName, 12, "Acknowledged")
            Dim strNotAcknowledged As String = Language.getMessage(mstrModuleName, 13, "Not Acknowledged")

            Dim intSrNo As Integer = 0
            Dim dr As DataRow = Nothing
            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dr = mdtExcel.NewRow
                intSrNo += 1

                dr.Item("sno") = intSrNo
                dr.Item("employeecode") = dsRow.Item("employeecode").ToString
                dr.Item("employeename") = dsRow.Item("employeename").ToString
                dr.Item("department") = dsRow.Item("department").ToString
                dr.Item("sectiongroup") = dsRow.Item("sectiongroup").ToString
                dr.Item("classgroup") = dsRow.Item("classgroup").ToString
                dr.Item("class") = dsRow.Item("class").ToString
                dr.Item("gradegroup") = dsRow.Item("gradegroup").ToString
                dr.Item("job_name") = dsRow.Item("job_name").ToString
                dr.Item("Email") = dsRow.Item("Email").ToString
                If CBool(dsRow.Item("isattestation")) = False Then
                    dr.Item("Status") = strNotAcknowledged
                Else
                    dr.Item("Status") = strAcknowledged
                End If
                If IsDBNull(dsRow.Item("attestation_date")) = True Then
                    dr.Item("Acknowledgement_Date") = ""
                Else
                    dr.Item("Acknowledgement_Date") = CDate(dsRow.Item("attestation_date")).ToString("dd-MMM-yyyy")
                End If
                dr.Item("GName") = dsRow.Item("GName").ToString
                dr.Item("EmpActiveStatus") = dsRow.Item("EmpActiveStatus").ToString

                mdtExcel.Rows.Add(dr)
            Next

            Me._UserName = strUserName

            Dim intArrayColumnWidth As Integer() = Nothing
            Dim strarrGroupColumns As String() = Nothing
            ReDim intArrayColumnWidth(mdtExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(ii) = 125
            Next
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing

            If mintViewIndex <= 0 Then
                mdtExcel.Columns.Remove("GName")
            Else
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If

            Dim strFilterTitle As String = Language.getMessage(mstrModuleName, 14, "Status :")
            If mintDeclarationStatusID > 0 Then
                strFilterTitle &= mstrDeclarationStatusName
            Else
                strFilterTitle &= Language.getMessage(mstrModuleName, 15, "All")
            End If
            If mintYearUnkId > 0 Then
                strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 19, "Year :") & mstrYearName
            End If
            strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 16, "As On Date :") & mdtAsOnDate.ToString("dd-MMM-yyyy")

            If mintEmployeeId > 0 Then
                strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 17, "Employee :") & mstrEmployeeName
            End If
            row = New WorksheetRow()
            wcell = New WorksheetCell(strFilterTitle, "s9bw")
            wcell.MergeAcross = mdtExcel.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, Nothing)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "SNo")
            Language.setMessage(mstrModuleName, 2, "Employee Code")
            Language.setMessage(mstrModuleName, 3, "Employee Name")
            Language.setMessage(mstrModuleName, 4, "Function")
            Language.setMessage(mstrModuleName, 5, "Department")
            Language.setMessage(mstrModuleName, 6, "Zone")
            Language.setMessage(mstrModuleName, 7, "Branch")
            Language.setMessage(mstrModuleName, 8, "Designation Level")
            Language.setMessage(mstrModuleName, 9, "Email")
            Language.setMessage(mstrModuleName, 10, "Status")
            Language.setMessage(mstrModuleName, 11, "Acknowledgement Date")
            Language.setMessage(mstrModuleName, 12, "Acknowledged")
            Language.setMessage(mstrModuleName, 13, "Not Acknowledged")
            Language.setMessage(mstrModuleName, 14, "Status :")
            Language.setMessage(mstrModuleName, 15, "All")
            Language.setMessage(mstrModuleName, 16, "As On Date :")
            Language.setMessage(mstrModuleName, 17, "Employee :")
            Language.setMessage(mstrModuleName, 18, "Job")
            Language.setMessage(mstrModuleName, 19, "Year :")
            Language.setMessage(mstrModuleName, 20, "Employee Status")
            Language.setMessage(mstrModuleName, 21, "Active")
            Language.setMessage(mstrModuleName, 22, "Inactive")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
