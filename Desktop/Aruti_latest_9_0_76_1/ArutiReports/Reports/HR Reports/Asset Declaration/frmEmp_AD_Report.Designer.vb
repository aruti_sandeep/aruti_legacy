﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmp_AD_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowEmpSal = New System.Windows.Forms.CheckBox
        Me.txtLAmountTo = New eZee.TextBox.NumericTextBox
        Me.txtDAmountTo = New eZee.TextBox.NumericTextBox
        Me.txtLAmountFrom = New eZee.TextBox.NumericTextBox
        Me.txtDAmountFrom = New eZee.TextBox.NumericTextBox
        Me.lblLAmtTo = New System.Windows.Forms.Label
        Me.lblDAmtTo = New System.Windows.Forms.Label
        Me.lblLiability = New System.Windows.Forms.Label
        Me.lblDecAmt = New System.Windows.Forms.Label
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.lblAppointment = New System.Windows.Forms.Label
        Me.cboAppointment = New System.Windows.Forms.ComboBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.dtpAsOnDate = New System.Windows.Forms.DateTimePicker
        Me.lblAsOnDate = New System.Windows.Forms.Label
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 489)
        Me.NavPanel.Size = New System.Drawing.Size(743, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmpSal)
        Me.gbFilterCriteria.Controls.Add(Me.txtLAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtDAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtLAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtDAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblLAmtTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDAmtTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblLiability)
        Me.gbFilterCriteria.Controls.Add(Me.lblDecAmt)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeLine1)
        Me.gbFilterCriteria.Controls.Add(Me.lblAppointment)
        Me.gbFilterCriteria.Controls.Add(Me.cboAppointment)
        Me.gbFilterCriteria.Controls.Add(Me.objlblCaption)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDate2)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDate1)
        Me.gbFilterCriteria.Controls.Add(Me.objelLine1)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(419, 300)
        Me.gbFilterCriteria.TabIndex = 19
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowEmpSal
        '
        Me.chkShowEmpSal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmpSal.Location = New System.Drawing.Point(106, 136)
        Me.chkShowEmpSal.Name = "chkShowEmpSal"
        Me.chkShowEmpSal.Size = New System.Drawing.Size(280, 17)
        Me.chkShowEmpSal.TabIndex = 124
        Me.chkShowEmpSal.Text = "Show Employee Salary"
        Me.chkShowEmpSal.UseVisualStyleBackColor = True
        '
        'txtLAmountTo
        '
        Me.txtLAmountTo.AllowNegative = False
        Me.txtLAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLAmountTo.DigitsInGroup = 0
        Me.txtLAmountTo.Flags = 65536
        Me.txtLAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLAmountTo.Location = New System.Drawing.Point(266, 266)
        Me.txtLAmountTo.MaxDecimalPlaces = 2
        Me.txtLAmountTo.MaxWholeDigits = 21
        Me.txtLAmountTo.Name = "txtLAmountTo"
        Me.txtLAmountTo.Prefix = ""
        Me.txtLAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtLAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtLAmountTo.Size = New System.Drawing.Size(120, 21)
        Me.txtLAmountTo.TabIndex = 122
        Me.txtLAmountTo.Text = "0"
        Me.txtLAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDAmountTo
        '
        Me.txtDAmountTo.AllowNegative = False
        Me.txtDAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDAmountTo.DigitsInGroup = 0
        Me.txtDAmountTo.Flags = 65536
        Me.txtDAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDAmountTo.Location = New System.Drawing.Point(266, 239)
        Me.txtDAmountTo.MaxDecimalPlaces = 2
        Me.txtDAmountTo.MaxWholeDigits = 21
        Me.txtDAmountTo.Name = "txtDAmountTo"
        Me.txtDAmountTo.Prefix = ""
        Me.txtDAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtDAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtDAmountTo.Size = New System.Drawing.Size(120, 21)
        Me.txtDAmountTo.TabIndex = 122
        Me.txtDAmountTo.Text = "0"
        Me.txtDAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLAmountFrom
        '
        Me.txtLAmountFrom.AllowNegative = False
        Me.txtLAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLAmountFrom.DigitsInGroup = 0
        Me.txtLAmountFrom.Flags = 65536
        Me.txtLAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLAmountFrom.Location = New System.Drawing.Point(106, 266)
        Me.txtLAmountFrom.MaxDecimalPlaces = 2
        Me.txtLAmountFrom.MaxWholeDigits = 21
        Me.txtLAmountFrom.Name = "txtLAmountFrom"
        Me.txtLAmountFrom.Prefix = ""
        Me.txtLAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtLAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtLAmountFrom.Size = New System.Drawing.Size(120, 21)
        Me.txtLAmountFrom.TabIndex = 121
        Me.txtLAmountFrom.Text = "0"
        Me.txtLAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDAmountFrom
        '
        Me.txtDAmountFrom.AllowNegative = False
        Me.txtDAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDAmountFrom.DigitsInGroup = 0
        Me.txtDAmountFrom.Flags = 65536
        Me.txtDAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDAmountFrom.Location = New System.Drawing.Point(106, 239)
        Me.txtDAmountFrom.MaxDecimalPlaces = 2
        Me.txtDAmountFrom.MaxWholeDigits = 21
        Me.txtDAmountFrom.Name = "txtDAmountFrom"
        Me.txtDAmountFrom.Prefix = ""
        Me.txtDAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtDAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtDAmountFrom.Size = New System.Drawing.Size(120, 21)
        Me.txtDAmountFrom.TabIndex = 121
        Me.txtDAmountFrom.Text = "0"
        Me.txtDAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLAmtTo
        '
        Me.lblLAmtTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLAmtTo.Location = New System.Drawing.Point(232, 268)
        Me.lblLAmtTo.Name = "lblLAmtTo"
        Me.lblLAmtTo.Size = New System.Drawing.Size(28, 16)
        Me.lblLAmtTo.TabIndex = 120
        Me.lblLAmtTo.Text = "To"
        Me.lblLAmtTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDAmtTo
        '
        Me.lblDAmtTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDAmtTo.Location = New System.Drawing.Point(232, 241)
        Me.lblDAmtTo.Name = "lblDAmtTo"
        Me.lblDAmtTo.Size = New System.Drawing.Size(28, 16)
        Me.lblDAmtTo.TabIndex = 119
        Me.lblDAmtTo.Text = "To"
        Me.lblDAmtTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLiability
        '
        Me.lblLiability.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLiability.Location = New System.Drawing.Point(13, 268)
        Me.lblLiability.Name = "lblLiability"
        Me.lblLiability.Size = New System.Drawing.Size(87, 17)
        Me.lblLiability.TabIndex = 118
        Me.lblLiability.Text = "Liability From"
        Me.lblLiability.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDecAmt
        '
        Me.lblDecAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDecAmt.Location = New System.Drawing.Point(13, 241)
        Me.lblDecAmt.Name = "lblDecAmt"
        Me.lblDecAmt.Size = New System.Drawing.Size(87, 17)
        Me.lblDecAmt.TabIndex = 117
        Me.lblDecAmt.Text = "Declared From"
        Me.lblDecAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(11, 223)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(385, 13)
        Me.EZeeLine1.TabIndex = 116
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAppointment
        '
        Me.lblAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointment.Location = New System.Drawing.Point(13, 174)
        Me.lblAppointment.Name = "lblAppointment"
        Me.lblAppointment.Size = New System.Drawing.Size(87, 17)
        Me.lblAppointment.TabIndex = 115
        Me.lblAppointment.Text = "Appointment"
        Me.lblAppointment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAppointment
        '
        Me.cboAppointment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppointment.FormattingEnabled = True
        Me.cboAppointment.Location = New System.Drawing.Point(106, 172)
        Me.cboAppointment.Name = "cboAppointment"
        Me.cboAppointment.Size = New System.Drawing.Size(280, 21)
        Me.cboAppointment.TabIndex = 114
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(13, 201)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(87, 17)
        Me.objlblCaption.TabIndex = 113
        Me.objlblCaption.Text = "#Caption"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.Checked = False
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(266, 199)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.ShowCheckBox = True
        Me.dtpDate2.Size = New System.Drawing.Size(120, 21)
        Me.dtpDate2.TabIndex = 112
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(232, 201)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(30, 16)
        Me.lblTo.TabIndex = 111
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.Checked = False
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(106, 199)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.ShowCheckBox = True
        Me.dtpDate1.Size = New System.Drawing.Size(120, 21)
        Me.dtpDate1.TabIndex = 110
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 156)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(385, 13)
        Me.objelLine1.TabIndex = 108
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(11, 61)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(87, 17)
        Me.lblReportType.TabIndex = 107
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(106, 59)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(280, 21)
        Me.cboReportType.TabIndex = 106
        '
        'dtpAsOnDate
        '
        Me.dtpAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsOnDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsOnDate.Location = New System.Drawing.Point(106, 32)
        Me.dtpAsOnDate.Name = "dtpAsOnDate"
        Me.dtpAsOnDate.Size = New System.Drawing.Size(120, 21)
        Me.dtpAsOnDate.TabIndex = 105
        '
        'lblAsOnDate
        '
        Me.lblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnDate.Location = New System.Drawing.Point(11, 34)
        Me.lblAsOnDate.Name = "lblAsOnDate"
        Me.lblAsOnDate.Size = New System.Drawing.Size(87, 17)
        Me.lblAsOnDate.TabIndex = 103
        Me.lblAsOnDate.Text = "As On Date"
        Me.lblAsOnDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(106, 113)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(281, 17)
        Me.chkInactiveemp.TabIndex = 20
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(323, 3)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 90
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(392, 86)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(106, 86)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(280, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(11, 88)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(87, 17)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 372)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(419, 63)
        Me.gbSortBy.TabIndex = 20
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(392, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(11, 34)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(87, 17)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(106, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(280, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmEmp_AD_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(743, 544)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmEmp_AD_Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmNotSubmittedADReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents lblAsOnDate As System.Windows.Forms.Label
    Friend WithEvents dtpAsOnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblAppointment As System.Windows.Forms.Label
    Friend WithEvents cboAppointment As System.Windows.Forms.ComboBox
    Friend WithEvents lblDecAmt As System.Windows.Forms.Label
    Friend WithEvents txtDAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtDAmountFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLAmtTo As System.Windows.Forms.Label
    Friend WithEvents lblDAmtTo As System.Windows.Forms.Label
    Friend WithEvents lblLiability As System.Windows.Forms.Label
    Friend WithEvents txtLAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtLAmountFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents chkShowEmpSal As System.Windows.Forms.CheckBox
End Class
