'************************************************************************************************************************************
'Class Name : clsAsset_Declaration_Status_ReportT2.vb
'Purpose    :
'Date       : 26-Nov-2018
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
#End Region
Public Class clsAsset_Declaration_Status_ReportT2
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAsset_Declaration_Status_ReportT2"
    'Hemant (24 Dec 2018) -- Start
    'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
    Private Shared ReadOnly mstrModuleNameForm As String = "frmEmployeeAssetDeclarationStatusReportT2"
    'Hemant (24 Dec 2018) -- End
    Private mstrReportId As String = enArutiReport.Asset_Declaration_Status_Report
    Private objDataOperation As clsDataOperation

#Region " Private Variables "
    Private mdtAsOnDate As DateTime = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintAppointmentTypeId As Integer = 0
    Private mdecDAmtFrom As Decimal = 0
    Private mdecDAmtTo As Decimal = 0
    Private mdecLAmtFrom As Decimal = 0
    Private mdecLAmtTo As Decimal = 0
    Private mintCompanyUnkid As Integer = -1
    Private mintUserUnkid As Integer = -1
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mblnShowSalary As Boolean = False
    Private mblnIncluderInactiveEmp As Boolean = False
    Private mstrAdvance_Filter As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintStatusId As Integer = -1
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrViewByIds As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mstrStatusName As String = ""
#End Region

#Region " Properties "
    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property
    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property
    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property
    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property
    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property
    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property
    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property
    Public WriteOnly Property _DAmtFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecDAmtFrom = value
        End Set
    End Property

    Public WriteOnly Property _DAmtTo() As Decimal
        Set(ByVal value As Decimal)
            mdecDAmtTo = value
        End Set
    End Property

    Public WriteOnly Property _LAmtFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecLAmtFrom = value
        End Set
    End Property

    Public WriteOnly Property _LAmtTo() As Decimal
        Set(ByVal value As Decimal)
            mdecLAmtTo = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _ShowSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSalary = value
        End Set
    End Property

    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property
    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property
    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property
    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    'Hemant (05 Dec 2018) -- Start
    'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
    Private mdtAssetDeclarationFromDate As DateTime = Nothing
    Public WriteOnly Property _AssetDeclarationFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAssetDeclarationFromDate = value
        End Set
    End Property

    Private mdtAssetDeclarationToDate As DateTime = Nothing
    Public WriteOnly Property _AssetDeclarationToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAssetDeclarationToDate = value
        End Set
    End Property
    'Hemant (05 Dec 2018) -- End

    Private mintFinancialYearId As Integer = 0
    Public WriteOnly Property _FinancialYearId() As Integer
        Set(ByVal value As Integer)
            mintFinancialYearId = value
        End Set
    End Property

    Private mstrFinancialYearName As String = String.Empty
    Public WriteOnly Property _FinancialYearName() As String
        Set(ByVal value As String)
            mstrFinancialYearName = value
        End Set
    End Property

    Private mdtFinStartDate As DateTime = Nothing
    Public WriteOnly Property _FinStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFinStartDate = value
        End Set
    End Property

    Private mdtFinEndDate As DateTime = Nothing
    Public WriteOnly Property _FinEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFinEndDate = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

#End Region
#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_StatusReport()
    End Sub

#End Region

#Region "Report Generation"
    Dim iColumn_NonOfficial As New IColumnCollection
    Private Sub Create_StatusReport()
        Try
            iColumn_NonOfficial.Clear()
            iColumn_NonOfficial.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 200, "Employee Code")))

            If mblnFirstNamethenSurname = False Then
                iColumn_NonOfficial.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 201, "Employee Name")))
            Else
                iColumn_NonOfficial.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 201, "Employee Name")))
            End If
            iColumn_NonOfficial.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 202, "Appointed Date")))


            iColumn_NonOfficial.Add(New IColumn("ISNULL(DM.name,'')", Language.getMessage(mstrModuleName, 1, "Department")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(JM.job_name,'')", Language.getMessage(mstrModuleName, 2, "Job Title")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(CGM.name,'')", Language.getMessage(mstrModuleName, 3, "Region")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(CM.name,'')", Language.getMessage(mstrModuleName, 4, "WorkStation")))
            iColumn_NonOfficial.Add(New IColumn("(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
                                                "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
                                                "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
                                                "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0))", Language.getMessage(mstrModuleName, 208, "Declared Amount")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(debttotal,0)", Language.getMessage(mstrModuleName, 209, "Liability Amount")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(Salary,0)", Language.getMessage(mstrModuleName, 207, "Salary")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_NonOfficialReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_Status_Report(ByVal strDatabaseName As String, _
                                                 ByVal intUserUnkid As Integer, _
                                                 ByVal intYearUnkid As Integer, _
                                                 ByVal intCompanyUnkid As Integer, _
                                                 ByVal dtPeriodStart As Date, _
                                                 ByVal dtPeriodEnd As Date, _
                                                 ByVal strUserModeSetting As String, _
                                                 ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT ECode,EName,App_Date,email,present_mobile,Department,Job_Title,Region,WorkStation,appointeddate,Salary,EmpId, assetdeclarationt2unkid, finyear_start, finyear_end, isfinalsaved, finalsavedate, Status, GradeGroupName, dayfraction ,Id,GName " & _
                   " FROM ( "
            StrQ &= "SELECT " & _
                       " ECode AS ECode " & _
                       ",EName AS EName " & _
                       ",App_Date AS App_Date " & _
                       ",email AS email " & _
                       ",present_mobile AS present_mobile" & _
                       ",Department AS Department " & _
                       ",Job_Title AS Job_Title " & _
                       ",Region AS Region " & _
                       ",WorkStation AS WorkStation " & _
                       ",appointeddate AS appointeddate "

            If mblnShowSalary = True Then
                StrQ &= ",Salary AS Salary "
            Else
                StrQ &= ",0 AS Salary "
            End If
            StrQ &= ",EmpId AS EmpId " & _
                    ",assetdeclarationt2unkid AS assetdeclarationt2unkid " & _
                    ",finyear_start as finyear_start " & _
                    ",finyear_end as finyear_end " & _
                    ",isfinalsaved AS isfinalsaved " & _
                    ",finalsavedate as finalsavedate " & _
                    ",Status as Status " & _
                    ",GradeGroupName as GradeGroupName " & _
                    ",dayfraction AS dayfraction " & _
                    ",Id AS Id " & _
                    ",GName AS GName " & _
                    "FROM " & _
                    "( " & _
                              "SELECT " & _
                              " ISNULL(hremployee_master.employeecode,'') AS ECode "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EName "
            End If


            StrQ &= ",ISNULL(CONVERT(CHAR(8), appointeddate,112),'') AS App_Date " & _
                    ", ISNULL(hremployee_master.email,'') AS email " & _
                    ", ISNULL(present_mobile, '') as present_mobile" & _
                              ",ISNULL(DM.name,'') AS Department " & _
                              ",ISNULL(JM.job_name,'') AS Job_Title " & _
                              ",ISNULL(CGM.name,'') AS Region " & _
                              ",ISNULL(CM.name,'') AS WorkStation " & _
                              ",ISNULL(Salary,0) AS Salary " & _
                              ",hremployee_master.appointeddate AS appointeddate " & _
                              ",hremployee_master.employeeunkid AS EmpId " & _
                                ", ISNULL(assetdeclarationt2unkid, 0) AS assetdeclarationt2unkid " & _
                                ",ISNULL(CONVERT(CHAR(8), finyear_start,112),@finyear_start) as finyear_start " & _
                                ",ISNULL(CONVERT(CHAR(8), finyear_end,112),@finyear_end) as finyear_end " & _
                                ", ( CASE WHEN isfinalsaved = 1 THEN 2 WHEN isfinalsaved = 0 THEN 1 ElSE 3 End ) AS isfinalsaved" & _
                                ", ( CASE WHEN isfinalsaved = 1 THEN '" & enAssetDeclarationStatusType.SUMBITTED & "' WHEN isfinalsaved = 0 THEN '" & enAssetDeclarationStatusType.NOT_SUBMITTED & "' ElSE '" & enAssetDeclarationStatusType.NOT_ATTEMPTED & "' End ) AS Status" & _
                                ", Adm.finalsavedate as finalsavedate " & _
                                ", GM.name as GradeGroupName " & _
                                ", ISNULL(LIT.dayfraction,0) AS dayfraction "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "FROM hremployee_master "

            StrQ &= "LEFT JOIN " & _
                "   ( " & _
                "       SELECT " & _
                "           assetdeclarationt2unkid " & _
            "           ,finyear_start " & _
            "           ,finyear_end " & _
                "           ,employeeunkid " & _
                "           ,isfinalsaved " & _
                "           ,finalsavedate " & _
                "       FROM hrassetdeclarationT2_master " & _
                "       WHERE isvoid = 0 " & _
                "   ) AS Adm ON Adm.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,classgroupunkid " & _
                    "           ,classunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                     "LEFT JOIN ( " & _
                         "SELECT gradegroupunkid " & _
                                 ",gradeunkid " & _
                                 ",gradelevelunkid " & _
                                 ",scale " & _
                                 ",employeeunkid AS GEmpId " & _
                         "FROM ( " & _
                                   "SELECT gradegroupunkid " & _
                                   ",gradeunkid " & _
                                   ",gradelevelunkid " & _
                                   ",employeeunkid " & _
                                   ",newscale AS scale " & _
                                   ",ROW_NUMBER() OVER ( " & _
                                        "PARTITION BY employeeunkid ORDER BY incrementdate DESC " & _
                                             ",salaryincrementtranunkid DESC " & _
                                        ") AS Rno " & _
                              "FROM prsalaryincrement_tran " & _
                              "WHERE isvoid = 0 " & _
                                   "AND isapproved = 1 " & _
                                   "AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                              ") AS GRD " & _
                         "WHERE GRD.Rno = 1 " & _
                         ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN hrgradegroup_master AS GM ON EGRD.gradegroupunkid = gm.gradegroupunkid " & _
                    "Left Join " & _
                    "( select employeeunkid,SUM(dayfraction) as dayfraction FROM lvleaveIssue_master " & _
                          " JOIN lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid =lvleaveIssue_master.leaveissueunkid " & _
                          " WHERE lvleaveIssue_tran.isvoid = 0 and lvleaveIssue_master.isvoid = 0 " & _
                            " AND CONVERT(CHAR(8), leavedate, 112) between  '" & eZeeDate.convertDate(mdtAssetDeclarationFromDate) & " '  AND '" & eZeeDate.convertDate(mdtAssetDeclarationToDate) & " '	AND dayfraction >= 1 " & _
                            " group by lvleaveIssue_master.employeeunkid " & _
                            ") AS LIT ON LIT.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "Eid " & _
                                   ",Salary " & _
                              "FROM " & _
                              "( " & _
                                   "SELECT " & _
                                         "employeeunkid AS Eid " & _
                                        ",ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                        ",newscale AS Salary " & _
                                   "FROM prsalaryincrement_tran " & _
                                   "WHERE incrementdate < = @SalDate AND isapproved = 1 AND isvoid = 0 " & _
                              ") AS A " & _
                              "WHERE   A.Rno = 1 " & _
                    "   ) AS B ON B.Eid = hremployee_master.employeeunkid "

            'Hemant (13 Dec 2018) -- [finyear_start,finyear_end]
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            'StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM hrassetdeclaration_master WHERE isvoid = 0) "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            'Hemant (15 Dec 2018) -- Start
            'ISSUE : report with checked and unchecked it gives same count which inactive employees are included for NMB
            StrQ &= " WHERE 1 = 1 "
            'Hemant (15 Dec 2018) -- End

            If mblnIncluderInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= ") AS B WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= ") AS C WHERE 1 = 1 "
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing
            Dim iCnt As Integer = 1 : Dim StrGrpName As String = String.Empty
            Dim dtTable As DataTable
            If mstrReport_GroupName.Trim.Length > 0 Then
                dtTable = New DataView(dsList.Tables("DataTable"), "", "GName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("DataTable")
            End If

            For Each dtRow As DataRow In dtTable.Rows
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                rpt_Rows.Item("Column2") = dtRow.Item("EName")
                If CDec(dtRow.Item("dayfraction").ToString) > 0 Then
                    rpt_Rows.Item("Column3") = "Yes"
                Else
                    rpt_Rows.Item("Column3") = "No"
                End If
                rpt_Rows.Item("Column4") = dtRow.Item("GradeGroupName")
                rpt_Rows.Item("Column5") = dtRow.Item("Email")
                rpt_Rows.Item("Column6") = dtRow.Item("Present_mobile")

                If dtRow.Item("Status") = enAssetDeclarationStatusType.SUMBITTED Then
                    rpt_Rows.Item("Column7") = Language.getMessage("clsMasterData", 870, "Submitted")
                ElseIf dtRow.Item("Status") = enAssetDeclarationStatusType.NOT_SUBMITTED Then
                    rpt_Rows.Item("Column7") = Language.getMessage("clsMasterData", 869, "Not Submitted")
                ElseIf dtRow.Item("Status") = enAssetDeclarationStatusType.NOT_ATTEMPTED Then
                    rpt_Rows.Item("Column7") = Language.getMessage("clsMasterData", 871, "Not Attempted")
                End If
                If dtRow.Item("finalsavedate") IsNot DBNull.Value Then
                    rpt_Rows.Item("Column8") = dtRow.Item("finalsavedate").ToShortDateString
                End If

                rpt_Rows.Item("Column9") = iCnt.ToString
                rpt_Rows.Item("Column10") = dtRow.Item("GName")

                'Sohail (10 Feb 2020) -- Start
                'NMB Issue # : Not able to generate report when analysis by is set with group name having single quote.
                'rpt_Rows.Item("Column82") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Salary)", "GName = '" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Rows.Item("Column82") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Salary)", "GName = '" & dtRow.Item("GName").ToString.Replace("'", "''") & "'")), GUI.fmtCurrency)
                'Sohail (10 Feb 2020) -- End
                rpt_Rows.Item("Column83") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Salary)", "")), GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                iCnt += 1
            Next

            objRpt = New ArutiReport.Designer.rptAssetDeclarationStatus

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 300, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 301, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 302, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 303, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            If mblnShowSalary = False Then
                'ReportFunction.EnableSuppress(objRpt, "Column81", True)
                ReportFunction.EnableSuppress(objRpt, "Column821", True)
                ReportFunction.EnableSuppress(objRpt, "txtTotalAmt1", True)
                ReportFunction.EnableSuppress(objRpt, "txtTotal", True)
                ReportFunction.EnableSuppress(objRpt, "txtGrpTotal", True) 'Sohail (07 Feb 2013)
            End If

            ReportFunction.TextChange(objRpt, "txtSN", Language.getMessage(mstrModuleName, 304, "Sno."))
            ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 200, "Employee Code"))
            ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 201, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtOnLeave", Language.getMessage(mstrModuleName, 5, "On Leave"))
            ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 203, "Email"))
            ReportFunction.TextChange(objRpt, "txtDesignationLevel", Language.getMessage(mstrModuleName, 204, "Designation Level"))
            ReportFunction.TextChange(objRpt, "txtPhoneNumber", Language.getMessage(mstrModuleName, 205, "Phone Number"))
            ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 206, "Status"))
            ReportFunction.TextChange(objRpt, "txtSubmittedDate", Language.getMessage(mstrModuleName, 6, "Submitted Date"))

            ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 307, "Group Count : "))
            ReportFunction.TextChange(objRpt, "txtGrandCount", Language.getMessage(mstrModuleName, 308, "Grand Count : "))
            ReportFunction.TextChange(objRpt, "txtGrpTotal", Language.getMessage(mstrModuleName, 309, "Group Total : "))
            ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 310, "Grand Total :"))
            ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                If IsDBNull(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column83")) = False Then
                    Call ReportFunction.TextChange(objRpt, "txtTotalAmt1", Format(CDec(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column83")), GUI.fmtCurrency))
                End If
            End If

            ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 305, "Printed By :"))
            ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 306, "Printed Date :"))

            ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)


            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            'ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Language.setLanguage(mstrModuleNameForm)
            ReportFunction.TextChange(objRpt, "txtReportName", Language._Object.getCaption(mstrModuleNameForm, Me._ReportName))
            'Hemant (24 Dec 2018) -- End
            ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 304, "Sno.")
                mdtTableExcel.Columns("Column9").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 200, "Employee Code")
                mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                mintColumn += 1


                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 201, "Employee Name")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 5, "On Leave")
                mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 204, "Designation Level")
                mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 203, "Email")
                mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 205, "Phone Number")
                mdtTableExcel.Columns("Column6").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 206, "Status")
                mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column8").Caption = Language.getMessage(mstrModuleName, 6, "Submitted Date")
                mdtTableExcel.Columns("Column8").SetOrdinal(mintColumn)
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column10").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If

                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next

            End If
            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Status_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

#Region " Public Function & Procedures "
    Private Sub FilterTitleAndFilterQuery()

        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@SalDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate).ToString)

            If mintFinancialYearId <> 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Financial Year : ") & " " & mstrFinancialYearName & " "
            End If

            objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate).ToString)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "As On Date : ") & " " & mdtAsOnDate.ToShortDateString & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 107, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND Isfinalsaved = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Status : ") & " " & mstrStatusName & " "
            End If

            If mdtFinStartDate <> Nothing Then
                objDataOperation.AddParameter("@finyear_start", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFinStartDate).ToString())
                Me._FilterQuery &= " AND CONVERT(CHAR(8),finyear_start,112)  >= @finyear_start"
            End If
            If mdtFinEndDate <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),finyear_end,112) <= @finyear_end"
                objDataOperation.AddParameter("@finyear_end", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFinEndDate).ToString())
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 108, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = xCompanyUnkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = xUserUnkid
            End If

            User._Object._Userunkid = mintUserUnkid

            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_Status_Report(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)


            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim strarrGroupColumns As String() = Nothing
                'Dim row As WorksheetRow
                'Dim wcell As WorksheetCell

                If mdtTableExcel IsNot Nothing Then
                    If mintViewIndex > 0 Then
                        Dim intGroupColumn As Integer = 0

                        Dim strGrpCols As String() = {"Column10"}
                        strarrGroupColumns = strGrpCols

                    End If
                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next
                End If


                ' Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_NonOfficial.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_NonOfficial.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Select Case intReportType
                Case 0, 2
                    Call OrderByExecute(iColumn_NonOfficial)
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Department")
            Language.setMessage(mstrModuleName, 2, "Job Title")
            Language.setMessage(mstrModuleName, 3, "Region")
            Language.setMessage(mstrModuleName, 4, "WorkStation")
            Language.setMessage(mstrModuleName, 5, "On Leave")
            Language.setMessage(mstrModuleName, 6, "Submitted Date")
            Language.setMessage(mstrModuleName, 7, "Status :")
            Language.setMessage(mstrModuleName, 8, "Financial Year :")
            Language.setMessage(mstrModuleName, 100, "As On Date :")
            Language.setMessage(mstrModuleName, 107, "Employee :")
            Language.setMessage(mstrModuleName, 108, "Order By :")
            Language.setMessage(mstrModuleName, 200, "Employee Code")
            Language.setMessage(mstrModuleName, 201, "Employee Name")
            Language.setMessage(mstrModuleName, 202, "Appointed Date")
            Language.setMessage(mstrModuleName, 203, "Email")
            Language.setMessage(mstrModuleName, 204, "Designation Level")
            Language.setMessage(mstrModuleName, 205, "Phone Number")
            Language.setMessage(mstrModuleName, 206, "Status")
            Language.setMessage(mstrModuleName, 207, "Salary")
            Language.setMessage(mstrModuleName, 208, "Declared Amount")
            Language.setMessage(mstrModuleName, 209, "Liability Amount")
            Language.setMessage(mstrModuleName, 300, "Prepared By :")
            Language.setMessage(mstrModuleName, 301, "Checked By :")
            Language.setMessage(mstrModuleName, 302, "Approved By :")
            Language.setMessage(mstrModuleName, 303, "Received By :")
            Language.setMessage(mstrModuleName, 304, "Sno.")
            Language.setMessage(mstrModuleName, 305, "Printed By :")
            Language.setMessage(mstrModuleName, 306, "Printed Date :")
            Language.setMessage(mstrModuleName, 307, "Group Count :")
            Language.setMessage(mstrModuleName, 308, "Grand Count :")
            Language.setMessage(mstrModuleName, 309, "Group Total :")
            Language.setMessage(mstrModuleName, 310, "Grand Total :")
            Language.setMessage("clsMasterData", 869, "Not Submitted")
            Language.setMessage("clsMasterData", 870, "Submitted")
            Language.setMessage("clsMasterData", 871, "Not Attempted")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
