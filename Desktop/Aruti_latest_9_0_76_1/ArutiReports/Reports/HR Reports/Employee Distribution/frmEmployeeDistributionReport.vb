'************************************************************************************************************************************
'Class Name : frmEmployeeDistributionReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeDistributionReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDistributionReport"
    Private objDistribution As clsEmployeeDistributionReport

#End Region

#Region " Contructor "

    Public Sub New()
        objDistribution = New clsEmployeeDistributionReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objDistribution.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Try
            dsCombos = objDistribution.GetDistributionBy("List")
            With cboDistribution
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 1
            End With

            '--------- Year
            dsCombos = objDistribution.GetYearList("List")
            With cboYear
                .DisplayMember = "Years"
                .DataSource = dsCombos.Tables("List")
            End With
            '--------- Month
            dsCombos = objDistribution.GetMonthList("List")
            With cboMonth
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboDistribution.SelectedValue = 1
            cboMonth.SelectedValue = 0
            chkViewBy.Checked = True


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objDistribution.SetDefaultValue()

            If chkViewBy.Checked = False Then
                If cboMonth.SelectedValue <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Month is mandatory information. Please select month to continue."), enMsgBoxStyle.Information)
                    cboMonth.Focus()
                    Return False
                End If
            End If
            
            objDistribution._DistributionId = cboDistribution.SelectedValue
            objDistribution._DistributionName = cboDistribution.Text

            objDistribution._YearName = cboYear.Text

            objDistribution._MonthId = cboMonth.SelectedValue
            objDistribution._MonthName = cboMonth.Text
            Dim iDate As DateTime = Nothing

            If chkViewBy.Checked = False Then
                iDate = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(cboYear.Text & Format(cboMonth.SelectedValue, "0#") & "01"))).AddDays(-1)
                objDistribution._EmplType_Date = eZeeDate.convertDate(iDate)
            End If


            If chkViewBy.Checked = True Then
                If Date.DaysInMonth(dtpAsOnDate.Value.Date.Year, dtpAsOnDate.Value.Month) = dtpAsOnDate.Value.Date.Day Then
                    'iDate = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dtpAsOnDate.Value.Date.Year & Format(dtpAsOnDate.Value.Date.Month - 1, "0#") & "01"))).AddDays(-1)
                    Dim dt As Date = Nothing
                    If dtpAsOnDate.Value.Date.Month = 1 Then
                        dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dtpAsOnDate.Value.Date.Year - 1 & Format(12, "0#") & "01"))).AddDays(-1)
                        iDate = dt
                    Else
                        dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dtpAsOnDate.Value.Date.Year & Format(dtpAsOnDate.Value.Date.Month, "0#") & "01"))).AddDays(-1)
                        iDate = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dt.Date.Year & Format(dt.Date.Month - 1, "0#") & "01"))).AddDays(-1)
                    End If


                    objDistribution._ViewTypeId = 2
                    objDistribution._MonthId = dtpAsOnDate.Value.Month
                    objDistribution._MonthName = MonthName(dtpAsOnDate.Value.Date.Month)
                    objDistribution._YearName = dtpAsOnDate.Value.Year
                    objDistribution._EmplType_Date = eZeeDate.convertDate(dtpAsOnDate.Value)
                Else
                    objDistribution._ViewTypeId = 1    'Date 
                End If

                objDistribution._AsOnDate = IIf(iDate = Nothing, dtpAsOnDate.Value.Date, iDate)
            Else
                Dim dt As Date = Nothing
                If cboMonth.SelectedValue = 1 Then
                    dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(CInt(cboYear.Text) - 1 & Format(12, "0#") & "01"))).AddDays(-1)
                    iDate = dt
                Else
                    dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(CInt(cboYear.Text) & Format(cboMonth.SelectedValue, "0#") & "01"))).AddDays(-1)
                    iDate = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dt.Date.Year & Format(dt.Date.Month - 1, "0#") & "01"))).AddDays(-1)
                End If


                objDistribution._MonthId = cboMonth.SelectedValue 'dt.Month
                objDistribution._MonthName = cboMonth.Text
                objDistribution._YearName = cboYear.Text
                objDistribution._ViewTypeId = 2    'Month
                objDistribution._AsOnDate = iDate
            End If


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objDistribution._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmployeeDistributionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objDistribution = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeDistributionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDistributionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            eZeeHeader.Title = objDistribution._ReportName
            eZeeHeader.Message = objDistribution._ReportDesc

            Call FillCombo()
            Call ResetValue()
            Call chkViewBy_CheckedChanged(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDistributionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            'Nilay (18-Mar-2015) -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'If ConfigParameter._Object._ExportReportPath.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please set the Export Report Path to export report."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Nilay (18-Mar-2015) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDistribution.Export_Employee_Distribution()
            objDistribution.Export_Employee_Distribution(FinancialYear._Object._DatabaseName, _
                                                         User._Object._Userunkid, _
                                                         FinancialYear._Object._YearUnkid, _
                                                         Company._Object._Companyunkid, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         ConfigParameter._Object._UserAccessModeSetting, True, False)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeDistributionReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeDistributionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region " Controls "

    Private Sub chkViewBy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkViewBy.CheckedChanged
        Try
            If chkViewBy.Checked = True Then
                cboMonth.Enabled = False
                cboYear.Enabled = False
                dtpAsOnDate.Enabled = True
            Else
                dtpAsOnDate.Enabled = False
                cboMonth.Enabled = True
                cboYear.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkViewBy_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblDistribution.Text = Language._Object.getCaption(Me.lblDistribution.Name, Me.lblDistribution.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
			Me.chkViewBy.Text = Language._Object.getCaption(Me.chkViewBy.Name, Me.chkViewBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Month is mandatory information. Please select month to continue.")
			Language.setMessage(mstrModuleName, 3, "Please set the Export Report Path to export report.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
