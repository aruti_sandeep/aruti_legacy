Imports Aruti.Data
Imports eZeeCommonLib
Imports ExcelWriter

Public Class clsATNonDisclouserDeclarationReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsATNonDisclouserDeclarationReport"
    Private mstrReportId As String = enArutiReport.ATNonDisclosureDeclaration_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnApplyAccessFilter As Boolean = True
    Private mdtDeclarationDateFrom As Date = Nothing
    Private mdtDeclarationDateTo As Date = Nothing
    Private mdtAuditDateFrom As Date = Nothing
    Private mdtAuditDateTo As Date = Nothing

    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrAdvance_Filter As String = ""
    Private mintActionId As Integer = 0
    Private mstrActionName As String = ""

    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ActionId() As Integer
        Set(ByVal value As Integer)
            mintActionId = value
        End Set
    End Property

    Public WriteOnly Property _ActionName() As String
        Set(ByVal value As String)
            mstrActionName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationDateFrom() As Date
        Set(ByVal value As Date)
            mdtDeclarationDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationDateTo() As Date
        Set(ByVal value As Date)
            mdtDeclarationDateTo = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateFrom() As Date
        Set(ByVal value As Date)
            mdtAuditDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateTo() As Date
        Set(ByVal value As Date)
            mdtAuditDateTo = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnApplyAccessFilter = True
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrAdvance_Filter = ""
            mintActionId = 0
            mstrActionName = ""
            mdtAuditDateFrom = Nothing
            mdtAuditDateTo = Nothing
            mdtDeclarationDateFrom = Nothing
            mdtDeclarationDateTo = Nothing
          
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EM.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & ": " & mstrEmployeeName & " "
            End If

            If mdtDeclarationDateFrom <> Nothing AndAlso mdtDeclarationDateTo <> Nothing Then
                objDataOperation.AddParameter("@DeclarationDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDeclarationDateFrom))
                objDataOperation.AddParameter("@DeclarationDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDeclarationDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),EN.declaration_date,112) BETWEEN @DeclarationDateFrom AND @DeclarationDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Declaration Start Date Between :") & " " & mdtDeclarationDateFrom.Date.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 3, "To :") & " " & mdtDeclarationDateTo.Date.ToShortDateString & " "
            End If


            If mdtAuditDateFrom <> Nothing AndAlso mdtAuditDateTo <> Nothing Then
                objDataOperation.AddParameter("@AuditDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateFrom))
                objDataOperation.AddParameter("@AuditDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),EN.auditdatetime,112) BETWEEN @AuditDateFrom AND @AuditDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Audit Start Date Between :") & " " & mdtAuditDateFrom.Date.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 3, "To :") & " " & mdtAuditDateTo.Date.ToShortDateString & " "
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid
            Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean)


        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "EM")
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "EM")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "EM")

            StrQ = "SELECT " & _
                           " EM.employeecode " & _
                           ",EM.firstname + ' ' + EM.surname AS Employee " & _
                           ",FY.financialyear_name AS financialyear_name " & _
                           ",EN.declaration_date " & _
                           ",Witness1.employeecode as Witness1EmpCode " & _
                           ",Witness1.firstname + ' ' + Witness1.surname AS Witness1Employee " & _
                           ",En.ip " & _
                           ",ISNULL(mac_address,'')  AS mac_address "

            '",Witness2.employeecode as Witness2EmpCode " & _
            '",Witness2.firstname + ' ' + Witness2.surname AS Witness2Employee " & _


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM athrempnondisclosure_declaration_tran AS EN " & _
                    "    JOIN hrmsConfiguration..cffinancial_year_tran AS FY ON EN.yearunkid = FY.yearunkid " & _
                    "    JOIN hremployee_master AS Witness1 ON Witness1.employeeunkid = EN.witness1userunkid " & _
                    "    JOIN hremployee_master AS Witness2 ON Witness2.employeeunkid = EN.witness2userunkid " & _
                    "    JOIN hremployee_master AS EM ON EM.employeeunkid = EN.employeeunkid " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             CT.jobunkid " & _
                    "            ,CT.employeeunkid " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                    "        FROM hremployee_categorization_tran AS CT " & _
                    "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                    "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                    "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             ET.classgroupunkid " & _
                    "            ,ET.classunkid " & _
                    "            ,ET.employeeunkid " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                    "        FROM hremployee_transfer_tran AS ET " & _
                    "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                    "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                    "    LEFT JOIN hrclassgroup_master AS ECG ON EC.classgroupunkid = ECG.classgroupunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE 1=1 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " ORDER BY EN.auditdatetime "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim mdtTableExcel As DataTable = dsList.Tables(0)
            Dim dcHeader As New Dictionary(Of Integer, String)
            Dim intColumnIndex As Integer = -1



            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If


            If mdtTableExcel.Columns.Contains("Id") Then
                mdtTableExcel.Columns.Remove("Id")
            End If

            If mintViewIndex <= 0 AndAlso mdtTableExcel.Columns.Contains("GName") Then
                mdtTableExcel.Columns.Remove("GName")
            End If


            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 5, "Employee Code")
            mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 6, "Employee Name")

            mdtTableExcel.Columns("financialyear_name").Caption = Language.getMessage(mstrModuleName, 7, "Financial Year")
            mdtTableExcel.Columns("declaration_date").Caption = Language.getMessage(mstrModuleName, 8, "Declaration Date & Time")

            mdtTableExcel.Columns("Witness1EmpCode").Caption = Language.getMessage(mstrModuleName, 9, "Witness1 Code")
            mdtTableExcel.Columns("Witness1Employee").Caption = Language.getMessage(mstrModuleName, 10, "Witness 1 Name")
            mdtTableExcel.Columns("ip").Caption = Language.getMessage(mstrModuleName, 11, "Ip Address")
            mdtTableExcel.Columns("mac_address").Caption = Language.getMessage(mstrModuleName, 12, "MAC Address")


            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Declaration Start Date Between :")
            Language.setMessage(mstrModuleName, 3, "To :")
            Language.setMessage(mstrModuleName, 4, "Audit Start Date Between :")
            Language.setMessage(mstrModuleName, 5, "Employee Code")
            Language.setMessage(mstrModuleName, 6, "Employee Name")
            Language.setMessage(mstrModuleName, 7, "Financial Year")
            Language.setMessage(mstrModuleName, 8, "Declaration Date & Time")
            Language.setMessage(mstrModuleName, 9, "Witness1 Code")
            Language.setMessage(mstrModuleName, 10, "Witness 1 Name")
            Language.setMessage(mstrModuleName, 11, "Ip Address")
            Language.setMessage(mstrModuleName, 12, "MAC Address")
            Language.setMessage(mstrModuleName, 13, "Prepared By :")
            Language.setMessage(mstrModuleName, 14, "Checked By :")
            Language.setMessage(mstrModuleName, 15, "Approved By")
            Language.setMessage(mstrModuleName, 16, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
