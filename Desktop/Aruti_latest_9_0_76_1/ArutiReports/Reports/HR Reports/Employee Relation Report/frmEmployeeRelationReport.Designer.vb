﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeRelationReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnInActiveDependentOption = New eZee.Common.eZeeLine
        Me.pnlInactivedependentoption = New System.Windows.Forms.Panel
        Me.rdAgeLimitWise = New System.Windows.Forms.RadioButton
        Me.rdExemptionWise = New System.Windows.Forms.RadioButton
        Me.chkIncludeInactiveDependents = New System.Windows.Forms.CheckBox
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.chkShowInactiveDependents = New System.Windows.Forms.CheckBox
        Me.LblCondition = New System.Windows.Forms.Label
        Me.dtpAsonDate = New System.Windows.Forms.DateTimePicker
        Me.LblAsonDate = New System.Windows.Forms.Label
        Me.chkShowExemptedDependents = New System.Windows.Forms.CheckBox
        Me.chkshowdependantswithoutphotos = New System.Windows.Forms.CheckBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.txtLastName = New eZee.TextBox.AlphanumericTextBox
        Me.txtFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.lblLastname = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblFirstname = New System.Windows.Forms.Label
        Me.lblRelation = New System.Windows.Forms.Label
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboRelation = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.chkShowSubGrandTotal = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlInactivedependentoption.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 511)
        Me.NavPanel.Size = New System.Drawing.Size(778, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowSubGrandTotal)
        Me.gbFilterCriteria.Controls.Add(Me.lnInActiveDependentOption)
        Me.gbFilterCriteria.Controls.Add(Me.pnlInactivedependentoption)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeInactiveDependents)
        Me.gbFilterCriteria.Controls.Add(Me.cboCondition)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowInactiveDependents)
        Me.gbFilterCriteria.Controls.Add(Me.LblCondition)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAsonDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblAsonDate)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowExemptedDependents)
        Me.gbFilterCriteria.Controls.Add(Me.chkshowdependantswithoutphotos)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.txtLastName)
        Me.gbFilterCriteria.Controls.Add(Me.txtFirstName)
        Me.gbFilterCriteria.Controls.Add(Me.lblLastname)
        Me.gbFilterCriteria.Controls.Add(Me.lblName)
        Me.gbFilterCriteria.Controls.Add(Me.lblFirstname)
        Me.gbFilterCriteria.Controls.Add(Me.lblRelation)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployeeCode)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.txtCode)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboRelation)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(7, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(455, 372)
        Me.gbFilterCriteria.TabIndex = 17
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnInActiveDependentOption
        '
        Me.lnInActiveDependentOption.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnInActiveDependentOption.Location = New System.Drawing.Point(5, 264)
        Me.lnInActiveDependentOption.Name = "lnInActiveDependentOption"
        Me.lnInActiveDependentOption.Size = New System.Drawing.Size(361, 14)
        Me.lnInActiveDependentOption.TabIndex = 108
        Me.lnInActiveDependentOption.Text = "InActive Dependant Options"
        '
        'pnlInactivedependentoption
        '
        Me.pnlInactivedependentoption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInactivedependentoption.Controls.Add(Me.rdAgeLimitWise)
        Me.pnlInactivedependentoption.Controls.Add(Me.rdExemptionWise)
        Me.pnlInactivedependentoption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlInactivedependentoption.Location = New System.Drawing.Point(28, 308)
        Me.pnlInactivedependentoption.Name = "pnlInactivedependentoption"
        Me.pnlInactivedependentoption.Size = New System.Drawing.Size(412, 55)
        Me.pnlInactivedependentoption.TabIndex = 111
        '
        'rdAgeLimitWise
        '
        Me.rdAgeLimitWise.Checked = True
        Me.rdAgeLimitWise.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAgeLimitWise.Location = New System.Drawing.Point(13, 7)
        Me.rdAgeLimitWise.Name = "rdAgeLimitWise"
        Me.rdAgeLimitWise.Size = New System.Drawing.Size(169, 17)
        Me.rdAgeLimitWise.TabIndex = 109
        Me.rdAgeLimitWise.TabStop = True
        Me.rdAgeLimitWise.Text = "Age Limit Wise"
        Me.rdAgeLimitWise.UseVisualStyleBackColor = True
        '
        'rdExemptionWise
        '
        Me.rdExemptionWise.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdExemptionWise.Location = New System.Drawing.Point(13, 30)
        Me.rdExemptionWise.Name = "rdExemptionWise"
        Me.rdExemptionWise.Size = New System.Drawing.Size(169, 17)
        Me.rdExemptionWise.TabIndex = 110
        Me.rdExemptionWise.TabStop = True
        Me.rdExemptionWise.Text = "Dependant Exception Wise"
        Me.rdExemptionWise.UseVisualStyleBackColor = True
        '
        'chkIncludeInactiveDependents
        '
        Me.chkIncludeInactiveDependents.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveDependents.Location = New System.Drawing.Point(30, 283)
        Me.chkIncludeInactiveDependents.Name = "chkIncludeInactiveDependents"
        Me.chkIncludeInactiveDependents.Size = New System.Drawing.Size(178, 19)
        Me.chkIncludeInactiveDependents.TabIndex = 95
        Me.chkIncludeInactiveDependents.Text = "Include Inactive Dependants"
        Me.chkIncludeInactiveDependents.UseVisualStyleBackColor = True
        '
        'cboCondition
        '
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.DropDownWidth = 230
        Me.cboCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Location = New System.Drawing.Point(295, 62)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(118, 21)
        Me.cboCondition.TabIndex = 106
        '
        'chkShowInactiveDependents
        '
        Me.chkShowInactiveDependents.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowInactiveDependents.Location = New System.Drawing.Point(219, 283)
        Me.chkShowInactiveDependents.Name = "chkShowInactiveDependents"
        Me.chkShowInactiveDependents.Size = New System.Drawing.Size(220, 19)
        Me.chkShowInactiveDependents.TabIndex = 101
        Me.chkShowInactiveDependents.Text = "Show Inactive Dependant Only"
        Me.chkShowInactiveDependents.UseVisualStyleBackColor = True
        '
        'LblCondition
        '
        Me.LblCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCondition.Location = New System.Drawing.Point(216, 64)
        Me.LblCondition.Name = "LblCondition"
        Me.LblCondition.Size = New System.Drawing.Size(74, 15)
        Me.LblCondition.TabIndex = 105
        Me.LblCondition.Text = "Condition"
        Me.LblCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAsonDate
        '
        Me.dtpAsonDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsonDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsonDate.Location = New System.Drawing.Point(92, 61)
        Me.dtpAsonDate.Name = "dtpAsonDate"
        Me.dtpAsonDate.Size = New System.Drawing.Size(118, 21)
        Me.dtpAsonDate.TabIndex = 104
        '
        'LblAsonDate
        '
        Me.LblAsonDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAsonDate.Location = New System.Drawing.Point(8, 64)
        Me.LblAsonDate.Name = "LblAsonDate"
        Me.LblAsonDate.Size = New System.Drawing.Size(78, 15)
        Me.LblAsonDate.TabIndex = 103
        Me.LblAsonDate.Text = "As on Date"
        Me.LblAsonDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowExemptedDependents
        '
        Me.chkShowExemptedDependents.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowExemptedDependents.Location = New System.Drawing.Point(92, 219)
        Me.chkShowExemptedDependents.Name = "chkShowExemptedDependents"
        Me.chkShowExemptedDependents.Size = New System.Drawing.Size(321, 17)
        Me.chkShowExemptedDependents.TabIndex = 99
        Me.chkShowExemptedDependents.Text = "Show Only Exempted Dependants"
        Me.chkShowExemptedDependents.UseVisualStyleBackColor = True
        '
        'chkshowdependantswithoutphotos
        '
        Me.chkshowdependantswithoutphotos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkshowdependantswithoutphotos.Location = New System.Drawing.Point(92, 194)
        Me.chkshowdependantswithoutphotos.Name = "chkshowdependantswithoutphotos"
        Me.chkshowdependantswithoutphotos.Size = New System.Drawing.Size(321, 17)
        Me.chkshowdependantswithoutphotos.TabIndex = 93
        Me.chkshowdependantswithoutphotos.Text = "Show Dependants Without Photos/Picture"
        Me.chkshowdependantswithoutphotos.UseVisualStyleBackColor = True
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(92, 169)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(321, 17)
        Me.chkInactiveemp.TabIndex = 91
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(358, 3)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 89
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtLastName
        '
        Me.txtLastName.Flags = 0
        Me.txtLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLastName.Location = New System.Drawing.Point(295, 142)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(118, 21)
        Me.txtLastName.TabIndex = 87
        '
        'txtFirstName
        '
        Me.txtFirstName.Flags = 0
        Me.txtFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFirstName.Location = New System.Drawing.Point(92, 142)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(118, 21)
        Me.txtFirstName.TabIndex = 86
        '
        'lblLastname
        '
        Me.lblLastname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastname.Location = New System.Drawing.Point(216, 145)
        Me.lblLastname.Name = "lblLastname"
        Me.lblLastname.Size = New System.Drawing.Size(73, 15)
        Me.lblLastname.TabIndex = 85
        Me.lblLastname.Text = "Lastname"
        Me.lblLastname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 145)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(78, 15)
        Me.lblName.TabIndex = 85
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFirstname
        '
        Me.lblFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstname.Location = New System.Drawing.Point(8, 145)
        Me.lblFirstname.Name = "lblFirstname"
        Me.lblFirstname.Size = New System.Drawing.Size(78, 15)
        Me.lblFirstname.TabIndex = 85
        Me.lblFirstname.Text = "Firstname"
        Me.lblFirstname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRelation
        '
        Me.lblRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRelation.Location = New System.Drawing.Point(8, 118)
        Me.lblRelation.Name = "lblRelation"
        Me.lblRelation.Size = New System.Drawing.Size(78, 15)
        Me.lblRelation.TabIndex = 73
        Me.lblRelation.Text = "Relation"
        Me.lblRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(216, 118)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(73, 15)
        Me.lblEmployeeCode.TabIndex = 80
        Me.lblEmployeeCode.Text = "Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(419, 88)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 90)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(78, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(295, 115)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(118, 21)
        Me.txtCode.TabIndex = 81
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(92, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(321, 21)
        Me.cboReportType.TabIndex = 82
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(78, 15)
        Me.lblReportType.TabIndex = 83
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRelation
        '
        Me.cboRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRelation.DropDownWidth = 230
        Me.cboRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRelation.FormattingEnabled = True
        Me.cboRelation.Location = New System.Drawing.Point(92, 115)
        Me.cboRelation.Name = "cboRelation"
        Me.cboRelation.Size = New System.Drawing.Size(118, 21)
        Me.cboRelation.TabIndex = 67
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(92, 88)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(321, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'chkShowSubGrandTotal
        '
        Me.chkShowSubGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSubGrandTotal.Location = New System.Drawing.Point(92, 242)
        Me.chkShowSubGrandTotal.Name = "chkShowSubGrandTotal"
        Me.chkShowSubGrandTotal.Size = New System.Drawing.Size(321, 17)
        Me.chkShowSubGrandTotal.TabIndex = 113
        Me.chkShowSubGrandTotal.Text = "Show Sub And Grand Total Relation Wise"
        Me.chkShowSubGrandTotal.UseVisualStyleBackColor = True
        '
        'frmEmployeeRelationReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 566)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeRelationReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Relation Report"
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.pnlInactivedependentoption.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtLastName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLastname As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblFirstname As System.Windows.Forms.Label
    Friend WithEvents lblRelation As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents cboRelation As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents chkshowdependantswithoutphotos As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeInactiveDependents As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowExemptedDependents As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowInactiveDependents As System.Windows.Forms.CheckBox
    Friend WithEvents LblAsonDate As System.Windows.Forms.Label
    Friend WithEvents dtpAsonDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblCondition As System.Windows.Forms.Label
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lnInActiveDependentOption As eZee.Common.eZeeLine
    Friend WithEvents rdExemptionWise As System.Windows.Forms.RadioButton
    Friend WithEvents rdAgeLimitWise As System.Windows.Forms.RadioButton
    Friend WithEvents pnlInactivedependentoption As System.Windows.Forms.Panel
    Friend WithEvents chkShowSubGrandTotal As System.Windows.Forms.CheckBox
End Class
