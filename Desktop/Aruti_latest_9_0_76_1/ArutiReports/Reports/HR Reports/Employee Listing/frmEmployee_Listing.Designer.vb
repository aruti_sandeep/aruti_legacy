﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployee_Listing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployee_Listing))
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbEmpDisplayName = New System.Windows.Forms.GroupBox
        Me.lblExample = New System.Windows.Forms.Label
        Me.txtNameSetting = New System.Windows.Forms.TextBox
        Me.radShowInCombination = New System.Windows.Forms.RadioButton
        Me.radShowAllSeparately = New System.Windows.Forms.RadioButton
        Me.lblSaveSelection = New System.Windows.Forms.LinkLabel
        Me.chkShowEmpScale = New System.Windows.Forms.CheckBox
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblEmployeementType = New System.Windows.Forms.Label
        Me.cboEmployeementType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.dtpFirstAppointDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblFirstAppointDateTo = New System.Windows.Forms.Label
        Me.dtpFirstAppointDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblFirstAppointDateFrom = New System.Windows.Forms.Label
        Me.dtpEOCDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblEOCDateTo = New System.Windows.Forms.Label
        Me.dtpEOCDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblEOCDateFrom = New System.Windows.Forms.Label
        Me.dtpReinstatementDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblReinstatementDateTo = New System.Windows.Forms.Label
        Me.dtpReinstatementDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblReinstatementDateFrom = New System.Windows.Forms.Label
        Me.dtpSuspendedEndDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblSuspendedEndDateTo = New System.Windows.Forms.Label
        Me.dtpSuspendedEndDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblSuspendedEndDateFrom = New System.Windows.Forms.Label
        Me.dtpAppointmentDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblAppointDateTo = New System.Windows.Forms.Label
        Me.dtpAppointmentDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblAppointDateFrom = New System.Windows.Forms.Label
        Me.dtpSuspendedStartDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblSuspendedStartDateTo = New System.Windows.Forms.Label
        Me.dtpSuspendedStartDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblSuspendedStartDateFrom = New System.Windows.Forms.Label
        Me.dtpProbationEndDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblProbationEndDateTo = New System.Windows.Forms.Label
        Me.dtpProbationEndDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblProbationEndDateFrom = New System.Windows.Forms.Label
        Me.dtpProbationStartDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblProbationStartDateTo = New System.Windows.Forms.Label
        Me.dtpProbationStartDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblProbationStartDateFrom = New System.Windows.Forms.Label
        Me.dtpConfirmationDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblConfirmationDateTo = New System.Windows.Forms.Label
        Me.dtpConfirmationDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblConfirmationDateFrom = New System.Windows.Forms.Label
        Me.dtpAnniversaryDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblAnniversaryDateTo = New System.Windows.Forms.Label
        Me.dtpAnniversaryDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblAnniversaryFrom = New System.Windows.Forms.Label
        Me.dtpBirthdateTo = New System.Windows.Forms.DateTimePicker
        Me.LblBirthdateTo = New System.Windows.Forms.Label
        Me.LblBirthdateFrom = New System.Windows.Forms.Label
        Me.dtpBirthdateFrom = New System.Windows.Forms.DateTimePicker
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbOtherFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbReportType = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.gbColumns = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.pnlFiledSerach = New System.Windows.Forms.Panel
        Me.chkAllChecked = New System.Windows.Forms.CheckBox
        Me.dgvchkEmpfields = New System.Windows.Forms.DataGridView
        Me.objcohchecked = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.cohField = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbEmpDisplayName.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbOtherFilter.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbReportType.SuspendLayout()
        Me.gbColumns.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnlFiledSerach.SuspendLayout()
        CType(Me.dgvchkEmpfields, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(926, 60)
        Me.EZeeHeader1.TabIndex = 0
        Me.EZeeHeader1.Title = ""
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.gbEmpDisplayName)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmpScale)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployeementType)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployeementType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(426, 270)
        Me.gbFilterCriteria.TabIndex = 34
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEmpDisplayName
        '
        Me.gbEmpDisplayName.Controls.Add(Me.lblExample)
        Me.gbEmpDisplayName.Controls.Add(Me.txtNameSetting)
        Me.gbEmpDisplayName.Controls.Add(Me.radShowInCombination)
        Me.gbEmpDisplayName.Controls.Add(Me.radShowAllSeparately)
        Me.gbEmpDisplayName.Controls.Add(Me.lblSaveSelection)
        Me.gbEmpDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmpDisplayName.Location = New System.Drawing.Point(11, 138)
        Me.gbEmpDisplayName.Name = "gbEmpDisplayName"
        Me.gbEmpDisplayName.Size = New System.Drawing.Size(408, 125)
        Me.gbEmpDisplayName.TabIndex = 134
        Me.gbEmpDisplayName.TabStop = False
        Me.gbEmpDisplayName.Text = "Employee Name Display Setting"
        '
        'lblExample
        '
        Me.lblExample.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExample.Location = New System.Drawing.Point(25, 101)
        Me.lblExample.Name = "lblExample"
        Me.lblExample.Size = New System.Drawing.Size(334, 15)
        Me.lblExample.TabIndex = 136
        Me.lblExample.Text = "Sample Format : #FName#-#SName# Put '-' in between"
        Me.lblExample.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNameSetting
        '
        Me.txtNameSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNameSetting.Location = New System.Drawing.Point(24, 77)
        Me.txtNameSetting.Name = "txtNameSetting"
        Me.txtNameSetting.Size = New System.Drawing.Size(340, 21)
        Me.txtNameSetting.TabIndex = 137
        '
        'radShowInCombination
        '
        Me.radShowInCombination.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowInCombination.Location = New System.Drawing.Point(6, 54)
        Me.radShowInCombination.Name = "radShowInCombination"
        Me.radShowInCombination.Size = New System.Drawing.Size(394, 17)
        Me.radShowInCombination.TabIndex = 136
        Me.radShowInCombination.TabStop = True
        Me.radShowInCombination.Text = "Show in desired format set, Please use #FName#, #OName#, #SName#"
        Me.radShowInCombination.UseVisualStyleBackColor = True
        '
        'radShowAllSeparately
        '
        Me.radShowAllSeparately.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowAllSeparately.Location = New System.Drawing.Point(6, 31)
        Me.radShowAllSeparately.Name = "radShowAllSeparately"
        Me.radShowAllSeparately.Size = New System.Drawing.Size(394, 17)
        Me.radShowAllSeparately.TabIndex = 135
        Me.radShowAllSeparately.TabStop = True
        Me.radShowAllSeparately.Text = "Show Firstname, Surname && Othername  Separately"
        Me.radShowAllSeparately.UseVisualStyleBackColor = True
        '
        'lblSaveSelection
        '
        Me.lblSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSaveSelection.BackColor = System.Drawing.SystemColors.Control
        Me.lblSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaveSelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lblSaveSelection.Location = New System.Drawing.Point(305, -2)
        Me.lblSaveSelection.Name = "lblSaveSelection"
        Me.lblSaveSelection.Size = New System.Drawing.Size(90, 17)
        Me.lblSaveSelection.TabIndex = 85
        Me.lblSaveSelection.TabStop = True
        Me.lblSaveSelection.Text = "Save Setting"
        Me.lblSaveSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowEmpScale
        '
        Me.chkShowEmpScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmpScale.Location = New System.Drawing.Point(124, 117)
        Me.chkShowEmpScale.Name = "chkShowEmpScale"
        Me.chkShowEmpScale.Size = New System.Drawing.Size(246, 17)
        Me.chkShowEmpScale.TabIndex = 132
        Me.chkShowEmpScale.Text = "Show Employee Scale"
        Me.chkShowEmpScale.UseVisualStyleBackColor = True
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(330, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 84
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(124, 93)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(246, 16)
        Me.chkInactiveemp.TabIndex = 82
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblEmployeementType
        '
        Me.lblEmployeementType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeementType.Location = New System.Drawing.Point(8, 66)
        Me.lblEmployeementType.Name = "lblEmployeementType"
        Me.lblEmployeementType.Size = New System.Drawing.Size(110, 15)
        Me.lblEmployeementType.TabIndex = 59
        Me.lblEmployeementType.Text = "Employment Type"
        Me.lblEmployeementType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployeementType
        '
        Me.cboEmployeementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeementType.DropDownWidth = 230
        Me.cboEmployeementType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeementType.FormattingEnabled = True
        Me.cboEmployeementType.Location = New System.Drawing.Point(124, 63)
        Me.cboEmployeementType.Name = "cboEmployeementType"
        Me.cboEmployeementType.Size = New System.Drawing.Size(251, 21)
        Me.cboEmployeementType.TabIndex = 60
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(381, 36)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 53
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(124, 36)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(251, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 39)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(110, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFirstAppointDateTo
        '
        Me.dtpFirstAppointDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFirstAppointDateTo.Checked = False
        Me.dtpFirstAppointDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFirstAppointDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFirstAppointDateTo.Location = New System.Drawing.Point(320, 63)
        Me.dtpFirstAppointDateTo.Name = "dtpFirstAppointDateTo"
        Me.dtpFirstAppointDateTo.ShowCheckBox = True
        Me.dtpFirstAppointDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpFirstAppointDateTo.TabIndex = 130
        '
        'LblFirstAppointDateTo
        '
        Me.LblFirstAppointDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFirstAppointDateTo.Location = New System.Drawing.Point(272, 66)
        Me.LblFirstAppointDateTo.Name = "LblFirstAppointDateTo"
        Me.LblFirstAppointDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblFirstAppointDateTo.TabIndex = 129
        Me.LblFirstAppointDateTo.Text = "To"
        Me.LblFirstAppointDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFirstAppointDateFrom
        '
        Me.dtpFirstAppointDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFirstAppointDateFrom.Checked = False
        Me.dtpFirstAppointDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFirstAppointDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFirstAppointDateFrom.Location = New System.Drawing.Point(157, 63)
        Me.dtpFirstAppointDateFrom.Name = "dtpFirstAppointDateFrom"
        Me.dtpFirstAppointDateFrom.ShowCheckBox = True
        Me.dtpFirstAppointDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpFirstAppointDateFrom.TabIndex = 128
        '
        'LblFirstAppointDateFrom
        '
        Me.LblFirstAppointDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFirstAppointDateFrom.Location = New System.Drawing.Point(3, 66)
        Me.LblFirstAppointDateFrom.Name = "LblFirstAppointDateFrom"
        Me.LblFirstAppointDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblFirstAppointDateFrom.TabIndex = 127
        Me.LblFirstAppointDateFrom.Text = "First Appointment Date From"
        Me.LblFirstAppointDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEOCDateTo
        '
        Me.dtpEOCDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEOCDateTo.Checked = False
        Me.dtpEOCDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEOCDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEOCDateTo.Location = New System.Drawing.Point(320, 279)
        Me.dtpEOCDateTo.Name = "dtpEOCDateTo"
        Me.dtpEOCDateTo.ShowCheckBox = True
        Me.dtpEOCDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpEOCDateTo.TabIndex = 125
        '
        'LblEOCDateTo
        '
        Me.LblEOCDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEOCDateTo.Location = New System.Drawing.Point(272, 282)
        Me.LblEOCDateTo.Name = "LblEOCDateTo"
        Me.LblEOCDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblEOCDateTo.TabIndex = 124
        Me.LblEOCDateTo.Text = "To"
        Me.LblEOCDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEOCDateFrom
        '
        Me.dtpEOCDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEOCDateFrom.Checked = False
        Me.dtpEOCDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEOCDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEOCDateFrom.Location = New System.Drawing.Point(157, 279)
        Me.dtpEOCDateFrom.Name = "dtpEOCDateFrom"
        Me.dtpEOCDateFrom.ShowCheckBox = True
        Me.dtpEOCDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpEOCDateFrom.TabIndex = 123
        '
        'LblEOCDateFrom
        '
        Me.LblEOCDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEOCDateFrom.Location = New System.Drawing.Point(3, 282)
        Me.LblEOCDateFrom.Name = "LblEOCDateFrom"
        Me.LblEOCDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblEOCDateFrom.TabIndex = 122
        Me.LblEOCDateFrom.Text = "End Of Contract Date From"
        Me.LblEOCDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpReinstatementDateTo
        '
        Me.dtpReinstatementDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReinstatementDateTo.Checked = False
        Me.dtpReinstatementDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReinstatementDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpReinstatementDateTo.Location = New System.Drawing.Point(320, 252)
        Me.dtpReinstatementDateTo.Name = "dtpReinstatementDateTo"
        Me.dtpReinstatementDateTo.ShowCheckBox = True
        Me.dtpReinstatementDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpReinstatementDateTo.TabIndex = 121
        '
        'LblReinstatementDateTo
        '
        Me.LblReinstatementDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblReinstatementDateTo.Location = New System.Drawing.Point(272, 255)
        Me.LblReinstatementDateTo.Name = "LblReinstatementDateTo"
        Me.LblReinstatementDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblReinstatementDateTo.TabIndex = 120
        Me.LblReinstatementDateTo.Text = "To"
        Me.LblReinstatementDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpReinstatementDateFrom
        '
        Me.dtpReinstatementDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReinstatementDateFrom.Checked = False
        Me.dtpReinstatementDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReinstatementDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpReinstatementDateFrom.Location = New System.Drawing.Point(157, 252)
        Me.dtpReinstatementDateFrom.Name = "dtpReinstatementDateFrom"
        Me.dtpReinstatementDateFrom.ShowCheckBox = True
        Me.dtpReinstatementDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpReinstatementDateFrom.TabIndex = 119
        '
        'LblReinstatementDateFrom
        '
        Me.LblReinstatementDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblReinstatementDateFrom.Location = New System.Drawing.Point(3, 255)
        Me.LblReinstatementDateFrom.Name = "LblReinstatementDateFrom"
        Me.LblReinstatementDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblReinstatementDateFrom.TabIndex = 118
        Me.LblReinstatementDateFrom.Text = "Reinstatement Date From"
        Me.LblReinstatementDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpSuspendedEndDateTo
        '
        Me.dtpSuspendedEndDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedEndDateTo.Checked = False
        Me.dtpSuspendedEndDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedEndDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpSuspendedEndDateTo.Location = New System.Drawing.Point(320, 225)
        Me.dtpSuspendedEndDateTo.Name = "dtpSuspendedEndDateTo"
        Me.dtpSuspendedEndDateTo.ShowCheckBox = True
        Me.dtpSuspendedEndDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpSuspendedEndDateTo.TabIndex = 117
        '
        'LblSuspendedEndDateTo
        '
        Me.LblSuspendedEndDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSuspendedEndDateTo.Location = New System.Drawing.Point(272, 228)
        Me.LblSuspendedEndDateTo.Name = "LblSuspendedEndDateTo"
        Me.LblSuspendedEndDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblSuspendedEndDateTo.TabIndex = 116
        Me.LblSuspendedEndDateTo.Text = "To"
        Me.LblSuspendedEndDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpSuspendedEndDateFrom
        '
        Me.dtpSuspendedEndDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedEndDateFrom.Checked = False
        Me.dtpSuspendedEndDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedEndDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpSuspendedEndDateFrom.Location = New System.Drawing.Point(157, 225)
        Me.dtpSuspendedEndDateFrom.Name = "dtpSuspendedEndDateFrom"
        Me.dtpSuspendedEndDateFrom.ShowCheckBox = True
        Me.dtpSuspendedEndDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpSuspendedEndDateFrom.TabIndex = 115
        '
        'LblSuspendedEndDateFrom
        '
        Me.LblSuspendedEndDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSuspendedEndDateFrom.Location = New System.Drawing.Point(3, 228)
        Me.LblSuspendedEndDateFrom.Name = "LblSuspendedEndDateFrom"
        Me.LblSuspendedEndDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblSuspendedEndDateFrom.TabIndex = 114
        Me.LblSuspendedEndDateFrom.Text = "Suspended End Date From"
        Me.LblSuspendedEndDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAppointmentDateTo
        '
        Me.dtpAppointmentDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppointmentDateTo.Checked = False
        Me.dtpAppointmentDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppointmentDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAppointmentDateTo.Location = New System.Drawing.Point(320, 90)
        Me.dtpAppointmentDateTo.Name = "dtpAppointmentDateTo"
        Me.dtpAppointmentDateTo.ShowCheckBox = True
        Me.dtpAppointmentDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpAppointmentDateTo.TabIndex = 113
        '
        'LblAppointDateTo
        '
        Me.LblAppointDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAppointDateTo.Location = New System.Drawing.Point(272, 93)
        Me.LblAppointDateTo.Name = "LblAppointDateTo"
        Me.LblAppointDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblAppointDateTo.TabIndex = 112
        Me.LblAppointDateTo.Text = "To"
        Me.LblAppointDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAppointmentDateFrom
        '
        Me.dtpAppointmentDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppointmentDateFrom.Checked = False
        Me.dtpAppointmentDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppointmentDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAppointmentDateFrom.Location = New System.Drawing.Point(157, 90)
        Me.dtpAppointmentDateFrom.Name = "dtpAppointmentDateFrom"
        Me.dtpAppointmentDateFrom.ShowCheckBox = True
        Me.dtpAppointmentDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpAppointmentDateFrom.TabIndex = 111
        '
        'LblAppointDateFrom
        '
        Me.LblAppointDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAppointDateFrom.Location = New System.Drawing.Point(3, 93)
        Me.LblAppointDateFrom.Name = "LblAppointDateFrom"
        Me.LblAppointDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblAppointDateFrom.TabIndex = 110
        Me.LblAppointDateFrom.Text = "Appointment Date From"
        Me.LblAppointDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpSuspendedStartDateTo
        '
        Me.dtpSuspendedStartDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedStartDateTo.Checked = False
        Me.dtpSuspendedStartDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedStartDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpSuspendedStartDateTo.Location = New System.Drawing.Point(320, 198)
        Me.dtpSuspendedStartDateTo.Name = "dtpSuspendedStartDateTo"
        Me.dtpSuspendedStartDateTo.ShowCheckBox = True
        Me.dtpSuspendedStartDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpSuspendedStartDateTo.TabIndex = 109
        '
        'LblSuspendedStartDateTo
        '
        Me.LblSuspendedStartDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSuspendedStartDateTo.Location = New System.Drawing.Point(272, 201)
        Me.LblSuspendedStartDateTo.Name = "LblSuspendedStartDateTo"
        Me.LblSuspendedStartDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblSuspendedStartDateTo.TabIndex = 108
        Me.LblSuspendedStartDateTo.Text = "To"
        Me.LblSuspendedStartDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpSuspendedStartDateFrom
        '
        Me.dtpSuspendedStartDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedStartDateFrom.Checked = False
        Me.dtpSuspendedStartDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspendedStartDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpSuspendedStartDateFrom.Location = New System.Drawing.Point(157, 198)
        Me.dtpSuspendedStartDateFrom.Name = "dtpSuspendedStartDateFrom"
        Me.dtpSuspendedStartDateFrom.ShowCheckBox = True
        Me.dtpSuspendedStartDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpSuspendedStartDateFrom.TabIndex = 107
        '
        'LblSuspendedStartDateFrom
        '
        Me.LblSuspendedStartDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSuspendedStartDateFrom.Location = New System.Drawing.Point(3, 201)
        Me.LblSuspendedStartDateFrom.Name = "LblSuspendedStartDateFrom"
        Me.LblSuspendedStartDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblSuspendedStartDateFrom.TabIndex = 106
        Me.LblSuspendedStartDateFrom.Text = "Suspended Start Date From"
        Me.LblSuspendedStartDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpProbationEndDateTo
        '
        Me.dtpProbationEndDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationEndDateTo.Checked = False
        Me.dtpProbationEndDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationEndDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProbationEndDateTo.Location = New System.Drawing.Point(320, 171)
        Me.dtpProbationEndDateTo.Name = "dtpProbationEndDateTo"
        Me.dtpProbationEndDateTo.ShowCheckBox = True
        Me.dtpProbationEndDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpProbationEndDateTo.TabIndex = 105
        '
        'LblProbationEndDateTo
        '
        Me.LblProbationEndDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblProbationEndDateTo.Location = New System.Drawing.Point(272, 174)
        Me.LblProbationEndDateTo.Name = "LblProbationEndDateTo"
        Me.LblProbationEndDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblProbationEndDateTo.TabIndex = 104
        Me.LblProbationEndDateTo.Text = "To"
        Me.LblProbationEndDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpProbationEndDateFrom
        '
        Me.dtpProbationEndDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationEndDateFrom.Checked = False
        Me.dtpProbationEndDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationEndDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProbationEndDateFrom.Location = New System.Drawing.Point(157, 171)
        Me.dtpProbationEndDateFrom.Name = "dtpProbationEndDateFrom"
        Me.dtpProbationEndDateFrom.ShowCheckBox = True
        Me.dtpProbationEndDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpProbationEndDateFrom.TabIndex = 103
        '
        'LblProbationEndDateFrom
        '
        Me.LblProbationEndDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblProbationEndDateFrom.Location = New System.Drawing.Point(3, 174)
        Me.LblProbationEndDateFrom.Name = "LblProbationEndDateFrom"
        Me.LblProbationEndDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblProbationEndDateFrom.TabIndex = 102
        Me.LblProbationEndDateFrom.Text = "Probation End Date From"
        Me.LblProbationEndDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpProbationStartDateTo
        '
        Me.dtpProbationStartDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationStartDateTo.Checked = False
        Me.dtpProbationStartDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationStartDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProbationStartDateTo.Location = New System.Drawing.Point(320, 144)
        Me.dtpProbationStartDateTo.Name = "dtpProbationStartDateTo"
        Me.dtpProbationStartDateTo.ShowCheckBox = True
        Me.dtpProbationStartDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpProbationStartDateTo.TabIndex = 101
        '
        'LblProbationStartDateTo
        '
        Me.LblProbationStartDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblProbationStartDateTo.Location = New System.Drawing.Point(272, 147)
        Me.LblProbationStartDateTo.Name = "LblProbationStartDateTo"
        Me.LblProbationStartDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblProbationStartDateTo.TabIndex = 100
        Me.LblProbationStartDateTo.Text = "To"
        Me.LblProbationStartDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpProbationStartDateFrom
        '
        Me.dtpProbationStartDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationStartDateFrom.Checked = False
        Me.dtpProbationStartDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationStartDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProbationStartDateFrom.Location = New System.Drawing.Point(157, 144)
        Me.dtpProbationStartDateFrom.Name = "dtpProbationStartDateFrom"
        Me.dtpProbationStartDateFrom.ShowCheckBox = True
        Me.dtpProbationStartDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpProbationStartDateFrom.TabIndex = 99
        '
        'LblProbationStartDateFrom
        '
        Me.LblProbationStartDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblProbationStartDateFrom.Location = New System.Drawing.Point(3, 147)
        Me.LblProbationStartDateFrom.Name = "LblProbationStartDateFrom"
        Me.LblProbationStartDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblProbationStartDateFrom.TabIndex = 98
        Me.LblProbationStartDateFrom.Text = "Probation Start Date From"
        Me.LblProbationStartDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpConfirmationDateTo
        '
        Me.dtpConfirmationDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpConfirmationDateTo.Checked = False
        Me.dtpConfirmationDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpConfirmationDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpConfirmationDateTo.Location = New System.Drawing.Point(320, 117)
        Me.dtpConfirmationDateTo.Name = "dtpConfirmationDateTo"
        Me.dtpConfirmationDateTo.ShowCheckBox = True
        Me.dtpConfirmationDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpConfirmationDateTo.TabIndex = 97
        '
        'LblConfirmationDateTo
        '
        Me.LblConfirmationDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblConfirmationDateTo.Location = New System.Drawing.Point(272, 120)
        Me.LblConfirmationDateTo.Name = "LblConfirmationDateTo"
        Me.LblConfirmationDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblConfirmationDateTo.TabIndex = 96
        Me.LblConfirmationDateTo.Text = "To"
        Me.LblConfirmationDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpConfirmationDateFrom
        '
        Me.dtpConfirmationDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpConfirmationDateFrom.Checked = False
        Me.dtpConfirmationDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpConfirmationDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpConfirmationDateFrom.Location = New System.Drawing.Point(157, 117)
        Me.dtpConfirmationDateFrom.Name = "dtpConfirmationDateFrom"
        Me.dtpConfirmationDateFrom.ShowCheckBox = True
        Me.dtpConfirmationDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpConfirmationDateFrom.TabIndex = 95
        '
        'LblConfirmationDateFrom
        '
        Me.LblConfirmationDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblConfirmationDateFrom.Location = New System.Drawing.Point(3, 120)
        Me.LblConfirmationDateFrom.Name = "LblConfirmationDateFrom"
        Me.LblConfirmationDateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblConfirmationDateFrom.TabIndex = 94
        Me.LblConfirmationDateFrom.Text = "Confirmation Date From"
        Me.LblConfirmationDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAnniversaryDateTo
        '
        Me.dtpAnniversaryDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnniversaryDateTo.Checked = False
        Me.dtpAnniversaryDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnniversaryDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAnniversaryDateTo.Location = New System.Drawing.Point(320, 36)
        Me.dtpAnniversaryDateTo.Name = "dtpAnniversaryDateTo"
        Me.dtpAnniversaryDateTo.ShowCheckBox = True
        Me.dtpAnniversaryDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpAnniversaryDateTo.TabIndex = 93
        '
        'LblAnniversaryDateTo
        '
        Me.LblAnniversaryDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAnniversaryDateTo.Location = New System.Drawing.Point(272, 39)
        Me.LblAnniversaryDateTo.Name = "LblAnniversaryDateTo"
        Me.LblAnniversaryDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblAnniversaryDateTo.TabIndex = 92
        Me.LblAnniversaryDateTo.Text = "To"
        Me.LblAnniversaryDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAnniversaryDateFrom
        '
        Me.dtpAnniversaryDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnniversaryDateFrom.Checked = False
        Me.dtpAnniversaryDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnniversaryDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAnniversaryDateFrom.Location = New System.Drawing.Point(157, 36)
        Me.dtpAnniversaryDateFrom.Name = "dtpAnniversaryDateFrom"
        Me.dtpAnniversaryDateFrom.ShowCheckBox = True
        Me.dtpAnniversaryDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpAnniversaryDateFrom.TabIndex = 91
        '
        'LblAnniversaryFrom
        '
        Me.LblAnniversaryFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAnniversaryFrom.Location = New System.Drawing.Point(3, 39)
        Me.LblAnniversaryFrom.Name = "LblAnniversaryFrom"
        Me.LblAnniversaryFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblAnniversaryFrom.TabIndex = 90
        Me.LblAnniversaryFrom.Text = "Anniversary Date From"
        Me.LblAnniversaryFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpBirthdateTo
        '
        Me.dtpBirthdateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpBirthdateTo.Checked = False
        Me.dtpBirthdateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpBirthdateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBirthdateTo.Location = New System.Drawing.Point(320, 9)
        Me.dtpBirthdateTo.Name = "dtpBirthdateTo"
        Me.dtpBirthdateTo.ShowCheckBox = True
        Me.dtpBirthdateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpBirthdateTo.TabIndex = 89
        '
        'LblBirthdateTo
        '
        Me.LblBirthdateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblBirthdateTo.Location = New System.Drawing.Point(272, 12)
        Me.LblBirthdateTo.Name = "LblBirthdateTo"
        Me.LblBirthdateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblBirthdateTo.TabIndex = 88
        Me.LblBirthdateTo.Text = "To"
        Me.LblBirthdateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblBirthdateFrom
        '
        Me.LblBirthdateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblBirthdateFrom.Location = New System.Drawing.Point(3, 12)
        Me.LblBirthdateFrom.Name = "LblBirthdateFrom"
        Me.LblBirthdateFrom.Size = New System.Drawing.Size(148, 15)
        Me.LblBirthdateFrom.TabIndex = 87
        Me.LblBirthdateFrom.Text = "BirthDate From"
        Me.LblBirthdateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpBirthdateFrom
        '
        Me.dtpBirthdateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpBirthdateFrom.Checked = False
        Me.dtpBirthdateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpBirthdateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBirthdateFrom.Location = New System.Drawing.Point(157, 9)
        Me.dtpBirthdateFrom.Name = "dtpBirthdateFrom"
        Me.dtpBirthdateFrom.ShowCheckBox = True
        Me.dtpBirthdateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpBirthdateFrom.TabIndex = 86
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 667)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(926, 55)
        Me.EZeeFooter1.TabIndex = 35
        '
        'btnAdvanceFilter
        '
        Me.btnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.btnAdvanceFilter.BackgroundImage = CType(resources.GetObject("btnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.btnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.btnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.btnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Location = New System.Drawing.Point(530, 13)
        Me.btnAdvanceFilter.Name = "btnAdvanceFilter"
        Me.btnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.btnAdvanceFilter.TabIndex = 36
        Me.btnAdvanceFilter.Text = "Adv. Filter"
        Me.btnAdvanceFilter.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(633, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(729, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(825, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 32
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbOtherFilter
        '
        Me.gbOtherFilter.BorderColor = System.Drawing.Color.Black
        Me.gbOtherFilter.Checked = False
        Me.gbOtherFilter.CollapseAllExceptThis = False
        Me.gbOtherFilter.CollapsedHoverImage = Nothing
        Me.gbOtherFilter.CollapsedNormalImage = Nothing
        Me.gbOtherFilter.CollapsedPressedImage = Nothing
        Me.gbOtherFilter.CollapseOnLoad = False
        Me.gbOtherFilter.Controls.Add(Me.Panel1)
        Me.gbOtherFilter.ExpandedHoverImage = Nothing
        Me.gbOtherFilter.ExpandedNormalImage = Nothing
        Me.gbOtherFilter.ExpandedPressedImage = Nothing
        Me.gbOtherFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtherFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOtherFilter.HeaderHeight = 25
        Me.gbOtherFilter.HeaderMessage = ""
        Me.gbOtherFilter.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOtherFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOtherFilter.HeightOnCollapse = 0
        Me.gbOtherFilter.LeftTextSpace = 0
        Me.gbOtherFilter.Location = New System.Drawing.Point(444, 66)
        Me.gbOtherFilter.Name = "gbOtherFilter"
        Me.gbOtherFilter.OpenHeight = 300
        Me.gbOtherFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOtherFilter.ShowBorder = True
        Me.gbOtherFilter.ShowCheckBox = False
        Me.gbOtherFilter.ShowCollapseButton = False
        Me.gbOtherFilter.ShowDefaultBorderColor = True
        Me.gbOtherFilter.ShowDownButton = False
        Me.gbOtherFilter.ShowHeader = True
        Me.gbOtherFilter.Size = New System.Drawing.Size(470, 348)
        Me.gbOtherFilter.TabIndex = 36
        Me.gbOtherFilter.Temp = 0
        Me.gbOtherFilter.Text = "Other Filters"
        Me.gbOtherFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.LblBirthdateFrom)
        Me.Panel1.Controls.Add(Me.dtpBirthdateFrom)
        Me.Panel1.Controls.Add(Me.LblSuspendedStartDateFrom)
        Me.Panel1.Controls.Add(Me.dtpSuspendedStartDateTo)
        Me.Panel1.Controls.Add(Me.dtpFirstAppointDateTo)
        Me.Panel1.Controls.Add(Me.dtpProbationEndDateTo)
        Me.Panel1.Controls.Add(Me.LblSuspendedStartDateTo)
        Me.Panel1.Controls.Add(Me.LblAppointDateFrom)
        Me.Panel1.Controls.Add(Me.LblFirstAppointDateTo)
        Me.Panel1.Controls.Add(Me.dtpAppointmentDateFrom)
        Me.Panel1.Controls.Add(Me.LblBirthdateTo)
        Me.Panel1.Controls.Add(Me.dtpProbationEndDateFrom)
        Me.Panel1.Controls.Add(Me.dtpFirstAppointDateFrom)
        Me.Panel1.Controls.Add(Me.LblProbationEndDateTo)
        Me.Panel1.Controls.Add(Me.dtpBirthdateTo)
        Me.Panel1.Controls.Add(Me.LblAppointDateTo)
        Me.Panel1.Controls.Add(Me.LblFirstAppointDateFrom)
        Me.Panel1.Controls.Add(Me.LblProbationEndDateFrom)
        Me.Panel1.Controls.Add(Me.dtpEOCDateTo)
        Me.Panel1.Controls.Add(Me.dtpAppointmentDateTo)
        Me.Panel1.Controls.Add(Me.dtpAnniversaryDateFrom)
        Me.Panel1.Controls.Add(Me.dtpProbationStartDateTo)
        Me.Panel1.Controls.Add(Me.LblAnniversaryFrom)
        Me.Panel1.Controls.Add(Me.LblSuspendedEndDateFrom)
        Me.Panel1.Controls.Add(Me.LblEOCDateTo)
        Me.Panel1.Controls.Add(Me.LblProbationStartDateTo)
        Me.Panel1.Controls.Add(Me.dtpSuspendedStartDateFrom)
        Me.Panel1.Controls.Add(Me.dtpSuspendedEndDateFrom)
        Me.Panel1.Controls.Add(Me.dtpEOCDateFrom)
        Me.Panel1.Controls.Add(Me.dtpProbationStartDateFrom)
        Me.Panel1.Controls.Add(Me.LblAnniversaryDateTo)
        Me.Panel1.Controls.Add(Me.LblSuspendedEndDateTo)
        Me.Panel1.Controls.Add(Me.LblEOCDateFrom)
        Me.Panel1.Controls.Add(Me.LblProbationStartDateFrom)
        Me.Panel1.Controls.Add(Me.dtpAnniversaryDateTo)
        Me.Panel1.Controls.Add(Me.dtpSuspendedEndDateTo)
        Me.Panel1.Controls.Add(Me.dtpReinstatementDateTo)
        Me.Panel1.Controls.Add(Me.dtpConfirmationDateTo)
        Me.Panel1.Controls.Add(Me.LblConfirmationDateFrom)
        Me.Panel1.Controls.Add(Me.LblReinstatementDateFrom)
        Me.Panel1.Controls.Add(Me.LblReinstatementDateTo)
        Me.Panel1.Controls.Add(Me.LblConfirmationDateTo)
        Me.Panel1.Controls.Add(Me.dtpConfirmationDateFrom)
        Me.Panel1.Controls.Add(Me.dtpReinstatementDateFrom)
        Me.Panel1.Location = New System.Drawing.Point(2, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(466, 321)
        Me.Panel1.TabIndex = 38
        '
        'gbReportType
        '
        Me.gbReportType.BorderColor = System.Drawing.Color.Black
        Me.gbReportType.Checked = False
        Me.gbReportType.CollapseAllExceptThis = False
        Me.gbReportType.CollapsedHoverImage = Nothing
        Me.gbReportType.CollapsedNormalImage = Nothing
        Me.gbReportType.CollapsedPressedImage = Nothing
        Me.gbReportType.CollapseOnLoad = False
        Me.gbReportType.Controls.Add(Me.lblPeriod)
        Me.gbReportType.Controls.Add(Me.cboPeriod)
        Me.gbReportType.ExpandedHoverImage = Nothing
        Me.gbReportType.ExpandedNormalImage = Nothing
        Me.gbReportType.ExpandedPressedImage = Nothing
        Me.gbReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReportType.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbReportType.HeaderHeight = 25
        Me.gbReportType.HeaderMessage = ""
        Me.gbReportType.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbReportType.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbReportType.HeightOnCollapse = 0
        Me.gbReportType.LeftTextSpace = 0
        Me.gbReportType.Location = New System.Drawing.Point(12, 589)
        Me.gbReportType.Name = "gbReportType"
        Me.gbReportType.OpenHeight = 300
        Me.gbReportType.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbReportType.ShowBorder = True
        Me.gbReportType.ShowCheckBox = True
        Me.gbReportType.ShowCollapseButton = False
        Me.gbReportType.ShowDefaultBorderColor = True
        Me.gbReportType.ShowDownButton = False
        Me.gbReportType.ShowHeader = True
        Me.gbReportType.Size = New System.Drawing.Size(426, 72)
        Me.gbReportType.TabIndex = 37
        Me.gbReportType.Temp = 0
        Me.gbReportType.Text = "Generate Report On Period"
        Me.gbReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 41)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(110, 15)
        Me.lblPeriod.TabIndex = 135
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(124, 38)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(251, 21)
        Me.cboPeriod.TabIndex = 136
        '
        'gbColumns
        '
        Me.gbColumns.BorderColor = System.Drawing.Color.Black
        Me.gbColumns.Checked = False
        Me.gbColumns.CollapseAllExceptThis = False
        Me.gbColumns.CollapsedHoverImage = Nothing
        Me.gbColumns.CollapsedNormalImage = Nothing
        Me.gbColumns.CollapsedPressedImage = Nothing
        Me.gbColumns.CollapseOnLoad = False
        Me.gbColumns.Controls.Add(Me.TableLayoutPanel1)
        Me.gbColumns.ExpandedHoverImage = Nothing
        Me.gbColumns.ExpandedNormalImage = Nothing
        Me.gbColumns.ExpandedPressedImage = Nothing
        Me.gbColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbColumns.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbColumns.HeaderHeight = 25
        Me.gbColumns.HeaderMessage = ""
        Me.gbColumns.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbColumns.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbColumns.HeightOnCollapse = 0
        Me.gbColumns.LeftTextSpace = 0
        Me.gbColumns.Location = New System.Drawing.Point(12, 342)
        Me.gbColumns.Name = "gbColumns"
        Me.gbColumns.OpenHeight = 300
        Me.gbColumns.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbColumns.ShowBorder = True
        Me.gbColumns.ShowCheckBox = False
        Me.gbColumns.ShowCollapseButton = False
        Me.gbColumns.ShowDefaultBorderColor = True
        Me.gbColumns.ShowDownButton = False
        Me.gbColumns.ShowHeader = True
        Me.gbColumns.Size = New System.Drawing.Size(426, 241)
        Me.gbColumns.TabIndex = 38
        Me.gbColumns.Temp = 0
        Me.gbColumns.Text = "Add/Remove Field(s)"
        Me.gbColumns.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.pnlFiledSerach, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtSearch, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(2, 26)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(419, 212)
        Me.TableLayoutPanel1.TabIndex = 83
        '
        'pnlFiledSerach
        '
        Me.pnlFiledSerach.Controls.Add(Me.chkAllChecked)
        Me.pnlFiledSerach.Controls.Add(Me.dgvchkEmpfields)
        Me.pnlFiledSerach.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFiledSerach.Location = New System.Drawing.Point(3, 28)
        Me.pnlFiledSerach.Name = "pnlFiledSerach"
        Me.pnlFiledSerach.Size = New System.Drawing.Size(413, 181)
        Me.pnlFiledSerach.TabIndex = 10
        '
        'chkAllChecked
        '
        Me.chkAllChecked.AutoSize = True
        Me.chkAllChecked.Location = New System.Drawing.Point(6, 4)
        Me.chkAllChecked.Name = "chkAllChecked"
        Me.chkAllChecked.Size = New System.Drawing.Size(15, 14)
        Me.chkAllChecked.TabIndex = 20
        Me.chkAllChecked.UseVisualStyleBackColor = True
        '
        'dgvchkEmpfields
        '
        Me.dgvchkEmpfields.AllowUserToAddRows = False
        Me.dgvchkEmpfields.AllowUserToDeleteRows = False
        Me.dgvchkEmpfields.AllowUserToResizeRows = False
        Me.dgvchkEmpfields.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvchkEmpfields.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvchkEmpfields.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvchkEmpfields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvchkEmpfields.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohchecked, Me.cohField})
        Me.dgvchkEmpfields.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvchkEmpfields.Location = New System.Drawing.Point(0, 0)
        Me.dgvchkEmpfields.Name = "dgvchkEmpfields"
        Me.dgvchkEmpfields.RowHeadersVisible = False
        Me.dgvchkEmpfields.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvchkEmpfields.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvchkEmpfields.Size = New System.Drawing.Size(413, 181)
        Me.dgvchkEmpfields.TabIndex = 9
        '
        'objcohchecked
        '
        Me.objcohchecked.Frozen = True
        Me.objcohchecked.HeaderText = ""
        Me.objcohchecked.Name = "objcohchecked"
        Me.objcohchecked.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcohchecked.Width = 25
        '
        'cohField
        '
        Me.cohField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cohField.HeaderText = "Fields"
        Me.cohField.Name = "cohField"
        Me.cohField.ReadOnly = True
        Me.cohField.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(413, 21)
        Me.txtSearch.TabIndex = 8
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmEmployee_Listing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(926, 722)
        Me.Controls.Add(Me.gbColumns)
        Me.Controls.Add(Me.gbReportType)
        Me.Controls.Add(Me.gbOtherFilter)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeHeader1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmEmployee_Listing"
        Me.Text = "7"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbEmpDisplayName.ResumeLayout(False)
        Me.gbEmpDisplayName.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbOtherFilter.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.gbReportType.ResumeLayout(False)
        Me.gbColumns.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.pnlFiledSerach.ResumeLayout(False)
        Me.pnlFiledSerach.PerformLayout()
        CType(Me.dgvchkEmpfields, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblEmployeementType As System.Windows.Forms.Label
    Friend WithEvents cboEmployeementType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents dtpBirthdateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblBirthdateFrom As System.Windows.Forms.Label
    Friend WithEvents LblBirthdateTo As System.Windows.Forms.Label
    Friend WithEvents dtpBirthdateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpAnniversaryDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblAnniversaryFrom As System.Windows.Forms.Label
    Friend WithEvents dtpAnniversaryDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblAnniversaryDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpConfirmationDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblConfirmationDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpConfirmationDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblConfirmationDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpSuspendedStartDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblSuspendedStartDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpSuspendedStartDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblSuspendedStartDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpProbationEndDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblProbationEndDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpProbationEndDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblProbationEndDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpProbationStartDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblProbationStartDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpProbationStartDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblProbationStartDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpAppointmentDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblAppointDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpAppointmentDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblAppointDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpSuspendedEndDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblSuspendedEndDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpSuspendedEndDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblSuspendedEndDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpEOCDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblEOCDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpEOCDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblEOCDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpReinstatementDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblReinstatementDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpReinstatementDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblReinstatementDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpFirstAppointDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblFirstAppointDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpFirstAppointDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblFirstAppointDateFrom As System.Windows.Forms.Label
    Friend WithEvents chkShowEmpScale As System.Windows.Forms.CheckBox
    Friend WithEvents gbOtherFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbReportType As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbEmpDisplayName As System.Windows.Forms.GroupBox
    Friend WithEvents lblSaveSelection As System.Windows.Forms.LinkLabel
    Friend WithEvents radShowInCombination As System.Windows.Forms.RadioButton
    Friend WithEvents radShowAllSeparately As System.Windows.Forms.RadioButton
    Friend WithEvents txtNameSetting As System.Windows.Forms.TextBox
    Friend WithEvents lblExample As System.Windows.Forms.Label
    Friend WithEvents gbColumns As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pnlFiledSerach As System.Windows.Forms.Panel
    Friend WithEvents chkAllChecked As System.Windows.Forms.CheckBox
    Friend WithEvents dgvchkEmpfields As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcohchecked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cohField As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
