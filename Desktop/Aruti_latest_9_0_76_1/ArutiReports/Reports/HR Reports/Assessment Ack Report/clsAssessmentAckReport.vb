'************************************************************************************************************************************
'Class Name : clsAssessmentAckReport.vb
'Purpose    :
'Date       : 04 DEC 2013
'Written By : Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
''' 
Public Class clsAssessmentAckReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAssessmentAckReport"
    Private mstrReportId As String = enArutiReport.Assessment_Acknowledgment_Report '200
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintReportModeId As Integer = 0
    Private mstrReportModeName As String = String.Empty
    Private mintAppointmentTypeId As Integer = 0
    Private mstrAppointmentTypeName As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    Private mblnFirstNamethenSurname As Boolean = False
    Private mblnConsiderEmployeeTerminationOnPeriodDate As Boolean = False
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    Private mstrAsrRvrIds As String = String.Empty
    Private mstrAsrRvrNames As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property
    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property
    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property
    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property
    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property
    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property
    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property
    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    Public WriteOnly Property _ReportModeId() As Integer
        Set(ByVal value As Integer)
            mintReportModeId = value
        End Set
    End Property
    Public WriteOnly Property _ReportModeName() As String
        Set(ByVal value As String)
            mstrReportModeName = value
        End Set
    End Property
    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property
    Public WriteOnly Property _AppointmentTypeName() As String
        Set(ByVal value As String)
            mstrAppointmentTypeName = value
        End Set
    End Property
    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property
    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    Public WriteOnly Property _ConsiderEmployeeTerminationOnPeriodDate() As Boolean
        Set(ByVal value As Boolean)
            mblnConsiderEmployeeTerminationOnPeriodDate = value
        End Set
    End Property
    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property
    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property
    Public WriteOnly Property _AsrRvrIds() As String
        Set(ByVal value As String)
            mstrAsrRvrIds = value
        End Set
    End Property
    Public WriteOnly Property _AsrRvrNames() As String
        Set(ByVal value As String)
            mstrAsrRvrNames = value
        End Set
    End Property
    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property
    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrUserAccessFilter = String.Empty
            mstrAdvance_Filter = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mintReportModeId = 0
            mstrReportModeName = String.Empty
            mintAppointmentTypeId = 0
            mstrAppointmentTypeName = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mblnConsiderEmployeeTerminationOnPeriodDate = False
            mintStatusId = 0
            mstrStatusName = String.Empty
            mstrAsrRvrIds = String.Empty
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function Export_Acknowledgement_Report(ByVal strDatabaseName As String, _
                                                  ByVal intUserUnkid As Integer, _
                                                  ByVal intYearUnkid As Integer, _
                                                  ByVal intCompanyUnkid As Integer, _
                                                  ByVal dtPeriodStart As Date, _
                                                  ByVal dtPeriodEnd As Date, _
                                                  ByVal strUserModeSetting As String, _
                                                  ByVal blnOnlyApproved As Boolean, _
                                                  ByVal strExportPath As String, _
                                                  ByVal blnOpenAfterExport As Boolean) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim dtExcelTable As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        Try

            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            If mblnConsiderEmployeeTerminationOnPeriodDate = True Then
                If mintPeriodId > 0 Then
                    Dim objPrd As New clscommom_period_Tran
                    objPrd._Periodunkid(strDatabaseName) = mintPeriodId
                    dtPeriodStart = objPrd._Start_Date
                    dtPeriodEnd = objPrd._End_Date
                    objPrd = Nothing
                End If
            End If

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            dtExcelTable = New DataTable("OmanSummary")
            Dim dCol As DataColumn = Nothing

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 100, "Sr.No.")
                .ColumnName = "SrNo"
                .AutoIncrement = True
                .AutoIncrementSeed = 1
                .AutoIncrementStep = 1
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 101, "Employee Code")
                .ColumnName = "EmpNo"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 102, "Employee Name")
                .ColumnName = "ename"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 103, "Status")
                .ColumnName = "ackstatus"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                If mintReportModeId = 1 Then
                    .Caption = Language.getMessage(mstrModuleName, 104, "Assessor Name")
                ElseIf mintReportModeId = 2 Then
                    .Caption = Language.getMessage(mstrModuleName, 105, "Reviewer Name")
                End If
                .ColumnName = "assessorname"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 106, "Apprisee's Comments")
                .ColumnName = "akcomments"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = ""
                .ColumnName = "assessormasterunkid"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtExcelTable.Columns.Add(dCol)

            Dim strInQ As String = String.Empty


            strInQ = "SELECT " & _
                     "     EP.employeecode AS EmpNo " & _
                     "    ,EP.firstname + ' ' + EP.surname as ename " & _
                     "    ,case when statusunkid = 1 then @agr " & _
                     "          when statusunkid = 2 then @dgr " & _
                     "     end as ackstatus " & _
                     "    ,#ASSR_NAME# AS assessorname " & _
                     "    ,acknowledgement_comments as akcomments " & _
                     "    ,hrassessor_master.assessormasterunkid as assessormasterunkid " & _
                     "FROM hrassess_acknowledgement_tran " & _
                     "    JOIN hremployee_master AS EP ON EP.employeeunkid = hrassess_acknowledgement_tran.employeeunkid " & _
                     "    JOIN hrevaluation_analysis_master ON hrassess_acknowledgement_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                     "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid " & _
                     "    #EMP_JOIN# " & _
                     "    #DAE_JOIN# " & _
                     "    #UAC_JOIN# " & _
                     "    #ADV_JOIN# " & _
                     " WHERE hrassess_acknowledgement_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = @assessmodeid " & _
                     " AND hrassessor_master.isexternalapprover = #ExValue# AND hrassess_acknowledgement_tran.periodunkid = @periodunkid " & _
                     " #DAE_FLTR# "

            Select Case mintReportModeId
                Case 1  'ASSESSOR
                    strInQ &= " AND hrassessor_master.isreviewer = 0 "
                Case 2  'REVIEWER
                    strInQ &= " AND hrassessor_master.isreviewer = 1 "
            End Select

            If mstrAsrRvrIds.Trim.Length > 0 Then
                strInQ &= " AND hrassessor_master.assessormasterunkid IN (" & mstrAsrRvrIds & ")"
            End If

            If mintEmployeeId > 0 Then
                strInQ &= " AND hrassess_acknowledgement_tran.employeeunkid = @employeeunkid "
            End If

            If mintStatusId > 0 Then
                strInQ &= " AND hrassess_acknowledgement_tran.statusunkid = @statusunkid "
            End If

            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        strInQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        strInQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        strInQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select


            StrQ = strInQ
            StrQ = StrQ.Replace("#DAE_JOIN#", xDateJoinQry & " ")
            StrQ = StrQ.Replace("#UAC_JOIN#", xUACQry & " ")
            StrQ = StrQ.Replace("#ADV_JOIN#", xAdvanceJoinQry & " ")
            StrQ = StrQ.Replace("#DAE_FLTR#", xDateFilterQry & " ")
            StrQ = StrQ.Replace("#ASSR_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') ")
            StrQ = StrQ.Replace("#ExValue#", "0")
            StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid ")

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
            objDataOperation.AddParameter("@agr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 113, "Agree"))
            objDataOperation.AddParameter("@dgr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 114, "Disagree"))

            Select Case mintReportModeId
                Case 1  'ASSESSOR
                    objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enAssessmentMode.APPRAISER_ASSESSMENT)
                Case 2  'REVIEWER
                    objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enAssessmentMode.REVIEWER_ASSESSMENT)
            End Select

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each r As DataRow In dsList.Tables("List").Rows
                dtExcelTable.ImportRow(r)
            Next

            Dim objAsr As New clsAssessor : Dim dsExAR As New DataSet

            dsExAR = objAsr.GetExternalAssessorReviewerList(objDataOperation, "List", , , , , IIf(mintReportModeId = 2, clsAssessor.enAssessorType.REVIEWER, clsAssessor.enAssessorType.ASSESSOR))

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If
            dsList = New DataSet
            For Each dr In dsExAR.Tables("List").Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                objDataOperation.AddParameter("@agr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 113, "Agree"))
                objDataOperation.AddParameter("@dgr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 114, "Disagree"))
                Select Case mintReportModeId
                    Case 1  'ASSESSOR
                        objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enAssessmentMode.APPRAISER_ASSESSMENT)
                    Case 2  'REVIEWER
                        objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enAssessmentMode.REVIEWER_ASSESSMENT)
                End Select

                Dim dstmp As New DataSet
                StrQ = strInQ
                If dr("DBName").ToString.Trim.Length <= 0 AndAlso dr("companyunkid") <= 0 Then
                    StrQ = StrQ.Replace("#DAE_JOIN#", "")
                    StrQ = StrQ.Replace("#UAC_JOIN#", "")
                    StrQ = StrQ.Replace("#ADV_JOIN#", "")
                    StrQ = StrQ.Replace("#DAE_FLTR#", " AND UEmp.isactive = 1 ")
                    StrQ = StrQ.Replace("#ASSR_NAME#", "ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#ExValue#", "1")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid ")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EMPASONDATE").ToString()).Date, eZeeDate.convertDate(dr("EMPASONDATE").ToString()).Date, , , dr("DBName"))
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(dr("EMPASONDATE").ToString()).Date, blnOnlyApproved, dr("DBName"), intUserUnkid, dr("companyunkid"), dr("yearunkid"), dr("ModeSet"))
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EMPASONDATE").ToString()).Date, dr("DBName"))

                    StrQ = StrQ.Replace("#DAE_JOIN#", xDateJoinQry & " ")
                    StrQ = StrQ.Replace("#UAC_JOIN#", xUACQry & " ")
                    StrQ = StrQ.Replace("#ADV_JOIN#", xAdvanceJoinQry & " ")
                    StrQ = StrQ.Replace("#DAE_FLTR#", xDateFilterQry & " ")
                    StrQ = StrQ.Replace("#ASSR_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') ")
                    StrQ = StrQ.Replace("#ExValue#", "0")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid " & _
                                                      " JOIN #DName#hremployee_master ON hremployee_master.employeeunkid = UEmp.employeeunkid ")
                    StrQ = StrQ.Replace("#DName#", dr("DBName") & "..")
                End If
                StrQ &= " AND UEmp.companyunkid = '" & dr("companyunkid") & "' "

                dstmp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next

            If dsList.Tables.Count > 0 Then
                For Each r As DataRow In dsList.Tables(0).Rows
                    dtExcelTable.ImportRow(r)
                Next
            End If

            If mintStatusId > 0 Then
                dtExcelTable.Columns.Remove("ackstatus")
            End If

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            row = New WorksheetRow()

            Dim strFilterStringTitle As String = ""

            If mintPeriodId > 0 Then
                strFilterStringTitle &= "," & Language.getMessage(mstrModuleName, 107, "Assessment Period :") & " " & mstrPeriodName
            End If

            If mintReportModeId > 0 Then
                strFilterStringTitle &= "," & Language.getMessage(mstrModuleName, 108, "Report Mode :") & " " & mstrReportModeName
            End If

            If mintEmployeeId > 0 Then
                strFilterStringTitle &= "," & Language.getMessage(mstrModuleName, 109, "Employee :") & " " & mstrEmployeeName
            End If

            If mintStatusId > 0 Then
                strFilterStringTitle &= "," & Language.getMessage(mstrModuleName, 110, "Status :") & " " & mstrStatusName
            End If

            If mstrAsrRvrIds.Trim.Length > 0 Then
                If mstrAsrRvrIds.ToString().Split(",").Length = 1 Then
                    strFilterStringTitle &= "," & mstrReportModeName & " : " & mstrAsrRvrNames.ToString().Split(",")(0)
                Else
                    strFilterStringTitle &= "," & mstrReportModeName & " : " & Language.getMessage(mstrModuleName, 111, "Multiple Checked")
                End If
            Else
                strFilterStringTitle &= "," & mstrReportModeName & " : " & Language.getMessage(mstrModuleName, 112, "All")
            End If

            strFilterStringTitle = Mid(strFilterStringTitle, 2)

            wcell = New WorksheetCell(strFilterStringTitle.ToString, "s10bw")
            row.Cells.Add(wcell)

            Dim xTable As DataTable = Nothing
            xTable = New DataView(dtExcelTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "assessorname", "assessormasterunkid")
            dCol = New DataColumn
            With dCol
                .Caption = ""
                .ColumnName = "summary"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            xTable.Columns.Add(dCol)

            StrQ = "select distinct " & _
                   "     hrevaluation_analysis_master.assessormasterunkid " & _
                   "    ,count(1) as tep " & _
                   "    ,isnull(a.atot,0) as agr " & _
                   "    ,isnull(d.dtot,0) as dgr " & _
                   "from hrevaluation_analysis_master " & _
                   "    join hrassess_acknowledgement_tran on hrassess_acknowledgement_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                   "    join hrassessor_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid " & _
                   "    join hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                   "    left join " & _
                   "    ( " & _
                   "        select " & _
                   "             hrevaluation_analysis_master.assessormasterunkid " & _
                   "            ,count(1) as atot " & _
                   "        from hrevaluation_analysis_master " & _
                   "            join hrassess_acknowledgement_tran on hrassess_acknowledgement_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                   "        where hrevaluation_analysis_master.periodunkid = @periodunkid and assessmodeid = @assessmodeid and hrevaluation_analysis_master.isvoid = 0 " & _
                   "        and hrassess_acknowledgement_tran.isvoid = 0 and statusunkid = 1 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hrassess_acknowledgement_tran.employeeunkid = @employeeunkid "
            End If
            StrQ &= "        group by hrevaluation_analysis_master.assessormasterunkid " & _
                    "    ) as a on hrassessor_master.assessormasterunkid = a.assessormasterunkid " & _
                    "    left join " & _
                    "    ( " & _
                    "        select " & _
                    "             hrevaluation_analysis_master.assessormasterunkid " & _
                    "            ,count(1) as dtot " & _
                    "        from hrevaluation_analysis_master " & _
                    "            join hrassess_acknowledgement_tran on hrassess_acknowledgement_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "        where hrevaluation_analysis_master.periodunkid = @periodunkid and assessmodeid = @assessmodeid and hrevaluation_analysis_master.isvoid = 0 " & _
                    "            and hrassess_acknowledgement_tran.isvoid = 0 and statusunkid = 2 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hrassess_acknowledgement_tran.employeeunkid = @employeeunkid "
            End If
            StrQ &= "        group by hrevaluation_analysis_master.assessormasterunkid " & _
                    "    ) as d on hrassessor_master.assessormasterunkid = d.assessormasterunkid " & _
                    "where hrevaluation_analysis_master.periodunkid = @periodunkid and assessmodeid = @assessmodeid and hrassessor_master.isvoid = 0 " & _
                    "    and hrevaluation_analysis_master.isvoid = 0 and hrassessor_master.visibletypeid = 1 and hrassessor_tran.visibletypeid = 1 " & _
                    "    and hrassess_acknowledgement_tran.isvoid = 0 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hrassess_acknowledgement_tran.employeeunkid = @employeeunkid "
            End If

            If mstrAsrRvrIds.Trim.Length > 0 Then
                StrQ &= " AND hrassessor_master.assessormasterunkid IN (" & mstrAsrRvrIds & ")"
            End If

            StrQ &= "group by hrevaluation_analysis_master.assessormasterunkid,a.atot,d.dtot "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            Select Case mintReportModeId
                Case 1  'ASSESSOR
                    objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enAssessmentMode.APPRAISER_ASSESSMENT)
                Case 2  'REVIEWER
                    objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enAssessmentMode.REVIEWER_ASSESSMENT)
            End Select

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If xTable.Rows.Count > 0 Then

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 116, "Summary By") & " : " & mstrReportModeName, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                'row = New WorksheetRow()
                'wcell = New WorksheetCell("", "s10bw")
                'row.Cells.Add(wcell)
                'rowsArrayFooter.Add(row)

                For Each r As DataRow In xTable.Rows
                    Dim tmp() As DataRow = dsList.Tables(0).Select("assessormasterunkid = '" & r("assessormasterunkid") & "'")
                    If tmp.Length > 0 Then
                        Select Case mintStatusId
                            Case clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE
                                r("summary") = tmp(0)("agr").ToString() & "/" & tmp(0)("tep").ToString()
                            Case clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE
                                r("summary") = tmp(0)("dgr").ToString() & "/" & tmp(0)("tep").ToString()
                            Case Else
                                r("summary") = Language.getMessage(mstrModuleName, 113, "Agree") & " : " & _
                                               tmp(0)("agr").ToString() & "/" & Language.getMessage(mstrModuleName, 114, "Disagree") & " : " & _
                                               tmp(0)("dgr").ToString() & "/" & Language.getMessage(mstrModuleName, 115, "Total Employee") & " : " & tmp(0)("tep").ToString()
                        End Select

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(r("assessorname") & " -> " & r("summary"), "s8bw")
                        wcell.MergeAcross = 4
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If
                Next
            End If

            If dtExcelTable.Columns.Contains("assessormasterunkid") Then
                dtExcelTable.Columns.Remove("assessormasterunkid")
            End If

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtExcelTable.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 70
                ElseIf i = intArrayColumnWidth.Length - 1 Then
                    intArrayColumnWidth(i) = 350
                Else
                    intArrayColumnWidth(i) = 100
                End If
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dtExcelTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", strFilterStringTitle, Nothing, "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Acknowledgement_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Sr.No.")
            Language.setMessage(mstrModuleName, 101, "Employee Code")
            Language.setMessage(mstrModuleName, 102, "Employee Name")
            Language.setMessage(mstrModuleName, 103, "Status")
            Language.setMessage(mstrModuleName, 104, "Assessor Name")
            Language.setMessage(mstrModuleName, 105, "Reviewer Name")
            Language.setMessage(mstrModuleName, 106, "Apprisee's Comments")
            Language.setMessage(mstrModuleName, 107, "Assessment Period :")
            Language.setMessage(mstrModuleName, 108, "Report Mode :")
            Language.setMessage(mstrModuleName, 109, "Employee :")
            Language.setMessage(mstrModuleName, 110, "Status :")
            Language.setMessage(mstrModuleName, 111, "Multiple Checked")
            Language.setMessage(mstrModuleName, 112, "All")
            Language.setMessage(mstrModuleName, 113, "Agree")
            Language.setMessage(mstrModuleName, 114, "Disagree")
            Language.setMessage(mstrModuleName, 115, "Total Employee")
            Language.setMessage(mstrModuleName, 116, "Summary By")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
