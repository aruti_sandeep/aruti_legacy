'************************************************************************************************************************************
'Class Name : frmAssessmentAckReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessmentAckReport

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentAckReport"
    Private objAckReport As clsAssessmentAckReport
    Dim dvEmpView As DataView
    Dim mdtEmployee As DataTable
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        objAckReport = New clsAssessmentAckReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAckReport.SetDefaultValue()
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            Dim iCboItems() As ComboBoxValue = {New ComboBoxValue(Language.getMessage(mstrModuleName, 1, "Select"), 0), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 2, "Agree"), clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 3, "Disagree"), clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE)}

            With cboAckStatus
                .Items.Clear()
                .Items.AddRange(iCboItems)
                .SelectedIndex = 2
                .SelectedValue = CType(cboAckStatus.SelectedItem, ComboBoxValue).Value
            End With

            dsList = objMaster.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 1
            End With

            With cboReportMode
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Assessor"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Reviewer"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboAckStatus.SelectedIndex = 2
            cboAppointment.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboReportMode.SelectedIndex = 0
            If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
            dtpDate1.Checked = False
            dtpDate2.Checked = False
            chkConsiderEmployeeTermination.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAckReport.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboReportMode.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Report mode."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Return False
            End If

            'If CType(cboAckStatus.SelectedItem, ComboBoxValue).Value <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Acknowledgement status."), enMsgBoxStyle.Information)
            '    cboAckStatus.Focus()
            '    Return False
            'End If

            objAckReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objAckReport._PeriodName = cboPeriod.Text
            objAckReport._ReportModeId = cboReportMode.SelectedIndex
            objAckReport._ReportModeName = cboReportMode.Text
            objAckReport._AppointmentTypeId = cboAppointment.SelectedValue
            objAckReport._AppointmentTypeName = cboAppointment.Text
            If dtpDate1.Checked = True Then objAckReport._Date1 = dtpDate1.Value
            If dtpDate2.Checked = True Then objAckReport._Date2 = dtpDate2.Value
            objAckReport._ConsiderEmployeeTerminationOnPeriodDate = chkConsiderEmployeeTermination.Checked
            objAckReport._StatusId = CType(cboAckStatus.SelectedItem, ComboBoxValue).Value
            objAckReport._StatusName = CType(cboAckStatus.SelectedItem, ComboBoxValue).Display
            objAckReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objAckReport._EmployeeName = cboEmployee.Text
            mdtEmployee.AcceptChanges()
            If mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                objAckReport._AsrRvrIds = String.Join(",", mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("assessormasterunkid").ToString()).ToArray())
                objAckReport._AsrRvrNames = String.Join(",", mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)("assessorname").ToString()).ToArray())
            End If
            objAckReport._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillGrid()
        Dim objAsr As New clsAssessor
        Dim dsList As New DataSet
        Try
            Dim blnIsReviewer As Boolean = False
            Select Case cboReportMode.SelectedIndex
                Case 1  'ASSESSOR
                    blnIsReviewer = False
                Case 2  'REVIEWER
                    blnIsReviewer = True
            End Select
            dsList = objAsr.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, False, "List", blnIsReviewer, clsAssessor.enARVisibilityTypeId.VISIBLE, False, "", False)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dCol As New DataColumn
                With dCol
                    .ColumnName = "ischeck"
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = False
                End With
                dsList.Tables(0).Columns.Add(dCol)
                mdtEmployee = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "ischeck", "employeecode", "assessorname", "assessormasterunkid")

                dgvEmployee.AutoGenerateColumns = False
                objdgcolhEName.DataPropertyName = "assessorname"
                objdgcolhEmpId.DataPropertyName = "assessormasterunkid"
                objdgcolhECheck.DataPropertyName = "ischeck"
                dvEmpView = mdtEmployee.DefaultView()
                dgvEmployee.DataSource = dvEmpView

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
            objAsr = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet
            Dim dtStartDate, dtEndDate As Date
            dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            dtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                dtStartDate = mdtPeriodStart
                dtEndDate = mdtPeriodEnd
            Else
                mdtPeriodStart = Nothing
                mdtPeriodEnd = Nothing
                dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportMode.SelectedIndexChanged
        Try
            If cboReportMode.SelectedIndex > 0 Then
                objdgcolhEName.HeaderText = cboReportMode.Text
                tblpAssessorEmployee.Enabled = True
                Call FillGrid()
            Else
                tblpAssessorEmployee.Enabled = False
                If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
                dgvEmployee.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportMode_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmAssessmentAckReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAckReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentAckReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentAckReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("TAB")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentAckReport_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentAckReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            EZeeHeader1.Title = objAckReport._ReportName
            EZeeHeader1.Message = objAckReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentAckReport_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = dgvEmployee.Rows(dgvEmployee.RowCount - 1).Index Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = objdgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dvEmpView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
            For Each dr As DataRowView In dvEmpView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvEmployee.Refresh()
            AddHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event "

    Private Sub dgvEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvEmployee.IsCurrentCellDirty Then
                    Me.dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dvEmpView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dvEmpView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmp.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessmentAckReport.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessmentAckReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objAckReport.Export_Acknowledgement_Report(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       mdtPeriodStart, mdtPeriodEnd, _
                                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                                       ConfigParameter._Object._ExportReportPath, _
                                                       ConfigParameter._Object._OpenAfterExport)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.chkConsiderEmployeeTermination.Text = Language._Object.getCaption(Me.chkConsiderEmployeeTermination.Name, Me.chkConsiderEmployeeTermination.Text)
            Me.lblAppointment.Text = Language._Object.getCaption(Me.lblAppointment.Name, Me.lblAppointment.Text)
            Me.lblReportMode.Text = Language._Object.getCaption(Me.lblReportMode.Name, Me.lblReportMode.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblAckStatus.Text = Language._Object.getCaption(Me.lblAckStatus.Name, Me.lblAckStatus.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Agree")
            Language.setMessage(mstrModuleName, 3, "Disagree")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Assessor Wise")
            Language.setMessage(mstrModuleName, 6, "Reviewer Wise")
            Language.setMessage(mstrModuleName, 7, "Please select Period.")
            Language.setMessage(mstrModuleName, 8, "Please select Report mode.")
            Language.setMessage(mstrModuleName, 9, "Please select Acknowledgement status.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
