'************************************************************************************************************************************
'Class Name : clsEmployeeWithOutReport.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsEmployeeWithOutReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeWithOutReport"
    Private mstrReportId As String = enArutiReport.Employee_With_Out_Report
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrOrderByQuery As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintReportTypeId As Integer = -1
    Private mstrReportTypeName As String = String.Empty
    Private mblnIsActive As Boolean = False
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    'Gajanan [31-Aug-2021] -- Start
    Private mblnIsNameConsolidate As Boolean = False
    'Gajanan [31-Aug-2021] -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    'Gajanan [31-Aug-2021] -- Start
    Public WriteOnly Property _Is_Name_Consolidate() As Boolean
        Set(ByVal value As Boolean)
            mblnIsNameConsolidate = value
        End Set
    End Property
    'Gajanan [31-Aug-2021] -- End
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrOrderByQuery = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mintReportTypeId = -1
            mstrReportTypeName = String.Empty
            mblnIsActive = False
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrUserAccessFilter = String.Empty
            mstrAdvance_Filter = String.Empty
            'Gajanan [31-Aug-2021] -- Start
            mblnIsNameConsolidate = False
            'Gajanan [31-Aug-2021] -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'Finally
        'End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 3, "Code")))
            iColumn_DetailReport.Add(New IColumn("firstname", Language.getMessage(mstrModuleName, 4, "Firstname")))
            iColumn_DetailReport.Add(New IColumn("othername", Language.getMessage(mstrModuleName, 5, "Othername")))
            iColumn_DetailReport.Add(New IColumn("surname", Language.getMessage(mstrModuleName, 6, "Surname")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(CG.name,'')", Language.getMessage(mstrModuleName, 7, "Location/Class Group")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            objDataOperation.ClearParameters()

            StrQ = "SELECT " & _
                   "   employeecode " & _
                   "  ,firstname " & _
                   "  ,othername " & _
                   "  ,surname " & _
                   "  ,ISNULL(CG.name,'') As location "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'S.SANDEEP [26 JUL 2016] -- START
            'ISSUE : OLD JOIN WAS NOT REPLACED BY NEW ONE
            'StrQ &= "FROM hremployee_master " & _
            '        "	LEFT JOIN hrclassgroup_master AS CG on CG.classgroupunkid = hremployee_master.classgroupunkid "

            StrQ &= "FROM hremployee_master "
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            StrQ &= "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            classgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN hrclassgroup_master AS CG on CG.classgroupunkid = Alloc.classgroupunkid "

            'S.SANDEEP [26 JUL 2016] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= " WHERE 1 = 1 "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If
            'Shani(24-Aug-2015) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Shani(24-Aug-2015) -- End

            Select Case mintReportTypeId
                Case 0  'DEPENDENTS

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hrdependants_beneficiaries_tran WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hrdependants_beneficiaries_tran WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 1  'QUALIFICATION

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hremp_qualification_tran WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hremp_qualification_tran WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 2  'EXPERIENCES

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hremp_experience_tran WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hremp_experience_tran WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 3  'REFERENCES

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_referee_tran WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_referee_tran WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 4  'SKILLS

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT emp_app_unkid FROM hremp_app_skills_tran WHERE isvoid = 0 AND isapplicant = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT emp_app_unkid FROM hremp_app_skills_tran WHERE isvoid = 0 AND isapplicant = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 5  'BENEFITS

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hremp_benefit_coverage WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hremp_benefit_coverage WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 6  'COMPANY ASSETS

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_assets_tran WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_assets_tran WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 7  'BRANCH
                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.stationunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                            "   SELECT EmpTran.employeeunkid FROM ( " & _
                            "       SELECT " & _
                            "            hremployee_transfer_tran.employeeunkid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                            "       FROM hremployee_transfer_tran " & _
                            "       WHERE   isvoid = 0 " & _
                            "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            "               AND ISNULL(hremployee_transfer_tran.stationunkid,0) <= 0 " & _
                            "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                            ") "
                    'Shani(24-Aug-2015) -- End

                Case 8  'DEPARTMENT GROUP

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.deptgroupunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                            "   SELECT EmpTran.employeeunkid FROM ( " & _
                            "       SELECT " & _
                            "            hremployee_transfer_tran.employeeunkid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                            "       FROM hremployee_transfer_tran " & _
                            "       WHERE   isvoid = 0 " & _
                            "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            "               AND ISNULL(hremployee_transfer_tran.deptgroupunkid,0) <= 0 " & _
                            "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                            ") "
                    'Shani(24-Aug-2015) -- End
                Case 9  'DEPARTMENT

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.departmentunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                            "   SELECT EmpTran.employeeunkid FROM ( " & _
                            "       SELECT " & _
                            "            hremployee_transfer_tran.employeeunkid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                            "       FROM hremployee_transfer_tran " & _
                            "       WHERE   isvoid = 0 " & _
                            "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            "               AND ISNULL(hremployee_transfer_tran.departmentunkid,0) <= 0 " & _
                            "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                            ") "
                    'Shani(24-Aug-2015) -- End
                Case 10 'SECTION GROUP

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.sectiongroupunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                           "   SELECT EmpTran.employeeunkid FROM ( " & _
                           "       SELECT " & _
                           "            hremployee_transfer_tran.employeeunkid " & _
                           "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                           "       FROM hremployee_transfer_tran " & _
                           "       WHERE   isvoid = 0 " & _
                           "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                           "               AND ISNULL(hremployee_transfer_tran.sectiongroupunkid,0) <= 0 " & _
                           "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                           ") "
                    'Shani(24-Aug-2015) -- End
                Case 11 'SECTION
                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.sectionunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                           "   SELECT EmpTran.employeeunkid FROM ( " & _
                           "       SELECT " & _
                           "            hremployee_transfer_tran.employeeunkid " & _
                           "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                           "       FROM hremployee_transfer_tran " & _
                           "       WHERE   isvoid = 0 " & _
                           "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                           "               AND ISNULL(hremployee_transfer_tran.sectionunkid,0) <= 0 " & _
                           "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                           ") "
                    'Shani(24-Aug-2015) -- End
                Case 12 'UNIT GROUP

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.unitgroupunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                          "   SELECT EmpTran.employeeunkid FROM ( " & _
                          "       SELECT " & _
                          "            hremployee_transfer_tran.employeeunkid " & _
                          "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                          "       FROM hremployee_transfer_tran " & _
                          "       WHERE   isvoid = 0 " & _
                          "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                          "               AND ISNULL(hremployee_transfer_tran.unitgroupunkid,0) <= 0 " & _
                          "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                          ") "
                    'Shani(24-Aug-2015) -- End

                Case 13 'UNIT

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.unitunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                         "   SELECT EmpTran.employeeunkid FROM ( " & _
                         "       SELECT " & _
                         "            hremployee_transfer_tran.employeeunkid " & _
                         "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                         "       FROM hremployee_transfer_tran " & _
                         "       WHERE   isvoid = 0 " & _
                         "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                         "               AND ISNULL(hremployee_transfer_tran.unitunkid,0) <= 0 " & _
                         "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                         ") "
                    'Shani(24-Aug-2015) -- End
                Case 14 'TEAM

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.teamunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                        "   SELECT EmpTran.employeeunkid FROM ( " & _
                        "       SELECT " & _
                        "            hremployee_transfer_tran.employeeunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                        "       FROM hremployee_transfer_tran " & _
                        "       WHERE   isvoid = 0 " & _
                        "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        "               AND ISNULL(hremployee_transfer_tran.teamunkid,0) <= 0 " & _
                        "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                        ") "
                    'Shani(24-Aug-2015) -- End
                Case 15 'JOB GROUP

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.jobgroupunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                       "   SELECT EmpTran.employeeunkid FROM ( " & _
                       "       SELECT " & _
                       "            hremployee_categorization_tran.employeeunkid " & _
                       "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS Rono " & _
                       "       FROM hremployee_categorization_tran " & _
                       "       WHERE   isvoid = 0 " & _
                       "               AND CONVERT(CHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                       "               AND ISNULL(hremployee_categorization_tran.jobgroupunkid,0) <= 0 " & _
                       "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                       ") "
                    'Shani(24-Aug-2015) -- End
                Case 16 'JOB
                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.jobunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                       "   SELECT EmpTran.employeeunkid FROM ( " & _
                       "       SELECT " & _
                       "            hremployee_categorization_tran.employeeunkid " & _
                       "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS Rono " & _
                       "       FROM hremployee_categorization_tran " & _
                       "       WHERE   isvoid = 0 " & _
                       "               AND CONVERT(CHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                       "               AND ISNULL(hremployee_categorization_tran.jobunkid,0) <= 0 " & _
                       "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                       ") "
                    'Shani(24-Aug-2015) -- End
                Case 17 'CLASS GROUP
                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.classgroupunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                       "   SELECT EmpTran.employeeunkid FROM ( " & _
                       "       SELECT " & _
                       "            hremployee_transfer_tran.employeeunkid " & _
                       "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                       "       FROM hremployee_transfer_tran " & _
                       "       WHERE   isvoid = 0 " & _
                       "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                       "               AND ISNULL(hremployee_transfer_tran.classgroupunkid,0) <= 0 " & _
                       "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                       ") "
                    'Shani(24-Aug-2015) -- End
                Case 18 'CLASSESS
                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.classunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                       "   SELECT EmpTran.employeeunkid FROM ( " & _
                       "       SELECT " & _
                       "            hremployee_transfer_tran.employeeunkid " & _
                       "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rono " & _
                       "       FROM hremployee_transfer_tran " & _
                       "       WHERE   isvoid = 0 " & _
                       "               AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                       "               AND ISNULL(hremployee_transfer_tran.classunkid,0) <= 0 " & _
                       "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                       ") "
                    'Shani(24-Aug-2015) -- End
                Case 19 'COSTCENTER

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND ISNULL(hremployee_master.costcenterunkid,0) <= 0 "
                    StrQ &= "AND hremployee_master.employeeunkid IN ( " & _
                      "   SELECT EmpTran.employeeunkid FROM ( " & _
                      "       SELECT " & _
                      "            hremployee_cctranhead_tran.employeeunkid " & _
                      "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_cctranhead_tran.employeeunkid ORDER BY hremployee_cctranhead_tran.effectivedate DESC) AS Rono " & _
                      "       FROM hremployee_cctranhead_tran " & _
                      "       WHERE   isvoid = 0 " & _
                      "               AND CONVERT(CHAR(8),hremployee_cctranhead_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                      "               AND ISNULL(hremployee_cctranhead_tran.cctranheadvalueid,0) <= 0 " & _
                      "   ) AS EmpTran WHERE EmpTran.Rono=1 " & _
                      ") "
                    'Shani(24-Aug-2015) -- End
                Case 20 'ASSESSMENT-ASSESSOR

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN (SELECT hrassessor_tran.employeeunkid FROM hrassessor_tran " & _
                    '        "	JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                    '        "WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isreviewer = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN (SELECT hrassessor_tran.employeeunkid FROM hrassessor_tran " & _
                            "	JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                            "WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isreviewer = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "') "
                    'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END


                    'Shani(24-Aug-2015) -- End
                Case 21 'ASSESSMENT-REVIEWER

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN (SELECT hrassessor_tran.employeeunkid FROM hrassessor_tran " & _
                    '        "	JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                    '        "WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isreviewer = 1) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN (SELECT hrassessor_tran.employeeunkid FROM hrassessor_tran " & _
                            "	JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                            "WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isreviewer = 1 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "') "
                    'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END

                    'Shani(24-Aug-2015) -- End
                Case 22 'SHIFT

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_shift_tran WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_shift_tran WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End
                Case 23 'POLICY

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= " AND employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_policy_tran WHERE isvoid = 0) "
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_policy_tran WHERE isvoid = 0) "
                    'Shani(24-Aug-2015) -- End

                    'S.SANDEEP |01-MAY-2020| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                Case 24 ' CALIBRATOR
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hrscore_calibration_approver_tran " & _
                            "JOIN hrscore_calibration_approver_master ON hrscore_calibration_approver_master.mappingunkid = hrscore_calibration_approver_tran.mappingunkid " & _
                            "WHERE hrscore_calibration_approver_master.isvoid = 0 AND isactive = 1 AND hrscore_calibration_approver_master.visibletypeid = 1 AND hrscore_calibration_approver_tran.visibletypeid = 1 " & _
                            "AND iscalibrator = 1) "
                Case 25 ' APPROVER
                    StrQ &= " AND hremployee_master.employeeunkid NOT IN(SELECT employeeunkid FROM hrscore_calibration_approver_tran " & _
                           "JOIN hrscore_calibration_approver_master ON hrscore_calibration_approver_master.mappingunkid = hrscore_calibration_approver_tran.mappingunkid " & _
                           "WHERE hrscore_calibration_approver_master.isvoid = 0 AND isactive = 1 AND hrscore_calibration_approver_master.visibletypeid = 1 AND hrscore_calibration_approver_tran.visibletypeid = 1 " & _
                           "AND iscalibrator = 0) "
                    'S.SANDEEP |01-MAY-2020| -- END

            End Select



            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("employeecode")
                'Gajanan [31-Aug-2021] -- Start

                'rpt_Rows.Item("Column3") = dtRow.Item("firstname")
                'rpt_Rows.Item("Column4") = dtRow.Item("othername")
                'rpt_Rows.Item("Column5") = dtRow.Item("surname")

                If mblnIsNameConsolidate Then
                    rpt_Rows.Item("Column3") = dtRow.Item("firstname") + " " + dtRow.Item("othername") + " " + dtRow.Item("surname")
                Else
                rpt_Rows.Item("Column3") = dtRow.Item("firstname")
                rpt_Rows.Item("Column4") = dtRow.Item("othername")
                rpt_Rows.Item("Column5") = dtRow.Item("surname")
                End If
                'Gajanan [31-Aug-2021] -- End

                rpt_Rows.Item("Column6") = dtRow.Item("location")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeWithoutReport

            If mblnIsNameConsolidate Then
                Dim txtFirstName = CType(objRpt.ReportDefinition.ReportObjects.Item("txtFirstName"), CrystalDecisions.CrystalReports.Engine.TextObject)
                Dim txtOthername = CType(objRpt.ReportDefinition.ReportObjects.Item("txtOthername"), CrystalDecisions.CrystalReports.Engine.TextObject)
                Dim txtSurname = CType(objRpt.ReportDefinition.ReportObjects.Item("txtSurname"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtFirstName.Width += txtOthername.Width + txtSurname.Width
                txtOthername.Width = 0
                txtSurname.Width = 0
                txtSurname.Text = ""
                txtOthername.Text = ""

                Dim fieldFname = CType(objRpt.ReportDefinition.ReportObjects.Item("Column31"), CrystalDecisions.CrystalReports.Engine.FieldObject)
                Dim fieldLname = CType(objRpt.ReportDefinition.ReportObjects.Item("Column41"), CrystalDecisions.CrystalReports.Engine.FieldObject)
                Dim fieldOname = CType(objRpt.ReportDefinition.ReportObjects.Item("Column51"), CrystalDecisions.CrystalReports.Engine.FieldObject)
                fieldFname.Width += fieldLname.Width + fieldOname.Width

                fieldLname.Width = 0
                fieldOname.Width = 0
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 8, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 9, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 10, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 11, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 3, "Code"))

            'Gajanan [31-Aug-2021] -- Start

            'Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 4, "Firstname"))
            'Call ReportFunction.TextChange(objRpt, "txtOthername", Language.getMessage(mstrModuleName, 5, "Othername"))
            'Call ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 6, "Surname"))

            If mblnIsNameConsolidate Then
                Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 15, "Name"))
            Else
            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 4, "Firstname"))
            Call ReportFunction.TextChange(objRpt, "txtOthername", Language.getMessage(mstrModuleName, 5, "Othername"))
            Call ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 6, "Surname"))
            End If
            'Gajanan [31-Aug-2021] -- End

            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 7, "Location/Class Group"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 14, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 15, "Grand Total :"))


            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " - [ " & mstrReportTypeName & " ]")
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, " Order By :")
            Language.setMessage(mstrModuleName, 3, "Code")
            Language.setMessage(mstrModuleName, 4, "Firstname")
            Language.setMessage(mstrModuleName, 5, "Othername")
            Language.setMessage(mstrModuleName, 6, "Surname")
            Language.setMessage(mstrModuleName, 7, "Location/Class Group")
            Language.setMessage(mstrModuleName, 8, "Prepared By :")
            Language.setMessage(mstrModuleName, 9, "Checked By :")
            Language.setMessage(mstrModuleName, 10, "Approved By :")
            Language.setMessage(mstrModuleName, 11, "Received By :")
            Language.setMessage(mstrModuleName, 12, "Printed By :")
            Language.setMessage(mstrModuleName, 13, "Printed Date :")
            Language.setMessage(mstrModuleName, 14, "Page :")
            Language.setMessage(mstrModuleName, 15, "Name")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
