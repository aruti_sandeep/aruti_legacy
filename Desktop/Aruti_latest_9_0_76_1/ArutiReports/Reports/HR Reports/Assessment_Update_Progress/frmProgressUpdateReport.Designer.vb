﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProgressUpdateReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProgressUpdateReport))
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lblApprovalStatus = New System.Windows.Forms.Label
        Me.cboApprovalStatus = New System.Windows.Forms.ComboBox
        Me.lblUpdateDateFrm = New System.Windows.Forms.Label
        Me.dtpUpdateFrom = New System.Windows.Forms.DateTimePicker
        Me.lblUpdateDateTo = New System.Windows.Forms.Label
        Me.dtpUpdateTo = New System.Windows.Forms.DateTimePicker
        Me.dtpApprovalFrom = New System.Windows.Forms.DateTimePicker
        Me.lblApprovalDateFrm = New System.Windows.Forms.Label
        Me.lblApprovalDateTo = New System.Windows.Forms.Label
        Me.dtpApprovalTo = New System.Windows.Forms.DateTimePicker
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocations = New System.Windows.Forms.ColumnHeader
        Me.objStLine2 = New eZee.Common.eZeeStraightLine
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objchkdisplay = New System.Windows.Forms.CheckBox
        Me.lvDisplayCol = New eZee.Common.eZeeListView(Me.components)
        Me.colhDisplayName = New System.Windows.Forms.ColumnHeader
        Me.objcolhSelectCol = New System.Windows.Forms.ColumnHeader
        Me.objcolhJoin = New System.Windows.Forms.ColumnHeader
        Me.objcolhDisplay = New System.Windows.Forms.ColumnHeader
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(1101, 58)
        Me.EZeeHeader1.TabIndex = 2
        Me.EZeeHeader1.Title = "Assessment Score Rating"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 650)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(1101, 55)
        Me.EZeeFooter1.TabIndex = 34
        '
        'btnAdvanceFilter
        '
        Me.btnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.btnAdvanceFilter.BackgroundImage = CType(resources.GetObject("btnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.btnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.btnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.btnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Location = New System.Drawing.Point(697, 13)
        Me.btnAdvanceFilter.Name = "btnAdvanceFilter"
        Me.btnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Size = New System.Drawing.Size(105, 30)
        Me.btnAdvanceFilter.TabIndex = 87
        Me.btnAdvanceFilter.Text = "&Advance Filter"
        Me.btnAdvanceFilter.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(808, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(904, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(1000, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 32
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.pnlData)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine2)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.objchkdisplay)
        Me.gbFilterCriteria.Controls.Add(Me.lvDisplayCol)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(807, 414)
        Me.gbFilterCriteria.TabIndex = 41
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(710, 5)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 16)
        Me.lnkSetAnalysis.TabIndex = 94
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.lblApprovalStatus)
        Me.pnlData.Controls.Add(Me.cboApprovalStatus)
        Me.pnlData.Controls.Add(Me.lblUpdateDateFrm)
        Me.pnlData.Controls.Add(Me.dtpUpdateFrom)
        Me.pnlData.Controls.Add(Me.lblUpdateDateTo)
        Me.pnlData.Controls.Add(Me.dtpUpdateTo)
        Me.pnlData.Controls.Add(Me.dtpApprovalFrom)
        Me.pnlData.Controls.Add(Me.lblApprovalDateFrm)
        Me.pnlData.Controls.Add(Me.lblApprovalDateTo)
        Me.pnlData.Controls.Add(Me.dtpApprovalTo)
        Me.pnlData.Controls.Add(Me.cboAllocations)
        Me.pnlData.Controls.Add(Me.objchkAll)
        Me.pnlData.Controls.Add(Me.lblAllocation)
        Me.pnlData.Controls.Add(Me.txtSearch)
        Me.pnlData.Controls.Add(Me.lvAllocation)
        Me.pnlData.Location = New System.Drawing.Point(403, 26)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(401, 385)
        Me.pnlData.TabIndex = 148
        '
        'lblApprovalStatus
        '
        Me.lblApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalStatus.Location = New System.Drawing.Point(16, 72)
        Me.lblApprovalStatus.Name = "lblApprovalStatus"
        Me.lblApprovalStatus.Size = New System.Drawing.Size(90, 15)
        Me.lblApprovalStatus.TabIndex = 143
        Me.lblApprovalStatus.Text = "Approval Status"
        Me.lblApprovalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApprovalStatus
        '
        Me.cboApprovalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprovalStatus.DropDownWidth = 210
        Me.cboApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprovalStatus.FormattingEnabled = True
        Me.cboApprovalStatus.Location = New System.Drawing.Point(116, 69)
        Me.cboApprovalStatus.Name = "cboApprovalStatus"
        Me.cboApprovalStatus.Size = New System.Drawing.Size(268, 21)
        Me.cboApprovalStatus.TabIndex = 142
        '
        'lblUpdateDateFrm
        '
        Me.lblUpdateDateFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpdateDateFrm.Location = New System.Drawing.Point(16, 18)
        Me.lblUpdateDateFrm.Name = "lblUpdateDateFrm"
        Me.lblUpdateDateFrm.Size = New System.Drawing.Size(90, 15)
        Me.lblUpdateDateFrm.TabIndex = 104
        Me.lblUpdateDateFrm.Text = "Updated From"
        Me.lblUpdateDateFrm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpUpdateFrom
        '
        Me.dtpUpdateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpUpdateFrom.Checked = False
        Me.dtpUpdateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpUpdateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpUpdateFrom.Location = New System.Drawing.Point(116, 15)
        Me.dtpUpdateFrom.Name = "dtpUpdateFrom"
        Me.dtpUpdateFrom.ShowCheckBox = True
        Me.dtpUpdateFrom.Size = New System.Drawing.Size(105, 21)
        Me.dtpUpdateFrom.TabIndex = 103
        '
        'lblUpdateDateTo
        '
        Me.lblUpdateDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpdateDateTo.Location = New System.Drawing.Point(231, 18)
        Me.lblUpdateDateTo.Name = "lblUpdateDateTo"
        Me.lblUpdateDateTo.Size = New System.Drawing.Size(35, 15)
        Me.lblUpdateDateTo.TabIndex = 105
        Me.lblUpdateDateTo.Text = "To"
        Me.lblUpdateDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpUpdateTo
        '
        Me.dtpUpdateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpUpdateTo.Checked = False
        Me.dtpUpdateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpUpdateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpUpdateTo.Location = New System.Drawing.Point(279, 15)
        Me.dtpUpdateTo.Name = "dtpUpdateTo"
        Me.dtpUpdateTo.ShowCheckBox = True
        Me.dtpUpdateTo.Size = New System.Drawing.Size(105, 21)
        Me.dtpUpdateTo.TabIndex = 106
        '
        'dtpApprovalFrom
        '
        Me.dtpApprovalFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApprovalFrom.Checked = False
        Me.dtpApprovalFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApprovalFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApprovalFrom.Location = New System.Drawing.Point(116, 42)
        Me.dtpApprovalFrom.Name = "dtpApprovalFrom"
        Me.dtpApprovalFrom.ShowCheckBox = True
        Me.dtpApprovalFrom.Size = New System.Drawing.Size(105, 21)
        Me.dtpApprovalFrom.TabIndex = 108
        '
        'lblApprovalDateFrm
        '
        Me.lblApprovalDateFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalDateFrm.Location = New System.Drawing.Point(16, 45)
        Me.lblApprovalDateFrm.Name = "lblApprovalDateFrm"
        Me.lblApprovalDateFrm.Size = New System.Drawing.Size(90, 15)
        Me.lblApprovalDateFrm.TabIndex = 107
        Me.lblApprovalDateFrm.Text = "Approved From"
        Me.lblApprovalDateFrm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprovalDateTo
        '
        Me.lblApprovalDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalDateTo.Location = New System.Drawing.Point(231, 45)
        Me.lblApprovalDateTo.Name = "lblApprovalDateTo"
        Me.lblApprovalDateTo.Size = New System.Drawing.Size(35, 15)
        Me.lblApprovalDateTo.TabIndex = 109
        Me.lblApprovalDateTo.Text = "To"
        Me.lblApprovalDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpApprovalTo
        '
        Me.dtpApprovalTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApprovalTo.Checked = False
        Me.dtpApprovalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApprovalTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApprovalTo.Location = New System.Drawing.Point(279, 42)
        Me.dtpApprovalTo.Name = "dtpApprovalTo"
        Me.dtpApprovalTo.ShowCheckBox = True
        Me.dtpApprovalTo.Size = New System.Drawing.Size(105, 21)
        Me.dtpApprovalTo.TabIndex = 110
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(116, 96)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(268, 21)
        Me.cboAllocations.TabIndex = 98
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(360, 156)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 102
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(16, 100)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(90, 15)
        Me.lblAllocation.TabIndex = 99
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(116, 123)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(268, 21)
        Me.txtSearch.TabIndex = 100
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(116, 150)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(268, 225)
        Me.lvAllocation.TabIndex = 101
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocations
        '
        Me.colhAllocations.Tag = "colhAllocations"
        Me.colhAllocations.Text = ""
        Me.colhAllocations.Width = 250
        '
        'objStLine2
        '
        Me.objStLine2.BackColor = System.Drawing.Color.Transparent
        Me.objStLine2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine2.Location = New System.Drawing.Point(400, 23)
        Me.objStLine2.Name = "objStLine2"
        Me.objStLine2.Size = New System.Drawing.Size(3, 391)
        Me.objStLine2.TabIndex = 147
        Me.objStLine2.Text = "EZeeStraightLine2"
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(17, 44)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(89, 15)
        Me.lblReportType.TabIndex = 146
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 210
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(112, 41)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(244, 21)
        Me.cboReportType.TabIndex = 145
        '
        'objchkdisplay
        '
        Me.objchkdisplay.AutoSize = True
        Me.objchkdisplay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkdisplay.Location = New System.Drawing.Point(319, 160)
        Me.objchkdisplay.Name = "objchkdisplay"
        Me.objchkdisplay.Size = New System.Drawing.Size(15, 14)
        Me.objchkdisplay.TabIndex = 143
        Me.objchkdisplay.UseVisualStyleBackColor = True
        '
        'lvDisplayCol
        '
        Me.lvDisplayCol.BackColorOnChecked = False
        Me.lvDisplayCol.CheckBoxes = True
        Me.lvDisplayCol.ColumnHeaders = Nothing
        Me.lvDisplayCol.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDisplayName, Me.objcolhSelectCol, Me.objcolhJoin, Me.objcolhDisplay})
        Me.lvDisplayCol.CompulsoryColumns = ""
        Me.lvDisplayCol.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDisplayCol.FullRowSelect = True
        Me.lvDisplayCol.GridLines = True
        Me.lvDisplayCol.GroupingColumn = Nothing
        Me.lvDisplayCol.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDisplayCol.HideSelection = False
        Me.lvDisplayCol.Location = New System.Drawing.Point(112, 149)
        Me.lvDisplayCol.MinColumnWidth = 50
        Me.lvDisplayCol.MultiSelect = False
        Me.lvDisplayCol.Name = "lvDisplayCol"
        Me.lvDisplayCol.OptionalColumns = ""
        Me.lvDisplayCol.ShowMoreItem = False
        Me.lvDisplayCol.ShowSaveItem = False
        Me.lvDisplayCol.ShowSelectAll = True
        Me.lvDisplayCol.ShowSizeAllColumnsToFit = True
        Me.lvDisplayCol.Size = New System.Drawing.Size(244, 252)
        Me.lvDisplayCol.Sortable = True
        Me.lvDisplayCol.TabIndex = 142
        Me.lvDisplayCol.UseCompatibleStateImageBehavior = False
        Me.lvDisplayCol.View = System.Windows.Forms.View.Details
        '
        'colhDisplayName
        '
        Me.colhDisplayName.Tag = "colhDisplayName"
        Me.colhDisplayName.Text = "Display Column On Report"
        Me.colhDisplayName.Width = 210
        '
        'objcolhSelectCol
        '
        Me.objcolhSelectCol.Tag = "objcolhSelectCol"
        Me.objcolhSelectCol.Text = ""
        Me.objcolhSelectCol.Width = 0
        '
        'objcolhJoin
        '
        Me.objcolhJoin.Tag = "objcolhJoin"
        Me.objcolhJoin.Text = ""
        Me.objcolhJoin.Width = 0
        '
        'objcolhDisplay
        '
        Me.objcolhDisplay.Tag = "objcolhDisplay"
        Me.objcolhDisplay.Text = ""
        Me.objcolhDisplay.Width = 0
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(17, 125)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(89, 15)
        Me.lblStatus.TabIndex = 141
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 210
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(112, 122)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(244, 21)
        Me.cboStatus.TabIndex = 140
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(362, 95)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 133
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(17, 98)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(89, 15)
        Me.lblEmployee.TabIndex = 131
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(112, 95)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(244, 21)
        Me.cboEmployee.TabIndex = 132
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(17, 71)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(89, 15)
        Me.lblPeriod.TabIndex = 79
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 210
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(112, 68)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(244, 21)
        Me.cboPeriod.TabIndex = 77
        '
        'frmProgressUpdateReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1101, 705)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.EZeeHeader1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProgressUpdateReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Progress Update Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents objchkdisplay As System.Windows.Forms.CheckBox
    Friend WithEvents lvDisplayCol As eZee.Common.eZeeListView
    Friend WithEvents colhDisplayName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSelectCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhJoin As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDisplay As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents objStLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents lblUpdateDateFrm As System.Windows.Forms.Label
    Friend WithEvents dtpUpdateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblUpdateDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpUpdateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpApprovalFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblApprovalDateFrm As System.Windows.Forms.Label
    Friend WithEvents lblApprovalDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpApprovalTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblApprovalStatus As System.Windows.Forms.Label
    Friend WithEvents cboApprovalStatus As System.Windows.Forms.ComboBox
End Class
