﻿'************************************************************************************************************************************
'Class Name : clsAssessment_Progress_Update.vb
'Purpose    :
'Date       :01-Jul-2021
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
''' 
Public Class clsProgressUpdateReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsProgressUpdateReport"
    Private mstrReportId As String = enArutiReport.Assessment_Progress_Update '270
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Enum "

    Public Enum enUpdateStatus
        Updated = 1
        NotUpdated = 2
        NotPlanned = 3
    End Enum

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintCompanyUnkid As Integer = 0
    Private mdtEmployeeAsOnDate As DateTime
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mstrSelectedCols As String = String.Empty
    Private mstrSelectedJoin As String = String.Empty
    Private mstrDisplayCols As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""

    'S.SANDEEP |24-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : Sprint 2023-13
    Private mstrAllocation As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    Private mintReportType_Id As Integer = -1
    Private mstrReportType_Name As String = String.Empty
    Private mintApprovalStatusId As Integer = 0
    Private mstrApprovalStatusName As String = ""
    Private mdtUpdateDateFrom As Date = Nothing
    Private mdtUpdateDateTo As Date = Nothing
    Private mdtApprovalDateFrom As Date = Nothing
    Private mdtApprovalDateTo As Date = Nothing
    'S.SANDEEP |24-JUN-2023| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _SelectedCols() As String
        Set(ByVal value As String)
            mstrSelectedCols = value
        End Set
    End Property

    Public WriteOnly Property _SelectedJoin() As String
        Set(ByVal value As String)
            mstrSelectedJoin = value
        End Set
    End Property

    Public WriteOnly Property _DisplayCols() As String
        Set(ByVal value As String)
            mstrDisplayCols = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property


    'S.SANDEEP |24-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : Sprint 2023-13
    Public WriteOnly Property _Allocation() As String
        Set(ByVal value As String)
            mstrAllocation = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _ReportType_Id() As Integer
        Set(ByVal value As Integer)
            mintReportType_Id = value
        End Set
    End Property

    Public WriteOnly Property _ReportType_Name() As String
        Set(ByVal value As String)
            mstrReportType_Name = value
        End Set
    End Property

    Public WriteOnly Property _ApprovalStatusId() As Integer
        Set(ByVal value As Integer)
            mintApprovalStatusId = value
        End Set
    End Property

    Public WriteOnly Property _ApprovalStatusName() As String
        Set(ByVal value As String)
            mstrApprovalStatusName = value
        End Set
    End Property

    Public WriteOnly Property _UpdateDateFrom() As Date
        Set(ByVal value As Date)
            mdtUpdateDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _UpdateDateTo() As Date
        Set(ByVal value As Date)
            mdtUpdateDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ApprovalDateFrom() As Date
        Set(ByVal value As Date)
            mdtApprovalDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ApprovalDateTo() As Date
        Set(ByVal value As Date)
            mdtApprovalDateTo = value
        End Set
    End Property
    'S.SANDEEP |24-JUN-2023| -- END


#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mintPeriodId = -1
            mstrPeriodName = String.Empty
            mintCompanyUnkid = 0
            mdtEmployeeAsOnDate = Nothing
            Rpt = Nothing
            mstrSelectedCols = String.Empty
            mstrSelectedJoin = String.Empty
            mstrDisplayCols = String.Empty
            mstrAdvanceFilter = String.Empty
            mblnFirstNamethenSurname = True
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            mstrAllocation = String.Empty
            mstrAllocationIds = String.Empty
            mintReportType_Id = -1
            mstrReportType_Name = String.Empty
            mintApprovalStatusId = 0
            mstrApprovalStatusName = ""
            mdtUpdateDateFrom = Nothing
            mdtUpdateDateTo = Nothing
            mdtApprovalDateFrom = Nothing
            mdtApprovalDateTo = Nothing
            'S.SANDEEP |24-JUN-2023| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function GetProgressStatus(ByVal strList As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = ""
        Dim ds As New DataSet
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT " & CInt(enUpdateStatus.Updated) & " AS Id, @Updated AS Name UNION " & _
                    "SELECT " & CInt(enUpdateStatus.NotUpdated) & " AS Id, @NotUpdated AS Name UNION " & _
                    "SELECT " & CInt(enUpdateStatus.NotPlanned) & " AS Id, @NotPlanned AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Select"))
            objDataOperation.AddParameter("@Updated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Updated"))
            objDataOperation.AddParameter("@NotUpdated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Not Updated"))
            objDataOperation.AddParameter("@NotPlanned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Not Planned"))

            ds = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetProgressStatus; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return ds
    End Function

    Public Sub ExportProgressUpdate(ByVal strDatabaseName As String, _
                                    ByVal intUserUnkid As Integer, _
                                    ByVal intYearUnkid As Integer, _
                                    ByVal intCompanyUnkid As Integer, _
                                    ByVal dtPeriodStart As Date, _
                                    ByVal dtPeriodEnd As Date, _
                                    ByVal strUserModeSetting As String, _
                                    ByVal blnOnlyApproved As Boolean, _
                                    ByVal strExportPath As String, _
                                    ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(mintPeriodId)
            Dim strMappeedTableName As String = ""
            Dim strMappedFiledName As String = ""

            Select Case intLinkedFld
                Case enWeight_Types.WEIGHT_FIELD1
                    strMappedFiledName = "empfield1unkid"
                    strMappeedTableName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD2
                    strMappedFiledName = "empfield2unkid"
                    strMappeedTableName = "hrassess_empfield2_master"
                Case enWeight_Types.WEIGHT_FIELD3
                    strMappedFiledName = "empfield3unkid"
                    strMappeedTableName = "hrassess_empfield3_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    strMappedFiledName = "empfield4unkid"
                    strMappeedTableName = "hrassess_empfield4_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    strMappedFiledName = "empfield5unkid"
                    strMappeedTableName = "hrassess_empfield5_master"
            End Select


            StrQ = "SELECT " & _
                   "     employeecode AS ecode "

            If mblnFirstNamethenSurname Then
                StrQ &= "    ,firstname + ' ' + surname AS ename "
            Else
                StrQ &= "    ,surname + ' ' + firstname AS ename "
            End If

            StrQ &= "    ,ISNULL(ejbm.job_name, '') AS job "

            StrQ &= mstrSelectedCols & " "

            StrQ &= "    ,CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN @NotPlanned " & _
                    "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN @NotUpdated " & _
                    "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN @Updated " & _
                    "     END AS gstatus " & _
                    "    ,CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN 0 " & _
                    "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN 0 " & _
                    "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN CAST(ISNULL(c.avgpct, 0) AS DECIMAL(36, 2)) " & _
                    "     END AS avgpct " & _
                    "    ,CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN '' " & _
                    "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN '' " & _
                    "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN ISNULL(c.adate, '') " & _
                    "     END AS adate "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields & " "
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        pt.gradegroupunkid " & _
                    "       ,pt.gradeunkid " & _
                    "       ,pt.gradelevelunkid " & _
                    "       ,pt.employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY pt.employeeunkid ORDER BY pt.incrementdate DESC, pt.salaryincrementtranunkid DESC) AS rno " & _
                    "   FROM prsalaryincrement_tran pt " & _
                    "   WHERE pt.isvoid = 0 AND pt.isapproved = 1 AND CONVERT(CHAR(8),pt.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS grds ON hremployee_master.employeeunkid = grds.employeeunkid AND grds.rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrSelectedJoin & " "

            StrQ &= mstrAnalysis_Join & " "

            'S.SANDEEP |27-MAY-2022| -- START
            'ISSUE/ENHANCEMENT : WRONG QUERY FORMAT USED 
            'StrQ &= "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        employeeunkid " & _
            '        "       ,jobunkid " & _
            '        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '        "   FROM hremployee_categorization_tran " & _
            '        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
            '        ") AS t ON t.employeeunkid = hremployee_master.employeeunkid AND t.rno = 1 " & _
            '        "LEFT JOIN hrjob_master as ejbm ON t.jobunkid = ejbm.jobunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        employeeunkid " & _
            '        "       ,COUNT(1) AS tgoal " & _
            '        "   FROM hrassess_empfield1_master " & _
            '        "   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
            '        "   GROUP BY employeeunkid " & _
            '        ") AS a ON a.employeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        hrassess_empupdate_tran.employeeunkid " & _
            '        "       ,COUNT(DISTINCT empfieldunkid) AS ugoal " & _
            '        "   FROM hrassess_empupdate_tran " & _
            '        "       JOIN hrassess_empfield1_master ON empfield1unkid = empfieldunkid " & _
            '        "   WHERE hrassess_empupdate_tran.periodunkid = @periodunkid " & _
            '        "       AND hrassess_empfield1_master.isvoid = 0 AND hrassess_empupdate_tran.isvoid = 0 AND hrassess_empfield1_master.periodunkid = hrassess_empupdate_tran.periodunkid " & _
            '        "       AND approvalstatusunkid = 2 AND hrassess_empfield1_master.periodunkid = @periodunkid " & _
            '        "   GROUP BY hrassess_empupdate_tran.employeeunkid " & _
            '        ") AS b ON b.employeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        hrassess_empupdate_tran.employeeunkid " & _
            '        "       ,SUM(hrassess_empupdate_tran.pct_completed) / COUNT(empfieldunkid) AS avgpct " & _
            '        "       ,CONVERT(CHAR(8), MAX(updatedate), 112) AS adate " & _
            '        "   FROM hrassess_empupdate_tran " & _
            '        "       JOIN hrassess_empfield1_master ON empfield1unkid = empfieldunkid " & _
            '        "   WHERE hrassess_empupdate_tran.periodunkid = @periodunkid " & _
            '        "   AND hrassess_empfield1_master.isvoid = 0 AND hrassess_empupdate_tran.isvoid = 0 AND hrassess_empfield1_master.periodunkid = hrassess_empupdate_tran.periodunkid " & _
            '        "   AND approvalstatusunkid = 2 AND hrassess_empfield1_master.periodunkid = @periodunkid " & _
            '        "   GROUP BY hrassess_empupdate_tran.employeeunkid " & _
            '        ") AS c ON c.employeeunkid = hremployee_master.employeeunkid " & _
            '        "WHERE 1 = 1 "

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid " & _
                    "       ,jobunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    ") AS t ON t.employeeunkid = hremployee_master.employeeunkid AND t.rno = 1 " & _
                    "LEFT JOIN hrjob_master as ejbm ON t.jobunkid = ejbm.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid " & _
                    "       ,COUNT(1) AS tgoal " & _
                    "   FROM " & strMappeedTableName & " " & _
                    "   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
                    "   GROUP BY employeeunkid " & _
                    ") AS a ON a.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hrassess_empupdate_tran.employeeunkid " & _
                    "       ,COUNT(DISTINCT empfieldunkid) AS ugoal " & _
                    "   FROM hrassess_empupdate_tran " & _
                    "       JOIN " & strMappeedTableName & " ON " & strMappedFiledName & " = empfieldunkid " & _
                    "   WHERE hrassess_empupdate_tran.periodunkid = @periodunkid " & _
                    "       AND " & strMappeedTableName & ".isvoid = 0 AND hrassess_empupdate_tran.isvoid = 0 AND " & strMappeedTableName & ".periodunkid = hrassess_empupdate_tran.periodunkid " & _
                    "       AND approvalstatusunkid = 2 AND " & strMappeedTableName & ".periodunkid = @periodunkid " & _
                    "   GROUP BY hrassess_empupdate_tran.employeeunkid " & _
                    ") AS b ON b.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hrassess_empupdate_tran.employeeunkid " & _
                    "       ,SUM(hrassess_empupdate_tran.pct_completed) / COUNT(empfieldunkid) AS avgpct " & _
                    "       ,CONVERT(CHAR(8), MAX(updatedate), 112) AS adate " & _
                    "   FROM hrassess_empupdate_tran " & _
                    "       JOIN " & strMappeedTableName & " ON " & strMappedFiledName & " = empfieldunkid " & _
                    "   WHERE hrassess_empupdate_tran.periodunkid = @periodunkid " & _
                    "   AND " & strMappeedTableName & ".isvoid = 0 AND hrassess_empupdate_tran.isvoid = 0 AND " & strMappeedTableName & ".periodunkid = hrassess_empupdate_tran.periodunkid " & _
                    "   AND approvalstatusunkid = 2 AND " & strMappeedTableName & ".periodunkid = @periodunkid " & _
                    "   GROUP BY hrassess_empupdate_tran.employeeunkid " & _
                    ") AS c ON c.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE 1 = 1 "
            'S.SANDEEP |27-MAY-2022| -- END


            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If


            Dim iExFliter As String = ""
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeid "
                iExFliter &= "," & Language.getMessage(mstrModuleName, 300, "Employee :") & " " & mstrEmployeeName
            End If

            If mintStatusId > 0 Then
                StrQ &= " AND CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN " & CInt(enUpdateStatus.NotPlanned) & " " & _
                        "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN " & CInt(enUpdateStatus.NotUpdated) & " " & _
                        "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN " & CInt(enUpdateStatus.Updated) & " " & _
                        "     END = @statusid "

                iExFliter &= "," & Language.getMessage(mstrModuleName, 301, "Status :") & " " & mstrStatusName
            End If


            objDataOperation.AddParameter("@NotPlanned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Not Planned"))
            objDataOperation.AddParameter("@Updated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Updated"))
            objDataOperation.AddParameter("@NotUpdated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Not Updated"))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim strarrGroupColumns As String() = Nothing
            'If mintViewIndex <= 0 Then
            '    dsList.Tables(0).Columns.Remove("Id") : dsList.Tables(0).Columns.Remove("GName")
            'Else
            '    dsList.Tables(0).Columns.Remove("Id")
            '    dsList.Tables(0).Columns("GName").Caption = mstrReport_GroupName
            '    Dim strGrpCols As String() = {"GName"}
            '    strarrGroupColumns = strGrpCols
            'End If
            dsList.Tables(0).Columns.Remove("Id") : dsList.Tables(0).Columns.Remove("GName")

            dsList.Tables(0).Columns("ecode").Caption = Language.getMessage(mstrModuleName, 400, "Employee Code")
            dsList.Tables(0).Columns("ename").Caption = Language.getMessage(mstrModuleName, 401, "Employee Name")
            dsList.Tables(0).Columns("job").Caption = Language.getMessage(mstrModuleName, 402, "Job")
            dsList.Tables(0).Columns("gstatus").Caption = Language.getMessage(mstrModuleName, 403, "Progress Update Status")
            dsList.Tables(0).Columns("avgpct").Caption = Language.getMessage(mstrModuleName, 404, "Average% updated to date")

            For Each iRow As DataRow In dsList.Tables(0).Rows
                If iRow("adate").ToString().Length > 0 Then iRow("adate") = eZeeDate.convertDate(iRow("adate").ToString).ToShortDateString()
            Next
            dsList.Tables(0).Columns("adate").Caption = Language.getMessage(mstrModuleName, 405, "As on Date")

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 90
                    Case 1
                        intArrayColumnWidth(i) = 140
                    Case 2
                        intArrayColumnWidth(i) = 160
                    Case Else
                        intArrayColumnWidth(i) = 100
                End Select
            Next

            If iExFliter.Trim.Length > 0 Then iExFliter = Mid(iExFliter, 2)
            Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, strarrGroupColumns, mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)
            'S.SANDEEP |24-JUN-2023| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportProgressUpdate; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |24-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : Sprint 2023-13
    Public Sub ExportProgressUpdateDetailReport(ByVal strDatabaseName As String, _
                                                ByVal intUserUnkid As Integer, _
                                                ByVal intYearUnkid As Integer, _
                                                ByVal intCompanyUnkid As Integer, _
                                                ByVal dtPeriodStart As Date, _
                                                ByVal dtPeriodEnd As Date, _
                                                ByVal strUserModeSetting As String, _
                                                ByVal blnOnlyApproved As Boolean, _
                                                ByVal strExportPath As String, _
                                                ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim objFldMstr As New clsAssess_Field_Master(True)
        Try

            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "EM")
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "EM")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "EM")

            StrQ = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mformat]') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' )) " & _
                   "BEGIN " & _
                   "    DROP FUNCTION mformat " & _
                   "END "

            objDataOperation.ExecNonQuery(StrQ)

            StrQ = "CREATE FUNCTION mformat(@NumStr nvarchar(max)) " & _
                   "RETURNS nvarchar(max) " & _
                   "AS " & _
                   "BEGIN " & _
                   "    declare @OutStr varchar(50) " & _
                   "    declare @i int " & _
                   "    declare @run int " & _
                   "    Select @i=CHARINDEX('.',@NumStr) " & _
                   "    IF @i = 0 " & _
                   "    BEGIN " & _
                   "        SET @i = LEN(@NumStr) " & _
                   "        SET @Outstr='' " & _
                   "    END " & _
                   "    ELSE " & _
                   "    BEGIN " & _
                   "        SET @Outstr=SUBSTRING(@NUmStr,@i,50) " & _
                   "        SET @i=@i -1 " & _
                   "    END " & _
                   "    SET @run=0 " & _
                   "    WHILE @i > 0 " & _
                   "    BEGIN " & _
                   "        IF @Run=3 " & _
                   "        BEGIN " & _
                   "            SET @Outstr=','+@Outstr " & _
                   "            SET @run=0 " & _
                   "        END " & _
                   "        SET @Outstr=SUBSTRING(@NumStr,@i,1) +@Outstr " & _
                   "        SET @i=@i-1 " & _
                   "        SET @run=@run + 1 " & _
                   "    END " & _
                   "    RETURN @OutStr " & _
                   "END; "

            objDataOperation.ExecNonQuery(StrQ)


            StrQ = "DECLARE @LinkFieldId AS INT " & _
                    "SET @LinkFieldId = ISNULL((SELECT fieldunkid FROM hrassess_field_mapping WITH (NOLOCK) WHERE periodunkid = @periodunkid AND isactive = 1),0) " & _
                    "SELECT " & _
                          "EM.employeecode AS [" & Language.getMessage(mstrModuleName, 707, "Employee Code") & "] " & _
                         ",EM.firstname + ' ' + EM.surname AS [" & Language.getMessage("clsassess_empfield1_master", 17, "Employee") & "] "

            StrQ &= mstrSelectedCols & " "

            StrQ &= ",ISNULL(P.name,'') AS [" & Language.getMessage("clsassess_empfield1_master", 27, "Perspective") & "] "

            If objFldMstr._Field1_Caption <> "" Then
                StrQ &= ",ISNULL(E1.field_data,'') AS [" & objFldMstr._Field1_Caption & "] "
            End If
            If objFldMstr._Field2_Caption <> "" Then
                StrQ &= ",ISNULL(E2.field_data,'') AS [" & objFldMstr._Field2_Caption & "] "
            End If
            If objFldMstr._Field3_Caption <> "" Then
                StrQ &= ",ISNULL(E3.field_data,'') AS [" & objFldMstr._Field3_Caption & "] "
            End If
            If objFldMstr._Field4_Caption <> "" Then
                StrQ &= ",ISNULL(E4.field_data,'') AS [" & objFldMstr._Field4_Caption & "] "
            End If
            If objFldMstr._Field5_Caption <> "" Then
                StrQ &= ",ISNULL(E5.field_data,'') AS [" & objFldMstr._Field5_Caption & "] "
            End If
            If objFldMstr._Field6_Caption <> "" Then
                StrQ &= ",ISNULL(Col_InfoFld6,'') AS [" & objFldMstr._Field6_Caption & "] "
            End If
            If objFldMstr._Field7_Caption <> "" Then
                StrQ &= ",ISNULL(Col_InfoFld7,'') AS [" & objFldMstr._Field7_Caption & "] "
            End If
            If objFldMstr._Field8_Caption <> "" Then
                StrQ &= ",ISNULL(Col_InfoFld8,'') AS [" & objFldMstr._Field8_Caption & "] "
            End If
                         
            StrQ &= ",ISNULL(CASE WHEN @LinkFieldId = E1.fieldunkid THEN ISNULL(E1.weight,0) " & _
                                         "WHEN @LinkFieldId = E2.fieldunkid THEN ISNULL(E2.weight,0) " & _
                                         "WHEN @LinkFieldId = E3.fieldunkid THEN ISNULL(E3.weight,0) " & _
                                         "WHEN @LinkFieldId = E4.fieldunkid THEN ISNULL(E4.weight,0) " & _
                                         "WHEN @LinkFieldId = E5.fieldunkid THEN ISNULL(E5.weight,0) " & _
                                   "END,0) AS [" & Language.getMessage("clsassess_empfield1_master", 22, "Weight") & "] " & _
                         ",CASE WHEN ISNULL((CASE WHEN @LinkFieldId = E1.fieldunkid THEN ISNULL(E1.goaltypeid,1) " & _
                                                       "WHEN @LinkFieldId = E2.fieldunkid THEN ISNULL(E2.goaltypeid,1) " & _
                                                       "WHEN @LinkFieldId = E3.fieldunkid THEN ISNULL(E3.goaltypeid,1) " & _
                                                       "WHEN @LinkFieldId = E4.fieldunkid THEN ISNULL(E4.goaltypeid,1) " & _
                                                       "WHEN @LinkFieldId = E5.fieldunkid THEN ISNULL(E5.goaltypeid,1) " & _
                          "END),1) = 1 THEN @GT_QUALITATIVE ELSE @GT_QUANTITATIVE END AS [" & Language.getMessage("clsAssess_Field_Master", 14, "Goal Type") & "] " & _
                         ",dbo.mformat(CAST(ISNULL(CASE WHEN @LinkFieldId = E1.fieldunkid THEN CAST(ISNULL(E1.goalvalue,0) AS DECIMAL(36,2)) " & _
                                               "WHEN @LinkFieldId = E2.fieldunkid THEN CAST(ISNULL(E2.goalvalue,0) AS DECIMAL(36,2)) " & _
                                               "WHEN @LinkFieldId = E3.fieldunkid THEN CAST(ISNULL(E3.goalvalue,0) AS DECIMAL(36,2)) " & _
                                               "WHEN @LinkFieldId = E4.fieldunkid THEN CAST(ISNULL(E4.goalvalue,0) AS DECIMAL(36,2)) " & _
                                               "WHEN @LinkFieldId = E5.fieldunkid THEN CAST(ISNULL(E5.goalvalue,0) AS DECIMAL(36,2)) " & _
                                   "END,0) AS NVARCHAR(MAX))) + ' ' + ISNULL(U.uom,'') AS [" & Language.getMessage("clsAssess_Field_Master", 15, "Goal Value") & "] " & _
                         ",ISNULL(U.pct_completed,0) AS [" & Language.getMessage("clsassess_empfield1_master", 23, "% Completed") & "] " & _
                         ",ISNULL(U.odate,'') AS [" & Language.getMessage(mstrModuleName, 708, "Update Date") & "] " & _
                         ",ISNULL(U.remark,'') AS [" & Language.getMessage(mstrModuleName, 703, "Update Remark") & "] " & _
                         ",ISNULL(U.dstatus,'') AS [" & Language.getMessage(mstrModuleName, 704, "Goal Status") & "] " & _
                         ",ISNULL(U.approval_date,'') AS [" & Language.getMessage(mstrModuleName, 705, "Approval Date") & "] " & _
                         ",ISNULL(U.changeby,'') AS [" & Language.getMessage(mstrModuleName, 706, "Change By") & "] " & _
                         ",ISNULL(U.GoalAccomplishmentStatus,'') AS [" & Language.getMessage("clsassess_empfield1_master", 28, "Goal Accomplishment Status") & "] " & _
                         ",ISNULL(U.approval_remark,'') AS [" & Language.getMessage(mstrModuleName, 706, "Approval Remark") & "] " & _
                         ",ISNULL(U.opct,0) AS opct " & _
                         "/*,ISNULL(C.field_data,'') AS Col_CoyField1 " & _
                         ",ISNULL(O.field_data,'') AS Col_OwrField1*/ "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields & " "
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hrassess_empfield1_master AS E1 " & _
                         "JOIN hremployee_master AS EM ON E1.employeeunkid = EM.employeeunkid " & _
                     "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = EM.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = EM.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS CC ON CC.employeeunkid = EM.employeeunkid AND CC.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        pt.gradegroupunkid " & _
                    "       ,pt.gradeunkid " & _
                    "       ,pt.gradelevelunkid " & _
                    "       ,pt.employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY pt.employeeunkid ORDER BY pt.incrementdate DESC, pt.salaryincrementtranunkid DESC) AS rno " & _
                    "   FROM prsalaryincrement_tran pt " & _
                    "   WHERE pt.isvoid = 0 AND pt.isapproved = 1 AND CONVERT(CHAR(8),pt.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS grds ON EM.employeeunkid = grds.employeeunkid AND grds.rno = 1 "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= mstrSelectedJoin & " "

            StrQ &= mstrAnalysis_Join & " "

            StrQ &= "LEFT JOIN hrassess_owrfield1_master AS O ON O.owrfield1unkid = E1.owrfield1unkid AND O.isvoid = 0 AND O.periodunkid = @periodunkid " & _
                         "LEFT JOIN hrassess_coyfield1_master AS C ON C.coyfield1unkid = O.coyfield1unkid AND C.isvoid = 0 AND C.periodunkid = @periodunkid " & _
                         "LEFT JOIN hrassess_empfield2_master AS E2 ON E1.empfield1unkid = E2.empfield1unkid AND E2.isvoid = 0 AND E2.periodunkid = @periodunkid " & _
                         "LEFT JOIN hrassess_empfield3_master AS E3 ON E2.empfield2unkid = E3.empfield2unkid AND E3.isvoid = 0 AND E3.periodunkid = @periodunkid " & _
                         "LEFT JOIN hrassess_empfield4_master AS E4 ON E3.empfield3unkid = E4.empfield3unkid AND E4.isvoid = 0 AND E4.periodunkid = @periodunkid " & _
                         "LEFT JOIN hrassess_empfield5_master AS E5 ON E4.empfield4unkid = E5.empfield4unkid AND E5.isvoid = 0 AND E5.periodunkid = @periodunkid " & _
                         "LEFT JOIN hrassess_perspective_master AS P ON P.perspectiveunkid = (CASE WHEN E1.perspectiveunkid > 0 THEN E1.perspectiveunkid ELSE ISNULL(C.perspectiveunkid,0) END) " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                                   "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                                   "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                                   "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                                   "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                           "END,0) AS EmpFldUnkid " & _
                                   ",ISNULL(I.field_data,'') AS Col_InfoFld6 " & _
                                   ",EM.employeeunkid " & _
                              "FROM hrassess_empinfofield_tran AS I " & _
                                   "LEFT JOIN hrassess_empfield1_master F1 ON F1.empfield1unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD1 & " AND F1.periodunkid = @periodunkid AND F1.isvoid = 0 " & _
                                   "JOIN hremployee_master AS EM ON F1.employeeunkid = EM.employeeunkid " & _
                                   "LEFT JOIN hrassess_empfield2_master F2 ON F2.empfield2unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD2 & " AND F2.periodunkid = @periodunkid AND F2.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield3_master F3 ON F3.empfield3unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD3 & " AND F3.periodunkid = @periodunkid AND F3.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield4_master F4 ON F4.empfield4unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD4 & " AND F4.periodunkid = @periodunkid AND F4.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield5_master F5 ON F5.empfield5unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD5 & " AND F5.periodunkid = @periodunkid AND F5.isvoid = 0 " & _
                              "WHERE I.fieldunkid = 6 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND EM.employeeunkid = @employeeid "
            End If
            StrQ &= "AND ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                                       "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                                       "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                                       "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                                       "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                             "END,0) > 0 " & _
                         ") AS F6 ON F6.EmpFldUnkid = (CASE WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD1 & " THEN E1.empfield1unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD2 & " THEN E2.empfield2unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD3 & " THEN E3.empfield3unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD4 & " THEN E4.empfield4unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD5 & " THEN E5.empfield5unkid " & _
                                                        "END) AND F6.employeeunkid = EM.employeeunkid " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                                   "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                                   "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                                   "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                                   "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                           "END,0) AS EmpFldUnkid " & _
                                   ",ISNULL(I.field_data,'') AS Col_InfoFld7 " & _
                                   ",EM.employeeunkid " & _
                              "FROM hrassess_empinfofield_tran AS I " & _
                                   "LEFT JOIN hrassess_empfield1_master F1 ON F1.empfield1unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD1 & " AND F1.periodunkid = @periodunkid AND F1.isvoid = 0 " & _
                                   "JOIN hremployee_master AS EM ON F1.employeeunkid = EM.employeeunkid " & _
                                   "LEFT JOIN hrassess_empfield2_master F2 ON F2.empfield2unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD2 & " AND F2.periodunkid = @periodunkid AND F2.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield3_master F3 ON F3.empfield3unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD3 & " AND F3.periodunkid = @periodunkid AND F3.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield4_master F4 ON F4.empfield4unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD4 & " AND F4.periodunkid = @periodunkid AND F4.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield5_master F5 ON F5.empfield5unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD5 & " AND F5.periodunkid = @periodunkid AND F5.isvoid = 0 " & _
                              "WHERE I.fieldunkid = 7 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND EM.employeeunkid = @employeeid "
            End If
            StrQ &= "AND ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                                       "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                                       "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                                       "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                                       "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                             "END,0) > 0 " & _
                         ") AS F7 ON F7.EmpFldUnkid = (CASE WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD1 & " THEN E1.empfield1unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD2 & " THEN E2.empfield2unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD3 & " THEN E3.empfield3unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD4 & " THEN E4.empfield4unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD5 & " THEN E5.empfield5unkid " & _
                                                       "END) AND F7.employeeunkid = EM.employeeunkid " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                                   "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                                   "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                                   "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                                   "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                           "END,0) AS EmpFldUnkid " & _
                                   ",ISNULL(I.field_data,'') AS Col_InfoFld8 " & _
                                   ",EM.employeeunkid " & _
                              "FROM hrassess_empinfofield_tran AS I " & _
                                   "LEFT JOIN hrassess_empfield1_master F1 ON F1.empfield1unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD1 & " AND F1.periodunkid = @periodunkid AND F1.isvoid = 0 " & _
                                   "JOIN hremployee_master AS EM ON F1.employeeunkid = EM.employeeunkid " & _
                                   "LEFT JOIN hrassess_empfield2_master F2 ON F2.empfield2unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD2 & " AND F2.periodunkid = @periodunkid AND F2.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield3_master F3 ON F3.empfield3unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD3 & " AND F3.periodunkid = @periodunkid AND F3.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield4_master F4 ON F4.empfield4unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD4 & " AND F4.periodunkid = @periodunkid AND F4.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield5_master F5 ON F5.empfield5unkid = I.empfieldunkid AND I.empfieldtypeid = " & enWeight_Types.WEIGHT_FIELD5 & " AND F5.periodunkid = @periodunkid AND F5.isvoid = 0 " & _
                              "WHERE I.fieldunkid = 8 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND EM.employeeunkid = @employeeid "
            End If
            StrQ &= "AND ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                                       "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                                       "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                                       "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                                       "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                             "END,0) > 0 " & _
                         ") AS F8 ON F8.EmpFldUnkid = (CASE WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD1 & " THEN E1.empfield1unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD2 & " THEN E2.empfield2unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD3 & " THEN E3.empfield3unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD4 & " THEN E4.empfield4unkid " & _
                                                           "WHEN @LinkFieldId = " & enWeight_Types.WEIGHT_FIELD5 & " THEN E5.empfield5unkid " & _
                                                      "END) AND F8.employeeunkid = EM.employeeunkid " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                              "* " & _
                              "FROM " & _
                              "( " & _
                              "SELECT " & _
                               "ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                              "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                              "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                              "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                              "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                        "END,0) AS EmpFldUnkid " & _
                              ",U.periodunkid " & _
                              ",U.employeeunkid " & _
                              ",CAST(CAST(U.pct_completed AS DECIMAL(36,2)) AS NVARCHAR(MAX))  AS pct_completed " & _
                              ",U.pct_completed AS opct " & _
                              ",U.remark " & _
                              ",CASE WHEN U.statusunkid = 1 THEN @ST_PENDING " & _
                                     "WHEN U.statusunkid = 2 THEN @ST_INPROGRESS " & _
                                     "WHEN U.statusunkid = 3 THEN @ST_COMPLETE " & _
                                     "WHEN U.statusunkid = 4 THEN @ST_CLOSED " & _
                                     "WHEN U.statusunkid = 5 THEN @ST_ONTRACK " & _
                                     "WHEN U.statusunkid = 6 THEN @ST_ATRISK " & _
                                     "WHEN U.statusunkid = 7 THEN @ST_NOTAPPLICABLE " & _
                                     "WHEN U.statusunkid = 8 THEN @ST_POSTPONED " & _
                                     "WHEN U.statusunkid = 9 THEN @ST_BEHIND_SCHEDULE " & _
                                     "WHEN U.statusunkid = 10 THEN @ST_AHEAD_SCHEDULE " & _
                               "END AS dstatus " & _
                              ",REPLACE(CONVERT(NVARCHAR(11),U.updatedate),' ','-') AS odate " & _
                              ",U.approvalstatusunkid AS approvalstatusunkid " & _
                              ",U.approval_remark AS approval_remark " & _
                              ",REPLACE(CONVERT(NVARCHAR(11),U.approval_date),' ','-') AS approval_date " & _
                              ",U.changebyid " & _
                              ",U.finalvalue " & _
                              ",CASE approvalstatusunkid WHEN 1 THEN @GA_Pending " & _
                                                              "WHEN 2 THEN @GA_Approved " & _
                                                              "WHEN 3 THEN @GA_Rejected " & _
                               "ELSE '' " & _
                               "END AS GoalAccomplishmentStatus " & _
                              ",ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.empfield1unkid,0) " & _
                                              "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.empfield2unkid,0) " & _
                                              "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.empfield3unkid,0) " & _
                                              "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.empfield4unkid,0) " & _
                                              "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.empfield5unkid,0) " & _
                                         "END,0) AS empfieldunkid " & _
                              ",U.empupdatetranunkid " & _
                              ",U.approvalstatusunkid AS Accomplished_statusId " & _
                              ",ISNULL(cfcommon_master.name, '') AS uom " & _
                              ",CASE WHEN U.changebyid = 1 THEN @P " & _
                                     "WHEN U.changebyid = 2 THEN @V " & _
                                   "END AS changeby " & _
                              ",CASE WHEN (CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.goaltypeid,1) " & _
                                                   "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.goaltypeid,1) " & _
                                                   "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.goaltypeid,1) " & _
                                                   "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.goaltypeid,1) " & _
                                                   "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.goaltypeid,1) " & _
                                    "END) = 2 AND changebyid = 1 " & _
                                        "THEN CAST(ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.goalvalue,0) " & _
                                        "END,0) * (finalvalue / 100) AS DECIMAL(36, 2)) " & _
                                   "ELSE finalvalue " & _
                                   "END AS ofinalvalue " & _
                              ",ISNULL(CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.goalvalue,0) " & _
                                              "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.goalvalue,0) " & _
                                        "END,0) AS Col_Target " & _
                              ",ROW_NUMBER()OVER(PARTITION BY U.empfieldunkid, U.periodunkid ORDER BY U.empupdatetranunkid DESC) AS xRno " & _
                              "FROM hrassess_empupdate_tran AS U " & _
                                   "LEFT JOIN hrassess_empfield1_master F1 ON F1.empfield1unkid = U.empfieldunkid AND U.empfieldtypeid = 1 AND F1.periodunkid = U.periodunkid AND F1.periodunkid = @periodunkid AND F1.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield2_master F2 ON F2.empfield2unkid = U.empfieldunkid AND U.empfieldtypeid = 2 AND F2.periodunkid = U.periodunkid AND F2.periodunkid = @periodunkid AND F2.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield3_master F3 ON F3.empfield3unkid = U.empfieldunkid AND U.empfieldtypeid = 3 AND F3.periodunkid = U.periodunkid AND F3.periodunkid = @periodunkid AND F3.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield4_master F4 ON F4.empfield4unkid = U.empfieldunkid AND U.empfieldtypeid = 4 AND F4.periodunkid = U.periodunkid AND F4.periodunkid = @periodunkid AND F4.isvoid = 0 " & _
                                   "LEFT JOIN hrassess_empfield5_master F5 ON F5.empfield5unkid = U.empfieldunkid AND U.empfieldtypeid = 5 AND F5.periodunkid = U.periodunkid AND F5.periodunkid = @periodunkid AND F5.isvoid = 0 " & _
                                   "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = (CASE WHEN @LinkFieldId = F1.fieldunkid THEN ISNULL(F1.uomtypeid,0) " & _
                                                                                                                    "WHEN @LinkFieldId = F2.fieldunkid THEN ISNULL(F2.uomtypeid,0) " & _
                                                                                                                    "WHEN @LinkFieldId = F3.fieldunkid THEN ISNULL(F3.uomtypeid,0) " & _
                                                                                                                    "WHEN @LinkFieldId = F4.fieldunkid THEN ISNULL(F4.uomtypeid,0) " & _
                                                                                                                    "WHEN @LinkFieldId = F5.fieldunkid THEN ISNULL(F5.uomtypeid,0) " & _
                                                                                                              "END) " & _
                              "WHERE U.isvoid = 0 AND U.periodunkid = @periodunkid "

            If mintEmployeeId > 0 Then
                StrQ &= " AND U.employeeunkid = @employeeid "
            End If

            If mdtUpdateDateFrom <> Nothing AndAlso mdtUpdateDateTo <> Nothing Then
                StrQ &= " AND CONVERT(CHAR(8),U.updatedate,112) BETWEEN '" & eZeeDate.convertDate(mdtUpdateDateFrom).ToString() & "' AND '" & eZeeDate.convertDate(mdtUpdateDateTo).ToString() & "' "
            End If

            If mdtApprovalDateFrom <> Nothing AndAlso mdtApprovalDateTo <> Nothing Then
                StrQ &= " AND CONVERT(CHAR(8),U.approval_date,112) BETWEEN '" & eZeeDate.convertDate(mdtApprovalDateFrom).ToString() & "' AND '" & eZeeDate.convertDate(mdtApprovalDateTo).ToString() & "' "
            End If

            If mintApprovalStatusId > 0 Then
                StrQ &= " AND U.approvalstatusunkid = " & mintApprovalStatusId
            End If

            StrQ &= ") AS U WHERE U.xRno = 1 " & _
                         ") AS U On U.empfieldunkid =  (CASE WHEN @LinkFieldId = 1 THEN E1.empfield1unkid " & _
                                                                    "WHEN @LinkFieldId = 2 THEN E2.empfield2unkid " & _
                                                                    "WHEN @LinkFieldId = 3 THEN E3.empfield3unkid " & _
                                                                    "WHEN @LinkFieldId = 4 THEN E4.empfield4unkid " & _
                                                                    "WHEN @LinkFieldId = 5 THEN E5.empfield5unkid " & _
                                                             "END) AND U.employeeunkid = EM.employeeunkid " & _
                    "WHERE E1.isvoid = 0 AND E1.periodunkid = @periodunkid "

            If mintEmployeeId > 0 Then
                StrQ &= " AND EM.employeeunkid = @employeeid "
            End If

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAllocationIds
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If
            StrQ &= "ORDER BY EM.employeecode "


            Dim iExFliter As String = ""
            If mintEmployeeId > 0 Then                
                iExFliter &= "," & Language.getMessage(mstrModuleName, 300, "Employee :") & " " & mstrEmployeeName
            End If

            If mintApprovalStatusId > 0 Then
                iExFliter &= "," & Language.getMessage(mstrModuleName, 601, "Approval Status :") & " " & mstrApprovalStatusName
            End If

            If mdtUpdateDateFrom <> Nothing AndAlso mdtUpdateDateTo <> Nothing Then
                iExFliter &= ", " & Language.getMessage(mstrModuleName, 602, "Update Date Between : ") & " " & mdtUpdateDateFrom.ToShortDateString() & " " & _
                                    Language.getMessage(mstrModuleName, 603, "And :") & " " & mdtUpdateDateTo.ToShortDateString()
            End If

            If mdtApprovalDateFrom <> Nothing AndAlso mdtApprovalDateTo <> Nothing Then
                iExFliter &= ", " & Language.getMessage(mstrModuleName, 604, "Approval Date Between : ") & " " & mdtApprovalDateFrom.ToShortDateString() & " " & _
                                    Language.getMessage(mstrModuleName, 603, "And :") & " " & mdtApprovalDateTo.ToShortDateString()
            End If

            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))
            objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Pending"))
            objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 760, "Approved"))
            objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 761, "Disapprove"))
            objDataOperation.AddParameter("@GT_QUALITATIVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 843, "Qualitative"))
            objDataOperation.AddParameter("@GT_QUANTITATIVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 844, "Quantitative"))
            objDataOperation.AddParameter("@ST_POSTPONED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 748, "Postponed"))
            objDataOperation.AddParameter("@ST_BEHIND_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 749, "Behind Schedule"))
            objDataOperation.AddParameter("@ST_AHEAD_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 750, "Ahead of Schedule"))
            objDataOperation.AddParameter("@P", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 101, "Percentage"))
            objDataOperation.AddParameter("@V", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 102, "Value"))

            dsList = objDataOperation.ExecQuery(StrQ.Replace("&nbsp;", ""), "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrQ = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mformat]') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' )) " & _
                   "BEGIN " & _
                   "    DROP FUNCTION mformat " & _
                   "END "

            objDataOperation.ExecNonQuery(StrQ)


            Dim dtExcelTab As New DataTable

            If dsList.Tables(0).Select("opct > 0").Length > 0 Then
                dtExcelTab = dsList.Tables(0).Select("opct > 0").CopyToDataTable()
            Else
                dtExcelTab = dsList.Tables(0).Clone()
            End If

            dtExcelTab.Columns.Remove("opct")

            Dim strarrGroupColumns As String() = Nothing
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtExcelTab.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 90
                    Case 1
                        intArrayColumnWidth(i) = 140
                    Case 2
                        intArrayColumnWidth(i) = 160
                    Case Else
                        intArrayColumnWidth(i) = 200
                End Select
            Next

            If mintViewIndex > 0 Then
                dtExcelTab.Columns.Remove("Id")
                dtExcelTab.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                dtExcelTab.Columns.Remove("Id")
                dtExcelTab.Columns.Remove("GName")
            End If


            If iExFliter.Trim.Length > 0 Then iExFliter = Mid(iExFliter, 2)
            Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dtExcelTab, intArrayColumnWidth, True, True, False, strarrGroupColumns, mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportProgressUpdateDetailReport; Module Name: " & mstrModuleName)
        Finally            
        End Try
    End Sub
    'S.SANDEEP |24-JUN-2023| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 18, "Period :")
            Language.setMessage(mstrModuleName, 100, "Select")
            Language.setMessage(mstrModuleName, 101, "Updated")
            Language.setMessage(mstrModuleName, 102, "Not Updated")
            Language.setMessage(mstrModuleName, 103, "Not Planned")
            Language.setMessage(mstrModuleName, 300, "Employee :")
            Language.setMessage(mstrModuleName, 301, "Status :")
            Language.setMessage(mstrModuleName, 400, "Employee Code")
            Language.setMessage(mstrModuleName, 401, "Employee Name")
            Language.setMessage(mstrModuleName, 402, "Job")
            Language.setMessage(mstrModuleName, 403, "Progress Update Status")
            Language.setMessage(mstrModuleName, 404, "Average% updated to date")
            Language.setMessage(mstrModuleName, 405, "As on Date")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
