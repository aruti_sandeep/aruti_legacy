'************************************************************************************************************************************
'Class Name : frmCustomItemValueReport.vb
'Purpose    : 
'CreateDate : 15 FEB 2017
'Written By : Shani K. Sheladiya
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmCustomItemValueReport


#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentSoreRatingReport"
    Private objCustomItemValue As clsCustomItemValueReport
    Private mstrAdvanceFilter As String = String.Empty

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
#End Region

#Region " Contructor "

    Public Sub New()
        objCustomItemValue = New clsCustomItemValueReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCustomItemValue.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = (New clsassess_custom_header).getComboList(0, True, "List")
            With cboCustomHeader
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
            With cboReporttype
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Custom Item Header & Period Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Custom Item Employee & Period Wise"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP [01-OCT-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose() : ObjEmp = Nothing : ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboCustomHeader.SelectedValue = 0
            mstrAdvanceFilter = ""
            mstrAnalysis_Join = ""
            mstrStringName = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_Fields = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objCustomItemValue.SetDefaultValue()

            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If cboCustomHeader.SelectedIndex <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Custom Header is mandatory information. Please select Custom Header to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If lvCustomItem.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one custom item to export report."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
            'objCustomItemValue._Report_Name = EZeeHeader1.Title
            objCustomItemValue._Report_Name = cboReporttype.Text
            'S.SANDEEP [01-OCT-2018] -- END
            objCustomItemValue._EmployeeId = cboEmployee.SelectedValue
            objCustomItemValue._EmployeeName = cboEmployee.Text
            objCustomItemValue._PeriodId = cboPeriod.SelectedValue
            objCustomItemValue._PeriodName = cboPeriod.Text
            objCustomItemValue._CustomHeaderText = cboCustomHeader.Text

            objCustomItemValue._Analysis_Join = mstrAnalysis_Join
            objCustomItemValue._ViewByName = mstrStringName
            objCustomItemValue._Report_GroupName = mstrReport_GroupName
            objCustomItemValue._Advance_Filter = mstrAdvanceFilter
            objCustomItemValue._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCustomItemValue._Analysis_Fields = mstrAnalysis_Fields

            Dim dRow() As DataRow
            Dim strItemIds As String = String.Join(",", lvCustomItem.CheckedItems.Cast(Of ListViewItem).Select(Function(x) x.Tag.ToString).ToArray)
            Dim dList As DataSet = (New clsassess_custom_items).GetList("List")
            dRow = dList.Tables(0).Select("customitemunkid IN (" & strItemIds & ")")
            objCustomItemValue._Item_Rows = dRow
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_List()
        Dim dList As DataSet
        Try
            dList = (New clsassess_custom_items).getComboList(CInt(cboPeriod.SelectedValue), CInt(cboCustomHeader.SelectedValue), False, "List")
            If dList Is Nothing Then Exit Sub

            lvCustomItem.Items.Clear()
            For Each dtRow As DataRow In dList.Tables(0).Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dtRow.Item("Name").ToString
                lvItem.Tag = dtRow.Item("Id")
                lvCustomItem.Items.Add(lvItem)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmAssessmentSoreRatingReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCustomItemValue = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAssessmentSoreRatingReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentSoreRatingReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Me.EZeeHeader1.Title = objCustomItemValue._ReportName
            Me.EZeeHeader1.Message = objCustomItemValue._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentSoreRatingReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            'objCustomItemValue.Export_Custom_Item_Value_Report(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Dim StrFileName As String = String.Empty
            objCustomItemValue.Export_Custom_Item_Value_Report(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, Company._Object._Companyunkid, User._Object._Userunkid, StrFileName)
            'S.SANDEEP [13-JUL-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCustomItemValueReport.SetMessages()
            objfrm._Other_ModuleNames = "clsCustomItemValueReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            objCustomItemValue.SetDefaultValue()

            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            objCustomItemValue._Report_Name = cboReporttype.Text
            objCustomItemValue._EmployeeId = cboEmployee.SelectedValue
            objCustomItemValue._EmployeeName = cboEmployee.Text
            objCustomItemValue._PeriodId = cboPeriod.SelectedValue
            objCustomItemValue._PeriodName = cboPeriod.Text

            objCustomItemValue.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReport_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [01-OCT-2018] -- END
    

#End Region

#Region " Controls Events "

    Private Sub cboCustomHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomHeader.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCustomHeader.SelectedValue) > 0 Then
                Call Fill_List()
            Else
                lvCustomItem.Items.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
    Private Sub cboReporttype_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReporttype.SelectedIndexChanged
        Try
            If cboReporttype.SelectedIndex = 0 Then
                cboCustomHeader.SelectedValue = 0
                cboCustomHeader.Enabled = True
                lnkSetAnalysis.Visible = True
                btnExport.Visible = True
                btnReport.Visible = False
            Else
                cboCustomHeader.SelectedValue = 0
                cboCustomHeader.Enabled = False
                lnkSetAnalysis.Visible = False
                btnExport.Visible = False
                btnReport.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReporttype_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [01-OCT-2018] -- END

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhItem.Text = Language._Object.getCaption(CStr(Me.colhItem.Tag), Me.colhItem.Text)
            Me.lblItemGroup.Text = Language._Object.getCaption(Me.lblItemGroup.Name, Me.lblItemGroup.Text)
            Me.lblItem.Text = Language._Object.getCaption(Me.lblItem.Name, Me.lblItem.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 2, "Custom Header is mandatory information. Please select Custom Header to continue.")
            Language.setMessage(mstrModuleName, 3, "Please select atleast one custom item to export report.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
