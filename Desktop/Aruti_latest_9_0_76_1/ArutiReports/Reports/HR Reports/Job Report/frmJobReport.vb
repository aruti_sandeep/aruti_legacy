'************************************************************************************************************************************
'Class Name : frmJobReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmJobReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmJobReport"
    Private objJob As clsJobReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    'Hemant (22 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private mblnCancel As Boolean = True
    Private mintJobunkid As Integer = 0
    'Hemant (22 Aug 2019) -- End
#End Region

#Region " Display Dialogue "
    'Hemant (22 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Public Function DisplayDialog(ByVal intJobunkid As Integer) As Boolean
        Try
            mintJobunkid = intJobunkid
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function
    'Hemant (22 Aug 2019) -- End
#End Region

#Region " Constructor "

    Public Sub New()
        objJob = New clsJobReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objJob.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objJob As New clsJobs
        Dim dsList As New DataSet
        Try

            dsList = objJob.getComboList("Job", True)

            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Job")
                'Hemant (22 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                '.SelectedValue = 0
                .SelectedValue = mintJobunkid
                'Hemant (22 Aug 2019) -- End
            End With


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            Dim objEmployee As New clsEmployee_Master
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                    , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables(0)

            'Pinkal (03-Dec-2015) -- End

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            Dim objclsMasterData As New clsMasterData
            dsList = objclsMasterData.getComboListForJobReportTemplate("JobReportList")
            cboReportType.ValueMember = "Id"
            cboReportType.DisplayMember = "Name"
            cboReportType.DataSource = dsList.Tables(0)
            'Hemant (15 Nov 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objJob = Nothing
            dsList = Nothing
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            If CInt(cboJob.SelectedValue) <= 0 AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Job Or Employee to view this report."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Return False
            End If

            'Pinkal (03-Dec-2015) -- End

            objJob.SetDefaultValue()

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objCategorize As New clsemployee_categorization_Tran
                Dim dsJobList As DataSet = objCategorize.Get_Current_Job(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboEmployee.SelectedValue))
                If dsJobList IsNot Nothing AndAlso dsJobList.Tables(0).Rows.Count > 0 Then
                    objJob._JobId = CInt(dsJobList.Tables(0).Rows(0)("jobunkid"))
                End If
                dsJobList = Nothing
                objCategorize = Nothing

                Dim objGrade As New clsMasterData
                Dim dsGrade As DataSet = objGrade.Get_Current_Scale("List", CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                If dsGrade IsNot Nothing AndAlso dsGrade.Tables(0).Rows.Count > 0 Then
                    objJob._GradeId = dsGrade.Tables(0).Rows(0)("gradeunkid")
                    objJob._EmployeeGrade = dsGrade.Tables(0).Rows(0)("Grade")
                End If

                objJob._EmployeeId = CInt(cboEmployee.SelectedValue)
                objJob._EmployeeName = cboEmployee.Text
            Else
                objJob._JobId = CInt(cboJob.SelectedValue)
                objJob._JobName = cboJob.Text
            End If
            objJob._EmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
            'Pinkal (03-Dec-2015) -- End

            objJob._ReportId = CInt(cboReportType.SelectedValue) 'Hemant (15 Nov 2019)
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objJob.setDefaultOrderBy(0)
            cboJob.SelectedIndex = 0

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            cboEmployee.SelectedValue = 0
            'Pinkal (03-Dec-2015) -- End

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmJobReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objJob = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objJob._ReportName
            Me._Message = objJob._ReportDesc
            Call FillCombo()
            'Hemant (22 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            If mintJobunkid <= 0 Then
                'Hemant (06 Aug 2019) -- End
            Call ResetValue()
            End If 'Hemant (22 Aug 2019) 
            'Sohail (19 Mar 2020) -- Start
            'NMB Enhancement # : Job listing report should be selected as default report type.
            cboReportType.SelectedValue = CInt(enJob_Report_Template.Job_Listing_Report)
            'Sohail (19 Mar 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmJobReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboJob
                objfrm.DataSource = CType(cboJob.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    Private Sub objbtnSearchEmployeee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployeee.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployeee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- End

    Private Sub frmJobReport_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objJob.generateReport(0, e.Type, enExportAction.None)
            objJob.generateReportNew(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                     ConfigParameter._Object._ExportReportPath, _
                                     ConfigParameter._Object._OpenAfterExport, _
                                     0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objJob.generateReport(0, enPrintAction.None, e.Type)
            objJob.generateReportNew(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                     ConfigParameter._Object._ExportReportPath, _
                                     ConfigParameter._Object._OpenAfterExport, _
                                     0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJobReport.SetMessages()
            objfrm._Other_ModuleNames = "clsJobReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

#Region "ComboBox Event"

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If cbo.Name.ToUpper = "CBOJOB" Then
                If CInt(cboJob.SelectedValue) <= 0 Then
                    cboEmployee.Enabled = True
                    objbtnSearchEmployeee.Enabled = True
                Else
                    cboEmployee.Enabled = False
                    objbtnSearchEmployeee.Enabled = False
                End If
            ElseIf cbo.Name.ToUpper = "CBOEMPLOYEE" Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    cboJob.Enabled = True
                    objbtnSearchJob.Enabled = True
                Else
                    cboJob.Enabled = False
                    objbtnSearchJob.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (03-Dec-2015) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()


            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "Please Select Job Or Employee to view this report.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

  
End Class
