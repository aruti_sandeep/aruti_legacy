Imports Aruti.Data
Imports eZeeCommonLib
Imports ExcelWriter

Public Class clsDisciplineSuspensionReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsDisciplineSuspensionReport"
    Private mstrReportId As String = enArutiReport.DisciplineSuspensionReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mdtSuspensionDateFrom As Date = Nothing
    Private mdtSuspensionDateTo As Date = Nothing
    Private mintOffenceCategoryId As Integer = 0
    Private mstrOffenceCategory As String = ""
    Private mintOffenceDescrId As Integer = 0
    Private mstrOffenceDescription As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnApplyAccessFilter As Boolean = True

    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrAdvance_Filter As String = ""
    Private mintActionId As Integer = 0
    Private mstrActionName As String = ""

    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    'S.SANDEEP |10-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
    Private mblnShowAllocationBasedOnChargeDate As Boolean = False
    Private mstrShowAllocationBasedOnChargeDateString As String = ""
    'S.SANDEEP |10-JUN-2020| -- END

    Private mblnIncludeInactiveEmployee As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _SuspensionDateFrom() As Date
        Set(ByVal value As Date)
            mdtSuspensionDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _SuspensionDateTo() As Date
        Set(ByVal value As Date)
            mdtSuspensionDateTo = value
        End Set
    End Property

    Public WriteOnly Property _OffenceCategoryId() As Integer
        Set(ByVal value As Integer)
            mintOffenceCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _OffenceCategory() As String
        Set(ByVal value As String)
            mstrOffenceCategory = value
        End Set
    End Property

    Public WriteOnly Property _OffenceDescrId() As Integer
        Set(ByVal value As Integer)
            mintOffenceDescrId = value
        End Set
    End Property

    Public WriteOnly Property _OffenceDescription() As String
        Set(ByVal value As String)
            mstrOffenceDescription = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ActionId() As Integer
        Set(ByVal value As Integer)
            mintActionId = value
        End Set
    End Property

    Public WriteOnly Property _ActionName() As String
        Set(ByVal value As String)
            mstrActionName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    'S.SANDEEP |10-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
    Public WriteOnly Property _ShowAllocationBasedOnChargeDate() As Boolean
        Set(ByVal value As Boolean)
            mblnShowAllocationBasedOnChargeDate = value
        End Set
    End Property
    Public WriteOnly Property _ShowAllocationBasedOnChargeDateString() As String
        Set(ByVal value As String)
            mstrShowAllocationBasedOnChargeDateString = value
        End Set
    End Property
    'S.SANDEEP |10-JUN-2020| -- END

    Public WriteOnly Property _IncludeInactiveEmployee() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmployee = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtSuspensionDateFrom = Nothing
            mdtSuspensionDateTo = Nothing
            mintOffenceCategoryId = 0
            mstrOffenceCategory = ""
            mintOffenceDescrId = 0
            mstrOffenceDescription = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnApplyAccessFilter = True
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrAdvance_Filter = ""
            mintActionId = 0
            mstrActionName = ""
            'S.SANDEEP |10-JUN-2020| -- START
            'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
            mblnShowAllocationBasedOnChargeDate = False
            mstrShowAllocationBasedOnChargeDateString = ""
            'S.SANDEEP |10-JUN-2020| -- END

            mblnIncludeInactiveEmployee = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EM.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Employee :") & " " & mstrEmployeeName & " "
            End If
            If mintOffenceCategoryId > 0 Then
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOffenceCategoryId)
                Me._FilterQuery &= " AND CM.masterunkid = @masterunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Offence Category :") & " " & mstrOffenceCategory & " "
            End If
            If mintOffenceDescrId > 0 Then
                objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOffenceDescrId)
                Me._FilterQuery &= " AND DS.disciplinetypeunkid = @disciplinetypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Offence :") & " " & mstrOffenceDescription & " "
            End If
            If mdtSuspensionDateFrom <> Nothing AndAlso mdtSuspensionDateTo <> Nothing Then
                objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtSuspensionDateFrom))
                objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtSuspensionDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),ED.date1,112) BETWEEN @Date1 AND @Date2 "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, "Suspension Start Date Between :") & " " & mdtSuspensionDateFrom.Date.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 104, "To :") & " " & mdtSuspensionDateTo.Date.ToShortDateString & " "
            End If

            'S.SANDEEP |10-JUN-2020| -- START
            'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
            If mblnShowAllocationBasedOnChargeDate Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 105, "Display Allocation Based On :") & " " & mstrShowAllocationBasedOnChargeDateString
            End If
            'S.SANDEEP |10-JUN-2020| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "EM")
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "EM")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "EM")

            If mblnShowAllocationBasedOnChargeDate Then
                StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                       "DROP TABLE #Results " & _
                       "CREATE TABLE #Results " & _
                       "( " & _
                       "    empid int, " & _
                       "    edate nvarchar(8), " & _
                       "    clsgrp nvarchar(max), " & _
                       "    cls nvarchar(max), " & _
                       "    ejob nvarchar(max) " & _
                       ") " & _
                       "DECLARE @Ddate  NVARCHAR(8), @EmpId INT " & _
                       "DECLARE discpl_alloc CURSOR " & _
                       "FOR " & _
                       "SELECT DISTINCT " & _
                       "     CONVERT(NVARCHAR(8),chargedate,112) AS efdate " & _
                       "    ,involved_employeeunkid as empid " & _
                       "FROM hrdiscipline_file_master " & _
                       "WHERE isvoid = 0 " & _
                       "OPEN discpl_alloc " & _
                       "FETCH NEXT FROM discpl_alloc INTO @Ddate,@EmpId " & _
                       "WHILE @@FETCH_STATUS = 0 " & _
                       "BEGIN " & _
                       "DECLARE @Q AS NVARCHAR(MAX) " & _
                       "SET @Q = 'INSERT INTO #Results(empid,edate,clsgrp,cls,ejob) " & _
                       "    SELECT " & _
                       "         EM.employeeunkid " & _
                       "        ,'''+@Ddate+''' " & _
                       "        ,ISNULL(ECG.name,'''') as clsgrp " & _
                       "        ,ISNULL(ECL.name,'''') as cls " & _
                       "        ,ISNULL(EJM.job_name,'''') as ejob " & _
                       "    FROM hremployee_master AS EM " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             CT.jobunkid " & _
                       "            ,CT.employeeunkid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                       "        FROM hremployee_categorization_tran AS CT " & _
                       "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '''+@Ddate+''' " & _
                       "        AND CT.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
                       "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                       "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ET.classgroupunkid " & _
                       "            ,ET.classunkid " & _
                       "            ,ET.employeeunkid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                       "        FROM hremployee_transfer_tran AS ET " & _
                       "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '''+@Ddate+''' " & _
                       "        AND ET.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
                       "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                       "    LEFT JOIN hrclassgroup_master AS ECG ON EC.classgroupunkid = ECG.classgroupunkid " & _
                       "    LEFT JOIN hrclasses_master AS ECL ON EC.classunkid = ECL.classesunkid " & _
                       "    WHERE EM.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) " & _
                       "    EXEC(@Q) " & _
                       "FETCH NEXT FROM discpl_alloc INTO @Ddate,@EmpId " & _
                       "END " & _
                       "CLOSE discpl_alloc " & _
                       "DEALLOCATE discpl_alloc " & _
                       "SELECT " & _
                       "     EM.employeecode " & _
                       "    ,EM.firstname + ' ' + EM.surname AS employee " & _
                       "    ,#Results.clsgrp " & _
                       "    ,#Results.cls " & _
                       "    ,#Results.ejob " & _
                       "    ,ISNULL(CM.name, '') AS offcat " & _
                       "    ,ISNULL(DS.name, '') AS offdes " & _
                       "    ,ISNULL(FT.incident_description, '') AS IncidentDesc " & _
                       "    ,ISNULL(CONVERT(CHAR(8), ED.date1, 112),'') AS suspensionfromdate " & _
                       "    ,ISNULL(CONVERT(CHAR(8), ED.date2, 112),'') AS suspensiontodate "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM #Results " & _
                        "    JOIN hrdiscipline_file_master AS FM ON FM.involved_employeeunkid = #Results.empid AND #Results.edate = CONVERT(CHAR(8),FM.chargedate,112) " & _
                        "    JOIN hremployee_dates_tran AS ED ON ed.disciplinefileunkid = FM.disciplinefileunkid " & _
                        "    JOIN hremployee_master AS EM ON EM.employeeunkid = FM.involved_employeeunkid " & _
                        "    JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                        "    JOIN hrdisciplinetype_master AS DS ON DS.disciplinetypeunkid = FT.offenceunkid " & _
                        "    LEFT JOIN hrdiscipline_proceeding_master AS PMT ON PMT.disciplinefiletranunkid = FT.disciplinefiletranunkid AND PMT.isvoid = 0 " & _
                        "    LEFT JOIN hraction_reason_master AS PMTR ON PMT.actionreasonunkid = PMTR.actionreasonunkid AND PMTR.isactive = 1 " & _
                        "    LEFT JOIN hrdiscipline_proceeding_master AS PMP ON PMP.disciplinefileunkid = FM.disciplinefileunkid AND PMP.isvoid = 0 " & _
                        "    LEFT JOIN hraction_reason_master AS PMPR ON PMP.actionreasonunkid = PMPR.actionreasonunkid AND PMPR.isactive = 1 " & _
                        "    JOIN cfcommon_master AS CM ON CM.masterunkid = DS.offencecategoryunkid "
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= mstrAnalysis_Join

                StrQ &= " WHERE FM.isvoid = 0 AND FT.isvoid = 0 "

                If mblnIncludeInactiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
                End If

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= " ORDER BY FM.reference_no "

                StrQ &= "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                     "DROP TABLE #Results "
            Else
                StrQ = "SELECT " & _
                           " EM.employeecode " & _
                           ",EM.firstname + ' ' + EM.surname AS employee " & _
                           ",ISNULL(EJM.job_name, '') AS ejob " & _
                           ",ISNULL(ECG.name, '') AS clsgrp " & _
                           ",ISNULL(ECL.name, '') AS cls " & _
                           ",ISNULL(CM.name, '') AS offcat " & _
                           ",ISNULL(DS.name, '') AS offdes " & _
                           ",ISNULL(FT.incident_description, '') AS IncidentDesc " & _
                           ",ISNULL(CONVERT(CHAR(8), ED.date1, 112),'') AS suspensionfromdate " & _
                           ",ISNULL(CONVERT(CHAR(8), ED.date2, 112),'') AS suspensiontodate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM hrdiscipline_file_master AS FM " & _
                            "    JOIN hremployee_dates_tran AS ED ON ed.disciplinefileunkid = FM.disciplinefileunkid " & _
                        "    JOIN hremployee_master AS EM ON EM.employeeunkid = FM.involved_employeeunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             CT.jobunkid " & _
                        "            ,CT.employeeunkid " & _
                        "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                        "        FROM hremployee_categorization_tran AS CT " & _
                        "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                        "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             ET.classgroupunkid " & _
                        "            ,ET.classunkid " & _
                        "            ,ET.employeeunkid " & _
                        "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                        "        FROM hremployee_transfer_tran AS ET " & _
                        "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                        "    LEFT JOIN hrclassgroup_master AS ECG ON EC.classgroupunkid = ECG.classgroupunkid " & _
                        "    LEFT JOIN hrclasses_master AS ECL ON EC.classunkid = ECL.classesunkid " & _
                        "    JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                        "    JOIN hrdisciplinetype_master AS DS ON DS.disciplinetypeunkid = FT.offenceunkid " & _
                        "    LEFT JOIN hrdiscipline_proceeding_master AS PMT ON PMT.disciplinefiletranunkid = FT.disciplinefiletranunkid AND PMT.isvoid = 0 " & _
                        "    LEFT JOIN hraction_reason_master AS PMTR ON PMT.actionreasonunkid = PMTR.actionreasonunkid AND PMTR.isactive = 1 " & _
                        "    LEFT JOIN hrdiscipline_proceeding_master AS PMP ON PMP.disciplinefileunkid = FM.disciplinefileunkid AND PMP.isvoid = 0 " & _
                        "    LEFT JOIN hraction_reason_master AS PMPR ON PMP.actionreasonunkid = PMPR.actionreasonunkid AND PMPR.isactive = 1 " & _
                        "    JOIN cfcommon_master AS CM ON CM.masterunkid = DS.offencecategoryunkid "
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= mstrAnalysis_Join

                StrQ &= " WHERE FM.isvoid = 0 AND ED.isvoid = 0 AND FT.isvoid = 0 "

                If mblnIncludeInactiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
                End If

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= " ORDER BY FM.reference_no "
            End If


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim mdtTableExcel As DataTable = dsList.Tables(0)
            Dim dcHeader As New Dictionary(Of Integer, String)
            Dim intColumnIndex As Integer = -1


            If IsNothing(mdtTableExcel) = False AndAlso mdtTableExcel.Rows.Count > 0 Then
                For Each rows As DataRow In mdtTableExcel.Rows
                    If rows("suspensionfromdate").ToString.Trim.Length > 0 Then
                        rows("suspensionfromdate") = eZeeDate.convertDate(rows("suspensionfromdate")).ToShortDateString
                    End If
                    If rows("suspensiontodate").ToString.Trim.Length > 0 Then
                        rows("suspensiontodate") = eZeeDate.convertDate(rows("suspensiontodate")).ToShortDateString
                    End If
                Next
            End If


            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If


            If mdtTableExcel.Columns.Contains("Id") Then
                mdtTableExcel.Columns.Remove("Id")
            End If

            If mintViewIndex <= 0 AndAlso mdtTableExcel.Columns.Contains("GName") Then
                mdtTableExcel.Columns.Remove("GName")
            End If


            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 210, "Employee Code")
            mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 210, "Employee Name")

            mdtTableExcel.Columns("clsgrp").Caption = Language.getMessage("clsMasterData", 420, "Class Group")
            mdtTableExcel.Columns("cls").Caption = Language.getMessage("clsMasterData", 419, "Classes")
            mdtTableExcel.Columns("ejob").Caption = Language.getMessage("clsMasterData", 421, "Jobs")

            mdtTableExcel.Columns("offcat").Caption = Language.getMessage(mstrModuleName, 211, "Offence Category")
            mdtTableExcel.Columns("offdes").Caption = Language.getMessage(mstrModuleName, 212, "Offence")
            mdtTableExcel.Columns("IncidentDesc").Caption = Language.getMessage(mstrModuleName, 213, "Incident Description")
            mdtTableExcel.Columns("suspensionfromdate").Caption = Language.getMessage(mstrModuleName, 215, "Suspension From Date")
            mdtTableExcel.Columns("suspensiontodate").Caption = Language.getMessage(mstrModuleName, 216, "Suspension To Date")


            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)


            'row = New WorksheetRow()
            'Dim intCount As Integer = 0
            'Do While intCount <= mdtTableExcel.Columns.Count - 1
            '    wcell = New WorksheetCell("", "HeaderStyle")

            '    row.Cells.Add(wcell)
            '    intCount += 1
            'Loop
            'rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage(mstrModuleName, 100, "Employee :")
            Language.setMessage(mstrModuleName, 101, "Offence Category :")
            Language.setMessage(mstrModuleName, 102, "Offence :")
            Language.setMessage(mstrModuleName, 103, "Charge Date From :")
            Language.setMessage(mstrModuleName, 104, "To :")
            Language.setMessage(mstrModuleName, 105, "Outcome :")
            Language.setMessage(mstrModuleName, 200, "Sr.No")
            Language.setMessage(mstrModuleName, 201, "Employee")
            Language.setMessage(mstrModuleName, 202, "Offence Category")
            Language.setMessage(mstrModuleName, 203, "Nature of allegations")
            Language.setMessage(mstrModuleName, 204, "Charge Date")
            Language.setMessage(mstrModuleName, 205, "Reference No")
            Language.setMessage(mstrModuleName, 206, "Printed By :")
            Language.setMessage(mstrModuleName, 207, "Printed Date :")
            Language.setMessage(mstrModuleName, 208, "Disciplinary Outcome")
            Language.setMessage(mstrModuleName, 209, "Outcome Date")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
