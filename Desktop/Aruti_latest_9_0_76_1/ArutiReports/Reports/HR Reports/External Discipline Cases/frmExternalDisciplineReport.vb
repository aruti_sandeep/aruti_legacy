#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExternalDisciplineReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDiscipline_Status_Summary_Report"
    Private objExDisciplineCase As clsExternalDisciplineCaseReport

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        objExDisciplineCase = New clsExternalDisciplineCaseReport(User._Object._Languageunkid,Company._Object._Companyunkid)
    End Sub

#End Region

#Region " Forms "

    Private Sub frmExternalDisciplineReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmExternalDisciplineReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExternalDisciplineReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objExDisciplineCase._ReportName
            Me._Message = objExDisciplineCase._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExternalDisciplineReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExternalDisciplineReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    'Call Form_Export_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmExternalDisciplineReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExternalDisciplineReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmExternalDisciplineReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim objEmp As New clsEmployee_Master
        Try
            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objEmp.GetEmployeeList("List", True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 15 MAY 2012 ] -- END
            With cboInvolvedPerson
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime
            cboInvolvedPerson.SelectedValue = 0
            chkInactiveemp.Checked = False
            txtCaseNo.Text = ""
            txtCourtName.Text = ""
            objExDisciplineCase.setDefaultOrderBy(0)
            txtOrderBy.Text = objExDisciplineCase.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objExDisciplineCase.SetDefaultValue()

            objExDisciplineCase._CaseNo = txtCaseNo.Text
            objExDisciplineCase._CourtName = txtCourtName.Text
            objExDisciplineCase._EmployeeId = cboInvolvedPerson.SelectedValue
            objExDisciplineCase._EmployeeName = cboInvolvedPerson.Text
            objExDisciplineCase._FromDate = dtpFromDate.Value.Date
            objExDisciplineCase._ToDate = dtpToDate.Value.Date
            objExDisciplineCase._IsActive = chkInactiveemp.Checked

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Button's Event "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExDisciplineCase.generateReport(0, e.Type, enExportAction.None)
            objExDisciplineCase.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, _
                                                  0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExDisciplineCase.generateReport(0, enPrintAction.None, e.Type)
            objExDisciplineCase.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, _
                                                  0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchInvPerson_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInvPerson.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboInvolvedPerson.ValueMember
                .DisplayMember = cboInvolvedPerson.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = cboInvolvedPerson.DataSource
            End With

            If frm.DisplayDialog Then
                cboInvolvedPerson.SelectedValue = frm.SelectedValue
                cboInvolvedPerson.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchInvPerson_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objExDisciplineCase.setOrderBy(0)
            txtOrderBy.Text = objExDisciplineCase.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExternalDisciplineCaseReport.SetMessages()
            objfrm._Other_ModuleNames = "clsExternalDisciplineCaseReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblInvolvedPerson.Text = Language._Object.getCaption(Me.lblInvolvedPerson.Name, Me.lblInvolvedPerson.Text)
			Me.lblCourtName.Text = Language._Object.getCaption(Me.lblCourtName.Name, Me.lblCourtName.Text)
			Me.lblCaseNo.Text = Language._Object.getCaption(Me.lblCaseNo.Name, Me.lblCaseNo.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblFormDate.Text = Language._Object.getCaption(Me.lblFormDate.Name, Me.lblFormDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
