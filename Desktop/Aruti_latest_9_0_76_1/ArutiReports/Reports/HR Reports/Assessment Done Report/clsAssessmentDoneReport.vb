'************************************************************************************************************************************
'Class Name : clsAssessmentDoneReport.vb
'Purpose    :
'Date       : 05 OCT 2017
'Written By : Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsAssessmentDoneReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAssessmentDoneReport"
    Private mstrReportId As String = enArutiReport.AssessmentDoneReport '195
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmplyeeName As String = String.Empty
    Private mblnIncludeSelfAssessment As Boolean = False
    Private mstrSelfAssessmentCaption As String = String.Empty
    Private mblnIncludeAssessorAssessment As Boolean = False
    Private mstrAssessorAssessmentCaption As String = String.Empty
    Private mblnIncludeReviewerAssessment As Boolean = False
    Private mstrReviewerAssessmentCaption As String = String.Empty
    Private mblnConsiderActiveEmpOnPeriod As Boolean = False
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnApplyUserAccessFilter As Boolean = True
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplyeeName() As String
        Set(ByVal value As String)
            mstrEmplyeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeSelfAssessment() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeSelfAssessment = value
        End Set
    End Property

    Public WriteOnly Property _SelfAssessmentCaption() As String
        Set(ByVal value As String)
            mstrSelfAssessmentCaption = value
        End Set
    End Property

    Public WriteOnly Property _IncludeAssessorAssessment() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAssessorAssessment = value
        End Set
    End Property

    Public WriteOnly Property _AssessorAssessmentCaption() As String
        Set(ByVal value As String)
            mstrAssessorAssessmentCaption = value
        End Set
    End Property

    Public WriteOnly Property _IncludeReviewerAssessment() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeReviewerAssessment = value
        End Set
    End Property

    Public WriteOnly Property _ReviewerAssessmentCaption() As String
        Set(ByVal value As String)
            mstrReviewerAssessmentCaption = value
        End Set
    End Property

    Public WriteOnly Property _ConsiderActiveEmpOnPeriod() As Boolean
        Set(ByVal value As Boolean)
            mblnConsiderActiveEmpOnPeriod = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mintEmployeeId = 0
            mstrEmplyeeName = String.Empty
            mblnIncludeSelfAssessment = False
            mstrSelfAssessmentCaption = String.Empty
            mblnIncludeAssessorAssessment = False
            mstrAssessorAssessmentCaption = String.Empty
            mblnIncludeReviewerAssessment = False
            mstrReviewerAssessmentCaption = String.Empty
            mblnConsiderActiveEmpOnPeriod = False
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mblnApplyUserAccessFilter = True
            mstrAdvance_Filter = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'objDataOperation.AddParameter("@yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Yes"))
            'objDataOperation.AddParameter("@no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "No"))

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            objDataOperation.AddParameter("@yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "v")
            objDataOperation.AddParameter("@no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "x")

            'objDataOperation.AddParameter("@yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "?")
            'objDataOperation.AddParameter("@no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "?")
            'S.SANDEEP |24-JUN-2023| -- END


            objDataOperation.AddParameter("@prdid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Period :") & " " & mstrPeriodName & " "

            If mintEmployeeId > 0 Then
                'S.SANDEEP [05-OCT-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 80
                'objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'S.SANDEEP [05-OCT-2017] -- END
                Me._FilterQuery &= " AND final.employeeunkid = @empid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Employee :") & " " & mstrEmplyeeName & " "
            End If

            If mblnIncludeSelfAssessment Then
                objDataOperation.AddParameter("@slf", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIncludeSelfAssessment)
                Me._FilterQuery &= " AND final.semp = @slf "
                Me._FilterTitle &= mstrSelfAssessmentCaption & " "
            End If

            If mblnIncludeAssessorAssessment Then
                objDataOperation.AddParameter("@alf", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIncludeAssessorAssessment)
                Me._FilterQuery &= " AND final.aemp = @alf "
                Me._FilterTitle &= mstrAssessorAssessmentCaption & " "
            End If

            If mblnIncludeReviewerAssessment Then
                objDataOperation.AddParameter("@rlf", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIncludeReviewerAssessment)
                Me._FilterQuery &= " AND final.remp = @rlf "
                Me._FilterTitle &= mstrReviewerAssessmentCaption & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsResult As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "select " & _
                   "     employeecode  " & _
                   "    ,empname " & _
                   "    ,selfassessment " & _
                   "    ,selfcommitted " & _
                   "    ,asrassessment " & _
                   "    ,asrcommitted " & _
                   "    ,rsrassessment " & _
                   "    ,rsrcommitted " & _
                   "    ,isnull(GName,'') as GName " & _
                   "    ,isnull(Id,0) as Id " & _
                   "from " & _
                   "( " & _
                   "    select " & _
                   "         hremployee_master.employeecode " & _
                   "        ,hremployee_master.employeeunkid " & _
                   "        ,hremployee_master.firstname + ' ' +hremployee_master.surname as empname " & _
                   "        ,case when isnull(slf.sempid,0) <= 0 then '--' else @yes end as selfassessment " & _
                   "        ,isnull(scmtt,'--') as selfcommitted " & _
                   "        ,case when isnull(alf.aempid,0) <= 0 then '--' else @yes end as asrassessment " & _
                   "        ,isnull(acmtt,'--') as asrcommitted " & _
                   "        ,case when isnull(rlf.rempid,0) <= 0 then '--' else @yes end as rsrassessment " & _
                   "        ,isnull(rcmtt,'--') as rsrcommitted " & _
                   "        ,case when isnull(slf.sempid,0) <= 0 then CAST(0 AS BIT) else CAST(1 AS BIT) end as semp " & _
                   "        ,case when isnull(alf.aempid,0) <= 0 then CAST(0 AS BIT) else CAST(1 AS BIT) end as aemp " & _
                   "        ,case when isnull(rlf.rempid,0) <= 0 then CAST(0 AS BIT) else CAST(1 AS BIT) end as remp "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "    from hremployee_master WITH (NOLOCK) "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= "    left join " & _
                    "    ( " & _
                    "        select " & _
                    "             selfemployeeunkid as sempid " & _
                    "            ,case when iscommitted = 0 then @no else @yes end as scmtt " & _
                    "        from hrevaluation_analysis_master WITH (NOLOCK) " & _
                    "        where periodunkid = @prdid and isvoid = 0 " & _
                    "        and assessmodeid = 1 " & _
                    "    ) as slf on hremployee_master.employeeunkid = slf.sempid " & _
                    "    left join " & _
                    "    ( " & _
                    "        select " & _
                    "             assessedemployeeunkid as aempid " & _
                    "            ,case when iscommitted = 0 then @no else @yes end as acmtt " & _
                    "        from hrevaluation_analysis_master WITH (NOLOCK) " & _
                    "        where periodunkid = @prdid and isvoid = 0 " & _
                    "        and assessmodeid = 2 " & _
                    "    ) as alf on hremployee_master.employeeunkid = alf.aempid " & _
                    "    left join " & _
                    "    ( " & _
                    "        select " & _
                    "             assessedemployeeunkid as rempid " & _
                    "            ,case when iscommitted = 0 then @no else @yes end as rcmtt " & _
                    "        from hrevaluation_analysis_master WITH (NOLOCK) " & _
                    "        where periodunkid = @prdid and isvoid = 0 " & _
                    "        and assessmodeid = 3 " & _
                    "    ) as rlf on hremployee_master.employeeunkid = rlf.rempid "

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= ") as final where 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                'S.SANDEEP |24-JUN-2023| -- START
                'ISSUE/ENHANCEMENT : Sprint 2023-13
                'rpt_Rows.Item("Column1") = dtRow.Item("employeecode") & " - " & dtRow.Item("empname")
                rpt_Rows.Item("Column1") = dtRow.Item("employeecode")
                'S.SANDEEP |24-JUN-2023| -- END

                rpt_Rows.Item("Column2") = dtRow.Item("selfassessment")
                rpt_Rows.Item("Column3") = dtRow.Item("selfcommitted")
                rpt_Rows.Item("Column4") = dtRow.Item("asrassessment")
                rpt_Rows.Item("Column5") = dtRow.Item("asrcommitted")
                rpt_Rows.Item("Column6") = dtRow.Item("rsrassessment")
                rpt_Rows.Item("Column7") = dtRow.Item("rsrcommitted")
                rpt_Rows.Item("Column8") = dtRow.Item("GName")
                'S.SANDEEP |24-JUN-2023| -- START
                'ISSUE/ENHANCEMENT : Sprint 2023-13
                rpt_Rows.Item("Column9") = dtRow.Item("empname")
                'S.SANDEEP |24-JUN-2023| -- END


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptAssessmentDoneReport

            Dim objReportFunction As New ReportFunction
            Call objReportFunction.GetReportSetting(CInt(mstrReportId), intCompanyUnkid)
            If objReportFunction._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If objReportFunction._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If objReportFunction._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If objReportFunction._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        objReportFunction._DisplayLogo, _
                                        objReportFunction._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        objReportFunction._LeftMargin, _
                                        objReportFunction._RightMargin, _
                                        objReportFunction._PageMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objReportFunction = Nothing

            objRpt.SetDataSource(rpt_Data)

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            'Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 200, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 251, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 250, "Name"))
            'S.SANDEEP |24-JUN-2023| -- END

            Call ReportFunction.TextChange(objRpt, "txtslf", Language.getMessage(mstrModuleName, 201, "Self"))
            Call ReportFunction.TextChange(objRpt, "txtslfcmt", Language.getMessage(mstrModuleName, 202, "Committed"))
            Call ReportFunction.TextChange(objRpt, "txtalf", Language.getMessage(mstrModuleName, 203, "Assessor"))
            Call ReportFunction.TextChange(objRpt, "txtalfcmt", Language.getMessage(mstrModuleName, 204, "Committed"))
            Call ReportFunction.TextChange(objRpt, "txtrlf", Language.getMessage(mstrModuleName, 205, "Reviewer"))
            Call ReportFunction.TextChange(objRpt, "txtrlfcmt", Language.getMessage(mstrModuleName, 206, "Committed"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 207, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 208, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 219, "Page :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 220, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:Generate_DetailReport ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
            Language.setMessage(mstrModuleName, 11, "Checked By :")
            Language.setMessage(mstrModuleName, 12, "Approved By :")
            Language.setMessage(mstrModuleName, 13, "Received By :")
            Language.setMessage(mstrModuleName, 100, "Period :")
            Language.setMessage(mstrModuleName, 101, "Employee :")
            Language.setMessage(mstrModuleName, 200, "Employee")
            Language.setMessage(mstrModuleName, 201, "Self")
            Language.setMessage(mstrModuleName, 202, "Committed")
            Language.setMessage(mstrModuleName, 203, "Assessor")
            Language.setMessage(mstrModuleName, 204, "Committed")
            Language.setMessage(mstrModuleName, 205, "Reviewer")
            Language.setMessage(mstrModuleName, 206, "Committed")
            Language.setMessage(mstrModuleName, 207, "Printed By :")
            Language.setMessage(mstrModuleName, 208, "Printed Date :")
            Language.setMessage(mstrModuleName, 219, "Page :")
            Language.setMessage(mstrModuleName, 220, "Grand Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
