'************************************************************************************************************************************
'Class Name : frmTrainingBudgetReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTrainingBudgetReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingBudgetReport"
    Private objTrainingBudget As clsTrainingBudgetReport

#End Region

#Region " Contructor "

    Public Sub New()
        objTrainingBudget = New clsTrainingBudgetReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTrainingBudget.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjInstitute As New clsinstitute_master
        Dim ObjTrain_Sched As New clsTraining_Scheduling
        Dim ObjDept As New clsDepartment
        Dim dsCombos As New DataSet

        'Anjan (10 Feb 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        Dim objCourseMaster As New clsCommon_Master
        'Anjan (10 Feb 2012)-End 

        Try


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsCombos = ObjEmp.GetEmployeeList("Emp", True, True)
            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombos = ObjEmp.GetEmployeeList("Emp", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsCombos = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 15 MAY 2012 ] -- END

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            dsCombos = ObjTrain_Sched.getComboList("Training", True, , True)
            'Anjan (10 Feb 2012)-End
            With cboTraining
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Training")
                .SelectedValue = 0
            End With



            dsCombos = ObjInstitute.getListForCombo(False, "Institute", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Institute")
                .SelectedValue = 0
            End With

            dsCombos = ObjDept.getComboList("Dept", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Dept")
                .SelectedValue = 0
            End With

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            Dim objMData As New clsMasterData
            dsCombos = objMData.GetEAllocation_Notification("List")
            Dim row As DataRow = dsCombos.Tables("List").NewRow
            With row
                .Item("Id") = 0
                .Item("Name") = Language.getMessage(mstrModuleName, 100, "Select")
            End With

            dsCombos.Tables("List").Rows.InsertAt(row, 0)
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing
            ObjInstitute = Nothing
            ObjTrain_Sched = Nothing
            ObjDept = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboInstitute.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboTraining.SelectedValue = 0


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            cboAllocations.SelectedValue = 0
            lvAllocation.Items.Clear()
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objTrainingBudget.SetDefaultValue()

            objTrainingBudget._TrainingId = cboTraining.SelectedValue
            objTrainingBudget._TrainingName = cboTraining.Text

            objTrainingBudget._EmployeeId = cboEmployee.SelectedValue
            objTrainingBudget._EmployeeName = cboEmployee.Text

            objTrainingBudget._DepaermentId = cboDepartment.SelectedValue
            objTrainingBudget._DepartmentName = cboDepartment.Text

            objTrainingBudget._InstituteId = cboInstitute.SelectedValue
            objTrainingBudget._InstituteName = cboInstitute.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objTrainingBudget._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            objTrainingBudget._AllocationTypeId = cboAllocations.SelectedValue
            objTrainingBudget._AllocationTypeName = cboAllocations.Text
            If lvAllocation.CheckedItems.Count > 0 Then
                objTrainingBudget._AllocationIds = String.Join(",", lvAllocation.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToArray)
                objTrainingBudget._AllocationNames = String.Join(",", lvAllocation.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.SubItems(colhAllocations.Index).Text).ToArray)
            End If
            'S.SANDEEP [08-FEB-2017] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJGrp As New clsJobGroup
                    dList = objJGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.GetList("List")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 8 Then
                colhAllocations.Width = 285 - 20
            Else
                colhAllocations.Width = 285
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean)
        Try
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Forms "

    Private Sub frmTrainingBudgetReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingBudget = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmTrainingBudgetReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingBudgetReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            eZeeHeader.Title = objTrainingBudget._ReportName
            eZeeHeader.Message = objTrainingBudget._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingBudgetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            'Nilay (18-Mar-2015) -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'If ConfigParameter._Object._ExportReportPath.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set the Export Report Path to export report."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Nilay (18-Mar-2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTrainingBudget.ExportTrainingBudget_Report()
            objTrainingBudget.ExportTrainingBudget_Report(FinancialYear._Object._DatabaseName, _
                                                          User._Object._Userunkid, _
                                                          FinancialYear._Object._YearUnkid, _
                                                          Company._Object._Companyunkid, _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          ConfigParameter._Object._UserAccessModeSetting, True)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTraining.DataSource
            frm.SelectedValue = cboTraining.SelectedValue
            frm.ValueMember = cboTraining.ValueMember
            frm.DisplayMember = cboTraining.DisplayMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboTraining.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.SelectedValue = cboEmployee.SelectedValue
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingBudgetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingBudgetReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
#Region " Combobox Event(s) "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Listview Events "

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState))
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [08-FEB-2017] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
			Me.lblTraining.Text = Language._Object.getCaption(Me.lblTraining.Name, Me.lblTraining.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please set the Export Report Path to export report.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
