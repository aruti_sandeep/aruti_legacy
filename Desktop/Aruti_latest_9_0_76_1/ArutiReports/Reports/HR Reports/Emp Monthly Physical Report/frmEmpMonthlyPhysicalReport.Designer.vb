﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpMonthlyPhysicalReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.cboMonth = New System.Windows.Forms.ComboBox
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 525)
        Me.NavPanel.Size = New System.Drawing.Size(784, 55)
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(201, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 16)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Month"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(8, 36)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(74, 16)
        Me.lblYear.TabIndex = 68
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(88, 34)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(107, 21)
        Me.cboYear.TabIndex = 67
        '
        'cboMonth
        '
        Me.cboMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMonth.FormattingEnabled = True
        Me.cboMonth.Location = New System.Drawing.Point(277, 34)
        Me.cboMonth.Name = "cboMonth"
        Me.cboMonth.Size = New System.Drawing.Size(139, 21)
        Me.cboMonth.TabIndex = 66
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkInactiveemp)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblYear)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboMonth)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboYear)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.Label2)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 67)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(430, 85)
        Me.EZeeCollapsibleContainer1.TabIndex = 70
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(88, 63)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(188, 16)
        Me.chkInactiveemp.TabIndex = 72
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'frmEmpMonthlyPhysicalReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 580)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpMonthlyPhysicalReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Monthly Physical Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.EZeeCollapsibleContainer1, 0)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboMonth As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class
