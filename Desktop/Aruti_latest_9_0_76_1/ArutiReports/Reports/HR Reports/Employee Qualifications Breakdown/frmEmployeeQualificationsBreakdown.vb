﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmEmployeeQualificationsBreakdown

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeQualificationsBreakdown"
    Private objEmployeeQualificationsBreakdown As clsEmployeeQualificationsBreakdown

#End Region

#Region " Constructor "

    Public Sub New()
        objEmployeeQualificationsBreakdown = New clsEmployeeQualificationsBreakdown(User._Object._Languageunkid, Company._Object._Companyunkid)
        objEmployeeQualificationsBreakdown.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjCMaster As New clsCommon_Master
        Dim ObjQMaster As New clsqualification_master
        Dim ObjBMaster As New clsStation
        Dim ObjDMaster As New clsDepartment
        Dim ObjJMaster As New clsJobs
        Dim ObjIMaster As New clsinstitute_master
        Dim dsList As New DataSet
        Try

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, True, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = ObjCMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QCAT")
            With cboQualifiyGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("QCAT")
                .SelectedValue = 0
            End With

            dsList = ObjQMaster.GetComboList("QMast", True)
            With cboQualifiy
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("QMast")
                .SelectedValue = 0
            End With


            dsList = ObjIMaster.getListForCombo(False, "Institute", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Institute")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing
            ObjCMaster = Nothing
            ObjQMaster = Nothing
            ObjBMaster = Nothing
            ObjDMaster = Nothing
            ObjJMaster = Nothing
            ObjIMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboInstitute.SelectedValue = 0
            cboQualifiy.SelectedValue = 0
            cboQualifiyGroup.SelectedValue = 0

            chkInactiveemp.Checked = False
            chkAppointmentDate.Checked = False

            objEmployeeQualificationsBreakdown.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmployeeQualificationsBreakdown.SetDefaultValue()

            objEmployeeQualificationsBreakdown._EmployeeId = cboEmployee.SelectedValue
            objEmployeeQualificationsBreakdown._EmployeeName = cboEmployee.Text

            objEmployeeQualificationsBreakdown._InstituteId = cboInstitute.SelectedValue
            objEmployeeQualificationsBreakdown._InstituteName = cboInstitute.Text

            objEmployeeQualificationsBreakdown._QualificationGrpId = cboQualifiyGroup.SelectedValue
            objEmployeeQualificationsBreakdown._QualificationGrpName = cboQualifiyGroup.Text

            objEmployeeQualificationsBreakdown._QualificationId = cboQualifiy.SelectedValue
            objEmployeeQualificationsBreakdown._QualificationName = cboQualifiy.Text

            objEmployeeQualificationsBreakdown._IsActive = CBool(chkInactiveemp.Checked)
            objEmployeeQualificationsBreakdown._IsAppointmentDate = CBool(chkAppointmentDate.Checked)
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmployeeQualificationsBreakdown_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeQualificationsBreakdown = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeQualificationsBreakdown_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeQualificationsBreakdown_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeQualificationsBreakdown_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeQualificationsBreakdown_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeQualificationsBreakdown_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeQualificationsBreakdown_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeQualificationsBreakdown_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeQualificationsBreakdown.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeQualificationsBreakdown"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub


            objEmployeeQualificationsBreakdown.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                    User._Object._Userunkid, _
                                                                    FinancialYear._Object._YearUnkid, _
                                                                    Company._Object._Companyunkid, _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                                    True, False, ConfigParameter._Object._ExportReportPath, _
                                                                    ConfigParameter._Object._OpenAfterExport, _
                                                                    0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
			Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
			Me.lblQualifyGroup.Text = Language._Object.getCaption(Me.lblQualifyGroup.Name, Me.lblQualifyGroup.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.chkAppointmentDate.Text = Language._Object.getCaption(Me.chkAppointmentDate.Name, Me.chkAppointmentDate.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class