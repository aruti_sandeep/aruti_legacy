'************************************************************************************************************************************
'Class Name : clsAssessmentCalibrationReport.vb
'Purpose    :
'Date       : 11 OCT 2019
'Written By : Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsAssessmentCalibrationReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAssessmentCalibrationReport"
    Private mstrReportId As String = enArutiReport.Assessment_Calibration_Report '215
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintReportViewTypeId As Integer = 0
    Private mstrReportViewName As String = ""
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmplyeeName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnApplyUserAccessFilter As Boolean = True
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintCalibrationStatusId As Integer = 0
    Private mstrCalibrationStatusName As String = String.Empty
    Private mDictRatingFrom As Dictionary(Of Integer, Decimal) = Nothing
    Private mDictRatingTo As Dictionary(Of Integer, Decimal) = Nothing
    Private mstrSelectedCols As String = String.Empty
    Private mstrSelectedJoin As String = String.Empty
    Private mstrDisplayCols As String = String.Empty
    Private mstrCalibrationUnkids As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportViewTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportViewTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportViewName() As String
        Set(ByVal value As String)
            mstrReportViewName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplyeeName() As String
        Set(ByVal value As String)
            mstrEmplyeeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _CalibrationStatusId() As Integer
        Set(ByVal value As Integer)
            mintCalibrationStatusId = value
        End Set
    End Property

    Public WriteOnly Property _CalibrationStatusName() As String
        Set(ByVal value As String)
            mstrCalibrationStatusName = value
        End Set
    End Property

    Public WriteOnly Property _mDictRatingFrom() As Dictionary(Of Integer, Decimal)
        Set(ByVal value As Dictionary(Of Integer, Decimal))
            mDictRatingFrom = value
        End Set
    End Property

    Public WriteOnly Property _mDictRatingTo() As Dictionary(Of Integer, Decimal)
        Set(ByVal value As Dictionary(Of Integer, Decimal))
            mDictRatingTo = value
        End Set
    End Property

    Public WriteOnly Property _SelectedCols() As String
        Set(ByVal value As String)
            mstrSelectedCols = value
        End Set
    End Property

    Public WriteOnly Property _SelectedJoin() As String
        Set(ByVal value As String)
            mstrSelectedJoin = value
        End Set
    End Property

    Public WriteOnly Property _DisplayCols() As String
        Set(ByVal value As String)
            mstrDisplayCols = value
        End Set
    End Property

    Public WriteOnly Property _CalibrationUnkids() As String
        Set(ByVal value As String)
            mstrCalibrationUnkids = value
        End Set
    End Property


#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportViewTypeId = 0
            mstrReportViewName = ""
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mintEmployeeId = 0
            mstrEmplyeeName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnApplyUserAccessFilter = True
            mstrAdvance_Filter = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mintCalibrationStatusId = 0
            mstrCalibrationStatusName = String.Empty
            mDictRatingFrom = Nothing
            mDictRatingTo = Nothing
            mstrSelectedCols = String.Empty
            mstrSelectedJoin = String.Empty
            mstrDisplayCols = String.Empty
            mstrCalibrationUnkids = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Sub Export_Calibratrion_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal strExportPath As String, _
                                           ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intYearUnkid <= 0 Then
                intYearUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            objDataOperation = New clsDataOperation
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            If mintReportViewTypeId = 0 Then    'NOT-CALIBRATED
                StrQ = "SELECT DISTINCT " & _
                       "     hremployee_master.employeecode AS [" & IIf(Language.getMessage(mstrModuleName, 100, "Code").Trim().Length <= 0, "Code", Language.getMessage(mstrModuleName, 100, "Code")) & "] " & _
                       "    ,firstname + ' ' + surname AS [" & IIf(Language.getMessage(mstrModuleName, 101, "Employee").Trim().Length <= 0, "Employee", Language.getMessage(mstrModuleName, 101, "Employee")) & "] " & _
                       "    ,F.finaloverallscore AS score "
                StrQ &= mstrSelectedCols & " "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM hrassess_compute_score_master " & _
                        "JOIN hremployee_master ON hrassess_compute_score_master.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        stationunkid " & _
                        "       ,deptgroupunkid " & _
                        "       ,departmentunkid " & _
                        "       ,sectiongroupunkid " & _
                        "       ,sectionunkid " & _
                        "       ,unitgroupunkid " & _
                        "       ,unitunkid " & _
                        "       ,teamunkid " & _
                        "       ,classgroupunkid " & _
                        "       ,classunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "   FROM hremployee_transfer_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "   FROM hremployee_categorization_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        employeeunkid " & _
                        "       ,gradegroupunkid " & _
                        "       ,gradeunkid " & _
                        "       ,gradelevelunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) as Rno " & _
                        "   FROM prsalaryincrement_tran " & _
                        "   WHERE isvoid = 0 AND isapproved = 1 " & _
                        "   AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        cctranheadvalueid AS costcenterunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "   FROM hremployee_cctranhead_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "   AND istransactionhead = 0 " & _
                        ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid ORDER BY finaloverallscore DESC) AS Rno " & _
                        "       ,finaloverallscore " & _
                        "   FROM hrassess_compute_score_master " & _
                        "   WHERE isvoid = 0 AND periodunkid = @periodunkid " & _
                        ") AS F ON F.employeeunkid = hrassess_compute_score_master.employeeunkid AND F.Rno = 1 "
                StrQ &= mstrSelectedJoin
                StrQ &= mstrAnalysis_Join

                'S.SANDEEP |12-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'S.SANDEEP |12-NOV-2019| -- END

                'S.SANDEEP |20-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
                'S.SANDEEP |20-NOV-2019| -- END


                StrQ &= "WHERE hrassess_compute_score_master.calibratnounkid <= 0 " & _
                        "AND hrassess_compute_score_master.isvoid = 0 " & _
                        "AND hrassess_compute_score_master.periodunkid = @periodunkid "

                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                End If

                'S.SANDEEP |20-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
                'S.SANDEEP |20-NOV-2019| -- END

                'S.SANDEEP |12-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If
                'S.SANDEEP |12-NOV-2019| -- END

            ElseIf mintReportViewTypeId = 1 Then    'CALIBRATED
                '"   ,cfuser_master.username AS [" & IIf(Language.getMessage(mstrModuleName, 104, "Calibrator").Trim().Length <= 0, "Calibrator", Language.getMessage(mstrModuleName, 104, "Calibrator")) & "] " & _
                StrQ = "SELECT DISTINCT " & _
                       "     hremployee_master.employeecode AS [" & IIf(Language.getMessage(mstrModuleName, 100, "Code").Trim().Length <= 0, "Code", Language.getMessage(mstrModuleName, 100, "Code")) & "] " & _
                       "    ,hremployee_master.firstname + ' ' + hremployee_master.surname AS [" & IIf(Language.getMessage(mstrModuleName, 101, "Employee").Trim().Length <= 0, "Employee", Language.getMessage(mstrModuleName, 101, "Employee")) & "] " & _
                       "    ,ISNULL(F.bscore,hrassess_computescore_approval_tran.calibrated_score)  AS score " & _
                       "    ,hrassess_computescore_approval_tran.calibratnounkid " & _
                       "    ,ISNULL(F.audituserunkid,hrassess_computescore_approval_tran.audituserunkid) AS audituserunkid "
                StrQ &= mstrSelectedCols & " "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "   ,ISNULL(PB.ProvisonalRating,'') AS [" & IIf(Language.getMessage(mstrModuleName, 109, "Prov. Rating").Trim().Length <= 0, "Prov. Rating", Language.getMessage(mstrModuleName, 109, "Prov. Rating")) & "] " & _
                        "   ,ISNULL((SELECT TOP 1 grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),'') AS [" & IIf(Language.getMessage(mstrModuleName, 110, "Calib. Rating").Trim().Length <= 0, "Calib. Rating", Language.getMessage(mstrModuleName, 110, "Calib. Rating")) & "] " & _
                        "   ,CASE WHEN hrassess_computescore_approval_tran.statusunkid = 0 THEN @NotSubmitted ELSE ISNULL(F.[iStatus], @Pending) END AS [" & IIf(Language.getMessage(mstrModuleName, 105, "Status").Trim().Length <= 0, "Status", Language.getMessage(mstrModuleName, 105, "Status")) & "] " & _
                        "   ,calibration_no AS [" & IIf(Language.getMessage(mstrModuleName, 103, "Calibration No").Trim().Length <= 0, "Calibration No", Language.getMessage(mstrModuleName, 103, "Calibration No")) & "] " & _
                        "FROM hrassess_computescore_approval_tran " & _
                        "    JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            A.employeeunkid " & _
                        "           ,A.calibratnounkid " & _
                        "       FROM " & _
                        "       ( " & _
                        "           SELECT " & _
                        "                calibratnounkid " & _
                        "               ,DENSE_RANK() OVER (PARTITION BY calibratnounkid,periodunkid ORDER BY statusunkid DESC) AS rno " & _
                        "               ,statusunkid " & _
                        "               ,employeeunkid " & _
                        "           FROM hrassess_computescore_approval_tran " & _
                        "           WHERE isvoid = 0 AND periodunkid = @periodunkid " & _
                        "       ) AS A " & _
                        "       WHERE A.rno = 1 AND A.statusunkid <> 3 " & _
                        "    ) AS P ON P.employeeunkid = hrassess_computescore_approval_tran.employeeunkid AND P.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            A.employeeunkid " & _
                        "           ,A.calibratnounkid " & _
                        "           ,A.periodunkid " & _
                        "           ,A.ProvisonalRating " & _
                        "       FROM " & _
                        "       ( " & _
                        "           SELECT " & _
                        "                employeeunkid " & _
                        "               ,calibratnounkid " & _
                        "               ,periodunkid " & _
                        "               ,CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                        "                       ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), '') END AS ProvisonalRating " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid ORDER BY finaloverallscore DESC) AS rno " & _
                        "           FROM hrassess_compute_score_master " & _
                        "           WHERE isvoid = 0 AND periodunkid = @periodunkid AND calibratnounkid > 0 " & _
                        "       ) AS A WHERE A.rno = 1 " & _
                        "    ) AS PB ON PB.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid AND PB.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                        "    AND PB.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                        "    JOIN hrmsConfiguration..cfuser_master ON hrassess_computescore_approval_tran.audituserunkid = hrmsConfiguration.dbo.cfuser_master.userunkid " & _
                        "    JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
                        "    JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "    LEFT JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON hrassess_computescore_approval_tran.[mappingunkid] = [HA].[mappingunkid] AND [HA].[iscalibrator] = 0 " & _
                        "    LEFT JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            hrassess_computescore_approval_tran.employeeunkid " & _
                        "           ,statusunkid " & _
                        "           ,DENSE_RANK() OVER (PARTITION BY hrassess_computescore_approval_tran.[employeeunkid],hrassess_computescore_approval_tran.periodunkid ORDER BY hrassess_computescore_approval_tran.[transactiondate] DESC) AS rno " & _
                        "           ,hrassess_computescore_approval_tran.calibratnounkid " & _
                        "           ,hrassess_computescore_approval_tran.calibrated_score AS bscore " & _
                        "           ,hrassess_computescore_approval_tran.calibration_remark AS bcalibration_remark " & _
                        "           ,hrassess_computescore_approval_tran.approvalremark AS bapprovalremark " & _
                        "           ,CASE WHEN hrassess_computescore_approval_tran.[statusunkid] = 1 THEN @Pending " & _
                        "                 WHEN hrassess_computescore_approval_tran.[statusunkid] = 2 THEN @Approved + ' : [ ' + CM.[username] + ' ]' " & _
                        "                 WHEN hrassess_computescore_approval_tran.[statusunkid] = 3 THEN @Reject + ' : [ ' + CM.[username] + ' ]' " & _
                        "            ELSE @PendingApproval END AS iStatus " & _
                        "           ,hrassess_computescore_approval_tran.periodunkid AS [iPeriodId] " & _
                        "           ,HM.priority AS [priority] " & _
                        "           ,hrassess_computescore_approval_tran.audituserunkid " & _
                        "       FROM hrassess_computescore_approval_tran " & _
                        "           JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON hrassess_computescore_approval_tran.[mappingunkid] = [HA].[mappingunkid] AND [HA].[iscalibrator] = 0 " & _
                        "           JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                        "           LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                        "       WHERE hrassess_computescore_approval_tran.isvoid = 0  AND HA.isvoid = 0 AND HA.isactive = 1 AND HM.isactive = 1 " & _
                        "       AND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
                        "    ) AS F ON F.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                        "    AND [F].[iPeriodId] = hrassess_computescore_approval_tran.[periodunkid] AND hrassess_computescore_approval_tran.calibratnounkid = [F].calibratnounkid AND F.rno = 1 " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "       FROM hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "       FROM hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            employeeunkid " & _
                        "           ,gradegroupunkid " & _
                        "           ,gradeunkid " & _
                        "           ,gradelevelunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) as Rno " & _
                        "       FROM prsalaryincrement_tran " & _
                        "       WHERE isvoid = 0 AND isapproved = 1 " & _
                        "       AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            cctranheadvalueid AS costcenterunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "       FROM hremployee_cctranhead_tran " & _
                        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "       AND istransactionhead = 0 " & _
                        "    ) AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

                'S.SANDEEP |18-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                '-------------------------- ADDED INFO -------------------------------------------
                '"   ,ISNULL(PB.ProvisonalRating,'') AS [" & IIf(Language.getMessage(mstrModuleName, 109, "Prov. Rating").Trim().Length <= 0, "Prov. Rating", Language.getMessage(mstrModuleName, 109, "Prov. Rating")) & "] " & _
                '"   ,ISNULL((SELECT TOP 1 grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),'') AS [" & IIf(Language.getMessage(mstrModuleName, 110, "Calib. Rating").Trim().Length <= 0, "Calib. Rating", Language.getMessage(mstrModuleName, 110, "Calib. Rating")) & "] " & _

                '"    JOIN " & _
                '        "    ( " & _
                '        "       SELECT " & _
                '        "            A.employeeunkid " & _
                '        "           ,A.calibratnounkid " & _
                '        "       FROM " & _
                '        "       ( " & _
                '        "           SELECT " & _
                '        "                calibratnounkid " & _
                '        "               ,DENSE_RANK() OVER (PARTITION BY calibratnounkid,periodunkid ORDER BY statusunkid DESC) AS rno " & _
                '        "               ,statusunkid " & _
                '        "               ,employeeunkid " & _
                '        "           FROM hrassess_computescore_approval_tran " & _
                '        "           WHERE isvoid = 0 AND periodunkid = @periodunkid " & _
                '        "       ) AS A " & _
                '        "       WHERE A.rno = 1 AND A.statusunkid <> 3 " & _
                '        "    ) AS P ON P.employeeunkid = hrassess_computescore_approval_tran.employeeunkid AND P.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                '        "    LEFT JOIN " & _
                '        "    ( " & _
                '        "       SELECT " & _
                '        "            A.employeeunkid " & _
                '        "           ,A.calibratnounkid " & _
                '        "           ,A.periodunkid " & _
                '        "           ,A.ProvisonalRating " & _
                '        "       FROM " & _
                '        "       ( " & _
                '        "           SELECT " & _
                '        "                employeeunkid " & _
                '        "               ,calibratnounkid " & _
                '        "               ,periodunkid " & _
                '        "               ,CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                '        "                       ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), '') END AS ProvisonalRating " & _
                '        "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid ORDER BY finaloverallscore DESC) AS rno " & _
                '        "           FROM hrassess_compute_score_master " & _
                '        "           WHERE isvoid = 0 AND periodunkid = @periodunkid AND calibratnounkid > 0 " & _
                '        "       ) AS A WHERE A.rno = 1 " & _
                '        "    ) AS PB ON PB.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid AND PB.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                '        "    AND PB.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _

                'S.SANDEEP |18-NOV-2019| -- END

                StrQ &= mstrSelectedJoin
                StrQ &= mstrAnalysis_Join
                'S.SANDEEP |12-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'S.SANDEEP |12-NOV-2019| -- END

                'S.SANDEEP |20-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
                'S.SANDEEP |20-NOV-2019| -- END

                StrQ &= "WHERE hrassess_computescore_approval_tran.isvoid = 0 " & _
                        "AND hrassess_computescore_approval_tran.periodunkid = @periodunkid "

                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                End If

                If mintCalibrationStatusId > 0 Then
                    StrQ &= " AND F.statusunkid = @statusunkid "
                    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalibrationStatusId)
                End If

                'S.SANDEEP |20-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
                'S.SANDEEP |20-NOV-2019| -- END

                'S.SANDEEP |12-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If
                'S.SANDEEP |12-NOV-2019| -- END

            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 103, "Pending for Approval"))
            objDataOperation.AddParameter("@NotSubmitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 106, "Not Submitted"))

            Dim dtFinalData As DataTable = Nothing
            If mDictRatingFrom IsNot Nothing AndAlso mDictRatingTo IsNot Nothing Then
                If mDictRatingFrom.Count > 0 AndAlso mDictRatingTo.Count > 0 Then

                    Dim StrCondition As String = String.Empty
                    If mintReportViewTypeId = 0 Then        'NOT-CALIBRATED
                        StrCondition = " AND F.finaloverallscore >= #SCRF# AND F.finaloverallscore <= #SCRT# "
                    ElseIf mintReportViewTypeId = 1 Then    'CALIBRATED
                        StrCondition = " AND hrassess_computescore_approval_tran.calibrated_score >= #SCRF# AND hrassess_computescore_approval_tran.calibrated_score <= #SCRT# "
                    End If
                    Dim strOrignalQ As String = StrQ
                    For Each iKey As Integer In mDictRatingFrom.Keys
                        strOrignalQ &= StrCondition
                        StrQ = strOrignalQ
                        StrQ = StrQ.Replace("#SCRF#", mDictRatingFrom(iKey))
                        StrQ = StrQ.Replace("#SCRT#", mDictRatingTo(iKey))

                        dsList = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        End If

                        If dtFinalData IsNot Nothing Then
                            dtFinalData.Merge(dsList.Tables(0).Copy(), True)
                        Else
                            dtFinalData = dsList.Tables(0).Copy
                        End If
                    Next
                End If
            Else
                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                dtFinalData = dsList.Tables(0).Copy
            End If

            If mintReportViewTypeId = 1 Then    'CALIBRATED
                Dim objSApproval As New clsScoreCalibrationApproval
                Dim dsCalibrator As New DataTable
                dsCalibrator = objSApproval.GetUserAllocation(mintPeriodId, intCompanyUnkid, objDataOperation)
                objSApproval = Nothing
                dtFinalData.AsEnumerable().ToList().ForEach(Function(x) UpdateCalibration(x, dsCalibrator))
            End If

            If mintReportViewTypeId = 0 Then        'NOT-CALIBRATED
                If mintViewIndex > 0 Then
                    dtFinalData = New DataView(dtFinalData, "", "GName", DataViewRowState.CurrentRows).ToTable()
                End If
            ElseIf mintReportViewTypeId = 1 Then    'CALIBRATED
                If mintViewIndex > 0 Then
                    dtFinalData = New DataView(dtFinalData, "", "calibratnounkid,GName", DataViewRowState.CurrentRows).ToTable()
                Else
                    dtFinalData = New DataView(dtFinalData, "", "calibratnounkid", DataViewRowState.CurrentRows).ToTable()
                End If
            End If


            If dtFinalData.Columns.Contains("score") Then
                dtFinalData.Columns.Remove("score")
            End If
            If dtFinalData.Columns.Contains("calibratnounkid") Then
                dtFinalData.Columns.Remove("calibratnounkid")
            End If
            If dtFinalData.Columns.Contains("audituserunkid") Then
                dtFinalData.Columns.Remove("audituserunkid")
            End If

            Dim str_NumCaption As String = IIf(Language.getMessage(mstrModuleName, 103, "Calibration No").Trim().Length <= 0, "Calibration No", Language.getMessage(mstrModuleName, 103, "Calibration No"))
            Dim strarrGroupColumns As String() = Nothing
            If mintViewIndex > 0 Then
                If mintReportViewTypeId = 0 Then        'NOT-CALIBRATED
                    If dtFinalData.Columns.Contains("Id") Then
                        dtFinalData.Columns.Remove("Id")
                    End If
                    dtFinalData.Columns("GName").Caption = mstrReport_GroupName
                    Dim strGrpCols As String() = {"GName"}
                    strarrGroupColumns = strGrpCols
                ElseIf mintReportViewTypeId = 1 Then    'CALIBRATED
                    If dtFinalData.Columns.Contains("Id") Then
                        dtFinalData.Columns.Remove("Id")
                    End If
                    dtFinalData.Columns("GName").Caption = mstrReport_GroupName
                    Dim strGrpCols As String() = {"GName", str_NumCaption}
                    strarrGroupColumns = strGrpCols
                End If
            Else
                If dtFinalData.Columns.Contains("Id") Then
                    dtFinalData.Columns.Remove("Id")
                End If
                If dtFinalData.Columns.Contains("GName") Then
                    dtFinalData.Columns.Remove("GName")
                End If
                If mintReportViewTypeId = 1 Then    'CALIBRATED
                    Dim strGrpCols As String() = {str_NumCaption}
                    strarrGroupColumns = strGrpCols
                End If
            End If

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtFinalData.Columns.Count - 1)
            'S.SANDEEP |12-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'For i As Integer = 0 To intArrayColumnWidth.Length - 2
            '    intArrayColumnWidth(i) = 125
            'Next
            If mintReportViewTypeId = 0 Then
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 125
                Next
            ElseIf mintReportViewTypeId = 1 Then
                For i As Integer = 0 To intArrayColumnWidth.Length - 2
                    intArrayColumnWidth(i) = 125
                Next
            End If
            'S.SANDEEP |12-NOV-2019| -- END
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dtFinalData, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " - (" & mstrReportViewName & ")", "", " ", , "", False, Nothing, Nothing, Nothing, Nothing, False)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function UpdateCalibration(ByVal x As DataRow, ByVal dt As DataTable, Optional ByVal blnIsStatusReport As Boolean = False) As Boolean
        Try
            Dim dr() As DataRow = Nothing
            If blnIsStatusReport = False Then
                dr = dt.Select("calibratnounkid = " & CInt(x.Item("calibratnounkid")) & " AND audituserunkid = " & CInt(x.Item("audituserunkid")))
                Dim str_NumCaption As String = String.Empty
                str_NumCaption = IIf(Language.getMessage(mstrModuleName, 103, "Calibration No").Trim().Length <= 0, "Calibration No", Language.getMessage(mstrModuleName, 103, "Calibration No"))
                If dr.Length > 0 Then
                    If dr(0)("allocation").ToString().Length > 0 Then
                        x.Item(str_NumCaption) = x.Item(str_NumCaption) & " - " & dr(0)("username").ToString() & "(" & dr(0)("allocation").ToString() & ")"
                    Else
                        x.Item(str_NumCaption) = x.Item(str_NumCaption) & " - " & dr(0)("username").ToString()
                    End If
                End If
            Else
                dr = dt.Select("calibratnounkid = " & CInt(x.Item("calibratnounkid")) & " AND audituserunkid = " & CInt(x.Item("audituserunkid")))
                If dr.Length > 0 Then
                    x.Item("allocation") = dr(0)("allocation").ToString()
                    x.Item("calibuser") = dr(0)("username").ToString()
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateCalibration; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Function UpdateRowValue(ByVal x As DataRow, ByVal strCol As String, ByVal strValue As String) As Boolean
        Try
            x(strCol) = strValue
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    'S.SANDEEP |15-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Private Function UpdateRowValue(ByVal x As DataRow, ByVal strCol As String, ByVal strValue As String, ByVal intPriorityId As Integer, ByVal iTab As DataTable) As Boolean
        Try
            Dim iRow = iTab.Select("calibratnounkid = '" & x("calibratnounkid") & "' AND employeeunkid = '" & x("employeeunkid") & "' AND priority = '" & intPriorityId & "' ")
            If iRow.Length > 0 Then
                If iRow(0)("iValue") = 1 Then
                    x(strCol) = strValue
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP |15-NOV-2019| -- END


    'S.SANDEEP |27-AUG-2021| -- START
    'Public Sub Export_CalibrationStatus_Report(ByVal strDatabaseName As String _
    '                                         , ByVal intCompanyId As Integer _
    '                                         , ByVal intYearId As Integer _
    '                                         , ByVal strUserAccessMode As String _
    '                                         , ByVal intPrivilegeId As Integer _
    '                                         , ByVal xEmployeeAsOnDate As String _
    '                                         , ByVal intUserId As Integer _
    '                                         , ByVal blnOnlyMyApproval As Boolean _
    '                                         , ByVal strExportPath As String _
    '                                         , ByVal blnOpenAfterExport As Boolean)
    '    Dim StrQ As String = String.Empty
    '    Dim dtList As DataTable = Nothing
    '    objDataOperation = New clsDataOperation
    '    Try

    '        Company._Object._Companyunkid = intCompanyId
    '        ConfigParameter._Object._Companyunkid = intCompanyId
    '        User._Object._Userunkid = intUserId

    '        Dim dsFinal As New DataSet
    '        'S.SANDEEP |20-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        'Dim strUACJoin, strUACFilter, xAdvanceJoinQry As String
    '        Dim xDateJoinQry, xDateFilterQry, strUACJoin, strUACFilter, xAdvanceJoinQry As String
    '        'S.SANDEEP |20-NOV-2019| -- END
    '        strUACJoin = "" : strUACFilter = "" : xAdvanceJoinQry = ""
    '        'S.SANDEEP |20-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        xDateJoinQry = "" : xDateFilterQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(xEmployeeAsOnDate), eZeeDate.convertDate(xEmployeeAsOnDate), , , strDatabaseName, "AEM")
    '        'S.SANDEEP |20-NOV-2019| -- END

    '        NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM")
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName, "AEM")

    '        'StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
    '        '       "DROP TABLE #USR "
    '        'StrQ &= "SELECT " & _
    '        '        "* " & _
    '        '        "INTO #USR " & _
    '        '        "FROM " & _
    '        '        "( "

    '        'If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        'Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
    '        'For index As Integer = 0 To strvalues.Length - 1
    '        '    Dim xStrJoinColName As String = ""
    '        '    Dim xIntAllocId As Integer = 0
    '        '    Select Case CInt(strvalues(index))
    '        '        Case enAllocation.BRANCH
    '        '            xStrJoinColName = "stationunkid"
    '        '            xIntAllocId = CInt(enAllocation.BRANCH)
    '        '        Case enAllocation.DEPARTMENT_GROUP
    '        '            xStrJoinColName = "deptgroupunkid"
    '        '            xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
    '        '        Case enAllocation.DEPARTMENT
    '        '            xStrJoinColName = "departmentunkid"
    '        '            xIntAllocId = CInt(enAllocation.DEPARTMENT)
    '        '        Case enAllocation.SECTION_GROUP
    '        '            xStrJoinColName = "sectiongroupunkid"
    '        '            xIntAllocId = CInt(enAllocation.SECTION_GROUP)
    '        '        Case enAllocation.SECTION
    '        '            xStrJoinColName = "sectionunkid"
    '        '            xIntAllocId = CInt(enAllocation.SECTION)
    '        '        Case enAllocation.UNIT_GROUP
    '        '            xStrJoinColName = "unitgroupunkid"
    '        '            xIntAllocId = CInt(enAllocation.UNIT_GROUP)
    '        '        Case enAllocation.UNIT
    '        '            xStrJoinColName = "unitunkid"
    '        '            xIntAllocId = CInt(enAllocation.UNIT)
    '        '        Case enAllocation.TEAM
    '        '            xStrJoinColName = "teamunkid"
    '        '            xIntAllocId = CInt(enAllocation.TEAM)
    '        '        Case enAllocation.JOB_GROUP
    '        '            xStrJoinColName = "jobgroupunkid"
    '        '            xIntAllocId = CInt(enAllocation.JOB_GROUP)
    '        '        Case enAllocation.JOBS
    '        '            xStrJoinColName = "jobunkid"
    '        '            xIntAllocId = CInt(enAllocation.JOBS)
    '        '        Case enAllocation.CLASS_GROUP
    '        '            xStrJoinColName = "classgroupunkid"
    '        '            xIntAllocId = CInt(enAllocation.CLASS_GROUP)
    '        '        Case enAllocation.CLASSES
    '        '            xStrJoinColName = "classunkid"
    '        '            xIntAllocId = CInt(enAllocation.CLASSES)
    '        '    End Select
    '        '    StrQ &= "SELECT " & _
    '        '            "    B" & index.ToString() & ".userunkid " & _
    '        '            "   ,A.employeeunkid " & _
    '        '            "FROM " & _
    '        '            "( " & _
    '        '            "   SELECT " & _
    '        '            "        AEM.employeeunkid " & _
    '        '            "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
    '        '            "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
    '        '            "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
    '        '            "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
    '        '            "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
    '        '            "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
    '        '            "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
    '        '            "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
    '        '            "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
    '        '            "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
    '        '            "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
    '        '            "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
    '        '            "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
    '        '            "   JOIN " & strDatabaseName & "..hrassess_computescore_approval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid " & _
    '        '            "   LEFT JOIN " & _
    '        '            "   ( " & _
    '        '            "       SELECT " & _
    '        '            "            stationunkid " & _
    '        '            "           ,deptgroupunkid " & _
    '        '            "           ,departmentunkid " & _
    '        '            "           ,sectiongroupunkid " & _
    '        '            "           ,sectionunkid " & _
    '        '            "           ,unitgroupunkid " & _
    '        '            "           ,unitunkid " & _
    '        '            "           ,teamunkid " & _
    '        '            "           ,classgroupunkid " & _
    '        '            "           ,classunkid " & _
    '        '            "           ,employeeunkid " & _
    '        '            "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '        '            "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
    '        '            "       WHERE isvoid = 0 " & _
    '        '            "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '        '            "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
    '        '            "   AND T.Rno = 1 " & _
    '        '            "   LEFT JOIN " & _
    '        '            "   ( " & _
    '        '            "       SELECT " & _
    '        '            "            jobgroupunkid " & _
    '        '            "           ,jobunkid " & _
    '        '            "           ,employeeunkid " & _
    '        '            "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '        '            "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
    '        '            "       WHERE isvoid = 0 " & _
    '        '            "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '        '            "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
    '        '            "   AND J.Rno = 1 " & _
    '        '            "   WHERE TAT.isvoid = 0 " & _
    '        '            ") AS A " & _
    '        '            "JOIN " & _
    '        '            "( " & _
    '        '            "   SELECT " & _
    '        '            "        UPM.userunkid " & _
    '        '            "       ,UPT.allocationunkid " & _
    '        '            "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '            "       JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid " & _
    '        '            "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '            "   WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
    '        '            "   AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
    '        '            ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
    '        '    If index < strvalues.Length - 1 Then
    '        '        StrQ &= " INTERSECT "
    '        '    End If
    '        'Next

    '        'StrQ &= ") AS Fl "

    '        StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
    '               "DROP TABLE #USR "
    '        StrQ &= "SELECT " & _
    '                "* " & _
    '                "INTO #USR " & _
    '                "FROM " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                "        CT.employeeunkid " & _
    '                "       ,AC.mapuserunkid as userunkid " & _
    '                "   FROM hrscore_calibration_approver_master AS AC " & _
    '                "       JOIN hrmsConfiguration..cfuser_master AS UM ON AC.mapuserunkid = UM.userunkid " & _
    '                "       JOIN hrscore_calibration_approverlevel_master AS AL ON AC.levelunkid = AL.levelunkid " & _
    '                "       JOIN hrscore_calibration_approver_tran AS CT ON AC.mappingunkid = CT.mappingunkid " & _
    '                "   WHERE AC.iscalibrator = 0 AND AC.isvoid = 0 AND CT.isvoid = 0 " & _
    '                "       AND AC.visibletypeid = 1 AND CT.visibletypeid = 1 " & _
    '                "       AND AC.isactive = 1 " & _
    '                ") AS Fl "

    '        StrQ &= "DECLARE @emp AS TABLE (empid INT,deptid INT,jobid INT,clsgrpid INT,clsid INT,branchid INT,deptgrpid INT,secgrpid INT,secid INT,unitgrpid INT,unitid INT,teamid INT,jgrpid INT,periodid INT,ecodename nvarchar(max),ename nvarchar(max),calibratnounkid INT,Code nvarchar(255), JobTitle nvarchar(max)) " & _
    '                "INSERT INTO @emp (empid, deptid, jobid, clsgrpid, clsid, branchid, deptgrpid, secgrpid, secid, unitgrpid, unitid, teamid, jgrpid, periodid, ecodename, ename,calibratnounkid,Code,JobTitle) " & _
    '                "SELECT " & _
    '                "     AEM.employeeunkid " & _
    '                "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
    '                "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
    '                "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
    '                "    ,ISNULL(T.classunkid,0) AS classunkid " & _
    '                "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
    '                "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
    '                "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
    '                "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
    '                "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
    '                "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
    '                "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
    '                "    ,TAT.periodunkid " & _
    '                "    ,AEM.employeecode + ' - ' + AEM.firstname + ' ' + AEM.surname " & _
    '                "    ,AEM.firstname + ' ' + AEM.surname " & _
    '                "    ,TAT.calibratnounkid " & _
    '                "    ,AEM.employeecode " & _
    '                "    ,EJOB.job_name AS JobTitle " & _
    '                "FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
    '                "    JOIN hrassess_computescore_approval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid "
    '        If strUACJoin.Trim.Length > 0 AndAlso intUserId > 0 Then
    '            StrQ &= strUACJoin
    '        End If
    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If
    '        'S.SANDEEP |20-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If
    '        'S.SANDEEP |20-NOV-2019| -- END
    '        StrQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        stationunkid " & _
    '                "       ,deptgroupunkid " & _
    '                "       ,departmentunkid " & _
    '                "       ,sectiongroupunkid " & _
    '                "       ,sectionunkid " & _
    '                "       ,unitgroupunkid " & _
    '                "       ,unitunkid " & _
    '                "       ,teamunkid " & _
    '                "       ,classgroupunkid " & _
    '                "       ,classunkid " & _
    '                "       ,employeeunkid " & _
    '                "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                "   FROM hremployee_transfer_tran " & _
    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
    '        If mintEmployeeId > 0 Then StrQ &= " AND employeeunkid IN (" & mintEmployeeId.ToString() & ") "
    '        StrQ &= ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        jobgroupunkid " & _
    '                "       ,jobunkid " & _
    '                "       ,employeeunkid " & _
    '                "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                "   FROM hremployee_categorization_tran " & _
    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
    '                "LEFT JOIN hrjob_master AS EJOB ON EJOB.jobunkid = J.jobunkid "
    '        StrQ &= "WHERE 1 = 1 AND TAT.isvoid = 0 AND TAT.statusunkid IN (0,1) AND  [TAT].periodunkid = @periodunkid "
    '        If blnOnlyMyApproval Then
    '            StrQ &= " AND ISNULL([TAT].[isprocessed],0) = 0 "
    '        End If
    '        If mintEmployeeId > 0 Then StrQ &= " AND AEM.employeeunkid IN (" & mintEmployeeId.ToString() & ") "

    '        If mstrCalibrationUnkids.Trim.Length > 0 Then
    '            StrQ &= " AND [TAT].calibratnounkid IN (" & mstrCalibrationUnkids & ") "
    '        End If

    '        'S.SANDEEP |12-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'S.SANDEEP |12-NOV-2019| -- END

    '        'S.SANDEEP |20-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        If xDateFilterQry.Trim.Length > 0 Then
    '            StrQ &= xDateFilterQry
    '        End If
    '        'S.SANDEEP |20-NOV-2019| -- END

    '        StrQ &= "SELECT DISTINCT " & _
    '                "    CAST(0 AS BIT) AS iCheck " & _
    '                "   ,iData.calibration_no " & _
    '                "   ,iData.Code " & _
    '                "   ,iData.Employee " & _
    '                "   ,iData.DATE " & _
    '                "   ,ISNULL(iData.povisionscore,0) AS povisionscore " & _
    '                "   ,ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) AS calibratescore " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.Date) ELSE NULL END AS transactiondate " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iValue], 0) ELSE 1 END AS iValue " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN Fn.userunkid ELSE 0 END AS userunkid " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
    '                "   ,[@emp].empid AS employeeunkid " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN Fn.uempid END AS uempid " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN Fn.ecompid END AS ecompid " & _
    '                "   ,[@emp].ecodename AS oemployee " & _
    '                "   ,[@emp].ename AS employee_name " & _
    '                "   ,iData.periodunkid " & _
    '                "   ,iData.statusunkid " & _
    '                "   ,iData.tranguid " & _
    '                "   ,iData.calibratnounkid " & _
    '                "   ,CASE WHEN ISNULL(bapprovalremark,'') = '' THEN iData.approvalremark ELSE ISNULL(bapprovalremark,'') END AS approvalremark " & _
    '                "   ,iData.isfinal " & _
    '                "   ,iData.isprocessed " & _
    '                "   ,iData.isgrp " & _
    '                "   ,iData.grpid " & _
    '                "   ,CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.periodunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno " & _
    '                "   ,[@emp].Code " & _
    '                "   ,[@emp].JobTitle " & _
    '                "   ,CAST(1 AS BIT) AS isChanged " & _
    '                "   ,ISNULL(NV.calibration_remark,ISNULL(iData.calibration_remark,ISNULL(iData.c_remark,''))) AS calibration_remark " & _
    '                "   ,iData.submitdate " & _
    '                "   ,iData.c_remark " & _
    '                "   ,iData.o_remark " & _
    '                "   ,iData.s_date " & _
    '                "   ,'' AS lstpRating " & _
    '                "   ,'' AS lstcRating " & _
    '                "   ,ISNULL(acal.acrating,'') AS acrating " & _
    '                "   ,ISNULL(acal.acremark,'') AS acremark " & _
    '                "   ,ISNULL(B.lstcRating,'') AS lstapRating " & _
    '                "   ,ISNULL(bcalibration_remark,'') AS apcalibremark " & _
    '                "   ,'' AS allocation " & _
    '                "   ,audituserunkid " & _
    '                "   ,'' AS calibuser " & _
    '                "   ,Fn.email "
    '        StrQ &= mstrSelectedCols & " "
    '        StrQ &= "FROM @emp " & _
    '                "   JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
    '                "   JOIN hremployee_master AS EMPL ON [@emp].empid = EMPL.employeeunkid AND EMPL.employeeunkid = #USR.employeeunkid " & _
    '                "   LEFT JOIN " & _
    '                "   ( " & _
    '                "      SELECT " & _
    '                "           stationunkid " & _
    '                "          ,deptgroupunkid " & _
    '                "          ,departmentunkid " & _
    '                "          ,sectiongroupunkid " & _
    '                "          ,sectionunkid " & _
    '                "          ,unitgroupunkid " & _
    '                "          ,unitunkid " & _
    '                "          ,teamunkid " & _
    '                "          ,classgroupunkid " & _
    '                "          ,classunkid " & _
    '                "          ,employeeunkid " & _
    '                "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
    '                "      FROM hremployee_transfer_tran " & _
    '                "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' " & _
    '                "   ) AS T ON T.employeeunkid = EMPL.employeeunkid AND T.Rno = 1 " & _
    '                "   LEFT JOIN " & _
    '                "   ( " & _
    '                "      SELECT " & _
    '                "           jobgroupunkid " & _
    '                "          ,jobunkid " & _
    '                "          ,employeeunkid " & _
    '                "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
    '                "      FROM hremployee_categorization_tran " & _
    '                "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' " & _
    '                "   ) AS J ON J.employeeunkid = EMPL.employeeunkid AND J.Rno = 1 " & _
    '                "   LEFT JOIN " & _
    '                "   ( " & _
    '                "      SELECT " & _
    '                "           employeeunkid " & _
    '                "          ,gradegroupunkid " & _
    '                "          ,gradeunkid " & _
    '                "          ,gradelevelunkid " & _
    '                "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) as Rno " & _
    '                "      FROM prsalaryincrement_tran " & _
    '                "      WHERE isvoid = 0 AND isapproved = 1 " & _
    '                "      AND CONVERT(CHAR(8),incrementdate,112) <= '" & xEmployeeAsOnDate & "' " & _
    '                "   ) AS G ON G.employeeunkid = EMPL.employeeunkid AND G.Rno = 1 " & _
    '                "   LEFT JOIN " & _
    '                "   ( " & _
    '                "      SELECT " & _
    '                "           cctranheadvalueid AS costcenterunkid " & _
    '                "          ,employeeunkid " & _
    '                "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
    '                "      FROM hremployee_cctranhead_tran " & _
    '                "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' " & _
    '                "      AND istransactionhead = 0 " & _
    '                "   ) AS C ON C.employeeunkid = EMPL.employeeunkid AND C.Rno = 1 "
    '        StrQ &= mstrSelectedJoin
    '        StrQ &= "   JOIN " & _
    '                "       ( " & _
    '                "           SELECT " & _
    '                "                 a.calibration_no " & _
    '                "                ,a.Code " & _
    '                "                ,a.Employee " & _
    '                "                ,a.DATE " & _
    '                "                ,a.povisionscore " & _
    '                "                ,a.calibratescore " & _
    '                "                ,a.isgrp " & _
    '                "                ,a.grpid " & _
    '                "                ,a.employeeunkid " & _
    '                "                ,a.periodunkid " & _
    '                "                ,a.statusunkid " & _
    '                "                ,a.tranguid " & _
    '                "                ,a.calibratnounkid " & _
    '                "                ,a.mappingunkid " & _
    '                "                ,a.approvalremark " & _
    '                "                ,a.isfinal " & _
    '                "                ,a.isprocessed " & _
    '                "                ,a.calibration_remark " & _
    '                "                ,a.submitdate " & _
    '                "                ,a.c_remark " & _
    '                "                ,a.o_remark " & _
    '                "                ,a.s_date " & _
    '                "                ,a.audituserunkid " & _
    '                "           FROM " & _
    '                "           ( " & _
    '                "               SELECT " & _
    '                "                    calibration_no " & _
    '                "                   ,employeecode AS Code " & _
    '                "                   ,firstname + ' ' + surname AS Employee " & _
    '                "                   ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
    '                "                   ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
    '                "                   ,ISNULL(pScore.c_remark, ISNULL(rScore.c_remark,'')) AS c_remark " & _
    '                "                   ,ISNULL(pScore.o_remark, ISNULL(rScore.o_remark,'')) AS o_remark " & _
    '                "                   ,CAST(ISNULL(pScore.finaloverallscore,rScore.finaloverallscore) AS DECIMAL(36, 2)) AS povisionscore " & _
    '                "                   ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
    '                "                   ,CAST(0 AS BIT) AS isgrp " & _
    '                "                   ,hrassess_calibration_number.calibratnounkid AS grpid " & _
    '                "                   ,hrassess_computescore_approval_tran.employeeunkid " & _
    '                "                   ,hrassess_computescore_approval_tran.periodunkid " & _
    '                "                   ,hrassess_computescore_approval_tran.statusunkid " & _
    '                "                   ,hrassess_computescore_approval_tran.tranguid " & _
    '                "                   ,hrassess_computescore_approval_tran.calibratnounkid " & _
    '                "                   ,hrassess_computescore_approval_tran.mappingunkid " & _
    '                "                   ,hrassess_computescore_approval_tran.approvalremark " & _
    '                "                   ,hrassess_computescore_approval_tran.isfinal " & _
    '                "                   ,hrassess_computescore_approval_tran.isprocessed " & _
    '                "                   ,hrassess_computescore_approval_tran.calibration_remark " & _
    '                "                   ,pScore.submit_date AS s_date " & _
    '                "                   ,hrassess_computescore_approval_tran.audituserunkid " & _
    '                "               FROM hrassess_computescore_approval_tran " & _
    '                "               LEFT JOIN " & _
    '                "               ( " & _
    '                "                   SELECT DISTINCT " & _
    '                "                        hrassess_computescore_rejection_tran.employeeunkid " & _
    '                "                       ,hrassess_computescore_rejection_tran.periodunkid " & _
    '                "                       ,hrassess_computescore_rejection_tran.calibratnounkid " & _
    '                "                       ,hrassess_computescore_rejection_tran.finaloverallscore " & _
    '                "                       ,hrassess_computescore_rejection_tran.calibration_remark AS c_remark " & _
    '                "                       ,hrassess_computescore_rejection_tran.overall_remark AS o_remark " & _
    '                "                   FROM hrassess_computescore_rejection_tran " & _
    '                "                   WHERE hrassess_computescore_rejection_tran.periodunkid = @periodunkid " & _
    '                "               ) AS rScore ON rScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
    '                "                   AND rScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
    '                "                   AND rScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
    '                "               JOIN " & _
    '                "               ( " & _
    '                "                   SELECT DISTINCT " & _
    '                "                        calibratnounkid " & _
    '                "                       ,employeeunkid " & _
    '                "                       ,periodunkid " & _
    '                "                       ,finaloverallscore " & _
    '                "                       ,submit_date " & _
    '                "                       ,calibration_remark AS c_remark " & _
    '                "                       ,overall_remark AS o_remark " & _
    '                "                   FROM hrassess_compute_score_master " & _
    '                "                   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
    '                "               ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
    '                "               AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
    '                "               JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
    '                "               JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "           WHERE statusunkid IN (0,1) AND isvoid = 0 aND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
    '                "       ) AS A WHERE 1 = 1 AND A.statusunkid IN (0,1) " & _
    '                "   ) AS iData ON iData.employeeunkid = [@emp].empid " & _
    '                "   JOIN " & _
    '                "   ( " & _
    '                "        SELECT DISTINCT " & _
    '                "             cfuser_master.userunkid " & _
    '                "            ,cfuser_master.username " & _
    '                "            ,cfuser_master.email " & _
    '                "            ,LM.priority " & _
    '                "            ,LM.levelname AS LvName " & _
    '                "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
    '                "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
    '                "            ,EM.mapuserunkid " & _
    '                "            ,EM.mappingunkid AS oMappingId " & _
    '                "        FROM hrmsConfiguration..cfuser_master " & _
    '                "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '                "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '                "        JOIN " & strDatabaseName & "..hrscore_calibration_approver_master EM ON EM.mapuserunkid = cfuser_master.userunkid AND [EM].[iscalibrator] = 0 " & _
    '                "        JOIN " & strDatabaseName & "..hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
    '                "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
    '        StrQ &= "     AND yearunkid = @Y " & _
    '                "     AND privilegeunkid = @P " & _
    '                " ) AS Fn ON #USR.userunkid = Fn.userunkid  " & _
    '                " LEFT JOIN " & _
    '                " ( " & _
    '                "   SELECT " & _
    '                "        [HA].[mapuserunkid] " & _
    '                "       ,[HM].[priority] " & _
    '                "       ,[HTAT].[statusunkid] AS iStatusId " & _
    '                "       ,[HTAT].[transactiondate] " & _
    '                "       ,[HTAT].[mappingunkid] " & _
    '                "       ,[HTAT].[periodunkid] AS iPeriodId " & _
    '                "       ,[HTAT].calibratnounkid " & _
    '                "       ,[HTAT].calibrated_score AS bscore " & _
    '                "       ,[HTAT].calibration_remark AS bcalibration_remark " & _
    '                "       ,[HTAT].approvalremark AS bapprovalremark " & _
    '                "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND [HTAT].calibrated_score >= score_from AND [HTAT].calibrated_score <= score_to) AS lstcRating " & _
    '                "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
    '                "             WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ ' + CM.[username] + ' ]' " & _
    '                "             WHEN [HTAT].[statusunkid] = 3 THEN @Reject + ' : [ ' + CM.[username] + ' ]' " & _
    '                "        ELSE @PendingApproval END AS iStatus " & _
    '                "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN 0 " & _
    '                "             WHEN [HTAT].[statusunkid] = 2 THEN 1 " & _
    '                "             WHEN [HTAT].[statusunkid] = 3 THEN 1 " & _
    '                "        ELSE 0 END AS iValue " & _
    '                "       ,DENSE_RANK() OVER (PARTITION BY [HTAT].[employeeunkid],[HTAT].periodunkid ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
    '                "       ,HTAT.[employeeunkid] " & _
    '                "   FROM hrassess_computescore_approval_tran AS HTAT " & _
    '                "       JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] AND [HA].[iscalibrator] = 0 " & _
    '                "       JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
    '                "       LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
    '                "   WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
    '                "   AND HTAT.periodunkid = @periodunkid "
    '        'S.SANDEEP |15-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        '---------- ADDED
    '        '"       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN 0 WHEN [HTAT].[statusunkid] = 2 THEN 1 WHEN [HTAT].[statusunkid] = 3 THEN 1 ELSE 0 END AS iValue "
    '        'S.SANDEEP |15-NOV-2019| -- END

    '        If blnOnlyMyApproval Then
    '            StrQ &= "   AND ISNULL(HTAT.isprocessed, 0) = 0 "
    '        End If
    '        StrQ &= " ) AS B ON [B].[priority] = [Fn].[priority] AND [B].[iPeriodId] = [iData].[periodunkid] AND [iData].calibratnounkid = [B].calibratnounkid " & _
    '                " AND [iData].employeeunkid = [B].employeeunkid " & _
    '                " LEFT JOIN " & _
    '                " ( " & _
    '                "   SELECT " & _
    '                "        X.xPeriodId " & _
    '                "       ,X.xPeriodName " & _
    '                "       ,X.employeeunkid " & _
    '                "       ,X.calibratnounkid " & _
    '                "       ,X.cscore " & _
    '                "       ,x.approvalremark " & _
    '                "       ,x.calibration_remark " & _
    '                "   FROM " & _
    '                "   ( " & _
    '                "       SELECT " & _
    '                "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) as xno " & _
    '                "           ,CP.periodunkid AS xPeriodId " & _
    '                "           ,CP.period_name AS xPeriodName " & _
    '                "           ,MAT.employeeunkid " & _
    '                "           ,MAT.calibratnounkid " & _
    '                "           ,CAST(MAT.calibrated_score AS DECIMAL(36,2)) AS cscore " & _
    '                "           ,MAT.approvalremark " & _
    '                "           ,MAT.calibration_remark " & _
    '                "       FROM hrassess_computescore_approval_tran AS MAT " & _
    '                "           JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
    '                "       WHERE MAT.isvoid = 0 AND MAT.periodunkid = @periodunkid "
    '        If blnOnlyMyApproval Then
    '            StrQ &= " AND MAT.isprocessed = 0 "
    '        End If
    '        StrQ &= "   ) AS X WHERE X.xno = 1 " & _
    '                " ) AS NV ON NV.employeeunkid = iData.employeeunkid AND NV.xPeriodId = iData.periodunkid AND  NV.calibratnounkid = iData.calibratnounkid " & _
    '                " LEFT JOIN " & _
    '                " ( " & _
    '                "   SELECT " & _
    '                "        employeeunkid AS aempid " & _
    '                "       ,periodunkid AS aprid " & _
    '                "       ,calibrated_score As acscore " & _
    '                "       ,calibration_remark AS acremark " & _
    '                "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to) AS acrating " & _
    '                "   FROM hrassess_computescore_approval_tran " & _
    '                "   WHERE mappingunkid = 0 AND isvoid = 0 AND periodunkid = @periodunkid " & _
    '                " ) AS acal ON acal.aempid = iData.employeeunkid AND acal.aprid = iData.periodunkid " & _
    '                "WHERE 1 = 1 AND iData.statusunkid IN (0,1) AND [iData].periodunkid = @periodunkid "

    '        If mstrCalibrationUnkids.Trim.Length > 0 Then
    '            StrQ &= " AND [iData].calibratnounkid IN (" & mstrCalibrationUnkids & ") "
    '        End If

    '        If blnOnlyMyApproval Then
    '            StrQ &= " AND ISNULL([iData].[isprocessed],0) = 0 "
    '        End If

    '        If blnOnlyMyApproval Then
    '            StrQ &= " AND Fn.userunkid = @U "
    '            objDataOperation.AddParameter("@U", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
    '        End If

    '        StrQ &= "ORDER BY iData.calibratnounkid "

    '        StrQ &= "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
    '                "DROP TABLE #USR "

    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 100, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 101, "Approved"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 102, "Rejected"))
    '        objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 103, "Pending for Approval"))

    '        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        Dim objSApproval As New clsScoreCalibrationApproval
    '        Dim dsCalibrator As New DataTable
    '        dsCalibrator = objSApproval.GetUserAllocation(mintPeriodId, intCompanyId, objDataOperation)
    '        objSApproval = Nothing
    '        dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateCalibration(x, dsCalibrator, True))

    '        Dim StrDistinctCols As List(Of String) = New List(Of String)
    '        Dim mstrDynamicCols As String = Mid(mstrDisplayCols, 2)
    '        Dim oCols As String() = mstrDynamicCols.Split(",")

    '        StrDistinctCols.Add("calibratnounkid")
    '        StrDistinctCols.Add("employeeunkid")
    '        StrDistinctCols.Add("calibration_no")
    '        StrDistinctCols.Add("Code")
    '        StrDistinctCols.Add("Employee")
    '        If oCols IsNot Nothing AndAlso oCols.Length > 0 Then
    '            For index As Integer = 0 To oCols.Length - 1
    '                StrDistinctCols.Add(oCols(index).Trim().Replace("[", "").Replace("]", ""))
    '            Next
    '        End If
    '        StrDistinctCols.Add("calibuser")
    '        dtList = dsList.Tables(0).DefaultView.ToTable(True, StrDistinctCols.ToArray())
    '        dtList.Columns("calibration_no").Caption = Language.getMessage(mstrModuleName, 103, "Calibration No")
    '        dtList.Columns("Code").Caption = Language.getMessage(mstrModuleName, 100, "Code")
    '        dtList.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 101, "Employee")
    '        dtList.Columns("calibuser").Caption = Language.getMessage(mstrModuleName, 108, "Calibrator")
    '        Dim xCol As New DataColumn
    '        With xCol
    '            .ColumnName = "cstatus"
    '            .Caption = Language.getMessage(mstrModuleName, 107, "Calibration Status")
    '            .DataType = GetType(System.String)
    '            .DefaultValue = "X"
    '            .ExtendedProperties.Add("1", "1")
    '        End With
    '        dtList.Columns.Add(xCol)

    '        Dim iLevels As Dictionary(Of Integer, String)
    '        Dim iKeys As List(Of Integer) = Nothing
    '        iLevels = dsList.Tables(0).AsEnumerable().Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("priority"), Key .attribute2_name = row.Field(Of String)("levelname")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
    '        If iLevels IsNot Nothing AndAlso iLevels.Count > 0 Then
    '            iKeys = iLevels.OrderBy(Function(x) x.Key).Select(Function(x) x.Key).ToList()
    '            For Each iKey In iKeys
    '                xCol = New DataColumn
    '                With xCol
    '                    .ColumnName = "lvl" + iKey.ToString()
    '                    .Caption = iLevels(iKey)
    '                    .DataType = GetType(System.String)
    '                    .DefaultValue = "X"
    '                End With
    '                dtList.Columns.Add(xCol)
    '            Next
    '        End If

    '        Dim iGroup As Dictionary(Of Integer, Integer)
    '        iGroup = dtList.AsEnumerable().Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("calibratnounkid"), Key .attribute2_name = row.Field(Of Integer)("calibratnounkid")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
    '        If iGroup IsNot Nothing AndAlso iGroup.Keys.Count > 0 Then
    '            For Each ikey In iGroup.Keys
    '                Dim iCalibId As Integer = ikey
    '                Dim iList As List(Of Integer) = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).Select(Function(x) x.Field(Of Integer)("iStatusId")).Distinct().ToList()
    '                If iList IsNot Nothing AndAlso iList.Count > 0 Then
    '                    If iList.Count > 1 Then
    '                        dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "cstatus", "✔"))
    '                    ElseIf iList.Count = 1 Then
    '                        'S.SANDEEP |12-NOV-2019| -- START
    '                        'ISSUE/ENHANCEMENT : Calibration Issues
    '                        'If iList(0) = clsScoreCalibrationApproval.enCalibrationStatus.Submitted Then
    '                        '    dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "cstatus", "✔"))
    '                        'End If
    '                        Select Case iList(0)
    '                            Case clsScoreCalibrationApproval.enCalibrationStatus.Submitted, _
    '                                 clsScoreCalibrationApproval.enCalibrationStatus.Approved
    '                                dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "cstatus", "✔"))
    '                        End Select
    '                        'S.SANDEEP |12-NOV-2019| -- END
    '                    End If
    '                End If

    '                'S.SANDEEP |15-NOV-2019| -- START
    '                'ISSUE/ENHANCEMENT : Calibration Issues
    '                'For Each iPrtId In iKeys
    '                '    Dim iPriotiry As Integer = iPrtId
    '                '    iList = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") = iPriotiry And x.Field(Of Integer)("calibratnounkid") = iCalibId).Select(Function(x) x.Field(Of Integer)("iStatusId")).Distinct().ToList()
    '                '    If iList IsNot Nothing AndAlso iList.Count > 0 Then
    '                '        Select Case CInt(iList(0))
    '                '            Case clsScoreCalibrationApproval.enCalibrationStatus.Approved
    '                '                dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "lvl" + iPriotiry.ToString(), "✔"))
    '                '            Case Else
    '                '                Exit For
    '                '        End Select
    '                '    End If
    '                'Next
    '                For Each iPrtId In iKeys
    '                    Dim iPriotiry As Integer = iPrtId
    '                    dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "lvl" + iPriotiry.ToString(), "✔", iPriotiry, dsList.Tables(0)))
    '                Next
    '                'iValue
    '                'S.SANDEEP |15-NOV-2019| -- END
    '            Next
    '        End If

    '        dtList.Columns.Remove("calibratnounkid")
    '        'S.SANDEEP |15-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        dtList.Columns.Remove("employeeunkid")
    '        'S.SANDEEP |15-NOV-2019| -- END
    '        Dim intArrayColumnWidth As Integer() = Nothing
    '        Dim idx As Integer = dtList.Columns.IndexOf("cstatus")
    '        ReDim intArrayColumnWidth(dtList.Columns.Count - 1)
    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '            If i = 0 Then
    '                intArrayColumnWidth(i) = 100
    '            ElseIf i >= idx Then
    '                intArrayColumnWidth(i) = 100
    '            Else
    '                intArrayColumnWidth(i) = 125
    '            End If
    '        Next

    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dtList, intArrayColumnWidth, True, True, True, Nothing, Me._ReportName & " - (" & mstrReportViewName & ")", "", " ", , "", True, Nothing, Nothing, Nothing, Nothing, False)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Export_CalibrationStatus_Report; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Public Sub Export_CalibrationStatus_Report(ByVal strDatabaseName As String _
                                             , ByVal intCompanyId As Integer _
                                             , ByVal intYearId As Integer _
                                             , ByVal strUserAccessMode As String _
                                             , ByVal intPrivilegeId As Integer _
                                             , ByVal xEmployeeAsOnDate As String _
                                             , ByVal intUserId As Integer _
                                             , ByVal blnOnlyMyApproval As Boolean _
                                             , ByVal strExportPath As String _
                                             , ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        objDataOperation = New clsDataOperation
        Try

            Company._Object._Companyunkid = intCompanyId
            ConfigParameter._Object._Companyunkid = intCompanyId
            User._Object._Userunkid = intUserId

            Dim dsFinal As New DataSet
            Dim xDateJoinQry, xDateFilterQry, strUACJoin, strUACFilter, xAdvanceJoinQry As String
            strUACJoin = "" : strUACFilter = "" : xAdvanceJoinQry = ""
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(xEmployeeAsOnDate), eZeeDate.convertDate(xEmployeeAsOnDate), , , strDatabaseName, "AEM")
            NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName, "AEM")

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL DROP TABLE #USR " & _
                   "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL DROP TABLE #EMP "

            StrQ &= "SELECT * INTO #USR " & _
                    "FROM " & _
                        "( " & _
                        "   SELECT " & _
                    "        CT.employeeunkid " & _
                    "       ,AC.mapuserunkid as userunkid " & _
                    "   FROM hrscore_calibration_approver_master AS AC " & _
                    "       JOIN hrmsConfiguration..cfuser_master AS UM ON AC.mapuserunkid = UM.userunkid " & _
                    "       JOIN hrscore_calibration_approverlevel_master AS AL ON AC.levelunkid = AL.levelunkid " & _
                    "       JOIN hrscore_calibration_approver_tran AS CT ON AC.mappingunkid = CT.mappingunkid " & _
                    "   WHERE AC.iscalibrator = 0 AND AC.isvoid = 0 AND CT.isvoid = 0 AND AC.visibletypeid = 1 AND CT.visibletypeid = 1 AND AC.isactive = 1 " & _
                    ") AS Fl "

            StrQ &= "SELECT " & _
                    "    AEM.employeeunkid AS empid " & _
                    "   ,ISNULL(T.departmentunkid, 0) AS deptid " & _
                    "   ,ISNULL(J.jobunkid, 0) AS jobid " & _
                    "   ,ISNULL(T.classgroupunkid, 0) AS clsgrpid " & _
                    "   ,ISNULL(T.classunkid, 0) AS clsid " & _
                    "   ,ISNULL(T.stationunkid, 0) AS branchid " & _
                    "   ,ISNULL(T.deptgroupunkid, 0) AS deptgrpid " & _
                    "   ,ISNULL(T.sectiongroupunkid, 0) AS secgrpid " & _
                    "   ,ISNULL(T.sectionunkid, 0) AS secid " & _
                    "   ,ISNULL(T.unitgroupunkid, 0) AS unitgrpid " & _
                    "   ,ISNULL(T.unitunkid, 0) AS unitid " & _
                    "   ,ISNULL(T.teamunkid, 0) AS teamid " & _
                    "   ,ISNULL(J.jobgroupunkid, 0) AS jgrpid " & _
                    "   ,hcsm.periodunkid AS periodid " & _
                    "   ,AEM.employeecode + ' - ' + AEM.firstname + ' ' + AEM.surname AS ecodename " & _
                    "   ,AEM.firstname + ' ' + AEM.surname AS ename " & _
                    "   ,ISNULL(hcsm.calibratnounkid, 0) AS calibratnounkid " & _
                    "   ,AEM.employeecode AS Code " & _
                    "   ,EJOB.job_name AS JobTitle " & _
                    "INTO #EMP " & _
                    "FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                    "   JOIN hrassess_compute_score_master hcsm ON AEM.employeeunkid = hcsm.employeeunkid "
            
            If strUACJoin.Trim.Length > 0 AndAlso intUserId > 0 Then
                StrQ &= strUACJoin
            End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        stationunkid " & _
                    "       ,deptgroupunkid " & _
                    "       ,departmentunkid " & _
                    "       ,sectiongroupunkid " & _
                    "       ,sectionunkid " & _
                    "       ,unitgroupunkid " & _
                    "       ,unitunkid " & _
                    "       ,teamunkid " & _
                    "       ,classgroupunkid " & _
                    "       ,classunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
            If mintEmployeeId > 0 Then StrQ &= " AND employeeunkid IN (" & mintEmployeeId.ToString() & ") "
            StrQ &= ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobgroupunkid " & _
                    "       ,jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN hrjob_master AS EJOB ON EJOB.jobunkid = J.jobunkid "

            StrQ &= "WHERE 1 = 1"
            If mintEmployeeId > 0 Then StrQ &= " AND AEM.employeeunkid IN (" & mintEmployeeId.ToString() & ") "
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If
            StrQ &= " AND hcsm.isvoid = 0 AND hcsm.periodunkid = @periodunkid "

            StrQ &= "SELECT DISTINCT " & _
                    "    CAST(0 AS BIT) AS iCheck " & _
                    "   ,ISNULL(iData.calibration_no,'') AS calibration_no " & _
                    "   ,e.Code AS Code " & _
                    "   ,e.ename AS Employee " & _
                    "   ,ISNULL(iData.DATE,'') AS DATE " & _
                    "   ,ISNULL(iData.povisionscore, 0) AS povisionscore " & _
                    "   ,ISNULL(NV.cscore, ISNULL([B].bscore, ISNULL(iData.calibratescore,0))) AS calibratescore " & _
                    "   ,ISNULL(B.[transactiondate], iData.DATE) AS transactiondate " & _
                    "   ,ISNULL(B.[mappingunkid], ISNULL(iData.mappingunkid,0)) AS mappingunkid " & _
                    "   ,ISNULL(B.[iStatus], @Pending) AS iStatus " & _
                    "   ,ISNULL(B.[iStatusId],CASE WHEN iData.calibratnounkid > 0 THEN 1 ELSE ISNULL(iData.statusunkid,0) END) AS iStatusId " & _
                    "   ,ISNULL(B.[iValue], 0) AS iValue " & _
                    "   ,ISNULL(Fn.userunkid,0) AS userunkid " & _
                    "   ,ISNULL(Fn.username,'') AS username " & _
                    "   ,ISNULL(Fn.email,'') AS approver_email " & _
                    "   ,ISNULL(Fn.priority,0) AS priority " & _
                    "   ,e.empid AS employeeunkid " & _
                    "   ,ISNULL(Fn.LvName, '') AS levelname " & _
                    "   ,ISNULL(Fn.uempid,0) AS uempid " & _
                    "   ,ISNULL(Fn.ecompid,0) AS ecompid " & _
                    "   ,e.ecodename AS oemployee " & _
                    "   ,e.ename AS employee_name " & _
                    "   ,e.periodid AS periodunkid " & _
                    "   ,ISNULL(iData.statusunkid,1) AS statusunkid " & _
                    "   ,ISNULL(iData.tranguid,'') AS tranguid " & _
                    "   ,ISNULL(iData.calibratnounkid,0) AS calibratnounkid " & _
                    "   ,ISNULL(bapprovalremark,ISNULL(iData.approvalremark,'')) AS approvalremark " & _
                    "   ,ISNULL(iData.isfinal,0) AS isfinal " & _
                    "   ,ISNULL(iData.isprocessed,0) AS isprocessed " & _
                    "   ,e.JobTitle " & _
                    "   ,CAST(1 AS BIT) AS isChanged " & _
                    "   ,ISNULL(NV.calibration_remark, ISNULL(iData.calibration_remark, ISNULL(iData.c_remark, ''))) AS calibration_remark " & _
                    "   ,ISNULL(iData.submitdate,'') AS submitdate " & _
                    "   ,ISNULL(iData.c_remark,'') AS c_remark " & _
                    "   ,ISNULL(iData.o_remark,'') AS o_remark " & _
                    "   ,iData.s_date " & _
                    "   ,'' AS lstpRating " & _
                    "   ,'' AS lstcRating " & _
                    "   ,ISNULL(acal.acrating, '') AS acrating " & _
                    "   ,ISNULL(acal.acremark, '') AS acremark " & _
                    "   ,ISNULL(B.lstcRating, '') AS lstapRating " & _
                    "   ,ISNULL(bcalibration_remark, '') AS apcalibremark " & _
                    "   ,'' AS allocation " & _
                    "   ,ISNULL(audituserunkid,0) AS audituserunkid " & _
                    "   ,ISNULL(CU.username,'') AS calibuser " & _
                    "   ,ISNULL(Fn.email,'') AS email "
            StrQ &= mstrSelectedCols & " "
            StrQ &= "FROM #EMP e " & _
                    "   LEFT JOIN #USR u ON e.empid = u.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT DISTINCT " & _
                    "            cm.username " & _
                    "           ,hcsm.employeeunkid " & _
                    "       FROM hrassess_compute_score_master hcsm " & _
                    "           JOIN hrscore_calibration_approver_tran hcat ON hcsm.employeeunkid = hcat.employeeunkid " & _
                    "           JOIN hrscore_calibration_approver_master hcam ON hcat.mappingunkid = hcam.mappingunkid " & _
                    "           JOIN hrmsConfiguration..cfuser_master cm ON hcam.mapuserunkid = cm.userunkid " & _
                    "       WHERE hcsm.isvoid = 0 AND hcsm.periodunkid = @periodunkid " & _
                    "       AND hcam.iscalibrator = 1 AND hcam.isvoid = 0 AND hcat.isvoid = 0 AND hcam.visibletypeid = 1 AND hcat.visibletypeid = 1 AND hcam.isactive = 1 " & _
                    "   ) AS CU ON CU.employeeunkid = e.empid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           stationunkid " & _
                    "          ,deptgroupunkid " & _
                    "          ,departmentunkid " & _
                    "          ,sectiongroupunkid " & _
                    "          ,sectionunkid " & _
                    "          ,unitgroupunkid " & _
                    "          ,unitunkid " & _
                    "          ,teamunkid " & _
                    "          ,classgroupunkid " & _
                    "          ,classunkid " & _
                    "          ,employeeunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "      FROM hremployee_transfer_tran " & _
                    "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' " & _
                    "   ) AS T ON T.employeeunkid = e.empid AND T.Rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           jobgroupunkid " & _
                    "          ,jobunkid " & _
                    "          ,employeeunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "      FROM hremployee_categorization_tran " & _
                    "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' " & _
                    "   ) AS J ON J.employeeunkid = e.empid AND J.Rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           employeeunkid " & _
                    "          ,gradegroupunkid " & _
                    "          ,gradeunkid " & _
                    "          ,gradelevelunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) as Rno " & _
                    "      FROM prsalaryincrement_tran " & _
                    "      WHERE isvoid = 0 AND isapproved = 1 " & _
                    "      AND CONVERT(CHAR(8),incrementdate,112) <= '" & xEmployeeAsOnDate & "' " & _
                    "   ) AS G ON G.employeeunkid = e.empid AND G.Rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           cctranheadvalueid AS costcenterunkid " & _
                    "          ,employeeunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "      FROM hremployee_cctranhead_tran " & _
                    "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' " & _
                    "      AND istransactionhead = 0 " & _
                    "   ) AS C ON C.employeeunkid = e.empid AND C.Rno = 1 "
            StrQ &= mstrSelectedJoin
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        a.calibration_no " & _
                    "       ,a.Code " & _
                    "       ,a.Employee " & _
                    "       ,a.DATE " & _
                    "       ,a.povisionscore " & _
                    "       ,a.calibratescore " & _
                    "       ,a.employeeunkid " & _
                    "       ,a.periodunkid " & _
                    "       ,a.statusunkid " & _
                    "       ,a.tranguid " & _
                    "       ,a.calibratnounkid " & _
                    "       ,a.mappingunkid " & _
                    "       ,a.approvalremark " & _
                    "       ,a.isfinal " & _
                    "       ,a.isprocessed " & _
                    "       ,a.calibration_remark " & _
                    "       ,a.submitdate " & _
                    "       ,a.c_remark " & _
                    "       ,a.o_remark " & _
                    "       ,a.s_date " & _
                    "       ,a.audituserunkid " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            calibration_no " & _
                    "           ,employeecode AS Code " & _
                    "           ,firstname + ' ' + surname AS Employee " & _
                    "           ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
                    "           ,ISNULL(CONVERT(NVARCHAR(8), pScore.submit_date, 112), '') AS submitdate " & _
                    "           ,ISNULL(pScore.c_remark, ISNULL(rScore.c_remark, '')) AS c_remark " & _
                    "           ,ISNULL(pScore.o_remark, ISNULL(rScore.o_remark, '')) AS o_remark " & _
                    "           ,CAST(ISNULL(pScore.finaloverallscore, rScore.finaloverallscore) AS DECIMAL(36, 2)) AS povisionscore " & _
                    "           ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
                    "           ,hrassess_computescore_approval_tran.employeeunkid " & _
                    "           ,hrassess_computescore_approval_tran.periodunkid " & _
                    "           ,hrassess_computescore_approval_tran.statusunkid " & _
                    "           ,hrassess_computescore_approval_tran.tranguid " & _
                    "           ,hrassess_computescore_approval_tran.calibratnounkid " & _
                    "           ,hrassess_computescore_approval_tran.mappingunkid " & _
                    "           ,hrassess_computescore_approval_tran.approvalremark " & _
                    "           ,hrassess_computescore_approval_tran.isfinal " & _
                    "           ,hrassess_computescore_approval_tran.isprocessed " & _
                    "           ,hrassess_computescore_approval_tran.calibration_remark " & _
                    "           ,pScore.submit_date AS s_date " & _
                    "           ,hrassess_computescore_approval_tran.audituserunkid " & _
                    "       FROM hrassess_computescore_approval_tran " & _
                    "       LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT DISTINCT " & _
                    "                hrassess_computescore_rejection_tran.employeeunkid " & _
                    "               ,hrassess_computescore_rejection_tran.periodunkid " & _
                    "               ,hrassess_computescore_rejection_tran.calibratnounkid " & _
                    "               ,hrassess_computescore_rejection_tran.finaloverallscore " & _
                    "               ,hrassess_computescore_rejection_tran.calibration_remark AS c_remark " & _
                    "               ,hrassess_computescore_rejection_tran.overall_remark AS o_remark " & _
                    "           FROM hrassess_computescore_rejection_tran " & _
                    "           WHERE hrassess_computescore_rejection_tran.periodunkid = @periodunkid " & _
                    "       ) AS rScore ON rScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                    "       AND rScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                    "       AND rScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                    "       JOIN " & _
                    "       ( " & _
                    "           SELECT DISTINCT " & _
                    "                calibratnounkid " & _
                    "               ,employeeunkid " & _
                    "               ,periodunkid " & _
                    "               ,finaloverallscore " & _
                    "               ,submit_date " & _
                    "               ,calibration_remark AS c_remark " & _
                    "               ,overall_remark AS o_remark " & _
                    "           FROM hrassess_compute_score_master " & _
                    "           WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
                    "       ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                    "       JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
                    "       JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "       WHERE statusunkid IN (0, 1) AND isvoid = 0 AND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
                    "   ) AS A WHERE 1 = 1 AND A.statusunkid IN (0, 1) AND  A.periodunkid = @periodunkid " & _
                    ") AS iData ON iData.employeeunkid = e.empid " & _
                    "AND iData.statusunkid IN (0, 1) AND [iData].periodunkid = @periodunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT DISTINCT " & _
                    "        cfuser_master.userunkid " & _
                    "       ,cfuser_master.username " & _
                    "       ,cfuser_master.email " & _
                    "       ,LM.priority " & _
                    "       ,LM.levelname AS LvName " & _
                    "       ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                    "       ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                    "       ,EM.mapuserunkid " & _
                    "       ,EM.mappingunkid AS oMappingId " & _
                    "   FROM hrmsConfiguration..cfuser_master " & _
                    "       JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    "       JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                    "       JOIN " & strDatabaseName & "..hrscore_calibration_approver_master EM ON EM.mapuserunkid = cfuser_master.userunkid AND [EM].[iscalibrator] = 0 " & _
                    "       JOIN " & strDatabaseName & "..hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
                    "   WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 AND yearunkid = @Y AND privilegeunkid = @P " & _
                    ") AS Fn ON u.userunkid = Fn.userunkid " & _
                    " LEFT JOIN " & _
                    " ( " & _
                    "   SELECT " & _
                    "        [HA].[mapuserunkid] " & _
                    "       ,[HM].[priority] " & _
                    "       ,[HTAT].[statusunkid] AS iStatusId " & _
                    "       ,[HTAT].[transactiondate] " & _
                    "       ,[HTAT].[mappingunkid] " & _
                    "       ,[HTAT].[periodunkid] AS iPeriodId " & _
                    "       ,[HTAT].calibratnounkid " & _
                    "       ,[HTAT].calibrated_score AS bscore " & _
                    "       ,[HTAT].calibration_remark AS bcalibration_remark " & _
                    "       ,[HTAT].approvalremark AS bapprovalremark " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND [HTAT].calibrated_score >= score_from AND [HTAT].calibrated_score <= score_to) AS lstcRating " & _
                    "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                    "             WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ ' + CM.[username] + ' ]' " & _
                    "             WHEN [HTAT].[statusunkid] = 3 THEN @Reject + ' : [ ' + CM.[username] + ' ]' " & _
                    "        ELSE @PendingApproval END AS iStatus " & _
                    "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN 0 " & _
                    "             WHEN [HTAT].[statusunkid] = 2 THEN 1 " & _
                    "             WHEN [HTAT].[statusunkid] = 3 THEN 1 " & _
                    "        ELSE 0 END AS iValue " & _
                    "       ,DENSE_RANK() OVER (PARTITION BY [HTAT].[employeeunkid],[HTAT].periodunkid ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                    "       ,HTAT.[employeeunkid] " & _
                    "   FROM hrassess_computescore_approval_tran AS HTAT " & _
                    "       JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] AND [HA].[iscalibrator] = 0 " & _
                    "       JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                    "       LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                    "   WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
                    "   AND HTAT.periodunkid = @periodunkid " & _
                    " ) AS B ON [B].[priority] = [Fn].[priority] AND [B].[iPeriodId] = [iData].[periodunkid] AND [iData].calibratnounkid = [B].calibratnounkid " & _
                    " AND [iData].employeeunkid = [B].employeeunkid " & _
                    " LEFT JOIN " & _
                    " ( " & _
                    "   SELECT " & _
                    "        X.xPeriodId " & _
                    "       ,X.xPeriodName " & _
                    "       ,X.employeeunkid " & _
                    "       ,X.calibratnounkid " & _
                    "       ,X.cscore " & _
                    "       ,x.approvalremark " & _
                    "       ,x.calibration_remark " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) as xno " & _
                    "           ,CP.periodunkid AS xPeriodId " & _
                    "           ,CP.period_name AS xPeriodName " & _
                    "           ,MAT.employeeunkid " & _
                    "           ,MAT.calibratnounkid " & _
                    "           ,CAST(MAT.calibrated_score AS DECIMAL(36,2)) AS cscore " & _
                    "           ,MAT.approvalremark " & _
                    "           ,MAT.calibration_remark " & _
                    "       FROM hrassess_computescore_approval_tran AS MAT " & _
                    "           JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
                    "       WHERE MAT.isvoid = 0 AND MAT.periodunkid = @periodunkid " & _
                    "   ) AS X WHERE X.xno = 1 " & _
                    " ) AS NV ON NV.employeeunkid = e.empid AND NV.xPeriodId = e.periodid AND NV.calibratnounkid = iData.calibratnounkid " & _
                    " LEFT JOIN " & _
                    " ( " & _
                    "   SELECT " & _
                    "        employeeunkid AS aempid " & _
                    "       ,periodunkid AS aprid " & _
                    "       ,calibrated_score As acscore " & _
                    "       ,calibration_remark AS acremark " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to) AS acrating " & _
                    "   FROM hrassess_computescore_approval_tran " & _
                    "   WHERE mappingunkid = 0 AND isvoid = 0 AND periodunkid = @periodunkid " & _
                    " ) AS acal ON acal.aempid = e.empid AND acal.aprid = e.periodid " & _
                    "WHERE 1 = 1 "

            If mstrCalibrationUnkids.Trim.Length > 0 Then
                StrQ &= " AND [iData].calibratnounkid IN (" & mstrCalibrationUnkids & ") "
            End If

            StrQ &= "IF OBJECT_ID('tempdb..#USR') IS NOT NULL DROP TABLE #USR " & _
                    "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL DROP TABLE #EMP "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsScoreCalibrationApproval", 103, "Pending for Approval"))

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Dim StrDistinctCols As List(Of String) = New List(Of String)
            Dim mstrDynamicCols As String = Mid(mstrDisplayCols, 2)
            Dim oCols As String() = mstrDynamicCols.Split(",")

            StrDistinctCols.Add("calibratnounkid")
            StrDistinctCols.Add("employeeunkid")
            StrDistinctCols.Add("calibration_no")
            StrDistinctCols.Add("Code")
            StrDistinctCols.Add("Employee")
            If oCols IsNot Nothing AndAlso oCols.Length > 0 Then
                For index As Integer = 0 To oCols.Length - 1
                    StrDistinctCols.Add(oCols(index).Trim().Replace("[", "").Replace("]", ""))
                Next
            End If
            StrDistinctCols.Add("calibuser")
            dtList = dsList.Tables(0).DefaultView.ToTable(True, StrDistinctCols.ToArray())
            dtList.Columns("calibration_no").Caption = Language.getMessage(mstrModuleName, 103, "Calibration No")
            dtList.Columns("Code").Caption = Language.getMessage(mstrModuleName, 100, "Code")
            dtList.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 101, "Employee")
            dtList.Columns("calibuser").Caption = Language.getMessage(mstrModuleName, 108, "Calibrator")
            Dim xCol As New DataColumn
            With xCol
                .ColumnName = "cstatus"
                .Caption = Language.getMessage(mstrModuleName, 107, "Calibration Status")
                .DataType = GetType(System.String)
                .DefaultValue = "X"
                .ExtendedProperties.Add("1", "1")
            End With
            dtList.Columns.Add(xCol)

            Dim iLevels As Dictionary(Of Integer, String)
            Dim iKeys As List(Of Integer) = Nothing
            iLevels = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") > 0).Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("priority"), Key .attribute2_name = row.Field(Of String)("levelname")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
            If iLevels IsNot Nothing AndAlso iLevels.Count > 0 Then
                iKeys = iLevels.OrderBy(Function(x) x.Key).Select(Function(x) x.Key).ToList()
                For Each iKey In iKeys
                    xCol = New DataColumn
                    With xCol
                        .ColumnName = "lvl" + iKey.ToString()
                        .Caption = iLevels(iKey)
                        .DataType = GetType(System.String)
                        .DefaultValue = "X"
                    End With
                    dtList.Columns.Add(xCol)
                Next
            End If

            Dim iGroup As Dictionary(Of Integer, Integer)
            iGroup = dtList.AsEnumerable().Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("calibratnounkid"), Key .attribute2_name = row.Field(Of Integer)("calibratnounkid")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
            If iGroup IsNot Nothing AndAlso iGroup.Keys.Count > 0 Then
                For Each ikey In iGroup.Keys
                    Dim iCalibId As Integer = ikey
                    Dim iList As List(Of Integer) = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).Select(Function(x) x.Field(Of Integer)("iStatusId")).Distinct().ToList()
                    If iList IsNot Nothing AndAlso iList.Count > 0 Then
                        If iList.Count > 1 Then
                            If iCalibId > 0 Then
                            dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "cstatus", "✔"))
                            End If
                        ElseIf iList.Count = 1 Then
                            Select Case iList(0)
                                Case clsScoreCalibrationApproval.enCalibrationStatus.Submitted, _
                                     clsScoreCalibrationApproval.enCalibrationStatus.Approved
                                    If iCalibId > 0 Then
                                    dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "cstatus", "✔"))
                                    End If
                            End Select
                        End If
                    End If
                    For Each iPrtId In iKeys
                        Dim iPriotiry As Integer = iPrtId
                        If iCalibId > 0 Then
                        dtList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = iCalibId).ToList().ForEach(Function(x) UpdateRowValue(x, "lvl" + iPriotiry.ToString(), "✔", iPriotiry, dsList.Tables(0)))
                        End If
                    Next
                Next
            End If

            dtList.Columns.Remove("calibratnounkid")
            dtList.Columns.Remove("employeeunkid")
            Dim intArrayColumnWidth As Integer() = Nothing
            Dim idx As Integer = dtList.Columns.IndexOf("cstatus")
            ReDim intArrayColumnWidth(dtList.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 100
                ElseIf i >= idx Then
                    intArrayColumnWidth(i) = 100
                Else
                    intArrayColumnWidth(i) = 125
                End If
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dtList, intArrayColumnWidth, True, True, True, Nothing, Me._ReportName & " - (" & mstrReportViewName & ")", "", " ", , "", True, Nothing, Nothing, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_CalibrationStatus_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |27-AUG-2021| -- END

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
    Public Sub GetNextEmployeeApproversNew(ByVal intPeriodUnkid As Integer _
                                         , ByVal strDatabaseName As String _
                                         , ByVal intCompanyId As Integer _
                                         , ByVal intYearId As Integer _
                                         , ByVal strUserAccessMode As String _
                                         , ByVal xEmployeeAsOnDate As String _
                                         , ByVal intUserId As Integer _
                                         , ByVal strExportPath As String _
                                         , ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = ""
        Try
            If intCompanyId <= 0 Then
                intCompanyId = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyId
            ConfigParameter._Object._Companyunkid = intCompanyId
            User._Object._Userunkid = intUserId

            objDataOperation = New clsDataOperation
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(xEmployeeAsOnDate), eZeeDate.convertDate(xEmployeeAsOnDate), , , strDatabaseName, "AEM")
            If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName, "AEM")


            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                   "DROP TABLE #USR "
            StrQ &= "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
                    "DROP TABLE #EMP "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( " & _
                    "   SELECT " & _
                    "        CT.employeeunkid " & _
                    "       ,AC.mapuserunkid as userunkid " & _
                    "   FROM hrscore_calibration_approver_master AS AC " & _
                    "       JOIN hrmsConfiguration..cfuser_master AS UM ON AC.mapuserunkid = UM.userunkid " & _
                    "       JOIN hrscore_calibration_approverlevel_master AS AL ON AC.levelunkid = AL.levelunkid " & _
                    "       JOIN hrscore_calibration_approver_tran AS CT ON AC.mappingunkid = CT.mappingunkid " & _
                    "   WHERE AC.iscalibrator = 0 AND AC.isvoid = 0 AND CT.isvoid = 0 " & _
                    "       AND AC.visibletypeid = 1 AND CT.visibletypeid = 1 AND AC.isactive = 1 " & _
                    ") AS Fl "
            StrQ &= "SELECT " & _
                    "     AEM.employeeunkid AS empid " & _
                    "    ,TAT.periodunkid AS periodunkid" & _
                    "    ,AEM.firstname + ' ' + AEM.surname  AS ename " & _
                    "    ,TAT.calibratnounkid AS calibratnounkid " & _
                    "    ,AEM.employeecode AS ecode "
            StrQ &= mstrSelectedCols & " "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "INTO #EMP " & _
                    "FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                    " JOIN hrassess_computescore_approval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           stationunkid " & _
                    "          ,deptgroupunkid " & _
                    "          ,departmentunkid " & _
                    "          ,sectiongroupunkid " & _
                    "          ,sectionunkid " & _
                    "          ,unitgroupunkid " & _
                    "          ,unitunkid " & _
                    "          ,teamunkid " & _
                    "          ,classgroupunkid " & _
                    "          ,classunkid " & _
                    "          ,employeeunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "      FROM hremployee_transfer_tran " & _
                    "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "   ) AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           jobgroupunkid " & _
                    "          ,jobunkid " & _
                    "          ,employeeunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "      FROM hremployee_categorization_tran " & _
                    "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "   ) AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           employeeunkid " & _
                    "          ,gradegroupunkid " & _
                    "          ,gradeunkid " & _
                    "          ,gradelevelunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) as Rno " & _
                    "      FROM prsalaryincrement_tran " & _
                    "      WHERE isvoid = 0 AND isapproved = 1 " & _
                    "      AND CONVERT(CHAR(8),incrementdate,112) <= '" & xEmployeeAsOnDate & "' "
            If mintEmployeeId > 0 Then
                StrQ &= " AND prsalaryincrement_tran.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "   ) AS G ON G.employeeunkid = AEM.employeeunkid AND G.Rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "      SELECT " & _
                    "           cctranheadvalueid AS costcenterunkid " & _
                    "          ,employeeunkid " & _
                    "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "      FROM hremployee_cctranhead_tran " & _
                    "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & xEmployeeAsOnDate & "' " & _
                    "      AND istransactionhead = 0 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_cctranhead_tran.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "   ) AS C ON C.employeeunkid = AEM.employeeunkid AND C.Rno = 1 "
            StrQ &= mstrSelectedJoin
            StrQ &= mstrAnalysis_Join

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= " WHERE 1 = 1 AND TAT.isvoid = 0 AND TAT.statusunkid IN (0,1) AND  [TAT].periodunkid = @periodunkid "

            If mintEmployeeId > 0 Then
                StrQ &= " AND AEM.employeeunkid = '" & mintEmployeeId & "' "
            End If

            If mstrCalibrationUnkids.Trim.Length > 0 Then
                StrQ &= " AND [TAT].calibratnounkid IN (" & mstrCalibrationUnkids & ") "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If

            StrQ &= "SELECT DISTINCT " & _
                    "    CAST(0 AS BIT) AS iCheck " & _
                    "   ,iData.calibration_no " & _
                    "   ,iData.Employee " & _
                    "   ,iData.DATE " & _
                    "   ,ISNULL(iData.povisionscore,0) AS povisionscore " & _
                    "   ,ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) AS calibratescore " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.Date) ELSE NULL END AS transactiondate " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.userunkid ELSE 0 END AS userunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
                    "   ,#EMP.empid AS employeeunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.uempid END AS uempid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.ecompid END AS ecompid " & _
                    "   ,#EMP.ename AS employee_name " & _
                    "   ,#EMP.ecode AS ecode " & _
                    "   ,iData.employeeunkid " & _
                    "   ,iData.periodunkid " & _
                    "   ,iData.statusunkid " & _
                    "   ,iData.tranguid " & _
                    "   ,iData.calibratnounkid " & _
                    "   ,CASE WHEN ISNULL(bapprovalremark,'') = '' THEN iData.approvalremark ELSE ISNULL(bapprovalremark,'') END AS approvalremark " & _
                    "   ,iData.isfinal " & _
                    "   ,iData.isprocessed " & _
                    "   ,iData.isgrp " & _
                    "   ,iData.grpid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.periodunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno " & _
                    "   ,CAST(1 AS BIT) AS isChanged " & _
                    "   ,ISNULL(NV.calibration_remark,ISNULL(iData.calibration_remark,ISNULL(iData.c_remark,''))) AS calibration_remark " & _
                    "   ,iData.submitdate " & _
                    "   ,iData.c_remark " & _
                    "   ,iData.o_remark " & _
                    "   ,iData.s_date " & _
                    "   ,'' AS lstpRating " & _
                    "   ,'' AS lstcRating " & _
                    "   ,ISNULL(acal.acrating,'') AS acrating " & _
                    "   ,ISNULL(acal.acremark,'') AS acremark " & _
                    "   ,ISNULL(B.lstcRating,'') AS lstapRating " & _
                    "   ,ISNULL(bcalibration_remark,'') AS apcalibremark " & _
                    "   ,'' AS allocation " & _
                    "   ,audituserunkid " & _
                    "   ,'' AS calibuser " & _
                    "   ,Fn.email " & _
                    "   ,#EMP.Id " & _
                    "   ,#EMP.GName " & _
                    "FROM #EMP " & _
                    "   JOIN #USR ON #EMP.empid = #USR.employeeunkid " & _
                    "   JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                 a.calibration_no " & _
                    "                ,a.Employee " & _
                    "                ,a.DATE " & _
                    "                ,a.povisionscore " & _
                    "                ,a.calibratescore " & _
                    "                ,a.isgrp " & _
                    "                ,a.grpid " & _
                    "                ,a.employeeunkid " & _
                    "                ,a.periodunkid " & _
                    "                ,a.statusunkid " & _
                    "                ,a.tranguid " & _
                    "                ,a.calibratnounkid " & _
                    "                ,a.mappingunkid " & _
                    "                ,a.approvalremark " & _
                    "                ,a.isfinal " & _
                    "                ,a.isprocessed " & _
                    "                ,a.calibration_remark " & _
                    "                ,a.submitdate " & _
                    "                ,a.c_remark " & _
                    "                ,a.o_remark " & _
                    "                ,a.s_date " & _
                    "                ,a.audituserunkid " & _
                    "           FROM " & _
                    "           ( " & _
                    "               SELECT " & _
                    "                    calibration_no " & _
                    "                   ,'/r/n' + employeecode + ' - ' + firstname + ' ' + surname AS Employee " & _
                    "                   ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
                    "                   ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
                    "                   ,ISNULL(pScore.c_remark, ISNULL(rScore.c_remark,'')) AS c_remark " & _
                    "                   ,ISNULL(pScore.o_remark, ISNULL(rScore.o_remark,'')) AS o_remark " & _
                    "                   ,CAST(ISNULL(pScore.finaloverallscore,rScore.finaloverallscore) AS DECIMAL(36, 2)) AS povisionscore " & _
                    "                   ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
                    "                   ,CAST(0 AS BIT) AS isgrp " & _
                    "                   ,hrassess_calibration_number.calibratnounkid AS grpid " & _
                    "                   ,hrassess_computescore_approval_tran.employeeunkid " & _
                    "                   ,hrassess_computescore_approval_tran.periodunkid " & _
                    "                   ,hrassess_computescore_approval_tran.statusunkid " & _
                    "                   ,hrassess_computescore_approval_tran.tranguid " & _
                    "                   ,hrassess_computescore_approval_tran.calibratnounkid " & _
                    "                   ,hrassess_computescore_approval_tran.mappingunkid " & _
                    "                   ,hrassess_computescore_approval_tran.approvalremark " & _
                    "                   ,hrassess_computescore_approval_tran.isfinal " & _
                    "                   ,hrassess_computescore_approval_tran.isprocessed " & _
                    "                   ,hrassess_computescore_approval_tran.calibration_remark " & _
                    "                   ,pScore.submit_date AS s_date " & _
                    "                   ,hrassess_computescore_approval_tran.audituserunkid " & _
                    "               FROM hrassess_computescore_approval_tran " & _
                    "               LEFT JOIN " & _
                    "               ( " & _
                    "                   SELECT DISTINCT " & _
                    "                        hrassess_computescore_rejection_tran.employeeunkid " & _
                    "                       ,hrassess_computescore_rejection_tran.periodunkid " & _
                    "                       ,hrassess_computescore_rejection_tran.calibratnounkid " & _
                    "                       ,hrassess_computescore_rejection_tran.finaloverallscore " & _
                    "                       ,hrassess_computescore_rejection_tran.calibration_remark AS c_remark " & _
                    "                       ,hrassess_computescore_rejection_tran.overall_remark AS o_remark " & _
                    "                   FROM hrassess_computescore_rejection_tran " & _
                    "                   WHERE hrassess_computescore_rejection_tran.periodunkid = @periodunkid "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hrassess_computescore_rejection_tran.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "               ) AS rScore ON rScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                    "                   AND rScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                    "                   AND rScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                    "               JOIN " & _
                    "               ( " & _
                    "                   SELECT DISTINCT " & _
                    "                        calibratnounkid " & _
                    "                       ,employeeunkid " & _
                    "                       ,periodunkid " & _
                    "                       ,finaloverallscore " & _
                    "                       ,submit_date " & _
                    "                       ,calibration_remark AS c_remark " & _
                    "                       ,overall_remark AS o_remark " & _
                    "                   FROM hrassess_compute_score_master " & _
                    "                   WHERE periodunkid = @periodunkid AND isvoid = 0 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hrassess_compute_score_master.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "               ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                    "               AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                    "               JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
                    "               JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "           WHERE statusunkid IN (0,1) AND isvoid = 0 aND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
                    "       ) AS A WHERE 1 = 1 AND A.statusunkid IN (0,1) " & _
                    "   ) AS iData ON iData.employeeunkid = #EMP.empid " & _
                    "    JOIN " & _
                    "    ( " & _
                    "        SELECT DISTINCT " & _
                    "             cfuser_master.userunkid " & _
                    "            ,cfuser_master.username " & _
                    "            ,cfuser_master.email " & _
                    "            ,LM.priority " & _
                    "            ,LM.levelname AS LvName " & _
                    "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                    "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                    "            ,EM.mapuserunkid " & _
                    "            ,EM.mappingunkid AS oMappingId " & _
                    "        FROM hrmsConfiguration..cfuser_master " & _
                    "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                    "        JOIN " & strDatabaseName & "..hrscore_calibration_approver_master EM ON EM.mapuserunkid = cfuser_master.userunkid AND EM.iscalibrator = 0 " & _
                    "        JOIN " & strDatabaseName & "..hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
                    "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
            StrQ &= "     AND yearunkid = @Y " & _
                    ") AS Fn ON #USR.userunkid = Fn.userunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        [HA].[mapuserunkid] " & _
                    "       ,[HM].[priority] " & _
                    "       ,[HTAT].[statusunkid] AS iStatusId " & _
                    "       ,[HTAT].[transactiondate] " & _
                    "       ,[HTAT].[mappingunkid] " & _
                    "       ,[HTAT].[periodunkid] AS iPeriodId " & _
                    "       ,[HTAT].calibratnounkid " & _
                    "       ,[HTAT].calibrated_score AS bscore " & _
                    "       ,[HTAT].calibration_remark AS bcalibration_remark " & _
                    "       ,[HTAT].approvalremark AS bapprovalremark " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND [HTAT].calibrated_score >= score_from AND [HTAT].calibrated_score <= score_to) AS lstcRating " & _
                    "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                    "             WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ ' + CM.[username] + ' ]' " & _
                    "             WHEN [HTAT].[statusunkid] = 3 THEN @Reject + ' : [ ' + CM.[username] + ' ]' " & _
                    "        ELSE @PendingApproval END AS iStatus " & _
                    "       ,DENSE_RANK() OVER (PARTITION BY [HTAT].[employeeunkid],[HTAT].periodunkid ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                    "       ,HTAT.[employeeunkid] " & _
                    "   FROM hrassess_computescore_approval_tran AS HTAT " & _
                    "       JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] AND HA.iscalibrator = 0 " & _
                    "       JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                    "       LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                    "   WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
                    "   AND HTAT.periodunkid = @periodunkid "
            If mintEmployeeId > 0 Then
                StrQ &= " AND HTAT.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= ") AS B ON [B].[priority] = [Fn].[priority] AND [B].[iPeriodId] = [iData].[periodunkid] AND [iData].calibratnounkid = [B].calibratnounkid " & _
                    "AND [iData].employeeunkid = [B].employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        X.xPeriodId " & _
                    "       ,X.xPeriodName " & _
                    "       ,X.employeeunkid " & _
                    "       ,X.calibratnounkid " & _
                    "       ,X.cscore " & _
                    "       ,x.approvalremark " & _
                    "       ,x.calibration_remark " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) as xno " & _
                    "           ,CP.periodunkid AS xPeriodId " & _
                    "           ,CP.period_name AS xPeriodName " & _
                    "           ,MAT.employeeunkid " & _
                    "           ,MAT.calibratnounkid " & _
                    "           ,CAST(MAT.calibrated_score AS DECIMAL(36,2)) AS cscore " & _
                    "           ,MAT.approvalremark " & _
                    "           ,MAT.calibration_remark " & _
                    "       FROM hrassess_computescore_approval_tran AS MAT " & _
                    "           JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
                    "       WHERE MAT.isvoid = 0 AND MAT.periodunkid = @periodunkid "
            If mintEmployeeId > 0 Then
                StrQ &= " AND MAT.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "   ) AS X WHERE X.xno = 1 " & _
                    ") AS NV ON NV.employeeunkid = iData.employeeunkid AND NV.xPeriodId = iData.periodunkid AND  NV.calibratnounkid = iData.calibratnounkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid AS aempid " & _
                    "       ,periodunkid AS aprid " & _
                    "       ,calibrated_score As acscore " & _
                    "       ,calibration_remark AS acremark " & _
                    "       ,calibratnounkid as acalibratnounkid " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to) AS acrating " & _
                    "   FROM hrassess_computescore_approval_tran " & _
                    "   WHERE mappingunkid = 0 AND isvoid = 0 AND periodunkid = @periodunkid " & _
                    ") AS acal ON acal.aempid = iData.employeeunkid AND acal.aprid = iData.periodunkid AND acal.acalibratnounkid = [iData].calibratnounkid " & _
                    "WHERE 1 = 1 AND iData.statusunkid IN (0,1) AND [iData].periodunkid = @periodunkid "

            If mstrCalibrationUnkids.Trim.Length > 0 Then
                StrQ &= " AND iData.calibratnounkid IN (" & mstrCalibrationUnkids & ") "
            End If

            StrQ &= "ORDER BY iData.calibratnounkid "

            StrQ &= "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                    "DROP TABLE #USR "
            StrQ &= "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
                    "DROP TABLE #EMP "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
            Dim objSCalib As New clsScoreCalibrationApproval
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtUsrAllocation As DataTable = objSCalib.GetUserAllocation(intPeriodUnkid, intCompanyId, objDataOperation)
                If dtUsrAllocation IsNot Nothing Then
                    If dtUsrAllocation.Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) objSCalib.UpdateRating(x, dtUsrAllocation, True))
                End If
                Dim objRating As New clsAppraisal_Rating
                Dim dsRating As New DataSet
                dsRating = objRating.getComboList("List", False)
                objRating = Nothing
                If dsRating.Tables(0).Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) objSCalib.UpdateRating(x, dsRating.Tables(0), False))
            End If

            Dim dtFinal As New DataTable
            dtFinal = dsList.Tables(0).DefaultView.ToTable(False, "calibratnounkid", "audituserunkid", "ecode", "employee_name", "calibration_no", "lstpRating", "acrating", "acremark", "lstapRating", "apcalibremark", "approvalremark", "iStatus", "username", "levelname", "allocation", "calibuser", "Id", "GName")
            dtFinal.Columns("calibration_no").ColumnName = IIf(Language.getMessage(mstrModuleName, 103, "Calibration No").Trim().Length <= 0, "Calibration No", Language.getMessage(mstrModuleName, 103, "Calibration No"))
            dtFinal.Columns("username").ColumnName = "approver"
            dtFinal.Columns("calibuser").ColumnName = "username"
            dtFinal.AsEnumerable().ToList.ForEach(Function(x) UpdateCalibration(x, dtFinal, False))


            dtFinal.Columns("ecode").ColumnName = IIf(Language.getMessage(mstrModuleName, 100, "Code").Trim().Length <= 0, "Code", Language.getMessage(mstrModuleName, 100, "Code"))
            dtFinal.Columns("employee_name").ColumnName = IIf(Language.getMessage(mstrModuleName, 101, "Employee").Trim().Length <= 0, "Employee", Language.getMessage(mstrModuleName, 101, "Employee"))
            dtFinal.Columns("lstpRating").ColumnName = IIf(Language.getMessage(mstrModuleName, 109, "Prov. Rating").Trim().Length <= 0, "Prov. Rating", Language.getMessage(mstrModuleName, 109, "Prov. Rating"))
            dtFinal.Columns("acrating").ColumnName = IIf(Language.getMessage(mstrModuleName, 111, "Applied Rating").Trim().Length <= 0, "Applied Rating", Language.getMessage(mstrModuleName, 111, "Applied Rating"))
            dtFinal.Columns("acremark").ColumnName = IIf(Language.getMessage(mstrModuleName, 112, "Applied Remark").Trim().Length <= 0, "Applied Remark", Language.getMessage(mstrModuleName, 112, "Applied Remark"))
            dtFinal.Columns("lstapRating").ColumnName = IIf(Language.getMessage(mstrModuleName, 113, "Approver Calib. Rating").Trim().Length <= 0, "Approver Calib. Rating", Language.getMessage(mstrModuleName, 113, "Approver Calib. Rating"))
            dtFinal.Columns("apcalibremark").ColumnName = IIf(Language.getMessage(mstrModuleName, 114, "Calibration Remark").Trim().Length <= 0, "Calibration Remark", Language.getMessage(mstrModuleName, 114, "Calibration Remark"))
            dtFinal.Columns("approvalremark").ColumnName = IIf(Language.getMessage(mstrModuleName, 115, "Approval Remark").Trim().Length <= 0, "Approval Remark", Language.getMessage(mstrModuleName, 115, "Approval Remark"))
            dtFinal.Columns("iStatus").ColumnName = IIf(Language.getMessage(mstrModuleName, 116, "Status").Trim().Length <= 0, "Status", Language.getMessage(mstrModuleName, 116, "Status"))
            dtFinal.Columns("approver").ColumnName = IIf(Language.getMessage(mstrModuleName, 117, "Approver").Trim().Length <= 0, "Approver", Language.getMessage(mstrModuleName, 117, "Approver"))
            dtFinal.Columns("levelname").ColumnName = IIf(Language.getMessage(mstrModuleName, 118, "Level").Trim().Length <= 0, "Level", Language.getMessage(mstrModuleName, 118, "Level"))

            If mintViewIndex > 0 Then
                dtFinal = New DataView(dtFinal, "", "calibratnounkid,GName", DataViewRowState.CurrentRows).ToTable()
            Else
                dtFinal = New DataView(dtFinal, "", "calibratnounkid", DataViewRowState.CurrentRows).ToTable()
            End If

            dtFinal.Columns.Remove("calibratnounkid")
            dtFinal.Columns.Remove("audituserunkid")
            dtFinal.Columns.Remove("username")
            dtFinal.Columns.Remove("allocation")
            Dim str_NumCaption As String = IIf(Language.getMessage(mstrModuleName, 103, "Calibration No").Trim().Length <= 0, "Calibration No", Language.getMessage(mstrModuleName, 103, "Calibration No"))
            Dim strarrGroupColumns As String() = Nothing

            If dtFinal.Columns.Contains("Id") Then
                dtFinal.Columns.Remove("Id")
            End If
            If mintViewIndex > 0 Then
                dtFinal.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName", str_NumCaption}
                strarrGroupColumns = strGrpCols
            Else
                Dim strGrpCols As String() = {str_NumCaption}
                strarrGroupColumns = strGrpCols
            End If
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtFinal.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 2
                intArrayColumnWidth(i) = 125
            Next
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dtFinal, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " - (" & mstrReportViewName & ")", "", " ", , "", False, Nothing, Nothing, Nothing, Nothing, False)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApproversNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |01-MAY-2020| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsScoreCalibrationApproval", 100, "Pending")
            Language.setMessage("clsScoreCalibrationApproval", 101, "Approved")
            Language.setMessage("clsScoreCalibrationApproval", 102, "Rejected")
            Language.setMessage("clsScoreCalibrationApproval", 103, "Pending for Approval")
            Language.setMessage(mstrModuleName, 100, "Code")
            Language.setMessage(mstrModuleName, 101, "Employee")
            Language.setMessage(mstrModuleName, 103, "Calibration No")
            Language.setMessage(mstrModuleName, 105, "Status")
            Language.setMessage(mstrModuleName, 106, "Not Submitted")
            Language.setMessage(mstrModuleName, 107, "Calibration Status")
            Language.setMessage(mstrModuleName, 108, "Calibrator")
            Language.setMessage(mstrModuleName, 109, "Prov. Rating")
            Language.setMessage(mstrModuleName, 110, "Calib. Rating")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
