﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrainingVoidCancelReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboInstitute = New System.Windows.Forms.ComboBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.cboQualification = New System.Windows.Forms.ComboBox
        Me.cboVoidUser = New System.Windows.Forms.ComboBox
        Me.cboCancelUser = New System.Windows.Forms.ComboBox
        Me.cboQGroup = New System.Windows.Forms.ComboBox
        Me.lblCancellationUser = New System.Windows.Forms.Label
        Me.lblVoidUser = New System.Windows.Forms.Label
        Me.lblQualification = New System.Windows.Forms.Label
        Me.lblInstitute = New System.Windows.Forms.Label
        Me.lblQGroup = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 523)
        Me.NavPanel.Size = New System.Drawing.Size(755, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboQualification)
        Me.gbFilterCriteria.Controls.Add(Me.cboVoidUser)
        Me.gbFilterCriteria.Controls.Add(Me.cboCancelUser)
        Me.gbFilterCriteria.Controls.Add(Me.cboQGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblCancellationUser)
        Me.gbFilterCriteria.Controls.Add(Me.lblVoidUser)
        Me.gbFilterCriteria.Controls.Add(Me.lblQualification)
        Me.gbFilterCriteria.Controls.Add(Me.lblInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.lblQGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(470, 117)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInstitute
        '
        Me.cboInstitute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitute.DropDownWidth = 230
        Me.cboInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitute.FormattingEnabled = True
        Me.cboInstitute.Location = New System.Drawing.Point(332, 87)
        Me.cboInstitute.Name = "cboInstitute"
        Me.cboInstitute.Size = New System.Drawing.Size(124, 21)
        Me.cboInstitute.TabIndex = 11
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 230
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(332, 60)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(124, 21)
        Me.cboStatus.TabIndex = 7
        '
        'cboQualification
        '
        Me.cboQualification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualification.DropDownWidth = 230
        Me.cboQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualification.FormattingEnabled = True
        Me.cboQualification.Location = New System.Drawing.Point(332, 33)
        Me.cboQualification.Name = "cboQualification"
        Me.cboQualification.Size = New System.Drawing.Size(124, 21)
        Me.cboQualification.TabIndex = 3
        '
        'cboVoidUser
        '
        Me.cboVoidUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVoidUser.DropDownWidth = 230
        Me.cboVoidUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVoidUser.FormattingEnabled = True
        Me.cboVoidUser.Location = New System.Drawing.Point(118, 87)
        Me.cboVoidUser.Name = "cboVoidUser"
        Me.cboVoidUser.Size = New System.Drawing.Size(124, 21)
        Me.cboVoidUser.TabIndex = 9
        '
        'cboCancelUser
        '
        Me.cboCancelUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCancelUser.DropDownWidth = 230
        Me.cboCancelUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCancelUser.FormattingEnabled = True
        Me.cboCancelUser.Location = New System.Drawing.Point(118, 60)
        Me.cboCancelUser.Name = "cboCancelUser"
        Me.cboCancelUser.Size = New System.Drawing.Size(124, 21)
        Me.cboCancelUser.TabIndex = 5
        '
        'cboQGroup
        '
        Me.cboQGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQGroup.DropDownWidth = 230
        Me.cboQGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQGroup.FormattingEnabled = True
        Me.cboQGroup.Location = New System.Drawing.Point(118, 33)
        Me.cboQGroup.Name = "cboQGroup"
        Me.cboQGroup.Size = New System.Drawing.Size(124, 21)
        Me.cboQGroup.TabIndex = 1
        '
        'lblCancellationUser
        '
        Me.lblCancellationUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancellationUser.Location = New System.Drawing.Point(8, 63)
        Me.lblCancellationUser.Name = "lblCancellationUser"
        Me.lblCancellationUser.Size = New System.Drawing.Size(104, 15)
        Me.lblCancellationUser.TabIndex = 4
        Me.lblCancellationUser.Text = "Cancellation User"
        Me.lblCancellationUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVoidUser
        '
        Me.lblVoidUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoidUser.Location = New System.Drawing.Point(8, 90)
        Me.lblVoidUser.Name = "lblVoidUser"
        Me.lblVoidUser.Size = New System.Drawing.Size(104, 15)
        Me.lblVoidUser.TabIndex = 8
        Me.lblVoidUser.Text = "Void User"
        Me.lblVoidUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(248, 36)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(78, 15)
        Me.lblQualification.TabIndex = 2
        Me.lblQualification.Text = "Qualification"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInstitute
        '
        Me.lblInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstitute.Location = New System.Drawing.Point(248, 90)
        Me.lblInstitute.Name = "lblInstitute"
        Me.lblInstitute.Size = New System.Drawing.Size(78, 15)
        Me.lblInstitute.TabIndex = 10
        Me.lblInstitute.Text = "Institute"
        Me.lblInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQGroup
        '
        Me.lblQGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblQGroup.Name = "lblQGroup"
        Me.lblQGroup.Size = New System.Drawing.Size(104, 15)
        Me.lblQGroup.TabIndex = 0
        Me.lblQGroup.Text = "Qualification Group"
        Me.lblQGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(248, 63)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(78, 15)
        Me.lblStatus.TabIndex = 6
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTrainingVoidCancelReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(755, 578)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTrainingVoidCancelReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = ""
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboQGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblCancellationUser As System.Windows.Forms.Label
    Friend WithEvents lblVoidUser As System.Windows.Forms.Label
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents lblInstitute As System.Windows.Forms.Label
    Friend WithEvents lblQGroup As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboVoidUser As System.Windows.Forms.ComboBox
    Friend WithEvents cboCancelUser As System.Windows.Forms.ComboBox
    Friend WithEvents cboInstitute As System.Windows.Forms.ComboBox
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents cboQualification As System.Windows.Forms.ComboBox
End Class
