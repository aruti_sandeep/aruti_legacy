'************************************************************************************************************************************
'Class Name : clsDiscipline_ChargesReport.vb
'Purpose    :
'Date       : 01-Jun-2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>
''' 

Public Class clsDiscipline_ChargesReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_ChargesReport"
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintEmployeeId As Integer = 0
    'Private mstrEmployeeName As String = String.Empty
    'Private mstrReferenceno As String = String.Empty

    'S.SANDEEP [25 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Private mstrDateString As String = ""
    'S.SANDEEP [25 JUL 2016] -- START

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private mblnAddUserAccess As Boolean = True
    Private mstrFinYear As String = String.Empty
    'S.SANDEEP |11-NOV-2019| -- END

    'S.SANDEEP |27-FEB-2020| -- START
    'ISSUE/ENHANCEMENT : ALL CHARGES DISPLAYED FOR PARTICULAR EMPLOYEE
    Private mintDisciplineFileTranId As Integer = 0
    'S.SANDEEP |27-FEB-2020| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    'S.SANDEEP [25 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public WriteOnly Property _DateString() As String
        Set(ByVal value As String)
            mstrDateString = value
        End Set
    End Property
    'S.SANDEEP [25 JUL 2016] -- START



    'Public WriteOnly Property _EmployeeName() As String
    '    Set(ByVal value As String)
    '        mstrEmployeeName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ReferenceNo() As String
    '    Set(ByVal value As String)
    '        mstrReferenceno = value
    '    End Set
    'End Property

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Public WriteOnly Property _AddUserAccess() As Boolean
        Set(ByVal value As Boolean)
            mblnAddUserAccess = value
        End Set
    End Property

    Public WriteOnly Property _FinYear() As String
        Set(ByVal value As String)
            mstrFinYear = value
        End Set
    End Property
    'S.SANDEEP |11-NOV-2019| -- END

    'S.SANDEEP |27-FEB-2020| -- START
    'ISSUE/ENHANCEMENT : ALL CHARGES DISPLAYED FOR PARTICULAR EMPLOYEE
    Public WriteOnly Property _DisciplineFileTranId() As Integer
        Set(ByVal value As Integer)
            mintDisciplineFileTranId = value
        End Set
    End Property
    'S.SANDEEP |27-FEB-2020| -- END

#End Region

#Region "Public Function & Procedures "

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then

                If PrintAction = enPrintAction.Print Then
                    Dim prd As New System.Drawing.Printing.PrintDocument
                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    objRpt.PrintToPrinter(1, False, 0, 0)
                ElseIf PrintAction = enPrintAction.Preview Then
                    Dim viewer As New ArutiReportViewer()
                    viewer.crwArutiReportViewer.ReportSource = objRpt
                    viewer.crwArutiReportViewer.EnableDrillDown = False
                    viewer.Show()
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            If Not IsNothing(objRpt) Then

                If PrintAction = enPrintAction.Print Then
                    Dim prd As New System.Drawing.Printing.PrintDocument
                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    objRpt.PrintToPrinter(1, False, 0, 0)
                ElseIf PrintAction = enPrintAction.Preview Then
                    Dim viewer As New ArutiReportViewer()
                    viewer.crwArutiReportViewer.ReportSource = objRpt
                    viewer.crwArutiReportViewer.EnableDrillDown = False
                    viewer.Show()
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnAddUserAccess Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT " & _
                   "     reference_no AS RefNo " & _
                   "    ,firstname + ' ' + surname AS eName " & _
                   "    ,employeecode AS eCode " & _
                   "    ,B.job_name AS eJob " & _
                   "    ,A.branch AS eBranch " & _
                   "    ,A.zone AS eZone " & _
                   "    ,'' AS cName " & _
                   "    ,'' AS cJob " & _
                   "    ,'" & mstrFinYear & "' AS FY " & _
                   "    ,CONVERT(NVARCHAR(8),chargedate,112) AS chrgDate " & _
                   "    ,charge_description AS chrgDesc " & _
                   "    ,@Complaint+' '+ CAST(ROW_NUMBER() OVER(PARTITION BY hrdiscipline_file_tran.disciplinefileunkid ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid) AS NVARCHAR(MAX))+ ' :' AS offence " & _
                   "    ,incident_description " & _
                   "    ,hrdiscipline_file_master.userunkid " & _
                   "    ,hrdisciplinetype_master.name as offencename " & _
                   "    ,hrdiscipline_file_tran.contrarytounkid as contrarytounkid " & _
                   "    ,ISNULL(CT.name,'') AS contraryto " & _
                   " FROM hrdiscipline_file_master " & _
                   "    JOIN hrdiscipline_file_tran ON hrdiscipline_file_master.disciplinefileunkid = hrdiscipline_file_tran.disciplinefileunkid " & _
                   "    LEFT JOIN cfcommon_master AS CT ON CT.masterunkid = hrdiscipline_file_tran.contrarytounkid AND CT.mastertype = '" & clsCommon_Master.enCommonMaster.DISCIPLINE_CONTRARY_TO & "' " & _
                   "    JOIN hrdisciplinetype_master ON hrdiscipline_file_tran.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                   "    JOIN hremployee_master ON hrdiscipline_file_master.involved_employeeunkid = hremployee_master.employeeunkid "


            'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[hrdiscipline_file_tran.contrarytounkid as contrarytounkid,ISNULL(CT.name,'') AS contraryto]

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             hremployee_transfer_tran.employeeunkid " & _
                    "            ,hrclasses_master.name AS branch " & _
                    "            ,hrclassgroup_master.name AS zone " & _
                    "            ,hremployee_transfer_tran.effectivedate " & _
                    "            ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                    "        FROM hremployee_transfer_tran " & _
                    "            LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hremployee_transfer_tran.classunkid " & _
                    "            LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hremployee_transfer_tran.classgroupunkid " & _
                    "        WHERE hremployee_transfer_tran.isvoid = 0 "
            If mintEmployeeId > 0 Then StrQ &= " AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeId & "' "
            StrQ &= "        AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & mstrDateString & "' " & _
                    "    ) AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             hremployee_categorization_tran.employeeunkid " & _
                    "            ,hrjob_master.job_name " & _
                    "            ,hremployee_categorization_tran.effectivedate " & _
                    "            ,ROW_NUMBER() OVER (PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                    "        FROM hremployee_categorization_tran " & _
                    "            JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                    "        WHERE hremployee_categorization_tran.isvoid = 0 "
            If mintEmployeeId > 0 Then StrQ &= " AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeId & "' "
            StrQ &= "        AND CONVERT(CHAR(8), hremployee_categorization_tran.effectivedate, 112) <= '" & mstrDateString & "' " & _
                   "    ) AS B ON B.employeeunkid = hremployee_master.employeeunkid AND B.rno = 1 " & _
                   "WHERE hrdiscipline_file_master.isvoid = 0 AND hrdiscipline_file_tran.isvoid = 0 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mintEmployeeId > 0 Then StrQ &= " AND hrdiscipline_file_master.involved_employeeunkid = '" & mintEmployeeId & "' "

            'S.SANDEEP |27-FEB-2020| -- START
            'ISSUE/ENHANCEMENT : ALL CHARGES DISPLAYED FOR PARTICULAR EMPLOYEE
            If mintDisciplineFileTranId > 0 Then
                StrQ &= " AND hrdiscipline_file_master.disciplinefileunkid = @disciplinefileunkid "
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineFileTranId)
            End If
            'S.SANDEEP |27-FEB-2020| -- END

            objDataOperation.AddParameter("@Complaint", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 111, "Complaint"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtComp As New DataTable
            Dim objDisciplineFile As New clsDiscipline_file_master
            dtComp = objDisciplineFile.GetComplainantInformation(objDataOperation, "List")
            objDisciplineFile = Nothing
            If dtComp IsNot Nothing AndAlso dtComp.Rows.Count > 0 Then
                dtComp.AsEnumerable().ToList().ForEach(Function(x) UpdateRow(x, dsList))
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("RefNo")
                rpt_Row.Item("Column2") = dtRow.Item("eName")
                rpt_Row.Item("Column3") = dtRow.Item("eCode")
                rpt_Row.Item("Column4") = dtRow.Item("eJob")
                rpt_Row.Item("Column5") = dtRow.Item("eBranch")
                rpt_Row.Item("Column6") = dtRow.Item("eZone")
                rpt_Row.Item("Column7") = dtRow.Item("cName")
                rpt_Row.Item("Column8") = dtRow.Item("cJob")
                'S.SANDEEP |27-FEB-2020| -- START
                'ISSUE/ENHANCEMENT : On that Date of misconduct where we were picking FY. They are now saying we leave it blank
                'rpt_Row.Item("Column9") = dtRow.Item("FY")
                rpt_Row.Item("Column9") = ""
                'S.SANDEEP |27-FEB-2020| -- END
                rpt_Row.Item("Column10") = eZeeDate.convertDate(dtRow.Item("chrgDate").ToString).ToShortDateString()
                rpt_Row.Item("Column11") = dtRow.Item("chrgDesc")
                rpt_Row.Item("Column12") = dtRow.Item("offence")
                rpt_Row.Item("Column13") = dtRow.Item("incident_description")
                rpt_Row.Item("Column14") = dtRow.Item("offencename").ToString() & " " & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 131, "Contrary To") & " : " & dtRow.Item("contraryto").ToString()

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptDisciplineChargeSheet

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow

            Dim objCmp As New clsCompany_Master
            objCmp._Companyunkid = intCompanyUnkid
            Dim objImg(0) As Object
            If objCmp._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            Else
                Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If
            arrImageRow.Item("arutiLogo") = objImg(0)
            objCmp = Nothing
            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblFormDesc", Language.getMessage(mstrModuleName, 112, "FORM DISC 3"))
            ReportFunction.TextChange(objRpt, "lblChargeSheet", Language.getMessage(mstrModuleName, 113, "COMPLAINT/CHARGE SHEET"))
            ReportFunction.TextChange(objRpt, "lblEmployeeName", Language.getMessage(mstrModuleName, 114, "Employee�s Name"))
            ReportFunction.TextChange(objRpt, "lblEmployeeId", Language.getMessage(mstrModuleName, 115, "Employee�s ID No."))
            ReportFunction.TextChange(objRpt, "lblJobTitle", Language.getMessage(mstrModuleName, 116, "Job Title"))
            ReportFunction.TextChange(objRpt, "lblBranch", Language.getMessage(mstrModuleName, 117, "Branch"))
            ReportFunction.TextChange(objRpt, "lblZone", Language.getMessage(mstrModuleName, 118, "Zone"))
            ReportFunction.TextChange(objRpt, "lblComplainant", Language.getMessage(mstrModuleName, 119, "Name of the complainant"))
            ReportFunction.TextChange(objRpt, "lblCJobTitle", Language.getMessage(mstrModuleName, 116, "Job Title"))
            ReportFunction.TextChange(objRpt, "lblMisconductDate", Language.getMessage(mstrModuleName, 120, "Date of misconduct (s)"))
            ReportFunction.TextChange(objRpt, "lblChargeDate", Language.getMessage(mstrModuleName, 130, "Date charges were prepared"))
            ReportFunction.TextChange(objRpt, "lblDescrMisconduct", Language.getMessage(mstrModuleName, 121, "Description of the misconduct (s)"))
            ReportFunction.TextChange(objRpt, "lblOffences", Language.getMessage(mstrModuleName, 122, "Disciplinary offence/Breaches of the HR Policies"))
            ReportFunction.TextChange(objRpt, "lblWrittenStatement", Language.getMessage(mstrModuleName, 123, "Submission of Written Statement of Defence"))
            ReportFunction.TextChange(objRpt, "lblSubmissionValue", Language.getMessage(mstrModuleName, 124, "You are obliged to submit your defence in writing within three (3) working days from the date of receipt of this disciplinary charge sheet. Note that you may also include all evidences/documents you deem relevant in responding to the disciplinary charge sheet and send them alongside your written statement of defence. Meanwhile, you are to acknowledge receipt by signing the copy of this charge sheet and return the same to the complainant through your Line Manager/Supervisor immediately on receipt."))
            ReportFunction.TextChange(objRpt, "lblNotification", Language.getMessage(mstrModuleName, 125, "Notification details"))
            ReportFunction.TextChange(objRpt, "lblNtfDateTime", Language.getMessage(mstrModuleName, 126, "Date and Time handed to Alleged Transgressor"))
            ReportFunction.TextChange(objRpt, "lblNtfSignature", Language.getMessage(mstrModuleName, 127, "Signature of alleged transgressor"))
            ReportFunction.TextChange(objRpt, "lblNtfLineManager", Language.getMessage(mstrModuleName, 128, "Line Manager/Supervisor�s Name and Signature"))
            ReportFunction.TextChange(objRpt, "lblNtfCSignature", Language.getMessage(mstrModuleName, 129, "Complainant�s Signature"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function UpdateRow(ByVal x As DataRow, ByVal ds As DataSet) As Boolean
        Try
            Dim itmp() As DataRow = Nothing
            itmp = ds.Tables(0).Select("userunkid = '" & x("cId").ToString() & "'")
            If itmp.Length > 0 Then
                For Each ir In itmp
                    ir("cName") = x("cName").ToString()
                    ir("cJob") = x("cJob").ToString()
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP |11-NOV-2019| -- END

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [ 16 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '            "	 ROW_NUMBER() OVER(ORDER BY disciplinefileunkid) AS RCNT " & _
            '            "	,ISNULL(hrdisciplinetype_master.name,'') AS OFFENCE " & _
            '            "	,ISNULL(incident,'') AS INCIDENT " & _
            '            "	,reference_no AS REFNO " & _
            '            "	,ISNULL(CONVERT(CHAR(8),interdictdate,112),'') AS IDATE " & _
            '            "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS IPERSON " & _
            '            "	,ISNULL(employeecode,'') AS ICODE " & _
            '            "FROM hrdiscipline_file " & _
            '            "	JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
            '            "	JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid " & _
            '            "WHERE involved_employeeunkid ='" & mintEmployeeId & "' AND isvoid = 0 "

            'S.SANDEEP [25 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}

            'StrQ = "SELECT " & _
            '       "	 ROW_NUMBER() OVER(ORDER BY disciplinefileunkid) AS RCNT " & _
            '       "	,ISNULL(hrdisciplinetype_master.name,'') AS OFFENCE " & _
            '       "	,ISNULL(incident,'') AS INCIDENT " & _
            '       "	,reference_no AS REFNO " & _
            '       "	,ISNULL(CONVERT(CHAR(8),interdictdate,112),'') AS IDATE " & _
            '       "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS IPERSON " & _
            '       "	,ISNULL(employeecode,'') AS ICODE " & _
            '            "   ,CASE WHEN ISNULL(present_address1,'')<>'' THEN ISNULL(present_address1,'')+ ', ' ELSE ' ' END + ISNULL(present_address2,'') AS IADDRESS " & _
            '            "   ,ISNULL(hrjob_master.job_name,'') AS IJOB " & _
            '            "   ,ISNULL(hrclasses_master.name,'') AS ICLASS " & _
            '       "FROM hrdiscipline_file " & _
            '       "	JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
            '       "	JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid " & _
            '            "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '            "   LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
            '       "WHERE involved_employeeunkid ='" & mintEmployeeId & "' AND isvoid = 0 "

            StrQ = "SELECT " & _
                   "     ROW_NUMBER() OVER(ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid) AS RCNT " & _
                   "	,ISNULL(hrdisciplinetype_master.name,'') AS OFFENCE " & _
                   "    ,ISNULL(hrdiscipline_file_tran.incident_description,'') AS INCIDENT " & _
                   "	,reference_no AS REFNO " & _
                   "    ,ISNULL(CONVERT(CHAR(8),hrdiscipline_file_master.interdictiondate,112),'') AS IDATE " & _
                   "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS IPERSON " & _
                   "	,ISNULL(employeecode,'') AS ICODE " & _
                        "   ,CASE WHEN ISNULL(present_address1,'')<>'' THEN ISNULL(present_address1,'')+ ', ' ELSE ' ' END + ISNULL(present_address2,'') AS IADDRESS " & _
                   "    ,ISNULL(B.job_name,'') AS IJOB " & _
                   "    ,ISNULL(A.name,'') AS ICLASS " & _
                   "FROM hrdiscipline_file_master " & _
                   "    JOIN hremployee_master ON hrdiscipline_file_master.involved_employeeunkid = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hremployee_transfer_tran.employeeunkid " & _
                   "            ,hrclasses_master.name " & _
                   "            ,hremployee_transfer_tran.effectivedate " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran " & _
                   "            JOIN hrclasses_master ON hrclasses_master.classesunkid = hremployee_transfer_tran.classunkid " & _
                   "        WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & mstrDateString & "' " & _
                   "            AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeId & "' " & _
                   "    ) AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hremployee_categorization_tran.employeeunkid " & _
                   "            ,hrjob_master.job_name " & _
                   "            ,hremployee_categorization_tran.effectivedate " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                   "        FROM hremployee_categorization_tran " & _
                   "            JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                   "        WHERE hremployee_categorization_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & mstrDateString & "' " & _
                   "        AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeId & "' " & _
                   "    ) AS B ON B.employeeunkid = hremployee_master.employeeunkid AND B.rno = 1 " & _
                   "    JOIN hrdiscipline_file_tran ON hrdiscipline_file_master.disciplinefileunkid = hrdiscipline_file_tran.disciplinefileunkid " & _
                   "    JOIN hrdisciplinetype_master ON hrdiscipline_file_tran.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                   "WHERE hrdiscipline_file_master.isvoid = 0 AND hrdiscipline_file_tran.isvoid = 0 AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            'S.SANDEEP [25 JUL 2016] -- START



            'S.SANDEEP [ 16 JAN 2012 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = Language.getMessage(mstrModuleName, 6, "COUNT NO.") & " " & dtRow.Item("RCNT")
                rpt_Row.Item("Column2") = dtRow.Item("OFFENCE")
                rpt_Row.Item("Column3") = dtRow.Item("INCIDENT")

                'S.SANDEEP [ 16 JAN 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                rpt_Row.Item("Column4") = dtRow.Item("ICODE")
                rpt_Row.Item("Column5") = dtRow.Item("IPERSON")
                rpt_Row.Item("Column6") = dtRow.Item("IADDRESS")
                rpt_Row.Item("Column7") = dtRow.Item("IJOB")
                rpt_Row.Item("Column8") = dtRow.Item("ICLASS")
                'S.SANDEEP [ 16 JAN 2012 ] -- END

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptDiscipline_Charges


            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtstatementOff", Language.getMessage(mstrModuleName, 1, "STATEMENT OF THE OFFENCE :"))
            Call ReportFunction.TextChange(objRpt, "txtParticularsOff", Language.getMessage(mstrModuleName, 2, "PARTICULARS OF THE OFFENCE : "))


            'Anjan (05 June 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            Call ReportFunction.TextChange(objRpt, "txtSign", Language.getMessage(mstrModuleName, 13, "Signature of Disciplinary Authority"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 7, "Date"))
            'Anjan (05 June 2012)-End 


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 3, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 4, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", ConfigParameter._Object._CurrentDateAndTime)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", User._Object._Username)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 5, "CHARGE"))
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

            'S.SANDEEP [ 16 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 8, "Code : "))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 9, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 10, "Address : "))
            Call ReportFunction.TextChange(objRpt, "txtPosition", Language.getMessage(mstrModuleName, 11, "Position : "))
            Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 12, "Station Of Duty : "))
            'S.SANDEEP [ 16 JAN 2012 ] -- END

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "STATEMENT OF THE OFFENCE :")
            Language.setMessage(mstrModuleName, 2, "PARTICULARS OF THE OFFENCE :")
            Language.setMessage(mstrModuleName, 3, "Printed By :")
            Language.setMessage(mstrModuleName, 4, "Printed Date :")
            Language.setMessage(mstrModuleName, 5, "CHARGE")
            Language.setMessage(mstrModuleName, 6, "COUNT NO.")
            Language.setMessage(mstrModuleName, 7, "Date")
            Language.setMessage(mstrModuleName, 8, "Code :")
            Language.setMessage(mstrModuleName, 9, "Employee :")
            Language.setMessage(mstrModuleName, 10, "Address :")
            Language.setMessage(mstrModuleName, 11, "Position :")
            Language.setMessage(mstrModuleName, 12, "Station Of Duty :")
            Language.setMessage(mstrModuleName, 13, "Signature of Disciplinary Authority")
            Language.setMessage(mstrModuleName, 111, "Complaint")
            Language.setMessage(mstrModuleName, 112, "FORM DISC 3")
            Language.setMessage(mstrModuleName, 113, "COMPLAINT/CHARGE SHEET")
            Language.setMessage(mstrModuleName, 114, "Employee�s Name")
            Language.setMessage(mstrModuleName, 115, "Employee�s ID No.")
            Language.setMessage(mstrModuleName, 116, "Job Title")
            Language.setMessage(mstrModuleName, 117, "Branch")
            Language.setMessage(mstrModuleName, 118, "Zone")
            Language.setMessage(mstrModuleName, 119, "Name of the complainant")
            Language.setMessage(mstrModuleName, 120, "Date of misconduct (s)")
            Language.setMessage(mstrModuleName, 121, "Description of the misconduct (s)")
            Language.setMessage(mstrModuleName, 122, "Disciplinary offence/Breaches of the HR Policies")
            Language.setMessage(mstrModuleName, 123, "Submission of Written Statement of Defence")
            Language.setMessage(mstrModuleName, 124, "You are obliged to submit your defence in writing within three (3) working days from the date of receipt of this disciplinary charge sheet. Note that you may also include all evidences/documents you deem relevant in responding to the disciplinary charge sheet and send them alongside your written statement of defence. Meanwhile, you are to acknowledge receipt by signing the copy of this charge sheet and return the same to the complainant through your Line Manager/Supervisor immediately on receipt.")
            Language.setMessage(mstrModuleName, 125, "Notification details")
            Language.setMessage(mstrModuleName, 126, "Date and Time handed to Alleged Transgressor")
            Language.setMessage(mstrModuleName, 127, "Signature of alleged transgressor")
            Language.setMessage(mstrModuleName, 128, "Line Manager/Supervisor�s Name and Signature")
            Language.setMessage(mstrModuleName, 129, "Complainant�s Signature")
            Language.setMessage(mstrModuleName, 130, "Date charges were prepared")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
