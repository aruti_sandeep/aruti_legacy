#Region " Imports "

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Text
Imports System.Windows.Forms

Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
Imports Aruti.Data
Imports ArutiReport
#End Region


Public Class frmEmployeeHeadCount

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmEmployeeHeadCount"
    Private objHeadCount As clsEmployeeHeadCount
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 04 JAN 2014 ] -- START
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 04 JAN 2014 ] -- END

#End Region

#Region " Constructor "
    Public Sub New()
        objHeadCount = New clsEmployeeHeadCount(User._Object._Languageunkid,Company._Object._Companyunkid)
        objHeadCount.SetDefaultValue()
        InitializeComponent()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub
#End Region

#Region "Public Functions"

    Public Sub FillCombo()
        Try
            'S.SANDEEP [ 20 DEC 2013 ] -- START
            Dim dList As New DataSet
            dList = objHeadCount.GetDisplayBy
            With cboViewBy
                .ValueMember = "id"
                .DisplayMember = "iName"
                .DataSource = dList.Tables(0)
                .SelectedValue = 1
            End With
            'If rabDept.Checked = True Then
            '    lblName.Text = Language.getMessage(mstrModuleName, 1, "Department")
            '    cboName.DataSource = (New clsDepartment).getComboList(, True).Tables(0)
            '    cboName.ValueMember = "departmentunkid"
            '    cboName.DisplayMember = "name"
            'ElseIf rabClass.Checked = True Then
            '    lblName.Text = Language.getMessage(mstrModuleName, 2, "Class")
            '    cboName.DataSource = (New clsClass).getComboList(, True).Tables(0)
            '    cboName.ValueMember = "classesunkid"
            '    cboName.DisplayMember = "name"
            'ElseIf rabGrade.Checked = True Then
            '    lblName.Text = Language.getMessage(mstrModuleName, 3, "Grade")
            '    cboName.DataSource = (New clsGrade).getComboList(, True).Tables(0)
            '    cboName.ValueMember = "gradeunkid"
            '    cboName.DisplayMember = "name"
            'ElseIf rabJob.Checked = True Then
            '    lblName.Text = Language.getMessage(mstrModuleName, 4, "Job")
            '    cboName.DataSource = (New clsJobs).getComboList(, True).Tables(0)
            '    cboName.ValueMember = "jobunkid"
            '    cboName.DisplayMember = "job_name"
            'ElseIf rabSection.Checked = True Then
            '    lblName.Text = Language.getMessage(mstrModuleName, 5, "Section")
            '    cboName.DataSource = (New clsSections).getComboList(, True).Tables(0)
            '    cboName.ValueMember = "sectionunkid"
            '    cboName.DisplayMember = "name"
            'ElseIf rabStation.Checked = True Then
            '    'Vimal (30 Nov 2010) -- Start 
            '    'lblName.Text = Language.getMessage(mstrModuleName, 6, "Station")
            '    lblName.Text = Language.getMessage(mstrModuleName, 6, "Branch")
            '    'Vimal (30 Nov 2010) -- End
            '    cboName.DataSource = (New clsStation).getComboList(, True).Tables(0)
            '    cboName.ValueMember = "stationunkid"
            '    cboName.DisplayMember = "name"
            'ElseIf rabUnit.Checked = True Then
            '    lblName.Text = Language.getMessage(mstrModuleName, 7, "Unit")
            '    cboName.DataSource = (New clsUnits).getComboList(, True).Tables(0)
            '    cboName.ValueMember = "unitunkid"
            '    cboName.DisplayMember = "name"
            '    'S.SANDEEP [ 16 NOV 2013 ] -- START
            'ElseIf radNationality.Checked = True Then
            '    lblName.Text = radNationality.Text
            '    Dim objCountry As New clsMasterData
            '    Dim dsList As New DataSet
            '    dsList = objCountry.getCountryList("List", True)
            '    With cboName
            '        .DataSource = dsList.Tables("List")
            '        .ValueMember = "countryunkid"
            '        .DisplayMember = "country_name"
            '        .SelectedValue = 0
            '    End With
            '    dsList.Dispose()
            '    objCountry = Nothing
            'ElseIf radReligion.Checked = True Then
            '    lblName.Text = radReligion.Text
            '    Dim objReligion As New clsCommon_Master
            '    Dim dsList As New DataSet
            '    dsList = objReligion.getComboList(clsCommon_Master.enCommonMaster.RELIGION, True, True)
            '    With cboName
            '        .DataSource = dsList.Tables(0)
            '        .ValueMember = "masterunkid"
            '        .DisplayMember = "name"
            '        .SelectedValue = 0
            '    End With
            '    dsList.Dispose()
            '    objReligion = Nothing
            'ElseIf radEmploymentType.Checked = True Then
            '    lblName.Text = radEmploymentType.Text
            '    Dim objEmplType As New clsCommon_Master
            '    Dim dsList As New DataSet
            '    dsList = objEmplType.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, True)
            '    With cboName
            '        .DataSource = dsList.Tables(0)
            '        .ValueMember = "masterunkid"
            '        .DisplayMember = "name"
            '        .SelectedValue = 0
            '    End With
            '    dsList.Dispose()
            '    objEmplType = Nothing
            '    'S.SANDEEP [ 16 NOV 2013 ] -- END
            'End If
            'S.SANDEEP [ 20 DEC 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            rabClass.Checked = False
            rabStation.Checked = False
            rabDept.Checked = True
            rabUnit.Checked = False
            rabGrade.Checked = False
            rabJob.Checked = False
            rabSection.Checked = False
            'Sohail (26 Nov 2010) -- Start
            cboName.SelectedValue = 0
            txtTotalFemaleFrom.Text = ""
            txtTotalFemaleTo.Text = ""
            txtTotalMaleFrom.Text = ""
            txtTotalMaleTo.Text = ""
            txtTotalHedsFrom.Text = ""
            txtTotalHedsTo.Text = ""
            'Sohail (26 Nov 2010) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 29 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkIncludeNAEmp.Checked = False
            'S.SANDEEP [ 29 JAN 2013 ] -- END

            chkShowChartDept.Checked = False 'Sohail (16 Apr 2012)
            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [ 04 JAN 2014 ] -- START
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'S.SANDEEP [ 04 JAN 2014 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objHeadCount.SetDefaultValue()

            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'If rabStation.Checked = True Then
            '    objHeadCount._Employee = True

            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = rabStation.Text
            'ElseIf rabDept.Checked = True Then
            '    objHeadCount._Department = True

            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = rabDept.Text
            'ElseIf rabClass.Checked = True Then
            '    objHeadCount._Class = True

            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = rabClass.Text
            'ElseIf rabUnit.Checked = True Then
            '    objHeadCount._CostCenter = True

            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = rabUnit.Text
            'ElseIf rabGrade.Checked = True Then
            '    objHeadCount._Grade = True

            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = rabGrade.Text
            'ElseIf rabJob.Checked = True Then
            '    objHeadCount._Job = True

            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = rabJob.Text
            'ElseIf rabSection.Checked = True Then
            '    objHeadCount._Section = True
            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = rabSection.Text
            '    'S.SANDEEP [ 16 NOV 2013 ] -- START
            'ElseIf radNationality.Checked = True Then
            '    objHeadCount._Nationality = True
            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = radNationality.Text
            'ElseIf radReligion.Checked = True Then
            '    objHeadCount._Religion = True
            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = radReligion.Text
            'ElseIf radEmploymentType.Checked = True Then
            '    objHeadCount._EmploymentType = True
            '    objHeadCount._Id = cboName.SelectedValue
            '    objHeadCount._Name = cboName.Text
            '    objHeadCount._Report_Type_Name = radEmploymentType.Text
            '    'S.SANDEEP [ 16 NOV 2013 ] -- END
            'End If
            objHeadCount._Id = cboName.SelectedValue
            objHeadCount._Name = cboName.Text
            objHeadCount._Report_Type_Name = cboViewBy.Text
            objHeadCount._DisplayViewIdx = cboViewBy.SelectedValue
            'S.SANDEEP [ 20 DEC 2013 ] -- END



            'Sohail (26 Nov 2010) -- Start
            objHeadCount._FemaleFrom = txtTotalFemaleFrom.Decimal
            objHeadCount._MaleFrom = txtTotalMaleFrom.Decimal
            objHeadCount._HeadFrom = txtTotalHedsFrom.Decimal
            objHeadCount._FemaleTo = txtTotalFemaleTo.Decimal
            objHeadCount._MaleTo = txtTotalMaleTo.Decimal
            objHeadCount._HeadTo = txtTotalHedsTo.Decimal
            'Sohail (26 Nov 2010) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objHeadCount._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 29 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objHeadCount._IsNAIncluded = chkIncludeNAEmp.Checked
            'S.SANDEEP [ 29 JAN 2013 ] -- END

            objHeadCount._ShowChartDept = chkShowChartDept.Checked 'Sohail (16 Apr 2012)

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objHeadCount._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'S.SANDEEP [ 04 JAN 2014 ] -- START
            objHeadCount._ViewByIds = mstrStringIds
            objHeadCount._ViewIndex = mintViewIdx
            objHeadCount._ViewByName = mstrStringName
            objHeadCount._Analysis_Fields = mstrAnalysis_Fields
            objHeadCount._Analysis_Join = mstrAnalysis_Join
            objHeadCount._Analysis_OrderBy = mstrAnalysis_OrderBy
            objHeadCount._Report_GroupName = mstrReport_GroupName
            'S.SANDEEP [ 04 JAN 2014 ] -- END


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


#End Region

#Region "Form's Events"

    Private Sub frmHeadCountReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objHeadCount = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHeadCountReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHeadCountReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objHeadCount._ReportName
            Me._Message = objHeadCount._ReportDesc

            Call ResetValue()
            Call FillCombo()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHeadCountReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHeadCountReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmHeadCountReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHeadCountReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHeadCountReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub frmHeadCountReport_Report_Click(ByVal sender As System.Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles MyBase.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objHeadCount.generateReport(0, e.Type, enExportAction.None)
            objHeadCount.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHeadCountReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHeadCountReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objHeadCount.generateReport(0, enPrintAction.None, e.Type)
            objHeadCount.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHeadCountReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHeadCountReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHeadCountReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHeadCountReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHeadCountReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeHeadCount.SetMessages()
            objfrm._Other_ModuleNames = "clsHeadCountReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'S.SANDEEP [ 20 DEC 2013 ] -- START
    Private Sub objbtnSearchFilterType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFilterType.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboViewBy.ValueMember
                .DisplayMember = cboViewBy.DisplayMember
                .DataSource = cboViewBy.DataSource
                If .DisplayDialog Then
                    cboViewBy.SelectedValue = .SelectedValue
                    cboViewBy.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFilterType_Click", mstrModuleName)
        Finally
            If frm Is Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchViewBy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchViewBy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboName.ValueMember
                .DisplayMember = cboName.DisplayMember
                .DataSource = cboName.DataSource
                If .DisplayDialog Then
                    cboName.SelectedValue = .SelectedValue
                    cboName.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchViewBy_Click", mstrModuleName)
        Finally
            If frm Is Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 20 DEC 2013 ] -- END

#End Region

    'S.SANDEEP [ 20 DEC 2013 ] -- START
#Region " Control's Events "

    'S.SANDEEP [ 04 JAN 2014 ] -- START
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 04 JAN 2014 ] -- END


    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            lblName.Text = cboViewBy.Text
            Select Case CInt(cboViewBy.SelectedValue)
                Case 1  'BRANCH
                    cboName.DataSource = (New clsStation).getComboList(, True).Tables(0)
                    cboName.ValueMember = "stationunkid"
                    cboName.DisplayMember = "name"
                Case 2  'DEPARTMENT GROUP
                    cboName.DataSource = (New clsDepartmentGroup).getComboList("list", True).Tables(0)
                    cboName.ValueMember = "deptgroupunkid"
                    cboName.DisplayMember = "name"
                Case 3  'DEPARTMENT
                    cboName.DataSource = (New clsDepartment).getComboList(, True).Tables(0)
                    cboName.ValueMember = "departmentunkid"
                    cboName.DisplayMember = "name"
                Case 4  'SECTION GROUP
                    cboName.DataSource = (New clsSectionGroup).getComboList(, True).Tables(0)
                    cboName.ValueMember = "sectiongroupunkid"
                    cboName.DisplayMember = "name"
                Case 5  'SECTION
                    cboName.DataSource = (New clsSections).getComboList(, True).Tables(0)
                    cboName.ValueMember = "sectionunkid"
                    cboName.DisplayMember = "name"
                Case 6  'UNIT GROUP
                    cboName.DataSource = (New clsUnitGroup).getComboList(, True).Tables(0)
                    cboName.ValueMember = "unitgroupunkid"
                    cboName.DisplayMember = "name"
                Case 7  'UNIT
                    cboName.DataSource = (New clsUnits).getComboList(, True).Tables(0)
                    cboName.ValueMember = "unitunkid"
                    cboName.DisplayMember = "name"
                Case 8  'TEAM
                    cboName.DataSource = (New clsTeams).getComboList(, True).Tables(0)
                    cboName.ValueMember = "teamunkid"
                    cboName.DisplayMember = "name"
                Case 9  'JOB GROUP
                    cboName.DataSource = (New clsJobGroup).getComboList(, True).Tables(0)
                    cboName.ValueMember = "jobgroupunkid"
                    cboName.DisplayMember = "name"
                Case 10 'JOB
                    cboName.DataSource = (New clsJobs).getComboList(, True).Tables(0)
                    cboName.ValueMember = "jobunkid"
                    cboName.DisplayMember = "name"
                Case 11 'GRADE GROUP
                    cboName.DataSource = (New clsGradeGroup).getComboList(, True).Tables(0)
                    cboName.ValueMember = "gradegroupunkid"
                    cboName.DisplayMember = "name"
                Case 12 'GRADE
                    cboName.DataSource = (New clsGrade).getComboList(, True).Tables(0)
                    cboName.ValueMember = "gradeunkid"
                    cboName.DisplayMember = "name"
                Case 13 'GRADE LEVEL
                    cboName.DataSource = (New clsGradeLevel).getComboList(, True).Tables(0)
                    cboName.ValueMember = "gradelevelunkid"
                    cboName.DisplayMember = "name"
                Case 14 'CLASS GROUP
                    cboName.DataSource = (New clsClassGroup).getComboList(, True).Tables(0)
                    cboName.ValueMember = "classgroupunkid"
                    cboName.DisplayMember = "name"
                Case 15 'CLASS
                    cboName.DataSource = (New clsClass).getComboList(, True).Tables(0)
                    cboName.ValueMember = "classesunkid"
                    cboName.DisplayMember = "name"
                Case 16 'COSTCENTER
                    cboName.DataSource = (New clscostcenter_master).getComboList(, True).Tables(0)
                    cboName.ValueMember = "costcenterunkid"
                    cboName.DisplayMember = "costcentername"
                Case 17 'RELEGION
                    cboName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELIGION, True, True).Tables(0)
                    cboName.ValueMember = "masterunkid"
                    cboName.DisplayMember = "name"
                Case 18 'NATIONALITY
                    cboName.DataSource = (New clsMasterData).getCountryList("List", True).Tables(0)
                    cboName.ValueMember = "countryunkid"
                    cboName.DisplayMember = "country_name"
                Case 19 'EMPLOYMENT TYPE
                    cboName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, True).Tables(0)
                    cboName.ValueMember = "masterunkid"
                    cboName.DisplayMember = "name"
                Case 20 'MARITAL STATUS
                    cboName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, True).Tables(0)
                    cboName.ValueMember = "masterunkid"
                    cboName.DisplayMember = "name"
                Case 21 'LANGUAGE
                    cboName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, True).Tables(0)
                    cboName.ValueMember = "masterunkid"
                    cboName.DisplayMember = "name"
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    '    'Sohail (26 Nov 2010) -- Start
    '#Region " Radio Button's Events "
    '    Private Sub rabDept_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabDept.CheckedChanged, rabClass.CheckedChanged, rabGrade.CheckedChanged, rabJob.CheckedChanged, rabSection.CheckedChanged, rabStation.CheckedChanged, rabUnit.CheckedChanged, _
    '                radNationality.CheckedChanged, radReligion.CheckedChanged, radEmploymentType.CheckedChanged 'S.SANDEEP [ 16 NOV 2013 ] -- START {radNationality,radReligion,radEmploymentType} -- END
    '        Try
    '            Call FillCombo()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabDept_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '#End Region
    '    'Sohail (26 Nov 2010) -- End
#End Region
    'S.SANDEEP [ 20 DEC 2013 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterType.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterType.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterType.Text = Language._Object.getCaption(Me.gbFilterType.Name, Me.gbFilterType.Text)
			Me.rabUnit.Text = Language._Object.getCaption(Me.rabUnit.Name, Me.rabUnit.Text)
			Me.rabClass.Text = Language._Object.getCaption(Me.rabClass.Name, Me.rabClass.Text)
			Me.rabGrade.Text = Language._Object.getCaption(Me.rabGrade.Name, Me.rabGrade.Text)
			Me.rabJob.Text = Language._Object.getCaption(Me.rabJob.Name, Me.rabJob.Text)
			Me.rabStation.Text = Language._Object.getCaption(Me.rabStation.Name, Me.rabStation.Text)
			Me.rabDept.Text = Language._Object.getCaption(Me.rabDept.Name, Me.rabDept.Text)
			Me.rabSection.Text = Language._Object.getCaption(Me.rabSection.Name, Me.rabSection.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTotalFemaleTo.Text = Language._Object.getCaption(Me.lblTotalFemaleTo.Name, Me.lblTotalFemaleTo.Text)
			Me.lblTotalFemaleFrom.Text = Language._Object.getCaption(Me.lblTotalFemaleFrom.Name, Me.lblTotalFemaleFrom.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblTHTo.Text = Language._Object.getCaption(Me.lblTHTo.Name, Me.lblTHTo.Text)
			Me.lblTotalHeadsFrom.Text = Language._Object.getCaption(Me.lblTotalHeadsFrom.Name, Me.lblTotalHeadsFrom.Text)
            Me.lblMaleTo.Text = Language._Object.getCaption(Me.lblMaleTo.Name, Me.lblMaleTo.Text)
			Me.lblTotalMaleFrom.Text = Language._Object.getCaption(Me.lblTotalMaleFrom.Name, Me.lblTotalMaleFrom.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.chkShowChartDept.Text = Language._Object.getCaption(Me.chkShowChartDept.Name, Me.chkShowChartDept.Text)
			Me.chkIncludeNAEmp.Text = Language._Object.getCaption(Me.chkIncludeNAEmp.Name, Me.chkIncludeNAEmp.Text)
			Me.radEmploymentType.Text = Language._Object.getCaption(Me.radEmploymentType.Name, Me.radEmploymentType.Text)
			Me.radReligion.Text = Language._Object.getCaption(Me.radReligion.Name, Me.radReligion.Text)
			Me.radNationality.Text = Language._Object.getCaption(Me.radNationality.Name, Me.radNationality.Text)
			Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class
