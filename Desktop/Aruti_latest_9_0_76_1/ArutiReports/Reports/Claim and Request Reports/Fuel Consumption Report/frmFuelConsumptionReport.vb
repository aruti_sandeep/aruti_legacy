'************************************************************************************************************************************
'Class Name : frmFuelConsumptionReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFuelConsumptionReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFuelConsumptionReport"
    Private objFuelConsumption As clsFuelConsumptionReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Contructor "

    Public Sub New()
        objFuelConsumption = New clsFuelConsumptionReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objFuelConsumption.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try

        
            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, False, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
            End With

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, False, False, False)
            Dim objExpenseCategory As New clsexpense_category_master
            dsCombos = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True, False, False, False)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End



            With cboExpenseCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
            End With


            FillStatus()

            dsCombos = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUOM
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            'Dim objPeriod As New clscommom_period_Tran
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            'RemoveHandler cboFromPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged
            'With cboFromPeriod
            '    .ValueMember = "periodunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables(0)
            '    .SelectedValue = 0
            'End With
            'AddHandler cboFromPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged

            'RemoveHandler cboToPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged
            'With cboToPeriod
            '    .ValueMember = "periodunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables(0).Copy
            '    .SelectedValue = 0
            'End With
            'AddHandler cboToPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged
            'objPeriod = Nothing



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            ObjEmp = Nothing
            ObjMaster = Nothing
        End Try
    End Sub

    Private Sub FillStatus()
        Dim objMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try

            dsCombos = objMasterData.getLeaveStatusList("List", "")

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombos.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "name"
                .DataSource = dtab
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillStatus", mstrModuleName)
        Finally
            objMasterData = Nothing
            dsCombos.Clear()
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpTranFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTranToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTranFromDate.Checked = False
            dtpTranToDate.Checked = False
            cboExpenseCategory.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboUOM.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            cboExpense.SelectedValue = 0
            chkShowRequestedQty.Checked = True
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            objFuelConsumption.setDefaultOrderBy(0)
            txtOrderBy.Text = objFuelConsumption.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objFuelConsumption.SetDefaultValue()

            If (dtpTranFromDate.Checked = False AndAlso dtpTranToDate.Checked = False) OrElse (dtpTranFromDate.Checked = True AndAlso dtpTranToDate.Checked = False) OrElse (dtpTranFromDate.Checked = False AndAlso dtpTranToDate.Checked = True) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue."), enMsgBoxStyle.Information)
                Return False

            ElseIf CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense Category is compulsory information.please select Expense Category."), enMsgBoxStyle.Information)
                cboExpenseCategory.Select()
                Return False
            End If


            objFuelConsumption._FromDate = IIf(dtpTranFromDate.Checked = True, dtpTranFromDate.Value.Date, Nothing)
            objFuelConsumption._ToDate = IIf(dtpTranToDate.Checked = True, dtpTranToDate.Value.Date, Nothing)
            objFuelConsumption._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            objFuelConsumption._ExpCateName = cboExpenseCategory.Text.ToString()
            objFuelConsumption._EmpUnkId = CInt(cboEmployee.SelectedValue)
            objFuelConsumption._EmpName = cboEmployee.Text.ToString
            objFuelConsumption._UOMId = CInt(cboUOM.SelectedValue)
            objFuelConsumption._UOM = cboUOM.Text
            objFuelConsumption._ExpenseID = CInt(cboExpense.SelectedValue)
            objFuelConsumption._Expense = cboExpense.Text
            objFuelConsumption._StatusId = CInt(cboStatus.SelectedValue)
            objFuelConsumption._StatusName = cboStatus.Text.ToString
            objFuelConsumption._ShowRequiredQuanitty = chkShowRequestedQty.Checked
            objFuelConsumption._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objFuelConsumption._UserUnkid = User._Object._Userunkid
            objFuelConsumption._CompanyUnkId = Company._Object._Companyunkid
            objFuelConsumption._ViewByIds = mstrStringIds
            objFuelConsumption._ViewIndex = mintViewIdx
            objFuelConsumption._ViewByName = mstrStringName
            objFuelConsumption._Analysis_Fields = mstrAnalysis_Fields
            objFuelConsumption._Analysis_Join = mstrAnalysis_Join
            objFuelConsumption._Analysis_OrderBy = mstrAnalysis_OrderBy
            objFuelConsumption._Report_GroupName = mstrReport_GroupName
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmFuelConsumptionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objFuelConsumption = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmFuelConsumptionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFuelConsumptionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()
            Me._Title = objFuelConsumption._ReportName
            Me._Message = objFuelConsumption._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFuelConsumptionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFuelConsumptionReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFuelConsumptionReport.SetMessages()
            objfrm._Other_ModuleNames = "clsFuelConsumptionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmFuelConsumptionReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            objFuelConsumption.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
         
            objFuelConsumption.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchStatus.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboStatus.DataSource
            frm.ValueMember = cboStatus.ValueMember
            frm.DisplayMember = cboStatus.DisplayMember
            If frm.DisplayDialog Then
                cboStatus.SelectedValue = frm.SelectedValue
                cboStatus.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSector_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpenseCategory.DataSource
            frm.ValueMember = cboExpenseCategory.ValueMember
            frm.DisplayMember = cboExpenseCategory.DisplayMember
            If frm.DisplayDialog Then
                cboExpenseCategory.SelectedValue = frm.SelectedValue
                cboExpenseCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objFuelConsumption.setOrderBy(0)
            txtOrderBy.Text = objFuelConsumption.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpense.DataSource
            frm.ValueMember = cboExpense.ValueMember
            frm.DisplayMember = cboExpense.DisplayMember
            If frm.DisplayDialog Then
                cboExpense.SelectedValue = frm.SelectedValue
                cboExpense.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objExpense = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTranFromDate.Text = Language._Object.getCaption(Me.lblTranFromDate.Name, Me.lblTranFromDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
			Me.lblTranToDate.Text = Language._Object.getCaption(Me.lblTranToDate.Name, Me.lblTranToDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.LblUOM.Text = Language._Object.getCaption(Me.LblUOM.Name, Me.LblUOM.Text)
			Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.Name, Me.LblExpense.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkShowRequestedQty.Text = Language._Object.getCaption(Me.chkShowRequestedQty.Name, Me.chkShowRequestedQty.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue.")
			Language.setMessage(mstrModuleName, 2, "Expense Category is compulsory information.please select Expense Category.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
