﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFuelConsumptionReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowRequestedQty = New System.Windows.Forms.CheckBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchExpense = New eZee.Common.eZeeGradientButton
        Me.LblExpense = New System.Windows.Forms.Label
        Me.cboExpense = New System.Windows.Forms.ComboBox
        Me.cboUOM = New System.Windows.Forms.ComboBox
        Me.LblUOM = New System.Windows.Forms.Label
        Me.dtpTranToDate = New System.Windows.Forms.DateTimePicker
        Me.lblTranToDate = New System.Windows.Forms.Label
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.LblExpenseCategory = New System.Windows.Forms.Label
        Me.cboExpenseCategory = New System.Windows.Forms.ComboBox
        Me.objbtnSearchStatus = New eZee.Common.eZeeGradientButton
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.dtpTranFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblTranFromDate = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 437)
        Me.NavPanel.Size = New System.Drawing.Size(573, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowRequestedQty)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExpense)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpense)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpense)
        Me.gbFilterCriteria.Controls.Add(Me.cboUOM)
        Me.gbFilterCriteria.Controls.Add(Me.LblUOM)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTranToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranToDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCategory)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpenseCategory)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpenseCategory)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTranFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(418, 221)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowRequestedQty
        '
        Me.chkShowRequestedQty.Checked = True
        Me.chkShowRequestedQty.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowRequestedQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowRequestedQty.Location = New System.Drawing.Point(122, 197)
        Me.chkShowRequestedQty.Name = "chkShowRequestedQty"
        Me.chkShowRequestedQty.Size = New System.Drawing.Size(239, 17)
        Me.chkShowRequestedQty.TabIndex = 102
        Me.chkShowRequestedQty.Text = "Show Requested Quantity"
        Me.chkShowRequestedQty.UseVisualStyleBackColor = True
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(320, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 22
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchExpense
        '
        Me.objbtnSearchExpense.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExpense.BorderSelected = False
        Me.objbtnSearchExpense.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExpense.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExpense.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExpense.Location = New System.Drawing.Point(394, 86)
        Me.objbtnSearchExpense.Name = "objbtnSearchExpense"
        Me.objbtnSearchExpense.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExpense.TabIndex = 100
        '
        'LblExpense
        '
        Me.LblExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpense.Location = New System.Drawing.Point(9, 89)
        Me.LblExpense.Name = "LblExpense"
        Me.LblExpense.Size = New System.Drawing.Size(105, 15)
        Me.LblExpense.TabIndex = 98
        Me.LblExpense.Text = "Expense"
        Me.LblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpense
        '
        Me.cboExpense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpense.FormattingEnabled = True
        Me.cboExpense.Location = New System.Drawing.Point(122, 86)
        Me.cboExpense.Name = "cboExpense"
        Me.cboExpense.Size = New System.Drawing.Size(265, 21)
        Me.cboExpense.TabIndex = 99
        '
        'cboUOM
        '
        Me.cboUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUOM.FormattingEnabled = True
        Me.cboUOM.Location = New System.Drawing.Point(122, 140)
        Me.cboUOM.Name = "cboUOM"
        Me.cboUOM.Size = New System.Drawing.Size(265, 21)
        Me.cboUOM.TabIndex = 97
        '
        'LblUOM
        '
        Me.LblUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUOM.Location = New System.Drawing.Point(9, 142)
        Me.LblUOM.Name = "LblUOM"
        Me.LblUOM.Size = New System.Drawing.Size(105, 15)
        Me.LblUOM.TabIndex = 95
        Me.LblUOM.Text = "UOM"
        Me.LblUOM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpTranToDate
        '
        Me.dtpTranToDate.Checked = False
        Me.dtpTranToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTranToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTranToDate.Location = New System.Drawing.Point(283, 33)
        Me.dtpTranToDate.Name = "dtpTranToDate"
        Me.dtpTranToDate.ShowCheckBox = True
        Me.dtpTranToDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpTranToDate.TabIndex = 91
        '
        'lblTranToDate
        '
        Me.lblTranToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranToDate.Location = New System.Drawing.Point(233, 36)
        Me.lblTranToDate.Name = "lblTranToDate"
        Me.lblTranToDate.Size = New System.Drawing.Size(47, 15)
        Me.lblTranToDate.TabIndex = 90
        Me.lblTranToDate.Text = "To"
        Me.lblTranToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(394, 59)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 88
        '
        'LblExpenseCategory
        '
        Me.LblExpenseCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpenseCategory.Location = New System.Drawing.Point(9, 62)
        Me.LblExpenseCategory.Name = "LblExpenseCategory"
        Me.LblExpenseCategory.Size = New System.Drawing.Size(105, 15)
        Me.LblExpenseCategory.TabIndex = 86
        Me.LblExpenseCategory.Text = "Exp. Category"
        Me.LblExpenseCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpenseCategory
        '
        Me.cboExpenseCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpenseCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpenseCategory.FormattingEnabled = True
        Me.cboExpenseCategory.Location = New System.Drawing.Point(122, 59)
        Me.cboExpenseCategory.Name = "cboExpenseCategory"
        Me.cboExpenseCategory.Size = New System.Drawing.Size(265, 21)
        Me.cboExpenseCategory.TabIndex = 87
        '
        'objbtnSearchStatus
        '
        Me.objbtnSearchStatus.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchStatus.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchStatus.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchStatus.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchStatus.BorderSelected = False
        Me.objbtnSearchStatus.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchStatus.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchStatus.Location = New System.Drawing.Point(394, 167)
        Me.objbtnSearchStatus.Name = "objbtnSearchStatus"
        Me.objbtnSearchStatus.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchStatus.TabIndex = 84
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(9, 170)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(105, 15)
        Me.lblStatus.TabIndex = 82
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(122, 167)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(265, 21)
        Me.cboStatus.TabIndex = 83
        '
        'dtpTranFromDate
        '
        Me.dtpTranFromDate.Checked = False
        Me.dtpTranFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTranFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTranFromDate.Location = New System.Drawing.Point(122, 33)
        Me.dtpTranFromDate.Name = "dtpTranFromDate"
        Me.dtpTranFromDate.ShowCheckBox = True
        Me.dtpTranFromDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpTranFromDate.TabIndex = 81
        '
        'lblTranFromDate
        '
        Me.lblTranFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranFromDate.Location = New System.Drawing.Point(9, 36)
        Me.lblTranFromDate.Name = "lblTranFromDate"
        Me.lblTranFromDate.Size = New System.Drawing.Size(105, 15)
        Me.lblTranFromDate.TabIndex = 79
        Me.lblTranFromDate.Text = "From Date"
        Me.lblTranFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(394, 113)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 116)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(105, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(122, 113)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(265, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 294)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(418, 63)
        Me.gbSortBy.TabIndex = 19
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(393, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(88, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(122, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(264, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmFuelConsumptionReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(573, 492)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFuelConsumptionReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Claim Request Summary Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTranFromDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents dtpTranFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents LblExpenseCategory As System.Windows.Forms.Label
    Friend WithEvents cboExpenseCategory As System.Windows.Forms.ComboBox
    Friend WithEvents dtpTranToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTranToDate As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchStatus As eZee.Common.eZeeGradientButton
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents LblUOM As System.Windows.Forms.Label
    Friend WithEvents cboUOM As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchExpense As eZee.Common.eZeeGradientButton
    Friend WithEvents LblExpense As System.Windows.Forms.Label
    Friend WithEvents cboExpense As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShowRequestedQty As System.Windows.Forms.CheckBox
End Class
