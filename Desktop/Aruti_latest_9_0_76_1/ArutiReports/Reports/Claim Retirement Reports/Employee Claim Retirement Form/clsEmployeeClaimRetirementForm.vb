
Imports eZeeCommonLib
Imports Aruti.Data
Imports System

Public Class clsEmployeeClaimRetirementForm
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeClaimRetirementForm"
    Private mstrReportId As String = enArutiReport.Employee_Claim_Retirement_Form
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintExpenseCategoryID As Integer = 0
    Private mstrExpenseCategory As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintClaimRetirementFormId As Integer = 0
    Private mstrClaimRetirementFormNo As String = ""
    Private mstrSpecialAllowanceIds As String = ""
    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintYearId As Integer = 0
    Private mdtFinStartDate As DateTime = Nothing
    Private mdtFinEndDate As DateTime = Nothing
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private mintLeaveBalanceSetting As Integer = 0
    Private mintLeaveAccrueTenureSetting As Integer = 0
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    Private mintPeriodID As Integer = 0
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    Private mdtClaimDate As Date = Nothing
    Private mblnShowEmpScale As Boolean = False
    Private mblnIsPrinted As Boolean = False
    Private mdtPrintedDateTime As DateTime = Nothing
    Private mintPrintUserId As Integer = 0
    Private mstrPrintedIp As String = ""
    Private mstrPrintedHost As String = ""
    Private mblnIsPrintFromWeb As Boolean = False
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategoryId() As Integer
        Set(ByVal value As Integer)
            mintExpenseCategoryID = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategory() As String
        Set(ByVal value As String)
            mstrExpenseCategory = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ClaimRetirementFormId() As Integer
        Set(ByVal value As Integer)
            mintClaimRetirementFormId = value
        End Set
    End Property

    Public WriteOnly Property _SpecialAllowanceIds() As String
        Set(ByVal value As String)
            mstrSpecialAllowanceIds = value
        End Set
    End Property

    Public WriteOnly Property _ClaimRetirementFormNo() As String
        Set(ByVal value As String)
            mstrClaimRetirementFormNo = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _Fin_StartDate() As Date
        Set(ByVal value As Date)
            mdtFinStartDate = value
        End Set
    End Property

    Public WriteOnly Property _Fin_Enddate() As Date
        Set(ByVal value As Date)
            mdtFinEndDate = value
        End Set
    End Property

    Public WriteOnly Property _PaymentApprovalwithLeaveApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    Public WriteOnly Property _PeiordID() As Integer
        Set(ByVal value As Integer)
            mintPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property

    Public Property _IsPrinted() As Boolean
        Get
            Return mblnIsPrinted
        End Get
        Set(ByVal value As Boolean)
            mblnIsPrinted = value
        End Set
    End Property

    Public Property _PrintedDateTime() As DateTime
        Get
            Return mdtPrintedDateTime
        End Get
        Set(ByVal value As DateTime)
            mdtPrintedDateTime = value
        End Set
    End Property

    Public Property _PrintUserId() As Integer
        Get
            Return mintPrintUserId
        End Get
        Set(ByVal value As Integer)
            mintPrintUserId = value
        End Set
    End Property

    Public Property _PrintedIp() As String
        Get
            Return mstrPrintedIp
        End Get
        Set(ByVal value As String)
            mstrPrintedIp = value
        End Set
    End Property

    Public Property _PrintedHost() As String
        Get
            Return mstrPrintedHost
        End Get
        Set(ByVal value As String)
            mstrPrintedHost = value
        End Set
    End Property

    Public Property _IsPrintFromWeb() As Boolean
        Get
            Return mblnIsPrintFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsPrintFromWeb = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintExpenseCategoryID = 0
            mstrExpenseCategory = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintClaimRetirementFormId = 0
            mstrClaimRetirementFormNo = ""
            mstrSpecialAllowanceIds = ""
            mstrOrderByQuery = ""
            mintYearId = 0
            mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year
            mdtFinEndDate = Nothing
            mdtFinEndDate = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintClaimRetirementFormId > 0 Then
                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRetirementFormId)
                Me._FilterQuery &= " AND cmclaim_request_master.crmasterunkid  = @crmasterunkid "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid  = @EmployeeId "
            End If

            If mintExpenseCategoryID > 0 Then
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
                Me._FilterQuery &= " AND  cmclaim_request_master.expensetypeid = @expensetypeid "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Try


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                               , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date _
                                                               , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String _
                                                               , ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                               , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                               , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid

            Dim mintCountryId As Integer = Company._Object._Countryunkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            mintUserUnkid = xUserUnkid
            User._Object._Userunkid = mintUserUnkid

            objRpt = GenerateEmployee_ClaimRetirementForm(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Private Function GenerateEmployee_ClaimRetirementForm(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_ClaimDetail As ArutiReport.Designer.dsArutiReport
        Dim rpt_ClaimApprovers As ArutiReport.Designer.dsArutiReport
        Dim mdtRequestDate As DateTime = Nothing
        Dim mstrClaimRemark As String = ""


        Dim strGrpName As String = String.Empty
        Dim objGrp As New clsGroup_Master
        objGrp._Groupunkid = 1
        strGrpName = objGrp._Groupname
        objGrp = Nothing

        Try

            objDataOperation = New clsDataOperation


            StrQ = " SELECT  cmclaim_request_master.employeeunkid " & _
                      ",cmclaim_request_master.crmasterunkid " & _
                      ",employeecode " & _
                      ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
                      ",cmclaim_request_master.claimrequestno " & _
                      ",cmclaim_request_master.transactiondate " & _
                      ",cmclaim_request_master.claim_remark " & _
                      ",hrjob_master.job_name AS jobtitle " & _
                      ",hrgrade_master.name AS grade " & _
                      ",hrdepartment_master.name AS department " & _
                      ",hrsectiongroup_master.name AS sectiongrp " & _
                      ",hrclassgroup_master.name AS classgrp " & _
                      ",hrclasses_master.name AS class " & _
                      " ,hremployee_master.appointeddate " & _
                      ",hremployee_master.confirmation_date " & _
                      " FROM cmclaim_request_master " & _
                      " JOIN hremployee_master ON cmclaim_request_master.employeeunkid = hremployee_master.employeeunkid " & _
                      " JOIN " & _
                        " ( " & _
                        "         SELECT " & _
                        "         jobunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                        " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                       " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                       " JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "        gradeunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                       "    FROM prsalaryincrement_tran " & _
                       "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                       ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                       " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                       " JOIN " & _
                               " ( " & _
                               "    SELECT " & _
                               "         departmentunkid " & _
                               "        ,sectiongroupunkid " & _
                               "        ,classgroupunkid " & _
                               "        ,classunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                               "    FROM hremployee_transfer_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                               " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                      " LEFT JOIN hrsectiongroup_master ON  hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid " & _
                      " LEFT JOIN hrclassgroup_master ON  hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid " & _
                      " LEFT JOIN hrclasses_master ON  hrclasses_master.classesunkid = Alloc.classunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         date1 " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_dates_tran " & _
                      "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                      " WHERE cmclaim_request_master.isvoid = 0 "


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_ClaimDetail = New ArutiReport.Designer.dsArutiReport
            rpt_ClaimApprovers = New ArutiReport.Designer.dsArutiReport


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow()

                rpt_Rows.Item("Column1") = dtRow.Item("claimrequestno")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
                rpt_Rows.Item("Column5") = dtRow.Item("department")
                rpt_Rows.Item("Column10") = dtRow.Item("sectiongrp")
                rpt_Rows.Item("Column11") = dtRow.Item("classgrp")
                rpt_Rows.Item("Column12") = dtRow.Item("class")
                rpt_Rows.Item("Column13") = dtRow.Item("grade")

                mdtRequestDate = CDate(dtRow.Item("transactiondate"))
                mstrClaimRemark = dtRow.Item("claim_remark").ToString()

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            '/* START CLAIM APPLICATION DETAILS

            Dim dsClaimDetails As DataSet = Nothing

            StrQ = "SELECT cmclaim_request_tran.crtranunkid " & _
                      ",cmclaim_request_tran.crmasterunkid " & _
                      ",cmclaim_request_tran.expenseunkid " & _
                      ",cmclaim_request_tran.secrouteunkid " & _
                      ",ISNULL(cmexpense_master.name,'') AS Expense " & _
                      ",ISNULL(cfcommon_master.name,'') AS Sector " & _
                      ",ISNULL(cmclaim_request_tran.quantity,0.00) AS quantity " & _
                      ",ISNULL(cmclaim_request_tran.unitprice,0.00) AS unitprice " & _
                      ",ISNULL(cmclaim_request_tran.amount,0.00) AS amount " & _
                      ",ISNULL(cmclaim_request_tran.base_amount,0.00) AS base_amount " & _
                      ",ISNULL(cmclaim_request_tran.expense_remark,'') AS Expense_Remark " & _
                      ",cfexchange_rate.currency_sign As currency_sign " & _
                      ",ex.currency_sign As base_currency_sign " & _
                      " FROM cmclaim_request_tran " & _
                      " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_request_tran.expenseunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_request_tran.secrouteunkid and cfcommon_master.mastertype= @mastertype " & _
                      " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_request_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_request_tran.countryunkid " & _
                      " LEFT JOIN cfexchange_rate ex ON ex.countryunkid = cmclaim_request_tran.base_countryunkid " & _
                      " WHERE cmclaim_request_tran.isvoid = 0 and cmclaim_request_tran.crmasterunkid = @crmasterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, clsCommon_Master.enCommonMaster.SECTOR_ROUTE)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRetirementFormId)
            dsClaimDetails = objDataOperation.ExecQuery(StrQ, "Detail")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsClaimDetails IsNot Nothing And dsClaimDetails.Tables(0).Rows.Count > 0 Then

                For Each dtRow As DataRow In dsClaimDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_ClaimDetail.Tables("ArutiTable").NewRow()

                    rpt_Row.Item("Column1") = mdtRequestDate.ToShortDateString()
                    rpt_Row.Item("Column2") = mstrClaimRemark.Trim()
                    rpt_Row.Item("Column3") = dtRow.Item("Expense")
                    rpt_Row.Item("Column4") = dtRow.Item("Sector")
                    rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("quantity")), GUI.fmtCurrency)
                    rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("unitprice")), GUI.fmtCurrency)
                    rpt_Row.Item("Column7") = dtRow.Item("currency_sign").ToString() & "  " & Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                    rpt_Row.Item("Column8") = dtRow.Item("Expense_Remark").ToString()
                    rpt_Row.Item("Column9") = dtRow.Item("currency_sign").ToString() & "  " & Format(CDec(IIf(IsDBNull(dsClaimDetails.Tables(0).Compute("SUM(amount)", "crmasterunkid = " & mintClaimRetirementFormId)), 0, dsClaimDetails.Tables(0).Compute("SUM(amount)", "crmasterunkid = " & mintClaimRetirementFormId))), GUI.fmtCurrency)
                    rpt_Row.Item("Column10") = mstrExpenseCategory
                    rpt_Row.Item("Column11") = dtRow.Item("base_currency_sign").ToString() & "  " & Format(CDec(dtRow.Item("base_amount")), GUI.fmtCurrency)
                    rpt_Row.Item("Column12") = dtRow.Item("base_currency_sign").ToString() & "  " & Format(CDec(IIf(IsDBNull(dsClaimDetails.Tables(0).Compute("SUM(base_amount)", "crmasterunkid = " & mintClaimRetirementFormId)), 0, dsClaimDetails.Tables(0).Compute("SUM(base_amount)", "crmasterunkid = " & mintClaimRetirementFormId))), GUI.fmtCurrency)
                    rpt_ClaimDetail.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

            End If

            '/* END CLAIM APPLICATION DETAILS

            '/* START CLAIM APPROVER DETAILS

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                       "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                       "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                       "  FROM cmclaim_approval_tran "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= "    JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid  " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid "
            Else
                StrQ &= "    JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                             "     AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmexpapprover_master.expensetypeid = @expensetypeid   " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid "
            End If

            StrQ &= "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                         "    WHERE cmclaim_approval_tran.isvoid = 0  AND cmclaim_approval_tran.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " AND lvleaveapprover_master.isexternalapprover = 1 "
            Else
                StrQ &= " AND cmexpapprover_master.isexternalapprover = 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRetirementFormId)
            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrInnerQry = " SELECT cmclaim_request_master.crmasterunkid  "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= ",   ISNULL(lvleaveapprover_master.approverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelname, '') AS LevelName " & _
                                       ",   ISNULL(lvapproverlevel_master.priority, 0) AS priority "
            Else
                StrInnerQry &= ",   ISNULL(cmexpapprover_master.crapproverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelname, '') AS LevelName " & _
                                       ",   ISNULL(cmapproverlevel_master.crpriority, 0) AS priority "
            End If

            StrInnerQry &= " ,#APPVR_NAME#  AS Approver " & _
                                  " ,cmclaim_approval_tran.approvaldate As ApprovalDate " & _
                                  " ,cmclaim_approval_tran.statusunkid " & _
                                  " ,CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
                                  "  END as Status " & _
                                  " ,ISNULL(cmclaim_approval_tran.amount,0.00) As amount " & _
                                  " ,ISNULL(cmclaim_approval_tran.base_amount,0.00) As base_amount " & _
                                  " ,ISNULL(cmclaim_approval_tran.expense_remark,'') As expense_remark " & _
                                  ",cfexchange_rate.currency_sign As currency_sign " & _
                                  ",ex.currency_sign As base_currency_sign " & _
                                  " FROM cmclaim_request_master " & _
                                  " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                                  " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid " & _
                                  " LEFT JOIN cfexchange_rate ex ON ex.countryunkid = cmclaim_approval_tran.base_countryunkid "


            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid AND lvleaveapprover_master.isvoid = 0 " & _
                                       " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
            Else
                StrInnerQry &= " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                                      " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid "
            End If

            StrInnerQry &= " #COMM_JOIN# "

            StrConditionQry = " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.expensetypeid = @expensetypeid AND cmclaim_request_master.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrConditionQry &= "  AND lvleaveapprover_master.isexternalapprover = #ExApprId# "
            Else
                StrConditionQry &= "  AND cmexpapprover_master.isexternalapprover = #ExApprId#  "
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid ")
            Else
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid ")
            End If

            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " ORDER BY lvapproverlevel_master.priority "
            Else
                StrQ &= " ORDER BY cmapproverlevel_master.crpriority "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRetirementFormId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid ")
                Else
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                                   " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim mintPriorty As Integer = -1
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
Recalculate:
                Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dsList.Tables(0).Rows.Remove(dRow)
                                    GoTo Recalculate
                                Next
                                dsList.AcceptChanges()
                            End If
                        End If


                    End If

                Next
            End If

            Dim mintApproverID As Integer = 0
            Dim xCount As Integer = -1
            Dim mdecAmount As Decimal = 0
            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            Dim mdecBaseAmount As Decimal = 0
            'Pinkal (24-Jun-2024) -- End

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow

                If mintApproverID <> CInt(dtRow("approverunkid")) Then
                    rpt_Row = rpt_ClaimApprovers.Tables("ArutiTable").NewRow()
                    rpt_Row.Item("Column1") = dtRow.Item("LevelName").ToString()
                    rpt_Row.Item("Column2") = dtRow.Item("Approver").ToString()
                    If Not IsDBNull(dtRow.Item("ApprovalDate")) Then
                        rpt_Row.Item("Column3") = CDate(dtRow.Item("ApprovalDate")).ToShortDateString()
                    Else
                        rpt_Row.Item("Column3") = ""
                    End If
                    mdecAmount = CDec(dtRow.Item("amount"))

                    'Pinkal (24-Jun-2024) -- Start
                    'NMB Enhancement : P2P & Expense Category Enhancements.
                    If strGrpName.ToUpper = "NMB PLC" Then
                        mdecBaseAmount = CDec(dtRow.Item("base_amount"))
                        rpt_Row.Item("Column4") = dtRow.Item("base_currency_sign").ToString() & "  " & Format(CDec(dtRow.Item("base_amount")), GUI.fmtCurrency)
                    Else
                        rpt_Row.Item("Column4") = dtRow.Item("currency_sign").ToString() & "  " & Format(mdecAmount, GUI.fmtCurrency)
                    End If
                    'Pinkal (24-Jun-2024) -- End
                    rpt_Row.Item("Column5") = dtRow.Item("Status").ToString()
                    rpt_Row.Item("Column6") = dtRow.Item("expense_remark").ToString()
                    mintApproverID = CInt(dtRow("approverunkid"))
                    rpt_ClaimApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
                    xCount += 1
                Else
                    If CDec(dtRow.Item("amount")) > 0 Then
                        'Pinkal (24-Jun-2024) -- Start
                        'NMB Enhancement : P2P & Expense Category Enhancements.
                        If strGrpName.ToUpper = "NMB PLC" Then
                            mdecBaseAmount += CDec(dtRow.Item("base_amount"))
                            rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column4") = dtRow.Item("base_currency_sign").ToString() & "  " & Format(mdecBaseAmount, GUI.fmtCurrency)
                        Else
                            mdecAmount += CDec(dtRow.Item("amount"))
                            rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column4") = dtRow.Item("currency_sign").ToString() & "  " & Format(mdecAmount, GUI.fmtCurrency)
                        End If
                        'Pinkal (24-Jun-2024) -- End
                    End If

                    If dtRow.Item("expense_remark").ToString().Trim.Length > 0 Then
                        rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column6") &= IIf(rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column5").ToString().Trim().Length > 0, ",", "") & dtRow.Item("expense_remark").ToString()
                    End If
                End If

            Next

            '/* END CLAIM APPROVER DETAILS


            objRpt = New ArutiReport.Designer.rptEmployeeClaimForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)



            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
            'Call ReportFunction.EnableSuppress(objRpt, "txtFilterDescription", True)

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            Call ReportFunction.TextChange(objRpt, "txtApplicationNo", info1.ToTitleCase(Language.getMessage(mstrModuleName, 1, "APPLICATION NUMBER").ToLower()) & " : ")
            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", Language.getMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 3, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 4, "Emp Code :"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 5, "Job Title :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 6, "Dept :"))
            Call ReportFunction.TextChange(objRpt, "txtSectionGroup", Language.getMessage(mstrModuleName, 41, "Section Group :"))
            Call ReportFunction.TextChange(objRpt, "txtClassGroup", Language.getMessage(mstrModuleName, 42, "Class Group :"))
            Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 43, "Class :"))
            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 44, "Grade :"))


            objRpt.Subreports("rptAdvanceApplicationDetails").SetDataSource(rpt_ClaimDetail)
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimApplicationDetails", Language.getMessage(mstrModuleName, 45, "CLAIM APPLICATION DETAILS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtRequestDate", Language.getMessage(mstrModuleName, 46, "REQUEST DATE :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimDescription", Language.getMessage(mstrModuleName, 47, "CLAIM DESCRIPTION"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtSectorRoute", Language.getMessage(mstrModuleName, 48, "SECTOR/ROUTE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtQuantity", Language.getMessage(mstrModuleName, 49, "QUANTITY"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtUnitPrice", Language.getMessage(mstrModuleName, 50, "UNIT PRICE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtRequestedAmt", Language.getMessage(mstrModuleName, 51, "REQUESTED AMT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtExpenseRemark", Language.getMessage(mstrModuleName, 60, "EXPENSE REMARK"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimRemark", Language.getMessage(mstrModuleName, 26, "CLAIM REMARK") & " :")
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtGrandTotal", Language.getMessage(mstrModuleName, 58, "GRAND TOTAL"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtExpenseCategory", Language.getMessage(mstrModuleName, 61, "EXPENSE CATEGORY :"))
            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtRequestedBaseAmt", Language.getMessage(mstrModuleName, 90, "BASE AMT"))

            If strGrpName.ToUpper = "NMB PLC" Then
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptAdvanceApplicationDetails"), "Column91", True)
            Else
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptAdvanceApplicationDetails"), "Column91", False)
            End If
            'Pinkal (24-Jun-2024) -- End


            objRpt.Subreports("rptAdvanceApprovals").SetDataSource(rpt_ClaimApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovalDetails", Language.getMessage(mstrModuleName, 52, "APPROVAL DETAILS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApproverLevel", Language.getMessage(mstrModuleName, 53, "APPROVAL LEVEL"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprover", Language.getMessage(mstrModuleName, 54, "APPROVER"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovedAmount", Language.getMessage(mstrModuleName, 55, "APPROVED AMOUNT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovalDate", Language.getMessage(mstrModuleName, 30, "APPROVAL DATE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtStatus", Language.getMessage(mstrModuleName, 56, "STATUS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApproverRemarks", Language.getMessage(mstrModuleName, 57, "APPROVER REMARKS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtTotalApprovedAmount", Language.getMessage(mstrModuleName, 59, "TOTAL APPROVED AMOUNT"))


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 36, "Prepared By :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "lblPreparedBy", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtPreparedBy", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 37, "Received By :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "lblReceivedBy", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtReceivedBy", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = False AndAlso ConfigParameter._Object._IsShowReceivedBy = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsclaim_request_tran", 100, "Dependant")
            Language.setMessage("clsclaim_request_tran", 101, "Other")
            Language.setMessage("clsclaim_request_tran", 102, "Self")
            Language.setMessage("clsExpCommonMethods", 18, "S/A")
            Language.setMessage("clsExpCommonMethods", 19, "FIRM")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage(mstrModuleName, 1, "APPLICATION NUMBER")
            Language.setMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS")
            Language.setMessage(mstrModuleName, 3, "Name :")
            Language.setMessage(mstrModuleName, 4, "Emp Code :")
            Language.setMessage(mstrModuleName, 5, "Job Title :")
            Language.setMessage(mstrModuleName, 6, "Dept :")
            Language.setMessage(mstrModuleName, 7, "Appt. Date :")
            Language.setMessage(mstrModuleName, 8, "Basic Salary :")
            Language.setMessage(mstrModuleName, 9, "Special Allowance :")
            Language.setMessage(mstrModuleName, 10, "Total (Basic Salary + Special Allowance) :")
            Language.setMessage(mstrModuleName, 11, "LEAVE RECORDS")
            Language.setMessage(mstrModuleName, 12, "Leave Type :")
            Language.setMessage(mstrModuleName, 13, "Leave B/F :")
            Language.setMessage(mstrModuleName, 14, "Accrued ToDate :")
            Language.setMessage(mstrModuleName, 15, "Total Issued ToDate :")
            Language.setMessage(mstrModuleName, 16, "Total Adjustment :")
            Language.setMessage(mstrModuleName, 17, "Leave Balance As on Date :")
            Language.setMessage(mstrModuleName, 18, "Last Leave Dates :")
            Language.setMessage(mstrModuleName, 19, "PREVIOUS RECORDS")
            Language.setMessage(mstrModuleName, 20, "CLAIM NAME")
            Language.setMessage(mstrModuleName, 21, "DATE")
            Language.setMessage(mstrModuleName, 22, "REQUESTED AMOUNT")
            Language.setMessage(mstrModuleName, 23, "APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 24, "TOTAL AMOUNT")
            Language.setMessage(mstrModuleName, 25, "APPLICATION DETAILS")
            Language.setMessage(mstrModuleName, 26, "CLAIM REMARK")
            Language.setMessage(mstrModuleName, 27, "APPROVALS DETAILS")
            Language.setMessage(mstrModuleName, 28, "LEVEL NAME")
            Language.setMessage(mstrModuleName, 29, "APPROVER NAME")
            Language.setMessage(mstrModuleName, 30, "APPROVAL DATE")
            Language.setMessage(mstrModuleName, 31, "APPROVAL STATUS")
            Language.setMessage(mstrModuleName, 32, "REMARKS/COMMENTS")
            Language.setMessage(mstrModuleName, 33, "APPLICATION FOR")
            Language.setMessage(mstrModuleName, 34, "Employee")
            Language.setMessage(mstrModuleName, 35, "Claims")
            Language.setMessage(mstrModuleName, 36, "Prepared By :")
            Language.setMessage(mstrModuleName, 37, "Received By :")
            Language.setMessage(mstrModuleName, 38, "Printed By :")
            Language.setMessage(mstrModuleName, 39, "Printed Date :")
            Language.setMessage(mstrModuleName, 40, "To")
            Language.setMessage(mstrModuleName, 41, "Section Group :")
            Language.setMessage(mstrModuleName, 42, "Class Group :")
            Language.setMessage(mstrModuleName, 43, "Class :")
            Language.setMessage(mstrModuleName, 44, "Grade :")
            Language.setMessage(mstrModuleName, 45, "CLAIM APPLICATION DETAILS")
            Language.setMessage(mstrModuleName, 46, "REQUEST DATE :")
            Language.setMessage(mstrModuleName, 47, "CLAIM DESCRIPTION")
            Language.setMessage(mstrModuleName, 48, "SECTOR/ROUTE")
            Language.setMessage(mstrModuleName, 49, "QUANTITY")
            Language.setMessage(mstrModuleName, 50, "UNIT PRICE")
            Language.setMessage(mstrModuleName, 51, "REQUESTED AMT")
            Language.setMessage(mstrModuleName, 52, "APPROVAL DETAILS")
            Language.setMessage(mstrModuleName, 53, "APPROVAL LEVEL")
            Language.setMessage(mstrModuleName, 54, "APPROVER")
            Language.setMessage(mstrModuleName, 55, "APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 56, "STATUS")
            Language.setMessage(mstrModuleName, 57, "APPROVER REMARKS")
            Language.setMessage(mstrModuleName, 58, "GRAND TOTAL")
            Language.setMessage(mstrModuleName, 59, "TOTAL APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 60, "EXPENSE REMARK")
            Language.setMessage(mstrModuleName, 61, "EXPENSE CATEGORY :")
            Language.setMessage(mstrModuleName, 62, "Fuel Application Details")
            Language.setMessage(mstrModuleName, 63, "Application Date :")
            Language.setMessage(mstrModuleName, 64, "Vehicle Reg. No :")
            Language.setMessage(mstrModuleName, 65, "Annual fuel balance of gasoline/petrol :")
            Language.setMessage(mstrModuleName, 66, "Amount of fuel requested :")
            Language.setMessage(mstrModuleName, 67, "Remaining fuel balance :")
            Language.setMessage(mstrModuleName, 68, "Approved amount :")
            Language.setMessage(mstrModuleName, 69, "Application Status :")
            Language.setMessage(mstrModuleName, 70, "Purpose :")
            Language.setMessage(mstrModuleName, 71, "Level Name")
            Language.setMessage(mstrModuleName, 72, "Approver Name")
            Language.setMessage(mstrModuleName, 73, "Approved Fuel Amt.")
            Language.setMessage(mstrModuleName, 74, "To : Fuel Supplier")
            Language.setMessage(mstrModuleName, 75, "Please Supply")
            Language.setMessage(mstrModuleName, 76, "LTS of")
            Language.setMessage(mstrModuleName, 77, "gasoline / petrol")
            Language.setMessage(mstrModuleName, 78, "to vehicle with Reg.No")
            Language.setMessage(mstrModuleName, 79, "Equipment No")
            Language.setMessage(mstrModuleName, 80, "Others")
            Language.setMessage(mstrModuleName, 81, "For KADCO")
            Language.setMessage(mstrModuleName, 82, "Date")
            Language.setMessage(mstrModuleName, 83, "Delivery Note No")
            Language.setMessage(mstrModuleName, 84, "Fuel/Lubricant Application Form")
            Language.setMessage(mstrModuleName, 85, "Form No:")
            Language.setMessage(mstrModuleName, 86, "Cumulative Fuel Balance :")
            Language.setMessage(mstrModuleName, 87, "Original Copy")
            Language.setMessage(mstrModuleName, 88, "Reprinted Copy")
            Language.setMessage(mstrModuleName, 90, "BASE AMT")
            Language.setMessage(mstrModuleName, 900, "Rebate Travel Authorization")
            Language.setMessage(mstrModuleName, 901, "Rebate No.")
            Language.setMessage(mstrModuleName, 902, "Recommender")
            Language.setMessage(mstrModuleName, 903, "Approver")
            Language.setMessage(mstrModuleName, 904, "Rebate Travel is")
            Language.setMessage(mstrModuleName, 906, "as below:")
            Language.setMessage(mstrModuleName, 907, "Staff No.")
            Language.setMessage(mstrModuleName, 908, "Name")
            Language.setMessage(mstrModuleName, 909, "Title")
            Language.setMessage(mstrModuleName, 910, "Date Of Employment")
            Language.setMessage(mstrModuleName, 911, "Passenger Details:-")
            Language.setMessage(mstrModuleName, 912, "Names")
            Language.setMessage(mstrModuleName, 913, "Relation")
            Language.setMessage(mstrModuleName, 914, "Age")
            Language.setMessage(mstrModuleName, 915, "Ticket No.")
            Language.setMessage(mstrModuleName, 916, "Routing Details:-")
            Language.setMessage(mstrModuleName, 917, "From")
            Language.setMessage(mstrModuleName, 918, "To")
            Language.setMessage(mstrModuleName, 919, "Airline")
            Language.setMessage(mstrModuleName, 920, "Flight No.")
            Language.setMessage(mstrModuleName, 921, "Travel Date")
            Language.setMessage(mstrModuleName, 922, "Reason/Purpose for Travel")
            Language.setMessage(mstrModuleName, 923, "Rebate Percentage")
            Language.setMessage(mstrModuleName, 924, "Free Baggage Allowance (Kgs)")
            Language.setMessage(mstrModuleName, 925, "Class Of Travel")
            Language.setMessage(mstrModuleName, 926, "ECONOMY")
            Language.setMessage(mstrModuleName, 927, "Department")
            Language.setMessage(mstrModuleName, 928, "Costcenter")
            Language.setMessage(mstrModuleName, 929, "Approval Date")
            Language.setMessage(mstrModuleName, 930, "This Rebate expires in the next 30 days,i.e. valid upto")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
