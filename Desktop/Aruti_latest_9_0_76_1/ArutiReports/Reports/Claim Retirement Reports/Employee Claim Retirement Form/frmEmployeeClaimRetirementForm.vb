#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region

Public Class frmEmployeeClaimRetirementForm

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmEmployeeClaimRetirementForm"
    Private objClaimRetirementForm As clsEmployeeClaimRetirementForm
    Private dtView As DataView = Nothing
#End Region

#Region "Constructor"
    Public Sub New()
        objClaimRetirementForm = New clsEmployeeClaimRetirementForm(User._Object._Languageunkid, Company._Object._Companyunkid)
        objClaimRetirementForm.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet

            RemoveHandler cboExpenseCategory.SelectedValueChanged, AddressOf cboExpenseCategory_SelectedValueChanged

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsList = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True, True, True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End


            cboExpenseCategory.DataSource = dsList.Tables(0)
            cboExpenseCategory.ValueMember = "Id"
            cboExpenseCategory.DisplayMember = "Name"
            cboExpenseCategory.SelectedValue = 0

            AddHandler cboExpenseCategory.SelectedValueChanged, AddressOf cboExpenseCategory_SelectedValueChanged

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtptoDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpFromDate.Checked = False
            dtptoDate.Checked = False
            'Pinkal (30-May-2020) -- End

            cboEmployee.SelectedIndex = 0
            cboExpenseCategory.SelectedIndex = 0
            cboRetirementForm.SelectedIndex = 0
            'txtFilterAllowances.Text = ""
            'objchkSelectAll.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objClaimRetirementForm.SetDefaultValue()

            objClaimRetirementForm._EmployeeId = cboEmployee.SelectedValue
            objClaimRetirementForm._EmployeeName = cboEmployee.Text
            objClaimRetirementForm._ExpenseCategoryId = CInt(cboExpenseCategory.SelectedValue)
            objClaimRetirementForm._ExpenseCategory = cboExpenseCategory.Text
            objClaimRetirementForm._ClaimRetirementFormId = CInt(cboRetirementForm.SelectedValue)
            objClaimRetirementForm._ClaimRetirementFormNo = cboRetirementForm.Text

            objClaimRetirementForm._YearId = FinancialYear._Object._YearUnkid
            objClaimRetirementForm._Fin_StartDate = FinancialYear._Object._Database_Start_Date.Date
            objClaimRetirementForm._Fin_Enddate = FinancialYear._Object._Database_End_Date.Date


            Dim objClaimRequest As New clsclaim_request_master
            objClaimRequest._Crmasterunkid = CInt(cboRetirementForm.SelectedValue)


            'Pinkal (19-Jul-2021)-- Start
            'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
            If Company._Object._Code.ToString().ToUpper() = "KADCO" Then
                'If Company._Object._Name.ToString().ToUpper() = "KADCO" Then
                If objClaimRequest._Statusunkid = 1 AndAlso objClaimRequest._IsPrinted = False Then
                    objClaimRetirementForm._IsPrinted = True
                    objClaimRetirementForm._PrintedDateTime = ConfigParameter._Object._CurrentDateAndTime
                    objClaimRetirementForm._PrintUserId = User._Object._Userunkid
                    objClaimRetirementForm._PrintedIp = getIP()
                    objClaimRetirementForm._PrintedHost = getHostName()
                    objClaimRetirementForm._IsPrintFromWeb = False
                Else
                    objClaimRetirementForm._IsPrinted = False
                    objClaimRetirementForm._PrintedDateTime = Nothing
                    objClaimRetirementForm._PrintUserId = -1
                    objClaimRetirementForm._PrintedIp = ""
                    objClaimRetirementForm._PrintedHost = ""
                    objClaimRetirementForm._IsPrintFromWeb = False
                End If
            End If
            'Pinkal (19-Jul-2021)-- End

            Dim objMasterData As New clsMasterData
            Dim mintPeriodID As Integer = objMasterData.getCurrentTnAPeriodID(enModuleReference.Payroll, objClaimRequest._Transactiondate.Date)
            objMasterData = Nothing

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodID
            objClaimRetirementForm._PeiordID = mintPeriodID
            objClaimRetirementForm._PeriodStartDate = objPeriod._TnA_StartDate.Date
            objClaimRetirementForm._PeriodEndDate = objPeriod._TnA_EndDate.Date
            objPeriod = Nothing
            objClaimRequest = Nothing

            If CInt(cboExpenseCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                objClaimRetirementForm._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                objClaimRetirementForm._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
                objClaimRetirementForm._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            End If

            If dtView IsNot Nothing AndAlso dtView.ToTable().Rows.Count > 0 Then
                objClaimRetirementForm._SpecialAllowanceIds = String.Join(",", dtView.ToTable().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("tranheadunkid").ToString()).ToArray())
            End If

            'objClaimRetirementForm._ShowEmployeeScale = chkShowEmpScale.Checked

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Public Function Validation() As Boolean

        Dim mblnFlag As Boolean = False
        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expese Category is compulsory information.Please Select Expense Category."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboExpenseCategory.Focus()
                Return False


            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboExpenseCategory.Focus()
                Return False

            ElseIf CInt(cboRetirementForm.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Claim Form is compulsory information.Please Select Claim Form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboRetirementForm.Focus()
                Return False

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub FillTranHead()
        'Dim objTranHead As clsTransactionHead = Nothing
        'Try
        '    objTranHead = New clsTransactionHead
        '    Dim dsList As DataSet = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, False, False, "typeof_id <> " & enTypeOf.Salary, False, False, False)

        '    Dim dcColumn As New DataColumn("ischeck", Type.GetType("System.Boolean"))
        '    dcColumn.DefaultValue = False
        '    dsList.Tables(0).Columns.Add(dcColumn)

        '    dgTranHeads.AutoGenerateColumns = False
        '    objdgcolhIscheck.DataPropertyName = "ischeck"
        '    dgcolhTranHeadCode.DataPropertyName = "code"
        '    dgcolhTranHead.DataPropertyName = "name"
        '    objdgcolhTranHeadId.DataPropertyName = "tranheadunkid"
        '    dtView = dsList.Tables(0).DefaultView()

        '    dgTranHeads.DataSource = dtView

        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "FillTranHead", mstrModuleName)
        'Finally
        '    objTranHead = Nothing
        'End Try
    End Sub

#End Region

#Region "Form's Events"

    Private Sub frmEmployeeClaimRetirementForm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objClaimRetirementForm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimRetirementForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objClaimRetirementForm._ReportName
            Me._Message = objClaimRetirementForm._ReportDesc

            Call FillCombo()
            FillTranHead()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimRetirementForm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeClaimRetirementForm_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimRetirementForm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub frmEmployeeClaimRetirementForm_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Validation() = False Then Exit Sub

            If SetFilter() = False Then Exit Sub

            objClaimRetirementForm.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                              , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                              , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                              , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, 0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimRetirementForm_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Validation() = False Then Exit Sub

            If SetFilter() = False Then Exit Sub

            objClaimRetirementForm.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                           , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                           , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, 0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimRetirementForm_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimRetirementForm_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimRetirementForm_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchExpenseCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpenseCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpenseCategory.DataSource
            frm.ValueMember = cboExpenseCategory.ValueMember
            frm.DisplayMember = cboExpenseCategory.DisplayMember
            If frm.DisplayDialog Then
                cboExpenseCategory.SelectedValue = frm.SelectedValue
                cboExpenseCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpenseCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmEmployeeClaimRetirementForm_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeClaimForm.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeClaimForm"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeClaimForm_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchClaimForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRetirementForm.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboRetirementForm.DataSource
            frm.ValueMember = cboRetirementForm.ValueMember
            frm.DisplayMember = cboRetirementForm.DisplayMember
            If frm.DisplayDialog Then
                cboRetirementForm.SelectedValue = frm.SelectedValue
                cboRetirementForm.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchClaimForm_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "ComboBox"

    Private Sub cboExpenseCategory_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedValueChanged, cboEmployee.SelectedValueChanged
        Try
            Dim objClaimRequest As New clsclaim_request_master
            Dim dtList As DataTable

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            If dtpFromDate.Checked AndAlso dtptoDate.Checked Then
                Dim mstrSearch = "CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) >= '" & eZeeDate.convertDate(dtpFromDate.Value.Date).ToString() & "' AND CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) <= '" & eZeeDate.convertDate(dtptoDate.Value.Date).ToString() & "'"
                dtList = objClaimRequest.GetEmployeeClaimForm(CInt(cboEmployee.SelectedValue), CInt(cboExpenseCategory.SelectedValue), True, mstrSearch)
            Else
                dtList = objClaimRequest.GetEmployeeClaimForm(CInt(cboEmployee.SelectedValue), CInt(cboExpenseCategory.SelectedValue), True)
            End If
            'Pinkal (30-May-2020) -- End


            cboRetirementForm.DataSource = dtList
            cboRetirementForm.DisplayMember = "claimrequestno"
            cboRetirementForm.ValueMember = "crmasterunkid"
            cboRetirementForm.SelectedValue = 0
            objClaimRequest = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Private Sub txtFilterAllowances_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    Dim strSearch As String = ""
        '    If txtFilterAllowances.Text.Trim.Length > 0 Then
        '        strSearch = dgcolhTranHeadCode.DataPropertyName & " LIKE '%" & txtFilterAllowances.Text & "%' OR " & _
        '                    dgcolhTranHead.DataPropertyName & " LIKE '%" & txtFilterAllowances.Text & "%'"
        '    End If
        '    dtView.RowFilter = strSearch
        '    dgTranHeads_CellContentClick(dgTranHeads, New DataGridViewCellEventArgs(objdgcolhIscheck.Index, 0))
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "txtFilterAllowances_TextChanged", mstrModuleName)
        'End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    RemoveHandler dgTranHeads.CellContentClick, AddressOf dgTranHeads_CellContentClick
        '    For Each dr As DataRowView In dtView
        '        dr.Item("ischeck") = CBool(objchkSelectAll.CheckState)
        '    Next
        '    dgTranHeads.Refresh()
        '    AddHandler dgTranHeads.CellContentClick, AddressOf dgTranHeads_CellContentClick
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        'Finally
        'End Try
    End Sub

#End Region

#Region "DataGridview Event"

    Private Sub dgTranHeads_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        'Try
        '    RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

        '    If e.ColumnIndex = objdgcolhIscheck.Index Then

        '        If Me.dgTranHeads.IsCurrentCellDirty Then
        '            Me.dgTranHeads.CommitEdit(DataGridViewDataErrorContexts.Commit)
        '        End If
        '        Dim drRow As DataRow() = dtView.ToTable().Select("ischeck = true", "")
        '        If drRow.Length > 0 Then
        '            If dtView.ToTable().Rows.Count = drRow.Length Then
        '                objchkSelectAll.CheckState = CheckState.Checked
        '            Else
        '                objchkSelectAll.CheckState = CheckState.Indeterminate
        '            End If
        '        Else
        '            objchkSelectAll.CheckState = CheckState.Unchecked
        '        End If
        '    End If

        '    AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "dgTranHeads_CellContentClick", mstrModuleName)
        'Finally
        'End Try
    End Sub

#End Region


    'Pinkal (30-May-2020) -- Start
    'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.

#Region "DatePicker Events"

    Private Sub dtpFromDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.ValueChanged, dtptoDate.ValueChanged
        Try
            If dtpFromDate.Checked AndAlso dtptoDate.Checked Then
                cboExpenseCategory_SelectedValueChanged(cboExpenseCategory, New EventArgs())
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpFromDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (30-May-2020) -- End




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.LblRetirementform.Text = Language._Object.getCaption(Me.LblRetirementform.Name, Me.LblRetirementform.Text)
            Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Expese Category is compulsory information.Please Select Expense Category.")
            Language.setMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 3, "Claim Form is compulsory information.Please Select Claim Form.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
