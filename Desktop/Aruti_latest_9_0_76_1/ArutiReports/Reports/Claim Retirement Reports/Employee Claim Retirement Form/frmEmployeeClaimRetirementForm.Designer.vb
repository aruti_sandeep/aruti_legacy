﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeClaimRetirementForm
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblToDate = New System.Windows.Forms.Label
        Me.dtptoDate = New System.Windows.Forms.DateTimePicker
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.LblFromDate = New System.Windows.Forms.Label
        Me.cboRetirementForm = New System.Windows.Forms.ComboBox
        Me.objbtnSearchRetirementForm = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchExpenseCategory = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.LblRetirementform = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.LblExpenseCategory = New System.Windows.Forms.Label
        Me.cboExpenseCategory = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 495)
        Me.NavPanel.Size = New System.Drawing.Size(668, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.LblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtptoDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboRetirementForm)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchRetirementForm)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExpenseCategory)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblRetirementform)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpenseCategory)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpenseCategory)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 65)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(400, 146)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblToDate
        '
        Me.LblToDate.BackColor = System.Drawing.Color.Transparent
        Me.LblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToDate.Location = New System.Drawing.Point(219, 36)
        Me.LblToDate.Name = "LblToDate"
        Me.LblToDate.Size = New System.Drawing.Size(31, 13)
        Me.LblToDate.TabIndex = 220
        Me.LblToDate.Text = "To"
        '
        'dtptoDate
        '
        Me.dtptoDate.Checked = False
        Me.dtptoDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtptoDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtptoDate.Location = New System.Drawing.Point(260, 32)
        Me.dtptoDate.Name = "dtptoDate"
        Me.dtptoDate.ShowCheckBox = True
        Me.dtptoDate.Size = New System.Drawing.Size(103, 21)
        Me.dtptoDate.TabIndex = 219
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(110, 32)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpFromDate.TabIndex = 218
        '
        'LblFromDate
        '
        Me.LblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.LblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFromDate.Location = New System.Drawing.Point(8, 36)
        Me.LblFromDate.Name = "LblFromDate"
        Me.LblFromDate.Size = New System.Drawing.Size(97, 15)
        Me.LblFromDate.TabIndex = 217
        Me.LblFromDate.Text = "From Date"
        '
        'cboRetirementForm
        '
        Me.cboRetirementForm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRetirementForm.DropDownWidth = 300
        Me.cboRetirementForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRetirementForm.FormattingEnabled = True
        Me.cboRetirementForm.Location = New System.Drawing.Point(110, 113)
        Me.cboRetirementForm.Name = "cboRetirementForm"
        Me.cboRetirementForm.Size = New System.Drawing.Size(253, 21)
        Me.cboRetirementForm.TabIndex = 3
        '
        'objbtnSearchRetirementForm
        '
        Me.objbtnSearchRetirementForm.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRetirementForm.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRetirementForm.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRetirementForm.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRetirementForm.BorderSelected = False
        Me.objbtnSearchRetirementForm.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRetirementForm.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRetirementForm.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRetirementForm.Location = New System.Drawing.Point(366, 113)
        Me.objbtnSearchRetirementForm.Name = "objbtnSearchRetirementForm"
        Me.objbtnSearchRetirementForm.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRetirementForm.TabIndex = 210
        '
        'objbtnSearchExpenseCategory
        '
        Me.objbtnSearchExpenseCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExpenseCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpenseCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpenseCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExpenseCategory.BorderSelected = False
        Me.objbtnSearchExpenseCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExpenseCategory.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExpenseCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExpenseCategory.Location = New System.Drawing.Point(366, 59)
        Me.objbtnSearchExpenseCategory.Name = "objbtnSearchExpenseCategory"
        Me.objbtnSearchExpenseCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExpenseCategory.TabIndex = 208
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(366, 86)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 207
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 90)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(97, 15)
        Me.lblEmployee.TabIndex = 174
        Me.lblEmployee.Text = "Employee"
        '
        'LblRetirementform
        '
        Me.LblRetirementform.BackColor = System.Drawing.Color.Transparent
        Me.LblRetirementform.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRetirementform.Location = New System.Drawing.Point(8, 117)
        Me.LblRetirementform.Name = "LblRetirementform"
        Me.LblRetirementform.Size = New System.Drawing.Size(97, 15)
        Me.LblRetirementform.TabIndex = 0
        Me.LblRetirementform.Text = "Retirement Form"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 86)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(253, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'LblExpenseCategory
        '
        Me.LblExpenseCategory.BackColor = System.Drawing.Color.Transparent
        Me.LblExpenseCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpenseCategory.Location = New System.Drawing.Point(8, 63)
        Me.LblExpenseCategory.Name = "LblExpenseCategory"
        Me.LblExpenseCategory.Size = New System.Drawing.Size(97, 15)
        Me.LblExpenseCategory.TabIndex = 3
        Me.LblExpenseCategory.Text = "Expense Category"
        '
        'cboExpenseCategory
        '
        Me.cboExpenseCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpenseCategory.DropDownWidth = 120
        Me.cboExpenseCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpenseCategory.FormattingEnabled = True
        Me.cboExpenseCategory.Location = New System.Drawing.Point(110, 59)
        Me.cboExpenseCategory.Name = "cboExpenseCategory"
        Me.cboExpenseCategory.Size = New System.Drawing.Size(253, 21)
        Me.cboExpenseCategory.TabIndex = 168
        '
        'frmEmployeeClaimRetirementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(668, 550)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmEmployeeClaimRetirementForm"
        Me.Text = "frmEmployeeClaimRetirementForm"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents cboRetirementForm As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchRetirementForm As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchExpenseCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Private WithEvents LblRetirementform As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Private WithEvents LblExpenseCategory As System.Windows.Forms.Label
    Public WithEvents cboExpenseCategory As System.Windows.Forms.ComboBox
    Private WithEvents LblFromDate As System.Windows.Forms.Label
    Private WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Private WithEvents LblToDate As System.Windows.Forms.Label
    Private WithEvents dtptoDate As System.Windows.Forms.DateTimePicker
End Class
