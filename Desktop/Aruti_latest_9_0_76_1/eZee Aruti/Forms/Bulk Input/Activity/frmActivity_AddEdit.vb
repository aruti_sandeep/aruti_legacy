﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmActivity_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmActivity_AddEdit"
    Private mblnCancel As Boolean = True
    Private objActivity As clsActivity_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintActivityUnkid As Integer = -1

    'Pinkal (23-Sep-2014) -- Start
    'Enhancement -  PAY_A CHANGES IN PPA
    Private mblnIsOTActivity As Boolean = False
    'Pinkal (23-Sep-2014) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintActivityUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintActivityUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboMeasures.BackColor = GUI.ColorOptional
            txtN_OT.BackColor = GUI.ColorComp
            txtP_OT.BackColor = GUI.ColorComp
            cboHeadType.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objActivity._Code
            txtName.Text = objActivity._Name
            txtDescription.Text = objActivity._Description
            cboMeasures.SelectedValue = objActivity._Measureunkid
            cboHeadType.SelectedValue = objActivity._TranheadTypeId
            chkOTActivity.Checked = objActivity._Is_Ot_Activity
            Call chkOTActivity_CheckedChanged(New Object, New EventArgs)
            If chkOTActivity.Checked = True Then
                txtN_OT.Decimal = objActivity._Normal_Otfactor
                txtP_OT.Decimal = objActivity._Ph_Otfactor
            Else
                txtN_OT.Text = "" : txtP_OT.Text = ""
            End If

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            If menAction = enAction.EDIT_ONE Then
                Dim objHead As New clsTransactionHead
                Dim intHeadID As Integer = 0
                ' NORMAL AMOUNT HEAD
                objHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY, mintActivityUnkid, intHeadID)
                If intHeadID > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objHead._Tranheadunkid = intHeadID
                    objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = intHeadID
                    'Sohail (21 Aug 2015) -- End
                    chkAppearOnPayslip.Checked = objHead._Isappearonpayslip
                    chkTaxable.Checked = objHead._Istaxable
                Else
                    chkAppearOnPayslip.Checked = True
                    chkTaxable.Checked = False
                End If

                ' OT AMOUNT HEAD
                intHeadID = 0
                objHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_OT, mintActivityUnkid, intHeadID)
                If intHeadID > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objHead._Tranheadunkid = intHeadID
                    objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = intHeadID
                    'Sohail (21 Aug 2015) -- End
                    chkAppearOTOnPayslip.Checked = objHead._Isappearonpayslip
                    chkTaxableOT.Checked = objHead._Istaxable
                Else
                    chkAppearOTOnPayslip.Checked = True
                    chkTaxableOT.Checked = False
                End If

                ' OT AMOUNT HEAD
                intHeadID = 0
                objHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_PH, mintActivityUnkid, intHeadID)
                If intHeadID > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objHead._Tranheadunkid = intHeadID
                    objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = intHeadID
                    'Sohail (21 Aug 2015) -- End
                    chkAppearPHOnPayslip.Checked = objHead._Isappearonpayslip
                    chkTaxablePH.Checked = objHead._Istaxable
                Else
                    chkAppearPHOnPayslip.Checked = True
                    chkTaxablePH.Checked = False
                End If

                If objActivity.isUsed(mintActivityUnkid) = True Then
                    cboHeadType.Enabled = False
                End If

                mblnIsOTActivity = chkOTActivity.Checked
            End If
            'Sohail (17 Sep 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objActivity._Code = txtCode.Text
            objActivity._Name = txtName.Text
            objActivity._Description = txtDescription.Text
            objActivity._Measureunkid = CInt(cboMeasures.SelectedValue)
            objActivity._TranheadTypeId = CInt(cboHeadType.SelectedValue)
            objActivity._Is_Ot_Activity = chkOTActivity.Checked
            objActivity._Normal_Otfactor = txtN_OT.Decimal
            objActivity._Ph_Otfactor = txtP_OT.Decimal

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            objActivity._Isappearonpayslip = chkAppearOnPayslip.Checked
            objActivity._IsappearOTonpayslip = chkAppearOTOnPayslip.Checked
            objActivity._IsappearPHonpayslip = chkAppearPHOnPayslip.Checked

            objActivity._Istaxable = chkTaxable.Checked
            objActivity._IstaxableOT = chkTaxableOT.Checked
            objActivity._IstaxablePH = chkTaxablePH.Checked
            'Sohail (17 Sep 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            objbtnAddMeasures.Enabled = User._Object.Privilege._AllowtoAddUnitOfMeasure

            If menAction = enAction.EDIT_ONE Then
                cboMeasures.Enabled = False : objbtnSearchMeasures.Enabled = False : objbtnAddMeasures.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objUMaster As New clsUnitMeasure_Master
        Dim objMMaster As New clsMasterData
        Try
            dsList = objUMaster.getComboList("List", True)
            With cboMeasures
                .ValueMember = "measureunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objMMaster.getComboListForHeadType("List")
            'Sohail (19 Aug 2014) -- Start
            'Enhancement - Include Informational heads on Pay Per Activity.
            'Dim dTab As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions & "," & enTranHeadType.Informational & ")", "Id", DataViewRowState.CurrentRows).ToTable
            Dim dTab As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions & ")", "Id", DataViewRowState.CurrentRows).ToTable
            'Sohail (19 Aug 2014) -- End
            With cboHeadType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dTab
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objUMaster = Nothing : objMMaster = Nothing
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            If txtCode.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Activity Code is mandatory information. Please provide Activity Code."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Return False
            End If


            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Activity Name is mandatory information. Please provide Activity Name."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If

            If CInt(cboMeasures.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Measure is mandatory information. Please select Measure."), enMsgBoxStyle.Information)
                cboMeasures.Focus()
                Return False
            End If

            If CInt(cboHeadType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Head Type is mandatory information. Please select Head Type."), enMsgBoxStyle.Information)
                cboHeadType.Focus()
                Return False
            End If

            'If chkOTActivity.Checked = True Then
            '    If txtN_OT.Decimal <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Normal OT Hours is mandatory information. Please provide Normal OT Hours."), enMsgBoxStyle.Information)
            '        txtN_OT.Focus()
            '        Return False
            '    End If

            '    If txtP_OT.Decimal <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Public Holiday OT Hours is mandatory information. Please provide Public Holiday OT Hours."), enMsgBoxStyle.Information)
            '        txtP_OT.Focus()
            '        Return False
            '    End If
            'End If

            If menAction = enAction.EDIT_ONE Then
                If objActivity._Is_Ot_Activity <> chkOTActivity.Checked Then


                    'Pinkal (23-Sep-2014) -- Start
                    'Enhancement -  PAY_A CHANGES IN PPA

                    'If objActivity.isUsed(mintActivityUnkid) = True Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot consider/reject this activity as OT activity. Reason : Data is already posted with this activity."), enMsgBoxStyle.Information)
                    '    Return False
                    'End If

                    Dim objMaster As New clsMasterData

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim mintPeriodnID As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
                    Dim mintPeriodnID As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                    'S.SANDEEP [04 JUN 2015] -- END

                    If mintPeriodnID > 0 Then
                        Dim objPeriod As New clscommom_period_Tran
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPeriod._Periodunkid = mintPeriodnID
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodnID
                        'Sohail (21 Aug 2015) -- End
                        Dim objActivitytran As New clsPayActivity_Tran
                        Dim dsList As DataSet = objActivitytran.GetList("List", , , mintActivityUnkid.ToString(), , , True, , "convert(char(8),activity_date,112) >='" & eZeeDate.convertDate(objPeriod._Start_Date.Date) & "'")
                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, ""), enMsgBoxStyle.Information)
                            Return False
                        End If
                        objActivitytran = Nothing
                        objPeriod = Nothing
                    End If

                    'Pinkal (23-Sep-2014) -- End
                End If
                'If objActivity._Normal_Otfactor <> txtN_OT.Decimal Then
                '    If objActivity.isUsed(mintActivityUnkid) = True Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot change the Normal OT Factor of this activity. Reason : Data is already posted with this activity."), enMsgBoxStyle.Information)
                '        Return False
                '    End If
                'End If

                'If objActivity._Ph_Otfactor <> txtP_OT.Decimal Then
                '    If objActivity.isUsed(mintActivityUnkid) = True Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot change the PH OT Factor of this activity. Reason : Data is already posted with this activity."), enMsgBoxStyle.Information)
                '        Return False
                '    End If
                'End If

                If objActivity._TranheadTypeId <> CInt(cboHeadType.SelectedValue) Then
                    Dim objActTran As New clsPayActivity_Tran
                    If objActTran.GetList("List", , , CStr(mintActivityUnkid), , , True).Tables(0).Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot change the activity type. Reason the activity is already posted to payroll."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    objActTran = Nothing
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isValid_Data", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmActivity_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objActivity = Nothing
    End Sub

    Private Sub frmActivity_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivity_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmActivity_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivity_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmActivity_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objActivity = New clsActivity_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objActivity._Activityunkid = mintActivityUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivity_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsActivity_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsActivity_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If isValid_Data() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objActivity.Update()

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objActivity.Update(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime)
                blnFlag = objActivity.Update(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime, User._Object._Userunkid)
                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objActivity.Insert()

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objActivity.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime)
                blnFlag = objActivity.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime, User._Object._Userunkid)
                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
            End If

            If blnFlag = False And objActivity._Message <> "" Then
                eZeeMsgBox.Show(objActivity._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objActivity = Nothing
                    objActivity = New clsActivity_Master
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintActivityUnkid = objActivity._Activityunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtName.Text, objActivity._Name1, objActivity._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchMeasures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMeasures.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboMeasures.ValueMember
                .DisplayMember = cboMeasures.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboMeasures.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboMeasures.SelectedValue = frm.SelectedValue
                cboMeasures.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMeasures_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboHeadType.ValueMember
                .DisplayMember = cboHeadType.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboHeadType.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboHeadType.SelectedValue = frm.SelectedValue
                cboHeadType.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddTransactionHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmTransactionHead_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim iRefId As Integer = 0
            Dim dsCombo As New DataSet
            Dim objTMaster As New clsTransactionHead
            If frm.displayDialog(iRefId, enAction.ADD_ONE) Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsCombo = objTMaster.getComboList("Lst", True)
                dsCombo = objTMaster.getComboList(FinancialYear._Object._DatabaseName, "Lst", True)
                'Sohail (21 Aug 2015) -- End
                With cboHeadType
                    .ValueMember = "tranheadunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables(0)
                    .SelectedValue = iRefId
                End With
            End If
            objTMaster = Nothing : dsCombo = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTransactionHead_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAddMeasures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddMeasures.Click
        Dim frm As New frmMeasure_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim iRefId As Integer = 0
            Dim dsCombo As New DataSet
            Dim objUMaster As New clsUnitMeasure_Master
            If frm.displayDialog(iRefId, enAction.ADD_ONE) Then
                dsCombo = objUMaster.getComboList("Lst", True)
                With cboMeasures
                    .ValueMember = "measureunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables(0)
                    .SelectedValue = iRefId
                End With
            End If
            objUMaster = Nothing : dsCombo = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddMeasures_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'Sohail (17 Sep 2014) -- Start
    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
#Region " Combobox Events "
    Private Sub cboHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboHeadType.SelectedIndexChanged
        Try
            If CInt(cboHeadType.SelectedValue) = enTranHeadType.EarningForEmployees Then
                chkTaxable.Enabled = True
                chkTaxableOT.Enabled = True
                chkTaxablePH.Enabled = True

                chkTaxable.Checked = True
                chkTaxableOT.Checked = True
                chkTaxablePH.Checked = True
            Else
                chkTaxable.Checked = False
                chkTaxable.Enabled = False

                chkTaxableOT.Checked = False
                chkTaxablePH.Enabled = False

                chkTaxablePH.Checked = False
                chkTaxablePH.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (17 Sep 2014) -- End

#Region " Control's Events "

    Private Sub chkOTActivity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOTActivity.CheckedChanged
        Try
            txtN_OT.Enabled = chkOTActivity.Checked
            txtP_OT.Enabled = chkOTActivity.Checked
            If chkOTActivity.Checked = False Then
                txtN_OT.Text = "" : txtP_OT.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkOTActivity_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbActivityInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbActivityInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbActivityInfo.Text = Language._Object.getCaption(Me.gbActivityInfo.Name, Me.gbActivityInfo.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblMeasures.Text = Language._Object.getCaption(Me.lblMeasures.Name, Me.lblMeasures.Text)
            Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
            Me.chkOTActivity.Text = Language._Object.getCaption(Me.chkOTActivity.Name, Me.chkOTActivity.Text)
            Me.lblPHoliday.Text = Language._Object.getCaption(Me.lblPHoliday.Name, Me.lblPHoliday.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.chkTaxable.Text = Language._Object.getCaption(Me.chkTaxable.Name, Me.chkTaxable.Text)
            Me.chkAppearOnPayslip.Text = Language._Object.getCaption(Me.chkAppearOnPayslip.Name, Me.chkAppearOnPayslip.Text)
            Me.chkTaxableOT.Text = Language._Object.getCaption(Me.chkTaxableOT.Name, Me.chkTaxableOT.Text)
            Me.chkAppearOTOnPayslip.Text = Language._Object.getCaption(Me.chkAppearOTOnPayslip.Name, Me.chkAppearOTOnPayslip.Text)
            Me.chkTaxablePH.Text = Language._Object.getCaption(Me.chkTaxablePH.Name, Me.chkTaxablePH.Text)
            Me.chkAppearPHOnPayslip.Text = Language._Object.getCaption(Me.chkAppearPHOnPayslip.Name, Me.chkAppearPHOnPayslip.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Activity Code is mandatory information. Please provide Activity Code.")
            Language.setMessage(mstrModuleName, 2, "Activity Name is mandatory information. Please provide Activity Name.")
            Language.setMessage(mstrModuleName, 3, "Measure is mandatory information. Please select Measure.")
            Language.setMessage(mstrModuleName, 4, "Head Type is mandatory information. Please select Head Type.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot change the activity type. Reason the activity is already posted to payroll.")
            Language.setMessage(mstrModuleName, 15, "")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class