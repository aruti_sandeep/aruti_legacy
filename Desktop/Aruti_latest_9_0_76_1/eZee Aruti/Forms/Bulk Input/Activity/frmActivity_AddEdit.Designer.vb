﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivity_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmActivity_AddEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbActivityInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkTaxablePH = New System.Windows.Forms.CheckBox
        Me.chkAppearPHOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkTaxableOT = New System.Windows.Forms.CheckBox
        Me.chkAppearOTOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkTaxable = New System.Windows.Forms.CheckBox
        Me.chkAppearOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkOTActivity = New System.Windows.Forms.CheckBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.objbtnAddMeasures = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchMeasures = New eZee.Common.eZeeGradientButton
        Me.cboMeasures = New System.Windows.Forms.ComboBox
        Me.lblMeasures = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboHeadType = New System.Windows.Forms.ComboBox
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.txtN_OT = New eZee.TextBox.NumericTextBox
        Me.lblPHoliday = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtP_OT = New eZee.TextBox.NumericTextBox
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbActivityInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.gbActivityInfo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(374, 357)
        Me.Panel1.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 302)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(374, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(162, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(265, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbActivityInfo
        '
        Me.gbActivityInfo.BorderColor = System.Drawing.Color.Black
        Me.gbActivityInfo.Checked = False
        Me.gbActivityInfo.CollapseAllExceptThis = False
        Me.gbActivityInfo.CollapsedHoverImage = Nothing
        Me.gbActivityInfo.CollapsedNormalImage = Nothing
        Me.gbActivityInfo.CollapsedPressedImage = Nothing
        Me.gbActivityInfo.CollapseOnLoad = False
        Me.gbActivityInfo.Controls.Add(Me.chkTaxablePH)
        Me.gbActivityInfo.Controls.Add(Me.chkAppearPHOnPayslip)
        Me.gbActivityInfo.Controls.Add(Me.chkTaxableOT)
        Me.gbActivityInfo.Controls.Add(Me.chkAppearOTOnPayslip)
        Me.gbActivityInfo.Controls.Add(Me.chkTaxable)
        Me.gbActivityInfo.Controls.Add(Me.chkAppearOnPayslip)
        Me.gbActivityInfo.Controls.Add(Me.chkOTActivity)
        Me.gbActivityInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbActivityInfo.Controls.Add(Me.objbtnAddMeasures)
        Me.gbActivityInfo.Controls.Add(Me.objbtnSearchMeasures)
        Me.gbActivityInfo.Controls.Add(Me.cboMeasures)
        Me.gbActivityInfo.Controls.Add(Me.lblMeasures)
        Me.gbActivityInfo.Controls.Add(Me.lblName)
        Me.gbActivityInfo.Controls.Add(Me.lblDescription)
        Me.gbActivityInfo.Controls.Add(Me.txtName)
        Me.gbActivityInfo.Controls.Add(Me.txtDescription)
        Me.gbActivityInfo.Controls.Add(Me.txtCode)
        Me.gbActivityInfo.Controls.Add(Me.lblCode)
        Me.gbActivityInfo.Controls.Add(Me.cboHeadType)
        Me.gbActivityInfo.Controls.Add(Me.lblTransactionHead)
        Me.gbActivityInfo.Controls.Add(Me.txtN_OT)
        Me.gbActivityInfo.Controls.Add(Me.lblPHoliday)
        Me.gbActivityInfo.Controls.Add(Me.Label1)
        Me.gbActivityInfo.Controls.Add(Me.txtP_OT)
        Me.gbActivityInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbActivityInfo.ExpandedHoverImage = Nothing
        Me.gbActivityInfo.ExpandedNormalImage = Nothing
        Me.gbActivityInfo.ExpandedPressedImage = Nothing
        Me.gbActivityInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbActivityInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbActivityInfo.HeaderHeight = 25
        Me.gbActivityInfo.HeaderMessage = ""
        Me.gbActivityInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbActivityInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbActivityInfo.HeightOnCollapse = 0
        Me.gbActivityInfo.LeftTextSpace = 0
        Me.gbActivityInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbActivityInfo.Name = "gbActivityInfo"
        Me.gbActivityInfo.OpenHeight = 300
        Me.gbActivityInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbActivityInfo.ShowBorder = True
        Me.gbActivityInfo.ShowCheckBox = False
        Me.gbActivityInfo.ShowCollapseButton = False
        Me.gbActivityInfo.ShowDefaultBorderColor = True
        Me.gbActivityInfo.ShowDownButton = False
        Me.gbActivityInfo.ShowHeader = True
        Me.gbActivityInfo.Size = New System.Drawing.Size(374, 357)
        Me.gbActivityInfo.TabIndex = 0
        Me.gbActivityInfo.Temp = 0
        Me.gbActivityInfo.Text = "Activity Info"
        Me.gbActivityInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkTaxablePH
        '
        Me.chkTaxablePH.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTaxablePH.Location = New System.Drawing.Point(226, 278)
        Me.chkTaxablePH.Name = "chkTaxablePH"
        Me.chkTaxablePH.Size = New System.Drawing.Size(110, 17)
        Me.chkTaxablePH.TabIndex = 25
        Me.chkTaxablePH.Text = "Taxable PH"
        Me.chkTaxablePH.UseVisualStyleBackColor = True
        '
        'chkAppearPHOnPayslip
        '
        Me.chkAppearPHOnPayslip.Checked = True
        Me.chkAppearPHOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearPHOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAppearPHOnPayslip.Location = New System.Drawing.Point(12, 278)
        Me.chkAppearPHOnPayslip.Name = "chkAppearPHOnPayslip"
        Me.chkAppearPHOnPayslip.Size = New System.Drawing.Size(160, 17)
        Me.chkAppearPHOnPayslip.TabIndex = 24
        Me.chkAppearPHOnPayslip.Text = "Appear PH on Payslip"
        Me.chkAppearPHOnPayslip.UseVisualStyleBackColor = True
        '
        'chkTaxableOT
        '
        Me.chkTaxableOT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTaxableOT.Location = New System.Drawing.Point(226, 255)
        Me.chkTaxableOT.Name = "chkTaxableOT"
        Me.chkTaxableOT.Size = New System.Drawing.Size(110, 17)
        Me.chkTaxableOT.TabIndex = 23
        Me.chkTaxableOT.Text = "Taxable OT"
        Me.chkTaxableOT.UseVisualStyleBackColor = True
        '
        'chkAppearOTOnPayslip
        '
        Me.chkAppearOTOnPayslip.Checked = True
        Me.chkAppearOTOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearOTOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAppearOTOnPayslip.Location = New System.Drawing.Point(12, 255)
        Me.chkAppearOTOnPayslip.Name = "chkAppearOTOnPayslip"
        Me.chkAppearOTOnPayslip.Size = New System.Drawing.Size(160, 17)
        Me.chkAppearOTOnPayslip.TabIndex = 22
        Me.chkAppearOTOnPayslip.Text = "Appear OT on Payslip"
        Me.chkAppearOTOnPayslip.UseVisualStyleBackColor = True
        '
        'chkTaxable
        '
        Me.chkTaxable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTaxable.Location = New System.Drawing.Point(226, 232)
        Me.chkTaxable.Name = "chkTaxable"
        Me.chkTaxable.Size = New System.Drawing.Size(110, 17)
        Me.chkTaxable.TabIndex = 21
        Me.chkTaxable.Text = "Taxable Activity"
        Me.chkTaxable.UseVisualStyleBackColor = True
        '
        'chkAppearOnPayslip
        '
        Me.chkAppearOnPayslip.Checked = True
        Me.chkAppearOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAppearOnPayslip.Location = New System.Drawing.Point(12, 232)
        Me.chkAppearOnPayslip.Name = "chkAppearOnPayslip"
        Me.chkAppearOnPayslip.Size = New System.Drawing.Size(160, 17)
        Me.chkAppearOnPayslip.TabIndex = 20
        Me.chkAppearOnPayslip.Text = "Appear Activity on Payslip"
        Me.chkAppearOnPayslip.UseVisualStyleBackColor = True
        '
        'chkOTActivity
        '
        Me.chkOTActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOTActivity.Location = New System.Drawing.Point(194, 35)
        Me.chkOTActivity.Name = "chkOTActivity"
        Me.chkOTActivity.Size = New System.Drawing.Size(168, 17)
        Me.chkOTActivity.TabIndex = 16
        Me.chkOTActivity.Text = "Consider This Activity for OT"
        Me.chkOTActivity.UseVisualStyleBackColor = True
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(318, 60)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'objbtnAddMeasures
        '
        Me.objbtnAddMeasures.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddMeasures.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddMeasures.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddMeasures.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddMeasures.BorderSelected = False
        Me.objbtnAddMeasures.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnAddMeasures.Image = CType(resources.GetObject("objbtnAddMeasures.Image"), System.Drawing.Image)
        Me.objbtnAddMeasures.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddMeasures.Location = New System.Drawing.Point(345, 87)
        Me.objbtnAddMeasures.Name = "objbtnAddMeasures"
        Me.objbtnAddMeasures.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddMeasures.TabIndex = 8
        '
        'objbtnSearchMeasures
        '
        Me.objbtnSearchMeasures.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMeasures.BorderSelected = False
        Me.objbtnSearchMeasures.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchMeasures.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMeasures.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMeasures.Location = New System.Drawing.Point(318, 87)
        Me.objbtnSearchMeasures.Name = "objbtnSearchMeasures"
        Me.objbtnSearchMeasures.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMeasures.TabIndex = 7
        '
        'cboMeasures
        '
        Me.cboMeasures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMeasures.DropDownWidth = 350
        Me.cboMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMeasures.FormattingEnabled = True
        Me.cboMeasures.Location = New System.Drawing.Point(80, 87)
        Me.cboMeasures.Name = "cboMeasures"
        Me.cboMeasures.Size = New System.Drawing.Size(232, 21)
        Me.cboMeasures.TabIndex = 6
        '
        'lblMeasures
        '
        Me.lblMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeasures.Location = New System.Drawing.Point(8, 90)
        Me.lblMeasures.Name = "lblMeasures"
        Me.lblMeasures.Size = New System.Drawing.Size(66, 15)
        Me.lblMeasures.TabIndex = 5
        Me.lblMeasures.Text = "Measures"
        Me.lblMeasures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 63)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(66, 15)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 144)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(66, 15)
        Me.lblDescription.TabIndex = 13
        Me.lblDescription.Text = "Description"
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(80, 60)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(232, 21)
        Me.txtName.TabIndex = 3
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(80, 141)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(232, 73)
        Me.txtDescription.TabIndex = 14
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(80, 33)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(108, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(66, 15)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHeadType
        '
        Me.cboHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadType.DropDownWidth = 350
        Me.cboHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadType.FormattingEnabled = True
        Me.cboHeadType.Location = New System.Drawing.Point(80, 114)
        Me.cboHeadType.Name = "cboHeadType"
        Me.cboHeadType.Size = New System.Drawing.Size(232, 21)
        Me.cboHeadType.TabIndex = 10
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(8, 117)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(66, 15)
        Me.lblTransactionHead.TabIndex = 9
        Me.lblTransactionHead.Text = "Head Type"
        Me.lblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtN_OT
        '
        Me.txtN_OT.AllowNegative = False
        Me.txtN_OT.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtN_OT.DigitsInGroup = 0
        Me.txtN_OT.Flags = 65536
        Me.txtN_OT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtN_OT.Location = New System.Drawing.Point(80, 141)
        Me.txtN_OT.MaxDecimalPlaces = 4
        Me.txtN_OT.MaxWholeDigits = 9
        Me.txtN_OT.Name = "txtN_OT"
        Me.txtN_OT.Prefix = ""
        Me.txtN_OT.RangeMax = 1.7976931348623157E+308
        Me.txtN_OT.RangeMin = -1.7976931348623157E+308
        Me.txtN_OT.Size = New System.Drawing.Size(73, 21)
        Me.txtN_OT.TabIndex = 18
        Me.txtN_OT.Text = "0"
        Me.txtN_OT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtN_OT.Visible = False
        '
        'lblPHoliday
        '
        Me.lblPHoliday.BackColor = System.Drawing.Color.Transparent
        Me.lblPHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPHoliday.Location = New System.Drawing.Point(159, 144)
        Me.lblPHoliday.Name = "lblPHoliday"
        Me.lblPHoliday.Size = New System.Drawing.Size(74, 15)
        Me.lblPHoliday.TabIndex = 17
        Me.lblPHoliday.Text = "Public Holiday"
        Me.lblPHoliday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPHoliday.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 144)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 15)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Normal OT"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.Visible = False
        '
        'txtP_OT
        '
        Me.txtP_OT.AllowNegative = False
        Me.txtP_OT.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtP_OT.DigitsInGroup = 0
        Me.txtP_OT.Flags = 65536
        Me.txtP_OT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtP_OT.Location = New System.Drawing.Point(239, 141)
        Me.txtP_OT.MaxDecimalPlaces = 4
        Me.txtP_OT.MaxWholeDigits = 9
        Me.txtP_OT.Name = "txtP_OT"
        Me.txtP_OT.Prefix = ""
        Me.txtP_OT.RangeMax = 1.7976931348623157E+308
        Me.txtP_OT.RangeMin = -1.7976931348623157E+308
        Me.txtP_OT.Size = New System.Drawing.Size(73, 21)
        Me.txtP_OT.TabIndex = 18
        Me.txtP_OT.Text = "0"
        Me.txtP_OT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtP_OT.Visible = False
        '
        'frmActivity_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(374, 357)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmActivity_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Activity"
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbActivityInfo.ResumeLayout(False)
        Me.gbActivityInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbActivityInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblMeasures As System.Windows.Forms.Label
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents cboHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents cboMeasures As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchMeasures As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddMeasures As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents chkOTActivity As System.Windows.Forms.CheckBox
    Friend WithEvents txtN_OT As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPHoliday As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtP_OT As eZee.TextBox.NumericTextBox
    Friend WithEvents chkTaxable As System.Windows.Forms.CheckBox
    Friend WithEvents chkAppearOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents chkTaxableOT As System.Windows.Forms.CheckBox
    Friend WithEvents chkAppearOTOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents chkTaxablePH As System.Windows.Forms.CheckBox
    Friend WithEvents chkAppearPHOnPayslip As System.Windows.Forms.CheckBox
End Class
