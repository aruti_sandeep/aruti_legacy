﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region

Public Class frmBulkInput

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBulkInput"
    Private objActTran As clsPayActivity_Tran
    Private mintPayActivityTranId As Integer = 0
    Private mintEmpCCId As Integer = 0
    Private mDecNF_OT As Decimal = 0
    Private mDecPH_OT As Decimal = 0
    Private mDblSftHrs As Double = 0
    Private mblnIsFormualBased As Boolean = False
    Private mDecAcitityRate As Decimal = 0
    Private mIntTrnHeadId As Integer = 0
    Private mDicWeekName As Dictionary(Of String, Integer)
    Private mTrnHeadName As String = String.Empty
    Private mAllocationIds As String = String.Empty
    Private mblnIsWeekEnd As Boolean = False
    Private mintShiftUnkid As Integer = 0
    Private mblnIsHoliday As Boolean = False
    Private mDecNormalAmt, mDecNormalOTAmt As Decimal
    Private dblOT_Hrs As Double


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA
    Private mIntOT_TrnHeadId As Integer = 0
    Private mIntPH_TrnHeadId As Integer = 0
    'Pinkal (4-Sep-2014) -- End


    'Pinkal (23-Sep-2014) -- Start
    'Enhancement -  PAY_A CHANGES IN PPA
    Private mblnIsOTActivity As Boolean = False
    'Pinkal (23-Sep-2014) -- End


#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objPMaster As New clscommom_period_Tran
        Dim objAMaster As New clsActivity_Master
        Dim objCC As New clscostcenter_master
        Dim objMData As New clsMasterData
        Try
            dsCombo = objAMaster.getComboList("List", True)
            With cboActivity
                .ValueMember = "activityunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPMaster.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombo = objPMaster.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                .Text = ""
            End With

            dsCombo = objCC.getComboList("List", True)
            With cboCostcenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                .Text = ""
            End With


            dsCombo = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objPMaster = Nothing : objAMaster = Nothing
            objCC = Nothing : objMData = Nothing
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objActTran._Activity_Date = dtpDate.Value.Date
            objActTran._Activity_Value = txtValue.Decimal
            objActTran._Activityunkid = CInt(cboActivity.SelectedValue)
            objActTran._Approveruserunkid = 0
            objActTran._Costcenterunkid = CInt(IIf(CInt(cboCostcenter.SelectedValue) <= 0, mintEmpCCId, CInt(cboCostcenter.SelectedValue)))
            objActTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objActTran._Isapproved = False
            objActTran._Isposted = False
            objActTran._Isvoid = False
            objActTran._Periodunkid = CInt(cboPeriod.SelectedValue)
            objActTran._Userunkid = User._Object._Userunkid
            objActTran._Voiddatetime = Nothing
            objActTran._Voidreason = ""
            objActTran._Voiduserunkid = -1
            If mblnIsFormualBased = False Then
                objActTran._Activity_Rate = CDec(txtRate.Text)
                objActTran._Amount = CDec(txtFinalAmt.Tag)
            Else
                objActTran._Activity_Rate = 0
                objActTran._Amount = 0
            End If
            objActTran._Isformula_Based = mblnIsFormualBased
            objActTran._Tranheadunkid = mIntTrnHeadId
            objActTran._Normal_Amount = mDecNormalAmt

            'Pinkal (23-Sep-2014) -- Start
            'Enhancement -  PAY_A CHANGES IN PPA
            If mblnIsOTActivity Then
                If mblnIsHoliday = True Or mblnIsWeekEnd = True Then
                    objActTran._PH_Factor = mDecPH_OT
                    objActTran._Normal_Factor = 0
                    objActTran._Normal_Hrs = 0
                Else
                    objActTran._PH_Factor = 0
                    objActTran._Normal_Factor = mDecNF_OT
                    objActTran._Normal_Hrs = CDec(mDblSftHrs)
                End If
            Else
                dblOT_Hrs = 0
                mDecNormalOTAmt = 0
                objActTran._PH_Factor = 0
                objActTran._Normal_Factor = 0
                objActTran._Normal_Hrs = CDec(mDblSftHrs)
            End If
            'Pinkal (23-Sep-2014) -- End



            objActTran._Ot_Amount = mDecNormalOTAmt
            objActTran._Ot_Hrs = CDec(IIf(dblOT_Hrs <= 0, 0, dblOT_Hrs))


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            objActTran._OT_Tranheadunkid = mIntOT_TrnHeadId
            objActTran._PH_Tranheadunkid = mIntPH_TrnHeadId
            'Pinkal (4-Sep-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue(ByVal iTag As Integer)
        Try
            objActTran._Payactivitytranunkid = iTag
            mintPayActivityTranId = iTag
            'txtValue.Decimal = CDec(objActTran._Activity_Value) 'Sohail (03 Jul 2017)
            cboActivity.SelectedValue = objActTran._Activityunkid
            cboCostcenter.SelectedValue = objActTran._Costcenterunkid
            cboEmployee.SelectedValue = objActTran._Employeeunkid
            cboPeriod.SelectedValue = objActTran._Periodunkid
            dtpDate.Value = objActTran._Activity_Date
            txtValue.Decimal = CDec(objActTran._Activity_Value) 'Sohail (03 Jul 2017)
            Call EnDi_Ctrls(False)
            cboEmployee.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub EnDi_Ctrls(ByVal blnFlag As Boolean)
        Try

            'Pinkal (23-Sep-2014) -- Start
            'Enhancement -  PAY_A CHANGES IN PPA

            'If chkCostCenter.Checked = True Then
            '    cboCostcenter.Enabled = blnFlag
            'End If
            'cboActivity.Enabled = blnFlag
            'chkCostCenter.Enabled = blnFlag

            'Pinkal (23-Sep-2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Enable_Disable_Ctrls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Detail_Info()
        Dim dList As New DataSet
        Try
            dList = objActTran.GetList("List", CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            lvDetails.Items.Clear()
            For Each dRow As DataRow In dList.Tables(0).Rows
                Dim lvItem As New ListViewItem



                'Pinkal (06-Mar-2014) -- Start
                'Enhancement : WTCL Changes
                'lvItem.Text = eZeeDate.convertDate(dRow.Item("ADate").ToString).ToShortDateString

                lvItem.Text = ""
                lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("ADate").ToString).ToShortDateString)

                'Pinkal (06-Mar-2014) -- End


                lvItem.SubItems.Add(dRow.Item("activityname").ToString)
                lvItem.SubItems.Add(Format(CDec(dRow.Item("activity_value")), GUI.fmtCurrency))
                lvItem.SubItems.Add(dRow.Item("measure").ToString)
                If CBool(dRow.Item("isformula_based")) = False Then
                    lvItem.SubItems.Add(Format(CDec(dRow.Item("activity_rate")), GUI.fmtCurrency).ToString)
                    lvItem.SubItems.Add(Format(CDec(dRow.Item("amount")), GUI.fmtCurrency).ToString)
                Else
                    lvItem.SubItems.Add("")
                    lvItem.SubItems.Add("")
                End If
                lvItem.SubItems.Add(dRow.Item("costcentername").ToString)
                lvItem.SubItems.Add(dRow.Item("employeename").ToString)
                lvItem.Tag = dRow.Item("payactivitytranunkid").ToString

                If CBool(dRow.Item("isposted")) = True Then
                    lvItem.ForeColor = Color.Gray
                End If

                lvDetails.Items.Add(lvItem)
            Next
            lvDetails.GroupingColumn = objcolhEmployee
            lvDetails.DisplayGroups(True)
            lvDetails.SortBy(colhDate.Index, SortOrder.Descending)

            If lvDetails.Items.Count > 0 Then
                Call Fill_Emp_Total()
            Else
                lvEmpTotal.Items.Clear()
            End If
            Call Fill_Comp_Total()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Detail_Info", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Emp_Total()
        Dim dList As New DataSet
        Try
            'S.SANDEEP [ 10 DEC 2014 ] -- START
            'dList = objActTran.Employee_Based_Total(CInt(IIf(radEByActivity.Checked = True, 1, 2)), dtpDate.Value.Date, CInt(cboEmployee.SelectedValue))
            If radEByDate.Checked = True Then
                dList = objActTran.Employee_Based_Total(CInt(IIf(radEByActivity.Checked = True, 1, 2)), 1, CInt(cboEmployee.SelectedValue), dtpDate.Value.Date)
            ElseIf radEPeriod.Checked = True Then
                dList = objActTran.Employee_Based_Total(CInt(IIf(radEByActivity.Checked = True, 1, 2)), 2, CInt(cboEmployee.SelectedValue), , CInt(cboPeriod.SelectedValue))
            End If
            'S.SANDEEP [ 10 DEC 2014 ] -- END
            lvEmpTotal.Items.Clear()
            For Each dRow As DataRow In dList.Tables(0).Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dRow.Item("TotalBy").ToString
                lvItem.SubItems.Add(Format(CDec(dRow.Item("Total")), GUI.fmtCurrency))
                lvItem.SubItems.Add(dRow.Item("employeename").ToString)
                lvEmpTotal.Items.Add(lvItem)
            Next
            lvEmpTotal.GroupingColumn = objcolhEmp
            lvEmpTotal.DisplayGroups(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Emp_Total", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Comp_Total()
        Dim dList As New DataSet
        Try
            If cboAllocations.SelectedValue IsNot Nothing Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

                'Select Case CInt(cboAllocations.SelectedValue)
                '    Case enAllocation.BRANCH
                '        mAllocationIds = " AND hremployee_master.stationunkid IN ("
                '    Case enAllocation.DEPARTMENT_GROUP
                '        mAllocationIds = " AND hremployee_master.deptgroupunkid IN ("
                '    Case enAllocation.DEPARTMENT
                '        mAllocationIds = " AND hremployee_master.departmentunkid IN ("
                '    Case enAllocation.SECTION_GROUP
                '        mAllocationIds = " AND hremployee_master.sectiongroupunkid IN ("
                '    Case enAllocation.SECTION
                '        mAllocationIds = " AND hremployee_master.sectionunkid IN ("
                '    Case enAllocation.UNIT_GROUP
                '        mAllocationIds = " AND hremployee_master.unitgroupunkid IN ("
                '    Case enAllocation.UNIT
                '        mAllocationIds = " AND hremployee_master.unitunkid IN ("
                '    Case enAllocation.TEAM
                '        mAllocationIds = " AND hremployee_master.teamunkid IN ("
                '    Case enAllocation.JOB_GROUP
                '        mAllocationIds = " AND hremployee_master.jobgroupunkid IN ("
                '    Case enAllocation.JOBS
                '        mAllocationIds = " AND hremployee_master.jobunkid IN ("
                '    Case enAllocation.CLASS_GROUP
                '        mAllocationIds = " AND hremployee_master.classgroupunkid IN ("
                '    Case enAllocation.CLASSES

                '        'Pinkal (06-Mar-2014) -- Start
                '        'Enhancement : WTCL Changes
                '        'mAllocationIds = " AND hremployee_master.classesunkid IN ("
                '        mAllocationIds = " AND hremployee_master.classunkid IN ("
                '        'Pinkal (06-Mar-2014) -- End


                'End Select

                Select Case CInt(cboAllocations.SelectedValue)
                    Case enAllocation.BRANCH
                        mAllocationIds = " AND Alloc.stationunkid IN ("
                    Case enAllocation.DEPARTMENT_GROUP
                        mAllocationIds = " AND Alloc.deptgroupunkid IN ("
                    Case enAllocation.DEPARTMENT
                        mAllocationIds = " AND Alloc.departmentunkid IN ("
                    Case enAllocation.SECTION_GROUP
                        mAllocationIds = " AND Alloc.sectiongroupunkid IN ("
                    Case enAllocation.SECTION
                        mAllocationIds = " AND Alloc.sectionunkid IN ("
                    Case enAllocation.UNIT_GROUP
                        mAllocationIds = " AND Alloc.unitgroupunkid IN ("
                    Case enAllocation.UNIT
                        mAllocationIds = " AND Alloc.unitunkid IN ("
                    Case enAllocation.TEAM
                        mAllocationIds = " AND Alloc.teamunkid IN ("
                    Case enAllocation.CLASS_GROUP
                        mAllocationIds = " AND Alloc.classgroupunkid IN ("
                    Case enAllocation.CLASSES
                        mAllocationIds = " AND Alloc.classunkid IN ("
                    Case enAllocation.JOB_GROUP
                        mAllocationIds = " AND Jobs.jobgroupunkid IN ("
                    Case enAllocation.JOBS
                        mAllocationIds = " AND Jobs.jobunkid IN ("
                    Case enAllocation.COST_CENTER
                        mAllocationIds = " AND CC.costcenterunkid IN ("

                End Select

                'Pinkal (24-Aug-2015) -- End

                Dim sAllocation As String = String.Empty
                Dim selectedTags As List(Of String) = lvAllocation.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
                sAllocation = String.Join(", ", selectedTags.ToArray())
                If sAllocation.Trim.Length > 0 Then
                    mAllocationIds &= sAllocation & ") "
                Else
                    mAllocationIds = " "
                End If
                If radByDate.Checked = True Then

                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                    'dList = objActTran.Company_Based_Total(1, CInt(IIf(radCByActivity.Checked = True, 1, 2)), 0, dtpDate.Value.Date, mAllocationIds)
                    dList = objActTran.Company_Based_Total(1, CInt(IIf(radCByActivity.Checked = True, 1, 2)), Company._Object._DatabaseName, User._Object._Userunkid _
                                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                 , ConfigParameter._Object._UserAccessModeSetting, True, 0, dtpDate.Value.Date, mAllocationIds)
                    'Pinkal (24-Aug-2015) -- End


                ElseIf radPeriod.Checked = True Then

                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                    dList = objActTran.Company_Based_Total(2, CInt(IIf(radCByActivity.Checked = True, 1, 2)), Company._Object._DatabaseName, User._Object._Userunkid _
                                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                 , ConfigParameter._Object._UserAccessModeSetting, True, CInt(cboPeriod.SelectedValue), Nothing, mAllocationIds)
                    'dList = objActTran.Company_Based_Total(2, CInt(IIf(radCByActivity.Checked = True, 1, 2)), CInt(cboPeriod.SelectedValue), Nothing, mAllocationIds)
                    'Pinkal (24-Aug-2015) -- End


                End If

                lvCompanyTotal.Items.Clear()
                For Each dRow As DataRow In dList.Tables(0).Rows
                    Dim lvItem As New ListViewItem
                    lvItem.Text = dRow.Item("TotalBy").ToString
                    lvItem.SubItems.Add(Format(CDec(dRow.Item("Total")), GUI.fmtCurrency))
                    lvCompanyTotal.Items.Add(lvItem)
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Comp_Total", mstrModuleName)
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period in order to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If (cboCostcenter.Enabled = True AndAlso CInt(cboCostcenter.SelectedValue) <= 0) AndAlso mintEmpCCId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Cost Center is mandatory information. Please select Cost Center in order to continue."), enMsgBoxStyle.Information)
                cboCostcenter.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee is mandatory information. Please select Employee in order to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboActivity.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Activity is mandatory information. Please select Activity in order to continue."), enMsgBoxStyle.Information)
                cboActivity.Focus()
                Return False
            End If

            If txtValue.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You have not entered the activity value. Activity value is mandatory information."), enMsgBoxStyle.Information)
                txtValue.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Insert_Update(ByVal IsOperationSelected As Boolean)
        Try
            If Is_Valid() = False Then Exit Sub
            If mintPayActivityTranId <= 0 Then
                If IsOperationSelected = False Then
                    mintPayActivityTranId = objActTran.isExist(CInt(cboEmployee.SelectedValue), dtpDate.Value.Date, CInt(cboActivity.SelectedValue), CInt(IIf(CInt(cboCostcenter.SelectedValue) <= 0, mintEmpCCId, CInt(cboCostcenter.SelectedValue))))
                    If mintPayActivityTranId > 0 Then
                        gbActivityDetails.Enabled = False
                        'S.SANDEEP [ 10 DEC 2014 ] -- START
                        'gbCompanyTotal.Enabled = False
                        'gbEmpTotal.Enabled = False
                        pnlEmpTotal.Enabled = False
                        plnCompanyTotals.Enabled = False
                        'S.SANDEEP [ 10 DEC 2014 ] -- END
                        gbOperation.Visible = True
                        gbOperation.BringToFront()
                        'Pinkal (28-Oct-2014) -- Start
                        'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT
                        objFooter.Enabled = False
                        gbOperation.Focus()
                        rdUpdateExisting.Focus()
                        rdUpdateExisting.Select()
                        'Pinkal (28-Oct-2014) -- End
                        Exit Sub
                    End If
                End If
            End If
            objActTran._Payactivitytranunkid = mintPayActivityTranId
            Call SetValue()
            If mintPayActivityTranId <= 0 Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objActTran.Insert()
                objActTran.Insert(ConfigParameter._Object._CurrentDateAndTime)
                'Shani(24-Aug-2015) -- End

            Else

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objActTran.Update()
                objActTran.Update(ConfigParameter._Object._CurrentDateAndTime)
                'Shani(24-Aug-2015) -- End

            End If
            Call Fill_Detail_Info()
            mintPayActivityTranId = 0
            txtValue.Decimal = 0 : cboActivity.SelectedValue = 0 : cboActivity.Text = "" : txtRate.Text = ""
            dtpDate.Enabled = False : cboEmployee.Enabled = True : cboEmployee.Focus() 'S.SANDEEP [ 20 JULY 2013 ] -- START-- END
            Call EnDi_Ctrls(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update", mstrModuleName)
        End Try
    End Sub

    Private Sub Clear_Data(Optional ByVal OnlyCtrls As Boolean = False)
        Try

            'Pinkal (28-Oct-2014) -- Start
            'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT
            'cboPeriod.SelectedValue = 0 : cboPeriod.Text = ""
            'Pinkal (28-Oct-2014) -- End
            chkCostCenter.Checked = False : cboCostcenter.SelectedValue = 0 : cboCostcenter.Text = ""
            'Pinkal (28-Oct-2014) -- Start
            'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT
            'cboEmployee.SelectedValue = 0 : cboEmployee.Text = ""
            'Pinkal (28-Oct-2014) -- End
            cboActivity.SelectedValue = 0 : cboActivity.Text = ""
            If OnlyCtrls = False Then
                lvDetails.Items.Clear() : lvEmpTotal.Items.Clear() : lvCompanyTotal.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Clear_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")

                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                Case enAllocation.COST_CENTER
                    Dim objCostMst As New clscostcenter_master
                    dList = objCostMst.GetList("List")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
                    'Pinkal (24-Aug-2015) -- End

            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 8 Then
                colhAllocations.Width = 366 - 20
            Else
                colhAllocations.Width = 366
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            objchkAll.Checked = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean)
        Try
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeletePayPerActivity
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBulkInput_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objActTran = New clsPayActivity_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call Fill_Combo()
            Call chkCostCenter_CheckedChanged(sender, e)

            Dim days() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames
            mDicWeekName = New Dictionary(Of String, Integer)
            For i As Integer = 0 To 6
                mDicWeekName.Add(days(i).ToString, i)
            Next


            'Pinkal (28-Oct-2014) -- Start
            'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT
            cboPeriod.Select()
            'Pinkal (28-Oct-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBulkInput_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            gbActivityDetails.Enabled = True
            'S.SANDEEP [ 10 DEC 2014 ] -- START
            'gbCompanyTotal.Enabled = True
            'gbEmpTotal.Enabled = True
            pnlEmpTotal.Enabled = True
            plnCompanyTotals.Enabled = True
            'S.SANDEEP [ 10 DEC 2014 ] -- END
            gbOperation.Visible = False
            If rdNoAction.Checked = True Then
                'Pinkal (28-Oct-2014) -- Start
                'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT
                objFooter.Enabled = True
                btnChangeDate.Select()
                Exit Sub
            ElseIf rdAddNew.Checked = True Then
                mintPayActivityTranId = -1
            End If
            Call Insert_Update(True)
            rdUpdateExisting.Checked = True

            'Pinkal (28-Oct-2014) -- Start
            'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT
            objFooter.Enabled = True
            btnChangeDate.Select()
            'Pinkal (28-Oct-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : WTCL Changes

            'If lvDetails.SelectedItems.Count > 0 Then
            '    If lvDetails.SelectedItems(0).ForeColor = Color.Gray Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot delete selected transaction. Reason : This transaction is already posted to payroll."), enMsgBoxStyle.Information)
            '        Call Clear_Data(True)
            '        Exit Sub
            '    Else
            '        objActTran._Isvoid = True
            '        objActTran._Voiduserunkid = User._Object._Userunkid
            '        objActTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            '        Dim frm As New frmReasonSelection
            '        If User._Object._Isrighttoleft = True Then
            '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '            frm.RightToLeftLayout = True
            '            Call Language.ctlRightToLeftlayOut(frm)
            '        End If
            '        Dim mstrVoidReason As String = String.Empty
            '        frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
            '        If mstrVoidReason.Length <= 0 Then
            '            Exit Sub
            '        Else
            '            objActTran._Voidreason = mstrVoidReason
            '        End If
            '        frm = Nothing
            '        If objActTran.Delete(CInt(lvDetails.SelectedItems(0).Tag)) = False Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Problem in deleting data."), enMsgBoxStyle.Information)
            '        End If
            '        Call Clear_Data(True)
            '        Call Fill_Detail_Info()
            '        If lvDetails.Items.Count <= 0 Then lvEmpTotal.Items.Clear()
            '    End If
            'End If


            If lvDetails.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please Check employee activities to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If lvDetails.CheckedItems.Count > 0 Then

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Are you sure you want to delete checked activites ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    objActTran._Isvoid = True
                    objActTran._Voiduserunkid = User._Object._Userunkid
                    objActTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objActTran._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing

                    For i As Integer = 0 To lvDetails.CheckedItems.Count - 1

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objActTran.Delete(CInt(lvDetails.CheckedItems(i).Tag)) = False Then
                        If objActTran.Delete(CInt(lvDetails.CheckedItems(i).Tag), ConfigParameter._Object._CurrentDateAndTime) = False Then
                            'Shani(24-Aug-2015) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Problem in deleting data."), enMsgBoxStyle.Information)
                            Exit For
                        End If
                    Next
                    chkSelectAll.Checked = False
                    Call Fill_Detail_Info()
                    If lvDetails.Items.Count <= 0 Then lvEmpTotal.Items.Clear()
                End If
            End If

            'Pinkal (06-Mar-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnChangeDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeDate.Click
        Try
            dtpDate.Enabled = True : cboEmployee.Enabled = True
            Call Clear_Data()

            'Pinkal (23-Sep-2014) -- Start
            'Enhancement -  PAY_A CHANGES IN PPA
            dtpDate.Select()
            'Pinkal (23-Sep-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeDate_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control Events "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
            Call objchkAll_CheckedChanged(Nothing, Nothing)
            Call Fill_Comp_Total()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvDetails_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvDetails.SelectedIndexChanged
        Try
            If lvDetails.SelectedItems.Count > 0 Then
                If lvDetails.SelectedItems(0).ForeColor = Color.Gray Then
                    lvDetails.SelectedItems(0).Selected = False : Exit Sub
                End If
                Call GetValue(CInt(lvDetails.SelectedItems(0).Tag))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDetails_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                'S.SANDEEP [ 20 JULY 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dtpDate.MinDate = CDate(eZeeDate.convertDate("17530101").ToShortDateString)
                'dtpDate.MaxDate = CDate(eZeeDate.convertDate("99981231").ToShortDateString)

                dtpDate.MinDate = CDate(eZeeDate.convertDate("17530101"))
                dtpDate.MaxDate = CDate(eZeeDate.convertDate("99981231"))
                'S.SANDEEP [ 20 JULY 2013 ] -- END
                dtpDate.Value = objPeriod._Start_Date
                dtpDate.MinDate = objPeriod._Start_Date
                dtpDate.MaxDate = objPeriod._End_Date
                objPeriod = Nothing
                Dim dsCombo As New DataSet
                Dim objEMaster As New clsEmployee_Master
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombo = objEMaster.GetEmployeeList("List", True, , , , , , , , , , , , , dtpDate.MinDate, dtpDate.MaxDate)
                dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     dtpDate.MinDate, _
                                                     dtpDate.MaxDate, _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, False, "List", True)
                'S.SANDEEP [04 JUN 2015] -- END


                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsCombo.Tables(0)
                    .SelectedValue = 0
                    .Text = ""
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboActivity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboActivity.SelectedIndexChanged, dtpDate.ValueChanged
        Try
            If CInt(cboActivity.SelectedValue) > 0 Then
                Dim objActivity As New clsActivity_Master
                Dim objMeasure As New clsUnitMeasure_Master

                objActivity._Activityunkid = CInt(cboActivity.SelectedValue)
                objMeasure._Measureunkid = objActivity._Measureunkid

                txtActivity.Text = objActivity._Name
                txtMeasure.Text = objMeasure._Name


                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA
                mblnIsOTActivity = objActivity._Is_Ot_Activity
                'Pinkal (23-Sep-2014) -- End


                Dim dLs As New DataSet
                dLs = objActTran.Get_Activity_Rate(objActivity._Activityunkid, dtpDate.Value.Date)
                If dLs.Tables(0).Rows.Count <= 0 Then
Validation:
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, No rate defined for the selected activity for the given date.") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 9, "Please define rate for this activity or select some other activity to continue."), enMsgBoxStyle.Information)
                    cboActivity.SelectedValue = 0
                    cboActivity.Focus() 'S.SANDEEP [ 20 JULY 2013 ] -- START -- END
                    Exit Sub

                ElseIf dLs.Tables(0).Rows.Count > 0 Then
                    Dim dRow() As DataRow = Nothing
                    dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.RATE)
                    If dRow.Length <= 0 Then
                        GoTo Validation
                    End If
                End If


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                'mDecNF_OT = objActivity._Normal_Otfactor
                'mDecPH_OT = objActivity._Ph_Otfactor

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    mDblSftHrs = objActTran.Get_Sft_WorkHrs(mDicWeekName, mintShiftUnkid, dtpDate.Value.Date, mblnIsWeekEnd)
                    Dim objEHoliday As New clsemployee_holiday
                    If CType(objEHoliday.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue), dtpDate.Value.Date), DataTable).Rows.Count > 0 Then
                        mblnIsHoliday = True
                    Else
                        mblnIsHoliday = False
                    End If
                    objEHoliday = Nothing
                End If


                'Pinkal (4-Sep-2014) -- End

                mblnIsFormualBased = objMeasure._Isformula_Based
                If dLs.Tables(0).Rows.Count > 0 Then

                    'Pinkal (4-Sep-2014) -- Start
                    'Enhancement - PAY_A CHANGES IN PPA


                    'Pinkal (23-Sep-2014) -- Start
                    'Enhancement -  PAY_A CHANGES IN PPA

                    Dim dRow() As DataRow = Nothing

                    dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.RATE)
                    If dRow.Length > 0 Then
                        mDecAcitityRate = CDec(dRow(0)("activity_rate"))
                        mIntTrnHeadId = CInt(dRow(0)("tranheadunkid"))
                        mTrnHeadName = dRow(0)("t_name").ToString
                    End If
                    If mblnIsWeekEnd = False AndAlso mblnIsHoliday = False AndAlso mblnIsOTActivity Then
                        dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.OT)
                        If dRow.Length > 0 Then
                            mDecNF_OT = CDec(dRow(0)("activity_rate"))
                            mIntOT_TrnHeadId = CInt(dRow(0)("tranheadunkid"))
                        End If
                    ElseIf (mblnIsWeekEnd OrElse mblnIsHoliday) AndAlso mblnIsOTActivity Then
                        dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.PH)
                        If dRow.Length > 0 Then
                            mDecPH_OT = CDec(dRow(0)("activity_rate"))
                            mIntPH_TrnHeadId = CInt(dRow(0)("tranheadunkid"))
                        End If
                    End If

                    'Pinkal (23-Sep-2014) -- End

                    'Pinkal (4-Sep-2014) -- End

                End If

                If mblnIsFormualBased = False Then
                    txtRate.Text = CStr(Format(CDec(mDecAcitityRate), GUI.fmtCurrency))
                Else
                    txtRate.Text = mTrnHeadName
                End If
                objActivity = Nothing : objMeasure = Nothing
                txtValue.Focus()
            Else
                txtMeasure.Text = "" : txtActivity.Text = "" : txtValue.Decimal = 0
                mDecNF_OT = 0 : mDecPH_OT = 0 : mblnIsFormualBased = False
                mDecAcitityRate = 0 : mIntTrnHeadId = 0 : mTrnHeadName = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboActivity_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkCostCenter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCostCenter.CheckedChanged
        Try
            cboCostcenter.Enabled = chkCostCenter.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCostCenter_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                mintEmpCCId = objEmp._Costcenterunkid


                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes
                'mintShiftUnkid = objEmp._Shiftunkid
                Dim objEmpShift As New clsEmployee_Shift_Tran
                mintShiftUnkid = objEmpShift.GetEmployee_Current_ShiftId(dtpDate.Value.Date, CInt(cboEmployee.SelectedValue))
                'Pinkal (15-Oct-2013) -- End

                mDblSftHrs = objActTran.Get_Sft_WorkHrs(mDicWeekName, mintShiftUnkid, dtpDate.Value.Date, mblnIsWeekEnd)
                objEmp = Nothing
                Call Fill_Detail_Info()
            Else
                mintEmpCCId = 0
                lvDetails.Items.Clear()
                lvEmpTotal.Items.Clear()
                mDblSftHrs = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtValue_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles txtValue.PreviewKeyDown
        Try
            If e.KeyData = Keys.Tab Then
                e.IsInputKey = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtValue_PreviewKeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtValue_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtValue.KeyDown
        Try
            If e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Enter Then
                Call Insert_Update(False)
                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA
                'Pinkal (28-Oct-2014) -- Start
                'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT

                If gbOperation.Visible = False Then
                    btnChangeDate.Select()
                Else
                    rdUpdateExisting.Focus()
                    rdUpdateExisting.Checked = True
                End If
                'Pinkal (28-Oct-2014) -- End
                'Pinkal (23-Sep-2014) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtValue_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.TextChanged
        Try
            Dim mdecFinalAmt As Decimal = 0 : dblOT_Hrs = 0 : mDecNormalAmt = 0 : mDecNormalOTAmt = 0


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA


            'Pinkal (23-Sep-2014) -- Start
            'Enhancement -  PAY_A CHANGES IN PPA

            If mblnIsFormualBased = False Then

                If mDecAcitityRate > 0 Then
                    If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                        If mblnIsOTActivity Then
                            dblOT_Hrs = txtValue.Decimal
                            mdecFinalAmt = ((mDecAcitityRate * mDecPH_OT) * txtValue.Decimal)
                            mDecNormalOTAmt = mdecFinalAmt
                        Else
                            mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                            mDecNormalAmt = 0
                        End If
                    Else

                        If mDblSftHrs > 0 Then

                            If mblnIsOTActivity Then
                                dblOT_Hrs = txtValue.Decimal - mDblSftHrs
                                If dblOT_Hrs <= 0 Then
                                    mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                                    mDecNormalAmt = mdecFinalAmt
                                Else
                                    mDecNormalAmt = (CDec(mDblSftHrs * mDecAcitityRate))
                                    mDecNormalOTAmt = CDec(dblOT_Hrs * (mDecAcitityRate * mDecNF_OT))
                                    mdecFinalAmt = mDecNormalAmt + mDecNormalOTAmt
                                End If
                            Else
                                mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                                mDecNormalAmt = 0
                            End If
                        Else
                            mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                        End If

                    End If

                End If

            Else
                If mblnIsOTActivity Then
                    If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                        dblOT_Hrs = txtValue.Decimal
                    Else
                        If mDblSftHrs > 0 Then
                            dblOT_Hrs = txtValue.Decimal - mDblSftHrs
                            If dblOT_Hrs <= 0 Then
                                dblOT_Hrs = 0
                                mIntOT_TrnHeadId = 0
                                mIntPH_TrnHeadId = 0
                            End If

                        End If

                    End If
                Else
                    mIntPH_TrnHeadId = 0
                    mIntOT_TrnHeadId = 0
                End If

            End If

            'Pinkal (23-Sep-2014) -- End

            'If mDecNF_OT > 0 AndAlso mDecPH_OT > 0 Then
            '    If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
            '        dblOT_Hrs = txtValue.Decimal
            '        mdecFinalAmt = ((mDecAcitityRate * mDecPH_OT) * txtValue.Decimal)
            '        mDecNormalOTAmt = mdecFinalAmt
            '    Else
            '        If mDblSftHrs > 0 Then
            '            dblOT_Hrs = txtValue.Decimal - mDblSftHrs
            '            If dblOT_Hrs <= 0 Then
            '                mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
            '                mDecNormalAmt = mdecFinalAmt
            '            Else
            '                mDecNormalAmt = (CDec(mDblSftHrs * mDecAcitityRate))
            '                mDecNormalOTAmt = CDec(dblOT_Hrs * (mDecAcitityRate * mDecNF_OT))
            '                mdecFinalAmt = mDecNormalAmt + mDecNormalOTAmt
            '            End If
            '        Else
            '            mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
            '        End If
            '    End If
            'Else
            '    mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
            'End If

            'Pinkal (4-Sep-2014) -- End

            txtFinalAmt.Text = Format(CDec(mdecFinalAmt), GUI.fmtCurrency)
            txtFinalAmt.Tag = mdecFinalAmt
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtValue_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboPeriod.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Enter, Keys.Tab
                    dtpDate.Focus()
                    Exit Select
            End Select
            If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboPeriod.ValueMember
                    .DisplayMember = cboPeriod.DisplayMember
                    .DataSource = CType(cboPeriod.DataSource, DataTable)
                    .CodeMember = ""
                End With
                If frm.DisplayDialog Then
                    cboPeriod.SelectedValue = frm.SelectedValue
                    dtpDate.Focus()
                Else
                    cboPeriod.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub dtpDate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtpDate.KeyDown
        Try
            'Pinkal (28-Oct-2014) -- Start
            'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT

            'Select Case e.KeyCode
            '    Case Keys.Enter, Keys.Tab
            '        chkCostCenter.Focus()
            '        Exit Select
            'End Select

            Select Case e.KeyCode
                Case Keys.Enter, Keys.Tab
                    cboEmployee.Focus()
                    Exit Select
            End Select

            'Pinkal (28-Oct-2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDate_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboCostcenter.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Enter, Keys.Tab
                    cboEmployee.Focus()
                    Exit Select
            End Select

            If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboCostcenter.ValueMember
                    .DisplayMember = cboCostcenter.DisplayMember
                    .DataSource = CType(cboCostcenter.DataSource, DataTable)
                    .CodeMember = ""
                End With

                If frm.DisplayDialog Then
                    cboCostcenter.SelectedValue = frm.SelectedValue
                    cboEmployee.Focus()
                Else
                    cboCostcenter.Text = ""
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCostcenter_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboEmployee.KeyDown
    '    Try
    '        Select Case e.KeyCode
    '            Case Keys.Enter, Keys.Tab
    '                cboActivity.Focus()
    '                Exit Select
    '        End Select
    '        If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then

    '            'SHANI (08 JUN 2015) -- Start
    '            If CInt(cboPeriod.SelectedValue) <= 0 Then Exit Sub
    '            'SHANI (06 JUN 2015) -- End 

    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboEmployee.ValueMember
    '                .DisplayMember = cboEmployee.DisplayMember
    '                .DataSource = CType(cboEmployee.DataSource, DataTable)
    '                .CodeMember = "employeecode"
    '                .SelectedText = cboEmployee.Text
    '            End With
    '            frm.TypedText = Chr(e.KeyCode)
    '            If frm.DisplayDialog Then
    '                cboEmployee.SelectedValue = frm.SelectedValue
    '                If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
    '                cboActivity.Focus()
    '            Else
    '                cboEmployee.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_KeyDown", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress

        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                    cboActivity.Focus()
                Else
                    cboEmployee.Text = ""
                End If
            ElseIf CInt(AscW(e.KeyChar)) = 13 Then
                cboActivity.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub cboActivity_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboActivity.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Enter, Keys.Tab
                    txtValue.Focus()
                    Exit Select
            End Select
            If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboActivity.ValueMember
                    .DisplayMember = cboActivity.DisplayMember
                    .DataSource = CType(cboActivity.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                If frm.DisplayDialog Then
                    cboActivity.SelectedValue = frm.SelectedValue
                    If CInt(cboActivity.SelectedValue) <= 0 Then cboActivity.Text = ""
                    'S.SANDEEP [ 20 JULY 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'txtValue.Focus()
                    If cboActivity.Text = "" Then
                        cboActivity.Focus()
                    Else
                        txtValue.Focus()
                    End If
                    'S.SANDEEP [ 20 JULY 2013 ] -- END
                Else
                    cboActivity.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radEByUoM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radEByUoM.CheckedChanged, radEByActivity.CheckedChanged
        Try
            If lvEmpTotal.Items.Count > 0 Then
                Call Fill_Emp_Total()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEByUoM_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 10 DEC 2014 ] -- START
    Private Sub radEByDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radEByDate.CheckedChanged, radEPeriod.CheckedChanged
        Try
            If lvEmpTotal.Items.Count > 0 Then
                Call Fill_Emp_Total()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEByDate_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 10 DEC 2014 ] -- END


    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState))
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub radByDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radByDate.CheckedChanged, radPeriod.CheckedChanged
        Try
            If CType(sender, RadioButton).Checked = True Then
                Select Case CType(sender, RadioButton).Name.ToString.ToUpper
                    Case radByDate.Name.ToUpper
                        Call Fill_Comp_Total()
                    Case radPeriod.Name.ToUpper
                        Call Fill_Comp_Total()
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radByDate_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radCByUoM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radCByUoM.CheckedChanged, radCByActivity.CheckedChanged
        Try
            If CType(sender, RadioButton).Checked = True Then
                Select Case CType(sender, RadioButton).Name.ToString.ToUpper
                    Case radCByUoM.Name.ToUpper
                        Call Fill_Comp_Total()
                    Case radCByActivity.Name.ToUpper
                        Call Fill_Comp_Total()
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radCByUoM_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuGetFileFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension

                Dim dTable As New DataTable

                dTable.Columns.Add("ECode") : dTable.Columns.Add("EName") : dTable.Columns.Add("Period")
                dTable.Columns.Add("Activity") : dTable.Columns.Add("ActivityDate")
                dTable.Columns.Add("ActivityValue") : dTable.Columns.Add("CostCenter")

                dsList.Tables.Add(dTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(strFilePath, dsList)
                strFilePath = dlgSaveFile.FileName
                OpenXML_Export(strFilePath, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList = Nothing
        End Try
    End Sub

    Private Sub mnuImportPPA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportPPA.Click
        Dim frm As New frmImport_Pay_Activity
        Try
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportPPA_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuExportPPA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportPPA.Click
        Dim mdtTran As DataTable
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'mdtTran = objActTran.Export_PayPerActivity(eZeeDate.convertDate(objPrd._Start_Date), eZeeDate.convertDate(objPrd._End_Date), mAllocationIds)
                mdtTran = objActTran.Export_PayPerActivity(eZeeDate.convertDate(objPrd._Start_Date), eZeeDate.convertDate(objPrd._End_Date), mAllocationIds _
                                                                                , Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                , ConfigParameter._Object._UserAccessModeSetting, True, "", "")
                'Pinkal (24-Aug-2015) -- End


                objPrd = Nothing
                Dim objExPPA As New clsExportPPA
                objExPPA.Export_PPA_Activity(mdtTran, Language.getMessage(mstrModuleName, 11, "Pay Per Activity ") & cboPeriod.Text.ToString)
                objExPPA = Nothing
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period in order to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportPPA_Click", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : WTCL Changes

    Private Sub lvDetails_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDetails.ItemChecked
        Try

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If lvDetails.CheckedItems.Count <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvDetails.CheckedItems.Count < lvDetails.Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvDetails.CheckedItems.Count = lvDetails.Items.Count Then
                chkSelectAll.CheckState = CheckState.Checked

            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If e.Item.ForeColor = Color.Gray Then e.Item.Checked = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDetails_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-Mar-2014) -- End

    'Pinkal (23-Sep-2014) -- Start
    'Enhancement -  PAY_A CHANGES IN PPA

    Private Sub mnuGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
        Try
            Dim frm As New frmGlobalAssignAcitivty
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (23-Sep-2014) -- End


    'Pinkal (10-Oct-2014) -- Start
    'Enhancement -  PAY_A CHANGES IN PPA


    Private Sub mnuExportWeeklyPPATemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportWeeklyPPATemplate.Click
        Try
            Dim frm As New frmExportWeeklyPPATemplate
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (10-Oct-2014) -- End


#End Region


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : WTCL Changes

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            RemoveHandler lvDetails.ItemChecked, AddressOf lvDetails_ItemChecked
            For Each lvItem As ListViewItem In lvDetails.Items
                If lvItem.ForeColor <> Color.Gray Then
                    lvItem.Checked = chkSelectAll.Checked
                End If
            Next
            AddHandler lvDetails.ItemChecked, AddressOf lvDetails_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (06-Mar-2014) -- End


    'Pinkal (28-Oct-2014) -- Start
    'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT

#Region "Radio Button Event"

    Private Sub rdUpdateExisting_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rdUpdateExisting.KeyDown, rdAddNew.KeyDown, rdNoAction.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Enter, Keys.Tab
                    btnOk.Select()
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdUpdateExisting_KeyDown", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (28-Oct-2014) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbActivityDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbActivityDetails.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOperation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOperation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeDate.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeDate.GradientForeColor = GUI._ButttonFontColor

            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.chkCostCenter.Text = Language._Object.getCaption(Me.chkCostCenter.Name, Me.chkCostCenter.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblActivityCode.Text = Language._Object.getCaption(Me.lblActivityCode.Name, Me.lblActivityCode.Text)
            Me.lblValue.Text = Language._Object.getCaption(Me.lblValue.Name, Me.lblValue.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.lblMeasure.Text = Language._Object.getCaption(Me.lblMeasure.Name, Me.lblMeasure.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.lblActivity.Text = Language._Object.getCaption(Me.lblActivity.Name, Me.lblActivity.Text)
            Me.radEByActivity.Text = Language._Object.getCaption(Me.radEByActivity.Name, Me.radEByActivity.Text)
            Me.radEByUoM.Text = Language._Object.getCaption(Me.radEByUoM.Name, Me.radEByUoM.Text)
            Me.colhEDisplayFor.Text = Language._Object.getCaption(CStr(Me.colhEDisplayFor.Tag), Me.colhEDisplayFor.Text)
            Me.colhETotal.Text = Language._Object.getCaption(CStr(Me.colhETotal.Tag), Me.colhETotal.Text)
            Me.radByDate.Text = Language._Object.getCaption(Me.radByDate.Name, Me.radByDate.Text)
            Me.radPeriod.Text = Language._Object.getCaption(Me.radPeriod.Name, Me.radPeriod.Text)
            Me.radCByActivity.Text = Language._Object.getCaption(Me.radCByActivity.Name, Me.radCByActivity.Text)
            Me.radCByUoM.Text = Language._Object.getCaption(Me.radCByUoM.Name, Me.radCByUoM.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhActivity.Text = Language._Object.getCaption(CStr(Me.colhActivity.Tag), Me.colhActivity.Text)
            Me.colhValue.Text = Language._Object.getCaption(CStr(Me.colhValue.Tag), Me.colhValue.Text)
            Me.colhUoM.Text = Language._Object.getCaption(CStr(Me.colhUoM.Tag), Me.colhUoM.Text)
            Me.colhCostCenter.Text = Language._Object.getCaption(CStr(Me.colhCostCenter.Tag), Me.colhCostCenter.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.colhAllocations.Text = Language._Object.getCaption(CStr(Me.colhAllocations.Tag), Me.colhAllocations.Text)
            Me.colhCDisplayFor.Text = Language._Object.getCaption(CStr(Me.colhCDisplayFor.Tag), Me.colhCDisplayFor.Text)
            Me.colhCTotal.Text = Language._Object.getCaption(CStr(Me.colhCTotal.Tag), Me.colhCTotal.Text)
            Me.btnChangeDate.Text = Language._Object.getCaption(Me.btnChangeDate.Name, Me.btnChangeDate.Text)
            Me.gbActivityDetails.Text = Language._Object.getCaption(Me.gbActivityDetails.Name, Me.gbActivityDetails.Text)
            Me.gbOperation.Text = Language._Object.getCaption(Me.gbOperation.Name, Me.gbOperation.Text)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.rdUpdateExisting.Text = Language._Object.getCaption(Me.rdUpdateExisting.Name, Me.rdUpdateExisting.Text)
            Me.rdAddNew.Text = Language._Object.getCaption(Me.rdAddNew.Name, Me.rdAddNew.Text)
            Me.rdNoAction.Text = Language._Object.getCaption(Me.rdNoAction.Name, Me.rdNoAction.Text)
            Me.lblActivityRate.Text = Language._Object.getCaption(Me.lblActivityRate.Name, Me.lblActivityRate.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.colhRate.Text = Language._Object.getCaption(CStr(Me.colhRate.Tag), Me.colhRate.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
            Me.mnuImportPPA.Text = Language._Object.getCaption(Me.mnuImportPPA.Name, Me.mnuImportPPA.Text)
            Me.mnuExportPPA.Text = Language._Object.getCaption(Me.mnuExportPPA.Name, Me.mnuExportPPA.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)
            Me.mnuExportWeeklyPPATemplate.Text = Language._Object.getCaption(Me.mnuExportWeeklyPPATemplate.Name, Me.mnuExportWeeklyPPATemplate.Text)
            Me.tabpEmployeeTotals.Text = Language._Object.getCaption(Me.tabpEmployeeTotals.Name, Me.tabpEmployeeTotals.Text)
            Me.tabpCompanyTotals.Text = Language._Object.getCaption(Me.tabpCompanyTotals.Name, Me.tabpCompanyTotals.Text)
            Me.radEPeriod.Text = Language._Object.getCaption(Me.radEPeriod.Name, Me.radEPeriod.Text)
            Me.radEByDate.Text = Language._Object.getCaption(Me.radEByDate.Name, Me.radEByDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period in order to continue.")
            Language.setMessage(mstrModuleName, 2, "Cost Center is mandatory information. Please select Cost Center in order to continue.")
            Language.setMessage(mstrModuleName, 3, "Employee is mandatory information. Please select Employee in order to continue.")
            Language.setMessage(mstrModuleName, 4, "Activity is mandatory information. Please select Activity in order to continue.")
            Language.setMessage(mstrModuleName, 5, "You have not entered the activity value. Activity value is mandatory information.")
            Language.setMessage(mstrModuleName, 6, "Sorry, No rate defined for the selected activity for the given date.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Problem in deleting data.")
            Language.setMessage(mstrModuleName, 9, "Please define rate for this activity or select some other activity to continue.")
            Language.setMessage(mstrModuleName, 10, "Template Exported Successfully.")
            Language.setMessage(mstrModuleName, 11, "Pay Per Activity")
            Language.setMessage(mstrModuleName, 12, "Please Check employee activities to do further operation on it.")
            Language.setMessage(mstrModuleName, 13, "Are you sure you want to delete checked activites ?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class