﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalAssignAcitivty
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalAssignAcitivty))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbAssignPayActivity = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.chkCostCenter = New System.Windows.Forms.CheckBox
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.pnl4 = New System.Windows.Forms.Panel
        Me.rdUpdateExisting = New System.Windows.Forms.RadioButton
        Me.rdAddNew = New System.Windows.Forms.RadioButton
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchActivity = New System.Windows.Forms.TextBox
        Me.pnlActivity = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvActivityList = New System.Windows.Forms.DataGridView
        Me.objdgcolhMCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhMeasureCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMeasureName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMeasureId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tblpEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtAssessorEmp = New eZee.TextBox.AlphanumericTextBox
        Me.pnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.eLine1 = New eZee.Common.eZeeLine
        Me.lblMeasures = New System.Windows.Forms.Label
        Me.objbtnSearchMeasures = New eZee.Common.eZeeGradientButton
        Me.radNF = New System.Windows.Forms.RadioButton
        Me.cboMeasures = New System.Windows.Forms.ComboBox
        Me.radFB = New System.Windows.Forms.RadioButton
        Me.lblValue = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.txtValue = New eZee.TextBox.NumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.gbAssignPayActivity.SuspendLayout()
        Me.pnl4.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnlActivity.SuspendLayout()
        CType(Me.dgvActivityList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpEmployee.SuspendLayout()
        Me.pnlEmp.SuspendLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.pnlMain)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(731, 473)
        Me.pnlMainInfo.TabIndex = 0
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbAssignPayActivity)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(731, 418)
        Me.pnlMain.TabIndex = 162
        '
        'gbAssignPayActivity
        '
        Me.gbAssignPayActivity.BorderColor = System.Drawing.Color.Black
        Me.gbAssignPayActivity.Checked = False
        Me.gbAssignPayActivity.CollapseAllExceptThis = False
        Me.gbAssignPayActivity.CollapsedHoverImage = Nothing
        Me.gbAssignPayActivity.CollapsedNormalImage = Nothing
        Me.gbAssignPayActivity.CollapsedPressedImage = Nothing
        Me.gbAssignPayActivity.CollapseOnLoad = False
        Me.gbAssignPayActivity.Controls.Add(Me.cboPeriod)
        Me.gbAssignPayActivity.Controls.Add(Me.lblPeriod)
        Me.gbAssignPayActivity.Controls.Add(Me.chkCostCenter)
        Me.gbAssignPayActivity.Controls.Add(Me.objAlloacationReset)
        Me.gbAssignPayActivity.Controls.Add(Me.cboCostcenter)
        Me.gbAssignPayActivity.Controls.Add(Me.lnkAllocation)
        Me.gbAssignPayActivity.Controls.Add(Me.pnl4)
        Me.gbAssignPayActivity.Controls.Add(Me.lblFromDate)
        Me.gbAssignPayActivity.Controls.Add(Me.lblTo)
        Me.gbAssignPayActivity.Controls.Add(Me.dtpDate2)
        Me.gbAssignPayActivity.Controls.Add(Me.dtpDate1)
        Me.gbAssignPayActivity.Controls.Add(Me.TableLayoutPanel1)
        Me.gbAssignPayActivity.Controls.Add(Me.tblpEmployee)
        Me.gbAssignPayActivity.Controls.Add(Me.eLine1)
        Me.gbAssignPayActivity.Controls.Add(Me.lblMeasures)
        Me.gbAssignPayActivity.Controls.Add(Me.objbtnSearchMeasures)
        Me.gbAssignPayActivity.Controls.Add(Me.radNF)
        Me.gbAssignPayActivity.Controls.Add(Me.cboMeasures)
        Me.gbAssignPayActivity.Controls.Add(Me.radFB)
        Me.gbAssignPayActivity.Controls.Add(Me.lblValue)
        Me.gbAssignPayActivity.Controls.Add(Me.objLine1)
        Me.gbAssignPayActivity.Controls.Add(Me.txtValue)
        Me.gbAssignPayActivity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssignPayActivity.ExpandedHoverImage = Nothing
        Me.gbAssignPayActivity.ExpandedNormalImage = Nothing
        Me.gbAssignPayActivity.ExpandedPressedImage = Nothing
        Me.gbAssignPayActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignPayActivity.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssignPayActivity.HeaderHeight = 25
        Me.gbAssignPayActivity.HeaderMessage = ""
        Me.gbAssignPayActivity.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignPayActivity.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssignPayActivity.HeightOnCollapse = 0
        Me.gbAssignPayActivity.LeftTextSpace = 0
        Me.gbAssignPayActivity.Location = New System.Drawing.Point(0, 0)
        Me.gbAssignPayActivity.Name = "gbAssignPayActivity"
        Me.gbAssignPayActivity.OpenHeight = 300
        Me.gbAssignPayActivity.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssignPayActivity.ShowBorder = True
        Me.gbAssignPayActivity.ShowCheckBox = False
        Me.gbAssignPayActivity.ShowCollapseButton = False
        Me.gbAssignPayActivity.ShowDefaultBorderColor = True
        Me.gbAssignPayActivity.ShowDownButton = False
        Me.gbAssignPayActivity.ShowHeader = True
        Me.gbAssignPayActivity.Size = New System.Drawing.Size(731, 418)
        Me.gbAssignPayActivity.TabIndex = 1
        Me.gbAssignPayActivity.Temp = 0
        Me.gbAssignPayActivity.Text = "Assign Activity"
        Me.gbAssignPayActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(76, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(219, 21)
        Me.cboPeriod.TabIndex = 310
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(62, 15)
        Me.lblPeriod.TabIndex = 309
        Me.lblPeriod.Text = "Period"
        '
        'chkCostCenter
        '
        Me.chkCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCostCenter.Location = New System.Drawing.Point(304, 64)
        Me.chkCostCenter.Name = "chkCostCenter"
        Me.chkCostCenter.Size = New System.Drawing.Size(84, 15)
        Me.chkCostCenter.TabIndex = 6
        Me.chkCostCenter.Text = "Cost Center"
        Me.chkCostCenter.UseVisualStyleBackColor = True
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(694, 2)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(25, 21)
        Me.objAlloacationReset.TabIndex = 306
        '
        'cboCostcenter
        '
        Me.cboCostcenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostcenter.DropDownWidth = 350
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(394, 61)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(175, 21)
        Me.cboCostcenter.TabIndex = 7
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.Location = New System.Drawing.Point(572, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(116, 17)
        Me.lnkAllocation.TabIndex = 307
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnl4
        '
        Me.pnl4.Controls.Add(Me.rdUpdateExisting)
        Me.pnl4.Controls.Add(Me.rdAddNew)
        Me.pnl4.Location = New System.Drawing.Point(575, 34)
        Me.pnl4.Name = "pnl4"
        Me.pnl4.Size = New System.Drawing.Size(149, 48)
        Me.pnl4.TabIndex = 183
        '
        'rdUpdateExisting
        '
        Me.rdUpdateExisting.Checked = True
        Me.rdUpdateExisting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdUpdateExisting.Location = New System.Drawing.Point(10, 4)
        Me.rdUpdateExisting.Name = "rdUpdateExisting"
        Me.rdUpdateExisting.Size = New System.Drawing.Size(131, 17)
        Me.rdUpdateExisting.TabIndex = 175
        Me.rdUpdateExisting.TabStop = True
        Me.rdUpdateExisting.Text = "Update Existing"
        Me.rdUpdateExisting.UseVisualStyleBackColor = True
        '
        'rdAddNew
        '
        Me.rdAddNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAddNew.Location = New System.Drawing.Point(10, 27)
        Me.rdAddNew.Name = "rdAddNew"
        Me.rdAddNew.Size = New System.Drawing.Size(131, 17)
        Me.rdAddNew.TabIndex = 174
        Me.rdAddNew.Text = "Add New"
        Me.rdAddNew.UseVisualStyleBackColor = True
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 62)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(62, 16)
        Me.lblFromDate.TabIndex = 181
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(171, 62)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(29, 16)
        Me.lblTo.TabIndex = 180
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(206, 60)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(89, 21)
        Me.dtpDate2.TabIndex = 178
        '
        'dtpDate1
        '
        Me.dtpDate1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(76, 60)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(89, 21)
        Me.dtpDate1.TabIndex = 179
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtSearchActivity, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pnlActivity, 0, 1)
        Me.TableLayoutPanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(11, 170)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(287, 245)
        Me.TableLayoutPanel1.TabIndex = 177
        '
        'txtSearchActivity
        '
        Me.txtSearchActivity.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchActivity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchActivity.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchActivity.Name = "txtSearchActivity"
        Me.txtSearchActivity.Size = New System.Drawing.Size(281, 21)
        Me.txtSearchActivity.TabIndex = 172
        '
        'pnlActivity
        '
        Me.pnlActivity.Controls.Add(Me.objchkSelectAll)
        Me.pnlActivity.Controls.Add(Me.dgvActivityList)
        Me.pnlActivity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlActivity.Location = New System.Drawing.Point(3, 29)
        Me.pnlActivity.Name = "pnlActivity"
        Me.pnlActivity.Size = New System.Drawing.Size(281, 213)
        Me.pnlActivity.TabIndex = 173
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(6, 5)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 171
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvActivityList
        '
        Me.dgvActivityList.AllowUserToAddRows = False
        Me.dgvActivityList.AllowUserToDeleteRows = False
        Me.dgvActivityList.AllowUserToResizeColumns = False
        Me.dgvActivityList.AllowUserToResizeRows = False
        Me.dgvActivityList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvActivityList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvActivityList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvActivityList.ColumnHeadersHeight = 21
        Me.dgvActivityList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvActivityList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhMCheck, Me.dgcolhMeasureCode, Me.dgcolhMeasureName, Me.objdgcolhMeasureId})
        Me.dgvActivityList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvActivityList.Location = New System.Drawing.Point(0, 0)
        Me.dgvActivityList.MultiSelect = False
        Me.dgvActivityList.Name = "dgvActivityList"
        Me.dgvActivityList.RowHeadersVisible = False
        Me.dgvActivityList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvActivityList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvActivityList.Size = New System.Drawing.Size(281, 213)
        Me.dgvActivityList.TabIndex = 172
        '
        'objdgcolhMCheck
        '
        Me.objdgcolhMCheck.DataPropertyName = "ischeck"
        Me.objdgcolhMCheck.HeaderText = ""
        Me.objdgcolhMCheck.Name = "objdgcolhMCheck"
        Me.objdgcolhMCheck.Width = 25
        '
        'dgcolhMeasureCode
        '
        Me.dgcolhMeasureCode.DataPropertyName = "code"
        Me.dgcolhMeasureCode.HeaderText = "Code"
        Me.dgcolhMeasureCode.Name = "dgcolhMeasureCode"
        Me.dgcolhMeasureCode.ReadOnly = True
        Me.dgcolhMeasureCode.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhMeasureCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMeasureCode.Width = 70
        '
        'dgcolhMeasureName
        '
        Me.dgcolhMeasureName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhMeasureName.DataPropertyName = "name"
        Me.dgcolhMeasureName.HeaderText = "Name"
        Me.dgcolhMeasureName.Name = "dgcolhMeasureName"
        Me.dgcolhMeasureName.ReadOnly = True
        Me.dgcolhMeasureName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhMeasureName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhMeasureId
        '
        Me.objdgcolhMeasureId.DataPropertyName = "activityunkid"
        Me.objdgcolhMeasureId.HeaderText = "objdgcolhMeasureId"
        Me.objdgcolhMeasureId.Name = "objdgcolhMeasureId"
        Me.objdgcolhMeasureId.ReadOnly = True
        Me.objdgcolhMeasureId.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhMeasureId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhMeasureId.Visible = False
        '
        'tblpEmployee
        '
        Me.tblpEmployee.ColumnCount = 1
        Me.tblpEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpEmployee.Controls.Add(Me.txtAssessorEmp, 0, 0)
        Me.tblpEmployee.Controls.Add(Me.pnlEmp, 0, 1)
        Me.tblpEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpEmployee.Location = New System.Drawing.Point(304, 88)
        Me.tblpEmployee.Name = "tblpEmployee"
        Me.tblpEmployee.RowCount = 2
        Me.tblpEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpEmployee.Size = New System.Drawing.Size(415, 327)
        Me.tblpEmployee.TabIndex = 108
        '
        'txtAssessorEmp
        '
        Me.txtAssessorEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAssessorEmp.Flags = 0
        Me.txtAssessorEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtAssessorEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtAssessorEmp.Name = "txtAssessorEmp"
        Me.txtAssessorEmp.Size = New System.Drawing.Size(409, 21)
        Me.txtAssessorEmp.TabIndex = 106
        '
        'pnlEmp
        '
        Me.pnlEmp.Controls.Add(Me.objchkEmployee)
        Me.pnlEmp.Controls.Add(Me.dgvEmployee)
        Me.pnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.pnlEmp.Name = "pnlEmp"
        Me.pnlEmp.Size = New System.Drawing.Size(409, 295)
        Me.pnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvEmployee
        '
        Me.dgvEmployee.AllowUserToAddRows = False
        Me.dgvEmployee.AllowUserToDeleteRows = False
        Me.dgvEmployee.AllowUserToResizeColumns = False
        Me.dgvEmployee.AllowUserToResizeRows = False
        Me.dgvEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvEmployee.ColumnHeadersHeight = 21
        Me.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmployee.MultiSelect = False
        Me.dgvEmployee.Name = "dgvEmployee"
        Me.dgvEmployee.RowHeadersVisible = False
        Me.dgvEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmployee.Size = New System.Drawing.Size(409, 295)
        Me.dgvEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'eLine1
        '
        Me.eLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eLine1.Location = New System.Drawing.Point(8, 84)
        Me.eLine1.Name = "eLine1"
        Me.eLine1.Size = New System.Drawing.Size(287, 16)
        Me.eLine1.TabIndex = 164
        Me.eLine1.Text = "UoM Type"
        Me.eLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMeasures
        '
        Me.lblMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeasures.Location = New System.Drawing.Point(11, 146)
        Me.lblMeasures.Name = "lblMeasures"
        Me.lblMeasures.Size = New System.Drawing.Size(59, 15)
        Me.lblMeasures.TabIndex = 169
        Me.lblMeasures.Text = "Measures"
        Me.lblMeasures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchMeasures
        '
        Me.objbtnSearchMeasures.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMeasures.BorderSelected = False
        Me.objbtnSearchMeasures.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchMeasures.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMeasures.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMeasures.Location = New System.Drawing.Point(274, 143)
        Me.objbtnSearchMeasures.Name = "objbtnSearchMeasures"
        Me.objbtnSearchMeasures.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMeasures.TabIndex = 173
        '
        'radNF
        '
        Me.radNF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNF.Location = New System.Drawing.Point(20, 106)
        Me.radNF.Name = "radNF"
        Me.radNF.Size = New System.Drawing.Size(108, 17)
        Me.radNF.TabIndex = 166
        Me.radNF.Text = "Non Formula"
        Me.radNF.UseVisualStyleBackColor = True
        '
        'cboMeasures
        '
        Me.cboMeasures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMeasures.DropDownWidth = 350
        Me.cboMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMeasures.FormattingEnabled = True
        Me.cboMeasures.Location = New System.Drawing.Point(76, 143)
        Me.cboMeasures.Name = "cboMeasures"
        Me.cboMeasures.Size = New System.Drawing.Size(192, 21)
        Me.cboMeasures.TabIndex = 170
        '
        'radFB
        '
        Me.radFB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radFB.Location = New System.Drawing.Point(134, 106)
        Me.radFB.Name = "radFB"
        Me.radFB.Size = New System.Drawing.Size(108, 17)
        Me.radFB.TabIndex = 167
        Me.radFB.Text = "Formula Based"
        Me.radFB.UseVisualStyleBackColor = True
        '
        'lblValue
        '
        Me.lblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValue.Location = New System.Drawing.Point(304, 37)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(84, 15)
        Me.lblValue.TabIndex = 162
        Me.lblValue.Text = "Value"
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(11, 126)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(284, 14)
        Me.objLine1.TabIndex = 168
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtValue
        '
        Me.txtValue.AcceptsReturn = True
        Me.txtValue.AcceptsTab = True
        Me.txtValue.AllowNegative = False
        Me.txtValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtValue.DigitsInGroup = 0
        Me.txtValue.Flags = 65536
        Me.txtValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValue.Location = New System.Drawing.Point(394, 34)
        Me.txtValue.MaxDecimalPlaces = 6
        Me.txtValue.MaxWholeDigits = 21
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Prefix = ""
        Me.txtValue.RangeMax = 1.7976931348623157E+308
        Me.txtValue.RangeMin = -1.7976931348623157E+308
        Me.txtValue.Size = New System.Drawing.Size(175, 21)
        Me.txtValue.TabIndex = 163
        Me.txtValue.Text = "0"
        Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnAssign)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 418)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(731, 55)
        Me.objFooter.TabIndex = 161
        '
        'btnAssign
        '
        Me.btnAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssign.BackColor = System.Drawing.Color.White
        Me.btnAssign.BackgroundImage = CType(resources.GetObject("btnAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssign.FlatAppearance.BorderSize = 0
        Me.btnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssign.ForeColor = System.Drawing.Color.Black
        Me.btnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Location = New System.Drawing.Point(519, 13)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Size = New System.Drawing.Size(97, 30)
        Me.btnAssign.TabIndex = 0
        Me.btnAssign.Text = "&Assign"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(622, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "code"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "name"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "activityunkid"
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'frmGlobalAssignAcitivty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 473)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalAssignAcitivty"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Assign Pay Per Activity"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.gbAssignPayActivity.ResumeLayout(False)
        Me.gbAssignPayActivity.PerformLayout()
        Me.pnl4.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.pnlActivity.ResumeLayout(False)
        Me.pnlActivity.PerformLayout()
        CType(Me.dgvActivityList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpEmployee.ResumeLayout(False)
        Me.tblpEmployee.PerformLayout()
        Me.pnlEmp.ResumeLayout(False)
        Me.pnlEmp.PerformLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbAssignPayActivity As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tblpEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtAssessorEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents eLine1 As eZee.Common.eZeeLine
    Friend WithEvents rdUpdateExisting As System.Windows.Forms.RadioButton
    Friend WithEvents lblMeasures As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMeasures As eZee.Common.eZeeGradientButton
    Friend WithEvents radNF As System.Windows.Forms.RadioButton
    Friend WithEvents rdAddNew As System.Windows.Forms.RadioButton
    Friend WithEvents cboMeasures As System.Windows.Forms.ComboBox
    Private WithEvents txtSearchActivity As System.Windows.Forms.TextBox
    Friend WithEvents radFB As System.Windows.Forms.RadioButton
    Friend WithEvents lblValue As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtValue As eZee.TextBox.NumericTextBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pnlActivity As System.Windows.Forms.Panel
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnl4 As System.Windows.Forms.Panel
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents chkCostCenter As System.Windows.Forms.CheckBox
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dgvActivityList As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhMeasureCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMeasureName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMeasureId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
