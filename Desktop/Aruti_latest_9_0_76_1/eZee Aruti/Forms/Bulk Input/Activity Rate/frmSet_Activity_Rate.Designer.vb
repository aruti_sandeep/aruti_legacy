﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSet_Activity_Rate
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSet_Activity_Rate))
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.LblMode = New System.Windows.Forms.Label
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchActivity = New System.Windows.Forms.TextBox
        Me.pnlActivity = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvActivityList = New System.Windows.Forms.DataGridView
        Me.objdgcolhMCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhMeasureCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMeasureName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMeasureId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchMeasures = New eZee.Common.eZeeGradientButton
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.radFB = New System.Windows.Forms.RadioButton
        Me.cboMeasures = New System.Windows.Forms.ComboBox
        Me.radNF = New System.Windows.Forms.RadioButton
        Me.lblMeasures = New System.Windows.Forms.Label
        Me.eLine1 = New eZee.Common.eZeeLine
        Me.gbRates = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.pnlView = New System.Windows.Forms.Panel
        Me.dgvFB = New System.Windows.Forms.DataGridView
        Me.dgvNF = New System.Windows.Forms.DataGridView
        Me.lblRate = New System.Windows.Forms.Label
        Me.txtRate = New eZee.TextBox.NumericTextBox
        Me.lblHeads = New System.Windows.Forms.Label
        Me.btnPost = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.cboTransactionHead = New System.Windows.Forms.ComboBox
        Me.objbtnSearchHeads = New eZee.Common.eZeeGradientButton
        Me.objLine2 = New eZee.Common.eZeeLine
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.elOperation = New eZee.Common.eZeeLine
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilter.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnlActivity.SuspendLayout()
        CType(Me.dgvActivityList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbRates.SuspendLayout()
        Me.pnlView.SuspendLayout()
        CType(Me.dgvFB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.cboMode)
        Me.gbFilter.Controls.Add(Me.LblMode)
        Me.gbFilter.Controls.Add(Me.TableLayoutPanel1)
        Me.gbFilter.Controls.Add(Me.objbtnSearchMeasures)
        Me.gbFilter.Controls.Add(Me.objLine1)
        Me.gbFilter.Controls.Add(Me.radFB)
        Me.gbFilter.Controls.Add(Me.cboMeasures)
        Me.gbFilter.Controls.Add(Me.radNF)
        Me.gbFilter.Controls.Add(Me.lblMeasures)
        Me.gbFilter.Controls.Add(Me.eLine1)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(1, 1)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(253, 443)
        Me.gbFilter.TabIndex = 0
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.DropDownWidth = 350
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(68, 112)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(155, 21)
        Me.cboMode.TabIndex = 181
        '
        'LblMode
        '
        Me.LblMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMode.Location = New System.Drawing.Point(5, 114)
        Me.LblMode.Name = "LblMode"
        Me.LblMode.Size = New System.Drawing.Size(57, 15)
        Me.LblMode.TabIndex = 180
        Me.LblMode.Text = "Mode"
        Me.LblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtSearchActivity, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pnlActivity, 0, 1)
        Me.TableLayoutPanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 142)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(247, 298)
        Me.TableLayoutPanel1.TabIndex = 178
        '
        'txtSearchActivity
        '
        Me.txtSearchActivity.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchActivity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchActivity.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchActivity.Name = "txtSearchActivity"
        Me.txtSearchActivity.Size = New System.Drawing.Size(241, 21)
        Me.txtSearchActivity.TabIndex = 172
        '
        'pnlActivity
        '
        Me.pnlActivity.Controls.Add(Me.objchkSelectAll)
        Me.pnlActivity.Controls.Add(Me.dgvActivityList)
        Me.pnlActivity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlActivity.Location = New System.Drawing.Point(3, 29)
        Me.pnlActivity.Name = "pnlActivity"
        Me.pnlActivity.Size = New System.Drawing.Size(241, 266)
        Me.pnlActivity.TabIndex = 173
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 6)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 171
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvActivityList
        '
        Me.dgvActivityList.AllowUserToAddRows = False
        Me.dgvActivityList.AllowUserToDeleteRows = False
        Me.dgvActivityList.AllowUserToResizeColumns = False
        Me.dgvActivityList.AllowUserToResizeRows = False
        Me.dgvActivityList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvActivityList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvActivityList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvActivityList.ColumnHeadersHeight = 21
        Me.dgvActivityList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvActivityList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhMCheck, Me.dgcolhMeasureCode, Me.dgcolhMeasureName, Me.objdgcolhMeasureId})
        Me.dgvActivityList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvActivityList.Location = New System.Drawing.Point(0, 0)
        Me.dgvActivityList.MultiSelect = False
        Me.dgvActivityList.Name = "dgvActivityList"
        Me.dgvActivityList.RowHeadersVisible = False
        Me.dgvActivityList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvActivityList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvActivityList.Size = New System.Drawing.Size(241, 266)
        Me.dgvActivityList.TabIndex = 172
        '
        'objdgcolhMCheck
        '
        Me.objdgcolhMCheck.DataPropertyName = "ischeck"
        Me.objdgcolhMCheck.HeaderText = ""
        Me.objdgcolhMCheck.Name = "objdgcolhMCheck"
        Me.objdgcolhMCheck.Width = 25
        '
        'dgcolhMeasureCode
        '
        Me.dgcolhMeasureCode.DataPropertyName = "code"
        Me.dgcolhMeasureCode.HeaderText = "Code"
        Me.dgcolhMeasureCode.Name = "dgcolhMeasureCode"
        Me.dgcolhMeasureCode.ReadOnly = True
        Me.dgcolhMeasureCode.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhMeasureCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMeasureCode.Width = 70
        '
        'dgcolhMeasureName
        '
        Me.dgcolhMeasureName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhMeasureName.DataPropertyName = "name"
        Me.dgcolhMeasureName.HeaderText = "Name"
        Me.dgcolhMeasureName.Name = "dgcolhMeasureName"
        Me.dgcolhMeasureName.ReadOnly = True
        Me.dgcolhMeasureName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhMeasureName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhMeasureId
        '
        Me.objdgcolhMeasureId.DataPropertyName = "activityunkid"
        Me.objdgcolhMeasureId.HeaderText = "objdgcolhActivityUnkid"
        Me.objdgcolhMeasureId.Name = "objdgcolhMeasureId"
        Me.objdgcolhMeasureId.ReadOnly = True
        Me.objdgcolhMeasureId.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhMeasureId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhMeasureId.Visible = False
        '
        'objbtnSearchMeasures
        '
        Me.objbtnSearchMeasures.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMeasures.BorderSelected = False
        Me.objbtnSearchMeasures.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchMeasures.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMeasures.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMeasures.Location = New System.Drawing.Point(229, 84)
        Me.objbtnSearchMeasures.Name = "objbtnSearchMeasures"
        Me.objbtnSearchMeasures.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMeasures.TabIndex = 26
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(3, 71)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(247, 10)
        Me.objLine1.TabIndex = 23
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radFB
        '
        Me.radFB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radFB.Location = New System.Drawing.Point(136, 51)
        Me.radFB.Name = "radFB"
        Me.radFB.Size = New System.Drawing.Size(108, 17)
        Me.radFB.TabIndex = 22
        Me.radFB.Text = "Formula Based"
        Me.radFB.UseVisualStyleBackColor = True
        '
        'cboMeasures
        '
        Me.cboMeasures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMeasures.DropDownWidth = 350
        Me.cboMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMeasures.FormattingEnabled = True
        Me.cboMeasures.Location = New System.Drawing.Point(68, 84)
        Me.cboMeasures.Name = "cboMeasures"
        Me.cboMeasures.Size = New System.Drawing.Size(155, 21)
        Me.cboMeasures.TabIndex = 25
        '
        'radNF
        '
        Me.radNF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNF.Location = New System.Drawing.Point(22, 51)
        Me.radNF.Name = "radNF"
        Me.radNF.Size = New System.Drawing.Size(108, 17)
        Me.radNF.TabIndex = 21
        Me.radNF.Text = "Non Formula"
        Me.radNF.UseVisualStyleBackColor = True
        '
        'lblMeasures
        '
        Me.lblMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeasures.Location = New System.Drawing.Point(5, 86)
        Me.lblMeasures.Name = "lblMeasures"
        Me.lblMeasures.Size = New System.Drawing.Size(57, 15)
        Me.lblMeasures.TabIndex = 24
        Me.lblMeasures.Text = "Measures"
        Me.lblMeasures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eLine1
        '
        Me.eLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eLine1.Location = New System.Drawing.Point(3, 31)
        Me.eLine1.Name = "eLine1"
        Me.eLine1.Size = New System.Drawing.Size(247, 17)
        Me.eLine1.TabIndex = 1
        Me.eLine1.Text = "UoM Type"
        Me.eLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbRates
        '
        Me.gbRates.BorderColor = System.Drawing.Color.Black
        Me.gbRates.Checked = False
        Me.gbRates.CollapseAllExceptThis = False
        Me.gbRates.CollapsedHoverImage = Nothing
        Me.gbRates.CollapsedNormalImage = Nothing
        Me.gbRates.CollapsedPressedImage = Nothing
        Me.gbRates.CollapseOnLoad = False
        Me.gbRates.Controls.Add(Me.cboPeriod)
        Me.gbRates.Controls.Add(Me.lblPeriod)
        Me.gbRates.Controls.Add(Me.pnlView)
        Me.gbRates.Controls.Add(Me.lblRate)
        Me.gbRates.Controls.Add(Me.txtRate)
        Me.gbRates.Controls.Add(Me.lblHeads)
        Me.gbRates.Controls.Add(Me.btnPost)
        Me.gbRates.Controls.Add(Me.lblFromDate)
        Me.gbRates.Controls.Add(Me.cboTransactionHead)
        Me.gbRates.Controls.Add(Me.objbtnSearchHeads)
        Me.gbRates.Controls.Add(Me.objLine2)
        Me.gbRates.Controls.Add(Me.lblTo)
        Me.gbRates.Controls.Add(Me.dtpDate2)
        Me.gbRates.Controls.Add(Me.dtpDate1)
        Me.gbRates.Controls.Add(Me.elOperation)
        Me.gbRates.ExpandedHoverImage = Nothing
        Me.gbRates.ExpandedNormalImage = Nothing
        Me.gbRates.ExpandedPressedImage = Nothing
        Me.gbRates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRates.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRates.HeaderHeight = 25
        Me.gbRates.HeaderMessage = ""
        Me.gbRates.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbRates.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRates.HeightOnCollapse = 0
        Me.gbRates.LeftTextSpace = 0
        Me.gbRates.Location = New System.Drawing.Point(257, 1)
        Me.gbRates.Name = "gbRates"
        Me.gbRates.OpenHeight = 300
        Me.gbRates.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRates.ShowBorder = True
        Me.gbRates.ShowCheckBox = False
        Me.gbRates.ShowCollapseButton = False
        Me.gbRates.ShowDefaultBorderColor = True
        Me.gbRates.ShowDownButton = False
        Me.gbRates.ShowHeader = True
        Me.gbRates.Size = New System.Drawing.Size(660, 443)
        Me.gbRates.TabIndex = 1
        Me.gbRates.Temp = 0
        Me.gbRates.Text = "Set Rates Information"
        Me.gbRates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(88, 48)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(219, 21)
        Me.cboPeriod.TabIndex = 5
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(20, 50)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(62, 16)
        Me.lblPeriod.TabIndex = 4
        Me.lblPeriod.Text = "Period"
        '
        'pnlView
        '
        Me.pnlView.Controls.Add(Me.dgvFB)
        Me.pnlView.Controls.Add(Me.dgvNF)
        Me.pnlView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlView.Location = New System.Drawing.Point(8, 112)
        Me.pnlView.Name = "pnlView"
        Me.pnlView.Size = New System.Drawing.Size(645, 326)
        Me.pnlView.TabIndex = 128
        '
        'dgvFB
        '
        Me.dgvFB.AllowUserToAddRows = False
        Me.dgvFB.AllowUserToDeleteRows = False
        Me.dgvFB.AllowUserToResizeColumns = False
        Me.dgvFB.AllowUserToResizeRows = False
        Me.dgvFB.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvFB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvFB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvFB.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFB.Location = New System.Drawing.Point(0, 0)
        Me.dgvFB.MultiSelect = False
        Me.dgvFB.Name = "dgvFB"
        Me.dgvFB.RowHeadersVisible = False
        Me.dgvFB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvFB.Size = New System.Drawing.Size(645, 326)
        Me.dgvFB.TabIndex = 127
        '
        'dgvNF
        '
        Me.dgvNF.AllowUserToAddRows = False
        Me.dgvNF.AllowUserToDeleteRows = False
        Me.dgvNF.AllowUserToResizeColumns = False
        Me.dgvNF.AllowUserToResizeRows = False
        Me.dgvNF.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvNF.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvNF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvNF.Location = New System.Drawing.Point(0, 0)
        Me.dgvNF.MultiSelect = False
        Me.dgvNF.Name = "dgvNF"
        Me.dgvNF.RowHeadersVisible = False
        Me.dgvNF.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvNF.Size = New System.Drawing.Size(645, 326)
        Me.dgvNF.TabIndex = 126
        '
        'lblRate
        '
        Me.lblRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRate.Location = New System.Drawing.Point(313, 50)
        Me.lblRate.Name = "lblRate"
        Me.lblRate.Size = New System.Drawing.Size(70, 16)
        Me.lblRate.TabIndex = 116
        Me.lblRate.Text = "Rate"
        Me.lblRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRate
        '
        Me.txtRate.AllowNegative = True
        Me.txtRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtRate.DigitsInGroup = 0
        Me.txtRate.Flags = 0
        Me.txtRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRate.Location = New System.Drawing.Point(389, 48)
        Me.txtRate.MaxDecimalPlaces = 4
        Me.txtRate.MaxWholeDigits = 9
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Prefix = ""
        Me.txtRate.RangeMax = 1.7976931348623157E+308
        Me.txtRate.RangeMin = -1.7976931348623157E+308
        Me.txtRate.Size = New System.Drawing.Size(174, 21)
        Me.txtRate.TabIndex = 115
        Me.txtRate.Text = "0"
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHeads
        '
        Me.lblHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeads.Location = New System.Drawing.Point(313, 77)
        Me.lblHeads.Name = "lblHeads"
        Me.lblHeads.Size = New System.Drawing.Size(70, 16)
        Me.lblHeads.TabIndex = 125
        Me.lblHeads.Text = "Tran. Heads"
        Me.lblHeads.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnPost
        '
        Me.btnPost.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPost.BackColor = System.Drawing.Color.White
        Me.btnPost.BackgroundImage = CType(resources.GetObject("btnPost.BackgroundImage"), System.Drawing.Image)
        Me.btnPost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPost.BorderColor = System.Drawing.Color.Empty
        Me.btnPost.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPost.FlatAppearance.BorderSize = 0
        Me.btnPost.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPost.ForeColor = System.Drawing.Color.Black
        Me.btnPost.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPost.GradientForeColor = System.Drawing.Color.Black
        Me.btnPost.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPost.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPost.Location = New System.Drawing.Point(596, 48)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPost.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPost.Size = New System.Drawing.Size(52, 48)
        Me.btnPost.TabIndex = 124
        Me.btnPost.Text = "&Post"
        Me.btnPost.UseVisualStyleBackColor = True
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(20, 77)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(62, 16)
        Me.lblFromDate.TabIndex = 123
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTransactionHead
        '
        Me.cboTransactionHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionHead.DropDownWidth = 350
        Me.cboTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionHead.FormattingEnabled = True
        Me.cboTransactionHead.Location = New System.Drawing.Point(389, 75)
        Me.cboTransactionHead.Name = "cboTransactionHead"
        Me.cboTransactionHead.Size = New System.Drawing.Size(174, 21)
        Me.cboTransactionHead.TabIndex = 122
        '
        'objbtnSearchHeads
        '
        Me.objbtnSearchHeads.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchHeads.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchHeads.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchHeads.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchHeads.BorderSelected = False
        Me.objbtnSearchHeads.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchHeads.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchHeads.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchHeads.Location = New System.Drawing.Point(569, 75)
        Me.objbtnSearchHeads.Name = "objbtnSearchHeads"
        Me.objbtnSearchHeads.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchHeads.TabIndex = 121
        '
        'objLine2
        '
        Me.objLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine2.Location = New System.Drawing.Point(9, 99)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(648, 10)
        Me.objLine2.TabIndex = 120
        Me.objLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(183, 77)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(29, 16)
        Me.lblTo.TabIndex = 119
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(218, 75)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(89, 21)
        Me.dtpDate2.TabIndex = 114
        '
        'dtpDate1
        '
        Me.dtpDate1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(88, 75)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(89, 21)
        Me.dtpDate1.TabIndex = 114
        '
        'elOperation
        '
        Me.elOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elOperation.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elOperation.Location = New System.Drawing.Point(8, 31)
        Me.elOperation.Name = "elOperation"
        Me.elOperation.Size = New System.Drawing.Size(641, 15)
        Me.elOperation.TabIndex = 110
        Me.elOperation.Text = "Post Rates"
        Me.elOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 449)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(918, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(706, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(809, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "code"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "name"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "activityunkid"
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhActivityUnkid"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'frmSet_Activity_Rate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(918, 504)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbRates)
        Me.Controls.Add(Me.gbFilter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSet_Activity_Rate"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Setting Activity Rates"
        Me.gbFilter.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.pnlActivity.ResumeLayout(False)
        Me.pnlActivity.PerformLayout()
        CType(Me.dgvActivityList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbRates.ResumeLayout(False)
        Me.gbRates.PerformLayout()
        Me.pnlView.ResumeLayout(False)
        CType(Me.dgvFB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbRates As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eLine1 As eZee.Common.eZeeLine
    Friend WithEvents radFB As System.Windows.Forms.RadioButton
    Friend WithEvents radNF As System.Windows.Forms.RadioButton
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchMeasures As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMeasures As System.Windows.Forms.ComboBox
    Friend WithEvents lblMeasures As System.Windows.Forms.Label
    Friend WithEvents elOperation As eZee.Common.eZeeLine
    Friend WithEvents lblRate As System.Windows.Forms.Label
    Friend WithEvents txtRate As eZee.TextBox.NumericTextBox
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents objLine2 As eZee.Common.eZeeLine
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents cboTransactionHead As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchHeads As eZee.Common.eZeeGradientButton
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblHeads As System.Windows.Forms.Label
    Friend WithEvents btnPost As eZee.Common.eZeeLightButton
    Friend WithEvents dgvNF As System.Windows.Forms.DataGridView
    Friend WithEvents pnlView As System.Windows.Forms.Panel    
    Friend WithEvents dgvFB As System.Windows.Forms.DataGridView
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Private WithEvents txtSearchActivity As System.Windows.Forms.TextBox
    Friend WithEvents pnlActivity As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvActivityList As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhMCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhMeasureCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMeasureName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMeasureId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents LblMode As System.Windows.Forms.Label
End Class
