﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmMembershipList

#Region "Private Variable"

    Private objMembershipMaster As clsmembership_master
    Private ReadOnly mstrModuleName As String = "frmMembershipList"

#End Region

#Region "Form's Event"

    Private Sub frmMembershipList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMembershipMaster = New clsmembership_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            fillList()
            If lvMembershipcodes.Items.Count > 0 Then lvMembershipcodes.Items(0).Selected = True
            lvMembershipcodes.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMembershipList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMembershipList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : TAB key fired instead of DELETE key.
                'SendKeys.Send("{TAB}")
                'e.Handled = True
                btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMembershipList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMembershipList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMembershipMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmembership_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmembership_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objfrmMembership_AddEdit As New frmMembership_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmMembership_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmMembership_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmMembership_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If objfrmMembership_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvMembershipcodes.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Membership from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvMembershipcodes.Select()
                Exit Sub
            End If
            Dim objfrmMembership_AddEdit As New frmMembership_AddEdit
            Try

                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmMembership_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmMembership_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmMembership_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvMembershipcodes.SelectedItems(0).Index
                If objfrmMembership_AddEdit.displayDialog(CInt(lvMembershipcodes.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                objfrmMembership_AddEdit = Nothing

                lvMembershipcodes.Items(intSelectedIndex).Selected = True
                lvMembershipcodes.EnsureVisible(intSelectedIndex)
                lvMembershipcodes.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmMembership_AddEdit IsNot Nothing Then objfrmMembership_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvMembershipcodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Membership from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvMembershipcodes.Select()
            Exit Sub
        End If
        If objMembershipMaster.isUsed(CInt(lvMembershipcodes.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Membership. Reason: This Membership is in use."), enMsgBoxStyle.Information) '?2
            lvMembershipcodes.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMembershipcodes.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Membership?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objMembershipMaster.Delete(CInt(lvMembershipcodes.SelectedItems(0).Tag))
                lvMembershipcodes.SelectedItems(0).Remove()

                If lvMembershipcodes.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvMembershipcodes.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvMembershipcodes.Items.Count - 1
                    lvMembershipcodes.Items(intSelectedIndex).Selected = True
                    lvMembershipcodes.EnsureVisible(intSelectedIndex)
                ElseIf lvMembershipcodes.Items.Count <> 0 Then
                    lvMembershipcodes.Items(intSelectedIndex).Selected = True
                    lvMembershipcodes.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvMembershipcodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsMembershipList As New DataSet
        Try

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowToViewMembershipMasterList = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 

            dsMembershipList = objMembershipMaster.GetList("List")

            Dim lvItem As ListViewItem

            lvMembershipcodes.Items.Clear()
            For Each drRow As DataRow In dsMembershipList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("name").ToString
                lvItem.Tag = drRow("membershipunkid")
                lvItem.SubItems.Add(drRow("membershipcode").ToString)
                lvItem.SubItems.Add(drRow("membershipname").ToString)
                'Nilay (05-Feb-2016) -- Start
                'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
                lvItem.SubItems.Add(drRow("employer_membershipno").ToString)
                'Nilay (05-Feb-2016) -- End
                lvItem.SubItems.Add(drRow("description").ToString)
                lvMembershipcodes.Items.Add(lvItem)
            Next

            If lvMembershipcodes.Items.Count > 16 Then
                colhDescription.Width = 270 - 18
            Else
                colhDescription.Width = 270
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsMembershipList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddMembership
            btnEdit.Enabled = User._Object.Privilege._EditMembership
            btnDelete.Enabled = User._Object.Privilege._DeleteMembership

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()

			Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.colhMembershipCategory.Text = Language._Object.getCaption(CStr(Me.colhMembershipCategory.Tag), Me.colhMembershipCategory.Text)
			Me.colhMembershipCode.Text = Language._Object.getCaption(CStr(Me.colhMembershipCode.Tag), Me.colhMembershipCode.Text)
			Me.colhMemberShipName.Text = Language._Object.getCaption(CStr(Me.colhMemberShipName.Tag), Me.colhMemberShipName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Membership from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Membership. Reason: This Membership is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Membership?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class