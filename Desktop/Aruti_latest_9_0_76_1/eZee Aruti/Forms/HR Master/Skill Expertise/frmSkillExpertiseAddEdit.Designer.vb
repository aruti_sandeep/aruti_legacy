﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSkillExpertiseAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSkillExpertiseAddEdit))
        Me.eZeeHeaderSkillExp = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.gbSkillExpertise = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objFooter.SuspendLayout()
        Me.gbSkillExpertise.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeaderSkillExp
        '
        Me.eZeeHeaderSkillExp.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeaderSkillExp.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeaderSkillExp.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeaderSkillExp.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeaderSkillExp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeaderSkillExp.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeaderSkillExp.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeaderSkillExp.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeaderSkillExp.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeaderSkillExp.Icon = Nothing
        Me.eZeeHeaderSkillExp.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeaderSkillExp.Message = ""
        Me.eZeeHeaderSkillExp.Name = "eZeeHeaderSkillExp"
        Me.eZeeHeaderSkillExp.Size = New System.Drawing.Size(406, 60)
        Me.eZeeHeaderSkillExp.TabIndex = 1
        Me.eZeeHeaderSkillExp.Title = "Skill Expertise Information"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 171)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(406, 55)
        Me.objFooter.TabIndex = 9
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(300, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(200, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(322, 63)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 100
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(97, 63)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(219, 21)
        Me.txtName.TabIndex = 99
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(23, 67)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(68, 13)
        Me.lblName.TabIndex = 102
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(97, 36)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(219, 21)
        Me.txtCode.TabIndex = 98
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(23, 40)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(68, 13)
        Me.lblCode.TabIndex = 101
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbSkillExpertise
        '
        Me.gbSkillExpertise.BorderColor = System.Drawing.Color.Black
        Me.gbSkillExpertise.Checked = False
        Me.gbSkillExpertise.CollapseAllExceptThis = False
        Me.gbSkillExpertise.CollapsedHoverImage = Nothing
        Me.gbSkillExpertise.CollapsedNormalImage = Nothing
        Me.gbSkillExpertise.CollapsedPressedImage = Nothing
        Me.gbSkillExpertise.CollapseOnLoad = False
        Me.gbSkillExpertise.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbSkillExpertise.Controls.Add(Me.txtCode)
        Me.gbSkillExpertise.Controls.Add(Me.lblCode)
        Me.gbSkillExpertise.Controls.Add(Me.txtName)
        Me.gbSkillExpertise.Controls.Add(Me.lblName)
        Me.gbSkillExpertise.ExpandedHoverImage = Nothing
        Me.gbSkillExpertise.ExpandedNormalImage = Nothing
        Me.gbSkillExpertise.ExpandedPressedImage = Nothing
        Me.gbSkillExpertise.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSkillExpertise.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSkillExpertise.HeaderHeight = 25
        Me.gbSkillExpertise.HeaderMessage = ""
        Me.gbSkillExpertise.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSkillExpertise.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSkillExpertise.HeightOnCollapse = 0
        Me.gbSkillExpertise.LeftTextSpace = 0
        Me.gbSkillExpertise.Location = New System.Drawing.Point(12, 66)
        Me.gbSkillExpertise.Name = "gbSkillExpertise"
        Me.gbSkillExpertise.OpenHeight = 300
        Me.gbSkillExpertise.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSkillExpertise.ShowBorder = True
        Me.gbSkillExpertise.ShowCheckBox = False
        Me.gbSkillExpertise.ShowCollapseButton = False
        Me.gbSkillExpertise.ShowDefaultBorderColor = True
        Me.gbSkillExpertise.ShowDownButton = False
        Me.gbSkillExpertise.ShowHeader = True
        Me.gbSkillExpertise.Size = New System.Drawing.Size(382, 99)
        Me.gbSkillExpertise.TabIndex = 103
        Me.gbSkillExpertise.Temp = 0
        Me.gbSkillExpertise.Text = "Skill Expertise"
        Me.gbSkillExpertise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSkillExpertiseAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(406, 226)
        Me.Controls.Add(Me.gbSkillExpertise)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.eZeeHeaderSkillExp)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSkillExpertiseAddEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Skill Expertise"
        Me.objFooter.ResumeLayout(False)
        Me.gbSkillExpertise.ResumeLayout(False)
        Me.gbSkillExpertise.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeaderSkillExp As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents gbSkillExpertise As eZee.Common.eZeeCollapsibleContainer
End Class
