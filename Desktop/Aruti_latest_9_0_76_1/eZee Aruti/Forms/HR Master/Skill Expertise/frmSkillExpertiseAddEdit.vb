﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSkillExpertiseAddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmSkillExpertiseAddEdit"
    Private mblnCancel As Boolean = True
    Private objSkillExpertise As clsSkillExpertise_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintSkillexpertiseunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintSkillexpertiseunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintSkillexpertiseunkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmSkillExpertiseAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmSkillExpertiseAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmSkillExpertiseAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmSkillExpertiseAddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSkillExpertise_master.SetMessages()
            objfrm._Other_ModuleNames = "clsSkillExpertise_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmSkillExpertiseAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objSkillExpertise = New clsSkillExpertise_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call OtherSettings()

            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objSkillExpertise._Skillexpertiseunkid = mintSkillexpertiseunkid
            End If

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkillExpertiseAddEdit_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objSkillExpertise._Code
            txtName.Text = objSkillExpertise._Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objSkillExpertise._Code = txtCode.Text.Trim
            objSkillExpertise._Name = txtName.Text.Trim
            objSkillExpertise._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If

            Call objFrmLangPopup.displayDialog(txtName.Text, objSkillExpertise._Name1, objSkillExpertise._Name2)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Skill Expertise Code cannot be blank. Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Skill Expertise Name cannot be blank. Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objSkillExpertise.Update()
            Else
                blnFlag = objSkillExpertise.Insert()
            End If

            If blnFlag = False And objSkillExpertise._Message <> "" Then
                eZeeMsgBox.Show(objSkillExpertise._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objSkillExpertise = Nothing
                    objSkillExpertise = New clsSkillExpertise_master
                    Call GetValue()
                Else
                    mintSkillexpertiseunkid = objSkillExpertise._Skillexpertiseunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSkillExpertise.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSkillExpertise.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeaderSkillExp.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeaderSkillExp.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeaderSkillExp.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeaderSkillExp.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeaderSkillExp.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeaderSkillExp.Title = Language._Object.getCaption(Me.eZeeHeaderSkillExp.Name & "_Title", Me.eZeeHeaderSkillExp.Title)
            Me.eZeeHeaderSkillExp.Message = Language._Object.getCaption(Me.eZeeHeaderSkillExp.Name & "_Message", Me.eZeeHeaderSkillExp.Message)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.gbSkillExpertise.Text = Language._Object.getCaption(Me.gbSkillExpertise.Name, Me.gbSkillExpertise.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Skill Expertise Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Skill Expertise Name cannot be blank. Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class