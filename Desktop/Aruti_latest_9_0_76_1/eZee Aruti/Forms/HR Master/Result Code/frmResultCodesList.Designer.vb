﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResultCodesList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmResultCodesList))
        Me.pnlAssessmentGroupList = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchResultGroup = New eZee.Common.eZeeGradientButton
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchResultName = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboResultName = New System.Windows.Forms.ComboBox
        Me.lblResultName = New System.Windows.Forms.Label
        Me.lvResultCodeList = New eZee.Common.eZeeListView(Me.components)
        Me.colhGroupName = New System.Windows.Forms.ColumnHeader
        Me.colhResultCode = New System.Windows.Forms.ColumnHeader
        Me.colhResultName = New System.Windows.Forms.ColumnHeader
        Me.colhResultLevel = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlAssessmentGroupList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAssessmentGroupList
        '
        Me.pnlAssessmentGroupList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlAssessmentGroupList.Controls.Add(Me.lvResultCodeList)
        Me.pnlAssessmentGroupList.Controls.Add(Me.objFooter)
        Me.pnlAssessmentGroupList.Controls.Add(Me.eZeeHeader)
        Me.pnlAssessmentGroupList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAssessmentGroupList.Location = New System.Drawing.Point(0, 0)
        Me.pnlAssessmentGroupList.Name = "pnlAssessmentGroupList"
        Me.pnlAssessmentGroupList.Size = New System.Drawing.Size(687, 447)
        Me.pnlAssessmentGroupList.TabIndex = 2
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchResultGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboResultGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblResultGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchResultName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboResultName)
        Me.gbFilterCriteria.Controls.Add(Me.lblResultName)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(662, 62)
        Me.gbFilterCriteria.TabIndex = 132
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchResultGroup
        '
        Me.objbtnSearchResultGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResultGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResultGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResultGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResultGroup.BorderSelected = False
        Me.objbtnSearchResultGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResultGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResultGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResultGroup.Location = New System.Drawing.Point(312, 30)
        Me.objbtnSearchResultGroup.Name = "objbtnSearchResultGroup"
        Me.objbtnSearchResultGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResultGroup.TabIndex = 90
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.DropDownWidth = 250
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Location = New System.Drawing.Point(100, 30)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(206, 21)
        Me.cboResultGroup.TabIndex = 1
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(9, 33)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(83, 15)
        Me.lblResultGroup.TabIndex = 89
        Me.lblResultGroup.Text = "Result Group"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(635, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchResultName
        '
        Me.objbtnSearchResultName.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResultName.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResultName.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResultName.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResultName.BorderSelected = False
        Me.objbtnSearchResultName.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResultName.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResultName.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResultName.Location = New System.Drawing.Point(632, 30)
        Me.objbtnSearchResultName.Name = "objbtnSearchResultName"
        Me.objbtnSearchResultName.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResultName.TabIndex = 86
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(612, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboResultName
        '
        Me.cboResultName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultName.DropDownWidth = 250
        Me.cboResultName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultName.FormattingEnabled = True
        Me.cboResultName.Location = New System.Drawing.Point(420, 30)
        Me.cboResultName.Name = "cboResultName"
        Me.cboResultName.Size = New System.Drawing.Size(206, 21)
        Me.cboResultName.TabIndex = 2
        '
        'lblResultName
        '
        Me.lblResultName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultName.Location = New System.Drawing.Point(354, 33)
        Me.lblResultName.Name = "lblResultName"
        Me.lblResultName.Size = New System.Drawing.Size(61, 15)
        Me.lblResultName.TabIndex = 85
        Me.lblResultName.Text = "Name"
        '
        'lvResultCodeList
        '
        Me.lvResultCodeList.BackColorOnChecked = True
        Me.lvResultCodeList.ColumnHeaders = Nothing
        Me.lvResultCodeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhGroupName, Me.colhResultCode, Me.colhResultName, Me.colhResultLevel, Me.colhDescription})
        Me.lvResultCodeList.CompulsoryColumns = ""
        Me.lvResultCodeList.FullRowSelect = True
        Me.lvResultCodeList.GridLines = True
        Me.lvResultCodeList.GroupingColumn = Nothing
        Me.lvResultCodeList.HideSelection = False
        Me.lvResultCodeList.Location = New System.Drawing.Point(12, 134)
        Me.lvResultCodeList.MinColumnWidth = 50
        Me.lvResultCodeList.MultiSelect = False
        Me.lvResultCodeList.Name = "lvResultCodeList"
        Me.lvResultCodeList.OptionalColumns = ""
        Me.lvResultCodeList.ShowMoreItem = False
        Me.lvResultCodeList.ShowSaveItem = False
        Me.lvResultCodeList.ShowSelectAll = True
        Me.lvResultCodeList.ShowSizeAllColumnsToFit = True
        Me.lvResultCodeList.Size = New System.Drawing.Size(663, 249)
        Me.lvResultCodeList.Sortable = True
        Me.lvResultCodeList.TabIndex = 131
        Me.lvResultCodeList.UseCompatibleStateImageBehavior = False
        Me.lvResultCodeList.View = System.Windows.Forms.View.Details
        '
        'colhGroupName
        '
        Me.colhGroupName.Tag = "colhGroupName"
        Me.colhGroupName.Text = "Result Group"
        Me.colhGroupName.Width = 0
        '
        'colhResultCode
        '
        Me.colhResultCode.Tag = "colhResultCode"
        Me.colhResultCode.Text = "Result Code"
        Me.colhResultCode.Width = 100
        '
        'colhResultName
        '
        Me.colhResultName.Tag = "colhResultName"
        Me.colhResultName.Text = "Name"
        Me.colhResultName.Width = 150
        '
        'colhResultLevel
        '
        Me.colhResultLevel.Tag = "colhResultLevel"
        Me.colhResultLevel.Text = "Level"
        Me.colhResultLevel.Width = 90
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 315
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 392)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(687, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(268, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(371, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(474, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(577, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(687, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Result Code List"
        '
        'frmResultCodesList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 447)
        Me.Controls.Add(Me.pnlAssessmentGroupList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmResultCodesList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Result Code List"
        Me.pnlAssessmentGroupList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlAssessmentGroupList As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents lvResultCodeList As eZee.Common.eZeeListView
    Friend WithEvents colhGroupName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResultCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResultName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResultLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchResultName As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboResultName As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultName As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchResultGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
End Class
