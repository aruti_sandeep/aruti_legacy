﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmBenefitplanList

#Region "Private Variable"

    Private objBenefitplanMaster As clsbenefitplan_master
    Private ReadOnly mstrModuleName As String = "frmBenefitplanList"

#End Region

#Region "Form's Event"

    Private Sub frmBenefitplanList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBenefitplanMaster = New clsbenefitplan_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            FillCombo()
            fillList()
            If lvBenefitplan.Items.Count > 0 Then lvBenefitplan.Items(0).Selected = True
            lvBenefitplan.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBenefitplanList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBenefitplanList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvBenefitplan.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : TAB key fired instead of DELETE key.
                'SendKeys.Send("{TAB}")
                'e.Handled = True
                btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBenefitplanList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBenefitplanList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsbenefitplan_master.SetMessages()
            objfrm._Other_ModuleNames = "clsbenefitplan_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmbenefitplan_AddEdit As New frmBenefitplan_AddEdit

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmbenefitplan_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmbenefitplan_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmbenefitplan_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If objfrmbenefitplan_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvBenefitplan.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Benefit Plan from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvBenefitplan.Select()
                Exit Sub
            End If

            'If objBenefitplanMaster.isUsed(CInt(lvBenefitplan.SelectedItems(0).Tag)) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot edit this Benefit Plan. Reason: This Benefit Plan is in use."), enMsgBoxStyle.Information) '?2
            '    lvBenefitplan.Select()
            '    Exit Sub
            'End If

            Dim objfrmBenefitplan_AddEdit As New frmBenefitplan_AddEdit
            Try
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmBenefitplan_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmBenefitplan_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmBenefitplan_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END


                Dim intSelectedIndex As Integer
                intSelectedIndex = lvBenefitplan.SelectedItems(0).Index
                If objfrmBenefitplan_AddEdit.displayDialog(CInt(lvBenefitplan.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                objfrmBenefitplan_AddEdit = Nothing

                lvBenefitplan.Items(intSelectedIndex).Selected = True
                lvBenefitplan.EnsureVisible(intSelectedIndex)
                lvBenefitplan.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmBenefitplan_AddEdit IsNot Nothing Then objfrmBenefitplan_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvBenefitplan.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Benefit Plan from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBenefitplan.Select()
            Exit Sub
        End If
        'If objBenefitplanMaster.isUsed(CInt(lvBenefitplan.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Benefit Plan. Reason: This Benefit Plan is in use."), enMsgBoxStyle.Information) '?2
        '    lvBenefitplan.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBenefitplan.SelectedItems(0).Index

            If objBenefitplanMaster.isUsed(CInt(lvBenefitplan.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot edit this Benefit Plan. Reason: This Benefit Plan is in use."), enMsgBoxStyle.Information) '?2
                lvBenefitplan.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Benefit Plan?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objBenefitplanMaster.Delete(CInt(lvBenefitplan.SelectedItems(0).Tag))
                lvBenefitplan.SelectedItems(0).Remove()

                If lvBenefitplan.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBenefitplan.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBenefitplan.Items.Count - 1
                    lvBenefitplan.Items(intSelectedIndex).Selected = True
                    lvBenefitplan.EnsureVisible(intSelectedIndex)
                ElseIf lvBenefitplan.Items.Count <> 0 Then
                    lvBenefitplan.Items(intSelectedIndex).Selected = True
                    lvBenefitplan.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBenefitplan.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboBenefitgroup.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim ObjCommonMaster As New clsCommon_Master
            Dim dsBenefitGroup As DataSet = ObjCommonMaster.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "Benefit Group")
            cboBenefitgroup.ValueMember = "masterunkid"
            cboBenefitgroup.DisplayMember = "name"
            cboBenefitgroup.DataSource = dsBenefitGroup.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsBenefitPlanList As New DataSet
        Dim strSearching As String = ""
        Dim dtBenefitPlan As New DataTable
        Try

            If User._Object.Privilege._AllowToViewBenefitPlanList = True Then   'Pinkal (09-Jul-2012) -- Start

                dsBenefitPlanList = objBenefitplanMaster.GetList("List")

                If CInt(cboBenefitgroup.SelectedValue) > 0 Then
                    strSearching = "AND benefitgroupunkid =" & CInt(cboBenefitgroup.SelectedValue) & " "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtBenefitPlan = New DataView(dsBenefitPlanList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtBenefitPlan = dsBenefitPlanList.Tables("List")
                End If

                Dim lvItem As ListViewItem

                lvBenefitplan.Items.Clear()
                For Each drRow As DataRow In dtBenefitPlan.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("name").ToString
                    lvItem.Tag = drRow("benefitplanunkid")
                    lvItem.SubItems.Add(drRow("benefitplancode").ToString)
                    lvItem.SubItems.Add(drRow("benefitplanname").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvBenefitplan.Items.Add(lvItem)
                Next

                If lvBenefitplan.Items.Count > 16 Then
                    colhBenefitplandesc.Width = 265 - 18
                Else
                    colhBenefitplandesc.Width = 265
                End If


            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsBenefitPlanList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddBenefitPlan
            btnEdit.Enabled = User._Object.Privilege._EditBenefitPlan
            btnDelete.Enabled = User._Object.Privilege._DeleteBenefitPlan

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbBenefitplan.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBenefitplan.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbBenefitplan.Text = Language._Object.getCaption(Me.gbBenefitplan.Name, Me.gbBenefitplan.Text)
            Me.lblBenefitGroup.Text = Language._Object.getCaption(Me.lblBenefitGroup.Name, Me.lblBenefitGroup.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhBenefitgroup.Text = Language._Object.getCaption(CStr(Me.colhBenefitgroup.Tag), Me.colhBenefitgroup.Text)
            Me.colhBenefitplancode.Text = Language._Object.getCaption(CStr(Me.colhBenefitplancode.Tag), Me.colhBenefitplancode.Text)
            Me.colhBenefitplanname.Text = Language._Object.getCaption(CStr(Me.colhBenefitplanname.Tag), Me.colhBenefitplanname.Text)
            Me.colhBenefitplandesc.Text = Language._Object.getCaption(CStr(Me.colhBenefitplandesc.Tag), Me.colhBenefitplandesc.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Benefit Plan from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot edit this Benefit Plan. Reason: This Benefit Plan is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Benefit Plan?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class