﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTraningAnalysis_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTraningAnalysis_AddEdit))
        Me.pnlTrainingAnalysis = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbTrainingAnalysis = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddResult = New eZee.Common.eZeeGradientButton
        Me.pnlOtherInfo = New System.Windows.Forms.Panel
        Me.objlblOColon3 = New System.Windows.Forms.Label
        Me.objlblOColon2 = New System.Windows.Forms.Label
        Me.objlblOColon1 = New System.Windows.Forms.Label
        Me.objlblOtherEmpContactNo = New System.Windows.Forms.Label
        Me.objlblOtherCompany = New System.Windows.Forms.Label
        Me.objlblOtherDeptName = New System.Windows.Forms.Label
        Me.objbtnOtherEmployee = New eZee.Common.eZeeGradientButton
        Me.cboOtherEmployee = New System.Windows.Forms.ComboBox
        Me.lblTrainerContactNo = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblPosition = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtRevewier_remark = New eZee.TextBox.AlphanumericTextBox
        Me.lvAnalysis = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhPosition = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.colhScore = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhResultId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhTrainerId = New System.Windows.Forms.ColumnHeader
        Me.objcolhanalysisdate = New System.Windows.Forms.ColumnHeader
        Me.radOthers = New System.Windows.Forms.RadioButton
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.pnlEmployeeInfo = New System.Windows.Forms.Panel
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objColon3 = New System.Windows.Forms.Label
        Me.objColon2 = New System.Windows.Forms.Label
        Me.objColon1 = New System.Windows.Forms.Label
        Me.lblEmpContact = New System.Windows.Forms.Label
        Me.lblEmpCompany = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objlblEmployeeContactNo = New System.Windows.Forms.Label
        Me.objlblCompanyValue = New System.Windows.Forms.Label
        Me.objlblDepartmentValue = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblScore = New System.Windows.Forms.Label
        Me.cboScore = New System.Windows.Forms.ComboBox
        Me.txtTraineeRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblTraineeRemark = New System.Windows.Forms.Label
        Me.objStline1 = New eZee.Common.eZeeStraightLine
        Me.objLine = New eZee.Common.eZeeLine
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnReviewerInfo = New eZee.Common.eZeeLine
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.dtpAnalysisDate = New System.Windows.Forms.DateTimePicker
        Me.lblAnalysisDate = New System.Windows.Forms.Label
        Me.lnTraineeInfo = New eZee.Common.eZeeLine
        Me.txtCourse = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmployeeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblCourse = New System.Windows.Forms.Label
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlTrainingAnalysis.SuspendLayout()
        Me.gbTrainingAnalysis.SuspendLayout()
        Me.pnlOtherInfo.SuspendLayout()
        Me.pnlEmployeeInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTrainingAnalysis
        '
        Me.pnlTrainingAnalysis.Controls.Add(Me.eZeeHeader)
        Me.pnlTrainingAnalysis.Controls.Add(Me.gbTrainingAnalysis)
        Me.pnlTrainingAnalysis.Controls.Add(Me.objFooter)
        Me.pnlTrainingAnalysis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTrainingAnalysis.Location = New System.Drawing.Point(0, 0)
        Me.pnlTrainingAnalysis.Name = "pnlTrainingAnalysis"
        Me.pnlTrainingAnalysis.Size = New System.Drawing.Size(861, 530)
        Me.pnlTrainingAnalysis.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(861, 58)
        Me.eZeeHeader.TabIndex = 34
        Me.eZeeHeader.Title = "Training Analysis"
        '
        'gbTrainingAnalysis
        '
        Me.gbTrainingAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.gbTrainingAnalysis.BorderColor = System.Drawing.Color.Black
        Me.gbTrainingAnalysis.Checked = False
        Me.gbTrainingAnalysis.CollapseAllExceptThis = False
        Me.gbTrainingAnalysis.CollapsedHoverImage = Nothing
        Me.gbTrainingAnalysis.CollapsedNormalImage = Nothing
        Me.gbTrainingAnalysis.CollapsedPressedImage = Nothing
        Me.gbTrainingAnalysis.CollapseOnLoad = False
        Me.gbTrainingAnalysis.Controls.Add(Me.objbtnAddResult)
        Me.gbTrainingAnalysis.Controls.Add(Me.pnlOtherInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtRevewier_remark)
        Me.gbTrainingAnalysis.Controls.Add(Me.lvAnalysis)
        Me.gbTrainingAnalysis.Controls.Add(Me.radOthers)
        Me.gbTrainingAnalysis.Controls.Add(Me.radEmployee)
        Me.gbTrainingAnalysis.Controls.Add(Me.pnlEmployeeInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblRemark)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblScore)
        Me.gbTrainingAnalysis.Controls.Add(Me.cboScore)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtTraineeRemark)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblTraineeRemark)
        Me.gbTrainingAnalysis.Controls.Add(Me.objStline1)
        Me.gbTrainingAnalysis.Controls.Add(Me.objLine)
        Me.gbTrainingAnalysis.Controls.Add(Me.btnEdit)
        Me.gbTrainingAnalysis.Controls.Add(Me.btnDelete)
        Me.gbTrainingAnalysis.Controls.Add(Me.lnReviewerInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.btnAdd)
        Me.gbTrainingAnalysis.Controls.Add(Me.objLine1)
        Me.gbTrainingAnalysis.Controls.Add(Me.dtpAnalysisDate)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblAnalysisDate)
        Me.gbTrainingAnalysis.Controls.Add(Me.lnTraineeInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtCourse)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtEmployeeName)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblCourse)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblEmployeeName)
        Me.gbTrainingAnalysis.ExpandedHoverImage = Nothing
        Me.gbTrainingAnalysis.ExpandedNormalImage = Nothing
        Me.gbTrainingAnalysis.ExpandedPressedImage = Nothing
        Me.gbTrainingAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrainingAnalysis.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrainingAnalysis.HeaderHeight = 25
        Me.gbTrainingAnalysis.HeaderMessage = ""
        Me.gbTrainingAnalysis.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTrainingAnalysis.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTrainingAnalysis.HeightOnCollapse = 0
        Me.gbTrainingAnalysis.LeftTextSpace = 0
        Me.gbTrainingAnalysis.Location = New System.Drawing.Point(11, 64)
        Me.gbTrainingAnalysis.Name = "gbTrainingAnalysis"
        Me.gbTrainingAnalysis.OpenHeight = 413
        Me.gbTrainingAnalysis.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrainingAnalysis.ShowBorder = True
        Me.gbTrainingAnalysis.ShowCheckBox = False
        Me.gbTrainingAnalysis.ShowCollapseButton = False
        Me.gbTrainingAnalysis.ShowDefaultBorderColor = True
        Me.gbTrainingAnalysis.ShowDownButton = False
        Me.gbTrainingAnalysis.ShowHeader = True
        Me.gbTrainingAnalysis.Size = New System.Drawing.Size(838, 405)
        Me.gbTrainingAnalysis.TabIndex = 0
        Me.gbTrainingAnalysis.Temp = 0
        Me.gbTrainingAnalysis.Text = "Training Analysis"
        Me.gbTrainingAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddResult
        '
        Me.objbtnAddResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResult.BorderSelected = False
        Me.objbtnAddResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResult.Location = New System.Drawing.Point(803, 76)
        Me.objbtnAddResult.Name = "objbtnAddResult"
        Me.objbtnAddResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResult.TabIndex = 204
        '
        'pnlOtherInfo
        '
        Me.pnlOtherInfo.Controls.Add(Me.objlblOColon3)
        Me.pnlOtherInfo.Controls.Add(Me.objlblOColon2)
        Me.pnlOtherInfo.Controls.Add(Me.objlblOColon1)
        Me.pnlOtherInfo.Controls.Add(Me.objlblOtherEmpContactNo)
        Me.pnlOtherInfo.Controls.Add(Me.objlblOtherCompany)
        Me.pnlOtherInfo.Controls.Add(Me.objlblOtherDeptName)
        Me.pnlOtherInfo.Controls.Add(Me.objbtnOtherEmployee)
        Me.pnlOtherInfo.Controls.Add(Me.cboOtherEmployee)
        Me.pnlOtherInfo.Controls.Add(Me.lblTrainerContactNo)
        Me.pnlOtherInfo.Controls.Add(Me.lblCompany)
        Me.pnlOtherInfo.Controls.Add(Me.lblPosition)
        Me.pnlOtherInfo.Controls.Add(Me.lblName)
        Me.pnlOtherInfo.Location = New System.Drawing.Point(304, 78)
        Me.pnlOtherInfo.Name = "pnlOtherInfo"
        Me.pnlOtherInfo.Size = New System.Drawing.Size(291, 101)
        Me.pnlOtherInfo.TabIndex = 13
        '
        'objlblOColon3
        '
        Me.objlblOColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOColon3.Location = New System.Drawing.Point(114, 76)
        Me.objlblOColon3.Name = "objlblOColon3"
        Me.objlblOColon3.Size = New System.Drawing.Size(8, 15)
        Me.objlblOColon3.TabIndex = 10
        Me.objlblOColon3.Text = ":"
        Me.objlblOColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblOColon2
        '
        Me.objlblOColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOColon2.Location = New System.Drawing.Point(114, 51)
        Me.objlblOColon2.Name = "objlblOColon2"
        Me.objlblOColon2.Size = New System.Drawing.Size(8, 15)
        Me.objlblOColon2.TabIndex = 7
        Me.objlblOColon2.Text = ":"
        Me.objlblOColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblOColon1
        '
        Me.objlblOColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOColon1.Location = New System.Drawing.Point(114, 31)
        Me.objlblOColon1.Name = "objlblOColon1"
        Me.objlblOColon1.Size = New System.Drawing.Size(8, 15)
        Me.objlblOColon1.TabIndex = 4
        Me.objlblOColon1.Text = ":"
        Me.objlblOColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblOtherEmpContactNo
        '
        Me.objlblOtherEmpContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOtherEmpContactNo.Location = New System.Drawing.Point(128, 76)
        Me.objlblOtherEmpContactNo.Name = "objlblOtherEmpContactNo"
        Me.objlblOtherEmpContactNo.Size = New System.Drawing.Size(134, 15)
        Me.objlblOtherEmpContactNo.TabIndex = 11
        Me.objlblOtherEmpContactNo.Text = "#OtherContact No"
        Me.objlblOtherEmpContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblOtherCompany
        '
        Me.objlblOtherCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOtherCompany.Location = New System.Drawing.Point(128, 51)
        Me.objlblOtherCompany.Name = "objlblOtherCompany"
        Me.objlblOtherCompany.Size = New System.Drawing.Size(134, 15)
        Me.objlblOtherCompany.TabIndex = 8
        Me.objlblOtherCompany.Text = "#OtherCompany"
        Me.objlblOtherCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblOtherDeptName
        '
        Me.objlblOtherDeptName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOtherDeptName.Location = New System.Drawing.Point(128, 31)
        Me.objlblOtherDeptName.Name = "objlblOtherDeptName"
        Me.objlblOtherDeptName.Size = New System.Drawing.Size(134, 15)
        Me.objlblOtherDeptName.TabIndex = 5
        Me.objlblOtherDeptName.Text = "#OtherEmpDepartmentName"
        Me.objlblOtherDeptName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherEmployee
        '
        Me.objbtnOtherEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherEmployee.BorderSelected = False
        Me.objbtnOtherEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherEmployee.Image = CType(resources.GetObject("objbtnOtherEmployee.Image"), System.Drawing.Image)
        Me.objbtnOtherEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherEmployee.Location = New System.Drawing.Point(246, 1)
        Me.objbtnOtherEmployee.Name = "objbtnOtherEmployee"
        Me.objbtnOtherEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherEmployee.TabIndex = 0
        '
        'cboOtherEmployee
        '
        Me.cboOtherEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEmployee.FormattingEnabled = True
        Me.cboOtherEmployee.Location = New System.Drawing.Point(111, 1)
        Me.cboOtherEmployee.Name = "cboOtherEmployee"
        Me.cboOtherEmployee.Size = New System.Drawing.Size(129, 21)
        Me.cboOtherEmployee.TabIndex = 1
        '
        'lblTrainerContactNo
        '
        Me.lblTrainerContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainerContactNo.Location = New System.Drawing.Point(10, 76)
        Me.lblTrainerContactNo.Name = "lblTrainerContactNo"
        Me.lblTrainerContactNo.Size = New System.Drawing.Size(94, 15)
        Me.lblTrainerContactNo.TabIndex = 9
        Me.lblTrainerContactNo.Text = "Contact No"
        Me.lblTrainerContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCompany
        '
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(10, 51)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(94, 15)
        Me.lblCompany.TabIndex = 6
        Me.lblCompany.Text = "Institue/Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition
        '
        Me.lblPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPosition.Location = New System.Drawing.Point(10, 31)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(94, 15)
        Me.lblPosition.TabIndex = 3
        Me.lblPosition.Text = "Position"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(10, 4)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(94, 15)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRevewier_remark
        '
        Me.txtRevewier_remark.Flags = 0
        Me.txtRevewier_remark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRevewier_remark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRevewier_remark.Location = New System.Drawing.Point(617, 129)
        Me.txtRevewier_remark.Multiline = True
        Me.txtRevewier_remark.Name = "txtRevewier_remark"
        Me.txtRevewier_remark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRevewier_remark.Size = New System.Drawing.Size(207, 49)
        Me.txtRevewier_remark.TabIndex = 17
        '
        'lvAnalysis
        '
        Me.lvAnalysis.BackColorOnChecked = True
        Me.lvAnalysis.ColumnHeaders = Nothing
        Me.lvAnalysis.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhPosition, Me.colhCompany, Me.colhContactNo, Me.colhScore, Me.colhRemark, Me.objcolhResultId, Me.objcolhGUID, Me.objcolhTrainerId, Me.objcolhanalysisdate})
        Me.lvAnalysis.CompulsoryColumns = ""
        Me.lvAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAnalysis.FullRowSelect = True
        Me.lvAnalysis.GridLines = True
        Me.lvAnalysis.GroupingColumn = Nothing
        Me.lvAnalysis.HideSelection = False
        Me.lvAnalysis.Location = New System.Drawing.Point(11, 198)
        Me.lvAnalysis.MinColumnWidth = 50
        Me.lvAnalysis.MultiSelect = False
        Me.lvAnalysis.Name = "lvAnalysis"
        Me.lvAnalysis.OptionalColumns = ""
        Me.lvAnalysis.ShowMoreItem = False
        Me.lvAnalysis.ShowSaveItem = False
        Me.lvAnalysis.ShowSelectAll = True
        Me.lvAnalysis.ShowSizeAllColumnsToFit = True
        Me.lvAnalysis.Size = New System.Drawing.Size(817, 163)
        Me.lvAnalysis.Sortable = True
        Me.lvAnalysis.TabIndex = 202
        Me.lvAnalysis.UseCompatibleStateImageBehavior = False
        Me.lvAnalysis.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 145
        '
        'colhPosition
        '
        Me.colhPosition.Tag = "colhPosition"
        Me.colhPosition.Text = "Position/Department"
        Me.colhPosition.Width = 110
        '
        'colhCompany
        '
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 110
        '
        'colhContactNo
        '
        Me.colhContactNo.Tag = "colhContactNo"
        Me.colhContactNo.Text = "Contact No"
        Me.colhContactNo.Width = 110
        '
        'colhScore
        '
        Me.colhScore.Tag = "colhScore"
        Me.colhScore.Text = "Score"
        Me.colhScore.Width = 110
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 225
        '
        'objcolhResultId
        '
        Me.objcolhResultId.Tag = "objcolhResultId"
        Me.objcolhResultId.Text = ""
        Me.objcolhResultId.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'objcolhTrainerId
        '
        Me.objcolhTrainerId.Tag = "objcolhTrainerId"
        Me.objcolhTrainerId.Text = ""
        Me.objcolhTrainerId.Width = 0
        '
        'objcolhanalysisdate
        '
        Me.objcolhanalysisdate.Tag = "objcolhanalysisdate"
        Me.objcolhanalysisdate.Text = ""
        Me.objcolhanalysisdate.Width = 0
        '
        'radOthers
        '
        Me.radOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOthers.Location = New System.Drawing.Point(433, 56)
        Me.radOthers.Name = "radOthers"
        Me.radOthers.Size = New System.Drawing.Size(144, 17)
        Me.radOthers.TabIndex = 12
        Me.radOthers.TabStop = True
        Me.radOthers.Text = "Other"
        Me.radOthers.UseVisualStyleBackColor = True
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(304, 56)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(123, 17)
        Me.radEmployee.TabIndex = 11
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'pnlEmployeeInfo
        '
        Me.pnlEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.pnlEmployeeInfo.Controls.Add(Me.objColon3)
        Me.pnlEmployeeInfo.Controls.Add(Me.objColon2)
        Me.pnlEmployeeInfo.Controls.Add(Me.objColon1)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblEmpContact)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblEmpCompany)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblDepartment)
        Me.pnlEmployeeInfo.Controls.Add(Me.objlblEmployeeContactNo)
        Me.pnlEmployeeInfo.Controls.Add(Me.objlblCompanyValue)
        Me.pnlEmployeeInfo.Controls.Add(Me.objlblDepartmentValue)
        Me.pnlEmployeeInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlEmployeeInfo.Controls.Add(Me.cboEmployee)
        Me.pnlEmployeeInfo.Location = New System.Drawing.Point(304, 78)
        Me.pnlEmployeeInfo.Name = "pnlEmployeeInfo"
        Me.pnlEmployeeInfo.Size = New System.Drawing.Size(270, 100)
        Me.pnlEmployeeInfo.TabIndex = 12
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(10, 4)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(94, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Name"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon3
        '
        Me.objColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon3.Location = New System.Drawing.Point(94, 75)
        Me.objColon3.Name = "objColon3"
        Me.objColon3.Size = New System.Drawing.Size(8, 15)
        Me.objColon3.TabIndex = 0
        Me.objColon3.Text = ":"
        Me.objColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon2
        '
        Me.objColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon2.Location = New System.Drawing.Point(94, 52)
        Me.objColon2.Name = "objColon2"
        Me.objColon2.Size = New System.Drawing.Size(8, 15)
        Me.objColon2.TabIndex = 11
        Me.objColon2.Text = ":"
        Me.objColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon1
        '
        Me.objColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon1.Location = New System.Drawing.Point(94, 31)
        Me.objColon1.Name = "objColon1"
        Me.objColon1.Size = New System.Drawing.Size(8, 15)
        Me.objColon1.TabIndex = 4
        Me.objColon1.Text = ":"
        Me.objColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpContact
        '
        Me.lblEmpContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContact.Location = New System.Drawing.Point(10, 75)
        Me.lblEmpContact.Name = "lblEmpContact"
        Me.lblEmpContact.Size = New System.Drawing.Size(78, 15)
        Me.lblEmpContact.TabIndex = 9
        Me.lblEmpContact.Text = "Contact No"
        Me.lblEmpContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpCompany
        '
        Me.lblEmpCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCompany.Location = New System.Drawing.Point(10, 52)
        Me.lblEmpCompany.Name = "lblEmpCompany"
        Me.lblEmpCompany.Size = New System.Drawing.Size(78, 15)
        Me.lblEmpCompany.TabIndex = 6
        Me.lblEmpCompany.Text = "Company"
        Me.lblEmpCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(10, 31)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(78, 15)
        Me.lblDepartment.TabIndex = 3
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmployeeContactNo
        '
        Me.objlblEmployeeContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmployeeContactNo.Location = New System.Drawing.Point(108, 75)
        Me.objlblEmployeeContactNo.Name = "objlblEmployeeContactNo"
        Me.objlblEmployeeContactNo.Size = New System.Drawing.Size(154, 15)
        Me.objlblEmployeeContactNo.TabIndex = 11
        Me.objlblEmployeeContactNo.Text = "#Contact No"
        Me.objlblEmployeeContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCompanyValue
        '
        Me.objlblCompanyValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCompanyValue.Location = New System.Drawing.Point(108, 52)
        Me.objlblCompanyValue.Name = "objlblCompanyValue"
        Me.objlblCompanyValue.Size = New System.Drawing.Size(154, 15)
        Me.objlblCompanyValue.TabIndex = 8
        Me.objlblCompanyValue.Text = "#CompanyName"
        Me.objlblCompanyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblDepartmentValue
        '
        Me.objlblDepartmentValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDepartmentValue.Location = New System.Drawing.Point(108, 31)
        Me.objlblDepartmentValue.Name = "objlblDepartmentValue"
        Me.objlblDepartmentValue.Size = New System.Drawing.Size(154, 15)
        Me.objlblDepartmentValue.TabIndex = 5
        Me.objlblDepartmentValue.Text = "#DepartmentName"
        Me.objlblDepartmentValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(246, 1)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(111, 1)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(129, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(620, 106)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(80, 15)
        Me.lblRemark.TabIndex = 16
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblScore
        '
        Me.lblScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore.Location = New System.Drawing.Point(617, 57)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(94, 15)
        Me.lblScore.TabIndex = 14
        Me.lblScore.Text = "Score"
        Me.lblScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboScore
        '
        Me.cboScore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScore.FormattingEnabled = True
        Me.cboScore.Location = New System.Drawing.Point(617, 76)
        Me.cboScore.Name = "cboScore"
        Me.cboScore.Size = New System.Drawing.Size(180, 21)
        Me.cboScore.TabIndex = 15
        '
        'txtTraineeRemark
        '
        Me.txtTraineeRemark.Flags = 0
        Me.txtTraineeRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTraineeRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTraineeRemark.Location = New System.Drawing.Point(74, 137)
        Me.txtTraineeRemark.Multiline = True
        Me.txtTraineeRemark.Name = "txtTraineeRemark"
        Me.txtTraineeRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTraineeRemark.Size = New System.Drawing.Size(207, 41)
        Me.txtTraineeRemark.TabIndex = 8
        '
        'lblTraineeRemark
        '
        Me.lblTraineeRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraineeRemark.Location = New System.Drawing.Point(18, 140)
        Me.lblTraineeRemark.Name = "lblTraineeRemark"
        Me.lblTraineeRemark.Size = New System.Drawing.Size(50, 15)
        Me.lblTraineeRemark.TabIndex = 7
        Me.lblTraineeRemark.Text = "Remark"
        Me.lblTraineeRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStline1
        '
        Me.objStline1.BackColor = System.Drawing.Color.Transparent
        Me.objStline1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStline1.Location = New System.Drawing.Point(599, 56)
        Me.objStline1.Name = "objStline1"
        Me.objStline1.Size = New System.Drawing.Size(12, 122)
        Me.objStline1.TabIndex = 13
        Me.objStline1.Text = "EZeeStraightLine1"
        '
        'objLine
        '
        Me.objLine.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine.Location = New System.Drawing.Point(8, 188)
        Me.objLine.Name = "objLine"
        Me.objLine.Size = New System.Drawing.Size(820, 7)
        Me.objLine.TabIndex = 187
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(628, 367)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 180
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(731, 367)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 181
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'lnReviewerInfo
        '
        Me.lnReviewerInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnReviewerInfo.Location = New System.Drawing.Point(304, 36)
        Me.lnReviewerInfo.Name = "lnReviewerInfo"
        Me.lnReviewerInfo.Size = New System.Drawing.Size(520, 17)
        Me.lnReviewerInfo.TabIndex = 10
        Me.lnReviewerInfo.Text = "Reviewer Info"
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(525, 367)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 179
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(287, 36)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(11, 142)
        Me.objLine1.TabIndex = 9
        Me.objLine1.Text = "EZeeStraightLine1"
        '
        'dtpAnalysisDate
        '
        Me.dtpAnalysisDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDate.Checked = False
        Me.dtpAnalysisDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAnalysisDate.Location = New System.Drawing.Point(74, 110)
        Me.dtpAnalysisDate.Name = "dtpAnalysisDate"
        Me.dtpAnalysisDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpAnalysisDate.TabIndex = 6
        '
        'lblAnalysisDate
        '
        Me.lblAnalysisDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnalysisDate.Location = New System.Drawing.Point(18, 113)
        Me.lblAnalysisDate.Name = "lblAnalysisDate"
        Me.lblAnalysisDate.Size = New System.Drawing.Size(50, 15)
        Me.lblAnalysisDate.TabIndex = 5
        Me.lblAnalysisDate.Text = "Date"
        Me.lblAnalysisDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnTraineeInfo
        '
        Me.lnTraineeInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnTraineeInfo.Location = New System.Drawing.Point(8, 36)
        Me.lnTraineeInfo.Name = "lnTraineeInfo"
        Me.lnTraineeInfo.Size = New System.Drawing.Size(273, 17)
        Me.lnTraineeInfo.TabIndex = 0
        Me.lnTraineeInfo.Text = "Trainee Info"
        '
        'txtCourse
        '
        Me.txtCourse.Flags = 0
        Me.txtCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCourse.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCourse.Location = New System.Drawing.Point(74, 83)
        Me.txtCourse.Name = "txtCourse"
        Me.txtCourse.ReadOnly = True
        Me.txtCourse.Size = New System.Drawing.Size(207, 21)
        Me.txtCourse.TabIndex = 4
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.Flags = 0
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeName.Location = New System.Drawing.Point(74, 56)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.Size = New System.Drawing.Size(207, 21)
        Me.txtEmployeeName.TabIndex = 2
        '
        'lblCourse
        '
        Me.lblCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.Location = New System.Drawing.Point(18, 86)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(50, 15)
        Me.lblCourse.TabIndex = 3
        Me.lblCourse.Text = "Course"
        Me.lblCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(18, 59)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(50, 14)
        Me.lblEmployeeName.TabIndex = 1
        Me.lblEmployeeName.Text = "Trainee"
        Me.lblEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 475)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(861, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(649, 15)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 126
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(752, 15)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 125
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmTraningAnalysis_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(861, 530)
        Me.Controls.Add(Me.pnlTrainingAnalysis)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTraningAnalysis_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Analysis"
        Me.pnlTrainingAnalysis.ResumeLayout(False)
        Me.gbTrainingAnalysis.ResumeLayout(False)
        Me.gbTrainingAnalysis.PerformLayout()
        Me.pnlOtherInfo.ResumeLayout(False)
        Me.pnlEmployeeInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTrainingAnalysis As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents txtCourse As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployeeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblAnalysisDate As System.Windows.Forms.Label
    Friend WithEvents dtpAnalysisDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbTrainingAnalysis As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents lnTraineeInfo As eZee.Common.eZeeLine
    Friend WithEvents lnReviewerInfo As eZee.Common.eZeeLine
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine As eZee.Common.eZeeLine
    Friend WithEvents objStline1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblTraineeRemark As System.Windows.Forms.Label
    Friend WithEvents txtTraineeRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents AlphanumericTextBox1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblScore As System.Windows.Forms.Label
    Friend WithEvents cboScore As System.Windows.Forms.ComboBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents radOthers As System.Windows.Forms.RadioButton
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents pnlOtherInfo As System.Windows.Forms.Panel
    Friend WithEvents objlblOtherEmpContactNo As System.Windows.Forms.Label
    Friend WithEvents objlblOtherCompany As System.Windows.Forms.Label
    Friend WithEvents objlblOtherDeptName As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainerContactNo As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeInfo As System.Windows.Forms.Panel
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objColon3 As System.Windows.Forms.Label
    Friend WithEvents objColon2 As System.Windows.Forms.Label
    Friend WithEvents objColon1 As System.Windows.Forms.Label
    Friend WithEvents lblEmpContact As System.Windows.Forms.Label
    Friend WithEvents lblEmpCompany As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblEmployeeContactNo As System.Windows.Forms.Label
    Friend WithEvents objlblDepartmentValue As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objlblOColon3 As System.Windows.Forms.Label
    Friend WithEvents objlblOColon2 As System.Windows.Forms.Label
    Friend WithEvents objlblOColon1 As System.Windows.Forms.Label
    Friend WithEvents lvAnalysis As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPosition As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScore As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtRevewier_remark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objlblCompanyValue As System.Windows.Forms.Label
    Friend WithEvents objcolhEobjcolhTrainerIdmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhResultId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhTrainerId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhanalysisdate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddResult As eZee.Common.eZeeGradientButton
End Class
