﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmTrainingAttendanceList

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmTrainingAttendanceList"
    Private objTrainingAttendance As clsTraining_Attendance_Master

#End Region

#Region "Private Functions"

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objTrainingSchedule As New clsTraining_Scheduling

        Try
            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objTrainingSchedule.getComboList("Course", True)
            dsList = objTrainingSchedule.getComboList("Course", True, , True)
            'S.SANDEEP [ 18 FEB 2012 ] -- END
            With cboCourse
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Course")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    Private Sub FillList()
        Dim dsAttendance As New DataSet
        Dim lvItem As ListViewItem
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable

        Try
            dsAttendance = objTrainingAttendance.GetList("Attendance")

            If CInt(cboCourse.SelectedValue) > 0 Then
                StrSearching &= "AND CourseId = " & CInt(cboCourse.SelectedValue) & ""
            End If

            If txtTotalPresent.Text.Trim <> "" Then
                If CDec(txtTotalPresent.Text.Trim) > 0 Then
                    StrSearching &= "AND ispresent = " & CDec(txtTotalPresent.Text.Trim) & " "
                End If
            End If

            If txtTotalAbsent.Text.Trim <> "" Then
                If CDec(txtTotalAbsent.Text.Trim) > 0 Then
                    StrSearching &= "AND isabsent = " & CDec(txtTotalAbsent.Text.Trim) & " "
                End If
            End If

            If txtRemark.Text.Trim <> "" Then
                StrSearching &= "AND Remark like '" & txtRemark.Text.Trim & "%' "
            End If

            If dtpAttendanceDate.Checked Then
                StrSearching &= "AND AttendanceDate ='" & eZeeDate.convertDate(dtpAttendanceDate.Value) & "' "
            End If

            If dtpStartDate.Checked Then
                StrSearching &= "AND StDate ='" & eZeeDate.convertDate(dtpStartDate.Value) & "' "
            End If

            If dtpEndDate.Checked Then
                StrSearching &= "AND EnDate ='" & eZeeDate.convertDate(dtpEndDate.Value) & "' "
            End If



            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsAttendance.Tables("Attendance"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsAttendance.Tables("Attendance")
            End If


            lvAttendanceList.Items.Clear()

            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = drRow.Item("Course").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(drRow.Item("StDate").ToString).ToShortDateString)
                lvItem.SubItems.Add(eZeeDate.convertDate(drRow.Item("EnDate").ToString).ToShortDateString)
                lvItem.SubItems.Add(eZeeDate.convertDate(drRow.Item("AttendanceDate").ToString).ToShortDateString)
                lvItem.SubItems.Add(drRow.Item("ispresent").ToString)
                lvItem.SubItems.Add(drRow.Item("isabsent").ToString)
                lvItem.SubItems.Add(drRow.Item("Remark").ToString)
                lvItem.SubItems.Add(drRow.Item("CourseId").ToString)

                lvItem.Tag = drRow.Item("AttID")

                lvAttendanceList.Items.Add(lvItem)

                lvItem = Nothing

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Form's Event"

    Private Sub frmTrainingAttendanceList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTrainingAttendance = New clsTraining_Attendance_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call FillCombo()
            Call FillList()
            If lvAttendanceList.Items.Count > 0 Then lvAttendanceList.Items(0).Selected = True
            lvAttendanceList.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingAttendanceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingAttendanceList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Delete And lvAttendanceList.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmTrainingAttendanceList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmTrainingAttendanceList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objTrainingAttendance = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Attendance_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsTraining_Attendance_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Buttons"

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCourse.SelectedValue = 0
            txtRemark.Text = ""
            txtTotalAbsent.Text = ""
            txtTotalPresent.Text = ""
            dtpAttendanceDate.Checked = False
            dtpStartDate.Checked = False
            dtpEndDate.Checked = False
           
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvAttendanceList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Attendance List from the list to perform further operation on it."), enMsgBoxStyle.Information)
            lvAttendanceList.Select()
            Exit Sub
        End If

        Dim frm As New frmCourseAttendance_AddEdit
        Try

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAttendanceList.SelectedItems(0).Index

            If frm.displayDialog(CInt(lvAttendanceList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            frm = Nothing

            lvAttendanceList.Items(intSelectedIndex).Selected = True
            lvAttendanceList.EnsureVisible(intSelectedIndex)
            lvAttendanceList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAttendanceList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Attendance List from the list to perform further operation on it."), enMsgBoxStyle.Information)
            lvAttendanceList.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAttendanceList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Attendance List?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objTrainingAttendance._Isvoid = True
                objTrainingAttendance._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                objTrainingAttendance._Voiduserunkid = User._Object._Userunkid

                objTrainingAttendance.Delete(CInt(lvAttendanceList.SelectedItems(0).Tag))
                lvAttendanceList.SelectedItems(0).Remove()

                If lvAttendanceList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAttendanceList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAttendanceList.Items.Count - 1
                    lvAttendanceList.Items(intSelectedIndex).Selected = True
                    lvAttendanceList.EnsureVisible(intSelectedIndex)
                ElseIf lvAttendanceList.Items.Count <> 0 Then
                    lvAttendanceList.Items(intSelectedIndex).Selected = True
                    lvAttendanceList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAttendanceList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblTraningCourse.Text = Language._Object.getCaption(Me.lblTraningCourse.Name, Me.lblTraningCourse.Text)
			Me.lblAttendanceDate.Text = Language._Object.getCaption(Me.lblAttendanceDate.Name, Me.lblAttendanceDate.Text)
			Me.lblTotalPresent.Text = Language._Object.getCaption(Me.lblTotalPresent.Name, Me.lblTotalPresent.Text)
			Me.lblTotalAbsent.Text = Language._Object.getCaption(Me.lblTotalAbsent.Name, Me.lblTotalAbsent.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.colhCourse.Text = Language._Object.getCaption(CStr(Me.colhCourse.Tag), Me.colhCourse.Text)
			Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
			Me.colhAttendanceDate.Text = Language._Object.getCaption(CStr(Me.colhAttendanceDate.Tag), Me.colhAttendanceDate.Text)
			Me.colhPresent.Text = Language._Object.getCaption(CStr(Me.colhPresent.Tag), Me.colhPresent.Text)
			Me.colhAbsent.Text = Language._Object.getCaption(CStr(Me.colhAbsent.Tag), Me.colhAbsent.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Attendance List from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Attendance List?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class