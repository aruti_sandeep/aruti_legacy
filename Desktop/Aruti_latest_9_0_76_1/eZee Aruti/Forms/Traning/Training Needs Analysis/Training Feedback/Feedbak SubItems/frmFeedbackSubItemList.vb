﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFeedbackSubItemList

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmFeedbackSubItemList"
    Private objFeedbackSubItem As clshrtnafdbk_subitem_master

#End Region

#Region "Form's Event"

    Private Sub frmFeedbackSubItemList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objFeedbackSubItem = New clshrtnafdbk_subitem_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END


            Call FillCombo()

            If lvSubItemList.Items.Count > 0 Then lvSubItemList.Items(0).Selected = True

            lvSubItemList.Select()

            lvSubItemList.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackSubItemList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmFeedbackSubItemList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvSubItemList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackSubItemList_KeyUp", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmFeedbackSubItemList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objFeedbackSubItem = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackSubItemList_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnafdbk_subitem_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnafdbk_subitem_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objFeedBackItem As New clshrtnafdbk_item_master
        Dim objFeedBackGrp As New clshrtnafdbk_group_master
        Try
            dsCombo = objFeedBackItem.getComboList("List", True)
            With cboFeedbackItem
                .ValueMember = "fdbkitemunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objFeedBackGrp.getComboList("List", True)
            With cboFeedbackGroup
                .ValueMember = "fdbkgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objFeedBackItem = Nothing : objFeedBackGrp = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Dim dtTable As DataTable
        Try

            If User._Object.Privilege._AllowToViewEvaluationSubItemList = True Then                'Pinkal (09-Jul-2012) -- Start


            dsList = objFeedbackSubItem.GetList("List")
            lvSubItemList.Items.Clear()

            If CInt(cboFeedbackGroup.SelectedValue) > 0 Then
                StrSearch &= "AND fdbkgroupunkid = '" & CInt(cboFeedbackGroup.SelectedValue) & "' "
            End If

            If CInt(cboFeedbackItem.SelectedValue) > 0 Then
                StrSearch &= "AND fdbkitemunkid = '" & CInt(cboFeedbackItem.SelectedValue) & "' "
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                dtTable = New DataView(dsList.Tables("List"), StrSearch, "FeedBackItem", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("List"), "", "FeedBackItem", DataViewRowState.CurrentRows).ToTable
            End If

            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("FeedBackItem").ToString
                lvItem.SubItems.Add(dtRow.Item("code").ToString)
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.SubItems.Add(dtRow.Item("description").ToString)

                lvItem.Tag = dtRow.Item("fdbksubitemunkid")

                lvSubItemList.Items.Add(lvItem)
            Next

            If lvSubItemList.Items.Count > 2 Then
                colhDescription.Width = 200 - 20
            Else
                colhDescription.Width = 200
            End If

            lvSubItemList.GroupingColumn = objcolhFeedbackItem
            lvSubItemList.DisplayGroups(True)

            lvSubItemList.GridLines = False

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddEvaluationSubItems
            btnEdit.Enabled = User._Object.Privilege._AllowToEditEvaluationSubItems
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEvaluationSubItems
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmFeedbackSubItem_AddEdit

            If User._Object._Isrighttoleft = True Then
                ObjFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjFrm)
            End If

            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvSubItemList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvSubItemList.Select()
                Exit Sub
            End If
            Dim frm As New frmFeedbackSubItem_AddEdit
            Try
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If


                If frm.displayDialog(CInt(lvSubItemList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call Fill_List()
                End If
                frm = Nothing
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvSubItemList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSubItemList.Select()
            Exit Sub
        End If
        If objFeedbackSubItem.isUsed(CInt(lvSubItemList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Item. Reason: This Assess Item is in use."), enMsgBoxStyle.Information) '?2
            lvSubItemList.Select()
            Exit Sub
        End If
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Item?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objFeedbackSubItem.Delete(CInt(lvSubItemList.SelectedItems(0).Tag))
                lvSubItemList.SelectedItems(0).Remove()

                If lvSubItemList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
            lvSubItemList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboFeedbackItem.SelectedValue = 0
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchItems.Click
        Dim frm As New frmCommonSearch
        Try
            If CType(cboFeedbackItem.DataSource, DataTable) IsNot Nothing Then
                With frm
                    .ValueMember = cboFeedbackItem.ValueMember
                    .DisplayMember = cboFeedbackItem.DisplayMember
                    .DataSource = CType(cboFeedbackItem.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboFeedbackItem.SelectedValue = frm.SelectedValue
                    cboFeedbackItem.Focus()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchItems_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSearchFeedbackGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchFeedbackGrp.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboFeedbackGroup.ValueMember
                .DisplayMember = cboFeedbackGroup.DisplayMember
                .DataSource = CType(cboFeedbackGroup.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboFeedbackGroup.SelectedValue = frm.SelectedValue
                cboFeedbackGroup.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearchFeedbackGrp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboFeedbackGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFeedbackGroup.SelectedIndexChanged
        Dim dsCombo As New DataSet
        Dim objFeedBackItem As New clshrtnafdbk_item_master
        Try
            If CInt(cboFeedbackGroup.SelectedValue) > 0 Then
                dsCombo = objFeedBackItem.getComboList("List", True, CInt(cboFeedbackGroup.SelectedValue))
                With cboFeedbackItem
                    .ValueMember = "fdbkitemunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("List")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFeedbackGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.btnSearchFeedbackGrp.Text = Language._Object.getCaption(Me.btnSearchFeedbackGrp.Name, Me.btnSearchFeedbackGrp.Text)
			Me.lblFeedbackGroup.Text = Language._Object.getCaption(Me.lblFeedbackGroup.Name, Me.lblFeedbackGroup.Text)
			Me.lblFeedbackItem.Text = Language._Object.getCaption(Me.lblFeedbackItem.Name, Me.lblFeedbackItem.Text)
			Me.colhSubItemCode.Text = Language._Object.getCaption(CStr(Me.colhSubItemCode.Tag), Me.colhSubItemCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Item. Reason: This Assess Item is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Item?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class