﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCCAccountConfigurationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCCAccountConfigurationList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSetInactive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbTranAccountList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlTranAccountList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvAccount = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgColhBlank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCCid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhCCName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCCCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTrnHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAllocationByName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAllocationByID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccountCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccountName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodStatusID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShortname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShortname2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShortname3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhactiveinactive = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboActiveInactive = New System.Windows.Forms.ComboBox
        Me.lblactiveinactive = New System.Windows.Forms.Label
        Me.EZeeGradientButton1 = New eZee.Common.eZeeGradientButton
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.lblAllocatoion = New System.Windows.Forms.Label
        Me.EZeeGradientButton2 = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTransactionType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchAccount = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.lblCostcenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.cboTransactionType = New System.Windows.Forms.ComboBox
        Me.lblTransactionType = New System.Windows.Forms.Label
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.cboAccountName = New System.Windows.Forms.ComboBox
        Me.lblAccuntName = New System.Windows.Forms.Label
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbTranAccountList.SuspendLayout()
        Me.pnlTranAccountList.SuspendLayout()
        CType(Me.dgvAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbTranAccountList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(894, 569)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSetInactive)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 514)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(894, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSetInactive
        '
        Me.btnSetInactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSetInactive.BackColor = System.Drawing.Color.White
        Me.btnSetInactive.BackgroundImage = CType(resources.GetObject("btnSetInactive.BackgroundImage"), System.Drawing.Image)
        Me.btnSetInactive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSetInactive.BorderColor = System.Drawing.Color.Empty
        Me.btnSetInactive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSetInactive.FlatAppearance.BorderSize = 0
        Me.btnSetInactive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetInactive.ForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSetInactive.GradientForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetInactive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.Location = New System.Drawing.Point(15, 13)
        Me.btnSetInactive.Name = "btnSetInactive"
        Me.btnSetInactive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetInactive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.Size = New System.Drawing.Size(97, 30)
        Me.btnSetInactive.TabIndex = 4
        Me.btnSetInactive.Text = "&Set Inactive"
        Me.btnSetInactive.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(682, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(579, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(476, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(785, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbTranAccountList
        '
        Me.gbTranAccountList.BorderColor = System.Drawing.Color.Black
        Me.gbTranAccountList.Checked = False
        Me.gbTranAccountList.CollapseAllExceptThis = False
        Me.gbTranAccountList.CollapsedHoverImage = Nothing
        Me.gbTranAccountList.CollapsedNormalImage = Nothing
        Me.gbTranAccountList.CollapsedPressedImage = Nothing
        Me.gbTranAccountList.CollapseOnLoad = False
        Me.gbTranAccountList.Controls.Add(Me.pnlTranAccountList)
        Me.gbTranAccountList.ExpandedHoverImage = Nothing
        Me.gbTranAccountList.ExpandedNormalImage = Nothing
        Me.gbTranAccountList.ExpandedPressedImage = Nothing
        Me.gbTranAccountList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTranAccountList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTranAccountList.HeaderHeight = 25
        Me.gbTranAccountList.HeaderMessage = ""
        Me.gbTranAccountList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTranAccountList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTranAccountList.HeightOnCollapse = 0
        Me.gbTranAccountList.LeftTextSpace = 0
        Me.gbTranAccountList.Location = New System.Drawing.Point(12, 162)
        Me.gbTranAccountList.Name = "gbTranAccountList"
        Me.gbTranAccountList.OpenHeight = 300
        Me.gbTranAccountList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTranAccountList.ShowBorder = True
        Me.gbTranAccountList.ShowCheckBox = False
        Me.gbTranAccountList.ShowCollapseButton = False
        Me.gbTranAccountList.ShowDefaultBorderColor = True
        Me.gbTranAccountList.ShowDownButton = False
        Me.gbTranAccountList.ShowHeader = True
        Me.gbTranAccountList.Size = New System.Drawing.Size(870, 338)
        Me.gbTranAccountList.TabIndex = 1
        Me.gbTranAccountList.Temp = 0
        Me.gbTranAccountList.Text = "Transaction Head / Account Info"
        Me.gbTranAccountList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTranAccountList
        '
        Me.pnlTranAccountList.Controls.Add(Me.objchkSelectAll)
        Me.pnlTranAccountList.Controls.Add(Me.dgvAccount)
        Me.pnlTranAccountList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlTranAccountList.Location = New System.Drawing.Point(3, 28)
        Me.pnlTranAccountList.Name = "pnlTranAccountList"
        Me.pnlTranAccountList.Size = New System.Drawing.Size(864, 278)
        Me.pnlTranAccountList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(10, 6)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 128
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvAccount
        '
        Me.dgvAccount.AllowUserToAddRows = False
        Me.dgvAccount.AllowUserToDeleteRows = False
        Me.dgvAccount.AllowUserToResizeRows = False
        Me.dgvAccount.BackgroundColor = System.Drawing.Color.White
        Me.dgvAccount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAccount.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgColhBlank, Me.objdgcolhID, Me.objdgcolhCCid, Me.dgColhCCName, Me.objdgcolhCCCode, Me.dgcolhTranType, Me.dgcolhTrnHead, Me.dgcolhAllocationByName, Me.objdgcolhAllocationByID, Me.dgcolhAccountCode, Me.dgcolhAccountName, Me.dgcolhPeriod, Me.objdgcolhPeriodStatusID, Me.dgcolhShortname, Me.dgcolhShortname2, Me.dgcolhShortname3, Me.objdgcolhIsGroup, Me.colhactiveinactive})
        Me.dgvAccount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAccount.Location = New System.Drawing.Point(0, 0)
        Me.dgvAccount.Name = "dgvAccount"
        Me.dgvAccount.RowHeadersVisible = False
        Me.dgvAccount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAccount.Size = New System.Drawing.Size(864, 278)
        Me.dgvAccount.TabIndex = 127
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 30
        '
        'objdgColhBlank
        '
        Me.objdgColhBlank.Frozen = True
        Me.objdgColhBlank.HeaderText = ""
        Me.objdgColhBlank.Name = "objdgColhBlank"
        Me.objdgColhBlank.ReadOnly = True
        Me.objdgColhBlank.Width = 30
        '
        'objdgcolhID
        '
        Me.objdgcolhID.Frozen = True
        Me.objdgcolhID.HeaderText = "ID"
        Me.objdgcolhID.Name = "objdgcolhID"
        Me.objdgcolhID.ReadOnly = True
        Me.objdgcolhID.Visible = False
        Me.objdgcolhID.Width = 5
        '
        'objdgcolhCCid
        '
        Me.objdgcolhCCid.Frozen = True
        Me.objdgcolhCCid.HeaderText = "objdgcolhEmpid"
        Me.objdgcolhCCid.Name = "objdgcolhCCid"
        Me.objdgcolhCCid.ReadOnly = True
        Me.objdgcolhCCid.Visible = False
        '
        'dgColhCCName
        '
        Me.dgColhCCName.Frozen = True
        Me.dgColhCCName.HeaderText = "Employee"
        Me.dgColhCCName.Name = "dgColhCCName"
        Me.dgColhCCName.ReadOnly = True
        Me.dgColhCCName.Width = 5
        '
        'objdgcolhCCCode
        '
        Me.objdgcolhCCCode.Frozen = True
        Me.objdgcolhCCCode.HeaderText = "emp code"
        Me.objdgcolhCCCode.Name = "objdgcolhCCCode"
        Me.objdgcolhCCCode.ReadOnly = True
        Me.objdgcolhCCCode.Width = 5
        '
        'dgcolhTranType
        '
        Me.dgcolhTranType.Frozen = True
        Me.dgcolhTranType.HeaderText = "Transaction Type"
        Me.dgcolhTranType.Name = "dgcolhTranType"
        Me.dgcolhTranType.ReadOnly = True
        Me.dgcolhTranType.Width = 150
        '
        'dgcolhTrnHead
        '
        Me.dgcolhTrnHead.Frozen = True
        Me.dgcolhTrnHead.HeaderText = "Transaction Head"
        Me.dgcolhTrnHead.Name = "dgcolhTrnHead"
        Me.dgcolhTrnHead.ReadOnly = True
        Me.dgcolhTrnHead.Width = 180
        '
        'dgcolhAllocationByName
        '
        Me.dgcolhAllocationByName.Frozen = True
        Me.dgcolhAllocationByName.HeaderText = "Allocation By Name"
        Me.dgcolhAllocationByName.Name = "dgcolhAllocationByName"
        Me.dgcolhAllocationByName.ReadOnly = True
        Me.dgcolhAllocationByName.Width = 150
        '
        'objdgcolhAllocationByID
        '
        Me.objdgcolhAllocationByID.Frozen = True
        Me.objdgcolhAllocationByID.HeaderText = "objdgcolhAllocationByID"
        Me.objdgcolhAllocationByID.Name = "objdgcolhAllocationByID"
        Me.objdgcolhAllocationByID.ReadOnly = True
        Me.objdgcolhAllocationByID.Visible = False
        '
        'dgcolhAccountCode
        '
        Me.dgcolhAccountCode.Frozen = True
        Me.dgcolhAccountCode.HeaderText = "Acc. Code"
        Me.dgcolhAccountCode.Name = "dgcolhAccountCode"
        Me.dgcolhAccountCode.ReadOnly = True
        Me.dgcolhAccountCode.Width = 70
        '
        'dgcolhAccountName
        '
        Me.dgcolhAccountName.Frozen = True
        Me.dgcolhAccountName.HeaderText = "Account Name"
        Me.dgcolhAccountName.Name = "dgcolhAccountName"
        Me.dgcolhAccountName.ReadOnly = True
        Me.dgcolhAccountName.Width = 180
        '
        'dgcolhPeriod
        '
        Me.dgcolhPeriod.Frozen = True
        Me.dgcolhPeriod.HeaderText = "Period"
        Me.dgcolhPeriod.Name = "dgcolhPeriod"
        Me.dgcolhPeriod.ReadOnly = True
        '
        'objdgcolhPeriodStatusID
        '
        Me.objdgcolhPeriodStatusID.Frozen = True
        Me.objdgcolhPeriodStatusID.HeaderText = "Period Status"
        Me.objdgcolhPeriodStatusID.Name = "objdgcolhPeriodStatusID"
        Me.objdgcolhPeriodStatusID.ReadOnly = True
        Me.objdgcolhPeriodStatusID.Visible = False
        '
        'dgcolhShortname
        '
        Me.dgcolhShortname.Frozen = True
        Me.dgcolhShortname.HeaderText = "Short Name"
        Me.dgcolhShortname.Name = "dgcolhShortname"
        Me.dgcolhShortname.ReadOnly = True
        Me.dgcolhShortname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhShortname.Width = 90
        '
        'dgcolhShortname2
        '
        Me.dgcolhShortname2.Frozen = True
        Me.dgcolhShortname2.HeaderText = "Short Name 2"
        Me.dgcolhShortname2.Name = "dgcolhShortname2"
        Me.dgcolhShortname2.ReadOnly = True
        Me.dgcolhShortname2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhShortname2.Width = 90
        '
        'dgcolhShortname3
        '
        Me.dgcolhShortname3.Frozen = True
        Me.dgcolhShortname3.HeaderText = "Short Name 3"
        Me.dgcolhShortname3.Name = "dgcolhShortname3"
        Me.dgcolhShortname3.ReadOnly = True
        Me.dgcolhShortname3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhShortname3.Width = 90
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.Frozen = True
        Me.objdgcolhIsGroup.HeaderText = "grp"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Visible = False
        '
        'colhactiveinactive
        '
        Me.colhactiveinactive.Frozen = True
        Me.colhactiveinactive.HeaderText = "Inactive Status"
        Me.colhactiveinactive.Name = "colhactiveinactive"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.lblactiveinactive)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeGradientButton2)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTransactionType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAccount)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.lblCostcenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboTransactionType)
        Me.gbFilterCriteria.Controls.Add(Me.lblTransactionType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboAccountName)
        Me.gbFilterCriteria.Controls.Add(Me.lblAccuntName)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeGradientButton1)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocatoion)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(870, 90)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboActiveInactive
        '
        Me.cboActiveInactive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActiveInactive.FormattingEnabled = True
        Me.cboActiveInactive.Location = New System.Drawing.Point(424, 60)
        Me.cboActiveInactive.Name = "cboActiveInactive"
        Me.cboActiveInactive.Size = New System.Drawing.Size(137, 21)
        Me.cboActiveInactive.TabIndex = 118
        '
        'lblactiveinactive
        '
        Me.lblactiveinactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblactiveinactive.Location = New System.Drawing.Point(307, 63)
        Me.lblactiveinactive.Name = "lblactiveinactive"
        Me.lblactiveinactive.Size = New System.Drawing.Size(111, 15)
        Me.lblactiveinactive.TabIndex = 119
        Me.lblactiveinactive.Text = "Inactive Status"
        '
        'EZeeGradientButton1
        '
        Me.EZeeGradientButton1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor1 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor2 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EZeeGradientButton1.BorderSelected = False
        Me.EZeeGradientButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.EZeeGradientButton1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.EZeeGradientButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EZeeGradientButton1.Location = New System.Drawing.Point(360, 44)
        Me.EZeeGradientButton1.Name = "EZeeGradientButton1"
        Me.EZeeGradientButton1.Size = New System.Drawing.Size(21, 21)
        Me.EZeeGradientButton1.TabIndex = 110
        Me.EZeeGradientButton1.Visible = False
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(329, 44)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(25, 21)
        Me.cboAllocation.TabIndex = 105
        Me.cboAllocation.Visible = False
        '
        'lblAllocatoion
        '
        Me.lblAllocatoion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocatoion.Location = New System.Drawing.Point(290, 47)
        Me.lblAllocatoion.Name = "lblAllocatoion"
        Me.lblAllocatoion.Size = New System.Drawing.Size(33, 15)
        Me.lblAllocatoion.TabIndex = 109
        Me.lblAllocatoion.Text = "Allocatoion"
        Me.lblAllocatoion.Visible = False
        '
        'EZeeGradientButton2
        '
        Me.EZeeGradientButton2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeGradientButton2.BackColor1 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton2.BackColor2 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EZeeGradientButton2.BorderSelected = False
        Me.EZeeGradientButton2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.EZeeGradientButton2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.EZeeGradientButton2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EZeeGradientButton2.Location = New System.Drawing.Point(567, 33)
        Me.EZeeGradientButton2.Name = "EZeeGradientButton2"
        Me.EZeeGradientButton2.Size = New System.Drawing.Size(21, 21)
        Me.EZeeGradientButton2.TabIndex = 108
        Me.EZeeGradientButton2.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(304, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(114, 15)
        Me.lblPeriod.TabIndex = 107
        Me.lblPeriod.Text = "Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(424, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(137, 21)
        Me.cboPeriod.TabIndex = 106
        '
        'objbtnSearchTransactionType
        '
        Me.objbtnSearchTransactionType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTransactionType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTransactionType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTransactionType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTransactionType.BorderSelected = False
        Me.objbtnSearchTransactionType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTransactionType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTransactionType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTransactionType.Location = New System.Drawing.Point(271, 33)
        Me.objbtnSearchTransactionType.Name = "objbtnSearchTransactionType"
        Me.objbtnSearchTransactionType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTransactionType.TabIndex = 103
        '
        'objbtnSearchAccount
        '
        Me.objbtnSearchAccount.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAccount.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccount.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccount.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAccount.BorderSelected = False
        Me.objbtnSearchAccount.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAccount.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAccount.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAccount.Location = New System.Drawing.Point(843, 57)
        Me.objbtnSearchAccount.Name = "objbtnSearchAccount"
        Me.objbtnSearchAccount.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAccount.TabIndex = 101
        '
        'objbtnSearchCostCenter
        '
        Me.objbtnSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCostCenter.BorderSelected = False
        Me.objbtnSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCostCenter.Location = New System.Drawing.Point(843, 30)
        Me.objbtnSearchCostCenter.Name = "objbtnSearchCostCenter"
        Me.objbtnSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCostCenter.TabIndex = 99
        '
        'lblCostcenter
        '
        Me.lblCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostcenter.Location = New System.Drawing.Point(595, 33)
        Me.lblCostcenter.Name = "lblCostcenter"
        Me.lblCostcenter.Size = New System.Drawing.Size(104, 15)
        Me.lblCostcenter.TabIndex = 98
        Me.lblCostcenter.Text = "Cost Center"
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(705, 30)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(132, 21)
        Me.cboCostCenter.TabIndex = 97
        '
        'cboTransactionType
        '
        Me.cboTransactionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionType.FormattingEnabled = True
        Me.cboTransactionType.Location = New System.Drawing.Point(128, 33)
        Me.cboTransactionType.Name = "cboTransactionType"
        Me.cboTransactionType.Size = New System.Drawing.Size(137, 21)
        Me.cboTransactionType.TabIndex = 0
        '
        'lblTransactionType
        '
        Me.lblTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionType.Location = New System.Drawing.Point(8, 36)
        Me.lblTransactionType.Name = "lblTransactionType"
        Me.lblTransactionType.Size = New System.Drawing.Size(114, 15)
        Me.lblTransactionType.TabIndex = 95
        Me.lblTransactionType.Text = "Transaction Type"
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(271, 60)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 93
        '
        'cboAccountName
        '
        Me.cboAccountName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountName.FormattingEnabled = True
        Me.cboAccountName.Location = New System.Drawing.Point(705, 57)
        Me.cboAccountName.Name = "cboAccountName"
        Me.cboAccountName.Size = New System.Drawing.Size(132, 21)
        Me.cboAccountName.TabIndex = 2
        '
        'lblAccuntName
        '
        Me.lblAccuntName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccuntName.Location = New System.Drawing.Point(595, 60)
        Me.lblAccuntName.Name = "lblAccuntName"
        Me.lblAccuntName.Size = New System.Drawing.Size(104, 15)
        Me.lblAccuntName.TabIndex = 90
        Me.lblAccuntName.Text = "Account Name"
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 63)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(114, 15)
        Me.lblTrnHead.TabIndex = 74
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(128, 60)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(137, 21)
        Me.cboTrnHead.TabIndex = 1
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(843, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(819, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(894, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Cost Center Account Configuration List"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 30
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        Me.DataGridViewTextBoxColumn2.Width = 5
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpid"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 5
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "emp code"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 5
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Transaction Type"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Transaction Head"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 180
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Allocation ByName"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhAllocationByID"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Acc. Code"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Account Name"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 180
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Period"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Period Status"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Short Name"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Width = 90
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Short Name 2"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Width = 90
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Short Name 3"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Width = 90
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "grp"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'frmCCAccountConfigurationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 569)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCCAccountConfigurationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cost Center Account Configuration List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbTranAccountList.ResumeLayout(False)
        Me.pnlTranAccountList.ResumeLayout(False)
        Me.pnlTranAccountList.PerformLayout()
        CType(Me.dgvAccount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbTranAccountList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlTranAccountList As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboAccountName As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccuntName As System.Windows.Forms.Label
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTransactionType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCostcenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchAccount As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTransactionType As eZee.Common.eZeeGradientButton
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAccount As System.Windows.Forms.DataGridView
    Friend WithEvents EZeeGradientButton1 As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocatoion As System.Windows.Forms.Label
    Friend WithEvents EZeeGradientButton2 As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSetInactive As eZee.Common.eZeeLightButton
    Friend WithEvents cboActiveInactive As System.Windows.Forms.ComboBox
    Friend WithEvents lblactiveinactive As System.Windows.Forms.Label
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgColhBlank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCCid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhCCName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCCCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTrnHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAllocationByName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAllocationByID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccountCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccountName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodStatusID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShortname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShortname2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShortname3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhactiveinactive As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
