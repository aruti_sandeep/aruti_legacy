﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportAccount
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportAccount))
        Me.eWizAccount = New eZee.Common.eZeeWizard
        Me.ewpImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuShowSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCodeExist = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNameExist = New System.Windows.Forms.ToolStripMenuItem
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.ewpGrpMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbAccGroupMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvMapping = New System.Windows.Forms.DataGridView
        Me.ewpMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.lblAccountName = New System.Windows.Forms.Label
        Me.cboAccountName = New System.Windows.Forms.ComboBox
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.lblAccountGroup = New System.Windows.Forms.Label
        Me.cboAccountGroup = New System.Windows.Forms.ComboBox
        Me.cboAccountCode = New System.Windows.Forms.ComboBox
        Me.lblAccountCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.ewpFileSelection = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFileAccGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhDBAccGroup = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objcolhSearch = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhAccCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccGrp = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.eWizAccount.SuspendLayout()
        Me.ewpImporting.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.ewpGrpMapping.SuspendLayout()
        Me.gbAccGroupMapping.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ewpMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.ewpFileSelection.SuspendLayout()
        Me.SuspendLayout()
        '
        'eWizAccount
        '
        Me.eWizAccount.Controls.Add(Me.ewpImporting)
        Me.eWizAccount.Controls.Add(Me.ewpGrpMapping)
        Me.eWizAccount.Controls.Add(Me.ewpMapping)
        Me.eWizAccount.Controls.Add(Me.ewpFileSelection)
        Me.eWizAccount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eWizAccount.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eWizAccount.Location = New System.Drawing.Point(0, 0)
        Me.eWizAccount.Name = "eWizAccount"
        Me.eWizAccount.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.ewpFileSelection, Me.ewpMapping, Me.ewpGrpMapping, Me.ewpImporting})
        Me.eWizAccount.SaveEnabled = True
        Me.eWizAccount.SaveText = "Save && Finish"
        Me.eWizAccount.SaveVisible = False
        Me.eWizAccount.SetSaveIndexBeforeFinishIndex = False
        Me.eWizAccount.Size = New System.Drawing.Size(587, 372)
        Me.eWizAccount.TabIndex = 2
        Me.eWizAccount.WelcomeImage = Nothing
        '
        'ewpImporting
        '
        Me.ewpImporting.Controls.Add(Me.btnExport)
        Me.ewpImporting.Controls.Add(Me.chkSelectAll)
        Me.ewpImporting.Controls.Add(Me.btnImport)
        Me.ewpImporting.Controls.Add(Me.btnFilter)
        Me.ewpImporting.Controls.Add(Me.dgData)
        Me.ewpImporting.Controls.Add(Me.pnlInfo)
        Me.ewpImporting.Controls.Add(Me.objbuttonBack)
        Me.ewpImporting.Controls.Add(Me.objbuttonCancel)
        Me.ewpImporting.Controls.Add(Me.objbuttonNext)
        Me.ewpImporting.Location = New System.Drawing.Point(0, 0)
        Me.ewpImporting.Name = "ewpImporting"
        Me.ewpImporting.Size = New System.Drawing.Size(587, 324)
        Me.ewpImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.ewpImporting.TabIndex = 10
        Me.ewpImporting.Tag = "ewpImporting"
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(120, 288)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 102
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(17, 74)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 24
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(478, 288)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 23
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(7, 288)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmnuOperations
        Me.btnFilter.TabIndex = 22
        Me.btnFilter.Text = "Filter"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuShowSuccessful, Me.mnuCodeExist, Me.mnuNameExist})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(180, 70)
        '
        'mnuShowSuccessful
        '
        Me.mnuShowSuccessful.Name = "mnuShowSuccessful"
        Me.mnuShowSuccessful.Size = New System.Drawing.Size(179, 22)
        Me.mnuShowSuccessful.Text = "Show Successful Data"
        '
        'mnuCodeExist
        '
        Me.mnuCodeExist.Name = "mnuCodeExist"
        Me.mnuCodeExist.Size = New System.Drawing.Size(179, 22)
        Me.mnuCodeExist.Text = "Code Already Exist"
        '
        'mnuNameExist
        '
        Me.mnuNameExist.Name = "mnuNameExist"
        Me.mnuNameExist.Size = New System.Drawing.Size(179, 22)
        Me.mnuNameExist.Text = "Name Already Exist"
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhAccCode, Me.dgcolhAccName, Me.dgcolhAccGrp})
        Me.dgData.Location = New System.Drawing.Point(7, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(572, 213)
        Me.dgData.TabIndex = 21
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Location = New System.Drawing.Point(7, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(572, 51)
        Me.pnlInfo.TabIndex = 4
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(335, 336)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(503, 336)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(419, 336)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'ewpGrpMapping
        '
        Me.ewpGrpMapping.Controls.Add(Me.gbAccGroupMapping)
        Me.ewpGrpMapping.Location = New System.Drawing.Point(0, 0)
        Me.ewpGrpMapping.Name = "ewpGrpMapping"
        Me.ewpGrpMapping.Size = New System.Drawing.Size(587, 324)
        Me.ewpGrpMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpGrpMapping.TabIndex = 9
        Me.ewpGrpMapping.Tag = "ewpGrpMapping"
        '
        'gbAccGroupMapping
        '
        Me.gbAccGroupMapping.BorderColor = System.Drawing.Color.Black
        Me.gbAccGroupMapping.Checked = False
        Me.gbAccGroupMapping.CollapseAllExceptThis = False
        Me.gbAccGroupMapping.CollapsedHoverImage = Nothing
        Me.gbAccGroupMapping.CollapsedNormalImage = Nothing
        Me.gbAccGroupMapping.CollapsedPressedImage = Nothing
        Me.gbAccGroupMapping.CollapseOnLoad = False
        Me.gbAccGroupMapping.Controls.Add(Me.Panel1)
        Me.gbAccGroupMapping.ExpandedHoverImage = Nothing
        Me.gbAccGroupMapping.ExpandedNormalImage = Nothing
        Me.gbAccGroupMapping.ExpandedPressedImage = Nothing
        Me.gbAccGroupMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAccGroupMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAccGroupMapping.HeaderHeight = 25
        Me.gbAccGroupMapping.HeaderMessage = ""
        Me.gbAccGroupMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAccGroupMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAccGroupMapping.HeightOnCollapse = 0
        Me.gbAccGroupMapping.LeftTextSpace = 0
        Me.gbAccGroupMapping.Location = New System.Drawing.Point(166, 0)
        Me.gbAccGroupMapping.Name = "gbAccGroupMapping"
        Me.gbAccGroupMapping.OpenHeight = 300
        Me.gbAccGroupMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAccGroupMapping.ShowBorder = True
        Me.gbAccGroupMapping.ShowCheckBox = False
        Me.gbAccGroupMapping.ShowCollapseButton = False
        Me.gbAccGroupMapping.ShowDefaultBorderColor = True
        Me.gbAccGroupMapping.ShowDownButton = False
        Me.gbAccGroupMapping.ShowHeader = True
        Me.gbAccGroupMapping.Size = New System.Drawing.Size(421, 322)
        Me.gbAccGroupMapping.TabIndex = 1
        Me.gbAccGroupMapping.Temp = 0
        Me.gbAccGroupMapping.Text = "Account Group Mapping"
        Me.gbAccGroupMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvMapping)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(13, 39)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(396, 274)
        Me.Panel1.TabIndex = 28
        '
        'dgvMapping
        '
        Me.dgvMapping.AllowUserToAddRows = False
        Me.dgvMapping.AllowUserToDeleteRows = False
        Me.dgvMapping.AllowUserToResizeRows = False
        Me.dgvMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMapping.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhFileAccGroup, Me.colhDBAccGroup, Me.objcolhSearch})
        Me.dgvMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMapping.Location = New System.Drawing.Point(0, 0)
        Me.dgvMapping.Name = "dgvMapping"
        Me.dgvMapping.RowHeadersVisible = False
        Me.dgvMapping.Size = New System.Drawing.Size(396, 274)
        Me.dgvMapping.TabIndex = 0
        '
        'ewpMapping
        '
        Me.ewpMapping.Controls.Add(Me.gbFiledMapping)
        Me.ewpMapping.Location = New System.Drawing.Point(0, 0)
        Me.ewpMapping.Name = "ewpMapping"
        Me.ewpMapping.Size = New System.Drawing.Size(587, 324)
        Me.ewpMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpMapping.TabIndex = 8
        Me.ewpMapping.Tag = "ewpMapping"
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.objlblSign2)
        Me.gbFiledMapping.Controls.Add(Me.lblAccountName)
        Me.gbFiledMapping.Controls.Add(Me.cboAccountName)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign3)
        Me.gbFiledMapping.Controls.Add(Me.lblAccountGroup)
        Me.gbFiledMapping.Controls.Add(Me.cboAccountGroup)
        Me.gbFiledMapping.Controls.Add(Me.cboAccountCode)
        Me.gbFiledMapping.Controls.Add(Me.lblAccountCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(166, 1)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(421, 322)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(55, 161)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign2.TabIndex = 4
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblAccountName
        '
        Me.lblAccountName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountName.Location = New System.Drawing.Point(78, 161)
        Me.lblAccountName.Name = "lblAccountName"
        Me.lblAccountName.Size = New System.Drawing.Size(119, 17)
        Me.lblAccountName.TabIndex = 5
        Me.lblAccountName.Text = "Account Name"
        Me.lblAccountName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAccountName
        '
        Me.cboAccountName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountName.FormattingEnabled = True
        Me.cboAccountName.Location = New System.Drawing.Point(203, 159)
        Me.cboAccountName.Name = "cboAccountName"
        Me.cboAccountName.Size = New System.Drawing.Size(141, 21)
        Me.cboAccountName.TabIndex = 6
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(55, 190)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign3.TabIndex = 10
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblAccountGroup
        '
        Me.lblAccountGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountGroup.Location = New System.Drawing.Point(78, 188)
        Me.lblAccountGroup.Name = "lblAccountGroup"
        Me.lblAccountGroup.Size = New System.Drawing.Size(119, 17)
        Me.lblAccountGroup.TabIndex = 11
        Me.lblAccountGroup.Text = "Account Group"
        Me.lblAccountGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAccountGroup
        '
        Me.cboAccountGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountGroup.FormattingEnabled = True
        Me.cboAccountGroup.Location = New System.Drawing.Point(203, 186)
        Me.cboAccountGroup.Name = "cboAccountGroup"
        Me.cboAccountGroup.Size = New System.Drawing.Size(141, 21)
        Me.cboAccountGroup.TabIndex = 12
        '
        'cboAccountCode
        '
        Me.cboAccountCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountCode.FormattingEnabled = True
        Me.cboAccountCode.Location = New System.Drawing.Point(203, 132)
        Me.cboAccountCode.Name = "cboAccountCode"
        Me.cboAccountCode.Size = New System.Drawing.Size(141, 21)
        Me.cboAccountCode.TabIndex = 3
        '
        'lblAccountCode
        '
        Me.lblAccountCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountCode.Location = New System.Drawing.Point(78, 134)
        Me.lblAccountCode.Name = "lblAccountCode"
        Me.lblAccountCode.Size = New System.Drawing.Size(119, 17)
        Me.lblAccountCode.TabIndex = 2
        Me.lblAccountCode.Text = "Account Code"
        Me.lblAccountCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(55, 134)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign1.TabIndex = 1
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(184, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(234, 19)
        Me.lblCaption.TabIndex = 0
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ewpFileSelection
        '
        Me.ewpFileSelection.Controls.Add(Me.btnOpenFile)
        Me.ewpFileSelection.Controls.Add(Me.txtFilePath)
        Me.ewpFileSelection.Controls.Add(Me.lblSelectfile)
        Me.ewpFileSelection.Controls.Add(Me.lblTitle)
        Me.ewpFileSelection.Location = New System.Drawing.Point(0, 0)
        Me.ewpFileSelection.Name = "ewpFileSelection"
        Me.ewpFileSelection.Size = New System.Drawing.Size(587, 324)
        Me.ewpFileSelection.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpFileSelection.TabIndex = 7
        Me.ewpFileSelection.Tag = "ewpFileSelection"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(533, 149)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(28, 20)
        Me.btnOpenFile.TabIndex = 24
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(176, 149)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(351, 21)
        Me.txtFilePath.TabIndex = 23
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 126)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(143, 17)
        Me.lblSelectfile.TabIndex = 22
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(172, 20)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(403, 23)
        Me.lblTitle.TabIndex = 19
        Me.lblTitle.Text = "Account Import Wizard"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Account Group From File"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Account Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Account Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objdgcolhEmployeeId"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objcolhstatus"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'colhFileAccGroup
        '
        Me.colhFileAccGroup.HeaderText = "Account Group From File"
        Me.colhFileAccGroup.Name = "colhFileAccGroup"
        Me.colhFileAccGroup.ReadOnly = True
        Me.colhFileAccGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFileAccGroup.Width = 150
        '
        'colhDBAccGroup
        '
        Me.colhDBAccGroup.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhDBAccGroup.HeaderText = "Account Group From Database"
        Me.colhDBAccGroup.Name = "colhDBAccGroup"
        Me.colhDBAccGroup.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'objcolhSearch
        '
        Me.objcolhSearch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        Me.objcolhSearch.HeaderText = ""
        Me.objcolhSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objcolhSearch.Name = "objcolhSearch"
        Me.objcolhSearch.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhSearch.Width = 5
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objdgcolhCheck.Width = 30
        '
        'dgcolhAccCode
        '
        Me.dgcolhAccCode.HeaderText = "Account Code"
        Me.dgcolhAccCode.Name = "dgcolhAccCode"
        Me.dgcolhAccCode.ReadOnly = True
        Me.dgcolhAccCode.Width = 175
        '
        'dgcolhAccName
        '
        Me.dgcolhAccName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhAccName.HeaderText = "Account Name"
        Me.dgcolhAccName.Name = "dgcolhAccName"
        Me.dgcolhAccName.ReadOnly = True
        '
        'dgcolhAccGrp
        '
        Me.dgcolhAccGrp.HeaderText = "Acccount Group"
        Me.dgcolhAccGrp.Name = "dgcolhAccGrp"
        Me.dgcolhAccGrp.ReadOnly = True
        Me.dgcolhAccGrp.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAccGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgcolhAccGrp.Width = 175
        '
        'frmImportAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(587, 372)
        Me.Controls.Add(Me.eWizAccount)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportAccount"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Account"
        Me.eWizAccount.ResumeLayout(False)
        Me.ewpImporting.ResumeLayout(False)
        Me.ewpImporting.PerformLayout()
        Me.cmnuOperations.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.ewpGrpMapping.ResumeLayout(False)
        Me.gbAccGroupMapping.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ewpMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.ewpFileSelection.ResumeLayout(False)
        Me.ewpFileSelection.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eWizAccount As eZee.Common.eZeeWizard
    Friend WithEvents ewpFileSelection As eZee.Common.eZeeWizardPage
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents ewpImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents ewpMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents lblAccountName As System.Windows.Forms.Label
    Friend WithEvents cboAccountName As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents lblAccountGroup As System.Windows.Forms.Label
    Friend WithEvents cboAccountGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboAccountCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents ewpGrpMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbAccGroupMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvMapping As System.Windows.Forms.DataGridView
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuShowSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCodeExist As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNameExist As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents colhFileAccGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhDBAccGroup As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objcolhSearch As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhAccCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccGrp As System.Windows.Forms.DataGridViewComboBoxColumn
End Class
