﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportEmpAccConfigMapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmImportEmpAccConfigMapping"
    Private dsData As New DataSet
    Private mblnCancel As Boolean = True
    Private dtTable As DataTable
    Private dtColumns As DataTable
    Dim objEmp As clsEmployee_Master
    'Sohail (25 Jul 2020) -- Start
    'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    'Sohail (25 Jul 2020) -- End

#End Region

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

    'Sohail (25 Jul 2020) -- Start
    'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
    Public ReadOnly Property _Period_StartDate() As Date
        Get
            Return mdtPeriodStart
        End Get
    End Property

    Public ReadOnly Property _Period_EndDate() As Date
        Get
            Return mdtPeriodEnd
        End Get
    End Property
    'Sohail (25 Jul 2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
        Try
            dsData = dsList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployeeCode.BackColor = GUI.ColorComp
            cboTransactionType.BackColor = GUI.ColorComp
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            cboPeriod.BackColor = GUI.ColorComp
            'Sohail (03 Jul 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    Private Sub FilCombo()
        Dim dsCombo As New DataSet
        'Dim dtTable As DataTable
        Dim objMaster As New clsMasterData
        'Sohail (03 Jul 2020) -- Start
        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
        Dim objPeriod As New clscommom_period_Tran
        Dim mintFirstPeriodID As Integer = 0
        'Sohail (03 Jul 2020) -- End
        Try

            'dsCombo = objMaster.getComboListJVTransactionType("HeadType")
            'dtTable = New DataView(dsCombo.Tables("HeadType"), "Id <> " & enJVTransactionType.PAY_PER_ACTIVITY & " ", "", DataViewRowState.CurrentRows).ToTable
            'With cboTransactionType
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dtTable
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With

            dtColumns = New DataTable
            dtColumns.Columns.Add("id", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtColumns.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""

            Dim dr As DataRow
            dr = dtColumns.NewRow
            dr.Item("id") = -1
            dr.Item("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dtColumns.Rows.Add(dr)

            For Each dtCol As DataColumn In dsData.Tables(0).Columns
                dr = dtColumns.NewRow

                dr.Item("id") = dtCol.Ordinal
                dr.Item("name") = dtCol.ColumnName

                dtColumns.Rows.Add(dr)
            Next

            With cboHeadCode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With

            With cboTransactionType
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With

            With cboEmployeeCode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With

            With cboAccountCode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With

            With cboShortName
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With

            With cboShortName2
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With

            'Sohail (03 Mar 2020) -- Start
            'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
            With cboShortName3
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With
            'Sohail (03 Mar 2020) -- End

            dsCombo = objMaster.getComboListJVEmpConfigRefCode(True, "RefCode")
            With cboMapRefCode
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("RefCode")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListJVEmpConfigRefName(True, "RefName")
            With cboMapRefName
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("RefName")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            mintFirstPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = mintFirstPeriodID
            End With
            'Sohail (03 Jul 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FiilCombo", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub SetData()
        Try
            dtTable = New DataTable("EmpAcc")

            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("accountconfigempunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            dtTable.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("period_code", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("period_code").Caption = "Period Code"
            dtTable.Columns.Add("period_name", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("period_name").Caption = "Period Name"
            'Sohail (03 Jul 2020) -- End
            dtTable.Columns.Add("transactiontype_Id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("transactiontype_name", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("headunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("headcode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("headcode").Caption = "Head Code"
            dtTable.Columns.Add("headname", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("headname").Caption = "Head Name"
            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("employeecode").Caption = "Employee Code"
            dtTable.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("employeename").Caption = "Employee Name"
            dtTable.Columns.Add("accountunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("accountcode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("accountcode").Caption = "Account Code"
            dtTable.Columns.Add("accountname", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("accountname").Caption = "Account Name"
            dtTable.Columns.Add("referencecodeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("referencenameid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("referencetypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("shortname", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("shortname2", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("shortname3", System.Type.GetType("System.String")).DefaultValue = "" 'Sohail (03 Mar 2020)
            'Hemant (17 Nov 2020) -- Start
            'Issue : AH-1643 Error Importing Employee account configurations
            dtTable.Columns.Add("usedefaultmapping", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Hemant (17 Nov 2020) -- End

            dtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("Message", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

    Private Sub AutoMap(ByVal lbl As Label, ByVal cbo As ComboBox)
        Try
            Dim dTable As DataTable = dtColumns

            Dim rw As DataRow = (From p In dTable Where (p.Item("name").ToString.ToUpper Like "*" & lbl.Text.Replace(" ", "").ToUpper & "*") Select (p)).FirstOrDefault
            If rw IsNot Nothing Then
                Dim combos As IEnumerable(Of ComboBox) = (From p In gbFieldMapping.Controls.OfType(Of ComboBox)() Where (CInt(p.SelectedValue) = CInt(rw.Item("id")) AndAlso p.Name <> cboMapRefCode.Name AndAlso p.Name <> cboMapRefName.Name AndAlso p.Name <> cboMapRefType.Name AndAlso p.Name <> cbo.Name) Select (p))
                For Each cb In combos
                    cb.SelectedValue = -1
                Next
                cbo.SelectedValue = -1
                cbo.SelectedValue = CInt(rw.Item("id"))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AutoMap", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmImportEmpAccConfigMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportEmpAccConfigMapping_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportEmpAccConfigMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmp = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FilCombo()

            Call SetData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportEmpAccConfigMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsaccountconfig_employee.SetMessages()
            objfrm._Other_ModuleNames = "clsaccountconfig_employee"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " ComboBox's Events "

    Private Sub cboMapRefCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMapRefCode.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            cboMapRefType.DataSource = Nothing
            cboMapRefType.Enabled = False
            Select Case CInt(cboMapRefCode.SelectedValue)
                Case enJVEmployeeConfigRefCode.IDType
                    Dim objID As New clsCommon_Master
                    dsCombo = objID.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "IDType")
                    With cboMapRefType
                        .Enabled = True
                        .ValueMember = "masterunkid"
                        .DisplayMember = "Name"
                        .DataSource = dsCombo.Tables("IDType")
                        If .Items.Count > 0 Then .SelectedValue = 0
                    End With
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMapCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTransactionType_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboTransactionType.Validating, _
                                                                                                                         cboHeadCode.Validating, cboEmployeeCode.Validating, _
                                                                                                                         cboAccountCode.Validating, cboShortName.Validating, _
                                                                                                                         cboShortName2.Validating, cboShortName3.Validating
        'Sohail (03 Mar 2020) - [shortname3]

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim intCount As Integer = (From t In gbFieldMapping.Controls.OfType(Of ComboBox)() Where (t.Name <> cbo.Name AndAlso t.Name <> cboMapRefCode.Name AndAlso t.Name <> cboMapRefName.Name AndAlso t.Name <> cboMapRefType.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue)) Select (t)).ToList.Count
                If intCount > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, This column is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTransactionType_Validating", mstrModuleName)
        End Try
    End Sub

    'Sohail (25 Jul 2020) -- Start
    'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                objPeriod = Nothing
            Else
                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 Jul 2020) -- End

#End Region

#Region " Buttons Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim dsEmployee As DataSet = Nothing
        Dim dRows() As DataRow
        Dim mintTranHeadTypeId As Integer = 0
        Dim strTranHeadTypeName As String = ""
        Dim strCode As String = ""
        Dim intHeadId As Integer = 0
        Dim intAccountId As Integer = 0
        Dim blnUsed As Boolean = False
        Dim objHead As New clsTransactionHead
        Dim objLoan As New clsLoan_Scheme
        Dim objSaving As New clsSavingScheme
        Dim objBank As New clsbankbranch_master
        Dim intEmpoyeeunkId As Integer = 0
        Dim objAcc As New clsAccount_master
        Dim objEmpAcc As New clsaccountconfig_employee

        Try
            If CInt(cboTransactionType.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Transaction Type. Transaction Type is mandatory information."), enMsgBoxStyle.Information)
                cboTransactionType.Focus()
                Exit Try
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Please select period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
                'Sohail (03 Jul 2020) -- End
            ElseIf CInt(cboHeadCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select head code column."), enMsgBoxStyle.Information)
                cboHeadCode.Focus()
                Exit Try
            ElseIf CInt(cboEmployeeCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select employee code column."), enMsgBoxStyle.Information)
                cboEmployeeCode.Focus()
                Exit Try
            ElseIf CInt(cboAccountCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select account code column."), enMsgBoxStyle.Information)
                cboAccountCode.Focus()
                Exit Try
            ElseIf CInt(cboShortName.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select short name column."), enMsgBoxStyle.Information)
                cboShortName.Focus()
                Exit Try
            ElseIf CInt(cboShortName2.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select short name 2 column."), enMsgBoxStyle.Information)
                cboShortName2.Focus()
                Exit Try
                'Sohail (03 Mar 2020) -- Start
                'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
            ElseIf CInt(cboShortName3.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Please select short name 3 column."), enMsgBoxStyle.Information)
                cboShortName3.Focus()
                Exit Try
                'Sohail (03 Mar 2020) -- End
            ElseIf CInt(cboMapRefCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Map Ref. code."), enMsgBoxStyle.Information)
                cboMapRefCode.Focus()
                Exit Try
            ElseIf CInt(cboMapRefName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Map Ref. name."), enMsgBoxStyle.Information)
                cboMapRefName.Focus()
                Exit Try
            ElseIf cboMapRefType.Enabled = True AndAlso CInt(cboMapRefType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Map Ref. type."), enMsgBoxStyle.Information)
                cboMapRefType.Focus()
                Exit Try
            End If

            mblnCancel = True

            dsEmployee = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()), _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()), _
                                      ConfigParameter._Object._UserAccessModeSetting, _
                                      True, False, "Employee", False)

            For Each dsRow As DataRow In dsData.Tables(0).Rows
                Dim dRow As DataRow
                dRows = Nothing

                dRow = dtTable.NewRow

                strTranHeadTypeName = dsRow.Item(cboTransactionType.Text).ToString.Replace("'", "''").Trim
                strCode = dsRow.Item(cboHeadCode.Text).ToString.Replace("'", "''").Trim
                intHeadId = 0
                intAccountId = 0
                intEmpoyeeunkId = 0
                blnUsed = False

                If strTranHeadTypeName.ToUpper = "TRANSACTION HEAD" Then
                    mintTranHeadTypeId = enJVTransactionType.TRANSACTION_HEAD

                    objHead.isExist(strCode, "", 0, intHeadId)
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.TRANSACTION_HEAD, mdtPeriodEnd, intHeadId)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                ElseIf strTranHeadTypeName.ToUpper = "LOAN" Then
                    mintTranHeadTypeId = enJVTransactionType.LOAN

                    objLoan.isExist(strCode, "", 0, intHeadId)
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.LOAN, mdtPeriodEnd, intHeadId)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                ElseIf strTranHeadTypeName.ToUpper = "ADVANCE" Then
                    mintTranHeadTypeId = enJVTransactionType.ADVANCE
                    intHeadId = -99
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.ADVANCE, mdtPeriodEnd, 0)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                ElseIf strTranHeadTypeName.ToUpper = "SAVINGS" Then
                    mintTranHeadTypeId = enJVTransactionType.SAVINGS

                    objSaving.isExist(strCode, "", 0, intHeadId)
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.SAVINGS, mdtPeriodEnd, intHeadId)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                ElseIf strTranHeadTypeName.ToUpper = "CASH" Then
                    mintTranHeadTypeId = enJVTransactionType.CASH
                    intHeadId = -99
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.CASH, mdtPeriodEnd, 0)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                ElseIf strTranHeadTypeName.ToUpper = "BANK" Then
                    mintTranHeadTypeId = enJVTransactionType.BANK

                    objBank.isExist(strCode, "", 0, 0, intHeadId)
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.BANK, mdtPeriodEnd, intHeadId)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                ElseIf strTranHeadTypeName.ToUpper = "COST CENTER" Then
                    mintTranHeadTypeId = enJVTransactionType.COST_CENTER
                    intHeadId = -99
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.COST_CENTER, mdtPeriodEnd, 0)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                ElseIf strTranHeadTypeName.ToUpper = "CLAIM REQUEST EXPENSE" Then
                    mintTranHeadTypeId = enJVTransactionType.CR_EXPENSE
                    Dim objCR As New clsExpense_Master
                    objCR.isExist(strCode, "", 0, 0, intHeadId)
                    blnUsed = objHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.CR_EXPENSE, mdtPeriodEnd, intHeadId)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]

                Else
                    mintTranHeadTypeId = 0
                    intHeadId = 0
                End If

                If mintTranHeadTypeId = 0 Then
                    If CInt(dRow.Item("rowtypeid")) = 0 Then
                        dRow.Item("rowtypeid") = 1 'Invalid Transaction Head Type
                        dRow.Item("Message") = Language.getMessage(mstrModuleName, 12, "Invalid Transaction Head Type.")
                    End If
                End If

                If intHeadId = 0 Then
                    If CInt(dRow.Item("rowtypeid")) = 0 Then
                        dRow.Item("rowtypeid") = 2 'Head code not found
                        dRow.Item("Message") = Language.getMessage(mstrModuleName, 13, "Head code not found.")
                    End If
                End If

                If blnUsed = True Then
                    If CInt(dRow.Item("rowtypeid")) = 0 Then
                        dRow.Item("rowtypeid") = 3 'Already mapped in another account configuration
                        dRow.Item("Message") = Language.getMessage(mstrModuleName, 14, "Head already mapped in another account configuration.")
                    End If
                End If

                dRows = dsEmployee.Tables(0).Select("employeecode = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                    intEmpoyeeunkId = CInt(dRows(0).Item("employeeunkid"))
                    dRow.Item("employeename") = dRows(0).Item("employeename").ToString
                Else
                    intEmpoyeeunkId = objEmp.GetEmployeeUnkidFromEmpCode(dsRow.Item(cboEmployeeCode.Text).ToString)
                    If intEmpoyeeunkId <= 0 Then
                        dRow.Item("employeename") = ""
                        If CInt(dRow.Item("rowtypeid")) = 0 Then
                            dRow.Item("rowtypeid") = 4 'Employee code not found
                            dRow.Item("Message") = Language.getMessage(mstrModuleName, 15, "Employee code not found.")
                        End If
                    Else
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())) = intEmpoyeeunkId
                        dRow.Item("employeename") = objEmp._Firstname & " " & objEmp._Surname
                        If CInt(dRow.Item("rowtypeid")) = 0 Then
                            dRow.Item("rowtypeid") = 5 'Inactive employee
                            dRow.Item("Message") = Language.getMessage(mstrModuleName, 16, "Employee is inactive.")
                        End If
                    End If
                End If


                objAcc.isExist(dsRow.Item(cboAccountCode.Text).ToString(), "", 0, intAccountId)
                If intAccountId = 0 Then
                    If CInt(dRow.Item("rowtypeid")) = 0 Then
                        dRow.Item("rowtypeid") = 6 'Account Code not found
                        dRow.Item("Message") = Language.getMessage(mstrModuleName, 17, "Account Code not found.")
                    End If
                End If

                If CInt(dRow.Item("rowtypeid")) = 0 Then
                    'Sohail (03 Jul 2020) -- Start
                    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                    'If objEmpAcc.isExist(mintTranHeadTypeId, intHeadId, intEmpoyeeunkId) = True Then
                    If objEmpAcc.isExist(mintTranHeadTypeId, intHeadId, intEmpoyeeunkId, CInt(cboPeriod.SelectedValue)) = True Then
                        'Sohail (03 Jul 2020) -- End
                        If CInt(dRow.Item("rowtypeid")) = 0 Then
                            dRow.Item("rowtypeid") = 7 'Head already mapped
                            dRow.Item("Message") = Language.getMessage(mstrModuleName, 18, "Head is already mapped.")
                        End If
                    End If
                End If


                dRow.Item("transactiontype_Id") = mintTranHeadTypeId
                dRow.Item("transactiontype_name") = strTranHeadTypeName

                dRow.Item("headunkid") = intHeadId
                dRow.Item("headcode") = strCode
                dRow.Item("headname") = ""

                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                dRow.Item("period_code") = CType(cboPeriod.SelectedItem, DataRowView).Item("code").ToString
                dRow.Item("period_name") = cboPeriod.Text
                'Sohail (03 Jul 2020) -- End

                dRow.Item("employeeunkid") = intEmpoyeeunkId
                dRow.Item("employeecode") = dsRow.Item(cboEmployeeCode.Text).ToString()

                dRow.Item("accountunkid") = intAccountId
                dRow.Item("accountcode") = dsRow.Item(cboAccountCode.Text).ToString()


                dRow.Item("referencecodeid") = CInt(cboMapRefCode.SelectedValue)
                dRow.Item("referencenameid") = CInt(cboMapRefName.SelectedValue)
                dRow.Item("referencetypeid") = CInt(cboMapRefName.SelectedValue)

                dRow.Item("shortname") = dsRow.Item(cboShortName.Text).ToString()
                dRow.Item("shortname2") = dsRow.Item(cboShortName2.Text).ToString()
                dRow.Item("shortname3") = dsRow.Item(cboShortName3.Text).ToString() 'Sohail (03 Mar 2020)


                dtTable.Rows.Add(dRow)

            Next

            If dtTable.Rows.Count > 0 Then
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Some head codes, account codes or Employee Codes are not matching from file!"), enMsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
            dsEmployee = Nothing
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim dTable As DataTable = dtColumns

            Call AutoMap(lblTransactionType, cboTransactionType)
            Call AutoMap(lblHeadCode, cboHeadCode)
            Call AutoMap(lblEmployeeCode, cboEmployeeCode)
            Call AutoMap(lblAccountCode, cboAccountCode)
            Call AutoMap(lblShortName, cboShortName)
            Call AutoMap(lblShortName2, cboShortName2)
            Call AutoMap(lblShortName3, cboShortName3) 'Sohail (03 Mar 2020)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblTransactionType.Text = Language._Object.getCaption(Me.lblTransactionType.Name, Me.lblTransactionType.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.lblHeadCode.Text = Language._Object.getCaption(Me.lblHeadCode.Name, Me.lblHeadCode.Text)
			Me.lblShortName.Text = Language._Object.getCaption(Me.lblShortName.Name, Me.lblShortName.Text)
			Me.lblAccountCode.Text = Language._Object.getCaption(Me.lblAccountCode.Name, Me.lblAccountCode.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblShortName2.Text = Language._Object.getCaption(Me.lblShortName2.Name, Me.lblShortName2.Text)
			Me.lblMapRefType.Text = Language._Object.getCaption(Me.lblMapRefType.Name, Me.lblMapRefType.Text)
			Me.lblMapRefName.Text = Language._Object.getCaption(Me.lblMapRefName.Name, Me.lblMapRefName.Text)
			Me.lblMapRefCode.Text = Language._Object.getCaption(Me.lblMapRefCode.Name, Me.lblMapRefCode.Text)
			Me.lblShortName3.Text = Language._Object.getCaption(Me.lblShortName3.Name, Me.lblShortName3.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Sorry, This column is already mapped.")
			Language.setMessage(mstrModuleName, 3, "Please select Transaction Type. Transaction Type is mandatory information.")
			Language.setMessage(mstrModuleName, 4, "Please select head code column.")
			Language.setMessage(mstrModuleName, 5, "Please select employee code column.")
			Language.setMessage(mstrModuleName, 6, "Please select account code column.")
			Language.setMessage(mstrModuleName, 7, "Please select short name column.")
			Language.setMessage(mstrModuleName, 8, "Please select short name 2 column.")
			Language.setMessage(mstrModuleName, 9, "Please select Map Ref. code.")
			Language.setMessage(mstrModuleName, 10, "Please select Map Ref. name.")
			Language.setMessage(mstrModuleName, 11, "Please select Map Ref. type.")
			Language.setMessage(mstrModuleName, 12, "Invalid Transaction Head Type.")
			Language.setMessage(mstrModuleName, 13, "Head code not found.")
			Language.setMessage(mstrModuleName, 14, "Head already mapped in another account configuration.")
			Language.setMessage(mstrModuleName, 15, "Employee code not found.")
			Language.setMessage(mstrModuleName, 16, "Employee is inactive.")
			Language.setMessage(mstrModuleName, 17, "Account Code not found.")
			Language.setMessage(mstrModuleName, 18, "Head is already mapped.")
			Language.setMessage(mstrModuleName, 19, "Sorry, Some head codes, account codes or Employee Codes are not matching from file!")
			Language.setMessage(mstrModuleName, 20, "Please select short name 3 column.")
            Language.setMessage(mstrModuleName, 21, "Please select period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class