﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpApproverAddEdit

#Region " Private Variables "

    Private mstrModuleName As String = "frmExpApproverAddEdit"
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    Private mdtEmployee As DataTable
    Private mblnCancel As Boolean = True
    Private mintAssessorMasterId As Integer = -1
    Private objExAssessorMaster As clsExpenseApprover_Master
    Private objExAssessorTran As clsExpenseApprover_Tran
    Private objExApprMapping As clsExpenseApproverMapping
    Private mdtAssessor As DataTable
    Private dtAseView As DataView
    Private dtEmpView As DataView
    Private mdtMapTran As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal iExAssessorId As Integer) As Boolean
        Try
            mintAssessorMasterId = iExAssessorId
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objUsr As New clsUserAddEdit
        Dim objPswd As New clsPassowdOptions
        Dim objEmp As New clsEmployee_Master
        Try

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, 975, FinancialYear._Object._YearUnkid, True)
            dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, CStr(975), FinancialYear._Object._YearUnkid, True)
            'S.SANDEEP [20-JUN-2018] -- END

            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("User")
                .SelectedValue = 0
            End With


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.

            'Dim blnInActiveEmp As Boolean = False
            'If menAction <> enAction.EDIT_ONE Then
            '    blnInActiveEmp = False
            'Else
            '    blnInActiveEmp = True
            'End If

            'dsCombo = objEmp.GetList(FinancialYear._Object._DatabaseName, _
            '                       User._Object._Userunkid, _
            '                       FinancialYear._Object._YearUnkid, _
            '                       Company._Object._Companyunkid, _
            '                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                       ConfigParameter._Object._UserAccessModeSetting, True, blnInActiveEmp, _
            '                        "List", ConfigParameter._Object._ShowFirstAppointmentDate, , , , , False)

            'mdtEmployee = dsCombo.Tables("List")
            'mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            Dim dtTable As DataTable = Nothing

            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
            '    dtTable = New DataView(dsCombo.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            'End If

            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True)
            objExpenseCategory = Nothing

            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                dtTable = New DataView(dsCombo.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Pinkal (24-Jun-2024) -- End

            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            objPswd = Nothing : objUsr = Nothing : objEmp = Nothing
        End Try
    End Sub

    Private Sub Fill_Employee()
        Try
                Dim dEmp As DataTable = Nothing
                Dim sFilter As String = String.Empty
                Dim mstrIds As String = String.Empty


                If mstrAdvanceFilter.Trim.Length > 0 Then
                    sFilter &= "AND " & mstrAdvanceFilter
                End If

                If chkExternalApprover.Checked = False Then
                    If Not txtName.Tag Is Nothing And CInt(txtName.Tag) > 0 Then
                    sFilter &= "AND hremployee_master.employeeunkid <> " & CInt(txtName.Tag) & " "
                    End If
                End If

            If sFilter.Trim.Length > 0 Then
                sFilter = sFilter.Trim.Substring(3)
            End If

            Dim blnInActiveEmp As Boolean = False
            If menAction <> enAction.EDIT_ONE Then
                blnInActiveEmp = False
            Else
                blnInActiveEmp = True
            End If

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            Dim objEmp As New clsEmployee_Master

            Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & _
                                                    "," & clsEmployee_Master.EmpColEnum.Col_Job

            Dim dsCombo As DataSet = objEmp.GetListForDynamicField(StrCheck_Fields, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                      , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                      , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                                      , True, blnInActiveEmp, "List", -1, False, sFilter, ConfigParameter._Object._ShowFirstAppointmentDate _
                                                                                      , False, False, True, Nothing, True)


            'Pinkal (31-May-2021) -- Start
            'Bug - Resolved Bug for Claim approver.
            'If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Columns.Contains("Employee Name") Then
            '    dsCombo.Tables(0).Columns("Employee Name").ColumnName = "Name"
            'End If
            'Pinkal (31-May-2021) -- End

            

            Dim dcColumn As New DataColumn("ischeck", System.Type.GetType("System.Boolean"))
            dcColumn.DefaultValue = False
            dsCombo.Tables(0).Columns.Add(dcColumn)

            objEmp = Nothing

            If mstrEmployeeIDs.Trim.Length > 0 Then
                dEmp = New DataView(dsCombo.Tables(0), "employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )", "", DataViewRowState.CurrentRows).ToTable
                Else
                dEmp = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
                End If

            'Pinkal (18-Jun-2020) -- End

                dtEmpView = dEmp.DefaultView

                dgvAEmployee.AutoGenerateColumns = False
                objdgcolhECheck.DataPropertyName = "ischeck"

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'dgcolhEcode.DataPropertyName = "employeecode"
            'Pinkal (18-Jun-2020) -- End


            'Pinkal (31-May-2021) -- Start
            'Bug - Resolved Bug for Claim approver.
            'dgcolhEcode.DataPropertyName = "code"
            'dgcolhEName.DataPropertyName = "name" 
            dgcolhEcode.DataPropertyName = Language.getMessage("clsEmployee_Master", 42, "Code")
            dgcolhEName.DataPropertyName = Language.getMessage("clsEmployee_Master", 46, "Employee Name")
            'Pinkal (31-May-2021) -- End


                objdgcolhEmpId.DataPropertyName = "employeeunkid"
                dgvAEmployee.DataSource = dtEmpView

                If txtName.Tag IsNot Nothing Then
                    If mdtAssessor IsNot Nothing Then
                        mdtAssessor.Rows.Clear()
                    End If
                    objExAssessorTran._EmployeeAsonDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date

                    objExAssessorTran._ExpApproverMasterId = mintAssessorMasterId
                    mdtAssessor = objExAssessorTran._DataTable
                    Call Fill_Assigned_Employee()
                End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assigned_Employee()
        Try
            dtAseView = mdtAssessor.DefaultView
            dtAseView.RowFilter = " AUD <> 'D' "
            dgvAssessor.AutoGenerateColumns = False
            objdgcolhaCheck.DataPropertyName = "ischeck"
            dgcolhaCode.DataPropertyName = "ecode"
            dgcolhaEmp.DataPropertyName = "ename"
            dgcolhaDepartment.DataPropertyName = "edept"
            dgcolhaJob.DataPropertyName = "ejob"
            objdgcolhaEmpId.DataPropertyName = "employeeunkid"
            objdgcolhAMasterId.DataPropertyName = "crapproverunkid"
            objdgcolhATranId.DataPropertyName = "crapprovertranunkid"
            dgvAssessor.DataSource = dtAseView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Assigned_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid_Data() As Boolean
        Try
            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Approver is mandatory information. Please provide Expense Approver to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboExCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense Category is mandatory information. Please provide Expense Category to continue"), enMsgBoxStyle.Information)
                cboExCategory.Focus()
                Return False
            End If

            If CInt(cboApproveLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Expense Approver Level is mandatory information. Please provide Expense Approver Level to continue"), enMsgBoxStyle.Information)
                cboExCategory.Focus()
                Return False
            End If

            If CInt(cboUser.SelectedValue) <= 0 Then
                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                If chkExternalApprover.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "User is mandatory information. Please provide user to continue"), enMsgBoxStyle.Information)
                    cboUser.Focus()
                    Return False
                End If
                'Pinkal (01-Mar-2016) -- End
            End If

            ' START -  IT DOESN'T ALLOW TO MAP SAME USERS WITH OTHER APPROVER THAT'S WHY COMMENTED.

            'If CInt(cboUser.SelectedValue) <> objExAssessorMaster._MappedUserId Then
            '    Dim sMessage As String = String.Empty
            '    If objExAssessorTran.IsUserMapped(CInt(cboUser.SelectedValue), sMessage) = True Then
            '        eZeeMsgBox.Show(sMessage, enMsgBoxStyle.Information)
            '        cboUser.Focus()
            '        Return False
            '    End If
            'End If

            ' END -  IT DOESN'T ALLOW TO MAP SAME USERS WITH OTHER APPROVER THAT'S WHY COMMENTED.

            Dim dtmp() As DataRow = Nothing
            mdtAssessor.AcceptChanges()
            dtmp = mdtAssessor.Select("AUD <> 'D'")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Expense Approver access is mandatory. Please provide Expense Approver access to save."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid_Data", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objExAssessorMaster._Employeeunkid = CInt(txtName.Tag)
            objExAssessorMaster._crLevelunkid = CInt(cboApproveLevel.SelectedValue)
            objExAssessorMaster._Expensetypeid = CInt(cboExCategory.SelectedValue)

            If chkExternalApprover.Checked = True Then
                objExAssessorMaster._MappedUserId = CInt(txtName.Tag)
            Else
                objExAssessorMaster._MappedUserId = CInt(cboUser.SelectedValue)
            End If

            objExAssessorMaster._Userunkid = User._Object._Userunkid
            If menAction = enAction.EDIT_ONE Then
                objExAssessorMaster._Isvoid = objExAssessorMaster._Isvoid
                objExAssessorMaster._Voiddatetime = objExAssessorMaster._Voiddatetime
                objExAssessorMaster._Voidreason = objExAssessorMaster._Voidreason
                objExAssessorMaster._Voiduserunkid = objExAssessorMaster._Voiduserunkid
                objExAssessorMaster._IsActive = objExAssessorMaster._IsActive
            Else
                objExAssessorMaster._Isvoid = False
                objExAssessorMaster._Voiddatetime = Nothing
                objExAssessorMaster._Voidreason = ""
                objExAssessorMaster._Voiduserunkid = -1
                objExAssessorMaster._IsActive = True
            End If

            objExAssessorMaster._Isexternalapprover = chkExternalApprover.Checked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtName.Tag = objExAssessorMaster._Employeeunkid
            cboUser.SelectedValue = objExAssessorMaster._MappedUserId
            txtName.Text = objExAssessorMaster._EmployeeName
            cboExCategory.SelectedValue = objExAssessorMaster._Expensetypeid
            cboApproveLevel.SelectedValue = objExAssessorMaster._crLevelunkid

            chkExternalApprover.Checked = objExAssessorMaster._Isexternalapprover
            If menAction = enAction.EDIT_ONE Then
                mstrEmployeeIDs = objExAssessorMaster.GetClaimApproverEmployeeId(CInt(cboExCategory.SelectedValue), CInt(txtName.Tag), objExAssessorMaster._Isexternalapprover)
                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                FillEmployeeForApprover()
                'Pinkal (11-Sep-2019) -- End
            End If

            If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        Try
            Dim mdtRow As DataRow = Nothing
            Dim dtmp() As DataRow = Nothing
            dtmp = mdtAssessor.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
            If dtmp.Length <= 0 Then
                mdtRow = mdtAssessor.NewRow
                mdtRow.Item("crapprovertranunkid") = -1
                mdtRow.Item("crapproverunkid") = mintAssessorMasterId
                mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                mdtRow.Item("userunkid") = User._Object._Userunkid
                mdtRow.Item("isvoid") = False
                mdtRow.Item("voiddatetime") = DBNull.Value
                mdtRow.Item("voidreason") = ""
                mdtRow.Item("voiduserunkid") = -1
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString

                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'mdtRow.Item("ecode") = dRow.Item("employeecode")
                'mdtRow.Item("ename") = dRow.Item("name")
                'mdtRow.Item("edept") = dRow.Item("DeptName")
                'mdtRow.Item("ejob") = dRow.Item("job_name")


                'Pinkal (31-May-2021) -- Start
                'Bug - Resolved Bug for Claim approver.
                'mdtRow.Item("ecode") = dRow.Item("code")
                'mdtRow.Item("ename") = dRow.Item("name")
                'mdtRow.Item("edept") = dRow.Item("Department")
                'mdtRow.Item("ejob") = dRow.Item("job")
                mdtRow.Item("ecode") = dRow.Item(Language.getMessage("clsEmployee_Master", 42, "Code"))
                mdtRow.Item("ename") = dRow.Item(Language.getMessage("clsEmployee_Master", 46, "Employee Name"))
                mdtRow.Item("edept") = dRow.Item(Language.getMessage("clsEmployee_Master", 120, "Department"))
                mdtRow.Item("ejob") = dRow.Item(Language.getMessage("clsEmployee_Master", 118, "Job"))
                'Pinkal (31-May-2021) -- End

                
                'Pinkal (18-Jun-2020) -- End
                mdtAssessor.Rows.Add(mdtRow)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    Private Sub FillEmployeeForApprover()
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim objEmp As New clsEmployee_Master

            Dim blnInActiveEmp As Boolean = False
            If menAction <> enAction.EDIT_ONE Then
                blnInActiveEmp = False
            Else
                blnInActiveEmp = True
            End If

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.


            'Dim dsCombo As DataSet = objEmp.GetList(FinancialYear._Object._DatabaseName, _
            '                       User._Object._Userunkid, _
            '                       FinancialYear._Object._YearUnkid, _
            '                       Company._Object._Companyunkid, _
            '                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                       ConfigParameter._Object._UserAccessModeSetting, True, blnInActiveEmp, _
            '                       "List", ConfigParameter._Object._ShowFirstAppointmentDate, , , , False, False)

            Dim dsCombo As DataSet = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, blnInActiveEmp, "List", False, 0)

            mdtEmployee = dsCombo.Tables("List")
            'Dim dcColumn As New DataColumn("ischeck", System.Type.GetType("System.Boolean"))
            'dcColumn.DefaultValue = False
            'mdtEmployee.Columns.Add(dcColumn)

            'Pinkal (18-Jun-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeForApprover", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    'Pinkal (11-Sep-2019) -- End


#End Region

#Region " Form's Events "

    Private Sub frmExApproverAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objExAssessorMaster = New clsExpenseApprover_Master
        objExAssessorTran = New clsExpenseApprover_Tran
        objExApprMapping = New clsExpenseApproverMapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Combo()
            If menAction = enAction.EDIT_ONE Then
                objExAssessorMaster._crApproverunkid = mintAssessorMasterId
                objbtnSearchEmployee.Enabled = False
                chkExternalApprover.Enabled = False
                cboExCategory.Enabled = False
            End If
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExApproverAddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseApprover_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsExpenseApprover_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            If CInt(cboExCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense Category is mandatory information. Please provide Expense Category to continue"), enMsgBoxStyle.Information)
                cboExCategory.Focus()
                Exit Sub
            End If
            'Pinkal (18-Jun-2020) -- End



            If chkExternalApprover.Checked = False Then

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                FillEmployeeForApprover()
                'Pinkal (11-Sep-2019) -- End


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'With frm
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "name"
                '    .CodeMember = "employeecode"
                '    .DataSource = mdtEmployee
                'End With
                With frm
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .CodeMember = "employeecode"
                    .DataSource = mdtEmployee
                End With
                'Pinkal (18-Jun-2020) -- End

                If frm.DisplayDialog Then
                    txtName.Text = frm.SelectedAlias & " - " & frm.SelectedText
                    txtName.Tag = frm.SelectedValue
                    Dim objOption As New clsPassowdOptions
                    If objOption._IsEmployeeAsUser Then
                        Dim objUser As New clsUserAddEdit
                        Dim mintUserID As Integer = objUser.Return_UserId(CInt(frm.SelectedValue), Company._Object._Companyunkid)
                        Dim drRow() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = " & mintUserID)
                        If drRow.Length > 0 Then
                            cboUser.SelectedValue = mintUserID
                        Else
                            cboUser.SelectedValue = 0
                        End If
                    End If
                End If

            Else

                Dim objUser As New clsUserAddEdit

                Dim dsList As DataSet = objUser.GetExternalApproverList("List", _
                                                         Company._Object._Companyunkid, _
                                                         FinancialYear._Object._YearUnkid, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), "975")
                objUser = Nothing

                frm.DataSource = dsList.Tables("List")
                frm.ValueMember = "userunkid"
                frm.DisplayMember = "Name"
                frm.CodeMember = "username"

                If frm.DisplayDialog Then
                    txtName.Text = frm.SelectedAlias & " - " & frm.SelectedText
                    txtName.Tag = frm.SelectedValue
                End If

            End If

            mstrEmployeeIDs = objExAssessorMaster.GetClaimApproverEmployeeId(CInt(cboExCategory.SelectedValue), CInt(txtName.Tag), chkExternalApprover.Checked)

            If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboUser.ValueMember
                .CodeMember = cboUser.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboApproveLevel.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboApproveLevel.ValueMember
                    .DisplayMember = cboApproveLevel.DisplayMember
                    .CodeMember = "Code"
                    .DataSource = CType(cboApproveLevel.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboUser.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If dtEmpView IsNot Nothing Then
                Dim drCheck() As DataRow = Nothing
                drCheck = dtEmpView.ToTable.Select("ischeck=true")
                Dim iblFlag As Boolean = False
                If drCheck.Length > 0 Then
                    For i As Integer = 0 To drCheck.Length - 1
                        Call Add_DataRow(drCheck(i))
                    Next
                End If
                Call Fill_Assigned_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteA.Click
        Try
            If dtAseView Is Nothing Then Exit Sub
            Dim drTemp As DataRow() = Nothing
            Dim dtmp() As DataRow = dtAseView.Table.Select("ischeck=True AND AUD <> 'D' ")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Me.SuspendLayout()
            Dim blnFlag As Boolean = False
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are you sure you want to delete Selected Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                Dim objExpenseForm As New clsclaim_request_master

                For i As Integer = 0 To dtmp.Length - 1

                    If objExpenseForm.GetApproverPendingExpenseFormCount(CInt(dtmp(i)("crapproverunkid")), CInt(dtmp(i)("employeeunkid")).ToString()) <= 0 Then
                        'Pinkal (31-May-2021) -- Start
                        'Bug - Resolved Bug for Claim approver.
                        'drTemp = dtAseView.Table.Select("employeeunkid = " & CInt(dtmp(i)("employeeunkid")))
                        drTemp = dtAseView.Table.Select("employeeunkid = " & CInt(dtmp(i)("employeeunkid")) & " AND AUD <> 'D' ")
                        'Pinkal (31-May-2021) -- End
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee."), enMsgBoxStyle.Information)
                        Exit For
                    End If

                    If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        drTemp(0).Item("isvoid") = True
                        'Pinkal (31-May-2021) -- Start
                        'Bug - Resolved Bug for Claim approver.
                        'drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime.Date
                        drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        'Pinkal (31-May-2021) -- End
                        drTemp(0).Item("voidreason") = ""
                        drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                        dtAseView.Table.AcceptChanges()
                    End If

                Next

                txtaSearch.Text = ""
                objchkAssessor.Checked = False
                Fill_Assigned_Employee()
            End If
            Me.ResumeLayout()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnDeleteA_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Valid_Data() = False Then Exit Sub
            Call SetValue()
            If menAction <> enAction.EDIT_ONE Then
                If objExAssessorMaster.Insert(mdtAssessor, mdtMapTran) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Expense Approver defined successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                Else
                    eZeeMsgBox.Show(objExAssessorMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If objExAssessorMaster.Update(mdtAssessor, mdtMapTran) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Expense Approver updated successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                Else
                    eZeeMsgBox.Show(objExAssessorMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objExLevel As New clsExApprovalLevel
            dsList = objExLevel.getListForCombo(CInt(cboExCategory.SelectedValue), "List", True)
            With cboApproveLevel
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objExLevel = Nothing : dsList.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkMapExpenses_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkMapExpenses.LinkClicked
        Dim frm As New frmMapExpenses_Approver
        Try
            frm.displayDialog(txtName.Text, cboApproveLevel.Text, CInt(cboExCategory.SelectedValue), mintAssessorMasterId, mdtMapTran)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkMapExpenses_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Close()
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'mstrAdvanceFilter = frm._GetFilterString.Replace("ADF.", "")
                mstrAdvanceFilter = frm._GetFilterString
                'Pinkal (18-Jun-2020) -- End
                Call Fill_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then

                'Pinkal (31-May-2021) -- Start
                'Bug - Resolved Bug for Claim approver.
                'strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                '            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"

                strSearch = "[" & dgcolhEcode.DataPropertyName & "] LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            "[" & dgcolhEName.DataPropertyName & "] LIKE '%" & txtSearchEmp.Text & "%'"
                'Pinkal (31-May-2021) -- End

            End If
            dtEmpView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtaSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtaSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAssessor.Rows.Count > 0 Then
                        If dgvAssessor.SelectedRows(0).Index = dgvAssessor.Rows(dgvAssessor.RowCount - 1).Index Then Exit Sub
                        dgvAssessor.Rows(dgvAssessor.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAssessor.Rows.Count > 0 Then
                        If dgvAssessor.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAssessor.Rows(dgvAssessor.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtaSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtaSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtaSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhaCode.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaEmp.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaDepartment.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaJob.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%'  AND AUD <> 'D' "
                dtAseView.RowFilter = strSearch
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In dtEmpView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAssessor.CheckedChanged
        Try
            RemoveHandler dgvAssessor.CellContentClick, AddressOf dgvAssessor_CellContentClick
            For Each dr As DataRowView In dtAseView
                dr.Item("ischeck") = CBool(objchkAssessor.CheckState)
            Next
            dtAseView.Table.AcceptChanges()
            dgvAssessor.Refresh()
            AddHandler dgvAssessor.CellContentClick, AddressOf dgvAssessor_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkExternalApprover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            LblUser.Visible = Not chkExternalApprover.Checked
            cboUser.Visible = Not chkExternalApprover.Checked
            objbtnSearchUser.Visible = Not chkExternalApprover.Checked
            If chkExternalApprover.Checked = False Then
                cboUser.SelectedValue = 0
            End If
            If menAction <> enAction.EDIT_ONE Then
                txtName.Text = ""
                txtName.Tag = Nothing
                mstrEmployeeIDs = ""
                dgvAEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExternalApprover_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtEmpView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtEmpView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub


    Private Sub dgvAssessor_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAssessor.CellContentClick
        Try
            RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAssessor.IsCurrentCellDirty Then
                    Me.dgvAssessor.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtAseView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtAseView.ToTable.Rows.Count = drRow.Length Then
                        objchkAssessor.CheckState = CheckState.Checked
                    Else
                        objchkAssessor.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAssessor.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssessor_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAssignedEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssignedEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteA.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteA.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.Name, Me.lblApproverName.Text)
            Me.LblUser.Text = Language._Object.getCaption(Me.LblUser.Name, Me.LblUser.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.Name, Me.lblApproveLevel.Text)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
            Me.gbAssignedEmployee.Text = Language._Object.getCaption(Me.gbAssignedEmployee.Name, Me.gbAssignedEmployee.Text)
            Me.btnDeleteA.Text = Language._Object.getCaption(Me.btnDeleteA.Name, Me.btnDeleteA.Text)
            Me.lnkMapExpenses.Text = Language._Object.getCaption(Me.lnkMapExpenses.Name, Me.lnkMapExpenses.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.dgcolhaCode.HeaderText = Language._Object.getCaption(Me.dgcolhaCode.Name, Me.dgcolhaCode.HeaderText)
            Me.dgcolhaEmp.HeaderText = Language._Object.getCaption(Me.dgcolhaEmp.Name, Me.dgcolhaEmp.HeaderText)
            Me.dgcolhaDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhaDepartment.Name, Me.dgcolhaDepartment.HeaderText)
            Me.dgcolhaJob.HeaderText = Language._Object.getCaption(Me.dgcolhaJob.Name, Me.dgcolhaJob.HeaderText)
            Me.chkExternalApprover.Text = Language._Object.getCaption(Me.chkExternalApprover.Name, Me.chkExternalApprover.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Expense Approver is mandatory information. Please provide Expense Approver to continue.")
            Language.setMessage(mstrModuleName, 2, "Expense Category is mandatory information. Please provide Expense Category to continue")
            Language.setMessage(mstrModuleName, 3, "Expense Approver Level is mandatory information. Please provide Expense Approver Level to continue")
            Language.setMessage(mstrModuleName, 4, "User is mandatory information. Please provide user to continue")
            Language.setMessage(mstrModuleName, 5, "Expense Approver access is mandatory. Please provide Expense Approver access to save.")
            Language.setMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassign.")
            Language.setMessage(mstrModuleName, 7, "Expense Approver defined successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 8, "Expense Approver updated successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 9, "Are you sure you want to delete Selected Employee?")
            Language.setMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class