﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpenseCategoryAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExpensesCategoryAddEdit"
    Private mblnCancel As Boolean = True
    Private objExpenseCategory As clsexpense_category_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintExpenseCategoryUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintExpenseCategoryUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintExpenseCategoryUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCategoryCode.BackColor = GUI.ColorComp
            txtCategoryName.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCategoryCode.Text = objExpenseCategory._Categorycode
            txtCategoryName.Text = objExpenseCategory._Categoryname
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objExpenseCategory._Categorycode = txtCategoryCode.Text
            objExpenseCategory._Categoryname = txtCategoryName.Text
            objExpenseCategory._Ispredefine = False
            objExpenseCategory._Isactive = True
            objExpenseCategory._Userunkid = User._Object._Userunkid
            objExpenseCategory._WebFormName = Me.Name
            objExpenseCategory._WebClientIP = getIP()
            objExpenseCategory._WebHostName = getHostName()
            objExpenseCategory._IsWeb = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            If txtCategoryCode.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category Code is mandatory information. Please provide Expense Category Code."), enMsgBoxStyle.Information)
                txtCategoryCode.Focus()
                Return False
            End If

            If txtCategoryName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense Category Name is mandatory information. Please provide Expense Category Name."), enMsgBoxStyle.Information)
                txtCategoryName.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isValid_Data", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmExpensesCategoryAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExpenseCategory = Nothing
    End Sub

    Private Sub frmExpensesCategoryAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesCategoryAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesCategoryAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesCategoryAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesCategoryAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExpenseCategory = New clsexpense_category_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objExpenseCategory._Categoryunkid = mintExpenseCategoryUnkid
            End If
            Call GetValue()
            txtCategoryCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesCategoryAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsexpense_category_master.SetMessages()
            objfrm._Other_ModuleNames = "clsexpense_category_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If isValid_Data() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objExpenseCategory.Update()
            Else
                blnFlag = objExpenseCategory.Insert()
            End If

            If blnFlag = False And objExpenseCategory._Message <> "" Then
                eZeeMsgBox.Show(objExpenseCategory._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objExpenseCategory = Nothing
                    objExpenseCategory = New clsexpense_category_master
                    Call GetValue()
                    txtCategoryName.Focus()
                Else
                    mintExpenseCategoryUnkid = objExpenseCategory._Categoryunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtCategoryName.Text, objExpenseCategory._Categoryname1, objExpenseCategory._Categoryname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbExpenseCategoryInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbExpenseCategoryInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbExpenseCategoryInformation.Text = Language._Object.getCaption(Me.gbExpenseCategoryInformation.Name, Me.gbExpenseCategoryInformation.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblExpenseCategoryName.Text = Language._Object.getCaption(Me.lblExpenseCategoryName.Name, Me.lblExpenseCategoryName.Text)
            Me.lblExpenseCategoryCode.Text = Language._Object.getCaption(Me.lblExpenseCategoryCode.Name, Me.lblExpenseCategoryCode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Expense Category Code is mandatory information. Please provide Expense Category Code.")
            Language.setMessage(mstrModuleName, 2, "Expense Category Name is mandatory information. Please provide Expense Category Name.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class