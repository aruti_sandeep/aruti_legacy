﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpenseCategoryList

#Region " Private Varaibles "

    Private objExpCategory As clsexpense_category_master
    Private ReadOnly mstrModuleName As String = "frmExpensesCategoryList"

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddExpenseCategory
            btnEdit.Enabled = User._Object.Privilege._AllowToEditExpenseCategory
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteExpenseCategory
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Try
            If User._Object.Privilege._AllowToViewExpenseCategoryList = False Then Exit Sub

            dsList = objExpCategory.GetList("List", True, "")

            Dim objGrp As New clsGroup_Master
            objGrp._Groupunkid = 1
            If objGrp._Groupname.ToUpper() <> "PW" Then
                Dim drRow() As DataRow = dsList.Tables(0).Select("categoryunkid IN (" & enExpenseType.EXP_REBATE_PRIVILEGE & "," & enExpenseType.EXP_REBATE_DUTY & "," & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT & ")")
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        dsList.Tables(0).Rows.Remove(drRow(i))
                    Next
                    dsList.Tables(0).AcceptChanges()
                End If
            End If
            objGrp = Nothing

            lvExpenseCategoryList.Items.Clear()
            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dRow("categorycode").ToString
                lvItem.SubItems.Add(dRow("categoryname").ToString)
                lvItem.SubItems.Add(dRow("ispredefine").ToString)
                lvItem.Tag = dRow("categoryunkid")
                lvExpenseCategoryList.Items.Add(lvItem)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExpensesList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvExpenseCategoryList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmExpensesList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesCategoryList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExpCategory = New clsexpense_category_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Fill_List()
            If lvExpenseCategoryList.Items.Count > 0 Then lvExpenseCategoryList.Items(0).Selected = True
            lvExpenseCategoryList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExpCategory = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsexpense_category_master.SetMessages()
            objfrm._Other_ModuleNames = "clsexpense_category_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmExpenseCategoryAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvExpenseCategoryList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Expense Category from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvExpenseCategoryList.Select()
            Exit Sub
        End If

        Dim frm As New frmExpenseCategoryAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvExpenseCategoryList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvExpenseCategoryList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Expense Category from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvExpenseCategoryList.Select()
            Exit Sub
        End If
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Expense Category?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objExpCategory._Userunkid = User._Object._Userunkid
                objExpCategory._WebFormName = Me.Name
                objExpCategory._WebClientIP = getIP()
                objExpCategory._WebHostName = getHostName()
                objExpCategory._IsWeb = False

                objExpCategory.Delete(CInt(lvExpenseCategoryList.SelectedItems(0).Tag))
                If objExpCategory._Message <> "" Then
                    eZeeMsgBox.Show(objExpCategory._Message, enMsgBoxStyle.Information)
                Else
                    lvExpenseCategoryList.SelectedItems(0).Remove()
                End If
            End If
            lvExpenseCategoryList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "ListView Events"

    Private Sub lvExpenseCategoryList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvExpenseCategoryList.SelectedIndexChanged
        Try
            If lvExpenseCategoryList.SelectedItems.Count > 0 Then
                If CBool(lvExpenseCategoryList.SelectedItems(0).SubItems(objcolhisPredefine.Index).Text) = True Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                Else
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvExpenseCategoryList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Expense Category from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Expense Category?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class