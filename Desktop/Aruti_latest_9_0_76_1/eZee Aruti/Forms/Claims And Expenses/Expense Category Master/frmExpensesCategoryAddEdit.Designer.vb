﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpenseCategoryAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExpenseCategoryAddEdit))
        Me.gbExpenseCategoryInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtCategoryName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCategoryCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblExpenseCategoryName = New System.Windows.Forms.Label
        Me.lblExpenseCategoryCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbExpenseCategoryInformation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbExpenseCategoryInformation
        '
        Me.gbExpenseCategoryInformation.BorderColor = System.Drawing.Color.Black
        Me.gbExpenseCategoryInformation.Checked = False
        Me.gbExpenseCategoryInformation.CollapseAllExceptThis = False
        Me.gbExpenseCategoryInformation.CollapsedHoverImage = Nothing
        Me.gbExpenseCategoryInformation.CollapsedNormalImage = Nothing
        Me.gbExpenseCategoryInformation.CollapsedPressedImage = Nothing
        Me.gbExpenseCategoryInformation.CollapseOnLoad = False
        Me.gbExpenseCategoryInformation.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbExpenseCategoryInformation.Controls.Add(Me.txtCategoryName)
        Me.gbExpenseCategoryInformation.Controls.Add(Me.txtCategoryCode)
        Me.gbExpenseCategoryInformation.Controls.Add(Me.lblExpenseCategoryName)
        Me.gbExpenseCategoryInformation.Controls.Add(Me.lblExpenseCategoryCode)
        Me.gbExpenseCategoryInformation.ExpandedHoverImage = Nothing
        Me.gbExpenseCategoryInformation.ExpandedNormalImage = Nothing
        Me.gbExpenseCategoryInformation.ExpandedPressedImage = Nothing
        Me.gbExpenseCategoryInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExpenseCategoryInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExpenseCategoryInformation.HeaderHeight = 25
        Me.gbExpenseCategoryInformation.HeaderMessage = ""
        Me.gbExpenseCategoryInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExpenseCategoryInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExpenseCategoryInformation.HeightOnCollapse = 0
        Me.gbExpenseCategoryInformation.LeftTextSpace = 0
        Me.gbExpenseCategoryInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbExpenseCategoryInformation.Name = "gbExpenseCategoryInformation"
        Me.gbExpenseCategoryInformation.OpenHeight = 300
        Me.gbExpenseCategoryInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExpenseCategoryInformation.ShowBorder = True
        Me.gbExpenseCategoryInformation.ShowCheckBox = False
        Me.gbExpenseCategoryInformation.ShowCollapseButton = False
        Me.gbExpenseCategoryInformation.ShowDefaultBorderColor = True
        Me.gbExpenseCategoryInformation.ShowDownButton = False
        Me.gbExpenseCategoryInformation.ShowHeader = True
        Me.gbExpenseCategoryInformation.Size = New System.Drawing.Size(381, 88)
        Me.gbExpenseCategoryInformation.TabIndex = 5
        Me.gbExpenseCategoryInformation.Temp = 0
        Me.gbExpenseCategoryInformation.Text = "Expense Category"
        Me.gbExpenseCategoryInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(344, 57)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 324
        '
        'txtCategoryName
        '
        Me.txtCategoryName.Flags = 0
        Me.txtCategoryName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCategoryName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCategoryName.Location = New System.Drawing.Point(116, 57)
        Me.txtCategoryName.Name = "txtCategoryName"
        Me.txtCategoryName.Size = New System.Drawing.Size(223, 21)
        Me.txtCategoryName.TabIndex = 322
        '
        'txtCategoryCode
        '
        Me.txtCategoryCode.Flags = 0
        Me.txtCategoryCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCategoryCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCategoryCode.Location = New System.Drawing.Point(116, 30)
        Me.txtCategoryCode.Name = "txtCategoryCode"
        Me.txtCategoryCode.Size = New System.Drawing.Size(223, 21)
        Me.txtCategoryCode.TabIndex = 320
        '
        'lblExpenseCategoryName
        '
        Me.lblExpenseCategoryName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseCategoryName.Location = New System.Drawing.Point(10, 59)
        Me.lblExpenseCategoryName.Name = "lblExpenseCategoryName"
        Me.lblExpenseCategoryName.Size = New System.Drawing.Size(100, 17)
        Me.lblExpenseCategoryName.TabIndex = 323
        Me.lblExpenseCategoryName.Text = "Category Name"
        Me.lblExpenseCategoryName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExpenseCategoryCode
        '
        Me.lblExpenseCategoryCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseCategoryCode.Location = New System.Drawing.Point(10, 32)
        Me.lblExpenseCategoryCode.Name = "lblExpenseCategoryCode"
        Me.lblExpenseCategoryCode.Size = New System.Drawing.Size(100, 17)
        Me.lblExpenseCategoryCode.TabIndex = 321
        Me.lblExpenseCategoryCode.Text = "Category Code"
        Me.lblExpenseCategoryCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 91)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(381, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(169, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(272, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmExpenseCategoryAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 146)
        Me.Controls.Add(Me.gbExpenseCategoryInformation)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseCategoryAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Expense Category"
        Me.gbExpenseCategoryInformation.ResumeLayout(False)
        Me.gbExpenseCategoryInformation.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbExpenseCategoryInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtCategoryName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCategoryCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblExpenseCategoryName As System.Windows.Forms.Label
    Friend WithEvents lblExpenseCategoryCode As System.Windows.Forms.Label
End Class
