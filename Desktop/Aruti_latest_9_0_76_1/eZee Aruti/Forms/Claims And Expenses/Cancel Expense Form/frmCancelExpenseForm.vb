﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region

Public Class frmCancelExpenseForm

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCancelExpenseForm"
    Private objClaimMaster As clsclaim_request_master
    Private objClaimTran As clsclaim_request_tran
    Private objExpApproverTran As clsclaim_request_approval_tran
    Private mintClaimRequestMasterId As Integer = 0
    Private mintClaimApproverId As Integer = 0
    Private mintClaimApproverEmpID As Integer = 0
    Private mintApproverPriority As Integer = 0
    Public mdtTran As DataTable
    Private mblnIsFromLeave As Boolean = False
    Private mblnCancel As Boolean = True

    'Pinkal (13-Jul-2015) -- Start
    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
    Dim objEmployee As clsEmployee_Master
    'Pinkal (13-Jul-2015) -- End


    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    Dim LstClaimEmailList As New List(Of clsEmailCollection)
    'Pinkal (20-May-2022) -- End


    'Pinkal (24-Jun-2024) -- Start
    'NMB Enhancement : P2P & Expense Category Enhancements.
    Private mstrGroupName As String = ""
    'Pinkal (24-Jun-2024) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intClaimRequestMstId As Integer, ByVal intClaimApproverId As Integer _
                                            , ByVal intApproverEmpId As Integer, ByVal intApproverPriority As Integer, Optional ByVal blnIsFromLeave As Boolean = False) As Boolean
        Try
            mintClaimRequestMasterId = intClaimRequestMstId
            mintClaimApproverId = intClaimApproverId
            mintClaimApproverEmpID = intApproverEmpId
            mintApproverPriority = intApproverPriority
            mblnIsFromLeave = blnIsFromLeave
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, False, True, "List", True, True, False, False, False)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End

            With cboExpCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try

            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then

                If mdtTran.Columns.Contains("IsPosted") = False Then
                    mdtTran.Columns.Add("IsPosted", Type.GetType("System.Boolean"))
                    mdtTran.Columns("IsPosted").DefaultValue = False
                End If


                Dim mstrApprovalTranunkid As String = ""
                Dim objClaimProces As New clsclaim_process_Tran
                Dim dsList As DataSet = objClaimProces.GetList("List", True, , "cmclaim_process_tran.crmasterunkid = " & mintClaimRequestMasterId)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        If CBool(dsList.Tables(0).Rows(i)("isposted")) Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objPeriod._Periodunkid = CInt(dsList.Tables(0).Rows(i)("periodunkid"))
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables(0).Rows(i)("periodunkid"))
                            'Sohail (21 Aug 2015) -- End
                            Dim drRow() As DataRow = mdtTran.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If objPeriod._Statusid = enStatusType.Close Then
                                If drRow.Length > 0 Then
                                    drRow(0).Delete()
                                End If
                            Else
                                If drRow.Length > 0 Then
                                    drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                                End If
                            End If
                        Else
                            Dim drRow() As DataRow = mdtTran.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If drRow.Length > 0 Then
                                drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                            End If
                        End If
                        mstrApprovalTranunkid &= CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")) & ","
                    Next

                    If mstrApprovalTranunkid.Trim.Length > 0 Then mstrApprovalTranunkid = mstrApprovalTranunkid.Trim.Substring(0, mstrApprovalTranunkid.Trim.Length - 1)

                    Dim dRows() As DataRow = mdtTran.Select("crapprovaltranunkid Not in ( " & mstrApprovalTranunkid & ")")
                    If dRows.Length > 0 Then
                        For j As Integer = 0 To dRows.Length - 1
                            dRows(j)("IsPosted") = False
                        Next
                    End If
                    mdtTran.AcceptChanges()
                Else
                    For j As Integer = 0 To mdtTran.Rows.Count - 1
                        mdtTran.Rows(j)("IsPosted") = False
                    Next
                    mdtTran.AcceptChanges()
                End If
            End If

            mdtTran = New DataView(mdtTran, "iscancel = 0", "", DataViewRowState.CurrentRows).ToTable
            dgvData.AutoGenerateColumns = False
            objcolhIsSelect.DataPropertyName = "Ischeck"
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhExpense.DataPropertyName = "expense"
            dgcolhQty.DataPropertyName = "quantity"
            dgcolhRemark.DataPropertyName = "expense_remark"
            dgcolhUnitPrice.DataPropertyName = "unitprice"
            dgcolhUoM.DataPropertyName = "uom"
            objdgcolhMasterId.DataPropertyName = "crmasterunkid"
            objdgcolhApproverTranId.DataPropertyName = "crapprovaltranunkid"
            objcolhRequestTranID.DataPropertyName = "crtranunkid"
            objdgcolhIsPosted.DataPropertyName = "IsPosted"


            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            dgcolhBaseAmount.DataPropertyName = "base_amount"
            dgcolhBaseAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhQty.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhUnitPrice.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhBaseAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'Pinkal (24-Jun-2024) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dgcolhCurrency.DataPropertyName = "currency_sign"
            'Pinkal (04-Feb-2019) -- End


            dgvData.DataSource = mdtTran

            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhUnitPrice.DefaultCellStyle.Format = GUI.fmtCurrency

            If mdtTran.Rows.Count > 0 Then
                'Pinkal (24-Jun-2024) -- Start
                'NMB Enhancement : P2P & Expense Category Enhancements.
                If mstrGroupName.ToUpper = "NMB PLC" AndAlso ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.ToString().Trim.Length > 0 Then
                    dgcolhBaseAmount.Visible = True
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(base_amount)", "AUD<>'D' AND iscancel = 0")), GUI.fmtCurrency)
                Else
                    dgcolhBaseAmount.Visible = False
                txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D' AND iscancel = 0")), GUI.fmtCurrency)
                End If
                'Pinkal (24-Jun-2024) -- End
            Else
                txtGrandTotal.Text = "0.00"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Expense", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try

            If txtCancelRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Cancel Remark cannot be blank.Cancel Remark is required information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtCancelRemark.Focus()
                Return False
            End If

            Dim drRow() As DataRow = mdtTran.Select("Ischeck = True")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast One Expense to cancel."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub GetValue()
        Try

            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            'Dim objEmployee As New clsEmployee_Master
            objEmployee = New clsEmployee_Master
            'Pinkal (13-Jul-2015) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objClaimMaster._Employeeunkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMaster._Employeeunkid
            'S.SANDEEP [04 JUN 2015] -- END

            txtEmployee.Text = objEmployee._Employeecode & " - " & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            txtEmployee.Tag = objClaimMaster._Employeeunkid
            txtClaimNo.Text = objClaimMaster._Claimrequestno
            cboExpCategory.SelectedValue = objClaimMaster._Expensetypeid
            objClaimMaster._Transactiondate = dtpDate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = mdtTran.Select("Ischeck = True")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Function SaveData(ByVal sender As Object) As Boolean
        Dim blnFlag As Boolean = False
        Dim blnLastApprover As Boolean = False
        Dim mstrRejectRemark As String = ""
        Dim mintMaxPriority As Integer = -1
        Try
            Dim mintStatusID As Integer = 2 'PENDING
            objExpApproverTran._DataTable = mdtTran

            Dim drRow() As DataRow = mdtTran.Select("AUD = ''")

            If drRow.Length > 0 Then
                For Each dr As DataRow In drRow
                    dr("AUD") = "U"
                    dr.AcceptChanges()
                Next
            End If

            Dim objExpAppr As New clsExpenseApprover_Master

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'Dim dsList As DataSet = objExpAppr.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(txtEmployee.Tag), "List", Nothing)
            Dim dsList As DataSet = Nothing
            Dim mdecTotalClaimAmount As Decimal = 0
            If mstrGroupName.ToUpper() = "NMB PLC" AndAlso ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                    mdecTotalClaimAmount = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Decimal)("base_amount")).DefaultIfEmpty.Sum()
                End If
                dsList = objExpAppr.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(txtEmployee.Tag), "List", Nothing, -1, False, mdecTotalClaimAmount)
            Else
                dsList = objExpAppr.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(txtEmployee.Tag), "List", Nothing)
            End If
            'Pinkal (24-Jun-2024) -- End



            Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & mintApproverPriority, "", DataViewRowState.CurrentRows).ToTable

            mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

            For i As Integer = 0 To dtApproverTable.Rows.Count - 1
                blnLastApprover = False
                objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))

                If mintClaimApproverId = objExpAppr._crApproverunkid Then
                    If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNAPPROVE" Then
                        mintStatusID = 1
                        If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then blnLastApprover = True
                    ElseIf CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNREMARKOK" Then
                        mintStatusID = 3
                        blnLastApprover = True
                    End If
                Else
                    mintStatusID = 2
                End If

                objExpApproverTran._YearId = FinancialYear._Object._YearUnkid
                objExpApproverTran._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                objExpApproverTran._EmployeeID = CInt(txtEmployee.Tag)  'TEMPARORALY


                'Pinkal (24-Jun-2024) -- Start
                'NMB Enhancement : P2P & Expense Category Enhancements.
                'If objExpApproverTran.Insert_Update_ApproverData(objExpAppr._Employeeunkid, objExpAppr._crApproverunkid, mintStatusID, objExpApproverTran._VisiblelId, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mintClaimRequestMasterId, Nothing, mstrRejectRemark, blnLastApprover) = False Then
                '    Return False
                'End If

                If objExpApproverTran.Insert_Update_ApproverData(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PaymentApprovalwithLeaveApproval _
                                                                , objExpAppr._Employeeunkid, objExpAppr._crApproverunkid, mintStatusID, objExpApproverTran._VisiblelId, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime _
                                                                , mintClaimRequestMasterId, Nothing, mstrRejectRemark, blnLastApprover) = False Then
                    Return False
                End If

                'Pinkal (24-Jun-2024) -- End


            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveData", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try

            If LstClaimEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In LstClaimEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    End If
                    objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), objEmail._ExportReportPath)
                Next
                If LstClaimEmailList.Count > 0 Then LstClaimEmailList.Clear()
                objSendMail = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Pinkal (20-May-2022) -- End

#End Region

#Region " Form's Events "

    Private Sub frmCancelExpenseForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objClaimMaster = New clsclaim_request_master
        objClaimTran = New clsclaim_request_tran
        objExpApproverTran = New clsclaim_request_approval_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrGroupName = objGroup._Groupname.ToString().Trim
            objGroup = Nothing
            'Pinkal (24-Jun-2024) -- End

            Call FillCombo()
            txtClaimNo.Enabled = False
            cboExpCategory.Enabled = False
            cboPeriod.Enabled = False
            dtpDate.Enabled = False
            objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
            Call GetValue()
            objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            'mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpCategory.SelectedValue), True, mintClaimApproverId, User._Object._Userunkid, "", mintClaimRequestMasterId).Tables(0)

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpCategory.SelectedValue), False, True, mintClaimApproverId, User._Object._Userunkid, "", mintClaimRequestMasterId).Tables(0)
            mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName _
                                                                                             , User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                                             , CInt(cboExpCategory.SelectedValue), False, True, mintClaimApproverId, "", mintClaimRequestMasterId).Tables(0)
            'Pinkal (24-Aug-2015) -- End


            'Pinkal (22-Jun-2015) -- End


            Call Fill_Expense()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCancelExpenseForm_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsclaim_request_approval_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsclaim_request_approval_tran"
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event(s) "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Valid() = False Then Exit Sub

            If mblnIsFromLeave Then
                mblnCancel = False
                Me.Close()
                Exit Sub
            End If

            objExpApproverTran._YearId = FinancialYear._Object._YearUnkid
            objExpApproverTran._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim blnFlag As Boolean = objExpApproverTran.CancelExpense(mdtTran, enExpenseType.EXP_NONE, txtCancelRemark.Text.Trim, User._Object._Userunkid)
            Dim blnFlag As Boolean = objExpApproverTran.CancelExpense(mdtTran, enExpenseType.EXP_NONE, txtCancelRemark.Text.Trim, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime)
            'Shani(24-Aug-2015) -- End


            If blnFlag Then
                'Pinkal (13-Jul-2015) -- Start
                'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objClaimMaster._Crmasterunkid = mintClaimApproverId
                objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
                'Sohail (30 Nov 2017) -- End
                objClaimMaster._EmployeeCode = objEmployee._Employeecode
                objClaimMaster._EmployeeFirstName = objEmployee._Firstname
                objClaimMaster._EmployeeMiddleName = objEmployee._Othername
                objClaimMaster._EmployeeSurName = objEmployee._Surname
                objClaimMaster._EmpMail = objEmployee._Email
                ' STATUSID = 6 CANCEL STATUS
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objClaimMaster.SendMailToEmployee(CInt(txtEmployee.Tag), txtClaimNo.Text.Trim, 6, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtCancelRemark.Text.Trim)

                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                'objClaimMaster.SendMailToEmployee(CInt(txtEmployee.Tag), txtClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtCancelRemark.Text.Trim)
                objClaimMaster.SendMailToEmployee(CInt(txtEmployee.Tag), txtClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtCancelRemark.Text.Trim, False)
                'Pinkal (20-May-2022) -- End

                'Sohail (30 Nov 2017) -- End
                'Pinkal (13-Jul-2015) -- End

                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If e.ColumnIndex = objcolhIsSelect.Index Then

                If dgvData.IsCurrentCellDirty Then
                    dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdtTran.AcceptChanges()
                End If

                If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsPosted.Index).Value) = True Then
                    dgvData.CurrentRow.Cells(objcolhIsSelect.Index).Value = False
                    dgvData.CurrentRow.Cells(objcolhIsSelect.Index).ReadOnly = True
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You cannot cancel this claim transaction.Reason:This claim transaction is already posted to current payroll period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                Else
                    dgvData.CurrentRow.Cells(objcolhIsSelect.Index).ReadOnly = False
                    SetCheckBoxValue()
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
                For Each dr As DataRow In mdtTran.Rows
                    If CBool(dr("isposted")) Then Continue For
                    dr("Ischeck") = chkSelectAll.Checked
                    dr.EndEdit()
                Next
                AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
                mdtTran.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbExpenseInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbExpenseInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbExpenseInformation.Text = Language._Object.getCaption(Me.gbExpenseInformation.Name, Me.gbExpenseInformation.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.Name, Me.lblExpCategory.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.Name, Me.lblGrandTotal.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblCancelRemark.Text = Language._Object.getCaption(Me.lblCancelRemark.Name, Me.lblCancelRemark.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.dgcolhExpense.HeaderText = Language._Object.getCaption(Me.dgcolhExpense.Name, Me.dgcolhExpense.HeaderText)
            Me.dgcolhUoM.HeaderText = Language._Object.getCaption(Me.dgcolhUoM.Name, Me.dgcolhUoM.HeaderText)
            Me.dgcolhQty.HeaderText = Language._Object.getCaption(Me.dgcolhQty.Name, Me.dgcolhQty.HeaderText)
            Me.dgcolhUnitPrice.HeaderText = Language._Object.getCaption(Me.dgcolhUnitPrice.Name, Me.dgcolhUnitPrice.HeaderText)
            Me.dgcolhBaseAmount.HeaderText = Language._Object.getCaption(Me.dgcolhBaseAmount.Name, Me.dgcolhBaseAmount.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhCurrency.HeaderText = Language._Object.getCaption(Me.dgcolhCurrency.Name, Me.dgcolhCurrency.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Cancel Remark cannot be blank.Cancel Remark is required information.")
            Language.setMessage(mstrModuleName, 2, "Please Select atleast One Expense to cancel.")
            Language.setMessage(mstrModuleName, 3, "You cannot cancel this claim transaction.Reason:This claim transaction is already posted to current payroll period.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class