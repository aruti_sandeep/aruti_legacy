﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmExpenseSecMappingAddEdit

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmExpenseSecMapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdvExpenseData As DataView
    Private mdvSectorData As DataView
    Dim objExpSecAssignment As clsassignexpense_sector

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function
#End Region

#Region " Form's Event "

    Private Sub frmExpenseSecMappingAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objExpSecAssignment = New clsassignexpense_sector
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeExpenses_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            'clsassignexpense_sector.SetMessages()
            objfrm._Other_ModuleNames = "clsassignexpense_sector"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End

            RemoveHandler cboExpType.SelectedValueChanged, AddressOf cboExpType_SelectedValueChanged

            With cboExpType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            AddHandler cboExpType.SelectedValueChanged, AddressOf cboExpType_SelectedValueChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim objExpense As New clsExpense_Master
        Dim objCommonmst As New clsCommon_Master

        Try
            Me.Cursor = Cursors.WaitCursor

            dsList = objExpense.getComboList(CInt(cboExpType.SelectedValue), False, "ExpList", 0, False, 0, "cmexpense_master.issecroutemandatory = 1")

            Dim column As New DataColumn
            column.DataType = System.Type.GetType("System.Boolean")
            column.DefaultValue = False
            column.ColumnName = "iCheck"

            dsList.Tables("ExpList").Columns.Add(column)

            dgExpenses.AutoGenerateColumns = False
            objdgcolhExpenseID.DataPropertyName = "Id"
            dgcolhExpense.DataPropertyName = "Name"
            dgcolhExpenseCode.DataPropertyName = "Code"
            objdgChkExpenseSelect.DataPropertyName = "iCheck"


            mdvExpenseData = dsList.Tables("ExpList").DefaultView
            dgExpenses.DataSource = mdvExpenseData

            dsList = objCommonmst.GetList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, "SectorList", , True)

            column = New DataColumn
            column.DataType = System.Type.GetType("System.Boolean")
            column.DefaultValue = False
            column.ColumnName = "iCheck"

            dsList.Tables("SectorList").Columns.Add(column)

            dgSector.AutoGenerateColumns = False
            objdgcolhExpenseID.DataPropertyName = "masterunkid"
            dgColhSector.DataPropertyName = "name"
            dgColhSectorCode.DataPropertyName = "Code"
            objdgChkSectorSelect.DataPropertyName = "iCheck"

            mdvSectorData = dsList.Tables("SectorList").DefaultView
            dgSector.DataSource = mdvSectorData

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Sub ChangeCheckState(ByVal sender As Object)
        Try
            Dim drRow As DataRow() = Nothing
            Select Case CType(sender, DataGridView).Name
                Case dgExpenses.Name
                    drRow = mdvExpenseData.ToTable.Select("iCheck = true")

                    If drRow.Length <= 0 Then
                        chkExpenseSelectAll.CheckState = CheckState.Unchecked
                    ElseIf drRow.Length < mdvExpenseData.ToTable().Rows.Count Then
                        chkExpenseSelectAll.CheckState = CheckState.Indeterminate
                    ElseIf mdvExpenseData.ToTable.Rows.Count = drRow.Length Then
                        chkExpenseSelectAll.CheckState = CheckState.Checked
                    End If

                Case dgSector.Name
                    drRow = mdvSectorData.ToTable.Select("iCheck = true")

                    If drRow.Length <= 0 Then
                        chkSectorSelectAll.CheckState = CheckState.Unchecked
                    ElseIf drRow.Length < mdvSectorData.ToTable.Rows.Count Then
                        chkSectorSelectAll.CheckState = CheckState.Indeterminate
                    ElseIf mdvSectorData.ToTable.Rows.Count = drRow.Length Then
                        chkSectorSelectAll.CheckState = CheckState.Checked
                    End If

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ChangeCheckState", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboExpType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is mandatory information. Please Select Expense Category to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            Dim dr() As DataRow = Nothing
            Dim drRow() As DataRow = Nothing

            If dgExpenses.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "There is no expense to Assign Sector/Route."), enMsgBoxStyle.Information)
                dgExpenses.Select()
                Exit Sub

            ElseIf dgSector.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no Sector/Route Assign to expense."), enMsgBoxStyle.Information)
                dgSector.Select()
                Exit Sub
            End If

            If mdvExpenseData IsNot Nothing Then
                dr = mdvExpenseData.ToTable().Select("iCheck=True")
                If dr.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Expense is compulsory information.Please check atleast one expense."), enMsgBoxStyle.Information)
                    dgExpenses.Select()
                    Exit Sub
                End If

            End If

            If mdvSectorData IsNot Nothing Then
                drRow = mdvSectorData.ToTable().Select("iCheck=True")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sector/Route is compulsory information.Please check atleast one Sector/Route."), enMsgBoxStyle.Information)
                    dgSector.Select()
                    Exit Sub
                End If
            End If

            Dim blnExists As Boolean = False
            Dim blnShown As Boolean = False


            Me.ControlBox = False
            Me.Enabled = False

            GC.Collect()

            Dim xCountSecRoute As Integer = 0
            Dim xCountEmp As Integer = 0

            Dim dtrdExpense As DataTableReader = dr.CopyToDataTable().CreateDataReader()

            While dtrdExpense.Read
                Application.DoEvents() : lnkExpenseProcess.Text = Language.getMessage(mstrModuleName, 7, "Expense Processed : ") & xCountEmp + 1 & "/" & dr.Length

                Dim dtrdSectorRoute As DataTableReader = drRow.CopyToDataTable().CreateDataReader()
                While dtrdSectorRoute.Read
                    If xCountSecRoute <= 0 Then Application.DoEvents() : lnkSecRouteProcess.Text = Language.getMessage(mstrModuleName, 6, "Sector/Route Processed : ") & xCountSecRoute & "/" & drRow.Length

                    If objExpSecAssignment.isExist(CInt(dtrdExpense("Id")), CInt(dtrdSectorRoute("masterunkid"))) Then
                        xCountSecRoute += 1
                        blnExists = True
                        Continue While
                    End If

                    objExpSecAssignment._Expensetypeid = CInt(cboExpType.SelectedValue)
                    objExpSecAssignment._Expenseunkid = CInt(dtrdExpense("Id"))
                    objExpSecAssignment._Secrouteunkid = CInt(dtrdSectorRoute("masterunkid"))
                    objExpSecAssignment._Userunkid = User._Object._Userunkid
                    objExpSecAssignment._Isvoid = False

                    blnFlag = objExpSecAssignment.Insert()

                    If blnFlag = False AndAlso objExpSecAssignment._Message <> "" Then
                        eZeeMsgBox.Show(objExpSecAssignment._Message & " - [ " & dtrdExpense("Name").ToString & " ] ", enMsgBoxStyle.Information)
                        Exit While
                    End If
                    xCountSecRoute += 1
                    Application.DoEvents() : lnkSecRouteProcess.Text = Language.getMessage(mstrModuleName, 8, "Sector/Route Processed : ") & xCountSecRoute & "/" & drRow.Length
                End While
                xCountEmp += 1
                xCountSecRoute = 0
            End While

            If blnExists Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Some of the checked expense(s) are already assigned the checked Sector/Route(s).") & vbCrLf & _
                                                    Language.getMessage(mstrModuleName, 10, "Due to this some expense(s) will be discarded from the Sector/Route(s) assignment process."), enMsgBoxStyle.Information)
                blnExists = False
                Call objbtnReset_Click(sender, e)
                Exit Sub
            End If

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Expense Sector/Route successfully assigned."), enMsgBoxStyle.Information)
                Call objbtnReset_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.ControlBox = True
            Me.Enabled = True
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            RemoveHandler cboExpType.Click, AddressOf cboExpType_SelectedValueChanged
            cboExpType.SelectedValue = 0
            AddHandler cboExpType.Click, AddressOf cboExpType_SelectedValueChanged

            txtExpenseSearch.Text = ""
            txtSectorSearch.Text = ""
            lnkExpenseProcess.Text = ""
            lnkSecRouteProcess.Text = ""

            chkExpenseSelectAll.Checked = False
            chkSectorSelectAll.Checked = False

            dgExpenses.DataSource = Nothing
            dgSector.DataSource = Nothing
            cboExpType.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox's Event "

    Private Sub txtExpenseSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExpenseSearch.TextChanged, _
                                                                                                                 txtSectorSearch.TextChanged
        Try
            Select Case CType(sender, eZee.TextBox.AlphanumericTextBox).Name
                Case txtExpenseSearch.Name

                    mdvExpenseData.RowFilter = dgcolhExpenseCode.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtExpenseSearch.Text) & "%' OR " & _
                                               dgcolhExpense.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtExpenseSearch.Text) & "%' "
                    ChangeCheckState(dgExpenses)
                Case txtSectorSearch.Name
                    mdvSectorData.RowFilter = dgColhSector.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSectorSearch.Text) & "%' OR " & _
                                              dgColhSectorCode.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSectorSearch.Text) & "%' "
                    ChangeCheckState(dgSector)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSecRouteSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox's Event "

    Private Sub chkExpenseSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExpenseSelectAll.CheckedChanged
        Try
            If mdvExpenseData Is Nothing Then Exit Sub
            RemoveHandler dgExpenses.CellContentClick, AddressOf dgExpenses_CellContentClick
            For Each dr As DataRowView In mdvExpenseData
                dr.Item(objdgChkExpenseSelect.DataPropertyName) = CBool(chkExpenseSelectAll.CheckState)
            Next
            dgExpenses.Refresh()
            AddHandler dgExpenses.CellContentClick, AddressOf dgExpenses_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkSectorSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSectorSelectAll.CheckedChanged
        Try
            If mdvSectorData Is Nothing Then Exit Sub
            RemoveHandler dgSector.CellContentClick, AddressOf dgSector_CellContentClick
            For Each dr As DataRowView In mdvSectorData
                dr.Item(objdgChkSectorSelect.DataPropertyName) = CBool(chkSectorSelectAll.CheckState)
            Next
            dgSector.Refresh()
            AddHandler dgSector.CellContentClick, AddressOf dgSector_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Private Sub cboExpType_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpType.SelectedValueChanged
        Try
            If CInt(cboExpType.SelectedValue) > 0 Then
                objbtnSearch_Click(Nothing, e)
            Else
                objbtnReset_Click(Nothing, e)
            End If
            chkExpenseSelectAll.Checked = False
            chkSectorSelectAll.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpType_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Gridview's Event "

    Private Sub dgExpenses_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgExpenses.CellContentClick
        Try
            RemoveHandler chkExpenseSelectAll.CheckedChanged, AddressOf chkExpenseSelectAll_CheckedChanged
            If e.ColumnIndex = objdgChkExpenseSelect.Index Then
                If Me.dgExpenses.IsCurrentCellDirty Then
                    Me.dgExpenses.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdvExpenseData.Table.AcceptChanges()
                End If
                ChangeCheckState(sender)
            End If
            AddHandler chkExpenseSelectAll.CheckedChanged, AddressOf chkExpenseSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgExpenses_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgSector_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgSector.CellContentClick
        Try
            RemoveHandler chkSectorSelectAll.CheckedChanged, AddressOf chkSectorSelectAll_CheckedChanged
            If e.ColumnIndex = objdgChkSectorSelect.Index Then
                If Me.dgSector.IsCurrentCellDirty Then
                    Me.dgSector.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdvSectorData.Table.AcceptChanges()
                End If
                ChangeCheckState(sender)
            End If
            AddHandler chkSectorSelectAll.CheckedChanged, AddressOf chkSectorSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgExpenses_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLeaveInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLeaveInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblExpenseType.Text = Language._Object.getCaption(Me.lblExpenseType.Name, Me.lblExpenseType.Text)
			Me.gbLeaveInfo.Text = Language._Object.getCaption(Me.gbLeaveInfo.Name, Me.gbLeaveInfo.Text)
			Me.chkExpenseSelectAll.Text = Language._Object.getCaption(Me.chkExpenseSelectAll.Name, Me.chkExpenseSelectAll.Text)
			Me.chkSectorSelectAll.Text = Language._Object.getCaption(Me.chkSectorSelectAll.Name, Me.chkSectorSelectAll.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhExpenseCode.HeaderText = Language._Object.getCaption(Me.dgcolhExpenseCode.Name, Me.dgcolhExpenseCode.HeaderText)
			Me.dgcolhExpense.HeaderText = Language._Object.getCaption(Me.dgcolhExpense.Name, Me.dgcolhExpense.HeaderText)
			Me.dgColhSectorCode.HeaderText = Language._Object.getCaption(Me.dgColhSectorCode.Name, Me.dgColhSectorCode.HeaderText)
			Me.dgColhSector.HeaderText = Language._Object.getCaption(Me.dgColhSector.Name, Me.dgColhSector.HeaderText)
			Me.lnkExpenseProcess.Text = Language._Object.getCaption(Me.lnkExpenseProcess.Name, Me.lnkExpenseProcess.Text)
			Me.lnkSecRouteProcess.Text = Language._Object.getCaption(Me.lnkSecRouteProcess.Name, Me.lnkSecRouteProcess.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expense Category is mandatory information. Please Select Expense Category to continue.")
			Language.setMessage(mstrModuleName, 2, "There is no expense to Assign Sector/Route.")
			Language.setMessage(mstrModuleName, 3, "There is no Sector/Route Assign to expense.")
			Language.setMessage(mstrModuleName, 4, "Expense is compulsory information.Please check atleast one expense.")
			Language.setMessage(mstrModuleName, 5, "Sector/Route is compulsory information.Please check atleast one Sector/Route.")
			Language.setMessage(mstrModuleName, 6, "Sector/Route Processed :")
			Language.setMessage(mstrModuleName, 7, "Expense Processed :")
			Language.setMessage(mstrModuleName, 8, "Sector/Route Processed :")
			Language.setMessage(mstrModuleName, 9, "Sorry, Some of the checked expense(s) are already assigned the checked Sector/Route(s).")
			Language.setMessage(mstrModuleName, 10, "Due to this some expense(s) will be discarded from the Sector/Route(s) assignment process.")
			Language.setMessage(mstrModuleName, 11, "Expense Sector/Route successfully assigned.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class