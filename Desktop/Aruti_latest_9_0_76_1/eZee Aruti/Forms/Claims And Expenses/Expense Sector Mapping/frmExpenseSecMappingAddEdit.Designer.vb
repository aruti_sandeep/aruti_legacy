﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpenseSecMappingAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExpenseSecMappingAddEdit))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboExpType = New System.Windows.Forms.ComboBox
        Me.lblExpenseType = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.gbLeaveInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.chkExpenseSelectAll = New System.Windows.Forms.CheckBox
        Me.dgExpenses = New System.Windows.Forms.DataGridView
        Me.objdgChkExpenseSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhExpenseCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExpense = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhExpenseID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtExpenseSearch = New eZee.TextBox.AlphanumericTextBox
        Me.chkSectorSelectAll = New System.Windows.Forms.CheckBox
        Me.dgSector = New System.Windows.Forms.DataGridView
        Me.objdgChkSectorSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgColhSectorCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhSector = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgColhSectorId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSectorSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lnkExpenseProcess = New System.Windows.Forms.LinkLabel
        Me.lnkSecRouteProcess = New System.Windows.Forms.LinkLabel
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLeaveInfo.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dgExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgSector, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboExpType)
        Me.gbFilterCriteria.Controls.Add(Me.lblExpenseType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(715, 58)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpType
        '
        Me.cboExpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpType.DropDownWidth = 300
        Me.cboExpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpType.FormattingEnabled = True
        Me.cboExpType.Location = New System.Drawing.Point(95, 30)
        Me.cboExpType.Name = "cboExpType"
        Me.cboExpType.Size = New System.Drawing.Size(216, 21)
        Me.cboExpType.TabIndex = 324
        '
        'lblExpenseType
        '
        Me.lblExpenseType.BackColor = System.Drawing.Color.Transparent
        Me.lblExpenseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseType.Location = New System.Drawing.Point(7, 33)
        Me.lblExpenseType.Name = "lblExpenseType"
        Me.lblExpenseType.Size = New System.Drawing.Size(82, 15)
        Me.lblExpenseType.TabIndex = 323
        Me.lblExpenseType.Text = "Expense Cat."
        Me.lblExpenseType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(688, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(665, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'gbLeaveInfo
        '
        Me.gbLeaveInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.Checked = False
        Me.gbLeaveInfo.CollapseAllExceptThis = False
        Me.gbLeaveInfo.CollapsedHoverImage = Nothing
        Me.gbLeaveInfo.CollapsedNormalImage = Nothing
        Me.gbLeaveInfo.CollapsedPressedImage = Nothing
        Me.gbLeaveInfo.CollapseOnLoad = False
        Me.gbLeaveInfo.Controls.Add(Me.SplitContainer1)
        Me.gbLeaveInfo.ExpandedHoverImage = Nothing
        Me.gbLeaveInfo.ExpandedNormalImage = Nothing
        Me.gbLeaveInfo.ExpandedPressedImage = Nothing
        Me.gbLeaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveInfo.HeaderHeight = 25
        Me.gbLeaveInfo.HeaderMessage = ""
        Me.gbLeaveInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.HeightOnCollapse = 0
        Me.gbLeaveInfo.LeftTextSpace = 0
        Me.gbLeaveInfo.Location = New System.Drawing.Point(0, 60)
        Me.gbLeaveInfo.Name = "gbLeaveInfo"
        Me.gbLeaveInfo.OpenHeight = 300
        Me.gbLeaveInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveInfo.ShowBorder = True
        Me.gbLeaveInfo.ShowCheckBox = False
        Me.gbLeaveInfo.ShowCollapseButton = False
        Me.gbLeaveInfo.ShowDefaultBorderColor = True
        Me.gbLeaveInfo.ShowDownButton = False
        Me.gbLeaveInfo.ShowHeader = True
        Me.gbLeaveInfo.Size = New System.Drawing.Size(715, 360)
        Me.gbLeaveInfo.TabIndex = 296
        Me.gbLeaveInfo.Temp = 0
        Me.gbLeaveInfo.Text = "Assignment Info"
        Me.gbLeaveInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 26)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkExpenseSelectAll)
        Me.SplitContainer1.Panel1.Controls.Add(Me.dgExpenses)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtExpenseSearch)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.chkSectorSelectAll)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgSector)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtSectorSearch)
        Me.SplitContainer1.Size = New System.Drawing.Size(709, 331)
        Me.SplitContainer1.SplitterDistance = 354
        Me.SplitContainer1.TabIndex = 297
        '
        'chkExpenseSelectAll
        '
        Me.chkExpenseSelectAll.AutoSize = True
        Me.chkExpenseSelectAll.Location = New System.Drawing.Point(7, 27)
        Me.chkExpenseSelectAll.Name = "chkExpenseSelectAll"
        Me.chkExpenseSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkExpenseSelectAll.TabIndex = 295
        Me.chkExpenseSelectAll.UseVisualStyleBackColor = True
        '
        'dgExpenses
        '
        Me.dgExpenses.AllowUserToAddRows = False
        Me.dgExpenses.AllowUserToDeleteRows = False
        Me.dgExpenses.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgExpenses.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgExpenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgExpenses.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgChkExpenseSelect, Me.dgcolhExpenseCode, Me.dgcolhExpense, Me.objdgcolhExpenseID})
        Me.dgExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgExpenses.Location = New System.Drawing.Point(0, 21)
        Me.dgExpenses.Name = "dgExpenses"
        Me.dgExpenses.RowHeadersVisible = False
        Me.dgExpenses.Size = New System.Drawing.Size(352, 308)
        Me.dgExpenses.TabIndex = 292
        '
        'objdgChkExpenseSelect
        '
        Me.objdgChkExpenseSelect.Frozen = True
        Me.objdgChkExpenseSelect.HeaderText = ""
        Me.objdgChkExpenseSelect.Name = "objdgChkExpenseSelect"
        Me.objdgChkExpenseSelect.Width = 25
        '
        'dgcolhExpenseCode
        '
        Me.dgcolhExpenseCode.HeaderText = "Code"
        Me.dgcolhExpenseCode.Name = "dgcolhExpenseCode"
        '
        'dgcolhExpense
        '
        Me.dgcolhExpense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhExpense.HeaderText = "Expenses"
        Me.dgcolhExpense.Name = "dgcolhExpense"
        Me.dgcolhExpense.ReadOnly = True
        Me.dgcolhExpense.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'objdgcolhExpenseID
        '
        Me.objdgcolhExpenseID.HeaderText = "ExpenseId"
        Me.objdgcolhExpenseID.Name = "objdgcolhExpenseID"
        Me.objdgcolhExpenseID.ReadOnly = True
        Me.objdgcolhExpenseID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhExpenseID.Visible = False
        '
        'txtExpenseSearch
        '
        Me.txtExpenseSearch.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtExpenseSearch.Flags = 0
        Me.txtExpenseSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpenseSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtExpenseSearch.Location = New System.Drawing.Point(0, 0)
        Me.txtExpenseSearch.Name = "txtExpenseSearch"
        Me.txtExpenseSearch.Size = New System.Drawing.Size(352, 21)
        Me.txtExpenseSearch.TabIndex = 291
        '
        'chkSectorSelectAll
        '
        Me.chkSectorSelectAll.AutoSize = True
        Me.chkSectorSelectAll.Location = New System.Drawing.Point(7, 27)
        Me.chkSectorSelectAll.Name = "chkSectorSelectAll"
        Me.chkSectorSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSectorSelectAll.TabIndex = 293
        Me.chkSectorSelectAll.UseVisualStyleBackColor = True
        '
        'dgSector
        '
        Me.dgSector.AllowUserToAddRows = False
        Me.dgSector.AllowUserToDeleteRows = False
        Me.dgSector.AllowUserToResizeRows = False
        Me.dgSector.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgSector.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgSector.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgSector.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgChkSectorSelect, Me.dgColhSectorCode, Me.dgColhSector, Me.objdgColhSectorId})
        Me.dgSector.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgSector.Location = New System.Drawing.Point(0, 21)
        Me.dgSector.Name = "dgSector"
        Me.dgSector.RowHeadersVisible = False
        Me.dgSector.RowHeadersWidth = 5
        Me.dgSector.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgSector.Size = New System.Drawing.Size(349, 308)
        Me.dgSector.TabIndex = 294
        '
        'objdgChkSectorSelect
        '
        Me.objdgChkSectorSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgChkSectorSelect.Frozen = True
        Me.objdgChkSectorSelect.HeaderText = ""
        Me.objdgChkSectorSelect.Name = "objdgChkSectorSelect"
        Me.objdgChkSectorSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgChkSectorSelect.Width = 25
        '
        'dgColhSectorCode
        '
        Me.dgColhSectorCode.HeaderText = "Code"
        Me.dgColhSectorCode.Name = "dgColhSectorCode"
        '
        'dgColhSector
        '
        Me.dgColhSector.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhSector.HeaderText = "Sector / Route"
        Me.dgColhSector.Name = "dgColhSector"
        Me.dgColhSector.ReadOnly = True
        Me.dgColhSector.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhSector.Width = 220
        '
        'objdgColhSectorId
        '
        Me.objdgColhSectorId.HeaderText = "SectorId"
        Me.objdgColhSectorId.Name = "objdgColhSectorId"
        Me.objdgColhSectorId.Visible = False
        '
        'txtSectorSearch
        '
        Me.txtSectorSearch.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtSectorSearch.Flags = 0
        Me.txtSectorSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSectorSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSectorSearch.Location = New System.Drawing.Point(0, 0)
        Me.txtSectorSearch.Name = "txtSectorSearch"
        Me.txtSectorSearch.Size = New System.Drawing.Size(349, 21)
        Me.txtSectorSearch.TabIndex = 290
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkExpenseProcess)
        Me.objFooter.Controls.Add(Me.lnkSecRouteProcess)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 422)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(715, 55)
        Me.objFooter.TabIndex = 297
        '
        'lnkExpenseProcess
        '
        Me.lnkExpenseProcess.AutoSize = True
        Me.lnkExpenseProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkExpenseProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkExpenseProcess.Location = New System.Drawing.Point(252, 9)
        Me.lnkExpenseProcess.Name = "lnkExpenseProcess"
        Me.lnkExpenseProcess.Size = New System.Drawing.Size(0, 13)
        Me.lnkExpenseProcess.TabIndex = 5
        '
        'lnkSecRouteProcess
        '
        Me.lnkSecRouteProcess.AutoSize = True
        Me.lnkSecRouteProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSecRouteProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSecRouteProcess.Location = New System.Drawing.Point(252, 30)
        Me.lnkSecRouteProcess.Name = "lnkSecRouteProcess"
        Me.lnkSecRouteProcess.Size = New System.Drawing.Size(0, 13)
        Me.lnkSecRouteProcess.TabIndex = 4
        Me.lnkSecRouteProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(503, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(606, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmExpenseSecMappingAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 477)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbLeaveInfo)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseSecMappingAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense Sector Mapping Add / Edit"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLeaveInfo.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dgExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgSector, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboExpType As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpenseType As System.Windows.Forms.Label
    Friend WithEvents gbLeaveInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents chkExpenseSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgExpenses As System.Windows.Forms.DataGridView
    Friend WithEvents txtExpenseSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkSectorSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtSectorSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgSector As System.Windows.Forms.DataGridView
    Friend WithEvents objSecRouteSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgChkExpenseSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhExpenseCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExpense As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhExpenseID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgChkSectorSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgColhSectorCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhSector As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgColhSectorId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkExpenseProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSecRouteProcess As System.Windows.Forms.LinkLabel
End Class
