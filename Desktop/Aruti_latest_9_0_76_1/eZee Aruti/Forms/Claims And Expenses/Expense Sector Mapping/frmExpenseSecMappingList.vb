﻿Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmExpenseSecMappingList

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmExpenseSecMappingList"
    Private objExpSector As clsassignexpense_sector
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private dtTab As DataTable
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
#End Region

#Region " Form's Event "

    Private Sub frmExpenseSecMappingList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objExpSector = New clsassignexpense_sector
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpenseSecMappingList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event's "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmExpenseSecMappingAddEdit
        Try

            If User._Object._Isrighttoleft Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(enAction.ADD_CONTINUE) Then
                Call FillGrid()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dtTab IsNot Nothing Then
                Dim dtmp() As DataRow = dtTab.Select("ischeck=true and isgrp=false")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please tick atleast one sector/route assigned to expense."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim blnUsed As Boolean = False
                Dim blnShown As Boolean = False
                Dim mstrVoidReason As String = String.Empty
                For i As Integer = 0 To dtmp.Length - 1
                    If blnShown = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction.") & vbCrLf & _
                                                            Language.getMessage(mstrModuleName, 3, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        frm.displayDialog(enVoidCategoryType.OTHERS, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        blnShown = True
                    End If
                    If objExpSector.isUsed(CInt(dtmp(i).Item("expenseunkid")), CInt(dtmp(i).Item("sectorId"))) = True Then
                        dgExpSecroute.Rows(dtTab.Rows.IndexOf(dtmp(i))).DefaultCellStyle.ForeColor = Color.Red
                        dgExpSecroute.Rows(dtTab.Rows.IndexOf(dtmp(i))).DefaultCellStyle.SelectionForeColor = Color.Red
                        blnUsed = True
                    Else
                        Me.Cursor = Cursors.WaitCursor
                        objExpSector._Crexpsectorrouteunkid = CInt(dtmp(i).Item("crexpsectorrouteunkid"))

                        objExpSector._Isvoid = True
                        objExpSector._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objExpSector._Voidreason = mstrVoidReason
                        objExpSector._Voiduserunkid = User._Object._Userunkid

                        If objExpSector.Delete(CInt(dtmp(i).Item("crexpsectorrouteunkid"))) = False Then
                            eZeeMsgBox.Show(objExpSector._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                Next
                If blnUsed = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, some of the ticked sector/route are already in used,therefor cannot be deleted and highlighted in red."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboExpType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Expense Category is compulsory information.Please select expense category to do further opeation on it."), enMsgBoxStyle.Information)
                cboExpType.Select()
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboExpType.SelectedValue = 0
            cboExpense.SelectedValue = 0
            cboSecRoute.SelectedValue = 0
            If dtTab IsNot Nothing Then dtTab.Rows.Clear()
            dgExpSecroute.DataSource = Nothing
            If dgExpSecroute.RowCount <= 0 Then chkSelectAll.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click, objbtnSearchSector.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim cbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchExpense.Name.ToUpper
                    cbo = cboExpense
                Case objbtnSearchSector.Name.ToUpper
                    cbo = cboSecRoute
            End Select
            If cbo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cbo.ValueMember
                .DisplayMember = cbo.DisplayMember
                .DataSource = CType(cbo.DataSource, DataTable)

                If .DisplayDialog = True Then
                    cbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End

            With cboExpType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            Dim objCMaster As New clsCommon_Master
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSecRoute
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objCMaster = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Try
            Me.Cursor = Cursors.WaitCursor
            dsList = objExpSector.GetList("List", CInt(cboExpType.SelectedValue), CInt(cboExpense.SelectedValue), CInt(cboSecRoute.SelectedValue))
            dtTab = dsList.Tables(0)

            Call SetDataSource()
            If dgExpSecroute.RowCount <= 0 Then
                chkSelectAll.Checked = False
                chkSelectAll.Enabled = False
            Else
                chkSelectAll.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dr = From dgdr As DataGridViewRow In dgExpSecroute.Rows.Cast(Of DataGridViewRow)() Where CBool(dgdr.Cells(objdgcolhIsGrp.Index).Value) = True Select dgdr
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetDataSource()
        Try
            dgExpSecroute.AutoGenerateColumns = False
            objdgcolhExpSectorunkid.DataPropertyName = "crexpsectorrouteunkid"
            objdgcolhGrpId.DataPropertyName = "grpid"
            objdgcolhIsGrp.DataPropertyName = "isgrp"
            objdgSelect.DataPropertyName = "ischeck"
            dgcolhSectorRoute.DataPropertyName = "Sector"
            objdgcolhExpenseID.DataPropertyName = "expenseunkid"
            objdgcolhSectorID.DataPropertyName = "sectorId"
            dgExpSecroute.DataSource = dtTab
            Call SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal mblnIsGroup As Boolean) As Boolean
        Try
            If mblnIsGroup Then
                Dim dgvcsHeader As New DataGridViewCellStyle
                dgvcsHeader.ForeColor = Color.White
                dgvcsHeader.SelectionBackColor = Color.Gray
                dgvcsHeader.SelectionForeColor = Color.White
                dgvcsHeader.BackColor = Color.Gray
                dgvcsHeader.Font = New Font(dgExpSecroute.Font.FontFamily, dgExpSecroute.Font.Size, FontStyle.Bold)
                xRow.DefaultCellStyle = dgvcsHeader

                If xRow.Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                    xRow.Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
    End Function

    Private Function UpdateAll(ByVal dr As DataRow, ByVal mblnisCheck As Boolean) As Boolean
        Try
            If dr Is Nothing Then Return False
            dr("ischeck") = mblnisCheck
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateAll", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "Combobox Events"

    Private Sub cboExpType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpType.SelectedIndexChanged
        Try
            Dim objExpense As New clsExpense_Master
            Dim dsList As DataSet = objExpense.getComboList(CInt(cboExpType.SelectedValue), True, "ExpList", 0, False, 0, "")
            With cboExpense
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsList.Tables(0)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Checkbox Event's "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dtTab Is Nothing Then Exit Sub
            RemoveHandler dgExpSecroute.CellContentClick, AddressOf dgExpSecroute_CellContentClick
            If dtTab.Rows.Count > 0 Then
                Me.Cursor = Cursors.WaitCursor
                Dim dr = dtTab.AsEnumerable()
                dr.ToList().ForEach(Function(x) UpdateAll(x, chkSelectAll.Checked))
            End If
            AddHandler dgExpSecroute.CellContentClick, AddressOf dgExpSecroute_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DataGrid Event's "

    Private Sub dgExpSecroute_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgExpSecroute.CellContentClick, dgExpSecroute.CellContentDoubleClick
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgExpSecroute.IsCurrentCellDirty Then
                Me.dgExpSecroute.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgExpSecroute.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case objdgSelect.Index
                        For i = e.RowIndex + 1 To dgExpSecroute.RowCount - 1
                            Dim blnFlg As Boolean = False
                            blnFlg = CBool(dgExpSecroute.Rows(e.RowIndex).Cells(objdgSelect.Index).Value)
                            If CInt(dgExpSecroute.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgExpSecroute.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgExpSecroute.Rows(i).Cells(objdgSelect.Index).Value = blnFlg
                            End If
                        Next
                    Case objdgcolhCollapse.Index
                        If dgExpSecroute.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgExpSecroute.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgExpSecroute.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgExpSecroute.RowCount - 1
                            If CInt(dgExpSecroute.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgExpSecroute.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgExpSecroute.Rows(i).Visible = False Then
                                    dgExpSecroute.Rows(i).Visible = True
                                Else
                                    dgExpSecroute.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgExpSecroute_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblExpenseType.Text = Language._Object.getCaption(Me.lblExpenseType.Name, Me.lblExpenseType.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.dgcolhSectorRoute.HeaderText = Language._Object.getCaption(Me.dgcolhSectorRoute.Name, Me.dgcolhSectorRoute.HeaderText)
			Me.LblSecRoute.Text = Language._Object.getCaption(Me.LblSecRoute.Name, Me.LblSecRoute.Text)
			Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.Name, Me.LblExpense.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Please tick atleast one sector/route assigned to expense.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction.")
			Language.setMessage(mstrModuleName, 3, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 4, "Expense Category is compulsory information.Please select expense category to do further opeation on it.")
			Language.setMessage(mstrModuleName, 5, "Sorry, some of the ticked sector/route are already in used,therefor cannot be deleted and highlighted in red.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class