﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpense_CostingList

#Region " Private Varaibles "

    Private objCosting As clsExpenseCosting
    Private ReadOnly mstrModuleName As String = "frmExpense_CostingList"

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddSectorRouteCost
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditSectorRouteCost
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteSectorRouteCost
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSecRoute
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim strSearch As String = ""
        Try


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            If User._Object.Privilege._AllowtoViewSectorRouteCostList = False Then Exit Sub
            'Pinkal (13-Jul-2015) -- End

            dsList = objCosting.GetList("List")

            If CInt(cboSecRoute.SelectedValue) > 0 Then
                strSearch &= "AND secrouteunkid = '" & CInt(cboSecRoute.SelectedValue) & "' "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtTable = New DataView(dsList.Tables("List"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("List")
            End If

            lvCostingList.Items.Clear()

            For Each dRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dRow("secroute").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(dRow("effdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(Format(CDec(dRow("amount").ToString), GUI.fmtCurrency))

                lvItem.Tag = dRow("costingunkid")
                lvCostingList.Items.Add(lvItem)
            Next

            If lvCostingList.Items.Count > 11 Then
                colhSecRoute.Width = 350 - 20
            Else
                colhSecRoute.Width = 350
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExpense_CostingList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvCostingList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpense_CostingList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmExpense_CostingList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpense_CostingList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpense_CostingList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCosting = New clsExpenseCosting
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call FillCombo()

            If lvCostingList.Items.Count > 0 Then lvCostingList.Items(0).Selected = True
            lvCostingList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpense_CostingList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpense_CostingList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objCosting = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseCosting.SetMessages()
            objfrm._Other_ModuleNames = "clsExpenseCosting"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmExpenseCostingAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvCostingList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Costing from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCostingList.Select()
            Exit Sub
        End If
        Dim frm As New frmExpenseCostingAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvCostingList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvCostingList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Costing from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCostingList.Select()
            Exit Sub
        End If
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Costing?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.OTHERS, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                objCosting._Isvoid = True
                objCosting._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objCosting._Voidreason = mstrVoidReason
                objCosting._Voiduserunkid = User._Object._Userunkid

                objCosting.Delete(CInt(lvCostingList.SelectedItems(0).Tag))
                If objCosting._Message <> "" Then
                    eZeeMsgBox.Show(objCosting._Message, enMsgBoxStyle.Information)
                Else
                    lvCostingList.SelectedItems(0).Remove()
                End If
            End If
            lvCostingList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboSecRoute.SelectedValue = 0
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRoute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRoute.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboSecRoute.ValueMember
                .DisplayMember = cboSecRoute.DisplayMember
                .DataSource = CType(cboSecRoute.DataSource, DataTable)
                If .DisplayDialog Then
                    cboSecRoute.SelectedValue = .SelectedValue
                    cboSecRoute.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRoute_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblSector_Route.Text = Language._Object.getCaption(Me.lblSector_Route.Name, Me.lblSector_Route.Text)
            Me.colhSecRoute.Text = Language._Object.getCaption(CStr(Me.colhSecRoute.Tag), Me.colhSecRoute.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Costing from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Costing?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class