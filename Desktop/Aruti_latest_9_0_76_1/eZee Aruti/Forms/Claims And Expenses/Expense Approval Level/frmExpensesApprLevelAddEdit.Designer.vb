﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpensesApprLevelAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExpensesApprLevelAddEdit))
        Me.gbExpenseInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPRemark = New System.Windows.Forms.Label
        Me.nudPriority = New System.Windows.Forms.NumericUpDown
        Me.lblPriority = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblLevelName = New System.Windows.Forms.Label
        Me.lblLevelCode = New System.Windows.Forms.Label
        Me.cboExCategory = New System.Windows.Forms.ComboBox
        Me.lblExpenseCat = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.LblMaxRange = New System.Windows.Forms.Label
        Me.nudMaximumRange = New System.Windows.Forms.NumericUpDown
        Me.nudMinimumRange = New System.Windows.Forms.NumericUpDown
        Me.LblMinRange = New System.Windows.Forms.Label
        Me.gbExpenseInformation.SuspendLayout()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.nudMaximumRange, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMinimumRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbExpenseInformation
        '
        Me.gbExpenseInformation.BorderColor = System.Drawing.Color.Black
        Me.gbExpenseInformation.Checked = False
        Me.gbExpenseInformation.CollapseAllExceptThis = False
        Me.gbExpenseInformation.CollapsedHoverImage = Nothing
        Me.gbExpenseInformation.CollapsedNormalImage = Nothing
        Me.gbExpenseInformation.CollapsedPressedImage = Nothing
        Me.gbExpenseInformation.CollapseOnLoad = False
        Me.gbExpenseInformation.Controls.Add(Me.LblMaxRange)
        Me.gbExpenseInformation.Controls.Add(Me.nudMaximumRange)
        Me.gbExpenseInformation.Controls.Add(Me.nudMinimumRange)
        Me.gbExpenseInformation.Controls.Add(Me.LblMinRange)
        Me.gbExpenseInformation.Controls.Add(Me.lblPRemark)
        Me.gbExpenseInformation.Controls.Add(Me.nudPriority)
        Me.gbExpenseInformation.Controls.Add(Me.lblPriority)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbExpenseInformation.Controls.Add(Me.txtName)
        Me.gbExpenseInformation.Controls.Add(Me.txtCode)
        Me.gbExpenseInformation.Controls.Add(Me.lblLevelName)
        Me.gbExpenseInformation.Controls.Add(Me.lblLevelCode)
        Me.gbExpenseInformation.Controls.Add(Me.cboExCategory)
        Me.gbExpenseInformation.Controls.Add(Me.lblExpenseCat)
        Me.gbExpenseInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbExpenseInformation.ExpandedHoverImage = Nothing
        Me.gbExpenseInformation.ExpandedNormalImage = Nothing
        Me.gbExpenseInformation.ExpandedPressedImage = Nothing
        Me.gbExpenseInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExpenseInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExpenseInformation.HeaderHeight = 25
        Me.gbExpenseInformation.HeaderMessage = ""
        Me.gbExpenseInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExpenseInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExpenseInformation.HeightOnCollapse = 0
        Me.gbExpenseInformation.LeftTextSpace = 0
        Me.gbExpenseInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbExpenseInformation.Name = "gbExpenseInformation"
        Me.gbExpenseInformation.OpenHeight = 300
        Me.gbExpenseInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExpenseInformation.ShowBorder = True
        Me.gbExpenseInformation.ShowCheckBox = False
        Me.gbExpenseInformation.ShowCollapseButton = False
        Me.gbExpenseInformation.ShowDefaultBorderColor = True
        Me.gbExpenseInformation.ShowDownButton = False
        Me.gbExpenseInformation.ShowHeader = True
        Me.gbExpenseInformation.Size = New System.Drawing.Size(381, 198)
        Me.gbExpenseInformation.TabIndex = 0
        Me.gbExpenseInformation.Temp = 0
        Me.gbExpenseInformation.Text = "Approval Levels"
        Me.gbExpenseInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPRemark
        '
        Me.lblPRemark.ForeColor = System.Drawing.Color.Red
        Me.lblPRemark.Location = New System.Drawing.Point(172, 119)
        Me.lblPRemark.Name = "lblPRemark"
        Me.lblPRemark.Size = New System.Drawing.Size(195, 13)
        Me.lblPRemark.TabIndex = 327
        Me.lblPRemark.Text = "Higher Priority means High Level."
        '
        'nudPriority
        '
        Me.nudPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPriority.Location = New System.Drawing.Point(118, 115)
        Me.nudPriority.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudPriority.Name = "nudPriority"
        Me.nudPriority.Size = New System.Drawing.Size(50, 21)
        Me.nudPriority.TabIndex = 8
        Me.nudPriority.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudPriority.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPriority
        '
        Me.lblPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPriority.Location = New System.Drawing.Point(12, 117)
        Me.lblPriority.Name = "lblPriority"
        Me.lblPriority.Size = New System.Drawing.Size(100, 17)
        Me.lblPriority.TabIndex = 7
        Me.lblPriority.Text = "Priority"
        Me.lblPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(346, 88)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 6
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(118, 88)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(223, 21)
        Me.txtName.TabIndex = 5
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(118, 61)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(159, 21)
        Me.txtCode.TabIndex = 3
        '
        'lblLevelName
        '
        Me.lblLevelName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelName.Location = New System.Drawing.Point(12, 90)
        Me.lblLevelName.Name = "lblLevelName"
        Me.lblLevelName.Size = New System.Drawing.Size(100, 17)
        Me.lblLevelName.TabIndex = 4
        Me.lblLevelName.Text = "Level Name"
        Me.lblLevelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLevelCode
        '
        Me.lblLevelCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelCode.Location = New System.Drawing.Point(12, 63)
        Me.lblLevelCode.Name = "lblLevelCode"
        Me.lblLevelCode.Size = New System.Drawing.Size(100, 17)
        Me.lblLevelCode.TabIndex = 2
        Me.lblLevelCode.Text = "Level Code"
        Me.lblLevelCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExCategory
        '
        Me.cboExCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExCategory.FormattingEnabled = True
        Me.cboExCategory.Location = New System.Drawing.Point(118, 34)
        Me.cboExCategory.Name = "cboExCategory"
        Me.cboExCategory.Size = New System.Drawing.Size(219, 21)
        Me.cboExCategory.TabIndex = 1
        '
        'lblExpenseCat
        '
        Me.lblExpenseCat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseCat.Location = New System.Drawing.Point(12, 36)
        Me.lblExpenseCat.Name = "lblExpenseCat"
        Me.lblExpenseCat.Size = New System.Drawing.Size(100, 17)
        Me.lblExpenseCat.TabIndex = 0
        Me.lblExpenseCat.Text = "Expense Category"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 198)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(381, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(169, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(272, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'LblMaxRange
        '
        Me.LblMaxRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMaxRange.Location = New System.Drawing.Point(12, 172)
        Me.LblMaxRange.Name = "LblMaxRange"
        Me.LblMaxRange.Size = New System.Drawing.Size(100, 17)
        Me.LblMaxRange.TabIndex = 11
        Me.LblMaxRange.Text = "Max.Range"
        '
        'nudMaximumRange
        '
        Me.nudMaximumRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudMaximumRange.DecimalPlaces = 2
        Me.nudMaximumRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaximumRange.Location = New System.Drawing.Point(118, 170)
        Me.nudMaximumRange.Maximum = New Decimal(New Integer() {1569325055, 23283064, 0, 0})
        Me.nudMaximumRange.Name = "nudMaximumRange"
        Me.nudMaximumRange.Size = New System.Drawing.Size(159, 21)
        Me.nudMaximumRange.TabIndex = 12
        Me.nudMaximumRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nudMinimumRange
        '
        Me.nudMinimumRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudMinimumRange.DecimalPlaces = 2
        Me.nudMinimumRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinimumRange.Location = New System.Drawing.Point(118, 143)
        Me.nudMinimumRange.Maximum = New Decimal(New Integer() {1569325055, 23283064, 0, 0})
        Me.nudMinimumRange.Name = "nudMinimumRange"
        Me.nudMinimumRange.Size = New System.Drawing.Size(159, 21)
        Me.nudMinimumRange.TabIndex = 10
        Me.nudMinimumRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblMinRange
        '
        Me.LblMinRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMinRange.Location = New System.Drawing.Point(12, 145)
        Me.LblMinRange.Name = "LblMinRange"
        Me.LblMinRange.Size = New System.Drawing.Size(100, 17)
        Me.LblMinRange.TabIndex = 9
        Me.LblMinRange.Text = "Min. Range"
        '
        'frmExpensesApprLevelAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 253)
        Me.Controls.Add(Me.gbExpenseInformation)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpensesApprLevelAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Expenses Approval Level"
        Me.gbExpenseInformation.ResumeLayout(False)
        Me.gbExpenseInformation.PerformLayout()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.nudMaximumRange, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMinimumRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbExpenseInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboExCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpenseCat As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblPRemark As System.Windows.Forms.Label
    Friend WithEvents nudPriority As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPriority As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLevelName As System.Windows.Forms.Label
    Friend WithEvents lblLevelCode As System.Windows.Forms.Label
    Friend WithEvents LblMaxRange As System.Windows.Forms.Label
    Friend WithEvents nudMaximumRange As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudMinimumRange As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblMinRange As System.Windows.Forms.Label
End Class
