﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpensesApprLevelAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExpensesApprLevelAddEdit"
    Private mblnCancel As Boolean = True
    Private objExLevel As clsExApprovalLevel
    Private menAction As enAction = enAction.ADD_ONE
    Private mintExLevelUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintExLevelUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintExLevelUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            cboExCategory.BackColor = GUI.ColorComp
            cboExCategory.BackColor = GUI.ColorComp
            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            nudMinimumRange.BackColor = GUI.ColorOptional
            nudMaximumRange.BackColor = GUI.ColorOptional
            'Pinkal (24-Jun-2024) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objExLevel._Crlevelcode
            txtName.Text = objExLevel._Crlevelname
            nudPriority.Value = objExLevel._Crpriority
            cboExCategory.SelectedValue = objExLevel._Expensetypeid
            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            nudMinimumRange.Value = objExLevel._MinRange
            nudMaximumRange.Value = objExLevel._MaxRange
            'Pinkal (24-Jun-2024) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objExLevel._Crlevelcode = txtCode.Text
            objExLevel._Crlevelname = txtName.Text
            objExLevel._Crpriority = CInt(nudPriority.Value)
            objExLevel._Expensetypeid = CInt(cboExCategory.SelectedValue)
            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            objExLevel._MinRange = CDec(nudMinimumRange.Value)
            objExLevel._MaxRange = CDec(nudMaximumRange.Value)
            'Pinkal (24-Jun-2024) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim dtTable As DataTable = Nothing

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
            '    dtTable = New DataView(dsList.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            'End If
            Dim objExpenseCategory As New clsexpense_category_master
            dsList = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True)
            objExpenseCategory = Nothing
            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                dtTable = New DataView(dsList.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Pinkal (24-Jun-2024) -- End

            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            If CInt(cboExCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is mandatory information. Please select Expense Category."), enMsgBoxStyle.Information)
                cboExCategory.Focus()
                Return False
            End If

            If txtCode.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense Code is mandatory information. Please provide Expense Code."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Return False
            End If

            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Expense Name is mandatory information. Please provide Expense Name."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isValid_Data", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmExpensesApprLevelAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExLevel = Nothing
    End Sub

    Private Sub frmExpensesApprLevelAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesApprLevelAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesApprLevelAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesApprLevelAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesApprLevelAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExLevel = New clsExApprovalLevel
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objExLevel._Crlevelunkid = mintExLevelUnkid
            End If

            Call GetValue()

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                Me.Size = New Size(387, 282)
            Else
                Me.Size = New Size(387, 227)
                nudMinimumRange.Enabled = False
                nudMaximumRange.Enabled = False
            End If
            'Pinkal (24-Jun-2024) -- End

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesApprLevelAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExApprovalLevel.SetMessages()
            clsExpCommonMethods.SetMessages()
            objfrm._Other_ModuleNames = "clsExApprovalLevel,clsExpCommonMethods"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If isValid_Data() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objExLevel.Update()
            Else
                blnFlag = objExLevel.Insert()
            End If

            If blnFlag = False And objExLevel._Message <> "" Then
                eZeeMsgBox.Show(objExLevel._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objExLevel = Nothing
                    objExLevel = New clsExApprovalLevel
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintExLevelUnkid = objExLevel._Crlevelunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtName.Text, objExLevel._Crlevelname1, objExLevel._Crlevelname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()
			
			Me.gbExpenseInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExpenseInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbExpenseInformation.Text = Language._Object.getCaption(Me.gbExpenseInformation.Name, Me.gbExpenseInformation.Text)
			Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblPRemark.Text = Language._Object.getCaption(Me.lblPRemark.Name, Me.lblPRemark.Text)
			Me.lblPriority.Text = Language._Object.getCaption(Me.lblPriority.Name, Me.lblPriority.Text)
			Me.lblLevelName.Text = Language._Object.getCaption(Me.lblLevelName.Name, Me.lblLevelName.Text)
			Me.lblLevelCode.Text = Language._Object.getCaption(Me.lblLevelCode.Name, Me.lblLevelCode.Text)
            Me.LblMaxRange.Text = Language._Object.getCaption(Me.LblMaxRange.Name, Me.LblMaxRange.Text)
            Me.LblMinRange.Text = Language._Object.getCaption(Me.LblMinRange.Name, Me.LblMinRange.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expense Category is mandatory information. Please select Expense Category.")
			Language.setMessage(mstrModuleName, 2, "Expense Code is mandatory information. Please provide Expense Code.")
			Language.setMessage(mstrModuleName, 3, "Expense Name is mandatory information. Please provide Expense Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class