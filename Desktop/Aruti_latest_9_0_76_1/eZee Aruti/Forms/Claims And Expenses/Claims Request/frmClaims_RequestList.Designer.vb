﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClaims_RequestList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClaims_RequestList))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuExpenseFormOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCancelExpenseForm = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalApproveExpense = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpTDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.dtpFDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblDate = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboExpCategory = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblExpCategory = New System.Windows.Forms.Label
        Me.lblClaimNo = New System.Windows.Forms.Label
        Me.txtClaimNo = New eZee.TextBox.AlphanumericTextBox
        Me.lvClaimRequestList = New eZee.Common.eZeeListView(Me.components)
        Me.colhClaimNo = New System.Windows.Forms.ColumnHeader
        Me.colhExpType = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhApprovedAmount = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.objColhStatusunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhExpenseTypeId = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployeeId = New System.Windows.Forms.ColumnHeader
        Me.objcolhP2PRequistionId = New System.Windows.Forms.ColumnHeader
        Me.objFooter.SuspendLayout()
        Me.cmnuExpenseFormOperations.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 408)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(780, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.cmnuExpenseFormOperations
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(97, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuExpenseFormOperations
        Me.btnOperation.TabIndex = 310
        Me.btnOperation.Text = "&Operation"
        '
        'cmnuExpenseFormOperations
        '
        Me.cmnuExpenseFormOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCancelExpenseForm, Me.mnuGlobalApproveExpense})
        Me.cmnuExpenseFormOperations.Name = "cmnuLeaveFormOperations"
        Me.cmnuExpenseFormOperations.Size = New System.Drawing.Size(203, 48)
        '
        'mnuCancelExpenseForm
        '
        Me.mnuCancelExpenseForm.Name = "mnuCancelExpenseForm"
        Me.mnuCancelExpenseForm.Size = New System.Drawing.Size(202, 22)
        Me.mnuCancelExpenseForm.Text = "Cancel Expense Form"
        '
        'mnuGlobalApproveExpense
        '
        Me.mnuGlobalApproveExpense.Name = "mnuGlobalApproveExpense"
        Me.mnuGlobalApproveExpense.Size = New System.Drawing.Size(202, 22)
        Me.mnuGlobalApproveExpense.Text = "Global Approve Expense"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(568, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(465, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(362, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(671, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(362, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 123
        Me.btnCancel.Text = "C&ancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(780, 58)
        Me.eZeeHeader.TabIndex = 16
        Me.eZeeHeader.Title = "Claims && Request List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblExpCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblClaimNo)
        Me.gbFilterCriteria.Controls.Add(Me.txtClaimNo)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 63)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(757, 90)
        Me.gbFilterCriteria.TabIndex = 17
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(619, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 350
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(337, 62)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(110, 21)
        Me.cboStatus.TabIndex = 308
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(275, 64)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(59, 16)
        Me.lblStatus.TabIndex = 307
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(611, 36)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 16)
        Me.lblTo.TabIndex = 31
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpTDate
        '
        Me.dtpTDate.Checked = False
        Me.dtpTDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTDate.Location = New System.Drawing.Point(640, 34)
        Me.dtpTDate.Name = "dtpTDate"
        Me.dtpTDate.ShowCheckBox = True
        Me.dtpTDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpTDate.TabIndex = 32
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(248, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 302
        '
        'dtpFDate
        '
        Me.dtpFDate.Checked = False
        Me.dtpFDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFDate.Location = New System.Drawing.Point(501, 34)
        Me.dtpFDate.Name = "dtpFDate"
        Me.dtpFDate.ShowCheckBox = True
        Me.dtpFDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpFDate.TabIndex = 30
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(730, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(453, 36)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(42, 16)
        Me.lblDate.TabIndex = 29
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(707, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(74, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(168, 21)
        Me.cboEmployee.TabIndex = 22
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(60, 16)
        Me.lblEmployee.TabIndex = 21
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(640, 61)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(104, 21)
        Me.cboPeriod.TabIndex = 20
        Me.cboPeriod.Visible = False
        '
        'cboExpCategory
        '
        Me.cboExpCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpCategory.DropDownWidth = 350
        Me.cboExpCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpCategory.FormattingEnabled = True
        Me.cboExpCategory.Location = New System.Drawing.Point(337, 34)
        Me.cboExpCategory.Name = "cboExpCategory"
        Me.cboExpCategory.Size = New System.Drawing.Size(110, 21)
        Me.cboExpCategory.TabIndex = 26
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(592, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(60, 16)
        Me.lblPeriod.TabIndex = 19
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPeriod.Visible = False
        '
        'lblExpCategory
        '
        Me.lblExpCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpCategory.Location = New System.Drawing.Point(275, 36)
        Me.lblExpCategory.Name = "lblExpCategory"
        Me.lblExpCategory.Size = New System.Drawing.Size(59, 16)
        Me.lblExpCategory.TabIndex = 25
        Me.lblExpCategory.Text = "Exp. Cat."
        Me.lblExpCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClaimNo
        '
        Me.lblClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimNo.Location = New System.Drawing.Point(8, 63)
        Me.lblClaimNo.Name = "lblClaimNo"
        Me.lblClaimNo.Size = New System.Drawing.Size(60, 16)
        Me.lblClaimNo.TabIndex = 23
        Me.lblClaimNo.Text = "Claim No."
        Me.lblClaimNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClaimNo
        '
        Me.txtClaimNo.Flags = 0
        Me.txtClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClaimNo.Location = New System.Drawing.Point(74, 61)
        Me.txtClaimNo.Name = "txtClaimNo"
        Me.txtClaimNo.Size = New System.Drawing.Size(168, 21)
        Me.txtClaimNo.TabIndex = 24
        '
        'lvClaimRequestList
        '
        Me.lvClaimRequestList.BackColorOnChecked = True
        Me.lvClaimRequestList.ColumnHeaders = Nothing
        Me.lvClaimRequestList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhClaimNo, Me.colhExpType, Me.colhEmployee, Me.colhDate, Me.colhAmount, Me.colhApprovedAmount, Me.colhStatus, Me.objColhStatusunkid, Me.objcolhExpenseTypeId, Me.objcolhEmployeeId, Me.objcolhP2PRequistionId})
        Me.lvClaimRequestList.CompulsoryColumns = ""
        Me.lvClaimRequestList.FullRowSelect = True
        Me.lvClaimRequestList.GridLines = True
        Me.lvClaimRequestList.GroupingColumn = Nothing
        Me.lvClaimRequestList.HideSelection = False
        Me.lvClaimRequestList.Location = New System.Drawing.Point(12, 157)
        Me.lvClaimRequestList.MinColumnWidth = 50
        Me.lvClaimRequestList.MultiSelect = False
        Me.lvClaimRequestList.Name = "lvClaimRequestList"
        Me.lvClaimRequestList.OptionalColumns = ""
        Me.lvClaimRequestList.ShowMoreItem = False
        Me.lvClaimRequestList.ShowSaveItem = False
        Me.lvClaimRequestList.ShowSelectAll = True
        Me.lvClaimRequestList.ShowSizeAllColumnsToFit = True
        Me.lvClaimRequestList.Size = New System.Drawing.Size(757, 248)
        Me.lvClaimRequestList.Sortable = True
        Me.lvClaimRequestList.TabIndex = 18
        Me.lvClaimRequestList.UseCompatibleStateImageBehavior = False
        Me.lvClaimRequestList.View = System.Windows.Forms.View.Details
        '
        'colhClaimNo
        '
        Me.colhClaimNo.Tag = "colhClaimNo"
        Me.colhClaimNo.Text = "Claim No."
        Me.colhClaimNo.Width = 112
        '
        'colhExpType
        '
        Me.colhExpType.Tag = "colhExpType"
        Me.colhExpType.Text = "Expense Category"
        Me.colhExpType.Width = 110
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 175
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 90
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 110
        '
        'colhApprovedAmount
        '
        Me.colhApprovedAmount.Tag = "colhApprovedAmount"
        Me.colhApprovedAmount.Text = "Approved Amount"
        Me.colhApprovedAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhApprovedAmount.Width = 110
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 120
        '
        'objColhStatusunkid
        '
        Me.objColhStatusunkid.Tag = "objColhStatusunkid"
        Me.objColhStatusunkid.Text = "Statusunkid"
        Me.objColhStatusunkid.Width = 0
        '
        'objcolhExpenseTypeId
        '
        Me.objcolhExpenseTypeId.Tag = "objcolhExpenseTypeId"
        Me.objcolhExpenseTypeId.Text = "ExpenseType Id"
        Me.objcolhExpenseTypeId.Width = 0
        '
        'objcolhEmployeeId
        '
        Me.objcolhEmployeeId.Tag = "objcolhEmployeeId"
        Me.objcolhEmployeeId.Text = "EmployeeId"
        Me.objcolhEmployeeId.Width = 0
        '
        'objcolhP2PRequistionId
        '
        Me.objcolhP2PRequistionId.Tag = "objcolhP2PRequistionId"
        Me.objcolhP2PRequistionId.Text = "P2PRequistionId"
        Me.objcolhP2PRequistionId.Width = 0
        '
        'frmClaims_RequestList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(780, 463)
        Me.Controls.Add(Me.lvClaimRequestList)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmClaims_RequestList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Claims & Request List"
        Me.objFooter.ResumeLayout(False)
        Me.cmnuExpenseFormOperations.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lvClaimRequestList As eZee.Common.eZeeListView
    Friend WithEvents cboExpCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpCategory As System.Windows.Forms.Label
    Friend WithEvents lblClaimNo As System.Windows.Forms.Label
    Friend WithEvents txtClaimNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpFDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpTDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents colhExpType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClaimNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents objColhStatusunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApprovedAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuExpenseFormOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuCancelExpenseForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhExpenseTypeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmployeeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuGlobalApproveExpense As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhP2PRequistionId As System.Windows.Forms.ColumnHeader
End Class
