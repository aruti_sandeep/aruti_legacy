﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProcessLeaveList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProcessLeaveList))
        Me.lvLeaveProcessList = New eZee.Common.eZeeListView(Me.components)
        Me.colhFormNo = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeeCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhApprover = New System.Windows.Forms.ColumnHeader
        Me.colLeaveType = New System.Windows.Forms.ColumnHeader
        Me.colhStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhLeaveDays = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhApplyDate = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhStatusunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhFormunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhLeavetypeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhapproverunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhLeaveIssueunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhUserunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhmapApproverunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsCancelForm = New System.Windows.Forms.ColumnHeader
        Me.objcolhLeaveApproverunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhPriority = New System.Windows.Forms.ColumnHeader
        Me.objcolhisexternalapprover = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeClosedFYTransactions = New System.Windows.Forms.CheckBox
        Me.chkMyApprovals = New System.Windows.Forms.CheckBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.btnShowRemark = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtApprover = New eZee.TextBox.AlphanumericTextBox
        Me.lblApprover = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpApplyDate = New System.Windows.Forms.DateTimePicker
        Me.txtFormNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblApplyDate = New System.Windows.Forms.Label
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnViewDiary = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnIssueLeave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbRemark = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnRemarkClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbRemark.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvLeaveProcessList
        '
        Me.lvLeaveProcessList.BackColorOnChecked = True
        Me.lvLeaveProcessList.ColumnHeaders = Nothing
        Me.lvLeaveProcessList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFormNo, Me.colhEmployeeCode, Me.colhEmployee, Me.colhApprover, Me.colLeaveType, Me.colhStartDate, Me.colhEndDate, Me.colhLeaveDays, Me.colhStatus, Me.colhApplyDate, Me.objcolhEmployeeunkid, Me.objcolhStatusunkid, Me.objcolhFormunkid, Me.objcolhLeavetypeunkid, Me.objcolhapproverunkid, Me.objcolhLeaveIssueunkid, Me.objcolhUserunkid, Me.objcolhmapApproverunkid, Me.objcolhIsCancelForm, Me.objcolhLeaveApproverunkid, Me.objcolhPriority, Me.objcolhisexternalapprover})
        Me.lvLeaveProcessList.CompulsoryColumns = ""
        Me.lvLeaveProcessList.FullRowSelect = True
        Me.lvLeaveProcessList.GridLines = True
        Me.lvLeaveProcessList.GroupingColumn = Nothing
        Me.lvLeaveProcessList.HideSelection = False
        Me.lvLeaveProcessList.Location = New System.Drawing.Point(9, 187)
        Me.lvLeaveProcessList.MinColumnWidth = 50
        Me.lvLeaveProcessList.MultiSelect = False
        Me.lvLeaveProcessList.Name = "lvLeaveProcessList"
        Me.lvLeaveProcessList.OptionalColumns = ""
        Me.lvLeaveProcessList.ShowMoreItem = False
        Me.lvLeaveProcessList.ShowSaveItem = False
        Me.lvLeaveProcessList.ShowSelectAll = True
        Me.lvLeaveProcessList.ShowSizeAllColumnsToFit = True
        Me.lvLeaveProcessList.Size = New System.Drawing.Size(822, 271)
        Me.lvLeaveProcessList.Sortable = True
        Me.lvLeaveProcessList.TabIndex = 15
        Me.lvLeaveProcessList.UseCompatibleStateImageBehavior = False
        Me.lvLeaveProcessList.View = System.Windows.Forms.View.Details
        '
        'colhFormNo
        '
        Me.colhFormNo.Tag = "colhFormNo"
        Me.colhFormNo.Text = "Form No."
        Me.colhFormNo.Width = 0
        '
        'colhEmployeeCode
        '
        Me.colhEmployeeCode.Tag = "colhEmployeeCode"
        Me.colhEmployeeCode.Text = "Employee Code"
        Me.colhEmployeeCode.Width = 100
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 150
        '
        'colhApprover
        '
        Me.colhApprover.Tag = "colhApprover"
        Me.colhApprover.Text = "Approver"
        Me.colhApprover.Width = 240
        '
        'colLeaveType
        '
        Me.colLeaveType.Tag = "colLeaveType"
        Me.colLeaveType.Text = "Leave"
        Me.colLeaveType.Width = 100
        '
        'colhStartDate
        '
        Me.colhStartDate.Tag = "colhStartDate"
        Me.colhStartDate.Text = "Start Date"
        Me.colhStartDate.Width = 90
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 90
        '
        'colhLeaveDays
        '
        Me.colhLeaveDays.Tag = "colhLeaveDays"
        Me.colhLeaveDays.Text = "Days"
        Me.colhLeaveDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 200
        '
        'colhApplyDate
        '
        Me.colhApplyDate.Tag = "colhApplyDate"
        Me.colhApplyDate.Text = "Apply Date"
        Me.colhApplyDate.Width = 0
        '
        'objcolhEmployeeunkid
        '
        Me.objcolhEmployeeunkid.Tag = "objcolhEmployeeunkid"
        Me.objcolhEmployeeunkid.Text = "Employeeunkid"
        Me.objcolhEmployeeunkid.Width = 0
        '
        'objcolhStatusunkid
        '
        Me.objcolhStatusunkid.Tag = "objcolhStatusunkid"
        Me.objcolhStatusunkid.Text = "Statusunkid"
        Me.objcolhStatusunkid.Width = 0
        '
        'objcolhFormunkid
        '
        Me.objcolhFormunkid.Tag = "objcolhFormunkid"
        Me.objcolhFormunkid.Text = "Formunkid"
        Me.objcolhFormunkid.Width = 0
        '
        'objcolhLeavetypeunkid
        '
        Me.objcolhLeavetypeunkid.Tag = "objcolhLeavetypeunkid"
        Me.objcolhLeavetypeunkid.Text = "LeaveTypeunkid"
        Me.objcolhLeavetypeunkid.Width = 0
        '
        'objcolhapproverunkid
        '
        Me.objcolhapproverunkid.Tag = "objcolhapproverunkid"
        Me.objcolhapproverunkid.Text = "approverunkid"
        Me.objcolhapproverunkid.Width = 0
        '
        'objcolhLeaveIssueunkid
        '
        Me.objcolhLeaveIssueunkid.Tag = "objcolhLeaveIssueunkid"
        Me.objcolhLeaveIssueunkid.Text = "LeaveIssueunkid"
        Me.objcolhLeaveIssueunkid.Width = 0
        '
        'objcolhUserunkid
        '
        Me.objcolhUserunkid.Tag = "objcolhUserunkid"
        Me.objcolhUserunkid.Text = "Userunkid"
        Me.objcolhUserunkid.Width = 0
        '
        'objcolhmapApproverunkid
        '
        Me.objcolhmapApproverunkid.Tag = "objcolhmapApproverunkid"
        Me.objcolhmapApproverunkid.Text = "mapApproverunkid"
        Me.objcolhmapApproverunkid.Width = 0
        '
        'objcolhIsCancelForm
        '
        Me.objcolhIsCancelForm.Tag = "objcolhIsCancelForm"
        Me.objcolhIsCancelForm.Text = ""
        Me.objcolhIsCancelForm.Width = 0
        '
        'objcolhLeaveApproverunkid
        '
        Me.objcolhLeaveApproverunkid.Tag = "objcolhLeaveApproverunkid"
        Me.objcolhLeaveApproverunkid.Text = "objcolhLeaveApproverunkid"
        Me.objcolhLeaveApproverunkid.Width = 0
        '
        'objcolhPriority
        '
        Me.objcolhPriority.Tag = "objcolhPriority"
        Me.objcolhPriority.Text = "Priority"
        Me.objcolhPriority.Width = 0
        '
        'objcolhisexternalapprover
        '
        Me.objcolhisexternalapprover.Tag = "objcolhisexternalapprover"
        Me.objcolhisexternalapprover.Text = "isexternalapprover"
        Me.objcolhisexternalapprover.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeClosedFYTransactions)
        Me.gbFilterCriteria.Controls.Add(Me.chkMyApprovals)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.btnShowRemark)
        Me.gbFilterCriteria.Controls.Add(Me.txtApprover)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprover)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpApplyDate)
        Me.gbFilterCriteria.Controls.Add(Me.txtFormNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblApplyDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblFormNo)
        Me.gbFilterCriteria.Controls.Add(Me.objLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 67)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(822, 115)
        Me.gbFilterCriteria.TabIndex = 14
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeClosedFYTransactions
        '
        Me.chkIncludeClosedFYTransactions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeClosedFYTransactions.Location = New System.Drawing.Point(456, 90)
        Me.chkIncludeClosedFYTransactions.Name = "chkIncludeClosedFYTransactions"
        Me.chkIncludeClosedFYTransactions.Size = New System.Drawing.Size(247, 17)
        Me.chkIncludeClosedFYTransactions.TabIndex = 241
        Me.chkIncludeClosedFYTransactions.Text = "Include Closed FY Transactions"
        Me.chkIncludeClosedFYTransactions.UseVisualStyleBackColor = True
        '
        'chkMyApprovals
        '
        Me.chkMyApprovals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMyApprovals.Location = New System.Drawing.Point(266, 89)
        Me.chkMyApprovals.Name = "chkMyApprovals"
        Me.chkMyApprovals.Size = New System.Drawing.Size(111, 17)
        Me.chkMyApprovals.TabIndex = 239
        Me.chkMyApprovals.Text = "My Approvals"
        Me.chkMyApprovals.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(685, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 237
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'btnShowRemark
        '
        Me.btnShowRemark.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnShowRemark.BackColor = System.Drawing.Color.White
        Me.btnShowRemark.BackgroundImage = CType(resources.GetObject("btnShowRemark.BackgroundImage"), System.Drawing.Image)
        Me.btnShowRemark.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnShowRemark.BorderColor = System.Drawing.Color.Empty
        Me.btnShowRemark.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnShowRemark.FlatAppearance.BorderSize = 0
        Me.btnShowRemark.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShowRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowRemark.ForeColor = System.Drawing.Color.Black
        Me.btnShowRemark.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnShowRemark.GradientForeColor = System.Drawing.Color.Black
        Me.btnShowRemark.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShowRemark.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnShowRemark.Location = New System.Drawing.Point(709, 78)
        Me.btnShowRemark.Name = "btnShowRemark"
        Me.btnShowRemark.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShowRemark.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnShowRemark.Size = New System.Drawing.Size(101, 30)
        Me.btnShowRemark.TabIndex = 77
        Me.btnShowRemark.Text = "Show &Remark"
        Me.btnShowRemark.UseVisualStyleBackColor = True
        '
        'txtApprover
        '
        Me.txtApprover.BackColor = System.Drawing.Color.White
        Me.txtApprover.Flags = 0
        Me.txtApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprover.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApprover.Location = New System.Drawing.Point(81, 87)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.ReadOnly = True
        Me.txtApprover.Size = New System.Drawing.Size(161, 21)
        Me.txtApprover.TabIndex = 235
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 90)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(67, 15)
        Me.lblApprover.TabIndex = 234
        Me.lblApprover.Text = "Approver"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(81, 60)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(161, 21)
        Me.cboLeaveType.TabIndex = 232
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(8, 63)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(67, 15)
        Me.lblLeaveType.TabIndex = 231
        Me.lblLeaveType.Text = "Leave "
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(714, 36)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(96, 21)
        Me.cboStatus.TabIndex = 229
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(641, 39)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(67, 15)
        Me.lblStatus.TabIndex = 228
        Me.lblStatus.Text = "Status"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(538, 60)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpEndDate.TabIndex = 224
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(453, 63)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(79, 15)
        Me.lblEndDate.TabIndex = 223
        Me.lblEndDate.Text = "End Date"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(538, 33)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpStartDate.TabIndex = 222
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(453, 36)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(79, 15)
        Me.lblStartDate.TabIndex = 221
        Me.lblStartDate.Text = "Start Date"
        '
        'dtpApplyDate
        '
        Me.dtpApplyDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Checked = False
        Me.dtpApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplyDate.Location = New System.Drawing.Point(351, 60)
        Me.dtpApplyDate.Name = "dtpApplyDate"
        Me.dtpApplyDate.ShowCheckBox = True
        Me.dtpApplyDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpApplyDate.TabIndex = 220
        '
        'txtFormNo
        '
        Me.txtFormNo.Flags = 0
        Me.txtFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFormNo.Location = New System.Drawing.Point(351, 33)
        Me.txtFormNo.Name = "txtFormNo"
        Me.txtFormNo.Size = New System.Drawing.Size(96, 21)
        Me.txtFormNo.TabIndex = 13
        '
        'lblApplyDate
        '
        Me.lblApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplyDate.Location = New System.Drawing.Point(263, 63)
        Me.lblApplyDate.Name = "lblApplyDate"
        Me.lblApplyDate.Size = New System.Drawing.Size(82, 15)
        Me.lblApplyDate.TabIndex = 219
        Me.lblApplyDate.Text = "Apply Date"
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(263, 36)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(82, 15)
        Me.lblFormNo.TabIndex = 217
        Me.lblFormNo.Text = "Application No"
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(248, 24)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(10, 94)
        Me.objLine1.TabIndex = 88
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(795, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(221, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(772, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(81, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(134, 21)
        Me.cboEmployee.TabIndex = 87
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(67, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(841, 60)
        Me.eZeeHeader.TabIndex = 16
        Me.eZeeHeader.Title = "Leave Process List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnViewDiary)
        Me.objFooter.Controls.Add(Me.btnIssueLeave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 465)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(841, 55)
        Me.objFooter.TabIndex = 17
        '
        'btnViewDiary
        '
        Me.btnViewDiary.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewDiary.BackColor = System.Drawing.Color.White
        Me.btnViewDiary.BackgroundImage = CType(resources.GetObject("btnViewDiary.BackgroundImage"), System.Drawing.Image)
        Me.btnViewDiary.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewDiary.BorderColor = System.Drawing.Color.Empty
        Me.btnViewDiary.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewDiary.FlatAppearance.BorderSize = 0
        Me.btnViewDiary.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewDiary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewDiary.ForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewDiary.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDiary.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.Location = New System.Drawing.Point(105, 13)
        Me.btnViewDiary.Name = "btnViewDiary"
        Me.btnViewDiary.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDiary.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.Size = New System.Drawing.Size(90, 30)
        Me.btnViewDiary.TabIndex = 77
        Me.btnViewDiary.Text = "&View Diary"
        Me.btnViewDiary.UseVisualStyleBackColor = True
        '
        'btnIssueLeave
        '
        Me.btnIssueLeave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnIssueLeave.BackColor = System.Drawing.Color.White
        Me.btnIssueLeave.BackgroundImage = CType(resources.GetObject("btnIssueLeave.BackgroundImage"), System.Drawing.Image)
        Me.btnIssueLeave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnIssueLeave.BorderColor = System.Drawing.Color.Empty
        Me.btnIssueLeave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnIssueLeave.FlatAppearance.BorderSize = 0
        Me.btnIssueLeave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnIssueLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIssueLeave.ForeColor = System.Drawing.Color.Black
        Me.btnIssueLeave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnIssueLeave.GradientForeColor = System.Drawing.Color.Black
        Me.btnIssueLeave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnIssueLeave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnIssueLeave.Location = New System.Drawing.Point(9, 13)
        Me.btnIssueLeave.Name = "btnIssueLeave"
        Me.btnIssueLeave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnIssueLeave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnIssueLeave.Size = New System.Drawing.Size(90, 30)
        Me.btnIssueLeave.TabIndex = 76
        Me.btnIssueLeave.Text = "&Issue Leave"
        Me.btnIssueLeave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(736, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 73
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(611, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(119, 30)
        Me.btnEdit.TabIndex = 75
        Me.btnEdit.Text = "Change &Status"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'gbRemark
        '
        Me.gbRemark.BorderColor = System.Drawing.Color.Black
        Me.gbRemark.Checked = False
        Me.gbRemark.CollapseAllExceptThis = False
        Me.gbRemark.CollapsedHoverImage = Nothing
        Me.gbRemark.CollapsedNormalImage = Nothing
        Me.gbRemark.CollapsedPressedImage = Nothing
        Me.gbRemark.CollapseOnLoad = False
        Me.gbRemark.Controls.Add(Me.btnRemarkClose)
        Me.gbRemark.Controls.Add(Me.txtRemarks)
        Me.gbRemark.ExpandedHoverImage = Nothing
        Me.gbRemark.ExpandedNormalImage = Nothing
        Me.gbRemark.ExpandedPressedImage = Nothing
        Me.gbRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRemark.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRemark.HeaderHeight = 25
        Me.gbRemark.HeaderMessage = ""
        Me.gbRemark.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRemark.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRemark.HeightOnCollapse = 0
        Me.gbRemark.LeftTextSpace = 0
        Me.gbRemark.Location = New System.Drawing.Point(275, 237)
        Me.gbRemark.Name = "gbRemark"
        Me.gbRemark.OpenHeight = 300
        Me.gbRemark.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRemark.ShowBorder = True
        Me.gbRemark.ShowCheckBox = False
        Me.gbRemark.ShowCollapseButton = False
        Me.gbRemark.ShowDefaultBorderColor = True
        Me.gbRemark.ShowDownButton = False
        Me.gbRemark.ShowHeader = True
        Me.gbRemark.Size = New System.Drawing.Size(343, 185)
        Me.gbRemark.TabIndex = 18
        Me.gbRemark.Temp = 0
        Me.gbRemark.Text = "Approver's Remark"
        Me.gbRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbRemark.Visible = False
        '
        'btnRemarkClose
        '
        Me.btnRemarkClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemarkClose.BackColor = System.Drawing.Color.White
        Me.btnRemarkClose.BackgroundImage = CType(resources.GetObject("btnRemarkClose.BackgroundImage"), System.Drawing.Image)
        Me.btnRemarkClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRemarkClose.BorderColor = System.Drawing.Color.Empty
        Me.btnRemarkClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRemarkClose.FlatAppearance.BorderSize = 0
        Me.btnRemarkClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemarkClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemarkClose.ForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRemarkClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemarkClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.Location = New System.Drawing.Point(245, 150)
        Me.btnRemarkClose.Name = "btnRemarkClose"
        Me.btnRemarkClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemarkClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.Size = New System.Drawing.Size(93, 30)
        Me.btnRemarkClose.TabIndex = 77
        Me.btnRemarkClose.Text = "Close"
        Me.btnRemarkClose.UseVisualStyleBackColor = True
        '
        'txtRemarks
        '
        Me.txtRemarks.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(4, 29)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ReadOnly = True
        Me.txtRemarks.Size = New System.Drawing.Size(334, 116)
        Me.txtRemarks.TabIndex = 1
        '
        'frmProcessLeaveList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(841, 520)
        Me.Controls.Add(Me.gbRemark)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.lvLeaveProcessList)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProcessLeaveList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Leave Process List"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbRemark.ResumeLayout(False)
        Me.gbRemark.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lvLeaveProcessList As eZee.Common.eZeeListView
    Friend WithEvents colhFormNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colLeaveType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLeaveDays As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApplyDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhStatusunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhLeavetypeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpApplyDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFormNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApplyDate As System.Windows.Forms.Label
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents colhApprover As System.Windows.Forms.ColumnHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhFormunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhapproverunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtApprover As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents btnIssueLeave As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhLeaveIssueunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhUserunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhmapApproverunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnShowRemark As eZee.Common.eZeeLightButton
    Friend WithEvents gbRemark As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnRemarkClose As eZee.Common.eZeeLightButton
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnViewDiary As eZee.Common.eZeeLightButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objcolhIsCancelForm As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkMyApprovals As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhLeaveApproverunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPriority As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployeeCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhisexternalapprover As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkIncludeClosedFYTransactions As System.Windows.Forms.CheckBox
End Class
