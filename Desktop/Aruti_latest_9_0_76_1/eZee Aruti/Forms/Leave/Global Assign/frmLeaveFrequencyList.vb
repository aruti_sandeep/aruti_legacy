﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLeaveFrequencyList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmLeaveFrequencyList"
    Private mstrAdvanceFilter As String = ""
    Private objLeaveBalance As clsleavebalance_tran
    Private mDicNoSkippedLeave As New Dictionary(Of Integer, Integer)

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True)
            dtFill = New DataView(dsFill.Tables("LeaveType"), "isshortleave = 1", "", DataViewRowState.CurrentRows).ToTable
            Dim drRow As DataRow
            drRow = dtFill.NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavename") = Language.getMessage(mstrModuleName, 1, "Select")
            dtFill.Rows.InsertAt(drRow, 0)
            With cboLeaveType
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "leavename"
                .DataSource = dtFill
                .SelectedValue = 0
            End With
            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsFill = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , )
            'Else
            '    dsFill = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If


            'Pinkal (14-Jun-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmLeaveFrequencyList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            'dsFill = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)


            dsFill = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                         mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, _
                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'Pinkal (14-Jun-2018) -- End


            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsFill.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dTable As DataTable = Nothing
        Try
            RemoveHandler lvLeaveFrequency.ItemChecked, AddressOf lvLeaveFrequency_ItemChecked
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If User._Object.Privilege._AllowToViewLeaveFrequency = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            Dim mIsCloseFy, mIsELC, mIsOpenELC As Boolean : mIsCloseFy = False : mIsELC = False : mIsOpenELC = False

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                mIsCloseFy = False : mIsELC = False : mIsOpenELC = False
            Else
                mIsCloseFy = False : mIsELC = True : mIsOpenELC = True
            End If


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            '   dTable = objLeaveBalance.GetLeaveFreq_Employee(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), mstrAdvanceFilter, mIsCloseFy, mIsELC, mIsOpenELC)

            'Pinkal (14-Jun-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmLeaveFrequencyList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dTable = objLeaveBalance.GetLeaveFreq_Employee(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                                                           , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                                                           , True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue) _
            '                                                                           , True, True, mIsCloseFy, mIsELC, mIsOpenELC, mstrAdvanceFilter)

            dTable = objLeaveBalance.GetLeaveFreq_Employee(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                       , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                   , mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue) _
                                                                                       , True, True, mIsCloseFy, mIsELC, mIsOpenELC, mstrAdvanceFilter)

            'Pinkal (14-Jun-2018) -- End



            'Pinkal (24-Aug-2015) -- End


            lvLeaveFrequency.Items.Clear()

            If dTable.Rows.Count > 0 Then
                lvLeaveFrequency.BeginUpdate() 'S.SANDEEP [15 NOV 2016] -- START -- END
                dTable = New DataView(dTable, "", "emp", DataViewRowState.CurrentRows).ToTable
                Dim lvItem As ListViewItem
                For Each dRow As DataRow In dTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.SubItems.Add(dRow.Item("EName").ToString)
                    lvItem.SubItems.Add(dRow.Item("LeaveName").ToString)
                    lvItem.SubItems.Add(dRow.Item("eligibilityafter").ToString)
                    'Pinkal (18-Aug-2015) -- Start
                    'Enhancement -WORKING ON CHANGES REALATED TO CALCULATION OF SHORT LEAVE.
                    'lvItem.SubItems.Add( dRow.Item("accrue_amount").ToString)
                    lvItem.SubItems.Add(Math.Round(CDec(dRow.Item("accrue_amount").ToString), 2).ToString())
                    'Pinkal (18-Aug-2015) -- End
                    lvItem.SubItems.Add(dRow.Item("LvFreq").ToString)
                    lvItem.SubItems.Add(dRow.Item("consecutivedays").ToString)
                    lvItem.SubItems.Add(dRow.Item("occurrence").ToString)
                    lvItem.SubItems.Add(dRow.Item("employeeunkid").ToString)
                    lvItem.SubItems.Add(dRow.Item("leavetypeunkid").ToString)

                    lvItem.Tag = dRow.Item("leavebalanceunkid").ToString

                    If mDicNoSkippedLeave.Keys.Count > 0 Then
                        If mDicNoSkippedLeave.ContainsKey(CInt(dRow.Item("leavebalanceunkid").ToString)) Then
                            lvItem.ForeColor = Color.Blue
                        End If
                    End If

                    lvLeaveFrequency.Items.Add(lvItem)
                Next
                lvLeaveFrequency.GroupingColumn = objcolhEmployee
                lvLeaveFrequency.DisplayGroups(True)
                lvLeaveFrequency.EndUpdate() 'S.SANDEEP [15 NOV 2016] -- START -- END
            End If

            If lvLeaveFrequency.Groups.Count > 2 Then
                colhLeave.Width = 170 - 20
            Else
                colhLeave.Width = 170
            End If

            If mDicNoSkippedLeave.Keys.Count > 0 Then mDicNoSkippedLeave.Clear()

            AddHandler lvLeaveFrequency.ItemChecked, AddressOf lvLeaveFrequency_ItemChecked
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            btnNew.Enabled = User._Object.Privilege._AllowToAddLeaveFrequency
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteLeaveFrequency
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmLeaveFrequencyList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLeaveBalance = New clsleavebalance_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            lvLeaveFrequency.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveFrequencyList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmLeaveFrequencyList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvLeaveFrequency.Focused Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveFrequencyList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveFrequencyList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveBalance = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleavebalance_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleavebalance_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmGlobalLvFreq_Assign
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvLeaveFrequency.CheckedItems.Count > 0 Then
                mDicNoSkippedLeave = New Dictionary(Of Integer, Integer)
                Dim mFlag As Boolean = False
                Dim iVoidReason As String = String.Empty
                Dim frm As New frmReasonSelection

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(enVoidCategoryType.LEAVE, iVoidReason)
                If iVoidReason.Trim.Length <= 0 Then Exit Sub
                Dim objLF As New clsleaveform
                For Each iLvItem As ListViewItem In lvLeaveFrequency.CheckedItems
                    If objLF.IsLeaveForm_Exists(CInt(iLvItem.SubItems(objcolhEmpId.Index).Text), CInt(iLvItem.SubItems(objcolhLeaveId.Index).Text)) = True Then
                        If mDicNoSkippedLeave.ContainsKey(CInt(iLvItem.Tag)) = False Then
                            mDicNoSkippedLeave.Add(CInt(iLvItem.Tag), CInt(iLvItem.Tag))
                        End If
                        mFlag = True
                        Continue For
                    End If
                    objLeaveBalance._Isvoid = True
                    objLeaveBalance._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objLeaveBalance._Voidreason = iVoidReason
                    objLeaveBalance._Voiduserunkid = User._Object._Userunkid
                    If objLeaveBalance.Delete(CInt(iLvItem.Tag)) = False Then
                        eZeeMsgBox.Show(objLeaveBalance._Message, enMsgBoxStyle.Information)
                        iLvItem.ForeColor = Color.Red
                        Exit Sub
                    End If
                Next
                objLF = Nothing
                If mFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "There are some leave type(s) which are already linked with leave form, and these leave are highlighted in blue."), enMsgBoxStyle.Information)
                End If
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboLeaveType.ValueMember
                .DisplayMember = cboLeaveType.DisplayMember
                .DataSource = CType(cboLeaveType.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboLeaveType.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboEmployee.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboLeaveType.SelectedValue = 0
            mstrAdvanceFilter = ""
            lvLeaveFrequency.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvLeaveFrequency.ItemChecked, AddressOf lvLeaveFrequency_ItemChecked
            For Each lvItem As ListViewItem In lvLeaveFrequency.Items
                lvItem.Checked = objchkAll.Checked
            Next
            AddHandler lvLeaveFrequency.ItemChecked, AddressOf lvLeaveFrequency_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvLeaveFrequency_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLeaveFrequency.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvLeaveFrequency.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvLeaveFrequency.CheckedItems.Count < lvLeaveFrequency.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvLeaveFrequency.CheckedItems.Count = lvLeaveFrequency.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveFrequency_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.colhLeave.Text = Language._Object.getCaption(CStr(Me.colhLeave.Tag), Me.colhLeave.Text)
			Me.colhMaxAmount.Text = Language._Object.getCaption(CStr(Me.colhMaxAmount.Tag), Me.colhMaxAmount.Text)
			Me.colhFrequency.Text = Language._Object.getCaption(CStr(Me.colhFrequency.Tag), Me.colhFrequency.Text)
			Me.colhConsecutiveDays.Text = Language._Object.getCaption(CStr(Me.colhConsecutiveDays.Tag), Me.colhConsecutiveDays.Text)
			Me.colhOccurance.Text = Language._Object.getCaption(CStr(Me.colhOccurance.Tag), Me.colhOccurance.Text)
			Me.colhEligiblity.Text = Language._Object.getCaption(CStr(Me.colhEligiblity.Tag), Me.colhEligiblity.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class