﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveSchedular
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveSchedular))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.btnMonthly = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.lbldepartment = New System.Windows.Forms.Label
        Me.lbljob = New System.Windows.Forms.Label
        Me.gbUpcomingThings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlUpcomingEvents = New System.Windows.Forms.Panel
        Me.lvUpcomingEvents = New System.Windows.Forms.ListView
        Me.colhUpcomingEvents = New System.Windows.Forms.ColumnHeader
        Me.btnWeek = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMonthCalender = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlCalender = New System.Windows.Forms.Panel
        Me.dtpCalender = New System.Windows.Forms.MonthCalendar
        Me.pnlViewCalender = New System.Windows.Forms.Panel
        Me.objlblRight = New System.Windows.Forms.Label
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.objlblLeft = New System.Windows.Forms.Label
        Me.objlblSelectedArea = New System.Windows.Forms.Label
        Me.dgvEmployeeCalender = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbUpcomingThings.SuspendLayout()
        Me.pnlUpcomingEvents.SuspendLayout()
        Me.gbMonthCalender.SuspendLayout()
        Me.pnlCalender.SuspendLayout()
        Me.pnlViewCalender.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEmployeeCalender, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.btnMonthly)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.gbUpcomingThings)
        Me.pnlMainInfo.Controls.Add(Me.btnWeek)
        Me.pnlMainInfo.Controls.Add(Me.gbMonthCalender)
        Me.pnlMainInfo.Controls.Add(Me.pnlViewCalender)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(944, 548)
        Me.pnlMainInfo.TabIndex = 0
        '
        'btnMonthly
        '
        Me.btnMonthly.BackColor = System.Drawing.Color.White
        Me.btnMonthly.BackgroundImage = CType(resources.GetObject("btnMonthly.BackgroundImage"), System.Drawing.Image)
        Me.btnMonthly.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMonthly.BorderColor = System.Drawing.Color.Empty
        Me.btnMonthly.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnMonthly.FlatAppearance.BorderSize = 0
        Me.btnMonthly.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMonthly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMonthly.ForeColor = System.Drawing.Color.Black
        Me.btnMonthly.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMonthly.GradientForeColor = System.Drawing.Color.Black
        Me.btnMonthly.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMonthly.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMonthly.Image = Global.Aruti.Main.My.Resources.Resources.NextReservation_16
        Me.btnMonthly.Location = New System.Drawing.Point(12, 12)
        Me.btnMonthly.Name = "btnMonthly"
        Me.btnMonthly.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMonthly.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMonthly.Size = New System.Drawing.Size(233, 30)
        Me.btnMonthly.TabIndex = 1
        Me.btnMonthly.TabStop = False
        Me.btnMonthly.Text = "Month View"
        Me.btnMonthly.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnMonthly.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objReset)
        Me.gbFilterCriteria.Controls.Add(Me.objSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.cboSection)
        Me.gbFilterCriteria.Controls.Add(Me.lbldepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lbljob)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(251, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(686, 66)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(543, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(83, 13)
        Me.lnkAllocation.TabIndex = 36
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objReset
        '
        Me.objReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objReset.BackColor = System.Drawing.Color.Transparent
        Me.objReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objReset.Image = CType(resources.GetObject("objReset.Image"), System.Drawing.Image)
        Me.objReset.Location = New System.Drawing.Point(661, 0)
        Me.objReset.Name = "objReset"
        Me.objReset.ResultMessage = ""
        Me.objReset.SearchMessage = ""
        Me.objReset.Size = New System.Drawing.Size(24, 24)
        Me.objReset.TabIndex = 34
        Me.objReset.TabStop = False
        '
        'objSearch
        '
        Me.objSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSearch.BackColor = System.Drawing.Color.Transparent
        Me.objSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objSearch.Image = CType(resources.GetObject("objSearch.Image"), System.Drawing.Image)
        Me.objSearch.Location = New System.Drawing.Point(638, 0)
        Me.objSearch.Name = "objSearch"
        Me.objSearch.ResultMessage = ""
        Me.objSearch.SearchMessage = ""
        Me.objSearch.Size = New System.Drawing.Size(24, 24)
        Me.objSearch.TabIndex = 33
        Me.objSearch.TabStop = False
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(517, 32)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(121, 21)
        Me.cboJob.TabIndex = 3
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(85, 32)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(121, 21)
        Me.cboDepartment.TabIndex = 1
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(221, 36)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(80, 16)
        Me.lblSection.TabIndex = 25
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(307, 32)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(121, 21)
        Me.cboSection.TabIndex = 2
        '
        'lbldepartment
        '
        Me.lbldepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldepartment.Location = New System.Drawing.Point(8, 36)
        Me.lbldepartment.Name = "lbldepartment"
        Me.lbldepartment.Size = New System.Drawing.Size(80, 17)
        Me.lbldepartment.TabIndex = 27
        Me.lbldepartment.Text = "Department"
        Me.lbldepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbljob
        '
        Me.lbljob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbljob.Location = New System.Drawing.Point(436, 36)
        Me.lbljob.Name = "lbljob"
        Me.lbljob.Size = New System.Drawing.Size(74, 16)
        Me.lbljob.TabIndex = 29
        Me.lbljob.Text = "Job"
        Me.lbljob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbUpcomingThings
        '
        Me.gbUpcomingThings.BorderColor = System.Drawing.Color.Black
        Me.gbUpcomingThings.Checked = False
        Me.gbUpcomingThings.CollapseAllExceptThis = False
        Me.gbUpcomingThings.CollapsedHoverImage = Nothing
        Me.gbUpcomingThings.CollapsedNormalImage = Nothing
        Me.gbUpcomingThings.CollapsedPressedImage = Nothing
        Me.gbUpcomingThings.CollapseOnLoad = False
        Me.gbUpcomingThings.Controls.Add(Me.pnlUpcomingEvents)
        Me.gbUpcomingThings.ExpandedHoverImage = Nothing
        Me.gbUpcomingThings.ExpandedNormalImage = Nothing
        Me.gbUpcomingThings.ExpandedPressedImage = Nothing
        Me.gbUpcomingThings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUpcomingThings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUpcomingThings.HeaderHeight = 25
        Me.gbUpcomingThings.HeaderMessage = ""
        Me.gbUpcomingThings.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbUpcomingThings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUpcomingThings.HeightOnCollapse = 0
        Me.gbUpcomingThings.LeftTextSpace = 0
        Me.gbUpcomingThings.Location = New System.Drawing.Point(12, 273)
        Me.gbUpcomingThings.Name = "gbUpcomingThings"
        Me.gbUpcomingThings.OpenHeight = 264
        Me.gbUpcomingThings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUpcomingThings.ShowBorder = True
        Me.gbUpcomingThings.ShowCheckBox = False
        Me.gbUpcomingThings.ShowCollapseButton = False
        Me.gbUpcomingThings.ShowDefaultBorderColor = True
        Me.gbUpcomingThings.ShowDownButton = False
        Me.gbUpcomingThings.ShowHeader = True
        Me.gbUpcomingThings.Size = New System.Drawing.Size(233, 214)
        Me.gbUpcomingThings.TabIndex = 4
        Me.gbUpcomingThings.Temp = 0
        Me.gbUpcomingThings.Text = "Forecasted Leaves"
        Me.gbUpcomingThings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlUpcomingEvents
        '
        Me.pnlUpcomingEvents.Controls.Add(Me.lvUpcomingEvents)
        Me.pnlUpcomingEvents.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlUpcomingEvents.Location = New System.Drawing.Point(2, 26)
        Me.pnlUpcomingEvents.Name = "pnlUpcomingEvents"
        Me.pnlUpcomingEvents.Size = New System.Drawing.Size(228, 185)
        Me.pnlUpcomingEvents.TabIndex = 0
        '
        'lvUpcomingEvents
        '
        Me.lvUpcomingEvents.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhUpcomingEvents})
        Me.lvUpcomingEvents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvUpcomingEvents.FullRowSelect = True
        Me.lvUpcomingEvents.GridLines = True
        Me.lvUpcomingEvents.Location = New System.Drawing.Point(0, 0)
        Me.lvUpcomingEvents.MultiSelect = False
        Me.lvUpcomingEvents.Name = "lvUpcomingEvents"
        Me.lvUpcomingEvents.Size = New System.Drawing.Size(228, 185)
        Me.lvUpcomingEvents.TabIndex = 1
        Me.lvUpcomingEvents.UseCompatibleStateImageBehavior = False
        Me.lvUpcomingEvents.View = System.Windows.Forms.View.Details
        '
        'colhUpcomingEvents
        '
        Me.colhUpcomingEvents.Text = "Forecasted Leaves"
        Me.colhUpcomingEvents.Width = 220
        '
        'btnWeek
        '
        Me.btnWeek.BackColor = System.Drawing.Color.White
        Me.btnWeek.BackgroundImage = CType(resources.GetObject("btnWeek.BackgroundImage"), System.Drawing.Image)
        Me.btnWeek.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnWeek.BorderColor = System.Drawing.Color.Empty
        Me.btnWeek.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnWeek.FlatAppearance.BorderSize = 0
        Me.btnWeek.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWeek.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWeek.ForeColor = System.Drawing.Color.Black
        Me.btnWeek.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnWeek.GradientForeColor = System.Drawing.Color.Black
        Me.btnWeek.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnWeek.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnWeek.Image = Global.Aruti.Main.My.Resources.Resources.NextReservation_16
        Me.btnWeek.Location = New System.Drawing.Point(12, 48)
        Me.btnWeek.Name = "btnWeek"
        Me.btnWeek.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnWeek.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnWeek.Size = New System.Drawing.Size(233, 30)
        Me.btnWeek.TabIndex = 2
        Me.btnWeek.TabStop = False
        Me.btnWeek.Text = "Weekly View"
        Me.btnWeek.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnWeek.UseVisualStyleBackColor = True
        '
        'gbMonthCalender
        '
        Me.gbMonthCalender.BorderColor = System.Drawing.Color.Black
        Me.gbMonthCalender.Checked = False
        Me.gbMonthCalender.CollapseAllExceptThis = False
        Me.gbMonthCalender.CollapsedHoverImage = Nothing
        Me.gbMonthCalender.CollapsedNormalImage = Nothing
        Me.gbMonthCalender.CollapsedPressedImage = Nothing
        Me.gbMonthCalender.CollapseOnLoad = False
        Me.gbMonthCalender.Controls.Add(Me.pnlCalender)
        Me.gbMonthCalender.ExpandedHoverImage = Nothing
        Me.gbMonthCalender.ExpandedNormalImage = Nothing
        Me.gbMonthCalender.ExpandedPressedImage = Nothing
        Me.gbMonthCalender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMonthCalender.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMonthCalender.HeaderHeight = 25
        Me.gbMonthCalender.HeaderMessage = ""
        Me.gbMonthCalender.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMonthCalender.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMonthCalender.HeightOnCollapse = 0
        Me.gbMonthCalender.LeftTextSpace = 0
        Me.gbMonthCalender.Location = New System.Drawing.Point(12, 84)
        Me.gbMonthCalender.Name = "gbMonthCalender"
        Me.gbMonthCalender.OpenHeight = 183
        Me.gbMonthCalender.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMonthCalender.ShowBorder = True
        Me.gbMonthCalender.ShowCheckBox = False
        Me.gbMonthCalender.ShowCollapseButton = False
        Me.gbMonthCalender.ShowDefaultBorderColor = True
        Me.gbMonthCalender.ShowDownButton = False
        Me.gbMonthCalender.ShowHeader = True
        Me.gbMonthCalender.Size = New System.Drawing.Size(233, 183)
        Me.gbMonthCalender.TabIndex = 3
        Me.gbMonthCalender.Temp = 0
        Me.gbMonthCalender.Text = "Calender"
        Me.gbMonthCalender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlCalender
        '
        Me.pnlCalender.Controls.Add(Me.dtpCalender)
        Me.pnlCalender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCalender.Location = New System.Drawing.Point(3, 26)
        Me.pnlCalender.Name = "pnlCalender"
        Me.pnlCalender.Size = New System.Drawing.Size(227, 155)
        Me.pnlCalender.TabIndex = 1
        '
        'dtpCalender
        '
        Me.dtpCalender.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpCalender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCalender.Location = New System.Drawing.Point(0, 0)
        Me.dtpCalender.Name = "dtpCalender"
        Me.dtpCalender.TabIndex = 6
        '
        'pnlViewCalender
        '
        Me.pnlViewCalender.Controls.Add(Me.objlblRight)
        Me.pnlViewCalender.Controls.Add(Me.picStayView)
        Me.pnlViewCalender.Controls.Add(Me.objlblLeft)
        Me.pnlViewCalender.Controls.Add(Me.objlblSelectedArea)
        Me.pnlViewCalender.Controls.Add(Me.dgvEmployeeCalender)
        Me.pnlViewCalender.Location = New System.Drawing.Point(251, 84)
        Me.pnlViewCalender.Name = "pnlViewCalender"
        Me.pnlViewCalender.Size = New System.Drawing.Size(686, 403)
        Me.pnlViewCalender.TabIndex = 1
        '
        'objlblRight
        '
        Me.objlblRight.BackColor = System.Drawing.Color.Black
        Me.objlblRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objlblRight.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.objlblRight.Location = New System.Drawing.Point(532, 224)
        Me.objlblRight.Name = "objlblRight"
        Me.objlblRight.Size = New System.Drawing.Size(8, 8)
        Me.objlblRight.TabIndex = 8
        Me.objlblRight.Visible = False
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.Cell_End_Image
        Me.picStayView.Location = New System.Drawing.Point(54, 38)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 2
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'objlblLeft
        '
        Me.objlblLeft.BackColor = System.Drawing.Color.Black
        Me.objlblLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objlblLeft.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.objlblLeft.Location = New System.Drawing.Point(224, 224)
        Me.objlblLeft.Name = "objlblLeft"
        Me.objlblLeft.Size = New System.Drawing.Size(8, 8)
        Me.objlblLeft.TabIndex = 7
        Me.objlblLeft.Visible = False
        '
        'objlblSelectedArea
        '
        Me.objlblSelectedArea.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.objlblSelectedArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.objlblSelectedArea.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.objlblSelectedArea.Location = New System.Drawing.Point(306, 213)
        Me.objlblSelectedArea.Name = "objlblSelectedArea"
        Me.objlblSelectedArea.Size = New System.Drawing.Size(202, 36)
        Me.objlblSelectedArea.TabIndex = 5
        Me.objlblSelectedArea.Text = "Label1"
        Me.objlblSelectedArea.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.objlblSelectedArea.Visible = False
        '
        'dgvEmployeeCalender
        '
        Me.dgvEmployeeCalender.AllowUserToAddRows = False
        Me.dgvEmployeeCalender.AllowUserToDeleteRows = False
        Me.dgvEmployeeCalender.AllowUserToResizeColumns = False
        Me.dgvEmployeeCalender.AllowUserToResizeRows = False
        Me.dgvEmployeeCalender.BackgroundColor = System.Drawing.Color.White
        Me.dgvEmployeeCalender.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmployeeCalender.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmployeeCalender.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmployeeCalender.Name = "dgvEmployeeCalender"
        Me.dgvEmployeeCalender.RowHeadersVisible = False
        Me.dgvEmployeeCalender.Size = New System.Drawing.Size(686, 403)
        Me.dgvEmployeeCalender.TabIndex = 0
        Me.dgvEmployeeCalender.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 493)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(944, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(839, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmLeaveSchedular
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 548)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveSchedular"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Planner Viewer"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbUpcomingThings.ResumeLayout(False)
        Me.pnlUpcomingEvents.ResumeLayout(False)
        Me.gbMonthCalender.ResumeLayout(False)
        Me.pnlCalender.ResumeLayout(False)
        Me.pnlViewCalender.ResumeLayout(False)
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEmployeeCalender, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbMonthCalender As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlViewCalender As System.Windows.Forms.Panel
    Friend WithEvents btnMonthly As eZee.Common.eZeeLightButton
    Friend WithEvents btnWeek As eZee.Common.eZeeLightButton
    Friend WithEvents pnlCalender As System.Windows.Forms.Panel
    Friend WithEvents dtpCalender As System.Windows.Forms.MonthCalendar
    Friend WithEvents gbUpcomingThings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlUpcomingEvents As System.Windows.Forms.Panel
    Friend WithEvents lvUpcomingEvents As System.Windows.Forms.ListView
    Friend WithEvents colhUpcomingEvents As System.Windows.Forms.ColumnHeader
    Friend WithEvents dgvEmployeeCalender As System.Windows.Forms.DataGridView
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lbljob As System.Windows.Forms.Label
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents lbldepartment As System.Windows.Forms.Label
    Friend WithEvents objReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents objlblSelectedArea As System.Windows.Forms.Label
    Friend WithEvents objlblRight As System.Windows.Forms.Label
    Friend WithEvents objlblLeft As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
End Class
