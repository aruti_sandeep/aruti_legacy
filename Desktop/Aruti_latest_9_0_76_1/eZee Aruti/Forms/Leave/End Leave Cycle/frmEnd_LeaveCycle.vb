﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEnd_LeaveCycle

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEnd_LeaveCycle"
    Private objBalTran As clsleavebalance_tran
    Private dgView As DataView
    Private dtFinalTab As DataTable
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mintEmployeeId As Integer = -1
    Private mintOptionId As Integer = -1
    Private mdtDate As Date = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal _EmployeeId As Integer, Optional ByVal _OptionId As Integer = -1, Optional ByVal _dtDate As Date = Nothing) As Boolean
        Try
            mintEmployeeId = _EmployeeId
            mintOptionId = _OptionId
            mdtDate = _dtDate
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms Events "

    Private Sub frmEnd_LeaveCycle_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBalTran = New clsleavebalance_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            If mdtDate.Date <> Nothing Then
                dtpDateAsOn.MinDate = mdtDate.Date
                dtpDateAsOn.MaxDate = mdtDate.Date
            Else
                dtpDateAsOn.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            If mintEmployeeId > 0 Then

                Select Case mintOptionId
                    Case 1
                        radSuspensionDate.Checked = True
                    Case 2
                        radProbationDate.Checked = True
                    Case 3
                        radEOCDate.Checked = True
                    Case 4
                        radTerminationDate.Checked = True
                    Case 5
                        radRetirementDate.Checked = True
                End Select

                objbtnSearch_Click(sender, e)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEnd_LeaveCycle_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub Fill_Data_Grid()
        Try
            If radSuspensionDate.Checked = True Then
                dtFinalTab = objBalTran.GetELC_Employee(1, dtpDateAsOn.Value.Date, ConfigParameter._Object._LeaveBalanceSetting, mintEmployeeId)
            ElseIf radProbationDate.Checked = True Then
                dtFinalTab = objBalTran.GetELC_Employee(2, dtpDateAsOn.Value.Date, ConfigParameter._Object._LeaveBalanceSetting, mintEmployeeId)
            ElseIf radEOCDate.Checked = True Then
                dtFinalTab = objBalTran.GetELC_Employee(3, dtpDateAsOn.Value.Date, ConfigParameter._Object._LeaveBalanceSetting, mintEmployeeId)
            ElseIf radTerminationDate.Checked = True Then
                dtFinalTab = objBalTran.GetELC_Employee(4, dtpDateAsOn.Value.Date, ConfigParameter._Object._LeaveBalanceSetting, mintEmployeeId)
            ElseIf radRetirementDate.Checked = True Then
                dtFinalTab = objBalTran.GetELC_Employee(5, dtpDateAsOn.Value.Date, ConfigParameter._Object._LeaveBalanceSetting, mintEmployeeId)
            End If

            dgView = dtFinalTab.DefaultView

            objdgcolhCheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "ecode"
            dgcolhEdDate.DataPropertyName = "edate"
            dgcolhEmployee.DataPropertyName = "ename"
            dgcolhJob.DataPropertyName = "ejob"
            dgcolhStDate.DataPropertyName = "sdate"

            dgData.AutoGenerateColumns = False

            dgData.DataSource = dgView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call Fill_Data_Grid()
            objChkAll.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            radTerminationDate.Checked = True
            dtpDateAsOn.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            Call Fill_Data_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnd.Click
        Try
            If dtFinalTab Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no employee to end the leave cycle.", enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly))
                Exit Sub
            End If

            Dim drRow() As DataRow = dtFinalTab.Select("ischeck = true")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select atleast one employee to do further operation.", enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly))
                Exit Sub
            End If

            Dim mintOptionId As Integer = -1

            If radSuspensionDate.Checked = True Then  'SUSPENSION
                mintOptionId = 1
            ElseIf radProbationDate.Checked = True Then   'PROBATION
                mintOptionId = 2
            ElseIf radEOCDate.Checked = True Then  'END OF CONTRACT
                mintOptionId = 3
            ElseIf radTerminationDate.Checked = True Then  'TERMITATED
                mintOptionId = 4
            ElseIf radRetirementDate.Checked = True Then  'RETIREMENT
                mintOptionId = 5
            End If



            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

            'If objBalTran.UpdateBalanceForTerminatedEmployee(mintOptionId, dtFinalTab) = False Then
            '    eZeeMsgBox.Show(objBalTran._Message, enMsgBoxStyle.Information)
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave cycle ends successfully for checked employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    If mintEmployeeId > 0 Then
            '        Me.Close()
            '    Else
            '        objbtnSearch_Click(sender, e)
            '    End If
            'End If

            If objBalTran.UpdateBalanceForTerminatedEmployee(mintOptionId, dtFinalTab, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                        , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                                        , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True _
                                                                                        , -1, "", ConfigParameter._Object._LeaveBalanceSetting) = False Then
                eZeeMsgBox.Show(objBalTran._Message, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave cycle ends successfully for checked employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                If mintEmployeeId > 0 Then
                    Me.Close()
                Else
                    objbtnSearch_Click(sender, e)
                End If
            End If

            'Pinkal (24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEnd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            If dtFinalTab Is Nothing Then Exit Sub

            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgData.Rows.Count > 0 Then
                        If dgData.SelectedRows(0).Index = dgData.Rows(dgData.RowCount - 1).Index Then Exit Sub
                        dgData.Rows(dgData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgData.Rows.Count > 0 Then
                        If dgData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgData.Rows(dgData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim strSearch As String = ""
        Try
            If dtFinalTab Is Nothing Then Exit Sub

            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhEmployee.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhJob.DataPropertyName & " LIKE '%" & txtSearch.Text & "%'"
                objChkAll.Checked = False
                objChkAll.Enabled = False
            Else
                objChkAll.Enabled = True
            End If
            dgView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgData.CellContentClick, dgData.CellContentDoubleClick
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If e.ColumnIndex = objdgcolhCheck.Index Then
                dtFinalTab.Rows(e.RowIndex)(e.ColumnIndex) = Not CBool(dtFinalTab.Rows(e.RowIndex)(e.ColumnIndex))
                dgData.RefreshEdit()
                Dim drRow As DataRow() = dtFinalTab.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtFinalTab.Rows.Count = drRow.Length Then
                        objChkAll.CheckState = CheckState.Checked
                    Else
                        objChkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objChkAll.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler dgData.CellContentClick, AddressOf dgData_CellContentClick
            For Each dr As DataRow In dtFinalTab.Rows
                dr.Item("ischeck") = CBool(objChkAll.CheckState)
            Next
            AddHandler dgData.CellContentClick, AddressOf dgData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnEnd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEnd.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnEnd.Text = Language._Object.getCaption(Me.btnEnd.Name, Me.btnEnd.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
			Me.radRetirementDate.Text = Language._Object.getCaption(Me.radRetirementDate.Name, Me.radRetirementDate.Text)
			Me.radTerminationDate.Text = Language._Object.getCaption(Me.radTerminationDate.Name, Me.radTerminationDate.Text)
			Me.radEOCDate.Text = Language._Object.getCaption(Me.radEOCDate.Name, Me.radEOCDate.Text)
			Me.radProbationDate.Text = Language._Object.getCaption(Me.radProbationDate.Name, Me.radProbationDate.Text)
			Me.radSuspensionDate.Text = Language._Object.getCaption(Me.radSuspensionDate.Name, Me.radSuspensionDate.Text)
			Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
			Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
			Me.dgcolhStDate.HeaderText = Language._Object.getCaption(Me.dgcolhStDate.Name, Me.dgcolhStDate.HeaderText)
			Me.dgcolhEdDate.HeaderText = Language._Object.getCaption(Me.dgcolhEdDate.Name, Me.dgcolhEdDate.HeaderText)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "There is no employee to end the leave cycle.",  enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
			Language.setMessage(mstrModuleName, 2, "Please select atleast one employee to do further operation.",  enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
			Language.setMessage(mstrModuleName, 3, "Leave cycle ends successfully for checked employee(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class