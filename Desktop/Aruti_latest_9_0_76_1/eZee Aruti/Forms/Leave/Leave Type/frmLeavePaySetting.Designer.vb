﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeavePaySetting
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeavePaySetting))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlLeavePaySettings = New System.Windows.Forms.Panel
        Me.ellvpayNotificationSetting = New eZee.Common.eZeeLine
        Me.chkLvpaysendNotifications = New System.Windows.Forms.CheckBox
        Me.objbtnLeavePayNtfUser = New eZee.Common.eZeeGradientButton
        Me.dgLeavePayTenure = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgcolhTenureUpto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDeduction_per = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLeavePaySettingId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlLeavePaySettings.SuspendLayout()
        CType(Me.dgLeavePayTenure, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlLeavePaySettings
        '
        Me.pnlLeavePaySettings.Controls.Add(Me.ellvpayNotificationSetting)
        Me.pnlLeavePaySettings.Controls.Add(Me.chkLvpaysendNotifications)
        Me.pnlLeavePaySettings.Controls.Add(Me.objbtnLeavePayNtfUser)
        Me.pnlLeavePaySettings.Controls.Add(Me.dgLeavePayTenure)
        Me.pnlLeavePaySettings.Controls.Add(Me.objFooter)
        Me.pnlLeavePaySettings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLeavePaySettings.Location = New System.Drawing.Point(0, 0)
        Me.pnlLeavePaySettings.Name = "pnlLeavePaySettings"
        Me.pnlLeavePaySettings.Size = New System.Drawing.Size(429, 345)
        Me.pnlLeavePaySettings.TabIndex = 0
        '
        'ellvpayNotificationSetting
        '
        Me.ellvpayNotificationSetting.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ellvpayNotificationSetting.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.ellvpayNotificationSetting.Location = New System.Drawing.Point(7, 238)
        Me.ellvpayNotificationSetting.Name = "ellvpayNotificationSetting"
        Me.ellvpayNotificationSetting.Size = New System.Drawing.Size(409, 15)
        Me.ellvpayNotificationSetting.TabIndex = 68
        Me.ellvpayNotificationSetting.Text = "Notification(s) Setting "
        Me.ellvpayNotificationSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkLvpaysendNotifications
        '
        Me.chkLvpaysendNotifications.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLvpaysendNotifications.Location = New System.Drawing.Point(14, 260)
        Me.chkLvpaysendNotifications.Name = "chkLvpaysendNotifications"
        Me.chkLvpaysendNotifications.Size = New System.Drawing.Size(323, 17)
        Me.chkLvpaysendNotifications.TabIndex = 60
        Me.chkLvpaysendNotifications.Text = "Send Notification to Selected user(s) on tenure exceeds"
        Me.chkLvpaysendNotifications.UseVisualStyleBackColor = True
        '
        'objbtnLeavePayNtfUser
        '
        Me.objbtnLeavePayNtfUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnLeavePayNtfUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnLeavePayNtfUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnLeavePayNtfUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnLeavePayNtfUser.BorderSelected = False
        Me.objbtnLeavePayNtfUser.ClickColor1 = System.Drawing.Color.Transparent
        Me.objbtnLeavePayNtfUser.ClickColor2 = System.Drawing.Color.Transparent
        Me.objbtnLeavePayNtfUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnLeavePayNtfUser.HoverColor1 = System.Drawing.Color.Transparent
        Me.objbtnLeavePayNtfUser.HoverColor2 = System.Drawing.Color.Transparent
        Me.objbtnLeavePayNtfUser.Image = Global.Aruti.Main.My.Resources.Resources.user_blue_male
        Me.objbtnLeavePayNtfUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnLeavePayNtfUser.Location = New System.Drawing.Point(358, 258)
        Me.objbtnLeavePayNtfUser.Name = "objbtnLeavePayNtfUser"
        Me.objbtnLeavePayNtfUser.Size = New System.Drawing.Size(27, 21)
        Me.objbtnLeavePayNtfUser.TabIndex = 59
        '
        'dgLeavePayTenure
        '
        Me.dgLeavePayTenure.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgLeavePayTenure.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgLeavePayTenure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgLeavePayTenure.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhTenureUpto, Me.dgcolhDeduction_per, Me.objdgcolhLeavePaySettingId})
        Me.dgLeavePayTenure.Location = New System.Drawing.Point(5, 5)
        Me.dgLeavePayTenure.Name = "dgLeavePayTenure"
        Me.dgLeavePayTenure.RowHeadersWidth = 25
        Me.dgLeavePayTenure.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgLeavePayTenure.Size = New System.Drawing.Size(419, 226)
        Me.dgLeavePayTenure.TabIndex = 13
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 286)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(429, 59)
        Me.objFooter.TabIndex = 12
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(217, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(320, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgcolhTenureUpto
        '
        Me.dgcolhTenureUpto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N0"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgcolhTenureUpto.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhTenureUpto.HeaderText = "TenureUpto"
        Me.dgcolhTenureUpto.Name = "dgcolhTenureUpto"
        Me.dgcolhTenureUpto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDeduction_per
        '
        Me.dgcolhDeduction_per.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhDeduction_per.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhDeduction_per.HeaderText = "Deduction(%)"
        Me.dgcolhDeduction_per.Name = "dgcolhDeduction_per"
        '
        'objdgcolhLeavePaySettingId
        '
        Me.objdgcolhLeavePaySettingId.HeaderText = "LeavePaySettingId"
        Me.objdgcolhLeavePaySettingId.Name = "objdgcolhLeavePaySettingId"
        Me.objdgcolhLeavePaySettingId.ReadOnly = True
        Me.objdgcolhLeavePaySettingId.Visible = False
        '
        'frmLeavePaySetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(429, 345)
        Me.Controls.Add(Me.pnlLeavePaySettings)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeavePaySetting"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Pay Settings"
        Me.pnlLeavePaySettings.ResumeLayout(False)
        CType(Me.dgLeavePayTenure, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlLeavePaySettings As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgLeavePayTenure As System.Windows.Forms.DataGridView
    Friend WithEvents chkLvpaysendNotifications As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnLeavePayNtfUser As eZee.Common.eZeeGradientButton
    Friend WithEvents ellvpayNotificationSetting As eZee.Common.eZeeLine
    Friend WithEvents dgcolhTenureUpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDeduction_per As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLeavePaySettingId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
