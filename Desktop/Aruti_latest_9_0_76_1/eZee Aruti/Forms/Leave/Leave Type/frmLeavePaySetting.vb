﻿Option Strict On

#Region " Imports "
Imports System.Windows.Forms
Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLeavePaySetting

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmLeavePaySetting"
    Private mblnCancel As Boolean = True
    Private mdtLeavePay As DataTable = Nothing
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLeaveTypeId As Integer = 0
    Private objLeavePay As clsleavepaysettings
    Private mstrLeaveNotifcationIds As String = ""
    Private dtOldLeavePaySetting As DataTable = Nothing
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef dtPaySetting As DataTable, ByRef _OldLeavePaySetting As DataTable, ByRef _LeaveNotificationUserIds As String, ByVal xLeaveTypeId As Integer, ByVal eAction As enAction) As Boolean
        objLeavePay = New clsleavepaysettings
        Try
            mintLeaveTypeId = xLeaveTypeId
            mstrLeaveNotifcationIds = _LeaveNotificationUserIds
            If dtPaySetting IsNot Nothing Then
                mdtLeavePay = dtPaySetting.Copy
            End If
            If _OldLeavePaySetting IsNot Nothing Then
                dtOldLeavePaySetting = _OldLeavePaySetting.Copy
            End If

            menAction = eAction
            Me.ShowDialog()
            dtPaySetting = mdtLeavePay
            _LeaveNotificationUserIds = mstrLeaveNotifcationIds
            _OldLeavePaySetting = dtOldLeavePaySetting
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmLeavePaySetting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeavePay = New clsleavepaysettings
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillLeavePayList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePaySetting_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePaySetting_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePaySetting_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePaySetting_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePaySetting_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePaySetting_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeavePay = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'objLeavePay.SetMessages()
            objfrm._Other_ModuleNames = "clsleavepaysettings"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillLeavePayList()
        Try
            Dim mstrFilter As String = ""

            If mdtLeavePay Is Nothing OrElse mdtLeavePay.Rows.Count <= 0 Then
                objLeavePay.GetList("List", mintLeaveTypeId, True, Nothing)
                mdtLeavePay = objLeavePay._dtPaySetting.Copy
            End If

            dgLeavePayTenure.AutoGenerateColumns = False
            dgcolhTenureUpto.DataPropertyName = "tenureupto"
            dgcolhDeduction_per.DataPropertyName = "deduction_percentage"
            objdgcolhLeavePaySettingId.DataPropertyName = "leavepayunkid"
            dgLeavePayTenure.DataSource = mdtLeavePay

            If mstrLeaveNotifcationIds.Trim.Length > 0 Then
                objbtnLeavePayNtfUser.Tag = mstrLeaveNotifcationIds
                chkLvpaysendNotifications.Checked = True
            Else
                chkLvpaysendNotifications.Checked = False
                objbtnLeavePayNtfUser.Tag = Nothing
            End If
            chkLvpaysendNotifications_CheckedChanged(chkLvpaysendNotifications, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLeavePayList", mstrModuleName)
        End Try
    End Sub

    Private Sub Update_DataGridview_DataTableColumns(ByVal objDataGridView As DataGridView, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Try

            If dgLeavePayTenure.Rows(e.RowIndex).IsNewRow = True Then Exit Try

            If mdtLeavePay Is Nothing OrElse mdtLeavePay.Rows.Count < 0 OrElse e.RowIndex > mdtLeavePay.Rows.Count Then Exit Sub

            If mdtLeavePay.Rows(e.RowIndex).RowState <> DataRowState.Deleted Then

                If IsDBNull(mdtLeavePay.Rows(e.RowIndex).Item("leavepayunkid")) Then
                    mdtLeavePay.Rows(e.RowIndex).Item("leavepayunkid") = 0
                End If

                If IsDBNull(mdtLeavePay.Rows(e.RowIndex).Item("leavetypeunkid")) Then
                    mdtLeavePay.Rows(e.RowIndex).Item("leavetypeunkid") = mintLeaveTypeId
                End If

                If IsDBNull(mdtLeavePay.Rows(e.RowIndex).Item("isvoid")) Then
                    mdtLeavePay.Rows(e.RowIndex).Item("isvoid") = False
                End If

                If IsDBNull(mdtLeavePay.Rows(e.RowIndex).Item("voiduserunkid")) Then
                    mdtLeavePay.Rows(e.RowIndex).Item("voiduserunkid") = 0
                End If

                If IsDBNull(mdtLeavePay.Rows(e.RowIndex).Item("voidreason")) Then
                    mdtLeavePay.Rows(e.RowIndex).Item("voidreason") = ""
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_DataGridview_DataTableColumns", mstrModuleName)
        End Try
    End Sub

    Public Sub tb_keypress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Dim isBackSpace As Boolean = False
        Try

            Dim tb As TextBox
            tb = CType(sender, TextBox)

            Select Case Asc(e.KeyChar)
                Case 48 To 59 'These are 0 to 9.
                    Dim d As Decimal

                    Decimal.TryParse(CStr(IIf(tb.Text = "", 0, tb.Text)), d)

                    If tb.SelectionLength = tb.Text.Length Then 'If 0 to 9 pressed on selected text, 
                        'Do nothing

                    ElseIf InStr(tb.Text, ".") = 0 AndAlso d.ToString.Length = 6 Then 'Allow only 6 digit before entering "."
                        e.Handled = True
                        Exit Sub
                    ElseIf InStr(tb.Text.Trim, ".") > 0 AndAlso tb.Text.Substring(InStr(tb.Text.Trim, ".") - 1).Trim.Length > 2 Then 'Allow only 2 Decimal
                        e.Handled = True
                        Exit Sub
                    ElseIf InStr(tb.Text.Trim, ".") > 0 AndAlso tb.Text.Substring(0, InStr(tb.Text.Trim, ".") - 1).Trim.Length >= 3 AndAlso tb.SelectionStart <= 3 Then 'Allow only 3 digit before Decimal after entering "."
                        e.Handled = True
                        Exit Sub
                    ElseIf InStr(tb.Text, "-") <> 0 AndAlso tb.SelectionStart = 0 Then 'Do not allow digit before - sign  e.g.  4-1234
                        e.Handled = True
                        Exit Sub
                    End If
                Case Keys.Back 'BACKSPACE
                    'This is fine. Do nothing

                Case Keys.Insert ' -  sign to allow negative
                    If tb.SelectionLength = tb.Text.Length Then 'If - pressed on selected text, 
                        'Do nothing

                    ElseIf InStr(tb.Text, "-") <> 0 Then 'Number can only have 1 minus sign
                        Dim i As Integer = tb.SelectionStart
                        tb.Text = tb.Text.Replace("-", "")
                        tb.SelectionStart = i
                        e.Handled = True
                        Exit Sub
                    End If

                    'if the insertion point is not sitting at zero, put minus at zero position
                    If tb.SelectionStart <> 0 Then
                        Dim i As Integer = tb.SelectionStart
                        tb.Text = e.KeyChar & tb.Text
                        tb.SelectionStart = i + 1
                        e.Handled = True
                        Exit Sub
                    End If
                Case Keys.Delete 'Period/Decimal "."
                    If tb.SelectionLength = tb.Text.Length Then 'If . pressed on selected text, 
                        'Do nothing

                    ElseIf InStr(tb.Text, ".") <> 0 Then 'number can have only 1 period
                        e.Handled = True
                        Exit Sub
                    End If

                Case Else
                    e.Handled = True
                    Exit Sub
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tb_keypress", "modglobal")
        End Try
    End Sub

#End Region

#Region "Button Events"

    Private Sub objbtnLeavePayNtfUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLeavePayNtfUser.Click
        Dim frm As New objfrmERCUserSelection
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim strButtonTagValues As String = String.Empty
            If CType(sender, eZee.Common.eZeeGradientButton).Tag Is Nothing Then
                mstrLeaveNotifcationIds = ""
            Else
                mstrLeaveNotifcationIds = CType(sender, eZee.Common.eZeeGradientButton).Tag.ToString
            End If
            If frm.displayDialog(mstrLeaveNotifcationIds, Company._Object._Companyunkid) Then
                CType(sender, eZee.Common.eZeeGradientButton).Tag = mstrLeaveNotifcationIds
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnLeavePayNtfUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If dgLeavePayTenure.DataSource IsNot Nothing AndAlso dgLeavePayTenure.RowCount > 0 Then
                mdtLeavePay = CType(dgLeavePayTenure.DataSource, DataTable)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please set atleast one tenure upto and deduction(%) to do further operation on it."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Events"

    Private Sub dgLeavePayTenure_UserDeletedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles dgLeavePayTenure.UserDeletedRow
        Try
            mdtLeavePay.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeavePayTenure_UserDeletedRow", mstrModuleName)
        End Try
    End Sub

    Private Sub dgLeavePayTenure_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgLeavePayTenure.UserDeletingRow
        Try
            If e.Row.Index < 0 Then Exit Sub

            If e.Row.Index + 1 < dgLeavePayTenure.RowCount - 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please delete the tenure up to from bottom to top."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeavePayTenure_UserDeletingRow", mstrModuleName)
        End Try
    End Sub

    Private Sub dgLeavePayTenure_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgLeavePayTenure.EditingControlShowing
        Dim tb As TextBox
        Try
            tb = CType(e.Control, TextBox)
            RemoveHandler tb.KeyPress, AddressOf tb_keypress
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeavePayTenure_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgLeavePayTenure_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgLeavePayTenure.CellValidating
        Try
            If dgLeavePayTenure.Rows(e.RowIndex).IsNewRow = True Then Exit Try

            If dgLeavePayTenure.CurrentCell.ColumnIndex = dgcolhTenureUpto.Index Then
                If e.RowIndex - 1 >= 0 AndAlso IsDBNull(e.FormattedValue) = False AndAlso IsDBNull(CInt(dgLeavePayTenure.Rows(e.RowIndex - 1).Cells(dgcolhTenureUpto.Index).Value)) = False AndAlso CInt(e.FormattedValue) < CInt(dgLeavePayTenure.Rows(e.RowIndex - 1).Cells(dgcolhTenureUpto.Index).Value) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Current Tenure upto should be greater than previous one."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    e.Cancel = True
                    Exit Sub
                ElseIf e.RowIndex > 0 AndAlso IsDBNull(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) = False Then
                    Dim drRow = mdtLeavePay.AsEnumerable().Where(Function(x) x.Field(Of Integer)("tenureupto") <= CInt(e.FormattedValue) And e.RowIndex < mdtLeavePay.Rows.IndexOf(x))
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Current Tenure upto should be less than next all tenure."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeavePayTenure_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgLeavePayTenure_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLeavePayTenure.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub
            If menAction = enAction.EDIT_ONE AndAlso (mdtLeavePay IsNot Nothing AndAlso mdtLeavePay.Rows.Count > 0) Then
                dgLeavePayTenure.UpdateCellValue(e.ColumnIndex, e.RowIndex)
                If e.RowIndex < mdtLeavePay.Rows.Count AndAlso CInt(mdtLeavePay.Rows(e.RowIndex)("leavepayunkid")) > 0 Then
                    mdtLeavePay.Rows(e.RowIndex)("AUD") = "U"
                    mdtLeavePay.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeavePayTenure_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgLeavePayTenure_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgLeavePayTenure.RowValidating
        Try

            If dgLeavePayTenure.Rows(e.RowIndex).IsNewRow = True Then Exit Try


            If IsDBNull(dgLeavePayTenure.Rows(e.RowIndex).Cells(dgcolhTenureUpto.Index).Value) OrElse CDec(dgLeavePayTenure.Rows(e.RowIndex).Cells(dgcolhTenureUpto.Index).Value) = 0 Then
                e.Cancel = True
                Exit Sub
            End If

            If dgLeavePayTenure.CurrentCell.ColumnIndex = dgcolhTenureUpto.Index Then
                If e.RowIndex - 1 >= 0 AndAlso IsDBNull(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) = False AndAlso IsDBNull(CInt(dgLeavePayTenure.Rows(e.RowIndex - 1).Cells(dgcolhTenureUpto.Index).Value)) = False AndAlso CInt(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) < CInt(dgLeavePayTenure.Rows(e.RowIndex - 1).Cells(dgcolhTenureUpto.Index).Value) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Current Tenure upto should be greater than previous one."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    e.Cancel = True
                    Exit Sub
                ElseIf e.RowIndex >= 0 AndAlso IsDBNull(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) = False Then
                    Dim drRow = mdtLeavePay.AsEnumerable().Where(Function(x) x.Field(Of Integer)("tenureupto") <= CInt(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) And e.RowIndex < mdtLeavePay.Rows.IndexOf(x))
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Current Tenure upto should be less than next all tenure."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeavePayTenure_RowValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgLeavePayTenure_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLeavePayTenure.RowValidated
        Try

            If dgLeavePayTenure.Rows(e.RowIndex).IsNewRow = True Then Exit Try

            If dgLeavePayTenure.CurrentCell.ColumnIndex = dgcolhTenureUpto.Index Then
                If e.RowIndex - 1 >= 0 AndAlso IsDBNull(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) = False AndAlso IsDBNull(CInt(dgLeavePayTenure.Rows(e.RowIndex - 1).Cells(dgcolhTenureUpto.Index).Value)) = False AndAlso CInt(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) < CInt(dgLeavePayTenure.Rows(e.RowIndex - 1).Cells(dgcolhTenureUpto.Index).Value) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Current Tenure upto should be greater than previous one."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                ElseIf e.RowIndex >= 0 AndAlso IsDBNull(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) = False Then
                    Dim drRow = mdtLeavePay.AsEnumerable().Where(Function(x) x.Field(Of Integer)("tenureupto") <= CInt(dgLeavePayTenure.CurrentRow.Cells(dgcolhTenureUpto.Index).Value) And e.RowIndex < mdtLeavePay.Rows.IndexOf(x))
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Current Tenure upto should be less than next all tenure."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                End If
            End If

            Update_DataGridview_DataTableColumns(dgLeavePayTenure, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeavePayTenure_RowValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgLeavePayTenure_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgLeavePayTenure.DataError

    End Sub

#End Region

#Region "Checkbox Events"

    Private Sub chkLvpaysendNotifications_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLvpaysendNotifications.CheckedChanged
        Try
            objbtnLeavePayNtfUser.Enabled = chkLvpaysendNotifications.Checked
            If chkLvpaysendNotifications.Checked = False Then
                mstrLeaveNotifcationIds = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkLvpaysendNotifications_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkLvpaysendNotifications.Text = Language._Object.getCaption(Me.chkLvpaysendNotifications.Name, Me.chkLvpaysendNotifications.Text)
            Me.ellvpayNotificationSetting.Text = Language._Object.getCaption(Me.ellvpayNotificationSetting.Name, Me.ellvpayNotificationSetting.Text)
            Me.dgcolhTenureUpto.HeaderText = Language._Object.getCaption(Me.dgcolhTenureUpto.Name, Me.dgcolhTenureUpto.HeaderText)
            Me.dgcolhDeduction_per.HeaderText = Language._Object.getCaption(Me.dgcolhDeduction_per.Name, Me.dgcolhDeduction_per.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


   
    
   
End Class