﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmAssignHolidays


#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssignHolidays"
    Private mblnCancel As Boolean = True
    Private objEmployeeholiday As clsemployee_holiday
    Private menAction As enAction = enAction.ADD_ONE
    Private mintEmployeeHolidayUnkid As Integer = -1
    Private mintEmployeeid As Integer = -1
    Private mstrAdvanceFilter As String = ""


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeId As Integer = -1) As Boolean
        Try
            mintEmployeeHolidayUnkid = intUnkId
            mintEmployeeid = intEmployeeId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintEmployeeHolidayUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmAssignHolidays_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployeeholiday = New clsemployee_holiday
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            If menAction = enAction.EDIT_ONE Then
                objEmployeeholiday._Holidaytranunkid = mintEmployeeHolidayUnkid
            End If
            FillCombo()
            FillEmployeeList()
            FillHolidayList()
            Getvalue()
            cboDepartment.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignHolidays_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignHolidays_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignHolidays_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignHolidays_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignHolidays_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignHolidays_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmployeeholiday = Nothing
    End Sub


    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Anjan (02 Sep 2011)-End 


#End Region

#Region "LinkButton's Event"

    'Private Sub lblSelectAllEmp_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
    '    Try
    '        chkSelectAll.Checked = True
    '        CheckAllEmployee(True)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lblSelectAllEmp_LinkClicked", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    'Private Sub cboLeaveYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        FillHolidayList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboLeaveYear_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            'Pinkal (10-Mar-2011) -- Start

            If lvEmployeeList.Items.Count = 0 Then Exit Sub
            CheckAllEmployee(chkSelectAll.Checked)

            'Pinkal (10-Mar-2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkHolidayAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkHolidayAll.CheckedChanged
        Try
            'For i As Integer = 0 To lvHolidays.Items.Count - 1
            '    lvHolidays.Items(i).Checked = chkHolidayAll.Checked
            'Next

            'Pinkal (10-Mar-2011) -- Start

            If lvHolidays.Items.Count = 0 Then Exit Sub
            SetholidayOperation(objchkHolidayAll.Checked)

            'Pinkal (10-Mar-2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkHolidayAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If lvEmployeeList.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select atleast One Employee."), enMsgBoxStyle.Information)
                lvEmployeeList.Select()
                Exit Sub
            ElseIf lvHolidays.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Holiday is compulsory information.Please Select atleast One Holiday."), enMsgBoxStyle.Information)
                lvHolidays.Select()
                If lvHolidays.Items.Count > 0 Then lvHolidays.Items(0).Selected = True
                Exit Sub
            End If

            objEmployeeholiday._Userunkid = User._Object._Userunkid
            objEmployeeholiday._Remarks = txtRemark.Text.Trim

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objEmployeeholiday.InsertEmployeeHoliday(GetSelectedEmployeeID(), GetSelectedHolidayID())

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
            'blnFlag = objEmployeeholiday.InsertEmployeeHoliday(GetSelectedEmployeeID(), GetSelectedHolidayID(), FinancialYear._Object._DatabaseName)
            blnFlag = objEmployeeholiday.InsertEmployeeHoliday(GetSelectedEmployeeID(), GetSelectedHolidayID(), FinancialYear._Object._DatabaseName, ConfigParameter._Object._AllowOverTimeToEmpTimesheet)
            'Pinkal (28-Mar-2018) --  End


            'Sohail (21 Aug 2015) -- End


            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            'If blnFlag = False And objEmployeeholiday._Message <> "" Then
            If objEmployeeholiday._Message <> "" Then
                eZeeMsgBox.Show(objEmployeeholiday._Message, enMsgBoxStyle.Information)
            End If
            'Pinkal (13-Jan-2015) -- End

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objEmployeeholiday = Nothing
                    objEmployeeholiday = New clsemployee_holiday
                    Getvalue()
                    cboDepartment.Select()
                Else
                    ' mintEmployeeHolidayUnkid = objEmployeeholiday._Holidaytranunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillEmployeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboDepartment.SelectedIndex = 0
            cboSections.SelectedIndex = 0
            cboJob.SelectedIndex = 0
            cboGrade.SelectedIndex = 0
            cboShiftInfo.SelectedIndex = 0
            cboClass.SelectedIndex = 0
            mstrAdvanceFilter = ""
            FillEmployeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub Getvalue()
        Try
            objchkHolidayAll.Checked = False
            chkSelectAll.Checked = False
            txtRemark.Text = objEmployeeholiday._Remarks

            'START FOR CLEAR EMPLOYEE LIST

            If lvEmployeeList.CheckedItems.Count > 0 Then
                For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
                    lvEmployeeList.CheckedItems(0).Checked = False
                Next
            End If

            'START FOR CLEAR EMPLOYEE LIST

            'START FOR CLEAR HOLIDAY LIST

            If lvHolidays.CheckedItems.Count > 0 Then
                For i As Integer = 0 To lvHolidays.CheckedItems.Count - 1
                    lvHolidays.CheckedItems(0).Checked = False
                Next
            End If

            'END FOR CLEAR HOLIDAY LIST


            If mintEmployeeid > 0 Then

                'START FOR CHECKED PARTICULAR EMPLOYEE
                For i As Integer = 0 To lvEmployeeList.Items.Count - 1
                    If CInt(lvEmployeeList.Items(i).Tag) = mintEmployeeid Then
                        lvEmployeeList.Items(i).Checked = True
                    End If
                Next
                'END FOR CHECKED PARTICULAR EMPLOYEE

                'START FOR CHECKED PARTICULAR EMPLOYEE HOLIDAY
                Dim dtHoliday As DataTable = objEmployeeholiday.GetEmployeeHoliday(mintEmployeeid)
                If Not dtHoliday Is Nothing And dtHoliday.Rows.Count > 0 Then
                    For i As Integer = 0 To dtHoliday.Rows.Count - 1
                        For j As Integer = 0 To lvHolidays.Items.Count - 1
                            If CInt(lvHolidays.Items(j).Tag) = CInt(dtHoliday.Rows(i)("holidayunkid")) Then
                                lvHolidays.Items(j).Checked = True
                                Exit For
                            End If
                        Next
                    Next
                End If

                'END FOR CHECKED PARTICULAR EMPLOYEE HOLIDAY



            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Getvalue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            'FOR DEPARTMENT
            Dim objDepartment As New clsDepartment
            dsFill = objDepartment.getComboList("Department", True)
            cboDepartment.ValueMember = "departmentunkid"
            cboDepartment.DisplayMember = "name"
            cboDepartment.DataSource = dsFill.Tables(0)

            'FOR SECTION
            dsFill = Nothing
            Dim objSection As New clsSections
            dsFill = objSection.getComboList("Section", True)
            cboSections.ValueMember = "sectionunkid"
            cboSections.DisplayMember = "name"
            cboSections.DataSource = dsFill.Tables(0)

            'FOR JOB
            dsFill = Nothing
            Dim objJob As New clsJobs
            dsFill = objJob.getComboList("Job", True)
            cboJob.ValueMember = "jobunkid"
            cboJob.DisplayMember = "name"
            cboJob.DataSource = dsFill.Tables(0)

            'FOR GRADE
            dsFill = Nothing
            Dim objGrade As New clsGrade
            dsFill = objGrade.getComboList("Grade", True)
            cboGrade.ValueMember = "gradeunkid"
            cboGrade.DisplayMember = "name"
            cboGrade.DataSource = dsFill.Tables(0)

            'FOR SHIFT
            dsFill = Nothing

            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes
            'Dim objShift As New clsshift_master
            Dim objShift As New clsNewshift_master
            'Pinkal (03-Jul-2013) -- End

            dsFill = objShift.getListForCombo("Shift", True)
            cboShiftInfo.ValueMember = "shiftunkid"
            cboShiftInfo.DisplayMember = "name"
            cboShiftInfo.DataSource = dsFill.Tables(0)

            'FOR CLASS
            dsFill = Nothing
            Dim objClass As New clsClass
            dsFill = objClass.getComboList("Clas", True)
            cboClass.ValueMember = "classesunkid"
            cboClass.DisplayMember = "name"
            cboClass.DataSource = dsFill.Tables(0)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillHolidayList()
        Dim dsHoliday As DataSet = Nothing
        Dim dtHoliday As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try
            Dim objHoliday As New clsholiday_master
            dsHoliday = objHoliday.GetList("Holiday", True)

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtHoliday = New DataView(dsHoliday.Tables("Holiday"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtHoliday = dsHoliday.Tables("Holiday")
            End If


            'Pinkal (12-Oct-2011) -- Start
            RemoveHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
            'Pinkal (12-Oct-2011) -- End


            lvHolidays.Items.Clear()
            If Not dtHoliday Is Nothing Then
                Dim lvItem As ListViewItem

                For Each drRow As DataRow In dtHoliday.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = eZeeDate.convertDate(drRow("holidaydate").ToString).ToShortDateString
                    lvItem.Tag = drRow("holidayunkid")
                    lvItem.SubItems.Add(drRow("holidayname").ToString)
                    lvHolidays.Items.Add(lvItem)
                Next
            End If
            If lvHolidays.Items.Count > 0 Then
                objchkHolidayAll.Enabled = True
            Else
                objchkHolidayAll.Enabled = False
            End If


            'Pinkal (12-Oct-2011) -- Start
            AddHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHolidayList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim dsEmployee As DataSet = Nothing
        Dim dtEmployee As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try
            Dim objEmployee As New clsEmployee_Master
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetList("Employee")

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsEmployee = objEmployee.GetList("Employee")
            'Else
            '    dsEmployee = objEmployee.GetList("Employee", , True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), mintEmployeeid)
            'End If

            ''Sohail (06 Jan 2012) -- End

            'If mintEmployeeid > 0 Then
            '    strSearch = "AND employeeunkid=" & mintEmployeeid & " "
            'End If
            'If CInt(cboDepartment.SelectedValue) > 0 Then
            '    strSearch &= "AND departmentunkid=" & CInt(cboDepartment.SelectedValue)
            'End If

            'If CInt(cboSections.SelectedValue) > 0 Then
            '    strSearch &= "AND sectionunkid=" & CInt(cboSections.SelectedValue) & " "
            'End If

            'If CInt(cboJob.SelectedValue) > 0 Then
            '    strSearch &= "AND jobunkid=" & CInt(cboJob.SelectedValue) & " "
            'End If

            'If CInt(cboGrade.SelectedValue) > 0 Then
            '    strSearch &= "AND gradeunkid=" & CInt(cboGrade.SelectedValue) & " "
            'End If

            'If CInt(cboGrade.SelectedValue) > 0 Then
            '    strSearch &= "AND gradeunkid=" & CInt(cboGrade.SelectedValue) & " "
            'End If

            'If CInt(cboShiftInfo.SelectedValue) > 0 Then
            '    strSearch &= "AND shiftunkid=" & CInt(cboShiftInfo.SelectedValue) & " "
            'End If

            'If CInt(cboClass.SelectedValue) > 0 Then
            '    strSearch &= "AND classunkid=" & CInt(cboClass.SelectedValue) & " "
            'End If

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : CHECK FOR ACTIVE EMPLOYEE


            ''If strSearch.Length > 0 Then
            ''    strSearch = strSearch.Substring(3)
            ''    dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            ''Else
            ''    dtEmployee = dsEmployee.Tables("Employee")
            ''End If

            ''If ConfigParameter._Object._IsIncludeInactiveEmp Then
            'If strSearch.Length > 0 Then
            '    strSearch = strSearch.Substring(3)
            '    dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtEmployee = dsEmployee.Tables("Employee")
            'End If

            '' Else

            '' If strSearch.Length > 0 Then
            ''strSearch = strSearch.Substring(3)
            ''  dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch & " AND isactive = 1", "", DataViewRowState.CurrentRows).ToTable
            '' Else
            '' dtEmployee = New DataView(dsEmployee.Tables("Employee"), " isactive = 1", "", DataViewRowState.CurrentRows).ToTable
            '' End If

            ''End If

            ''Pinkal (24-Jun-2011) -- End

            If mintEmployeeid > 0 Then
                'strSearch = "AND employeeunkid=" & mintEmployeeid & " "
                strSearch = "AND hremployee_master.employeeunkid=" & mintEmployeeid & " "
            End If
            If CInt(cboDepartment.SelectedValue) > 0 Then
                strSearch &= "AND ETRF.departmentunkid=" & CInt(cboDepartment.SelectedValue)
            End If

            If CInt(cboSections.SelectedValue) > 0 Then
                strSearch &= "AND ETRF.sectionunkid=" & CInt(cboSections.SelectedValue) & " "
            End If

            If CInt(cboJob.SelectedValue) > 0 Then
                strSearch &= "AND ERECAT.jobunkid=" & CInt(cboJob.SelectedValue) & " "
            End If

            If CInt(cboGrade.SelectedValue) > 0 Then
                strSearch &= "AND EGRD.gradeunkid=" & CInt(cboGrade.SelectedValue) & " "
            End If

            If CInt(cboGrade.SelectedValue) > 0 Then
                strSearch &= "AND EGRD.gradeunkid=" & CInt(cboGrade.SelectedValue) & " "
            End If

            If CInt(cboShiftInfo.SelectedValue) > 0 Then
                strSearch &= "AND shiftunkid=" & CInt(cboShiftInfo.SelectedValue) & " "
            End If

            If CInt(cboClass.SelectedValue) > 0 Then
                strSearch &= "AND ETRF.classunkid=" & CInt(cboClass.SelectedValue) & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             "Employee", _
                                             ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearch)

            dtEmployee = dsEmployee.Tables(0)

            'S.SANDEEP [04 JUN 2015] -- END

            RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked

            If Not dtEmployee Is Nothing Then
                Dim lvItem As ListViewItem
                'Pinkal (10-Aug-2016) -- Start
                'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
                Dim lvArray As New List(Of ListViewItem)
                'Pinkal (10-Aug-2016) -- End
                lvEmployeeList.Items.Clear()

                For Each drRow As DataRow In dtEmployee.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = ""
                    lvItem.Tag = drRow("employeeunkid")
                    lvItem.SubItems.Add(drRow("employeecode").ToString)
                    lvItem.SubItems.Add(drRow("name").ToString)
                    'Pinkal (10-Aug-2016) -- Start
                    'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
                    'lvEmployeeList.Items.Add(lvItem)
                    lvArray.Add(lvItem)
                    'Pinkal (10-Aug-2016) -- End
                Next
                'Pinkal (10-Aug-2016) -- Start
                'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
                lvEmployeeList.Items.AddRange(lvArray.ToArray)
                'Pinkal (10-Aug-2016) -- End
            End If
            If lvEmployeeList.Items.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
            End If

            AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : CHECK FOR ACTIVE EMPLOYEE

    Private Sub CheckAllEmployee(ByVal blnChecked As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmployeeList.Items
                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                lvItem.Checked = blnChecked
                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Jun-2011) -- End

    Private Function GetSelectedEmployeeID() As String
        Dim strEmp As String = String.Empty
        Try
            For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
                strEmp = strEmp & CInt(lvEmployeeList.CheckedItems(i).Tag) & ","
            Next

            If strEmp.Length > 0 Then
                strEmp = strEmp.Remove(strEmp.Length - 1)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSelectedEmployeeID", mstrModuleName)
        End Try
        Return strEmp
    End Function

    Private Function GetSelectedHolidayID() As String
        Dim strHoliday As String = String.Empty
        Try
            For i As Integer = 0 To lvHolidays.CheckedItems.Count - 1
                strHoliday = strHoliday & CInt(lvHolidays.CheckedItems(i).Tag) & ","
            Next
            If strHoliday.Length > 0 Then
                strHoliday = strHoliday.Remove(strHoliday.Length - 1)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSelectedHolidayID", mstrModuleName)
        End Try
        Return strHoliday
    End Function


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : CHECK FOR ACTIVE EMPLOYEE

    Private Sub SetholidayOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvHolidays.Items
                RemoveHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
                Item.Checked = blnOperation
                AddHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetholidayOperation", mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Jun-2011) -- End

#End Region

    'Pinkal (10-Mar-2011) -- Start

#Region "ListView Event"

    Private Sub lvEmployeeList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If lvEmployeeList.CheckedItems.Count <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
                chkSelectAll.CheckState = CheckState.Checked

            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvHolidays_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvHolidays.ItemChecked
        Try
            RemoveHandler objchkHolidayAll.CheckedChanged, AddressOf chkHolidayAll_CheckedChanged
            If lvHolidays.CheckedItems.Count <= 0 Then
                objchkHolidayAll.CheckState = CheckState.Unchecked

            ElseIf lvHolidays.CheckedItems.Count < lvHolidays.Items.Count Then
                objchkHolidayAll.CheckState = CheckState.Indeterminate

            ElseIf lvHolidays.CheckedItems.Count = lvHolidays.Items.Count Then
                objchkHolidayAll.CheckState = CheckState.Checked

            End If
            AddHandler objchkHolidayAll.CheckedChanged, AddressOf chkHolidayAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvHolidays_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (10-Mar-2011) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbHolidays.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbHolidays.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbRemark.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRemark.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhEmployeeName.Text = Language._Object.getCaption(CStr(Me.colhEmployeeName.Tag), Me.colhEmployeeName.Text)
            Me.gbHolidays.Text = Language._Object.getCaption(Me.gbHolidays.Name, Me.gbHolidays.Text)
            Me.gbRemark.Text = Language._Object.getCaption(Me.gbRemark.Name, Me.gbRemark.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.lblholidays.Text = Language._Object.getCaption(Me.lblholidays.Name, Me.lblholidays.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select atleast One Employee.")
            Language.setMessage(mstrModuleName, 2, "Holiday is compulsory information.Please Select atleast One Holiday.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class