﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

'Last Message Index = 6

Public Class frmImportLeaveAdjustment

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportLeaveAdjustment"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Dim objLeaveAdjustment As clsleaveadjustment_Tran
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmImportLeaveAdjustment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objLeaveAdjustment = New clsleaveadjustment_Tran
            chkBalanceAdjustment_CheckedChanged(chkBalanceAdjustment, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportLeaveAdjustment_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveadjustment_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveadjustment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillGirdView()
        Try

            If dsList IsNot Nothing Then


                dsList.Tables(0).Columns.Add("error", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("employeeunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("leavetypeunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("leavebalanceunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("iselc", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("isopenelc", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("isclose_fy", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("image", Type.GetType("System.Object"))


                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objLeave As New clsleavetype_master
                    Dim objAccrue As New clsleavebalance_tran
                    Dim mblnELC As Boolean = False
                    Dim mblnOpenELC As Boolean = False

                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        mblnELC = True
                        mblnOpenELC = True
                    End If

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        dsList.Tables(0).Rows(i)("error") = ""
                        dsList.Tables(0).Rows(i)("image") = New Drawing.Bitmap(1, 1).Clone
                        dsList.Tables(0).Rows(i)("employeeunkid") = objEmp.GetEmployeeUnkid("", dsList.Tables(0).Rows(i)("employeecode").ToString.Trim)
                        dsList.Tables(0).Rows(i)("leavetypeunkid") = objLeave.GetLeaveTypeUnkId(dsList.Tables(0).Rows(i)("leavename").ToString.Trim)
                        Dim dsBalance As DataSet = objAccrue.GetEmployeeBalanceData(CInt(dsList.Tables(0).Rows(i)("leavetypeunkid")).ToString(), CInt(dsList.Tables(0).Rows(i)("employeeunkid")).ToString(), objLeave._IsPaid, FinancialYear._Object._YearUnkid, mblnELC, mblnOpenELC, False, Nothing, FinancialYear._Object._DatabaseName)
                        If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                            dsList.Tables(0).Rows(i)("leavebalanceunkid") = CInt(dsBalance.Tables(0).Rows(0)("leavebalanceunkid"))
                            dsList.Tables(0).Rows(i)("iselc") = CBool(dsBalance.Tables(0).Rows(0)("iselc"))
                            dsList.Tables(0).Rows(i)("isopenelc") = CBool(dsBalance.Tables(0).Rows(0)("isopenelc"))
                            dsList.Tables(0).Rows(i)("isclose_fy") = CBool(dsBalance.Tables(0).Rows(0)("isclose_fy"))
                        Else
                            dsList.Tables(0).Rows(i)("leavebalanceunkid") = 0
                            dsList.Tables(0).Rows(i)("iselc") = mblnELC
                            dsList.Tables(0).Rows(i)("isopenelc") = mblnOpenELC
                            dsList.Tables(0).Rows(i)("isclose_fy") = False
                        End If
                    Next
                    objAccrue = Nothing
                    objLeave = Nothing
                    objEmp = Nothing

                End If

            End If

            dgLeaveAdjustment.AutoGenerateColumns = False
            dgLeaveAdjustment.DataSource = dsList.Tables(0)

            colhImage.DataPropertyName = "image"
            colhEmployeecode.DataPropertyName = "employeecode"
            colhEmployee.DataPropertyName = "employee"
            colhLeave.DataPropertyName = "leavename"
            colhBalanceAdjustment.DataPropertyName = "Balance_Adjustment"
            colhLeaveBFAdjustment.DataPropertyName = "LeaveBF_Adjustment"
            objcolhEmployeeId.DataPropertyName = "employeeunkid"
            objcolhLeavetypeId.DataPropertyName = "leavetypeunkid"
            objcolhLeavebalanceId.DataPropertyName = "leavebalanceunkid"
            colhMessage.DataPropertyName = "error"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then

                Cursor = Cursors.WaitCursor
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                dsList = OpenXML_Import(txtFilePath.Text)

                Dim strQuery As String = ""
                For i As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    dsList.Tables(0).Columns(i).ColumnName = dsList.Tables(0).Columns(i).ColumnName.Replace(" ", "_")
                    strQuery &= " AND " & dsList.Tables(0).Columns(i).ColumnName & " IS NULL "
                Next

                If strQuery.Trim.Length > 0 Then
                    strQuery = strQuery.Trim.Substring(4, strQuery.Trim.Length - 4)
                End If

                Dim drRow As DataRow() = dsList.Tables(0).Select(strQuery)

                If drRow.Length > 0 Then

                    For j As Integer = 0 To drRow.Length - 1
                        dsList.Tables(0).Rows.Remove(drRow(j))
                    Next
                    dsList.Tables(0).AcceptChanges()

                End If

                FillGirdView()
                Cursor = Cursors.Default
            End If
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try

            Dim blnError As Boolean = False

            If dgLeaveAdjustment.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to import leave adjustment."), enMsgBoxStyle.Information)
                Exit Sub
            Else

                'Pinkal (20-Jan-2023) -- Start
                'Resolved bug for importation of Leave Adjustments.
                If chkBalanceAdjustment.Checked = False AndAlso chkLeaveBFAdjustment.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select atleast one option to import leave adjustments."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    chkBalanceAdjustment.Focus()
                    Exit Sub
                ElseIf chkBalanceAdjustment.Checked AndAlso dsList.Tables(0).Columns.Contains("Balance_Adjustment") = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You can't import this file.Reason : There is no balance adjustment column in this file.Please check file format."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                ElseIf chkLeaveBFAdjustment.Checked AndAlso dsList.Tables(0).Columns.Contains("LeaveBF_Adjustment") = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You can't import this file.Reason : There is no leave bf adjustment column in this file.Please check file format."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
                'Pinkal (20-Jan-2023) -- End

                Dim objLeaveAdjustment As clsleaveadjustment_Tran = Nothing

                Dim intIndex As Integer = 0
                For Each dr As DataRow In dsList.Tables(0).Rows

                    objLeaveAdjustment = New clsleaveadjustment_Tran

                    If CInt(dr("employeeunkid")) <= 0 Then
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhEmployeecode.Index).ErrorText = Language.getMessage(mstrModuleName, 4, "Invalid Employee Code.")
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhEmployeecode.Index).ToolTipText = dgLeaveAdjustment.Rows(intIndex).Cells(colhEmployeecode.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 4, "Invalid Employee Code.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For

                    ElseIf CInt(dr("leavetypeunkid")) <= 0 Then
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhLeave.Index).ErrorText = Language.getMessage(mstrModuleName, 5, "Invalid Leave Name.")
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhLeave.Index).ToolTipText = dgLeaveAdjustment.Rows(intIndex).Cells(colhLeave.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 5, "Invalid Leave Name.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For

                    ElseIf CInt(dr("leavebalanceunkid")) <= 0 Then
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhLeave.Index).ErrorText = Language.getMessage(mstrModuleName, 6, "you can not do adjustment for this employee.Reason : Leave Accrue is not assigned to this employee.")
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhLeave.Index).ToolTipText = dgLeaveAdjustment.Rows(intIndex).Cells(colhLeave.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 6, "you can not do adjustment for this employee.Reason : Leave Accrue is not assigned to this employee.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For
                    End If

                    objLeaveAdjustment._Leavebalanceunkid = CInt(dr("leavebalanceunkid"))
                    objLeaveAdjustment._Employeeunkid = CInt(dr("employeeunkid"))
                    objLeaveAdjustment._Leavetypeunkid = CInt(dr("leavetypeunkid"))

                    If colhBalanceAdjustment.Visible Then
                        objLeaveAdjustment._Adjustment_Amt = CDec(dr("Balance_Adjustment"))
                    Else
                        objLeaveAdjustment._Adjustment_Amt = 0
                    End If

                    If colhLeaveBFAdjustment.Visible Then
                        objLeaveAdjustment._LeaveBf_Adjustment = CDec(dr("LeaveBF_Adjustment"))
                    Else
                        objLeaveAdjustment._LeaveBf_Adjustment = 0
                    End If

                    objLeaveAdjustment._Remarks = "Adjustments On" & Now.ToShortDateString()
                    objLeaveAdjustment._Userunkid = User._Object._Userunkid
                    objLeaveAdjustment._Isopenelc = CBool(dr("Isopenelc"))
                    objLeaveAdjustment._Iselc = CBool(dr("Iselc"))
                    objLeaveAdjustment._Isclose_Fy = CBool(dr("Isclose_Fy"))
                    objLeaveAdjustment._Isvoid = False


                    objLeaveAdjustment.Insert()

                    If objLeaveAdjustment._Message <> "" Then
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhEmployee.Index).ErrorText = objLeaveAdjustment._Message
                        dgLeaveAdjustment.Rows(intIndex).Cells(colhEmployee.Index).ToolTipText = objLeaveAdjustment._Message
                        dr("error") = objLeaveAdjustment._Message
                        dr("Image") = imgError
                        blnError = True
                        Continue For
                    End If

                    dr("Image") = imgAccept
                    intIndex += 1
                Next

                Dim drError As DataRow() = dsList.Tables(0).Select("error <> '' ")

                If drError.Length > 0 Then
                    If drError.Length < dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Some Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub

                    ElseIf drError.Length = dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                End If

                If blnError = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Data successfully imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtFilePath.Text = ""
                    dsList.Tables(0).Rows.Clear()
                    If chkBalanceAdjustment.Checked Then chkBalanceAdjustment.Checked = False
                    If chkLeaveBFAdjustment.Checked Then chkLeaveBFAdjustment.Checked = False
                End If

                dgLeaveAdjustment.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnImportData_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetFileFormat.Click
        Try
            If chkBalanceAdjustment.Checked = False AndAlso chkLeaveBFAdjustment.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select atleast one option to get the file format."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim dsList As DataSet = objLeaveAdjustment.GetImportAdjustmentFileFormat(chkBalanceAdjustment.Checked, chkLeaveBFAdjustment.Checked)
                OpenXML_Export(objSave.FileName, dsList)
            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnGetFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportError.Click
        Try

            If dgLeaveAdjustment.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "There is no data to show error(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dsList IsNot Nothing Then
                Dim dvGriddata As DataView = dsList.Tables(0).DefaultView
                dvGriddata.RowFilter = "error <>''"
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("leavebalanceunkid")
                        dtTable.Columns.Remove("employeeunkid")
                        dtTable.Columns.Remove("leavetypeunkid")
                        dtTable.Columns.Remove("Isopenelc")
                        dtTable.Columns.Remove("Iselc")
                        dtTable.Columns.Remove("Isclose_Fy")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Leave Adjustment") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportError_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkBalanceAdjustment_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBalanceAdjustment.CheckedChanged, chkLeaveBFAdjustment.CheckedChanged
        Try
            If CType(sender, CheckBox).Name.ToUpper() = chkBalanceAdjustment.Name.ToUpper() Then
                colhBalanceAdjustment.Visible = CType(sender, CheckBox).Checked
            ElseIf CType(sender, CheckBox).Name.ToUpper() = chkLeaveBFAdjustment.Name.ToUpper() Then
                colhLeaveBFAdjustment.Visible = CType(sender, CheckBox).Checked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShortLeave_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
            Me.btnGetFileFormat.Text = Language._Object.getCaption(Me.btnGetFileFormat.Name, Me.btnGetFileFormat.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.elMandatoryInfo.Text = Language._Object.getCaption(Me.elMandatoryInfo.Name, Me.elMandatoryInfo.Text)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.chkBalanceAdjustment.Text = Language._Object.getCaption(Me.chkBalanceAdjustment.Name, Me.chkBalanceAdjustment.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)
            Me.chkLeaveBFAdjustment.Text = Language._Object.getCaption(Me.chkLeaveBFAdjustment.Name, Me.chkLeaveBFAdjustment.Text)
            Me.colhImage.HeaderText = Language._Object.getCaption(Me.colhImage.Name, Me.colhImage.HeaderText)
            Me.colhEmployeecode.HeaderText = Language._Object.getCaption(Me.colhEmployeecode.Name, Me.colhEmployeecode.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhLeave.HeaderText = Language._Object.getCaption(Me.colhLeave.Name, Me.colhLeave.HeaderText)
            Me.colhBalanceAdjustment.HeaderText = Language._Object.getCaption(Me.colhBalanceAdjustment.Name, Me.colhBalanceAdjustment.HeaderText)
            Me.colhLeaveBFAdjustment.HeaderText = Language._Object.getCaption(Me.colhLeaveBFAdjustment.Name, Me.colhLeaveBFAdjustment.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no data to import leave adjustment.")
            Language.setMessage(mstrModuleName, 2, "You can't import this file.Reason : There is no balance adjustment column in this file.Please check file format.")
            Language.setMessage(mstrModuleName, 3, "You can't import this file.Reason : There is no leave bf adjustment column in this file.Please check file format.")
            Language.setMessage(mstrModuleName, 4, "Invalid Employee Code.")
            Language.setMessage(mstrModuleName, 5, "Invalid Leave Name.")
            Language.setMessage(mstrModuleName, 6, "you can not do adjustment for this employee.Reason : Leave Accrue is not assigned to this employee.")
            Language.setMessage(mstrModuleName, 7, "Data successfully imported.")
            Language.setMessage(mstrModuleName, 8, "Please select atleast one option to get the file format.")
            Language.setMessage(mstrModuleName, 9, "Some Data did not import due to error.")
            Language.setMessage(mstrModuleName, 10, "Data did not import due to error.")
            Language.setMessage(mstrModuleName, 11, "There is no data to show error(s).")
            Language.setMessage(mstrModuleName, 12, "Please select atleast one option to import leave adjustments.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class