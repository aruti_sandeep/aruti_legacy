﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSalaryIncrement_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSalaryIncrement_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbSalaryIncrementInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPromotionDate = New System.Windows.Forms.Label
        Me.lblChangeType = New System.Windows.Forms.Label
        Me.dtpPromotionDate = New System.Windows.Forms.DateTimePicker
        Me.cboChangeType = New System.Windows.Forms.ComboBox
        Me.chkChangeGrade = New System.Windows.Forms.CheckBox
        Me.lblNewScaleGrade = New System.Windows.Forms.Label
        Me.txtNewScaleGrade = New eZee.TextBox.NumericTextBox
        Me.cboIncrementBy = New System.Windows.Forms.ComboBox
        Me.lblIncrementBy = New System.Windows.Forms.Label
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.cboReason = New System.Windows.Forms.ComboBox
        Me.lblReason = New System.Windows.Forms.Label
        Me.objbtnAddScale = New eZee.Common.eZeeGradientButton
        Me.objbtnAddGLevel = New eZee.Common.eZeeGradientButton
        Me.objbtnAddGrade = New eZee.Common.eZeeGradientButton
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.lblLatestScale = New System.Windows.Forms.Label
        Me.txtLatestScale = New eZee.TextBox.NumericTextBox
        Me.lblNewScale = New System.Windows.Forms.Label
        Me.txtNewScale = New eZee.TextBox.NumericTextBox
        Me.txtMaximum = New eZee.TextBox.NumericTextBox
        Me.txtBasicScale = New eZee.TextBox.NumericTextBox
        Me.txtLastIncrementDate = New eZee.TextBox.AlphanumericTextBox
        Me.dtpIncrementdate = New System.Windows.Forms.DateTimePicker
        Me.lblIncrementDate = New System.Windows.Forms.Label
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.lblLastIncrement = New System.Windows.Forms.Label
        Me.lblBasicScale = New System.Windows.Forms.Label
        Me.lblMaximum = New System.Windows.Forms.Label
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblIncrement = New System.Windows.Forms.Label
        Me.txtIncrementAmount = New eZee.TextBox.NumericTextBox
        Me.txtCurrentScale = New eZee.TextBox.NumericTextBox
        Me.lblCurrentScale = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuBenefits = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRecategorization = New System.Windows.Forms.ToolStripMenuItem
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboPayYear = New System.Windows.Forms.ComboBox
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.dtpActualDate = New System.Windows.Forms.DateTimePicker
        Me.lblActualDate = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbSalaryIncrementInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbSalaryIncrementInfo)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.cboPayYear)
        Me.pnlMainInfo.Controls.Add(Me.lblPayYear)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(659, 456)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbSalaryIncrementInfo
        '
        Me.gbSalaryIncrementInfo.BorderColor = System.Drawing.Color.Black
        Me.gbSalaryIncrementInfo.Checked = False
        Me.gbSalaryIncrementInfo.CollapseAllExceptThis = False
        Me.gbSalaryIncrementInfo.CollapsedHoverImage = Nothing
        Me.gbSalaryIncrementInfo.CollapsedNormalImage = Nothing
        Me.gbSalaryIncrementInfo.CollapsedPressedImage = Nothing
        Me.gbSalaryIncrementInfo.CollapseOnLoad = False
        Me.gbSalaryIncrementInfo.Controls.Add(Me.dtpActualDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblActualDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblPromotionDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblChangeType)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.dtpPromotionDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboChangeType)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.chkChangeGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblNewScaleGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtNewScaleGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboIncrementBy)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblIncrementBy)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnAddReason)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboReason)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblReason)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnAddScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnAddGLevel)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnAddGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnAddGroup)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblPercentage)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtPercentage)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboGradeLevel)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboGradeGroup)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblLatestScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtLatestScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblNewScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtNewScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtMaximum)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtBasicScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtLastIncrementDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.dtpIncrementdate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblIncrementDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblGradeGroup)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.EZeeStraightLine2)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblLastIncrement)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblBasicScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblMaximum)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblGradeLevel)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblIncrement)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtIncrementAmount)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtCurrentScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblCurrentScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboEmployee)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblEmployee)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblPayPeriod)
        Me.gbSalaryIncrementInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbSalaryIncrementInfo.ExpandedHoverImage = Nothing
        Me.gbSalaryIncrementInfo.ExpandedNormalImage = Nothing
        Me.gbSalaryIncrementInfo.ExpandedPressedImage = Nothing
        Me.gbSalaryIncrementInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSalaryIncrementInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSalaryIncrementInfo.HeaderHeight = 25
        Me.gbSalaryIncrementInfo.HeaderMessage = ""
        Me.gbSalaryIncrementInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSalaryIncrementInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSalaryIncrementInfo.HeightOnCollapse = 0
        Me.gbSalaryIncrementInfo.LeftTextSpace = 0
        Me.gbSalaryIncrementInfo.Location = New System.Drawing.Point(0, 58)
        Me.gbSalaryIncrementInfo.Name = "gbSalaryIncrementInfo"
        Me.gbSalaryIncrementInfo.OpenHeight = 182
        Me.gbSalaryIncrementInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSalaryIncrementInfo.ShowBorder = True
        Me.gbSalaryIncrementInfo.ShowCheckBox = False
        Me.gbSalaryIncrementInfo.ShowCollapseButton = False
        Me.gbSalaryIncrementInfo.ShowDefaultBorderColor = True
        Me.gbSalaryIncrementInfo.ShowDownButton = False
        Me.gbSalaryIncrementInfo.ShowHeader = True
        Me.gbSalaryIncrementInfo.Size = New System.Drawing.Size(659, 343)
        Me.gbSalaryIncrementInfo.TabIndex = 0
        Me.gbSalaryIncrementInfo.Temp = 0
        Me.gbSalaryIncrementInfo.Text = "Salary Change Information"
        Me.gbSalaryIncrementInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPromotionDate
        '
        Me.lblPromotionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPromotionDate.Location = New System.Drawing.Point(8, 198)
        Me.lblPromotionDate.Name = "lblPromotionDate"
        Me.lblPromotionDate.Size = New System.Drawing.Size(102, 15)
        Me.lblPromotionDate.TabIndex = 262
        Me.lblPromotionDate.Text = "Promotion Date"
        '
        'lblChangeType
        '
        Me.lblChangeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChangeType.Location = New System.Drawing.Point(9, 171)
        Me.lblChangeType.Name = "lblChangeType"
        Me.lblChangeType.Size = New System.Drawing.Size(102, 15)
        Me.lblChangeType.TabIndex = 261
        Me.lblChangeType.Text = "Change Type"
        '
        'dtpPromotionDate
        '
        Me.dtpPromotionDate.Checked = False
        Me.dtpPromotionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPromotionDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPromotionDate.Location = New System.Drawing.Point(116, 195)
        Me.dtpPromotionDate.Name = "dtpPromotionDate"
        Me.dtpPromotionDate.ShowCheckBox = True
        Me.dtpPromotionDate.Size = New System.Drawing.Size(122, 21)
        Me.dtpPromotionDate.TabIndex = 260
        '
        'cboChangeType
        '
        Me.cboChangeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeType.FormattingEnabled = True
        Me.cboChangeType.Location = New System.Drawing.Point(116, 168)
        Me.cboChangeType.Name = "cboChangeType"
        Me.cboChangeType.Size = New System.Drawing.Size(202, 21)
        Me.cboChangeType.TabIndex = 259
        '
        'chkChangeGrade
        '
        Me.chkChangeGrade.BackColor = System.Drawing.Color.Transparent
        Me.chkChangeGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkChangeGrade.Location = New System.Drawing.Point(545, 4)
        Me.chkChangeGrade.Name = "chkChangeGrade"
        Me.chkChangeGrade.Size = New System.Drawing.Size(109, 17)
        Me.chkChangeGrade.TabIndex = 2
        Me.chkChangeGrade.Text = "Change &Grade"
        Me.chkChangeGrade.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkChangeGrade.UseVisualStyleBackColor = False
        '
        'lblNewScaleGrade
        '
        Me.lblNewScaleGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewScaleGrade.Location = New System.Drawing.Point(338, 117)
        Me.lblNewScaleGrade.Name = "lblNewScaleGrade"
        Me.lblNewScaleGrade.Size = New System.Drawing.Size(98, 15)
        Me.lblNewScaleGrade.TabIndex = 257
        Me.lblNewScaleGrade.Text = "New Scale"
        '
        'txtNewScaleGrade
        '
        Me.txtNewScaleGrade.AllowNegative = True
        Me.txtNewScaleGrade.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNewScaleGrade.DigitsInGroup = 0
        Me.txtNewScaleGrade.Flags = 0
        Me.txtNewScaleGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewScaleGrade.Location = New System.Drawing.Point(442, 114)
        Me.txtNewScaleGrade.MaxDecimalPlaces = 6
        Me.txtNewScaleGrade.MaxWholeDigits = 21
        Me.txtNewScaleGrade.Name = "txtNewScaleGrade"
        Me.txtNewScaleGrade.Prefix = ""
        Me.txtNewScaleGrade.RangeMax = 1.7976931348623157E+308
        Me.txtNewScaleGrade.RangeMin = -1.7976931348623157E+308
        Me.txtNewScaleGrade.Size = New System.Drawing.Size(178, 21)
        Me.txtNewScaleGrade.TabIndex = 12
        Me.txtNewScaleGrade.Text = "0"
        Me.txtNewScaleGrade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboIncrementBy
        '
        Me.cboIncrementBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncrementBy.FormattingEnabled = True
        Me.cboIncrementBy.Location = New System.Drawing.Point(116, 141)
        Me.cboIncrementBy.Name = "cboIncrementBy"
        Me.cboIncrementBy.Size = New System.Drawing.Size(202, 21)
        Me.cboIncrementBy.TabIndex = 3
        '
        'lblIncrementBy
        '
        Me.lblIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementBy.Location = New System.Drawing.Point(8, 144)
        Me.lblIncrementBy.Name = "lblIncrementBy"
        Me.lblIncrementBy.Size = New System.Drawing.Size(102, 15)
        Me.lblIncrementBy.TabIndex = 254
        Me.lblIncrementBy.Text = "Change By"
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(627, 249)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 251
        '
        'cboReason
        '
        Me.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReason.FormattingEnabled = True
        Me.cboReason.Location = New System.Drawing.Point(442, 249)
        Me.cboReason.Name = "cboReason"
        Me.cboReason.Size = New System.Drawing.Size(179, 21)
        Me.cboReason.TabIndex = 8
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(339, 252)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(97, 15)
        Me.lblReason.TabIndex = 239
        Me.lblReason.Text = "Reason"
        '
        'objbtnAddScale
        '
        Me.objbtnAddScale.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddScale.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddScale.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddScale.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddScale.BorderSelected = False
        Me.objbtnAddScale.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddScale.Image = CType(resources.GetObject("objbtnAddScale.Image"), System.Drawing.Image)
        Me.objbtnAddScale.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddScale.Location = New System.Drawing.Point(626, 141)
        Me.objbtnAddScale.Name = "objbtnAddScale"
        Me.objbtnAddScale.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddScale.TabIndex = 236
        '
        'objbtnAddGLevel
        '
        Me.objbtnAddGLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGLevel.BorderSelected = False
        Me.objbtnAddGLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGLevel.Image = CType(resources.GetObject("objbtnAddGLevel.Image"), System.Drawing.Image)
        Me.objbtnAddGLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGLevel.Location = New System.Drawing.Point(626, 87)
        Me.objbtnAddGLevel.Name = "objbtnAddGLevel"
        Me.objbtnAddGLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGLevel.TabIndex = 235
        '
        'objbtnAddGrade
        '
        Me.objbtnAddGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGrade.BorderSelected = False
        Me.objbtnAddGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGrade.Image = CType(resources.GetObject("objbtnAddGrade.Image"), System.Drawing.Image)
        Me.objbtnAddGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGrade.Location = New System.Drawing.Point(626, 60)
        Me.objbtnAddGrade.Name = "objbtnAddGrade"
        Me.objbtnAddGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGrade.TabIndex = 234
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = CType(resources.GetObject("objbtnAddGroup.Image"), System.Drawing.Image)
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(626, 33)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 233
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(8, 279)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(102, 15)
        Me.lblPercentage.TabIndex = 231
        Me.lblPercentage.Text = "Percentage"
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = True
        Me.txtPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 0
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(116, 276)
        Me.txtPercentage.MaxDecimalPlaces = 6
        Me.txtPercentage.MaxWholeDigits = 21
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(202, 21)
        Me.txtPercentage.TabIndex = 6
        Me.txtPercentage.Text = "0"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(442, 87)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(178, 21)
        Me.cboGradeLevel.TabIndex = 11
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(442, 60)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(178, 21)
        Me.cboGrade.TabIndex = 10
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(442, 33)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(178, 21)
        Me.cboGradeGroup.TabIndex = 9
        '
        'lblLatestScale
        '
        Me.lblLatestScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLatestScale.Location = New System.Drawing.Point(338, 225)
        Me.lblLatestScale.Name = "lblLatestScale"
        Me.lblLatestScale.Size = New System.Drawing.Size(98, 15)
        Me.lblLatestScale.TabIndex = 228
        Me.lblLatestScale.Text = "Latest Scale"
        '
        'txtLatestScale
        '
        Me.txtLatestScale.AllowNegative = True
        Me.txtLatestScale.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtLatestScale.DigitsInGroup = 0
        Me.txtLatestScale.Flags = 0
        Me.txtLatestScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLatestScale.Location = New System.Drawing.Point(442, 222)
        Me.txtLatestScale.MaxDecimalPlaces = 6
        Me.txtLatestScale.MaxWholeDigits = 21
        Me.txtLatestScale.Name = "txtLatestScale"
        Me.txtLatestScale.Prefix = ""
        Me.txtLatestScale.RangeMax = 1.7976931348623157E+308
        Me.txtLatestScale.RangeMin = -1.7976931348623157E+308
        Me.txtLatestScale.ReadOnly = True
        Me.txtLatestScale.Size = New System.Drawing.Size(178, 21)
        Me.txtLatestScale.TabIndex = 16
        Me.txtLatestScale.Text = "0.00"
        Me.txtLatestScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNewScale
        '
        Me.lblNewScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewScale.Location = New System.Drawing.Point(8, 306)
        Me.lblNewScale.Name = "lblNewScale"
        Me.lblNewScale.Size = New System.Drawing.Size(102, 15)
        Me.lblNewScale.TabIndex = 225
        Me.lblNewScale.Text = "New Scale"
        '
        'txtNewScale
        '
        Me.txtNewScale.AllowNegative = True
        Me.txtNewScale.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNewScale.DigitsInGroup = 0
        Me.txtNewScale.Flags = 0
        Me.txtNewScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewScale.Location = New System.Drawing.Point(116, 303)
        Me.txtNewScale.MaxDecimalPlaces = 6
        Me.txtNewScale.MaxWholeDigits = 21
        Me.txtNewScale.Name = "txtNewScale"
        Me.txtNewScale.Prefix = ""
        Me.txtNewScale.RangeMax = 1.7976931348623157E+308
        Me.txtNewScale.RangeMin = -1.7976931348623157E+308
        Me.txtNewScale.ReadOnly = True
        Me.txtNewScale.Size = New System.Drawing.Size(202, 21)
        Me.txtNewScale.TabIndex = 7
        Me.txtNewScale.Text = "0"
        Me.txtNewScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMaximum
        '
        Me.txtMaximum.AllowNegative = True
        Me.txtMaximum.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtMaximum.DigitsInGroup = 0
        Me.txtMaximum.Flags = 0
        Me.txtMaximum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaximum.Location = New System.Drawing.Point(442, 168)
        Me.txtMaximum.MaxDecimalPlaces = 6
        Me.txtMaximum.MaxWholeDigits = 21
        Me.txtMaximum.Name = "txtMaximum"
        Me.txtMaximum.Prefix = ""
        Me.txtMaximum.RangeMax = 1.7976931348623157E+308
        Me.txtMaximum.RangeMin = -1.7976931348623157E+308
        Me.txtMaximum.ReadOnly = True
        Me.txtMaximum.Size = New System.Drawing.Size(178, 21)
        Me.txtMaximum.TabIndex = 14
        Me.txtMaximum.Text = "0.00"
        Me.txtMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBasicScale
        '
        Me.txtBasicScale.AllowNegative = True
        Me.txtBasicScale.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtBasicScale.DigitsInGroup = 0
        Me.txtBasicScale.Flags = 0
        Me.txtBasicScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBasicScale.Location = New System.Drawing.Point(442, 141)
        Me.txtBasicScale.MaxDecimalPlaces = 6
        Me.txtBasicScale.MaxWholeDigits = 21
        Me.txtBasicScale.Name = "txtBasicScale"
        Me.txtBasicScale.Prefix = ""
        Me.txtBasicScale.RangeMax = 1.7976931348623157E+308
        Me.txtBasicScale.RangeMin = -1.7976931348623157E+308
        Me.txtBasicScale.ReadOnly = True
        Me.txtBasicScale.Size = New System.Drawing.Size(178, 21)
        Me.txtBasicScale.TabIndex = 13
        Me.txtBasicScale.Text = "0.00"
        Me.txtBasicScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLastIncrementDate
        '
        Me.txtLastIncrementDate.Flags = 0
        Me.txtLastIncrementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastIncrementDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLastIncrementDate.Location = New System.Drawing.Point(442, 195)
        Me.txtLastIncrementDate.MaxLength = 50
        Me.txtLastIncrementDate.Name = "txtLastIncrementDate"
        Me.txtLastIncrementDate.ReadOnly = True
        Me.txtLastIncrementDate.Size = New System.Drawing.Size(178, 21)
        Me.txtLastIncrementDate.TabIndex = 15
        '
        'dtpIncrementdate
        '
        Me.dtpIncrementdate.Checked = False
        Me.dtpIncrementdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIncrementdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIncrementdate.Location = New System.Drawing.Point(116, 87)
        Me.dtpIncrementdate.Name = "dtpIncrementdate"
        Me.dtpIncrementdate.ShowCheckBox = True
        Me.dtpIncrementdate.Size = New System.Drawing.Size(122, 21)
        Me.dtpIncrementdate.TabIndex = 2
        '
        'lblIncrementDate
        '
        Me.lblIncrementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementDate.Location = New System.Drawing.Point(8, 89)
        Me.lblIncrementDate.Name = "lblIncrementDate"
        Me.lblIncrementDate.Size = New System.Drawing.Size(102, 15)
        Me.lblIncrementDate.TabIndex = 222
        Me.lblIncrementDate.Text = "Change Date"
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(338, 35)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(98, 15)
        Me.lblGradeGroup.TabIndex = 219
        Me.lblGradeGroup.Text = "Grade Group"
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(324, 24)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(8, 317)
        Me.EZeeStraightLine2.TabIndex = 217
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'lblLastIncrement
        '
        Me.lblLastIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastIncrement.Location = New System.Drawing.Point(338, 197)
        Me.lblLastIncrement.Name = "lblLastIncrement"
        Me.lblLastIncrement.Size = New System.Drawing.Size(98, 15)
        Me.lblLastIncrement.TabIndex = 164
        Me.lblLastIncrement.Text = "Last Change On"
        '
        'lblBasicScale
        '
        Me.lblBasicScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasicScale.Location = New System.Drawing.Point(338, 143)
        Me.lblBasicScale.Name = "lblBasicScale"
        Me.lblBasicScale.Size = New System.Drawing.Size(98, 15)
        Me.lblBasicScale.TabIndex = 156
        Me.lblBasicScale.Text = "Minimum"
        '
        'lblMaximum
        '
        Me.lblMaximum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaximum.Location = New System.Drawing.Point(338, 171)
        Me.lblMaximum.Name = "lblMaximum"
        Me.lblMaximum.Size = New System.Drawing.Size(98, 15)
        Me.lblMaximum.TabIndex = 155
        Me.lblMaximum.Text = "Maximum"
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(338, 89)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(98, 15)
        Me.lblGradeLevel.TabIndex = 154
        Me.lblGradeLevel.Text = "Grade Level"
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(338, 62)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(98, 15)
        Me.lblGrade.TabIndex = 153
        Me.lblGrade.Text = "Grade"
        '
        'lblIncrement
        '
        Me.lblIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrement.Location = New System.Drawing.Point(8, 252)
        Me.lblIncrement.Name = "lblIncrement"
        Me.lblIncrement.Size = New System.Drawing.Size(102, 15)
        Me.lblIncrement.TabIndex = 152
        Me.lblIncrement.Text = "Change Amount"
        '
        'txtIncrementAmount
        '
        Me.txtIncrementAmount.AllowNegative = True
        Me.txtIncrementAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIncrementAmount.DigitsInGroup = 0
        Me.txtIncrementAmount.Flags = 0
        Me.txtIncrementAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncrementAmount.Location = New System.Drawing.Point(116, 249)
        Me.txtIncrementAmount.MaxDecimalPlaces = 6
        Me.txtIncrementAmount.MaxWholeDigits = 21
        Me.txtIncrementAmount.Name = "txtIncrementAmount"
        Me.txtIncrementAmount.Prefix = ""
        Me.txtIncrementAmount.RangeMax = 1.7976931348623157E+308
        Me.txtIncrementAmount.RangeMin = -1.7976931348623157E+308
        Me.txtIncrementAmount.Size = New System.Drawing.Size(202, 21)
        Me.txtIncrementAmount.TabIndex = 5
        Me.txtIncrementAmount.Text = "0"
        Me.txtIncrementAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCurrentScale
        '
        Me.txtCurrentScale.AllowNegative = True
        Me.txtCurrentScale.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentScale.DigitsInGroup = 0
        Me.txtCurrentScale.Flags = 0
        Me.txtCurrentScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentScale.Location = New System.Drawing.Point(116, 222)
        Me.txtCurrentScale.MaxDecimalPlaces = 6
        Me.txtCurrentScale.MaxWholeDigits = 21
        Me.txtCurrentScale.Name = "txtCurrentScale"
        Me.txtCurrentScale.Prefix = ""
        Me.txtCurrentScale.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentScale.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentScale.ReadOnly = True
        Me.txtCurrentScale.Size = New System.Drawing.Size(202, 21)
        Me.txtCurrentScale.TabIndex = 4
        Me.txtCurrentScale.Text = "0"
        Me.txtCurrentScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCurrentScale
        '
        Me.lblCurrentScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentScale.Location = New System.Drawing.Point(8, 225)
        Me.lblCurrentScale.Name = "lblCurrentScale"
        Me.lblCurrentScale.Size = New System.Drawing.Size(102, 15)
        Me.lblCurrentScale.TabIndex = 149
        Me.lblCurrentScale.Text = "Current Scale"
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(116, 33)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(202, 21)
        Me.cboPayPeriod.TabIndex = 0
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(297, 87)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 147
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(116, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(202, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(102, 15)
        Me.lblEmployee.TabIndex = 146
        Me.lblEmployee.Text = "Employee"
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(102, 15)
        Me.lblPayPeriod.TabIndex = 140
        Me.lblPayPeriod.Text = "Pay Period"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(659, 58)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "Add / Edit Salary Change"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.objFooter.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 401)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(659, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(96, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperations.TabIndex = 89
        Me.btnOperations.Text = "&Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBenefits, Me.mnuRecategorization})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(170, 48)
        '
        'mnuBenefits
        '
        Me.mnuBenefits.Name = "mnuBenefits"
        Me.mnuBenefits.Size = New System.Drawing.Size(169, 22)
        Me.mnuBenefits.Tag = "mnuBenefits"
        Me.mnuBenefits.Text = "Benefits"
        '
        'mnuRecategorization
        '
        Me.mnuRecategorization.Name = "mnuRecategorization"
        Me.mnuRecategorization.Size = New System.Drawing.Size(169, 22)
        Me.mnuRecategorization.Tag = "mnuRecategorization"
        Me.mnuRecategorization.Text = "Re-Categorization"
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(128, 30)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(266, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 239
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(128, 8)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(266, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 238
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(447, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(550, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboPayYear
        '
        Me.cboPayYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYear.FormattingEnabled = True
        Me.cboPayYear.Location = New System.Drawing.Point(322, 82)
        Me.cboPayYear.Name = "cboPayYear"
        Me.cboPayYear.Size = New System.Drawing.Size(42, 21)
        Me.cboPayYear.TabIndex = 0
        Me.cboPayYear.Visible = False
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(282, 85)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(60, 15)
        Me.lblPayYear.TabIndex = 162
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.Visible = False
        '
        'dtpActualDate
        '
        Me.dtpActualDate.Checked = False
        Me.dtpActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpActualDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpActualDate.Location = New System.Drawing.Point(116, 114)
        Me.dtpActualDate.Name = "dtpActualDate"
        Me.dtpActualDate.ShowCheckBox = True
        Me.dtpActualDate.Size = New System.Drawing.Size(122, 21)
        Me.dtpActualDate.TabIndex = 264
        '
        'lblActualDate
        '
        Me.lblActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualDate.Location = New System.Drawing.Point(8, 116)
        Me.lblActualDate.Name = "lblActualDate"
        Me.lblActualDate.Size = New System.Drawing.Size(102, 15)
        Me.lblActualDate.TabIndex = 265
        Me.lblActualDate.Text = "Actual Date"
        '
        'frmSalaryIncrement_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 456)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSalaryIncrement_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Salary Change"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbSalaryIncrementInfo.ResumeLayout(False)
        Me.gbSalaryIncrementInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbSalaryIncrementInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrentScale As System.Windows.Forms.Label
    Friend WithEvents txtCurrentScale As eZee.TextBox.NumericTextBox
    Friend WithEvents lblIncrement As System.Windows.Forms.Label
    Friend WithEvents txtIncrementAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMaximum As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblBasicScale As System.Windows.Forms.Label
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents cboPayYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblLastIncrement As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents lblIncrementDate As System.Windows.Forms.Label
    Friend WithEvents dtpIncrementdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtLastIncrementDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMaximum As eZee.TextBox.NumericTextBox
    Friend WithEvents txtBasicScale As eZee.TextBox.NumericTextBox
    Friend WithEvents txtNewScale As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNewScale As System.Windows.Forms.Label
    Friend WithEvents lblLatestScale As System.Windows.Forms.Label
    Friend WithEvents txtLatestScale As eZee.TextBox.NumericTextBox
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents txtPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnAddScale As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents cboReason As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents cboIncrementBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblIncrementBy As System.Windows.Forms.Label
    Friend WithEvents lblNewScaleGrade As System.Windows.Forms.Label
    Friend WithEvents txtNewScaleGrade As eZee.TextBox.NumericTextBox
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuBenefits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecategorization As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkChangeGrade As System.Windows.Forms.CheckBox
    Friend WithEvents lblPromotionDate As System.Windows.Forms.Label
    Friend WithEvents lblChangeType As System.Windows.Forms.Label
    Friend WithEvents dtpPromotionDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboChangeType As System.Windows.Forms.ComboBox
    Friend WithEvents dtpActualDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblActualDate As System.Windows.Forms.Label
End Class
