﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimesheetApprovalList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimesheetApprovalList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvEmpTimesheet = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkMyApprovals = New System.Windows.Forms.CheckBox
        Me.gbEmployeeTimesheet = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchApproverStatus = New eZee.Common.eZeeGradientButton
        Me.cboApproverStatus = New System.Windows.Forms.ComboBox
        Me.LblApproverStatus = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchStatus = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchActivity = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchProject = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchDonor = New eZee.Common.eZeeGradientButton
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.LblStatus = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.LblApprover = New System.Windows.Forms.Label
        Me.cboActivity = New System.Windows.Forms.ComboBox
        Me.LblActivity = New System.Windows.Forms.Label
        Me.cboProject = New System.Windows.Forms.ComboBox
        Me.LblProject = New System.Windows.Forms.Label
        Me.cboDonor = New System.Windows.Forms.ComboBox
        Me.LblDonor = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.LblDate = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhShowDetails = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhParticular = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprovalDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostCenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDonor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSubmissionRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTotalActivityHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhShiftHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhShiftHoursInSec = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTotalActivityHoursInMin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityHrsInMins = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityPercentage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFundSourceID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProjectID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMappedUserId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApproverunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApproverEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpTimesheetID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTimesheetApprovalID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsExternalApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhpercentage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsHoliday = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgvEmpTimesheet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbEmployeeTimesheet.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.chkSelectAll)
        Me.pnlMain.Controls.Add(Me.dgvEmpTimesheet)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbEmployeeTimesheet)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(816, 511)
        Me.pnlMain.TabIndex = 0
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelectAll.Location = New System.Drawing.Point(16, 154)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 251
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvEmpTimesheet
        '
        Me.dgvEmpTimesheet.AllowUserToAddRows = False
        Me.dgvEmpTimesheet.AllowUserToDeleteRows = False
        Me.dgvEmpTimesheet.AllowUserToResizeColumns = False
        Me.dgvEmpTimesheet.AllowUserToResizeRows = False
        Me.dgvEmpTimesheet.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmpTimesheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmpTimesheet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhIsCheck, Me.objdgcolhShowDetails, Me.dgcolhParticular, Me.dgcolhApprover, Me.dgcolhApprovalDate, Me.dgcolhActivity, Me.dgcolhCostCenter, Me.dgcolhDonor, Me.dgcolhProject, Me.dgcolhHours, Me.dgcolhEmpDescription, Me.dgcolhSubmissionRemark, Me.dgcolhTotalActivityHours, Me.dgcolhStatus, Me.objdgcolhShiftHours, Me.objdgcolhShiftHoursInSec, Me.objdgcolhTotalActivityHoursInMin, Me.objdgcolhActivityHrsInMins, Me.objdgcolhActivityPercentage, Me.objdgcolhPeriodID, Me.objdgcolhEmployeeID, Me.objdgcolhFundSourceID, Me.objdgcolhProjectID, Me.objdgcolhActivityID, Me.objdgcolhIsGrp, Me.objdgcolhMappedUserId, Me.objdgcolhStatusID, Me.objdgcolhEmployeeName, Me.objdgcolhApproverunkid, Me.objdgcolhApproverEmployeeID, Me.objdgcolhActivityDate, Me.objdgcolhPriority, Me.objdgcolhEmpTimesheetID, Me.objdgcolhTimesheetApprovalID, Me.objdgcolhIsExternalApprover, Me.objdgcolhpercentage, Me.objdgcolhIsHoliday})
        Me.dgvEmpTimesheet.Location = New System.Drawing.Point(9, 148)
        Me.dgvEmpTimesheet.Name = "dgvEmpTimesheet"
        Me.dgvEmpTimesheet.ReadOnly = True
        Me.dgvEmpTimesheet.RowHeadersVisible = False
        Me.dgvEmpTimesheet.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmpTimesheet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpTimesheet.Size = New System.Drawing.Size(795, 300)
        Me.dgvEmpTimesheet.TabIndex = 2
        Me.dgvEmpTimesheet.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.chkMyApprovals)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 456)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(816, 55)
        Me.objFooter.TabIndex = 18
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(711, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 73
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(586, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(119, 30)
        Me.btnEdit.TabIndex = 75
        Me.btnEdit.Text = "Change &Status"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'chkMyApprovals
        '
        Me.chkMyApprovals.Checked = True
        Me.chkMyApprovals.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMyApprovals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMyApprovals.Location = New System.Drawing.Point(18, 21)
        Me.chkMyApprovals.Name = "chkMyApprovals"
        Me.chkMyApprovals.Size = New System.Drawing.Size(288, 17)
        Me.chkMyApprovals.TabIndex = 242
        Me.chkMyApprovals.Text = "My Approvals"
        Me.chkMyApprovals.UseVisualStyleBackColor = True
        Me.chkMyApprovals.Visible = False
        '
        'gbEmployeeTimesheet
        '
        Me.gbEmployeeTimesheet.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeTimesheet.Checked = False
        Me.gbEmployeeTimesheet.CollapseAllExceptThis = False
        Me.gbEmployeeTimesheet.CollapsedHoverImage = Nothing
        Me.gbEmployeeTimesheet.CollapsedNormalImage = Nothing
        Me.gbEmployeeTimesheet.CollapsedPressedImage = Nothing
        Me.gbEmployeeTimesheet.CollapseOnLoad = False
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboApprover)
        Me.gbEmployeeTimesheet.Controls.Add(Me.lnkAllocation)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchApproverStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboApproverStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblApproverStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchActivity)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchProject)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchDonor)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objLine1)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblApprover)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboActivity)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblActivity)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboProject)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblProject)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboDonor)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblDonor)
        Me.gbEmployeeTimesheet.Controls.Add(Me.dtpDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboPeriod)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblPeriod)
        Me.gbEmployeeTimesheet.ExpandedHoverImage = Nothing
        Me.gbEmployeeTimesheet.ExpandedNormalImage = Nothing
        Me.gbEmployeeTimesheet.ExpandedPressedImage = Nothing
        Me.gbEmployeeTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeTimesheet.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeTimesheet.HeaderHeight = 25
        Me.gbEmployeeTimesheet.HeaderMessage = ""
        Me.gbEmployeeTimesheet.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeTimesheet.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeTimesheet.HeightOnCollapse = 0
        Me.gbEmployeeTimesheet.LeftTextSpace = 0
        Me.gbEmployeeTimesheet.Location = New System.Drawing.Point(9, 3)
        Me.gbEmployeeTimesheet.Name = "gbEmployeeTimesheet"
        Me.gbEmployeeTimesheet.OpenHeight = 300
        Me.gbEmployeeTimesheet.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeTimesheet.ShowBorder = True
        Me.gbEmployeeTimesheet.ShowCheckBox = False
        Me.gbEmployeeTimesheet.ShowCollapseButton = False
        Me.gbEmployeeTimesheet.ShowDefaultBorderColor = True
        Me.gbEmployeeTimesheet.ShowDownButton = False
        Me.gbEmployeeTimesheet.ShowHeader = True
        Me.gbEmployeeTimesheet.Size = New System.Drawing.Size(796, 140)
        Me.gbEmployeeTimesheet.TabIndex = 1
        Me.gbEmployeeTimesheet.Temp = 0
        Me.gbEmployeeTimesheet.Text = "Filter Information"
        Me.gbEmployeeTimesheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.DropDownWidth = 400
        Me.cboApprover.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(103, 86)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(293, 21)
        Me.cboApprover.TabIndex = 257
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(654, 2)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(85, 20)
        Me.lnkAllocation.TabIndex = 238
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(769, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 77
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(746, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 76
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchApproverStatus
        '
        Me.objbtnSearchApproverStatus.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApproverStatus.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApproverStatus.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApproverStatus.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApproverStatus.BorderSelected = False
        Me.objbtnSearchApproverStatus.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApproverStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchApproverStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApproverStatus.Location = New System.Drawing.Point(402, 113)
        Me.objbtnSearchApproverStatus.Name = "objbtnSearchApproverStatus"
        Me.objbtnSearchApproverStatus.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApproverStatus.TabIndex = 253
        '
        'cboApproverStatus
        '
        Me.cboApproverStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApproverStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApproverStatus.FormattingEnabled = True
        Me.cboApproverStatus.Location = New System.Drawing.Point(103, 113)
        Me.cboApproverStatus.Name = "cboApproverStatus"
        Me.cboApproverStatus.Size = New System.Drawing.Size(293, 21)
        Me.cboApproverStatus.TabIndex = 252
        '
        'LblApproverStatus
        '
        Me.LblApproverStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblApproverStatus.Location = New System.Drawing.Point(6, 115)
        Me.LblApproverStatus.Name = "LblApproverStatus"
        Me.LblApproverStatus.Size = New System.Drawing.Size(91, 16)
        Me.LblApproverStatus.TabIndex = 251
        Me.LblApproverStatus.Text = "Approver Status"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(402, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchEmployee.TabIndex = 249
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchPeriod.Image = CType(resources.GetObject("objbtnSearchPeriod.Image"), System.Drawing.Image)
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(219, 31)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchPeriod.TabIndex = 248
        '
        'objbtnSearchStatus
        '
        Me.objbtnSearchStatus.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchStatus.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchStatus.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchStatus.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchStatus.BorderSelected = False
        Me.objbtnSearchStatus.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchStatus.Image = CType(resources.GetObject("objbtnSearchStatus.Image"), System.Drawing.Image)
        Me.objbtnSearchStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchStatus.Location = New System.Drawing.Point(767, 113)
        Me.objbtnSearchStatus.Name = "objbtnSearchStatus"
        Me.objbtnSearchStatus.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchStatus.TabIndex = 247
        '
        'objbtnSearchActivity
        '
        Me.objbtnSearchActivity.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchActivity.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchActivity.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchActivity.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchActivity.BorderSelected = False
        Me.objbtnSearchActivity.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchActivity.Image = CType(resources.GetObject("objbtnSearchActivity.Image"), System.Drawing.Image)
        Me.objbtnSearchActivity.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchActivity.Location = New System.Drawing.Point(767, 86)
        Me.objbtnSearchActivity.Name = "objbtnSearchActivity"
        Me.objbtnSearchActivity.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchActivity.TabIndex = 246
        '
        'objbtnSearchProject
        '
        Me.objbtnSearchProject.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProject.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProject.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProject.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProject.BorderSelected = False
        Me.objbtnSearchProject.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchProject.Image = CType(resources.GetObject("objbtnSearchProject.Image"), System.Drawing.Image)
        Me.objbtnSearchProject.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProject.Location = New System.Drawing.Point(767, 59)
        Me.objbtnSearchProject.Name = "objbtnSearchProject"
        Me.objbtnSearchProject.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProject.TabIndex = 245
        '
        'objbtnSearchDonor
        '
        Me.objbtnSearchDonor.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDonor.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDonor.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDonor.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDonor.BorderSelected = False
        Me.objbtnSearchDonor.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDonor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchDonor.Image = CType(resources.GetObject("objbtnSearchDonor.Image"), System.Drawing.Image)
        Me.objbtnSearchDonor.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDonor.Location = New System.Drawing.Point(767, 31)
        Me.objbtnSearchDonor.Name = "objbtnSearchDonor"
        Me.objbtnSearchDonor.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDonor.TabIndex = 244
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(532, 113)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(229, 21)
        Me.cboStatus.TabIndex = 241
        '
        'LblStatus
        '
        Me.LblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblStatus.Location = New System.Drawing.Point(437, 115)
        Me.LblStatus.Name = "LblStatus"
        Me.LblStatus.Size = New System.Drawing.Size(91, 16)
        Me.LblStatus.TabIndex = 240
        Me.LblStatus.Text = "TimeSheet Status"
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(429, 25)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(4, 113)
        Me.objLine1.TabIndex = 89
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'LblApprover
        '
        Me.LblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblApprover.Location = New System.Drawing.Point(6, 88)
        Me.LblApprover.Name = "LblApprover"
        Me.LblApprover.Size = New System.Drawing.Size(63, 16)
        Me.LblApprover.TabIndex = 25
        Me.LblApprover.Text = "Approver"
        '
        'cboActivity
        '
        Me.cboActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivity.DropDownWidth = 400
        Me.cboActivity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivity.FormattingEnabled = True
        Me.cboActivity.Location = New System.Drawing.Point(532, 86)
        Me.cboActivity.Name = "cboActivity"
        Me.cboActivity.Size = New System.Drawing.Size(229, 21)
        Me.cboActivity.TabIndex = 11
        '
        'LblActivity
        '
        Me.LblActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblActivity.Location = New System.Drawing.Point(437, 88)
        Me.LblActivity.Name = "LblActivity"
        Me.LblActivity.Size = New System.Drawing.Size(91, 16)
        Me.LblActivity.TabIndex = 10
        Me.LblActivity.Text = "Activity"
        '
        'cboProject
        '
        Me.cboProject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProject.DropDownWidth = 400
        Me.cboProject.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProject.FormattingEnabled = True
        Me.cboProject.Location = New System.Drawing.Point(532, 59)
        Me.cboProject.Name = "cboProject"
        Me.cboProject.Size = New System.Drawing.Size(229, 21)
        Me.cboProject.TabIndex = 9
        '
        'LblProject
        '
        Me.LblProject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblProject.Location = New System.Drawing.Point(437, 61)
        Me.LblProject.Name = "LblProject"
        Me.LblProject.Size = New System.Drawing.Size(91, 16)
        Me.LblProject.TabIndex = 8
        Me.LblProject.Text = "Project"
        '
        'cboDonor
        '
        Me.cboDonor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDonor.DropDownWidth = 400
        Me.cboDonor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDonor.FormattingEnabled = True
        Me.cboDonor.Location = New System.Drawing.Point(532, 31)
        Me.cboDonor.Name = "cboDonor"
        Me.cboDonor.Size = New System.Drawing.Size(229, 21)
        Me.cboDonor.TabIndex = 7
        '
        'LblDonor
        '
        Me.LblDonor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDonor.Location = New System.Drawing.Point(437, 33)
        Me.LblDonor.Name = "LblDonor"
        Me.LblDonor.Size = New System.Drawing.Size(91, 16)
        Me.LblDonor.TabIndex = 6
        Me.LblDonor.Text = "Donor/Grant"
        '
        'dtpDate
        '
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(288, 31)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.ShowCheckBox = True
        Me.dtpDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpDate.TabIndex = 3
        '
        'LblDate
        '
        Me.LblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDate.Location = New System.Drawing.Point(243, 33)
        Me.LblDate.Name = "LblDate"
        Me.LblDate.Size = New System.Drawing.Size(40, 16)
        Me.LblDate.TabIndex = 2
        Me.LblDate.Text = "Date"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(103, 59)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(293, 21)
        Me.cboEmployee.TabIndex = 5
        '
        'LblEmployee
        '
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(6, 61)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(63, 16)
        Me.LblEmployee.TabIndex = 4
        Me.LblEmployee.Text = "Employee"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(103, 31)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(112, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'LblPeriod
        '
        Me.LblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(6, 33)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(63, 16)
        Me.LblPeriod.TabIndex = 0
        Me.LblPeriod.Text = "Period"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Particular"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 225
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Approval Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 125
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Activity"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 150
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "Donor/Grant"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.Frozen = True
        Me.DataGridViewTextBoxColumn6.HeaderText = "Project Code"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.Frozen = True
        Me.DataGridViewTextBoxColumn7.HeaderText = "Activity Hours"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.Frozen = True
        Me.DataGridViewTextBoxColumn8.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 175
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.Frozen = True
        Me.DataGridViewTextBoxColumn9.HeaderText = "Submission Remark"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 150
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.Frozen = True
        Me.DataGridViewTextBoxColumn10.HeaderText = "Total Activity Hours"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 125
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.Frozen = True
        Me.DataGridViewTextBoxColumn11.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Width = 200
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.Frozen = True
        Me.DataGridViewTextBoxColumn12.HeaderText = "Shift Hours"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.Frozen = True
        Me.DataGridViewTextBoxColumn13.HeaderText = "Shift Hours In Sec"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.Frozen = True
        Me.DataGridViewTextBoxColumn14.HeaderText = "Total Activity Hours In Min"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.Frozen = True
        Me.DataGridViewTextBoxColumn15.HeaderText = "ActivityHrsInMins"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.Frozen = True
        Me.DataGridViewTextBoxColumn16.HeaderText = "ActivityPercentage"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.Frozen = True
        Me.DataGridViewTextBoxColumn17.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.Frozen = True
        Me.DataGridViewTextBoxColumn18.HeaderText = "EmployeeID"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.Frozen = True
        Me.DataGridViewTextBoxColumn19.HeaderText = "FundSourceID"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.Frozen = True
        Me.DataGridViewTextBoxColumn20.HeaderText = "ProjectID"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.Frozen = True
        Me.DataGridViewTextBoxColumn21.HeaderText = "ActivityID"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.Frozen = True
        Me.DataGridViewTextBoxColumn22.HeaderText = "IsGrp"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.Frozen = True
        Me.DataGridViewTextBoxColumn23.HeaderText = "MappedUserId"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.Frozen = True
        Me.DataGridViewTextBoxColumn24.HeaderText = "StatusID"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.Frozen = True
        Me.DataGridViewTextBoxColumn25.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.Frozen = True
        Me.DataGridViewTextBoxColumn26.HeaderText = "tsapproverunkid"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn26.Visible = False
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.Frozen = True
        Me.DataGridViewTextBoxColumn27.HeaderText = "ApproverEmployeeID"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn27.Visible = False
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.Frozen = True
        Me.DataGridViewTextBoxColumn28.HeaderText = "Activity Date"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.Visible = False
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.Frozen = True
        Me.DataGridViewTextBoxColumn29.HeaderText = "Priority"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.Visible = False
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.Frozen = True
        Me.DataGridViewTextBoxColumn30.HeaderText = "EmpTimesheetID"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.Frozen = True
        Me.DataGridViewTextBoxColumn31.HeaderText = "TimesheetApprovalID"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.Frozen = True
        Me.DataGridViewTextBoxColumn32.HeaderText = "IsExternalApprover"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        Me.DataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn32.Visible = False
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.Frozen = True
        Me.DataGridViewTextBoxColumn33.HeaderText = "percentage"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.Frozen = True
        Me.DataGridViewTextBoxColumn34.HeaderText = "isholiday"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'objdgcolhIsCheck
        '
        Me.objdgcolhIsCheck.Frozen = True
        Me.objdgcolhIsCheck.HeaderText = ""
        Me.objdgcolhIsCheck.Name = "objdgcolhIsCheck"
        Me.objdgcolhIsCheck.ReadOnly = True
        Me.objdgcolhIsCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIsCheck.Width = 25
        '
        'objdgcolhShowDetails
        '
        Me.objdgcolhShowDetails.Frozen = True
        Me.objdgcolhShowDetails.HeaderText = ""
        Me.objdgcolhShowDetails.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objdgcolhShowDetails.Name = "objdgcolhShowDetails"
        Me.objdgcolhShowDetails.ReadOnly = True
        Me.objdgcolhShowDetails.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhShowDetails.ToolTipText = "Show Project Hour Details"
        Me.objdgcolhShowDetails.Width = 25
        '
        'dgcolhParticular
        '
        Me.dgcolhParticular.HeaderText = "Particular"
        Me.dgcolhParticular.Name = "dgcolhParticular"
        Me.dgcolhParticular.ReadOnly = True
        Me.dgcolhParticular.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhParticular.Width = 150
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.ReadOnly = True
        Me.dgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprover.Width = 225
        '
        'dgcolhApprovalDate
        '
        Me.dgcolhApprovalDate.HeaderText = "Approval Date"
        Me.dgcolhApprovalDate.Name = "dgcolhApprovalDate"
        Me.dgcolhApprovalDate.ReadOnly = True
        Me.dgcolhApprovalDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprovalDate.Width = 125
        '
        'dgcolhActivity
        '
        Me.dgcolhActivity.HeaderText = "Activity"
        Me.dgcolhActivity.Name = "dgcolhActivity"
        Me.dgcolhActivity.ReadOnly = True
        Me.dgcolhActivity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhActivity.Width = 150
        '
        'dgcolhCostCenter
        '
        Me.dgcolhCostCenter.HeaderText = "Cost Center"
        Me.dgcolhCostCenter.Name = "dgcolhCostCenter"
        Me.dgcolhCostCenter.ReadOnly = True
        Me.dgcolhCostCenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCostCenter.Visible = False
        Me.dgcolhCostCenter.Width = 150
        '
        'dgcolhDonor
        '
        Me.dgcolhDonor.HeaderText = "Donor/Grant"
        Me.dgcolhDonor.Name = "dgcolhDonor"
        Me.dgcolhDonor.ReadOnly = True
        Me.dgcolhDonor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDonor.Width = 150
        '
        'dgcolhProject
        '
        Me.dgcolhProject.HeaderText = "Project Code"
        Me.dgcolhProject.Name = "dgcolhProject"
        Me.dgcolhProject.ReadOnly = True
        Me.dgcolhProject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProject.Width = 150
        '
        'dgcolhHours
        '
        Me.dgcolhHours.HeaderText = "Activity Hours"
        Me.dgcolhHours.Name = "dgcolhHours"
        Me.dgcolhHours.ReadOnly = True
        Me.dgcolhHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmpDescription
        '
        Me.dgcolhEmpDescription.HeaderText = "Description"
        Me.dgcolhEmpDescription.Name = "dgcolhEmpDescription"
        Me.dgcolhEmpDescription.ReadOnly = True
        Me.dgcolhEmpDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpDescription.Width = 175
        '
        'dgcolhSubmissionRemark
        '
        Me.dgcolhSubmissionRemark.HeaderText = "Submission Remark"
        Me.dgcolhSubmissionRemark.Name = "dgcolhSubmissionRemark"
        Me.dgcolhSubmissionRemark.ReadOnly = True
        Me.dgcolhSubmissionRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSubmissionRemark.Width = 150
        '
        'dgcolhTotalActivityHours
        '
        Me.dgcolhTotalActivityHours.HeaderText = "Total Activity Hours"
        Me.dgcolhTotalActivityHours.Name = "dgcolhTotalActivityHours"
        Me.dgcolhTotalActivityHours.ReadOnly = True
        Me.dgcolhTotalActivityHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTotalActivityHours.Width = 125
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStatus.Width = 200
        '
        'objdgcolhShiftHours
        '
        Me.objdgcolhShiftHours.HeaderText = "Shift Hours"
        Me.objdgcolhShiftHours.Name = "objdgcolhShiftHours"
        Me.objdgcolhShiftHours.ReadOnly = True
        Me.objdgcolhShiftHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhShiftHours.Visible = False
        '
        'objdgcolhShiftHoursInSec
        '
        Me.objdgcolhShiftHoursInSec.HeaderText = "Shift Hours In Sec"
        Me.objdgcolhShiftHoursInSec.Name = "objdgcolhShiftHoursInSec"
        Me.objdgcolhShiftHoursInSec.ReadOnly = True
        Me.objdgcolhShiftHoursInSec.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhShiftHoursInSec.Visible = False
        '
        'objdgcolhTotalActivityHoursInMin
        '
        Me.objdgcolhTotalActivityHoursInMin.HeaderText = "Total Activity Hours In Min"
        Me.objdgcolhTotalActivityHoursInMin.Name = "objdgcolhTotalActivityHoursInMin"
        Me.objdgcolhTotalActivityHoursInMin.ReadOnly = True
        Me.objdgcolhTotalActivityHoursInMin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhTotalActivityHoursInMin.Visible = False
        '
        'objdgcolhActivityHrsInMins
        '
        Me.objdgcolhActivityHrsInMins.HeaderText = "ActivityHrsInMins"
        Me.objdgcolhActivityHrsInMins.Name = "objdgcolhActivityHrsInMins"
        Me.objdgcolhActivityHrsInMins.ReadOnly = True
        Me.objdgcolhActivityHrsInMins.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityHrsInMins.Visible = False
        '
        'objdgcolhActivityPercentage
        '
        Me.objdgcolhActivityPercentage.HeaderText = "ActivityPercentage"
        Me.objdgcolhActivityPercentage.Name = "objdgcolhActivityPercentage"
        Me.objdgcolhActivityPercentage.ReadOnly = True
        Me.objdgcolhActivityPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityPercentage.Visible = False
        '
        'objdgcolhPeriodID
        '
        Me.objdgcolhPeriodID.HeaderText = "PeriodID"
        Me.objdgcolhPeriodID.Name = "objdgcolhPeriodID"
        Me.objdgcolhPeriodID.ReadOnly = True
        Me.objdgcolhPeriodID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhPeriodID.Visible = False
        '
        'objdgcolhEmployeeID
        '
        Me.objdgcolhEmployeeID.HeaderText = "EmployeeID"
        Me.objdgcolhEmployeeID.Name = "objdgcolhEmployeeID"
        Me.objdgcolhEmployeeID.ReadOnly = True
        Me.objdgcolhEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeID.Visible = False
        '
        'objdgcolhFundSourceID
        '
        Me.objdgcolhFundSourceID.HeaderText = "FundSourceID"
        Me.objdgcolhFundSourceID.Name = "objdgcolhFundSourceID"
        Me.objdgcolhFundSourceID.ReadOnly = True
        Me.objdgcolhFundSourceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhFundSourceID.Visible = False
        '
        'objdgcolhProjectID
        '
        Me.objdgcolhProjectID.HeaderText = "ProjectID"
        Me.objdgcolhProjectID.Name = "objdgcolhProjectID"
        Me.objdgcolhProjectID.ReadOnly = True
        Me.objdgcolhProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhProjectID.Visible = False
        '
        'objdgcolhActivityID
        '
        Me.objdgcolhActivityID.HeaderText = "ActivityID"
        Me.objdgcolhActivityID.Name = "objdgcolhActivityID"
        Me.objdgcolhActivityID.ReadOnly = True
        Me.objdgcolhActivityID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityID.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "IsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhMappedUserId
        '
        Me.objdgcolhMappedUserId.HeaderText = "MappedUserId"
        Me.objdgcolhMappedUserId.Name = "objdgcolhMappedUserId"
        Me.objdgcolhMappedUserId.ReadOnly = True
        Me.objdgcolhMappedUserId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhMappedUserId.Visible = False
        '
        'objdgcolhStatusID
        '
        Me.objdgcolhStatusID.HeaderText = "StatusID"
        Me.objdgcolhStatusID.Name = "objdgcolhStatusID"
        Me.objdgcolhStatusID.ReadOnly = True
        Me.objdgcolhStatusID.Visible = False
        '
        'objdgcolhEmployeeName
        '
        Me.objdgcolhEmployeeName.HeaderText = "Employee Name"
        Me.objdgcolhEmployeeName.Name = "objdgcolhEmployeeName"
        Me.objdgcolhEmployeeName.ReadOnly = True
        Me.objdgcolhEmployeeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeName.Visible = False
        '
        'objdgcolhApproverunkid
        '
        Me.objdgcolhApproverunkid.HeaderText = "tsapproverunkid"
        Me.objdgcolhApproverunkid.Name = "objdgcolhApproverunkid"
        Me.objdgcolhApproverunkid.ReadOnly = True
        Me.objdgcolhApproverunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApproverunkid.Visible = False
        '
        'objdgcolhApproverEmployeeID
        '
        Me.objdgcolhApproverEmployeeID.HeaderText = "ApproverEmployeeID"
        Me.objdgcolhApproverEmployeeID.Name = "objdgcolhApproverEmployeeID"
        Me.objdgcolhApproverEmployeeID.ReadOnly = True
        Me.objdgcolhApproverEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApproverEmployeeID.Visible = False
        '
        'objdgcolhActivityDate
        '
        Me.objdgcolhActivityDate.HeaderText = "Activity Date"
        Me.objdgcolhActivityDate.Name = "objdgcolhActivityDate"
        Me.objdgcolhActivityDate.ReadOnly = True
        Me.objdgcolhActivityDate.Visible = False
        '
        'objdgcolhPriority
        '
        Me.objdgcolhPriority.HeaderText = "Priority"
        Me.objdgcolhPriority.Name = "objdgcolhPriority"
        Me.objdgcolhPriority.ReadOnly = True
        Me.objdgcolhPriority.Visible = False
        '
        'objdgcolhEmpTimesheetID
        '
        Me.objdgcolhEmpTimesheetID.HeaderText = "EmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.Name = "objdgcolhEmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.ReadOnly = True
        Me.objdgcolhEmpTimesheetID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpTimesheetID.Visible = False
        '
        'objdgcolhTimesheetApprovalID
        '
        Me.objdgcolhTimesheetApprovalID.HeaderText = "TimesheetApprovalID"
        Me.objdgcolhTimesheetApprovalID.Name = "objdgcolhTimesheetApprovalID"
        Me.objdgcolhTimesheetApprovalID.ReadOnly = True
        Me.objdgcolhTimesheetApprovalID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhTimesheetApprovalID.Visible = False
        '
        'objdgcolhIsExternalApprover
        '
        Me.objdgcolhIsExternalApprover.HeaderText = "IsExternalApprover"
        Me.objdgcolhIsExternalApprover.Name = "objdgcolhIsExternalApprover"
        Me.objdgcolhIsExternalApprover.ReadOnly = True
        Me.objdgcolhIsExternalApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsExternalApprover.Visible = False
        '
        'objdgcolhpercentage
        '
        Me.objdgcolhpercentage.HeaderText = "percentage"
        Me.objdgcolhpercentage.Name = "objdgcolhpercentage"
        Me.objdgcolhpercentage.ReadOnly = True
        Me.objdgcolhpercentage.Visible = False
        '
        'objdgcolhIsHoliday
        '
        Me.objdgcolhIsHoliday.HeaderText = "isholiday"
        Me.objdgcolhIsHoliday.Name = "objdgcolhIsHoliday"
        Me.objdgcolhIsHoliday.ReadOnly = True
        Me.objdgcolhIsHoliday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsHoliday.Visible = False
        '
        'frmTimesheetApprovalList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 511)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTimesheetApprovalList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Timesheet Approval List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.dgvEmpTimesheet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbEmployeeTimesheet.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbEmployeeTimesheet As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboActivity As System.Windows.Forms.ComboBox
    Friend WithEvents LblActivity As System.Windows.Forms.Label
    Friend WithEvents cboProject As System.Windows.Forms.ComboBox
    Friend WithEvents LblProject As System.Windows.Forms.Label
    Friend WithEvents cboDonor As System.Windows.Forms.ComboBox
    Friend WithEvents LblDonor As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblDate As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents LblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents LblPeriod As System.Windows.Forms.Label
    Friend WithEvents LblApprover As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents dgvEmpTimesheet As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents LblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents chkMyApprovals As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchActivity As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchProject As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDonor As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchStatus As eZee.Common.eZeeGradientButton
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchApproverStatus As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApproverStatus As System.Windows.Forms.ComboBox
    Friend WithEvents LblApproverStatus As System.Windows.Forms.Label
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhShowDetails As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhParticular As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprovalDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDonor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSubmissionRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTotalActivityHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhShiftHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhShiftHoursInSec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTotalActivityHoursInMin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityHrsInMins As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityPercentage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFundSourceID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProjectID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMappedUserId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApproverunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApproverEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpTimesheetID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTimesheetApprovalID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsExternalApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhpercentage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsHoliday As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
