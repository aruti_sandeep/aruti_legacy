﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO

Public Class frmBudgetEmp_Timesheet

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmBudgetEmp_Timesheet"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrSearchText As String = ""
    Dim objBudgetTimesheet As clsBudgetEmp_timesheet
    Dim imgBlank As New Drawing.Bitmap(1, 1)
    Dim mintEmptimesheetunkid As Integer = 0
    Dim mintBudgetMstID As Integer = 0
    Dim mintShiftWorkingHrs As Integer = 0
    Dim mdecPercentageHrs As Decimal = 0
    Dim mintActivityWorkingHrs As Integer = 0
    Dim mstrDefaultWorkingHRs As String = ""
    Dim mdtPeriodStartDate As Date = Nothing
    Dim mdtPeriodEndDate As Date = Nothing
    Dim mintShiftID As Integer = 0
    Dim mblnLastColumn As Boolean = False
    Private mdtEmpTimesheet As DataTable = Nothing 'Nilay (10 Jan 2017)
    Private mblnIsSubmitForApproval As Boolean = False 'Nilay (10 Jan 2017) 
    'Nilay (21 Mar 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private mintOldActivityWorkingHrsInMin As Integer = 0
    'Nilay (21 Mar 2017) -- End

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
    Dim mstrCompanyGrpName As String = ""
    Dim mintEditedCostCenterID As Integer = 0
    'Pinkal (06-Jan-2023) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintEmptimesheetunkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintEmptimesheetunkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName _
                                                            , FinancialYear._Object._Database_Start_Date.Date, "List", True)

            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            cboPeriod.DataSource = dsList.Tables(0)
            cboPeriod.SelectedValue = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, True, Nothing)
            objPeriod = Nothing
            dsList = Nothing


            'Pinkal (06-Dec-2016) -- Start
            'Enhancement - Working on Budget Employee timesheet Changes as Per Suzan and Andrew Requirement.
            'Dim objFundCode As New clsFundSource_Master
            'dsList = objFundCode.GetComboList("List", True)
            'cboDonor.DisplayMember = "fundname"
            'cboDonor.ValueMember = "fundsourceunkid"
            'cboDonor.DataSource = dsList.Tables(0)
            'cboDonor.SelectedValue = 0
            'objFundCode = Nothing
            'dsList = Nothing

            'cboDonor_SelectedIndexChanged(New Object(), New EventArgs())
            'cboProject_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (06-Dec-2016) -- End

            dtpDate_ValueChanged(New Object(), New EventArgs())



            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            Dim objcostCenter As New clscostcenter_master
            dsList = objcostCenter.getComboList("List", True, "", "")
            Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("costcenterunkid") <= 0)
            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                drRow(0)("costcentername") = ""
                drRow(0).AcceptChanges()
            End If
            With dgcolhCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables(0)
            End With
            'Pinkal (06-Jan-2023) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
            'Dim dsList As DataSet = objBudgetTimesheet.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
            '                                                                            , FinancialYear._Object._YearUnkid, FinancialYear._Object._Companyunkid _
            '                                                                            , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, ConfigParameter._Object._UserAccessModeSetting _
            '                                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp _
            '                                                                            , CInt(cboEmployee.SelectedValue), True, True, CInt(IIf(CInt(cboPeriod.SelectedValue) > 0, CInt(cboPeriod.SelectedValue), -1)))

            Dim dsList As DataSet = objBudgetTimesheet.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                        , FinancialYear._Object._YearUnkid, FinancialYear._Object._Companyunkid _
                                                                                        , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                        , ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
                                                                                        , CInt(cboEmployee.SelectedValue), True, True, CInt(IIf(CInt(cboPeriod.SelectedValue) > 0, CInt(cboPeriod.SelectedValue), -1)))

            'Pinkal (28-Mar-2018) -- End

            dgvEmpTimesheetList.AutoGenerateColumns = False

            dgcolhEmployee.DataPropertyName = "Particular" 'Nilay (21 Mar 2017) -- [Employee Replaced By Particular]
            dgcolhDate.DataPropertyName = "activitydate"
            dgcolhDonor.DataPropertyName = "fundname"
            dgcolhProject.DataPropertyName = "fundprojectname"
            dgcolhActivity.DataPropertyName = "activity_name"
            dgcolhHours.DataPropertyName = "activity_hrs"
            dgcolhApprovedActHrs.DataPropertyName = "approvedactivity_hrs"
            dgcolhDescription.DataPropertyName = "description"
            dgcolhStatus.DataPropertyName = "status"
            objdgcolhEmpTimesheetID.DataPropertyName = "emptimesheetunkid"
            objdgcolhPeriodID.DataPropertyName = "periodunkid"
            objdgcolhEmployeeID.DataPropertyName = "employeeunkid"
            objdgcolhFundSourceID.DataPropertyName = "fundsourceunkid"
            objdgcolhProjectID.DataPropertyName = "fundprojectcodeunkid"
            objdgcolhActivityID.DataPropertyName = "fundactivityunkid"
            objdgcolhStatusId.DataPropertyName = "statusunkid"
            objdgcolhIsGrp.DataPropertyName = "IsGrp" 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
            objdgcolhIsHoliday.DataPropertyName = "isholiday"
            objdgcolhIsDayOFF.DataPropertyName = "isDayOff"
            objdgcolhIsLeave.DataPropertyName = "isLeave"

            'Nilay (15 Feb 2017) -- Start
            objdgcolhApprovedActivityHoursInMin.DataPropertyName = "ApprovedActivityHoursInMin"
            'Nilay (15 Feb 2017) -- End

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            objdgcolhActivityHoursInMin.DataPropertyName = "ActivityHoursInMin"
            'Pinkal (28-Jul-2018) -- End


            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'dgvEmpTimesheetList.DataSource = dsList.Tables(0)
            objdgcolhIsSubmitForApproval.DataPropertyName = "issubmit_approval"

            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'objdgcolhSelect.DataPropertyName = "IsChecked"
            'Nilay (27 Feb 2017) -- End

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            objdgcolhCostCenterIdList.DataPropertyName = "costcenterunkid"
            dgcolhCostCenterList.DataPropertyName = "costcentername"
            If mstrCompanyGrpName.Trim.ToUpper() = "PSI MALAWI" Then
                dgcolhCostCenterList.Visible = True
            End If
            'Pinkal (06-Jan-2023) -- End

            mdtEmpTimesheet = dsList.Tables("List")
            dgvEmpTimesheetList.DataSource = mdtEmpTimesheet
            'Nilay (10 Jan 2017) -- End

            SetGridColor()

            'Pinkal (06-Dec-2016) -- Start
            'Enhancement - Working on Budget Employee timesheet Changes as Per Suzan and Andrew Requirement.
            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    objLblValue.Text = Language.getMessage(mstrModuleName, 12, "Total Working Hrs:") & " " & CDec(CalculateTime(True, objBudgetTimesheet.GetEmpWorkingHrsForTheDay(dtpDate.Value.Date, CInt(cboEmployee.SelectedValue)) * 60)).ToString("#00.00").Replace(".", ":")
            'Else
            '    objLblValue.Text = Language.getMessage(mstrModuleName, 12, "Total Working Hrs:") & " 00:00"
            'End If
            'Pinkal (06-Dec-2016) -- End

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If CBool(ConfigParameter._Object._NotAllowIncompleteTimesheet) = True Then
                dgvEmpTimesheetList.Columns(objdgcolhCancel.Index).Visible = False
            End If
            'Nilay (01 Apr 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Focus()
                Return False

                'Nilay (27 Feb 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes

                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            ElseIf dtpFromDate.Value.Date = dtpToDate.Value.Date AndAlso (dtpFromDate.Value.Date > ConfigParameter._Object._CurrentDateAndTime.Date OrElse dtpToDate.Value.Date > ConfigParameter._Object._CurrentDateAndTime.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You cannot assign activity hours for future date."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dtpFromDate.Focus()
                Return False
                'Pinkal (13-Oct-2017) -- End

                'Nilay (27 Feb 2017) -- End

            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboEmployee.Focus()
                Return False


                'Pinkal (06-Dec-2016) -- Start
                'Enhancement - Working on Budget Employee timesheet Changes as Per Suzan and Andrew Requirement.

                'ElseIf CInt(cboDonor.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Donor/Grant is compulsory information.Please Select Donor/Grant."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    cboDonor.Focus()
                '    Return False

                'ElseIf CInt(cboProject.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Project is compulsory information.Please Select Project."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    cboProject.Focus()
                '    Return False

                'ElseIf CInt(cboActivity.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Activity is compulsory information.Please Select Activity."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    cboActivity.Focus()
                '    Return False

                'ElseIf txtHours.MaskCompleted = False OrElse txtHours.MaskFull = False OrElse txtHours.Text = "00:00" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Hours is compulsory information.Please enter activity hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    txtHours.Focus()
                '    Return False
                'ElseIf ConfigParameter._Object._AllowOverTimeToEmpTimesheet = False AndAlso (IsDate(txtHours.Text.Trim) = False OrElse TimeValue(mstrDefaultWorkingHRs) < TimeValue(txtHours.Text.Trim) OrElse (TimeValue(mstrDefaultWorkingHRs) = Nothing OrElse TimeValue(txtHours.Text.Trim) = Nothing)) Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This employee does not allow to do activity more than define activity percentage."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    txtHours.Focus()
                '    Return False

                'Pinkal (06-Dec-2016) -- End

            Else
                Dim objapprover As New clstsapprover_master
                Dim dtList As DataTable = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                        , -1, CStr(cboEmployee.SelectedValue), -1, Nothing, False, "")
                'Nilay (10 Jan 2017) -- [CInt(cboEmployee.SelectedValue) REPLACED BY CStr(cboEmployee.SelectedValue)]

                If dtList.Rows.Count = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Please Assign Approver to this employee and also map approver to system user."), enMsgBoxStyle.Information)
                    Return False
                End If
                dtList.Clear()
                dtList = Nothing
                objapprover = Nothing


                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                If dgvTimesheet.RowCount <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "There is no activity assign to this employee for this tenure.please assign activity to this employee."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Pinkal (13-Oct-2017) -- End


                'Pinkal (24-Jul-2017) -- Start
                'Enhancement - Budget Timesheet Issue For THPS .
                'Dim objEmpHoliday As New clsemployee_holiday
                'Dim dtTable As DataTable = objEmpHoliday.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue), dtpDate.Value.Date)
                'If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry,you cannot add activity for this employee for selected date. Reason : Holiday is assigned to this employee for this date."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'dtTable = Nothing
                'objEmpHoliday = Nothing
                'Pinkal (24-Jul-2017) -- End


                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
                'Dim objEHoliday As New clsemployee_holiday
                'If objEHoliday.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue)).Rows.Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Sorry,you cannot add activity for this employee. Reason : Not a Single Holiday is assigned to this employee for current financial year."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'objEHoliday = Nothing
                'Pinkal (28-Mar-2018) -- End

                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.

                If dtpFromDate.Value.Date = dtpToDate.Value.Date Then

                Dim objEmpDayOFF As New clsemployee_dayoff_Tran

                'Pinkal (17-Jan-2017) -- Start
                'Enhancement - Working on DAYOFF Error given by AMOS. 
                'Dim dsList As DataSet = objEmpDayOFF.GetList("List", "", CInt(cboEmployee.SelectedValue), dtpDate.Value.Date, Nothing)


                Dim dsList As DataSet = objEmpDayOFF.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                         , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                            , "", CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, dtpFromDate.Value.Date)
                'Pinkal (17-Jan-2017) -- End
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry,you cannot add activity for this employee for selected date. Reason : Day Off is assigned to this employee for this date."), enMsgBoxStyle.Information)
                    Return False
                End If
                dsList = Nothing
                objEmpDayOFF = Nothing

                If CBool(ConfigParameter._Object._AllowOverTimeToEmpTimesheet) = False Then
                    Dim objShiftTran As New clsshift_tran
                    objShiftTran.GetShiftTran(mintShiftID)
                        Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpFromDate.Value.Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 1")  'Pinkal (13-Oct-2017) -- Start
                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry,you cannot add activity for this employee for selected date. Reason : Weekend is assigned to this employee for this date."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    drRow = Nothing
                    objShiftTran = Nothing
                End If



                    'Pinkal (13-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                Dim objLeaveIssue As New clsleaveissue_Tran
                    If objLeaveIssue.GetIssuedDayFractionForViewer(CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(dtpFromDate.Value.Date)) > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already issued on this date by this employee."), enMsgBoxStyle.Information)
                    Return False
                    Else
                        'Pinkal (11-Sep-2019) -- Start
                        'Defect PACT - Cancel Days are not inserting in bugdet timesheet .
                        Dim objCancelform As New clsCancel_Leaveform
                        If objCancelform.GetCancelDayExistForEmployee(CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, Nothing) = False Then
                    Dim objLeave As New clsleaveform
                    If objLeave.isDayExist(True, CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, dtpFromDate.Value.Date, -1) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already approved on this date by this employee."), enMsgBoxStyle.Information)
                        objLeave = Nothing
                        Return False
                                'Pinkal (21-Oct-2019) -- Start
                                'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.
                                'ElseIf objLeave.isDayExist(False, CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, dtpFromDate.Value.Date, -1) Then
                                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already applied on this date by this employee."), enMsgBoxStyle.Information)
                                '    objLeave = Nothing
                                '    Return False
                                'Pinkal (21-Oct-2019) -- End
            End If
                    objLeave = Nothing
                    End If
                        objCancelform = Nothing
                        'Pinkal (11-Sep-2019) -- End
                    End If
                    objLeaveIssue = Nothing
                    'Pinkal (13-Aug-2019) -- End

            End If


            End If

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

            If dtpFromDate.Value.Date = dtpToDate.Value.Date AndAlso ConfigParameter._Object._AllowOverTimeToEmpTimesheet = False Then
            Dim objEmpHoliday As New clsemployee_holiday
                Dim dtTable As DataTable = objEmpHoliday.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry,you cannot add activity for this employee for selected date. Reason : Holiday is assigned to this employee for this date."), enMsgBoxStyle.Information)
                Return False
            End If
            dtTable = Nothing
            objEmpHoliday = Nothing
            End If

            'Pinkal (28-Jul-2018) -- End


            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            Dim intActivityHoursInMin As Integer = CInt(CType(dgvTimesheet.DataSource, DataTable).Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
            If intActivityHoursInMin <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Total Activity Hours must be greater than 0. Please define hours for atleast one activity."), enMsgBoxStyle.Information)
                Return False
            End If
            'Nilay (07 Feb 2017) -- End


            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If mstrCompanyGrpName.Trim.ToUpper() = "PSI MALAWI" AndAlso dgcolhCostCenter.Visible Then
                Dim dRow() As DataRow = CType(dgvTimesheet.DataSource, DataTable).Select("costcenterunkid <= 0 AND budgetunkid <> -999 ")
                If dRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Cost Center is compulsory information.Please select cost center for each activity."), enMsgBoxStyle.Information)
                    dgvTimesheet.CurrentCell = dgvTimesheet.Rows(CType(dgvTimesheet.DataSource, DataTable).Rows.IndexOf(dRow(0))).Cells(dgcolhCostCenter.Index)
                    SendKeys.Send("{f4}")
                    dgvTimesheet.Focus()
                    Return False
                End If
            End If

            'Pinkal (06-Jan-2023) -- End


            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If CBool(ConfigParameter._Object._DescriptionMandatoryForActivity) = True Then
                Dim dRow() As DataRow = CType(dgvTimesheet.DataSource, DataTable).Select("ActivityHoursInMin > 0 AND (Description = '' OR Description IS NULL) AND budgetunkid <> -999 ")
                If dRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Description is a mandatory information for each activity. Please mention description for each activity."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Nilay (21 Mar 2017) -- End



            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            If ConfigParameter._Object._AllowEmpAssignedProjectExceedTime = False Then
            If CBool(ConfigParameter._Object._AllowToExceedTimeAssignedToActivity) = True AndAlso CBool(ConfigParameter._Object._AllowOverTimeToEmpTimesheet) = False Then
                If dgvTimesheet.RowCount > 0 Then
                    dgvTimesheet.Focus()
                    For i As Integer = 0 To dgvTimesheet.RowCount - 1

                            'Pinkal (30-Aug-2019) -- Start
                            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                            'If CInt(dgvTimesheet.Rows(i).Cells(objdgcolhEmpHourInMin.Index).Value) > 0 Then
                            If CInt(dgvTimesheet.Rows(i).Cells(objdgcolhEmpHourInMin.Index).Value) > 0 AndAlso CInt(dgvTimesheet.Rows(i).Cells(objdgcolhEmpHourInMin.Index).Value) > CInt(dgvTimesheet.Rows(i).Cells(objdgcolhEmpAssignedActivityHrsInMin.Index).Value) Then
                                'Pinkal (30-Aug-2019) -- End
                            If ValidateProjectHrs(i, dgcolhHours.Index) = False Then
                                dgvTimesheet.CurrentCell = dgvTimesheet.Rows(i).Cells(dgcolhEmpHours.Index)
                                dgvTimesheet.Rows(i).Cells(dgcolhEmpHours.Index).Selected = True
                                SendKeys.Send("{f2}")
                                Return False
                            End If
                        End If
                    Next
                End If
            End If
            End If
            'Pinkal (13-Aug-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try


            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            If mintEmptimesheetunkid > 0 Then
                objBudgetTimesheet._Emptimesheetunkid = mintEmptimesheetunkid
            End If
            'Pinkal (13-Oct-2017) -- End

            objBudgetTimesheet._Periodunkid = CInt(cboPeriod.SelectedValue)

            objBudgetTimesheet._Activitydate = dtpFromDate.Value.Date
            objBudgetTimesheet._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objBudgetTimesheet._Activity_Hrs = mintActivityWorkingHrs
            objBudgetTimesheet._ApprovedActivity_Hrs = 0

            objBudgetTimesheet._Statusunkid = 2 'Pending
            objBudgetTimesheet._Userunkid = User._Object._Userunkid

            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'Dim objApproverMst As New clstsapprover_master
            'Dim dtList As DataTable = objApproverMst.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
            '                                                           , -1, CStr(cboEmployee.SelectedValue), -1, Nothing)
            'Nilay (10 Jan 2017) -- End


            'Nilay (10 Jan 2017) -- [CStr(cboEmployee.SelectedValue)]

            'Dim drRow() As DataRow = dtList.Select("priority = Min(priority)")
            'If drRow.Length > 0 Then
            '    objBudgetTimesheet._dtApproverList = drRow.ToList.CopyToDataTable()
            'Else
            '    objBudgetTimesheet._dtApproverList = Nothing
            'End If

            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'objBudgetTimesheet._dtApproverList = dtList
            'Nilay (10 Jan 2017) -- End

            'Nilay (15 Feb 2017) -- Start
            'objBudgetTimesheet._dtEmployeeActivity = New DataView(CType(dgvTimesheet.DataSource, DataTable), "budgetunkid >0", "", DataViewRowState.CurrentRows).ToTable
            objBudgetTimesheet._dtEmployeeActivity = New DataView(CType(dgvTimesheet.DataSource, DataTable), "budgetunkid >0 AND ActivityHoursInMin > 0", "", DataViewRowState.CurrentRows).ToTable
            'Nilay (15 Feb 2017) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try

    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date  'Pinkal (13-Oct-2017) -- Start
            cboEmployee.SelectedValue = 0


            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (13-Oct-2017) -- End


            'Pinkal (06-Dec-2016) -- Start
            'Enhancement - Working on Budget Employee timesheet Changes as Per Suzan and Andrew Requirement.
            'cboDonor.SelectedValue = 0
            'cboProject.SelectedValue = 0
            'cboActivity.SelectedValue = 0
            'txtHours.Text = ""
            'txtDescription.Text = ""
            'Pinkal (06-Dec-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dr = From p As DataGridViewRow In dgvEmpTimesheetList.Rows.Cast(Of DataGridViewRow)() Where CBool(p.Cells(objdgcolhIsGrp.Index).Value) = True Select p
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

            Dim drDays = From p As DataGridViewRow In dgvEmpTimesheetList.Rows.Cast(Of DataGridViewRow)() _
                                Where CBool(p.Cells(objdgcolhIsHoliday.Index).Value) = True OrElse CBool(p.Cells(objdgcolhIsDayOFF.Index).Value) = True OrElse CBool(p.Cells(objdgcolhIsLeave.Index).Value) = True Select p
            drDays.ToList.ForEach(Function(x) SetRowStyle(x, False))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal isHeader As Boolean) As Boolean
        Try
            If isHeader Then
                Dim dgvcsHeader As New DataGridViewCellStyle
                dgvcsHeader.ForeColor = Color.White
                dgvcsHeader.SelectionBackColor = Color.Gray
                dgvcsHeader.SelectionForeColor = Color.White
                dgvcsHeader.BackColor = Color.Gray
                xRow.DefaultCellStyle = dgvcsHeader
                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                xRow.Cells(objdgcolhEdit.Index).Value = imgBlank
                'Pinkal (13-Aug-2019) -- End
            Else
                xRow.DefaultCellStyle.ForeColor = Color.Red
            End If


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            If (CBool(xRow.Cells(objdgcolhIsDayOFF.Index).Value) OrElse CBool(xRow.Cells(objdgcolhIsLeave.Index).Value) OrElse (CBool(xRow.Cells(objdgcolhIsHoliday.Index).Value) AndAlso ConfigParameter._Object._AllowOverTimeToEmpTimesheet = False)) Then
            xRow.Cells(objdgcolhEdit.Index).Value = imgBlank
            End If

            If CInt(xRow.Cells(objdgcolhActivityHoursInMin.Index).Value) <= 0 Then
            xRow.Cells(objdgcolhDelete.Index).Value = imgBlank
            xRow.Cells(objdgcolhCancel.Index).Value = imgBlank
            End If
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub GetEmployeeBudgetCodes()
        Try
            '            Dim objBudgetCodes As New clsBudgetcodes_master
            '            Dim dsList As DataSet = Nothing
            '            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboActivity.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
            '                dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPeriod.SelectedValue), mintBudgetMstID, CInt(cboActivity.SelectedValue), CInt(cboEmployee.SelectedValue))
            '                Dim objEmpshift As New clsEmployee_Shift_Tran
            '                mintShiftID = objEmpshift.GetEmployee_Current_ShiftId(dtpDate.Value.Date, CInt(cboEmployee.SelectedValue))
            '                objEmpshift = Nothing
            '                Dim objShiftTran As New clsshift_tran
            '                objShiftTran.GetShiftTran(mintShiftID)
            '                Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpDate.Value.Date, FirstDayOfWeek.Sunday) - 1)
            '                If drRow.Length > 0 AndAlso (dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0) Then
            '                    mintShiftWorkingHrs = CInt(drRow(0)("workinghrsinsec"))
            '                    mdecPercentageHrs = CDec(dsList.Tables(0).Rows(0)("percentage"))
            '                    GetWorkingMins(mintShiftWorkingHrs, mdecPercentageHrs, "")
            '                Else
            '                    mintShiftWorkingHrs = 0
            '                    mdecPercentageHrs = 0
            '                    mintActivityWorkingHrs = 0
            '                    txtHours.Text = ""
            '                    mstrDefaultWorkingHRs = "00:00"
            '                    GoTo ResetValue
            '                End If
            '            Else
            'ResetValue:
            '                mintShiftWorkingHrs = 0
            '                mdecPercentageHrs = 0
            '                txtHours.Text = ""
            '            End If
            '            objBudgetCodes = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeBudgetCodes", mstrModuleName)
        End Try
    End Sub

    'Private Sub GetWorkingMins(ByVal xShiftWorkingHrs As Integer, ByVal xPercentageHrs As Decimal, Optional ByVal mstrWorkingHrs As String = "")
    '    Try
    '        If mstrWorkingHrs.Trim.Length <= 0 Then
    '            Dim mintActivityHrs As Integer = CInt(mintShiftWorkingHrs * mdecPercentageHrs / 100)
    '            mstrDefaultWorkingHRs = CDec(CalculateTime(True, mintActivityHrs)).ToString("#00.00").Replace(".", ":")
    '            'txthours.text = mstrDefaultWorkingHRs
    '        Else
    '            Dim mintHours As Integer = CInt(mstrWorkingHrs.Trim.Substring(0, mstrWorkingHrs.Trim.IndexOf(":")))
    '            Dim mintMins As Integer = CInt(mstrWorkingHrs.Trim.Substring(mstrWorkingHrs.Trim.IndexOf(":") + 1))
    '            mintActivityWorkingHrs = (mintHours * 60) + mintMins
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    Private Function TextBoxTimeValidation(ByVal txt As MaskedTextBox) As Boolean
        Try

            If txt.Text.Contains(" ") OrElse txt.Text.Contains("_") Then
                txt.Text = txt.Text.Trim.Replace(" ", "0")
                txt.Text = txt.Text.Trim.Replace("_", "0")
            End If


            If txt.Text.Trim = ":" Then
                txt.Text = "00:00"
            ElseIf CDec(txt.Text.Trim.Replace(":", ".")) >= 24 Then
Isvalid:
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Invalid working hrs.Reason:Working hrs cannot greater than or equal to 24. "), enMsgBoxStyle.Information)
                txt.Focus()
                Return False

            ElseIf txt.Text.Trim.Contains(":") Then
                If txt.Text.Trim.Contains(":60") Then

calFromHr:
                    If CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4) <= 24 Then
                        txt.Text = CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4).ToString("#00.00").Replace(".", ":")
                    Else
                        GoTo Isvalid
                    End If
                ElseIf txt.Text <> "" And txt.Text <> ":" Then
                    If txt.Text.Trim.IndexOf(":") = 0 And txt.Text.Trim.Contains(":60") Then
                        GoTo calFromHr
                    ElseIf txt.Text.Substring(txt.Text.Trim.IndexOf(":"), txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":")) = ":" Then
                        txt.Text = txt.Text & "00"
                    ElseIf CDec(IIf(txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1) = "", "0", txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1))) > 59 Then
                        GoTo calFromHr
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TextBoxTimeValidation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function


    'Pinkal (06-Dec-2016) -- Start
    'Enhancement - Working on Budget Employee timesheet Changes as Per Suzan and Andrew Requirement.

    Private Sub FillEmpActivityList()
        Try

            Dim objEmpshift As New clsEmployee_Shift_Tran
            mintShiftID = objEmpshift.GetEmployee_Current_ShiftId(dtpFromDate.Value.Date, CInt(cboEmployee.SelectedValue))  'Pinkal (13-Oct-2017) -- Start
            objEmpshift = Nothing

            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(mintShiftID)
            Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpFromDate.Value.Date, FirstDayOfWeek.Sunday) - 1)  'Pinkal (13-Oct-2017) -- Start
            If drRow.Length > 0 Then
                mintShiftWorkingHrs = CInt(drRow(0)("workinghrsinsec"))
            End If
            objShiftTran = Nothing

            Dim objFundSource As New clsBudgetCodesfundsource_Tran
            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'Dim dsList As DataSet = objFundSource.GetEmployeeAssignActivities(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), _
            '                                                                  mintShiftWorkingHrs, dtpDate.Value.Date, mintEmptimesheetunkid)

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'Dim dsList As DataSet = objFundSource.GetEmployeeAssignActivities(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), _
            '                                                                  mintShiftWorkingHrs, dtpFromDate.Value.Date, _
            '                                                                  CBool(ConfigParameter._Object._AllowActivityHoursByPercentage), _
            '                                                                  mintEmptimesheetunkid)

            Dim dsList As DataSet = objFundSource.GetEmployeeAssignActivities(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), _
                                                                             mintShiftWorkingHrs, dtpFromDate.Value.Date, dtpToDate.Value.Date, _
                                                                              CBool(ConfigParameter._Object._AllowActivityHoursByPercentage), _
                                                                              mintEmptimesheetunkid)
            'Pinkal (13-Oct-2017) -- End


            'Nilay (21 Mar 2017) -- End


            dgvTimesheet.AutoGenerateColumns = False

            dgcolhEmpDonor.DataPropertyName = "Donor"
            dgcolhEmpProject.DataPropertyName = "Project"
            dgcolhEmpActivity.DataPropertyName = "Activity"
            dgcolhEmpHours.DataPropertyName = "ActivityHours"
            dgcolhEmpDescription.DataPropertyName = "Description"
            objdgcolhEmpPeriodunkid.DataPropertyName = "periodunkid"
            objdgcolhEmpDonorID.DataPropertyName = "fundsourceunkid"
            objdgcolhEmpProjectunkid.DataPropertyName = "fundprojectcodeunkid"
            objdgcolhEmpActivityunkid.DataPropertyName = "fundactivityunkid"

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..
            'objdgcolhEmpPercentage.DataPropertyName = "percentage"
            dgcolhEmpPercentage.DataPropertyName = "percentage"
            'Pinkal (28-Mar-2018) -- End

            objdgcolhEmpBudgetID.DataPropertyName = "budgetunkid"
            objdgcolhEmpHourInMin.DataPropertyName = "ActivityHoursInMin"
            objdgcolhEmpAssignedActivityHrsInMin.DataPropertyName = "AssignedActivityHrsInMin"



            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            dgcolhCostCenter.DataPropertyName = "costcenterunkid"
            If mstrCompanyGrpName.Trim.ToUpper() = "PSI MALAWI" Then
                dgcolhCostCenter.Visible = True
            End If
            'Pinkal (06-Jan-2023) -- End

            dgvTimesheet.DataSource = dsList.Tables(0)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim drTotalRow As DataRow = dsList.Tables(0).NewRow()
                'drTotalRow("budgetcodesfundsourcetranunkid") = -1
                drTotalRow("budgetunkid") = -999
                'drTotalRow("budgettranunkid") = -1
                drTotalRow("periodunkid") = -1
                drTotalRow("Employeeunkid") = -1
                drTotalRow("fundsourceunkid") = -1
                drTotalRow("Donor") = ""
                drTotalRow("fundprojectcodeunkid") = -1
                drTotalRow("Project") = ""
                drTotalRow("fundactivityunkid") = -1
                drTotalRow("Activity") = Language.getMessage(mstrModuleName, 29, "Total Activity Hrs :")

                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..
                'drTotalRow("percentage") = 0
                drTotalRow("percentage") = CDec(dsList.Tables(0).Compute("SUM(percentage)", "1=1"))
                'Pinkal (28-Mar-2018) -- End

                drTotalRow("emptimesheetunkid") = -1
                Dim mintActivityMins As Integer = CInt(dsList.Tables(0).Compute("SUM(ActivityHoursInMin)", "1=1"))
                drTotalRow("ActivityHours") = CalculateTime(True, mintActivityMins * 60).ToString("#00.00").Replace(".", ":")
                drTotalRow("ActivityHoursInMin") = 0
                drTotalRow("AssignedActivityHrsInMin") = 0
                drTotalRow("Description") = ""

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                drTotalRow("costcenterunkid") = 0
                'Pinkal (06-Jan-2023) -- End

                dsList.Tables(0).Rows.Add(drTotalRow)

                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'dgvTimesheet.Focus()
                'dgvTimesheet.CurrentCell = dgvTimesheet.Rows(0).Cells(dgcolhEmpHours.Index)
                'dgvTimesheet.CurrentCell.Selected = True
                'Nilay (21 Mar 2017) -- End
            End If

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            If dgvTimesheet.RowCount > 0 Then
                dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).Cells(objdgcolhShowDetails.Index).Value = imgBlank
            End If
            'Pinkal (28-Mar-2018) -- End

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If menAction = enAction.EDIT_ONE AndAlso dgcolhCostCenter.Visible AndAlso mintEditedCostCenterID > 0 Then
                Dim cbocostCenter As DataGridViewComboBoxCell = CType(dgvTimesheet.Rows(0).Cells(dgcolhCostCenter.Index), DataGridViewComboBoxCell)
                cbocostCenter.Value = mintEditedCostCenterID
                cbocostCenter = Nothing
            End If
            mintEditedCostCenterID = 0
            'Pinkal (06-Jan-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmpActivityList", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-Dec-2016) -- End

    'Nilay (21 Mar 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private Function isTimesheetExists(ByVal intRowIndex As Integer) As Boolean
        Try
            Return objBudgetTimesheet.isExist(CInt(cboEmployee.SelectedValue), _
                                              CInt(dgvTimesheet.Rows(intRowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value), _
                                              dtpFromDate.Value.Date, Nothing, _
                                              mintEmptimesheetunkid)
       'Pinkal (13-Oct-2017) -- Start
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isTimesheetExists", mstrModuleName)
        End Try
    End Function
    'Nilay (21 Mar 2017) -- End


    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowToAddEmployeeBudgetTimesheet
            btnEdit.Enabled = User._Object.Privilege._AllowToEditEmployeeBudgetTimesheet
            objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditEmployeeBudgetTimesheet
            objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteEmployeeBudgetTimesheet
            mnuGlobalDeleteTimesheet.Enabled = User._Object.Privilege._AllowToDeleteEmployeeBudgetTimesheet
            objdgcolhCancel.Visible = User._Object.Privilege._AllowToCancelEmployeeBudgetTimesheet
            mnuGlobalCancelTimesheet.Enabled = User._Object.Privilege._AllowToCancelEmployeeBudgetTimesheet
            mnuViewPendingSubmitApproval.Enabled = User._Object.Privilege._AllowToViewPendingEmpBudgetTimesheetSubmitForApproval
            mnuViewCompletedSubmitApproval.Enabled = User._Object.Privilege._AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgcolhActivity.Visible = ConfigParameter._Object._ShowBgTimesheetActivity
            dgcolhEmpActivity.Visible = ConfigParameter._Object._ShowBgTimesheetActivity
            dgcolhEmpPercentage.Visible = ConfigParameter._Object._ShowBgTimesheetAssignedActivityPercentage
            dgcolhEmpProject.Visible = ConfigParameter._Object._ShowBgTimesheetActivityProject
            dgcolhProject.Visible = ConfigParameter._Object._ShowBgTimesheetActivityProject
            objdgcolhShowDetails.Visible = ConfigParameter._Object._ShowBgTimesheetActivityHrsDetail
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Pinkal (03-May-2017) -- End


    'Pinkal (04-May-2018) -- Start
    'Enhancement - Budget timesheet Enhancement for MST/THPS.

    Private Function ValidateProjectHrs(ByVal xRowIndex As Integer, ByVal xColumnIndex As Integer) As Boolean
        Try
            Dim objProjectDetail As New frmProjectHoursDetails

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]

            'Dim dtTable As DataTable = objProjectDetail.GetProjectDetails(CInt(cboEmployee.SelectedValue), dtpFromDate.MinDate.Date, dtpToDate.MaxDate.Date _
            '                                                                                       , CInt(dgvTimesheet.Rows(xRowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value), CDec(dgvTimesheet.Rows(xRowIndex).Cells(dgcolhEmpPercentage.Index).Value) _
            '                                                                                       , False, CType(dgvEmpTimesheetList.DataSource, DataTable))

            Dim drRow As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
            If drRow IsNot Nothing Then

                'Pinkal (30-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                'objProjectDetail.mdtPeriodStartDate = eZeeDate.convertDate(drRow("start_date").ToString())
                'objProjectDetail.mdtPeriodEndDate = eZeeDate.convertDate(drRow("end_date").ToString())
                objProjectDetail.mdtPeriodStartDate = mdtPeriodStartDate
                objProjectDetail.mdtPeriodEndDate = mdtPeriodEndDate
                'Pinkal (30-Aug-2019) -- End
            End If

            Dim dtTable As DataTable = objProjectDetail.GetProjectDetails(CInt(cboEmployee.SelectedValue), dtpFromDate.MinDate.Date, dtpToDate.MaxDate.Date _
                                                                                                      , CInt(dgvTimesheet.Rows(xRowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value), CDec(dgvTimesheet.Rows(xRowIndex).Cells(dgcolhEmpPercentage.Index).Value) _
                                                                                                      , False, CType(dgvEmpTimesheetList.DataSource, DataTable))
            'Pinkal (13-Aug-2018) -- End

         

            Dim mstrMonthlyProjectHrs As String = "00:00"
            Dim mstrRemainingBalance As String = "00:00"
            Dim mstrTotalHrsEntries As String = "00:00"
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrMonthlyProjectHrs = dtTable.Rows(0)("Hours").ToString()  'Remaining Balance
                mstrRemainingBalance = dtTable.Rows(4)("Hours").ToString()  'Remaining Balance
                mstrTotalHrsEntries = dtTable.Rows(3)("Hours").ToString()  'Total Actual(running) number of hours (only Entries)
            End If

            Dim mintWorkingDays As Integer = 0
            Dim mdblWorkingHours As Double = 0
            Dim objEmpshiftTran As New clsEmployee_Shift_Tran
            objEmpshiftTran.GetTotalWorkingHours(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                              , ConfigParameter._Object._UserAccessModeSetting, CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, dtpToDate.Value.Date _
                                                              , mintWorkingDays, mdblWorkingHours, False)
            objEmpshiftTran = Nothing


            'Pinkal (30-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

            If menAction <> enAction.EDIT_ONE Then
            If CalculateSecondsFromHrs(mstrMonthlyProjectHrs) < (mintWorkingDays * mintActivityWorkingHrs * 60) + CalculateSecondsFromHrs(mstrTotalHrsEntries) Then
                objProjectDetail = Nothing
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "This Project Activity has only") & " " & mstrRemainingBalance & " " & Language.getMessage(mstrModuleName, 41, "Hours remaining."), enMsgBoxStyle.Information)
                Return False
            End If
            Else
                If CalculateSecondsFromHrs(mstrMonthlyProjectHrs) < (mintWorkingDays * mintActivityWorkingHrs * 60) + (CalculateSecondsFromHrs(mstrTotalHrsEntries) - (CInt(dgvTimesheet.Rows(xRowIndex).Cells(objdgcolhEmpAssignedActivityHrsInMin.Index).Value) * 60)) Then
                    objProjectDetail = Nothing
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "This Project Activity has only") & " " & mstrRemainingBalance & " " & Language.getMessage(mstrModuleName, 41, "Hours remaining."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Pinkal (30-Aug-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ValidateProjectHrs", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (04-May-2018) -- End

#End Region

#Region " Form's Events "

    Private Sub frmBudgetEmp_Timesheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objBudgetTimesheet = New clsBudgetEmp_timesheet
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()

            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            SetVisibility()
            'Pinkal (03-May-2017) -- End

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..
            dgcolhEmpPercentage.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'Pinkal (28-Mar-2018) -- End

            dtpFromDate.Focus()  'Pinkal (13-Oct-2017) -- Start
            btnSave.Visible = True
            btnEdit.Visible = False

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrCompanyGrpName = objGroup._Groupname.Trim
            objGroup = Nothing
            'Pinkal (06-Jan-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetEmp_Timesheet_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetEmp_Timesheet_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetEmp_Timesheet_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetEmp_Timesheet_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudgetEmp_timesheet.SetMessages()
            objfrm._Other_ModuleNames = "clsBudgetEmp_timesheet"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmBudgetEmp_Timesheet_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchPeriod.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            Dim cboCombo As ComboBox = Nothing
            If CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchPeriod.Name.ToUpper Then
                cboCombo = cboPeriod
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchEmployee.Name.ToUpper Then
                cboCombo = cboEmployee
                'ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchDonor.Name.ToUpper Then
                '    cboCombo = cboDonor
                'ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchProject.Name.ToUpper Then
                '    cboCombo = cboProject
                'ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchActivity.Name.ToUpper Then
                '    cboCombo = cboActivity
            End If

            dtList = CType(cboCombo.DataSource, DataTable)
            With cboCombo
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If cboCombo.Name.ToUpper = cboEmployee.Name.ToUpper Then objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Validation() = False Then Exit Sub
            mblnIsSubmitForApproval = False
            SetValue()

            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

            'If objBudgetTimesheet.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                         , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
            '                                        , ConfigParameter._Object._AllowActivityHoursByPercentage, ConfigParameter._Object._AllowToExceedTimeAssignedToActivity) = False Then

            Dim mstrExceededDates As String = ""

            If objBudgetTimesheet.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
                                                   , ConfigParameter._Object._AllowActivityHoursByPercentage, ConfigParameter._Object._AllowToExceedTimeAssignedToActivity _
                                                   , ConfigParameter._Object._AllowEmpAssignedProjectExceedTime, mstrExceededDates) = False Then

                'Pinkal (13-Aug-2019) -- End

                eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
            Else


                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                If mstrExceededDates.Trim.Length > 0 Then
                    mstrExceededDates = mstrExceededDates.Trim.Substring(0, mstrExceededDates.Trim().Length - 1)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Timesheet was not able to be inserted on the following dates:") & vbCrLf & _
                                                                     mstrExceededDates & vbCrLf & Language.getMessage(mstrModuleName, 48, "Reason: Allocated hours are greater than shift hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Timesheet inserted Successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                End If
                'Pinkal (13-Aug-2019) -- End

                dgvTimesheet.DataSource = Nothing
                objBudgetTimesheet = New clsBudgetEmp_timesheet
                FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If dgvTimesheet.RowCount <= 0 Then Exit Sub
            If Validation() = False Then Exit Sub
            SetValue()

            Dim mblnFlag As Boolean = False


            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            Dim mstrExceededDates As String = ""
            'Pinkal (13-Aug-2019) -- End


            If mintEmptimesheetunkid > 0 Then


                'Pinkal (30-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                'mblnFlag = objBudgetTimesheet.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
                '                                                           Company._Object._Companyunkid, dtpFromDate.Value.Date, ConfigParameter._Object._IsIncludeInactiveEmp _
                '                                                           , False, ConfigParameter._Object._AllowOverTimeToEmpTimesheet)

                mblnFlag = objBudgetTimesheet.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
                                                                         Company._Object._Companyunkid, dtpFromDate.Value.Date, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                         , False, ConfigParameter._Object._AllowOverTimeToEmpTimesheet, Nothing, False, mstrExceededDates)

                'Pinkal (30-Aug-2019) -- End


                '/* IT IS ONLY ALLOW FOR HOLIDAY INSERT WHEN OVERTIME IS ALLOW
            ElseIf mintEmptimesheetunkid <= 0 AndAlso ConfigParameter._Object._AllowOverTimeToEmpTimesheet Then


                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                
                'mblnFlag = objBudgetTimesheet.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                               , True, ConfigParameter._Object._IsIncludeInactiveEmp, dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
                '                               , ConfigParameter._Object._AllowActivityHoursByPercentage, ConfigParameter._Object._AllowToExceedTimeAssignedToActivity)

                mblnFlag = objBudgetTimesheet.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                  , True, ConfigParameter._Object._IsIncludeInactiveEmp, dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
                                             , ConfigParameter._Object._AllowActivityHoursByPercentage, ConfigParameter._Object._AllowToExceedTimeAssignedToActivity _
                                             , ConfigParameter._Object._AllowEmpAssignedProjectExceedTime, mstrExceededDates)


                'Pinkal (13-Aug-2019) -- End


                '/* IT IS ONLY ALLOW FOR HOLIDAY INSERT WHEN OVERTIME IS ALLOW
            End If

            If mblnFlag = False Then
                eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
            Else
                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                If mstrExceededDates.Trim.Length > 0 Then
                    mstrExceededDates = mstrExceededDates.Trim.Substring(0, mstrExceededDates.Trim().Length - 1)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Timesheet was not able to be inserted on the following dates:") & vbCrLf & _
                                                                     mstrExceededDates & vbCrLf & Language.getMessage(mstrModuleName, 48, "Reason: Allocated hours are greater than shift hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))

                    'Pinkal (30-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    If mintEmptimesheetunkid > 0 Then Exit Sub
                    'Pinkal (30-Aug-2019) -- End
                Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Timesheet updated Successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                End If
                mintEmptimesheetunkid = 0
                cboPeriod.Enabled = True
                dtpFromDate.Enabled = True
                cboEmployee.Enabled = True
                btnSave.Visible = True
                btnEdit.Visible = False
                dtpToDate.Enabled = True

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
                cboPeriod.SelectedValue = 0
                AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
                'Pinkal (28-Jul-2018) -- End

                cboPeriod.SelectedValue = objBudgetTimesheet._Periodunkid
                objBudgetTimesheet = New clsBudgetEmp_timesheet
                 mintActivityWorkingHrs = 0
                RemoveHandler dgvTimesheet.CellValidating, AddressOf dgvTimesheet_CellValidating
                dgvTimesheet.DataSource = Nothing
                AddHandler dgvTimesheet.CellValidating, AddressOf dgvTimesheet_CellValidating
            End If

            'Pinkal (28-Jul-2018) -- End


            'Pinkal (30-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            menAction = enAction.ADD_ONE
            'Pinkal (30-Aug-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (10 Jan 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification

    'Nilay (27 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    'Private Sub btnSubmitForApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
    '    Try

    '        objBudgetTimesheet = New clsBudgetEmp_timesheet

    '        mdtEmpTimesheet = New DataView(mdtEmpTimesheet, "IsChecked=1 AND IsGroup=0", "", DataViewRowState.CurrentRows).ToTable

    '        If mdtEmpTimesheet.Rows.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Dim dR As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGroup=0 AND issubmit_approval=1")
    '        If dR.Length > 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Some of the checked employee activity(s) is already sent for approval."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Dim objApproverMst As New clstsapprover_master
    '        Dim dtList As DataTable = objApproverMst.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
    '                                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                                     ConfigParameter._Object._IsIncludeInactiveEmp, -1, _
    '                                                                     CStr(cboEmployee.SelectedValue), -1, Nothing)

    '        objBudgetTimesheet._dtApproverList = dtList
    '        objBudgetTimesheet._dtEmployeeActivity = mdtEmpTimesheet
    '        'mblnIsSubmitForApproval = True
    '        objBudgetTimesheet._Employeeunkid = CInt(cboEmployee.SelectedValue)
    '        objBudgetTimesheet._Periodunkid = CInt(cboPeriod.SelectedValue)
    '        objBudgetTimesheet._Userunkid = User._Object._Userunkid
    '        'If objBudgetTimesheet.IsSubmitForApproval(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
    '        '                                          Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '        '                                          ConfigParameter._Object._IsIncludeInactiveEmp, mdtEmpTimesheet) = False Then
    '        If objBudgetTimesheet.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
            '                                          Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                     ConfigParameter._Object._IsIncludeInactiveEmp, True) = False Then

    '            eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
    '            Exit Sub
    '        Else
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Employee Timesheet activity(s) submitted for approval successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))

    '            Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue)) _
    '                                                               .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

    '            objBudgetTimesheet.Send_Notification_Approver(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
    '                                                          Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                          ConfigParameter._Object._IsIncludeInactiveEmp, CStr(cboEmployee.SelectedValue), _
    '                                                          strEmpTimesheetIDs, CInt(cboPeriod.SelectedValue), Nothing, False, -1, enLogin_Mode.DESKTOP, _
    '                                                          0, User._Object._Userunkid, ConfigParameter._Object._ArutiSelfServiceURL)
    '            chkSelectAll.Checked = False
    '            FillList()
    '        End If



    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnSubmitForApproval_Click", mstrModuleName)
    '    End Try
    'End Sub
    'Nilay (27 Feb 2017) -- End

    
    'Nilay (10 Jan 2017) -- End

#End Region

#Region " Combobox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            dtpFromDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date  'Pinkal (13-Oct-2017) -- Start
            dtpFromDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date  'Pinkal (13-Oct-2017) -- Start
            dtpToDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpToDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date
            'Pinkal (13-Oct-2017) -- End

            If CInt(cboPeriod.SelectedValue) > 0 Then

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)


                'Pinkal (05-Dec-2017) -- Start
                'Enhancement - In Budget Timesheet, Employee/User can't able to apply for future dates.

                Dim mdtEndDate As DateTime = ConfigParameter._Object._CurrentDateAndTime.Date
                If mdtEndDate.Date >= objPeriod._TnA_EndDate.Date Then
                    mdtEndDate = objPeriod._TnA_EndDate.Date
                End If

                'dtpFromDate.MinDate = objPeriod._TnA_StartDate.Date  'Pinkal (13-Oct-2017) -- Start
                'dtpFromDate.MaxDate = objPeriod._TnA_EndDate.Date  'Pinkal (13-Oct-2017) -- Start

                dtpFromDate.MinDate = objPeriod._TnA_StartDate.Date
                dtpFromDate.MaxDate = mdtEndDate.Date
                dtpToDate.MinDate = objPeriod._TnA_StartDate.Date
                dtpToDate.MaxDate = mdtEndDate.Date
                dtpFromDate.Value = dtpFromDate.MinDate.Date
                dtpToDate.Value = dtpToDate.MaxDate.Date
                mdtPeriodStartDate = objPeriod._TnA_StartDate.Date
                mdtPeriodEndDate = mdtEndDate.Date
                objPeriod = Nothing

            Else
                dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                'Pinkal (13-Oct-2017) -- End
            End If

            '
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, dtpFromDate.Value.Date _
                                                                   , dtpToDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , , , , , , , )


            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0
            objEmployee = Nothing
            dsList = Nothing

            GetEmployeeBudgetCodes()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try

            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue.
            Dim mdtDate As Date = Nothing


            RemoveHandler dtpFromDate.ValueChanged, AddressOf dtpDate_ValueChanged
            RemoveHandler dtpToDate.ValueChanged, AddressOf dtpDate_ValueChanged

            dtpFromDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpToDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpToDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date

            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtDate = CDate(objPeriod._TnA_StartDate).Date
                dtpFromDate.Value = mdtDate

                'Pinkal (05-Dec-2017) -- Start
                'Enhancement - In Budget Timesheet, Employee/User can't able to apply for future dates.
                Dim mdtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
                If mdtEndDate.Date >= CDate(objPeriod._TnA_EndDate).Date Then
                    mdtEndDate = CDate(objPeriod._TnA_EndDate).Date
                End If
                'dtpToDate.Value = CDate(objPeriod._TnA_EndDate).Date
                'dtpToDate.MaxDate = CDate(objPeriod._TnA_EndDate).Date
                dtpToDate.Value = mdtEndDate.Date
                dtpToDate.MaxDate = mdtEndDate.Date
                'Pinkal (05-Dec-2017) -- End

                objPeriod = Nothing
            End If
            'Pinkal (31-Oct-2017) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then


                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.

                Dim objemployee As New clsEmployee_Master
                objemployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                'Pinkal (31-Oct-2017) -- Start
                'Enhancement - Solve Budget Timesheet Issue.
                If objemployee._Reinstatementdate <> Nothing Then

                    If objemployee._Appointeddate.Date < objemployee._Reinstatementdate.Date AndAlso (objemployee._Reinstatementdate.Date >= dtpFromDate.Value.Date AndAlso objemployee._Reinstatementdate.Date <= dtpToDate.Value.Date) Then
                        mdtDate = objemployee._Reinstatementdate.Date
                    Else
                        mdtDate = dtpFromDate.Value.Date
                    End If

                ElseIf (objemployee._Appointeddate.Date >= dtpFromDate.Value.Date AndAlso objemployee._Appointeddate.Date <= dtpToDate.Value.Date) Then
                    mdtDate = objemployee._Appointeddate.Date

                    'If objemployee._Reinstatementdate <> Nothing OrElse objemployee._Appointeddate.Date > dtpFromDate.Value.Date Then
                    '    If objemployee._Appointeddate.Date > dtpFromDate.Value.Date Then
                    '        mdtDate = objemployee._Appointeddate.Date
                    '    ElseIf objemployee._Reinstatementdate.Date > dtpFromDate.Value.Date Then
                    '        mdtDate = dtpFromDate.Value.Date
                    '    Else
                    '        mdtDate = objemployee._Reinstatementdate.Date
                    '    End If

                    'Else
                    'If CInt(cboPeriod.SelectedValue) > 0 Then
                    '    Dim objPeriod As New clscommom_period_Tran
                    '    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    '    mdtDate = CDate(objPeriod._TnA_StartDate).Date
                    '    objPeriod = Nothing
                    'End If
                End If

                If objemployee._Termination_From_Date.Date <> Nothing OrElse objemployee._Empl_Enddate.Date <> Nothing Then
                    If objemployee._Termination_From_Date.Date <> Nothing AndAlso (objemployee._Termination_From_Date.Date >= dtpFromDate.Value.Date AndAlso objemployee._Termination_From_Date.Date <= dtpToDate.Value.Date) Then
                        dtpFromDate.MaxDate = objemployee._Termination_From_Date.Date
                        dtpToDate.MaxDate = objemployee._Termination_From_Date.Date
                    ElseIf objemployee._Empl_Enddate.Date <> Nothing AndAlso (objemployee._Empl_Enddate.Date >= dtpFromDate.Value.Date AndAlso objemployee._Empl_Enddate.Date <= dtpToDate.Value.Date) Then
                        dtpFromDate.MaxDate = objemployee._Empl_Enddate.Date
                        dtpToDate.MaxDate = objemployee._Empl_Enddate.Date
                    End If
                ElseIf (objemployee._Termination_To_Date.Date >= dtpFromDate.Value.Date AndAlso objemployee._Termination_To_Date.Date <= dtpToDate.Value.Date) Then
                    dtpFromDate.MaxDate = objemployee._Termination_To_Date.Date
                    dtpToDate.MaxDate = objemployee._Termination_To_Date.Date
                End If

                'Pinkal (31-Oct-2017) -- End


                dtpFromDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
                dtpToDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date

                'Pinkal (31-Oct-2017) -- Start
                'Enhancement - Solve Budget Timesheet Issue.
                'RemoveHandler dtpFromDate.ValueChanged, AddressOf dtpDate_ValueChanged
                'RemoveHandler dtpToDate.ValueChanged, AddressOf dtpDate_ValueChanged
                'Pinkal (31-Oct-2017) -- End
                dtpFromDate.MinDate = mdtDate
                dtpToDate.MinDate = mdtDate


                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                dtpFromDate.Value = dtpFromDate.MinDate.Date
                dtpToDate.Value = dtpToDate.MaxDate.Date
                'Pinkal (13-Oct-2017) -- End

                'Pinkal (31-Oct-2017) -- Start
                'Enhancement - Solve Budget Timesheet Issue.
                'AddHandler dtpToDate.ValueChanged, AddressOf dtpDate_ValueChanged
                'AddHandler dtpFromDate.ValueChanged, AddressOf dtpDate_ValueChanged
                'Pinkal (31-Oct-2017) -- End

                mdtPeriodStartDate = dtpFromDate.Value.Date
                mdtPeriodEndDate = dtpToDate.Value.Date
                objemployee = Nothing
                'Pinkal (13-Oct-2017) -- End

                FillList()
                FillEmpActivityList()
            Else
                dgvTimesheet.DataSource = Nothing
                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                dgvEmpTimesheetList.DataSource = Nothing
                'Pinkal (13-Oct-2017) -- End
            End If

            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue.
            AddHandler dtpToDate.ValueChanged, AddressOf dtpDate_ValueChanged
            AddHandler dtpFromDate.ValueChanged, AddressOf dtpDate_ValueChanged
            'Pinkal (31-Oct-2017) -- End


            GetEmployeeBudgetCodes()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboDonor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim objProjectCode As New clsFundProjectCode
    '        Dim dsList As DataSet = objProjectCode.GetComboList("List", True, CInt(cboDonor.SelectedValue))
    '        cboProject.DisplayMember = "fundprojectname"
    '        cboProject.ValueMember = "fundprojectcodeunkid"
    '        cboProject.DataSource = dsList.Tables(0)
    '        cboProject.SelectedValue = 0
    '        dsList = Nothing
    '        objProjectCode = Nothing
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboDonor_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboProject_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    'Dim objActivity As New clsfundactivity_Tran
    '        Dim dsList As DataSet = objActivity.GetComboList("List", True, CInt(cboProject.SelectedValue))
    '        cboActivity.DisplayMember = "activityname"
    '        cboActivity.ValueMember = "fundactivityunkid"
    '        cboActivity.DataSource = dsList.Tables(0)
    '        cboActivity.SelectedValue = 0
    '        dsList = Nothing
    '        objActivity = Nothing
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboProject_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboActivity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        GetEmployeeBudgetCodes()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.GotFocus, cboEmployee.GotFocus, cboDonor.GotFocus, cboProject.GotFocus, cboActivity.GotFocus
    '    Try
    '        With CType(sender, ComboBox)
    '            .ForeColor = Color.Black
    '            .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
    '            If .Text = mstrSearchText Then
    '                .Text = ""
    '            End If
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_GotFocus", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.Leave, cboEmployee.Leave, cboDonor.Leave, cboProject.Leave, cboActivity.Leave
    '    Try
    '        If CInt(CType(sender, ComboBox).SelectedValue) <= 0 Then
    '            Call SetDefaultSearchText(CType(sender, ComboBox))
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_Leave", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress, cboEmployee.KeyPress, cboDonor.KeyPress, cboProject.KeyPress, cboActivity.KeyPress
    '    Try
    '        If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = CType(sender, ComboBox).ValueMember
    '                .DisplayMember = CType(sender, ComboBox).DisplayMember
    '                .DataSource = CType(CType(sender, ComboBox).DataSource, DataTable)
    '                If CType(sender, ComboBox).Name = cboPeriod.Name Then
    '                    .CodeMember = "code"
    '                ElseIf CType(sender, ComboBox).Name = cboEmployee.Name Then
    '                    .CodeMember = "employeecode"
    '                ElseIf CType(sender, ComboBox).Name = cboDonor.Name Then
    '                    .CodeMember = "fundcode"
    '                ElseIf CType(sender, ComboBox).Name = cboProject.Name Then
    '                    .CodeMember = "fundprojectcode"
    '                ElseIf CType(sender, ComboBox).Name = cboActivity.Name Then
    '                    .CodeMember = "activitycode"
    '                End If



    '            End With

    '            Dim c As Char = Convert.ToChar(e.KeyChar)
    '            frm.TypedText = c.ToString

    '            If frm.DisplayDialog Then
    '                CType(sender, ComboBox).SelectedValue = frm.SelectedValue
    '                CType(sender, ComboBox).Tag = frm.SelectedAlias
    '                e.KeyChar = ChrW(Keys.ShiftKey)
    '            Else
    '                CType(sender, ComboBox).Text = ""
    '                CType(sender, ComboBox).Tag = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_KeyPress", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region "DatePicker Events"


    Private Sub dtpDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.ValueChanged, dtpToDate.ValueChanged
        Try

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            FillEmpActivityList()
            'Pinkal (28-Mar-2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDate_ValueChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "Datagrid Event"

    Private Sub dgvEmpTimesheetList_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpTimesheetList.CellContentClick, dgvEmpTimesheetList.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhEdit.Index Then


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                If CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhActivityHoursInMin.Index).Value) > 0 Then

                If CBool(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhIsSubmitForApproval.Index).Value) = True AndAlso CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 2 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "You can't Edit this timesheet. Reason: This timesheet is already submitted for approval."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                End If

                If CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 1 Then 'Approved
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You can't Edit this timesheet. Reason: This timesheet is already approved."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                ElseIf CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 3 Then 'Rejected
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You can't Edit this timesheet. Reason: This timesheet is already rejected."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                ElseIf CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 2 Then  'Pending
                    Dim objApproval As New clstsemptimesheet_approval

                        'Pinkal (13-Aug-2018) -- Start
                        'Enhancement - Changes For PACT [Ref #249,252]
                        'Dim dsPendingList As DataSet = objApproval.GetList("List", FinancialYear._Object._DatabaseName, CDate(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(dgcolhDate.Index).Value).Date _
                        '                                                                            , ConfigParameter._Object._CompanyDateFormat, User._Object._Userunkid, False, Nothing, True _
                        '                                                                            , CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value), False _
                        '                                                                            , "tsemptimesheet_approval.statusunkid <> 2 ")
                    Dim dsPendingList As DataSet = objApproval.GetList("List", FinancialYear._Object._DatabaseName, CDate(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(dgcolhDate.Index).Value).Date _
                                                                                                    , ConfigParameter._Object._CompanyDateFormat, User._Object._Userunkid, False, ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
                                                                                                    , Nothing, True, CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value), False _
                                                                                                , "tsemptimesheet_approval.statusunkid <> 2 ")
                        'Pinkal (13-Aug-2018) -- End
                        

                    If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "You can't edit this timesheet. Reason: This timesheet is already approved by some of the approver(s) of this employee."), enMsgBoxStyle.Information) '?1
                        Exit Sub
                    End If
                    dsPendingList.Clear()
                    dsPendingList = Nothing
                    objApproval = Nothing
                End If

                End If

                'Pinkal (28-Jul-2018) -- End

                cboPeriod.Enabled = False
                dtpFromDate.Enabled = False
                dtpToDate.Enabled = False
                cboEmployee.Enabled = False
                btnEdit.Visible = True
                btnSave.Visible = False

SetEditValue:
                RemoveHandler dgvTimesheet.CellValidating, AddressOf dgvTimesheet_CellValidating 'Nilay (21 Mar 2017)

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                If CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhPeriodID.Index).Value) > 0 Then
                cboPeriod.SelectedValue = CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhPeriodID.Index).Value)
                End If
                'Pinkal (28-Jul-2018) -- End

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                If dgcolhCostCenterList.Visible AndAlso CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhCostCenterIdList.Index).Value) > 0 Then
                    mintEditedCostCenterID = CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhCostCenterIdList.Index).Value)
                End If
                'Pinkal (06-Jan-2023) -- End



                RemoveHandler dtpFromDate.ValueChanged, AddressOf dtpDate_ValueChanged
                dtpFromDate.Value = CDate(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(dgcolhDate.Index).Value).Date
                AddHandler dtpFromDate.ValueChanged, AddressOf dtpDate_ValueChanged

                RemoveHandler dtpToDate.ValueChanged, AddressOf dtpDate_ValueChanged
                dtpToDate.Value = CDate(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(dgcolhDate.Index).Value).Date
                AddHandler dtpToDate.ValueChanged, AddressOf dtpDate_ValueChanged


                cboEmployee.SelectedValue = CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value)

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                If IsDBNull(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value) = False AndAlso CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value) > 0 Then
                mintEmptimesheetunkid = CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value)
                End If
                'Pinkal (28-Jul-2018) -- End
                btnSave.Visible = False
                btnEdit.Visible = True
                AddHandler dgvTimesheet.CellValidating, AddressOf dgvTimesheet_CellValidating 'Nilay (21 Mar 2017)

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                menAction = enAction.EDIT_ONE
                'Pinkal (06-Jan-2023) -- End

                FillEmpActivityList()

                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                mintOldActivityWorkingHrsInMin = CInt(dgvTimesheet.Rows(0).Cells(objdgcolhEmpHourInMin.Index).Value)
                'Nilay (21 Mar 2017) -- End


            ElseIf e.ColumnIndex = objdgcolhDelete.Index Then


                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                If CBool(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhIsSubmitForApproval.Index).Value) = True AndAlso CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 2 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "You can't Delete this timesheet. Reason: This timesheet is already submitted for approval."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                End If
                'Pinkal (13-Oct-2017) -- End


                If CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 1 Then 'Approved
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You can't delete this timesheet. Reason: This timesheet is already approved."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                ElseIf CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 3 Then 'Rejected
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "You can't delete this timesheet. Reason: This timesheet is already rejected."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                ElseIf CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 2 Then  'Pending
                    Dim objApproval As New clstsemptimesheet_approval

                    'Pinkal (13-Aug-2018) -- Start
                    'Enhancement - Changes For PACT [Ref #249,252]

                    'Dim dsPendingList As DataSet = objApproval.GetList("List", FinancialYear._Object._DatabaseName, CDate(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(dgcolhDate.Index).Value).Date _
                    '                                                , ConfigParameter._Object._CompanyDateFormat, User._Object._Userunkid, False, Nothing, True _
                    '                                                , CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value), False _
                    '                                                , " tsemptimesheet_approval.statusunkid <> 2 ")

                    Dim dsPendingList As DataSet = objApproval.GetList("List", FinancialYear._Object._DatabaseName, CDate(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(dgcolhDate.Index).Value).Date _
                                                                                                 , ConfigParameter._Object._CompanyDateFormat, User._Object._Userunkid, False, ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
                                                                                                 , Nothing, True, CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value), False _
                                                                     , " tsemptimesheet_approval.statusunkid <> 2 ")

                    'Pinkal (13-Aug-2018) -- End
                   

                    If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "You can't delete this timesheet. Reason: This timesheet is already approved by some of the approver(s) of this employee."), enMsgBoxStyle.Information) '?1
                        Exit Sub
                    End If
                    dsPendingList.Clear()
                    dsPendingList = Nothing
                    objApproval = Nothing

                End If

                'Nilay (02-Jan-2017) -- Start
                'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
                'ResetValue()
                'Nilay (02-Jan-2017) -- End
                cboPeriod.Enabled = True
                dtpFromDate.Enabled = True  'Pinkal (13-Oct-2017) -- Start
                cboEmployee.Enabled = True
                btnEdit.Visible = True
                btnSave.Visible = True

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Are you sure you want to delete this employee timesheet?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason) 'Nilay (10 Jan 2017)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBudgetTimesheet._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objBudgetTimesheet._Voiduserunkid = User._Object._Userunkid
                objBudgetTimesheet._LoginEmployeeunkid = -1

                If objBudgetTimesheet.Delete(FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value)) = False Then
                    eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Timesheet deleted Successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))

                    'Nilay (10 Jan 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                    objBudgetTimesheet.Send_Notification_Employee(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                                  Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                  ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                  CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value), _
                                                                  CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmpTimesheetID.Index).Value), _
                                                                  CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED, _
                                                                  enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, _
                                                                  ConfigParameter._Object._ArutiSelfServiceURL, mstrVoidReason, , , False)
                    'Nilay (01 Apr 2017) -- [CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value), False]

                    'Nilay (10 Jan 2017) -- End

                    'Nilay (02-Jan-2017) -- Start
                    'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
                    'ResetValue()
                    objBudgetTimesheet = New clsBudgetEmp_timesheet
                    'Nilay (02-Jan-2017) -- End
                    FillList()
                End If

            ElseIf e.ColumnIndex = objdgcolhCancel.Index Then

                If CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 2 Then 'Pending
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "You can't cancel this timesheet. Reason: This timesheet is in pending status."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                ElseIf CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhStatusId.Index).Value) = 3 Then 'Rejected
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "You can't cancel this timesheet. Reason: This timesheet is already rejected."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Are you sure you want to cancel this employee timesheet?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                Dim mstrReason As String = ""
                Dim objFrm As New frmRemark
                objFrm.Text = Language.getMessage(mstrModuleName, 21, "Cancel Reason")
                objFrm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 21, "Cancel Reason")
                If objFrm.displayDialog(mstrReason, enArutiApplicatinType.Aruti_Payroll) = False Then
                    Exit Sub
                End If

                'Nilay (01 Apr 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'Dim objApproverMst As New clstsapprover_master
                'Dim dtList As DataTable = objApproverMst.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                '                                                           , -1, CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value), -1, Nothing)
                'objBudgetTimesheet._Emptimesheetunkid = CInt(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmpTimesheetID.Index).Value)
                'objBudgetTimesheet._dtApproverList = dtList
                'Nilay (01 Apr 2017) -- End

                objBudgetTimesheet._Userunkid = User._Object._Userunkid

                'Nilay (15 Feb 2017) -- Start
                'Nilay (01 Apr 2017) -- Start
                'Dim intApprovedActivityHours As Integer = CInt(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhApprovedActivityHoursInMin.Index).Value)
                'Nilay (01 Apr 2017) -- End
                'Nilay (15 Feb 2017) -- End

                'Nilay (01 Apr 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'If objBudgetTimesheet.CancelTimesheet(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid _
                '                                    , Company._Object._Companyunkid, CDate(dgvEmpTimesheetList.SelectedRows(0).Cells(dgcolhDate.Index).Value).Date _
                '                                    , ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._UserAccessModeSetting, _
                '                                      mstrReason, CInt(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmpTimesheetID.Index).Value), _
                '                                      Nothing) = False Then

                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.


                'If objBudgetTimesheet.CancelTimesheet(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
                '                                      Company._Object._Companyunkid, ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                      ConfigParameter._Object._UserAccessModeSetting, mstrReason, _
                '                                      CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmpTimesheetID.Index).Value), _
                '                                      Nothing) = False Then


                If objBudgetTimesheet.CancelTimesheet(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
                                                      Company._Object._Companyunkid, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                      ConfigParameter._Object._UserAccessModeSetting, mstrReason, _
                                                      CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmpTimesheetID.Index).Value), _
                                                      ConfigParameter._Object._AllowOverTimeToEmpTimesheet, Nothing) = False Then

                    'Pinkal (28-Mar-2018) -- End

                    'Nilay (01 Apr 2017) -- End

                    eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                Else
                    FillList()

                    'Nilay (15 Feb 2017) -- Start

                    'Nilay (01 Apr 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    'objBudgetTimesheet._ApprovedActivity_Hrs = intApprovedActivityHours
                    'Nilay (01 Apr 2017) -- End

                    'Nilay (15 Feb 2017) -- End

                    'Nilay (10 Jan 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                    objBudgetTimesheet.Send_Notification_Employee(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                                  Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                  ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                  CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value), _
                                                                  CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmpTimesheetID.Index).Value), _
                                                                  CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                                  enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, ConfigParameter._Object._ArutiSelfServiceURL, _
                                                                  mstrReason, , , False)
                    'Nilay (01 Apr 2017) -- [CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value), CStr(dgvEmpTimesheetList.SelectedRows(0).Cells(objdgcolhEmpTimesheetID.Index).Value), False]
                    'Nilay (10 Jan 2017) -- End
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheetList_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTimesheet_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvTimesheet.CellValidating
        Try
            If e.RowIndex < 0 OrElse CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpBudgetID.Index).Value) < 0 Then Exit Sub 'Nilay (21 Mar 2017)

            If e.FormattedValue.ToString().Trim.Length <= 0 Then Exit Sub

            If dgvTimesheet.CurrentCell.ColumnIndex = dgcolhEmpHours.Index Then

            If e.ColumnIndex = dgcolhEmpHours.Index Then

                If dgvTimesheet.IsCurrentCellDirty Then
                    dgvTimesheet.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim objBudgetCodes As New clsBudgetcodes_master

                If CInt(cboEmployee.SelectedValue) > 0 Then

                    Dim txtHours As New MaskedTextBox
                    txtHours.Mask = dgcolhEmpHours.Mask
                    txtHours.TextMaskFormat = MaskFormat.IncludePromptAndLiterals
                    txtHours.Text = dgvTimesheet.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()

                    If TextBoxTimeValidation(txtHours) = False Then
                        e.Cancel = True
                        Exit Sub
                    End If

                    If txtHours.MaskCompleted = False OrElse txtHours.MaskFull = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Hours is compulsory information.Please enter activity hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        txtHours.Text = "00:00"
                        txtHours.Focus()
                        e.Cancel = True
                        Exit Sub
                    End If

                    dgvTimesheet.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = txtHours.Text

                    Dim mstrAssignedHrs As String = ""
                    Dim dsList As DataSet = objBudgetCodes.GetEmployeeActivityPercentage(CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpPeriodunkid.Index).Value), _
                                                                                         CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpBudgetID.Index).Value), _
                                                                                         CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value), _
                                                                                         CInt(cboEmployee.SelectedValue))

                    Dim objShiftTran As New clsshift_tran
                    objShiftTran.GetShiftTran(mintShiftID)
                        Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpFromDate.Value.Date, FirstDayOfWeek.Sunday) - 1)  'Pinkal (13-Oct-2017) -- Start

            
                    If drRow.Length > 0 Then

                        Dim mstrWorkingHrs As String = txtHours.Text
                        Dim mintHours As Integer = CInt(mstrWorkingHrs.Trim.Substring(0, mstrWorkingHrs.Trim.IndexOf(":")))
                        Dim mintMins As Integer = CInt(mstrWorkingHrs.Trim.Substring(mstrWorkingHrs.Trim.IndexOf(":") + 1))
                        mintActivityWorkingHrs = (mintHours * 60) + mintMins
                            dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpHourInMin.Index).Value = mintActivityWorkingHrs

                            If isTimesheetExists(e.RowIndex) = True AndAlso dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpHours.Index).IsInEditMode = True Then
                                eZeeMsgBox.Show(Language.getMessage("clsBudgetEmp_timesheet", 1, "This Activity is already defined of this employee for same date. Please define different activity for this date."), enMsgBoxStyle.Information)
                                e.Cancel = True
                                Exit Sub
                            End If

                        Dim objBudgetTsApproval As New clstsemptimesheet_approval


                            Dim intComputeActivityHoursInMin As Integer = 0
                        Dim intTotalActivityHoursInMin As Integer = objBudgetTsApproval.ComputeActivityHours(CInt(cboEmployee.SelectedValue), _
                                                                                                                 dtpFromDate.Value.Date, _
                                                                                                             CInt(cboPeriod.SelectedValue))

                        If intTotalActivityHoursInMin <= 0 Then
                                If mintEmptimesheetunkid > 0 Then
                            intTotalActivityHoursInMin = CInt(CType(dgvTimesheet.DataSource, DataTable).Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
                                Else

                                    If IsDBNull(CType(dgvTimesheet.DataSource, DataTable).Compute("SUM(ActivityHoursInMin)", "fundactivityunkid <> " & _
                                                                                                                        CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value) & _
                                                                                                                        " AND  budgetunkid <> -999")) = False Then
                                    intTotalActivityHoursInMin = CInt(CType(dgvTimesheet.DataSource, DataTable).Compute("SUM(ActivityHoursInMin)", "fundactivityunkid <> " & _
                                                                                                                        CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value) & _
                                                                                                                        " AND  budgetunkid <> -999"))
                                End If
                                End If
                            End If

                            If mintEmptimesheetunkid > 0 Then
                                intComputeActivityHoursInMin = intTotalActivityHoursInMin - mintOldActivityWorkingHrsInMin + mintActivityWorkingHrs 'Nilay (21 Mar 2017)
                            Else
                                intComputeActivityHoursInMin = intTotalActivityHoursInMin + mintActivityWorkingHrs
                        End If

                        If CBool(ConfigParameter._Object._AllowToExceedTimeAssignedToActivity) = True Then

                            If CBool(ConfigParameter._Object._AllowOverTimeToEmpTimesheet) = False Then


                                    'Pinkal (13-Aug-2019) -- Start
                                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

                                    'If CInt(CInt(drRow(0)("workinghrsinsec")) / 60) < mintActivityWorkingHrs Then 'workinghrsinsec=shifthours

                                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "This employee does not allow to do this activity hours more than define shift hours."), enMsgBoxStyle.Information)
                                    '    dgvTimesheet.CurrentCell = dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpHours.Index)
                                    '    e.Cancel = True
                                    '    Exit Sub

                                    'ElseIf CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpAssignedActivityHrsInMin.Index).Value) < mintActivityWorkingHrs AndAlso CInt(CInt(drRow(0)("workinghrsinsec")) / 60) >= mintActivityWorkingHrs Then


                                    '    If intComputeActivityHoursInMin > CInt(CInt(drRow(0)("workinghrsinsec")) / 60) Then
                                    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Total activity hours cannot be greater than shift hours."), enMsgBoxStyle.Information)
                                    '        dgvTimesheet.CurrentCell = dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpHours.Index)
                                        '    e.Cancel = True
                                        '    Exit Sub
                                    '    End If

                                    If CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpAssignedActivityHrsInMin.Index).Value) < mintActivityWorkingHrs AndAlso CInt(CInt(drRow(0)("workinghrsinsec")) / 60) >= mintActivityWorkingHrs Then

                                        If ConfigParameter._Object._AllowEmpAssignedProjectExceedTime Then
                                            Dim objExemptEmp As New clstsexemptemployee_tran
                                            If objExemptEmp.isExist(CInt(cboEmployee.SelectedValue), -1) Then
                                                If CType(dgvTimesheet.DataSource, DataTable) IsNot Nothing AndAlso CType(dgvTimesheet.DataSource, DataTable).Rows.Count > 0 Then
                                                    Dim mintActivityMins As Integer = CInt(CType(dgvTimesheet.DataSource, DataTable).Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
                                                    CType(dgvTimesheet.DataSource, DataTable).Rows(dgvTimesheet.Rows.Count - 1)("ActivityHours") = CalculateTime(True, mintActivityMins * 60).ToString("#00.00").Replace(".", ":")
                                                    CType(dgvTimesheet.DataSource, DataTable).Rows(dgvTimesheet.Rows.Count - 1)("activity") = Language.getMessage(mstrModuleName, 29, "Total Activity Hrs :")
                                                End If
                                                objExemptEmp = Nothing
                                                Exit Sub
                                            End If
                                            objExemptEmp = Nothing
                                        Else
                                        If ValidateProjectHrs(e.RowIndex, e.ColumnIndex) = False Then
                                            dgvTimesheet.CurrentCell = dgvTimesheet.Rows(e.RowIndex).Cells(e.ColumnIndex)
                                            btnSave.Enabled = False
                                            e.Cancel = True
                                            Exit Sub
                                        End If
                                            If btnSave.Enabled = False Then btnSave.Enabled = True
                                        End If

                                        'ElseIf intComputeActivityHoursInMin > CInt(CInt(drRow(0)("workinghrsinsec")) / 60) Then
                                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Total activity hours cannot be greater than shift hours."), enMsgBoxStyle.Information)
                                        '    dgvTimesheet.CurrentCell = dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpHours.Index)
                                        '    e.Cancel = True
                                        '    Exit Sub

                                        'Pinkal (13-Aug-2019) -- End

                                    End If

                                        If btnSave.Enabled = False Then btnSave.Enabled = True

                            End If

                        Else


                                'Pinkal (30-Aug-2019) -- Start
                                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                If ConfigParameter._Object._AllowEmpAssignedProjectExceedTime Then
                                    Dim objExemptEmp As New clstsexemptemployee_tran
                                    If objExemptEmp.isExist(CInt(cboEmployee.SelectedValue), -1) Then
                                        If CType(dgvTimesheet.DataSource, DataTable) IsNot Nothing AndAlso CType(dgvTimesheet.DataSource, DataTable).Rows.Count > 0 Then
                                            Dim mintActivityMins As Integer = CInt(CType(dgvTimesheet.DataSource, DataTable).Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
                                            CType(dgvTimesheet.DataSource, DataTable).Rows(dgvTimesheet.Rows.Count - 1)("ActivityHours") = CalculateTime(True, mintActivityMins * 60).ToString("#00.00").Replace(".", ":")
                                            CType(dgvTimesheet.DataSource, DataTable).Rows(dgvTimesheet.Rows.Count - 1)("activity") = Language.getMessage(mstrModuleName, 29, "Total Activity Hrs :")
                                        End If
                                        objExemptEmp = Nothing
                                    Exit Sub
                                        'End If  ' If intComputeActivityHoursInMin > CInt(CInt(drRow(0)("workinghrsinsec")) / 60) Then
                                    Else
                                        If CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpAssignedActivityHrsInMin.Index).Value) < mintActivityWorkingHrs Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This employee does not allow to do this activity more than define activity percentage."), enMsgBoxStyle.Information)
                                            dgvTimesheet.CurrentCell = dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpHours.Index)
                                            e.Cancel = True
                                            Exit Sub
                                        ElseIf ValidateProjectHrs(e.RowIndex, e.ColumnIndex) = False Then
                                            dgvTimesheet.CurrentCell = dgvTimesheet.Rows(e.RowIndex).Cells(e.ColumnIndex)
                                            btnSave.Enabled = False
                                            e.Cancel = True
                                            Exit Sub
                                End If
                                        If btnSave.Enabled = False Then btnSave.Enabled = True
                                    End If ' If objExemptEmp.isExist(CInt(cboEmployee.SelectedValue), -1) Then
                                    objExemptEmp = Nothing
                            End If
                                'Pinkal (30-Aug-2019) -- End

                                'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                '    If CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpAssignedActivityHrsInMin.Index).Value) < mintActivityWorkingHrs Then
                                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This employee does not allow to do this activity more than define activity percentage."), enMsgBoxStyle.Information)
                                '        dgvTimesheet.CurrentCell = dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpHours.Index)
                                '        e.Cancel = True
                                '        Exit Sub
                                '    End If
                                'End If

                                'Pinkal (13-Aug-2019) -- End

                        End If


                        If CType(dgvTimesheet.DataSource, DataTable) IsNot Nothing AndAlso CType(dgvTimesheet.DataSource, DataTable).Rows.Count > 0 Then
                            Dim mintActivityMins As Integer = CInt(CType(dgvTimesheet.DataSource, DataTable).Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
                            CType(dgvTimesheet.DataSource, DataTable).Rows(dgvTimesheet.Rows.Count - 1)("ActivityHours") = CalculateTime(True, mintActivityMins * 60).ToString("#00.00").Replace(".", ":")
                            CType(dgvTimesheet.DataSource, DataTable).Rows(dgvTimesheet.Rows.Count - 1)("activity") = Language.getMessage(mstrModuleName, 29, "Total Activity Hrs :")

                            If mintActivityMins >= (24 * 60) Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Invalid Total working hrs.Reason:Total Working hrs cannot greater than or equal to 24. "), enMsgBoxStyle.Information)
                                e.Cancel = True
                                Exit Sub
                            End If
                        End If
                    End If
                End If
                objBudgetCodes = Nothing
            End If
            End If
       

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If e.ColumnIndex = dgcolhCostCenter.Index Then
                dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhCostCenter.Index).Value = CInt(dgvTimesheet.CurrentRow.Cells(dgcolhCostCenter.Index).Value)
            End If
            'Pinkal (06-Jan-2023) -- End

            If e.ColumnIndex = dgcolhDescription.Index Then
                dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhDescription.Index).Value = dgvTimesheet.CurrentRow.Cells(dgcolhDescription.Index).Value.ToString.Trim
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimesheet_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTimesheet_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvTimesheet.DataBindingComplete
        Try
            If dgvTimesheet.RowCount <= 0 Then Exit Try

            If CInt(dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).Cells(objdgcolhEmpBudgetID.Index).Value) = -999 Then 'Grand Total
                dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).ReadOnly = True
                dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).DefaultCellStyle.BackColor = Color.Gray
                dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).DefaultCellStyle.ForeColor = Color.White
                dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).DefaultCellStyle.SelectionBackColor = Color.Gray
                dgvTimesheet.Rows(dgvTimesheet.RowCount - 1).DefaultCellStyle.SelectionForeColor = Color.White
                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                Dim cmbCostCenter As DataGridViewComboBoxCell = CType(dgvTimesheet.Item(dgcolhCostCenter.Index, dgvTimesheet.RowCount - 1), DataGridViewComboBoxCell)
                cmbCostCenter.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                cmbCostCenter.Style.BackColor = Color.Gray
                'Pinkal (06-Jan-2023) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimesheet_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTimesheet_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvTimesheet.CurrentCellDirtyStateChanged
        Try
            If dgvTimesheet.IsCurrentCellDirty Then
                dgvTimesheet.CommitEdit(DataGridViewDataErrorContexts.Commit)
                dgvTimesheet.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimesheet_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

    Private Sub dgvTimesheet_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTimesheet.CellContentClick, dgvTimesheet.CellContentDoubleClick
        Try
            If e.ColumnIndex = objdgcolhShowDetails.Index Then

                Dim mstrEmployeeCode As String = ""
                Dim drEmpDataRow As DataRowView = CType(cboEmployee.SelectedItem, DataRowView)
                If drEmpDataRow IsNot Nothing Then
                    mstrEmployeeCode = drEmpDataRow("employeecode").ToString()
                End If


                Dim objFrm As New frmProjectHoursDetails()
                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                'objFrm.displayDialog(CInt(cboEmployee.SelectedValue), mstrEmployeeCode & " - " & cboEmployee.Text, cboPeriod.Text _
                '                                    , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpProject.Index).Value.ToString() _
                '                                    , dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpDonor.Index).Value.ToString(), CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value) _
                '                                    , dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpActivity.Index).Value.ToString() _
                '                                    , Convert.ToDouble(dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpPercentage.Index).Value), CType(dgvEmpTimesheetList.DataSource, DataTable), False)

                Dim drRow As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                Dim mdtPStartDate As Date = Nothing
                Dim mdtPEndDate As Date = Nothing
                If drRow IsNot Nothing Then

                    'Pinkal (30-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    'mdtPStartDate = eZeeDate.convertDate(drRow("start_date").ToString())
                    'mdtPEndDate = eZeeDate.convertDate(drRow("end_date").ToString())
                    mdtPStartDate = mdtPeriodStartDate.Date
                    mdtPEndDate = mdtPeriodEndDate.Date
                    'Pinkal (30-Aug-2019) -- End
                End If

                objFrm.displayDialog(CInt(cboEmployee.SelectedValue), mstrEmployeeCode & " - " & cboEmployee.Text, cboPeriod.Text _
                                                    , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpProject.Index).Value.ToString() _
                                                    , dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpDonor.Index).Value.ToString(), CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value) _
                                                    , dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpActivity.Index).Value.ToString() _
                                                , Convert.ToDouble(dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpPercentage.Index).Value), CType(dgvEmpTimesheetList.DataSource, DataTable), False _
                                                , mdtPStartDate, mdtPEndDate)
                'Pinkal (16-May-2018) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimesheet_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTimesheet_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTimesheet.CellValidated
        Try
            If CBool(ConfigParameter._Object._AllowOverTimeToEmpTimesheet) AndAlso e.ColumnIndex = dgcolhEmpHours.Index Then
                Dim RowIndex As Integer = dgvTimesheet.CurrentCell.RowIndex
                Dim objProjectDetail As New frmProjectHoursDetails

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                objProjectDetail.mdtPeriodStartDate = dtpFromDate.MinDate.Date
                objProjectDetail.mdtPeriodEndDate = dtpToDate.MaxDate.Date
                'Pinkal (28-Jul-2018) -- End
                Dim dtTable As DataTable = objProjectDetail.GetProjectDetails(CInt(cboEmployee.SelectedValue), dtpFromDate.MinDate.Date, dtpToDate.MaxDate.Date _
                                                                                                          , CInt(dgvTimesheet.Rows(RowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value), CDec(dgvTimesheet.Rows(RowIndex).Cells(dgcolhEmpPercentage.Index).Value) _
                                                                                                          , False, CType(dgvEmpTimesheetList.DataSource, DataTable))

                Dim mstrProjectedTotalHrs As String = "00:00"
                Dim mstrRemainingBalance As String = "00:00"
                Dim mintRemainingBalance As Integer = 0
                Dim mblnOvertime As Boolean = False
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    mstrProjectedTotalHrs = dtTable.Rows(0)("Hours").ToString().ToString
                    mintRemainingBalance = CalculateSecondsFromHrs(dtTable.Rows(4)("Hours").ToString())
                    Dim mintCurrentBalance = CalculateSecondsFromHrs(dgvTimesheet.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString())

                    If CBool(dtTable.Rows(dtTable.Rows.Count - 1)("Hours")) Then
                        mintRemainingBalance = mintRemainingBalance + mintCurrentBalance
                    Else
                        If mintRemainingBalance < mintCurrentBalance Then
                            mblnOvertime = True
                        End If
                        mintRemainingBalance = mintCurrentBalance - mintRemainingBalance
                    End If
                    mstrRemainingBalance = CalculateTime(True, mintRemainingBalance).ToString().Replace(".", ":")
                End If
                objProjectDetail = Nothing
                If CBool(dtTable.Rows(dtTable.Rows.Count - 1)("Hours")) OrElse mblnOvertime Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Are you sure want to exceed total projected activity hrs,") & vbCrLf & Language.getMessage(mstrModuleName, 43, "Projected hours will be") & " " & mstrProjectedTotalHrs _
                                                 & " " & Language.getMessage(mstrModuleName, 44, "hours.") & vbCrLf & Language.getMessage(mstrModuleName, 45, "Acummulated Overtime hours will be") & " " & mstrRemainingBalance & " " & Language.getMessage(mstrModuleName, 44, "hours."), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Exclamation, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then

                        dgvTimesheet.CurrentCell = dgvTimesheet.Rows(RowIndex).Cells(dgcolhEmpHours.Index)
                        Exit Sub
                    End If
                End If

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            ElseIf e.ColumnIndex = dgcolhCostCenter.Index Then
                Dim RowIndex As Integer = dgvTimesheet.CurrentCell.RowIndex
                If dgvTimesheet.Rows(RowIndex).Cells(dgcolhCostCenter.Index).Visible = True AndAlso CInt(dgvTimesheet.Rows(RowIndex).Cells(dgcolhCostCenter.Index).Value) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Cost Center is compulsory information.Please select cost center for each activity."), enMsgBoxStyle.Information)
                    dgvTimesheet.CurrentCell = dgvTimesheet.Rows(RowIndex).Cells(dgcolhCostCenter.Index)
                    SendKeys.Send("{F4}")
                    Exit Sub
            End If
                'Pinkal (06-Jan-2023) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimesheet_CellValidated", mstrModuleName)
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- End

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
    Private Sub dgvTimesheet_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTimesheet.CellEnter
        Try
            If e.ColumnIndex = dgcolhCostCenter.Index Then
                SendKeys.Send("{F4}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimesheet_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTimesheet_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvTimesheet.DataError

    End Sub

    'Pinkal (06-Jan-2023) -- End

#End Region

#Region "MaskTextBox Events"

    'Private Sub txtHours_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        TextBoxTimeValidation(txtHours)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtHours_Validated", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region " Context Menu Events "

    Private Sub mnuViewPendingSubmitApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuViewPendingSubmitApproval.Click
        Try
            Dim frm As New frmSubmitForApproval

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Focus()
                Exit Sub
            End If

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'frm.displayDialog(CInt(cboPeriod.SelectedValue), True)
            frm.displayDialog(CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL)
            'Nilay (01 Apr 2017) -- End


            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                FillList()
            End If
            'Pinkal (13-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewPendingSubmitApproval_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuViewCompletedSubmitApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuViewCompletedSubmitApproval.Click
        Try
            Dim frm As New frmSubmitForApproval

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Focus()
                Exit Sub
            End If

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'frm.displayDialog(CInt(cboPeriod.SelectedValue), False)
            frm.displayDialog(CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED)
            'Nilay (01 Apr 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewCompletedSubmitApproval_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuGlobalCancelTimesheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGlobalCancelTimesheet.Click
        Try
            Dim frm As New frmSubmitForApproval

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Focus()
                Exit Sub
            End If

            frm.displayDialog(CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED)

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                FillList()
                FillEmpActivityList()
            End If
            'Pinkal (13-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalCancelTimesheet_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuGlobalDeleteTimesheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGlobalDeleteTimesheet.Click
        Try
            Dim frm As New frmSubmitForApproval

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Focus()
                Exit Sub
            End If

            frm.displayDialog(CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED)

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                FillList()
                FillEmpActivityList()
            End If
            'Pinkal (13-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalDeleteTimesheet_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "CheckBox's Events"
    'Nilay (27 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    'Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
    '    Try
    '        If mdtEmpTimesheet.Rows.Count > 0 Then
    '            For Each dRow As DataRow In mdtEmpTimesheet.Select("IsGroup=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND issubmit_approval=0")
    '                dRow.Item("IsChecked") = chkSelectAll.Checked
    '            Next
    '        End If
    '        mdtEmpTimesheet.AcceptChanges()
    '        Call SetGridColor()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Nilay (27 Feb 2017) -- End

    
#End Region





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbEmployeeTimesheet.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeTimesheet.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnSubmitForApproval.GradientBackColor = GUI._ButttonBackColor
            Me.btnSubmitForApproval.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbEmployeeTimesheet.Text = Language._Object.getCaption(Me.gbEmployeeTimesheet.Name, Me.gbEmployeeTimesheet.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
            Me.LblDate.Text = Language._Object.getCaption(Me.LblDate.Name, Me.LblDate.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnSubmitForApproval.Text = Language._Object.getCaption(Me.btnSubmitForApproval.Name, Me.btnSubmitForApproval.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuViewPendingSubmitApproval.Text = Language._Object.getCaption(Me.mnuViewPendingSubmitApproval.Name, Me.mnuViewPendingSubmitApproval.Text)
            Me.mnuViewCompletedSubmitApproval.Text = Language._Object.getCaption(Me.mnuViewCompletedSubmitApproval.Name, Me.mnuViewCompletedSubmitApproval.Text)
            Me.mnuGlobalDeleteTimesheet.Text = Language._Object.getCaption(Me.mnuGlobalDeleteTimesheet.Name, Me.mnuGlobalDeleteTimesheet.Text)
            Me.mnuGlobalCancelTimesheet.Text = Language._Object.getCaption(Me.mnuGlobalCancelTimesheet.Name, Me.mnuGlobalCancelTimesheet.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
			Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
			Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
			Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
			Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
			Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
			Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
			Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)
			Me.DataGridViewTextBoxColumn26.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn26.Name, Me.DataGridViewTextBoxColumn26.HeaderText)
			Me.DataGridViewTextBoxColumn27.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn27.Name, Me.DataGridViewTextBoxColumn27.HeaderText)
			Me.DataGridViewTextBoxColumn28.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn28.Name, Me.DataGridViewTextBoxColumn28.HeaderText)
			Me.DataGridViewTextBoxColumn29.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn29.Name, Me.DataGridViewTextBoxColumn29.HeaderText)
			Me.DataGridViewTextBoxColumn30.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn30.Name, Me.DataGridViewTextBoxColumn30.HeaderText)
			Me.DataGridViewTextBoxColumn31.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn31.Name, Me.DataGridViewTextBoxColumn31.HeaderText)
			Me.DataGridViewTextBoxColumn32.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn32.Name, Me.DataGridViewTextBoxColumn32.HeaderText)
			Me.DataGridViewTextBoxColumn33.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn33.Name, Me.DataGridViewTextBoxColumn33.HeaderText)
			Me.DataGridViewTextBoxColumn34.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn34.Name, Me.DataGridViewTextBoxColumn34.HeaderText)
			Me.DataGridViewTextBoxColumn35.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn35.Name, Me.DataGridViewTextBoxColumn35.HeaderText)
            Me.dgcolhEmpProject.HeaderText = Language._Object.getCaption(Me.dgcolhEmpProject.Name, Me.dgcolhEmpProject.HeaderText)
            Me.dgcolhEmpDonor.HeaderText = Language._Object.getCaption(Me.dgcolhEmpDonor.Name, Me.dgcolhEmpDonor.HeaderText)
            Me.dgcolhEmpActivity.HeaderText = Language._Object.getCaption(Me.dgcolhEmpActivity.Name, Me.dgcolhEmpActivity.HeaderText)
            Me.dgcolhEmpPercentage.HeaderText = Language._Object.getCaption(Me.dgcolhEmpPercentage.Name, Me.dgcolhEmpPercentage.HeaderText)
            Me.dgcolhCostCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenter.Name, Me.dgcolhCostCenter.HeaderText)
            Me.dgcolhEmpHours.HeaderText = Language._Object.getCaption(Me.dgcolhEmpHours.Name, Me.dgcolhEmpHours.HeaderText)
            Me.dgcolhEmpDescription.HeaderText = Language._Object.getCaption(Me.dgcolhEmpDescription.Name, Me.dgcolhEmpDescription.HeaderText)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
			Me.dgcolhDate.HeaderText = Language._Object.getCaption(Me.dgcolhDate.Name, Me.dgcolhDate.HeaderText)
			Me.dgcolhDonor.HeaderText = Language._Object.getCaption(Me.dgcolhDonor.Name, Me.dgcolhDonor.HeaderText)
			Me.dgcolhProject.HeaderText = Language._Object.getCaption(Me.dgcolhProject.Name, Me.dgcolhProject.HeaderText)
			Me.dgcolhActivity.HeaderText = Language._Object.getCaption(Me.dgcolhActivity.Name, Me.dgcolhActivity.HeaderText)
            Me.dgcolhCostCenterList.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenterList.Name, Me.dgcolhCostCenterList.HeaderText)
			Me.dgcolhHours.HeaderText = Language._Object.getCaption(Me.dgcolhHours.Name, Me.dgcolhHours.HeaderText)
			Me.dgcolhApprovedActHrs.HeaderText = Language._Object.getCaption(Me.dgcolhApprovedActHrs.Name, Me.dgcolhApprovedActHrs.HeaderText)
			Me.dgcolhDescription.HeaderText = Language._Object.getCaption(Me.dgcolhDescription.Name, Me.dgcolhDescription.HeaderText)
			Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsBudgetEmp_timesheet", 1, "This Activity is already defined of this employee for same date. Please define different activity for this date.")
            Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 3, "You cannot assign activity hours for future date.")
            Language.setMessage(mstrModuleName, 6, "Hours is compulsory information.Please enter activity hours.")
            Language.setMessage(mstrModuleName, 7, "Timesheet inserted Successfully.")
            Language.setMessage(mstrModuleName, 8, "Timesheet updated Successfully.")
            Language.setMessage(mstrModuleName, 9, "Timesheet deleted Successfully.")
            Language.setMessage(mstrModuleName, 10, "This employee does not allow to do this activity more than define activity percentage.")
            Language.setMessage(mstrModuleName, 11, "Are you sure you want to delete this employee timesheet?")
            Language.setMessage(mstrModuleName, 13, "Invalid working hrs.Reason:Working hrs cannot greater than or equal to 24.")
            Language.setMessage(mstrModuleName, 14, "You can't Edit this timesheet. Reason: This timesheet is already approved.")
            Language.setMessage(mstrModuleName, 15, "You can't Edit this timesheet. Reason: This timesheet is already rejected.")
            Language.setMessage(mstrModuleName, 16, "You can't delete this timesheet. Reason: This timesheet is already approved.")
            Language.setMessage(mstrModuleName, 17, "You can't delete this timesheet. Reason: This timesheet is already rejected.")
            Language.setMessage(mstrModuleName, 18, "You can't cancel this timesheet. Reason: This timesheet is in pending status.")
            Language.setMessage(mstrModuleName, 19, "You can't cancel this timesheet. Reason: This timesheet is already rejected.")
            Language.setMessage(mstrModuleName, 20, "Are you sure you want to cancel this employee timesheet?")
            Language.setMessage(mstrModuleName, 21, "Cancel Reason")
            Language.setMessage(mstrModuleName, 22, "You can't edit this timesheet. Reason: This timesheet is already approved by some of the approver(s) of this employee.")
            Language.setMessage(mstrModuleName, 23, "You can't delete this timesheet. Reason: This timesheet is already approved by some of the approver(s) of this employee.")
            Language.setMessage(mstrModuleName, 24, "Please Assign Approver to this employee and also map approver to system user.")
            Language.setMessage(mstrModuleName, 25, "Sorry,you cannot add activity for this employee for selected date. Reason : Holiday is assigned to this employee for this date.")
            Language.setMessage(mstrModuleName, 26, "Sorry,you cannot add activity for this employee for selected date. Reason : Day Off is assigned to this employee for this date.")
            Language.setMessage(mstrModuleName, 27, "Sorry,you cannot add activity for this employee for selected date. Reason : Weekend is assigned to this employee for this date.")
            Language.setMessage(mstrModuleName, 28, "Invalid Total working hrs.Reason:Total Working hrs cannot greater than or equal to 24.")
            Language.setMessage(mstrModuleName, 29, "Total Activity Hrs :")
            Language.setMessage(mstrModuleName, 30, "You can't Edit this timesheet. Reason: This timesheet is already submitted for approval.")
            Language.setMessage(mstrModuleName, 31, "You can't Delete this timesheet. Reason: This timesheet is already submitted for approval.")
            Language.setMessage(mstrModuleName, 32, "Total Activity Hours must be greater than 0. Please define hours for atleast one activity.")
            Language.setMessage(mstrModuleName, 35, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already issued on this date by this employee.")
            Language.setMessage(mstrModuleName, 36, "Description is a mandatory information for each activity. Please mention description for each activity.")
            Language.setMessage(mstrModuleName, 37, "There is no activity assign to this employee for this tenure.please assign activity to this employee.")
			Language.setMessage(mstrModuleName, 39, "Sorry,you cannot add activity for this employee. Reason : Not a Single Holiday is assigned to this employee for current financial year.")
			Language.setMessage(mstrModuleName, 40, "This Project Activity has only")
			Language.setMessage(mstrModuleName, 41, "Hours remaining.")
			Language.setMessage(mstrModuleName, 42, "Are you sure want to exceed total projected activity hrs,")
			Language.setMessage(mstrModuleName, 43, "Projected hours will be")
			Language.setMessage(mstrModuleName, 44, "hours.")
			Language.setMessage(mstrModuleName, 45, "Acummulated Overtime hours will be")
			Language.setMessage(mstrModuleName, 46, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already approved on this date by this employee.")
            Language.setMessage(mstrModuleName, 47, "Timesheet was not able to be inserted on the following dates:")
            Language.setMessage(mstrModuleName, 48, "Reason: Allocated hours are greater than shift hours.")
            Language.setMessage(mstrModuleName, 49, "Cost Center is compulsory information.Please select cost center for each activity.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class