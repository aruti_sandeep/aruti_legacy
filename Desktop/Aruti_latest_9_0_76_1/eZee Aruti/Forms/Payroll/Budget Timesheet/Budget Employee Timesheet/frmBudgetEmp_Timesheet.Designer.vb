﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetEmp_Timesheet
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBudgetEmp_Timesheet))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbEmployeeTimesheet = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.LblToDate = New System.Windows.Forms.Label
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuViewPendingSubmitApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewCompletedSubmitApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalCancelTimesheet = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalDeleteTimesheet = New System.Windows.Forms.ToolStripMenuItem
        Me.btnSubmitForApproval = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.pnlTimesheet = New System.Windows.Forms.Panel
        Me.dgvTimesheet = New System.Windows.Forms.DataGridView
        Me.objdgcolhShowDetails = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhEmpProject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpDonor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpActivity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpPercentage = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhCostCenter = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhEmpHours = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.dgcolhEmpDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpHourInMin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpAssignedActivityHrsInMin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpDonorID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpProjectunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpActivityunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpBudgetID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpemptimesheetunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.LblDate = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvEmpTimesheetList = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhCancel = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDonor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostCenterList = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprovedActHrs = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpTimesheetID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFundSourceID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProjectID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsHoliday = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsDayOFF = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsLeave = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsSubmitForApproval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApprovedActivityHoursInMin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityHoursInMin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCostCenterIdList = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbEmployeeTimesheet.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.pnlTimesheet.SuspendLayout()
        CType(Me.dgvTimesheet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEmpTimesheetList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.Panel1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(893, 540)
        Me.pnlMain.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbEmployeeTimesheet)
        Me.Panel1.Controls.Add(Me.chkSelectAll)
        Me.Panel1.Controls.Add(Me.dgvEmpTimesheetList)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(893, 540)
        Me.Panel1.TabIndex = 0
        '
        'gbEmployeeTimesheet
        '
        Me.gbEmployeeTimesheet.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeTimesheet.Checked = False
        Me.gbEmployeeTimesheet.CollapseAllExceptThis = False
        Me.gbEmployeeTimesheet.CollapsedHoverImage = Nothing
        Me.gbEmployeeTimesheet.CollapsedNormalImage = Nothing
        Me.gbEmployeeTimesheet.CollapsedPressedImage = Nothing
        Me.gbEmployeeTimesheet.CollapseOnLoad = False
        Me.gbEmployeeTimesheet.Controls.Add(Me.dtpToDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblToDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.btnOperations)
        Me.gbEmployeeTimesheet.Controls.Add(Me.btnSubmitForApproval)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbEmployeeTimesheet.Controls.Add(Me.pnlTimesheet)
        Me.gbEmployeeTimesheet.Controls.Add(Me.dtpFromDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.btnSave)
        Me.gbEmployeeTimesheet.Controls.Add(Me.btnEdit)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboPeriod)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblPeriod)
        Me.gbEmployeeTimesheet.Controls.Add(Me.btnClose)
        Me.gbEmployeeTimesheet.ExpandedHoverImage = Nothing
        Me.gbEmployeeTimesheet.ExpandedNormalImage = Nothing
        Me.gbEmployeeTimesheet.ExpandedPressedImage = Nothing
        Me.gbEmployeeTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeTimesheet.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeTimesheet.HeaderHeight = 30
        Me.gbEmployeeTimesheet.HeaderMessage = ""
        Me.gbEmployeeTimesheet.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeTimesheet.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeTimesheet.HeightOnCollapse = 0
        Me.gbEmployeeTimesheet.LeftTextSpace = 0
        Me.gbEmployeeTimesheet.Location = New System.Drawing.Point(2, 2)
        Me.gbEmployeeTimesheet.Name = "gbEmployeeTimesheet"
        Me.gbEmployeeTimesheet.OpenHeight = 300
        Me.gbEmployeeTimesheet.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeTimesheet.ShowBorder = True
        Me.gbEmployeeTimesheet.ShowCheckBox = False
        Me.gbEmployeeTimesheet.ShowCollapseButton = False
        Me.gbEmployeeTimesheet.ShowDefaultBorderColor = True
        Me.gbEmployeeTimesheet.ShowDownButton = False
        Me.gbEmployeeTimesheet.ShowHeader = True
        Me.gbEmployeeTimesheet.Size = New System.Drawing.Size(889, 238)
        Me.gbEmployeeTimesheet.TabIndex = 0
        Me.gbEmployeeTimesheet.Temp = 0
        Me.gbEmployeeTimesheet.Text = "Employee Timesheet Information"
        Me.gbEmployeeTimesheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(463, 34)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpToDate.TabIndex = 40
        '
        'LblToDate
        '
        Me.LblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToDate.Location = New System.Drawing.Point(406, 36)
        Me.LblToDate.Name = "LblToDate"
        Me.LblToDate.Size = New System.Drawing.Size(51, 16)
        Me.LblToDate.TabIndex = 39
        Me.LblToDate.Text = "To Date"
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(9, 202)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 37
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewPendingSubmitApproval, Me.mnuViewCompletedSubmitApproval, Me.mnuGlobalCancelTimesheet, Me.mnuGlobalDeleteTimesheet})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(274, 92)
        '
        'mnuViewPendingSubmitApproval
        '
        Me.mnuViewPendingSubmitApproval.Name = "mnuViewPendingSubmitApproval"
        Me.mnuViewPendingSubmitApproval.Size = New System.Drawing.Size(273, 22)
        Me.mnuViewPendingSubmitApproval.Tag = ""
        Me.mnuViewPendingSubmitApproval.Text = "View &Pending Submit For Approval"
        '
        'mnuViewCompletedSubmitApproval
        '
        Me.mnuViewCompletedSubmitApproval.Name = "mnuViewCompletedSubmitApproval"
        Me.mnuViewCompletedSubmitApproval.Size = New System.Drawing.Size(273, 22)
        Me.mnuViewCompletedSubmitApproval.Tag = ""
        Me.mnuViewCompletedSubmitApproval.Text = "&View Completed Submit For Approval"
        '
        'mnuGlobalCancelTimesheet
        '
        Me.mnuGlobalCancelTimesheet.Name = "mnuGlobalCancelTimesheet"
        Me.mnuGlobalCancelTimesheet.Size = New System.Drawing.Size(273, 22)
        Me.mnuGlobalCancelTimesheet.Text = "Global &Cancel Timesheet"
        '
        'mnuGlobalDeleteTimesheet
        '
        Me.mnuGlobalDeleteTimesheet.Name = "mnuGlobalDeleteTimesheet"
        Me.mnuGlobalDeleteTimesheet.Size = New System.Drawing.Size(273, 22)
        Me.mnuGlobalDeleteTimesheet.Text = "Global &Delete Timesheet"
        '
        'btnSubmitForApproval
        '
        Me.btnSubmitForApproval.BackColor = System.Drawing.Color.White
        Me.btnSubmitForApproval.BackgroundImage = CType(resources.GetObject("btnSubmitForApproval.BackgroundImage"), System.Drawing.Image)
        Me.btnSubmitForApproval.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSubmitForApproval.BorderColor = System.Drawing.Color.Empty
        Me.btnSubmitForApproval.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSubmitForApproval.FlatAppearance.BorderSize = 0
        Me.btnSubmitForApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmitForApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmitForApproval.ForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSubmitForApproval.GradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForApproval.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.Location = New System.Drawing.Point(123, 202)
        Me.btnSubmitForApproval.Name = "btnSubmitForApproval"
        Me.btnSubmitForApproval.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForApproval.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.Size = New System.Drawing.Size(132, 30)
        Me.btnSubmitForApproval.TabIndex = 35
        Me.btnSubmitForApproval.Text = "Submit For Approval"
        Me.btnSubmitForApproval.UseVisualStyleBackColor = True
        Me.btnSubmitForApproval.Visible = False
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchPeriod.Image = CType(resources.GetObject("objbtnSearchPeriod.Image"), System.Drawing.Image)
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(196, 35)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(18, 19)
        Me.objbtnSearchPeriod.TabIndex = 2
        '
        'pnlTimesheet
        '
        Me.pnlTimesheet.Controls.Add(Me.dgvTimesheet)
        Me.pnlTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlTimesheet.Location = New System.Drawing.Point(9, 61)
        Me.pnlTimesheet.Name = "pnlTimesheet"
        Me.pnlTimesheet.Size = New System.Drawing.Size(871, 133)
        Me.pnlTimesheet.TabIndex = 33
        '
        'dgvTimesheet
        '
        Me.dgvTimesheet.AllowUserToAddRows = False
        Me.dgvTimesheet.AllowUserToDeleteRows = False
        Me.dgvTimesheet.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvTimesheet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvTimesheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvTimesheet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhShowDetails, Me.dgcolhEmpProject, Me.dgcolhEmpDonor, Me.dgcolhEmpActivity, Me.dgcolhEmpPercentage, Me.dgcolhCostCenter, Me.dgcolhEmpHours, Me.dgcolhEmpDescription, Me.objdgcolhEmpHourInMin, Me.objdgcolhEmpAssignedActivityHrsInMin, Me.objdgcolhEmpPeriodunkid, Me.objdgcolhEmpDonorID, Me.objdgcolhEmpProjectunkid, Me.objdgcolhEmpActivityunkid, Me.objdgcolhEmpBudgetID, Me.objdgcolhEmpemptimesheetunkid})
        Me.dgvTimesheet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvTimesheet.Location = New System.Drawing.Point(0, 0)
        Me.dgvTimesheet.Name = "dgvTimesheet"
        Me.dgvTimesheet.RowHeadersVisible = False
        Me.dgvTimesheet.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        Me.dgvTimesheet.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTimesheet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTimesheet.Size = New System.Drawing.Size(871, 133)
        Me.dgvTimesheet.TabIndex = 0
        Me.dgvTimesheet.TabStop = False
        '
        'objdgcolhShowDetails
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.NullValue = Nothing
        Me.objdgcolhShowDetails.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhShowDetails.Frozen = True
        Me.objdgcolhShowDetails.HeaderText = ""
        Me.objdgcolhShowDetails.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objdgcolhShowDetails.Name = "objdgcolhShowDetails"
        Me.objdgcolhShowDetails.ReadOnly = True
        Me.objdgcolhShowDetails.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhShowDetails.ToolTipText = "Show Project Hour Details"
        Me.objdgcolhShowDetails.Width = 25
        '
        'dgcolhEmpProject
        '
        Me.dgcolhEmpProject.Frozen = True
        Me.dgcolhEmpProject.HeaderText = "Project"
        Me.dgcolhEmpProject.Name = "dgcolhEmpProject"
        Me.dgcolhEmpProject.ReadOnly = True
        Me.dgcolhEmpProject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpProject.Width = 175
        '
        'dgcolhEmpDonor
        '
        Me.dgcolhEmpDonor.HeaderText = "Donor/Grant"
        Me.dgcolhEmpDonor.Name = "dgcolhEmpDonor"
        Me.dgcolhEmpDonor.ReadOnly = True
        Me.dgcolhEmpDonor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpDonor.Width = 175
        '
        'dgcolhEmpActivity
        '
        Me.dgcolhEmpActivity.HeaderText = "Activity"
        Me.dgcolhEmpActivity.Name = "dgcolhEmpActivity"
        Me.dgcolhEmpActivity.ReadOnly = True
        Me.dgcolhEmpActivity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpActivity.Width = 175
        '
        'dgcolhEmpPercentage
        '
        Me.dgcolhEmpPercentage.DecimalLength = 2
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhEmpPercentage.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhEmpPercentage.HeaderText = "Assigned Activity (%)"
        Me.dgcolhEmpPercentage.Name = "dgcolhEmpPercentage"
        Me.dgcolhEmpPercentage.ReadOnly = True
        Me.dgcolhEmpPercentage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhEmpPercentage.ShowNullWhenZero = True
        Me.dgcolhEmpPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpPercentage.Width = 125
        '
        'dgcolhCostCenter
        '
        Me.dgcolhCostCenter.HeaderText = "Cost Center"
        Me.dgcolhCostCenter.Name = "dgcolhCostCenter"
        Me.dgcolhCostCenter.Visible = False
        Me.dgcolhCostCenter.Width = 150
        '
        'dgcolhEmpHours
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "00:00"
        Me.dgcolhEmpHours.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhEmpHours.HeaderText = "Hours"
        Me.dgcolhEmpHours.Mask = "00:00"
        Me.dgcolhEmpHours.Name = "dgcolhEmpHours"
        '
        'dgcolhEmpDescription
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhEmpDescription.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhEmpDescription.HeaderText = "Description"
        Me.dgcolhEmpDescription.Name = "dgcolhEmpDescription"
        Me.dgcolhEmpDescription.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEmpDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpDescription.Width = 250
        '
        'objdgcolhEmpHourInMin
        '
        Me.objdgcolhEmpHourInMin.HeaderText = "HourInMin"
        Me.objdgcolhEmpHourInMin.Name = "objdgcolhEmpHourInMin"
        Me.objdgcolhEmpHourInMin.ReadOnly = True
        Me.objdgcolhEmpHourInMin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpHourInMin.Visible = False
        '
        'objdgcolhEmpAssignedActivityHrsInMin
        '
        Me.objdgcolhEmpAssignedActivityHrsInMin.HeaderText = "AssignedActivityHrsInMin"
        Me.objdgcolhEmpAssignedActivityHrsInMin.Name = "objdgcolhEmpAssignedActivityHrsInMin"
        Me.objdgcolhEmpAssignedActivityHrsInMin.ReadOnly = True
        Me.objdgcolhEmpAssignedActivityHrsInMin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpAssignedActivityHrsInMin.Visible = False
        '
        'objdgcolhEmpPeriodunkid
        '
        Me.objdgcolhEmpPeriodunkid.HeaderText = "Periodunkid"
        Me.objdgcolhEmpPeriodunkid.Name = "objdgcolhEmpPeriodunkid"
        Me.objdgcolhEmpPeriodunkid.ReadOnly = True
        Me.objdgcolhEmpPeriodunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpPeriodunkid.Visible = False
        '
        'objdgcolhEmpDonorID
        '
        Me.objdgcolhEmpDonorID.HeaderText = "DonorID"
        Me.objdgcolhEmpDonorID.Name = "objdgcolhEmpDonorID"
        Me.objdgcolhEmpDonorID.ReadOnly = True
        Me.objdgcolhEmpDonorID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpDonorID.Visible = False
        '
        'objdgcolhEmpProjectunkid
        '
        Me.objdgcolhEmpProjectunkid.HeaderText = "ProjectID"
        Me.objdgcolhEmpProjectunkid.Name = "objdgcolhEmpProjectunkid"
        Me.objdgcolhEmpProjectunkid.ReadOnly = True
        Me.objdgcolhEmpProjectunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpProjectunkid.Visible = False
        '
        'objdgcolhEmpActivityunkid
        '
        Me.objdgcolhEmpActivityunkid.HeaderText = "ActivityID"
        Me.objdgcolhEmpActivityunkid.Name = "objdgcolhEmpActivityunkid"
        Me.objdgcolhEmpActivityunkid.ReadOnly = True
        Me.objdgcolhEmpActivityunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpActivityunkid.Visible = False
        '
        'objdgcolhEmpBudgetID
        '
        Me.objdgcolhEmpBudgetID.HeaderText = "BudgetID"
        Me.objdgcolhEmpBudgetID.Name = "objdgcolhEmpBudgetID"
        Me.objdgcolhEmpBudgetID.ReadOnly = True
        Me.objdgcolhEmpBudgetID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpBudgetID.Visible = False
        '
        'objdgcolhEmpemptimesheetunkid
        '
        Me.objdgcolhEmpemptimesheetunkid.HeaderText = "Empemptimesheetunkid"
        Me.objdgcolhEmpemptimesheetunkid.Name = "objdgcolhEmpemptimesheetunkid"
        Me.objdgcolhEmpemptimesheetunkid.ReadOnly = True
        Me.objdgcolhEmpemptimesheetunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpemptimesheetunkid.Visible = False
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(292, 34)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpFromDate.TabIndex = 4
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(866, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 7
        '
        'LblDate
        '
        Me.LblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDate.Location = New System.Drawing.Point(225, 36)
        Me.LblDate.Name = "LblDate"
        Me.LblDate.Size = New System.Drawing.Size(63, 16)
        Me.LblDate.TabIndex = 3
        Me.LblDate.Text = "From Date"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(636, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(224, 21)
        Me.cboEmployee.TabIndex = 6
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(694, 200)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(694, 200)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 9
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'LblEmployee
        '
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(570, 36)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(62, 16)
        Me.LblEmployee.TabIndex = 5
        Me.LblEmployee.Text = "Employee"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 400
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(66, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(124, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'LblPeriod
        '
        Me.LblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(7, 36)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(55, 16)
        Me.LblPeriod.TabIndex = 0
        Me.LblPeriod.Text = "Period"
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(790, 200)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(7, 249)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 2
        Me.chkSelectAll.UseVisualStyleBackColor = True
        Me.chkSelectAll.Visible = False
        '
        'dgvEmpTimesheetList
        '
        Me.dgvEmpTimesheetList.AllowUserToAddRows = False
        Me.dgvEmpTimesheetList.AllowUserToDeleteRows = False
        Me.dgvEmpTimesheetList.AllowUserToResizeColumns = False
        Me.dgvEmpTimesheetList.AllowUserToResizeRows = False
        Me.dgvEmpTimesheetList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmpTimesheetList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEmpTimesheetList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmpTimesheetList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhCancel, Me.dgcolhEmployee, Me.dgcolhDate, Me.dgcolhDonor, Me.dgcolhProject, Me.dgcolhActivity, Me.dgcolhCostCenterList, Me.dgcolhHours, Me.dgcolhApprovedActHrs, Me.dgcolhDescription, Me.dgcolhStatus, Me.objdgcolhEmpTimesheetID, Me.objdgcolhPeriodID, Me.objdgcolhEmployeeID, Me.objdgcolhFundSourceID, Me.objdgcolhProjectID, Me.objdgcolhActivityID, Me.objdgcolhStatusId, Me.objdgcolhIsGrp, Me.objdgcolhIsHoliday, Me.objdgcolhIsDayOFF, Me.objdgcolhIsLeave, Me.objdgcolhIsSubmitForApproval, Me.objdgcolhApprovedActivityHoursInMin, Me.objdgcolhActivityHoursInMin, Me.objdgcolhCostCenterIdList})
        Me.dgvEmpTimesheetList.Location = New System.Drawing.Point(1, 243)
        Me.dgvEmpTimesheetList.Name = "dgvEmpTimesheetList"
        Me.dgvEmpTimesheetList.ReadOnly = True
        Me.dgvEmpTimesheetList.RowHeadersVisible = False
        Me.dgvEmpTimesheetList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmpTimesheetList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpTimesheetList.Size = New System.Drawing.Size(890, 294)
        Me.dgvEmpTimesheetList.TabIndex = 1
        Me.dgvEmpTimesheetList.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Donor/Grant"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 175
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Project"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 175
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Activity"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 175
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn4.HeaderText = "HourInMin"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "AssignedActivityHrsInMin"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Periodunkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "DonorID"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "ProjectID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "ActivityID"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Percentage"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "BudgetID"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Empemptimesheetunkid"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.Frozen = True
        Me.DataGridViewTextBoxColumn13.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        Me.DataGridViewTextBoxColumn13.Width = 200
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.Frozen = True
        Me.DataGridViewTextBoxColumn14.HeaderText = "Activity Date"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Width = 200
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.Frozen = True
        Me.DataGridViewTextBoxColumn15.HeaderText = "Donor/Grant"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Width = 150
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Project Code"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Width = 150
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Activity Code"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Width = 150
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Activity Hours"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Width = 150
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Approved Activity Hrs"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Width = 130
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn20.Width = 250
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn21.Width = 125
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "EmpTimesheetID"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn22.Visible = False
        Me.DataGridViewTextBoxColumn22.Width = 125
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "EmployeeID"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "FundSourceID"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "ProjectID"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn26.Visible = False
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "ActivityID"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn27.Visible = False
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Statusunkid"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn28.Visible = False
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "IsGrp"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn29.Visible = False
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "IsHoliday"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "IsDayOFF"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "IsLeave"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        Me.DataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn32.Visible = False
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "IsSubmitForApproval"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "ApprovedActivityHoursInMin"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "ApprovedActivityHoursInMin"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.ReadOnly = True
        Me.DataGridViewTextBoxColumn35.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn35.Visible = False
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.ToolTipText = "Edit Timesheet"
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.ToolTipText = "Delete Timesheet"
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhCancel
        '
        Me.objdgcolhCancel.Frozen = True
        Me.objdgcolhCancel.HeaderText = ""
        Me.objdgcolhCancel.Image = Global.Aruti.Main.My.Resources.Resources.cancel
        Me.objdgcolhCancel.Name = "objdgcolhCancel"
        Me.objdgcolhCancel.ReadOnly = True
        Me.objdgcolhCancel.ToolTipText = "Cancel Timesheet"
        Me.objdgcolhCancel.Width = 25
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.Frozen = True
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 200
        '
        'dgcolhDate
        '
        Me.dgcolhDate.Frozen = True
        Me.dgcolhDate.HeaderText = "Activity Date"
        Me.dgcolhDate.Name = "dgcolhDate"
        Me.dgcolhDate.ReadOnly = True
        Me.dgcolhDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDonor
        '
        Me.dgcolhDonor.HeaderText = "Donor/Grant"
        Me.dgcolhDonor.Name = "dgcolhDonor"
        Me.dgcolhDonor.ReadOnly = True
        Me.dgcolhDonor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDonor.Width = 150
        '
        'dgcolhProject
        '
        Me.dgcolhProject.HeaderText = "Project Code"
        Me.dgcolhProject.Name = "dgcolhProject"
        Me.dgcolhProject.ReadOnly = True
        Me.dgcolhProject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProject.Width = 150
        '
        'dgcolhActivity
        '
        Me.dgcolhActivity.HeaderText = "Activity Code"
        Me.dgcolhActivity.Name = "dgcolhActivity"
        Me.dgcolhActivity.ReadOnly = True
        Me.dgcolhActivity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhActivity.Width = 150
        '
        'dgcolhCostCenterList
        '
        Me.dgcolhCostCenterList.HeaderText = "Cost Center"
        Me.dgcolhCostCenterList.Name = "dgcolhCostCenterList"
        Me.dgcolhCostCenterList.ReadOnly = True
        Me.dgcolhCostCenterList.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCostCenterList.Visible = False
        Me.dgcolhCostCenterList.Width = 150
        '
        'dgcolhHours
        '
        Me.dgcolhHours.HeaderText = "Activity Hours"
        Me.dgcolhHours.Name = "dgcolhHours"
        Me.dgcolhHours.ReadOnly = True
        Me.dgcolhHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhApprovedActHrs
        '
        Me.dgcolhApprovedActHrs.HeaderText = "Approved Activity Hrs"
        Me.dgcolhApprovedActHrs.Name = "dgcolhApprovedActHrs"
        Me.dgcolhApprovedActHrs.ReadOnly = True
        Me.dgcolhApprovedActHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprovedActHrs.Width = 130
        '
        'dgcolhDescription
        '
        Me.dgcolhDescription.HeaderText = "Description"
        Me.dgcolhDescription.Name = "dgcolhDescription"
        Me.dgcolhDescription.ReadOnly = True
        Me.dgcolhDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDescription.Width = 250
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStatus.Width = 125
        '
        'objdgcolhEmpTimesheetID
        '
        Me.objdgcolhEmpTimesheetID.HeaderText = "EmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.Name = "objdgcolhEmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.ReadOnly = True
        Me.objdgcolhEmpTimesheetID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpTimesheetID.Visible = False
        '
        'objdgcolhPeriodID
        '
        Me.objdgcolhPeriodID.HeaderText = "PeriodID"
        Me.objdgcolhPeriodID.Name = "objdgcolhPeriodID"
        Me.objdgcolhPeriodID.ReadOnly = True
        Me.objdgcolhPeriodID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhPeriodID.Visible = False
        '
        'objdgcolhEmployeeID
        '
        Me.objdgcolhEmployeeID.HeaderText = "EmployeeID"
        Me.objdgcolhEmployeeID.Name = "objdgcolhEmployeeID"
        Me.objdgcolhEmployeeID.ReadOnly = True
        Me.objdgcolhEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeID.Visible = False
        '
        'objdgcolhFundSourceID
        '
        Me.objdgcolhFundSourceID.HeaderText = "FundSourceID"
        Me.objdgcolhFundSourceID.Name = "objdgcolhFundSourceID"
        Me.objdgcolhFundSourceID.ReadOnly = True
        Me.objdgcolhFundSourceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhFundSourceID.Visible = False
        '
        'objdgcolhProjectID
        '
        Me.objdgcolhProjectID.HeaderText = "ProjectID"
        Me.objdgcolhProjectID.Name = "objdgcolhProjectID"
        Me.objdgcolhProjectID.ReadOnly = True
        Me.objdgcolhProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhProjectID.Visible = False
        '
        'objdgcolhActivityID
        '
        Me.objdgcolhActivityID.HeaderText = "ActivityID"
        Me.objdgcolhActivityID.Name = "objdgcolhActivityID"
        Me.objdgcolhActivityID.ReadOnly = True
        Me.objdgcolhActivityID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityID.Visible = False
        '
        'objdgcolhStatusId
        '
        Me.objdgcolhStatusId.HeaderText = "Statusunkid"
        Me.objdgcolhStatusId.Name = "objdgcolhStatusId"
        Me.objdgcolhStatusId.ReadOnly = True
        Me.objdgcolhStatusId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhStatusId.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "IsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhIsHoliday
        '
        Me.objdgcolhIsHoliday.HeaderText = "IsHoliday"
        Me.objdgcolhIsHoliday.Name = "objdgcolhIsHoliday"
        Me.objdgcolhIsHoliday.ReadOnly = True
        Me.objdgcolhIsHoliday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsHoliday.Visible = False
        '
        'objdgcolhIsDayOFF
        '
        Me.objdgcolhIsDayOFF.HeaderText = "IsDayOFF"
        Me.objdgcolhIsDayOFF.Name = "objdgcolhIsDayOFF"
        Me.objdgcolhIsDayOFF.ReadOnly = True
        Me.objdgcolhIsDayOFF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsDayOFF.Visible = False
        '
        'objdgcolhIsLeave
        '
        Me.objdgcolhIsLeave.HeaderText = "IsLeave"
        Me.objdgcolhIsLeave.Name = "objdgcolhIsLeave"
        Me.objdgcolhIsLeave.ReadOnly = True
        Me.objdgcolhIsLeave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsLeave.Visible = False
        '
        'objdgcolhIsSubmitForApproval
        '
        Me.objdgcolhIsSubmitForApproval.HeaderText = "IsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.Name = "objdgcolhIsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.ReadOnly = True
        Me.objdgcolhIsSubmitForApproval.Visible = False
        '
        'objdgcolhApprovedActivityHoursInMin
        '
        Me.objdgcolhApprovedActivityHoursInMin.HeaderText = "ApprovedActivityHoursInMin"
        Me.objdgcolhApprovedActivityHoursInMin.Name = "objdgcolhApprovedActivityHoursInMin"
        Me.objdgcolhApprovedActivityHoursInMin.ReadOnly = True
        Me.objdgcolhApprovedActivityHoursInMin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApprovedActivityHoursInMin.Visible = False
        '
        'objdgcolhActivityHoursInMin
        '
        Me.objdgcolhActivityHoursInMin.HeaderText = "ActivityHoursInMin"
        Me.objdgcolhActivityHoursInMin.Name = "objdgcolhActivityHoursInMin"
        Me.objdgcolhActivityHoursInMin.ReadOnly = True
        Me.objdgcolhActivityHoursInMin.Visible = False
        '
        'objdgcolhCostCenterIdList
        '
        Me.objdgcolhCostCenterIdList.HeaderText = "CostCenterId"
        Me.objdgcolhCostCenterIdList.Name = "objdgcolhCostCenterIdList"
        Me.objdgcolhCostCenterIdList.ReadOnly = True
        Me.objdgcolhCostCenterIdList.Visible = False
        '
        'frmBudgetEmp_Timesheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(893, 540)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBudgetEmp_Timesheet"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Timesheet"
        Me.pnlMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbEmployeeTimesheet.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.pnlTimesheet.ResumeLayout(False)
        CType(Me.dgvTimesheet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEmpTimesheetList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents dgvEmpTimesheetList As System.Windows.Forms.DataGridView
    Friend WithEvents gbEmployeeTimesheet As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents LblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents LblEmployee As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblDate As System.Windows.Forms.Label
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents dgvTimesheet As System.Windows.Forms.DataGridView
    Friend WithEvents pnlTimesheet As System.Windows.Forms.Panel
    Friend WithEvents btnSubmitForApproval As eZee.Common.eZeeLightButton
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuViewPendingSubmitApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewCompletedSubmitApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalDeleteTimesheet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalCancelTimesheet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhShowDetails As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhEmpProject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpDonor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpActivity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpPercentage As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhCostCenter As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhEmpHours As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents dgcolhEmpDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpHourInMin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpAssignedActivityHrsInMin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpDonorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpProjectunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpActivityunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpBudgetID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpemptimesheetunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhCancel As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDonor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostCenterList As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprovedActHrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpTimesheetID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFundSourceID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProjectID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsHoliday As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsDayOFF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsLeave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsSubmitForApproval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApprovedActivityHoursInMin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityHoursInMin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCostCenterIdList As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
