﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 6

Public Class frmVendor_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmVendor_AddEdit"
    Private mblnCancel As Boolean = True
    Private objVendorMaster As clsvendor_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintVendorMasterUnkid As Integer = -1
    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintVendorMasterUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintVendorMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmVendor_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objVendorMaster = New clsvendor_master
        Try
            Call Set_Logo(Me, gApplicationType)
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objVendorMaster._Vendorunkid = mintVendorMasterUnkid
                'Sohail (11 Sep 2010) -- Start
                cboGroup.Enabled = False
                objbtnAddGroup.Enabled = False
                'Sohail (11 Sep 2010) -- End
            End If
            FillPayrollGroup()
            FillCountry()
            GetValue()
            cboGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendor_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVendor_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendor_AddEdit_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVendor_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendor_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVendor_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'Sohail (11 Sep 2010) -- Start
        Try
        objVendorMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendor_AddEdit_FormClosed", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCity.SelectedIndexChanged
        Try
            FillZipcode()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'S.SANDEEP [ 11 MAR 2014 ] -- START
        'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
        Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
        'S.SANDEEP [ 11 MAR 2014 ] -- END

        Try
            If CInt(cboGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Group is compulsory information.Please Select Group."), enMsgBoxStyle.Information)
                cboGroup.Select()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Vendor Code cannot be blank. Vendor Code is required information."), enMsgBoxStyle.Information)
                txtCode.Select()
                Exit Sub
            ElseIf Trim(txtCompany.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Company cannot be blank. Company is required information."), enMsgBoxStyle.Information)
                txtCompany.Select()
                Exit Sub
                'Sohail (11 Sep 2010) -- Start
                'Reason : As per Feeddback
                'ElseIf Trim(txtFirstName.Text) = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "First Name cannot be blank. First Name is required information."), enMsgBoxStyle.Information)
                '    txtFirstName.Select()
                '    Exit Sub
                'ElseIf Trim(txtLastname.Text) = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Last Name cannot be blank. Last Name is required information."), enMsgBoxStyle.Information)
                '    txtLastname.Select()
                '    Exit Sub
                'Sohail (11 Sep 2010) -- End
            ElseIf txtwebsite.Text.Trim.Length > 0 Then
                'If Expression.IsMatch(txtwebsite.Text.Trim) = False Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invalid Website.Please Enter Valid Website"), enMsgBoxStyle.Information)
                '    txtwebsite.Focus()
                '    Exit Sub
                'End If
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objVendorMaster.Update()
            Else
                blnFlag = objVendorMaster.Insert()
            End If

            If blnFlag = False And objVendorMaster._Message <> "" Then
                eZeeMsgBox.Show(objVendorMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objVendorMaster = Nothing
                    objVendorMaster = New clsvendor_master
                    Call GetValue()
                    cboCountry.Select()
                Else
                    mintVendorMasterUnkid = objVendorMaster._Vendorunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim ObjPayrollGroups_AddEdit As New frmPayrollGroups_AddEdit
            ObjPayrollGroups_AddEdit.lblwebsite.Visible = False
            ObjPayrollGroups_AddEdit.txtwebsite.Visible = False
            ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(374, 180)
            ObjPayrollGroups_AddEdit.Size = New Size(403, 279)
            If ObjPayrollGroups_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, PayrollGroupType.Vendor) Then
                FillPayrollGroup()
                cboGroup.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Sohail (11 Sep 2010) -- Start
        Try
        Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub


#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboGroup.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorOptional
            txtCompany.BackColor = GUI.ColorComp
            txtFirstName.BackColor = GUI.ColorComp
            txtLastname.BackColor = GUI.ColorComp
            txtaddress1.BackColor = GUI.ColorOptional
            txtaddress2.BackColor = GUI.ColorOptional
            cboCountry.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboCity.BackColor = GUI.ColorOptional
            cboZipcode.BackColor = GUI.ColorOptional
            txtcontactno.BackColor = GUI.ColorOptional
            txtcontactperson.BackColor = GUI.ColorOptional
            txtwebsite.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboGroup.SelectedValue = objVendorMaster._Vendorgroupunkid
            txtCode.Text = objVendorMaster._Vendorcode
            txtCompany.Text = objVendorMaster._Companyname
            txtFirstName.Text = objVendorMaster._Firstname
            txtLastname.Text = objVendorMaster._Lastname
            txtaddress1.Text = objVendorMaster._Address1
            txtaddress2.Text = objVendorMaster._Address2
            cboCountry.SelectedValue = objVendorMaster._Countryunkid
            cboState.SelectedValue = objVendorMaster._Stateunkid
            cboCity.SelectedValue = objVendorMaster._Cityunkid
            cboZipcode.SelectedValue = objVendorMaster._Pincodeunkid
            txtcontactno.Text = objVendorMaster._Contactno
            txtcontactperson.Text = objVendorMaster._Contactperson
            txtwebsite.Text = objVendorMaster._Website
            chkBenefitProvider.Checked = objVendorMaster._Isbenefitprovider
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objVendorMaster._Vendorgroupunkid = CInt(cboGroup.SelectedValue)
            objVendorMaster._Vendorcode = txtCode.Text.Trim
            objVendorMaster._Companyname = txtCompany.Text.Trim
            objVendorMaster._Firstname = txtFirstName.Text.Trim
            objVendorMaster._Lastname = txtLastname.Text.Trim
            objVendorMaster._Address1 = txtaddress1.Text.Trim
            objVendorMaster._Address2 = txtaddress2.Text.Trim
            objVendorMaster._Countryunkid = CInt(cboCountry.SelectedValue)
            objVendorMaster._Stateunkid = CInt(cboState.SelectedValue)
            objVendorMaster._Cityunkid = CInt(cboCity.SelectedValue)
            objVendorMaster._Pincodeunkid = CInt(cboZipcode.SelectedValue)
            objVendorMaster._Contactno =txtcontactno.Text.Trim 
            objVendorMaster._Contactperson = txtcontactperson.Text.Trim
            objVendorMaster._Website = txtwebsite.Text.Trim
            objVendorMaster._Isbenefitprovider = chkBenefitProvider.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillPayrollGroup()
        Try
            Dim objPayollGroup As New clspayrollgroup_master
            Dim dsGroup As DataSet = objPayollGroup.getListForCombo(CInt(PayrollGroupType.Vendor), "Group", True)
            cboGroup.DisplayMember = "name"
            cboGroup.ValueMember = "groupmasterunkid"
            cboGroup.DataSource = dsGroup.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPayrollGroup", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCountry()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCountry", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Dim dsState As DataSet = Nothing
        Try
            Dim objStatemaster As New clsstate_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStatemaster.GetList("State", True, True)
            End If
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCity()
        Dim dsCity As DataSet = Nothing
        Try
            Dim objCitymaster As New clscity_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsCity = objCitymaster.GetList("City", True, False, CInt(cboState.SelectedValue))
            Else
                dsCity = objCitymaster.GetList("City", True, True)
            End If
            cboCity.ValueMember = "cityunkid"
            cboCity.DisplayMember = "name"
            cboCity.DataSource = dsCity.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCity", mstrModuleName)
        End Try
    End Sub

    Private Sub FillZipcode()
        Dim dsZipcode As DataSet = Nothing
        Try
            Dim objZipcodemaster As New clszipcode_master
            If CInt(cboCity.SelectedValue) > 0 Then
                dsZipcode = objZipcodemaster.GetList("ZipCode", True, False, CInt(cboCity.SelectedValue))
            Else
                dsZipcode = objZipcodemaster.GetList("ZipCode", True, True)
            End If
            cboZipcode.ValueMember = "zipcodeunkid"
            cboZipcode.DisplayMember = "zipcode_no"
            cboZipcode.DataSource = dsZipcode.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillZipcode", mstrModuleName)
        End Try
    End Sub


#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbVendorInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbVendorInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbVendorInformation.Text = Language._Object.getCaption(Me.gbVendorInformation.Name, Me.gbVendorInformation.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblLastname.Text = Language._Object.getCaption(Me.lblLastname.Name, Me.lblLastname.Text)
			Me.lblwebsite.Text = Language._Object.getCaption(Me.lblwebsite.Name, Me.lblwebsite.Text)
			Me.lblcontactperson.Text = Language._Object.getCaption(Me.lblcontactperson.Name, Me.lblcontactperson.Text)
			Me.lblContactNo.Text = Language._Object.getCaption(Me.lblContactNo.Name, Me.lblContactNo.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.chkBenefitProvider.Text = Language._Object.getCaption(Me.chkBenefitProvider.Name, Me.chkBenefitProvider.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblPostal.Text = Language._Object.getCaption(Me.lblPostal.Name, Me.lblPostal.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Group is compulsory information.Please Select Group.")
			Language.setMessage(mstrModuleName, 2, "Vendor Code cannot be blank. Vendor Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Company cannot be blank. Company is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class