﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeBankList

#Region " Private Varaibles "
    Private objEmpBankTran As clsEmployeeBanks
    Private ReadOnly mstrModuleName As String = "frmEmployeeBankList"
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    'Sohail (25 Apr 2014) -- End
    Private mdicModes As New Dictionary(Of Integer, String) 'Sohail (27 May 2014)
#End Region

#Region " Private Functions "

    'Sohail (16 Oct 2010) -- Start
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboBankGroup.BackColor = GUI.ColorOptional
            cboBankBranch.BackColor = GUI.ColorOptional
            cboAccType.BackColor = GUI.ColorOptional
            txtAccNo.BackColor = GUI.ColorOptional
            cboPeriod.BackColor = GUI.ColorComp 'Sohail (25 Apr 2014)
            cboMode.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Oct 2010) -- End

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewEmpBankList = True Then                'Pinkal (02-Jul-2012) -- Start

                'dsList = objEmpBankTran.GetList("EmployeeBank") 'Sohail (25 Apr 2014)

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND premployee_bank_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboBankGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND cfpayrollgroup_master.groupmasterunkid = " & CInt(cboBankGroup.SelectedValue) & " "
                End If

                If CInt(cboBankBranch.SelectedValue) > 0 Then
                    StrSearching &= "AND premployee_bank_tran.branchunkid = " & CInt(cboBankBranch.SelectedValue) & " "
                End If

                If CInt(cboAccType.SelectedValue) > 0 Then
                    StrSearching &= "AND premployee_bank_tran.accounttypeunkid = " & CInt(cboAccType.SelectedValue) & " "
                End If

                If txtAccNo.Text.Trim.Length > 0 Then
                    StrSearching &= "AND premployee_bank_tran.accountno LIKE '%" & txtAccNo.Text & "%'"
                End If

                'Sohail (27 May 2014) -- Start
                'Enhancement - TANAPA 
                If CInt(cboMode.SelectedValue) > 0 Then
                    StrSearching &= "AND premployee_bank_tran.modeid = " & CInt(cboMode.SelectedValue) & " "
                End If
                'Sohail (27 May 2014) -- End

                'Anjan (21 Nov 2011)-Start
                'ENHANCEMENT : TRA COMMENTS
                'Sohail (12 Oct 2021) -- Start
                'NMB Issue :  : Performance issue on process payroll.
                'If mstrAdvanceFilter.Length > 0 Then
                '    StrSearching &= "AND " & mstrAdvanceFilter
                'End If
                'Sohail (12 Oct 2021) -- End
                'Anjan (21 Nov 2011)-End 

                'Sohail (16 Oct 2010) -- Start
                If StrSearching.Trim.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    '    dtTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables(0)
                End If
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                'dtTable = New DataView(dsList.Tables(0), StrSearching, "BankGrp", DataViewRowState.CurrentRows).ToTable
                'Sohail (27 May 2014) -- Start
                'Enhancement - TANAPA 
                'dsList = objEmpBankTran.GetList("EmployeeBank", , mdtPeriod_enddate, StrSearching, "BankGrp, BranchName, end_date", CInt(cboPeriod.SelectedValue))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If chkGroupByEmpName.Checked = False Then
                '    dsList = objEmpBankTran.GetList("EmployeeBank", , mdtPeriod_enddate, StrSearching, "BankGrp, BranchName, EmpName, end_date DESC", CInt(cboPeriod.SelectedValue))
                'Else
                '    dsList = objEmpBankTran.GetList("EmployeeBank", , mdtPeriod_enddate, StrSearching, "EmpName, end_date DESC, BankGrp, BranchName", CInt(cboPeriod.SelectedValue))
                'End If
                Dim xPeriodStart As Date
                Dim xPeriodEnd As Date
                If CInt(cboPeriod.SelectedValue) > 0 Then
                    xPeriodStart = mdtPeriod_startdate
                    xPeriodEnd = mdtPeriod_enddate
                Else
                    xPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    xPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                End If
                If chkGroupByEmpName.Checked = False Then
                    'Sohail (12 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , StrSearching, , mdtPeriod_enddate, "BankGrp, BranchName, EmpName, end_date DESC")
                    dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , mstrAdvanceFilter, , mdtPeriod_enddate, "BankGrp, BranchName, EmpName, end_date DESC", StrSearching)
                    'Sohail (12 Oct 2021) -- End
                Else
                    'Sohail (12 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , StrSearching, , mdtPeriod_enddate, "EmpName, end_date DESC, BankGrp, BranchName")
                    dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , mstrAdvanceFilter, , mdtPeriod_enddate, "EmpName, end_date DESC, BankGrp, BranchName", StrSearching)
                    'Sohail (12 Oct 2021) -- End
                End If
                'Sohail (21 Aug 2015) -- End
                'Sohail (27 May 2014) -- End
                dtTable = New DataView(dsList.Tables(0)).ToTable
                'Sohail (25 Apr 2014) -- End

                'Sohail (16 Oct 2010) -- End

                Dim lvItem As ListViewItem

                lvEmpBankList.Items.Clear()
                'Sohail (16 Oct 2010) -- Start
                Dim lvArray As New List(Of ListViewItem)
                lvEmpBankList.BeginUpdate()
                'Sohail (16 Oct 2010) -- End

                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    'Sohail (16 Oct 2010) -- Start
                    'lvItem.Text = dtRow.Item("BankGrp").ToString
                    'lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)

                    'Sohail (28 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'lvItem.Text = dtRow.Item("EmpName").ToString
                    lvItem.Text = dtRow.Item("employeecode").ToString
                    lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                    'Sohail (28 Jan 2012) -- End
                    'Hemant (07 July 2023) -- Start
                    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                    If dtRow.Item("BankGrp").ToString.Trim.Length <= 0 Then
                        lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 4, "Mobile Money"))
                    Else
                        'Hemant (07 July 2023) -- End
                    lvItem.SubItems.Add(dtRow.Item("BankGrp").ToString)
                    End If 'Hemant (07 July 2023)
                    'Sohail (16 Oct 2010) -- End
                    lvItem.SubItems.Add(dtRow.Item("BranchName").ToString)
                    lvItem.SubItems.Add(dtRow.Item("period_name").ToString)     'Sohail (25 Apr 2014)
                    lvItem.SubItems.Add(dtRow.Item("AccName").ToString)
                    lvItem.SubItems.Add(dtRow.Item("accountno").ToString)
                    'Hemant (07 July 2023) -- Start
                    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                    lvItem.SubItems.Add(dtRow.Item("mobileno").ToString)
                    'Hemant (07 July 2023) -- End
                    'Sohail (27 May 2014) -- Start
                    'Enhancement - TANAPA
                    lvItem.SubItems.Add(mdicModes.Item(CInt(dtRow.Item("modeid"))))
                    If CInt(dtRow.Item("modeid")) = enPaymentBy.Value Then
                        lvItem.SubItems.Add(Format(dtRow.Item("amount"), GUI.fmtCurrency))
                    Else
                        lvItem.SubItems.Add(Format(dtRow.Item("percentage"), "00.00"))
                    End If
                    'Sohail (27 May 2014) -- End

                    lvItem.Tag = dtRow.Item("empbanktranunkid")

                    'Sohail (16 Oct 2010) -- Start
                    'lvEmpBankList.Items.Add(lvItem)
                    lvArray.Add(lvItem)
                    'Sohail (16 Oct 2010) -- End
                Next
                'Sohail (16 Oct 2010) -- Start
                lvEmpBankList.Items.AddRange(lvArray.ToArray)
                lvEmpBankList.EndUpdate()
                'Sohail (16 Oct 2010) -- End

                'Sohail (27 May 2014) -- Start
                'Enhancement - TANAPA 
                'lvEmpBankList.GroupingColumn = colhBankGroup
                If chkGroupByEmpName.Checked = False Then
                    lvEmpBankList.GroupingColumn = colhBankGroup
                Else
                    lvEmpBankList.GroupingColumn = colhEmpName
                End If
                'Sohail (27 May 2014) -- End
                lvEmpBankList.DisplayGroups(True)

                'Sohail (16 Oct 2010) -- Start
                'colhBankGroup.Width += colhEmpName.Width

                'colhBankGroup.Width = 0
                lvEmpBankList.GridLines = False
                'Sohail (16 Oct 2010) -- End

                If lvEmpBankList.Items.Count > 6 Then
                    If chkGroupByEmpName.Checked = False Then
                        colhEmpName.Width = 150 - 15
                        colhBankGroup.Width = 0
                    Else
                        colhBankGroup.Width = 150 - 15
                        colhEmpName.Width = 0
                    End If
                Else
                    If chkGroupByEmpName.Checked = False Then
                        colhEmpName.Width = 150
                        colhBankGroup.Width = 0
                    Else
                        colhBankGroup.Width = 150
                        colhEmpName.Width = 0
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Finally
            Call objbtnSearch.ShowResult(lvEmpBankList.Items.Count.ToString)
            'Sohail (21 Aug 2015) -- End
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        'Dim objEmployee As New clsEmployee_Master 'Sohail (25 Apr 2014)
        Dim objBankGrp As New clspayrollgroup_master
        'Dim objBranch As New clsbankbranch_master 'Sohail (10 Aug 2012)
        Dim objAccType As New clsBankAccType
        Dim objPeriod As New clscommom_period_Tran 'Sohail (25 Apr 2014)
        Dim objMaster As New clsMasterData 'Sohail (27 May 2014)
        Try

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Employee", True, True)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True)
            'End If

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , , , , False)
            'End If
            'Sohail (25 Apr 2014) -- End
            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Employee")
            '    .SelectedValue = 0
            'End With
            'Sohail (25 Apr 2014) -- End
            dsCombos = objBankGrp.getListForCombo(enGroupType.BankGroup, "BankGrp", True)
            With cboBankGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("BankGrp")
                .SelectedValue = 0
            End With
            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objBranch.getListForCombo("Branch", True)
            'With cboBankBranch
            '    .ValueMember = "branchunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Branch")
            '    .SelectedValue = 0
            'End With
            'Sohail (10 Aug 2012) -- End
            dsCombos = objAccType.getComboList(True, "AccType")
            With cboAccType
                .ValueMember = "accounttypeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AccType")
                .SelectedValue = 0
            End With

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, 0)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
            End With
            'Sohail (25 Apr 2014) -- End

            'Sohail (27 May 2014) -- Start
            dsCombos = objMaster.GetPaymentBy("Mode")
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Mode")
                .SelectedValue = 0
            End With
            mdicModes = (From p In dsCombos.Tables("Mode").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
            'Sohail (27 May 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            'objEmployee = Nothing 'Sohail (25 Apr 2014)
            objBankGrp = Nothing
            'objBranch = Nothing 'Sohail (10 Aug 2012)
            objAccType = Nothing
            objPeriod = Nothing 'Sohail (25 Apr 2014)
            objMaster = Nothing
            dsCombos = Nothing
        End Try
    End Sub

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Try


            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPeriod_startdate, mdtPeriod_enddate)

            'S.SANDEEP [20-JUN-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 mdtPeriod_startdate, _
            '                                 mdtPeriod_enddate, _
            '                                 ConfigParameter._Object._UserAccessModeSetting, _
            '                                 True, False, "EmployeeList", True)

            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployeeBankList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriod_startdate, _
                                           mdtPeriod_enddate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                 mblnOnlyApproved, False, "EmployeeList", True, , , , , , , , , , , , , , , , , , mblnAddApprovalCondition)
            'Anjan [10 June 2015] -- End


            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo_Employee", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub
    'Sohail (25 Apr 2014) -- End

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeBanks
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeBanks
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeBanks


            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuExEmployeeBanks.Enabled = User._Object.Privilege._AllowtoExportEmpBanks
            mnuImEmployeeBanks.Enabled = User._Object.Privilege._AllowtoImportEmpBanks
            'Anjan (25 Oct 2012)-End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try


    End Sub
#End Region

#Region " From's Events "
    Private Sub frmEmployeeBankList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpBankTran = New clsEmployeeBanks
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()
            Call SetVisibility()

            Call SetColor() 'Sohail (16 Oct 2010)
            Call FillCombo()
            Call FillList()


            If lvEmpBankList.Items.Count > 0 Then lvEmpBankList.Items(0).Selected = True
            lvEmpBankList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeBankList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        'Sohail (16 Oct 2010) -- Start
        'If e.KeyCode = Keys.Delete And lvEmpBankList.Focused = True Then
        'Call btnDelete.PerformClick()
        'End If
        'Sohail (16 Oct 2010) -- End
    End Sub

    Private Sub frmEmployeeBankList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeBankList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmpBankTran = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeBanks.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeBanks"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAccType.SelectedValue = 0
            cboBankBranch.SelectedValue = 0
            cboBankGroup.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End
            txtAccNo.Text = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            'Sandeep [ 16 Oct 2010 ] -- Start
            'If lvEmpBankList.CheckedItems.Count < 1 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            '    lvEmpBankList.Select()
            '    Exit Sub
            'End If

            If lvEmpBankList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvEmpBankList.Select()
                Exit Sub
            End If
            'Sandeep [ 16 Oct 2010 ] -- End

            'If objEmpBankTran.isUsed(CInt(strTag(0))) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Account. Reason: This Account is in use."), enMsgBoxStyle.Information) '?2
            '    lvEmpBankList.Select()
            '    Exit Sub
            'End If

            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvEmpBankList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete selected Account?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'For i As Integer = 0 To lvEmpBankList.CheckedItems.Count - 1
                'Sandeep [ 16 Oct 2010 ] -- End 
                objEmpBankTran._Isvoid = True
                'Sandeep [ 16 Oct 2010 ] -- Start
                'objEmpBankTran._Voidreason = "TESTING"
                'objEmpBankTran._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objEmpBankTran._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                'Sandeep [ 16 Oct 2010 ] -- End 
                objEmpBankTran._Voiduserunkid = User._Object._Userunkid
                objEmpBankTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objEmpBankTran.Delete(CInt(lvEmpBankList.CheckedItems(0).Tag))
                'lvEmpBankList.CheckedItems(0).Remove()
                'Next
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objEmpBankTran.Delete(CInt(lvEmpBankList.SelectedItems(0).Tag))
                objEmpBankTran.Delete(CInt(lvEmpBankList.SelectedItems(0).Tag), ConfigParameter._Object._CurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
                lvEmpBankList.SelectedItems(0).Remove()
                'Sandeep [ 16 Oct 2010 ] -- End 
                If lvEmpBankList.Items.Count <= 0 Then
                    Exit Try
                End If

                'If lvEmpBankList.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvEmpBankList.Items.Count - 1
                '    lvEmpBankList.Items(intSelectedIndex).Selected = True
                '    lvEmpBankList.EnsureVisible(intSelectedIndex)
                'ElseIf lvEmpBankList.Items.Count <> 0 Then
                '    lvEmpBankList.Items(intSelectedIndex).Selected = True
                '    lvEmpBankList.EnsureVisible(intSelectedIndex)
                'End If
            End If
            lvEmpBankList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvEmpBankList.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditEmployeeBanks = False OrElse User._Object.Privilege._DeleteEmployeeBanks = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        Dim frm As New frmEmployeeBank_AddEdit
        Try
            If lvEmpBankList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmpBankList.Select()
                Exit Sub
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEmpBankList.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvEmpBankList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            frm = Nothing

            'Sohail (16 Oct 2010) -- Start
            'lvEmpBankList.Items(intSelectedIndex).Selected = True
            'lvEmpBankList.EnsureVisible(intSelectedIndex)
            'Sohail (16 Oct 2010) -- End
            lvEmpBankList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeBank_AddEdit
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (10 Aug 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "
    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim objBranch As New clsbankbranch_master
        Dim dsCombos As DataSet
        Try
            dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            With cboBankBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objBranch = Nothing
        End Try
    End Sub

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
            End If
            Call FillCombo_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 Apr 2014) -- End
#End Region
    'Sohail (10 Aug 2012) -- End

#Region " Other Control's Events "
    'Sandeep [ 17 July 2010 ] -- Start
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 17 July 2010 ] -- End


    'S.SANDEEP [ 21 JUNE 2011 ] -- START
    'ISSUE : MR. RUTTA'S COMMENTS.
    Private Sub mnuExEmployeeBanks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExEmployeeBanks.Click
        'Dim IExcel As New ExcelData
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Excel files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)

                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                'strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath = ObjFile.DirectoryName & "\" & ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'Sohail (25 Apr 2014) -- End
                strFilePath &= ObjFile.Extension

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objEmpBankTran.GetEmployeeBanksTemplate("EmpBanks")
                dsList = objEmpBankTran.GetEmployeeBanksTemplate(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, False, "", "EmpBanks")
                'Sohail (21 Aug 2015) -- End

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)

                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : mnuExEmployeeBanks_Click ; Module Name : " & mstrModuleName)
        Finally
            'IExcel = Nothing
            dsList.Dispose()
            path = String.Empty
            strFilePath = String.Empty
            dlgSaveFile = Nothing
            ObjFile = Nothing
        End Try
    End Sub

    Private Sub mnuImEmployeeBanks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImEmployeeBanks.Click
        Try
            Dim frm As New frmImportEmployeeBanks
            frm.ShowDialog()
            Call FillList()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : mnuImEmployeeBanks_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 21 JUNE 2011 ] -- END 
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 

    'Sohail (10 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchBankGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankGroup.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboBankGroup.ValueMember
                .DisplayMember = cboBankGroup.DisplayMember
                .DataSource = CType(cboBankGroup.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objfrm.DisplayDialog Then
                cboBankGroup.SelectedValue = objfrm.SelectedValue
                cboBankGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchBankBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankBranch.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboBankBranch.ValueMember
                .DisplayMember = cboBankBranch.DisplayMember
                .DataSource = CType(cboBankBranch.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objfrm.DisplayDialog Then
                cboBankBranch.SelectedValue = objfrm.SelectedValue
                cboBankBranch.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankBranch_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Aug 2012) -- End

    'Sohail (27 May 2014) -- Start
    'Enhancement - TANAPA 
    Private Sub chkGroupByEmpName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupByEmpName.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkGroupByEmpName_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 May 2014) -- End

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
            Me.colhBankGroup.Text = Language._Object.getCaption(CStr(Me.colhBankGroup.Tag), Me.colhBankGroup.Text)
            Me.colhBankBranch.Text = Language._Object.getCaption(CStr(Me.colhBankBranch.Tag), Me.colhBankBranch.Text)
            Me.colhAccType.Text = Language._Object.getCaption(CStr(Me.colhAccType.Tag), Me.colhAccType.Text)
            Me.colhAccNo.Text = Language._Object.getCaption(CStr(Me.colhAccNo.Tag), Me.colhAccNo.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblAccNo.Text = Language._Object.getCaption(Me.lblAccNo.Name, Me.lblAccNo.Text)
            Me.lblBank.Text = Language._Object.getCaption(Me.lblBank.Name, Me.lblBank.Text)
            Me.lblAccType.Text = Language._Object.getCaption(Me.lblAccType.Name, Me.lblAccType.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuExEmployeeBanks.Text = Language._Object.getCaption(Me.mnuExEmployeeBanks.Name, Me.mnuExEmployeeBanks.Text)
            Me.mnuImEmployeeBanks.Text = Language._Object.getCaption(Me.mnuImEmployeeBanks.Name, Me.mnuImEmployeeBanks.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.colhMode.Text = Language._Object.getCaption(CStr(Me.colhMode.Tag), Me.colhMode.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
            Me.chkGroupByEmpName.Text = Language._Object.getCaption(Me.chkGroupByEmpName.Name, Me.chkGroupByEmpName.Text)
			Me.colhMobileNo.Text = Language._Object.getCaption(CStr(Me.colhMobileNo.Tag), Me.colhMobileNo.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


	Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete selected Account?")
            Language.setMessage(mstrModuleName, 3, "File exported successfully.")
			Language.setMessage(mstrModuleName, 4, "Mobile Money")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class