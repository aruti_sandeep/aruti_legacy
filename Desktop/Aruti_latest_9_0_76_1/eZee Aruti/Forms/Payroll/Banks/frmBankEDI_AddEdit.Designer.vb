﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBankEDI_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBankEDI_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbHeaderInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtVersion = New eZee.TextBox.AlphanumericTextBox
        Me.txtWorkCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtReceiver = New eZee.TextBox.AlphanumericTextBox
        Me.txtSender = New eZee.TextBox.AlphanumericTextBox
        Me.txtHeaderCompanyName = New eZee.TextBox.AlphanumericTextBox
        Me.txtFileID = New eZee.TextBox.AlphanumericTextBox
        Me.txtHeaderClearCode = New eZee.TextBox.AlphanumericTextBox
        Me.dtpProcessDate = New System.Windows.Forms.DateTimePicker
        Me.txtFileTypeChar = New eZee.TextBox.AlphanumericTextBox
        Me.lblVersion = New System.Windows.Forms.Label
        Me.lblWorkCode = New System.Windows.Forms.Label
        Me.lblReceiver = New System.Windows.Forms.Label
        Me.lblSender = New System.Windows.Forms.Label
        Me.lblHeaderCompanyName = New System.Windows.Forms.Label
        Me.lblFileID = New System.Windows.Forms.Label
        Me.lblHeaderClearCode = New System.Windows.Forms.Label
        Me.lblProcessDate = New System.Windows.Forms.Label
        Me.lblFileTypeChar = New System.Windows.Forms.Label
        Me.txtHeaderID = New eZee.TextBox.AlphanumericTextBox
        Me.lblHeaderID = New System.Windows.Forms.Label
        Me.EZeeHeader = New eZee.Common.eZeeHeader
        Me.gbBankEDIInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.gbDataRecord = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAccNo = New System.Windows.Forms.Label
        Me.txtAccNo = New eZee.TextBox.AlphanumericTextBox
        Me.elDebitBankInfo = New eZee.Common.eZeeLine
        Me.cboAccType = New System.Windows.Forms.ComboBox
        Me.lblAccType = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.dtpPayDate = New System.Windows.Forms.DateTimePicker
        Me.lblBranch = New System.Windows.Forms.Label
        Me.txtDataCompanyName = New eZee.TextBox.AlphanumericTextBox
        Me.txtContraCode = New eZee.TextBox.AlphanumericTextBox
        Me.cboBank = New System.Windows.Forms.ComboBox
        Me.txtProcessRef = New eZee.TextBox.AlphanumericTextBox
        Me.lblBank = New System.Windows.Forms.Label
        Me.txtSalaryCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.lblPayDate = New System.Windows.Forms.Label
        Me.lblContraCode = New System.Windows.Forms.Label
        Me.lblProcessRef = New System.Windows.Forms.Label
        Me.lblSalaryCode = New System.Windows.Forms.Label
        Me.lblDataCompanyName = New System.Windows.Forms.Label
        Me.txtPaymentID = New eZee.TextBox.AlphanumericTextBox
        Me.lblPaymentID = New System.Windows.Forms.Label
        Me.gbTrailerInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblTrailerClearCode = New System.Windows.Forms.Label
        Me.lblTrailerID = New System.Windows.Forms.Label
        Me.txtTrailerClearCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtTrailerID = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddBranch = New eZee.Common.eZeeGradientButton
        Me.objbtnAddAccountType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbHeaderInfo.SuspendLayout()
        Me.gbBankEDIInformation.SuspendLayout()
        Me.gbDataRecord.SuspendLayout()
        Me.gbTrailerInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbHeaderInfo)
        Me.pnlMainInfo.Controls.Add(Me.EZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbBankEDIInformation)
        Me.pnlMainInfo.Controls.Add(Me.gbDataRecord)
        Me.pnlMainInfo.Controls.Add(Me.gbTrailerInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(840, 468)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 418)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(840, 50)
        Me.objFooter.TabIndex = 4
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(735, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(635, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbHeaderInfo
        '
        Me.gbHeaderInfo.BorderColor = System.Drawing.Color.Black
        Me.gbHeaderInfo.Checked = False
        Me.gbHeaderInfo.CollapseAllExceptThis = False
        Me.gbHeaderInfo.CollapsedHoverImage = Nothing
        Me.gbHeaderInfo.CollapsedNormalImage = Nothing
        Me.gbHeaderInfo.CollapsedPressedImage = Nothing
        Me.gbHeaderInfo.CollapseOnLoad = False
        Me.gbHeaderInfo.Controls.Add(Me.txtVersion)
        Me.gbHeaderInfo.Controls.Add(Me.txtWorkCode)
        Me.gbHeaderInfo.Controls.Add(Me.txtReceiver)
        Me.gbHeaderInfo.Controls.Add(Me.txtSender)
        Me.gbHeaderInfo.Controls.Add(Me.txtHeaderCompanyName)
        Me.gbHeaderInfo.Controls.Add(Me.txtFileID)
        Me.gbHeaderInfo.Controls.Add(Me.txtHeaderClearCode)
        Me.gbHeaderInfo.Controls.Add(Me.dtpProcessDate)
        Me.gbHeaderInfo.Controls.Add(Me.txtFileTypeChar)
        Me.gbHeaderInfo.Controls.Add(Me.lblVersion)
        Me.gbHeaderInfo.Controls.Add(Me.lblWorkCode)
        Me.gbHeaderInfo.Controls.Add(Me.lblReceiver)
        Me.gbHeaderInfo.Controls.Add(Me.lblSender)
        Me.gbHeaderInfo.Controls.Add(Me.lblHeaderCompanyName)
        Me.gbHeaderInfo.Controls.Add(Me.lblFileID)
        Me.gbHeaderInfo.Controls.Add(Me.lblHeaderClearCode)
        Me.gbHeaderInfo.Controls.Add(Me.lblProcessDate)
        Me.gbHeaderInfo.Controls.Add(Me.lblFileTypeChar)
        Me.gbHeaderInfo.Controls.Add(Me.txtHeaderID)
        Me.gbHeaderInfo.Controls.Add(Me.lblHeaderID)
        Me.gbHeaderInfo.ExpandedHoverImage = Nothing
        Me.gbHeaderInfo.ExpandedNormalImage = Nothing
        Me.gbHeaderInfo.ExpandedPressedImage = Nothing
        Me.gbHeaderInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHeaderInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHeaderInfo.HeaderHeight = 25
        Me.gbHeaderInfo.HeightOnCollapse = 0
        Me.gbHeaderInfo.LeftTextSpace = 0
        Me.gbHeaderInfo.Location = New System.Drawing.Point(12, 157)
        Me.gbHeaderInfo.Name = "gbHeaderInfo"
        Me.gbHeaderInfo.OpenHeight = 300
        Me.gbHeaderInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHeaderInfo.ShowBorder = True
        Me.gbHeaderInfo.ShowCheckBox = False
        Me.gbHeaderInfo.ShowCollapseButton = False
        Me.gbHeaderInfo.ShowDefaultBorderColor = True
        Me.gbHeaderInfo.ShowDownButton = False
        Me.gbHeaderInfo.ShowHeader = True
        Me.gbHeaderInfo.Size = New System.Drawing.Size(409, 169)
        Me.gbHeaderInfo.TabIndex = 1
        Me.gbHeaderInfo.Temp = 0
        Me.gbHeaderInfo.Text = "Header Info"
        Me.gbHeaderInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVersion
        '
        Me.txtVersion.Flags = 0
        Me.txtVersion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersion.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVersion.Location = New System.Drawing.Point(291, 141)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.Size = New System.Drawing.Size(105, 21)
        Me.txtVersion.TabIndex = 9
        '
        'txtWorkCode
        '
        Me.txtWorkCode.Flags = 0
        Me.txtWorkCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWorkCode.Location = New System.Drawing.Point(98, 141)
        Me.txtWorkCode.Name = "txtWorkCode"
        Me.txtWorkCode.Size = New System.Drawing.Size(105, 21)
        Me.txtWorkCode.TabIndex = 4
        '
        'txtReceiver
        '
        Me.txtReceiver.Flags = 0
        Me.txtReceiver.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReceiver.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReceiver.Location = New System.Drawing.Point(291, 114)
        Me.txtReceiver.Name = "txtReceiver"
        Me.txtReceiver.Size = New System.Drawing.Size(105, 21)
        Me.txtReceiver.TabIndex = 8
        '
        'txtSender
        '
        Me.txtSender.Flags = 0
        Me.txtSender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSender.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSender.Location = New System.Drawing.Point(291, 87)
        Me.txtSender.Name = "txtSender"
        Me.txtSender.Size = New System.Drawing.Size(105, 21)
        Me.txtSender.TabIndex = 7
        '
        'txtHeaderCompanyName
        '
        Me.txtHeaderCompanyName.Flags = 0
        Me.txtHeaderCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeaderCompanyName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHeaderCompanyName.Location = New System.Drawing.Point(98, 114)
        Me.txtHeaderCompanyName.Name = "txtHeaderCompanyName"
        Me.txtHeaderCompanyName.Size = New System.Drawing.Size(105, 21)
        Me.txtHeaderCompanyName.TabIndex = 3
        '
        'txtFileID
        '
        Me.txtFileID.Flags = 0
        Me.txtFileID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFileID.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFileID.Location = New System.Drawing.Point(98, 87)
        Me.txtFileID.Name = "txtFileID"
        Me.txtFileID.Size = New System.Drawing.Size(105, 21)
        Me.txtFileID.TabIndex = 2
        '
        'txtHeaderClearCode
        '
        Me.txtHeaderClearCode.Flags = 0
        Me.txtHeaderClearCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeaderClearCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHeaderClearCode.Location = New System.Drawing.Point(291, 60)
        Me.txtHeaderClearCode.Name = "txtHeaderClearCode"
        Me.txtHeaderClearCode.Size = New System.Drawing.Size(105, 21)
        Me.txtHeaderClearCode.TabIndex = 6
        '
        'dtpProcessDate
        '
        Me.dtpProcessDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProcessDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProcessDate.Location = New System.Drawing.Point(291, 33)
        Me.dtpProcessDate.Name = "dtpProcessDate"
        Me.dtpProcessDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpProcessDate.TabIndex = 5
        '
        'txtFileTypeChar
        '
        Me.txtFileTypeChar.Flags = 0
        Me.txtFileTypeChar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFileTypeChar.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFileTypeChar.Location = New System.Drawing.Point(98, 60)
        Me.txtFileTypeChar.Name = "txtFileTypeChar"
        Me.txtFileTypeChar.Size = New System.Drawing.Size(105, 21)
        Me.txtFileTypeChar.TabIndex = 1
        '
        'lblVersion
        '
        Me.lblVersion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(209, 142)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(76, 16)
        Me.lblVersion.TabIndex = 22
        Me.lblVersion.Text = "Version"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWorkCode
        '
        Me.lblWorkCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkCode.Location = New System.Drawing.Point(9, 143)
        Me.lblWorkCode.Name = "lblWorkCode"
        Me.lblWorkCode.Size = New System.Drawing.Size(83, 16)
        Me.lblWorkCode.TabIndex = 21
        Me.lblWorkCode.Text = "Work Code"
        Me.lblWorkCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReceiver
        '
        Me.lblReceiver.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiver.Location = New System.Drawing.Point(209, 116)
        Me.lblReceiver.Name = "lblReceiver"
        Me.lblReceiver.Size = New System.Drawing.Size(76, 16)
        Me.lblReceiver.TabIndex = 20
        Me.lblReceiver.Text = "Receiver"
        Me.lblReceiver.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSender
        '
        Me.lblSender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSender.Location = New System.Drawing.Point(209, 89)
        Me.lblSender.Name = "lblSender"
        Me.lblSender.Size = New System.Drawing.Size(76, 16)
        Me.lblSender.TabIndex = 19
        Me.lblSender.Text = "Sender"
        Me.lblSender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHeaderCompanyName
        '
        Me.lblHeaderCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderCompanyName.Location = New System.Drawing.Point(9, 116)
        Me.lblHeaderCompanyName.Name = "lblHeaderCompanyName"
        Me.lblHeaderCompanyName.Size = New System.Drawing.Size(83, 16)
        Me.lblHeaderCompanyName.TabIndex = 18
        Me.lblHeaderCompanyName.Text = "Company Name"
        Me.lblHeaderCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFileID
        '
        Me.lblFileID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileID.Location = New System.Drawing.Point(9, 89)
        Me.lblFileID.Name = "lblFileID"
        Me.lblFileID.Size = New System.Drawing.Size(83, 16)
        Me.lblFileID.TabIndex = 17
        Me.lblFileID.Text = "File ID"
        Me.lblFileID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHeaderClearCode
        '
        Me.lblHeaderClearCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderClearCode.Location = New System.Drawing.Point(209, 62)
        Me.lblHeaderClearCode.Name = "lblHeaderClearCode"
        Me.lblHeaderClearCode.Size = New System.Drawing.Size(76, 16)
        Me.lblHeaderClearCode.TabIndex = 16
        Me.lblHeaderClearCode.Text = "Clear Code"
        Me.lblHeaderClearCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProcessDate
        '
        Me.lblProcessDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessDate.Location = New System.Drawing.Point(209, 35)
        Me.lblProcessDate.Name = "lblProcessDate"
        Me.lblProcessDate.Size = New System.Drawing.Size(76, 16)
        Me.lblProcessDate.TabIndex = 15
        Me.lblProcessDate.Text = "Process Date"
        Me.lblProcessDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFileTypeChar
        '
        Me.lblFileTypeChar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileTypeChar.Location = New System.Drawing.Point(9, 62)
        Me.lblFileTypeChar.Name = "lblFileTypeChar"
        Me.lblFileTypeChar.Size = New System.Drawing.Size(83, 16)
        Me.lblFileTypeChar.TabIndex = 14
        Me.lblFileTypeChar.Text = "File Type Char"
        Me.lblFileTypeChar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHeaderID
        '
        Me.txtHeaderID.Flags = 0
        Me.txtHeaderID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeaderID.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHeaderID.Location = New System.Drawing.Point(98, 33)
        Me.txtHeaderID.Name = "txtHeaderID"
        Me.txtHeaderID.Size = New System.Drawing.Size(105, 21)
        Me.txtHeaderID.TabIndex = 0
        '
        'lblHeaderID
        '
        Me.lblHeaderID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderID.Location = New System.Drawing.Point(9, 35)
        Me.lblHeaderID.Name = "lblHeaderID"
        Me.lblHeaderID.Size = New System.Drawing.Size(83, 16)
        Me.lblHeaderID.TabIndex = 12
        Me.lblHeaderID.Text = "Header ID"
        Me.lblHeaderID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeHeader
        '
        Me.EZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader.Icon = Nothing
        Me.EZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader.Message = ""
        Me.EZeeHeader.Name = "EZeeHeader"
        Me.EZeeHeader.Size = New System.Drawing.Size(840, 60)
        Me.EZeeHeader.TabIndex = 5
        Me.EZeeHeader.Title = "Bank EDI Information"
        '
        'gbBankEDIInformation
        '
        Me.gbBankEDIInformation.BorderColor = System.Drawing.Color.Black
        Me.gbBankEDIInformation.Checked = False
        Me.gbBankEDIInformation.CollapseAllExceptThis = False
        Me.gbBankEDIInformation.CollapsedHoverImage = Nothing
        Me.gbBankEDIInformation.CollapsedNormalImage = Nothing
        Me.gbBankEDIInformation.CollapsedPressedImage = Nothing
        Me.gbBankEDIInformation.CollapseOnLoad = False
        Me.gbBankEDIInformation.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbBankEDIInformation.Controls.Add(Me.txtName)
        Me.gbBankEDIInformation.Controls.Add(Me.lblName)
        Me.gbBankEDIInformation.Controls.Add(Me.txtCode)
        Me.gbBankEDIInformation.Controls.Add(Me.lblCode)
        Me.gbBankEDIInformation.ExpandedHoverImage = Nothing
        Me.gbBankEDIInformation.ExpandedNormalImage = Nothing
        Me.gbBankEDIInformation.ExpandedPressedImage = Nothing
        Me.gbBankEDIInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBankEDIInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBankEDIInformation.HeaderHeight = 25
        Me.gbBankEDIInformation.HeightOnCollapse = 0
        Me.gbBankEDIInformation.LeftTextSpace = 0
        Me.gbBankEDIInformation.Location = New System.Drawing.Point(12, 66)
        Me.gbBankEDIInformation.Name = "gbBankEDIInformation"
        Me.gbBankEDIInformation.OpenHeight = 300
        Me.gbBankEDIInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBankEDIInformation.ShowBorder = True
        Me.gbBankEDIInformation.ShowCheckBox = False
        Me.gbBankEDIInformation.ShowCollapseButton = False
        Me.gbBankEDIInformation.ShowDefaultBorderColor = True
        Me.gbBankEDIInformation.ShowDownButton = False
        Me.gbBankEDIInformation.ShowHeader = True
        Me.gbBankEDIInformation.Size = New System.Drawing.Size(409, 85)
        Me.gbBankEDIInformation.TabIndex = 0
        Me.gbBankEDIInformation.Temp = 0
        Me.gbBankEDIInformation.Text = "Bank EDI Information"
        Me.gbBankEDIInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(375, 58)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 288
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(98, 58)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(271, 21)
        Me.txtName.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(9, 60)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(83, 16)
        Me.lblName.TabIndex = 47
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(98, 31)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(132, 21)
        Me.txtCode.TabIndex = 0
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(9, 33)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(83, 16)
        Me.lblCode.TabIndex = 11
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbDataRecord
        '
        Me.gbDataRecord.BorderColor = System.Drawing.Color.Black
        Me.gbDataRecord.Checked = False
        Me.gbDataRecord.CollapseAllExceptThis = False
        Me.gbDataRecord.CollapsedHoverImage = Nothing
        Me.gbDataRecord.CollapsedNormalImage = Nothing
        Me.gbDataRecord.CollapsedPressedImage = Nothing
        Me.gbDataRecord.CollapseOnLoad = False
        Me.gbDataRecord.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbDataRecord.Controls.Add(Me.objbtnAddAccountType)
        Me.gbDataRecord.Controls.Add(Me.objbtnAddBranch)
        Me.gbDataRecord.Controls.Add(Me.lblAccNo)
        Me.gbDataRecord.Controls.Add(Me.txtAccNo)
        Me.gbDataRecord.Controls.Add(Me.elDebitBankInfo)
        Me.gbDataRecord.Controls.Add(Me.cboAccType)
        Me.gbDataRecord.Controls.Add(Me.lblAccType)
        Me.gbDataRecord.Controls.Add(Me.cboCurrency)
        Me.gbDataRecord.Controls.Add(Me.cboBranch)
        Me.gbDataRecord.Controls.Add(Me.cboTrnHead)
        Me.gbDataRecord.Controls.Add(Me.dtpPayDate)
        Me.gbDataRecord.Controls.Add(Me.lblBranch)
        Me.gbDataRecord.Controls.Add(Me.txtDataCompanyName)
        Me.gbDataRecord.Controls.Add(Me.txtContraCode)
        Me.gbDataRecord.Controls.Add(Me.cboBank)
        Me.gbDataRecord.Controls.Add(Me.txtProcessRef)
        Me.gbDataRecord.Controls.Add(Me.lblBank)
        Me.gbDataRecord.Controls.Add(Me.txtSalaryCode)
        Me.gbDataRecord.Controls.Add(Me.lblCurrency)
        Me.gbDataRecord.Controls.Add(Me.lblTrnHead)
        Me.gbDataRecord.Controls.Add(Me.lblPayDate)
        Me.gbDataRecord.Controls.Add(Me.lblContraCode)
        Me.gbDataRecord.Controls.Add(Me.lblProcessRef)
        Me.gbDataRecord.Controls.Add(Me.lblSalaryCode)
        Me.gbDataRecord.Controls.Add(Me.lblDataCompanyName)
        Me.gbDataRecord.Controls.Add(Me.txtPaymentID)
        Me.gbDataRecord.Controls.Add(Me.lblPaymentID)
        Me.gbDataRecord.ExpandedHoverImage = Nothing
        Me.gbDataRecord.ExpandedNormalImage = Nothing
        Me.gbDataRecord.ExpandedPressedImage = Nothing
        Me.gbDataRecord.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDataRecord.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDataRecord.HeaderHeight = 25
        Me.gbDataRecord.HeightOnCollapse = 0
        Me.gbDataRecord.LeftTextSpace = 0
        Me.gbDataRecord.Location = New System.Drawing.Point(427, 66)
        Me.gbDataRecord.Name = "gbDataRecord"
        Me.gbDataRecord.OpenHeight = 300
        Me.gbDataRecord.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDataRecord.ShowBorder = True
        Me.gbDataRecord.ShowCheckBox = False
        Me.gbDataRecord.ShowCollapseButton = False
        Me.gbDataRecord.ShowDefaultBorderColor = True
        Me.gbDataRecord.ShowDownButton = False
        Me.gbDataRecord.ShowHeader = True
        Me.gbDataRecord.Size = New System.Drawing.Size(399, 330)
        Me.gbDataRecord.TabIndex = 2
        Me.gbDataRecord.Temp = 0
        Me.gbDataRecord.Text = "Data Record Info"
        Me.gbDataRecord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAccNo
        '
        Me.lblAccNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccNo.Location = New System.Drawing.Point(8, 302)
        Me.lblAccNo.Name = "lblAccNo"
        Me.lblAccNo.Size = New System.Drawing.Size(69, 16)
        Me.lblAccNo.TabIndex = 49
        Me.lblAccNo.Text = "Account No"
        Me.lblAccNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAccNo
        '
        Me.txtAccNo.Flags = 0
        Me.txtAccNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAccNo.Location = New System.Drawing.Point(83, 300)
        Me.txtAccNo.Name = "txtAccNo"
        Me.txtAccNo.Size = New System.Drawing.Size(278, 21)
        Me.txtAccNo.TabIndex = 11
        '
        'elDebitBankInfo
        '
        Me.elDebitBankInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elDebitBankInfo.Location = New System.Drawing.Point(3, 192)
        Me.elDebitBankInfo.Name = "elDebitBankInfo"
        Me.elDebitBankInfo.Size = New System.Drawing.Size(385, 21)
        Me.elDebitBankInfo.TabIndex = 43
        Me.elDebitBankInfo.Text = "Debit Bank"
        Me.elDebitBankInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAccType
        '
        Me.cboAccType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccType.FormattingEnabled = True
        Me.cboAccType.Location = New System.Drawing.Point(83, 273)
        Me.cboAccType.Name = "cboAccType"
        Me.cboAccType.Size = New System.Drawing.Size(278, 21)
        Me.cboAccType.TabIndex = 9
        '
        'lblAccType
        '
        Me.lblAccType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccType.Location = New System.Drawing.Point(8, 275)
        Me.lblAccType.Name = "lblAccType"
        Me.lblAccType.Size = New System.Drawing.Size(69, 16)
        Me.lblAccType.TabIndex = 48
        Me.lblAccType.Text = "Acc. Type"
        Me.lblAccType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(83, 168)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(166, 21)
        Me.cboCurrency.TabIndex = 7
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(83, 246)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(278, 21)
        Me.cboBranch.TabIndex = 10
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(83, 141)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(278, 21)
        Me.cboTrnHead.TabIndex = 6
        '
        'dtpPayDate
        '
        Me.dtpPayDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPayDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPayDate.Location = New System.Drawing.Point(283, 33)
        Me.dtpPayDate.Name = "dtpPayDate"
        Me.dtpPayDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpPayDate.TabIndex = 4
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 248)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(69, 16)
        Me.lblBranch.TabIndex = 47
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDataCompanyName
        '
        Me.txtDataCompanyName.Flags = 0
        Me.txtDataCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDataCompanyName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDataCompanyName.Location = New System.Drawing.Point(283, 60)
        Me.txtDataCompanyName.Name = "txtDataCompanyName"
        Me.txtDataCompanyName.Size = New System.Drawing.Size(105, 21)
        Me.txtDataCompanyName.TabIndex = 5
        '
        'txtContraCode
        '
        Me.txtContraCode.Flags = 0
        Me.txtContraCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContraCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtContraCode.Location = New System.Drawing.Point(83, 114)
        Me.txtContraCode.Name = "txtContraCode"
        Me.txtContraCode.Size = New System.Drawing.Size(305, 21)
        Me.txtContraCode.TabIndex = 3
        '
        'cboBank
        '
        Me.cboBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBank.FormattingEnabled = True
        Me.cboBank.Location = New System.Drawing.Point(83, 219)
        Me.cboBank.Name = "cboBank"
        Me.cboBank.Size = New System.Drawing.Size(278, 21)
        Me.cboBank.TabIndex = 8
        '
        'txtProcessRef
        '
        Me.txtProcessRef.Flags = 0
        Me.txtProcessRef.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProcessRef.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtProcessRef.Location = New System.Drawing.Point(83, 87)
        Me.txtProcessRef.Name = "txtProcessRef"
        Me.txtProcessRef.Size = New System.Drawing.Size(305, 21)
        Me.txtProcessRef.TabIndex = 2
        '
        'lblBank
        '
        Me.lblBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBank.Location = New System.Drawing.Point(8, 221)
        Me.lblBank.Name = "lblBank"
        Me.lblBank.Size = New System.Drawing.Size(69, 16)
        Me.lblBank.TabIndex = 46
        Me.lblBank.Text = "Bank"
        Me.lblBank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSalaryCode
        '
        Me.txtSalaryCode.Flags = 0
        Me.txtSalaryCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalaryCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSalaryCode.Location = New System.Drawing.Point(83, 60)
        Me.txtSalaryCode.Name = "txtSalaryCode"
        Me.txtSalaryCode.Size = New System.Drawing.Size(105, 21)
        Me.txtSalaryCode.TabIndex = 1
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 170)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(69, 16)
        Me.lblCurrency.TabIndex = 27
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 143)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(69, 16)
        Me.lblTrnHead.TabIndex = 26
        Me.lblTrnHead.Text = "Trn. Head"
        Me.lblTrnHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayDate
        '
        Me.lblPayDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayDate.Location = New System.Drawing.Point(194, 35)
        Me.lblPayDate.Name = "lblPayDate"
        Me.lblPayDate.Size = New System.Drawing.Size(83, 16)
        Me.lblPayDate.TabIndex = 25
        Me.lblPayDate.Text = "Pay Date"
        Me.lblPayDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContraCode
        '
        Me.lblContraCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContraCode.Location = New System.Drawing.Point(8, 116)
        Me.lblContraCode.Name = "lblContraCode"
        Me.lblContraCode.Size = New System.Drawing.Size(69, 16)
        Me.lblContraCode.TabIndex = 24
        Me.lblContraCode.Text = "Contra Code"
        Me.lblContraCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProcessRef
        '
        Me.lblProcessRef.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessRef.Location = New System.Drawing.Point(8, 89)
        Me.lblProcessRef.Name = "lblProcessRef"
        Me.lblProcessRef.Size = New System.Drawing.Size(69, 16)
        Me.lblProcessRef.TabIndex = 23
        Me.lblProcessRef.Text = "Process Ref."
        Me.lblProcessRef.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSalaryCode
        '
        Me.lblSalaryCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryCode.Location = New System.Drawing.Point(8, 64)
        Me.lblSalaryCode.Name = "lblSalaryCode"
        Me.lblSalaryCode.Size = New System.Drawing.Size(69, 16)
        Me.lblSalaryCode.TabIndex = 22
        Me.lblSalaryCode.Text = "Salary Code"
        Me.lblSalaryCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDataCompanyName
        '
        Me.lblDataCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDataCompanyName.Location = New System.Drawing.Point(194, 62)
        Me.lblDataCompanyName.Name = "lblDataCompanyName"
        Me.lblDataCompanyName.Size = New System.Drawing.Size(83, 16)
        Me.lblDataCompanyName.TabIndex = 21
        Me.lblDataCompanyName.Text = "Company Name"
        Me.lblDataCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPaymentID
        '
        Me.txtPaymentID.Flags = 0
        Me.txtPaymentID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaymentID.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPaymentID.Location = New System.Drawing.Point(83, 33)
        Me.txtPaymentID.Name = "txtPaymentID"
        Me.txtPaymentID.Size = New System.Drawing.Size(105, 21)
        Me.txtPaymentID.TabIndex = 0
        '
        'lblPaymentID
        '
        Me.lblPaymentID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentID.Location = New System.Drawing.Point(8, 36)
        Me.lblPaymentID.Name = "lblPaymentID"
        Me.lblPaymentID.Size = New System.Drawing.Size(69, 16)
        Me.lblPaymentID.TabIndex = 19
        Me.lblPaymentID.Text = "Payment ID"
        Me.lblPaymentID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbTrailerInfo
        '
        Me.gbTrailerInfo.BorderColor = System.Drawing.Color.Black
        Me.gbTrailerInfo.Checked = False
        Me.gbTrailerInfo.CollapseAllExceptThis = False
        Me.gbTrailerInfo.CollapsedHoverImage = Nothing
        Me.gbTrailerInfo.CollapsedNormalImage = Nothing
        Me.gbTrailerInfo.CollapsedPressedImage = Nothing
        Me.gbTrailerInfo.CollapseOnLoad = False
        Me.gbTrailerInfo.Controls.Add(Me.lblTrailerClearCode)
        Me.gbTrailerInfo.Controls.Add(Me.lblTrailerID)
        Me.gbTrailerInfo.Controls.Add(Me.txtTrailerClearCode)
        Me.gbTrailerInfo.Controls.Add(Me.txtTrailerID)
        Me.gbTrailerInfo.ExpandedHoverImage = Nothing
        Me.gbTrailerInfo.ExpandedNormalImage = Nothing
        Me.gbTrailerInfo.ExpandedPressedImage = Nothing
        Me.gbTrailerInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrailerInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrailerInfo.HeaderHeight = 25
        Me.gbTrailerInfo.HeightOnCollapse = 0
        Me.gbTrailerInfo.LeftTextSpace = 0
        Me.gbTrailerInfo.Location = New System.Drawing.Point(12, 332)
        Me.gbTrailerInfo.Name = "gbTrailerInfo"
        Me.gbTrailerInfo.OpenHeight = 300
        Me.gbTrailerInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrailerInfo.ShowBorder = True
        Me.gbTrailerInfo.ShowCheckBox = False
        Me.gbTrailerInfo.ShowCollapseButton = False
        Me.gbTrailerInfo.ShowDefaultBorderColor = True
        Me.gbTrailerInfo.ShowDownButton = False
        Me.gbTrailerInfo.ShowHeader = True
        Me.gbTrailerInfo.Size = New System.Drawing.Size(409, 64)
        Me.gbTrailerInfo.TabIndex = 3
        Me.gbTrailerInfo.Temp = 0
        Me.gbTrailerInfo.Text = "Trailer Info"
        Me.gbTrailerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTrailerClearCode
        '
        Me.lblTrailerClearCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrailerClearCode.Location = New System.Drawing.Point(209, 36)
        Me.lblTrailerClearCode.Name = "lblTrailerClearCode"
        Me.lblTrailerClearCode.Size = New System.Drawing.Size(76, 16)
        Me.lblTrailerClearCode.TabIndex = 31
        Me.lblTrailerClearCode.Text = "Clear Code"
        Me.lblTrailerClearCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTrailerID
        '
        Me.lblTrailerID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrailerID.Location = New System.Drawing.Point(9, 36)
        Me.lblTrailerID.Name = "lblTrailerID"
        Me.lblTrailerID.Size = New System.Drawing.Size(83, 16)
        Me.lblTrailerID.TabIndex = 30
        Me.lblTrailerID.Text = "Trailer ID"
        Me.lblTrailerID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTrailerClearCode
        '
        Me.txtTrailerClearCode.Flags = 0
        Me.txtTrailerClearCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTrailerClearCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTrailerClearCode.Location = New System.Drawing.Point(291, 34)
        Me.txtTrailerClearCode.Name = "txtTrailerClearCode"
        Me.txtTrailerClearCode.Size = New System.Drawing.Size(105, 21)
        Me.txtTrailerClearCode.TabIndex = 1
        '
        'txtTrailerID
        '
        Me.txtTrailerID.Flags = 0
        Me.txtTrailerID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTrailerID.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTrailerID.Location = New System.Drawing.Point(98, 34)
        Me.txtTrailerID.Name = "txtTrailerID"
        Me.txtTrailerID.Size = New System.Drawing.Size(105, 21)
        Me.txtTrailerID.TabIndex = 0
        '
        'objbtnAddBranch
        '
        Me.objbtnAddBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBranch.BorderSelected = False
        Me.objbtnAddBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBranch.Image = CType(resources.GetObject("objbtnAddBranch.Image"), System.Drawing.Image)
        Me.objbtnAddBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBranch.Location = New System.Drawing.Point(367, 246)
        Me.objbtnAddBranch.Name = "objbtnAddBranch"
        Me.objbtnAddBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBranch.TabIndex = 289
        '
        'objbtnAddAccountType
        '
        Me.objbtnAddAccountType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAccountType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAccountType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAccountType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAccountType.BorderSelected = False
        Me.objbtnAddAccountType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAccountType.Image = CType(resources.GetObject("objbtnAddAccountType.Image"), System.Drawing.Image)
        Me.objbtnAddAccountType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAccountType.Location = New System.Drawing.Point(367, 273)
        Me.objbtnAddAccountType.Name = "objbtnAddAccountType"
        Me.objbtnAddAccountType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAccountType.TabIndex = 290
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(367, 141)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 291
        '
        'frmBankEDI_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(840, 468)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBankEDI_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Bank EDI"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbHeaderInfo.ResumeLayout(False)
        Me.gbHeaderInfo.PerformLayout()
        Me.gbBankEDIInformation.ResumeLayout(False)
        Me.gbBankEDIInformation.PerformLayout()
        Me.gbDataRecord.ResumeLayout(False)
        Me.gbDataRecord.PerformLayout()
        Me.gbTrailerInfo.ResumeLayout(False)
        Me.gbTrailerInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents lblAccNo As System.Windows.Forms.Label
    Friend WithEvents txtAccNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents elDebitBankInfo As eZee.Common.eZeeLine
    Friend WithEvents cboAccType As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccType As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents dtpPayDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents txtDataCompanyName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtContraCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboBank As System.Windows.Forms.ComboBox
    Friend WithEvents txtProcessRef As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBank As System.Windows.Forms.Label
    Friend WithEvents lblSalaryCode As System.Windows.Forms.Label
    Friend WithEvents gbTrailerInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTrailerClearCode As System.Windows.Forms.Label
    Friend WithEvents lblTrailerID As System.Windows.Forms.Label
    Friend WithEvents txtTrailerClearCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTrailerID As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSalaryCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents lblPayDate As System.Windows.Forms.Label
    Friend WithEvents gbDataRecord As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblContraCode As System.Windows.Forms.Label
    Friend WithEvents lblProcessRef As System.Windows.Forms.Label
    Friend WithEvents lblDataCompanyName As System.Windows.Forms.Label
    Friend WithEvents txtPaymentID As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPaymentID As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbHeaderInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtVersion As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtWorkCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtReceiver As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSender As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtHeaderCompanyName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFileID As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtHeaderClearCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpProcessDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFileTypeChar As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents lblWorkCode As System.Windows.Forms.Label
    Friend WithEvents lblReceiver As System.Windows.Forms.Label
    Friend WithEvents lblSender As System.Windows.Forms.Label
    Friend WithEvents lblHeaderCompanyName As System.Windows.Forms.Label
    Friend WithEvents lblFileID As System.Windows.Forms.Label
    Friend WithEvents lblHeaderClearCode As System.Windows.Forms.Label
    Friend WithEvents lblProcessDate As System.Windows.Forms.Label
    Friend WithEvents lblFileTypeChar As System.Windows.Forms.Label
    Friend WithEvents txtHeaderID As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblHeaderID As System.Windows.Forms.Label
    Friend WithEvents EZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbBankEDIInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddAccountType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
End Class
