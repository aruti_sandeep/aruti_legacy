﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3


Public Class frmPeriodList


#Region "Private Variable"

    Private objPeriodMaster As clscommom_period_Tran
    Private ReadOnly mstrModuleName As String = "frmPeriodList"
    Friend mintModuleRefId As Integer
    Friend isAssessment As Boolean = False
#End Region


#Region " Display Dialog "

    Public Function displayDialog(ByVal _isAssessment As Boolean) As Boolean
        Try
            isAssessment = _isAssessment
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region


#Region "Form's Event"

    Private Sub frmPeriodList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPeriodMaster = New clscommom_period_Tran
        Try

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If isAssessment = True Then
                Me.Name = "frmAssessment_Period_List"
            Else
                Me.Name = "frmPayroll_Period_List"
            End If
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            fillList()
            If lvPeriodList.Items.Count > 0 Then lvPeriodList.Items(0).Selected = True
            lvPeriodList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeriodList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeriodList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvPeriodList.Focused = True Then
                'Sohail (24 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (24 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeriodList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeriodList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPeriodMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clscommom_period_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clscommom_period_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmPeriod_AddEdit As New frmPeriod_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmPeriod_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmPeriod_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmPeriod_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End 

            If objfrmPeriod_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, mintModuleRefId, isAssessment) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvPeriodList.DoubleClick 'Sohail (19 Nov 2010)
        'Sohail (24 Jun 2011) -- Start
        If isAssessment = False Then
            If User._Object.Privilege._EditPeriods = False Then Exit Sub
        Else
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._EditAssessmentPeriod = False Then Exit Sub
            If User._Object.Privilege._AllowtoEditAssessmentPeriod = False Then Exit Sub
            'S.SANDEEP [28 MAY 2015] -- END
        End If
        'Sohail (24 Jun 2011) -- End
        Try
            If lvPeriodList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Period from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvPeriodList.Select()
                Exit Sub
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Sohail (24 Mar 2018) -- Start
            'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar. [Salary month should be character for example Jan to be displayed as 01, Feb as 02, two digit number]
            'If CInt(lvPeriodList.SelectedItems(0).SubItems(colhstatusid.Index).Text) = enStatusType.Close Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot edit this period. Reason : Period is already closed."), enMsgBoxStyle.Information) '?1
            '    lvPeriodList.Select()
            '    Exit Sub
            'End If
            'Sohail (24 Mar 2018) -- End
            'S.SANDEEP [ 04 JULY 2012 ] -- END

            Dim objfrmPeriod_AddEdit As New frmPeriod_AddEdit

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmPeriod_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmPeriod_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmPeriod_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End 

            Try
                Dim intSelectedIndex As Integer

                'Pinkal (24-Jan-2011) -- Start

                If btnEdit.Enabled Then
                    intSelectedIndex = lvPeriodList.SelectedItems(0).Index
                    If objfrmPeriod_AddEdit.displayDialog(CInt(lvPeriodList.SelectedItems(0).Tag), enAction.EDIT_ONE, mintModuleRefId, isAssessment) Then
                        fillList()
                    End If
                End If

                'Pinkal (24-Jan-2011) -- End

                objfrmPeriod_AddEdit = Nothing

                lvPeriodList.Items(intSelectedIndex).Selected = True
                lvPeriodList.EnsureVisible(intSelectedIndex)
                lvPeriodList.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmPeriod_AddEdit IsNot Nothing Then objfrmPeriod_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvPeriodList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Period from the list to perform further operation."), enMsgBoxStyle.Information)
            lvPeriodList.Select()
            Exit Sub
        End If
        If objPeriodMaster.isUsed(CInt(lvPeriodList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Period. Reason: This Period is in use."), enMsgBoxStyle.Information)
            lvPeriodList.Select()
            Exit Sub
        End If

        'S.SANDEEP [ 04 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        If CInt(lvPeriodList.SelectedItems(0).SubItems(colhstatusid.Index).Text) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot delete this period. Reason : Period is already closed."), enMsgBoxStyle.Information) '?1
            lvPeriodList.Select()
            Exit Sub
        End If
        'S.SANDEEP [ 04 JULY 2012 ] -- END

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPeriodList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Period?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriodMaster.Delete(CInt(lvPeriodList.SelectedItems(0).Tag))
                objPeriodMaster.Delete(CInt(lvPeriodList.SelectedItems(0).Tag), User._Object._Userunkid)
                'Sohail (21 Aug 2015) -- End
                lvPeriodList.SelectedItems(0).Remove()

                If lvPeriodList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvPeriodList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvPeriodList.Items.Count - 1
                    lvPeriodList.Items(intSelectedIndex).Selected = True
                    lvPeriodList.EnsureVisible(intSelectedIndex)
                ElseIf lvPeriodList.Items.Count <> 0 Then
                    lvPeriodList.Items(intSelectedIndex).Selected = True
                    lvPeriodList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvPeriodList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Sohail (12 Oct 2011) -- Start
    Private Sub lnkAuoGenerateMonthlyPeriod_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAuoGenerateMonthlyPeriod.LinkClicked
        Try
            Dim objFrm As New frmAutoGeneratePeriods
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            If objFrm.displayDialog(-1, enAction.ADD_ONE, mintModuleRefId, isAssessment) Then
                Call fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAuoGenerateMonthlyPeriod_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Oct 2011) -- End

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsPeriodList As New DataSet
        Dim intDescColWidth As Integer = 0 'Sohail (07 Jan 2014)
        Try

            'Pinkal (09-Jul-2012) -- Start
            'Enhancement : TRA Changes

            If User._Object.Privilege._AllowToViewPayrollPeriodList = False AndAlso isAssessment = False Then Exit Sub

            If User._Object.Privilege._AllowtoViewAssessmentPeriodList = False AndAlso isAssessment = True Then Exit Sub

            'Pinkal (09-Jul-2012) -- End

            'S.SANDEEP [ 25 JUNE 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'dsPeriodList = objPeriodMaster.GetList("Period", mintModuleRefId)
            If isAssessment = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPeriodList = objPeriodMaster.GetList("Period", mintModuleRefId, True, , , True)
                dsPeriodList = objPeriodMaster.GetList("Period", mintModuleRefId, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, , , True)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPeriodList = objPeriodMaster.GetList("Period", mintModuleRefId)
                dsPeriodList = objPeriodMaster.GetList("Period", mintModuleRefId, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date)
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 25 JUNE 2013 ] -- END

            'dsPeriodList = objPeriodMaster.GetList("Period", mintModuleRefId)

            Dim lvItem As ListViewItem

            lvPeriodList.Items.Clear()
            For Each drRow As DataRow In dsPeriodList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("year").ToString
                lvItem.Tag = drRow("periodunkid")
                lvItem.SubItems.Add(drRow("period_code").ToString)
                lvItem.SubItems.Add(drRow("period_name").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(drRow("start_date").ToString).ToShortDateString())
                lvItem.SubItems.Add(eZeeDate.convertDate(drRow("end_date").ToString).ToShortDateString())
                lvItem.SubItems.Add(eZeeDate.convertDate(drRow("tna_enddate").ToString).ToShortDateString()) 'Sohail (07 Jan 2014)
                lvItem.SubItems.Add(drRow("status").ToString)
                lvItem.SubItems.Add(drRow("description").ToString)
                lvItem.SubItems.Add(drRow("statusid").ToString)
                lvPeriodList.Items.Add(lvItem)
            Next

            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            If isAssessment = True OrElse ConfigParameter._Object._IsSeparateTnAPeriod = False Then
                colhTnAEndDate.Width = 0
                colhPerioddesc.Width = 200
            Else
                colhTnAEndDate.Width = 85
                colhPerioddesc.Width = 120
            End If
            intDescColWidth = colhPerioddesc.Width
            'Sohail (07 Jan 2014) -- End

            If lvPeriodList.Items.Count > 16 Then
                colhPerioddesc.Width = intDescColWidth - 18
            Else
                colhPerioddesc.Width = intDescColWidth
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsPeriodList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            If isAssessment = False Then
                btnNew.Enabled = User._Object.Privilege._AddPeriods
                btnEdit.Enabled = User._Object.Privilege._EditPeriods
                btnDelete.Enabled = User._Object.Privilege._DeletePeriods

                'Anjan (25 Oct 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                lnkAuoGenerateMonthlyPeriod.Visible = User._Object.Privilege._AddPeriods
                'Anjan (25 Oct 2012)-End 


            Else

                'S.SANDEEP [28 MAY 2015] -- START
                'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                'btnNew.Enabled = User._Object.Privilege._AddAssessmentPeriod
                'btnEdit.Enabled = User._Object.Privilege._EditAssessmentPeriod
                'btnDelete.Enabled = User._Object.Privilege._DeleteAssessmentPeriod

                ''Anjan (25 Oct 2012)-Start
                ''ENHANCEMENT : TRA COMMENTS on Andrew's Request
                'lnkAuoGenerateMonthlyPeriod.Visible = User._Object.Privilege._AddAssessmentPeriod
                ''Anjan (25 Oct 2012)-End 

                btnNew.Enabled = User._Object.Privilege._AllowtoAddAssessmentPeriod
                btnEdit.Enabled = User._Object.Privilege._AllowtoEditAssessmentPeriod
                btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteAssessmentPeriod

                'Anjan (25 Oct 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                lnkAuoGenerateMonthlyPeriod.Visible = User._Object.Privilege._AllowtoAddAssessmentPeriod
                'Anjan (25 Oct 2012)-End 

                'S.SANDEEP [28 MAY 2015] -- END




            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

    'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

#Region "ListView Event"

    Private Sub lvPeriodList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPeriodList.SelectedIndexChanged
        Try
            If lvPeriodList.SelectedItems.Count > 0 Then

                If CInt(lvPeriodList.SelectedItems(0).SubItems(colhstatusid.Index).Text) = 2 Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                ElseIf CInt(lvPeriodList.SelectedItems(0).SubItems(colhstatusid.Index).Text) = 1 Then
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                End If
            End If
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriodList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhPeriodcode.Text = Language._Object.getCaption(CStr(Me.colhPeriodcode.Tag), Me.colhPeriodcode.Text)
            Me.colhPeriodname.Text = Language._Object.getCaption(CStr(Me.colhPeriodname.Tag), Me.colhPeriodname.Text)
            Me.colhPerioddesc.Text = Language._Object.getCaption(CStr(Me.colhPerioddesc.Tag), Me.colhPerioddesc.Text)
            Me.colhStartperiod.Text = Language._Object.getCaption(CStr(Me.colhStartperiod.Tag), Me.colhStartperiod.Text)
            Me.colhEndperiod.Text = Language._Object.getCaption(CStr(Me.colhEndperiod.Tag), Me.colhEndperiod.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhYear.Text = Language._Object.getCaption(CStr(Me.colhYear.Tag), Me.colhYear.Text)
            Me.colhstatusid.Text = Language._Object.getCaption(CStr(Me.colhstatusid.Tag), Me.colhstatusid.Text)
            Me.lnkAuoGenerateMonthlyPeriod.Text = Language._Object.getCaption(Me.lnkAuoGenerateMonthlyPeriod.Name, Me.lnkAuoGenerateMonthlyPeriod.Text)
            Me.colhTnAEndDate.Text = Language._Object.getCaption(CStr(Me.colhTnAEndDate.Tag), Me.colhTnAEndDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Period from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Period. Reason: This Period is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Period?")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot edit this period. Reason : Period is already closed.")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot delete this period. Reason : Period is already closed.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class