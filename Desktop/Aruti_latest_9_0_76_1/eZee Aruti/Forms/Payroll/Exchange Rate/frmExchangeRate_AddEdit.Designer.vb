﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExchangeRate_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExchangeRate_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.txtExchangeRate = New eZee.TextBox.NumericTextBox
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbCurrencyInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtCurrencyName = New eZee.TextBox.AlphanumericTextBox
        Me.txtExchangeRate1 = New eZee.TextBox.NumericTextBox
        Me.txtExchangeRate2 = New eZee.TextBox.NumericTextBox
        Me.lblDecimalPlace = New System.Windows.Forms.Label
        Me.nudDecimalPlaces = New System.Windows.Forms.NumericUpDown
        Me.radCurrencySignSuffix = New System.Windows.Forms.RadioButton
        Me.radCurrencySignPrefix = New System.Windows.Forms.RadioButton
        Me.lblDisplaySign = New System.Windows.Forms.Label
        Me.objlblSign = New System.Windows.Forms.Label
        Me.objlblEqual = New System.Windows.Forms.Label
        Me.lblBasecurrency = New System.Windows.Forms.Label
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.txtCurrencySign = New eZee.TextBox.AlphanumericTextBox
        Me.lblSign = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboCurrencySignName = New System.Windows.Forms.ComboBox
        Me.cboCurrencySign = New System.Windows.Forms.ComboBox
        Me.lblDateOfRate = New System.Windows.Forms.Label
        Me.dtpDateOfRate = New System.Windows.Forms.DateTimePicker
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbCurrencyInfo.SuspendLayout()
        CType(Me.nudDecimalPlaces, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbCurrencyInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(355, 271)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.txtExchangeRate)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 218)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(355, 53)
        Me.objFooter.TabIndex = 1
        '
        'txtExchangeRate
        '
        Me.txtExchangeRate.AllowNegative = False
        Me.txtExchangeRate.Decimal = New Decimal(New Integer() {10000, 0, 0, 262144})
        Me.txtExchangeRate.DigitsInGroup = 0
        Me.txtExchangeRate.Flags = 65536
        Me.txtExchangeRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExchangeRate.Location = New System.Drawing.Point(20, 11)
        Me.txtExchangeRate.MaxDecimalPlaces = 10
        Me.txtExchangeRate.MaxWholeDigits = 12
        Me.txtExchangeRate.Name = "txtExchangeRate"
        Me.txtExchangeRate.Prefix = ""
        Me.txtExchangeRate.RangeMax = 1.7976931348623157E+308
        Me.txtExchangeRate.RangeMin = -1.7976931348623157E+308
        Me.txtExchangeRate.Size = New System.Drawing.Size(69, 21)
        Me.txtExchangeRate.TabIndex = 1
        Me.txtExchangeRate.Text = "1.0000"
        Me.txtExchangeRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExchangeRate.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(246, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(143, 11)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbCurrencyInfo
        '
        Me.gbCurrencyInfo.BorderColor = System.Drawing.Color.Black
        Me.gbCurrencyInfo.Checked = False
        Me.gbCurrencyInfo.CollapseAllExceptThis = False
        Me.gbCurrencyInfo.CollapsedHoverImage = Nothing
        Me.gbCurrencyInfo.CollapsedNormalImage = Nothing
        Me.gbCurrencyInfo.CollapsedPressedImage = Nothing
        Me.gbCurrencyInfo.CollapseOnLoad = False
        Me.gbCurrencyInfo.Controls.Add(Me.lblDateOfRate)
        Me.gbCurrencyInfo.Controls.Add(Me.dtpDateOfRate)
        Me.gbCurrencyInfo.Controls.Add(Me.txtCurrencyName)
        Me.gbCurrencyInfo.Controls.Add(Me.txtExchangeRate1)
        Me.gbCurrencyInfo.Controls.Add(Me.txtExchangeRate2)
        Me.gbCurrencyInfo.Controls.Add(Me.lblDecimalPlace)
        Me.gbCurrencyInfo.Controls.Add(Me.nudDecimalPlaces)
        Me.gbCurrencyInfo.Controls.Add(Me.radCurrencySignSuffix)
        Me.gbCurrencyInfo.Controls.Add(Me.radCurrencySignPrefix)
        Me.gbCurrencyInfo.Controls.Add(Me.lblDisplaySign)
        Me.gbCurrencyInfo.Controls.Add(Me.objlblSign)
        Me.gbCurrencyInfo.Controls.Add(Me.objlblEqual)
        Me.gbCurrencyInfo.Controls.Add(Me.lblBasecurrency)
        Me.gbCurrencyInfo.Controls.Add(Me.EZeeLine1)
        Me.gbCurrencyInfo.Controls.Add(Me.txtCurrencySign)
        Me.gbCurrencyInfo.Controls.Add(Me.lblSign)
        Me.gbCurrencyInfo.Controls.Add(Me.cboCountry)
        Me.gbCurrencyInfo.Controls.Add(Me.lblCountry)
        Me.gbCurrencyInfo.Controls.Add(Me.lblCurrency)
        Me.gbCurrencyInfo.Controls.Add(Me.cboCurrencySignName)
        Me.gbCurrencyInfo.Controls.Add(Me.cboCurrencySign)
        Me.gbCurrencyInfo.ExpandedHoverImage = Nothing
        Me.gbCurrencyInfo.ExpandedNormalImage = Nothing
        Me.gbCurrencyInfo.ExpandedPressedImage = Nothing
        Me.gbCurrencyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCurrencyInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCurrencyInfo.HeaderHeight = 25
        Me.gbCurrencyInfo.HeaderMessage = ""
        Me.gbCurrencyInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCurrencyInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCurrencyInfo.HeightOnCollapse = 0
        Me.gbCurrencyInfo.LeftTextSpace = 0
        Me.gbCurrencyInfo.Location = New System.Drawing.Point(12, 6)
        Me.gbCurrencyInfo.Name = "gbCurrencyInfo"
        Me.gbCurrencyInfo.OpenHeight = 182
        Me.gbCurrencyInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCurrencyInfo.ShowBorder = True
        Me.gbCurrencyInfo.ShowCheckBox = False
        Me.gbCurrencyInfo.ShowCollapseButton = False
        Me.gbCurrencyInfo.ShowDefaultBorderColor = True
        Me.gbCurrencyInfo.ShowDownButton = False
        Me.gbCurrencyInfo.ShowHeader = True
        Me.gbCurrencyInfo.Size = New System.Drawing.Size(331, 206)
        Me.gbCurrencyInfo.TabIndex = 1
        Me.gbCurrencyInfo.Temp = 0
        Me.gbCurrencyInfo.Text = "Exchange Rate Info"
        Me.gbCurrencyInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCurrencyName
        '
        Me.txtCurrencyName.Flags = 0
        Me.txtCurrencyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrencyName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCurrencyName.Location = New System.Drawing.Point(101, 60)
        Me.txtCurrencyName.Name = "txtCurrencyName"
        Me.txtCurrencyName.Size = New System.Drawing.Size(104, 21)
        Me.txtCurrencyName.TabIndex = 5
        '
        'txtExchangeRate1
        '
        Me.txtExchangeRate1.AllowNegative = False
        Me.txtExchangeRate1.Decimal = New Decimal(New Integer() {10000, 0, 0, 262144})
        Me.txtExchangeRate1.DigitsInGroup = 0
        Me.txtExchangeRate1.Flags = 65536
        Me.txtExchangeRate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExchangeRate1.Location = New System.Drawing.Point(10, 175)
        Me.txtExchangeRate1.MaxDecimalPlaces = 10
        Me.txtExchangeRate1.MaxWholeDigits = 12
        Me.txtExchangeRate1.Name = "txtExchangeRate1"
        Me.txtExchangeRate1.Prefix = ""
        Me.txtExchangeRate1.RangeMax = 1.7976931348623157E+308
        Me.txtExchangeRate1.RangeMin = -1.7976931348623157E+308
        Me.txtExchangeRate1.Size = New System.Drawing.Size(69, 21)
        Me.txtExchangeRate1.TabIndex = 15
        Me.txtExchangeRate1.Text = "1.0000"
        Me.txtExchangeRate1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtExchangeRate2
        '
        Me.txtExchangeRate2.AllowNegative = False
        Me.txtExchangeRate2.Decimal = New Decimal(New Integer() {10000, 0, 0, 262144})
        Me.txtExchangeRate2.DigitsInGroup = 0
        Me.txtExchangeRate2.Flags = 65536
        Me.txtExchangeRate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExchangeRate2.Location = New System.Drawing.Point(175, 175)
        Me.txtExchangeRate2.MaxDecimalPlaces = 10
        Me.txtExchangeRate2.MaxWholeDigits = 12
        Me.txtExchangeRate2.Name = "txtExchangeRate2"
        Me.txtExchangeRate2.Prefix = ""
        Me.txtExchangeRate2.RangeMax = 1.7976931348623157E+308
        Me.txtExchangeRate2.RangeMin = -1.7976931348623157E+308
        Me.txtExchangeRate2.Size = New System.Drawing.Size(69, 21)
        Me.txtExchangeRate2.TabIndex = 18
        Me.txtExchangeRate2.Text = "1.0000"
        Me.txtExchangeRate2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDecimalPlace
        '
        Me.lblDecimalPlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDecimalPlace.Location = New System.Drawing.Point(8, 112)
        Me.lblDecimalPlace.Name = "lblDecimalPlace"
        Me.lblDecimalPlace.Size = New System.Drawing.Size(87, 16)
        Me.lblDecimalPlace.TabIndex = 11
        Me.lblDecimalPlace.Text = "Decimal Place"
        Me.lblDecimalPlace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDecimalPlaces
        '
        Me.nudDecimalPlaces.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDecimalPlaces.Location = New System.Drawing.Point(101, 110)
        Me.nudDecimalPlaces.Name = "nudDecimalPlaces"
        Me.nudDecimalPlaces.Size = New System.Drawing.Size(104, 21)
        Me.nudDecimalPlaces.TabIndex = 12
        Me.nudDecimalPlaces.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'radCurrencySignSuffix
        '
        Me.radCurrencySignSuffix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCurrencySignSuffix.Location = New System.Drawing.Point(214, 87)
        Me.radCurrencySignSuffix.Name = "radCurrencySignSuffix"
        Me.radCurrencySignSuffix.Size = New System.Drawing.Size(107, 17)
        Me.radCurrencySignSuffix.TabIndex = 10
        Me.radCurrencySignSuffix.TabStop = True
        Me.radCurrencySignSuffix.Text = "Suffix"
        Me.radCurrencySignSuffix.UseVisualStyleBackColor = True
        '
        'radCurrencySignPrefix
        '
        Me.radCurrencySignPrefix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCurrencySignPrefix.Location = New System.Drawing.Point(101, 87)
        Me.radCurrencySignPrefix.Name = "radCurrencySignPrefix"
        Me.radCurrencySignPrefix.Size = New System.Drawing.Size(104, 17)
        Me.radCurrencySignPrefix.TabIndex = 9
        Me.radCurrencySignPrefix.TabStop = True
        Me.radCurrencySignPrefix.Text = "Prefix"
        Me.radCurrencySignPrefix.UseVisualStyleBackColor = True
        '
        'lblDisplaySign
        '
        Me.lblDisplaySign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplaySign.Location = New System.Drawing.Point(8, 87)
        Me.lblDisplaySign.Name = "lblDisplaySign"
        Me.lblDisplaySign.Size = New System.Drawing.Size(87, 16)
        Me.lblDisplaySign.TabIndex = 8
        Me.lblDisplaySign.Text = "Display Sign"
        Me.lblDisplaySign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign
        '
        Me.objlblSign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign.Location = New System.Drawing.Point(250, 177)
        Me.objlblSign.Name = "objlblSign"
        Me.objlblSign.Size = New System.Drawing.Size(66, 16)
        Me.objlblSign.TabIndex = 0
        Me.objlblSign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEqual
        '
        Me.objlblEqual.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEqual.Location = New System.Drawing.Point(151, 177)
        Me.objlblEqual.Name = "objlblEqual"
        Me.objlblEqual.Size = New System.Drawing.Size(13, 16)
        Me.objlblEqual.TabIndex = 17
        Me.objlblEqual.Text = "="
        Me.objlblEqual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBasecurrency
        '
        Me.lblBasecurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasecurrency.Location = New System.Drawing.Point(90, 177)
        Me.lblBasecurrency.Name = "lblBasecurrency"
        Me.lblBasecurrency.Size = New System.Drawing.Size(55, 16)
        Me.lblBasecurrency.TabIndex = 16
        Me.lblBasecurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(11, 139)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(310, 6)
        Me.EZeeLine1.TabIndex = 14
        '
        'txtCurrencySign
        '
        Me.txtCurrencySign.Flags = 0
        Me.txtCurrencySign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrencySign.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCurrencySign.Location = New System.Drawing.Point(266, 60)
        Me.txtCurrencySign.Name = "txtCurrencySign"
        Me.txtCurrencySign.Size = New System.Drawing.Size(55, 21)
        Me.txtCurrencySign.TabIndex = 7
        '
        'lblSign
        '
        Me.lblSign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSign.Location = New System.Drawing.Point(211, 62)
        Me.lblSign.Name = "lblSign"
        Me.lblSign.Size = New System.Drawing.Size(49, 16)
        Me.lblSign.TabIndex = 6
        Me.lblSign.Text = "Sign"
        Me.lblSign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Items.AddRange(New Object() {"Tanzania"})
        Me.cboCountry.Location = New System.Drawing.Point(101, 33)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(220, 21)
        Me.cboCountry.TabIndex = 1
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(8, 36)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(87, 16)
        Me.lblCountry.TabIndex = 0
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 62)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(87, 16)
        Me.lblCurrency.TabIndex = 4
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrencySignName
        '
        Me.cboCurrencySignName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrencySignName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrencySignName.FormattingEnabled = True
        Me.cboCurrencySignName.Items.AddRange(New Object() {"Tanzania"})
        Me.cboCurrencySignName.Location = New System.Drawing.Point(101, 33)
        Me.cboCurrencySignName.Name = "cboCurrencySignName"
        Me.cboCurrencySignName.Size = New System.Drawing.Size(60, 21)
        Me.cboCurrencySignName.TabIndex = 2
        '
        'cboCurrencySign
        '
        Me.cboCurrencySign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrencySign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrencySign.FormattingEnabled = True
        Me.cboCurrencySign.Items.AddRange(New Object() {"Tanzania"})
        Me.cboCurrencySign.Location = New System.Drawing.Point(261, 33)
        Me.cboCurrencySign.Name = "cboCurrencySign"
        Me.cboCurrencySign.Size = New System.Drawing.Size(60, 21)
        Me.cboCurrencySign.TabIndex = 3
        '
        'lblDateOfRate
        '
        Me.lblDateOfRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateOfRate.Location = New System.Drawing.Point(11, 150)
        Me.lblDateOfRate.Name = "lblDateOfRate"
        Me.lblDateOfRate.Size = New System.Drawing.Size(84, 16)
        Me.lblDateOfRate.TabIndex = 20
        Me.lblDateOfRate.Text = "Date of Rate"
        Me.lblDateOfRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDateOfRate
        '
        Me.dtpDateOfRate.Checked = False
        Me.dtpDateOfRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateOfRate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateOfRate.Location = New System.Drawing.Point(101, 148)
        Me.dtpDateOfRate.Name = "dtpDateOfRate"
        Me.dtpDateOfRate.Size = New System.Drawing.Size(104, 21)
        Me.dtpDateOfRate.TabIndex = 21
        '
        'frmExchangeRate_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 271)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExchangeRate_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Exchange Rate"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.gbCurrencyInfo.ResumeLayout(False)
        Me.gbCurrencyInfo.PerformLayout()
        CType(Me.nudDecimalPlaces, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbCurrencyInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents lblSign As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtCurrencySign As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objlblEqual As System.Windows.Forms.Label
    Friend WithEvents lblBasecurrency As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents objlblSign As System.Windows.Forms.Label
    Friend WithEvents lblDisplaySign As System.Windows.Forms.Label
    Friend WithEvents lblDecimalPlace As System.Windows.Forms.Label
    Friend WithEvents nudDecimalPlaces As System.Windows.Forms.NumericUpDown
    Friend WithEvents radCurrencySignSuffix As System.Windows.Forms.RadioButton
    Friend WithEvents radCurrencySignPrefix As System.Windows.Forms.RadioButton
    Friend WithEvents cboCurrencySign As System.Windows.Forms.ComboBox
    Friend WithEvents cboCurrencySignName As System.Windows.Forms.ComboBox
    Friend WithEvents txtExchangeRate As eZee.TextBox.NumericTextBox
    Friend WithEvents txtExchangeRate1 As eZee.TextBox.NumericTextBox
    Friend WithEvents txtExchangeRate2 As eZee.TextBox.NumericTextBox
    Friend WithEvents txtCurrencyName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDateOfRate As System.Windows.Forms.Label
    Friend WithEvents dtpDateOfRate As System.Windows.Forms.DateTimePicker
End Class
