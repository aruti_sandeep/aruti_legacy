﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStatutoryPayment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStatutoryPayment))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbAdvanceAmountInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.txtTotalPaymentAmount = New eZee.TextBox.NumericTextBox
        Me.lblTotalPaymentAmount = New System.Windows.Forms.Label
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvEmployeeList = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhEmpContribution = New System.Windows.Forms.ColumnHeader
        Me.colhEmplContribution = New System.Windows.Forms.ColumnHeader
        Me.colhTotContribution = New System.Windows.Forms.ColumnHeader
        Me.objcolhMembertranunkid = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbStatutoryPaymentInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblReceiptAmount = New System.Windows.Forms.Label
        Me.txtReceiptAmount = New eZee.TextBox.NumericTextBox
        Me.txtReceiptNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblReceiptNo = New System.Windows.Forms.Label
        Me.lblReceiptDate = New System.Windows.Forms.Label
        Me.dtpReceiptDate = New System.Windows.Forms.DateTimePicker
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblMembership = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbAdvanceAmountInfo.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.gbStatutoryPaymentInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbAdvanceAmountInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbStatutoryPaymentInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(735, 518)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbAdvanceAmountInfo
        '
        Me.gbAdvanceAmountInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAdvanceAmountInfo.Checked = False
        Me.gbAdvanceAmountInfo.CollapseAllExceptThis = False
        Me.gbAdvanceAmountInfo.CollapsedHoverImage = Nothing
        Me.gbAdvanceAmountInfo.CollapsedNormalImage = Nothing
        Me.gbAdvanceAmountInfo.CollapsedPressedImage = Nothing
        Me.gbAdvanceAmountInfo.CollapseOnLoad = False
        Me.gbAdvanceAmountInfo.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbAdvanceAmountInfo.ExpandedHoverImage = Nothing
        Me.gbAdvanceAmountInfo.ExpandedNormalImage = Nothing
        Me.gbAdvanceAmountInfo.ExpandedPressedImage = Nothing
        Me.gbAdvanceAmountInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAdvanceAmountInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAdvanceAmountInfo.HeaderHeight = 25
        Me.gbAdvanceAmountInfo.HeaderMessage = ""
        Me.gbAdvanceAmountInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAdvanceAmountInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAdvanceAmountInfo.HeightOnCollapse = 0
        Me.gbAdvanceAmountInfo.LeftTextSpace = 0
        Me.gbAdvanceAmountInfo.Location = New System.Drawing.Point(13, 399)
        Me.gbAdvanceAmountInfo.Name = "gbAdvanceAmountInfo"
        Me.gbAdvanceAmountInfo.OpenHeight = 300
        Me.gbAdvanceAmountInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAdvanceAmountInfo.ShowBorder = True
        Me.gbAdvanceAmountInfo.ShowCheckBox = False
        Me.gbAdvanceAmountInfo.ShowCollapseButton = False
        Me.gbAdvanceAmountInfo.ShowDefaultBorderColor = True
        Me.gbAdvanceAmountInfo.ShowDownButton = False
        Me.gbAdvanceAmountInfo.ShowHeader = True
        Me.gbAdvanceAmountInfo.Size = New System.Drawing.Size(329, 59)
        Me.gbAdvanceAmountInfo.TabIndex = 3
        Me.gbAdvanceAmountInfo.Temp = 0
        Me.gbAdvanceAmountInfo.Text = "Total Payment Amount"
        Me.gbAdvanceAmountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.txtTotalPaymentAmount)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblTotalPaymentAmount)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(324, 31)
        Me.pnlAdvanceTaken.TabIndex = 0
        '
        'txtTotalPaymentAmount
        '
        Me.txtTotalPaymentAmount.AllowNegative = True
        Me.txtTotalPaymentAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotalPaymentAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalPaymentAmount.DigitsInGroup = 3
        Me.txtTotalPaymentAmount.Flags = 0
        Me.txtTotalPaymentAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPaymentAmount.Location = New System.Drawing.Point(152, 6)
        Me.txtTotalPaymentAmount.MaxDecimalPlaces = 6
        Me.txtTotalPaymentAmount.MaxWholeDigits = 21
        Me.txtTotalPaymentAmount.Name = "txtTotalPaymentAmount"
        Me.txtTotalPaymentAmount.Prefix = ""
        Me.txtTotalPaymentAmount.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPaymentAmount.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPaymentAmount.ReadOnly = True
        Me.txtTotalPaymentAmount.Size = New System.Drawing.Size(167, 21)
        Me.txtTotalPaymentAmount.TabIndex = 0
        Me.txtTotalPaymentAmount.Text = "0"
        Me.txtTotalPaymentAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalPaymentAmount
        '
        Me.lblTotalPaymentAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPaymentAmount.Location = New System.Drawing.Point(4, 8)
        Me.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount"
        Me.lblTotalPaymentAmount.Size = New System.Drawing.Size(142, 16)
        Me.lblTotalPaymentAmount.TabIndex = 0
        Me.lblTotalPaymentAmount.Text = "Total Payment Amount"
        Me.lblTotalPaymentAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(329, 327)
        Me.gbEmployeeList.TabIndex = 0
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(182, 4)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(71, 16)
        Me.objlblEmpCount.TabIndex = 161
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvEmployeeList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(327, 301)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 17
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvEmployeeList
        '
        Me.lvEmployeeList.BackColorOnChecked = True
        Me.lvEmployeeList.CheckBoxes = True
        Me.lvEmployeeList.ColumnHeaders = Nothing
        Me.lvEmployeeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhCode, Me.colhName, Me.colhEmpContribution, Me.colhEmplContribution, Me.colhTotContribution, Me.objcolhMembertranunkid, Me.colhPeriod})
        Me.lvEmployeeList.CompulsoryColumns = ""
        Me.lvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmployeeList.FullRowSelect = True
        Me.lvEmployeeList.GridLines = True
        Me.lvEmployeeList.GroupingColumn = Nothing
        Me.lvEmployeeList.HideSelection = False
        Me.lvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmployeeList.MinColumnWidth = 50
        Me.lvEmployeeList.MultiSelect = False
        Me.lvEmployeeList.Name = "lvEmployeeList"
        Me.lvEmployeeList.OptionalColumns = ""
        Me.lvEmployeeList.ShowMoreItem = False
        Me.lvEmployeeList.ShowSaveItem = False
        Me.lvEmployeeList.ShowSelectAll = True
        Me.lvEmployeeList.ShowSizeAllColumnsToFit = True
        Me.lvEmployeeList.Size = New System.Drawing.Size(327, 301)
        Me.lvEmployeeList.Sortable = True
        Me.lvEmployeeList.TabIndex = 0
        Me.lvEmployeeList.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeList.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 0
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 150
        '
        'colhEmpContribution
        '
        Me.colhEmpContribution.Tag = "colhEmpContribution"
        Me.colhEmpContribution.Text = "Emp Contribution"
        Me.colhEmpContribution.Width = 0
        '
        'colhEmplContribution
        '
        Me.colhEmplContribution.Tag = "colhEmplContribution"
        Me.colhEmplContribution.Text = "Empl Contribution"
        Me.colhEmplContribution.Width = 0
        '
        'colhTotContribution
        '
        Me.colhTotContribution.Tag = "colhTotContribution"
        Me.colhTotContribution.Text = "Total Contribution"
        Me.colhTotContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTotContribution.Width = 140
        '
        'objcolhMembertranunkid
        '
        Me.objcolhMembertranunkid.Tag = "objcolhMembertranunkid"
        Me.objcolhMembertranunkid.Width = 0
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(735, 60)
        Me.eZeeHeader.TabIndex = 11
        Me.eZeeHeader.Title = "Statutory Payment"
        '
        'gbStatutoryPaymentInfo
        '
        Me.gbStatutoryPaymentInfo.BorderColor = System.Drawing.Color.Black
        Me.gbStatutoryPaymentInfo.Checked = False
        Me.gbStatutoryPaymentInfo.CollapseAllExceptThis = False
        Me.gbStatutoryPaymentInfo.CollapsedHoverImage = Nothing
        Me.gbStatutoryPaymentInfo.CollapsedNormalImage = Nothing
        Me.gbStatutoryPaymentInfo.CollapsedPressedImage = Nothing
        Me.gbStatutoryPaymentInfo.CollapseOnLoad = False
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.txtRemark)
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.lblRemark)
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.lblReceiptAmount)
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.txtReceiptAmount)
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.txtReceiptNo)
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.lblReceiptNo)
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.lblReceiptDate)
        Me.gbStatutoryPaymentInfo.Controls.Add(Me.dtpReceiptDate)
        Me.gbStatutoryPaymentInfo.ExpandedHoverImage = Nothing
        Me.gbStatutoryPaymentInfo.ExpandedNormalImage = Nothing
        Me.gbStatutoryPaymentInfo.ExpandedPressedImage = Nothing
        Me.gbStatutoryPaymentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStatutoryPaymentInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStatutoryPaymentInfo.HeaderHeight = 25
        Me.gbStatutoryPaymentInfo.HeaderMessage = ""
        Me.gbStatutoryPaymentInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStatutoryPaymentInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStatutoryPaymentInfo.HeightOnCollapse = 0
        Me.gbStatutoryPaymentInfo.LeftTextSpace = 0
        Me.gbStatutoryPaymentInfo.Location = New System.Drawing.Point(347, 219)
        Me.gbStatutoryPaymentInfo.Name = "gbStatutoryPaymentInfo"
        Me.gbStatutoryPaymentInfo.OpenHeight = 182
        Me.gbStatutoryPaymentInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStatutoryPaymentInfo.ShowBorder = True
        Me.gbStatutoryPaymentInfo.ShowCheckBox = False
        Me.gbStatutoryPaymentInfo.ShowCollapseButton = False
        Me.gbStatutoryPaymentInfo.ShowDefaultBorderColor = True
        Me.gbStatutoryPaymentInfo.ShowDownButton = False
        Me.gbStatutoryPaymentInfo.ShowHeader = True
        Me.gbStatutoryPaymentInfo.Size = New System.Drawing.Size(376, 239)
        Me.gbStatutoryPaymentInfo.TabIndex = 2
        Me.gbStatutoryPaymentInfo.Temp = 0
        Me.gbStatutoryPaymentInfo.Text = "Statutory Payment Information"
        Me.gbStatutoryPaymentInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(118, 115)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(238, 102)
        Me.txtRemark.TabIndex = 189
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(8, 117)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(104, 16)
        Me.lblRemark.TabIndex = 190
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReceiptAmount
        '
        Me.lblReceiptAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiptAmount.Location = New System.Drawing.Point(8, 90)
        Me.lblReceiptAmount.Name = "lblReceiptAmount"
        Me.lblReceiptAmount.Size = New System.Drawing.Size(104, 16)
        Me.lblReceiptAmount.TabIndex = 187
        Me.lblReceiptAmount.Text = "Receipt Amount"
        Me.lblReceiptAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtReceiptAmount
        '
        Me.txtReceiptAmount.AllowNegative = False
        Me.txtReceiptAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtReceiptAmount.DigitsInGroup = 0
        Me.txtReceiptAmount.Flags = 65536
        Me.txtReceiptAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReceiptAmount.Location = New System.Drawing.Point(118, 88)
        Me.txtReceiptAmount.MaxDecimalPlaces = 6
        Me.txtReceiptAmount.MaxWholeDigits = 20
        Me.txtReceiptAmount.Name = "txtReceiptAmount"
        Me.txtReceiptAmount.Prefix = ""
        Me.txtReceiptAmount.RangeMax = 1.7976931348623157E+308
        Me.txtReceiptAmount.RangeMin = -1.7976931348623157E+308
        Me.txtReceiptAmount.Size = New System.Drawing.Size(124, 21)
        Me.txtReceiptAmount.TabIndex = 2
        Me.txtReceiptAmount.Text = "0"
        Me.txtReceiptAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtReceiptNo
        '
        Me.txtReceiptNo.Flags = 0
        Me.txtReceiptNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReceiptNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReceiptNo.Location = New System.Drawing.Point(118, 34)
        Me.txtReceiptNo.Name = "txtReceiptNo"
        Me.txtReceiptNo.Size = New System.Drawing.Size(124, 21)
        Me.txtReceiptNo.TabIndex = 0
        '
        'lblReceiptNo
        '
        Me.lblReceiptNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiptNo.Location = New System.Drawing.Point(8, 36)
        Me.lblReceiptNo.Name = "lblReceiptNo"
        Me.lblReceiptNo.Size = New System.Drawing.Size(104, 16)
        Me.lblReceiptNo.TabIndex = 186
        Me.lblReceiptNo.Text = "Receipt No"
        Me.lblReceiptNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReceiptDate
        '
        Me.lblReceiptDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiptDate.Location = New System.Drawing.Point(8, 63)
        Me.lblReceiptDate.Name = "lblReceiptDate"
        Me.lblReceiptDate.Size = New System.Drawing.Size(104, 16)
        Me.lblReceiptDate.TabIndex = 156
        Me.lblReceiptDate.Text = "Receipt Date"
        Me.lblReceiptDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpReceiptDate
        '
        Me.dtpReceiptDate.Checked = False
        Me.dtpReceiptDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReceiptDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpReceiptDate.Location = New System.Drawing.Point(118, 61)
        Me.dtpReceiptDate.Name = "dtpReceiptDate"
        Me.dtpReceiptDate.ShowCheckBox = True
        Me.dtpReceiptDate.Size = New System.Drawing.Size(124, 21)
        Me.dtpReceiptDate.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboFromPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(347, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(376, 147)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(205, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(106, 13)
        Me.lnkAllocation.TabIndex = 310
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Advance Filter"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(305, 115)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(118, 115)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(181, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 118)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(104, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 150
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(118, 88)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(181, 21)
        Me.cboMembership.TabIndex = 2
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(349, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 90)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(104, 16)
        Me.lblMembership.TabIndex = 171
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(326, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.DropDownWidth = 150
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(118, 61)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(124, 21)
        Me.cboToPeriod.TabIndex = 1
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(104, 16)
        Me.lblToPeriod.TabIndex = 169
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(104, 16)
        Me.lblFromPeriod.TabIndex = 141
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromPeriod.DropDownWidth = 150
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(118, 34)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(124, 21)
        Me.cboFromPeriod.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblSearchEmp)
        Me.objFooter.Controls.Add(Me.txtSearchEmp)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 463)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(735, 55)
        Me.objFooter.TabIndex = 4
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(10, 22)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(98, 13)
        Me.lblSearchEmp.TabIndex = 13
        Me.lblSearchEmp.Text = "Search Employee"
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(110, 18)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(142, 21)
        Me.txtSearchEmp.TabIndex = 12
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(523, 13)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(97, 30)
        Me.btnProcess.TabIndex = 0
        Me.btnProcess.Text = "&Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(626, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmStatutoryPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 518)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStatutoryPayment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Statutory Payment"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbAdvanceAmountInfo.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.pnlAdvanceTaken.PerformLayout()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.gbStatutoryPaymentInfo.ResumeLayout(False)
        Me.gbStatutoryPaymentInfo.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbStatutoryPaymentInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblReceiptDate As System.Windows.Forms.Label
    Friend WithEvents dtpReceiptDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents txtReceiptAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtReceiptNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblReceiptNo As System.Windows.Forms.Label
    Friend WithEvents lblReceiptAmount As System.Windows.Forms.Label
    Friend WithEvents gbAdvanceAmountInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents txtTotalPaymentAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalPaymentAmount As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvEmployeeList As eZee.Common.eZeeListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTotContribution As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpContribution As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmplContribution As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents objcolhMembertranunkid As System.Windows.Forms.ColumnHeader
End Class
