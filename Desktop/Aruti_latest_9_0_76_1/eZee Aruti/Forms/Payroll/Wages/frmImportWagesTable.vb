﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmImportWagesTable

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmImportWagesTable"
    Private mblnCancel As Boolean = True
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As New ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    Private mDataview As DataView
    Private mintPeriodunkid As Integer = 0
    Private mdtFilteredData As New DataTable

#End Region

#Region " Display Dialog "
    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmImportWagesTable_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Try
            Dim objfrm As New frmLanguage

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsWagesTran.SetMessages()
            objfrm._Other_ModuleNames = "clsWagesTran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportWagesTable_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportWagesTable_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            txtFilePath.BackColor = GUI.ColorComp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportWagesTable_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub FillGrid()
        Try
            dgvWagesList.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "ischecked"
            dgcolhPeriodName.DataPropertyName = "periodname"
            dgcolhGradeGroupCode.DataPropertyName = "gradegroupcode"
            dgcolhGradeCode.DataPropertyName = "gradecode"
            dgcolhGradeLevelCode.DataPropertyName = "gradelevelcode"
            dgcolhMinimum.DataPropertyName = "minimum"
            dgcolhMinimum.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhMinimum.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhMinimum.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhMidpoint.DataPropertyName = "midpoint"
            dgcolhMidpoint.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhMidpoint.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhMidpoint.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhMaximum.DataPropertyName = "maximum"
            dgcolhMaximum.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhMaximum.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhMaximum.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhIncrement.DataPropertyName = "increment"
            dgcolhIncrement.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIncrement.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIncrement.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhRemark.DataPropertyName = "remark"

            dgvWagesList.DataSource = mDataview

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim opfldlg As New OpenFileDialog
            Dim objFileInfo As FileInfo
            Dim frm As New frmWagesTableMapping

            opfldlg.InitialDirectory = ConfigParameter._Object._ExportReportPath
            opfldlg.Filter = "Excel files (*.xlsx)|*.xlsx"
            opfldlg.FilterIndex = 0

            If opfldlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                objFileInfo = New FileInfo(opfldlg.FileName)
                txtFilePath.Text = opfldlg.FileName

                Select Case opfldlg.FilterIndex
                    Case 1, 2
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'dsList = objIExcel.Import(txtFilePath.Text)
                        dsList = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select

                If frm.displayDialog(dsList) = False Then
                    mblnCancel = True
                    Exit Sub
                End If

                mdtTable = frm._DataTable
                mintPeriodunkid = frm._Periodunkid

                mDataview = New DataView(mdtTable)

                mDataview.RowFilter = "rowtypeid <> 0 "
                If mDataview.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Some Wages will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button."), enMsgBoxStyle.Information)
                End If

                mDataview.RowFilter = "rowtypeid = 0 "

                Call FillGrid()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGetFileFomat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetFileFomat.Click
        Try
            Dim dsList As New DataSet
            Dim svfldlg As New SaveFileDialog
            Dim objFileInfo As FileInfo

            svfldlg.Filter = "Execl files(*.xlsx)|*.xlsx"

            If svfldlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                objFileInfo = New FileInfo(svfldlg.FileName)

                Dim dtTable As New DataTable("Import_Format")

                dtTable.Columns.Add("Grade Group Code")
                dtTable.Columns.Add("Grade Code")
                dtTable.Columns.Add("Grade Level Code")
                dtTable.Columns.Add("Minimum")
                dtTable.Columns.Add("Mid Point")
                dtTable.Columns.Add("Maximum")
                dtTable.Columns.Add("Increment")

                dsList.Tables.Add(dtTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'objIExcel.Export(svfldlg.FileName, dsList)
                OpenXML_Export(svfldlg.FileName, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Template Exported Successfully."), enMsgBoxStyle.Information)

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGetFileFomat_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click

        Dim blnFlag As Boolean = False
        Dim objWages As New clsWages
        Try
            If mdtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you can not Import this file. Reason: There is no transaction in this file to Import."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            mDataview.RowFilter = "rowtypeid = 0"
            mdtFilteredData = mDataview.ToTable

            mdtFilteredData = New DataView(mDataview.ToTable, "rowtypeid = 0 AND  ischecked = 1 ", "", DataViewRowState.CurrentRows).ToTable

            If mdtFilteredData.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            objWages._Userunkid = User._Object._Userunkid
            objWages._Isvoid = False
            objWages._Voiddatetime = Nothing
            objWages._Voiduserunkid = -1
            objWages._Voidreason = ""

            blnFlag = objWages.InsertUpdateDelete(mdtFilteredData, ConfigParameter._Object._CurrentDateAndTime, mintPeriodunkid)

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Wages Importation process completed successfully."), enMsgBoxStyle.Information)
                chkCheckAll.Checked = False
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim svfldlg As New SaveFileDialog

            If mDataview Is Nothing Then Exit Sub

            mDataview.Table.Columns.Remove("periodunkid")
            mDataview.Table.Columns.Remove("wagesunkid")
            mDataview.Table.Columns.Remove("gradegroupunkid")
            mDataview.Table.Columns.Remove("gradeunkid")
            mDataview.Table.Columns.Remove("gradelevelunkid")
            mDataview.Table.Columns.Remove("isvoid")
            mDataview.Table.Columns.Remove("voiduserunkid")
            mDataview.Table.Columns.Remove("voiddatetime")
            mDataview.Table.Columns.Remove("voidreason")
            mDataview.Table.Columns.Remove("ischecked")
            mDataview.Table.Columns.Remove("rowtypeid")

            mDataview.ToTable.AcceptChanges()

            svfldlg.Filter = "Execl files(*.xlsx)|*.xlsx"
            If svfldlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                If modGlobal.Export_ErrorList(svfldlg.FileName, mDataview.ToTable, "Wages Importation") = True Then
                    Process.Start(svfldlg.FileName)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub chkCheckAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCheckAll.CheckedChanged
        Try
            'If mDataview Is Nothing Then Exit Sub

            'For Each dRow As DataRow In mDataview
            '    dRow.Item("ischecked") = chkCheckAll.Checked
            'Next
            'dgvWagesList.DataSource = mDataview
            'dgvWagesList.Refresh()

            If dgvWagesList.RowCount > 0 Then
                For Each dRow As DataGridViewRow In dgvWagesList.Rows
                    dRow.Cells(objdgcolhCheck.Index).Value = chkCheckAll.Checked
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Menu Events "

    Private Sub mnuShowSuccessful_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If mDataview Is Nothing Then Exit Sub

            mDataview.RowFilter = "rowtypeid = 0"

            For Each dtRow As DataRowView In mDataview
                dtRow.Item("ischecked") = False
            Next

            chkCheckAll.Visible = True
            dgvWagesList.Columns(0).Visible = True
            Call FillGrid()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowUnSuccessful_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuShowUnSuccessful.Click
        Try
            btnImport.Enabled = False
            If mDataview Is Nothing Then Exit Sub

            mDataview.RowFilter = "rowtypeid <> 0"

            For Each dtRow As DataRowView In mDataview
                dtRow.Item("ischecked") = False
            Next

            chkCheckAll.Visible = False
            dgvWagesList.Columns(0).Visible = False
            Call FillGrid()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowUnSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuShowAll.Click
        Try
            btnImport.Enabled = False
            If mDataview Is Nothing Then Exit Sub

            mDataview.RowFilter = ""

            For Each dtRow As DataRowView In mDataview
                dtRow.Item("ischecked") = False
            Next

            chkCheckAll.Visible = True
            dgvWagesList.Columns(0).Visible = True
            Call FillGrid()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowAll_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try

            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnGetFileFomat.GradientBackColor = GUI._ButttonBackColor
            Me.btnGetFileFomat.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor

            Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.btnGetFileFomat.Text = Language._Object.getCaption(Me.btnGetFileFomat.Name, Me.btnGetFileFomat.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
            Me.chkCheckAll.Text = Language._Object.getCaption(Me.chkCheckAll.Name, Me.chkCheckAll.Text)
            Me.dgcolhPeriodName.HeaderText = Language._Object.getCaption(Me.dgcolhPeriodName.Name, Me.dgcolhPeriodName.HeaderText)
            Me.dgcolhGradeGroupCode.HeaderText = Language._Object.getCaption(Me.dgcolhGradeGroupCode.Name, Me.dgcolhGradeGroupCode.HeaderText)
            Me.dgcolhGradeCode.HeaderText = Language._Object.getCaption(Me.dgcolhGradeCode.Name, Me.dgcolhGradeCode.HeaderText)
            Me.dgcolhGradeLevelCode.HeaderText = Language._Object.getCaption(Me.dgcolhGradeLevelCode.Name, Me.dgcolhGradeLevelCode.HeaderText)
            Me.dgcolhMinimum.HeaderText = Language._Object.getCaption(Me.dgcolhMinimum.Name, Me.dgcolhMinimum.HeaderText)
            Me.dgcolhMidpoint.HeaderText = Language._Object.getCaption(Me.dgcolhMidpoint.Name, Me.dgcolhMidpoint.HeaderText)
            Me.dgcolhMaximum.HeaderText = Language._Object.getCaption(Me.dgcolhMaximum.Name, Me.dgcolhMaximum.HeaderText)
            Me.dgcolhIncrement.HeaderText = Language._Object.getCaption(Me.dgcolhIncrement.Name, Me.dgcolhIncrement.HeaderText)
            Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
            Me.mnuShowUnSuccessful.Text = Language._Object.getCaption(Me.mnuShowUnSuccessful.Name, Me.mnuShowUnSuccessful.Text)
            Me.mnuShowAll.Text = Language._Object.getCaption(Me.mnuShowAll.Name, Me.mnuShowAll.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Template Exported Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class