﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmWagetable_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmWagetable_AddEdit"
    Private mintGradeGroupUnkID As Integer = -1
    Private objWages As clsWages
    Private objWagesTran As clsWagesTran
    Private mdtTran As DataTable
    Private mintItemIndex As Integer = -1
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
        cboGradegroup.BackColor = GUI.ColorComp
            cboGrade.BackColor = GUI.ColorOptional 'Sohail (27 Apr 2016)
            dgWagetable.Columns(colhMinimum.Index).DefaultCellStyle.BackColor = GUI.ColorComp
        dgWagetable.Columns(colhIncrement.Index).DefaultCellStyle.BackColor = GUI.ColorOptional
            dgWagetable.Columns(colhMaximum.Index).DefaultCellStyle.BackColor = GUI.ColorComp
            dgWagetable.Columns(colhMidpoint.Index).DefaultCellStyle.BackColor = GUI.ColorComp 'Sohail (31 Mar 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    'Private Sub GetValue()
    '    Try
    '        objWagesTran._WageUnkID = objWages._Wagesunkid
    '        mdtTran = objWagesTran._DataList
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (27 Apr 2016) -- End

    Private Sub SetValue()
        Try
        objWages._Gradegroupunkid = CInt(cboGradegroup.SelectedValue)
            objWages._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
            objWages._Periodunkid = CInt(cboPeriod.SelectedValue) 'Sohail (27 Apr 2016)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objGradeGroup As New clsGradeGroup
        'Sohail (27 Apr 2016) -- Start
        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        'Sohail (27 Apr 2016) -- End
        Dim dsCombo As DataSet = Nothing
        Try

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
            With cboPeriod
                .DisplayMember = "name"
                .ValueMember = "periodunkid"
                .DataSource = dsCombo.Tables("period")
                If intFirstPeriodId > 0 Then
                    .SelectedValue = intFirstPeriodId
                Else
                    'S.SANDEEP [05-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#ARUTI-11}
                    '.SelectedIndex = 0
                    If dsCombo.Tables("period").Rows.Count > 0 Then .SelectedIndex = 0
                    'S.SANDEEP [05-Mar-2018] -- END
                End If
            End With
            'Sohail (27 Apr 2016) -- End

            dsCombo = objGradeGroup.getComboList("GradeGroup", True)
            With cboGradegroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("GradeGroup")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objGradeGroup = Nothing
        End Try
    End Sub

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    'Private Sub FillDataTable()
    '    Dim dtRow As DataRow = Nothing
    '    Try
    '        mdtTran.Rows.Clear()

    '        For i = 0 To dgWagetable.Rows.Count - 1
    '            dtRow = mdtTran.NewRow
    '            With dgWagetable.Rows(i)
    '                dtRow.Item("wagestranunkid") = CInt(.Cells(objcolhwagetranunkid.Index).Value)
    '                dtRow.Item("wagesunkid") = CInt(.Cells(objcolhwageunkid.Index).Value)
    '                dtRow.Item("gradeunkid") = CInt(.Cells(objcolhGradeID.Index).Value)
    '                dtRow.Item("gradelevelunkid") = CInt(.Cells(objcolhLevelID.Index).Value) 'Sohail (11 May 2011)
    '                dtRow.Item("salary") = CDec(.Cells(colhMinimum.Index).Value) 'Sohail (11 May 2011)
    '                dtRow.Item("increment") = CDec(.Cells(colhIncrement.Index).Value) 'Sohail (11 May 2011)
    '                dtRow.Item("maximum") = CDec(.Cells(colhMaximum.Index).Value)
    '                dtRow.Item("midpoint") = CDec(.Cells(colhMidpoint.Index).Value) 'Sohail (31 Mar 2012)
    '                dtRow.Item("isvoid") = False
    '                'Sohail (11 Nov 2010) -- Start
    '                'dtRow.Item("voiduserunkid") = DBNull.Value
    '                'Sohail (04 Mar 2011) -- Start
    '                'dtRow.Item("voiduserunkid") = User._Object._Userunkid
    '                dtRow.Item("voiduserunkid") = 0
    '                'Sohail (04 Mar 2011) -- End
    '                'Sohail (11 Nov 2010) -- End
    '                dtRow.Item("voiddatetime") = DBNull.Value
    '                dtRow.Item("voidreason") = ""
    '            End With
    '            mdtTran.Rows.Add(dtRow)
    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillDataTable", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub FillList()
        Try

            dgWagetable.AutoGenerateColumns = False

            objcolhPeriodunkid.DataPropertyName = "periodunkid"
            colhPeriod.DataPropertyName = "period_name"
            objcolhwagetranunkid.DataPropertyName = "wagestranunkid"
            objcolhwageunkid.DataPropertyName = "wagesunkid"
            objcolhGradeID.DataPropertyName = "gradeunkid"
            objcolhLevelID.DataPropertyName = "gradelevelunkid"
            colhGrade.DataPropertyName = "gradename"
            colhGradeLevel.DataPropertyName = "gradelevelname"
            colhMinimum.DataPropertyName = "Minimum"
            colhIncrement.DataPropertyName = "increment"
            colhMaximum.DataPropertyName = "maximum"
            colhMidpoint.DataPropertyName = "midpoint"
            objcolhPeriodStatus.DataPropertyName = "statusid"

            Dim dtView As DataView = New DataView(mdtTran)

            If CInt(cboGradegroup.SelectedValue) <= 0 Then
                dtView.RowFilter = " 1 = 2 "
            Else

                Dim strFilter As String = " gradegroupunkid = " & CInt(cboGradegroup.SelectedValue) & " "

                If CInt(cboGrade.SelectedValue) > 0 Then
                    strFilter &= " AND gradeunkid = " & CInt(cboGrade.SelectedValue) & " "
                End If

                dtView.RowFilter = strFilter
            End If

            dgWagetable.DataSource = dtView
            dgWagetable.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDataTable", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Apr 2016) -- End
#End Region

#Region " Private Functions "
    Private Function IsValidate() As Boolean
        Try
            If CInt(cboGradegroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Grade Group."), enMsgBoxStyle.Information)
                cboGradegroup.Focus()
                Exit Function
            ElseIf dgWagetable.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter wages in order to save."), enMsgBoxStyle.Information)
                cboGradegroup.Focus()
                Exit Function
            End If
            For i = 0 To dgWagetable.RowCount - 1
                With dgWagetable.Rows(i)

                    'Anjan (18 May 2011)-Start
                    'If cdec(.Cells(colhSalary.Index).Value) <= 0 Then
                    If CDec(.Cells(colhMinimum.Index).Value) < 0 Then
                        'Anjan (18 May 2011)-End 
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Enter Minimum Amount. Minimum Amount is mandatory information."), enMsgBoxStyle.Information)
                        .Cells(colhMinimum.Index).Selected = True
                        Exit Function
                        'Sohail (31 Mar 2012) -- Start
                        'TRA - ENHANCEMENT
                    ElseIf CDec(.Cells(colhMidpoint.Index).Value) < 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Midpoint amount should not be less than zero."), enMsgBoxStyle.Information)
                        .Cells(colhMidpoint.Index).Selected = True
                        Exit Function
                        'Sohail (31 Mar 2012) -- End
                    ElseIf CDec(.Cells(colhMaximum.Index).Value) <= 0 Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Enter Maximum Amount. Maximum Amount is mandatory information."), enMsgBoxStyle.Information)
                        .Cells(colhMaximum.Index).Selected = True
                        Exit Function
                    ElseIf CDec(.Cells(colhIncrement.Index).Value) < 0 Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Increment amount should not be less than zero."), enMsgBoxStyle.Information)
                        .Cells(colhIncrement.Index).Selected = True
                        Exit Function
                        'Sohail (16 Oct 2010) -- Start
                    ElseIf CDec(.Cells(colhMinimum.Index).Value) >= CDec(.Cells(colhMaximum.Index).Value) Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Maximum Amount should be greater than Minimum Amount."), enMsgBoxStyle.Information)
                        .Cells(colhMaximum.Index).Selected = True
                        Exit Function
                        'Sohail (31 Mar 2012) -- Start
                        'TRA - ENHANCEMENT
                    ElseIf CDec(.Cells(colhMidpoint.Index).Value) > CDec(.Cells(colhMaximum.Index).Value) OrElse CDec(.Cells(colhMidpoint.Index).Value) < CDec(.Cells(colhMinimum.Index).Value) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Midpoint Amount should be in between Minimum Amount and Maximum Amount."), enMsgBoxStyle.Information)
                        .Cells(colhMidpoint.Index).Selected = True
                        Exit Function
                        'Sohail (31 Mar 2012) -- End
                    ElseIf CDec(.Cells(colhIncrement.Index).Value) > (CDec(.Cells(colhMaximum.Index).Value) - CDec(.Cells(colhMinimum.Index).Value)) Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Increment Amount should fall in between Minimum Amount and Maximum Amount."), enMsgBoxStyle.Information)
                        .Cells(colhIncrement.Index).Selected = True 'Sohail (03 Nov 2010)
                        Exit Function
                        'Sohail (16 Oct 2010) -- End
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 81, "Please select period."), enMsgBoxStyle.Information)
                        cboPeriod.Focus()
                        Exit Function

                    ElseIf CInt(.Cells(objcolhPeriodStatus.Index).Value) = enStatusType.Close Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 77, "Sorry, You cannot change the Wages table information. Reason: Transaction period is already closed.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 78, "Please Copy Current Period slab."), enMsgBoxStyle.Information)
                        Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs)
                        Exit Function
                        'Sohail (27 Apr 2016) -- End
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function
#End Region

#Region " ComboBox's Events "

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim ds As DataSet
        Try
            mdtTran = Nothing
            If CInt(cboPeriod.SelectedValue) > 0 Then
                ds = objWages.GetWagesList(eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString), 0, 0, "Wages")

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    mdtTran = ds.Tables("Wages")
                    'Sohail (01 Jul 2016) -- Start
                    'Issue - 61.1 - Error conversion from DBNull to type Integer for newly created company if there is no transaction in wages master and tran table.
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    Dim lstRow As List(Of DataRow) = (From p In mdtTran Where (IsDBNull(p.Item("periodunkid")) = True) Select (p)).ToList
                    For Each dsRow As DataRow In lstRow
                        dsRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        dsRow.Item("period_code") = objPeriod._Period_Code
                        dsRow.Item("period_name") = objPeriod._Period_Name
                        dsRow.Item("start_date") = CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString
                        dsRow.Item("end_date") = CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString
                        dsRow.Item("statusid") = objPeriod._Statusid
                    Next
                    mdtTran.AcceptChanges()
                    'Sohail (01 Jul 2016) -- End
                End If
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Apr 2016) -- End

    Private Sub cboGradegroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradegroup.SelectedIndexChanged
        Dim objPayrollMaster As New clsPayrollMasterData
        Dim dsList As DataSet = Nothing
        'Sohail (27 Apr 2016) -- Start
        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
        'Dim dsWages As DataSet = Nothing
        'Dim dRow As DataRow() = Nothing
        'Sohail (27 Apr 2016) -- End
        Dim intWageID As Integer = -1
        'Sohail (27 Apr 2016) -- Start
        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
        Dim objGrade As New clsGrade
        'Dim dblSalary As Decimal = 0, dblIncrement As Decimal = 0, dblMaximum As Decimal = 0 'Sohail (11 May 2011)
        'Sohail (27 Apr 2016) -- End
        Try

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'dgWagetable.Rows.Clear()
            'objWages._Wagesunkid = -1
            'Call GetValue()

            'dsList = objWages.GetList(CInt(cboGradegroup.SelectedValue), "GradeGroup")
            'If dsList.Tables("GradeGroup").Rows.Count > 0 Then
            '    intWageID = CInt(dsList.Tables("GradeGroup").Rows(0).Item("wagesunkID").ToString)
            '    objWages._Wagesunkid = intWageID
            '    Call GetValue()
            'End If

            'dsList = objPayrollMaster.GetGradeGroupLevelList(CInt(cboGradegroup.SelectedValue), "GradeLevel", True)
            'For Each dsRow As DataRow In dsList.Tables("GradeLevel").Rows
            '    With dgWagetable
            '        .Rows.Add()

            '        .Rows(.Rows.Count - 1).Cells(objcolhwagetranunkid.Index).Value = -1
            '        .Rows(.Rows.Count - 1).Cells(objcolhwageunkid.Index).Value = intWageID
            '        .Rows(.Rows.Count - 1).Cells(objcolhGradeID.Index).Value = dsRow.Item("gradeunkid").ToString
            '        .Rows(.Rows.Count - 1).Cells(colhGrade.Index).Value = dsRow.Item("gradename").ToString
            '        .Rows(.Rows.Count - 1).Cells(objcolhLevelID.Index).Value = dsRow.Item("gradelevelunkid").ToString
            '        .Rows(.Rows.Count - 1).Cells(colhGradeLevel.Index).Value = dsRow.Item("levelname").ToString
            '        .Rows(.Rows.Count - 1).Cells(colhMinimum.Index).Value = 0
            '        .Rows(.Rows.Count - 1).Cells(colhIncrement.Index).Value = 0
            '        .Rows(.Rows.Count - 1).Cells(colhMaximum.Index).Value = 0
            '        .Rows(.Rows.Count - 1).Cells(colhMidpoint.Index).Value = 0 'Sohail (31 Mar 2012)

            '        If intWageID > 0 Then
            '            dRow = mdtTran.Select("wagesunkid=" & intWageID & " AND gradeunkid = " & CInt(dsRow.Item("gradeunkid").ToString) & " AND gradelevelunkid = " & CInt(dsRow.Item("gradelevelunkid").ToString))
            '            If dRow.Length > 0 Then
            '                .Rows(.Rows.Count - 1).Cells(objcolhwagetranunkid.Index).Value = CDec(dRow(0).Item("wagestranunkid").ToString)
            '                .Rows(.Rows.Count - 1).Cells(colhMinimum.Index).Value = CDec(dRow(0).Item("salary").ToString) 'Sohail (11 May 2011)
            '                .Rows(.Rows.Count - 1).Cells(colhIncrement.Index).Value = CDec(dRow(0).Item("increment").ToString) 'Sohail (11 May 2011)
            '                .Rows(.Rows.Count - 1).Cells(colhMaximum.Index).Value = CDec(dRow(0).Item("maximum").ToString) 'Sohail (11 May 2011)
            '                .Rows(.Rows.Count - 1).Cells(colhMidpoint.Index).Value = CDec(dRow(0).Item("midpoint").ToString) 'Sohail (31 Mar 2012)
            '            End If
            '        End If
            '    End With
            'Next
            If CInt(cboGradegroup.SelectedValue) <= 0 Then
            objWages._Wagesunkid = -1
                'Call GetValue()
            Else
            dsList = objWages.GetList(CInt(cboGradegroup.SelectedValue), "GradeGroup")
            If dsList.Tables("GradeGroup").Rows.Count > 0 Then
                intWageID = CInt(dsList.Tables("GradeGroup").Rows(0).Item("wagesunkID").ToString)
                objWages._Wagesunkid = intWageID
                Else
                    objWages._Wagesunkid = CInt(cboGradegroup.SelectedValue)
                End If
                'Call GetValue()
            End If

            dsList = objGrade.getComboList("Grade", True, CInt(cboGradegroup.SelectedValue))
            With cboGrade
                .DataSource = Nothing
                .DisplayMember = "name"
                .ValueMember = "gradeunkid"
                .DataSource = dsList.Tables("Grade")
                .SelectedValue = 0
                End With
            'Sohail (27 Apr 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradegroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objPayrollMaster = Nothing
        End Try
    End Sub

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Private Sub cboGrade_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedValueChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Apr 2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmWagetable_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objWages = Nothing
            objWagesTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWagetable_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWagetable_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    Windows.Forms.SendKeys.Send("{Tab}")
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWagetable_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWagetable_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objWages = New clsWages
        objWagesTran = New clsWagesTran

        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'Call GetValue()
            Call FillList()
            'Sohail (27 Apr 2016) -- End

            'Nilay (06-Jun-2016) -- Start
            'Enhancement : Import option in Wages Table for KBC
            mnuImport.Enabled = User._Object.Privilege._AllowToImportWagesTable
            mnuExport.Enabled = User._Object.Privilege._AllowToExportWagesTable
            'Nilay (06-Jun-2016) -- End

            If dgWagetable.Rows.Count > 0 Then dgWagetable.Rows(0).Selected = True
            dgWagetable.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWagetable_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'Call FillDataTable()
            Dim strFilter As String = ""
            'If CInt(cboGradegroup.SelectedValue) > 0 Then
            '    strFilter &= "gradegroupunkid = " & CInt(cboGradegroup.SelectedValue) & "  "
            'End If
            If CInt(cboGrade.SelectedValue) > 0 Then
                strFilter &= " gradeunkid = " & CInt(cboGrade.SelectedValue) & "  "
            End If
            mdtTran = New DataView(mdtTran, strFilter, "", DataViewRowState.CurrentRows).ToTable
            'Sohail (27 Apr 2016) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objWages.InsertUpdateDelete(mdtTran)
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'blnFlag = objWages.InsertUpdateDelete(mdtTran, ConfigParameter._Object._CurrentDateAndTime)
            blnFlag = objWages.InsertUpdateDelete(mdtTran, ConfigParameter._Object._CurrentDateAndTime, CInt(cboPeriod.SelectedValue))
            'Sohail (27 Apr 2016) -- End
            'Sohail (21 Aug 2015) -- End

            If blnFlag = False And objWages._Message <> "" Then
                eZeeMsgBox.Show(objWages._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                cboGradegroup.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (11 May 2011) -- Start
#Region " Datagridview Events "

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Private Sub dgWagetable_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgWagetable.CellBeginEdit
        Try
            If (dgWagetable.CurrentCell.ColumnIndex = colhMinimum.Index OrElse _
                dgWagetable.CurrentCell.ColumnIndex = colhMidpoint.Index OrElse _
                dgWagetable.CurrentCell.ColumnIndex = colhIncrement.Index OrElse _
                dgWagetable.CurrentCell.ColumnIndex = colhMaximum.Index) Then

                If CInt(dgWagetable.CurrentRow.Cells(objcolhPeriodStatus.Index).Value) = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 77, "Sorry, You cannot change the Wages table information. Reason: Transaction period is already closed.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 78, "Please Copy Current Period slab."), enMsgBoxStyle.Information)
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgWagetable_CellBeginEdit", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Apr 2016) -- End

    Private Sub dgWagetable_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgWagetable.EditingControlShowing
        Dim tb As TextBox
        Try
            'Sohail (31 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            If (dgWagetable.CurrentCell.ColumnIndex = colhMinimum.Index OrElse _
                dgWagetable.CurrentCell.ColumnIndex = colhMidpoint.Index OrElse _
                dgWagetable.CurrentCell.ColumnIndex = colhIncrement.Index OrElse _
                dgWagetable.CurrentCell.ColumnIndex = colhMaximum.Index) AndAlso TypeOf e.Control Is TextBox Then
                'If (dgWagetable.CurrentCell.ColumnIndex = colhMinimum.Index OrElse dgWagetable.CurrentCell.ColumnIndex = colhIncrement.Index OrElse dgWagetable.CurrentCell.ColumnIndex = colhMaximum.Index) AndAlso TypeOf e.Control Is TextBox Then
                'Sohail (31 Mar 2012) -- End
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress 'Sohail (31 Mar 2012) 
                AddHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgWagetable_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (11 May 2011) -- End

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
#Region " Menu Events "

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        Try
            If mdtTran Is Nothing Then Exit Sub

            Dim dt As DataTable = mdtTran.Copy

            dt.Columns.Remove("wagestranunkid")
            dt.Columns.Remove("wagesunkid")
            dt.Columns.Remove("gradegroupunkid")
            dt.Columns.Remove("gradeunkid")
            dt.Columns.Remove("gradelevelunkid")
            dt.Columns.Remove("isvoid")
            dt.Columns.Remove("voiduserunkid")
            dt.Columns.Remove("voiddatetime")
            dt.Columns.Remove("voidreason")

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"

            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                'If modGlobal.Export_ErrorList(savDialog.FileName, dt, "Import Grade") = True Then
                '    Process.Start(savDialog.FileName)
                'End If

                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim iExcel As New ExcelData
                'Dim dsDataSet As New DataSet
                'dsDataSet.Tables.Add(dt)
                'iExcel.Export(savDialog.FileName, dsDataSet)

                Dim dsDataSet As New DataSet
                dsDataSet.Tables.Add(dt)
                OpenXML_Export(savDialog.FileName, dsDataSet)
                'S.SANDEEP [12-Jan-2018] -- END


                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Data Exported Successfully."), enMsgBoxStyle.Information)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportAll_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuExportListed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportListed.Click
        Try
            If mdtTran Is Nothing Then Exit Sub

            Dim dtView As DataView = CType(dgWagetable.DataSource, DataView)

            Dim dt As DataTable = dtView.ToTable

            dt.Columns.Remove("wagestranunkid")
            dt.Columns.Remove("wagesunkid")
            dt.Columns.Remove("gradegroupunkid")
            dt.Columns.Remove("gradeunkid")
            dt.Columns.Remove("gradelevelunkid")
            dt.Columns.Remove("isvoid")
            dt.Columns.Remove("voiduserunkid")
            dt.Columns.Remove("voiddatetime")
            dt.Columns.Remove("voidreason")

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"

            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                'If modGlobal.Export_ErrorList(savDialog.FileName, dt, "Import Grade") = True Then
                '    Process.Start(savDialog.FileName)
                'End If

                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim iExcel As New ExcelData
                'Dim dsDataSet As New DataSet
                'dsDataSet.Tables.Add(dt)
                'iExcel.Export(savDialog.FileName, dsDataSet)

                Dim dsDataSet As New DataSet
                dsDataSet.Tables.Add(dt)
                OpenXML_Export(savDialog.FileName, dsDataSet)
                'S.SANDEEP [12-Jan-2018] -- END


                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Data Exported Successfully."), enMsgBoxStyle.Information)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportListed_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImport.Click
        Try
            'Nilay (06-Jun-2016) -- Start
            'Enhancement : Import option in Wages Table for KBC
            Dim frm As New frmImportWagesTable
            frm.ShowDialog()
            'Nilay (06-Jun-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Link Button Events "
    Private Sub lnkCopyCurrentSlab_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyEffectivePeriodSlab.LinkClicked
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 80, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            ElseIf CInt(cboGradegroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 81, "Please select Grade Group."), enMsgBoxStyle.Information)
                cboGradegroup.Focus()
                Exit Sub
            Else

                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 82, "Sorry, Selected period is already closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If mdtTran IsNot Nothing Then
                For Each dtRow As DataRow In mdtTran.Rows
                    dtRow.Item("wagesunkid") = -1
                    dtRow.Item("wagestranunkid") = -1
                    dtRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                    dtRow.Item("period_code") = objPeriod._Period_Code
                    dtRow.Item("period_name") = objPeriod._Period_Name
                    dtRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                    dtRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
                    dtRow.Item("statusid") = objPeriod._Statusid
                Next
                mdtTran.AcceptChanges()
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyCurrentSlab_LinkClicked", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (27 Apr 2016) -- End

#Region " Message List "
    '1, "Please Select Grade Group."
    '2, "No Data found to save."
    '3, "Please Enter Salary. Salary is mandatory information."
    '4, "Sorry, Increment amount should not be less than zero.
    '5, "Please Enter Maximum Amount. Maximum Amount is mandatory information."
    '6, "Sorry, Maximum Amount should be greater than Salary."
    '7, "Sorry, Increment Amount should fall in between Salary Amount and Maximum Amount."
#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbWagetable.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbWagetable.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbWagetable.Text = Language._Object.getCaption(Me.gbWagetable.Name, Me.gbWagetable.Text)
			Me.lblGradegroup.Text = Language._Object.getCaption(Me.lblGradegroup.Name, Me.lblGradegroup.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.colhGrade.HeaderText = Language._Object.getCaption(Me.colhGrade.Name, Me.colhGrade.HeaderText)
			Me.colhGradeLevel.HeaderText = Language._Object.getCaption(Me.colhGradeLevel.Name, Me.colhGradeLevel.HeaderText)
            Me.colhMinimum.HeaderText = Language._Object.getCaption(Me.colhMinimum.Name, Me.colhMinimum.HeaderText)
			Me.colhMidpoint.HeaderText = Language._Object.getCaption(Me.colhMidpoint.Name, Me.colhMidpoint.HeaderText)
			Me.colhMaximum.HeaderText = Language._Object.getCaption(Me.colhMaximum.Name, Me.colhMaximum.HeaderText)
			Me.colhIncrement.HeaderText = Language._Object.getCaption(Me.colhIncrement.Name, Me.colhIncrement.HeaderText)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuExport.Text = Language._Object.getCaption(Me.mnuExport.Name, Me.mnuExport.Text)
			Me.mnuImport.Text = Language._Object.getCaption(Me.mnuImport.Name, Me.mnuImport.Text)
			Me.mnuExportAll.Text = Language._Object.getCaption(Me.mnuExportAll.Name, Me.mnuExportAll.Text)
			Me.mnuExportListed.Text = Language._Object.getCaption(Me.mnuExportListed.Name, Me.mnuExportListed.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Grade Group.")
			Language.setMessage(mstrModuleName, 2, "Please enter wages in order to save.")
			Language.setMessage(mstrModuleName, 3, "Please Enter Minimum Amount. Minimum Amount is mandatory information.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Increment amount should not be less than zero.")
			Language.setMessage(mstrModuleName, 5, "Please Enter Maximum Amount. Maximum Amount is mandatory information.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Maximum Amount should be greater than Minimum Amount.")
			Language.setMessage(mstrModuleName, 7, "Sorry, Increment Amount should fall in between Minimum Amount and Maximum Amount.")
			Language.setMessage(mstrModuleName, 8, "Sorry, Midpoint amount should not be less than zero.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Midpoint Amount should be in between Minimum Amount and Maximum Amount.")
			Language.setMessage(mstrModuleName, 10, "Data Exported Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class