﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpDistributedCostCenter_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpDistributedCostCenter_AddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbCostCenterInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkNotAssignedHead = New System.Windows.Forms.CheckBox
        Me.chkShowNewlyHiredEmployees = New System.Windows.Forms.CheckBox
        Me.chkShowReinstatedEmployees = New System.Windows.Forms.CheckBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.chkIsExist = New System.Windows.Forms.CheckBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblPerc = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblSalaryDistrib = New System.Windows.Forms.Label
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.lblDefCC = New System.Windows.Forms.Label
        Me.objSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.objSearchED = New eZee.Common.eZeeGradientButton
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsPaymentDone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvEmpCostCenter = New System.Windows.Forms.DataGridView
        Me.objdgcolhID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAllocation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostCenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPerc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgColhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhTranHeadID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAllocationID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhCostCenterID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbgWorker = New System.ComponentModel.BackgroundWorker
        Me.btnStop = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbCostCenterInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvEmpCostCenter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbCostCenterInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.Panel1)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(1078, 612)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbCostCenterInfo
        '
        Me.gbCostCenterInfo.BorderColor = System.Drawing.Color.Black
        Me.gbCostCenterInfo.Checked = False
        Me.gbCostCenterInfo.CollapseAllExceptThis = False
        Me.gbCostCenterInfo.CollapsedHoverImage = Nothing
        Me.gbCostCenterInfo.CollapsedNormalImage = Nothing
        Me.gbCostCenterInfo.CollapsedPressedImage = Nothing
        Me.gbCostCenterInfo.CollapseOnLoad = False
        Me.gbCostCenterInfo.Controls.Add(Me.chkNotAssignedHead)
        Me.gbCostCenterInfo.Controls.Add(Me.chkShowNewlyHiredEmployees)
        Me.gbCostCenterInfo.Controls.Add(Me.chkShowReinstatedEmployees)
        Me.gbCostCenterInfo.Controls.Add(Me.lblAllocation)
        Me.gbCostCenterInfo.Controls.Add(Me.cboAllocation)
        Me.gbCostCenterInfo.Controls.Add(Me.chkIsExist)
        Me.gbCostCenterInfo.Controls.Add(Me.lnkAllocation)
        Me.gbCostCenterInfo.Controls.Add(Me.objbtnReset)
        Me.gbCostCenterInfo.Controls.Add(Me.objbtnSearch)
        Me.gbCostCenterInfo.Controls.Add(Me.btnDelete)
        Me.gbCostCenterInfo.Controls.Add(Me.btnEdit)
        Me.gbCostCenterInfo.Controls.Add(Me.btnAdd)
        Me.gbCostCenterInfo.Controls.Add(Me.lblPeriod)
        Me.gbCostCenterInfo.Controls.Add(Me.lblPerc)
        Me.gbCostCenterInfo.Controls.Add(Me.cboPeriod)
        Me.gbCostCenterInfo.Controls.Add(Me.lblSalaryDistrib)
        Me.gbCostCenterInfo.Controls.Add(Me.txtPercentage)
        Me.gbCostCenterInfo.Controls.Add(Me.lblDefCC)
        Me.gbCostCenterInfo.Controls.Add(Me.objSearchCostCenter)
        Me.gbCostCenterInfo.Controls.Add(Me.objSearchED)
        Me.gbCostCenterInfo.Controls.Add(Me.lblCostCenter)
        Me.gbCostCenterInfo.Controls.Add(Me.cboCostcenter)
        Me.gbCostCenterInfo.Controls.Add(Me.cboTranHead)
        Me.gbCostCenterInfo.Controls.Add(Me.lblTrnHead)
        Me.gbCostCenterInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbCostCenterInfo.Controls.Add(Me.cboEmployee)
        Me.gbCostCenterInfo.Controls.Add(Me.lblEmployee)
        Me.gbCostCenterInfo.ExpandedHoverImage = Nothing
        Me.gbCostCenterInfo.ExpandedNormalImage = Nothing
        Me.gbCostCenterInfo.ExpandedPressedImage = Nothing
        Me.gbCostCenterInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCostCenterInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCostCenterInfo.HeaderHeight = 25
        Me.gbCostCenterInfo.HeaderMessage = ""
        Me.gbCostCenterInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCostCenterInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCostCenterInfo.HeightOnCollapse = 0
        Me.gbCostCenterInfo.LeftTextSpace = 0
        Me.gbCostCenterInfo.Location = New System.Drawing.Point(257, 66)
        Me.gbCostCenterInfo.Name = "gbCostCenterInfo"
        Me.gbCostCenterInfo.OpenHeight = 91
        Me.gbCostCenterInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCostCenterInfo.ShowBorder = True
        Me.gbCostCenterInfo.ShowCheckBox = False
        Me.gbCostCenterInfo.ShowCollapseButton = False
        Me.gbCostCenterInfo.ShowDefaultBorderColor = True
        Me.gbCostCenterInfo.ShowDownButton = False
        Me.gbCostCenterInfo.ShowHeader = True
        Me.gbCostCenterInfo.Size = New System.Drawing.Size(810, 171)
        Me.gbCostCenterInfo.TabIndex = 0
        Me.gbCostCenterInfo.Temp = 0
        Me.gbCostCenterInfo.Text = "Employee Cost Center Info"
        Me.gbCostCenterInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNotAssignedHead
        '
        Me.chkNotAssignedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNotAssignedHead.Location = New System.Drawing.Point(13, 82)
        Me.chkNotAssignedHead.Name = "chkNotAssignedHead"
        Me.chkNotAssignedHead.Size = New System.Drawing.Size(429, 21)
        Me.chkNotAssignedHead.TabIndex = 322
        Me.chkNotAssignedHead.Text = "Show Employees Not Assigned Cost Center for Selected Head in Selected Period"
        Me.chkNotAssignedHead.UseVisualStyleBackColor = True
        '
        'chkShowNewlyHiredEmployees
        '
        Me.chkShowNewlyHiredEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowNewlyHiredEmployees.Location = New System.Drawing.Point(13, 59)
        Me.chkShowNewlyHiredEmployees.Name = "chkShowNewlyHiredEmployees"
        Me.chkShowNewlyHiredEmployees.Size = New System.Drawing.Size(282, 21)
        Me.chkShowNewlyHiredEmployees.TabIndex = 321
        Me.chkShowNewlyHiredEmployees.Text = "Show Only Newly Hired Employees in selected Period"
        Me.chkShowNewlyHiredEmployees.UseVisualStyleBackColor = True
        '
        'chkShowReinstatedEmployees
        '
        Me.chkShowReinstatedEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReinstatedEmployees.Location = New System.Drawing.Point(299, 59)
        Me.chkShowReinstatedEmployees.Name = "chkShowReinstatedEmployees"
        Me.chkShowReinstatedEmployees.Size = New System.Drawing.Size(273, 21)
        Me.chkShowReinstatedEmployees.TabIndex = 320
        Me.chkShowReinstatedEmployees.Text = "Show Only Reinstated Employees in selected Period"
        Me.chkShowReinstatedEmployees.UseVisualStyleBackColor = True
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(577, 36)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(67, 15)
        Me.lblAllocation.TabIndex = 318
        Me.lblAllocation.Text = "Allocation"
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.DropDownWidth = 160
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(655, 33)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(146, 21)
        Me.cboAllocation.TabIndex = 317
        '
        'chkIsExist
        '
        Me.chkIsExist.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsExist.Location = New System.Drawing.Point(322, 142)
        Me.chkIsExist.Name = "chkIsExist"
        Me.chkIsExist.Size = New System.Drawing.Size(161, 18)
        Me.chkIsExist.TabIndex = 315
        Me.chkIsExist.Text = "Overwrite if exist"
        Me.chkIsExist.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(641, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(106, 13)
        Me.lnkAllocation.TabIndex = 313
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Advance Filter"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(785, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 312
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(762, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 311
        Me.objbtnSearch.TabStop = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(700, 130)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 263
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(597, 130)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 262
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(489, 130)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 260
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(27, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(76, 15)
        Me.lblPeriod.TabIndex = 258
        Me.lblPeriod.Text = "As On Period"
        '
        'lblPerc
        '
        Me.lblPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerc.Location = New System.Drawing.Point(747, 90)
        Me.lblPerc.Name = "lblPerc"
        Me.lblPerc.Size = New System.Drawing.Size(32, 15)
        Me.lblPerc.TabIndex = 246
        Me.lblPerc.Text = "(%)"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 160
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(106, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(127, 21)
        Me.cboPeriod.TabIndex = 257
        '
        'lblSalaryDistrib
        '
        Me.lblSalaryDistrib.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryDistrib.Location = New System.Drawing.Point(549, 90)
        Me.lblSalaryDistrib.Name = "lblSalaryDistrib"
        Me.lblSalaryDistrib.Size = New System.Drawing.Size(102, 15)
        Me.lblSalaryDistrib.TabIndex = 245
        Me.lblSalaryDistrib.Text = "Salary Distribution"
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = False
        Me.txtPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 65536
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(657, 87)
        Me.txtPercentage.MaxDecimalPlaces = 6
        Me.txtPercentage.MaxWholeDigits = 21
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(85, 21)
        Me.txtPercentage.TabIndex = 3
        Me.txtPercentage.Text = "0"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDefCC
        '
        Me.lblDefCC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefCC.Location = New System.Drawing.Point(15, 106)
        Me.lblDefCC.Name = "lblDefCC"
        Me.lblDefCC.Size = New System.Drawing.Size(347, 31)
        Me.lblDefCC.TabIndex = 229
        Me.lblDefCC.Text = "Default Cost Center :"
        '
        'objSearchCostCenter
        '
        Me.objSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchCostCenter.BorderSelected = False
        Me.objSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchCostCenter.Location = New System.Drawing.Point(490, 103)
        Me.objSearchCostCenter.Name = "objSearchCostCenter"
        Me.objSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objSearchCostCenter.TabIndex = 6
        Me.objSearchCostCenter.Visible = False
        '
        'objSearchED
        '
        Me.objSearchED.BackColor = System.Drawing.Color.Transparent
        Me.objSearchED.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchED.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchED.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchED.BorderSelected = False
        Me.objSearchED.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchED.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchED.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchED.Location = New System.Drawing.Point(448, 103)
        Me.objSearchED.Name = "objSearchED"
        Me.objSearchED.Size = New System.Drawing.Size(21, 21)
        Me.objSearchED.TabIndex = 4
        Me.objSearchED.Visible = False
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(578, 66)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(74, 15)
        Me.lblCostCenter.TabIndex = 227
        Me.lblCostCenter.Text = "Cost Center"
        '
        'cboCostcenter
        '
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(656, 60)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(144, 21)
        Me.cboCostcenter.TabIndex = 2
        '
        'cboTranHead
        '
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(386, 33)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(163, 21)
        Me.cboTranHead.TabIndex = 1
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(283, 36)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(95, 15)
        Me.lblTrnHead.TabIndex = 220
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(400, 103)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(118, 147)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(196, 21)
        Me.cboEmployee.TabIndex = 0
        Me.cboEmployee.Visible = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(16, 150)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(85, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.Visible = False
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(6, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(245, 483)
        Me.gbEmployeeList.TabIndex = 8
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(122, 5)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblEmpCount.TabIndex = 160
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgEmployee)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchEmp)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(242, 454)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmpCode, Me.dgColhEmployee, Me.objdgcolhIsPaymentDone})
        Me.dgEmployee.Location = New System.Drawing.Point(1, 28)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(239, 423)
        Me.dgEmployee.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Emp. Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        '
        'objdgcolhIsPaymentDone
        '
        Me.objdgcolhIsPaymentDone.HeaderText = "IsPaymentDone"
        Me.objdgcolhIsPaymentDone.Name = "objdgcolhIsPaymentDone"
        Me.objdgcolhIsPaymentDone.ReadOnly = True
        Me.objdgcolhIsPaymentDone.Visible = False
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(238, 21)
        Me.txtSearchEmp.TabIndex = 12
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvEmpCostCenter)
        Me.Panel1.Location = New System.Drawing.Point(256, 244)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(815, 308)
        Me.Panel1.TabIndex = 1
        '
        'dgvEmpCostCenter
        '
        Me.dgvEmpCostCenter.AllowUserToAddRows = False
        Me.dgvEmpCostCenter.AllowUserToDeleteRows = False
        Me.dgvEmpCostCenter.AllowUserToResizeRows = False
        Me.dgvEmpCostCenter.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmpCostCenter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmpCostCenter.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhID, Me.dgcolhEmployee1, Me.dgcolhTranHead, Me.dgcolhAllocation, Me.dgcolhCostCenter, Me.dgcolhPerc, Me.objdgColhGUID, Me.dgcolhPeriod, Me.objdgcolhPeriodID, Me.objcolhTranHeadID, Me.objcolhAllocationID, Me.objcolhCostCenterID})
        Me.dgvEmpCostCenter.Location = New System.Drawing.Point(2, 9)
        Me.dgvEmpCostCenter.Name = "dgvEmpCostCenter"
        Me.dgvEmpCostCenter.ReadOnly = True
        Me.dgvEmpCostCenter.RowHeadersVisible = False
        Me.dgvEmpCostCenter.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmpCostCenter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpCostCenter.Size = New System.Drawing.Size(808, 294)
        Me.dgvEmpCostCenter.TabIndex = 149
        Me.dgvEmpCostCenter.TabStop = False
        '
        'objdgcolhID
        '
        Me.objdgcolhID.HeaderText = "ID"
        Me.objdgcolhID.Name = "objdgcolhID"
        Me.objdgcolhID.ReadOnly = True
        Me.objdgcolhID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhID.Visible = False
        Me.objdgcolhID.Width = 5
        '
        'dgcolhEmployee1
        '
        Me.dgcolhEmployee1.HeaderText = "Employee"
        Me.dgcolhEmployee1.Name = "dgcolhEmployee1"
        Me.dgcolhEmployee1.ReadOnly = True
        Me.dgcolhEmployee1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee1.Width = 200
        '
        'dgcolhTranHead
        '
        Me.dgcolhTranHead.HeaderText = "Transaction Head"
        Me.dgcolhTranHead.Name = "dgcolhTranHead"
        Me.dgcolhTranHead.ReadOnly = True
        Me.dgcolhTranHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTranHead.Width = 150
        '
        'dgcolhAllocation
        '
        Me.dgcolhAllocation.HeaderText = "Allocation"
        Me.dgcolhAllocation.Name = "dgcolhAllocation"
        Me.dgcolhAllocation.ReadOnly = True
        Me.dgcolhAllocation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCostCenter
        '
        Me.dgcolhCostCenter.HeaderText = "Cost Center"
        Me.dgcolhCostCenter.Name = "dgcolhCostCenter"
        Me.dgcolhCostCenter.ReadOnly = True
        Me.dgcolhCostCenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCostCenter.Width = 200
        '
        'dgcolhPerc
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgcolhPerc.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhPerc.HeaderText = "Perc.(%)"
        Me.dgcolhPerc.Name = "dgcolhPerc"
        Me.dgcolhPerc.ReadOnly = True
        Me.dgcolhPerc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPerc.Width = 75
        '
        'objdgColhGUID
        '
        Me.objdgColhGUID.HeaderText = "objcolhGUID"
        Me.objdgColhGUID.Name = "objdgColhGUID"
        Me.objdgColhGUID.ReadOnly = True
        Me.objdgColhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgColhGUID.Visible = False
        Me.objdgColhGUID.Width = 5
        '
        'dgcolhPeriod
        '
        Me.dgcolhPeriod.HeaderText = "Period"
        Me.dgcolhPeriod.Name = "dgcolhPeriod"
        Me.dgcolhPeriod.ReadOnly = True
        Me.dgcolhPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhPeriodID
        '
        Me.objdgcolhPeriodID.HeaderText = "PeriodID"
        Me.objdgcolhPeriodID.Name = "objdgcolhPeriodID"
        Me.objdgcolhPeriodID.ReadOnly = True
        Me.objdgcolhPeriodID.Visible = False
        Me.objdgcolhPeriodID.Width = 5
        '
        'objcolhTranHeadID
        '
        Me.objcolhTranHeadID.HeaderText = "TranHeadID"
        Me.objcolhTranHeadID.Name = "objcolhTranHeadID"
        Me.objcolhTranHeadID.ReadOnly = True
        Me.objcolhTranHeadID.Visible = False
        Me.objcolhTranHeadID.Width = 5
        '
        'objcolhAllocationID
        '
        Me.objcolhAllocationID.HeaderText = "AllocationID"
        Me.objcolhAllocationID.Name = "objcolhAllocationID"
        Me.objcolhAllocationID.ReadOnly = True
        Me.objcolhAllocationID.Visible = False
        Me.objcolhAllocationID.Width = 5
        '
        'objcolhCostCenterID
        '
        Me.objcolhCostCenterID.HeaderText = "CostCenterID"
        Me.objcolhCostCenterID.Name = "objcolhCostCenterID"
        Me.objcolhCostCenterID.ReadOnly = True
        Me.objcolhCostCenterID.Visible = False
        Me.objcolhCostCenterID.Width = 5
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(1078, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Employee Distributed Cost Center Add/Edit"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnStop)
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.lblSearchEmp)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 557)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1078, 55)
        Me.objFooter.TabIndex = 1
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(12, 13)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(98, 13)
        Me.lblSearchEmp.TabIndex = 10
        Me.lblSearchEmp.Text = "Search Employee"
        Me.lblSearchEmp.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(866, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(969, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        '
        'objbgWorker
        '
        Me.objbgWorker.WorkerReportsProgress = True
        Me.objbgWorker.WorkerSupportsCancellation = True
        '
        'btnStop
        '
        Me.btnStop.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStop.BackColor = System.Drawing.Color.White
        Me.btnStop.BackgroundImage = CType(resources.GetObject("btnStop.BackgroundImage"), System.Drawing.Image)
        Me.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStop.BorderColor = System.Drawing.Color.Empty
        Me.btnStop.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStop.FlatAppearance.BorderSize = 0
        Me.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStop.ForeColor = System.Drawing.Color.Black
        Me.btnStop.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStop.GradientForeColor = System.Drawing.Color.Black
        Me.btnStop.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Location = New System.Drawing.Point(751, 13)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Size = New System.Drawing.Size(109, 30)
        Me.btnStop.TabIndex = 15
        Me.btnStop.Text = "&Stop Process"
        Me.btnStop.UseVisualStyleBackColor = True
        Me.btnStop.Visible = False
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(556, 22)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(141, 13)
        Me.objlblProgress.TabIndex = 16
        '
        'frmEmpDistributedCostCenter_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1078, 612)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpDistributedCostCenter_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Distributed Cost Center Add/Edit"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbCostCenterInfo.ResumeLayout(False)
        Me.gbCostCenterInfo.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvEmpCostCenter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbCostCenterInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPerc As System.Windows.Forms.Label
    Friend WithEvents lblSalaryDistrib As System.Windows.Forms.Label
    Friend WithEvents txtPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDefCC As System.Windows.Forms.Label
    Friend WithEvents objSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objSearchED As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsPaymentDone As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents chkIsExist As System.Windows.Forms.CheckBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents dgvEmpCostCenter As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAllocation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPerc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgColhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhTranHeadID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAllocationID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhCostCenterID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkShowReinstatedEmployees As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowNewlyHiredEmployees As System.Windows.Forms.CheckBox
    Friend WithEvents chkNotAssignedHead As System.Windows.Forms.CheckBox
    Friend WithEvents objbgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnStop As eZee.Common.eZeeLightButton
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
End Class
