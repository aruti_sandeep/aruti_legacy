﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmPaySlipMsgList

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmPaySlipMsgList"
    Private objPayslipMsg As clsPayslipMessages_master

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboGrade.BackColor = GUI.ColorOptional
            cboCostCenter.BackColor = GUI.ColorOptional
            cboPayPoint.BackColor = GUI.ColorOptional
            cboDepartment.BackColor = GUI.ColorOptional
            cboJob.BackColor = GUI.ColorOptional
            cboSection.BackColor = GUI.ColorOptional
            cboClass.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objGrade As New clsGrade
        Dim objCostCenter As New clscostcenter_master
        Dim objPayPoint As New clspaypoint_master
        Dim objJob As New clsJobs
        Dim objDept As New clsDepartment
        Dim objSection As New clsSections
        Dim objClass As New clsClass
        Dim dsCombo As DataSet
        'Nilay (28-Apr-2016) -- Start
        'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
        Dim objPeriod As New clscommom_period_Tran
        'Nilay (28-Apr-2016) -- End

        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombo = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
            'End If
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeList", True)

            'Sohail (06 Jan 2012) -- End
            'Anjan [10 June 2015] -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("EmployeeList")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            dsCombo = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Grade")
                .SelectedValue = 0
            End With

            dsCombo = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombo.Tables("CostCenter")
                .SelectedValue = 0
            End With

            dsCombo = objPayPoint.getListForCombo("PayPoint", True)
            With cboPayPoint
                .ValueMember = "paypointunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPoint")
                .SelectedValue = 0
            End With

            dsCombo = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Job")
                .SelectedValue = 0
            End With

            dsCombo = objDept.getComboList("Dept", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Dept")
                .SelectedValue = 0
            End With

            dsCombo = objSection.getComboList("Section", True)
            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Section")
                .SelectedValue = 0
            End With

            dsCombo = objClass.getComboList("Class", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Class")
                .SelectedValue = 0
            End With

            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                                FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'Nilay (28-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
            objGrade = Nothing
            objCostCenter = Nothing
            objPayPoint = Nothing
            objJob = Nothing
            objDept = Nothing
            objSection = Nothing
            objClass = Nothing
            objPeriod = Nothing 'Nilay (28-Apr-2016)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim lvItem As ListViewItem
        Dim intAnalysisRefID As Integer = 0
        Try

            If User._Object.Privilege._AllowToViewPayslipMessageList = True Then    'Pinkal (09-Jul-2012) -- Start

                lvPayslipMsgList.Items.Clear()

                If rabEmployee.Checked Then
                    intAnalysisRefID = enAnalysisRef.Employee
                ElseIf rabGrade.Checked Then
                    intAnalysisRefID = enAnalysisRef.Grade
                ElseIf rabCostCenter.Checked Then
                    intAnalysisRefID = enAnalysisRef.CostCenter
                ElseIf rabPayPoint.Checked Then
                    intAnalysisRefID = enAnalysisRef.PayPoint
                ElseIf rabDepartment.Checked Then
                    intAnalysisRefID = enAnalysisRef.DeptInDeptGroup
                ElseIf rabSection.Checked Then
                    intAnalysisRefID = enAnalysisRef.SectionInDepartment
                ElseIf rabJob.Checked Then
                    intAnalysisRefID = enAnalysisRef.JobInJobGroup
                ElseIf rabClass.Checked Then
                    intAnalysisRefID = enAnalysisRef.ClassInClassGroup
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objPayslipMsg.GetList("PayslipMsg", False, intAnalysisRefID, CInt(cboEmployee.SelectedValue), CInt(cboGrade.SelectedValue), CInt(cboCostCenter.SelectedValue), CInt(cboPayPoint.SelectedValue), CInt(cboDepartment.SelectedValue), CInt(cboSection.SelectedValue), CInt(cboJob.SelectedValue), CInt(cboClass.SelectedValue), True)

                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                'dsList = objPayslipMsg.GetList(FinancialYear._Object._DatabaseName, _
                '                            User._Object._Userunkid, _
                '                            FinancialYear._Object._YearUnkid, _
                '                            Company._Object._Companyunkid, _
                '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                            ConfigParameter._Object._UserAccessModeSetting, True, _
                '                            ConfigParameter._Object._IsIncludeInactiveEmp, "PayslipMsg", False, intAnalysisRefID, CInt(cboEmployee.SelectedValue), CInt(cboGrade.SelectedValue), CInt(cboCostCenter.SelectedValue), CInt(cboPayPoint.SelectedValue), CInt(cboDepartment.SelectedValue), CInt(cboSection.SelectedValue), CInt(cboJob.SelectedValue), CInt(cboClass.SelectedValue), True)

                dsList = objPayslipMsg.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, "PayslipMsg", False, intAnalysisRefID, _
                                               CInt(cboEmployee.SelectedValue), CInt(cboGrade.SelectedValue), _
                                               CInt(cboCostCenter.SelectedValue), CInt(cboPayPoint.SelectedValue), _
                                               CInt(cboDepartment.SelectedValue), CInt(cboSection.SelectedValue), _
                                               CInt(cboJob.SelectedValue), CInt(cboClass.SelectedValue), True, , _
                                               CInt(cboPeriod.SelectedValue))
                'Nilay (28-Apr-2016) -- End

                'S.SANDEEP [04 JUN 2015] -- END

                For Each dsRow As DataRow In dsList.Tables("PayslipMsg").Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dsRow.Item("messageunkid").ToString
                    lvItem.Tag = CInt(dsRow.Item("messageunkid").ToString)

                    lvItem.SubItems.Add(dsRow.Item("Analysis").ToString)
                    lvItem.SubItems(colhAnalysisIndex.Index).Tag = CInt(dsRow.Item("analysisref_id").ToString)

                    lvItem.SubItems.Add(dsRow.Item("AnalysisName").ToString)
                    lvItem.SubItems(colhAnalysisName.Index).Tag = CInt(dsRow.Item("analysistranunkid").ToString)

                    'Nilay (28-Apr-2016) -- Start
                    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                    'lvItem.SubItems.Add(dsRow.Item("msg1").ToString + " " + dsRow.Item("msg2").ToString + " " + dsRow.Item("msg3").ToString + " " + dsRow.Item("msg4").ToString)
                    lvItem.SubItems.Add(dsRow.Item("period_name").ToString)
                    lvItem.SubItems.Add(dsRow.Item("msg1").ToString)
                    lvItem.SubItems.Add(dsRow.Item("msg2").ToString)
                    lvItem.SubItems.Add(dsRow.Item("msg3").ToString)
                    lvItem.SubItems.Add(dsRow.Item("msg4").ToString)
                    lvItem.SubItems.Add(dsRow.Item("statusid").ToString)
                    lvItem.SubItems(colhStatusid.Index).Tag = CInt(dsRow.Item("statusid").ToString)
                    'Nilay (28-Apr-2016) -- End

                    lvPayslipMsgList.Items.Add(lvItem)

                Next

                lvPayslipMsgList.GroupingColumn = colhAnalysisIndex
                lvPayslipMsgList.DisplayGroups(True)

                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                'If lvPayslipMsgList.Items.Count > 4 Then
                '    colhMessage1.Width = 640 - 18
                'Else
                '    colhMessage1.Width = 640
                'End If
                'Nilay (28-Apr-2016) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddPayslipMessage
            btnEdit.Enabled = User._Object.Privilege._EditPayslipMessage
            btnDelete.Enabled = User._Object.Privilege._DeletePayslipMessage
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmPaySlipMsgList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayslipMsg = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaySlipMsgList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaySlipMsgList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try

            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Close()
                Case Keys.Delete
                    btnDelete.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaySlipMsgList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaySlipMsgList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPayslipMsg = New clsPayslipMessages_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call SetVisibility()

            Call SetColor()
            Call FillCombo()

            Call FillList()

            If lvPayslipMsgList.Items.Count > 0 Then lvPayslipMsgList.Items(0).Selected = True
            lvPayslipMsgList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaySlipMsgList_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayslipMessages_master.SetMessages()
            objfrm._Other_ModuleNames = "clsPayslipMessages_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objFrm As New frmPaySlipMsg
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvPayslipMsgList.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditPayslipMessage = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvPayslipMsgList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payslip Message from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvPayslipMsgList.Select()
            Exit Sub
        End If

        'Nilay (28-Apr-2016) -- Start
        'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
        If CInt(lvPayslipMsgList.SelectedItems(0).Tag) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot Edit/Delete this Payslip Message. Reason: Period of selected message is closed."), enMsgBoxStyle.Information)
            lvPayslipMsgList.Focus()
            Exit Sub
        End If
        'Nilay (28-Apr-2016) -- End

        Dim objFrm As New frmPaySlipMsg
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPayslipMsgList.SelectedItems(0).Index

            If objFrm.displayDialog(CInt(lvPayslipMsgList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            lvPayslipMsgList.Items(intSelectedIndex).Selected = True
            lvPayslipMsgList.EnsureVisible(intSelectedIndex)
            lvPayslipMsgList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvPayslipMsgList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payslip Message from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvPayslipMsgList.Select()
            Exit Sub
        End If
        'Nilay (28-Apr-2016) -- Start
        'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
        If CInt(lvPayslipMsgList.SelectedItems(0).Tag) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot Edit/Delete this Payslip Message. Reason: Period of selected message is closed."), enMsgBoxStyle.Information)
            lvPayslipMsgList.Focus()
            Exit Sub
        End If
        'Nilay (28-Apr-2016) -- End
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPayslipMsgList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Payslip Message?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objPayslipMsg.Delete(CInt(lvPayslipMsgList.SelectedItems(0).Tag))
                lvPayslipMsgList.SelectedItems(0).Remove()

                If lvPayslipMsgList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvPayslipMsgList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvPayslipMsgList.Items.Count - 1
                    lvPayslipMsgList.Items(intSelectedIndex).Selected = True
                    lvPayslipMsgList.EnsureVisible(intSelectedIndex)
                ElseIf lvPayslipMsgList.Items.Count <> 0 Then
                    lvPayslipMsgList.Items(intSelectedIndex).Selected = True
                    lvPayslipMsgList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvPayslipMsgList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboPeriod.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPeriod.SelectedValue = .SelectedValue
                    cboPeriod.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (28-Apr-2016) -- End

#End Region

#Region " RadioButton's Event "
    Private Sub rabAllRadButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabAllGroups.CheckedChanged, rabEmployee.CheckedChanged, rabGrade.CheckedChanged, _
                                            rabCostCenter.CheckedChanged, rabPayPoint.CheckedChanged, rabDepartment.CheckedChanged, rabSection.CheckedChanged, rabJob.CheckedChanged, rabClass.CheckedChanged

        Dim rb As RadioButton = CType(sender, RadioButton)

        objbtnSearchEmployee.Enabled = False
        cboEmployee.Enabled = False
        cboGrade.Enabled = False
        cboCostCenter.Enabled = False
        cboPayPoint.Enabled = False
        cboDepartment.Enabled = False
        cboSection.Enabled = False
        cboJob.Enabled = False
        cboClass.Enabled = False

        cboEmployee.SelectedValue = 0
        cboGrade.SelectedValue = 0
        cboCostCenter.SelectedValue = 0
        cboPayPoint.SelectedValue = 0
        cboDepartment.SelectedValue = 0
        cboSection.SelectedValue = 0
        cboJob.SelectedValue = 0
        cboClass.SelectedValue = 0

        If rb.Name = rabEmployee.Name Then 'rabEmployee Checked
            cboEmployee.Enabled = True
            objbtnSearchEmployee.Enabled = True
        ElseIf rb.Name = rabGrade.Name Then
            cboGrade.Enabled = True
        ElseIf rb.Name = rabCostCenter.Name Then
            cboCostCenter.Enabled = True
        ElseIf rb.Name = rabPayPoint.Name Then
            cboPayPoint.Enabled = True
        ElseIf rb.Name = rabDepartment.Name Then
            cboDepartment.Enabled = True
        ElseIf rb.Name = rabSection.Name Then
            cboSection.Enabled = True
        ElseIf rb.Name = rabJob.Name Then
            cboJob.Enabled = True
        ElseIf rb.Name = rabClass.Name Then
            cboClass.Enabled = True
        End If
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'End If
            'Sohail (06 Jan 2012) -- End


            'Anjan [10 June 2015] -- End


            With cboEmployee
                objfrm.DataSource = CType(.DataSource, DataTable) 'Anjan [10 June 2015] -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            rabAllGroups.Checked = True
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Message List "
    '1, "Please select Payslip Message from the list to perform further operation on it."
    '2, "Are you sure you want to delete this Payslip Message?"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPayslipMsgList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPayslipMsgList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.rabAllGroups.Text = Language._Object.getCaption(Me.rabAllGroups.Name, Me.rabAllGroups.Text)
            Me.rabEmployee.Text = Language._Object.getCaption(Me.rabEmployee.Name, Me.rabEmployee.Text)
            Me.rabPayPoint.Text = Language._Object.getCaption(Me.rabPayPoint.Name, Me.rabPayPoint.Text)
            Me.rabCostCenter.Text = Language._Object.getCaption(Me.rabCostCenter.Name, Me.rabCostCenter.Text)
            Me.rabGrade.Text = Language._Object.getCaption(Me.rabGrade.Name, Me.rabGrade.Text)
            Me.rabJob.Text = Language._Object.getCaption(Me.rabJob.Name, Me.rabJob.Text)
            Me.rabSection.Text = Language._Object.getCaption(Me.rabSection.Name, Me.rabSection.Text)
            Me.rabDepartment.Text = Language._Object.getCaption(Me.rabDepartment.Name, Me.rabDepartment.Text)
            Me.rabClass.Text = Language._Object.getCaption(Me.rabClass.Name, Me.rabClass.Text)
            Me.gbPayslipMsgList.Text = Language._Object.getCaption(Me.gbPayslipMsgList.Name, Me.gbPayslipMsgList.Text)
            Me.lblAsOnPeriod.Text = Language._Object.getCaption(Me.lblAsOnPeriod.Name, Me.lblAsOnPeriod.Text)
            Me.colhUnkID.Text = Language._Object.getCaption(CStr(Me.colhUnkID.Tag), Me.colhUnkID.Text)
            Me.colhAnalysisIndex.Text = Language._Object.getCaption(CStr(Me.colhAnalysisIndex.Tag), Me.colhAnalysisIndex.Text)
            Me.colhAnalysisName.Text = Language._Object.getCaption(CStr(Me.colhAnalysisName.Tag), Me.colhAnalysisName.Text)
            Me.colhMessage1.Text = Language._Object.getCaption(CStr(Me.colhMessage1.Tag), Me.colhMessage1.Text)
            Me.colhMessage2.Text = Language._Object.getCaption(CStr(Me.colhMessage2.Tag), Me.colhMessage2.Text)
            Me.colhMessage3.Text = Language._Object.getCaption(CStr(Me.colhMessage3.Tag), Me.colhMessage3.Text)
            Me.colhMessage4.Text = Language._Object.getCaption(CStr(Me.colhMessage4.Tag), Me.colhMessage4.Text)
            Me.colhStatusid.Text = Language._Object.getCaption(CStr(Me.colhStatusid.Tag), Me.colhStatusid.Text)
            Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Payslip Message from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Payslip Message?")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot Edit/Delete this Payslip Message. Reason: Period of selected message is closed.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class