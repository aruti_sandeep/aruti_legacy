﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmPaySlipMsg

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmPaySlipMsg"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMessageUnkID As Integer = -1

    Private objPayslipMsg As clsPayslipMessages_master
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMessageUnkID = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintMessageUnkID

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private Sub FillCombo()
        Try
            Dim dsCombo As DataSet = Nothing
            Dim objPeriod As New clscommom_period_Tran

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                                FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
    'Nilay (28-Apr-2016) -- End

    Private Sub SetColor()
        Try
            txtMsg1.BackColor = GUI.ColorComp
            txtMsg2.BackColor = GUI.ColorOptional
            txtMsg3.BackColor = GUI.ColorOptional
            txtMsg4.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()

        Try
            With objPayslipMsg
                txtMsg1.Text = ._Msg1
                txtMsg2.Text = ._Msg2
                txtMsg3.Text = ._Msg3
                txtMsg4.Text = ._Msg4

                If menAction = enAction.EDIT_ONE Then
                    Select Case ._Analysisref_Id
                        Case enAnalysisRef.Employee
                            rabEmployee.Checked = True
                        Case enAnalysisRef.Grade
                            rabGrade.Checked = True
                        Case enAnalysisRef.SectionInDepartment
                            rabSectionDept.Checked = True
                        Case enAnalysisRef.CostCenter
                            rabCostCenter.Checked = True
                        Case enAnalysisRef.PayPoint
                            rabPayPoint.Checked = True
                        Case enAnalysisRef.DeptInDeptGroup
                            rabDeptGroup.Checked = True
                        Case enAnalysisRef.JobInJobGroup
                            rabJobnJobGrp.Checked = True
                        Case enAnalysisRef.ClassInClassGroup
                            rabClassnClassGrp.Checked = True
                    End Select
                    'Nilay (28-Apr-2016) -- Start
                    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                    cboPeriod.SelectedValue = ._Periodunkid
                    'Nilay (28-Apr-2016) -- End
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If rabEmployee.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.Employee
            ElseIf rabGrade.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.Grade
            ElseIf rabSection.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.Section
            ElseIf rabCostCenter.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.CostCenter
            ElseIf rabPayPoint.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.PayPoint
            ElseIf rabDeptGroup.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.DeptInDeptGroup
            ElseIf rabSectionDept.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.SectionInDepartment
            ElseIf rabJobnJobGrp.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.JobInJobGroup
            ElseIf rabClassnClassGrp.Checked Then
                objPayslipMsg._Analysisref_Id = enAnalysisRef.ClassInClassGroup
            End If

            With objPayslipMsg
                ._Analysistranunkid = CInt(lstList.CheckedItems(0).Tag)
                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                ._Periodunkid = CInt(cboPeriod.SelectedValue)
                If menAction = enAction.EDIT_ONE Then
                    ._Messageunkid = mintMessageUnkID
                End If
                'Nilay (28-Apr-2016) -- End
                ._Msg1 = txtMsg1.Text.Trim
                ._Msg2 = txtMsg2.Text.Trim
                ._Msg3 = txtMsg3.Text.Trim
                ._Msg4 = txtMsg4.Text.Trim

                ._Isglobalmessage = False
                ._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                ._Isactive = True
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private Function IsValidate() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select effective period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If lstList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one item from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lstList.Focus()
                Return False
            End If

            If txtMsg1.Text.Trim.Length <= 0 AndAlso (txtMsg2.Text.Trim.Length > 0 OrElse txtMsg3.Text.Trim.Length > 0 OrElse txtMsg4.Text.Trim.Length > 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter Message1. Message1 is mandatory information."), enMsgBoxStyle.Information)
                txtMsg1.Focus()
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function
    'Nilay (28-Apr-2016) -- End

#End Region

#Region " Form's Event "

    Private Sub frmPaySlipMsg_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayslipMsg = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaySlipMsg_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaySlipMsg_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    Windows.Forms.SendKeys.Send("{Tab}")
                    e.Handled = True
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaySlipMsg_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaySlipMsg_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPayslipMsg = New clsPayslipMessages_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End

            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            Call FillCombo()
            'Nilay (28-Apr-2016) -- End

            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objPayslipMsg._Messageunkid = mintMessageUnkID
                gbAnalysis.Enabled = False
                lstGroup.Enabled = False
                lstList.Enabled = False
                objchkSelectallGroup.Enabled = False
                objchkSelectallList.Enabled = False
                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                cboPeriod.Enabled = False
                objbtnSearchPeriod.Enabled = False
                'Nilay (28-Apr-2016) -- End

            Else
                rabEmployee.Checked = True
            End If
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaySlipMsg_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Event "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            'If lstList.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one item from the list to perform further operation on it."), enMsgBoxStyle.Information)
            '    lstList.Focus()
            '    Exit Sub
            'ElseIf txtMsg1.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter Message1. Message1 is mandatory information."), enMsgBoxStyle.Information)
            '    txtMsg1.Focus()
            '    Exit Sub
            'End If

            If IsValidate() = False Then Exit Sub
            'Nilay (28-Apr-2016) -- End

            'Gajanan [4-July-2020] -- Start
            Dim Analysistranunkis As String = ""
            For index As Integer = 0 To lstList.CheckedItems.Count - 1
                Analysistranunkis &= "," & lstList.CheckedItems(index).Tag.ToString()
            Next
            'Gajanan [4-July-2020] -- End

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objPayslipMsg.Update()
            Else
                blnFlag = objPayslipMsg.InsertAll(Mid(Analysistranunkis, 2), chkOverwrite.Checked)
            End If

            If blnFlag = False And objPayslipMsg._Message <> "" Then
                eZeeMsgBox.Show(objPayslipMsg._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objPayslipMsg = Nothing
                    objPayslipMsg = New clsPayslipMessages_master
                    Call GetValue()
                    txtMsg1.Focus()
                    'Nilay (28-Apr-2016) -- Start
                    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                    cboPeriod.SelectedValue = 0
                    'Nilay (28-Apr-2016) -- End
                Else
                    mintMessageUnkID = objPayslipMsg._Messageunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboPeriod.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPeriod.SelectedValue = .SelectedValue
                    cboPeriod.Focus()
                End If
            End With
        Catch ex As Exception

        End Try
    End Sub
    'Nilay (28-Apr-2016) -- End
#End Region

#Region " RadioButton's Event "
    Private Sub rabEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabEmployee.CheckedChanged
        Dim objEmployee As New clsEmployee_Master
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabEmployee.Checked Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsList As DataSet = objEmployee.GetList("Employee")
                Dim dsList As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                                            User._Object._Userunkid, _
                                                            FinancialYear._Object._YearUnkid, _
                                                            Company._Object._Companyunkid, _
                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                                            True, _
                                                            "Employee", _
                                                            ConfigParameter._Object._ShowFirstAppointmentDate)
                'S.SANDEEP [04 JUN 2015] -- END
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("Employee").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("name").ToString
                    lvItem.Tag = CInt(dsRow.Item("employeeunkid").ToString)
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                        lvItem.Checked = True
                    End If
                    lstList.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabEmployee_CheckedChanged", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub rabGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabGrade.CheckedChanged
        Dim objGradeGrp As New clsGradeGroup
        Dim objGrade As New clsGrade
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabGrade.Checked Then
                If menAction = enAction.EDIT_ONE Then
                    objGrade._Gradeunkid = objPayslipMsg._Analysistranunkid
                End If
                Dim dsList As DataSet = objGradeGrp.GetList("Grade", True)
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("Grade").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("name").ToString
                    lvItem.Tag = CInt(dsRow.Item("gradegroupunkid").ToString)
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objGrade._Gradegroupunkid Then
                        lvItem.Checked = True
                    End If
                    lstGroup.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabGrade_CheckedChanged", mstrModuleName)
        Finally
            objGradeGrp = Nothing
            objGrade = Nothing
        End Try
    End Sub

    Private Sub rabSection_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabSection.CheckedChanged
        Dim objSection As New clsSections
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabSection.Checked Then
                Dim dsList As DataSet = objSection.GetList("Section", True)
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("Section").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("name").ToString
                    lvItem.Tag = CInt(dsRow.Item("sectionunkid").ToString)
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                        lvItem.Checked = True
                    End If
                    lstList.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabSection_CheckedChanged", mstrModuleName)
        Finally
            objSection = Nothing
        End Try
    End Sub

    Private Sub rabCostCenter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabCostCenter.CheckedChanged
        Dim objCostcenterGrp As New clspayrollgroup_master
        Dim objCostCenter As New clscostcenter_master
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabCostCenter.Checked Then
                If menAction = enAction.EDIT_ONE Then
                    objCostCenter._Costcenterunkid = objPayslipMsg._Analysistranunkid
                End If
                Dim dsFill As DataSet = objCostcenterGrp.GetList("CostCenter", True)
                Dim dtTable As DataTable = New DataView(dsFill.Tables("CostCenter"), "grouptype_id=" & PayrollGroupType.CostCenter, "", DataViewRowState.CurrentRows).ToTable
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("groupname").ToString
                    lvItem.Tag = CInt(dsRow.Item("groupmasterunkid"))
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objCostCenter._Costcentergroupmasterunkid Then
                        lvItem.Checked = True
                    End If
                    lstGroup.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabCostCenter_CheckedChanged", mstrModuleName)
        Finally
            objCostcenterGrp = Nothing
            objCostCenter = Nothing
        End Try
    End Sub

    Private Sub rabPayPoint_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabPayPoint.CheckedChanged
        Dim objPaypoint As New clspaypoint_master
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabPayPoint.Checked Then
                Dim dsList As DataSet = objPaypoint.GetList("PayPoint", True)
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("PayPoint").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("paypointname").ToString
                    lvItem.Tag = CInt(dsRow.Item("paypointunkid").ToString)
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                        lvItem.Checked = True
                    End If
                    lstList.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabSection_CheckedChanged", mstrModuleName)
        Finally
            objPaypoint = Nothing
        End Try
    End Sub

    Private Sub rabDeptGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabDeptGroup.CheckedChanged
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objDept As New clsDepartment
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabDeptGroup.Checked Then
                If menAction = enAction.EDIT_ONE Then
                    objDept._Departmentunkid = objPayslipMsg._Analysistranunkid
                End If
                Dim dsList As DataSet = objDeptGroup.GetList("DeptGroup", True)
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("DeptGroup").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("name").ToString
                    lvItem.Tag = CInt(dsRow.Item("deptgroupunkid").ToString)
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objDept._Deptgroupunkid Then
                        lvItem.Checked = True
                    End If
                    lstGroup.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabDeptGroup_CheckedChanged", mstrModuleName)
        Finally
            objDeptGroup = Nothing
            objDept = Nothing
        End Try
    End Sub

    Private Sub rabSectionDept_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabSectionDept.CheckedChanged
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabSectionDept.Checked Then
                If menAction = enAction.EDIT_ONE Then
                    objSection._Sectionunkid = objPayslipMsg._Analysistranunkid
                End If
                Dim dsList As DataSet = objDepartment.GetList("Department", True)
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("Department").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("name").ToString
                    lvItem.Tag = CInt(dsRow.Item("departmentunkid").ToString)
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objSection._Departmentunkid Then
                        lvItem.Checked = True
                    End If
                    lstGroup.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabSectionDept_CheckedChanged", mstrModuleName)
        Finally
            objDepartment = Nothing
            objSection = Nothing
        End Try
    End Sub

    Private Sub rabJobnJobGrp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabJobnJobGrp.CheckedChanged
        Dim objJobGroup As New clsJobGroup
        Dim objJob As New clsJobs
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabJobnJobGrp.Checked Then
                If menAction = enAction.EDIT_ONE Then
                    objJob._Jobunkid = objPayslipMsg._Analysistranunkid
                End If
                Dim dsList As DataSet = objJobGroup.GetList("JobGroup", True)
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("JobGroup").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("name").ToString
                    lvItem.Tag = CInt(dsRow.Item("jobgroupunkid"))
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objJob._Jobgroupunkid Then
                        lvItem.Checked = True
                    End If
                    lstGroup.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabJobnJobGrp_CheckedChanged", mstrModuleName)
        Finally
            objJobGroup = Nothing
            objJob = Nothing
        End Try
    End Sub

    Private Sub rabClassnClassGrp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabClassnClassGrp.CheckedChanged
        Dim objClassGroup As New clsClassGroup
        Dim objClass As New clsClass
        Try
            lstGroup.Items.Clear()
            lstList.Items.Clear()
            objchkSelectallGroup.Checked = False
            objchkSelectallList.Checked = False
            If rabClassnClassGrp.Checked Then
                If menAction = enAction.EDIT_ONE Then
                    objClass._Classesunkid = objPayslipMsg._Analysistranunkid
                End If
                Dim dsList As DataSet = objClassGroup.GetList("ClassGroup", True)
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsList.Tables("ClassGroup").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dsRow.Item("name").ToString
                    lvItem.Tag = CInt(dsRow.Item("classgroupunkid").ToString)
                    If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objClass._Classgroupunkid Then
                        lvItem.Checked = True
                    End If
                    lstGroup.Items.Add(lvItem)
                Next
                lstGroup.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabClassnClassGrp_CheckedChanged", mstrModuleName)
        Finally
            objClassGroup = Nothing
            objClass = Nothing
        End Try
    End Sub
#End Region

#Region " CheckBox's Event "

    Private Sub chkSelectallGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectallGroup.CheckedChanged
        Try
            If lstGroup.Items.Count > 0 Then
                For Each lvItem As ListViewItem In lstGroup.Items
                    lvItem.Checked = objchkSelectallGroup.Checked
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectallGroup_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectallList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectallList.CheckedChanged
        Try
            If lstList.Items.Count > 0 Then
                For Each lvItem As ListViewItem In lstList.Items
                    lvItem.Checked = objchkSelectallList.Checked
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectallList_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Listview's Event "
    Private Sub lstGroup_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lstGroup.ItemChecked
        Dim dsFill As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim strGroupId As String = String.Empty
        Try

            If e.Item.Tag Is Nothing Or e.Item.Tag.ToString() = "" Then Exit Sub

            If lstGroup.CheckedItems.Count = 0 Then lstList.Items.Clear() : Exit Sub

            For i As Integer = 0 To lstGroup.CheckedItems.Count - 1
                strGroupId &= "'" & lstGroup.CheckedItems(i).Tag.ToString & "',"
            Next
            strGroupId = strGroupId.Substring(0, strGroupId.Length - 1)

            lstList.Items.Clear()
            objchkSelectallList.Checked = False

            'FOR GRADE GROUP
            If rabGrade.Checked Then
                If strGroupId.Length > 0 Then
                    Dim objGrade As New clsGrade
                    dsFill = objGrade.GetList("Grade", True)
                    dtTable = New DataView(dsFill.Tables("Grade"), "gradegroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Dim lvItem As ListViewItem
                    For Each dtRow As DataRow In dtTable.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow.Item("name").ToString
                        lvItem.Tag = CInt(dtRow.Item("gradeunkid").ToString)
                        If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                            lvItem.Checked = True
                        End If
                        lstList.Items.Add(lvItem)
                    Next
                End If

                'FOR COST CENTER GROUP
            ElseIf rabCostCenter.Checked Then
                If strGroupId.Length > 0 Then
                    Dim objCostCenter As New clscostcenter_master
                    dsFill = objCostCenter.GetList("CostCenter", True)
                    dtTable = New DataView(dsFill.Tables("CostCenter"), "costcentergroupmasterunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Dim lvItem As ListViewItem
                    For Each dtRow As DataRow In dtTable.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow.Item("costcentername").ToString
                        lvItem.Tag = CInt(dtRow.Item("costcenterunkid").ToString)
                        If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                            lvItem.Checked = True
                        End If
                        lstList.Items.Add(lvItem)
                    Next
                End If

                'FOR DEPARTMENT GROUP
            ElseIf rabDeptGroup.Checked Then
                If strGroupId.Length > 0 Then
                    Dim objDepartment As New clsDepartment
                    dsFill = objDepartment.GetList("Department", True)
                    dtTable = New DataView(dsFill.Tables("Department"), "deptgroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Dim lvItem As ListViewItem
                    For Each dtRow As DataRow In dtTable.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow.Item("name").ToString
                        lvItem.Tag = CInt(dtRow.Item("departmentunkid"))
                        If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                            lvItem.Checked = True
                        End If
                        lstList.Items.Add(lvItem)
                    Next
                End If

                'FOR SECTION IN DEPARTMENT GROUP
            ElseIf rabSectionDept.Checked Then
                If strGroupId.Length > 0 Then
                    Dim objSection As New clsSections
                    dsFill = objSection.GetList("Section", True)
                    dtTable = New DataView(dsFill.Tables("Section"), "departmentunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Dim lvItem As ListViewItem
                    For Each dtRow As DataRow In dtTable.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow.Item("name").ToString
                        lvItem.Tag = CInt(dtRow.Item("sectionunkid"))
                        If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                            lvItem.Checked = True
                        End If
                        lstList.Items.Add(lvItem)
                    Next
                End If

                'FOR JOB IN JOB GROUP
            ElseIf rabJobnJobGrp.Checked Then
                If strGroupId.Length > 0 Then
                    Dim objJob As New clsJobs
                    dsFill = objJob.GetList("Job", True)
                    dtTable = New DataView(dsFill.Tables("Job"), "jobgroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Dim lvItem As ListViewItem
                    For Each dtRow As DataRow In dtTable.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow.Item("JobName").ToString
                        lvItem.Tag = CInt(dtRow.Item("jobunkid"))
                        If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                            lvItem.Checked = True
                        End If
                        lstList.Items.Add(lvItem)
                    Next
                End If

                'FOR CLASS IN CLASS GROUP
            ElseIf rabClassnClassGrp.Checked Then
                If strGroupId.Length > 0 Then
                    Dim objClass As New clsClass
                    dsFill = objClass.GetList("Class", True)
                    dtTable = New DataView(dsFill.Tables("Class"), "classgroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Dim lvItem As ListViewItem
                    For Each dtRow As DataRow In dtTable.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow.Item("name").ToString
                        lvItem.Tag = CInt(dtRow.Item("classesunkid"))
                        If menAction = enAction.EDIT_ONE AndAlso CInt(lvItem.Tag) = objPayslipMsg._Analysistranunkid Then
                            lvItem.Checked = True
                        End If
                        lstList.Items.Add(lvItem)
                    Next
                End If

            End If
            lstList.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lstGroup_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'Gajanan [04-July-2020] -- Start
    'Private Sub lstList_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lstList.ItemCheck
    '    Try
    '        If e.NewValue = CheckState.Checked Then
    '            For Each lvItem As ListViewItem In lstList.CheckedItems
    '                lvItem.Checked = False
    '            Next
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lstList_ItemCheck", mstrModuleName)
    '    End Try
    'End Sub
    'Gajanan [04-July-2020] -- End

#End Region

#Region " Message "
    '1, "Please select atleast one item from the list to perform further operation on it."
    '2, "Please enter Message1. Message1 is mandatory information."
#End Region
    
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbPayslipMsg.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPayslipMsg.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbAnalysis.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAnalysis.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbPayslipMsg.Text = Language._Object.getCaption(Me.gbPayslipMsg.Name, Me.gbPayslipMsg.Text)
			Me.lblMsg2.Text = Language._Object.getCaption(Me.lblMsg2.Name, Me.lblMsg2.Text)
			Me.lblMsg4.Text = Language._Object.getCaption(Me.lblMsg4.Name, Me.lblMsg4.Text)
			Me.lblMsg3.Text = Language._Object.getCaption(Me.lblMsg3.Name, Me.lblMsg3.Text)
			Me.lblMsg1.Text = Language._Object.getCaption(Me.lblMsg1.Name, Me.lblMsg1.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbAnalysis.Text = Language._Object.getCaption(Me.gbAnalysis.Name, Me.gbAnalysis.Text)
			Me.rabPayPoint.Text = Language._Object.getCaption(Me.rabPayPoint.Name, Me.rabPayPoint.Text)
			Me.rabCostCenter.Text = Language._Object.getCaption(Me.rabCostCenter.Name, Me.rabCostCenter.Text)
			Me.rabSection.Text = Language._Object.getCaption(Me.rabSection.Name, Me.rabSection.Text)
			Me.rabClassnClassGrp.Text = Language._Object.getCaption(Me.rabClassnClassGrp.Name, Me.rabClassnClassGrp.Text)
			Me.rabGrade.Text = Language._Object.getCaption(Me.rabGrade.Name, Me.rabGrade.Text)
			Me.rabJobnJobGrp.Text = Language._Object.getCaption(Me.rabJobnJobGrp.Name, Me.rabJobnJobGrp.Text)
			Me.rabEmployee.Text = Language._Object.getCaption(Me.rabEmployee.Name, Me.rabEmployee.Text)
			Me.rabDeptGroup.Text = Language._Object.getCaption(Me.rabDeptGroup.Name, Me.rabDeptGroup.Text)
			Me.rabSectionDept.Text = Language._Object.getCaption(Me.rabSectionDept.Name, Me.rabSectionDept.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.lblList.Text = Language._Object.getCaption(Me.lblList.Name, Me.lblList.Text)
			Me.ColumnHeader1.Text = Language._Object.getCaption(CStr(Me.ColumnHeader1.Tag), Me.ColumnHeader1.Text)
			Me.ColumnHeader2.Text = Language._Object.getCaption(CStr(Me.ColumnHeader2.Tag), Me.ColumnHeader2.Text)
            Me.lblEffPeriod.Text = Language._Object.getCaption(Me.lblEffPeriod.Name, Me.lblEffPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one item from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Please enter Message1. Message1 is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select effective period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class