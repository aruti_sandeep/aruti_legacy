﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmBatchTransactionList

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmBatchTransactionList"
    Private objBatchTransaction As clsBatchTransaction
#End Region

#Region " Private Methods "
    Private Sub FillList()
        Dim dsBatch As New DataSet
        Try

            If User._Object.Privilege._AllowToViewBatchTransactionList = True Then                'Pinkal (09-Jul-2012) -- Start

                dsBatch = objBatchTransaction.GetList("TranHead")

                Dim lvItem As ListViewItem

                lvBatch.Items.Clear()
                For Each drRow As DataRow In dsBatch.Tables("TranHead").Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("batch_code").ToString
                    lvItem.Tag = drRow("batchtransactionunkid")
                    lvItem.SubItems.Add(drRow("batch_name").ToString)
                    lvBatch.Items.Add(lvItem)
                Next

                If lvBatch.Items.Count > 14 Then
                    colhBatchName.Width = 535 - 18
                Else
                    colhBatchName.Width = 535
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            dsBatch.Dispose()
            dsBatch = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddBatchTransaction
            btnEdit.Enabled = User._Object.Privilege._EditBatchTransaction
            btnDelete.Enabled = User._Object.Privilege._DeleteBatchTransaction

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmBatchTransactionList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBatchTransaction = Nothing
    End Sub

    Private Sub frmBatchTransactionList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = Keys.Escape Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchTransactionList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatchTransactionList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchTransactionList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatchTransactionList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBatchTransaction = New clsBatchTransaction
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            Call SetVisibility()
            Call FillList()

            If lvBatch.Items.Count > 0 Then lvBatch.Items(0).Selected = True
            lvBatch.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchTransactionList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBatchTransaction.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchTransaction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmBatchTransaction_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvBatch.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditBatchTransaction = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvBatch.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Batch from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBatch.Select()
            Exit Sub
        End If
        Dim frm As New frmBatchTransaction_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBatch.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(CInt(lvBatch.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvBatch.Items(intSelectedIndex).Selected = True
            lvBatch.EnsureVisible(intSelectedIndex)
            lvBatch.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvBatch.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Batch from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBatch.Select()
            Exit Sub
        End If
        If objBatchTransaction.isUsed(CInt(lvBatch.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Batch. Reason: This Batch is in use."), enMsgBoxStyle.Information) '?2
            lvBatch.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBatch.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Batch?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objBatchTransaction.Void(CInt(lvBatch.SelectedItems(0).Tag), User._Object._Userunkid, Now, "") 'Sohail (14 Oct 2010)
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                objBatchTransaction.Void(CInt(lvBatch.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                'Sandeep [ 16 Oct 2010 ] -- End 
                lvBatch.SelectedItems(0).Remove()

                If lvBatch.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBatch.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBatch.Items.Count - 1
                    lvBatch.Items(intSelectedIndex).Selected = True
                    lvBatch.EnsureVisible(intSelectedIndex)
                ElseIf lvBatch.Items.Count <> 0 Then
                    lvBatch.Items(intSelectedIndex).Selected = True
                    lvBatch.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBatch.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Message List "
    '1, "Please select Batch from the list to perform further operation on it."
    '2, "Please select Batch from the list to perform further operation on it."
    '3, "Sorry, You cannot delete this Batch. Reason: This Batch is in use."
    '4, "Are you sure you want to delete this Batch?"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

          
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.colhBatchCode.Text = Language._Object.getCaption(CStr(Me.colhBatchCode.Tag), Me.colhBatchCode.Text)
            Me.colhBatchName.Text = Language._Object.getCaption(CStr(Me.colhBatchName.Tag), Me.colhBatchName.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Batch from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Batch. Reason: This Batch is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Batch?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class