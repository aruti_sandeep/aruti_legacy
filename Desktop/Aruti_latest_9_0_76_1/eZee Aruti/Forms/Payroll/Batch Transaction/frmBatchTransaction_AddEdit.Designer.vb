﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBatchTransaction_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBatchTransaction_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.chkFindCheck = New System.Windows.Forms.CheckBox
        Me.lvTrnHeadList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhFindCheck = New System.Windows.Forms.ColumnHeader
        Me.objcolhFindTranHeadID = New System.Windows.Forms.ColumnHeader
        Me.colhFindTranHead = New System.Windows.Forms.ColumnHeader
        Me.colhFindHeadType = New System.Windows.Forms.ColumnHeader
        Me.colhFindTypeOf = New System.Windows.Forms.ColumnHeader
        Me.colhFindCalcType = New System.Windows.Forms.ColumnHeader
        Me.objbtnUnassign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblEffectiveFrom = New System.Windows.Forms.Label
        Me.cboCalcType = New System.Windows.Forms.ComboBox
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.lblTypeOf = New System.Windows.Forms.Label
        Me.cboTypeOf = New System.Windows.Forms.ComboBox
        Me.gbBatchInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblBatchCode = New System.Windows.Forms.Label
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.txtBatchCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtBatchName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlTransactionHead = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvBatchHeads = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.objcolhTranHeadID = New System.Windows.Forms.ColumnHeader
        Me.colhTranHead = New System.Windows.Forms.ColumnHeader
        Me.colhHeadtype = New System.Windows.Forms.ColumnHeader
        Me.colhTypeOf = New System.Windows.Forms.ColumnHeader
        Me.colhCalcType = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBatchInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlTransactionHead.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.Panel1)
        Me.pnlMainInfo.Controls.Add(Me.objbtnUnassign)
        Me.pnlMainInfo.Controls.Add(Me.objbtnAssign)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.gbBatchInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.pnlTransactionHead)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(737, 527)
        Me.pnlMainInfo.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkFindCheck)
        Me.Panel1.Controls.Add(Me.lvTrnHeadList)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(12, 135)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(327, 330)
        Me.Panel1.TabIndex = 248
        '
        'chkFindCheck
        '
        Me.chkFindCheck.AutoSize = True
        Me.chkFindCheck.Location = New System.Drawing.Point(8, 4)
        Me.chkFindCheck.Name = "chkFindCheck"
        Me.chkFindCheck.Size = New System.Drawing.Size(15, 14)
        Me.chkFindCheck.TabIndex = 18
        Me.chkFindCheck.UseVisualStyleBackColor = True
        '
        'lvTrnHeadList
        '
        Me.lvTrnHeadList.BackColorOnChecked = True
        Me.lvTrnHeadList.CheckBoxes = True
        Me.lvTrnHeadList.ColumnHeaders = Nothing
        Me.lvTrnHeadList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhFindCheck, Me.objcolhFindTranHeadID, Me.colhFindTranHead, Me.colhFindHeadType, Me.colhFindTypeOf, Me.colhFindCalcType})
        Me.lvTrnHeadList.CompulsoryColumns = ""
        Me.lvTrnHeadList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvTrnHeadList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTrnHeadList.FullRowSelect = True
        Me.lvTrnHeadList.GridLines = True
        Me.lvTrnHeadList.GroupingColumn = Nothing
        Me.lvTrnHeadList.HideSelection = False
        Me.lvTrnHeadList.Location = New System.Drawing.Point(0, 0)
        Me.lvTrnHeadList.MinColumnWidth = 50
        Me.lvTrnHeadList.MultiSelect = False
        Me.lvTrnHeadList.Name = "lvTrnHeadList"
        Me.lvTrnHeadList.OptionalColumns = ""
        Me.lvTrnHeadList.ShowMoreItem = False
        Me.lvTrnHeadList.ShowSaveItem = False
        Me.lvTrnHeadList.ShowSelectAll = True
        Me.lvTrnHeadList.ShowSizeAllColumnsToFit = True
        Me.lvTrnHeadList.Size = New System.Drawing.Size(327, 330)
        Me.lvTrnHeadList.Sortable = True
        Me.lvTrnHeadList.TabIndex = 0
        Me.lvTrnHeadList.UseCompatibleStateImageBehavior = False
        Me.lvTrnHeadList.View = System.Windows.Forms.View.Details
        '
        'objcolhFindCheck
        '
        Me.objcolhFindCheck.Text = ""
        Me.objcolhFindCheck.Width = 30
        '
        'objcolhFindTranHeadID
        '
        Me.objcolhFindTranHeadID.Width = 0
        '
        'colhFindTranHead
        '
        Me.colhFindTranHead.Tag = "colhFindTranHead"
        Me.colhFindTranHead.Text = "Transaction Head Name"
        Me.colhFindTranHead.Width = 150
        '
        'colhFindHeadType
        '
        Me.colhFindHeadType.Tag = "colhFindHeadType"
        Me.colhFindHeadType.Text = "Transaction Head Type"
        Me.colhFindHeadType.Width = 140
        '
        'colhFindTypeOf
        '
        Me.colhFindTypeOf.Tag = "colhFindTypeOf"
        Me.colhFindTypeOf.Text = "Type Of"
        Me.colhFindTypeOf.Width = 100
        '
        'colhFindCalcType
        '
        Me.colhFindCalcType.Tag = "colhFindCalcType"
        Me.colhFindCalcType.Text = "Calc. Type"
        Me.colhFindCalcType.Width = 100
        '
        'objbtnUnassign
        '
        Me.objbtnUnassign.BackColor = System.Drawing.Color.White
        Me.objbtnUnassign.BackgroundImage = CType(resources.GetObject("objbtnUnassign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUnassign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUnassign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUnassign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUnassign.FlatAppearance.BorderSize = 0
        Me.objbtnUnassign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUnassign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUnassign.ForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUnassign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnassign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnUnassign.Location = New System.Drawing.Point(349, 271)
        Me.objbtnUnassign.Name = "objbtnUnassign"
        Me.objbtnUnassign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnassign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnUnassign.TabIndex = 247
        Me.objbtnUnassign.UseVisualStyleBackColor = True
        '
        'objbtnAssign
        '
        Me.objbtnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAssign.BackgroundImage = CType(resources.GetObject("objbtnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnAssign.Location = New System.Drawing.Point(349, 225)
        Me.objbtnAssign.Name = "objbtnAssign"
        Me.objbtnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAssign.TabIndex = 246
        Me.objbtnAssign.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblEffectiveFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrnHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.lblCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.lblTypeOf)
        Me.gbFilterCriteria.Controls.Add(Me.cboTypeOf)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(355, 117)
        Me.gbFilterCriteria.TabIndex = 245
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Transaction Head Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(328, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(304, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'lblEffectiveFrom
        '
        Me.lblEffectiveFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveFrom.Location = New System.Drawing.Point(8, 36)
        Me.lblEffectiveFrom.Name = "lblEffectiveFrom"
        Me.lblEffectiveFrom.Size = New System.Drawing.Size(133, 15)
        Me.lblEffectiveFrom.TabIndex = 234
        Me.lblEffectiveFrom.Text = "Tran. Head Type"
        '
        'cboCalcType
        '
        Me.cboCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcType.FormattingEnabled = True
        Me.cboCalcType.Location = New System.Drawing.Point(147, 87)
        Me.cboCalcType.Name = "cboCalcType"
        Me.cboCalcType.Size = New System.Drawing.Size(179, 21)
        Me.cboCalcType.TabIndex = 231
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(147, 33)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(179, 21)
        Me.cboTrnHeadType.TabIndex = 229
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(8, 90)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(133, 15)
        Me.lblCalcType.TabIndex = 232
        Me.lblCalcType.Text = "Calculation Type"
        '
        'lblTypeOf
        '
        Me.lblTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOf.Location = New System.Drawing.Point(8, 63)
        Me.lblTypeOf.Name = "lblTypeOf"
        Me.lblTypeOf.Size = New System.Drawing.Size(133, 15)
        Me.lblTypeOf.TabIndex = 236
        Me.lblTypeOf.Text = "Transaction Type Of"
        '
        'cboTypeOf
        '
        Me.cboTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOf.FormattingEnabled = True
        Me.cboTypeOf.Location = New System.Drawing.Point(147, 60)
        Me.cboTypeOf.Name = "cboTypeOf"
        Me.cboTypeOf.Size = New System.Drawing.Size(179, 21)
        Me.cboTypeOf.TabIndex = 230
        '
        'gbBatchInfo
        '
        Me.gbBatchInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBatchInfo.Checked = False
        Me.gbBatchInfo.CollapseAllExceptThis = False
        Me.gbBatchInfo.CollapsedHoverImage = Nothing
        Me.gbBatchInfo.CollapsedNormalImage = Nothing
        Me.gbBatchInfo.CollapsedPressedImage = Nothing
        Me.gbBatchInfo.CollapseOnLoad = False
        Me.gbBatchInfo.Controls.Add(Me.lblBatchCode)
        Me.gbBatchInfo.Controls.Add(Me.lblBatchName)
        Me.gbBatchInfo.Controls.Add(Me.txtBatchCode)
        Me.gbBatchInfo.Controls.Add(Me.txtBatchName)
        Me.gbBatchInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbBatchInfo.ExpandedHoverImage = Nothing
        Me.gbBatchInfo.ExpandedNormalImage = Nothing
        Me.gbBatchInfo.ExpandedPressedImage = Nothing
        Me.gbBatchInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBatchInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBatchInfo.HeaderHeight = 25
        Me.gbBatchInfo.HeaderMessage = ""
        Me.gbBatchInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBatchInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBatchInfo.HeightOnCollapse = 0
        Me.gbBatchInfo.LeftTextSpace = 0
        Me.gbBatchInfo.Location = New System.Drawing.Point(373, 12)
        Me.gbBatchInfo.Name = "gbBatchInfo"
        Me.gbBatchInfo.OpenHeight = 182
        Me.gbBatchInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBatchInfo.ShowBorder = True
        Me.gbBatchInfo.ShowCheckBox = False
        Me.gbBatchInfo.ShowCollapseButton = False
        Me.gbBatchInfo.ShowDefaultBorderColor = True
        Me.gbBatchInfo.ShowDownButton = False
        Me.gbBatchInfo.ShowHeader = True
        Me.gbBatchInfo.Size = New System.Drawing.Size(355, 117)
        Me.gbBatchInfo.TabIndex = 3
        Me.gbBatchInfo.Temp = 0
        Me.gbBatchInfo.Text = "Batch Information"
        Me.gbBatchInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchCode
        '
        Me.lblBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchCode.Location = New System.Drawing.Point(15, 33)
        Me.lblBatchCode.Name = "lblBatchCode"
        Me.lblBatchCode.Size = New System.Drawing.Size(109, 15)
        Me.lblBatchCode.TabIndex = 139
        Me.lblBatchCode.Text = "Batch Code"
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(15, 60)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(109, 15)
        Me.lblBatchName.TabIndex = 140
        Me.lblBatchName.Text = "Batch Name"
        '
        'txtBatchCode
        '
        Me.txtBatchCode.Flags = 0
        Me.txtBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchCode.Location = New System.Drawing.Point(130, 30)
        Me.txtBatchCode.MaxLength = 50
        Me.txtBatchCode.Name = "txtBatchCode"
        Me.txtBatchCode.Size = New System.Drawing.Size(117, 21)
        Me.txtBatchCode.TabIndex = 0
        '
        'txtBatchName
        '
        Me.txtBatchName.Flags = 0
        Me.txtBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchName.Location = New System.Drawing.Point(130, 57)
        Me.txtBatchName.MaxLength = 50
        Me.txtBatchName.Name = "txtBatchName"
        Me.txtBatchName.Size = New System.Drawing.Size(188, 21)
        Me.txtBatchName.TabIndex = 1
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(324, 57)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 161
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 472)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(737, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(525, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(628, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlTransactionHead
        '
        Me.pnlTransactionHead.Controls.Add(Me.chkSelectAll)
        Me.pnlTransactionHead.Controls.Add(Me.lvBatchHeads)
        Me.pnlTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlTransactionHead.Location = New System.Drawing.Point(401, 135)
        Me.pnlTransactionHead.Name = "pnlTransactionHead"
        Me.pnlTransactionHead.Size = New System.Drawing.Size(327, 330)
        Me.pnlTransactionHead.TabIndex = 143
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 18
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'lvBatchHeads
        '
        Me.lvBatchHeads.BackColorOnChecked = True
        Me.lvBatchHeads.CheckBoxes = True
        Me.lvBatchHeads.ColumnHeaders = Nothing
        Me.lvBatchHeads.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.objcolhTranHeadID, Me.colhTranHead, Me.colhHeadtype, Me.colhTypeOf, Me.colhCalcType, Me.objcolhGUID})
        Me.lvBatchHeads.CompulsoryColumns = ""
        Me.lvBatchHeads.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvBatchHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvBatchHeads.FullRowSelect = True
        Me.lvBatchHeads.GridLines = True
        Me.lvBatchHeads.GroupingColumn = Nothing
        Me.lvBatchHeads.HideSelection = False
        Me.lvBatchHeads.Location = New System.Drawing.Point(0, 0)
        Me.lvBatchHeads.MinColumnWidth = 50
        Me.lvBatchHeads.MultiSelect = False
        Me.lvBatchHeads.Name = "lvBatchHeads"
        Me.lvBatchHeads.OptionalColumns = ""
        Me.lvBatchHeads.ShowMoreItem = False
        Me.lvBatchHeads.ShowSaveItem = False
        Me.lvBatchHeads.ShowSelectAll = True
        Me.lvBatchHeads.ShowSizeAllColumnsToFit = True
        Me.lvBatchHeads.Size = New System.Drawing.Size(327, 330)
        Me.lvBatchHeads.Sortable = True
        Me.lvBatchHeads.TabIndex = 0
        Me.lvBatchHeads.UseCompatibleStateImageBehavior = False
        Me.lvBatchHeads.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 30
        '
        'objcolhTranHeadID
        '
        Me.objcolhTranHeadID.Width = 0
        '
        'colhTranHead
        '
        Me.colhTranHead.Tag = "colhTranHead"
        Me.colhTranHead.Text = "Transaction Head Name"
        Me.colhTranHead.Width = 150
        '
        'colhHeadtype
        '
        Me.colhHeadtype.Tag = "colhHeadtype"
        Me.colhHeadtype.Text = "Transaction Head Type"
        Me.colhHeadtype.Width = 140
        '
        'colhTypeOf
        '
        Me.colhTypeOf.Tag = "colhTypeOf"
        Me.colhTypeOf.Text = "Type Of"
        Me.colhTypeOf.Width = 100
        '
        'colhCalcType
        '
        Me.colhCalcType.Tag = "colhCalcType"
        Me.colhCalcType.Text = "Calc. Type"
        Me.colhCalcType.Width = 100
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Text = "GUID"
        Me.objcolhGUID.Width = 0
        '
        'frmBatchTransaction_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(737, 527)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBatchTransaction_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/ Edit Batch Transaction"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBatchInfo.ResumeLayout(False)
        Me.gbBatchInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.pnlTransactionHead.ResumeLayout(False)
        Me.pnlTransactionHead.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlTransactionHead As System.Windows.Forms.Panel
    Friend WithEvents lvBatchHeads As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHeadtype As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtBatchName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBatchCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents lblBatchCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbBatchInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblEffectiveFrom As System.Windows.Forms.Label
    Friend WithEvents cboCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents lblTypeOf As System.Windows.Forms.Label
    Friend WithEvents cboTypeOf As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnUnassign As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lvTrnHeadList As eZee.Common.eZeeListView
    Friend WithEvents objcolhFindCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFindTranHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFindHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkFindCheck As System.Windows.Forms.CheckBox
    Friend WithEvents colhFindTypeOf As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFindCalcType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTypeOf As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCalcType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFindTranHeadID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhTranHeadID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
End Class
