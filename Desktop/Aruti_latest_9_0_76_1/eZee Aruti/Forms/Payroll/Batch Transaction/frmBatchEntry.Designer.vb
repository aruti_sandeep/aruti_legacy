﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBatchEntry
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBatchEntry))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowReinstatedEmployees = New System.Windows.Forms.CheckBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.chkShowNewlyHiredEmployees = New System.Windows.Forms.CheckBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbBatchEntry = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.cboBatch = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvBatch = New System.Windows.Forms.DataGridView
        Me.objcolhBatchID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhTranHeadID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhTrnHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhCurrency = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhVendor = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhMedicalRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.btnInsert = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.objbgwGlobalBatchEntry = New System.ComponentModel.BackgroundWorker
        Me.pnlMainInfo.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBatchEntry.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvBatch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbBatchEntry)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(1013, 572)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(292, 445)
        Me.gbEmployeeList.TabIndex = 5
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(122, 5)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblEmpCount.TabIndex = 160
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgEmployee)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchEmp)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(289, 416)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmpCode, Me.dgColhEmployee})
        Me.dgEmployee.Location = New System.Drawing.Point(1, 28)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(286, 388)
        Me.dgEmployee.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Emp. Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(285, 21)
        Me.txtSearchEmp.TabIndex = 12
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowReinstatedEmployees)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowNewlyHiredEmployees)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(310, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(690, 117)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Employee Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowReinstatedEmployees
        '
        Me.chkShowReinstatedEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReinstatedEmployees.Location = New System.Drawing.Point(258, 33)
        Me.chkShowReinstatedEmployees.Name = "chkShowReinstatedEmployees"
        Me.chkShowReinstatedEmployees.Size = New System.Drawing.Size(280, 21)
        Me.chkShowReinstatedEmployees.TabIndex = 310
        Me.chkShowReinstatedEmployees.Text = "Show Only Reinstated Employees in selected Period"
        Me.chkShowReinstatedEmployees.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(491, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(128, 13)
        Me.lnkAllocation.TabIndex = 308
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Advance Filter"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkShowNewlyHiredEmployees
        '
        Me.chkShowNewlyHiredEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowNewlyHiredEmployees.Location = New System.Drawing.Point(82, 60)
        Me.chkShowNewlyHiredEmployees.Name = "chkShowNewlyHiredEmployees"
        Me.chkShowNewlyHiredEmployees.Size = New System.Drawing.Size(280, 21)
        Me.chkShowNewlyHiredEmployees.TabIndex = 221
        Me.chkShowNewlyHiredEmployees.Text = "Show Only Newly Hired Employees in selected Period"
        Me.chkShowNewlyHiredEmployees.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(218, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(82, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(130, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(72, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(663, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(640, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(1013, 60)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "Add Batch Entry"
        '
        'gbBatchEntry
        '
        Me.gbBatchEntry.BorderColor = System.Drawing.Color.Black
        Me.gbBatchEntry.Checked = False
        Me.gbBatchEntry.CollapseAllExceptThis = False
        Me.gbBatchEntry.CollapsedHoverImage = Nothing
        Me.gbBatchEntry.CollapsedNormalImage = Nothing
        Me.gbBatchEntry.CollapsedPressedImage = Nothing
        Me.gbBatchEntry.CollapseOnLoad = False
        Me.gbBatchEntry.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.gbBatchEntry.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbBatchEntry.Controls.Add(Me.cboBatch)
        Me.gbBatchEntry.Controls.Add(Me.lblPeriod)
        Me.gbBatchEntry.Controls.Add(Me.cboPeriod)
        Me.gbBatchEntry.Controls.Add(Me.chkOverwrite)
        Me.gbBatchEntry.Controls.Add(Me.Panel1)
        Me.gbBatchEntry.Controls.Add(Me.lblBatchName)
        Me.gbBatchEntry.ExpandedHoverImage = Nothing
        Me.gbBatchEntry.ExpandedNormalImage = Nothing
        Me.gbBatchEntry.ExpandedPressedImage = Nothing
        Me.gbBatchEntry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBatchEntry.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBatchEntry.HeaderHeight = 25
        Me.gbBatchEntry.HeaderMessage = ""
        Me.gbBatchEntry.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBatchEntry.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBatchEntry.HeightOnCollapse = 0
        Me.gbBatchEntry.LeftTextSpace = 0
        Me.gbBatchEntry.Location = New System.Drawing.Point(310, 189)
        Me.gbBatchEntry.Name = "gbBatchEntry"
        Me.gbBatchEntry.OpenHeight = 182
        Me.gbBatchEntry.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBatchEntry.ShowBorder = True
        Me.gbBatchEntry.ShowCheckBox = False
        Me.gbBatchEntry.ShowCollapseButton = False
        Me.gbBatchEntry.ShowDefaultBorderColor = True
        Me.gbBatchEntry.ShowDownButton = False
        Me.gbBatchEntry.ShowHeader = True
        Me.gbBatchEntry.Size = New System.Drawing.Size(690, 322)
        Me.gbBatchEntry.TabIndex = 0
        Me.gbBatchEntry.Temp = 0
        Me.gbBatchEntry.Text = "Batch Entry Information"
        Me.gbBatchEntry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(407, 60)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(280, 21)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 236
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(258, 35)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(143, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 2
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'cboBatch
        '
        Me.cboBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatch.FormattingEnabled = True
        Me.cboBatch.Location = New System.Drawing.Point(104, 60)
        Me.cboBatch.Name = "cboBatch"
        Me.cboBatch.Size = New System.Drawing.Size(135, 21)
        Me.cboBatch.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(91, 14)
        Me.lblPeriod.TabIndex = 219
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(105, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(135, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'chkOverwrite
        '
        Me.chkOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwrite.Location = New System.Drawing.Point(258, 54)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(143, 31)
        Me.chkOverwrite.TabIndex = 3
        Me.chkOverwrite.Text = "Overwrite selected batch heads if exist"
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvBatch)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(104, 88)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(573, 231)
        Me.Panel1.TabIndex = 143
        '
        'dgvBatch
        '
        Me.dgvBatch.AllowUserToAddRows = False
        Me.dgvBatch.AllowUserToDeleteRows = False
        Me.dgvBatch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBatch.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhBatchID, Me.objcolhTranHeadID, Me.colhTrnHead, Me.objcolhCurrency, Me.colhAmount, Me.objcolhVendor, Me.colhMedicalRefNo})
        Me.dgvBatch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBatch.Location = New System.Drawing.Point(0, 0)
        Me.dgvBatch.Name = "dgvBatch"
        Me.dgvBatch.RowHeadersVisible = False
        Me.dgvBatch.Size = New System.Drawing.Size(573, 231)
        Me.dgvBatch.TabIndex = 0
        '
        'objcolhBatchID
        '
        Me.objcolhBatchID.HeaderText = "BatchID"
        Me.objcolhBatchID.Name = "objcolhBatchID"
        Me.objcolhBatchID.ReadOnly = True
        Me.objcolhBatchID.Visible = False
        '
        'objcolhTranHeadID
        '
        Me.objcolhTranHeadID.HeaderText = "TranHeadID"
        Me.objcolhTranHeadID.Name = "objcolhTranHeadID"
        Me.objcolhTranHeadID.ReadOnly = True
        Me.objcolhTranHeadID.Visible = False
        '
        'colhTrnHead
        '
        Me.colhTrnHead.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhTrnHead.HeaderText = "Transaction Head"
        Me.colhTrnHead.Name = "colhTrnHead"
        Me.colhTrnHead.ReadOnly = True
        '
        'objcolhCurrency
        '
        Me.objcolhCurrency.HeaderText = "Currency"
        Me.objcolhCurrency.Name = "objcolhCurrency"
        Me.objcolhCurrency.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhCurrency.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhCurrency.Visible = False
        '
        'colhAmount
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N4"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colhAmount.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhAmount.HeaderText = "Amount"
        Me.colhAmount.Name = "colhAmount"
        Me.colhAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhAmount.Width = 200
        '
        'objcolhVendor
        '
        Me.objcolhVendor.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.objcolhVendor.HeaderText = "Vendor"
        Me.objcolhVendor.Name = "objcolhVendor"
        Me.objcolhVendor.Visible = False
        '
        'colhMedicalRefNo
        '
        Me.colhMedicalRefNo.HeaderText = "Reference No."
        Me.colhMedicalRefNo.Name = "colhMedicalRefNo"
        Me.colhMedicalRefNo.Width = 120
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(8, 63)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(91, 15)
        Me.lblBatchName.TabIndex = 140
        Me.lblBatchName.Text = "Batch Name"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.lblSearchEmp)
        Me.objFooter.Controls.Add(Me.btnInsert)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1013, 55)
        Me.objFooter.TabIndex = 3
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(9, 13)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(98, 13)
        Me.lblSearchEmp.TabIndex = 11
        Me.lblSearchEmp.Text = "Search Employee"
        '
        'btnInsert
        '
        Me.btnInsert.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInsert.BackColor = System.Drawing.Color.White
        Me.btnInsert.BackgroundImage = CType(resources.GetObject("btnInsert.BackgroundImage"), System.Drawing.Image)
        Me.btnInsert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInsert.BorderColor = System.Drawing.Color.Empty
        Me.btnInsert.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInsert.FlatAppearance.BorderSize = 0
        Me.btnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInsert.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInsert.ForeColor = System.Drawing.Color.Black
        Me.btnInsert.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInsert.GradientForeColor = System.Drawing.Color.Black
        Me.btnInsert.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInsert.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInsert.Location = New System.Drawing.Point(801, 13)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInsert.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInsert.Size = New System.Drawing.Size(97, 30)
        Me.btnInsert.TabIndex = 0
        Me.btnInsert.Text = "&Insert"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(904, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Transaction Head"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Transaction Head"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Transaction Head"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N4"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn4.HeaderText = "CurrencyID"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.Visible = False
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "VendorID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        Me.DataGridViewTextBoxColumn5.Width = 120
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(450, 22)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(141, 13)
        Me.objlblProgress.TabIndex = 16
        '
        'objbgwGlobalBatchEntry
        '
        Me.objbgwGlobalBatchEntry.WorkerReportsProgress = True
        Me.objbgwGlobalBatchEntry.WorkerSupportsCancellation = True
        '
        'frmBatchEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1013, 572)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBatchEntry"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Batch Entry"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBatchEntry.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvBatch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbBatchEntry As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboBatch As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvBatch As System.Windows.Forms.DataGridView
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnInsert As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkOverwrite As System.Windows.Forms.CheckBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objcolhBatchID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhTranHeadID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhTrnHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhCurrency As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhVendor As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhMedicalRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowNewlyHiredEmployees As System.Windows.Forms.CheckBox
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShowReinstatedEmployees As System.Windows.Forms.CheckBox
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents objbgwGlobalBatchEntry As System.ComponentModel.BackgroundWorker
End Class
