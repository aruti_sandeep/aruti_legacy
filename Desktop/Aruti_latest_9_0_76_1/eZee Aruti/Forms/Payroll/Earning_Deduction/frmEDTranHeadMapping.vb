﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEDTranHeadMapping
    Private ReadOnly mstrModuleName As String = "frmEDTranHeadMapping"
    Private dsData As New DataSet
    Private mblnCancel As Boolean = True
    Private dtTable As DataTable
    Private dsDataList As New DataSet
    Dim objEmp As clsEmployee_Master
    Dim objTranHead As clsTransactionHead
    'Sohail (16 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
    'Sohail (16 Oct 2012) -- End

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

    'Sohail (26 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnCopyPreviousEDSlab As Boolean
    Public ReadOnly Property _CopyPreviousEDSlab() As Boolean
        Get
            Return mblnCopyPreviousEDSlab
        End Get
    End Property
    'Sohail (26 Mar 2012) -- End

    'Sohail (25 Jun 2020) -- Start
    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
    Public ReadOnly Property _PeriodStart() As Date
        Get
            Return mdtPeriodStartDate
        End Get
    End Property

    Public ReadOnly Property _PeriodEnd() As Date
        Get
            Return mdtPeriodEndDate
        End Get
    End Property
    'Sohail (25 Jun 2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
        Try
            dsData = dsList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    'Sohail (26 May 2011) -- Start
    Private Sub frmEDTranHeadMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmp = Nothing
            objTranHead = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDTranHeadMapping_FormClosed", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    Private Sub frmTranHeadMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmp = New clsEmployee_Master
        objTranHead = New clsTransactionHead
        Try
            Call Set_Logo(Me, gApplicationType)
            'Sohail (10 Jul 2014) -- Start
            'Enhancement - Custom Language.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Sohail (10 Jul 2014) -- End
            Call SetColor() 'Sohail (26 May 2011)
            cboAmount.Items.Clear()
            cboEmployeeCode.Items.Clear()
            cboTranHeadCode.Items.Clear()
            cboVendorCategoryId.Items.Clear()
            cboVendorId.Items.Clear()
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            cboMedicalRefNo.Items.Clear()
            Call FillCombo()
            'Sohail (18 Jan 2012) -- End
            Call SetData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTranHeadMapping_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEarningDeduction.SetMessages()
            objfrm._Other_ModuleNames = "clsEarningDeduction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End
#End Region

#Region " Private Methods "

    'Sohail (26 May 2011) -- Start
    Private Sub SetColor()
        Try
            cboEmployeeCode.BackColor = GUI.ColorComp
            cboTranHeadCode.BackColor = GUI.ColorComp
            cboAmount.BackColor = GUI.ColorComp
            cboVendorId.BackColor = GUI.ColorComp
            cboVendorCategoryId.BackColor = GUI.ColorComp
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            cboPeriod.BackColor = GUI.ColorComp
            cboMedicalRefNo.BackColor = GUI.ColorOptional
            'Sohail (18 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
                .EndUpdate()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (18 Jan 2012) -- End

    Private Sub SetData()
        Try
            dtTable = New DataTable("ED")
            For Each dtColumns As DataColumn In dsData.Tables(0).Columns
                cboAmount.Items.Add(dtColumns.ColumnName)
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboTranHeadCode.Items.Add(dtColumns.ColumnName)
                cboVendorCategoryId.Items.Add(dtColumns.ColumnName)
                cboVendorId.Items.Add(dtColumns.ColumnName)
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                cboMedicalRefNo.Items.Add(dtColumns.ColumnName)
                'Sohail (18 Jan 2012) -- End
            Next
            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("EmpCode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("TranCode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("batchtransactionunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("isdeduct", System.Type.GetType("System.Boolean")).DefaultValue = 0
            dtTable.Columns.Add("trnheadtype_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("typeof_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("calctype_id", System.Type.GetType("System.Int32")).DefaultValue = enCalcType.FlatRate_Others
            dtTable.Columns.Add("computeon_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("formula", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("formulaid", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = 1
            dtTable.Columns.Add("vendorunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = User._Object._Userunkid
            dtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("membership_categoryunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("HeadType", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("TypeOf", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Sohail (26 May 2011) -- Start
            dtTable.Columns.Add("trnheadtype_name", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("typeof_name", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("calctype_name", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (26 May 2011) -- End


            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            dtTable.Columns.Add("TranHeadName", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Anjan (21 Nov 2011)-End 

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            dtTable.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("medicalrefno", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (18 Jan 2012) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            dtTable.Columns.Add("start_date", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("end_date", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (21 Aug 2015) -- End
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            dtTable.Columns.Add("costcenterunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("empbatchpostingunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("membershiptranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("disciplinefileunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("cumulative_startdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            dtTable.Columns.Add("edstart_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            dtTable.Columns.Add("stop_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            'Sohail (25 Jun 2020) -- End
            'Hemant (23 Jun 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-976 -  Transaction head add/edit screen modification to indicate the number of pay periods a transaction would run from the assignment date
            dtTable.Columns.Add("assignedperiodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("activeperiods_stopdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            'Hemant (23 Jun 2023) -- End
        Catch ex As Exception
            'mblnCancel = False
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim objTnA As New clsTnALeaveTran 'Sohail (16 Oct 2012)
        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            End If
            'Sohail (18 Jan 2012) -- End

         'Sohail (26 May 2011) -- Start
            If CInt(cboEmployeeCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select field for Employee ID."), enMsgBoxStyle.Information)
                cboEmployeeCode.Focus()
                Exit Try
            ElseIf CInt(cboTranHeadCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select field for Transaction Head ID."), enMsgBoxStyle.Information)
                cboTranHeadCode.Focus()
                Exit Try
            ElseIf CInt(cboAmount.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select field for Amount."), enMsgBoxStyle.Information)
                cboAmount.Focus()
                Exit Try
                'ElseIf CInt(cboVendorId.SelectedIndex) < 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select field for Vendor/Membership ID."), enMsgBoxStyle.Information)
                '    cboVendorId.Focus()
                '    Exit Try
                'ElseIf CInt(cboVendorCategoryId.SelectedIndex) < 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select field for Vendor/Membership Category ID."), enMsgBoxStyle.Information)
                '    cboVendorCategoryId.Focus()
                '    Exit Try
            End If

            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 7, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If
            'Sohail (26 Mar 2012) -- End

            Dim objEd As New clsEarningDeduction
            Dim dsEmployee As DataSet
            Dim dtEmployee As DataTable
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmp.GetEmployeeList("Employee", False, True)
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmployee = objEmp.GetEmployeeList("Employee", False, True, , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate)

            dsEmployee = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriodStartDate, _
                                           mdtPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Employee", False)
            'Anjan [10 June 2015] -- End
            'Sohail (23 Nov 2012) -- End
            'Sohail (26 May 2011) -- End

            

            Dim dtData As DataTable = New DataView(dsData.Tables(0), " [" & cboEmployeeCode.Text & "] IS NOT NULL ", "", DataViewRowState.CurrentRows).ToTable

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            dsDataList = objTranHead.GetList("THead", , , True)
            Dim strEmpIDs As String = String.Join(",", (From p In dsEmployee.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
            Dim dsEDUnkId As DataSet = objEd.GetAllEDUnkIdForSelectedPeriod(Nothing, FinancialYear._Object._DatabaseName, CInt(cboPeriod.SelectedValue), "", 0)
            'Sohail (25 Jun 2020) -- End

            'Sohail (26 May 2011) -- Start
            'For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
            For Each dsRow As DataRow In dtData.Rows
                'Sohail (26 May 2011) -- End
                Dim dRow As DataRow
                'Sohail (26 May 2011) -- Start
                'dRow = dtTable.NewRow

                'dRow.Item("employeeunkid") = dsData.Tables(0).Rows(i)(cboEmployeeId.Text)
                'dRow.Item("tranheadunkid") = dsData.Tables(0).Rows(i)(cboTranHeadId.Text)
                'dRow.Item("amount") = dsData.Tables(0).Rows(i)(cboAmount.Text)
                'dRow.Item("vendorunkid") = dsData.Tables(0).Rows(i)(cboVendorId.Text)
                'dRow.Item("userunkid") = User._Object._Userunkid
                'dRow.Item("membership_categoryunkid") = dsData.Tables(0).Rows(i)(cboVendorCategoryId.Text)

                'dsDataList = objEmp.GetEmployeeList("Lst", False, True, CInt(dsData.Tables(0).Rows(i)(cboEmployeeId.Text)))
                'If dsDataList.Tables(0).Rows.Count > 0 Then
                '    dRow.Item("EmpCode") = dsDataList.Tables(0).Rows(0)("employeecode")
                'End If

                'dsDataList = objTranHead.GetList("THead", CInt(dsData.Tables(0).Rows(i)(cboTranHeadId.Text)))
                'If dsDataList.Tables(0).Rows.Count > 0 Then
                '    dRow.Item("TranCode") = dsDataList.Tables(0).Rows(0)("trnheadcode")
                'End If

                'dtTable.Rows.Add(dRow)


                'Anjan (21 Nov 2011)-Start
                'ENHANCEMENT : TRA COMMENTS
                'dtEmployee = New DataView(dsEmployee.Tables("Employee"), "employeeunkid = " & CInt(dsRow.Item(cboEmployeeId.Text).ToString.Trim) & "", "", DataViewRowState.CurrentRows).ToTable
                dtEmployee = New DataView(dsEmployee.Tables("Employee"), "EmployeeCode = '" & CStr(dsRow.Item(cboEmployeeCode.Text).ToString.Trim) & "' ", "", DataViewRowState.CurrentRows).ToTable
                'Anjan (21 Nov 2011)-End 


                dRow = dtTable.NewRow
                If dtEmployee.Rows.Count > 0 Then

                    'Sohail (16 Oct 2012) -- Start
                    'TRA - ENHANCEMENT
                    If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), dtEmployee.Rows(0).Item("employeeunkid").ToString, mdtPeriodEndDate) = True Then
                        dRow.Item("tranheadunkid") = 0
                        dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                        dRow.Item("EmpCode") = dtEmployee.Rows(0).Item("EmployeeCode").ToString
                        dRow.Item("TranCode") = dsRow.Item(cboTranHeadCode.Text).ToString.Trim
                        dRow.Item("TranHeadName") = ""

                        If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                            dRow.Item("amount") = 0
                        Else
                            dRow.Item("amount") = CDec(dsRow.Item(cboAmount.Text))
                        End If
                        dRow.Item("rowtypeid") = 4 'Process Payroll is already done for last date of selected Period for current the employee.

                        dtTable.Rows.Add(dRow)

                    Else
                        'Sohail (16 Oct 2012) -- End


                    'Anjan (21 Nov 2011)-Start
                    'ENHANCEMENT : TRA COMMENTS
                    Dim intHeadId As Integer = 0
                    'dsDataList = objTranHead.GetList("THead", CInt(dsRow.Item(cboTranHeadId.Text).ToString.Trim), , True)
                    'Anjan (21 Nov 2011)-End 


                        'Sohail (29 Oct 2012) -- Start
                        'TRA - ENHANCEMENT
                        'If objTranHead.isExist(dsRow.Item(cboTranHeadCode.Text).ToString.Trim, , , intHeadId) = True Then
                        'Sohail (25 Jun 2020) -- Start
                        'NMB Enhancement # : Performance enhancement on Import earning deduction heads.

                        'If objTranHead.isExist(dsRow.Item(cboTranHeadCode.Text).ToString.Trim, , , intHeadId, True) = True Then
                        Dim drHead() As DataRow = dsDataList.Tables(0).Select(" trnheadcode = '" & dsRow.Item(cboTranHeadCode.Text).ToString.Trim & "' ")
                        If drHead.Length > 0 Then
                            'Sohail (29 Oct 2012) -- End

                            'Sohail (25 Jun 2020) -- Start
                            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                            'dsDataList = objTranHead.GetList("THead", intHeadId, , True)
                            intHeadId = CInt(drHead(0).Item("tranheadunkid"))
                            'Sohail (25 Jun 2020) -- End

                        'If dsDataList.Tables(0).Rows.Count > 0 Then

                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'If objEd.GetEDUnkID(CInt(dtEmployee.Rows(0).Item("employeeunkid").ToString), CInt(dsDataList.Tables(0).Rows(0)("tranheadunkid")), CInt(cboPeriod.SelectedValue)) <= 0 Then 'If Not assigned on ED, 'Sohail (18 Jan 2012)
                            'Sohail (25 Jun 2020) -- Start
                            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                            'If objEd.GetEDUnkID(FinancialYear._Object._DatabaseName, CInt(dtEmployee.Rows(0).Item("employeeunkid").ToString), CInt(dsDataList.Tables(0).Rows(0)("tranheadunkid")), CInt(cboPeriod.SelectedValue)) <= 0 Then
                            Dim drED() As DataRow = dsEDUnkId.Tables(0).Select("employeeunkid = " & CInt(dtEmployee.Rows(0).Item("employeeunkid").ToString) & " AND tranheadunkid = " & intHeadId & " ")
                            If drED.Length <= 0 Then
                                'Sohail (25 Jun 2020) -- End
                                'Sohail (21 Aug 2015) -- End
                            'If objEd.GetEDUnkID(CInt(dsRow.Item(cboEmployeeId.Text).ToString.Trim), CInt(dsRow.Item(cboTranHeadId.Text).ToString)) <= 0 Then 'If Not assigned on ED

                                'Sohail (29 Mar 2017) -- Start
                                'Issue - 65.2 - New Salary type head was inserting on ED resulted in 2 salary .
                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'If CInt(dsDataList.Tables(0).Rows(0)("typeof_id")) = enTypeOf.Salary Then
                                If CInt(drHead(0)("typeof_id")) = enTypeOf.Salary Then
                                    'Sohail (25 Jun 2020) -- End
                                    dRow.Item("tranheadunkid") = 0
                                    dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                                    dRow.Item("EmpCode") = dtEmployee.Rows(0).Item("EmployeeCode").ToString
                                    dRow.Item("TranCode") = dsRow.Item(cboTranHeadCode.Text).ToString.Trim
                                    'Sohail (25 Jun 2020) -- Start
                                    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                    'dRow.Item("TranHeadName") = dsDataList.Tables(0).Rows(0)("trnheadname").ToString
                                    dRow.Item("TranHeadName") = drHead(0)("trnheadname").ToString
                                    'Sohail (25 Jun 2020) -- End

                                    If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                        dRow.Item("amount") = 0
                                    Else
                                        dRow.Item("amount") = CDec(dsRow.Item(cboAmount.Text))
                                    End If
                                    dRow.Item("rowtypeid") = 5 'Salary head not assigned on employee master, Cannot assign different salary head which is not set on employee master

                                    dtTable.Rows.Add(dRow)

                                    Continue For
                                End If
                                'Sohail (29 Mar 2017) -- End

                            'dRow.Item("employeeunkid") = CInt(dsRow.Item(cboEmployeeCode.Text).ToString)
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid").ToString)

                            dRow.Item("EmpCode") = dtEmployee.Rows(0).Item("employeecode").ToString
                            'dRow.Item("tranheadunkid") = CInt(dsRow.Item(cboTranHeadCode.Text).ToString)
                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'dRow.Item("tranheadunkid") = CInt(dsDataList.Tables(0).Rows(0)("tranheadunkid"))
                                dRow.Item("tranheadunkid") = intHeadId
                                'Sohail (25 Jun 2020) -- End
                            'dRow.Item("TranCode") = dsDataList.Tables(0).Rows(0)("trnheadcode")
                            dRow.Item("TranCode") = dsRow.Item(cboTranHeadCode.Text).ToString.Trim
                            dRow.Item("batchtransactionunkid") = 0
                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'If CInt(dsDataList.Tables(0).Rows(0)("trnheadtype_id")) = enTranHeadType.EarningForEmployees Then
                                '    dRow.Item("isdeduct") = False
                                'ElseIf CInt(dsDataList.Tables(0).Rows(0)("trnheadtype_id")) = enTranHeadType.DeductionForEmployee OrElse CInt(dsDataList.Tables(0).Rows(0)("trnheadtype_id")) = enTranHeadType.EmployeesStatutoryDeductions Then
                                '    dRow.Item("isdeduct") = True
                                'Else
                                '    dRow.Item("isdeduct") = False '(Nothing)
                                'End If
                                'dRow.Item("trnheadtype_id") = CInt(dsDataList.Tables(0).Rows(0)("trnheadtype_id"))
                                'dRow.Item("typeof_id") = CInt(dsDataList.Tables(0).Rows(0)("typeof_id"))
                                'dRow.Item("calctype_id") = CInt(dsDataList.Tables(0).Rows(0)("calctype_id"))
                                'dRow.Item("computeon_id") = CInt(dsDataList.Tables(0).Rows(0)("computeon_id"))
                                'dRow.Item("formula") = dsDataList.Tables(0).Rows(0)("formula").ToString
                                'dRow.Item("formulaid") = dsDataList.Tables(0).Rows(0)("formulaid").ToString
                                'dRow.Item("trnheadtype_name") = dsDataList.Tables(0).Rows(0)("headtype")
                                'dRow.Item("typeof_name") = dsDataList.Tables(0).Rows(0)("typeof")
                                'dRow.Item("calctype_name") = dsDataList.Tables(0).Rows(0)("calctype")
                                If CInt(drHead(0)("trnheadtype_id")) = enTranHeadType.EarningForEmployees Then
                                dRow.Item("isdeduct") = False
                                ElseIf CInt(drHead(0)("trnheadtype_id")) = enTranHeadType.DeductionForEmployee OrElse CInt(drHead(0)("trnheadtype_id")) = enTranHeadType.EmployeesStatutoryDeductions Then
                                dRow.Item("isdeduct") = True
                            Else
                                dRow.Item("isdeduct") = False '(Nothing)
                            End If
                                dRow.Item("trnheadtype_id") = CInt(drHead(0)("trnheadtype_id"))
                                dRow.Item("typeof_id") = CInt(drHead(0)("typeof_id"))
                                dRow.Item("calctype_id") = CInt(drHead(0)("calctype_id"))
                                dRow.Item("computeon_id") = CInt(drHead(0)("computeon_id"))
                                dRow.Item("formula") = drHead(0)("formula").ToString
                                dRow.Item("formulaid") = drHead(0)("formulaid").ToString
                                dRow.Item("trnheadtype_name") = drHead(0)("headtype")
                                dRow.Item("typeof_name") = drHead(0)("typeof")
                                dRow.Item("calctype_name") = drHead(0)("calctype")
                                'Sohail (25 Jun 2020) -- End
                            dRow.Item("currencyunkid") = 0
                            dRow.Item("voidreason") = ""
                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'If CInt(dsDataList.Tables(0).Rows(0)("calctype_id")) = enCalcType.FlatRate_Others Then
                                If CInt(drHead(0)("calctype_id")) = enCalcType.FlatRate_Others Then
                                    'Sohail (25 Jun 2020) -- End
                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                    dRow.Item("amount") = 0
                                Else
                                    dRow.Item("amount") = CDec(dsRow.Item(cboAmount.Text))
                                End If
                            Else
                                dRow.Item("amount") = 0
                            End If

                            dRow.Item("membership_categoryunkid") = 0
                            'Anjan (04 Jan 2012)-Start
                            'Issue : Cost center is to be included instead of vendor/membership
                            'If CInt(dsRow.Item(cboVendorCategoryId.Text).ToString) > 0 Then
                            'dsDataList = objEmp.GetEmployee_MembershipCategory(CInt(dsRow.Item(cboEmployeeId.Text).ToString), False, "MemCategory")
                            'If dsDataList.Tables(0).Rows.Count > 0 Then
                            '    dRow.Item("membership_categoryunkid") = CInt(dsRow.Item(cboVendorCategoryId.Text).ToString)
                            'End If
                            'End If
                            'Anjan (04 Jan 2012)-End
                            dRow.Item("vendorunkid") = 0
                            'Anjan (04 Jan 2012)-Start
                            'Issue : Cost center is to be included instead of vendor/membership
                            'If CInt(dsRow.Item(cboVendorId.Text).ToString) > 0 Then
                            '    dsDataList = objEmp.GetEmployee_Membership(CInt(dsRow.Item(cboEmployeeId.Text).ToString), False, "MemType")
                            '    If dsDataList.Tables(0).Rows.Count > 0 Then
                            '        dRow.Item("vendorunkid") = CInt(dsRow.Item(cboVendorId.Text).ToString)
                            '    End If
                            'End If
                            'Anjan (04 Jan 2012)-End

                            dRow.Item("userunkid") = User._Object._Userunkid

                            'Anjan (21 Nov 2011)-Start
                            'ENHANCEMENT : TRA COMMENTS
                            dRow.Item("rowtypeid") = 0 'proper entry
                            'Anjan (21 Nov 2011)-End 

                            'Sohail (18 Jan 2012) -- Start
                            'TRA - ENHANCEMENT
                            dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                dRow.Item("start_date") = eZeeDate.convertDate(mdtPeriodStartDate)
                                dRow.Item("end_date") = eZeeDate.convertDate(mdtPeriodEndDate)
                                'Sohail (21 Aug 2015) -- End
                            If cboMedicalRefNo.SelectedIndex > 0 Then
                                dRow.Item("medicalrefno") = dsRow.Item(cboMedicalRefNo.Text).ToString.Trim
                            Else
                                dRow.Item("medicalrefno") = ""
                            End If
                            'Sohail (18 Jan 2012) -- End

                                'Hemant (23 Jun 2023) -- Start
                                'ENHANCEMENT(TRA) : A1X-976 -  Transaction head add/edit screen modification to indicate the number of pay periods a transaction would run from the assignment date
                                Dim objTranHead As New clsTransactionHead
                                objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = intHeadId
                                dRow.Item("AssignedPeriodUnkid") = 0
                                dRow.Item("ActivePeriods_StopDate") = DBNull.Value
                                If CInt(objTranHead._ActivePeriods) > 0 Then
                                    Dim dtDate As Date = mdtPeriodEndDate.AddMonths(objTranHead._ActivePeriods - 1)
                                    Dim dtActivePeriods_StopDate As Date = New DateTime(dtDate.Year, dtDate.Month, DateTime.DaysInMonth(dtDate.Year, dtDate.Month))

                                    dRow.Item("AssignedPeriodUnkid") = CInt(cboPeriod.SelectedValue)
                                    dRow.Item("ActivePeriods_StopDate") = CDate(dtActivePeriods_StopDate)
                                End If
                                objTranHead = Nothing
                                'Hemant (23 Jun 2023) -- End

                            dtTable.Rows.Add(dRow)

                            'Anjan (21 Nov 2011)-Start
                            'ENHANCEMENT : TRA COMMENTS
                        Else
                            '*** If Already assigned on ED
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dtEmployee.Rows(0).Item("EmployeeCode").ToString
                            dRow.Item("TranCode") = dsRow.Item(cboTranHeadCode.Text).ToString.Trim
                                'If objTranHead.isExist(dsRow.Item(cboTranHeadCode.Text).ToString.Trim) = True Then 'Sohail (25 Jun 2020)
                                'Sohail (18 Jan 2012) -- Start
                                'TRA - ENHANCEMENT
                                'dRow.Item("tranheadunkid") = objTranHead._Tranheadunkid
                                'dRow.Item("TranHeadName") = objTranHead._Trnheadname
                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'dRow.Item("tranheadunkid") = CInt(dsDataList.Tables(0).Rows(0)("tranheadunkid"))
                                'dRow.Item("TranHeadName") = dsDataList.Tables(0).Rows(0)("trnheadname")
                                'dRow.Item("trnheadtype_id") = CInt(dsDataList.Tables(0).Rows(0)("trnheadtype_id"))
                                'dRow.Item("typeof_id") = CInt(dsDataList.Tables(0).Rows(0)("typeof_id"))
                                'dRow.Item("calctype_id") = CInt(dsDataList.Tables(0).Rows(0)("calctype_id"))
                                'dRow.Item("trnheadtype_name") = dsDataList.Tables(0).Rows(0)("headtype")
                                'dRow.Item("typeof_name") = dsDataList.Tables(0).Rows(0)("typeof")
                                'dRow.Item("calctype_name") = dsDataList.Tables(0).Rows(0)("calctype")
                                dRow.Item("tranheadunkid") = CInt(drHead(0)("tranheadunkid"))
                                dRow.Item("TranHeadName") = drHead(0)("trnheadname")
                                dRow.Item("trnheadtype_id") = CInt(drHead(0)("trnheadtype_id"))
                                dRow.Item("typeof_id") = CInt(drHead(0)("typeof_id"))
                                dRow.Item("calctype_id") = CInt(drHead(0)("calctype_id"))
                                dRow.Item("trnheadtype_name") = drHead(0)("headtype")
                                dRow.Item("typeof_name") = drHead(0)("typeof")
                                dRow.Item("calctype_name") = drHead(0)("calctype")
                                'Sohail (25 Jun 2020) -- End
                                'Sohail (18 Jan 2012) -- End
                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'Else
                                '    dRow.Item("TranHeadName") = ""
                                '    dRow.Item("tranheadunkid") = 0
                                'End If
                                'Sohail (25 Jun 2020) -- End
                            'If CInt(dsDataList.Tables(0).Rows(0)("calctype_id")) = enCalcType.FlatRate_Others Then
                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                dRow.Item("amount") = 0
                            Else
                                dRow.Item("amount") = CDec(dsRow.Item(cboAmount.Text))
                            End If
                            '    Else
                            '    dRow.Item("amount") = 0
                            'End If
                            dRow.Item("rowtypeid") = 3 'Already assigned heads on Ed

                            dtTable.Rows.Add(dRow)

                            'Anjan (21 Nov 2011)-End 

                        End If
                    Else

                        'Anjan (21 Nov 2011)-Start
                        'ENHANCEMENT : TRA COMMENTS
                        'Transaction head not found
                        'intEDunkId <= 0 (not found in ED)
                        dRow.Item("tranheadunkid") = 0
                        dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                        dRow.Item("EmpCode") = dtEmployee.Rows(0).Item("EmployeeCode").ToString
                        dRow.Item("TranCode") = dsRow.Item(cboTranHeadCode.Text).ToString.Trim
                        dRow.Item("TranHeadName") = ""
                        'If CInt(dsDataList.Tables(0).Rows(0)("calctype_id")) = enCalcType.FlatRate_Others Then
                        If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                            dRow.Item("amount") = 0
                        Else
                            dRow.Item("amount") = CDec(dsRow.Item(cboAmount.Text))
                        End If
                        '    Else
                        '    dRow.Item("amount") = 0
                        'End If
                        dRow.Item("rowtypeid") = 1 'Unassigned heads on Ed

                        dtTable.Rows.Add(dRow)
                        'Anjan (21 Nov 2011)-End 


                    End If
                    End If
                Else
                    'employee not found
                    dRow.Item("employeeunkid") = 0
                    dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                    dRow.Item("TranCode") = dsRow.Item(cboTranHeadCode.Text).ToString.Trim
                    If objTranHead.isExist(dsRow.Item(cboTranHeadCode.Text).ToString.Trim) = True Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'dRow.Item("tranheadunkid") = objTranHead._Tranheadunkid
                        dRow.Item("tranheadunkid") = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        dRow.Item("TranHeadName") = objTranHead._Trnheadname
                    Else
                        dRow.Item("tranheadunkid") = 0
                        dRow.Item("TranHeadName") = ""
                    End If
                    'If CInt(dsDataList.Tables(0).Rows(0)("calctype_id")) = enCalcType.FlatRate_Others Then
                    If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                        dRow.Item("amount") = 0
                    Else
                        dRow.Item("amount") = CDec(dsRow.Item(cboAmount.Text))
                    End If
                    '    Else
                    '    dRow.Item("amount") = 0
                    'End If
                    dRow.Item("rowtypeid") = 2 'employee code not found

                    dtTable.Rows.Add(dRow)
                End If
                'Sohail (26 May 2011) -- End
            Next
            'Sohail (26 May 2011) -- Start
            'mblnCancel = False 
            'objEmp = Nothing
            'objTranHead = Nothing
            'Me.Close()

            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            mblnCopyPreviousEDSlab = chkCopyPreviousEDSlab.Checked
            'Sohail (26 Mar 2012) -- End

            If dtTable.Rows.Count > 0 Then
            mblnCancel = False
            Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot import this file. Reason : There are all Assigned transaction heads in this file."), enMsgBoxStyle.Exclamation)
            End If
            'Sohail (26 May 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (16 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub
    'Sohail (16 Oct 2012) -- End

    'Sohail (26 May 2011) -- Start
#Region " Message "
    '1, "Please select field for Employee ID."
    '2, "Please select field for Transaction Head ID."
    '3, "Please select field for Amount."
    '4, "Please select field for Vendor/Membership ID."
    '5, "Please select field for Vendor/Membership Category ID."
    '6, "Sorry,You cannot import this file. Reason : There are all Assigned transaction heads in this file."
#End Region
    'Sohail (26 May 2011) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblVendorCategory.Text = Language._Object.getCaption(Me.lblVendorCategory.Name, Me.lblVendorCategory.Text)
			Me.lblVendorId.Text = Language._Object.getCaption(Me.lblVendorId.Name, Me.lblVendorId.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblTranHeadCode.Text = Language._Object.getCaption(Me.lblTranHeadCode.Name, Me.lblTranHeadCode.Text)
			Me.lblEmployeecode.Text = Language._Object.getCaption(Me.lblEmployeecode.Name, Me.lblEmployeecode.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.lblMedicalRefNo.Text = Language._Object.getCaption(Me.lblMedicalRefNo.Name, Me.lblMedicalRefNo.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select field for Employee ID.")
			Language.setMessage(mstrModuleName, 2, "Please select field for Transaction Head ID.")
			Language.setMessage(mstrModuleName, 3, "Please select field for Amount.")
			Language.setMessage(mstrModuleName, 4, "Sorry, You cannot import this file. Reason : There are all Assigned transaction heads in this file.")
			Language.setMessage(mstrModuleName, 5, "Please select Period. Period is mandatory information.")
			Language.setMessage(mstrModuleName, 6, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.")
			Language.setMessage(mstrModuleName, 7, "Do you want to continue?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class