﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmImportExportHeads

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportExportHeads"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    Private mdtFilteredTable As DataTable
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    Private m_Dataview As DataView 'Sohail (18 Jan 2012)

    Private mblnCopyPreviousEDSlab As Boolean 'Sohail (26 Mar 2012)
    'Sohail (25 Jun 2020) -- Start
    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    'Sohail (25 Jun 2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'Sohail (26 May 2011) -- Start
    Private Sub SetColor()
        Try
            'cboHeadTypeId.BackColor = GUI.ColorComp
            'cboTypeOfId.BackColor = GUI.ColorComp
            'cboCalcTypeId.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            'dsCombos = objMaster.getComboListForHeadType("HeadType")
            'With cboHeadTypeId
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombos.Tables("HeadType")
            '    .SelectedValue = 0
            'End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhEmployee.DataPropertyName = "EmpCode"
            dgcolhTranCode.DataPropertyName = "TranCode"
            'Sohail (26 May 2011) -- Start
            'dgcolhHeadType.DataPropertyName = "HeadType"
            'dgcolhTypeOF.DataPropertyName = "TypeOf"
            dgcolhHeadType.DataPropertyName = "trnheadtype_name"
            dgcolhTypeOF.DataPropertyName = "typeof_name"
            dgcolhCalcType.DataPropertyName = "calctype_name"
            'Sohail (26 May 2011) -- End


            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            'dgvImportInfo.DataSource = mdtTable
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dgvImportInfo.DataSource = mdtFilteredTable
            dgvImportInfo.DataSource = m_Dataview
            'Sohail (18 Jan 2012) -- End
            'Anjan (21 Nov 2011)-End 
            dgvImportInfo.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

    ' IF YOU UNCOMMENT THEN CHECK LANGUAGE FROM CLSMASTER CLASS AND THEN UNCOMMENT THIS CODE.
    'Private Sub FillList()
    '    Dim LvItem As ListViewItem
    '    Dim intIndex As Integer = 0
    '    Try
    '        lvData.Items.Clear()
    '        lvData.LVItems = Nothing
    '        If mdtTable.Rows.Count > 0 Then
    '            lvData.LVItems = New ListViewItem(mdtTable.Rows.Count - 1) {}
    '        End If
    '        For Each dtRow As DataRow In mdtTable.Rows
    '            LvItem = New ListViewItem
    '            Dim objEmp As New clsEmployee_Master
    '            objEmp._Employeeunkid = CInt(dtRow.Item("employeeunkid"))
    '            LvItem.Text = objEmp._Employeecode
    '            'objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
    '            objEmp = Nothing
    '            Dim objTranHead As New clsTransactionHead
    '            objTranHead._Tranheadunkid = CInt(dtRow.Item("tranheadunkid"))
    '            LvItem.SubItems.Add(objTranHead._Trnheadcode.ToString)
    '            objTranHead = Nothing
    '            LvItem.SubItems.Add(dtRow.Item("amount").ToString)
    '            Select Case CInt(dtRow.Item("trnheadtype_id"))
    '                Case enTranHeadType.EarningForEmployees     '1
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 2, "Earning for Employee"))
    '                Case enTranHeadType.DeductionForEmployee    '2
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 3, "Deduction From Employee"))
    '                Case enTranHeadType.EmployeesStatutoryDeductions    '3
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 4, "Employees Statutory Deductions"))
    '                Case enTranHeadType.EmployersStatutoryContributions '4
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 5, "Employers Statutory Contributions"))
    '                Case enTranHeadType.Informational   '5
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 82, "Informational"))
    '                Case Else
    '                    LvItem.SubItems.Add("")
    '            End Select
    '            Select Case CInt(dtRow.Item("typeof_id"))
    '                Case enTypeOf.Salary        '1
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 7, "Salary"))
    '                Case enTypeOf.Allowance     '2
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 8, "Allowance"))
    '                Case enTypeOf.Bonus         '3
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 9, "Bonus"))
    '                Case enTypeOf.Commission    '4
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 10, "Commission"))
    '                Case enTypeOf.BENEFIT       '5
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 123, "Benefit"))
    '                Case enTypeOf.Other_Earnings    '6
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 11, "Other Earnings"))
    '                Case enTypeOf.LEAVE_DEDUCTION   '7
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 122, "Leave Deduction"))
    '                Case enTypeOf.Other_Deductions_Emp  '8
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 12, "Other Deductions"))
    '                Case enTypeOf.Taxes         '9
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 203, "Taxes"))
    '                Case enTypeOf.Employee_Statutory_Contributions  '10
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 13, "Employers Contributions"))
    '                Case enTypeOf.Other_Deductions_Satutary '11
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 14, "Other Deductions"))
    '                Case enTypeOf.Employers_Statutory_Contribution  '12
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 116, "Employee Statutory Contributions"))
    '                Case enTypeOf.Informational     '13
    '                    LvItem.SubItems.Add(Language.getMessage("clsMasterData", 82, "Informational"))
    '                Case Else
    '                    LvItem.SubItems.Add("")
    '            End Select
    '            LvItem.Tag = intIndex
    '            lvData.LVItems(intIndex) = LvItem
    '            intIndex += 1
    '        Next
    '        If intIndex > 0 Then
    '            lvData.VirtualListSize = intIndex
    '        Else
    '            lvData.VirtualListSize = 0
    '        End If

    '        lvData.Invalidate()
    '        lvData.Reset()

    '        If lvData.Items.Count > 10 Then
    '            colhEmployeeId.Width = colhEmployeeId.Width - 20
    '        Else
    '            colhEmployeeId.Width = colhEmployeeId.Width
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region " Form's Events "

    Private Sub frmImportExportHeads_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'objIExcel = New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            Call Set_Logo(Me, gApplicationType)
            'Sohail (10 Jul 2014) -- Start
            'Enhancement - Custom Language.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Sohail (10 Jul 2014) -- End
            'Sohail (26 May 2011) -- Start
            Call SetColor()
            txtFilePath.ReadOnly = True
            'Sohail (26 May 2011) -- End
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportExportHeads_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEarningDeduction.SetMessages()
            objfrm._Other_ModuleNames = "clsEarningDeduction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End

#End Region

#Region " Buttons "

    Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSet.Click
        'If radApplytoAll.Checked = False And radApplySelected.Checked = False And radApplytoChecked.Checked = False Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please atleast one action to set the value."), enMsgBoxStyle.Information)
        '    Exit Sub
        'End If

        'If CInt(cboHeadTypeId.SelectedValue) <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Transaction Head Type is compulsory Information. Please Select Transaction Head Type to continue."), enMsgBoxStyle.Information)
        '    cboHeadTypeId.Focus()
        '    Exit Sub
        'End If

        'If CInt(cboTypeOfId.SelectedValue) <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Type Of is compulsory Information. Please Select Type Of to continue.."), enMsgBoxStyle.Information)
        '    cboTypeOfId.Focus()
        '    Exit Sub
        'End If


        'Try
        '    If radApplytoAll.Checked = True Then
        '        For Each dRow As DataRow In mdtTable.Rows
        '            dRow.Item("trnheadtype_id") = CInt(cboHeadTypeId.SelectedValue)
        '            dRow.Item("typeof_id") = CInt(cboTypeOfId.SelectedValue)

        '            dRow.Item("HeadType") = cboHeadTypeId.Text
        '            dRow.Item("TypeOf") = cboTypeOfId.Text

        '            Select Case CInt(cboHeadTypeId.SelectedValue)
        '                Case enTranHeadType.EarningForEmployees
        '                    dRow.Item("isdeduct") = 0
        '                Case enTranHeadType.DeductionForEmployee
        '                    dRow.Item("isdeduct") = 1
        '                Case enTranHeadType.EmployeesStatutoryDeductions
        '                    dRow.Item("isdeduct") = 1
        '                Case enTranHeadType.EmployersStatutoryContributions
        '                    dRow.Item("isdeduct") = 0
        '                Case enTranHeadType.Informational
        '                    dRow.Item("isdeduct") = 0
        '            End Select
        '            mdtTable.AcceptChanges()
        '        Next
        '    ElseIf radApplySelected.Checked = True Then
        '        Dim intSelectedIndex As Integer
        '        If dgvImportInfo.SelectedRows.Count > 0 Then
        '            intSelectedIndex = dgvImportInfo.SelectedRows(0).Index
        '            mdtTable.Rows(intSelectedIndex).Item("trnheadtype_id") = CInt(cboHeadTypeId.SelectedValue)
        '            mdtTable.Rows(intSelectedIndex).Item("typeof_id") = CInt(cboTypeOfId.SelectedValue)

        '            mdtTable.Rows(intSelectedIndex).Item("HeadType") = cboHeadTypeId.Text
        '            mdtTable.Rows(intSelectedIndex).Item("TypeOf") = cboTypeOfId.Text

        '            Select Case CInt(cboHeadTypeId.SelectedValue)
        '                Case enTranHeadType.EarningForEmployees
        '                    mdtTable.Rows(intSelectedIndex).Item("isdeduct") = 0
        '                Case enTranHeadType.DeductionForEmployee
        '                    mdtTable.Rows(intSelectedIndex).Item("isdeduct") = 1
        '                Case enTranHeadType.EmployeesStatutoryDeductions
        '                    mdtTable.Rows(intSelectedIndex).Item("isdeduct") = 1
        '                Case enTranHeadType.EmployersStatutoryContributions
        '                    mdtTable.Rows(intSelectedIndex).Item("isdeduct") = 0
        '                Case enTranHeadType.Informational
        '                    mdtTable.Rows(intSelectedIndex).Item("isdeduct") = 0
        '            End Select
        '        End If
        '    ElseIf radApplytoChecked.Checked = True Then
        '        Dim dtTemp() As DataRow = mdtTable.Select("IsChecked = '" & True & "'")
        '        If dtTemp.Length <= 0 Then
        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one item to assign."), enMsgBoxStyle.Information)
        '            Exit Sub
        '        End If

        '        For i As Integer = 0 To dtTemp.Length - 1
        '            dtTemp(i)("trnheadtype_id") = CInt(cboHeadTypeId.SelectedValue)
        '            dtTemp(i)("typeof_id") = CInt(cboTypeOfId.SelectedValue)

        '            dtTemp(i)("HeadType") = cboHeadTypeId.Text
        '            dtTemp(i)("TypeOf") = cboTypeOfId.Text

        '            Select Case CInt(cboHeadTypeId.SelectedValue)
        '                Case enTranHeadType.EarningForEmployees
        '                    dtTemp(i)("isdeduct") = 0
        '                Case enTranHeadType.DeductionForEmployee
        '                    dtTemp(i)("isdeduct") = 1
        '                Case enTranHeadType.EmployeesStatutoryDeductions
        '                    dtTemp(i)("isdeduct") = 1
        '                Case enTranHeadType.EmployersStatutoryContributions
        '                    dtTemp(i)("isdeduct") = 0
        '                Case enTranHeadType.Informational
        '                    dtTemp(i)("isdeduct") = 0
        '            End Select
        '        Next
        '    End If
        '    Call FillGirdView()
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "objbtnSet_Click", mstrModuleName)
        'End Try
    End Sub

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
            ofdlgOpen.Filter = "XML files (*.xml)|*.xml|Excel files(*.xlsx)|*.xlsx"
            ofdlgOpen.FilterIndex = 2 'Sohail (26 May 2011)
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                Select Case ofdlgOpen.FilterIndex
                    Case 1
                        dsList.ReadXml(txtFilePath.Text)
                    Case 2
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'dsList = objIExcel.Import(txtFilePath.Text)
                        dsList = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                Dim frm As New frmEDTranHeadMapping
                If frm.displayDialog(dsList) = False Then
                    mblnCancel = False
                    Exit Sub
                End If
                mdtTable = frm._DataTable
                mblnCopyPreviousEDSlab = frm._CopyPreviousEDSlab 'Sohail (26 Mar 2012)

                'Sohail (25 Jun 2020) -- Start
                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                mdtPeriodStart = frm._PeriodStart
                mdtPeriodEnd = frm._PeriodEnd
                'Sohail (25 Jun 2020) -- End

                'Anjan (21 Nov 2011)-Start
                'ENHANCEMENT : TRA COMMENTS
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'mdtFilteredTable = New DataView(mdtTable, "rowtypeid = 0 ", "", DataViewRowState.CurrentRows).ToTable
                m_Dataview = New DataView(mdtTable)
                m_Dataview.RowFilter = "rowtypeid = 0 "
                'Sohail (18 Jan 2012) -- End
                'Anjan (21 Nov 2011)-End 

                Call FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim blnFlag As Boolean = False

        mblnCancel = True 'Sohail (23 Apr 2011)

        'Sohail (26 May 2011) -- Start
        If mdtTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot import this file. Reason :Unassigned heads are not in this import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (26 May 2011) -- End

        'Anjan (21 Nov 2011)-Start
        'ENHANCEMENT : TRA COMMENTS
        'Sohail (18 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'mdtFilteredTable = New DataView(mdtTable, "rowtypeid = 0 ", "", DataViewRowState.CurrentRows).ToTable
        m_Dataview.RowFilter = "rowtypeid = 0 "
        mdtFilteredTable = m_Dataview.ToTable
        'Sohail (18 Jan 2012) -- End

        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot import this file. Reason :Some transaction Codes or Employee Codes are not in the system."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Anjan (21 Nov 2011)-End 

        'Sohail (18 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (18 Jan 2012) -- End

        'Dim dtTemp() As DataRow = mdtTable.Select("trnheadtype_id = " & -1)
        'If dtTemp.Length > 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please set the transaction head type in order to Import file."), enMsgBoxStyle.Information)
        '    Exit Sub
        'End If

        'Dim drTemp() As DataRow = mdtTable.Select("typeof_id = " & -1)
        'If drTemp.Length > 0 Then
        '    'Sohail (26 May 2011) -- Start
        '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please set the transaction Type Of in order to Import file."), enMsgBoxStyle.Information)
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please set the transaction Type Of in order to Import file."), enMsgBoxStyle.Information)
        '    'Sohail (26 May 2011) -- End
        '    Exit Sub
        'End If

        Try
            Dim ObjED As New clsEarningDeduction
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            ''Anjan (21 Nov 2011)-Start
            ''ENHANCEMENT : TRA COMMENTS
            'For Each dtRow As DataRow In mdtFilteredTable.Rows
            '    'For Each dtRow As DataRow In mdtTable.Rows
            '    'Anjan (21 Nov 2011)-End 

            '    'Sohail (26 Mar 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'ObjED._Amount = CDec(dtRow.Item("amount"))
            '    'ObjED._Batchtransactionunkid = CInt(dtRow.Item("batchtransactionunkid"))
            '    'ObjED._Calctype_Id = CInt(dtRow.Item("calctype_id"))
            '    'ObjED._Computeon_Id = CInt(dtRow.Item("computeon_id"))
            '    'ObjED._Currencyunkid = CInt(dtRow.Item("currencyunkid"))
            '    'ObjED._Employeeunkid = CInt(dtRow.Item("employeeunkid"))
            '    'ObjED._Formula = CStr(dtRow.Item("formula"))
            '    'ObjED._FormulaId = CStr(dtRow.Item("formulaid"))
            '    'ObjED._Isdeduct = CBool(dtRow.Item("isdeduct"))
            '    'ObjED._Isvoid = CBool(dtRow.Item("isvoid"))
            '    'ObjED._Membership_Categoryunkid = CInt(dtRow.Item("membership_categoryunkid"))
            '    'ObjED._Tranheadunkid = CInt(dtRow.Item("tranheadunkid"))
            '    'ObjED._Trnheadtype_Id = CInt(dtRow.Item("trnheadtype_id"))
            '    'ObjED._Typeof_Id = CInt(dtRow.Item("typeof_id"))
            '    'ObjED._Userunkid = CInt(dtRow.Item("userunkid"))
            '    'ObjED._Vendorunkid = CInt(dtRow.Item("vendorunkid"))
            '    'ObjED._Voiddatetime = Nothing
            '    'ObjED._Voidreason = CStr(dtRow.Item("voidreason"))
            '    'ObjED._Voiduserunkid = -1
            '    ''Sohail (14 Jun 2011) -- Start
            '    ''Sohail (05 Jul 2011) -- Start
            '    'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
            '    '    ObjED._Isapproved = True
            '    '    ObjED._Approveruserunkid = User._Object._Userunkid
            '    'Else
            '    '    ObjED._Isapproved = False
            '    '    ObjED._Approveruserunkid = -1
            '    'End If
            '    ''Sohail (14 Jun 2011) -- End

            '    ''Sohail (18 Jan 2012) -- Start
            '    ''TRA - ENHANCEMENT
            '    'ObjED._Periodunkid = CInt(dtRow.Item("periodunkid"))
            '    'ObjED._MedicalRefNo = dtRow.Item("medicalrefno").ToString
            '    ''Sohail (18 Jan 2012) -- End

            '    ''Sohail (18 Jan 2012) -- Start
            '    ''TRA - ENHANCEMENT
            '    ''blnFlag = ObjED.Insert(False)                
            '    'blnFlag = ObjED.Insert(False, True)
            '    'Sohail (09 Nov 2013) -- Start
            '    'TRA - ENHANCEMENT
            '    'blnFlag = ObjED.InsertAllByEmployeeList(dtRow.Item("employeeunkid").ToString, CInt(dtRow.Item("tranheadunkid")), CInt(dtRow.Item("currencyunkid")), CInt(dtRow.Item("vendorunkid")), CInt(dtRow.Item("batchtransactionunkid")), CDec(dtRow.Item("amount")), False, False, CInt(dtRow.Item("periodunkid")), dtRow.Item("medicalrefno").ToString, True, mblnCopyPreviousEDSlab)
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'blnFlag = ObjED.InsertAllByEmployeeList(dtRow.Item("employeeunkid").ToString, CInt(dtRow.Item("tranheadunkid")), CInt(dtRow.Item("currencyunkid")), CInt(dtRow.Item("vendorunkid")), CInt(dtRow.Item("batchtransactionunkid")), CDec(dtRow.Item("amount")), False, False, CInt(dtRow.Item("periodunkid")), dtRow.Item("medicalrefno").ToString, Nothing, Nothing, True, mblnCopyPreviousEDSlab)
            '    'Sohail (15 Dec 2018) -- Start
            '    'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            '    'blnFlag = ObjED.InsertAllByEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(dtRow.Item("start_date").ToString), eZeeDate.convertDate(dtRow.Item("end_date").ToString), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, dtRow.Item("employeeunkid").ToString, CInt(dtRow.Item("tranheadunkid")), CInt(dtRow.Item("currencyunkid")), CInt(dtRow.Item("vendorunkid")), CInt(dtRow.Item("batchtransactionunkid")), CDec(dtRow.Item("amount")), False, False, CInt(dtRow.Item("periodunkid")), dtRow.Item("medicalrefno").ToString, Nothing, Nothing, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, mblnCopyPreviousEDSlab, , True, "")
            '    blnFlag = ObjED.InsertAllByEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(dtRow.Item("start_date").ToString), eZeeDate.convertDate(dtRow.Item("end_date").ToString), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, dtRow.Item("employeeunkid").ToString, CInt(dtRow.Item("tranheadunkid")), CInt(dtRow.Item("currencyunkid")), CInt(dtRow.Item("vendorunkid")), CInt(dtRow.Item("batchtransactionunkid")), CDec(dtRow.Item("amount")), False, False, CInt(dtRow.Item("periodunkid")), dtRow.Item("medicalrefno").ToString, Nothing, Nothing, User._Object.Privilege._AllowToApproveEarningDeduction, Nothing, ConfigParameter._Object._CurrentDateAndTime, True, mblnCopyPreviousEDSlab, , True, "")
            '    'Sohail (15 Dec 2018) -- End
            '    'Sohail (21 Aug 2015) -- End
            '    'Sohail (09 Nov 2013) -- End
            '    'Sohail (26 Mar 2012) -- End
            '    'Sohail (18 Jan 2012) -- End
            '    If blnFlag = False And ObjED._Message <> "" Then
            '        eZeeMsgBox.Show(ObjED._Message, enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'Next
            Dim dtTable As DataTable = mdtFilteredTable.Copy
            dtTable.Columns("currencyunkid").ColumnName = "currencyid"
            dtTable.Columns("vendorunkid").ColumnName = "vendorid"
            blnFlag = ObjED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, dtTable, True, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True, mblnCopyPreviousEDSlab, False, True, "", True)

                If blnFlag = False And ObjED._Message <> "" Then
                    eZeeMsgBox.Show(ObjED._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            'Sohail (25 Jun 2020) -- End

            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub mnuTransactionHeads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTransactionHeads.Click
        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub
            'Sohail (18 Jan 2012) -- End

            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'mdtFilteredTable = New DataView(mdtTable, "rowtypeid = 1 ", "", DataViewRowState.CurrentRows).ToTable
            m_Dataview.RowFilter = "rowtypeid = 1 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next
            'Sohail (18 Jan 2012) -- End
            'Anjan (21 Nov 2011)-End 

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuTransactionHeads_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployees.Click
        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub
            'Sohail (18 Jan 2012) -- End

            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'mdtFilteredTable = New DataView(mdtTable, "rowtypeid = 2 ", "", DataViewRowState.CurrentRows).ToTable
            m_Dataview.RowFilter = "rowtypeid = 2 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next
            'Sohail (18 Jan 2012) -- End
            'Anjan (21 Nov 2011)-End 

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEmployees_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            btnImport.Enabled = True
            If m_Dataview Is Nothing Then Exit Sub
            'Sohail (18 Jan 2012) -- End

            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'mdtFilteredTable = New DataView(mdtTable, "rowtypeid = 0 ", "", DataViewRowState.CurrentRows).ToTable
            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next
            'Sohail (18 Jan 2012) -- End
            'Anjan (21 Nov 2011)-End 

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub ShowAlreadyAssignedHeadsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowAlreadyAssignedHeadsToolStripMenuItem.Click
        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub
            'Sohail (18 Jan 2012) -- End

            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'mdtFilteredTable = New DataView(mdtTable, "rowtypeid = 3 ", "", DataViewRowState.CurrentRows).ToTable
            m_Dataview.RowFilter = "rowtypeid = 3 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next
            'Sohail (18 Jan 2012) -- End
            'Anjan (21 Nov 2011)-End 

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ShowAlreadyAssignedHeadsToolStripMenuItem_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (16 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuPayrollProcessed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayrollProcessed.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 4 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayrollProcessed_Click", mstrModuleName)
        End Try

    End Sub
    'Sohail (16 Oct 2012) -- End

    'Sohail (29 Mar 2017) -- Start
    'Issue - 65.2 - New Salary type head was inserting on ED resulted in 2 salary .
    Private Sub mnuSalaryHeadNotAssignedOnEmployeeMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSalaryHeadNotAssignedOnEmployeeMaster.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 5 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSalaryHeadNotAssignedOnEmployeeMaster_Click", mstrModuleName)
        End Try

    End Sub
    'Sohail (29 Mar 2017) -- End

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If m_Dataview Is Nothing Then Exit Sub
            'Sohail (18 Jan 2012) -- End

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                If modGlobal.Export_ErrorList(savDialog.FileName, m_Dataview.ToTable, "Import Employee Wizard") = True Then
                    'If modGlobal.Export_ErrorList(savDialog.FileName, mdtFilteredTable, "Import Employee Wizard") = True Then
                    'Sohail (18 Jan 2012) -- End
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkSelectAll.Checked
            Next
            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jan 2012) -- End

    'Private Sub cboHeadTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboHeadTypeId.SelectedIndexChanged
    '    Try
    '        'If CInt(cboHeadTypeId.SelectedValue) > 0 Then 'Sohail (26 May 2011)
    '            Dim objMaster As New clsMasterData
    '            Dim dsCombos As New DataSet
    '            Dim dtTable As DataTable 'Sohail (28 Dec 2010)
    '            dsCombos = objMaster.getComboListTypeOf("TypeOF", CInt(cboHeadTypeId.SelectedValue))
    '            'Sohail (28 Dec 2010) -- Start
    '            dtTable = New DataView(dsCombos.Tables("TypeOF"), "ID <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Don't allow user to make SALARY heads
    '            'Sohail (28 Dec 2010) -- End
    '            With cboTypeOfId
    '                .ValueMember = "Id"
    '                .DisplayMember = "NAME"
    '                'Sohail (28 Dec 2010) -- Start
    '                '.DataSource = dsCombos.Tables("TypeOF")
    '                .DataSource = dtTable
    '                'Sohail (28 Dec 2010) -- End
    '                .SelectedValue = 0
    '            End With
    '        'End If'Sohail (26 May 2011)

    '        'Sohail (26 May 2011) -- Start
    '        If CInt(cboHeadTypeId.SelectedValue) = enTranHeadType.Informational Then
    '            cboTypeOfId.SelectedValue = enTypeOf.Informational
    '            cboTypeOfId.Enabled = False
    '        Else
    '            cboTypeOfId.Enabled = True
    '        End If
    '        objMaster = Nothing
    '        'Sohail (26 May 2011) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboHeadTypeId_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Anjan (29 Jan 2011)-Start
    'Issue : this made is been commented temporarily.
    'Private Sub dgvImportInfo_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvImportInfo.CellContentClick
    '    Try
    '        If e.RowIndex <= -1 Then Exit Sub
    '        If CBool(mdtTable.Rows(e.RowIndex)("IsChecked")) = True Then
    '            mdtTable.Rows(e.RowIndex)("IsChecked") = False
    '            mdtTable.Rows(e.RowIndex).Item("trnheadtype_id") = -1
    '            mdtTable.Rows(e.RowIndex).Item("typeof_id") = -1
    '            mdtTable.Rows(e.RowIndex).Item("HeadType") = ""
    '            mdtTable.Rows(e.RowIndex).Item("TypeOf") = ""
    '            dgvImportInfo.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = mdtTable.Rows(e.RowIndex)("IsChecked")
    '            dgvImportInfo.RefreshEdit()
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub
    'Anjan (29 Jan 2011)-End
#End Region

    'Sohail (26 May 2011) -- Start
#Region " Message "
    '1, "Please atleast one action to set the value."
    '2, "Transaction Head Type is compulsory Information. Please Select Transaction Head Type to continue."
    '3, "Type Of is compulsory Information. Please Select Type Of to continue.."
    '4, "Please select atleast one item to assign."
    '5, "Please set the transaction head type in order to Import file."
    '6, "Please set the transaction Type Of in order to Import file."
    '7, "Sorry, you cannot import this file. Reason :Unassigned heads are not there in this file to import."
    '8, "File Imported Successfully."
#End Region
    'Sohail (26 May 2011) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnSet.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnSet.GradientForeColor = GUI._ButttonFontColor

            Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.elMandatoryInfo.Text = Language._Object.getCaption(Me.elMandatoryInfo.Name, Me.elMandatoryInfo.Text)
            Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
            Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
            Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
            Me.radApplySelected.Text = Language._Object.getCaption(Me.radApplySelected.Name, Me.radApplySelected.Text)
            Me.lblTypeOfId.Text = Language._Object.getCaption(Me.lblTypeOfId.Name, Me.lblTypeOfId.Text)
            Me.lblHeadTypeId.Text = Language._Object.getCaption(Me.lblHeadTypeId.Name, Me.lblHeadTypeId.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
            Me.mnuTransactionHeads.Text = Language._Object.getCaption(Me.mnuTransactionHeads.Name, Me.mnuTransactionHeads.Text)
            Me.mnuEmployees.Text = Language._Object.getCaption(Me.mnuEmployees.Name, Me.mnuEmployees.Text)
            Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
            Me.ShowAlreadyAssignedHeadsToolStripMenuItem.Text = Language._Object.getCaption(Me.ShowAlreadyAssignedHeadsToolStripMenuItem.Name, Me.ShowAlreadyAssignedHeadsToolStripMenuItem.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhTranCode.HeaderText = Language._Object.getCaption(Me.dgcolhTranCode.Name, Me.dgcolhTranCode.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhHeadType.HeaderText = Language._Object.getCaption(Me.dgcolhHeadType.Name, Me.dgcolhHeadType.HeaderText)
            Me.dgcolhTypeOF.HeaderText = Language._Object.getCaption(Me.dgcolhTypeOF.Name, Me.dgcolhTypeOF.HeaderText)
            Me.dgcolhCalcType.HeaderText = Language._Object.getCaption(Me.dgcolhCalcType.Name, Me.dgcolhCalcType.HeaderText)
            Me.mnuPayrollProcessed.Text = Language._Object.getCaption(Me.mnuPayrollProcessed.Name, Me.mnuPayrollProcessed.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, you cannot import this file. Reason :Unassigned heads are not in this import file.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot import this file. Reason :Some transaction Codes or Employee Codes are not in the system.")
            Language.setMessage(mstrModuleName, 3, "Please Tick atleast one transaction from list to Import.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class