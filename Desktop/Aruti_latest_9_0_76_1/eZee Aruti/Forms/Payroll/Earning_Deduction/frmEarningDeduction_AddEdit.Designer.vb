﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEarningDeduction_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEarningDeduction_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.TabED = New System.Windows.Forms.TabControl
        Me.tabpgED = New System.Windows.Forms.TabPage
        Me.gbEarningDeduction = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpCumulativeStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblCumulativeStartDate = New System.Windows.Forms.Label
        Me.dtpStopDate = New System.Windows.Forms.DateTimePicker
        Me.lblStopDate = New System.Windows.Forms.Label
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.txtMedicalRefNo = New eZee.TextBox.AlphanumericTextBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.lblMedicalRefNo = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.objbtnAddMembership = New eZee.Common.eZeeGradientButton
        Me.lblMembershipName = New System.Windows.Forms.Label
        Me.lblMembershipType = New System.Windows.Forms.Label
        Me.cboMembershipType = New System.Windows.Forms.ComboBox
        Me.lblTypeOf = New System.Windows.Forms.Label
        Me.cboTypeOf = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objComputeOn = New System.Windows.Forms.ComboBox
        Me.objCalcType = New System.Windows.Forms.ComboBox
        Me.objTranHeadType = New System.Windows.Forms.ComboBox
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblVendor = New System.Windows.Forms.Label
        Me.objlblCurrencyName = New System.Windows.Forms.Label
        Me.cboVendor = New System.Windows.Forms.ComboBox
        Me.lblCurrencyCountry = New System.Windows.Forms.Label
        Me.cboCurrencyCountry = New System.Windows.Forms.ComboBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.lblTrnHeadType = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.cboMemCategory = New System.Windows.Forms.ComboBox
        Me.tabpgHistory = New System.Windows.Forms.TabPage
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.chkAddNonRecurrentHeads = New System.Windows.Forms.CheckBox
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.lnkCopyEDSlab = New System.Windows.Forms.LinkLabel
        Me.lblCopyEDPeriod = New System.Windows.Forms.Label
        Me.cboCopyEDPeriod = New System.Windows.Forms.ComboBox
        Me.lvEDHistory = New eZee.Common.eZeeListView(Me.components)
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhTranHead = New System.Windows.Forms.ColumnHeader
        Me.colhTranheadType = New System.Windows.Forms.ColumnHeader
        Me.colhCalcType = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSaveEDHistory = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblDefCC = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.dtpEDStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblEDStartDate = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.TabED.SuspendLayout()
        Me.tabpgED.SuspendLayout()
        Me.gbEarningDeduction.SuspendLayout()
        Me.tabpgHistory.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.TabED)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(490, 524)
        Me.pnlMainInfo.TabIndex = 0
        '
        'TabED
        '
        Me.TabED.Controls.Add(Me.tabpgED)
        Me.TabED.Controls.Add(Me.tabpgHistory)
        Me.TabED.Location = New System.Drawing.Point(12, 66)
        Me.TabED.Name = "TabED"
        Me.TabED.SelectedIndex = 0
        Me.TabED.Size = New System.Drawing.Size(466, 401)
        Me.TabED.TabIndex = 0
        '
        'tabpgED
        '
        Me.tabpgED.Controls.Add(Me.gbEarningDeduction)
        Me.tabpgED.Location = New System.Drawing.Point(4, 22)
        Me.tabpgED.Name = "tabpgED"
        Me.tabpgED.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpgED.Size = New System.Drawing.Size(458, 375)
        Me.tabpgED.TabIndex = 0
        Me.tabpgED.Text = "Earning Deduction Information"
        Me.tabpgED.UseVisualStyleBackColor = True
        '
        'gbEarningDeduction
        '
        Me.gbEarningDeduction.BorderColor = System.Drawing.Color.Black
        Me.gbEarningDeduction.Checked = False
        Me.gbEarningDeduction.CollapseAllExceptThis = False
        Me.gbEarningDeduction.CollapsedHoverImage = Nothing
        Me.gbEarningDeduction.CollapsedNormalImage = Nothing
        Me.gbEarningDeduction.CollapsedPressedImage = Nothing
        Me.gbEarningDeduction.CollapseOnLoad = False
        Me.gbEarningDeduction.Controls.Add(Me.dtpEDStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.lblEDStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.dtpCumulativeStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.lblCumulativeStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.dtpStopDate)
        Me.gbEarningDeduction.Controls.Add(Me.lblStopDate)
        Me.gbEarningDeduction.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.gbEarningDeduction.Controls.Add(Me.txtMedicalRefNo)
        Me.gbEarningDeduction.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbEarningDeduction.Controls.Add(Me.lblMedicalRefNo)
        Me.gbEarningDeduction.Controls.Add(Me.lblPeriod)
        Me.gbEarningDeduction.Controls.Add(Me.cboPeriod)
        Me.gbEarningDeduction.Controls.Add(Me.lblCostCenter)
        Me.gbEarningDeduction.Controls.Add(Me.cboCostCenter)
        Me.gbEarningDeduction.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbEarningDeduction.Controls.Add(Me.objbtnAddMembership)
        Me.gbEarningDeduction.Controls.Add(Me.lblMembershipName)
        Me.gbEarningDeduction.Controls.Add(Me.lblMembershipType)
        Me.gbEarningDeduction.Controls.Add(Me.cboMembershipType)
        Me.gbEarningDeduction.Controls.Add(Me.lblTypeOf)
        Me.gbEarningDeduction.Controls.Add(Me.cboTypeOf)
        Me.gbEarningDeduction.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEarningDeduction.Controls.Add(Me.cboEmployee)
        Me.gbEarningDeduction.Controls.Add(Me.lblEmployee)
        Me.gbEarningDeduction.Controls.Add(Me.objComputeOn)
        Me.gbEarningDeduction.Controls.Add(Me.objCalcType)
        Me.gbEarningDeduction.Controls.Add(Me.objTranHeadType)
        Me.gbEarningDeduction.Controls.Add(Me.cboTrnHeadType)
        Me.gbEarningDeduction.Controls.Add(Me.lblVendor)
        Me.gbEarningDeduction.Controls.Add(Me.objlblCurrencyName)
        Me.gbEarningDeduction.Controls.Add(Me.cboVendor)
        Me.gbEarningDeduction.Controls.Add(Me.lblCurrencyCountry)
        Me.gbEarningDeduction.Controls.Add(Me.cboCurrencyCountry)
        Me.gbEarningDeduction.Controls.Add(Me.lblAmount)
        Me.gbEarningDeduction.Controls.Add(Me.cboTrnHead)
        Me.gbEarningDeduction.Controls.Add(Me.lblTrnHead)
        Me.gbEarningDeduction.Controls.Add(Me.lblTrnHeadType)
        Me.gbEarningDeduction.Controls.Add(Me.txtAmount)
        Me.gbEarningDeduction.Controls.Add(Me.cboMemCategory)
        Me.gbEarningDeduction.ExpandedHoverImage = Nothing
        Me.gbEarningDeduction.ExpandedNormalImage = Nothing
        Me.gbEarningDeduction.ExpandedPressedImage = Nothing
        Me.gbEarningDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEarningDeduction.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEarningDeduction.HeaderHeight = 25
        Me.gbEarningDeduction.HeaderMessage = ""
        Me.gbEarningDeduction.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEarningDeduction.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEarningDeduction.HeightOnCollapse = 0
        Me.gbEarningDeduction.LeftTextSpace = 0
        Me.gbEarningDeduction.Location = New System.Drawing.Point(6, 6)
        Me.gbEarningDeduction.Name = "gbEarningDeduction"
        Me.gbEarningDeduction.OpenHeight = 182
        Me.gbEarningDeduction.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEarningDeduction.ShowBorder = True
        Me.gbEarningDeduction.ShowCheckBox = False
        Me.gbEarningDeduction.ShowCollapseButton = False
        Me.gbEarningDeduction.ShowDefaultBorderColor = True
        Me.gbEarningDeduction.ShowDownButton = False
        Me.gbEarningDeduction.ShowHeader = True
        Me.gbEarningDeduction.Size = New System.Drawing.Size(434, 367)
        Me.gbEarningDeduction.TabIndex = 0
        Me.gbEarningDeduction.Temp = 0
        Me.gbEarningDeduction.Text = "Earning And Deduction Information"
        Me.gbEarningDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpCumulativeStartDate
        '
        Me.dtpCumulativeStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCumulativeStartDate.Checked = False
        Me.dtpCumulativeStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCumulativeStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCumulativeStartDate.Location = New System.Drawing.Point(140, 249)
        Me.dtpCumulativeStartDate.Name = "dtpCumulativeStartDate"
        Me.dtpCumulativeStartDate.ShowCheckBox = True
        Me.dtpCumulativeStartDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpCumulativeStartDate.TabIndex = 8
        '
        'lblCumulativeStartDate
        '
        Me.lblCumulativeStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCumulativeStartDate.Location = New System.Drawing.Point(11, 252)
        Me.lblCumulativeStartDate.Name = "lblCumulativeStartDate"
        Me.lblCumulativeStartDate.Size = New System.Drawing.Size(123, 15)
        Me.lblCumulativeStartDate.TabIndex = 241
        Me.lblCumulativeStartDate.Text = "Cumulative Start Date"
        '
        'dtpStopDate
        '
        Me.dtpStopDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Checked = False
        Me.dtpStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStopDate.Location = New System.Drawing.Point(140, 303)
        Me.dtpStopDate.Name = "dtpStopDate"
        Me.dtpStopDate.ShowCheckBox = True
        Me.dtpStopDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpStopDate.TabIndex = 10
        '
        'lblStopDate
        '
        Me.lblStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopDate.Location = New System.Drawing.Point(11, 306)
        Me.lblStopDate.Name = "lblStopDate"
        Me.lblStopDate.Size = New System.Drawing.Size(123, 15)
        Me.lblStopDate.TabIndex = 239
        Me.lblStopDate.Text = "Stop Date"
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(140, 345)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(229, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 12
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'txtMedicalRefNo
        '
        Me.txtMedicalRefNo.Flags = 0
        Me.txtMedicalRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMedicalRefNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMedicalRefNo.Location = New System.Drawing.Point(140, 222)
        Me.txtMedicalRefNo.MaxLength = 50
        Me.txtMedicalRefNo.Name = "txtMedicalRefNo"
        Me.txtMedicalRefNo.Size = New System.Drawing.Size(229, 21)
        Me.txtMedicalRefNo.TabIndex = 7
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(140, 330)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(229, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 11
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'lblMedicalRefNo
        '
        Me.lblMedicalRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalRefNo.Location = New System.Drawing.Point(8, 225)
        Me.lblMedicalRefNo.Name = "lblMedicalRefNo"
        Me.lblMedicalRefNo.Size = New System.Drawing.Size(126, 15)
        Me.lblMedicalRefNo.TabIndex = 220
        Me.lblMedicalRefNo.Text = "Reference No."
        Me.lblMedicalRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(126, 14)
        Me.lblPeriod.TabIndex = 217
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(140, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(8, 198)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(126, 15)
        Me.lblCostCenter.TabIndex = 214
        Me.lblCostCenter.Text = "Cost Center"
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(140, 195)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(229, 21)
        Me.cboCostCenter.TabIndex = 6
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = CType(resources.GetObject("objbtnSearchTranHead.Image"), System.Drawing.Image)
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(375, 141)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 211
        '
        'objbtnAddMembership
        '
        Me.objbtnAddMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddMembership.BorderSelected = False
        Me.objbtnAddMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddMembership.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddMembership.Location = New System.Drawing.Point(267, 367)
        Me.objbtnAddMembership.Name = "objbtnAddMembership"
        Me.objbtnAddMembership.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddMembership.TabIndex = 209
        Me.objbtnAddMembership.ToolTipText = "Add Membership on Employee Master"
        Me.objbtnAddMembership.Visible = False
        '
        'lblMembershipName
        '
        Me.lblMembershipName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipName.Location = New System.Drawing.Point(8, 366)
        Me.lblMembershipName.Name = "lblMembershipName"
        Me.lblMembershipName.Size = New System.Drawing.Size(126, 16)
        Me.lblMembershipName.TabIndex = 159
        Me.lblMembershipName.Text = "Membership Category"
        Me.lblMembershipName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblMembershipName.Visible = False
        '
        'lblMembershipType
        '
        Me.lblMembershipType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipType.Location = New System.Drawing.Point(8, 367)
        Me.lblMembershipType.Name = "lblMembershipType"
        Me.lblMembershipType.Size = New System.Drawing.Size(126, 16)
        Me.lblMembershipType.TabIndex = 160
        Me.lblMembershipType.Text = "Membership / Vendor"
        Me.lblMembershipType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblMembershipType.Visible = False
        '
        'cboMembershipType
        '
        Me.cboMembershipType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembershipType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembershipType.FormattingEnabled = True
        Me.cboMembershipType.Location = New System.Drawing.Point(140, 364)
        Me.cboMembershipType.Name = "cboMembershipType"
        Me.cboMembershipType.Size = New System.Drawing.Size(121, 21)
        Me.cboMembershipType.TabIndex = 6
        Me.cboMembershipType.Visible = False
        '
        'lblTypeOf
        '
        Me.lblTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOf.Location = New System.Drawing.Point(8, 117)
        Me.lblTypeOf.Name = "lblTypeOf"
        Me.lblTypeOf.Size = New System.Drawing.Size(126, 15)
        Me.lblTypeOf.TabIndex = 155
        Me.lblTypeOf.Text = "Transaction Type Of"
        '
        'cboTypeOf
        '
        Me.cboTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOf.FormattingEnabled = True
        Me.cboTypeOf.Location = New System.Drawing.Point(140, 114)
        Me.cboTypeOf.Name = "cboTypeOf"
        Me.cboTypeOf.Size = New System.Drawing.Size(229, 21)
        Me.cboTypeOf.TabIndex = 3
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(375, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 152
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(140, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(229, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(126, 15)
        Me.lblEmployee.TabIndex = 151
        Me.lblEmployee.Text = "Employee"
        '
        'objComputeOn
        '
        Me.objComputeOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objComputeOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objComputeOn.FormattingEnabled = True
        Me.objComputeOn.Location = New System.Drawing.Point(218, 364)
        Me.objComputeOn.Name = "objComputeOn"
        Me.objComputeOn.Size = New System.Drawing.Size(43, 21)
        Me.objComputeOn.TabIndex = 148
        Me.objComputeOn.Visible = False
        '
        'objCalcType
        '
        Me.objCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCalcType.FormattingEnabled = True
        Me.objCalcType.Location = New System.Drawing.Point(326, 141)
        Me.objCalcType.Name = "objCalcType"
        Me.objCalcType.Size = New System.Drawing.Size(43, 21)
        Me.objCalcType.TabIndex = 147
        Me.objCalcType.Visible = False
        '
        'objTranHeadType
        '
        Me.objTranHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objTranHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTranHeadType.FormattingEnabled = True
        Me.objTranHeadType.Location = New System.Drawing.Point(326, 87)
        Me.objTranHeadType.Name = "objTranHeadType"
        Me.objTranHeadType.Size = New System.Drawing.Size(43, 21)
        Me.objTranHeadType.TabIndex = 146
        Me.objTranHeadType.Visible = False
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(140, 87)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(229, 21)
        Me.cboTrnHeadType.TabIndex = 2
        '
        'lblVendor
        '
        Me.lblVendor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendor.Location = New System.Drawing.Point(309, 364)
        Me.lblVendor.Name = "lblVendor"
        Me.lblVendor.Size = New System.Drawing.Size(43, 15)
        Me.lblVendor.TabIndex = 144
        Me.lblVendor.Text = "Vendor"
        Me.lblVendor.Visible = False
        '
        'objlblCurrencyName
        '
        Me.objlblCurrencyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCurrencyName.Location = New System.Drawing.Point(315, 367)
        Me.objlblCurrencyName.Name = "objlblCurrencyName"
        Me.objlblCurrencyName.Size = New System.Drawing.Size(41, 15)
        Me.objlblCurrencyName.TabIndex = 141
        Me.objlblCurrencyName.Text = "###"
        Me.objlblCurrencyName.Visible = False
        '
        'cboVendor
        '
        Me.cboVendor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVendor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVendor.FormattingEnabled = True
        Me.cboVendor.Location = New System.Drawing.Point(358, 370)
        Me.cboVendor.Name = "cboVendor"
        Me.cboVendor.Size = New System.Drawing.Size(38, 21)
        Me.cboVendor.TabIndex = 5
        Me.cboVendor.Visible = False
        '
        'lblCurrencyCountry
        '
        Me.lblCurrencyCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrencyCountry.Location = New System.Drawing.Point(265, 367)
        Me.lblCurrencyCountry.Name = "lblCurrencyCountry"
        Me.lblCurrencyCountry.Size = New System.Drawing.Size(46, 15)
        Me.lblCurrencyCountry.TabIndex = 140
        Me.lblCurrencyCountry.Text = "Country Curr."
        Me.lblCurrencyCountry.Visible = False
        '
        'cboCurrencyCountry
        '
        Me.cboCurrencyCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrencyCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrencyCountry.FormattingEnabled = True
        Me.cboCurrencyCountry.Location = New System.Drawing.Point(356, 364)
        Me.cboCurrencyCountry.Name = "cboCurrencyCountry"
        Me.cboCurrencyCountry.Size = New System.Drawing.Size(40, 21)
        Me.cboCurrencyCountry.TabIndex = 3
        Me.cboCurrencyCountry.Visible = False
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(8, 171)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(126, 15)
        Me.lblAmount.TabIndex = 135
        Me.lblAmount.Text = "Amount"
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.DropDownWidth = 300
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(140, 141)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(229, 21)
        Me.cboTrnHead.TabIndex = 4
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 144)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(126, 15)
        Me.lblTrnHead.TabIndex = 134
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'lblTrnHeadType
        '
        Me.lblTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHeadType.Location = New System.Drawing.Point(8, 90)
        Me.lblTrnHeadType.Name = "lblTrnHeadType"
        Me.lblTrnHeadType.Size = New System.Drawing.Size(126, 15)
        Me.lblTrnHeadType.TabIndex = 133
        Me.lblTrnHeadType.Text = "Transaction Head Type"
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(140, 168)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(229, 21)
        Me.txtAmount.TabIndex = 5
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboMemCategory
        '
        Me.cboMemCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMemCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMemCategory.FormattingEnabled = True
        Me.cboMemCategory.Location = New System.Drawing.Point(140, 364)
        Me.cboMemCategory.Name = "cboMemCategory"
        Me.cboMemCategory.Size = New System.Drawing.Size(121, 21)
        Me.cboMemCategory.TabIndex = 5
        Me.cboMemCategory.Visible = False
        '
        'tabpgHistory
        '
        Me.tabpgHistory.Controls.Add(Me.Panel1)
        Me.tabpgHistory.Location = New System.Drawing.Point(4, 22)
        Me.tabpgHistory.Name = "tabpgHistory"
        Me.tabpgHistory.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpgHistory.Size = New System.Drawing.Size(458, 375)
        Me.tabpgHistory.TabIndex = 1
        Me.tabpgHistory.Text = "Show ED History"
        Me.tabpgHistory.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkAddNonRecurrentHeads)
        Me.Panel1.Controls.Add(Me.chkOverwrite)
        Me.Panel1.Controls.Add(Me.lnkCopyEDSlab)
        Me.Panel1.Controls.Add(Me.lblCopyEDPeriod)
        Me.Panel1.Controls.Add(Me.cboCopyEDPeriod)
        Me.Panel1.Controls.Add(Me.lvEDHistory)
        Me.Panel1.Location = New System.Drawing.Point(6, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(446, 372)
        Me.Panel1.TabIndex = 0
        '
        'chkAddNonRecurrentHeads
        '
        Me.chkAddNonRecurrentHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAddNonRecurrentHeads.Location = New System.Drawing.Point(16, 348)
        Me.chkAddNonRecurrentHeads.Name = "chkAddNonRecurrentHeads"
        Me.chkAddNonRecurrentHeads.Size = New System.Drawing.Size(160, 17)
        Me.chkAddNonRecurrentHeads.TabIndex = 2
        Me.chkAddNonRecurrentHeads.Text = "Add Non-Recurrent Heads"
        Me.chkAddNonRecurrentHeads.UseVisualStyleBackColor = True
        '
        'chkOverwrite
        '
        Me.chkOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwrite.Location = New System.Drawing.Point(16, 325)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(160, 17)
        Me.chkOverwrite.TabIndex = 1
        Me.chkOverwrite.Text = "Overwrite if exist"
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'lnkCopyEDSlab
        '
        Me.lnkCopyEDSlab.Location = New System.Drawing.Point(294, 350)
        Me.lnkCopyEDSlab.Name = "lnkCopyEDSlab"
        Me.lnkCopyEDSlab.Size = New System.Drawing.Size(131, 13)
        Me.lnkCopyEDSlab.TabIndex = 5
        Me.lnkCopyEDSlab.TabStop = True
        Me.lnkCopyEDSlab.Text = "Copy Previous ED Slab"
        '
        'lblCopyEDPeriod
        '
        Me.lblCopyEDPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCopyEDPeriod.Location = New System.Drawing.Point(191, 326)
        Me.lblCopyEDPeriod.Name = "lblCopyEDPeriod"
        Me.lblCopyEDPeriod.Size = New System.Drawing.Size(100, 14)
        Me.lblCopyEDPeriod.TabIndex = 3
        Me.lblCopyEDPeriod.Text = "Effective Period"
        Me.lblCopyEDPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCopyEDPeriod
        '
        Me.cboCopyEDPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCopyEDPeriod.DropDownWidth = 215
        Me.cboCopyEDPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCopyEDPeriod.FormattingEnabled = True
        Me.cboCopyEDPeriod.Location = New System.Drawing.Point(297, 323)
        Me.cboCopyEDPeriod.Name = "cboCopyEDPeriod"
        Me.cboCopyEDPeriod.Size = New System.Drawing.Size(104, 21)
        Me.cboCopyEDPeriod.TabIndex = 4
        '
        'lvEDHistory
        '
        Me.lvEDHistory.BackColorOnChecked = True
        Me.lvEDHistory.ColumnHeaders = Nothing
        Me.lvEDHistory.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPeriod, Me.colhTranHead, Me.colhTranheadType, Me.colhCalcType, Me.colhAmount})
        Me.lvEDHistory.CompulsoryColumns = ""
        Me.lvEDHistory.FullRowSelect = True
        Me.lvEDHistory.GridLines = True
        Me.lvEDHistory.GroupingColumn = Nothing
        Me.lvEDHistory.HideSelection = False
        Me.lvEDHistory.Location = New System.Drawing.Point(3, 3)
        Me.lvEDHistory.MinColumnWidth = 50
        Me.lvEDHistory.MultiSelect = False
        Me.lvEDHistory.Name = "lvEDHistory"
        Me.lvEDHistory.OptionalColumns = ""
        Me.lvEDHistory.ShowMoreItem = False
        Me.lvEDHistory.ShowSaveItem = False
        Me.lvEDHistory.ShowSelectAll = True
        Me.lvEDHistory.ShowSizeAllColumnsToFit = True
        Me.lvEDHistory.Size = New System.Drawing.Size(440, 314)
        Me.lvEDHistory.Sortable = True
        Me.lvEDHistory.TabIndex = 0
        Me.lvEDHistory.UseCompatibleStateImageBehavior = False
        Me.lvEDHistory.View = System.Windows.Forms.View.Details
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 0
        '
        'colhTranHead
        '
        Me.colhTranHead.Tag = "colhTranHead"
        Me.colhTranHead.Text = "Transaction Head"
        Me.colhTranHead.Width = 120
        '
        'colhTranheadType
        '
        Me.colhTranheadType.Tag = "colhTranheadType"
        Me.colhTranheadType.Text = "Tran. Head Type"
        Me.colhTranheadType.Width = 100
        '
        'colhCalcType
        '
        Me.colhCalcType.Tag = "colhCalcType"
        Me.colhCalcType.Text = "Calc. Type"
        Me.colhCalcType.Width = 100
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 110
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSaveEDHistory)
        Me.objFooter.Controls.Add(Me.lblDefCC)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 469)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(490, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSaveEDHistory
        '
        Me.btnSaveEDHistory.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveEDHistory.BackColor = System.Drawing.Color.White
        Me.btnSaveEDHistory.BackgroundImage = CType(resources.GetObject("btnSaveEDHistory.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveEDHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveEDHistory.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveEDHistory.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveEDHistory.FlatAppearance.BorderSize = 0
        Me.btnSaveEDHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveEDHistory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveEDHistory.ForeColor = System.Drawing.Color.Black
        Me.btnSaveEDHistory.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveEDHistory.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveEDHistory.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveEDHistory.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveEDHistory.Location = New System.Drawing.Point(278, 13)
        Me.btnSaveEDHistory.Name = "btnSaveEDHistory"
        Me.btnSaveEDHistory.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveEDHistory.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveEDHistory.Size = New System.Drawing.Size(97, 30)
        Me.btnSaveEDHistory.TabIndex = 216
        Me.btnSaveEDHistory.Text = "&Save"
        Me.btnSaveEDHistory.UseVisualStyleBackColor = True
        '
        'lblDefCC
        '
        Me.lblDefCC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefCC.Location = New System.Drawing.Point(12, 7)
        Me.lblDefCC.Name = "lblDefCC"
        Me.lblDefCC.Size = New System.Drawing.Size(209, 39)
        Me.lblDefCC.TabIndex = 215
        Me.lblDefCC.Text = "Default Cost Center :"
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(278, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(381, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(490, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Add / Edit Employee Earning and Deduction"
        '
        'dtpEDStartDate
        '
        Me.dtpEDStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEDStartDate.Checked = False
        Me.dtpEDStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEDStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEDStartDate.Location = New System.Drawing.Point(140, 276)
        Me.dtpEDStartDate.Name = "dtpEDStartDate"
        Me.dtpEDStartDate.ShowCheckBox = True
        Me.dtpEDStartDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpEDStartDate.TabIndex = 9
        '
        'lblEDStartDate
        '
        Me.lblEDStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEDStartDate.Location = New System.Drawing.Point(11, 279)
        Me.lblEDStartDate.Name = "lblEDStartDate"
        Me.lblEDStartDate.Size = New System.Drawing.Size(123, 15)
        Me.lblEDStartDate.TabIndex = 244
        Me.lblEDStartDate.Text = "E. D. Start Date"
        '
        'frmEarningDeduction_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(490, 524)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEarningDeduction_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Earning and Deduction"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.TabED.ResumeLayout(False)
        Me.tabpgED.ResumeLayout(False)
        Me.gbEarningDeduction.ResumeLayout(False)
        Me.gbEarningDeduction.PerformLayout()
        Me.tabpgHistory.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEarningDeduction As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objComputeOn As System.Windows.Forms.ComboBox
    Friend WithEvents objCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents objTranHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVendor As System.Windows.Forms.Label
    Friend WithEvents objlblCurrencyName As System.Windows.Forms.Label
    Friend WithEvents cboVendor As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrencyCountry As System.Windows.Forms.Label
    Friend WithEvents cboCurrencyCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents lblTrnHeadType As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboTypeOf As System.Windows.Forms.ComboBox
    Friend WithEvents lblTypeOf As System.Windows.Forms.Label
    Friend WithEvents cboMembershipType As System.Windows.Forms.ComboBox
    Friend WithEvents cboMemCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembershipName As System.Windows.Forms.Label
    Friend WithEvents lblMembershipType As System.Windows.Forms.Label
    Friend WithEvents objbtnAddMembership As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblDefCC As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents TabED As System.Windows.Forms.TabControl
    Friend WithEvents tabpgED As System.Windows.Forms.TabPage
    Friend WithEvents tabpgHistory As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lvEDHistory As eZee.Common.eZeeListView
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranheadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCalcType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblCopyEDPeriod As System.Windows.Forms.Label
    Friend WithEvents cboCopyEDPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lnkCopyEDSlab As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSaveEDHistory As eZee.Common.eZeeLightButton
    Friend WithEvents chkOverwrite As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents chkAddNonRecurrentHeads As System.Windows.Forms.CheckBox
    Friend WithEvents txtMedicalRefNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMedicalRefNo As System.Windows.Forms.Label
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents dtpCumulativeStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCumulativeStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpStopDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStopDate As System.Windows.Forms.Label
    Friend WithEvents dtpEDStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEDStartDate As System.Windows.Forms.Label
End Class
