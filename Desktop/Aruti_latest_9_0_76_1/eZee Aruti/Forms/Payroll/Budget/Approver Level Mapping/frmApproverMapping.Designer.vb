﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApproverMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApproverMapping))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbApproverMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.cboApproverUser = New System.Windows.Forms.ComboBox
        Me.objbtnSearchLevel = New eZee.Common.eZeeGradientButton
        Me.lblApprLevel = New System.Windows.Forms.Label
        Me.cboLevel = New System.Windows.Forms.ComboBox
        Me.lblApprUser = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbApproverMapping.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbApproverMapping)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(363, 152)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 97)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(363, 55)
        Me.objFooter.TabIndex = 12
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(151, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(254, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbApproverMapping
        '
        Me.gbApproverMapping.BorderColor = System.Drawing.Color.Black
        Me.gbApproverMapping.Checked = False
        Me.gbApproverMapping.CollapseAllExceptThis = False
        Me.gbApproverMapping.CollapsedHoverImage = Nothing
        Me.gbApproverMapping.CollapsedNormalImage = Nothing
        Me.gbApproverMapping.CollapsedPressedImage = Nothing
        Me.gbApproverMapping.CollapseOnLoad = False
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchUser)
        Me.gbApproverMapping.Controls.Add(Me.cboApproverUser)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchLevel)
        Me.gbApproverMapping.Controls.Add(Me.lblApprLevel)
        Me.gbApproverMapping.Controls.Add(Me.cboLevel)
        Me.gbApproverMapping.Controls.Add(Me.lblApprUser)
        Me.gbApproverMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbApproverMapping.ExpandedHoverImage = Nothing
        Me.gbApproverMapping.ExpandedNormalImage = Nothing
        Me.gbApproverMapping.ExpandedPressedImage = Nothing
        Me.gbApproverMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproverMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproverMapping.HeaderHeight = 25
        Me.gbApproverMapping.HeaderMessage = ""
        Me.gbApproverMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbApproverMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproverMapping.HeightOnCollapse = 0
        Me.gbApproverMapping.LeftTextSpace = 0
        Me.gbApproverMapping.Location = New System.Drawing.Point(0, 0)
        Me.gbApproverMapping.Name = "gbApproverMapping"
        Me.gbApproverMapping.OpenHeight = 300
        Me.gbApproverMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproverMapping.ShowBorder = True
        Me.gbApproverMapping.ShowCheckBox = False
        Me.gbApproverMapping.ShowCollapseButton = False
        Me.gbApproverMapping.ShowDefaultBorderColor = True
        Me.gbApproverMapping.ShowDownButton = False
        Me.gbApproverMapping.ShowHeader = True
        Me.gbApproverMapping.Size = New System.Drawing.Size(363, 152)
        Me.gbApproverMapping.TabIndex = 0
        Me.gbApproverMapping.Temp = 0
        Me.gbApproverMapping.Text = "Approver Mapping"
        Me.gbApproverMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(316, 60)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 6
        '
        'cboApproverUser
        '
        Me.cboApproverUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApproverUser.DropDownWidth = 300
        Me.cboApproverUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApproverUser.FormattingEnabled = True
        Me.cboApproverUser.Location = New System.Drawing.Point(124, 60)
        Me.cboApproverUser.Name = "cboApproverUser"
        Me.cboApproverUser.Size = New System.Drawing.Size(186, 21)
        Me.cboApproverUser.TabIndex = 5
        '
        'objbtnSearchLevel
        '
        Me.objbtnSearchLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLevel.BorderSelected = False
        Me.objbtnSearchLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLevel.Location = New System.Drawing.Point(316, 33)
        Me.objbtnSearchLevel.Name = "objbtnSearchLevel"
        Me.objbtnSearchLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLevel.TabIndex = 4
        '
        'lblApprLevel
        '
        Me.lblApprLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprLevel.Location = New System.Drawing.Point(15, 35)
        Me.lblApprLevel.Name = "lblApprLevel"
        Me.lblApprLevel.Size = New System.Drawing.Size(102, 16)
        Me.lblApprLevel.TabIndex = 1
        Me.lblApprLevel.Text = "Approver Level"
        Me.lblApprLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLevel
        '
        Me.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLevel.DropDownWidth = 300
        Me.cboLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLevel.FormattingEnabled = True
        Me.cboLevel.Location = New System.Drawing.Point(123, 33)
        Me.cboLevel.Name = "cboLevel"
        Me.cboLevel.Size = New System.Drawing.Size(187, 21)
        Me.cboLevel.TabIndex = 2
        '
        'lblApprUser
        '
        Me.lblApprUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprUser.Location = New System.Drawing.Point(16, 62)
        Me.lblApprUser.Name = "lblApprUser"
        Me.lblApprUser.Size = New System.Drawing.Size(104, 16)
        Me.lblApprUser.TabIndex = 1
        Me.lblApprUser.Text = "Payment Approver"
        Me.lblApprUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmApproverMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(363, 152)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApproverMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approver Mapping"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbApproverMapping.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbApproverMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblApprLevel As System.Windows.Forms.Label
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprUser As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApproverUser As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchLevel As eZee.Common.eZeeGradientButton
End Class
