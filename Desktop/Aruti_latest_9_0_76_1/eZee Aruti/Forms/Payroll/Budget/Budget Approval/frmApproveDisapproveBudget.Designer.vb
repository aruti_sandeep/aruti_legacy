﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApproveDisapproveBudget
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApproveDisapproveBudget))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbBudgetReqApprovalnfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblApprovalStatus = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.objtlpJob = New System.Windows.Forms.TableLayoutPanel
        Me.lblApproverInfo = New System.Windows.Forms.Label
        Me.objLevelPriorityVal = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.lblLevelPriority = New System.Windows.Forms.Label
        Me.objApproverName = New System.Windows.Forms.Label
        Me.objApproverLevelVal = New System.Windows.Forms.Label
        Me.lblApproverLevel = New System.Windows.Forms.Label
        Me.gbBudgetList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtBudgetDate = New eZee.TextBox.AlphanumericTextBox
        Me.txtWhoToInclude = New eZee.TextBox.AlphanumericTextBox
        Me.txtPresentation = New eZee.TextBox.AlphanumericTextBox
        Me.txtViewBy = New eZee.TextBox.AlphanumericTextBox
        Me.txtBudgetYear = New eZee.TextBox.AlphanumericTextBox
        Me.lblBudgetDate = New System.Windows.Forms.Label
        Me.txtBudgetName = New eZee.TextBox.AlphanumericTextBox
        Me.lblBudgetName = New System.Windows.Forms.Label
        Me.txtBudgetCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblBudgetCode = New System.Windows.Forms.Label
        Me.lblBudgetYear = New System.Windows.Forms.Label
        Me.lblWhoToInclude = New System.Windows.Forms.Label
        Me.lblPresentation = New System.Windows.Forms.Label
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbBudgetReqApprovalnfo.SuspendLayout()
        Me.objtlpJob.SuspendLayout()
        Me.gbBudgetList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbBudgetReqApprovalnfo)
        Me.pnlMainInfo.Controls.Add(Me.gbBudgetList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(601, 425)
        Me.pnlMainInfo.TabIndex = 1
        '
        'gbBudgetReqApprovalnfo
        '
        Me.gbBudgetReqApprovalnfo.BorderColor = System.Drawing.Color.Black
        Me.gbBudgetReqApprovalnfo.Checked = False
        Me.gbBudgetReqApprovalnfo.CollapseAllExceptThis = False
        Me.gbBudgetReqApprovalnfo.CollapsedHoverImage = Nothing
        Me.gbBudgetReqApprovalnfo.CollapsedNormalImage = Nothing
        Me.gbBudgetReqApprovalnfo.CollapsedPressedImage = Nothing
        Me.gbBudgetReqApprovalnfo.CollapseOnLoad = False
        Me.gbBudgetReqApprovalnfo.Controls.Add(Me.cboStatus)
        Me.gbBudgetReqApprovalnfo.Controls.Add(Me.lblApprovalStatus)
        Me.gbBudgetReqApprovalnfo.Controls.Add(Me.txtRemarks)
        Me.gbBudgetReqApprovalnfo.Controls.Add(Me.lblRemarks)
        Me.gbBudgetReqApprovalnfo.Controls.Add(Me.objtlpJob)
        Me.gbBudgetReqApprovalnfo.ExpandedHoverImage = Nothing
        Me.gbBudgetReqApprovalnfo.ExpandedNormalImage = Nothing
        Me.gbBudgetReqApprovalnfo.ExpandedPressedImage = Nothing
        Me.gbBudgetReqApprovalnfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBudgetReqApprovalnfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBudgetReqApprovalnfo.HeaderHeight = 25
        Me.gbBudgetReqApprovalnfo.HeaderMessage = ""
        Me.gbBudgetReqApprovalnfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBudgetReqApprovalnfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBudgetReqApprovalnfo.HeightOnCollapse = 0
        Me.gbBudgetReqApprovalnfo.LeftTextSpace = 0
        Me.gbBudgetReqApprovalnfo.Location = New System.Drawing.Point(282, 66)
        Me.gbBudgetReqApprovalnfo.Name = "gbBudgetReqApprovalnfo"
        Me.gbBudgetReqApprovalnfo.OpenHeight = 182
        Me.gbBudgetReqApprovalnfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBudgetReqApprovalnfo.ShowBorder = True
        Me.gbBudgetReqApprovalnfo.ShowCheckBox = False
        Me.gbBudgetReqApprovalnfo.ShowCollapseButton = False
        Me.gbBudgetReqApprovalnfo.ShowDefaultBorderColor = True
        Me.gbBudgetReqApprovalnfo.ShowDownButton = False
        Me.gbBudgetReqApprovalnfo.ShowHeader = True
        Me.gbBudgetReqApprovalnfo.Size = New System.Drawing.Size(306, 297)
        Me.gbBudgetReqApprovalnfo.TabIndex = 206
        Me.gbBudgetReqApprovalnfo.Temp = 0
        Me.gbBudgetReqApprovalnfo.Text = "Budget Approval Information"
        Me.gbBudgetReqApprovalnfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(104, 171)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(199, 21)
        Me.cboStatus.TabIndex = 0
        '
        'lblApprovalStatus
        '
        Me.lblApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalStatus.Location = New System.Drawing.Point(17, 174)
        Me.lblApprovalStatus.Name = "lblApprovalStatus"
        Me.lblApprovalStatus.Size = New System.Drawing.Size(81, 15)
        Me.lblApprovalStatus.TabIndex = 248
        Me.lblApprovalStatus.Text = "Status"
        Me.lblApprovalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.HideSelection = False
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtRemarks.Location = New System.Drawing.Point(17, 226)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(286, 58)
        Me.txtRemarks.TabIndex = 1
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(17, 203)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(149, 16)
        Me.lblRemarks.TabIndex = 208
        Me.lblRemarks.Text = "Approval Remarks"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objtlpJob
        '
        Me.objtlpJob.BackColor = System.Drawing.SystemColors.Control
        Me.objtlpJob.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objtlpJob.ColumnCount = 2
        Me.objtlpJob.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.objtlpJob.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.objtlpJob.Controls.Add(Me.lblApproverInfo, 0, 0)
        Me.objtlpJob.Controls.Add(Me.objLevelPriorityVal, 1, 3)
        Me.objtlpJob.Controls.Add(Me.lblApprover, 0, 1)
        Me.objtlpJob.Controls.Add(Me.lblLevelPriority, 0, 3)
        Me.objtlpJob.Controls.Add(Me.objApproverName, 1, 1)
        Me.objtlpJob.Controls.Add(Me.objApproverLevelVal, 1, 2)
        Me.objtlpJob.Controls.Add(Me.lblApproverLevel, 0, 2)
        Me.objtlpJob.Location = New System.Drawing.Point(0, 44)
        Me.objtlpJob.Name = "objtlpJob"
        Me.objtlpJob.RowCount = 4
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtlpJob.Size = New System.Drawing.Size(305, 105)
        Me.objtlpJob.TabIndex = 210
        '
        'lblApproverInfo
        '
        Me.objtlpJob.SetColumnSpan(Me.lblApproverInfo, 2)
        Me.lblApproverInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblApproverInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverInfo.Location = New System.Drawing.Point(4, 1)
        Me.lblApproverInfo.Name = "lblApproverInfo"
        Me.lblApproverInfo.Size = New System.Drawing.Size(297, 25)
        Me.lblApproverInfo.TabIndex = 273
        Me.lblApproverInfo.Text = "Approver Information"
        Me.lblApproverInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objLevelPriorityVal
        '
        Me.objLevelPriorityVal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objLevelPriorityVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLevelPriorityVal.Location = New System.Drawing.Point(186, 79)
        Me.objLevelPriorityVal.Name = "objLevelPriorityVal"
        Me.objLevelPriorityVal.Size = New System.Drawing.Size(115, 25)
        Me.objLevelPriorityVal.TabIndex = 272
        Me.objLevelPriorityVal.Text = "0"
        Me.objLevelPriorityVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(4, 27)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(175, 25)
        Me.lblApprover.TabIndex = 141
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLevelPriority
        '
        Me.lblLevelPriority.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblLevelPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelPriority.Location = New System.Drawing.Point(4, 79)
        Me.lblLevelPriority.Name = "lblLevelPriority"
        Me.lblLevelPriority.Size = New System.Drawing.Size(175, 25)
        Me.lblLevelPriority.TabIndex = 271
        Me.lblLevelPriority.Text = "Level Priority"
        Me.lblLevelPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objApproverName
        '
        Me.objApproverName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverName.Location = New System.Drawing.Point(186, 27)
        Me.objApproverName.Name = "objApproverName"
        Me.objApproverName.Size = New System.Drawing.Size(115, 25)
        Me.objApproverName.TabIndex = 207
        Me.objApproverName.Text = "0"
        Me.objApproverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objApproverLevelVal
        '
        Me.objApproverLevelVal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objApproverLevelVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverLevelVal.Location = New System.Drawing.Point(186, 53)
        Me.objApproverLevelVal.Name = "objApproverLevelVal"
        Me.objApproverLevelVal.Size = New System.Drawing.Size(115, 25)
        Me.objApproverLevelVal.TabIndex = 208
        Me.objApproverLevelVal.Text = "0"
        Me.objApproverLevelVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApproverLevel
        '
        Me.lblApproverLevel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverLevel.Location = New System.Drawing.Point(4, 53)
        Me.lblApproverLevel.Name = "lblApproverLevel"
        Me.lblApproverLevel.Size = New System.Drawing.Size(175, 25)
        Me.lblApproverLevel.TabIndex = 204
        Me.lblApproverLevel.Text = "Approver Level"
        Me.lblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBudgetList
        '
        Me.gbBudgetList.BorderColor = System.Drawing.Color.Black
        Me.gbBudgetList.Checked = False
        Me.gbBudgetList.CollapseAllExceptThis = False
        Me.gbBudgetList.CollapsedHoverImage = Nothing
        Me.gbBudgetList.CollapsedNormalImage = Nothing
        Me.gbBudgetList.CollapsedPressedImage = Nothing
        Me.gbBudgetList.CollapseOnLoad = False
        Me.gbBudgetList.Controls.Add(Me.txtBudgetDate)
        Me.gbBudgetList.Controls.Add(Me.txtWhoToInclude)
        Me.gbBudgetList.Controls.Add(Me.txtPresentation)
        Me.gbBudgetList.Controls.Add(Me.txtViewBy)
        Me.gbBudgetList.Controls.Add(Me.txtBudgetYear)
        Me.gbBudgetList.Controls.Add(Me.lblBudgetDate)
        Me.gbBudgetList.Controls.Add(Me.txtBudgetName)
        Me.gbBudgetList.Controls.Add(Me.lblBudgetName)
        Me.gbBudgetList.Controls.Add(Me.txtBudgetCode)
        Me.gbBudgetList.Controls.Add(Me.lblBudgetCode)
        Me.gbBudgetList.Controls.Add(Me.lblBudgetYear)
        Me.gbBudgetList.Controls.Add(Me.lblWhoToInclude)
        Me.gbBudgetList.Controls.Add(Me.lblPresentation)
        Me.gbBudgetList.Controls.Add(Me.lblViewBy)
        Me.gbBudgetList.ExpandedHoverImage = Nothing
        Me.gbBudgetList.ExpandedNormalImage = Nothing
        Me.gbBudgetList.ExpandedPressedImage = Nothing
        Me.gbBudgetList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBudgetList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBudgetList.HeaderHeight = 25
        Me.gbBudgetList.HeaderMessage = ""
        Me.gbBudgetList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBudgetList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBudgetList.HeightOnCollapse = 0
        Me.gbBudgetList.LeftTextSpace = 0
        Me.gbBudgetList.Location = New System.Drawing.Point(12, 66)
        Me.gbBudgetList.Name = "gbBudgetList"
        Me.gbBudgetList.OpenHeight = 300
        Me.gbBudgetList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBudgetList.ShowBorder = True
        Me.gbBudgetList.ShowCheckBox = False
        Me.gbBudgetList.ShowCollapseButton = False
        Me.gbBudgetList.ShowDefaultBorderColor = True
        Me.gbBudgetList.ShowDownButton = False
        Me.gbBudgetList.ShowHeader = True
        Me.gbBudgetList.Size = New System.Drawing.Size(264, 297)
        Me.gbBudgetList.TabIndex = 0
        Me.gbBudgetList.Temp = 0
        Me.gbBudgetList.Text = "Budget Information"
        Me.gbBudgetList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBudgetDate
        '
        Me.txtBudgetDate.Flags = 0
        Me.txtBudgetDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBudgetDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBudgetDate.Location = New System.Drawing.Point(104, 193)
        Me.txtBudgetDate.Name = "txtBudgetDate"
        Me.txtBudgetDate.ReadOnly = True
        Me.txtBudgetDate.Size = New System.Drawing.Size(139, 21)
        Me.txtBudgetDate.TabIndex = 210
        '
        'txtWhoToInclude
        '
        Me.txtWhoToInclude.Flags = 0
        Me.txtWhoToInclude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWhoToInclude.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWhoToInclude.Location = New System.Drawing.Point(104, 166)
        Me.txtWhoToInclude.Name = "txtWhoToInclude"
        Me.txtWhoToInclude.ReadOnly = True
        Me.txtWhoToInclude.Size = New System.Drawing.Size(139, 21)
        Me.txtWhoToInclude.TabIndex = 209
        '
        'txtPresentation
        '
        Me.txtPresentation.Flags = 0
        Me.txtPresentation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresentation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPresentation.Location = New System.Drawing.Point(104, 139)
        Me.txtPresentation.Name = "txtPresentation"
        Me.txtPresentation.ReadOnly = True
        Me.txtPresentation.Size = New System.Drawing.Size(139, 21)
        Me.txtPresentation.TabIndex = 208
        '
        'txtViewBy
        '
        Me.txtViewBy.Flags = 0
        Me.txtViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtViewBy.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtViewBy.Location = New System.Drawing.Point(104, 112)
        Me.txtViewBy.Name = "txtViewBy"
        Me.txtViewBy.ReadOnly = True
        Me.txtViewBy.Size = New System.Drawing.Size(139, 21)
        Me.txtViewBy.TabIndex = 207
        '
        'txtBudgetYear
        '
        Me.txtBudgetYear.Flags = 0
        Me.txtBudgetYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBudgetYear.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBudgetYear.Location = New System.Drawing.Point(104, 85)
        Me.txtBudgetYear.Name = "txtBudgetYear"
        Me.txtBudgetYear.ReadOnly = True
        Me.txtBudgetYear.Size = New System.Drawing.Size(139, 21)
        Me.txtBudgetYear.TabIndex = 206
        '
        'lblBudgetDate
        '
        Me.lblBudgetDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetDate.Location = New System.Drawing.Point(15, 195)
        Me.lblBudgetDate.Name = "lblBudgetDate"
        Me.lblBudgetDate.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetDate.TabIndex = 205
        Me.lblBudgetDate.Text = "Budget Date"
        Me.lblBudgetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBudgetName
        '
        Me.txtBudgetName.Flags = 0
        Me.txtBudgetName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBudgetName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBudgetName.Location = New System.Drawing.Point(104, 58)
        Me.txtBudgetName.Name = "txtBudgetName"
        Me.txtBudgetName.ReadOnly = True
        Me.txtBudgetName.Size = New System.Drawing.Size(139, 21)
        Me.txtBudgetName.TabIndex = 193
        '
        'lblBudgetName
        '
        Me.lblBudgetName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetName.Location = New System.Drawing.Point(15, 60)
        Me.lblBudgetName.Name = "lblBudgetName"
        Me.lblBudgetName.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetName.TabIndex = 203
        Me.lblBudgetName.Text = "Budget Name"
        Me.lblBudgetName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBudgetCode
        '
        Me.txtBudgetCode.Flags = 0
        Me.txtBudgetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBudgetCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBudgetCode.Location = New System.Drawing.Point(104, 31)
        Me.txtBudgetCode.Name = "txtBudgetCode"
        Me.txtBudgetCode.ReadOnly = True
        Me.txtBudgetCode.Size = New System.Drawing.Size(139, 21)
        Me.txtBudgetCode.TabIndex = 192
        '
        'lblBudgetCode
        '
        Me.lblBudgetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetCode.Location = New System.Drawing.Point(15, 32)
        Me.lblBudgetCode.Name = "lblBudgetCode"
        Me.lblBudgetCode.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetCode.TabIndex = 202
        Me.lblBudgetCode.Text = "Budget Code"
        Me.lblBudgetCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBudgetYear
        '
        Me.lblBudgetYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetYear.Location = New System.Drawing.Point(15, 87)
        Me.lblBudgetYear.Name = "lblBudgetYear"
        Me.lblBudgetYear.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetYear.TabIndex = 201
        Me.lblBudgetYear.Text = "Budget Year"
        Me.lblBudgetYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWhoToInclude
        '
        Me.lblWhoToInclude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWhoToInclude.Location = New System.Drawing.Point(15, 167)
        Me.lblWhoToInclude.Name = "lblWhoToInclude"
        Me.lblWhoToInclude.Size = New System.Drawing.Size(83, 16)
        Me.lblWhoToInclude.TabIndex = 200
        Me.lblWhoToInclude.Text = "Who to Include"
        Me.lblWhoToInclude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPresentation
        '
        Me.lblPresentation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPresentation.Location = New System.Drawing.Point(15, 141)
        Me.lblPresentation.Name = "lblPresentation"
        Me.lblPresentation.Size = New System.Drawing.Size(83, 16)
        Me.lblPresentation.TabIndex = 199
        Me.lblPresentation.Text = "Presentation"
        Me.lblPresentation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(15, 114)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(83, 16)
        Me.lblViewBy.TabIndex = 198
        Me.lblViewBy.Text = "Budget For"
        Me.lblViewBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(601, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Approve / Disapprove Budget"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 370)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(601, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(389, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(492, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmApproveDisapproveBudget
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(601, 425)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApproveDisapproveBudget"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Approve / Disapprove Budget"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbBudgetReqApprovalnfo.ResumeLayout(False)
        Me.gbBudgetReqApprovalnfo.PerformLayout()
        Me.objtlpJob.ResumeLayout(False)
        Me.gbBudgetList.ResumeLayout(False)
        Me.gbBudgetList.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbBudgetReqApprovalnfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovalStatus As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents objtlpJob As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblApproverInfo As System.Windows.Forms.Label
    Friend WithEvents objLevelPriorityVal As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents lblLevelPriority As System.Windows.Forms.Label
    Friend WithEvents objApproverName As System.Windows.Forms.Label
    Friend WithEvents objApproverLevelVal As System.Windows.Forms.Label
    Friend WithEvents lblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents gbBudgetList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblBudgetDate As System.Windows.Forms.Label
    Friend WithEvents txtBudgetName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBudgetName As System.Windows.Forms.Label
    Friend WithEvents txtBudgetCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBudgetCode As System.Windows.Forms.Label
    Friend WithEvents lblBudgetYear As System.Windows.Forms.Label
    Friend WithEvents lblWhoToInclude As System.Windows.Forms.Label
    Friend WithEvents lblPresentation As System.Windows.Forms.Label
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents txtBudgetDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtWhoToInclude As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPresentation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtViewBy As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBudgetYear As eZee.TextBox.AlphanumericTextBox
End Class
