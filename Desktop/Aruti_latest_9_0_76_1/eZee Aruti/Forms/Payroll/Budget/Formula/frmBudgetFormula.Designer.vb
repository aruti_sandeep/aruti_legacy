﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetFormula
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBudgetFormula))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbTrnHeadInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.dtpCumulativeStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblSimpleSlabPeriod = New System.Windows.Forms.Label
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnAddComputation = New eZee.Common.eZeeLine
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.pnlComputation = New System.Windows.Forms.Panel
        Me.lvFormula = New eZee.Common.eZeeListView(Me.components)
        Me.colhFunction = New System.Windows.Forms.ColumnHeader
        Me.colhFormula = New System.Windows.Forms.ColumnHeader
        Me.colhDefaultValue = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.lblFormula = New System.Windows.Forms.Label
        Me.txtHeadName = New eZee.TextBox.AlphanumericTextBox
        Me.txtConstantValue = New eZee.TextBox.NumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.objbtnSearchFunction = New eZee.Common.eZeeGradientButton
        Me.txtHeadCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboDefaultValue = New System.Windows.Forms.ComboBox
        Me.cboFunction = New System.Windows.Forms.ComboBox
        Me.lblDefaultValue = New System.Windows.Forms.Label
        Me.lblTrnHead_Leave = New System.Windows.Forms.Label
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.lblFunction = New System.Windows.Forms.Label
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.cboSalaryHead = New System.Windows.Forms.ComboBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbTrnHeadInfo.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlComputation.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.gbTrnHeadInfo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(489, 515)
        Me.Panel1.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 460)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(489, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(278, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(381, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbTrnHeadInfo
        '
        Me.gbTrnHeadInfo.BorderColor = System.Drawing.Color.Black
        Me.gbTrnHeadInfo.Checked = False
        Me.gbTrnHeadInfo.CollapseAllExceptThis = False
        Me.gbTrnHeadInfo.CollapsedHoverImage = Nothing
        Me.gbTrnHeadInfo.CollapsedNormalImage = Nothing
        Me.gbTrnHeadInfo.CollapsedPressedImage = Nothing
        Me.gbTrnHeadInfo.CollapseOnLoad = False
        Me.gbTrnHeadInfo.Controls.Add(Me.Panel2)
        Me.gbTrnHeadInfo.ExpandedHoverImage = Nothing
        Me.gbTrnHeadInfo.ExpandedNormalImage = Nothing
        Me.gbTrnHeadInfo.ExpandedPressedImage = Nothing
        Me.gbTrnHeadInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrnHeadInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrnHeadInfo.HeaderHeight = 25
        Me.gbTrnHeadInfo.HeaderMessage = ""
        Me.gbTrnHeadInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTrnHeadInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTrnHeadInfo.HeightOnCollapse = 0
        Me.gbTrnHeadInfo.LeftTextSpace = 0
        Me.gbTrnHeadInfo.Location = New System.Drawing.Point(12, 7)
        Me.gbTrnHeadInfo.Name = "gbTrnHeadInfo"
        Me.gbTrnHeadInfo.OpenHeight = 182
        Me.gbTrnHeadInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrnHeadInfo.ShowBorder = True
        Me.gbTrnHeadInfo.ShowCheckBox = False
        Me.gbTrnHeadInfo.ShowCollapseButton = False
        Me.gbTrnHeadInfo.ShowDefaultBorderColor = True
        Me.gbTrnHeadInfo.ShowDownButton = False
        Me.gbTrnHeadInfo.ShowHeader = True
        Me.gbTrnHeadInfo.Size = New System.Drawing.Size(466, 447)
        Me.gbTrnHeadInfo.TabIndex = 0
        Me.gbTrnHeadInfo.Temp = 0
        Me.gbTrnHeadInfo.Text = "Formula Transaction Head Information"
        Me.gbTrnHeadInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.dtpCumulativeStartDate)
        Me.Panel2.Controls.Add(Me.lblSimpleSlabPeriod)
        Me.Panel2.Controls.Add(Me.btnDelete)
        Me.Panel2.Controls.Add(Me.btnAdd)
        Me.Panel2.Controls.Add(Me.lnAddComputation)
        Me.Panel2.Controls.Add(Me.objbtnOtherLanguage)
        Me.Panel2.Controls.Add(Me.txtFormula)
        Me.Panel2.Controls.Add(Me.pnlComputation)
        Me.Panel2.Controls.Add(Me.lblFormula)
        Me.Panel2.Controls.Add(Me.txtHeadName)
        Me.Panel2.Controls.Add(Me.txtConstantValue)
        Me.Panel2.Controls.Add(Me.lblName)
        Me.Panel2.Controls.Add(Me.objbtnSearchFunction)
        Me.Panel2.Controls.Add(Me.txtHeadCode)
        Me.Panel2.Controls.Add(Me.lblCode)
        Me.Panel2.Controls.Add(Me.cboDefaultValue)
        Me.Panel2.Controls.Add(Me.cboFunction)
        Me.Panel2.Controls.Add(Me.lblDefaultValue)
        Me.Panel2.Controls.Add(Me.lblTrnHead_Leave)
        Me.Panel2.Controls.Add(Me.objbtnSearchTranHead)
        Me.Panel2.Controls.Add(Me.lblFunction)
        Me.Panel2.Controls.Add(Me.cboTrnHead)
        Me.Panel2.Controls.Add(Me.objelLine1)
        Me.Panel2.Controls.Add(Me.cboSalaryHead)
        Me.Panel2.Location = New System.Drawing.Point(2, 26)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(462, 419)
        Me.Panel2.TabIndex = 0
        '
        'dtpCumulativeStartDate
        '
        Me.dtpCumulativeStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCumulativeStartDate.Checked = False
        Me.dtpCumulativeStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCumulativeStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCumulativeStartDate.Location = New System.Drawing.Point(99, 144)
        Me.dtpCumulativeStartDate.Name = "dtpCumulativeStartDate"
        Me.dtpCumulativeStartDate.Size = New System.Drawing.Size(99, 21)
        Me.dtpCumulativeStartDate.TabIndex = 16
        '
        'lblSimpleSlabPeriod
        '
        Me.lblSimpleSlabPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSimpleSlabPeriod.Location = New System.Drawing.Point(11, 147)
        Me.lblSimpleSlabPeriod.Name = "lblSimpleSlabPeriod"
        Me.lblSimpleSlabPeriod.Size = New System.Drawing.Size(82, 14)
        Me.lblSimpleSlabPeriod.TabIndex = 15
        Me.lblSimpleSlabPeriod.Text = "Effec. Period"
        Me.lblSimpleSlabPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(313, 139)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(115, 30)
        Me.btnDelete.TabIndex = 18
        Me.btnDelete.Text = "&Delete Formula"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(226, 139)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(81, 30)
        Me.btnAdd.TabIndex = 17
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lnAddComputation
        '
        Me.lnAddComputation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnAddComputation.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnAddComputation.Location = New System.Drawing.Point(8, 67)
        Me.lnAddComputation.Name = "lnAddComputation"
        Me.lnAddComputation.Size = New System.Drawing.Size(380, 12)
        Me.lnAddComputation.TabIndex = 5
        Me.lnAddComputation.Text = "Additional Computation"
        Me.lnAddComputation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(433, 38)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 20)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'txtFormula
        '
        Me.txtFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormula.Location = New System.Drawing.Point(94, 195)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.ReadOnly = True
        Me.txtFormula.Size = New System.Drawing.Size(351, 21)
        Me.txtFormula.TabIndex = 21
        Me.txtFormula.TabStop = False
        '
        'pnlComputation
        '
        Me.pnlComputation.Controls.Add(Me.lvFormula)
        Me.pnlComputation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlComputation.Location = New System.Drawing.Point(20, 222)
        Me.pnlComputation.Name = "pnlComputation"
        Me.pnlComputation.Size = New System.Drawing.Size(425, 189)
        Me.pnlComputation.TabIndex = 99
        '
        'lvFormula
        '
        Me.lvFormula.BackColorOnChecked = True
        Me.lvFormula.ColumnHeaders = Nothing
        Me.lvFormula.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFunction, Me.colhFormula, Me.colhDefaultValue, Me.colhEndDate})
        Me.lvFormula.CompulsoryColumns = ""
        Me.lvFormula.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvFormula.FullRowSelect = True
        Me.lvFormula.GridLines = True
        Me.lvFormula.GroupingColumn = Nothing
        Me.lvFormula.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvFormula.HideSelection = False
        Me.lvFormula.Location = New System.Drawing.Point(0, 0)
        Me.lvFormula.MinColumnWidth = 50
        Me.lvFormula.MultiSelect = False
        Me.lvFormula.Name = "lvFormula"
        Me.lvFormula.OptionalColumns = ""
        Me.lvFormula.ShowMoreItem = False
        Me.lvFormula.ShowSaveItem = False
        Me.lvFormula.ShowSelectAll = True
        Me.lvFormula.ShowSizeAllColumnsToFit = True
        Me.lvFormula.Size = New System.Drawing.Size(425, 189)
        Me.lvFormula.Sortable = False
        Me.lvFormula.TabIndex = 0
        Me.lvFormula.UseCompatibleStateImageBehavior = False
        Me.lvFormula.View = System.Windows.Forms.View.Details
        '
        'colhFunction
        '
        Me.colhFunction.Tag = "colhFunction"
        Me.colhFunction.Text = "Function"
        Me.colhFunction.Width = 120
        '
        'colhFormula
        '
        Me.colhFormula.Tag = "colhFormula"
        Me.colhFormula.Text = "Formula"
        Me.colhFormula.Width = 220
        '
        'colhDefaultValue
        '
        Me.colhDefaultValue.Tag = "colhDefaultValue"
        Me.colhDefaultValue.Text = "Default Value"
        Me.colhDefaultValue.Width = 80
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = ""
        Me.colhEndDate.Width = 0
        '
        'lblFormula
        '
        Me.lblFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormula.Location = New System.Drawing.Point(14, 199)
        Me.lblFormula.Name = "lblFormula"
        Me.lblFormula.Size = New System.Drawing.Size(74, 13)
        Me.lblFormula.TabIndex = 20
        Me.lblFormula.Text = "Formula"
        '
        'txtHeadName
        '
        Me.txtHeadName.Flags = 0
        Me.txtHeadName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeadName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHeadName.Location = New System.Drawing.Point(173, 38)
        Me.txtHeadName.MaxLength = 50
        Me.txtHeadName.Name = "txtHeadName"
        Me.txtHeadName.Size = New System.Drawing.Size(255, 21)
        Me.txtHeadName.TabIndex = 3
        '
        'txtConstantValue
        '
        Me.txtConstantValue.AllowNegative = True
        Me.txtConstantValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtConstantValue.DigitsInGroup = 0
        Me.txtConstantValue.Flags = 0
        Me.txtConstantValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConstantValue.Location = New System.Drawing.Point(173, 112)
        Me.txtConstantValue.MaxDecimalPlaces = 6
        Me.txtConstantValue.MaxWholeDigits = 21
        Me.txtConstantValue.Name = "txtConstantValue"
        Me.txtConstantValue.Prefix = ""
        Me.txtConstantValue.RangeMax = 1.7976931348623157E+308
        Me.txtConstantValue.RangeMin = -1.7976931348623157E+308
        Me.txtConstantValue.Size = New System.Drawing.Size(98, 21)
        Me.txtConstantValue.TabIndex = 10
        Me.txtConstantValue.Text = "0"
        Me.txtConstantValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 41)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(156, 13)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Tran. Head Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchFunction
        '
        Me.objbtnSearchFunction.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFunction.BorderSelected = False
        Me.objbtnSearchFunction.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFunction.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFunction.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFunction.Location = New System.Drawing.Point(433, 85)
        Me.objbtnSearchFunction.Name = "objbtnSearchFunction"
        Me.objbtnSearchFunction.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFunction.TabIndex = 8
        '
        'txtHeadCode
        '
        Me.txtHeadCode.Flags = 0
        Me.txtHeadCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeadCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHeadCode.Location = New System.Drawing.Point(173, 11)
        Me.txtHeadCode.MaxLength = 50
        Me.txtHeadCode.Name = "txtHeadCode"
        Me.txtHeadCode.Size = New System.Drawing.Size(255, 21)
        Me.txtHeadCode.TabIndex = 1
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 14)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(156, 13)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Tran. Head Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDefaultValue
        '
        Me.cboDefaultValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDefaultValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDefaultValue.FormattingEnabled = True
        Me.cboDefaultValue.Location = New System.Drawing.Point(173, 139)
        Me.cboDefaultValue.Name = "cboDefaultValue"
        Me.cboDefaultValue.Size = New System.Drawing.Size(255, 21)
        Me.cboDefaultValue.TabIndex = 14
        Me.cboDefaultValue.Tag = ""
        Me.cboDefaultValue.Visible = False
        '
        'cboFunction
        '
        Me.cboFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFunction.FormattingEnabled = True
        Me.cboFunction.Location = New System.Drawing.Point(173, 85)
        Me.cboFunction.Name = "cboFunction"
        Me.cboFunction.Size = New System.Drawing.Size(255, 21)
        Me.cboFunction.TabIndex = 7
        Me.cboFunction.Tag = ""
        '
        'lblDefaultValue
        '
        Me.lblDefaultValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefaultValue.Location = New System.Drawing.Point(11, 143)
        Me.lblDefaultValue.Name = "lblDefaultValue"
        Me.lblDefaultValue.Size = New System.Drawing.Size(156, 29)
        Me.lblDefaultValue.TabIndex = 13
        Me.lblDefaultValue.Text = "Default Value to be Considered if not assigned on ED"
        Me.lblDefaultValue.Visible = False
        '
        'lblTrnHead_Leave
        '
        Me.lblTrnHead_Leave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead_Leave.Location = New System.Drawing.Point(11, 116)
        Me.lblTrnHead_Leave.Name = "lblTrnHead_Leave"
        Me.lblTrnHead_Leave.Size = New System.Drawing.Size(156, 13)
        Me.lblTrnHead_Leave.TabIndex = 9
        Me.lblTrnHead_Leave.Text = "Budget Head"
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(433, 112)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 12
        '
        'lblFunction
        '
        Me.lblFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFunction.Location = New System.Drawing.Point(11, 89)
        Me.lblFunction.Name = "lblFunction"
        Me.lblFunction.Size = New System.Drawing.Size(156, 13)
        Me.lblFunction.TabIndex = 6
        Me.lblFunction.Text = "Function"
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Items.AddRange(New Object() {"Select"})
        Me.cboTrnHead.Location = New System.Drawing.Point(173, 112)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(255, 21)
        Me.cboTrnHead.TabIndex = 11
        Me.cboTrnHead.Tag = ""
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(14, 182)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(404, 10)
        Me.objelLine1.TabIndex = 19
        '
        'cboSalaryHead
        '
        Me.cboSalaryHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalaryHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalaryHead.FormattingEnabled = True
        Me.cboSalaryHead.Items.AddRange(New Object() {"Select"})
        Me.cboSalaryHead.Location = New System.Drawing.Point(173, 112)
        Me.cboSalaryHead.Name = "cboSalaryHead"
        Me.cboSalaryHead.Size = New System.Drawing.Size(255, 21)
        Me.cboSalaryHead.TabIndex = 227
        Me.cboSalaryHead.Tag = ""
        '
        'frmBudgetFormula
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(489, 515)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBudgetFormula"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Formula"
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbTrnHeadInfo.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlComputation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbTrnHeadInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtHeadName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtHeadCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtConstantValue As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchFunction As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSimpleSlabPeriod As System.Windows.Forms.Label
    Friend WithEvents cboDefaultValue As System.Windows.Forms.ComboBox
    Friend WithEvents lblDefaultValue As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents lblFormula As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents pnlComputation As System.Windows.Forms.Panel
    Friend WithEvents lvFormula As eZee.Common.eZeeListView
    Friend WithEvents colhFunction As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFormula As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDefaultValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents cboFunction As System.Windows.Forms.ComboBox
    Friend WithEvents lblFunction As System.Windows.Forms.Label
    Friend WithEvents lblTrnHead_Leave As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dtpCumulativeStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnAddComputation As eZee.Common.eZeeLine
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cboSalaryHead As System.Windows.Forms.ComboBox
End Class
