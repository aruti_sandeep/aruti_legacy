﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBudgetCodesList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBudgetCodesList"
    Private objBudgetCodes As clsBudgetcodes_master

    Private mstrSearchText As String = ""
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Try

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearch As String = String.Empty
        Dim dsList As DataSet
        Try

            If User._Object.Privilege._AllowToViewBudgetCodes = False Then Exit Sub

            dsList = objBudgetCodes.GetList("List", CInt(cboPeriod.SelectedValue), strSearch)

            dgvBudgetList.AutoGenerateColumns = False

            objcolhBudgetCodesUnkid.DataPropertyName = "budgetcodesunkid"
            colhPeriod.DataPropertyName = "period_name"
            objcolhBudgetUnkid.DataPropertyName = "budgetunkid"
            colhBudgetCode.DataPropertyName = "budget_code"
            colhBudgetName.DataPropertyName = "budget_name"
            objcolhPayyearUnkid.DataPropertyName = "payyearunkid"
            colhPayyear.DataPropertyName = "payyear"
            objcolhViewById.DataPropertyName = "viewbyid"
            colhViewBy.DataPropertyName = "ViewBy"
            objcolhAllocationById.DataPropertyName = "allocationbyid"
            colhAllocationBy.DataPropertyName = "AllocationBy"
            objcolhPresentationmodeId.DataPropertyName = "presentationmodeid"
            colhPresentationmode.DataPropertyName = "PresentationMode"
            objcolhWhotoIncludeId.DataPropertyName = "whotoincludeid"
            colhWhotoInclude.DataPropertyName = "WhoToInclude"
            objcolhSalaryLevelId.DataPropertyName = "salarylevelid"
            colhSalaryLevel.DataPropertyName = "SalaryLevel"
            colhIsDefault.DataPropertyName = "isdefaultYesNo"
            objcolhIsDefault.DataPropertyName = "isdefault"
            colhIsApproved.DataPropertyName = "budget_status"
            objcolhIsApproved.DataPropertyName = "budget_statusunkid"
            objcolhPeriodStatusId.DataPropertyName = "statusid"

            dgvBudgetList.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvBudgetList.RowCount.ToString)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddBudgetCodes
            btnEdit.Enabled = User._Object.Privilege._AllowToEditBudgetCodes
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBudgetCodes

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText()
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 4, "Type to Search")
            With cboPeriod
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBudgetCodesList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmBudgetCodesList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetCodesList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetCodesList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvBudgetList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetCodesList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetCodesList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudget_MasterNew.SetMessages()
            clsBudget_TranNew.SetMessages()
            objfrm._Other_ModuleNames = "clsBudget_MasterNew, clsBudget_TranNew"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmBudgetCodesList_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmBudgetCodesList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBudgetCodes = New clsBudgetcodes_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            Call FillList()

            Call SetDefaultSearchText()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmBudgetCodesList_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.GotFocus
        Try
            With cboPeriod
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.Leave
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Call SetDefaultSearchText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboPeriod.ValueMember
                    .DisplayMember = cboPeriod.DisplayMember
                    .DataSource = CType(cboPeriod.DataSource, DataTable)
                    .CodeMember = "code"

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cboPeriod.SelectedValue = frm.SelectedValue
                    cboPeriod.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboPeriod.Text = ""
                    cboPeriod.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmBudgetCodes
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
                Call FillCombo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmBudgetCodes
        Try
            If dgvBudgetList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            End If

            If frm.displayDialog(CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhBudgetCodesUnkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try
            If dgvBudgetList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
                'ElseIf CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhPeriodStatusId.Index).Value) = enStatusType.Close Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this budget code. Reason : Period is already closed."), enMsgBoxStyle.Information)
                '    dgvBudgetList.Focus()
                '    Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Delete this Budget codes transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objBudgetCodes = New clsBudgetcodes_master

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBudgetCodes._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objBudgetCodes._Isvoid = True
                objBudgetCodes._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objBudgetCodes._Voiduserunkid = User._Object._Userunkid

                If objBudgetCodes.Void(CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhBudgetCodesUnkid.Index).Value), ConfigParameter._Object._CurrentDateAndTime) Then
                    Call FillList()
                    Call FillCombo()
                Else
                    If objBudgetCodes._Message <> "" Then
                        eZeeMsgBox.Show(objBudgetCodes._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            Call SetDefaultSearchText()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbEmployeeExempetion.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeExempetion.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbEmployeeExempetion.Text = Language._Object.getCaption(Me.gbEmployeeExempetion.Name, Me.gbEmployeeExempetion.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhPeriod.HeaderText = Language._Object.getCaption(Me.colhPeriod.Name, Me.colhPeriod.HeaderText)
			Me.colhBudgetCode.HeaderText = Language._Object.getCaption(Me.colhBudgetCode.Name, Me.colhBudgetCode.HeaderText)
			Me.colhBudgetName.HeaderText = Language._Object.getCaption(Me.colhBudgetName.Name, Me.colhBudgetName.HeaderText)
			Me.colhPayyear.HeaderText = Language._Object.getCaption(Me.colhPayyear.Name, Me.colhPayyear.HeaderText)
			Me.colhViewBy.HeaderText = Language._Object.getCaption(Me.colhViewBy.Name, Me.colhViewBy.HeaderText)
			Me.colhAllocationBy.HeaderText = Language._Object.getCaption(Me.colhAllocationBy.Name, Me.colhAllocationBy.HeaderText)
			Me.colhPresentationmode.HeaderText = Language._Object.getCaption(Me.colhPresentationmode.Name, Me.colhPresentationmode.HeaderText)
			Me.colhWhotoInclude.HeaderText = Language._Object.getCaption(Me.colhWhotoInclude.Name, Me.colhWhotoInclude.HeaderText)
			Me.colhSalaryLevel.HeaderText = Language._Object.getCaption(Me.colhSalaryLevel.Name, Me.colhSalaryLevel.HeaderText)
			Me.colhIsDefault.HeaderText = Language._Object.getCaption(Me.colhIsDefault.Name, Me.colhIsDefault.HeaderText)
			Me.colhIsApproved.HeaderText = Language._Object.getCaption(Me.colhIsApproved.Name, Me.colhIsApproved.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this budget code. Reason : Period is already closed.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to Delete this Budget codes transaction?")
			Language.setMessage(mstrModuleName, 4, "Type to Search")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class