﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetCodes
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBudgetCodes))
        Me.objchkSelectAll_Budget = New System.Windows.Forms.CheckBox
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuExportPercentageAllocation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportPercentageAllocation = New System.Windows.Forms.ToolStripMenuItem
        Me.objbgw = New System.ComponentModel.BackgroundWorker
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkCopyPreviousPeriod = New System.Windows.Forms.LinkLabel
        Me.gbBudget = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkActivityCode = New System.Windows.Forms.CheckBox
        Me.chkPercentage = New System.Windows.Forms.CheckBox
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.btnApply = New eZee.Common.eZeeLightButton(Me.components)
        Me.radApplyToAll = New System.Windows.Forms.RadioButton
        Me.radApplyToChecked = New System.Windows.Forms.RadioButton
        Me.cboActivityCode = New System.Windows.Forms.ComboBox
        Me.lblActivityCode = New System.Windows.Forms.Label
        Me.cboProjectCode = New System.Windows.Forms.ComboBox
        Me.lblProjectCode = New System.Windows.Forms.Label
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.txtTotalPercentage = New eZee.TextBox.NumericTextBox
        Me.lblTotalPercentage = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboBudget = New System.Windows.Forms.ComboBox
        Me.lblBudget = New System.Windows.Forms.Label
        Me.dgvBudget = New Aruti.Data.GroupByGrid
        Me.mnuOperations.SuspendLayout()
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbBudget.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBudget, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objchkSelectAll_Budget
        '
        Me.objchkSelectAll_Budget.AutoSize = True
        Me.objchkSelectAll_Budget.Location = New System.Drawing.Point(19, 121)
        Me.objchkSelectAll_Budget.Name = "objchkSelectAll_Budget"
        Me.objchkSelectAll_Budget.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll_Budget.TabIndex = 194
        Me.objchkSelectAll_Budget.UseVisualStyleBackColor = True
        Me.objchkSelectAll_Budget.Visible = False
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExportPercentageAllocation, Me.mnuImportPercentageAllocation})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(230, 48)
        '
        'mnuExportPercentageAllocation
        '
        Me.mnuExportPercentageAllocation.Name = "mnuExportPercentageAllocation"
        Me.mnuExportPercentageAllocation.Size = New System.Drawing.Size(229, 22)
        Me.mnuExportPercentageAllocation.Text = "&Export Percentage Allocation"
        '
        'mnuImportPercentageAllocation
        '
        Me.mnuImportPercentageAllocation.Name = "mnuImportPercentageAllocation"
        Me.mnuImportPercentageAllocation.Size = New System.Drawing.Size(229, 22)
        Me.mnuImportPercentageAllocation.Text = "&Import Percentage Allocation"
        '
        'objbgw
        '
        Me.objbgw.WorkerReportsProgress = True
        Me.objbgw.WorkerSupportsCancellation = True
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbBudget)
        Me.pnlMainInfo.Controls.Add(Me.dgvBudget)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(998, 652)
        Me.pnlMainInfo.TabIndex = 195
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.lnkCopyPreviousPeriod)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 602)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(998, 50)
        Me.objFooter.TabIndex = 195
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(613, 19)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(141, 13)
        Me.objlblProgress.TabIndex = 197
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 9)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 196
        Me.btnOperations.Text = "Operations"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(893, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(793, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lnkCopyPreviousPeriod
        '
        Me.lnkCopyPreviousPeriod.Location = New System.Drawing.Point(351, 18)
        Me.lnkCopyPreviousPeriod.Name = "lnkCopyPreviousPeriod"
        Me.lnkCopyPreviousPeriod.Size = New System.Drawing.Size(226, 13)
        Me.lnkCopyPreviousPeriod.TabIndex = 195
        Me.lnkCopyPreviousPeriod.TabStop = True
        Me.lnkCopyPreviousPeriod.Text = "Copy Previous Period Budget Codes"
        '
        'gbBudget
        '
        Me.gbBudget.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbBudget.BorderColor = System.Drawing.Color.Black
        Me.gbBudget.Checked = False
        Me.gbBudget.CollapseAllExceptThis = False
        Me.gbBudget.CollapsedHoverImage = Nothing
        Me.gbBudget.CollapsedNormalImage = Nothing
        Me.gbBudget.CollapsedPressedImage = Nothing
        Me.gbBudget.CollapseOnLoad = False
        Me.gbBudget.Controls.Add(Me.chkActivityCode)
        Me.gbBudget.Controls.Add(Me.chkPercentage)
        Me.gbBudget.Controls.Add(Me.lblPercentage)
        Me.gbBudget.Controls.Add(Me.txtPercentage)
        Me.gbBudget.Controls.Add(Me.btnApply)
        Me.gbBudget.Controls.Add(Me.radApplyToAll)
        Me.gbBudget.Controls.Add(Me.radApplyToChecked)
        Me.gbBudget.Controls.Add(Me.cboActivityCode)
        Me.gbBudget.Controls.Add(Me.lblActivityCode)
        Me.gbBudget.Controls.Add(Me.cboProjectCode)
        Me.gbBudget.Controls.Add(Me.lblProjectCode)
        Me.gbBudget.Controls.Add(Me.cboCondition)
        Me.gbBudget.Controls.Add(Me.txtTotalPercentage)
        Me.gbBudget.Controls.Add(Me.lblTotalPercentage)
        Me.gbBudget.Controls.Add(Me.lnkAllocation)
        Me.gbBudget.Controls.Add(Me.objbtnReset)
        Me.gbBudget.Controls.Add(Me.objbtnSearch)
        Me.gbBudget.Controls.Add(Me.cboPeriod)
        Me.gbBudget.Controls.Add(Me.lblPeriod)
        Me.gbBudget.Controls.Add(Me.cboBudget)
        Me.gbBudget.Controls.Add(Me.lblBudget)
        Me.gbBudget.ExpandedHoverImage = Nothing
        Me.gbBudget.ExpandedNormalImage = Nothing
        Me.gbBudget.ExpandedPressedImage = Nothing
        Me.gbBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBudget.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBudget.HeaderHeight = 29
        Me.gbBudget.HeaderMessage = ""
        Me.gbBudget.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBudget.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBudget.HeightOnCollapse = 0
        Me.gbBudget.LeftTextSpace = 0
        Me.gbBudget.Location = New System.Drawing.Point(12, 6)
        Me.gbBudget.Name = "gbBudget"
        Me.gbBudget.OpenHeight = 300
        Me.gbBudget.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBudget.ShowBorder = True
        Me.gbBudget.ShowCheckBox = False
        Me.gbBudget.ShowCollapseButton = False
        Me.gbBudget.ShowDefaultBorderColor = True
        Me.gbBudget.ShowDownButton = False
        Me.gbBudget.ShowHeader = True
        Me.gbBudget.Size = New System.Drawing.Size(974, 95)
        Me.gbBudget.TabIndex = 194
        Me.gbBudget.Temp = 0
        Me.gbBudget.Text = "Budget Codes Information"
        Me.gbBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActivityCode
        '
        Me.chkActivityCode.AutoSize = True
        Me.chkActivityCode.Location = New System.Drawing.Point(593, 63)
        Me.chkActivityCode.Name = "chkActivityCode"
        Me.chkActivityCode.Size = New System.Drawing.Size(90, 17)
        Me.chkActivityCode.TabIndex = 218
        Me.chkActivityCode.Text = "Activity Code"
        Me.chkActivityCode.UseVisualStyleBackColor = True
        '
        'chkPercentage
        '
        Me.chkPercentage.AutoSize = True
        Me.chkPercentage.Location = New System.Drawing.Point(501, 63)
        Me.chkPercentage.Name = "chkPercentage"
        Me.chkPercentage.Size = New System.Drawing.Size(81, 17)
        Me.chkPercentage.TabIndex = 217
        Me.chkPercentage.Text = "Percentage"
        Me.chkPercentage.UseVisualStyleBackColor = True
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(710, 63)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(62, 16)
        Me.lblPercentage.TabIndex = 215
        Me.lblPercentage.Text = "Percentage"
        Me.lblPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = True
        Me.txtPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 0
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(778, 61)
        Me.txtPercentage.MaxDecimalPlaces = 2
        Me.txtPercentage.MaxWholeDigits = 21
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(60, 21)
        Me.txtPercentage.TabIndex = 214
        Me.txtPercentage.Text = "0"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.BackColor = System.Drawing.Color.White
        Me.btnApply.BackgroundImage = CType(resources.GetObject("btnApply.BackgroundImage"), System.Drawing.Image)
        Me.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApply.BorderColor = System.Drawing.Color.Empty
        Me.btnApply.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApply.FlatAppearance.BorderSize = 0
        Me.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApply.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.ForeColor = System.Drawing.Color.Black
        Me.btnApply.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApply.GradientForeColor = System.Drawing.Color.Black
        Me.btnApply.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Location = New System.Drawing.Point(874, 58)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Size = New System.Drawing.Size(94, 30)
        Me.btnApply.TabIndex = 213
        Me.btnApply.Text = "&Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'radApplyToAll
        '
        Me.radApplyToAll.Location = New System.Drawing.Point(501, 36)
        Me.radApplyToAll.Name = "radApplyToAll"
        Me.radApplyToAll.Size = New System.Drawing.Size(86, 16)
        Me.radApplyToAll.TabIndex = 211
        Me.radApplyToAll.Text = "Apply To All"
        Me.radApplyToAll.UseVisualStyleBackColor = True
        '
        'radApplyToChecked
        '
        Me.radApplyToChecked.Checked = True
        Me.radApplyToChecked.Location = New System.Drawing.Point(593, 36)
        Me.radApplyToChecked.Name = "radApplyToChecked"
        Me.radApplyToChecked.Size = New System.Drawing.Size(118, 16)
        Me.radApplyToChecked.TabIndex = 212
        Me.radApplyToChecked.TabStop = True
        Me.radApplyToChecked.Text = "Apply To Checked"
        Me.radApplyToChecked.UseVisualStyleBackColor = True
        '
        'cboActivityCode
        '
        Me.cboActivityCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivityCode.FormattingEnabled = True
        Me.cboActivityCode.Location = New System.Drawing.Point(346, 61)
        Me.cboActivityCode.Name = "cboActivityCode"
        Me.cboActivityCode.Size = New System.Drawing.Size(139, 21)
        Me.cboActivityCode.TabIndex = 209
        '
        'lblActivityCode
        '
        Me.lblActivityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityCode.Location = New System.Drawing.Point(253, 63)
        Me.lblActivityCode.Name = "lblActivityCode"
        Me.lblActivityCode.Size = New System.Drawing.Size(87, 16)
        Me.lblActivityCode.TabIndex = 210
        Me.lblActivityCode.Text = "Activity Code"
        Me.lblActivityCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboProjectCode
        '
        Me.cboProjectCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProjectCode.FormattingEnabled = True
        Me.cboProjectCode.Location = New System.Drawing.Point(101, 61)
        Me.cboProjectCode.Name = "cboProjectCode"
        Me.cboProjectCode.Size = New System.Drawing.Size(139, 21)
        Me.cboProjectCode.TabIndex = 207
        '
        'lblProjectCode
        '
        Me.lblProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProjectCode.Location = New System.Drawing.Point(8, 63)
        Me.lblProjectCode.Name = "lblProjectCode"
        Me.lblProjectCode.Size = New System.Drawing.Size(87, 16)
        Me.lblProjectCode.TabIndex = 208
        Me.lblProjectCode.Text = "Project Code"
        Me.lblProjectCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCondition
        '
        Me.cboCondition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCondition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Location = New System.Drawing.Point(713, 4)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(58, 21)
        Me.cboCondition.TabIndex = 206
        '
        'txtTotalPercentage
        '
        Me.txtTotalPercentage.AllowNegative = True
        Me.txtTotalPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalPercentage.DigitsInGroup = 0
        Me.txtTotalPercentage.Flags = 0
        Me.txtTotalPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPercentage.Location = New System.Drawing.Point(777, 4)
        Me.txtTotalPercentage.MaxDecimalPlaces = 2
        Me.txtTotalPercentage.MaxWholeDigits = 21
        Me.txtTotalPercentage.Name = "txtTotalPercentage"
        Me.txtTotalPercentage.Prefix = ""
        Me.txtTotalPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPercentage.Size = New System.Drawing.Size(60, 21)
        Me.txtTotalPercentage.TabIndex = 205
        Me.txtTotalPercentage.Text = "0"
        Me.txtTotalPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalPercentage
        '
        Me.lblTotalPercentage.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPercentage.Location = New System.Drawing.Point(652, 6)
        Me.lblTotalPercentage.Name = "lblTotalPercentage"
        Me.lblTotalPercentage.Size = New System.Drawing.Size(55, 16)
        Me.lblTotalPercentage.TabIndex = 204
        Me.lblTotalPercentage.Text = "Total %"
        Me.lblTotalPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(824, 7)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(88, 15)
        Me.lnkAllocation.TabIndex = 203
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(943, 3)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(25, 24)
        Me.objbtnReset.TabIndex = 202
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(918, 3)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(23, 24)
        Me.objbtnSearch.TabIndex = 201
        Me.objbtnSearch.TabStop = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(346, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(139, 21)
        Me.cboPeriod.TabIndex = 198
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(253, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(87, 16)
        Me.lblPeriod.TabIndex = 199
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBudget
        '
        Me.cboBudget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudget.FormattingEnabled = True
        Me.cboBudget.Location = New System.Drawing.Point(101, 34)
        Me.cboBudget.Name = "cboBudget"
        Me.cboBudget.Size = New System.Drawing.Size(139, 21)
        Me.cboBudget.TabIndex = 195
        '
        'lblBudget
        '
        Me.lblBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudget.Location = New System.Drawing.Point(8, 36)
        Me.lblBudget.Name = "lblBudget"
        Me.lblBudget.Size = New System.Drawing.Size(87, 16)
        Me.lblBudget.TabIndex = 196
        Me.lblBudget.Text = "Default Budget"
        Me.lblBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvBudget
        '
        Me.dgvBudget.AllowUserToAddRows = False
        Me.dgvBudget.AllowUserToDeleteRows = False
        Me.dgvBudget.BackgroundColor = System.Drawing.Color.White
        Me.dgvBudget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBudget.IgnoreFirstColumn = False
        Me.dgvBudget.Location = New System.Drawing.Point(12, 107)
        Me.dgvBudget.Name = "dgvBudget"
        Me.dgvBudget.RowHeadersVisible = False
        Me.dgvBudget.Size = New System.Drawing.Size(974, 483)
        Me.dgvBudget.TabIndex = 196
        '
        'frmBudgetCodes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(998, 652)
        Me.Controls.Add(Me.objchkSelectAll_Budget)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBudgetCodes"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Codes"
        Me.mnuOperations.ResumeLayout(False)
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbBudget.ResumeLayout(False)
        Me.gbBudget.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBudget, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents objchkSelectAll_Budget As System.Windows.Forms.CheckBox
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuExportPercentageAllocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportPercentageAllocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objbgw As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents lnkCopyPreviousPeriod As System.Windows.Forms.LinkLabel
    Friend WithEvents gbBudget As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkActivityCode As System.Windows.Forms.CheckBox
    Friend WithEvents chkPercentage As System.Windows.Forms.CheckBox
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents txtPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents btnApply As eZee.Common.eZeeLightButton
    Friend WithEvents radApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents radApplyToChecked As System.Windows.Forms.RadioButton
    Friend WithEvents cboActivityCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblActivityCode As System.Windows.Forms.Label
    Friend WithEvents cboProjectCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblProjectCode As System.Windows.Forms.Label
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents txtTotalPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalPercentage As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboBudget As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudget As System.Windows.Forms.Label
    Friend WithEvents dgvBudget As Aruti.Data.GroupByGrid
End Class
