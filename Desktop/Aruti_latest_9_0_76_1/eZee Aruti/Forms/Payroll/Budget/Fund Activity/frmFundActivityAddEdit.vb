﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFundActivityAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFundActivityAddEdit"
    Private mblnCancel As Boolean = True
    Private objFundActivity As clsfundactivity_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintFundActivitytranunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFundActivitytranunkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintFundActivitytranunkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            cboFundProjectCode.BackColor = GUI.ColorComp
            txtActivityCode.BackColor = GUI.ColorComp
            txtActivityName.BackColor = GUI.ColorComp
            txtAmount.BackColor = GUI.ColorComp
            txtNotifyAmount.BackColor = GUI.ColorOptional
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Description on Fund Activity).
            txtRemarks.BackColor = GUI.ColorOptional
            'Sohail (02 Sep 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboFundProjectCode.SelectedValue = objFundActivity._FundProjectCodeunkid
            txtActivityCode.Text = objFundActivity._Activity_Code
            txtActivityName.Text = objFundActivity._Activity_Name
            txtAmount.Decimal = objFundActivity._Amount
            txtNotifyAmount.Decimal = objFundActivity._Notify_Amount
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Description on Fund Activity).
            txtRemarks.Text = objFundActivity._Remarks
            'Sohail (02 Sep 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objFundActivity._FundProjectCodeunkid = CInt(cboFundProjectCode.SelectedValue)
            objFundActivity._Activity_Code = txtActivityCode.Text.Trim
            objFundActivity._Activity_Name = txtActivityName.Text.Trim
            objFundActivity._Amount = txtAmount.Decimal
            objFundActivity._Notify_Amount = txtNotifyAmount.Decimal
            objFundActivity._Userunkid = User._Object._Userunkid
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Description on Fund Activity).
            objFundActivity._Remarks = txtRemarks.Text.Trim
            'Sohail (02 Sep 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objFundProjectCode As New clsFundProjectCode
            Dim dsList As DataSet = objFundProjectCode.GetComboList("List", True)
            cboFundProjectCode.ValueMember = "fundprojectcodeunkid"
            cboFundProjectCode.DisplayMember = "fundprojectname"
            cboFundProjectCode.DataSource = dsList.Tables(0)
            cboFundProjectCode.SelectedValue = 0
            objFundProjectCode = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmFundActivityAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objFundActivity = Nothing
    End Sub

    Private Sub frmFundActivityAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFundActivity = New clsfundactivity_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call setColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objFundActivity._Fundactivityunkid = mintFundActivitytranunkid
                'Sohail (02 Sep 2016) -- Start
                'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Description on Fund Activity).
                cboFundProjectCode.Enabled = False
                objbtnSearchFundProjectCode.Enabled = False
                'Sohail (02 Sep 2016) -- End
            End If
            Call GetValue()
            cboFundProjectCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivity_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundActivityAddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundActivityAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsfundactivity_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsfundactivity_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If CInt(cboFundProjectCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Fund Project Code is compulsory information.Please Select Fund Project Code."), enMsgBoxStyle.Information) '?1
                cboFundProjectCode.Select()
                Exit Sub
            ElseIf Trim(txtActivityCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Activity Code cannot be blank. Activity Code is required information."), enMsgBoxStyle.Information) '?1
                txtActivityCode.Focus()
                Exit Sub
            ElseIf Trim(txtActivityName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Activity Name cannot be blank. Activity Name is required information."), enMsgBoxStyle.Information) '?1
                txtActivityName.Focus()
                Exit Sub
            End If

            Dim mdecBalance As Decimal = 0
            Dim strProjectCode As String = ""
            Dim mdecActivityTotal As Decimal = objFundActivity.GetFundActivityTotal(CInt(cboFundProjectCode.SelectedValue), mdecBalance, strProjectCode)

            If mdecBalance < ((mdecActivityTotal - objFundActivity._Amount) + txtAmount.Decimal) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Total Amount of all activities in selected Fund Project Code should not be greater than selected Fund Project Code ceiling amount.") & vbCrLf & vbCrLf & strProjectCode & " Project Code Balance : " & Format(mdecBalance, GUI.fmtCurrency) & vbCrLf & "Expected Total of Activities in the Project Code : " & Format(((mdecActivityTotal - objFundActivity._Amount) + txtAmount.Decimal), GUI.fmtCurrency), enMsgBoxStyle.Information) '?1
                txtAmount.Focus()
                Exit Sub
            End If


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objFundActivity.Update(ConfigParameter._Object._CurrentDateAndTime)
            Else
                blnFlag = objFundActivity.Insert()
            End If

            If blnFlag = False And objFundActivity._Message <> "" Then
                eZeeMsgBox.Show(objFundActivity._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objFundActivity = Nothing
                    objFundActivity = New clsfundactivity_Tran
                    Call GetValue()
                    cboFundProjectCode.Focus()
                Else
                    mintFundActivitytranunkid = objFundActivity._Fundactivityunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtActivityName.Text, objFundActivity._Activity_Name1, objFundActivity._Activity_Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFundProjectCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFundProjectCode.Click
        Dim frm As New frmCommonSearch
        Try
            If cboFundProjectCode.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboFundProjectCode.ValueMember
                .DisplayMember = cboFundProjectCode.DisplayMember
                .CodeMember = "fundprojectcode"
                .DataSource = CType(cboFundProjectCode.DataSource, DataTable)

                If .DisplayDialog Then
                    cboFundProjectCode.SelectedValue = .SelectedValue
                    cboFundProjectCode.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFundProjectCode_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbActivityInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbActivityInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblActivityCode.Text = Language._Object.getCaption(Me.lblActivityCode.Name, Me.lblActivityCode.Text)
			Me.lblActivityName.Text = Language._Object.getCaption(Me.lblActivityName.Name, Me.lblActivityName.Text)
			Me.lblFundProjectCode.Text = Language._Object.getCaption(Me.lblFundProjectCode.Name, Me.lblFundProjectCode.Text)
			Me.gbActivityInformation.Text = Language._Object.getCaption(Me.gbActivityInformation.Name, Me.gbActivityInformation.Text)
            Me.lblCurrentBalance.Text = Language._Object.getCaption(Me.lblCurrentBalance.Name, Me.lblCurrentBalance.Text)
			Me.lblNotifyAmount.Text = Language._Object.getCaption(Me.lblNotifyAmount.Name, Me.lblNotifyAmount.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Fund Project Code is compulsory information.Please Select Fund Project Code.")
			Language.setMessage(mstrModuleName, 2, "Activity Code cannot be blank. Activity Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Activity Name cannot be blank. Activity Name is required information.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Total Amount of all activities in selected Fund Project Code should not be greater than selected Fund Project Code ceiling amount.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class