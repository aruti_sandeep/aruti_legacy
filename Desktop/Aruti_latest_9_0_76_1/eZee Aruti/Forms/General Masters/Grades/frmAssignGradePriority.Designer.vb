﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignGradePriority
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssignGradePriority))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblMessage2 = New System.Windows.Forms.Label
        Me.lblMessage1 = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvGradePriority = New System.Windows.Forms.DataGridView
        Me.objcolhGradeGroupUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeGroupCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeGroupName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhGradeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhGradeLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeLevelCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeLevelName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvGradePriority, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.Panel1)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(687, 483)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblMessage2)
        Me.objFooter.Controls.Add(Me.lblMessage1)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 428)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(687, 55)
        Me.objFooter.TabIndex = 12
        '
        'lblMessage2
        '
        Me.lblMessage2.ForeColor = System.Drawing.Color.Red
        Me.lblMessage2.Location = New System.Drawing.Point(9, 30)
        Me.lblMessage2.Name = "lblMessage2"
        Me.lblMessage2.Size = New System.Drawing.Size(460, 13)
        Me.lblMessage2.TabIndex = 122
        Me.lblMessage2.Text = "* Priority 0 = Ignore for Auto Salary Increment"
        '
        'lblMessage1
        '
        Me.lblMessage1.ForeColor = System.Drawing.Color.Red
        Me.lblMessage1.Location = New System.Drawing.Point(9, 13)
        Me.lblMessage1.Name = "lblMessage1"
        Me.lblMessage1.Size = New System.Drawing.Size(460, 13)
        Me.lblMessage1.TabIndex = 121
        Me.lblMessage1.Text = "* Priority 1 = Lower Priority"
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(475, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 120
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(578, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvGradePriority)
        Me.Panel1.Location = New System.Drawing.Point(12, 83)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(662, 339)
        Me.Panel1.TabIndex = 11
        '
        'dgvGradePriority
        '
        Me.dgvGradePriority.AllowUserToAddRows = False
        Me.dgvGradePriority.AllowUserToDeleteRows = False
        Me.dgvGradePriority.BackgroundColor = System.Drawing.Color.White
        Me.dgvGradePriority.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGradePriority.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhGradeGroupUnkId, Me.colhGradeGroupCode, Me.colhGradeGroupName, Me.objcolhGradeId, Me.colhGradeCode, Me.colhGradeName, Me.objcolhGradeLevelId, Me.colhGradeLevelCode, Me.colhGradeLevelName, Me.colhPriority})
        Me.dgvGradePriority.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvGradePriority.Location = New System.Drawing.Point(0, 0)
        Me.dgvGradePriority.Name = "dgvGradePriority"
        Me.dgvGradePriority.RowHeadersVisible = False
        Me.dgvGradePriority.Size = New System.Drawing.Size(662, 339)
        Me.dgvGradePriority.TabIndex = 0
        '
        'objcolhGradeGroupUnkId
        '
        Me.objcolhGradeGroupUnkId.HeaderText = "Grade Group Id"
        Me.objcolhGradeGroupUnkId.Name = "objcolhGradeGroupUnkId"
        Me.objcolhGradeGroupUnkId.ReadOnly = True
        Me.objcolhGradeGroupUnkId.Visible = False
        '
        'colhGradeGroupCode
        '
        Me.colhGradeGroupCode.HeaderText = "Grade Group Code"
        Me.colhGradeGroupCode.Name = "colhGradeGroupCode"
        Me.colhGradeGroupCode.ReadOnly = True
        Me.colhGradeGroupCode.Width = 150
        '
        'colhGradeGroupName
        '
        Me.colhGradeGroupName.HeaderText = "Grade Group Name"
        Me.colhGradeGroupName.Name = "colhGradeGroupName"
        Me.colhGradeGroupName.ReadOnly = True
        Me.colhGradeGroupName.Width = 200
        '
        'objcolhGradeId
        '
        Me.objcolhGradeId.HeaderText = "Grade Id"
        Me.objcolhGradeId.Name = "objcolhGradeId"
        Me.objcolhGradeId.ReadOnly = True
        Me.objcolhGradeId.Visible = False
        '
        'colhGradeCode
        '
        Me.colhGradeCode.HeaderText = "Grade Code"
        Me.colhGradeCode.Name = "colhGradeCode"
        Me.colhGradeCode.ReadOnly = True
        Me.colhGradeCode.Width = 150
        '
        'colhGradeName
        '
        Me.colhGradeName.HeaderText = "Grade Name"
        Me.colhGradeName.Name = "colhGradeName"
        Me.colhGradeName.ReadOnly = True
        Me.colhGradeName.Width = 200
        '
        'objcolhGradeLevelId
        '
        Me.objcolhGradeLevelId.HeaderText = "Grade Level Id"
        Me.objcolhGradeLevelId.Name = "objcolhGradeLevelId"
        Me.objcolhGradeLevelId.ReadOnly = True
        Me.objcolhGradeLevelId.Visible = False
        '
        'colhGradeLevelCode
        '
        Me.colhGradeLevelCode.HeaderText = "Grade Level Code"
        Me.colhGradeLevelCode.Name = "colhGradeLevelCode"
        Me.colhGradeLevelCode.ReadOnly = True
        Me.colhGradeLevelCode.Width = 150
        '
        'colhGradeLevelName
        '
        Me.colhGradeLevelName.HeaderText = "Grade Level Name"
        Me.colhGradeLevelName.Name = "colhGradeLevelName"
        Me.colhGradeLevelName.ReadOnly = True
        Me.colhGradeLevelName.Width = 200
        '
        'colhPriority
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhPriority.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhPriority.HeaderText = "Priority"
        Me.colhPriority.Name = "colhPriority"
        Me.colhPriority.Width = 150
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboGradeGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblGradeGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(662, 65)
        Me.gbFilterCriteria.TabIndex = 10
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(446, 33)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(205, 21)
        Me.cboGrade.TabIndex = 142
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(380, 36)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(60, 15)
        Me.lblGrade.TabIndex = 141
        Me.lblGrade.Text = "Grade "
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(108, 33)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(205, 21)
        Me.cboGradeGroup.TabIndex = 30
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(94, 15)
        Me.lblGradeGroup.TabIndex = 29
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(635, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 139
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(612, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 138
        Me.objbtnSearch.TabStop = False
        '
        'frmAssignGradePriority
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 483)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssignGradePriority"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Assign Grade Priority"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvGradePriority, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvGradePriority As System.Windows.Forms.DataGridView
    Friend WithEvents lblMessage1 As System.Windows.Forms.Label
    Friend WithEvents lblMessage2 As System.Windows.Forms.Label
    Friend WithEvents objcolhGradeGroupUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeGroupCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeGroupName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhGradeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhGradeLevelId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeLevelCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeLevelName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
