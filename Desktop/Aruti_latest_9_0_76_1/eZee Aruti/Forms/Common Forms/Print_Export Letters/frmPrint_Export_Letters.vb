Option Strict On

#Region " Imports "

Imports System
Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Text.RegularExpressions
Imports System.Text
Imports Aruti.Data.Language
Imports System.Threading
Imports System.IO
Imports System.Collections.Specialized

#End Region

Public Class frmPrint_Export_Letters

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPrint_Export_Letters"
    Private strData As String = ""
    Private mintModulRefId As Integer = -1
    Private dsList As New DataSet
    Private strMailAddess As String()
    Private mstrEmployeeId As String
    Private intCount As Integer = 0
    Private dtRow As DataRow() = Nothing
    Private mstrPeriodIds As String = String.Empty
    Private checkPrint As Integer = 0
    Private mblnIsPriting As Boolean = False
    Private mblnIsEmployee As Boolean = False
    Delegate Sub Set_Rich_Delegate(ByVal [rtfDoc] As RichTextBox, ByVal [rtfText] As String)
    Private objEmail_Master As clsEmail_Master


    'Pinkal (18-Sep-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrLeaveFormIds As String = ""
    'Pinkal (18-Sep-2012) -- End


#End Region

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    <Runtime.InteropServices.DllImport("gdiplus.dll")> _
 Private Shared Function GdipEmfToWmfBits(ByVal _hEmf As IntPtr, ByVal _bufferSize As UInteger, ByVal _buffer() As Byte, ByVal _mappingMode As Integer, ByVal _flags As EmfToWmfBitsFlags) As UInteger
    End Function

    Private Enum EmfToWmfBitsFlags
        EmfToWmfBitsFlagsDefault = &H0
        EmfToWmfBitsFlagsEmbedEmf = &H1
        EmfToWmfBitsFlagsIncludePlaceable = &H2
        EmfToWmfBitsFlagsNoXORClip = &H4
    End Enum
    'S.SANDEEP [05-Apr-2018] -- END

#Region " Properties "

    Public Property _ModulRefId() As Integer
        Get
            Return mintModulRefId
        End Get
        Set(ByVal value As Integer)
            mintModulRefId = value
        End Set
    End Property

    Public Property _IsPrinting() As Boolean
        Get
            Return mblnIsPriting
        End Get
        Set(ByVal value As Boolean)
            mblnIsPriting = value
        End Set
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intRefId As Integer, ByVal strEmailAddress As String, ByVal dsData As DataSet) As Boolean
        Try
            mintModulRefId = intRefId
            txtToAddress.Text = strEmailAddress
            dsList = dsData
            btnTo.Enabled = False

            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtToAddress.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetAddress()
        strMailAddess = txtToAddress.Text.Split(CChar(","))
        Try
            For i As Integer = 0 To strMailAddess.Length - 1
                Dim strFinalAddess As String = ""
                Dim k As Integer = strMailAddess(i).IndexOf("<")
                Dim j As Integer = strMailAddess(i).IndexOf(">")
                If k > 0 Then
                    strFinalAddess = strMailAddess(i).Substring(k)
                    strFinalAddess = strFinalAddess.Replace("<", "")
                    strFinalAddess = strFinalAddess.Replace(">", "")
                    strMailAddess.SetValue(Trim(strFinalAddess), i)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetAddress", mstrModuleName)
        End Try
    End Sub

    Private Sub ViewMergeData(ByVal intNumber As Integer)
        Try
            RemoveHandler rtbDocument.TextChanged, AddressOf rtbDocument_TextChanged
            Dim strDataName As String = ""


            'Pinkal (18-Sep-2012) -- Start
            'Enhancement : TRA Changes

            For i As Integer = intNumber To intNumber
                Dim StrCol As String = ""

                If mstrLeaveFormIds.Trim.Length <= 0 Then

                    For j As Integer = 0 To dtRow(0).Table.Columns.Count - 1
                        StrCol = dtRow(0).Table.Columns(j).ColumnName
                        If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
                            rtbDocument.Focus()
                            'S.SANDEEP [05-Apr-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                            'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                            If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                Dim data As Byte() = CType(dtRow(0).Item(StrCol), Byte())
                                Dim ms As IO.MemoryStream = New MemoryStream(data)
                                Dim img As Image = Image.FromStream(ms)
                                Dim strValue As String = embedImage(img)
                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", strValue)
                            ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString.Replace("\r\n", "\line"))
                            Else
                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                            End If
                            'S.SANDEEP [05-Apr-2018] -- END
                            rtbDocument.Rtf = strDataName
                        End If
                    Next

                Else

                    For j As Integer = 0 To dtRow(intNumber).Table.Columns.Count - 1
                        StrCol = dtRow(intNumber).Table.Columns(j).ColumnName
                        If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
                            rtbDocument.Focus()
                            'S.SANDEEP [05-Apr-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                            'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                            If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                Dim data As Byte() = CType(dtRow(intNumber).Item(StrCol), Byte())
                                Dim ms As MemoryStream = New MemoryStream(data)
                                Dim img As Image = Image.FromStream(ms)
                                Dim strValue As String = embedImage(img)
                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", strValue)
                            ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString.Replace("\r\n", "\line"))
                            Else
                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString)
                            End If
                            'S.SANDEEP [05-Apr-2018] -- END
                            rtbDocument.Rtf = strDataName
                        End If
                    Next

                End If

                'Sohail (20 Jan 2021) -- Start
                'NMB Enhancement : - Allow calculation in letter template.
                Dim arr As MatchCollection = Regex.Matches(rtbDocument.Rtf, "<calc[^>]*>(?<TEXT>[^<]*)<\/calc>", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim dt As New DataTable
                    Dim ans As Object = Nothing
                    Try
                        ans = dt.Compute(s.Groups("TEXT").ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<calc>" & s.Groups("TEXT").ToString & "</calc>", Format(ans, "###,###,###,##0.00"), , , CompareMethod.Text)
                    rtbDocument.Rtf = sreplace
                Next
                'Sohail (20 Jan 2021) -- End


                'S.SANDEEP |13-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : BONUS LETTER
                arr = Nothing
                arr = Regex.Matches(rtbDocument.Rtf, "<hcode[^>]*>(?<TEXT>[^<]*)<\/hcode>", RegexOptions.IgnoreCase)
                If arr IsNot Nothing AndAlso arr.Count > 0 Then
                    Dim objPR As New clsPayrollProcessTran
                    Dim objMD As New clsMasterData
                    Dim objPD As New clscommom_period_Tran
                    Dim objCS As New clsComputeScore_master
                    Dim intPeriodId As Integer = -1
                    For Each s As RegularExpressions.Match In arr
                        intPeriodId = objMD.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, False, False)
                        '*********************************** PAYROLL AMOUNT START
                        Dim amt As Decimal = 0
                        If intPeriodId > 0 Then
                            objPD._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                            Dim dsAmt As New DataSet
                            dsAmt = objPR.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPD._Start_Date, objPD._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(dtRow(0)("EmpId")), intPeriodId, 0, False, "prtranhead_master.trnheadcode = '" & s.Groups("TEXT").ToString() & "' ", "", False, "")
                            If dsAmt IsNot Nothing AndAlso dsAmt.Tables.Count > 0 Then
                                If dsAmt.Tables(0).Rows.Count > 0 Then
                                    amt = CDec(dsAmt.Tables(0).Rows(0)("amount"))
                                End If
                            End If
                        End If
                        Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<hcode>" & s.Groups("TEXT").ToString & "</hcode>", Format(amt, "###,###,###,##0.00"), , , CompareMethod.Text)
                        rtbDocument.Rtf = sreplace
                        '*********************************** PAYROLL AMOUNT END

                        '*********************************** PERFORMANCE RATING START
                        intPeriodId = -1
                        Dim strRating As String = ""
                        intPeriodId = objMD.getFirstPeriodID(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, 1, True, False)
                        If intPeriodId > 0 Then
                            strRating = objCS.GetRating(CInt(dtRow(0)("EmpId")), intPeriodId, ConfigParameter._Object._IsCalibrationSettingActive)
                        End If
                        sreplace = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<rating>", strRating, , , CompareMethod.Text)
                        rtbDocument.Rtf = sreplace
                        '*********************************** PERFORMANCE RATING END
                    Next
                    objPR = Nothing
                    objMD = Nothing
                    objPD = Nothing
                    objCS = Nothing
                End If
                'S.SANDEEP |13-FEB-2021| -- END

            Next

            'Pinkal (18-Sep-2012) -- End



            AddHandler rtbDocument.TextChanged, AddressOf rtbDocument_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        End Try
    End Sub

    Private Sub CallFrom(ByVal intModuleRefId As Integer)
        Try

            Select Case intModuleRefId
                Case enImg_Email_RefId.Employee_Module
                    Dim objFrm As New frmEmployeeList
                    If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
                        objFrm._DataView = dsList
                    End If
                    objFrm.objFooter.Visible = False
                    objFrm.IsExport_Print = True
                    objFrm._IsFromMail = True
                    objFrm.ShowDialog()
                    mstrEmployeeId = objFrm.EmployeeIDs
                    dsList = objFrm._DataView
                    txtToAddress.Text = ""
                    If objFrm.EmailList IsNot Nothing Then
                        For Each str1 As String In objFrm.EmailList
                            If str1 Is Nothing Then
                                Exit For
                            End If
                            If txtToAddress.Text.Contains(str1) Then
                                Continue For
                            Else
                                txtToAddress.Text &= str1 & " , "
                            End If
                        Next
                    End If

                Case enImg_Email_RefId.Applicant_Module
                    Dim objFrm As New frmApplicantList
                    If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
                        objFrm._DataView = dsList
                    End If
                    objFrm.objFooter.Visible = False
                    objFrm.IsExport_Print = True
                    objFrm._IsFromMail = True
                    objFrm.ShowDialog()
                    mstrEmployeeId = objFrm.EmployeeIDs
                    dsList = objFrm._DataView
                    txtToAddress.Text = ""
                    If objFrm.EmailList IsNot Nothing Then
                        For Each str1 As String In objFrm.EmailList
                            If str1 Is Nothing Then
                                Exit For
                            End If
                            If txtToAddress.Text.Contains(str1) Then
                                Continue For
                            Else
                                txtToAddress.Text &= str1 & " , "
                            End If
                        Next
                    End If

                Case enImg_Email_RefId.Leave_Module


                    'Pinkal (18-Sep-2012) -- Start
                    'Enhancement : TRA Changes
                    Dim frm As New frmLeaveFormList
                    frm.mblnExport_Print = True
                    frm.objFooter.Visible = False
                    frm.objEFooter.Visible = True
                    frm._IsFromMail = True
                    frm.ShowDialog()
                    dsList = frm._Email_Data
                    mstrLeaveFormIds = frm.LeaveFormIDs
                    txtToAddress.Text = ""
                    If frm._Email_Data IsNot Nothing Then
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If txtToAddress.Text.Contains(dtRow.Item("Firstname").ToString.Trim & " " & dtRow.Item("Othername").ToString.Trim & " " & dtRow.Item("Surname").ToString.Trim) Then
                                Continue For
                            Else
                                txtToAddress.Text &= dtRow.Item("Firstname").ToString.Trim & " " & dtRow.Item("Othername").ToString.Trim & " " & dtRow.Item("Surname").ToString.Trim & ","
                                mstrEmployeeId &= dtRow.Item("EmpId").ToString.Trim & ","
                            End If
                        Next

                        If mstrEmployeeId.Trim.Length > 0 Then
                            mstrEmployeeId = mstrEmployeeId.Trim.Substring(0, mstrEmployeeId.Trim.Length - 1)
                        End If

                    End If
                    'Pinkal (18-Sep-2012) -- End


                Case enImg_Email_RefId.Payroll_Module
                    Dim frm As New frmPayslipList
                    frm.objFooter.Visible = False
                    frm.objEFooter.Visible = True
                    frm.ShowDialog()
                    dsList = frm._Email_Data
                    mstrPeriodIds = frm._PeriodUnkids
                    txtToAddress.Text = ""
                    If dsList.Tables.Count > 0 Then
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
                                Continue For
                            Else
                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
                            End If
                        Next
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CallFrom", mstrModuleName)
        End Try

    End Sub

    Private Sub Set_Rich_Rtf(ByVal [rtfDoc] As RichTextBox, ByVal [rtfText] As String)
        If rtfDoc.InvokeRequired Then
            Dim MyDelegate As New Set_Rich_Delegate(AddressOf Set_Rich_Rtf)
            Me.Invoke(MyDelegate, New Object() {[rtfDoc], [rtfText]})
        Else
            [rtfDoc].Rtf = [rtfText]
        End If
    End Sub

    Private Sub CreateTreeView()
        Try
            Dim objLetterType As New clsLetterType
            Dim dsList As DataSet = objLetterType.getList(clsCommon_Master.enCommonMaster.LETTER_TYPE)
            Dim intLastId As Integer = 0
            Dim nGroupNode As New TreeNode
            Dim nCurrentNode As New TreeNode
            tvLetterType.Nodes.Clear()

            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If intLastId <> CInt(dsList.Tables(0).Rows(i).Item("lettermasterunkid")) Then
                        nGroupNode = New TreeNode
                        nGroupNode = tvLetterType.Nodes.Add(CStr(dsList.Tables(0).Rows(i).Item("lettermasterunkid")), CStr(dsList.Tables(0).Rows(i).Item("name")))
                        nGroupNode.Tag = "Group"
                        nGroupNode.ForeColor = Color.Blue
                        intLastId = CInt(dsList.Tables(0).Rows(i).Item("lettermasterunkid"))
                    End If
                    nCurrentNode = nGroupNode.Nodes.Add(CStr(dsList.Tables(0).Rows(i).Item("lettertypeunkid")), CStr(dsList.Tables(0).Rows(i).Item("lettertypename")))
                Next
            Else
                btnTo.Enabled = False
                btnPrint.Enabled = False
                BtnExport.Enabled = False
            End If
            tvLetterType.ExpandAll()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateTreeView", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    Private Function embedImage(ByVal img As Image) As String
        Dim rtf As New StringBuilder()
        Try
            rtf.Append("{\rtf1\ansi\ansicpg1252\deff0\deflang1033")
            rtf.Append(GetFontTable(Me.Font))
            rtf.Append(GetImagePrefix(img))
            rtf.Append(getRtfImage(img))
            rtf.Append("}")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "embedImage", mstrModuleName)
        Finally
        End Try
        Return rtf.ToString()
    End Function

    Private Function GetFontTable(ByVal font As Font) As String
        Dim fontTable = New StringBuilder()
        ' Append table control string
        fontTable.Append("{\fonttbl{\f0")
        fontTable.Append("\")
        Dim rtfFontFamily = New HybridDictionary()
        rtfFontFamily.Add(FontFamily.GenericMonospace.Name, "\fmodern")
        rtfFontFamily.Add(FontFamily.GenericSansSerif, "\fswiss")
        rtfFontFamily.Add(FontFamily.GenericSerif, "\froman")
        rtfFontFamily.Add("UNKNOWN", "\fnil")

        ' If the font's family corresponds to an RTF family, append the
        ' RTF family name, else, append the RTF for unknown font family.
        fontTable.Append(If(rtfFontFamily.Contains(font.FontFamily.Name), rtfFontFamily(font.FontFamily.Name), rtfFontFamily("UNKNOWN")))
        ' \fcharset specifies the character set of a font in the font table.
        ' 0 is for ANSI.
        fontTable.Append("\fcharset0 ")
        ' Append the name of the font
        fontTable.Append(font.Name)
        ' Close control string
        fontTable.Append(";}}")
        Return fontTable.ToString()
    End Function

    Private Function GetImagePrefix(ByVal _image As Image) As String
        Dim xDpi, yDpi As Single
        Dim rtf = New StringBuilder()
        Using graphics As Graphics = CreateGraphics()
            xDpi = graphics.DpiX
            yDpi = graphics.DpiY
        End Using
        ' Calculate the current width of the image in (0.01)mm
        Dim picw = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 2540)))
        ' Calculate the current height of the image in (0.01)mm
        Dim pich = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 2540)))
        ' Calculate the target width of the image in twips
        Dim picwgoal = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 1440)))
        ' Calculate the target height of the image in twips
        Dim pichgoal = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 1440)))
        ' Append values to RTF string
        rtf.Append("{\pict\wmetafile8")
        rtf.Append("\picw")
        rtf.Append(picw)
        rtf.Append("\pich")
        rtf.Append(pich)
        rtf.Append("\picwgoal")
        rtf.Append(picwgoal)
        rtf.Append("\pichgoal")
        rtf.Append(pichgoal)
        rtf.Append(" ")

        Return rtf.ToString()
    End Function

    Private Function getRtfImage(ByVal image As Image) As String
        ' Used to store the enhanced metafile
        Dim stream As MemoryStream = Nothing
        ' Used to create the metafile and draw the image
        Dim graphics As Graphics = Nothing
        ' The enhanced metafile
        Dim metaFile As Imaging.Metafile = Nothing
        Try
            Dim rtf = New StringBuilder()
            stream = New MemoryStream()
            ' Get a graphics context from the RichTextBox
            graphics = CreateGraphics()
            Using graphics
                ' Get the device context from the graphics context
                Dim hdc As IntPtr = graphics.GetHdc()
                ' Create a new Enhanced Metafile from the device context
                metaFile = New Imaging.Metafile(stream, hdc)
                ' Release the device context
                graphics.ReleaseHdc(hdc)
            End Using

            ' Get a graphics context from the Enhanced Metafile
            graphics = graphics.FromImage(metaFile)
            Using graphics
                ' Draw the image on the Enhanced Metafile
                graphics.DrawImage(image, New Rectangle(0, 0, image.Width, image.Height))
            End Using

            ' Get the handle of the Enhanced Metafile
            Dim hEmf As IntPtr = metaFile.GetHenhmetafile()
            ' A call to EmfToWmfBits with a null buffer return the size of the
            ' buffer need to store the WMF bits.  Use this to get the buffer
            ' size.
            Dim bufferSize As UInteger = GdipEmfToWmfBits(hEmf, 0, Nothing, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Create an array to hold the bits
            Dim buffer = New Byte(CInt(bufferSize - 1)) {}
            ' A call to EmfToWmfBits with a valid buffer copies the bits into the
            ' buffer an returns the number of bits in the WMF.  
            Dim _convertedSize As UInteger = GdipEmfToWmfBits(hEmf, bufferSize, buffer, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Append the bits to the RTF string
            For Each t As Byte In buffer
                rtf.Append(String.Format("{0:X2}", t))
            Next t
            Return rtf.ToString()
        Finally
            If graphics IsNot Nothing Then
                graphics.Dispose()
            End If
            If metaFile IsNot Nothing Then
                metaFile.Dispose()
            End If
            If stream IsNot Nothing Then
                stream.Close()
            End If
        End Try
    End Function
    'S.SANDEEP [05-Apr-2018] -- END
#End Region

#Region " Form's Events "

    Private Sub frmPrint_Export_Letters_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objEmail_Master = New clsEmail_Master

            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            If mblnIsPriting Then
                btnPrint.Visible = True
                BtnExport.Visible = False
            Else
                btnPrint.Visible = False
                BtnExport.Visible = True
            End If

            CreateTreeView()
            If rtbDocument.SelectedText.Length = 0 Then
                mnuCut.Enabled = False
                mnuCopy.Enabled = False
                mnuPaste.Enabled = False
            End If
            If txtToAddress.Text = "" Then
                btnViewMerge.Enabled = False
                btnFinish.Enabled = False
            End If
            objbtnNext.Visible = False
            objbtnPrevious.Visible = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPrint_Export_Letters_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            If rtbDocument.SelectedText.Length > 0 Then
                mnuCut.Enabled = True
                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
                rtbDocument.SelectedText = ""
            Else
                mnuCut.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "mnuCut_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            If rtbDocument.SelectedText.Length > 0 Then
                mnuCopy.Enabled = True
                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
            Else
                mnuCopy.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "mnuCopy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim strString As String = ""

            If My.Computer.Clipboard.ContainsText Then
                mnuPaste.Enabled = True
                strString = My.Computer.Clipboard.GetText()

                rtbDocument.AppendText(strString)
            Else
                mnuPaste.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "mnuPaste_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuApplicant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuApplicant.Click
        Try
            If mblnIsEmployee = True Then dsList = Nothing
            Call CallFrom(enImg_Email_RefId.Applicant_Module)
            mblnIsEmployee = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApplicant_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub tlmnuHtml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlmnuHtml.Click, tlmnuWord.Click, tlmnuRTF.Click, tlmnuMergedWord.Click
        Try


            'Pinkal (18-Sep-2012) -- Start
            'Enhancement : TRA Changes


            If mstrEmployeeId = "" Then Exit Sub

            strMailAddess = mstrEmployeeId.Split(CChar(","))

            If strMailAddess.Length <= 0 Then Exit Sub

            Dim objFile As New FolderBrowserDialog()

            If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then

                Me.Cursor = Cursors.WaitCursor

                Dim strMergedWordPath As String = objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & Format(DateAndTime.Now, "yyMMddhhmmss") & ".doc"

                For i As Integer = 0 To strMailAddess.Length - 1

                    If strMailAddess(i).Trim.Length > 0 Then

                        dtRow = dsList.Tables(0).Select("EmpId = " & "'" & strMailAddess(i) & "'")
                        Dim StrCol As String = ""
                        Dim strDataName As String = ""

                        For k As Integer = 0 To dtRow.Length - 1

                            For j As Integer = 0 To dtRow(k).Table.Columns.Count - 1
                                StrCol = dtRow(k).Table.Columns(j).ColumnName
                                If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
                                    rtbDocument.Focus()
                                    'S.SANDEEP [05-Apr-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                                    'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(k).Item(StrCol).ToString)
                                    If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                        Dim data As Byte() = CType(dtRow(k).Item(StrCol), Byte())
                                        Dim ms As IO.MemoryStream = New MemoryStream(data)
                                        Dim img As Image = Image.FromStream(ms)
                                        Dim strValue As String = embedImage(img)
                                        strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", strValue)
                                    ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                                        strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(k).Item(StrCol).ToString.Replace("\r\n", "\line"))
                                    Else
                                        strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(k).Item(StrCol).ToString)
                                    End If
                                    'S.SANDEEP [05-Apr-2018] -- END
                                    rtbDocument.Rtf = strDataName
                                End If
                            Next

                            'Sohail (20 Jan 2021) -- Start
                            'NMB Enhancement : - Allow calculation in letter template.
                            Dim arr As MatchCollection = Regex.Matches(rtbDocument.Rtf, "<calc[^>]*>(?<TEXT>[^<]*)<\/calc>", RegexOptions.IgnoreCase)
                            For Each s As RegularExpressions.Match In arr
                                Dim dt As New DataTable
                                Dim ans As Object = Nothing
                                Try
                                    ans = dt.Compute(s.Groups("TEXT").ToString.Replace(",", ""), "")
                                Catch ex As Exception

                                End Try
                                If ans Is Nothing Then
                                    ans = 0
                                End If
                                Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<calc>" & s.Groups("TEXT").ToString & "</calc>", Format(ans, "###,###,###,##0.00"), , , CompareMethod.Text)
                                rtbDocument.Rtf = sreplace
                            Next
                            'Sohail (20 Jan 2021) -- End

                            'S.SANDEEP |13-FEB-2021| -- START
                            'ISSUE/ENHANCEMENT : BONUS LETTER
                            arr = Nothing
                            arr = Regex.Matches(rtbDocument.Rtf, "<hcode[^>]*>(?<TEXT>[^<]*)<\/hcode>", RegexOptions.IgnoreCase)
                            If arr IsNot Nothing AndAlso arr.Count > 0 Then
                                Dim objPR As New clsPayrollProcessTran
                                Dim objMD As New clsMasterData
                                Dim objPD As New clscommom_period_Tran
                                Dim objCS As New clsComputeScore_master
                                Dim intPeriodId As Integer = -1
                                For Each s As RegularExpressions.Match In arr
                                    intPeriodId = objMD.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, False, False)
                                    '*********************************** PAYROLL AMOUNT START
                                    Dim amt As Decimal = 0
                                    If intPeriodId > 0 Then
                                        objPD._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                                        Dim dsAmt As New DataSet
                                        dsAmt = objPR.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPD._Start_Date, objPD._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(dtRow(0)("EmpId")), intPeriodId, 0, False, "prtranhead_master.trnheadcode = '" & s.Groups("TEXT").ToString() & "' ", "", False, "")
                                        If dsAmt IsNot Nothing AndAlso dsAmt.Tables.Count > 0 Then
                                            If dsAmt.Tables(0).Rows.Count > 0 Then
                                                amt = CDec(dsAmt.Tables(0).Rows(0)("amount"))
                                            End If
                                        End If
                                    End If
                                    Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<hcode>" & s.Groups("TEXT").ToString & "</hcode>", Format(amt, "###,###,###,##0.00"), , , CompareMethod.Text)
                                    rtbDocument.Rtf = sreplace
                                    '*********************************** PAYROLL AMOUNT END

                                    '*********************************** PERFORMANCE RATING START
                                    intPeriodId = -1
                                    Dim strRating As String = ""
                                    intPeriodId = objMD.getFirstPeriodID(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, 1, True, False)
                                    If intPeriodId > 0 Then
                                        strRating = objCS.GetRating(CInt(dtRow(0)("EmpId")), intPeriodId, ConfigParameter._Object._IsCalibrationSettingActive)
                                    End If
                                    sreplace = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<rating>", strRating, , , CompareMethod.Text)
                                    rtbDocument.Rtf = sreplace
                                    '*********************************** PERFORMANCE RATING END
                                Next
                                objPR = Nothing
                                objMD = Nothing
                                objPD = Nothing
                                objCS = Nothing
                            End If
                            'S.SANDEEP |13-FEB-2021| -- END

                            If strDataName.Length <= 0 Then strDataName = rtbDocument.Rtf

                            If dtRow IsNot Nothing AndAlso dtRow.Length > 0 Then

                                'S.SANDEEP [05-Apr-2018] -- START
                                'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                                'If mstrLeaveFormIds.Trim.Length <= 0 Then

                                '    If CType(sender, ToolStripMenuItem).Name = "tlmnuWord" Then

                                '        Dim strString As String = objEmail_Master.rtf2html3(strDataName, "+H+G+T+CR+I")
                                '        Dim strPath As String = objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & dtRow(k)("EmpCode").ToString() & ".doc"
                                '        System.IO.File.WriteAllText(strPath, strString, Encoding.Default)

                                '        'Sohail (19 Sep 2013) -- Start
                                '        'TRA - ENHANCEMENT
                                '    ElseIf CType(sender, ToolStripMenuItem).Name = tlmnuMergedWord.Name Then
                                '        Dim strString As String = objEmail_Master.rtf2html3(strDataName, "+H+G+T+CR+I")
                                '        If i > 0 Then
                                '            strString = strString.Replace("<!DOCTYPE HTML PUBLIC " & Chr(34) & "-//W3C//DTD HTML 3.2//EN" & Chr(34) & ">" & vbCrLf & "<HTML>" & vbCrLf & "<HEAD>" & vbCrLf & "<TITLE></TITLE>" & vbCrLf & "<META NAME=" & Chr(34) & "GENERATOR" & Chr(34) & " CONTENT=" & Chr(34) & "RTF2HTML By eZee Technosys" & Chr(34) & "></HEAD>" & vbCrLf & "<BODY>", "")
                                '        End If
                                '        If i < strMailAddess.Length - 1 Then
                                '            strString = strString.Replace("</BODY>" & vbCrLf & "</HTML>", "<p style='page-break-before: always'>&nbsp;</p>")
                                '        End If
                                '        System.IO.File.AppendAllText(strMergedWordPath, strString, Encoding.Default)
                                '        'Sohail (19 Sep 2013) -- End


                                '    ElseIf CType(sender, ToolStripMenuItem).Name = "tlmnuHtml" Then
                                '        System.IO.File.WriteAllText(objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & dtRow(k)("EmpCode").ToString() & ".html", objEmail_Master.rtf2html3(strDataName, "+H+G+T+CR+I"))

                                '    ElseIf CType(sender, ToolStripMenuItem).Name = "tlmnuRTF" Then
                                '        System.IO.File.WriteAllText(objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & dtRow(k)("EmpCode").ToString() & ".rtf", rtbDocument.Rtf)

                                '    End If

                                'Else

                                '    If CType(sender, ToolStripMenuItem).Name = "tlmnuWord" Then

                                '        Dim strString As String = objEmail_Master.rtf2html3(strDataName, "+H+G+T+CR+I")
                                '        Dim strPath As String = objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & dtRow(k)("EmpCode").ToString() & "_" & dtRow(k)("Formno").ToString() & ".doc"
                                '        System.IO.File.WriteAllText(strPath, strString, Encoding.Default)

                                '        'Sohail (19 Sep 2013) -- Start
                                '        'TRA - ENHANCEMENT
                                '    ElseIf CType(sender, ToolStripMenuItem).Name = tlmnuMergedWord.Name Then
                                '        Dim strString As String = objEmail_Master.rtf2html3(strDataName, "+H+G+T+CR+I")
                                '        If i > 0 Then
                                '            strString = strString.Replace("<!DOCTYPE HTML PUBLIC " & Chr(34) & "-//W3C//DTD HTML 3.2//EN" & Chr(34) & ">" & vbCrLf & "<HTML>" & vbCrLf & "<HEAD>" & vbCrLf & "<TITLE></TITLE>" & vbCrLf & "<META NAME=" & Chr(34) & "GENERATOR" & Chr(34) & " CONTENT=" & Chr(34) & "RTF2HTML By eZee Technosys" & Chr(34) & "></HEAD>" & vbCrLf & "<BODY>", "")
                                '        End If
                                '        If i < strMailAddess.Length - 1 Then
                                '            strString = strString.Replace("</BODY>" & vbCrLf & "</HTML>", "<p style='page-break-before: always'>&nbsp;</p>")
                                '        End If
                                '        System.IO.File.AppendAllText(strMergedWordPath, strString, Encoding.Default)
                                '        'Sohail (19 Sep 2013) -- End

                                '    ElseIf CType(sender, ToolStripMenuItem).Name = "tlmnuHtml" Then
                                '        System.IO.File.WriteAllText(objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & dtRow(k)("EmpCode").ToString() & "_" & dtRow(k)("Formno").ToString() & ".html", objEmail_Master.rtf2html3(strDataName, "+H+G+T+CR+I"))

                                '    ElseIf CType(sender, ToolStripMenuItem).Name = "tlmnuRTF" Then
                                '        System.IO.File.WriteAllText(objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & dtRow(k)("EmpCode").ToString() & "_" & dtRow(k)("Formno").ToString() & ".rtf", rtbDocument.Rtf)

                                '    End If

                                'End If
                                Dim htmlOutput As String = ""
                                If mstrLeaveFormIds.Trim.Length <= 0 Then
                                    htmlOutput = dtRow(k)("EmpCode").ToString
                                Else
                                    htmlOutput = dtRow(k)("EmpCode").ToString() & "_" & dtRow(k)("Formno").ToString()
                                End If

                                'S.SANDEEP [27-Apr-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002227|#ARUTI-135}
                                'REMOVE ALL SPECIAL CHARACTERS
                                htmlOutput = Regex.Replace(htmlOutput, "[^0-9a-zA-Z]+", "")
                                'S.SANDEEP [27-Apr-2018] -- END


                                Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                                Dim htmlResult = RtfToHtmlConverter.RtfToHtml(rtbDocument.Rtf, contentUriPrefix)

                                If CType(sender, ToolStripMenuItem).Name = "tlmnuWord" Then
                                    htmlResult.WriteToFile(tvLetterType.SelectedNode.Text & "_" & htmlOutput)
                                    Dim strPath As String = objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & htmlOutput & ".doc"
                                    System.IO.File.WriteAllText(strPath, rtbDocument.Rtf, Encoding.Default)

                                ElseIf CType(sender, ToolStripMenuItem).Name = tlmnuMergedWord.Name Then
                                    If i > 0 Then
                                        htmlResult._HTML = htmlResult._HTML.Replace("<!DOCTYPE HTML PUBLIC " & Chr(34) & "-//W3C//DTD HTML 3.2//EN" & Chr(34) & ">" & vbCrLf & "<HTML>" & vbCrLf & "<HEAD>" & vbCrLf & "<TITLE></TITLE>" & vbCrLf & "<META NAME=" & Chr(34) & "GENERATOR" & Chr(34) & " CONTENT=" & Chr(34) & "></HEAD>" & vbCrLf & "<BODY>", "")
                                    End If
                                    If i < strMailAddess.Length - 1 Then
                                        htmlResult._HTML = htmlResult._HTML.Replace("</BODY>" & vbCrLf & "</HTML>", "<p style='page-break-before: always'>&nbsp;</p>")
                                    End If
                                    System.IO.File.AppendAllText(strMergedWordPath, rtbDocument.Rtf, Encoding.Default)

                                ElseIf CType(sender, ToolStripMenuItem).Name = "tlmnuHtml" Then
                                    htmlResult.WriteToFile(objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & htmlOutput & ".html")

                                ElseIf CType(sender, ToolStripMenuItem).Name = "tlmnuRTF" Then
                                    System.IO.File.WriteAllText(objFile.SelectedPath & "/" & tvLetterType.SelectedNode.Text & "_" & dtRow(k)("EmpCode").ToString() & ".rtf", rtbDocument.Rtf)
                                End If
                                'S.SANDEEP [05-Apr-2018] -- END

                                rtbDocument.Rtf = strData
                            End If
                        Next

                    End If
                Next
                'Sohail (19 Sep 2013) -- Start
                'TRA - ENHANCEMENT
                If CType(sender, ToolStripMenuItem).Name = tlmnuMergedWord.Name Then
                    System.IO.File.AppendAllText(strMergedWordPath, vbCrLf & "</BODY>" & vbCrLf & "</HTML>", Encoding.Default)
                End If
                'Sohail (19 Sep 2013) -- End

                Me.Cursor = Cursors.Default
                eZeeMsgBox.Show("Letter(s) Exported successfully to the specified location.", enMsgBoxStyle.Information)
                Shell("explorer " & objFile.SelectedPath, AppWinStyle.MaximizedFocus)

            End If

            'Pinkal (18-Sep-2012) -- End

        Catch ex As Exception
            rtbDocument.Rtf = strData
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "tlmnuHtml_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub rtbDocument_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            mnuCut.Enabled = True
            mnuCopy.Enabled = True
            mnuPaste.Enabled = True
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "rtbDocument_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtToAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtToAddress.TextChanged
        Try
            If txtToAddress.Text.Length > 0 Then
                btnViewMerge.Enabled = True
                btnFinish.Enabled = True
                btnPrint.Enabled = True
            Else
                btnViewMerge.Enabled = False
                btnFinish.Enabled = False
                btnPrint.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtToAddress_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnViewMerge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewMerge.Click
        Try
            rtbDocument.ReadOnly = True
            rtbDocument.BackColor = GUI.ColorComp
            btnViewMerge.Hide()
            btnFinish.Show()
            objbtnNext.Visible = True
            objbtnPrevious.Visible = True

            If rtbDocument.Rtf <> strData Then
                strData = rtbDocument.Rtf
            End If

            btnPrint.Enabled = False
            BtnExport.Enabled = False

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnViewMerge_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            rtbDocument.ReadOnly = False
            rtbDocument.BackColor = GUI.ColorOptional
            btnViewMerge.Show()
            btnFinish.Hide()
            objbtnNext.Visible = False
            objbtnPrevious.Visible = False
            rtbDocument.Rtf = strData
            btnPrint.Enabled = True
            BtnExport.Enabled = True
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnViewMerge_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployee.Click
        Try
            If mblnIsEmployee = False Then dsList = Nothing
            Call CallFrom(enImg_Email_RefId.Employee_Module)
            mblnIsEmployee = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnNext.Click
        Try
            '            Call SetAddress()


            If mstrEmployeeId = "" Then Exit Sub

            strMailAddess = mstrEmployeeId.Split(CChar(","))

            If dsList.Tables(0).Rows.Count = 1 Then
                If rtbDocument.Rtf <> strData Then
                    rtbDocument.Rtf = strData
                End If
                dtRow = dsList.Tables(0).Select("EmpId = " & "'" & strMailAddess(intCount) & "'")

                'Pinkal (18-Sep-2012) -- Start
                'Enhancement : TRA Changes

                For i As Integer = 0 To dtRow.Length - 1
                    Call ViewMergeData(i)
                Next

                'Pinkal (18-Sep-2012) -- End

            Else
                If dsList Is Nothing Then
                    Exit Sub
                ElseIf intCount = dsList.Tables(0).Rows.Count Then
                    Exit Sub
                ElseIf dsList.Tables(0).Rows.Count - intCount = 1 Then
                    Exit Sub

                Else
                    If rtbDocument.Rtf <> strData Then
                        intCount += 1
                    End If
                    rtbDocument.Rtf = strData

                    'Pinkal (18-Sep-2012) -- Start
                    'Enhancement : TRA Changes

                    If mstrLeaveFormIds.Trim.Length > 0 AndAlso strMailAddess.Length = intCount Then
                        Exit Sub
                    End If

                    dtRow = dsList.Tables(0).Select("EmpId = " & "'" & strMailAddess(intCount) & "'")

                    For i As Integer = 0 To dtRow.Length - 1
                        Call ViewMergeData(i)
                    Next

                    'Pinkal (18-Sep-2012) -- End
                End If

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnNext_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnPrevious.Click
        Try

            If strMailAddess IsNot Nothing Then
                If CInt(strMailAddess.Length) <= 0 Then
                    If mstrEmployeeId = "" Then Exit Sub
                    strMailAddess = mstrEmployeeId.Split(CChar(","))
                End If
            End If
            If intCount <= 0 Then
                Exit Sub
            ElseIf intCount <> dsList.Tables(0).Rows.Count Then
                intCount -= 1
                rtbDocument.Rtf = strData
                dtRow = dsList.Tables(0).Select("EmpId = " & "'" & strMailAddess(intCount) & "'")

                'Pinkal (18-Sep-2012) -- Start
                'Enhancement : TRA Changes

                For i As Integer = 0 To dtRow.Length - 1
                    Call ViewMergeData(i)
                Next

                'Pinkal (18-Sep-2012) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try


            'Pinkal (18-Sep-2012) -- Start
            'Enhancement : TRA Changes

            If mstrEmployeeId = "" Then Exit Sub

            strMailAddess = mstrEmployeeId.Split(CChar(","))

            If strMailAddess.Length <= 0 Then Exit Sub

            For i As Integer = 0 To strMailAddess.Length - 1
                If strMailAddess(i).Trim.Length > 0 Then
                    dtRow = dsList.Tables(0).Select("EmpId = " & "'" & strMailAddess(i) & "'")
                    Dim StrCol As String = ""
                    Dim strDataName As String = ""

                    For k As Integer = 0 To dtRow.Length - 1

                        For j As Integer = 0 To dtRow(k).Table.Columns.Count - 1
                            StrCol = dtRow(k).Table.Columns(j).ColumnName
                            If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
                                rtbDocument.Focus()
                                'S.SANDEEP [05-Apr-2018] -- START
                                'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                                'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(k).Item(StrCol).ToString)
                                If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                    Dim data As Byte() = CType(dtRow(k).Item(StrCol), Byte())
                                    Dim ms As IO.MemoryStream = New MemoryStream(data)
                                    Dim img As Image = Image.FromStream(ms)
                                    Dim strValue As String = embedImage(img)
                                    strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", strValue)
                                ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                                    strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(k).Item(StrCol).ToString.Replace("\r\n", "\line"))
                                Else
                                    strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(k).Item(StrCol).ToString)
                                End If
                                'S.SANDEEP [05-Apr-2018] -- END                                
                                rtbDocument.Rtf = strDataName
                            End If
                        Next

                        'Sohail (20 Jan 2021) -- Start
                        'NMB Enhancement : - Allow calculation in letter template.
                        Dim arr As MatchCollection = Regex.Matches(rtbDocument.Rtf, "<calc[^>]*>(?<TEXT>[^<]*)<\/calc>", RegexOptions.IgnoreCase)
                        For Each s As RegularExpressions.Match In arr
                            Dim dt As New DataTable
                            Dim ans As Object = Nothing
                            Try
                                ans = dt.Compute(s.Groups("TEXT").ToString.Replace(",", ""), "")
                            Catch ex As Exception

                            End Try
                            If ans Is Nothing Then
                                ans = 0
                            End If
                            Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<calc>" & s.Groups("TEXT").ToString & "</calc>", Format(ans, "###,###,###,##0.00"), , , CompareMethod.Text)
                            rtbDocument.Rtf = sreplace
                        Next
                        'Sohail (20 Jan 2021) -- End

                        'S.SANDEEP |13-FEB-2021| -- START
                        'ISSUE/ENHANCEMENT : BONUS LETTER
                        arr = Nothing
                        arr = Regex.Matches(rtbDocument.Rtf, "<hcode[^>]*>(?<TEXT>[^<]*)<\/hcode>", RegexOptions.IgnoreCase)
                        If arr IsNot Nothing AndAlso arr.Count > 0 Then
                            Dim objPR As New clsPayrollProcessTran
                            Dim objMD As New clsMasterData
                            Dim objPD As New clscommom_period_Tran
                            Dim objCS As New clsComputeScore_master
                            Dim intPeriodId As Integer = -1
                            For Each s As RegularExpressions.Match In arr
                                intPeriodId = objMD.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, False, False)
                                '*********************************** PAYROLL AMOUNT START
                                Dim amt As Decimal = 0
                                If intPeriodId > 0 Then
                                    objPD._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                                    Dim dsAmt As New DataSet
                                    dsAmt = objPR.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPD._Start_Date, objPD._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(dtRow(0)("EmpId")), intPeriodId, 0, False, "prtranhead_master.trnheadcode = '" & s.Groups("TEXT").ToString() & "' ", "", False, "")
                                    If dsAmt IsNot Nothing AndAlso dsAmt.Tables.Count > 0 Then
                                        If dsAmt.Tables(0).Rows.Count > 0 Then
                                            amt = CDec(dsAmt.Tables(0).Rows(0)("amount"))
                                        End If
                                    End If
                                End If
                                Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<hcode>" & s.Groups("TEXT").ToString & "</hcode>", Format(amt, "###,###,###,##0.00"), , , CompareMethod.Text)
                                rtbDocument.Rtf = sreplace
                                '*********************************** PAYROLL AMOUNT END

                                '*********************************** PERFORMANCE RATING START
                                intPeriodId = -1
                                Dim strRating As String = ""
                                intPeriodId = objMD.getFirstPeriodID(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, 1, True, False)
                                If intPeriodId > 0 Then
                                    strRating = objCS.GetRating(CInt(dtRow(0)("EmpId")), intPeriodId, ConfigParameter._Object._IsCalibrationSettingActive)
                                End If
                                sreplace = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<rating>", strRating, , , CompareMethod.Text)
                                rtbDocument.Rtf = sreplace
                                '*********************************** PERFORMANCE RATING END
                            Next
                            objPR = Nothing
                            objMD = Nothing
                            objPD = Nothing
                            objCS = Nothing
                        End If
                        'S.SANDEEP |13-FEB-2021| -- END

                        PrintDocument1.Print()
                        rtbDocument.Rtf = strData

                    Next
                End If
            Next

            'Pinkal (18-Sep-2012) -- End

        Catch ex As Exception
            rtbDocument.Rtf = strData
            DisplayError.Show("-1", ex.Message, "btnPrint_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (18-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub mnuLeaveForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLeaveForm.Click
        Try
            Call CallFrom(enImg_Email_RefId.Leave_Module)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTo_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Sep-2012) -- End
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

#End Region

#Region " Treeview Events "

    Private Sub tvLetterType_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvLetterType.AfterSelect
        Try
            Dim objLetterType As New clsLetterType
            objLetterType._LettertypeUnkId = CInt(e.Node.Name)
            rtbDocument.Rtf = objLetterType._Lettercontent
            strData = objLetterType._Lettercontent
            intCount = 0
            mstrEmployeeId = ""
            mstrLeaveFormIds = ""
            txtToAddress.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tvLetterType_AfterSelect", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Print Document Event"

    Private Sub PrintDocument1_BeginPrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        Try
            checkPrint = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "PrintDocument1_BeginPrint", mstrModuleName)
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Try
            checkPrint = rtbDocument.Print(checkPrint, rtbDocument.TextLength, e)
            ' Look for more pages
            If checkPrint < rtbDocument.TextLength Then
                e.HasMorePages = True
            Else
                e.HasMorePages = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "PrintDocument1_PrintPage", mstrModuleName)
        End Try
    End Sub

#End Region





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnFinish.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinish.GradientForeColor = GUI._ButttonFontColor

            Me.btnViewMerge.GradientBackColor = GUI._ButttonBackColor
            Me.btnViewMerge.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnPrint.GradientBackColor = GUI._ButttonBackColor
            Me.btnPrint.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnPrevious.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnPrevious.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnNext.GradientForeColor = GUI._ButttonFontColor

            Me.btnTo.GradientBackColor = GUI._ButttonBackColor
            Me.btnTo.GradientForeColor = GUI._ButttonFontColor

            Me.BtnExport.GradientBackColor = GUI._ButttonBackColor
            Me.BtnExport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.mnuCut.Text = Language._Object.getCaption(Me.mnuCut.Name, Me.mnuCut.Text)
            Me.mnuCopy.Text = Language._Object.getCaption(Me.mnuCopy.Name, Me.mnuCopy.Text)
            Me.mnuPaste.Text = Language._Object.getCaption(Me.mnuPaste.Name, Me.mnuPaste.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnFinish.Text = Language._Object.getCaption(Me.btnFinish.Name, Me.btnFinish.Text)
            Me.btnViewMerge.Text = Language._Object.getCaption(Me.btnViewMerge.Name, Me.btnViewMerge.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnPrint.Text = Language._Object.getCaption(Me.btnPrint.Name, Me.btnPrint.Text)
            Me.mnuApplicant.Text = Language._Object.getCaption(Me.mnuApplicant.Name, Me.mnuApplicant.Text)
            Me.mnuEmployee.Text = Language._Object.getCaption(Me.mnuEmployee.Name, Me.mnuEmployee.Text)
            Me.btnTo.Text = Language._Object.getCaption(Me.btnTo.Name, Me.btnTo.Text)
            Me.rtbDocument.Text = Language._Object.getCaption(Me.rtbDocument.Name, Me.rtbDocument.Text)
            Me.BtnExport.Text = Language._Object.getCaption(Me.BtnExport.Name, Me.BtnExport.Text)
            Me.tlmnuWord.Text = Language._Object.getCaption(Me.tlmnuWord.Name, Me.tlmnuWord.Text)
            Me.tlmnuHtml.Text = Language._Object.getCaption(Me.tlmnuHtml.Name, Me.tlmnuHtml.Text)
            Me.tlmnuRTF.Text = Language._Object.getCaption(Me.tlmnuRTF.Name, Me.tlmnuRTF.Text)
            Me.mnuLeaveForm.Text = Language._Object.getCaption(Me.mnuLeaveForm.Name, Me.mnuLeaveForm.Text)
            Me.tlmnuMergedWord.Text = Language._Object.getCaption(Me.tlmnuMergedWord.Name, Me.tlmnuMergedWord.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class