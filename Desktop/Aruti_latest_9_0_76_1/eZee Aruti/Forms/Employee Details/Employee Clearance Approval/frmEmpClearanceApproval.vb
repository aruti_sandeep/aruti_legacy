﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmEmpClearanceApproval

#Region " Private Varaibles "

    Private objEmpClearanceApprovalTran As New clsempclearanceapproval_Tran
    Private ReadOnly mstrModuleName As String = "frmEmpClearanceApproval"
    Private mdvData As DataView
    Private dtNotification As DataTable = Nothing

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsEmp As New DataSet
        Try
            dsEmp = objEmpClearanceApprovalTran.GetEmployeeClearanceList(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, User._Object._Userunkid)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsEmp.Tables("List")
                .SelectedValue = 0
                If CInt(.SelectedValue) <= 0 Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim dtTable As New DataTable

            dtTable = objEmpClearanceApprovalTran.GetEmployeeApprovalData(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting _
                                                                                                            , enUserPriviledge.AllowToApproveEmployeeClearance, User._Object._Userunkid, dtNotification, Nothing, "")

            dgvData.AutoGenerateColumns = False

            objchkCheck.DataPropertyName = "icheck"
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhDepartment.DataPropertyName = "Department"
            dgcolhJob.DataPropertyName = "Job"
            dgcolhClassGrp.DataPropertyName = "ClassGroup"
            dgcolhClass.DataPropertyName = "Class"
            dgcolhStatus.DataPropertyName = "Status"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolhApprUsrId.DataPropertyName = "userunkid"
            objdgcolhiRead.DataPropertyName = "iRead"

            mdvData = dtTable.DefaultView()

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mdvData.RowFilter = "employeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            dgvData.DataSource = mdvData

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Sub SetGridFormat()
        Try
            'RemoveHandler dgvData.DataBindingComplete, AddressOf dgvData_DataBindingComplete
            'For Each dgrow As DataGridViewRow In dgvData.Rows
            '    If CInt(User._Object._Userunkid) <> CInt(dgrow.Cells(objdgcolhApprUsrId.Index).Value) Then
            '        If CInt(dgrow.Cells(objdgcolhiRead.Index).Value) <= 0 Then dgrow.Cells(objdgcolhiRead.Index).Value = 2
            '    End If

            '    Select Case CInt(dgrow.Cells(objdgcolhiRead.Index).Value)
            '        Case 1
            '            dgrow.Cells(objchkCheck.Index).ReadOnly = True
            '            dgrow.DefaultCellStyle.ForeColor = Color.Blue
            '        Case 2
            '            dgrow.Cells(objchkCheck.Index).ReadOnly = True
            '            dgrow.DefaultCellStyle.ForeColor = Color.Gray
            '    End Select
            'Next
            'AddHandler dgvData.DataBindingComplete, AddressOf dgvData_DataBindingComplete
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridFormat", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try
            objEmpClearanceApprovalTran._Tranguid = Guid.NewGuid().ToString()
            objEmpClearanceApprovalTran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            objEmpClearanceApprovalTran._Mappingunkid = User._Object._Userunkid
            objEmpClearanceApprovalTran._Isvoid = False
            objEmpClearanceApprovalTran._Isnotified = False
            objEmpClearanceApprovalTran._Audittype = 1
            objEmpClearanceApprovalTran._Audituserunkid = User._Object._Userunkid
            objEmpClearanceApprovalTran._Form_Name = mstrModuleName
            objEmpClearanceApprovalTran._Hostname = getHostName()
            objEmpClearanceApprovalTran._Ip = getIP()
            objEmpClearanceApprovalTran._Isweb = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmpClearanceApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpClearanceApprovalTran = New clsempclearanceapproval_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpClearanceApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpClearanceApproval_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmpClearanceApprovalTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsempclearanceapproval_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsempclearanceapproval_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            If mdvData IsNot Nothing Then

                Dim dtTable As DataTable = mdvData.ToTable()

                If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objchkCheck.DataPropertyName) = True).Count() <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to approve employee(s) without remark?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    Case btnReject.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue."), enMsgBoxStyle.Information)
                            txtRemark.Focus()
                            Exit Sub
                        End If
                End Select


                Dim strCheckedEmpIds As String = ""
                Dim drtemp() As DataRow = Nothing
                Dim mblnIsFinalFound As Boolean = False
                Dim iCount As Integer = 0

                drtemp = dtTable.Select("icheck = true ")

                If drtemp.Length > 0 Then
                    For index As Integer = 0 To drtemp.Length - 1
                        drtemp(index)("isfinal") = True
                    Next
                    iCount = drtemp.Length
                    mblnIsFinalFound = True
                End If
                dtTable.AcceptChanges()

                Call SetValue()
                Dim mblnIsRejection As Boolean = False
                objEmpClearanceApprovalTran._Remark = txtRemark.Text
                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        objEmpClearanceApprovalTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
                    Case btnReject.Name
                        objEmpClearanceApprovalTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
                        mblnIsRejection = True
                End Select

                dtTable = New DataView(dtTable, "icheck = true", "", DataViewRowState.CurrentRows).ToTable

                If objEmpClearanceApprovalTran.Insert(dtTable, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo) _
                                                                        , Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting _
                                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                        , User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon _
                                                                        , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP, Nothing) = False Then
                    eZeeMsgBox.Show(objEmpClearanceApprovalTran._Message)
                Else
                    If objEmpClearanceApprovalTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected Then

                        If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                            For Each dr As DataRow In dtTable.Rows
                                objEmpClearanceApprovalTran.SendClearanceApproveRejectNotification(True, txtRemark.Text.Trim(), FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, CInt(dr("employeeunkid")).ToString(), ConfigParameter._Object._UserAccessModeSetting _
                                                                                                                                , enUserPriviledge.AllowToApproveEmployeeClearance, Me.Name, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username, dr("employeecode").ToString(), dr("EmpName").ToString())
                            Next
                        End If
                    End If
                    FillGrid()
                    txtRemark.Text = ""
                    objchkAll.Checked = False
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            mdvData.Table.Rows.Clear()
            objbtnReset.ShowResult(dgvData.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillGrid()
            objbtnSearch.ShowResult(dgvData.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim str As String = ""
        Try
            If (txtSearch.Text.Length > 0) Then
                str = dgcolhEmployee.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhEmployee.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhDepartment.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhJob.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhClassGrp.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhClass.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhStatus.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' "
            End If
            mdvData.RowFilter = str
            objbtnSearch.ShowResult(dgvData.RowCount.ToString())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In mdvData
                'If CInt(dr.Item(objdgcolhiRead.DataPropertyName)) <= 0 Then
                dr.Item(objchkCheck.DataPropertyName) = CBool(objchkAll.CheckState)
                'End If
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkMyApprovals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Call FillGrid()
            objbtnSearch.ShowResult(dgvData.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMyApprovals_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Datagrid Event(s) "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If e.ColumnIndex = objchkCheck.Index Then
                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = mdvData.ToTable.Select("icheck = true", "")
                If drRow.Length > 0 Then
                    If mdvData.ToTable.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            SetGridFormat()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnReject.GradientBackColor = GUI._ButttonBackColor
            Me.btnReject.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhClassGrp.HeaderText = Language._Object.getCaption(Me.dgcolhClassGrp.Name, Me.dgcolhClassGrp.HeaderText)
            Me.dgcolhClass.HeaderText = Language._Object.getCaption(Me.dgcolhClass.Name, Me.dgcolhClass.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue.")
            Language.setMessage(mstrModuleName, 1, "Are you sure you want to approve employee(s) without remark?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class