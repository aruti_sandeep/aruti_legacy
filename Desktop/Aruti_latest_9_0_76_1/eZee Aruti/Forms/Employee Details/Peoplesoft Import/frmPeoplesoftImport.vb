﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmPeoplesoftImport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPeoplesoftImport"
    Private mblnCancel As Boolean = True
    Private mdtTable As DataTable
    Private m_Dataview As DataView
    Private m_Dataview_Dep As DataView
    Private mdtDependant As DataTable

#End Region

#Region " Properties "

    Public WriteOnly Property _DataTable() As DataTable
        Set(ByVal value As DataTable)
            mdtTable = value
            m_Dataview = New DataView(mdtTable)
        End Set
    End Property

    Public WriteOnly Property _DependantTable() As DataTable
        Set(ByVal value As DataTable)
            mdtDependant = value
            m_Dataview_Dep = New DataView(mdtDependant)
        End Set
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhGlobalID.DataPropertyName = "identity_no"
            dgcolhEmpCode.DataPropertyName = "employeecode"
            dgcolhRemark.DataPropertyName = "remark"
            dgcolhEmpID.DataPropertyName = "employeeunkid"
            dgcolhTitle.DataPropertyName = "title"
            dgcolhFirstName.DataPropertyName = "firstname"
            dgcolhOtherName.DataPropertyName = "othername"
            dgcolhSurname.DataPropertyName = "surname"
            dgcolhAppointedDate.DataPropertyName = "appointeddate"
            dgcolhGender.DataPropertyName = "gendername"
            dgcolhEmplType.DataPropertyName = "employmenttype"
            dgcolhBirthDate.DataPropertyName = "birthdate"
            dgcolhBirthCountry.DataPropertyName = "birthcountry"
            dgcolhVillage.DataPropertyName = "birth_village"
            dgcolhMaritalStatus.DataPropertyName = "maritalstatus"
            dgcolhAnniversaryDate.DataPropertyName = "anniversary_date"
            dgcolhDept.DataPropertyName = "department"
            dgcolhJob.DataPropertyName = "job"
            dgcolhBranch.DataPropertyName = "station"
            dgcolhJobGroup.DataPropertyName = "jobgroup"
            dgcolhCCenter.DataPropertyName = "costcenter"
            dgcolhClassGroup.DataPropertyName = "classgroup" 'Sohail (16 Jun 2015)
            dgcolhIncrementDate.DataPropertyName = "incrementdate"
            dgcolhScale.DataPropertyName = "scale"
            dgcolhTranHead.DataPropertyName = "tranhed"
            dgcolhHeadType.DataPropertyName = "trnheadtype_id"
            dgcolhLeavingDate.DataPropertyName = "termination_from_date"
            dgcolhEOCDate.DataPropertyName = "empl_enddate"
            dgcolhRetirementDate.DataPropertyName = "termination_to_date"
            dgcolhReinstatementDate.DataPropertyName = "reinstatement_date"
            dgcolhLastUpdateime.DataPropertyName = "last_update_time"
            'dgcolhCurrScale.DataPropertyName = "currentscale"
            'dgcolhIncrement.DataPropertyName = "Increment"
            'dgcolhNewScale.DataPropertyName = "newscale"


            'dgcolhCurrScale.DefaultCellStyle.Format = GUI.fmtCurrency
            'dgcolhIncrement.DefaultCellStyle.Format = GUI.fmtCurrency
            'dgcolhNewScale.DefaultCellStyle.Format = GUI.fmtCurrency

            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

        Catch ex As Exception
            eZeeMsgBox.Show("FillGirdView: " & ex.Message, enMsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub FillDependantGrid()
        Try
            dgvDependant.AutoGenerateColumns = False

            dpcolhGlobalID.DataPropertyName = "identity_no"
            dpcolhLocalID.DataPropertyName = "employeecode"
            dpcolhEmpID.DataPropertyName = "employeeunkid"
            dpcolhFirstName.DataPropertyName = "first_name"
            dpcolhOtherName.DataPropertyName = "middle_name"
            dpcolhSurname.DataPropertyName = "last_name"
            dpcolhGender.DataPropertyName = "gendername"
            dpcolhRelation.DataPropertyName = "relation"
            dpcolhBirthDate.DataPropertyName = "birthdate"
            dpcolhAddress.DataPropertyName = "address"
            dpcolhMobileNo.DataPropertyName = "mobile_no"
            dpcolhCountry.DataPropertyName = "country"
            dpcolhState.DataPropertyName = "state"
            dpcolhCity.DataPropertyName = "city"
            dpcolhEmail.DataPropertyName = "email"
            dpcolhRemark.DataPropertyName = "remark"

            dgvDependant.DataSource = m_Dataview_Dep
            dgvDependant.Refresh()

        Catch ex As Exception
            eZeeMsgBox.Show("FillGirdView: " & ex.Message, enMsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkSelectAll.Checked
            Next
            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

        Catch ex As Exception
            eZeeMsgBox.Show("CheckAll: " & ex.Message, enMsgBoxStyle.Information)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmPeoplesoftImport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeoplesoftImport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeoplesoftImport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.I Then
                'Call btnImport.PerformClick()
            ElseIf e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{Tab}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeoplesoftImport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeoplesoftImport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Set_Logo(Me, gApplicationType)

            m_Dataview.RowFilter = "rowtypeid <> 0 "
            If m_Dataview.Count > 0 Then
                btnImport.Enabled = False
                eZeeMsgBox.Show("Some Transactions will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button.", enMsgBoxStyle.Information)
            Else
                m_Dataview.RowFilter = "rowtypeid = 0 "
                btnImport.Enabled = True
            End If


            Call FillGirdView()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeoplesoftImport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Master"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim objEmp As New clsEmployee_Master
        Dim objId_Tran As New clsIdentity_tran
        Dim objSalary As New clsSalaryIncrement
        Dim objDependants_Benefice As New clsDependants_Beneficiary_tran

        Dim mdtFilteredTable As DataTable
        Dim mdtIdTran As DataTable = Nothing

        Try
            If mdtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show("Sorry, you can not import this file. Reason :There is no transaction in this file to import.", enMsgBoxStyle.Information)
                Exit Sub
            End If

            m_Dataview.RowFilter = "rowtypeid = 0 "
            mdtFilteredTable = m_Dataview.ToTable

            If mdtFilteredTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show("Sorry, you can not import this file. Reason :Either Employee Codes are not there in the system or transactions are already exist.", enMsgBoxStyle.Information)
                Exit Sub
            End If

            'If mdtTable.Rows.Count <> mdtFilteredTable.Rows.Count Then
            'If mdtTable.Select("rowtypeid <> 17 ").Length > 0 Then
            '    eZeeMsgBox.Show("Sorry, All data should be appropriate in order to import. Please check unsuccessfull data list from operation button.", enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If mdtFilteredTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show("Please Tick atleast one transaction from list to Import.", enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim intEmpID As Integer = 0


            'Varsha (10 Nov 2017) -- Start
            'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
            Dim objMasterData As New clsMasterData
            Dim intThemeId As Integer = objMasterData.GetDefaultThemeId(Company._Object._Companyunkid, Nothing)
            'Varsha (10 Nov 2017) -- End


            For Each dtRow As DataRow In mdtFilteredTable.Rows

                intEmpID = objEmp.GetEmployeeUnkidFromEmpCode(dtRow.Item("employeecode").ToString)

                objEmp = New clsEmployee_Master
                objId_Tran = New clsIdentity_tran

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = intEmpID
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID
                'S.SANDEEP [04 JUN 2015] -- END


                objId_Tran._EmployeeUnkid = intEmpID
                mdtIdTran = objId_Tran._DataList

                If dtRow.Item("identity_aud").ToString = "A" Then
                    Dim dr As DataRow = mdtIdTran.NewRow

                    dr.Item("identitytranunkid") = -1
                    dr.Item("employeeunkid") = intEmpID
                    dr.Item("idtypeunkid") = CInt(dtRow.Item("idtypeunkid"))
                    dr.Item("identity_no") = dtRow.Item("identity_no")
                    dr.Item("countryunkid") = 0
                    dr.Item("isdefault") = CBool(dtRow.Item("isdefault"))
                    dr.Item("AUD") = dtRow.Item("identity_aud").ToString

                    mdtIdTran.Rows.Add(dr)
                End If

                objEmp._Employeecode = dtRow.Item("employeecode").ToString
                objEmp._Titalunkid = CInt(dtRow.Item("titleunkid"))
                objEmp._Firstname = dtRow.Item("firstname").ToString
                objEmp._Surname = dtRow.Item("surname").ToString
                objEmp._Othername = dtRow.Item("othername").ToString
                objEmp._Appointeddate = CDate(dtRow.Item("appointeddate"))
                objEmp._Confirmation_Date = CDate(dtRow.Item("appointeddate"))
                objEmp._Gender = CInt(dtRow.Item("gender"))
                objEmp._Employmenttypeunkid = CInt(dtRow.Item("employmenttypeunkid"))
                objEmp._Displayname = dtRow.Item("employeecode").ToString
                objEmp._Shiftunkid = CInt(dtRow.Item("shiftunkid"))
                If IsDBNull(dtRow.Item("birthdate")) = False Then
                    objEmp._Birthdate = CDate(dtRow.Item("birthdate"))
                Else
                    objEmp._Birthdate = Nothing
                End If
                objEmp._Birthcountryunkid = CInt(dtRow.Item("birthcountryunkid"))
                objEmp._Birth_Village = dtRow.Item("birth_village").ToString
                objEmp._Maritalstatusunkid = CInt(dtRow.Item("maritalstatusunkid"))
                If IsDBNull(dtRow.Item("anniversary_date")) = False Then
                    objEmp._Anniversary_Date = CDate(dtRow.Item("anniversary_date"))
                Else
                    objEmp._Anniversary_Date = Nothing
                End If
                objEmp._Departmentunkid = CInt(dtRow.Item("departmentunkid"))
                objEmp._Jobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                objEmp._Jobunkid = CInt(dtRow.Item("jobunkid"))
                'Sohail (16 Jun 2015) -- Start
                'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
                objEmp._Classgroupunkid = CInt(dtRow.Item("classgroupunkid"))
                'Sohail (16 Jun 2015) -- End
                objEmp._Gradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                objEmp._Gradeunkid = CInt(dtRow.Item("gradeunkid"))
                objEmp._Gradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                objEmp._Scale = CDec(dtRow.Item("scale"))
                objEmp._Costcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                objEmp._Tranhedunkid = CInt(dtRow.Item("tranhedunkid"))
                objEmp._TrnHeadTypeId = CInt(dtRow.Item("trnheadtype_id"))
                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
                    objEmp._Termination_From_Date = CDate(dtRow.Item("termination_from_date"))
                End If
                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
                    objEmp._Termination_To_Date = CDate(dtRow.Item("termination_to_date"))
                Else
                    objEmp._Termination_To_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
                    objEmp._Empl_Enddate = CDate(dtRow.Item("empl_enddate"))
                End If
                If IsDBNull(dtRow.Item("reinstatement_date")) = False Then
                    objEmp._Reinstatementdate = CDate(dtRow.Item("reinstatement_date"))
                    objEmp._Termination_From_Date = Nothing
                    objEmp._Empl_Enddate = Nothing
                End If
                If objEmp._Isapproved = False Then
                    If User._Object.Privilege._AllowToApproveEmployee Then
                objEmp._Isapproved = True
                    Else
                        objEmp._Isapproved = False
                    End If
                End If
                objEmp._PSoft_SyncDateTime = CDate(dtRow.Item("last_update_time"))

                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                objEmp._AssignDefaultTransactionHeads = CBool(dtRow.Item("AssignDefaulTranHeads"))
                'Sohail (18 Feb 2019) -- End


                If intEmpID > 0 Then
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objEmp.Update(mdtIdTran) = False Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objEmp.Update(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, mdtIdTran) = False Then

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objEmp.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, mdtIdTran, , , , , , , , , , , , getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP) = False Then
                    If objEmp.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, mdtIdTran, , , , , , , , , , , , getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP) = False Then
                        'Pinkal (18-Aug-2018) -- End
                        'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP})} -- END
                        'Sohail (21 Aug 2015) -- End
                        'S.SANDEEP [04 JUN 2015] -- END
                        eZeeMsgBox.Show(objEmp._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If

                    Dim objTransfer As New clsemployee_transfer_tran
                    Dim dsTransfer As New DataSet

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsTransfer = objTransfer.Get_Current_Allocation(CDate(dtRow.Item("allocation_change_date")), objEmp._Employeeunkid)
                    dsTransfer = objTransfer.Get_Current_Allocation(CDate(dtRow.Item("allocation_change_date")), objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)))
                    'S.SANDEEP [04 JUN 2015] -- END

                    If dsTransfer.Tables(0).Rows.Count > 0 Then
                        objTransfer._Stationunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("stationunkid"))
                        objTransfer._Deptgroupunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("deptgroupunkid"))
                        objTransfer._Sectiongroupunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("sectiongroupunkid"))
                        objTransfer._Sectionunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("sectionunkid"))
                        objTransfer._Unitgroupunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("unitgroupunkid"))
                        objTransfer._Unitunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("unitunkid"))
                        objTransfer._Teamunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("teamunkid"))
                        objTransfer._Classgroupunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("classgroupunkid"))
                        objTransfer._Classunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("classunkid"))
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objTransfer._Employeeunkid = objEmp._Employeeunkid
                    objTransfer._Employeeunkid = objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    'S.SANDEEP [04 JUN 2015] -- END

                    objTransfer._Effectivedate = CDate(dtRow.Item("allocation_change_date"))
                    objTransfer._Departmentunkid = CInt(dtRow.Item("departmentunkid"))
                     'Sohail (16 Jun 2015) -- Start
                    'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
                    objTransfer._Classgroupunkid = CInt(dtRow.Item("classgroupunkid"))
                    'Sohail (16 Jun 2015) -- End
                    If objTransfer.isExist(objTransfer._Effectivedate, objTransfer._Stationunkid, objTransfer._Deptgroupunkid, objTransfer._Departmentunkid, objTransfer._Sectiongroupunkid, objTransfer._Sectionunkid, objTransfer._Unitgroupunkid, objTransfer._Unitunkid, objTransfer._Teamunkid, objTransfer._Classgroupunkid, objTransfer._Classunkid, objTransfer._Employeeunkid) = False Then

                        'S.SANDEEP [10-MAY-2017] -- START
                        'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
                        'If objTransfer.Insert() = False Then
                        '    eZeeMsgBox.Show(objTransfer._Message, enMsgBoxStyle.Information)
                        '    Exit Try
                        'End If



                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'If objTransfer.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid) = False Then
                        If objTransfer.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid) = False Then
                            'Pinkal (12-Oct-2020) -- End
                            'Pinkal (18-Aug-2018) -- End
                            eZeeMsgBox.Show(objTransfer._Message, enMsgBoxStyle.Information)
                            Exit Try
                        End If
                        'S.SANDEEP [10-MAY-2017] -- END
                    End If

                    Dim objReCategorize As New clsemployee_categorization_Tran

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objReCategorize._Employeeunkid = objEmp._Employeeunkid
                    objReCategorize._Employeeunkid = objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    'S.SANDEEP [04 JUN 2015] -- END

                    objReCategorize._Effectivedate = CDate(dtRow.Item("allocation_change_date"))
                    objReCategorize._JobGroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                    objReCategorize._Jobunkid = CInt(dtRow.Item("jobunkid"))
                    objReCategorize._Gradeunkid = 0
                    objReCategorize._Gradelevelunkid = 0
                    If objReCategorize.isExist(objReCategorize._Effectivedate, objReCategorize._Employeeunkid, objReCategorize._JobGroupunkid, objReCategorize._Jobunkid, objReCategorize._Gradeunkid, objReCategorize._Gradelevelunkid) = False Then
                        'S.SANDEEP [10-MAY-2017] -- START
                        'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
                        'If objReCategorize.Insert() = False Then
                        '    eZeeMsgBox.Show(objReCategorize._Message, enMsgBoxStyle.Information)
                        '    Exit Try
                        'End If


                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'If objReCategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid) = False Then
                        If objReCategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName) = False Then
                            'Pinkal (12-Oct-2020) -- End
                            eZeeMsgBox.Show(objReCategorize._Message, enMsgBoxStyle.Information)
                            Exit Try
                        End If
                        'S.SANDEEP [10-MAY-2017] -- END
                    End If


                    If CInt(dtRow.Item("periodunkid")) > 0 Then
                        If objSalary.isExistOnSameDate(CDate(dtRow.Item("incrementdate")), intEmpID) = False Then
                            objSalary = New clsSalaryIncrement

                            objSalary._Periodunkid = CInt(dtRow.Item("periodunkid"))
                            objSalary._Employeeunkid = intEmpID
                            objSalary._Incrementdate = CDate(dtRow.Item("incrementdate"))
                            objSalary._Currentscale = CDec(dtRow.Item("currentscale"))
                            objSalary._Increment = CDec(dtRow.Item("increment"))
                            objSalary._Newscale = CDec(dtRow.Item("newscale"))
                            objSalary._Gradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                            objSalary._Gradeunkid = CInt(dtRow.Item("gradeunkid"))
                            objSalary._Gradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                            objSalary._Isgradechange = CBool(dtRow.Item("isgradechange"))
                            objSalary._Isfromemployee = CBool(dtRow.Item("isfromemployee"))
                            objSalary._Reason_Id = CInt(dtRow.Item("reason_id"))
                            objSalary._Increment_Mode = CInt(dtRow.Item("increment_mode"))
                            objSalary._Percentage = 0
                            'Sohail (02 Mar 2020) -- Start
                            'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
                            objSalary._ActualDate = CDate(dtRow.Item("incrementdate"))
                            objSalary._Arrears_Countryid = Company._Object._Localization_Country
                            'Sohail (02 Mar 2020) -- End

                            objSalary._Userunkid = User._Object._Userunkid
                            objSalary._Isvoid = False
                            objSalary._Voiduserunkid = -1
                            objSalary._Voiddatetime = Nothing
                            objSalary._Voidreason = ""

                            If User._Object.Privilege._AllowToApproveSalaryChange = False Then
                                objSalary._Isapproved = False
                                objSalary._Approveruserunkid = -1
                            Else
                                objSalary._Isapproved = True
                                objSalary._Approveruserunkid = User._Object._Userunkid
                            End If

                            objSalary._PSoft_SyncDateTime = CDate(dtRow.Item("last_update_time"))

                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'If objSalary.Insert(True, True, True) = False Then
                            'Sohail (24 Feb 2022) -- Start
                            'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                            'If objSalary.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(dtRow.Item("start_date").ToString), eZeeDate.convertDate(dtRow.Item("end_date").ToString), ConfigParameter._Object._UserAccessModeSetting, True, True, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, True) = False Then
                            If objSalary.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(dtRow.Item("start_date").ToString), eZeeDate.convertDate(dtRow.Item("end_date").ToString), ConfigParameter._Object._UserAccessModeSetting, True, True, True, ConfigParameter._Object._CurrentDateAndTime, True, True) = False Then
                                'Sohail ((24 Feb 2022) -- End
                                'Sohail (21 Aug 2015) -- End
                                eZeeMsgBox.Show(objSalary._Message, enMsgBoxStyle.Information)
                                Exit Try
                            End If

                        End If
                    End If
                Else

                    Dim objShiftTran As New clsEmployee_Shift_Tran
                    Dim mdtShiftTran As New DataTable
                    Dim dtSRow As DataRow
                    objShiftTran._EmployeeUnkid = intEmpID
                    mdtShiftTran = objShiftTran._SDataTable

                    dtSRow = mdtShiftTran.NewRow

                    dtSRow.Item("shifttranunkid") = -1
                    dtSRow.Item("employeeunkid") = 0
                    dtSRow.Item("shiftunkid") = CInt(dtRow.Item("shiftunkid"))
                    dtSRow.Item("isdefault") = False
                    dtSRow.Item("userunkid") = User._Object._Userunkid
                    dtSRow.Item("isvoid") = False
                    dtSRow.Item("voiduserunkid") = -1
                    dtSRow.Item("voiddatetime") = DBNull.Value
                    dtSRow.Item("voidreason") = ""
                    dtSRow.Item("AUD") = "A"
                    dtSRow.Item("GUID") = Guid.NewGuid.ToString
                    dtSRow.Item("effectivedate") = CDate(dtRow.Item("appointeddate"))
                    dtSRow.Item("shiftname") = dtRow.Item("shift")

                    mdtShiftTran.Rows.Add(dtSRow)

                    objEmp._dtShiftTran = mdtShiftTran

                    'Varsha (10 Nov 2017) -- Start
                    'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
                    objEmp._Theme_Id = intThemeId
                    'Varsha (10 Nov 2017) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objEmp.Insert(mdtIdTran) = False Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objEmp.Insert(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, mdtIdTran) = False Then

                    'Nilay (27 Apr 2016) -- Start
                    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
                    'If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, mdtIdTran) = False Then

                    'S.SANDEEP [08 DEC 2016] -- START
                    'ENHANCEMENT : Issue for Wrong User Name Sent in Email (Edit By User : <UserName>)
                    'If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                    '                 CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, _
                    '                 ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, _
                    '                 ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, _
                    '                 FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                    '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                    '                 True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                    '                 ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._SalaryAnniversarySetting, _
                    '                 ConfigParameter._Object._SalaryAnniversaryMonthBy, mdtIdTran) = False Then

                    'S.SANDEEP [26-SEP-2018] -- START
                    objEmp._IsFlexAccountCreated = False
                    'S.SANDEEP [26-SEP-2018] -- END
                    'Sohail (02 Mar 2020) -- Start
                    'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
                    objEmp._BaseCountryunkid = Company._Object._Localization_Country
                    'Sohail (02 Mar 2020) -- End

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                    'If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                    '                 CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, _
                    '                 ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, _
                    '                 ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, _
                    '                 FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                    '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                    '                 True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                    '                 ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._SalaryAnniversarySetting, _
                    '                 ConfigParameter._Object._SalaryAnniversaryMonthBy, mdtIdTran, , , , , , , , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) = False Then

                    If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                                 CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, _
                                 ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, _
                                 ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, _
                                 FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                                 ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._SalaryAnniversarySetting, _
                                 ConfigParameter._Object._SalaryAnniversaryMonthBy, ConfigParameter._Object._CreateADUserFromEmpMst, mdtIdTran, , , , , , , , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) = False Then


                        'Pinkal (18-Aug-2018) -- End

                        'S.SANDEEP [08 DEC 2016] -- END

                        'Nilay (27 Apr 2016) -- End

                        'Sohail (21 Aug 2015) -- End
                        'S.SANDEEP [04 JUN 2015] -- END
                        eZeeMsgBox.Show(objEmp._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'intEmpID = objEmp._Employeeunkid
                    intEmpID = objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                If intEmpID > 0 Then
                    For Each dsRow As DataRow In mdtDependant.Select("employeecode = '" & dtRow.Item("employeecode").ToString & "' AND identity_no = '" & dtRow.Item("identity_no").ToString & "' AND rowtypeid = 0 ")

                        objDependants_Benefice = New clsDependants_Beneficiary_tran

                        Dim intDPUnkID As Integer = objDependants_Benefice.GetDependantUnkid(intEmpID, dsRow.Item("first_name").ToString, dsRow.Item("last_name").ToString)

                        If intDPUnkID > 0 Then
                            objDependants_Benefice._Dpndtbeneficetranunkid = intDPUnkID
                        End If

                        objDependants_Benefice._Employeeunkid = intEmpID
                        objDependants_Benefice._First_Name = dsRow.Item("first_name").ToString
                        objDependants_Benefice._Middle_Name = dsRow.Item("middle_name").ToString
                        objDependants_Benefice._Last_Name = dsRow.Item("last_name").ToString
                        objDependants_Benefice._Address = dsRow.Item("address").ToString
                        objDependants_Benefice._Birthdate = CDate(dsRow.Item("birthdate"))
                        objDependants_Benefice._Cityunkid = CInt(dsRow.Item("cityunkid"))
                        objDependants_Benefice._Countryunkid = CInt(dsRow.Item("countryunkid"))
                        objDependants_Benefice._Email = dsRow.Item("email").ToString
                        objDependants_Benefice._Identify_No = ""
                        objDependants_Benefice._Isdependant = True
                        objDependants_Benefice._Mobile_No = dsRow.Item("mobile_no").ToString
                        objDependants_Benefice._Nationalityunkid = 0
                        objDependants_Benefice._Post_Box = dsRow.Item("post_box").ToString
                        objDependants_Benefice._Relationunkid = CInt(dsRow.Item("relationunkid"))
                        objDependants_Benefice._Stateunkid = CInt(dsRow.Item("stateunkid"))
                        objDependants_Benefice._Telephone_No = ""

                        objDependants_Benefice._Isvoid = False
                        objDependants_Benefice._Userunkid = 1
                        objDependants_Benefice._Voiddatetime = Nothing
                        objDependants_Benefice._Voidreason = ""

                        objDependants_Benefice._Zipcodeunkid = 0
                        objDependants_Benefice._Gender = CInt(dsRow.Item("gender"))

                        objDependants_Benefice._CompanyId = ConfigParameter._Object._Companyunkid
                        objDependants_Benefice._blnImgInDb = ConfigParameter._Object._IsImgInDataBase
                        objDependants_Benefice._PSoft_SyncDateTime = CDate(dtRow.Item("last_update_time"))

                        'Sohail (18 May 2019) -- Start
                        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                        With objDependants_Benefice
                            ._WebFormName = mstrModuleName
                            ._Loginemployeeunkid = 0
                            ._WebClientIP = getIP()
                            ._WebHostName = getHostName()
                            ._Isweb = False
                            ._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        'Sohail (18 May 2019) -- End

                        If intDPUnkID > 0 Then
                            If objDependants_Benefice.Update() = False Then
                                eZeeMsgBox.Show(objDependants_Benefice._Message, enMsgBoxStyle.Information)
                                Exit Try
                            End If
                        Else
                            If objDependants_Benefice.Insert() = False Then
                                eZeeMsgBox.Show(objDependants_Benefice._Message, enMsgBoxStyle.Information)
                                Exit Try
                            End If
                        End If


                    Next
                End If

            Next

            mblnCancel = False

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                If modGlobal.Export_ErrorList(savDialog.FileName, m_Dataview.ToTable, "Import Employee Wizard") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAll(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Datagridview Events "
    Private Sub dgvImportInfo_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvImportInfo.SelectionChanged
        Try

            If dgvImportInfo.CurrentRow Is Nothing Then Exit Try

            m_Dataview_Dep.RowFilter = "employeecode = '" & dgvImportInfo.CurrentRow.Cells(dgcolhEmpCode.Index).Value.ToString & "' AND identity_no = '" & dgvImportInfo.CurrentRow.Cells(dgcolhGlobalID.Index).Value.ToString & "' "
            Call FillDependantGrid()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_SelectionChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other control's Events "
    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowUnSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowUnSuccessful.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid <> 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowUnSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowAll.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = ""

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowAll_Click", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
			Me.mnuShowUnSuccessful.Text = Language._Object.getCaption(Me.mnuShowUnSuccessful.Name, Me.mnuShowUnSuccessful.Text)
			Me.mnuShowAll.Text = Language._Object.getCaption(Me.mnuShowAll.Name, Me.mnuShowAll.Text)
			Me.lblDependant.Text = Language._Object.getCaption(Me.lblDependant.Name, Me.lblDependant.Text)
			Me.dpcolhGlobalID.HeaderText = Language._Object.getCaption(Me.dpcolhGlobalID.Name, Me.dpcolhGlobalID.HeaderText)
			Me.dpcolhLocalID.HeaderText = Language._Object.getCaption(Me.dpcolhLocalID.Name, Me.dpcolhLocalID.HeaderText)
			Me.dpcolhRemark.HeaderText = Language._Object.getCaption(Me.dpcolhRemark.Name, Me.dpcolhRemark.HeaderText)
			Me.dpcolhEmpID.HeaderText = Language._Object.getCaption(Me.dpcolhEmpID.Name, Me.dpcolhEmpID.HeaderText)
			Me.dpcolhFirstName.HeaderText = Language._Object.getCaption(Me.dpcolhFirstName.Name, Me.dpcolhFirstName.HeaderText)
			Me.dpcolhOtherName.HeaderText = Language._Object.getCaption(Me.dpcolhOtherName.Name, Me.dpcolhOtherName.HeaderText)
			Me.dpcolhSurname.HeaderText = Language._Object.getCaption(Me.dpcolhSurname.Name, Me.dpcolhSurname.HeaderText)
			Me.dpcolhGender.HeaderText = Language._Object.getCaption(Me.dpcolhGender.Name, Me.dpcolhGender.HeaderText)
			Me.dpcolhRelation.HeaderText = Language._Object.getCaption(Me.dpcolhRelation.Name, Me.dpcolhRelation.HeaderText)
			Me.dpcolhBirthDate.HeaderText = Language._Object.getCaption(Me.dpcolhBirthDate.Name, Me.dpcolhBirthDate.HeaderText)
			Me.dpcolhAddress.HeaderText = Language._Object.getCaption(Me.dpcolhAddress.Name, Me.dpcolhAddress.HeaderText)
			Me.dpcolhMobileNo.HeaderText = Language._Object.getCaption(Me.dpcolhMobileNo.Name, Me.dpcolhMobileNo.HeaderText)
			Me.dpcolhCountry.HeaderText = Language._Object.getCaption(Me.dpcolhCountry.Name, Me.dpcolhCountry.HeaderText)
			Me.dpcolhState.HeaderText = Language._Object.getCaption(Me.dpcolhState.Name, Me.dpcolhState.HeaderText)
			Me.dpcolhCity.HeaderText = Language._Object.getCaption(Me.dpcolhCity.Name, Me.dpcolhCity.HeaderText)
			Me.dpcolhEmail.HeaderText = Language._Object.getCaption(Me.dpcolhEmail.Name, Me.dpcolhEmail.HeaderText)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.dgcolhGlobalID.HeaderText = Language._Object.getCaption(Me.dgcolhGlobalID.Name, Me.dgcolhGlobalID.HeaderText)
			Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
			Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)
			Me.dgcolhEmpID.HeaderText = Language._Object.getCaption(Me.dgcolhEmpID.Name, Me.dgcolhEmpID.HeaderText)
			Me.dgcolhTitle.HeaderText = Language._Object.getCaption(Me.dgcolhTitle.Name, Me.dgcolhTitle.HeaderText)
			Me.dgcolhFirstName.HeaderText = Language._Object.getCaption(Me.dgcolhFirstName.Name, Me.dgcolhFirstName.HeaderText)
			Me.dgcolhOtherName.HeaderText = Language._Object.getCaption(Me.dgcolhOtherName.Name, Me.dgcolhOtherName.HeaderText)
			Me.dgcolhSurname.HeaderText = Language._Object.getCaption(Me.dgcolhSurname.Name, Me.dgcolhSurname.HeaderText)
			Me.dgcolhGender.HeaderText = Language._Object.getCaption(Me.dgcolhGender.Name, Me.dgcolhGender.HeaderText)
			Me.dgcolhAppointedDate.HeaderText = Language._Object.getCaption(Me.dgcolhAppointedDate.Name, Me.dgcolhAppointedDate.HeaderText)
			Me.dgcolhEmplType.HeaderText = Language._Object.getCaption(Me.dgcolhEmplType.Name, Me.dgcolhEmplType.HeaderText)
			Me.dgcolhBirthDate.HeaderText = Language._Object.getCaption(Me.dgcolhBirthDate.Name, Me.dgcolhBirthDate.HeaderText)
			Me.dgcolhBirthCountry.HeaderText = Language._Object.getCaption(Me.dgcolhBirthCountry.Name, Me.dgcolhBirthCountry.HeaderText)
			Me.dgcolhVillage.HeaderText = Language._Object.getCaption(Me.dgcolhVillage.Name, Me.dgcolhVillage.HeaderText)
			Me.dgcolhMaritalStatus.HeaderText = Language._Object.getCaption(Me.dgcolhMaritalStatus.Name, Me.dgcolhMaritalStatus.HeaderText)
			Me.dgcolhAnniversaryDate.HeaderText = Language._Object.getCaption(Me.dgcolhAnniversaryDate.Name, Me.dgcolhAnniversaryDate.HeaderText)
			Me.dgcolhDept.HeaderText = Language._Object.getCaption(Me.dgcolhDept.Name, Me.dgcolhDept.HeaderText)
			Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
			Me.dgcolhBranch.HeaderText = Language._Object.getCaption(Me.dgcolhBranch.Name, Me.dgcolhBranch.HeaderText)
			Me.dgcolhJobGroup.HeaderText = Language._Object.getCaption(Me.dgcolhJobGroup.Name, Me.dgcolhJobGroup.HeaderText)
			Me.dgcolhCCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCCenter.Name, Me.dgcolhCCenter.HeaderText)
			Me.dgcolhClassGroup.HeaderText = Language._Object.getCaption(Me.dgcolhClassGroup.Name, Me.dgcolhClassGroup.HeaderText)
			Me.dgcolhIncrementDate.HeaderText = Language._Object.getCaption(Me.dgcolhIncrementDate.Name, Me.dgcolhIncrementDate.HeaderText)
			Me.dgcolhScale.HeaderText = Language._Object.getCaption(Me.dgcolhScale.Name, Me.dgcolhScale.HeaderText)
			Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
			Me.dgcolhHeadType.HeaderText = Language._Object.getCaption(Me.dgcolhHeadType.Name, Me.dgcolhHeadType.HeaderText)
			Me.dgcolhLeavingDate.HeaderText = Language._Object.getCaption(Me.dgcolhLeavingDate.Name, Me.dgcolhLeavingDate.HeaderText)
			Me.dgcolhEOCDate.HeaderText = Language._Object.getCaption(Me.dgcolhEOCDate.Name, Me.dgcolhEOCDate.HeaderText)
			Me.dgcolhRetirementDate.HeaderText = Language._Object.getCaption(Me.dgcolhRetirementDate.Name, Me.dgcolhRetirementDate.HeaderText)
			Me.dgcolhReinstatementDate.HeaderText = Language._Object.getCaption(Me.dgcolhReinstatementDate.Name, Me.dgcolhReinstatementDate.HeaderText)
			Me.dgcolhLastUpdateime.HeaderText = Language._Object.getCaption(Me.dgcolhLastUpdateime.Name, Me.dgcolhLastUpdateime.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class