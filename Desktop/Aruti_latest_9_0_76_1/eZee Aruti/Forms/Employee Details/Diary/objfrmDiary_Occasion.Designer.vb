﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Occasion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elAchievement = New eZee.Common.eZeeLine
        Me.lvQualification = New eZee.Common.eZeeListView(Me.components)
        Me.colhQGrp = New System.Windows.Forms.ColumnHeader
        Me.colhQualification = New System.Windows.Forms.ColumnHeader
        Me.colhRefNo = New System.Windows.Forms.ColumnHeader
        Me.colhStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.objcolhGrp = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elAchievement
        '
        Me.elAchievement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elAchievement.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elAchievement.Location = New System.Drawing.Point(2, 9)
        Me.elAchievement.Name = "elAchievement"
        Me.elAchievement.Size = New System.Drawing.Size(575, 16)
        Me.elAchievement.TabIndex = 394
        Me.elAchievement.Text = "Qualification"
        Me.elAchievement.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvQualification
        '
        Me.lvQualification.BackColorOnChecked = False
        Me.lvQualification.ColumnHeaders = Nothing
        Me.lvQualification.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhQGrp, Me.colhQualification, Me.colhRefNo, Me.colhStartDate, Me.colhEndDate, Me.objcolhGrp})
        Me.lvQualification.CompulsoryColumns = ""
        Me.lvQualification.FullRowSelect = True
        Me.lvQualification.GroupingColumn = Nothing
        Me.lvQualification.HideSelection = False
        Me.lvQualification.Location = New System.Drawing.Point(5, 28)
        Me.lvQualification.MinColumnWidth = 50
        Me.lvQualification.MultiSelect = False
        Me.lvQualification.Name = "lvQualification"
        Me.lvQualification.OptionalColumns = ""
        Me.lvQualification.ShowMoreItem = False
        Me.lvQualification.ShowSaveItem = False
        Me.lvQualification.ShowSelectAll = True
        Me.lvQualification.ShowSizeAllColumnsToFit = True
        Me.lvQualification.Size = New System.Drawing.Size(572, 378)
        Me.lvQualification.Sortable = True
        Me.lvQualification.TabIndex = 407
        Me.lvQualification.UseCompatibleStateImageBehavior = False
        Me.lvQualification.View = System.Windows.Forms.View.Details
        '
        'colhQGrp
        '
        Me.colhQGrp.Tag = "colhQGrp"
        Me.colhQGrp.Text = "Qualification Group"
        Me.colhQGrp.Width = 135
        '
        'colhQualification
        '
        Me.colhQualification.Tag = "colhQualification"
        Me.colhQualification.Text = "Qualification"
        Me.colhQualification.Width = 135
        '
        'colhRefNo
        '
        Me.colhRefNo.Tag = "colhRefNo"
        Me.colhRefNo.Text = "Ref. No."
        Me.colhRefNo.Width = 115
        '
        'colhStartDate
        '
        Me.colhStartDate.Tag = "colhStartDate"
        Me.colhStartDate.Text = "Start Date"
        Me.colhStartDate.Width = 90
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 90
        '
        'objcolhGrp
        '
        Me.objcolhGrp.Tag = "objcolhGrp"
        Me.objcolhGrp.Text = "objcolhGrp"
        Me.objcolhGrp.Width = 0
        '
        'objfrmDiary_Occasion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvQualification)
        Me.Controls.Add(Me.elAchievement)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Occasion"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elAchievement As eZee.Common.eZeeLine
    Friend WithEvents lvQualification As eZee.Common.eZeeListView
    Friend WithEvents colhQGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhQualification As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGrp As System.Windows.Forms.ColumnHeader
End Class
