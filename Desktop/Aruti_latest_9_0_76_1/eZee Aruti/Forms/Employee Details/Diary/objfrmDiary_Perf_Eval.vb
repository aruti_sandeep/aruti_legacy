﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Perf_Eval

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1
    Private strDatabaseName As String = ""
    Private mdsList As DataSet

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Perf_Eval_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Perf_Eval_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany_Master
        Dim dtTable As DataTable
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "List")
            dtTable = New DataView(dsCombo.Tables("List"), "", "end_date DESC ", DataViewRowState.CurrentRows).ToTable
            With cboYear
                .ValueMember = "yearunkid"
                .DisplayMember = "financialyear_name"
                .DataSource = dtTable
                .SelectedIndex = 0
            End With

            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "General Evaluation"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "BSC Evaluation"))
                '.Items.Add(Language.getMessage(mstrModuleName, 202, "Appraisal"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillAssessmentResult()
        Try

            RemoveHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

            If mdsList.Tables(0).Columns.Count > 0 Then
                Dim dtTable As New DataTable("Data")
                dtTable = mdsList.Tables(0).Copy
                dtTable.Rows.Clear()
                Dim mdecValue() As Decimal
                Dim mdecValueTotal() As Decimal : mdecValueTotal = New Decimal(mdsList.Tables(0).Columns.Count) {}
                Dim mdicIsNew As New Dictionary(Of Integer, String)
                For Each dtRow As DataRow In mdsList.Tables(0).Rows
                    mdecValue = New Decimal(mdsList.Tables(0).Columns.Count) {}
                    If mdicIsNew.ContainsKey(CInt(dtRow.Item("assessitemunkid"))) Then Continue For
                    mdicIsNew.Add(CInt(dtRow.Item("assessitemunkid")), CStr(dtRow.Item("MainGrp")))
                    Dim dtTemp() As DataRow = mdsList.Tables(0).Select("assessitemunkid = '" & CInt(dtRow.Item("assessitemunkid")) & "'")
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp.Length - 1
                            For j As Integer = 0 To dtTemp(i).ItemArray.Length - 1
                                Select Case j
                                    Case 0, 1, 3, 4, 5
                                    Case Else
                                        If j Mod 2 = 0 Then
                                            If IsDBNull(dtTemp(i)(j)) = False Then
                                                If IsNumeric(dtTemp(i)(j)) Then
                                                    mdecValue(j) = mdecValue(j) + CDec(dtTemp(i)(j))
                                                    mdecValueTotal(j) = mdecValueTotal(j) + CDec(dtTemp(i)(j))
                                                End If
                                            End If
                                        End If
                                End Select
                            Next
                            dtTable.ImportRow(dtTemp(i))
                        Next
                    End If

                    Dim dRow As DataRow = dtTable.NewRow

                    For i As Integer = 0 To mdecValue.Length - 1
                        If mdecValue(i) <= 0 Then
                            Select Case i
                                Case 0
                                    dRow.Item(i) = mdicIsNew(CInt(dtRow.Item("assessitemunkid")))
                                Case 1
                                    dRow.Item(i) = "Group Total : "
                                Case 4, 5
                                    dRow.Item(i) = "-1"
                            End Select
                        Else
                            dRow.Item(i) = mdecValue(i)
                        End If
                    Next
                    dtTable.Rows.Add(dRow)
                Next

                Dim drRow As DataRow = dtTable.NewRow

                For i As Integer = 0 To mdecValueTotal.Length - 1
                    If mdecValueTotal(i) <= 0 Then
                        Select Case i
                            Case 0
                                drRow.Item(i) = ""
                            Case 1
                                drRow.Item(i) = "Grand Total : "
                            Case 4, 5
                                drRow.Item(i) = "-2"
                        End Select
                    Else
                        drRow.Item(i) = mdecValueTotal(i)
                    End If
                Next

                dtTable.Rows.Add(drRow)

                Dim strColumns As String = String.Empty
                Dim iCntColumns As Integer = 0
                For Each dCol As DataColumn In dtTable.Columns
                    Select Case iCntColumns
                        Case 0, 4, 5
                            lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
                        Case 1
                            lvAssessments.Columns.Add(dCol.Caption, 500, HorizontalAlignment.Left)
                        Case Else
                            lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
                    End Select
                    strColumns &= "," & dCol.ColumnName
                    iCntColumns += 1
                Next

                strColumns = Mid(strColumns, 2)
                Dim strColumnName() As String = strColumns.Split(CChar(","))

                For Each dtRow As DataRow In dtTable.Rows
                    Dim lvItem As New ListViewItem
                    For i As Integer = 0 To iCntColumns - 1
                        Select Case i
                            Case 0
                                lvItem.Text = dtRow.Item(strColumnName(i)).ToString
                            Case Else
                                lvItem.SubItems.Add(dtRow.Item(strColumnName(i)).ToString)
                        End Select
                    Next
                    lvAssessments.Items.Add(lvItem)
                Next

                lvAssessments.GroupingColumn = lvAssessments.Columns(0)
                lvAssessments.DisplayGroups(True)

                For Each lvItem As ListViewItem In lvAssessments.Items
                    If CInt(lvItem.SubItems(4).Text) = -1 Then
                        lvItem.BackColor = Color.SteelBlue
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    ElseIf CInt(lvItem.SubItems(4).Text) = -2 Then
                        lvItem.BackColor = Color.Maroon
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    End If
                Next
            End If

            AddHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAssessmentResult", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillRemarks(ByVal lvListView As eZee.Common.eZeeListView)
        Try
            Dim objAssessMaster As New clsassess_analysis_master
            lvListView.Items.Clear() : lvListView.GridLines = False
            Select Case CType(lvListView, eZee.Common.eZeeListView).Name.ToUpper
                Case "LVIMPROVEMENT"
                    mdsList = objAssessMaster.GetRemarksTran(True, mintEmployeeId, CInt(cboGroup.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), strDatabaseName)
                    For Each dRow As DataRow In mdsList.Tables(0).Rows
                        Dim lvItem As New ListViewItem

                        lvItem.Text = dRow.Item("Major_Area").ToString
                        lvItem.SubItems.Add(dRow.Item("Action_Activity").ToString)
                        lvItem.SubItems.Add(dRow.Item("Support").ToString)
                        lvItem.SubItems.Add(dRow.Item("Course_Master").ToString)
                        If dRow.Item("TimeFrame").ToString.Trim.Length > 0 Then
                            lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("TimeFrame").ToString).ToShortDateString)
                        Else
                            lvItem.SubItems.Add("")
                        End If
                        lvItem.SubItems.Add(dRow.Item("Other").ToString)

                        lvListView.Items.Add(lvItem)
                        lvItem = Nothing
                    Next
                Case "LVPERSONALDEVELOP"
                    mdsList = objAssessMaster.GetRemarksTran(False, mintEmployeeId, CInt(cboGroup.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), strDatabaseName)

                    For Each dRow As DataRow In mdsList.Tables(0).Rows
                        Dim lvItem As New ListViewItem

                        lvItem.Text = dRow.Item("Major_Area").ToString
                        lvItem.SubItems.Add(dRow.Item("Action_Activity").ToString)
                        lvItem.SubItems.Add(dRow.Item("Support").ToString)
                        lvItem.SubItems.Add(dRow.Item("Course_Master").ToString)
                        If dRow.Item("TimeFrame").ToString.Trim.Length > 0 Then
                            lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("TimeFrame").ToString).ToShortDateString)
                        Else
                            lvItem.SubItems.Add("")
                        End If
                        lvItem.SubItems.Add(dRow.Item("Other").ToString)
                        lvItem.SubItems.Add(dRow.Item("Appraiser").ToString)

                        lvListView.Items.Add(lvItem)
                        'lvItem = Nothing
                    Next
                    CType(lvListView, eZee.Common.eZeeListView).GroupingColumn = objcolhAppraiser
                    CType(lvListView, eZee.Common.eZeeListView).DisplayGroups(True)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillRemarks", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillBSC_Result()
        Try

            RemoveHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

            If mdsList.Tables(0).Columns.Count > 0 Then
                Dim dtTable As New DataTable("Data")
                dtTable = mdsList.Tables(0).Copy
                dtTable.Rows.Clear()
                Dim mdecValue() As Decimal
                Dim mdecValueTotal() As Decimal : mdecValueTotal = New Decimal(mdsList.Tables(0).Columns.Count) {}
                Dim mdicIsNew As New Dictionary(Of Integer, String)
                For Each dtRow As DataRow In mdsList.Tables(0).Rows
                    mdecValue = New Decimal(mdsList.Tables(0).Columns.Count) {}
                    If mdicIsNew.ContainsKey(CInt(dtRow.Item("PId"))) Then Continue For
                    mdicIsNew.Add(CInt(dtRow.Item("PId")), CStr(dtRow.Item("perspective")))
                    Dim dtTemp() As DataRow = mdsList.Tables(0).Select("PId = '" & CInt(dtRow.Item("PId")) & "'")
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp.Length - 1
                            For j As Integer = 0 To dtTemp(i).ItemArray.Length - 1
                                Select Case j
                                    'S.SANDEEP [ 04 JULY 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    'Case Is >= 12
                                    Case Is > 12
                                        'S.SANDEEP [ 04 JULY 2012 ] -- END
                                        If j Mod 2 = 0 Then
                                            If IsDBNull(dtTemp(i)(j)) = False Then
                                                If IsNumeric(dtTemp(i)(j)) Then
                                                    mdecValue(j) = mdecValue(j) + CDec(dtTemp(i)(j))
                                                    mdecValueTotal(j) = mdecValueTotal(j) + CDec(dtTemp(i)(j))
                                                End If
                                            End If
                                        End If
                                End Select
                            Next
                            dtTable.ImportRow(dtTemp(i))
                        Next
                    End If

                    Dim dRow As DataRow = dtTable.NewRow

                    For i As Integer = 0 To mdecValue.Length - 1
                        If mdecValue(i) <= 0 Then
                            Select Case i
                                Case 0, 1, 2, 3, 4
                                    dRow.Item(i) = -2
                                Case 5
                                    dRow.Item(i) = mdicIsNew(CInt(dtRow.Item("PId")))
                                Case 6
                                    dRow.Item(i) = "Group Total : "
                            End Select
                        Else
                            dRow.Item(i) = mdecValue(i)
                        End If
                    Next
                    dtTable.Rows.Add(dRow)
                Next
                Dim drRow As DataRow = dtTable.NewRow

                For i As Integer = 0 To mdecValueTotal.Length - 1
                    If mdecValueTotal(i) <= 0 Then
                        Select Case i
                            Case 0, 1, 2, 3, 4
                                drRow.Item(i) = -1
                            Case 6
                                drRow.Item(i) = "Grand Total : "
                        End Select
                    Else
                        drRow.Item(i) = mdecValueTotal(i)
                    End If
                Next

                dtTable.Rows.Add(drRow)

                Dim strColumns As String = String.Empty
                Dim iCntColumns As Integer = 0

                For Each dCol As DataColumn In dtTable.Columns
                    Select Case iCntColumns
                        Case 0 To 5
                            lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
                        Case 6 To 9
                            lvAssessments.Columns.Add(dCol.Caption, 150, HorizontalAlignment.Left)
                        Case 10, 11
                            lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
                        Case Is >= 12
                            lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
                    End Select
                    strColumns &= "," & dCol.ColumnName
                    iCntColumns += 1
                Next

                strColumns = Mid(strColumns, 2)
                Dim strColumnName() As String = strColumns.Split(CChar(","))

                For Each dtRow As DataRow In dtTable.Rows
                    Dim lvItem As New ListViewItem
                    For i As Integer = 0 To iCntColumns - 1
                        Select Case i
                            Case 0
                                lvItem.Text = dtRow.Item(strColumnName(i)).ToString
                            Case Else
                                lvItem.SubItems.Add(dtRow.Item(strColumnName(i)).ToString)
                        End Select
                    Next
                    lvAssessments.Items.Add(lvItem)
                Next

                lvAssessments.GroupingColumn = lvAssessments.Columns(5)
                lvAssessments.DisplayGroups(True)

                For Each lvItem As ListViewItem In lvAssessments.Items
                    If CInt(lvItem.SubItems(0).Text) = -2 Then
                        lvItem.BackColor = Color.SteelBlue
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    ElseIf CInt(lvItem.SubItems(0).Text) = -1 Then
                        lvItem.BackColor = Color.Maroon
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    End If
                Next

            End If


            'If mdsList.Tables(0).Columns.Count > 0 Then
            '    Dim dtTable As New DataTable("Data")
            '    dtTable = mdsList.Tables(0).Copy
            '    dtTable.Rows.Clear()
            '    Dim mdecValue() As Decimal
            '    Dim mdecValueTotal() As Decimal : mdecValueTotal = New Decimal(mdsList.Tables(0).Columns.Count) {}
            '    Dim mdicIsNew As New Dictionary(Of Integer, String)
            '    For Each dtRow As DataRow In mdsList.Tables(0).Rows
            '        mdecValue = New Decimal(mdsList.Tables(0).Columns.Count) {}
            '        If mdicIsNew.ContainsKey(CInt(dtRow.Item("assessitemunkid"))) Then Continue For
            '        mdicIsNew.Add(CInt(dtRow.Item("assessitemunkid")), CStr(dtRow.Item("MainGrp")))
            '        Dim dtTemp() As DataRow = mdsList.Tables(0).Select("assessitemunkid = '" & CInt(dtRow.Item("assessitemunkid")) & "'")
            '        If dtTemp.Length > 0 Then
            '            For i As Integer = 0 To dtTemp.Length - 1
            '                For j As Integer = 0 To dtTemp(i).ItemArray.Length - 1
            '                    Select Case j
            '                        Case 0, 1, 3, 4
            '                        Case Else
            '                            If IsDBNull(dtTemp(i)(j)) = False Then
            '                                If IsNumeric(dtTemp(i)(j)) Then
            '                                    mdecValue(j) = mdecValue(j) + CDec(dtTemp(i)(j))
            '                                    mdecValueTotal(j) = mdecValueTotal(j) + CDec(dtTemp(i)(j))
            '                                End If
            '                            End If
            '                    End Select
            '                Next
            '                dtTable.ImportRow(dtTemp(i))
            '            Next
            '        End If

            '        Dim dRow As DataRow = dtTable.NewRow

            '        For i As Integer = 0 To mdecValue.Length - 1
            '            If mdecValue(i) <= 0 Then
            '                Select Case i
            '                    Case 0
            '                        dRow.Item(i) = mdicIsNew(CInt(dtRow.Item("assessitemunkid")))
            '                    Case 1
            '                        dRow.Item(i) = "Group Total : "
            '                    Case 3, 4
            '                        dRow.Item(i) = "-1"
            '                End Select
            '            Else
            '                dRow.Item(i) = mdecValue(i)
            '            End If
            '        Next
            '        dtTable.Rows.Add(dRow)
            '    Next

            '    Dim drRow As DataRow = dtTable.NewRow

            '    For i As Integer = 0 To mdecValueTotal.Length - 1
            '        If mdecValueTotal(i) <= 0 Then
            '            Select Case i
            '                Case 0
            '                    drRow.Item(i) = ""
            '                Case 1
            '                    drRow.Item(i) = "Grand Total : "
            '                Case 3, 4
            '                    drRow.Item(i) = "-2"
            '            End Select
            '        Else
            '            drRow.Item(i) = mdecValueTotal(i)
            '        End If
            '    Next

            '    dtTable.Rows.Add(drRow)

            '    Dim strColumns As String = String.Empty
            '    Dim iCntColumns As Integer = 0
            '    For Each dCol As DataColumn In dtTable.Columns
            '        Select Case iCntColumns
            '            Case 0, 3, 4
            '                lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
            '            Case 1
            '                lvAssessments.Columns.Add(dCol.Caption, 500, HorizontalAlignment.Left)
            '            Case Else
            '                lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
            '        End Select
            '        strColumns &= "," & dCol.ColumnName
            '        iCntColumns += 1
            '    Next

            '    strColumns = Mid(strColumns, 2)
            '    Dim strColumnName() As String = strColumns.Split(CChar(","))

            '    For Each dtRow As DataRow In dtTable.Rows
            '        Dim lvItem As New ListViewItem
            '        For i As Integer = 0 To iCntColumns - 1
            '            Select Case i
            '                Case 0
            '                    lvItem.Text = dtRow.Item(strColumnName(i)).ToString
            '                Case Else
            '                    lvItem.SubItems.Add(dtRow.Item(strColumnName(i)).ToString)
            '            End Select
            '        Next
            '        lvAssessments.Items.Add(lvItem)
            '    Next

            '    lvAssessments.GroupingColumn = lvAssessments.Columns(0)
            '    lvAssessments.DisplayGroups(True)

            '    For Each lvItem As ListViewItem In lvAssessments.Items
            '        If CInt(lvItem.SubItems(4).Text) = -1 Then
            '            lvItem.BackColor = Color.SteelBlue
            '            lvItem.ForeColor = Color.White
            '            lvItem.Font = New Font(Me.Font, FontStyle.Bold)
            '        ElseIf CInt(lvItem.SubItems(4).Text) = -2 Then
            '            lvItem.BackColor = Color.Maroon
            '            lvItem.ForeColor = Color.White
            '            lvItem.Font = New Font(Me.Font, FontStyle.Bold)
            '        End If
            '    Next
            'End If

            AddHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBSC_Result", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Try
            Dim objCompany As New clsCompany_Master
            dsCombo = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "List", CInt(cboYear.SelectedValue))
            If dsCombo.Tables("List").Rows.Count > 0 Then
                strDatabaseName = dsCombo.Tables("List").Rows(0).Item("database_name").ToString
            End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "Period", True, , , , strDatabaseName)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), strDatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, , )
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            Select Case cboViewBy.SelectedIndex
                Case 0
                    cboGroup.Visible = True : lblGroup.Visible = True
                    If tabcAssessments.TabPages.Contains(tabpAssessmentRemarks) = False Then
                        tabcAssessments.TabPages.Add(tabpAssessmentRemarks)
                    End If
                    Dim dsCombo As New DataSet : Dim objAssessGroup As New clsassess_group_master
                    dsCombo = objAssessGroup.getListForCombo(CInt(mintEmployeeId), "AssessGroup", True)
                    With cboGroup
                        .ValueMember = "assessgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dsCombo.Tables("AssessGroup")
                    End With
                Case Else
                    cboGroup.Visible = False : lblGroup.Visible = False
                    tabcAssessments.TabPages.Remove(tabpAssessmentRemarks)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkShow_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShow.LinkClicked
        Try
            lvAssessments.Columns.Clear()
            lvAssessments.Items.Clear()

            'lvImprovement.Columns.Clear()
            lvImprovement.Items.Clear()

            'lvPersonalDevelop.Columns.Clear()
            lvPersonalDevelop.Items.Clear()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If cboGroup.Visible = True Then
                If CInt(cboGroup.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Group is mandatory information. Please select Group to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Select Case CInt(cboViewBy.SelectedIndex)
                Case 0  'GENERAL ASSESSMENT
                    Dim objAssessMaster As New clsassess_analysis_master
                    mdsList = objAssessMaster.GetAssessmentResultView(mintEmployeeId, CInt(cboGroup.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), strDatabaseName)
                    Call FillAssessmentResult()
                    Call FillRemarks(lvImprovement)
                    Call FillRemarks(lvPersonalDevelop)
                Case 1  'BSC ASSESSMENT
                    Dim objBSCMaster As New clsBSC_Analysis_Master
                    mdsList = objBSCMaster.GetAssessmentResultView(mintEmployeeId, CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
                    Call FillBSC_Result()
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShow_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAssessments_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvAssessments.ItemSelectionChanged
        Try
            If lvAssessments.SelectedItems.Count > 0 Then
                If lvAssessments.SelectedItems(0).SubItems(4).Text = "-1" Or _
                   lvAssessments.SelectedItems(0).SubItems(4).Text = "-2" Then
                    e.Item.Selected = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssessments_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbPerfomanceEvaluation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPerfomanceEvaluation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.tabcRemarks.BackColor = GUI.FormColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.gbPerfomanceEvaluation.Text = Language._Object.getCaption(Me.gbPerfomanceEvaluation.Name, Me.gbPerfomanceEvaluation.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblView.Text = Language._Object.getCaption(Me.lblView.Name, Me.lblView.Text)
			Me.tabpAssessmentResults.Text = Language._Object.getCaption(Me.tabpAssessmentResults.Name, Me.tabpAssessmentResults.Text)
			Me.tabpAssessmentRemarks.Text = Language._Object.getCaption(Me.tabpAssessmentRemarks.Name, Me.tabpAssessmentRemarks.Text)
			Me.tabpSelf.Text = Language._Object.getCaption(Me.tabpSelf.Name, Me.tabpSelf.Text)
			Me.colhI_Improvement.Text = Language._Object.getCaption(CStr(Me.colhI_Improvement.Tag), Me.colhI_Improvement.Text)
			Me.colhI_ActivityReq.Text = Language._Object.getCaption(CStr(Me.colhI_ActivityReq.Tag), Me.colhI_ActivityReq.Text)
			Me.colhI_Support.Text = Language._Object.getCaption(CStr(Me.colhI_Support.Tag), Me.colhI_Support.Text)
			Me.colhI_Course.Text = Language._Object.getCaption(CStr(Me.colhI_Course.Tag), Me.colhI_Course.Text)
			Me.colhI_Timeframe.Text = Language._Object.getCaption(CStr(Me.colhI_Timeframe.Tag), Me.colhI_Timeframe.Text)
			Me.colhI_OtherTraning.Text = Language._Object.getCaption(CStr(Me.colhI_OtherTraning.Tag), Me.colhI_OtherTraning.Text)
			Me.tabpAppriasers.Text = Language._Object.getCaption(Me.tabpAppriasers.Name, Me.tabpAppriasers.Text)
			Me.colhP_Development.Text = Language._Object.getCaption(CStr(Me.colhP_Development.Tag), Me.colhP_Development.Text)
			Me.colhP_ActivityReq.Text = Language._Object.getCaption(CStr(Me.colhP_ActivityReq.Tag), Me.colhP_ActivityReq.Text)
			Me.colhP_Support.Text = Language._Object.getCaption(CStr(Me.colhP_Support.Tag), Me.colhP_Support.Text)
			Me.colhP_Course.Text = Language._Object.getCaption(CStr(Me.colhP_Course.Tag), Me.colhP_Course.Text)
			Me.colhP_Timeframe.Text = Language._Object.getCaption(CStr(Me.colhP_Timeframe.Tag), Me.colhP_Timeframe.Text)
			Me.colhP_OtherTraning.Text = Language._Object.getCaption(CStr(Me.colhP_OtherTraning.Tag), Me.colhP_OtherTraning.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.lnkShow.Text = Language._Object.getCaption(Me.lnkShow.Name, Me.lnkShow.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "General Evaluation")
			Language.setMessage(mstrModuleName, 2, "BSC Evaluation")
			Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 4, "Group is mandatory information. Please select Group to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class