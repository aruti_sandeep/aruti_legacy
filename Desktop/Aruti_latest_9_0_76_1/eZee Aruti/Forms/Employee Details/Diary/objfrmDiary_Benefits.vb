﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Benefits

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Benefits_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            lvBenefitCoverage.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Benefits_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Dim objBenefit As New clsBenefitCoverage_tran
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objBenefit.GetList("BCoverage", mintEmployeeId)
            dsList = objBenefit.GetList(FinancialYear._Object._DatabaseName, _
                                        User._Object._Userunkid, _
                                        FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        ConfigParameter._Object._UserAccessModeSetting, True, _
                                        ConfigParameter._Object._IsIncludeInactiveEmp, "BCoverage", , _
                                        mintEmployeeId)
            'S.SANDEEP [04 JUN 2015] -- END

            StrSearching &= "AND isvoid = 0 "

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables("BCoverage"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("BCoverage"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            End If

            lvBenefitCoverage.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(dtRow.Item("BPlanName").ToString)
                lvItem.SubItems.Add(dtRow.Item("TranHead").ToString)
                lvItem.SubItems.Add(dtRow.Item("EmpId").ToString)
                lvItem.SubItems.Add(dtRow.Item("BGroupName").ToString)
                If IsDBNull(dtRow.Item("isvoid")) = False Then
                    If CBool(dtRow.Item("isvoid")) = True Then
                        lvItem.ForeColor = Color.Red
                    End If
                End If
                lvItem.Tag = dtRow.Item("BCoverageId")
                lvBenefitCoverage.Items.Add(lvItem)
                lvItem = Nothing
            Next

            If lvBenefitCoverage.Items.Count > 16 Then
                colhBenefitPlan.Width = 150 - 15
            Else
                colhBenefitPlan.Width = 150
            End If


            lvBenefitCoverage.GroupingColumn = colhEmployee
            lvBenefitCoverage.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elEmployeeBenefits.Text = Language._Object.getCaption(Me.elEmployeeBenefits.Name, Me.elEmployeeBenefits.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhBenefitPlan.Text = Language._Object.getCaption(CStr(Me.colhBenefitPlan.Tag), Me.colhBenefitPlan.Text)
			Me.colhTransactionHead.Text = Language._Object.getCaption(CStr(Me.colhTransactionHead.Tag), Me.colhTransactionHead.Text)
			Me.colhBenefitGroup.Text = Language._Object.getCaption(CStr(Me.colhBenefitGroup.Tag), Me.colhBenefitGroup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class