﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Benefits
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elEmployeeBenefits = New eZee.Common.eZeeLine
        Me.lvBenefitCoverage = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitPlan = New System.Windows.Forms.ColumnHeader
        Me.colhTransactionHead = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpID = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitGroup = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elEmployeeBenefits
        '
        Me.elEmployeeBenefits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEmployeeBenefits.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEmployeeBenefits.Location = New System.Drawing.Point(3, 12)
        Me.elEmployeeBenefits.Name = "elEmployeeBenefits"
        Me.elEmployeeBenefits.Size = New System.Drawing.Size(575, 17)
        Me.elEmployeeBenefits.TabIndex = 317
        Me.elEmployeeBenefits.Text = "Employee Benefits"
        Me.elEmployeeBenefits.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvBenefitCoverage
        '
        Me.lvBenefitCoverage.BackColorOnChecked = True
        Me.lvBenefitCoverage.ColumnHeaders = Nothing
        Me.lvBenefitCoverage.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhBenefitPlan, Me.colhTransactionHead, Me.objcolhEmpID, Me.colhBenefitGroup})
        Me.lvBenefitCoverage.CompulsoryColumns = ""
        Me.lvBenefitCoverage.FullRowSelect = True
        Me.lvBenefitCoverage.GridLines = True
        Me.lvBenefitCoverage.GroupingColumn = Nothing
        Me.lvBenefitCoverage.HideSelection = False
        Me.lvBenefitCoverage.Location = New System.Drawing.Point(6, 32)
        Me.lvBenefitCoverage.MinColumnWidth = 50
        Me.lvBenefitCoverage.MultiSelect = False
        Me.lvBenefitCoverage.Name = "lvBenefitCoverage"
        Me.lvBenefitCoverage.OptionalColumns = ""
        Me.lvBenefitCoverage.ShowMoreItem = False
        Me.lvBenefitCoverage.ShowSaveItem = False
        Me.lvBenefitCoverage.ShowSelectAll = True
        Me.lvBenefitCoverage.ShowSizeAllColumnsToFit = True
        Me.lvBenefitCoverage.Size = New System.Drawing.Size(572, 374)
        Me.lvBenefitCoverage.Sortable = True
        Me.lvBenefitCoverage.TabIndex = 318
        Me.lvBenefitCoverage.UseCompatibleStateImageBehavior = False
        Me.lvBenefitCoverage.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhBenefitPlan
        '
        Me.colhBenefitPlan.Tag = "colhBenefitPlan"
        Me.colhBenefitPlan.Text = "Benefit Plan"
        Me.colhBenefitPlan.Width = 255
        '
        'colhTransactionHead
        '
        Me.colhTransactionHead.Tag = "colhTransactionHead"
        Me.colhTransactionHead.Text = "Transaction Head"
        Me.colhTransactionHead.Width = 160
        '
        'objcolhEmpID
        '
        Me.objcolhEmpID.Tag = "objcolhEmpID"
        Me.objcolhEmpID.Text = ""
        Me.objcolhEmpID.Width = 0
        '
        'colhBenefitGroup
        '
        Me.colhBenefitGroup.Tag = "colhBenefitGroup"
        Me.colhBenefitGroup.Text = "Benefit Group"
        Me.colhBenefitGroup.Width = 150
        '
        'objfrmDiary_Benefits
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvBenefitCoverage)
        Me.Controls.Add(Me.elEmployeeBenefits)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Benefits"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elEmployeeBenefits As eZee.Common.eZeeLine
    Friend WithEvents lvBenefitCoverage As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitPlan As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTransactionHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmpID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitGroup As System.Windows.Forms.ColumnHeader
End Class
