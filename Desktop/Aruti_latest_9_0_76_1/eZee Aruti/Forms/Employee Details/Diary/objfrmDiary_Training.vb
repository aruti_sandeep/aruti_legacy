﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Training

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mobjTEnroll As clsTraining_Enrollment_Tran
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mobjTEnroll = New clsTraining_Enrollment_Tran
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Training_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Holiday_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim objTSchedule As New clsTraining_Scheduling
        Try
            'S.SANDEEP [ 24 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
            'dsList = mobjTEnroll.GetList("List")

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = mobjTEnroll.GetList("List", True)
            dsList = mobjTEnroll.GetList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, _
                                         "hremployee_master.employeeunkid = '" & mintEmployeeId & "'")
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 24 FEB 2012 ] -- END 

            If dsList.Tables(0).Rows.Count > 0 Then
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable

                For Each dtRow As DataRow In dtTable.Rows
                    Dim lvItem As New ListViewItem

                    lvItem.Text = dtRow.Item("Title").ToString
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EnrollDate").ToString).ToShortDateString)
                    objTSchedule._Trainingschedulingunkid = CInt(dtRow.Item("trainingschedulingunkid"))
                    lvItem.SubItems.Add(objTSchedule._Start_Date.Date.ToShortDateString)
                    lvItem.SubItems.Add(objTSchedule._End_Date.Date.ToShortDateString)
                    lvItem.SubItems.Add(dtRow.Item("STATUS").ToString)

                    lvEnrollList.Items.Add(lvItem)
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvEnrollList_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvEnrollList.ItemSelectionChanged
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEnrollList_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elTrininEnrolment.Text = Language._Object.getCaption(Me.elTrininEnrolment.Name, Me.elTrininEnrolment.Text)
			Me.colhTrainingtitle.Text = Language._Object.getCaption(CStr(Me.colhTrainingtitle.Tag), Me.colhTrainingtitle.Text)
			Me.colhEnrollDate.Text = Language._Object.getCaption(CStr(Me.colhEnrollDate.Tag), Me.colhEnrollDate.Text)
			Me.colhStDate.Text = Language._Object.getCaption(CStr(Me.colhStDate.Tag), Me.colhStDate.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class