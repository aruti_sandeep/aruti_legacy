﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeReferences
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeReferences))
        Me.pnlEmployeeReferences = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbEmployeeReferences = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddReferences = New eZee.Common.eZeeGradientButton
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lnReferenceInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtReferenceNo = New eZee.TextBox.AlphanumericTextBox
        Me.cboReference = New System.Windows.Forms.ComboBox
        Me.lblReferencesNo = New System.Windows.Forms.Label
        Me.lblReference = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlEmployeeReferences.SuspendLayout()
        Me.gbEmployeeReferences.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlEmployeeReferences
        '
        Me.pnlEmployeeReferences.Controls.Add(Me.eZeeHeader)
        Me.pnlEmployeeReferences.Controls.Add(Me.gbEmployeeReferences)
        Me.pnlEmployeeReferences.Controls.Add(Me.objFooter)
        Me.pnlEmployeeReferences.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmployeeReferences.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmployeeReferences.Name = "pnlEmployeeReferences"
        Me.pnlEmployeeReferences.Size = New System.Drawing.Size(542, 337)
        Me.pnlEmployeeReferences.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(542, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Employee References"
        '
        'gbEmployeeReferences
        '
        Me.gbEmployeeReferences.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeReferences.Checked = False
        Me.gbEmployeeReferences.CollapseAllExceptThis = False
        Me.gbEmployeeReferences.CollapsedHoverImage = Nothing
        Me.gbEmployeeReferences.CollapsedNormalImage = Nothing
        Me.gbEmployeeReferences.CollapsedPressedImage = Nothing
        Me.gbEmployeeReferences.CollapseOnLoad = False
        Me.gbEmployeeReferences.Controls.Add(Me.objbtnAddReferences)
        Me.gbEmployeeReferences.Controls.Add(Me.txtRemark)
        Me.gbEmployeeReferences.Controls.Add(Me.lblRemark)
        Me.gbEmployeeReferences.Controls.Add(Me.lnReferenceInfo)
        Me.gbEmployeeReferences.Controls.Add(Me.lnEmployeeName)
        Me.gbEmployeeReferences.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeReferences.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeReferences.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeReferences.Controls.Add(Me.txtReferenceNo)
        Me.gbEmployeeReferences.Controls.Add(Me.cboReference)
        Me.gbEmployeeReferences.Controls.Add(Me.lblReferencesNo)
        Me.gbEmployeeReferences.Controls.Add(Me.lblReference)
        Me.gbEmployeeReferences.ExpandedHoverImage = Nothing
        Me.gbEmployeeReferences.ExpandedNormalImage = Nothing
        Me.gbEmployeeReferences.ExpandedPressedImage = Nothing
        Me.gbEmployeeReferences.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeReferences.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeReferences.HeaderHeight = 25
        Me.gbEmployeeReferences.HeightOnCollapse = 0
        Me.gbEmployeeReferences.LeftTextSpace = 0
        Me.gbEmployeeReferences.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeReferences.Name = "gbEmployeeReferences"
        Me.gbEmployeeReferences.OpenHeight = 235
        Me.gbEmployeeReferences.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeReferences.ShowBorder = True
        Me.gbEmployeeReferences.ShowCheckBox = False
        Me.gbEmployeeReferences.ShowCollapseButton = False
        Me.gbEmployeeReferences.ShowDefaultBorderColor = True
        Me.gbEmployeeReferences.ShowDownButton = False
        Me.gbEmployeeReferences.ShowHeader = True
        Me.gbEmployeeReferences.Size = New System.Drawing.Size(519, 209)
        Me.gbEmployeeReferences.TabIndex = 0
        Me.gbEmployeeReferences.Temp = 0
        Me.gbEmployeeReferences.Text = "Employee References"
        Me.gbEmployeeReferences.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddReferences
        '
        Me.objbtnAddReferences.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReferences.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReferences.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReferences.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReferences.BorderSelected = False
        Me.objbtnAddReferences.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReferences.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReferences.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReferences.Location = New System.Drawing.Point(258, 110)
        Me.objbtnAddReferences.Name = "objbtnAddReferences"
        Me.objbtnAddReferences.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReferences.TabIndex = 7
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(109, 137)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(399, 64)
        Me.txtRemark.TabIndex = 11
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(29, 140)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(74, 15)
        Me.lblRemark.TabIndex = 10
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnReferenceInfo
        '
        Me.lnReferenceInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnReferenceInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnReferenceInfo.Location = New System.Drawing.Point(8, 86)
        Me.lnReferenceInfo.Name = "lnReferenceInfo"
        Me.lnReferenceInfo.Size = New System.Drawing.Size(244, 21)
        Me.lnReferenceInfo.TabIndex = 4
        Me.lnReferenceInfo.Text = "References Info"
        Me.lnReferenceInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(244, 21)
        Me.lnEmployeeName.TabIndex = 0
        Me.lnEmployeeName.Text = "Employee Info"
        Me.lnEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(487, 62)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(109, 62)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(372, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(29, 65)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.Flags = 0
        Me.txtReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceNo.Location = New System.Drawing.Point(365, 110)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.Size = New System.Drawing.Size(143, 21)
        Me.txtReferenceNo.TabIndex = 9
        '
        'cboReference
        '
        Me.cboReference.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReference.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReference.FormattingEnabled = True
        Me.cboReference.Location = New System.Drawing.Point(109, 110)
        Me.cboReference.Name = "cboReference"
        Me.cboReference.Size = New System.Drawing.Size(143, 21)
        Me.cboReference.TabIndex = 6
        '
        'lblReferencesNo
        '
        Me.lblReferencesNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferencesNo.Location = New System.Drawing.Point(285, 113)
        Me.lblReferencesNo.Name = "lblReferencesNo"
        Me.lblReferencesNo.Size = New System.Drawing.Size(74, 15)
        Me.lblReferencesNo.TabIndex = 8
        Me.lblReferencesNo.Text = "Reference No"
        Me.lblReferencesNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReference
        '
        Me.lblReference.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReference.Location = New System.Drawing.Point(29, 113)
        Me.lblReference.Name = "lblReference"
        Me.lblReference.Size = New System.Drawing.Size(74, 15)
        Me.lblReference.TabIndex = 5
        Me.lblReference.Text = "Reference"
        Me.lblReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 282)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(542, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(330, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 120
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(433, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEmployeeReferences
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 337)
        Me.Controls.Add(Me.pnlEmployeeReferences)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeReferences"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee References"
        Me.pnlEmployeeReferences.ResumeLayout(False)
        Me.gbEmployeeReferences.ResumeLayout(False)
        Me.gbEmployeeReferences.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlEmployeeReferences As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbEmployeeReferences As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblReferencesNo As System.Windows.Forms.Label
    Friend WithEvents lblReference As System.Windows.Forms.Label
    Friend WithEvents txtReferenceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboReference As System.Windows.Forms.ComboBox
    Friend WithEvents lnReferenceInfo As eZee.Common.eZeeLine
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnAddReferences As eZee.Common.eZeeGradientButton
End Class
