﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmDependantStatus

#Region " Private Varaibles "
    Private objDependants_Benefice As clsDependants_Beneficiary_tran
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private ReadOnly mstrModuleName As String = "frmDependantStatus"
    Private mstrAdvanceFilter As String = ""
    Private mdtView As DataView
    Private mstrSearchText As String = ""

    'Private mintTransactionId As Integer = 0
    'Private objADependantTran As clsDependant_beneficiaries_approval_tran

    Private Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Private DependantApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
    Private isEmployeeApprove As Boolean = False
    'Private intParentRowIndex As Integer = -1
    'Sohail (09 Jul 2019) -- Start
    'NMB Enhancement - 76.1 - Employee and Dependant should automatically selected on Dependant status screen if is selected on Dependant list screen.
    Private mintDependantUnkid As Integer = 0
    'Sohail (09 Jul 2019) -- End
#End Region

#Region " Properties "
    'Public Property _IsBeneficiaries() As Boolean
    '    Get
    '        Return mblnIsBeneficiaries
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsBeneficiaries = value
    '    End Set
    'End Property

    Private mintEmployeeUnkid As Integer = -1
    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

#End Region

    'Sohail (09 Jul 2019) -- Start
    'NMB Enhancement - 76.1 - Employee and Dependant should automatically selected on Dependant status screen if is selected on Dependant list screen.
#Region " Display Dialog "

    Public Function displayDialog(Optional ByVal intDependantId As Integer = 0) As Boolean
        Try
            mintDependantUnkid = intDependantId
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region
    'Sohail (09 Jul 2019) -- End

#Region " Private Functions "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objCountry As New clsMasterData
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMembership As New clsmembership_master
        Dim objBenefit As New clsbenefitplan_master
        Dim objMaster As New clsMasterData
        Try


            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            'If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
            '    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantStatus))) Then
            '        mblnOnlyApproved = False
            '        mblnAddApprovalCondition = False
            '    End If
            'End If


            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                          mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboEmployee)
            End With

            

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            With cboDBRelation
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Relation")
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.DEPENDANT_ACTIVE_INACTIVE_REASON, True, "DepReason")
            With cboReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("DepReason")
                .SelectedValue = 0
            End With

            dsCombos = objMaster.getGenderList("List", True)
            With cboGender
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objMaster.getComboListTranHeadActiveInActive("List", True)
            With cboStatus
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                'Sohail (03 Jul 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'If .Items.Count > 0 Then .SelectedValue = 0
                If .Items.Count > 0 Then .SelectedValue = 1
                'Sohail (03 Jul 2019) -- End
                .EndUpdate()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim StrApproverSearching As String = String.Empty

        'Dim dtTable As DataTable

        Try

            btnSetActive.Visible = False
            btnSetInactive.Visible = False
            Dim dtAsOnDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            If CInt(cboDependant.SelectedValue) > 0 Then
                cboStatus.SelectedValue = 0
                dtAsOnDate = Nothing
                'S.SANDEEP |03-SEP-2022| -- START
                'ISSUE/ENHANCEMENT : Sprint_2022-13
                objpnlStatus.Visible = False
                objpnlDateReason.Visible = False
            Else
                If objpnlStatus.Visible = False Then objpnlStatus.Visible = True
                If objpnlDateReason.Visible = False Then objpnlDateReason.Visible = True
                'S.SANDEEP |03-SEP-2022| -- END
            End If
            Select Case CInt(cboStatus.SelectedValue)
                Case enEDHeadApprovalStatus.Approved
                    btnSetInactive.Visible = True
                Case 2
                    btnSetActive.Visible = True
            End Select

            If User._Object.Privilege._AllowToViewDependentStatus = True Then

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboDBRelation.SelectedValue) > 0 Then
                    StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboDBRelation.SelectedValue) & " "
                End If

                If txtDBPassportNo.Text.Trim <> "" Then
                    StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.identify_no,'') LIKE '%" & txtDBPassportNo.Text & "%'" & " "
                End If

                If CInt(cboGender.SelectedValue) > 0 Then
                    StrSearching &= "AND hrdependants_beneficiaries_tran.gender = '" & CInt(cboGender.SelectedValue) & "' "
                End If

                If dtBirthdate.Checked = True Then
                    StrSearching &= "AND ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') = '" & eZeeDate.convertDate(dtBirthdate.Value) & "' "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If



                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If


                dsList = objDependants_Benefice.GetList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        dtAsOnDate, _
                                                        dtAsOnDate, _
                                                        ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                                        ConfigParameter._Object._IsIncludeInactiveEmp, "List", , CInt(cboEmployee.SelectedValue), StrSearching, , mblnAddApprovalCondition, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboStatus.SelectedValue), True, CInt(cboDependant.SelectedValue))

                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsList.Tables(0).Columns.Add(dcol)

                'If DependantApprovalFlowVal Is Nothing AndAlso mintTransactionId <= 0 Then
                '    Dim dsPending As New DataSet
                '    dsPending = objADependantTran.GetList(FinancialYear._Object._DatabaseName, _
                '                              User._Object._Userunkid, _
                '                              FinancialYear._Object._YearUnkid, _
                '                              Company._Object._Companyunkid, _
                '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                              ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                '                              ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , , StrApproverSearching, , mblnAddApprovalCondition)

                '    If dsPending.Tables(0).Rows.Count > 0 Then
                '        For Each row As DataRow In dsPending.Tables(0).Rows
                '            dsList.Tables(0).ImportRow(row)
                '        Next
                '    End If
                'End If

                'dtTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                mdtView = dsList.Tables(0).DefaultView

                objdgcolhicheck.DataPropertyName = "IsChecked"
                objColDelete.DataPropertyName = ""
                objColDelete.Visible = False
                If CInt(cboDependant.SelectedValue) > 0 Then
                    If dsList.Tables(0).Select("IsGrp = 0 AND ROWNO = 2").Length > 0 Then
                        If User._Object.Privilege._AllowToDeleteDependentStatus = True Then objColDelete.Visible = True
                    End If
                End If
                dgcolhEmployee.DataPropertyName = "EmpName"
                dgcolhDependant.DataPropertyName = "DpndtBefName"
                dgcolhRelation.DataPropertyName = "Relation"
                dgcolhGender.DataPropertyName = "Gender"
                dgcolhBirthDate.DataPropertyName = "dtbirthdate"
                dgcolhBirthDate.DefaultCellStyle.Format = "dd-MMM-yyyy"
                dgcolhEffectiveDate.DataPropertyName = "dteffective_date"
                dgcolhEffectiveDate.DefaultCellStyle.Format = "dd-MMM-yyyy"
                dgcolhActive.DataPropertyName = "isactive"
                dgcolhReason.DataPropertyName = "Reason"
                objdgcolhIsGroup.DataPropertyName = "IsGrp"
                objdgcolhDependandstatustranunkid.DataPropertyName = "dpndtbeneficestatustranunkid"
                objdgcolhEmpid.DataPropertyName = "EmpId"
                objdgcolhDpndtbeneficetranunkid.DataPropertyName = "DpndtTranId"
                objdgcolhROWNO.DataPropertyName = "ROWNO"
                dgcolhEmployee.Visible = False

                With dgvDependantStatus
                    .AutoGenerateColumns = False

                    .DataSource = mdtView

                End With

                'Call SetGridColor()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            'Sohail (29 Jan 2020) -- Start
            'NMB Issue # : Object refrence error on search click if view dependant status priviledge not assigned.
            'objbtnSearch.ShowResult(mdtView.ToTable.Select("IsGrp = 0").Length.ToString)
            If mdtView IsNot Nothing Then
                Call objbtnSearch.ShowResult(mdtView.ToTable.Select("IsGrp = 0").Length.ToString)
            Else
                Call objbtnSearch.ShowResult("0")
            End If
            'Sohail (29 Jan 2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnSetActive.Enabled = User._Object.Privilege._AllowToSetDependentActive
            btnSetInactive.Enabled = User._Object.Privilege._AllowToSetDependentInactive

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub SetGridColor()
        Try
            'Dim dr = From p As DataGridViewRow In dgvDependantStatus.Rows.Cast(Of DataGridViewRow)() Where CBool(p.Cells(objdgcolhIsGroup.Index).Value) = True Select p
            'dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

            If DependantApprovalFlowVal Is Nothing Then
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If

                Dim objDBApproval As New clsDependant_beneficiaries_approval_tran
                Dim dsPending As DataSet = objDBApproval.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", False, , , , mblnAddApprovalCondition)
                Dim arrPendingIDs() As String = (From p In dsPending.Tables(0) Select (p.Item("DpndtTranId").ToString)).ToArray
                Dim dr1 As List(Of DataGridViewRow) = (From p As DataGridViewRow In dgvDependantStatus.Rows.Cast(Of DataGridViewRow)() Where (arrPendingIDs.Contains(p.Cells(objdgcolhDpndtbeneficetranunkid.Index).Value.ToString) = True) Select p).ToList
                dr1.ForEach(Function(x) SetRowStyle(x, True))

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal isHeader As Boolean) As Boolean
        Try
            If isHeader Then
                Dim dgvcsHeader As New DataGridViewCellStyle
                dgvcsHeader.ForeColor = Color.Black
                dgvcsHeader.SelectionForeColor = Color.White
                dgvcsHeader.BackColor = Color.PowderBlue
                dgvcsHeader.Font = New Font(Me.Font, FontStyle.Bold)
                xRow.DefaultCellStyle = dgvcsHeader
            Else
                xRow.DefaultCellStyle.ForeColor = Color.Red
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
        Return True
    End Function

    Private Function IsValidated(ByVal btn As Button) As Boolean
        Try
            If mdtView Is Nothing Then Exit Try

            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set effective date."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            ElseIf eZeeDate.convertDate(dtpEffectiveDate.Value) > eZeeDate.convertDate(DateAndTime.Today.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Effective date should not be greater than current date."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            ElseIf CInt(cboReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Reason."), enMsgBoxStyle.Information)
                cboReason.Focus()
                Return False
            ElseIf mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please tick atleast one dependant."), enMsgBoxStyle.Information)
                dgvDependantStatus.Focus()
                Return False
            Else
                '***
                Dim dtMax As String = (From p In mdtView.ToTable Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("effective_date").ToString)).Max
                If eZeeDate.convertDate(dtpEffectiveDate.Value) <= dtMax Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Effective date should be greater than last effective date") & " " & eZeeDate.convertDate(dtMax).ToString("dd-MMM-yyyy"), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                End If

                Dim arrIDs() As String = (From p In mdtView.ToTable Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("DpndtTranId").ToString)).ToArray
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True

                If DependantApprovalFlowVal Is Nothing Then

                    '***
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                        If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                            mblnOnlyApproved = False
                            mblnAddApprovalCondition = False
                        End If
                    End If
                    Dim objDBApproval As New clsDependant_beneficiaries_approval_tran
                    Dim dsPending As DataSet = objDBApproval.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", False, , , , mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then

                        Dim intCount As Integer = (From p In dsPending.Tables(0) Where (arrIDs.Contains(p.Item("DpndtTranId").ToString) = True) Select (p)).Count

                        If intCount > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot do any operation. Reason: Some of the transactions are in approval process.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 7, "Please Approve / Reject transction."), enMsgBoxStyle.Information)
                            dgvDependantStatus.Focus()
                            Return False
                        End If
                    End If
                End If


                '***
                Dim dsList As DataSet
                dsList = objDependants_Benefice.GetList(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                                       ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , "hrdependants_beneficiaries_tran.dpndtbeneficetranunkid IN (" & String.Join(",", arrIDs) & ")", , mblnAddApprovalCondition, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enEDHeadApprovalStatus.All, False)

                If btn.Name = btnSetInactive.Name Then
                    If dsList.Tables(0).Select("isactive = 0").Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Some of the dependants are already inactive. Please refresh the dependant list and the try again."), enMsgBoxStyle.Information)
                        dgvDependantStatus.Focus()
                        Return False
                    End If
                ElseIf btn.Name = btnSetActive.Name Then
                    If dsList.Tables(0).Select("isactive = 1").Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Some of the dependants are already active. Please refresh the dependant list and the try again."), enMsgBoxStyle.Information)
                        dgvDependantStatus.Focus()
                        Return False
                    End If
                End If

            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidated", mstrModuleName)
        End Try
    End Function

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 10, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRegularFont", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmDependantStatus_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete AndAlso dgvDependantStatus.SelectedRows.Count > 0 AndAlso dgvDependantStatus.Focused = True Then
            'Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmDependantStatus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmDependantStatus_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDependants_Benefice = Nothing
    End Sub

    Private Sub frmDependantStatus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDependants_Benefice = New clsDependants_Beneficiary_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call FillCombo()


            dtBirthdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtBirthdate.Checked = False

            'Sohail (09 Jul 2019) -- Start
            'NMB Enhancement - 76.1 - Employee and Dependant should automatically selected on Dependant status screen if is selected on Dependant list screen.
            If mintDependantUnkid > 0 Then
                objDependants_Benefice._Dpndtbeneficetranunkid = mintDependantUnkid
                mintEmployeeUnkid = objDependants_Benefice._Employeeunkid
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboDependant.SelectedValue = mintDependantUnkid
            End If
            'Sohail (09 Jul 2019) -- End
            'If mintEmployeeUnkid > 0 Then
            '    cboEmployee.SelectedValue = mintEmployeeUnkid
            '    cboEmployee.Enabled = False
            '    Call FillList()
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDependantStatus_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            'Call SetMessages()

            clsDependants_Beneficiary_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsDependants_Beneficiary_tran"
            objfrm.displayDialog(Me)

            'Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress, cboDependant.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    ElseIf cbo.Name = cboDependant.Name Then
                        '.CodeMember = "code"
                    Else

                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboDependant.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Dim dsCombos As DataSet
        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If

            If cbo.Name = cboEmployee.Name Then
                dsCombos = objDependants_Benefice.GetListForCombo(CInt(cboEmployee.SelectedValue), True, Nothing, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False)
                With cboDependant
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0
                    Call SetDefaultSearchText(cboDependant)
                End With
                'S.SANDEEP |03-SEP-2022| -- START
                'ISSUE/ENHANCEMENT : Sprint_2022-13
            ElseIf cbo.Name = cboDependant.Name Then
                If CInt(cboDependant.SelectedValue) > 0 Then
                    cboStatus.SelectedValue = 0
                    objpnlStatus.Visible = False
                    objpnlDateReason.Visible = False
                Else
                    If objpnlStatus.Visible = False Then objpnlStatus.Visible = True
                    If objpnlDateReason.Visible = False Then objpnlDateReason.Visible = True
                End If
                'S.SANDEEP |03-SEP-2022| -- END
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus, cboDependant.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave, cboDependant.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSetActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetActive.Click
        Dim objDBStatus As New clsDependant_Benefice_Status_tran
        Dim objADependant As New clsDependant_beneficiaries_approval_tran
        Dim blnFlag As Boolean = False
        Try
            If IsValidated(btnSetActive) = False Then Exit Try

            If DependantApprovalFlowVal Is Nothing Then

                Dim objApprovalData As New clsEmployeeDataApproval
                Dim dtEmp As DataTable = New DataView(mdtView.ToTable, "IsChecked = 1 AND IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable(True, "EmpId", "EmpName")
                dtEmp.Columns.Add("Reason", System.Type.GetType("System.String")).DefaultValue = ""
                dtEmp.Columns.Add("IsValid", System.Type.GetType("System.Boolean")).DefaultValue = True
                For Each dtRow As DataRow In dtEmp.Rows
                    If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, FinancialYear._Object._DatabaseName, _
                                                    ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                    FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                    User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(dtRow.Item("EmpId")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                        dtRow.Item("Reason") = objApprovalData._Message
                        dtRow.Item("IsValid") = False
                    End If
                Next
                dtEmp.AcceptChanges()

                Dim strInvalidEmpIDs As String = "-999"
                If dtEmp.Select("IsValid = 0").Length > 0 Then
                    strInvalidEmpIDs = String.Join(",", (From p In dtEmp Where (CBool(p.Item("IsValid")) = False) Select (p.Item("EmpId").ToString)).ToArray)

                    dtEmp.Columns.Remove("EmpId")
                    dtEmp.Columns.Remove("IsValid")

                    Dim frmValid As New frmCommonValidationList
                    If frmValid.displayDialog(True, Language.getMessage(mstrModuleName, 11, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?"), dtEmp) = False Then
                        Exit Try
                    End If
                End If

                Dim dtApproved As DataTable = Nothing
                Dim dtNotApproved As DataTable = Nothing
                Dim dr() As DataRow = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 AND isapproved = 1 AND EmpId NOT IN (" & strInvalidEmpIDs & ")")
                If dr.Length > 0 Then
                    dtApproved = dr.CopyToDataTable
                End If
                dr = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 AND isapproved = 0 AND EmpId NOT IN (" & strInvalidEmpIDs & ")")
                If dr.Length > 0 Then
                    dtNotApproved = dr.CopyToDataTable
                End If

                If dtApproved IsNot Nothing AndAlso dtApproved.Rows.Count > 0 Then

                    objADependant._Audittype = enAuditType.ADD
                    objADependant._Audituserunkid = User._Object._Userunkid
                    objADependant._Loginemployeeunkid = 0
                    objADependant._Isvoid = False
                    objADependant._Isactive = True
                    objADependant._ReasonUnkid = CInt(cboReason.SelectedValue)
                    objADependant._Tranguid = Guid.NewGuid.ToString()
                    objADependant._Transactiondate = Now
                    objADependant._Approvalremark = ""
                    objADependant._Isweb = False
                    objADependant._Isfinal = False
                    objADependant._Effective_date = dtpEffectiveDate.Value
                    objADependant._Ip = getIP()
                    objADependant._Host = getHostName()
                    objADependant._Form_Name = mstrModuleName
                    objADependant._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                    objADependant._Audittype = enAuditType.EDIT
                    objADependant._Isprocessed = False
                    objADependant._CompanyId = Company._Object._Companyunkid
                    objADependant._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                    objADependant._Operationtypeid = clsEmployeeDataApproval.enOperationType.EDITED
                    objADependant._Newattacheddocumnetid = ""
                    objADependant._Deletedattachdocumnetid = ""
                    objADependant._ImagePath = ""
                    objADependant._IsFromStatus = True

                    blnFlag = objADependant.InsertAll(dtTable:=dtApproved, blnGetLatestStatus:=False)

                    If blnFlag = False And objDBStatus._Message <> "" Then
                        eZeeMsgBox.Show(objDBStatus._Message, enMsgBoxStyle.Information)
                    Else

                        Dim arrIDs() As String = (From p In dtApproved Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("DpndtTranId").ToString)).ToArray
                        Dim arrEmpIDs() As String = (From p In dtApproved Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("EmpId").ToString)).ToArray

                        For Each dtRow As DataRow In dtApproved.Rows
                            dtRow.Item("dteffective_date") = dtpEffectiveDate.Value
                            dtRow.Item("effective_date") = eZeeDate.convertDate(dtpEffectiveDate.Value)
                            dtRow.Item("isactive") = True
                            dtRow.Item("reasonunkid") = CInt(cboReason.SelectedValue)
                            dtRow.Item("reason") = cboReason.Text
                        Next
                        dtApproved.AcceptChanges()

                        'Dim objApprovalData As New clsEmployeeDataApproval
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                            ConfigParameter._Object._UserAccessModeSetting, _
                                                            Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                            CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                            enScreenName.frmDependantStatus, ConfigParameter._Object._EmployeeAsOnDate, _
                                                            User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                            User._Object._Username, clsEmployeeDataApproval.enOperationType.EDITED, 0, String.Join(",", arrEmpIDs), "", Nothing, _
                                                            "dpndtbeneficetranunkid IN (" & String.Join(",", arrIDs) & ") ", dtApproved)

                    End If
                End If

                If dtNotApproved IsNot Nothing AndAlso dtNotApproved.Rows.Count > 0 Then

                    With objDBStatus
                        ._Effective_Date = dtpEffectiveDate.Value
                        ._Isactive = True
                        ._Reasonunkid = CInt(cboReason.SelectedValue)
                        ._Userunkid = User._Object._Userunkid
                        ._Loginemployeeunkid = 0
                        ._Isweb = False
                        ._Isvoid = False
                        ._FormName = mstrModuleName
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        ._xDataOp = Nothing

                        blnFlag = .InsertAll(dtNotApproved)

                    End With

                    If blnFlag = False And objDBStatus._Message <> "" Then
                        eZeeMsgBox.Show(objDBStatus._Message, enMsgBoxStyle.Information)
                    End If

                End If
            Else

                With objDBStatus
                    ._Effective_Date = dtpEffectiveDate.Value
                    ._Isactive = True
                    ._Reasonunkid = CInt(cboReason.SelectedValue)
                    ._Userunkid = User._Object._Userunkid
                    ._Loginemployeeunkid = 0
                    ._Isweb = False
                    ._Isvoid = False
                    ._FormName = mstrModuleName
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._xDataOp = Nothing

                    Dim dtTable As DataTable = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").CopyToDataTable

                    blnFlag = .InsertAll(dtTable)

                End With

                If blnFlag = False And objDBStatus._Message <> "" Then
                    eZeeMsgBox.Show(objDBStatus._Message, enMsgBoxStyle.Information)
                End If

            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDBStatus = Nothing
                    objDBStatus = New clsDependant_Benefice_Status_tran
                    Call FillList()

                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSetActive_Click", mstrModuleName)
        Finally
            objDBStatus = Nothing
            objADependant = Nothing
        End Try
    End Sub

    Private Sub btnSetInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetInactive.Click
        Dim objDBStatus As New clsDependant_Benefice_Status_tran
        Dim objADependant As New clsDependant_beneficiaries_approval_tran
        Dim blnFlag As Boolean = False
        Try
            If IsValidated(btnSetInactive) = False Then Exit Try

            If DependantApprovalFlowVal Is Nothing Then

                Dim objApprovalData As New clsEmployeeDataApproval
                Dim dtEmp As DataTable = New DataView(mdtView.ToTable, "IsChecked = 1 AND IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable(True, "EmpId", "EmpName")
                dtEmp.Columns.Add("Reason", System.Type.GetType("System.String")).DefaultValue = ""
                dtEmp.Columns.Add("IsValid", System.Type.GetType("System.Boolean")).DefaultValue = True
                For Each dtRow As DataRow In dtEmp.Rows
                    If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, FinancialYear._Object._DatabaseName, _
                                                    ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                    FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                    User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(dtRow.Item("EmpId")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                        
                        dtRow.Item("Reason") = objApprovalData._Message
                        dtRow.Item("IsValid") = False
                    End If
                Next
                dtEmp.AcceptChanges()

                Dim strInvalidEmpIDs As String = "-999"
                If dtEmp.Select("IsValid = 0").Length > 0 Then
                    strInvalidEmpIDs = String.Join(",", (From p In dtEmp Where (CBool(p.Item("IsValid")) = False) Select (p.Item("EmpId").ToString)).ToArray)

                    dtEmp.Columns.Remove("EmpId")
                    dtEmp.Columns.Remove("IsValid")

                    Dim frmValid As New frmCommonValidationList
                    If frmValid.displayDialog(True, Language.getMessage(mstrModuleName, 11, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?"), dtEmp) = False Then
                        Exit Try
                    End If
                End If

                Dim dtApproved As DataTable = Nothing
                Dim dtNotApproved As DataTable = Nothing
                Dim dr() As DataRow = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 AND isapproved = 1 AND EmpId NOT IN (" & strInvalidEmpIDs & ")")
                If dr.Length > 0 Then
                    dtApproved = dr.CopyToDataTable
                End If
                dr = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 AND isapproved = 0 AND EmpId NOT IN (" & strInvalidEmpIDs & ")")
                If dr.Length > 0 Then
                    dtNotApproved = dr.CopyToDataTable
                End If

                If dtApproved IsNot Nothing AndAlso dtApproved.Rows.Count > 0 Then

                    objADependant._Audittype = enAuditType.ADD
                    objADependant._Audituserunkid = User._Object._Userunkid
                    objADependant._Loginemployeeunkid = 0
                    objADependant._Isvoid = False
                    objADependant._Isactive = False
                    objADependant._ReasonUnkid = CInt(cboReason.SelectedValue)
                    objADependant._Tranguid = Guid.NewGuid.ToString()
                    objADependant._Transactiondate = Now
                    objADependant._Approvalremark = ""
                    objADependant._Isweb = False
                    objADependant._Isfinal = False
                    objADependant._Effective_date = dtpEffectiveDate.Value
                    objADependant._Ip = getIP()
                    objADependant._Host = getHostName()
                    objADependant._Form_Name = mstrModuleName
                    objADependant._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                    objADependant._Audittype = enAuditType.EDIT
                    objADependant._Isprocessed = False
                    objADependant._CompanyId = Company._Object._Companyunkid
                    objADependant._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                    objADependant._Operationtypeid = clsEmployeeDataApproval.enOperationType.EDITED
                    objADependant._Newattacheddocumnetid = ""
                    objADependant._Deletedattachdocumnetid = ""
                    objADependant._ImagePath = ""
                    objADependant._IsFromStatus = True

                    blnFlag = objADependant.InsertAll(dtTable:=dtApproved, blnGetLatestStatus:=False)

                    If blnFlag = False And objDBStatus._Message <> "" Then
                        eZeeMsgBox.Show(objDBStatus._Message, enMsgBoxStyle.Information)
                    Else

                        Dim arrIDs() As String = (From p In dtApproved Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("DpndtTranId").ToString)).ToArray
                        Dim arrEmpIDs() As String = (From p In dtApproved Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("EmpId").ToString)).ToArray

                        For Each dtRow As DataRow In dtApproved.Rows
                            dtRow.Item("dteffective_date") = dtpEffectiveDate.Value
                            dtRow.Item("effective_date") = eZeeDate.convertDate(dtpEffectiveDate.Value)
                            dtRow.Item("isactive") = False
                            dtRow.Item("reasonunkid") = CInt(cboReason.SelectedValue)
                            dtRow.Item("reason") = cboReason.Text
                        Next
                        dtApproved.AcceptChanges()

                        'Dim objApprovalData As New clsEmployeeDataApproval
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                            ConfigParameter._Object._UserAccessModeSetting, _
                                                            Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                            CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                            enScreenName.frmDependantStatus, ConfigParameter._Object._EmployeeAsOnDate, _
                                                            User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                            User._Object._Username, clsEmployeeDataApproval.enOperationType.EDITED, 0, String.Join(",", arrEmpIDs), "", Nothing, _
                                                            "dpndtbeneficetranunkid IN (" & String.Join(",", arrIDs) & ") ", dtApproved)

                    End If
                End If

                If dtNotApproved IsNot Nothing AndAlso dtNotApproved.Rows.Count > 0 Then

                    With objDBStatus
                        ._Effective_Date = dtpEffectiveDate.Value
                        ._Isactive = False
                        ._Reasonunkid = CInt(cboReason.SelectedValue)
                        ._Userunkid = User._Object._Userunkid
                        ._Loginemployeeunkid = 0
                        ._Isweb = False
                        ._Isvoid = False
                        ._FormName = mstrModuleName
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        ._xDataOp = Nothing

                        blnFlag = .InsertAll(dtNotApproved)

                    End With

                    If blnFlag = False And objDBStatus._Message <> "" Then
                        eZeeMsgBox.Show(objDBStatus._Message, enMsgBoxStyle.Information)
                    End If

                End If
            Else

                With objDBStatus
                    ._Effective_Date = dtpEffectiveDate.Value
                    ._Isactive = False
                    ._Reasonunkid = CInt(cboReason.SelectedValue)
                    ._Userunkid = User._Object._Userunkid
                    ._Loginemployeeunkid = 0
                    ._Isweb = False
                    ._Isvoid = False
                    ._FormName = mstrModuleName
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._xDataOp = Nothing

                    Dim dtTable As DataTable = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").CopyToDataTable

                    blnFlag = .InsertAll(dtTable)

                End With

                If blnFlag = False And objDBStatus._Message <> "" Then
                    eZeeMsgBox.Show(objDBStatus._Message, enMsgBoxStyle.Information)
                End If

            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDBStatus = Nothing
                    objDBStatus = New clsDependant_Benefice_Status_tran
                    Call FillList()

                Else
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSetInactive_Click", mstrModuleName)
        Finally
            objDBStatus = Nothing
            objADependant = Nothing
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            Else
                cboEmployee.SelectedValue = 0
            End If
            cboDependant.SelectedValue = 0
            cboDBRelation.SelectedValue = 0
            cboGender.SelectedValue = 0
            txtDBPassportNo.Text = ""
            'Sohail (29 Jan 2020) -- Start
            'NMB Enhancement # Advance filter on dependant status screen: .
            mstrAdvanceFilter = ""
            'Sohail (29 Jan 2020) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.DEPENDANT_ACTIVE_INACTIVE_REASON, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(CType(clsCommon_Master.enCommonMaster.DEPENDANT_ACTIVE_INACTIVE_REASON, clsCommon_Master.enCommonMaster), True, "List")
                With cboReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objSearchReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchReason.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            xCbo = cboReason
            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchReason", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Sohail (29 Jan 2020) -- Start
    'NMB Enhancement # Advance filter on dependant status screen: .
    Private Sub lnkAllocations_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocations.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocations_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2020) -- End

    'Private Sub btnApprovalinfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
    '    Try
    '        Dim empid As Integer = 0
    '        If dgvDependantStatus.SelectedRows.Count > 0 Then
    '            empid = CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text)
    '        End If
    '        Dim frm As New frmViewEmployeeDataApproval
    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If
    '        frm.displayDialog(User._Object._Userunkid, 0, enUserPriviledge.AllowToApproveRejectEmployeeDependants, enScreenName.frmDependantsAndBeneficiariesList, clsEmployeeDataApproval.enOperationType.NONE, "EM.employeeunkid = " & empid, False)
    '        If frm IsNot Nothing Then frm.Dispose()
    '        Exit Sub
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnApprovalinfo_Click", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            If mdtView Is Nothing Then Exit Sub
            RemoveHandler dgvDependantStatus.CellContentClick, AddressOf dgvDependantStatus_CellContentClick
            For Each dr As DataRowView In mdtView
                dr.Item(objdgcolhicheck.DataPropertyName) = CBool(objchkAll.CheckState)
            Next
            dgvDependantStatus.Refresh()
            AddHandler dgvDependantStatus.CellContentClick, AddressOf dgvDependantStatus_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    Private Sub dgvDependantStatus_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDependantStatus.CellClick
        Try

            If e.RowIndex < 0 Then Exit Sub

            'If e.ColumnIndex = objdgcolhDownloadDocument.Index AndAlso dgvData.Rows(e.RowIndex).Cells(objdgcolhDownloadDocument.Index).Value Is imgView Then
            '    Dim Location As Point = dgvData.GetCellDisplayRectangle(objdgcolhDownloadDocument.Index, e.RowIndex, False).Location
            '    cmDownloadAttachment.Show(dgvData, New Point(Location.X + 10, Location.Y + 10))
            '    rowIndex = e.RowIndex
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDependantStatus_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvDependantStatus_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDependantStatus.CellContentClick, dgvDependantStatus.CellContentDoubleClick
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            'Sohail (03 Jul 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'isEmployeeApprove = CBool(CType(dgvDependantStatus.CurrentRow.DataBoundItem, DataRowView).Item("isapproved"))
            'Sohail (03 Jul 2019) -- End

            If e.ColumnIndex = objdgcolhicheck.Index Then
                If Me.dgvDependantStatus.IsCurrentCellDirty Then
                    Me.dgvDependantStatus.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = Nothing
                If CBool(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    drRow = mdtView.Table.Select(objdgcolhEmpid.DataPropertyName & " = '" & dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhEmpid.Index).Value.ToString() & "'", "")
                    If drRow.Length > 0 Then
                        For index As Integer = 0 To drRow.Length - 1
                            drRow(index)("isChecked") = CBool(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhicheck.Index).Value)
                        Next
                    End If
                End If
                'mdtView.Table.AcceptChanges()
                drRow = mdtView.ToTable.Select("isChecked = true", "")

                If drRow.Length > 0 Then
                    If mdtView.ToTable.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If

            ElseIf e.ColumnIndex = objColDelete.Index Then

                If CBool(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then Exit Try
                'Sohail (03 Jul 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                isEmployeeApprove = CBool(CType(dgvDependantStatus.CurrentRow.DataBoundItem, DataRowView).Item("isapproved"))
                'Sohail (03 Jul 2019) -- End

                If mdtView.Table.Select("IsGrp = 0 AND DpndtTranId = " & CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDpndtbeneficetranunkid.Index).Value) & " ").Length <= 1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot delete default transaction."), enMsgBoxStyle.Information)
                    dgvDependantStatus.Focus()
                    Exit Try
                ElseIf CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhROWNO.Index).Value) <> 1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You can delete last tranasction only."), enMsgBoxStyle.Information)
                    dgvDependantStatus.Focus()
                    Exit Try
                End If

                Dim arrIDs() As String = {CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDpndtbeneficetranunkid.Index).Value).ToString}
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                Dim objApprovalData As New clsEmployeeDataApproval
                If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                    If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                        If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                            mblnOnlyApproved = False
                            mblnAddApprovalCondition = False
                        End If
                    End If
                    Dim objDBApproval As New clsDependant_beneficiaries_approval_tran
                    Dim dsPending As DataSet = objDBApproval.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", False, , , , mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then

                        Dim intCount As Integer = (From p In dsPending.Tables(0) Where (arrIDs.Contains(p.Item("DpndtTranId").ToString) = True) Select (p)).Count

                        If intCount > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You cannot do any operation. Reason: Some of the transactions are in approval process.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 15, "Please Approve / Reject transction."), enMsgBoxStyle.Information)
                            dgvDependantStatus.Focus()
                            Exit Try
                        End If
                    End If

                    Dim dtEmp As DataTable = New DataView(mdtView.ToTable, "IsGrp = 0 AND DpndtTranId = " & CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDpndtbeneficetranunkid.Index).Value) & " ", "", DataViewRowState.CurrentRows).ToTable(True, "EmpId", "EmpName")
                    dtEmp.Columns.Add("Reason", System.Type.GetType("System.String")).DefaultValue = ""
                    dtEmp.Columns.Add("IsValid", System.Type.GetType("System.Boolean")).DefaultValue = True
                    For Each dtRow As DataRow In dtEmp.Rows

                        If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, FinancialYear._Object._DatabaseName, _
                                                        ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                        FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                        User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(dtRow.Item("EmpId")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                            dtRow.Item("Reason") = objApprovalData._Message
                            dtRow.Item("IsValid") = False
                        End If
                    Next
                    dtEmp.AcceptChanges()

                    Dim strInvalidEmpIDs As String = "-999"
                    If dtEmp.Select("IsValid = 0").Length > 0 Then
                        strInvalidEmpIDs = String.Join(",", (From p In dtEmp Where (CBool(p.Item("IsValid")) = False) Select (p.Item("EmpId").ToString)).ToArray)

                        dtEmp.Columns.Remove("EmpId")
                        dtEmp.Columns.Remove("IsValid")

                        Dim frmValid As New frmCommonValidationList
                        If frmValid.displayDialog(True, Language.getMessage(mstrModuleName, 16, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?"), dtEmp) = False Then
                            Exit Try
                        End If
                    End If

                End If

                If CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhROWNO.Index).Value) = 1 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Are you sure you want to delete this transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Try
                    End If

                    Dim objDependants_Beneficiary_tran As New clsDependants_Beneficiary_tran
                    objDependants_Beneficiary_tran._Dpndtbeneficetranunkid = CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDpndtbeneficetranunkid.Index).Value)

                    Dim objDPStatus As New clsDependant_Benefice_Status_tran
                    objDPStatus._Dpndtbeneficestatustranunkid = CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDependandstatustranunkid.Index).Value)
                    objDPStatus._Isvoid = True
                    objDPStatus._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objDPStatus._Voiduserunkid = User._Object._Userunkid
                    objDPStatus._CompanyUnkid = ConfigParameter._Object._Companyunkid
                    objDPStatus._ClientIP = getIP()
                    objDPStatus._FormName = mstrForm_Name
                    objDPStatus._HostName = getHostName()
                    objDPStatus._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objDPStatus._AuditUserId = User._Object._Userunkid
                    objDPStatus._Loginemployeeunkid = 0

                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objDPStatus._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing

                    If DependantApprovalFlowVal Is Nothing Then
                        Dim objADependantTran As New clsDependant_beneficiaries_approval_tran

                        objADependantTran._Isvoid = True
                        objADependantTran._Audituserunkid = User._Object._Userunkid
                        objADependantTran._Isweb = False
                        objADependantTran._Ip = getIP()
                        objADependantTran._Host = getHostName()
                        objADependantTran._Form_Name = mstrModuleName
                        objADependantTran._Audittype = enAuditType.DELETE
                        objADependantTran._Audituserunkid = User._Object._Userunkid
                        objADependantTran._Address = objDependants_Beneficiary_tran._Address
                        objADependantTran._Birthdate = objDependants_Beneficiary_tran._Birthdate
                        objADependantTran._Cityunkid = objDependants_Beneficiary_tran._Cityunkid
                        objADependantTran._Countryunkid = objDependants_Beneficiary_tran._Countryunkid
                        objADependantTran._Email = objDependants_Beneficiary_tran._Email
                        objADependantTran._Employeeunkid = objDependants_Beneficiary_tran._Employeeunkid
                        objADependantTran._First_Name = objDependants_Beneficiary_tran._First_Name
                        objADependantTran._Identify_No = objDependants_Beneficiary_tran._Identify_No
                        objADependantTran._Isdependant = objDependants_Beneficiary_tran._Isdependant

                        objADependantTran._Last_Name = objDependants_Beneficiary_tran._Last_Name
                        objADependantTran._Middle_Name = objDependants_Beneficiary_tran._Middle_Name
                        objADependantTran._Mobile_No = objDependants_Beneficiary_tran._Mobile_No
                        objADependantTran._Nationalityunkid = objDependants_Beneficiary_tran._Nationalityunkid
                        objADependantTran._Post_Box = objDependants_Beneficiary_tran._Post_Box
                        objADependantTran._Relationunkid = objDependants_Beneficiary_tran._Relationunkid
                        objADependantTran._Stateunkid = objDependants_Beneficiary_tran._Stateunkid
                        objADependantTran._Telephone_No = objDependants_Beneficiary_tran._Telephone_No
                        objADependantTran._Zipcodeunkid = objDependants_Beneficiary_tran._Zipcodeunkid
                        objADependantTran._Gender = objDependants_Beneficiary_tran._Gender
                        objADependantTran._CompanyId = ConfigParameter._Object._Companyunkid
                        objADependantTran._ImagePath = objDependants_Beneficiary_tran._ImagePath
                        objADependantTran._Dpndtbeneficetranunkid = CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDpndtbeneficetranunkid.Index).Value)
                        objADependantTran._Isprocessed = False
                        objADependantTran._Isvoid = False
                        objADependantTran._Tranguid = Guid.NewGuid.ToString()
                        objADependantTran._Transactiondate = Now
                        objADependantTran._Approvalremark = ""
                        objADependantTran._Mappingunkid = 0
                        objADependantTran._Isfinal = False
                        objADependantTran._Operationtypeid = clsEmployeeDataApproval.enOperationType.DELETED
                        objADependantTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADependantTran._IsFromStatus = True

                        objADependantTran._Effective_date = objDependants_Beneficiary_tran._Effective_date
                        objADependantTran._Isactive = objDependants_Beneficiary_tran._Isactive
                        objADependantTran._ReasonUnkid = objDependants_Beneficiary_tran._Reasonunkid
                        objADependantTran._DeletedpndtbeneficestatustranIDs = dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDependandstatustranunkid.Index).Value.ToString
                        objADependantTran._Loginemployeeunkid = 0
                        objADependantTran._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime

                        If objADependantTran.DeleteStatus(CInt(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhDependandstatustranunkid.Index).Value), mstrVoidReason, Company._Object._Companyunkid) = False Then
                            If objADependantTran._Message <> "" Then
                                eZeeMsgBox.Show(objADependantTran._Message, enMsgBoxStyle.Information)
                            End If
                            Exit Sub

                        Else
                            objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                                 enScreenName.frmDependantsAndBeneficiariesList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                 User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                 User._Object._Username, clsEmployeeDataApproval.enOperationType.DELETED, , objDependants_Beneficiary_tran._Employeeunkid.ToString, , , _
                                                                 " dpndtbeneficetranunkid = " & objDependants_Beneficiary_tran._Dpndtbeneficetranunkid, Nothing, , , _
                                                                 " dpndtbeneficetranunkid = " & objDependants_Beneficiary_tran._Dpndtbeneficetranunkid, Nothing)

                        End If

                    Else

                        If objDPStatus.Delete() = False Then
                            If objDPStatus._Message <> "" Then
                                eZeeMsgBox.Show(objDPStatus._Message, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If
                    End If

                    Call FillList()
                End If

            End If

            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            'Call SetGridStyle()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDependantStatus_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvDependantStatus_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvDependantStatus.DataError

    End Sub

    Private Sub dgvDependantStatus_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvDependantStatus.CellPainting
        Try
            If e.RowIndex >= 0 AndAlso e.RowIndex < dgvDependantStatus.RowCount - 1 AndAlso CBool(dgvDependantStatus.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True AndAlso e.ColumnIndex > 0 Then
                If (e.ColumnIndex = objdgColhBlank.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim totWidth As Integer = 0
                    For i As Integer = 1 To dgvDependantStatus.Columns.Count - 1
                        totWidth += dgvDependantStatus.Columns(i).Width
                    Next
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, totWidth, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)

                    e.Graphics.DrawString(CType(dgvDependantStatus.Rows(e.RowIndex).Cells(dgcolhEmployee.Index).Value.ToString, String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)

                ElseIf (e.ColumnIndex = objColDelete.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, e.ClipBounds.Right, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)
                End If

                e.Handled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDependantStatus_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvDependantStatus_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvDependantStatus.DataBindingComplete
        Try
            Call SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDependantStatus_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSetActive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSetActive.GradientForeColor = GUI._ButttonFontColor

			Me.btnSetInactive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSetInactive.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblDependant.Text = Language._Object.getCaption(Me.lblDependant.Name, Me.lblDependant.Text)
			Me.lblBirthdate.Text = Language._Object.getCaption(Me.lblBirthdate.Name, Me.lblBirthdate.Text)
			Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
			Me.lnkAllocations.Text = Language._Object.getCaption(Me.lnkAllocations.Name, Me.lnkAllocations.Text)
			Me.lblDBIdNo.Text = Language._Object.getCaption(Me.lblDBIdNo.Name, Me.lblDBIdNo.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblDBRelation.Text = Language._Object.getCaption(Me.lblDBRelation.Name, Me.lblDBRelation.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.btnSetActive.Text = Language._Object.getCaption(Me.btnSetActive.Name, Me.btnSetActive.Text)
			Me.btnSetInactive.Text = Language._Object.getCaption(Me.btnSetInactive.Name, Me.btnSetInactive.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
			Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
			Me.dgcolhDependant.HeaderText = Language._Object.getCaption(Me.dgcolhDependant.Name, Me.dgcolhDependant.HeaderText)
			Me.dgcolhRelation.HeaderText = Language._Object.getCaption(Me.dgcolhRelation.Name, Me.dgcolhRelation.HeaderText)
			Me.dgcolhGender.HeaderText = Language._Object.getCaption(Me.dgcolhGender.Name, Me.dgcolhGender.HeaderText)
			Me.dgcolhBirthDate.HeaderText = Language._Object.getCaption(Me.dgcolhBirthDate.Name, Me.dgcolhBirthDate.HeaderText)
			Me.dgcolhEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffectiveDate.Name, Me.dgcolhEffectiveDate.HeaderText)
			Me.dgcolhActive.HeaderText = Language._Object.getCaption(Me.dgcolhActive.Name, Me.dgcolhActive.HeaderText)
			Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please set effective date.")
			Language.setMessage(mstrModuleName, 2, "Effective date should not be greater than current date.")
			Language.setMessage(mstrModuleName, 3, "Please select Reason.")
			Language.setMessage(mstrModuleName, 4, "Please tick atleast one dependant.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Effective date should be greater than last effective date")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot do any operation. Reason: Some of the transactions are in approval process.")
			Language.setMessage(mstrModuleName, 7, "Please Approve / Reject transction.")
			Language.setMessage(mstrModuleName, 8, "Sorry, Some of the dependants are already inactive. Please refresh the dependant list and the try again.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Some of the dependants are already active. Please refresh the dependant list and the try again.")
			Language.setMessage(mstrModuleName, 10, "Type to Search")
			Language.setMessage(mstrModuleName, 11, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?")
			Language.setMessage(mstrModuleName, 12, "Sorry, You cannot delete default transaction.")
			Language.setMessage(mstrModuleName, 13, "Sorry, You can delete last tranasction only.")
			Language.setMessage(mstrModuleName, 14, "Sorry, You cannot do any operation. Reason: Some of the transactions are in approval process.")
			Language.setMessage(mstrModuleName, 15, "Please Approve / Reject transction.")
			Language.setMessage(mstrModuleName, 16, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?")
			Language.setMessage(mstrModuleName, 17, "Are you sure you want to delete this transaction?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class