﻿Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSetDependantAgeLimit

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmSetDependantAgeLimit"
    Private objCMaster As clsCommon_Master
    Private mdtTran As New DataTable
    Private mintNewRelationId As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer) As Boolean
        Try
            mintNewRelationId = intUnkId

            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillGrid()
        Try
            dgvAgeLimit.AutoGenerateColumns = False

            dgcolhAgeLimit.DataPropertyName = "Age"
            dgcolhRelationName.DataPropertyName = "RName"
            objdgcolhRelationId.DataPropertyName = "Id"


            'Pinkal (06-Mar-2013) -- Start
            'Enhancement : TRA Changes
            dgColhmedicalbenefit.DataPropertyName = "ismedicalbenefit"
            'Pinkal (06-Mar-2013) -- End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            dgcolhCRBenefit.DataPropertyName = "iscrbenefit"
            dgcolhMaxCRCount.DataPropertyName = "maxcnt_crdependants"
            dgcolhMinAgeForCRBenefit.DataPropertyName = "minagelimit_crdependants"
            'Pinkal (25-Oct-2018) -- End


            dgvAgeLimit.DataSource = mdtTran

            dgcolhAgeLimit.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgcolhAgeLimit.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Sub SetCRBenefitOptionsWriteOnly()
        Try
            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtTran.Select("iscrbenefit = 1")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        dgvAgeLimit.Rows(mdtTran.Rows.IndexOf(drRow(i))).Cells(dgcolhMaxCRCount.Index).ReadOnly = Not CBool(drRow(i)("iscrbenefit"))
                        dgvAgeLimit.Rows(mdtTran.Rows.IndexOf(drRow(i))).Cells(dgcolhMinAgeForCRBenefit.Index).ReadOnly = Not CBool(drRow(i)("iscrbenefit"))
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCRBenefitOptionsWriteOnly", mstrModuleName)
        End Try
    End Sub

    'Pinkal (25-Oct-2018) -- End


#End Region

#Region " Form's Event(s) "

    Private Sub frmSetDependantAgeLimit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCMaster = New clsCommon_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            objlblNote.Text = "Note : Please enter 0 (zero) for no limit."
            mdtTran = objCMaster.GetDependantsAgeLimit("List", mintNewRelationId)
            Call FillGrid()

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            SetCRBenefitOptionsWriteOnly()
            'Pinkal (25-Oct-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (04-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCommon_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsCommon_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (04-Apr-2013) -- End


#End Region

#Region " Button's Event(s) "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim blnFlag As Boolean = False
            Dim mstrMessage As String = ""
            For Each dtRow As DataRow In mdtTran.Rows
                objCMaster = New clsCommon_Master

                objCMaster._Masterunkid = dtRow.Item("Id")
                objCMaster._Dependant_Limit = CInt(dtRow.Item("Age"))


                'Pinkal (06-Mar-2013) -- Start
                'Enhancement : TRA Changes
                objCMaster._IsMedicalBenefit = CInt(dtRow.Item("ismedicalbenefit"))
                'Pinkal (06-Mar-2013) -- End


                'Pinkal (25-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                objCMaster._IsCRBenefit = CBool(dtRow.Item("iscrbenefit"))
                objCMaster._MinAgeLimitForCRBenefit = CInt(dtRow.Item("minagelimit_crdependants"))
                objCMaster._MaxCountForCRBenefit = CInt(dtRow.Item("maxcnt_crdependants"))
                'Pinkal (25-Oct-2018) -- End


                If objCMaster.Update() = False Then
                    blnFlag = False
                    mstrMessage = objCMaster._Message
                    Exit For
                Else
                    blnFlag = True
                End If
            Next

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Information successfully saved."), enMsgBoxStyle.Information)
                Me.Close()
            Else
                eZeeMsgBox.Show(mstrMessage, enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Event(s) "

    Private Sub RateKeyPress(ByVal sender As Object, ByVal e As Windows.Forms.KeyPressEventArgs)
        Try
            If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
                If Asc(e.KeyChar) = 8 Then Exit Sub
                e.KeyChar = CChar("")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RateKeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAgeLimit_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAgeLimit.CellEndEdit
        Try
            If e.ColumnIndex = dgcolhMaxCRCount.Index Then
                If CBool(dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhCRBenefit.Index).Value) = True AndAlso CInt(dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) <= 0 Then
                    dgvAgeLimit.CancelEdit()
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry,You can not set 0 for max CR Max Dependants count if dependants gets CR benefit."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAgeLimit_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAgeLimit_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvAgeLimit.DataError
        Try
            e.Cancel = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAgeLimit_DataError", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAgeLimit_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvAgeLimit.EditingControlShowing
        Try
            If dgvAgeLimit.CurrentCell.ColumnIndex > 0 Then
                Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                AddHandler txt.KeyPress, AddressOf RateKeyPress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAgeLimit_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAgeLimit_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAgeLimit.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub
            If e.ColumnIndex = 1 Then
                If IsDBNull(dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 0
                If CInt(dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) >= 100 Then
                    dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 100
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAgeLimit_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAgeLimit_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAgeLimit.CellValidated
        Try

            If e.RowIndex < 0 Then Exit Sub

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            'If e.ColumnIndex = 3 Then
            '    mdtTran.Rows(e.RowIndex)("ismedicalbenefit") = dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            'End If

            If e.ColumnIndex = dgColhmedicalbenefit.Index Then
                mdtTran.Rows(e.RowIndex)("ismedicalbenefit") = dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            End If

            If e.ColumnIndex = dgcolhCRBenefit.Index Then
                mdtTran.Rows(e.RowIndex)("iscrbenefit") = dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                If CBool(dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhCRBenefit.Index).Value) Then
                    If dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMaxCRCount.Index).Value <= 0 Then dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMaxCRCount.Index).Value = 1
                Else
                    dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMaxCRCount.Index).Value = 0
                End If
            End If

            If e.ColumnIndex = dgcolhMaxCRCount.Index Then
                mdtTran.Rows(e.RowIndex)("maxcnt_crdependants") = dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            End If

            If e.ColumnIndex = dgcolhMinAgeForCRBenefit.Index Then
                mdtTran.Rows(e.RowIndex)("minagelimit_crdependants") = dgvAgeLimit.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            End If

            'Pinkal (25-Oct-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAgeLimit_CellValidated", mstrModuleName)
        End Try
    End Sub

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Sub dgvAgeLimit_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAgeLimit.CellContentClick, dgvAgeLimit.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvAgeLimit.IsCurrentCellDirty Then
                Me.dgvAgeLimit.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = dgcolhCRBenefit.Index Then
                dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMaxCRCount.Index).ReadOnly = Not CBool(dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhCRBenefit.Index).Value)
                dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMinAgeForCRBenefit.Index).ReadOnly = Not CBool(dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhCRBenefit.Index).Value)

                If CBool(dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhCRBenefit.Index).Value) Then
                    If CInt(dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMaxCRCount.Index).Value) <= 0 Then dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMaxCRCount.Index).Value = 1
                Else
                    dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMaxCRCount.Index).Value = 0
                    dgvAgeLimit.Rows(e.RowIndex).Cells(dgcolhMinAgeForCRBenefit.Index).Value = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAgeLimit_CellContentClick", mstrModuleName)
        End Try
    End Sub

    'Pinkal (25-Oct-2018) -- End


#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhRelationName.HeaderText = Language._Object.getCaption(Me.dgcolhRelationName.Name, Me.dgcolhRelationName.HeaderText)
			Me.dgcolhAgeLimit.HeaderText = Language._Object.getCaption(Me.dgcolhAgeLimit.Name, Me.dgcolhAgeLimit.HeaderText)
			Me.dgColhmedicalbenefit.HeaderText = Language._Object.getCaption(Me.dgColhmedicalbenefit.Name, Me.dgColhmedicalbenefit.HeaderText)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Information successfully saved.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class