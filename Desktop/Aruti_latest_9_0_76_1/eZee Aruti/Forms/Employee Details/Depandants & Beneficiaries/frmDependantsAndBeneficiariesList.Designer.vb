﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDependantsAndBeneficiariesList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDependantsAndBeneficiariesList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvDepandants_Benefice = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDBName = New System.Windows.Forms.ColumnHeader
        Me.colhRelation = New System.Windows.Forms.ColumnHeader
        Me.colhbirthdate = New System.Windows.Forms.ColumnHeader
        Me.colhIdNo = New System.Windows.Forms.ColumnHeader
        Me.colhGender = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployeeId = New System.Windows.Forms.ColumnHeader
        Me.objdgcolhtranguid = New System.Windows.Forms.ColumnHeader
        Me.colhOperationType = New System.Windows.Forms.ColumnHeader
        Me.colhIsActive = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtBirthdate = New System.Windows.Forms.DateTimePicker
        Me.lblBirthdate = New System.Windows.Forms.Label
        Me.lblGender = New System.Windows.Forms.Label
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lnkAllocations = New System.Windows.Forms.LinkLabel
        Me.txtDBPassportNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblDBIdNo = New System.Windows.Forms.Label
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.cboDBPostCountry = New System.Windows.Forms.ComboBox
        Me.lblDBPostCountry = New System.Windows.Forms.Label
        Me.cboDBRelation = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblDBRelation = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtDBLastName = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.txtDBFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblDBLastName = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblDBFirstName = New System.Windows.Forms.Label
        Me.objeZeeFooter = New eZee.Common.eZeeFooter
        Me.btnApprovalinfo = New eZee.Common.eZeeLightButton(Me.components)
        Me.objtblPanel = New System.Windows.Forms.TableLayoutPanel
        Me.lblParentData = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lblPendingData = New System.Windows.Forms.Label
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuImportDependants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportDependants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetAgeLimit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDependantsException = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportImages = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportToDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuSetActiveInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objeZeeFooter.SuspendLayout()
        Me.objtblPanel.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvDepandants_Benefice)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objeZeeFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(748, 438)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lvDepandants_Benefice
        '
        Me.lvDepandants_Benefice.BackColorOnChecked = True
        Me.lvDepandants_Benefice.ColumnHeaders = Nothing
        Me.lvDepandants_Benefice.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhDBName, Me.colhRelation, Me.colhbirthdate, Me.colhIdNo, Me.colhGender, Me.objcolhEmployeeId, Me.objdgcolhtranguid, Me.colhOperationType, Me.colhIsActive})
        Me.lvDepandants_Benefice.CompulsoryColumns = ""
        Me.lvDepandants_Benefice.FullRowSelect = True
        Me.lvDepandants_Benefice.GridLines = True
        Me.lvDepandants_Benefice.GroupingColumn = Nothing
        Me.lvDepandants_Benefice.HideSelection = False
        Me.lvDepandants_Benefice.Location = New System.Drawing.Point(12, 159)
        Me.lvDepandants_Benefice.MinColumnWidth = 50
        Me.lvDepandants_Benefice.MultiSelect = False
        Me.lvDepandants_Benefice.Name = "lvDepandants_Benefice"
        Me.lvDepandants_Benefice.OptionalColumns = ""
        Me.lvDepandants_Benefice.ShowMoreItem = False
        Me.lvDepandants_Benefice.ShowSaveItem = False
        Me.lvDepandants_Benefice.ShowSelectAll = True
        Me.lvDepandants_Benefice.ShowSizeAllColumnsToFit = True
        Me.lvDepandants_Benefice.Size = New System.Drawing.Size(724, 219)
        Me.lvDepandants_Benefice.Sortable = True
        Me.lvDepandants_Benefice.TabIndex = 1
        Me.lvDepandants_Benefice.UseCompatibleStateImageBehavior = False
        Me.lvDepandants_Benefice.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 150
        '
        'colhDBName
        '
        Me.colhDBName.Tag = "colhDBName"
        Me.colhDBName.Text = "Name"
        Me.colhDBName.Width = 160
        '
        'colhRelation
        '
        Me.colhRelation.Tag = "colhRelation"
        Me.colhRelation.Text = "Relation"
        Me.colhRelation.Width = 115
        '
        'colhbirthdate
        '
        Me.colhbirthdate.DisplayIndex = 4
        Me.colhbirthdate.Tag = "colhbirthdate"
        Me.colhbirthdate.Text = "Birth Date"
        Me.colhbirthdate.Width = 110
        '
        'colhIdNo
        '
        Me.colhIdNo.DisplayIndex = 5
        Me.colhIdNo.Tag = "colhIdNo"
        Me.colhIdNo.Text = "Identify No."
        Me.colhIdNo.Width = 120
        '
        'colhGender
        '
        Me.colhGender.DisplayIndex = 3
        Me.colhGender.Tag = "colhGender"
        Me.colhGender.Text = "Gender"
        Me.colhGender.Width = 65
        '
        'objcolhEmployeeId
        '
        Me.objcolhEmployeeId.Tag = "objcolhEmployeeId"
        Me.objcolhEmployeeId.Text = "EmployeeId"
        Me.objcolhEmployeeId.Width = 0
        '
        'objdgcolhtranguid
        '
        Me.objdgcolhtranguid.Tag = "objdgcolhtranguid"
        Me.objdgcolhtranguid.Text = "tranguid"
        Me.objdgcolhtranguid.Width = 0
        '
        'colhOperationType
        '
        Me.colhOperationType.Tag = "colhOperationType"
        Me.colhOperationType.Text = "Opration Type"
        Me.colhOperationType.Width = 100
        '
        'colhIsActive
        '
        Me.colhIsActive.Tag = "colhIsActive"
        Me.colhIsActive.Text = "Is Active"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.dtBirthdate)
        Me.gbFilterCriteria.Controls.Add(Me.lblBirthdate)
        Me.gbFilterCriteria.Controls.Add(Me.lblGender)
        Me.gbFilterCriteria.Controls.Add(Me.cboGender)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocations)
        Me.gbFilterCriteria.Controls.Add(Me.txtDBPassportNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDBIdNo)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine1)
        Me.gbFilterCriteria.Controls.Add(Me.cboDBPostCountry)
        Me.gbFilterCriteria.Controls.Add(Me.lblDBPostCountry)
        Me.gbFilterCriteria.Controls.Add(Me.cboDBRelation)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblDBRelation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.txtDBLastName)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.txtDBFirstName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblDBLastName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblDBFirstName)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(724, 89)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtBirthdate
        '
        Me.dtBirthdate.Checked = False
        Me.dtBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtBirthdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBirthdate.Location = New System.Drawing.Point(598, 33)
        Me.dtBirthdate.Name = "dtBirthdate"
        Me.dtBirthdate.ShowCheckBox = True
        Me.dtBirthdate.Size = New System.Drawing.Size(121, 21)
        Me.dtBirthdate.TabIndex = 10
        '
        'lblBirthdate
        '
        Me.lblBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthdate.Location = New System.Drawing.Point(536, 37)
        Me.lblBirthdate.Name = "lblBirthdate"
        Me.lblBirthdate.Size = New System.Drawing.Size(56, 13)
        Me.lblBirthdate.TabIndex = 9
        Me.lblBirthdate.Text = "Birth Date"
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(258, 36)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(73, 15)
        Me.lblGender.TabIndex = 5
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.DropDownWidth = 250
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(258, 60)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(81, 21)
        Me.cboGender.TabIndex = 13
        '
        'lnkAllocations
        '
        Me.lnkAllocations.AutoSize = True
        Me.lnkAllocations.Location = New System.Drawing.Point(601, 6)
        Me.lnkAllocations.Name = "lnkAllocations"
        Me.lnkAllocations.Size = New System.Drawing.Size(69, 13)
        Me.lnkAllocations.TabIndex = 19
        Me.lnkAllocations.TabStop = True
        Me.lnkAllocations.Text = "Allocations"
        '
        'txtDBPassportNo
        '
        Me.txtDBPassportNo.Flags = 0
        Me.txtDBPassportNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBPassportNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBPassportNo.Location = New System.Drawing.Point(598, 60)
        Me.txtDBPassportNo.Name = "txtDBPassportNo"
        Me.txtDBPassportNo.Size = New System.Drawing.Size(121, 21)
        Me.txtDBPassportNo.TabIndex = 17
        '
        'lblDBIdNo
        '
        Me.lblDBIdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBIdNo.Location = New System.Drawing.Point(536, 62)
        Me.lblDBIdNo.Name = "lblDBIdNo"
        Me.lblDBIdNo.Size = New System.Drawing.Size(56, 13)
        Me.lblDBIdNo.TabIndex = 16
        Me.lblDBIdNo.Text = "ID. No"
        Me.lblDBIdNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(244, 25)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(8, 64)
        Me.objStLine1.TabIndex = 4
        Me.objStLine1.Text = "EZeeStraightLine2"
        '
        'cboDBPostCountry
        '
        Me.cboDBPostCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBPostCountry.FormattingEnabled = True
        Me.cboDBPostCountry.Location = New System.Drawing.Point(604, 33)
        Me.cboDBPostCountry.Name = "cboDBPostCountry"
        Me.cboDBPostCountry.Size = New System.Drawing.Size(111, 21)
        Me.cboDBPostCountry.TabIndex = 15
        Me.cboDBPostCountry.Visible = False
        '
        'lblDBPostCountry
        '
        Me.lblDBPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBPostCountry.Location = New System.Drawing.Point(536, 35)
        Me.lblDBPostCountry.Name = "lblDBPostCountry"
        Me.lblDBPostCountry.Size = New System.Drawing.Size(62, 15)
        Me.lblDBPostCountry.TabIndex = 14
        Me.lblDBPostCountry.Text = "Country"
        Me.lblDBPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDBPostCountry.Visible = False
        '
        'cboDBRelation
        '
        Me.cboDBRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBRelation.DropDownWidth = 300
        Me.cboDBRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBRelation.FormattingEnabled = True
        Me.cboDBRelation.Location = New System.Drawing.Point(73, 60)
        Me.cboDBRelation.Name = "cboDBRelation"
        Me.cboDBRelation.Size = New System.Drawing.Size(138, 21)
        Me.cboDBRelation.TabIndex = 12
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(59, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'lblDBRelation
        '
        Me.lblDBRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBRelation.Location = New System.Drawing.Point(8, 63)
        Me.lblDBRelation.Name = "lblDBRelation"
        Me.lblDBRelation.Size = New System.Drawing.Size(59, 15)
        Me.lblDBRelation.TabIndex = 11
        Me.lblDBRelation.Text = "Relation"
        Me.lblDBRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(217, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'txtDBLastName
        '
        Me.txtDBLastName.Flags = 0
        Me.txtDBLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBLastName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBLastName.Location = New System.Drawing.Point(415, 60)
        Me.txtDBLastName.Name = "txtDBLastName"
        Me.txtDBLastName.Size = New System.Drawing.Size(115, 21)
        Me.txtDBLastName.TabIndex = 15
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(73, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(138, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'txtDBFirstName
        '
        Me.txtDBFirstName.BackColor = System.Drawing.Color.White
        Me.txtDBFirstName.Flags = 0
        Me.txtDBFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBFirstName.Location = New System.Drawing.Point(415, 33)
        Me.txtDBFirstName.Name = "txtDBFirstName"
        Me.txtDBFirstName.Size = New System.Drawing.Size(115, 21)
        Me.txtDBFirstName.TabIndex = 8
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(697, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'lblDBLastName
        '
        Me.lblDBLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBLastName.Location = New System.Drawing.Point(345, 63)
        Me.lblDBLastName.Name = "lblDBLastName"
        Me.lblDBLastName.Size = New System.Drawing.Size(64, 15)
        Me.lblDBLastName.TabIndex = 14
        Me.lblDBLastName.Text = "Last Name"
        Me.lblDBLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(674, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'lblDBFirstName
        '
        Me.lblDBFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBFirstName.Location = New System.Drawing.Point(345, 36)
        Me.lblDBFirstName.Name = "lblDBFirstName"
        Me.lblDBFirstName.Size = New System.Drawing.Size(64, 15)
        Me.lblDBFirstName.TabIndex = 7
        Me.lblDBFirstName.Text = "First Name"
        Me.lblDBFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objeZeeFooter
        '
        Me.objeZeeFooter.BorderColor = System.Drawing.Color.Silver
        Me.objeZeeFooter.Controls.Add(Me.btnApprovalinfo)
        Me.objeZeeFooter.Controls.Add(Me.objtblPanel)
        Me.objeZeeFooter.Controls.Add(Me.btnOperation)
        Me.objeZeeFooter.Controls.Add(Me.btnNew)
        Me.objeZeeFooter.Controls.Add(Me.btnEdit)
        Me.objeZeeFooter.Controls.Add(Me.btnDelete)
        Me.objeZeeFooter.Controls.Add(Me.btnClose)
        Me.objeZeeFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objeZeeFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objeZeeFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objeZeeFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.Location = New System.Drawing.Point(0, 383)
        Me.objeZeeFooter.Name = "objeZeeFooter"
        Me.objeZeeFooter.Size = New System.Drawing.Size(748, 55)
        Me.objeZeeFooter.TabIndex = 121
        '
        'btnApprovalinfo
        '
        Me.btnApprovalinfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprovalinfo.BackColor = System.Drawing.Color.White
        Me.btnApprovalinfo.BackgroundImage = CType(resources.GetObject("btnApprovalinfo.BackgroundImage"), System.Drawing.Image)
        Me.btnApprovalinfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprovalinfo.BorderColor = System.Drawing.Color.Empty
        Me.btnApprovalinfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprovalinfo.FlatAppearance.BorderSize = 0
        Me.btnApprovalinfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprovalinfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprovalinfo.ForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprovalinfo.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprovalinfo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.Location = New System.Drawing.Point(12, 13)
        Me.btnApprovalinfo.Name = "btnApprovalinfo"
        Me.btnApprovalinfo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprovalinfo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.Size = New System.Drawing.Size(86, 30)
        Me.btnApprovalinfo.TabIndex = 188
        Me.btnApprovalinfo.Text = "&View Detail"
        Me.btnApprovalinfo.UseVisualStyleBackColor = True
        '
        'objtblPanel
        '
        Me.objtblPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objtblPanel.ColumnCount = 2
        Me.objtblPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtblPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 184.0!))
        Me.objtblPanel.Controls.Add(Me.lblParentData, 1, 1)
        Me.objtblPanel.Controls.Add(Me.Panel2, 0, 0)
        Me.objtblPanel.Controls.Add(Me.Panel3, 0, 1)
        Me.objtblPanel.Controls.Add(Me.lblPendingData, 1, 0)
        Me.objtblPanel.Location = New System.Drawing.Point(140, 6)
        Me.objtblPanel.Margin = New System.Windows.Forms.Padding(0)
        Me.objtblPanel.Name = "objtblPanel"
        Me.objtblPanel.RowCount = 2
        Me.objtblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtblPanel.Size = New System.Drawing.Size(187, 43)
        Me.objtblPanel.TabIndex = 189
        '
        'lblParentData
        '
        Me.lblParentData.BackColor = System.Drawing.Color.Transparent
        Me.lblParentData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblParentData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblParentData.ForeColor = System.Drawing.Color.Black
        Me.lblParentData.Location = New System.Drawing.Point(25, 22)
        Me.lblParentData.Name = "lblParentData"
        Me.lblParentData.Size = New System.Drawing.Size(178, 20)
        Me.lblParentData.TabIndex = 183
        Me.lblParentData.Text = "Parent Detail"
        Me.lblParentData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.PowderBlue
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(14, 14)
        Me.Panel2.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightCoral
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(4, 25)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(14, 14)
        Me.Panel3.TabIndex = 1
        '
        'lblPendingData
        '
        Me.lblPendingData.BackColor = System.Drawing.Color.Transparent
        Me.lblPendingData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendingData.ForeColor = System.Drawing.Color.Black
        Me.lblPendingData.Location = New System.Drawing.Point(25, 1)
        Me.lblPendingData.Name = "lblPendingData"
        Me.lblPendingData.Size = New System.Drawing.Size(178, 20)
        Me.lblPendingData.TabIndex = 182
        Me.lblPendingData.Text = "Pending Approval"
        Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(106, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 121
        Me.btnOperation.Text = "Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportDependants, Me.mnuExportDependants, Me.mnuSetAgeLimit, Me.mnuDependantsException, Me.mnuImportImages, Me.ToolStripMenuItem1, Me.mnuSetActiveInactive})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(192, 164)
        '
        'mnuImportDependants
        '
        Me.mnuImportDependants.Name = "mnuImportDependants"
        Me.mnuImportDependants.Size = New System.Drawing.Size(191, 22)
        Me.mnuImportDependants.Text = "&Import Dependants"
        '
        'mnuExportDependants
        '
        Me.mnuExportDependants.Name = "mnuExportDependants"
        Me.mnuExportDependants.Size = New System.Drawing.Size(191, 22)
        Me.mnuExportDependants.Text = "E&xport Dependants"
        '
        'mnuSetAgeLimit
        '
        Me.mnuSetAgeLimit.Name = "mnuSetAgeLimit"
        Me.mnuSetAgeLimit.Size = New System.Drawing.Size(191, 22)
        Me.mnuSetAgeLimit.Text = "Set Age Limit"
        '
        'mnuDependantsException
        '
        Me.mnuDependantsException.Name = "mnuDependantsException"
        Me.mnuDependantsException.Size = New System.Drawing.Size(191, 22)
        Me.mnuDependantsException.Tag = "mnuDependantsException"
        Me.mnuDependantsException.Text = "Dependants Exception"
        '
        'mnuImportImages
        '
        Me.mnuImportImages.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportToDatabase, Me.mnuGetFileFormat})
        Me.mnuImportImages.Name = "mnuImportImages"
        Me.mnuImportImages.Size = New System.Drawing.Size(191, 22)
        Me.mnuImportImages.Tag = "mnuImportImages"
        Me.mnuImportImages.Text = "Import Images"
        '
        'mnuImportToDatabase
        '
        Me.mnuImportToDatabase.Name = "mnuImportToDatabase"
        Me.mnuImportToDatabase.Size = New System.Drawing.Size(177, 22)
        Me.mnuImportToDatabase.Tag = "mnuImportToDatabase"
        Me.mnuImportToDatabase.Text = "Import To Database"
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(177, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "Get File Format"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(188, 6)
        '
        'mnuSetActiveInactive
        '
        Me.mnuSetActiveInactive.Name = "mnuSetActiveInactive"
        Me.mnuSetActiveInactive.Size = New System.Drawing.Size(191, 22)
        Me.mnuSetActiveInactive.Text = "Set Active / Inactive"
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(331, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(434, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 119
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(537, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 118
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(640, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 117
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(748, 58)
        Me.eZeeHeader.TabIndex = 109
        Me.eZeeHeader.Title = "Dependants And Beneficiaries List"
        '
        'frmDependantsAndBeneficiariesList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(748, 438)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDependantsAndBeneficiariesList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Dependants And Beneficiaries List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objeZeeFooter.ResumeLayout(False)
        Me.objtblPanel.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objeZeeFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents txtDBLastName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDBFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDBLastName As System.Windows.Forms.Label
    Friend WithEvents lblDBFirstName As System.Windows.Forms.Label
    Friend WithEvents cboDBRelation As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBRelation As System.Windows.Forms.Label
    Friend WithEvents cboDBPostCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBPostCountry As System.Windows.Forms.Label
    Friend WithEvents txtDBPassportNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDBIdNo As System.Windows.Forms.Label
    Friend WithEvents lvDepandants_Benefice As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDBName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRelation As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhbirthdate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuImportDependants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportDependants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lnkAllocations As System.Windows.Forms.LinkLabel
    Friend WithEvents mnuSetAgeLimit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colhGender As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblBirthdate As System.Windows.Forms.Label
    Friend WithEvents dtBirthdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents mnuImportImages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportToDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhEmployeeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuDependantsException As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objtblPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblParentData As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
    Friend WithEvents objdgcolhtranguid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOperationType As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnApprovalinfo As eZee.Common.eZeeLightButton
    Friend WithEvents colhIsActive As System.Windows.Forms.ColumnHeader
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetActiveInactive As System.Windows.Forms.ToolStripMenuItem
End Class
