﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportIdentities

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportIdentities"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim mdicEmpAdded As New Dictionary(Of String, Integer)

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objAIdInfoTran As New clsIdentity_Approval_tran
    Private Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Private IdentityApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmIdentityInfoList)))
    Private mdtApprovalIdTran As DataTable
    'Gajanan [22-Feb-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

#End Region

#Region " From's Events "

    Private Sub frmImportMemberships_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportMemberships_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub WizImportIdentity_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizImportIdentity.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizImportIdentity.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportIdentity_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub WizImportIdentity_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizImportIdentity.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizImportIdentity.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()

                Case WizImportIdentity.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportIdentity_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "



    'Gajanan [27-May-2019] -- Start              


    'Private Sub CreateDataTable()
    '    Dim blnIsNotThrown As Boolean = True
    '    Dim objEmployee As New clsEmployee_Master
    '    Try
    '        ezWait.Active = True

    '        mdt_ImportData_Others.Columns.Add("employeeid", System.Type.GetType("System.Int32"))
    '        mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("identitytype", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("identityno", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("isdefault", System.Type.GetType("System.Boolean"))
    '        mdt_ImportData_Others.Columns.Add("country", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("issued_place", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("dl_class", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("issue_date", System.Type.GetType("System.DateTime"))
    '        mdt_ImportData_Others.Columns.Add("expiry_date", System.Type.GetType("System.DateTime"))
    '        mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
    '        mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
    '        'Gajanan [22-Feb-2019] -- Start
    '        'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '        mdt_ImportData_Others.Columns.Add("objIsApproved", GetType(System.Boolean))
    '        'Gajanan [22-Feb-2019] -- End

    '        Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
    '        For i As Integer = 0 To dtTemp.Length - 1
    '            mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
    '        Next
    '        mds_ImportData.AcceptChanges()

    '        Dim drNewRow As DataRow
    '        Dim intEmpId As Integer = -1

    '        For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
    '            blnIsNotThrown = CheckInvalidData(dtRow)

    '            If blnIsNotThrown = False Then Exit Sub

    '            If mdicEmpAdded.ContainsKey(dtRow.Item(cboEmployeeCode.Text).ToString.Trim) Then Continue For

    '            drNewRow = mdt_ImportData_Others.NewRow
    '            'Gajanan [22-Feb-2019] -- Start
    '            'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '            'intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim)
    '            Dim blnIsApproved As Boolean = True
    '            intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim, blnIsApproved)
    '            'Gajanan [22-Feb-2019] -- End

    '            If intEmpId <= 0 Then
    '                drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
    '                drNewRow.Item("image") = imgError
    '                drNewRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Employee Not Found.")
    '                drNewRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                drNewRow.Item("objStatus") = 2
    '                objError.Text = CStr(Val(objError.Text) + 1)
    '            Else
    '                drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
    '                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
    '                drNewRow.Item("message") = ""
    '                drNewRow.Item("status") = ""
    '                drNewRow.Item("objStatus") = ""
    '                drNewRow.Item("employeeid") = intEmpId.ToString
    '                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
    '                drNewRow.Item("message") = ""
    '                drNewRow.Item("status") = ""
    '                drNewRow.Item("objStatus") = ""
    '                'Gajanan [22-Feb-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                drNewRow.Item("objIsApproved") = blnIsApproved
    '                'Gajanan [22-Feb-2019] -- End
    '                mdicEmpAdded.Add(dtRow.Item(cboEmployeeCode.Text).ToString.Trim, intEmpId)
    '                objTotal.Text = CStr(Val(objTotal.Text) + 1)
    '            End If

    '            intEmpId = -1
    '            mdt_ImportData_Others.Rows.Add(drNewRow)

    '        Next

    '        If blnIsNotThrown = True Then
    '            colhEmployee.DataPropertyName = "employeecode"
    '            colhMessage.DataPropertyName = "message"
    '            objcolhImage.DataPropertyName = "image"
    '            colhStatus.DataPropertyName = "status"
    '            objdgcolhEmployeeId.DataPropertyName = "employeeid"
    '            objcolhstatus.DataPropertyName = "objStatus"
    '            dgData.AutoGenerateColumns = False
    '            dvGriddata = New DataView(mdt_ImportData_Others)
    '            dgData.DataSource = dvGriddata
    '        End If

    '        Call Import_Data()

    '        ezWait.Active = False
    '        WizImportIdentity.BackEnabled = False
    '        WizImportIdentity.CancelText = "Finish"

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
    '    Finally
    '        objEmployee = Nothing
    '    End Try
    'End Sub

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Dim objEmployee As New clsEmployee_Master
        Try
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("employeeid", System.Type.GetType("System.Int32"))
            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Firstname", GetType(String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("Surname", GetType(String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("identitytype", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("identityno", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("isdefault", System.Type.GetType("System.Boolean"))
            mdt_ImportData_Others.Columns.Add("country", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("PlaceofIssue", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("dlclass", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("issuedate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("expirydate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("SerialNo", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns("identitytype").ExtendedProperties.Add("col", "identitytype")
            mdt_ImportData_Others.Columns("identityno").ExtendedProperties.Add("col", "identityno")
            mdt_ImportData_Others.Columns("isdefault").ExtendedProperties.Add("col", "isdefault")
            mdt_ImportData_Others.Columns("country").ExtendedProperties.Add("col", "country")
            mdt_ImportData_Others.Columns("PlaceofIssue").ExtendedProperties.Add("col", "issued_place")
            mdt_ImportData_Others.Columns("dlclass").ExtendedProperties.Add("col", "dl_class")
            mdt_ImportData_Others.Columns("issuedate").ExtendedProperties.Add("col", "issue_date")
            mdt_ImportData_Others.Columns("expirydate").ExtendedProperties.Add("col", "expiry_date")



            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objIsApproved", GetType(System.Boolean))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            Dim intEmpId As Integer = -1

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)

                If blnIsNotThrown = False Then Exit Sub


                drNewRow = mdt_ImportData_Others.NewRow
                Dim blnIsApproved As Boolean = True
                intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim, blnIsApproved)

                If intEmpId <= 0 Then
                    drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                    drNewRow.Item("image") = imgError
                    drNewRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Employee Not Found.")
                    drNewRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                    drNewRow.Item("objStatus") = 2
                    'S.SANDEEP |06-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    drNewRow.Item("objIsApproved") = False
                    'S.SANDEEP |06-SEP-2019| -- END
                    objError.Text = CStr(Val(objError.Text) + 1)
                Else


                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId

                    drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                    drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                    drNewRow.Item("message") = ""
                    drNewRow.Item("status") = ""
                    drNewRow.Item("objStatus") = ""
                    drNewRow.Item("employeeid") = intEmpId.ToString
                    drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                    drNewRow.Item("message") = ""
                    drNewRow.Item("status") = ""
                    drNewRow.Item("objStatus") = ""
                    drNewRow.Item("objIsApproved") = blnIsApproved

                    drNewRow.Item("Firstname") = objEmployee._Firstname
                    drNewRow.Item("Surname") = objEmployee._Surname

                End If

                If cboIdentityType.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("identitytype") = dtRow.Item(cboIdentityType.Text).ToString.Trim
                Else
                    drNewRow.Item("identitytype") = ""
                End If

                If cboIdentityNo.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("identityno") = dtRow.Item(cboIdentityNo.Text).ToString.Trim
                Else
                    drNewRow.Item("identityno") = ""
                End If


                If cboIsDefault.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("isdefault") = CBool(dtRow.Item(cboIsDefault.Text).ToString.Trim)
                Else
                    drNewRow.Item("isdefault") = False
                End If

                If cboCountry.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("country") = dtRow.Item(cboCountry.Text).ToString.Trim
                Else
                    drNewRow.Item("country") = ""
                End If

                If cboPlaceofIssue.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("PlaceofIssue") = dtRow.Item(cboPlaceofIssue.Text).ToString.Trim
                Else
                    drNewRow.Item("PlaceofIssue") = ""
                End If

                If cboDLClass.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("dlclass") = dtRow.Item(cboDLClass.Text).ToString.Trim
                Else
                    drNewRow.Item("dlclass") = ""
                End If

                If cboIssueDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("issuedate") = dtRow.Item(cboIssueDate.Text).ToString.Trim
                Else
                    drNewRow.Item("issuedate") = ""
                End If

                If cboExpiryDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("expirydate") = dtRow.Item(cboExpiryDate.Text).ToString.Trim
                Else
                    drNewRow.Item("expirydate") = ""
                End If

                objTotal.Text = CStr(Val(objTotal.Text) + 1)
                intEmpId = -1
                mdt_ImportData_Others.Rows.Add(drNewRow)

            Next


            If mdt_ImportData_Others.Rows.Count > 0 Then

                mdt_ImportData_Others.DefaultView.Sort = "employeecode ASC"
                mdt_ImportData_Others = mdt_ImportData_Others.DefaultView.ToTable()


            End If


            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "employeecode"
                colhIdentityType.DataPropertyName = "identitytype"
                colhIdentityNo.DataPropertyName = "identityno"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objdgcolhEmployeeId.DataPropertyName = "employeeid"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizImportIdentity.BackEnabled = False
            WizImportIdentity.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee Code cannot be blank. Please set the Employee Code to import employee identities."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboIdentityType.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Identity Type cannot be blank. Please set the Identity Type to import employee identities."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboIdentityNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Identity No cannot be blank. Please set the Identity No to import employee identities."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboIsDefault.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Is Default cannot be blank. Please set the Is Default to import employee identities."), enMsgBoxStyle.Information)
                    Return False
                End If

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function



    'Gajanan [27-May-2019] -- Start              


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Private Sub Import_Data()
    '    Try
    '        btnFilter.Enabled = False

    '        If mdt_ImportData_Others.Rows.Count <= 0 Then
    '            Exit Sub
    '        End If

    '        If mdicEmpAdded.Keys.Count <= 0 Then
    '            Exit Sub
    '        End If

    '        Dim objCMaster As New clsCommon_Master
    '        Dim objIdentitytran As clsIdentity_tran = Nothing
    '        Dim dtIdentityTran As New DataTable
    '        Dim mdtTran As New DataTable
    '        Dim intIdentityTypeId As Integer = -1



    '        For Each dtMRow As DataRow In mdt_ImportData_Others.Rows
    '            If mdicEmpAdded.ContainsKey(dtMRow.Item("employeecode").ToString.Trim) = False Then Continue For

    '            'Gajanan [22-Feb-2019] -- Start
    '            'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '            'objIdentitytran = New clsIdentity_tran
    '            'objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '            'dtIdentityTran = objIdentitytran._DataList.Copy
    '            If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                objAIdInfoTran = New clsIdentity_Approval_tran
    '                objAIdInfoTran._Employeeunkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                dtIdentityTran = objAIdInfoTran._DataTable.Copy
    '            Else
    '                objIdentitytran = New clsIdentity_tran
    '                objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                dtIdentityTran = objIdentitytran._DataList.Copy
    '            End If
    '            'Gajanan [22-Feb-2019] -- End

    '            Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable

    '            If dtFilter.Rows.Count > 0 Then
    '                Dim intAssignedEmpId As Integer = -1
    '                For Each dtRow As DataRow In dtFilter.Rows
    '                    Dim dtERow As DataRow = dtIdentityTran.NewRow
    '                    If dtRow.Item(cboIdentityType.Text).ToString.Trim.Length > 0 Then
    '                        intIdentityTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, dtRow.Item(cboIdentityType.Text).ToString.Trim)
    '                        If intIdentityTypeId <= 0 Then
    '                            If Not objCMaster Is Nothing Then objCMaster = Nothing
    '                            objCMaster = New clsCommon_Master

    '                            If dtRow.Item(cboIdentityType.Text).ToString.Trim.Length > 5 Then
    '                                objCMaster._Alias = dtRow.Item(cboIdentityType.Text).ToString.Substring(0, 4).Trim
    '                            Else
    '                                objCMaster._Alias = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                            End If

    '                            objCMaster._Code = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                            objCMaster._Isactive = True
    '                            objCMaster._Mastertype = clsCommon_Master.enCommonMaster.IDENTITY_TYPES
    '                            objCMaster._Name = dtRow.Item(cboIdentityType.Text).ToString.Trim

    '                            If objCMaster.Insert = False Then
    '                                dtMRow.Item("image") = imgWarring
    '                                dtMRow.Item("message") = objCMaster._Message
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 0
    '                                objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                            End If
    '                            intIdentityTypeId = objCMaster._Masterunkid
    '                        End If

    '                    End If

    '                    'Gajanan [22-Feb-2019] -- Start
    '                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                    'intAssignedEmpId = objIdentitytran.GetEmployeeIdentityUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), intIdentityTypeId, dtRow.Item(cboIdentityNo.Text).ToString.Trim)

    '                    'If intAssignedEmpId <= 0 Then

    '                    '    dtERow.Item("identitytranunkid") = -1
    '                    '    dtERow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                    '    dtERow.Item("idtypeunkid") = intIdentityTypeId
    '                    '    dtERow.Item("identity_no") = dtRow.Item(cboIdentityNo.Text).ToString.Trim
    '                    '    dtERow.Item("isdefault") = CBool(dtRow.Item(cboIsDefault.Text))

    '                    '    If cboCountry.Text.Trim.Length > 0 Then
    '                    '        If dtRow.Item(cboCountry.Text).ToString.Trim.Length > 0 Then
    '                    '            Dim objCountry As New clsMasterData
    '                    '            dtERow.Item("countryunkid") = objCountry.GetCountryUnkId(dtRow.Item(cboCountry.Text).ToString.Trim)
    '                    '        Else
    '                    '            dtERow.Item("countryunkid") = 0
    '                    '        End If
    '                    '    Else
    '                    '        dtERow.Item("countryunkid") = 0
    '                    '    End If

    '                    '    If cboPlaceofIssue.Text.Trim.Length > 0 Then
    '                    '        If dtRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
    '                    '            If dtRow.Item(cboPlaceofIssue.Text).ToString Is Nothing OrElse dtRow.Item(cboPlaceofIssue.Text).ToString = "" Then
    '                    '                dtERow.Item("issued_place") = ""
    '                    '            Else
    '                    '                dtERow.Item("issued_place") = dtRow.Item(cboPlaceofIssue.Text)
    '                    '            End If
    '                    '        Else
    '                    '            dtERow.Item("issued_place") = ""
    '                    '        End If
    '                    '    Else
    '                    '        dtERow.Item("issued_place") = ""
    '                    '    End If

    '                    '    If cboDLClass.Text.Trim.Length > 0 Then
    '                    '        If dtRow.Item(cboDLClass.Text).ToString.Trim.Length > 0 Then
    '                    '            If dtRow.Item(cboDLClass.Text).ToString.Trim Is Nothing OrElse dtRow.Item(cboDLClass.Text).ToString.Trim = "" Then
    '                    '                dtERow.Item("dl_class") = ""
    '                    '            Else
    '                    '                dtERow.Item("dl_class") = dtRow.Item(cboDLClass.Text)
    '                    '            End If
    '                    '        Else
    '                    '            dtERow.Item("dl_class") = ""
    '                    '        End If
    '                    '    Else
    '                    '        dtERow.Item("dl_class") = ""
    '                    '    End If

    '                    '    If cboIssueDate.Text.Trim.Length > 0 Then
    '                    '        If dtRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
    '                    '            If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                    '                dtERow.Item("issue_date") = DBNull.Value
    '                    '            Else
    '                    '                dtERow.Item("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                    '            End If
    '                    '        Else
    '                    '            dtERow.Item("issue_date") = DBNull.Value
    '                    '        End If
    '                    '    Else
    '                    '        dtERow.Item("issue_date") = DBNull.Value
    '                    '    End If

    '                    '    If cboExpiryDate.Text.Trim.Length > 0 Then
    '                    '        If dtRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
    '                    '            If dtRow.Item(cboExpiryDate.Text).ToString = "0000-00-00" Then
    '                    '                dtERow.Item("expiry_date") = DBNull.Value
    '                    '            Else
    '                    '                dtERow.Item("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                    '            End If
    '                    '        Else
    '                    '            dtERow.Item("expiry_date") = DBNull.Value
    '                    '        End If
    '                    '    Else
    '                    '        dtERow.Item("expiry_date") = DBNull.Value
    '                    '    End If

    '                    '    dtERow.Item("AUD") = "A"
    '                    '    dtERow.Item("GUID") = Guid.NewGuid.ToString
    '                    '    dtIdentityTran.Rows.Add(dtERow)

    '                    'Else
    '                    '    dtMRow.Item("image") = imgWarring
    '                    '    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
    '                    '    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                    '    dtMRow.Item("objStatus") = 0
    '                    '    objWarning.Text = CStr(Val(objWarning.Text) + 1)

    '                    'End If

    '                    If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                        intAssignedEmpId = objAIdInfoTran.GetEmployeeIdentityUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), intIdentityTypeId)
    '                    Else
    '                        intAssignedEmpId = objIdentitytran.GetEmployeeIdentityUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), intIdentityTypeId, dtRow.Item(cboIdentityNo.Text).ToString.Trim)
    '                    End If

    '                    If intAssignedEmpId <= 0 Then
    '                        dtERow.Item("identitytranunkid") = -1
    '                        dtERow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                        dtERow.Item("idtypeunkid") = intIdentityTypeId
    '                        dtERow.Item("identity_no") = dtRow.Item(cboIdentityNo.Text).ToString.Trim
    '                        dtERow.Item("isdefault") = CBool(dtRow.Item(cboIsDefault.Text))
    '                        If cboCountry.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboCountry.Text).ToString.Trim.Length > 0 Then
    '                                Dim objCountry As New clsMasterData
    '                                dtERow.Item("countryunkid") = objCountry.GetCountryUnkId(dtRow.Item(cboCountry.Text).ToString.Trim)
    '                            Else
    '                                dtERow.Item("countryunkid") = 0
    '                            End If
    '                        Else
    '                            dtERow.Item("countryunkid") = 0
    '                        End If
    '                        If cboPlaceofIssue.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
    '                                If dtRow.Item(cboPlaceofIssue.Text).ToString Is Nothing OrElse dtRow.Item(cboPlaceofIssue.Text).ToString = "" Then
    '                                    dtERow.Item("issued_place") = ""
    '                                Else
    '                                    dtERow.Item("issued_place") = dtRow.Item(cboPlaceofIssue.Text)
    '                                End If
    '                            Else
    '                                dtERow.Item("issued_place") = ""
    '                            End If
    '                        Else
    '                            dtERow.Item("issued_place") = ""
    '                        End If
    '                        If cboDLClass.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboDLClass.Text).ToString.Trim.Length > 0 Then
    '                                If dtRow.Item(cboDLClass.Text).ToString.Trim Is Nothing OrElse dtRow.Item(cboDLClass.Text).ToString.Trim = "" Then
    '                                    dtERow.Item("dl_class") = ""
    '                                Else
    '                                    dtERow.Item("dl_class") = dtRow.Item(cboDLClass.Text)
    '                                End If
    '                            Else
    '                                dtERow.Item("dl_class") = ""
    '                            End If
    '                        Else
    '                            dtERow.Item("dl_class") = ""
    '                        End If
    '                        If cboIssueDate.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                    dtERow.Item("issue_date") = DBNull.Value
    '                                Else
    '                                    dtERow.Item("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                End If
    '                            Else
    '                                dtERow.Item("issue_date") = DBNull.Value
    '                            End If
    '                        Else
    '                            dtERow.Item("issue_date") = DBNull.Value
    '                        End If
    '                        If cboExpiryDate.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
    '                                If dtRow.Item(cboExpiryDate.Text).ToString = "0000-00-00" Then
    '                                    dtERow.Item("expiry_date") = DBNull.Value
    '                                Else
    '                                    dtERow.Item("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                End If
    '                            Else
    '                                dtERow.Item("expiry_date") = DBNull.Value
    '                            End If
    '                        Else
    '                            dtERow.Item("expiry_date") = DBNull.Value
    '                        End If
    '                        dtERow.Item("AUD") = "A"
    '                        If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                            dtERow.Item("tranguid") = Guid.NewGuid.ToString()
    '                            dtERow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
    '                            dtERow.Item("mappingunkid") = 0
    '                            dtERow.Item("approvalremark") = ""
    '                            dtERow.Item("isfinal") = False
    '                            dtERow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
    '                            dtERow.Item("loginemployeeunkid") = 0
    '                            dtERow.Item("isvoid") = False
    '                            dtERow.Item("voidreason") = ""
    '                            dtERow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                            dtERow.Item("audittype") = enAuditType.ADD
    '                            dtERow.Item("audituserunkid") = User._Object._Userunkid
    '                            dtERow.Item("ip") = getIP()
    '                            dtERow.Item("host") = getHostName()
    '                            dtERow.Item("form_name") = mstrModuleName
    '                            dtERow.Item("isweb") = False
    '                            dtERow.Item("isprocessed") = False
    '                            dtERow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.ADDED
    '                        Else
    '                            dtERow.Item("GUID") = Guid.NewGuid.ToString
    '                        End If
    '                        dtIdentityTran.Rows.Add(dtERow)
    '                    Else
    '                        dtMRow.Item("image") = imgWarring
    '                        dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
    '                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                        dtMRow.Item("objStatus") = 0
    '                        objWarning.Text = CStr(Val(objWarning.Text) + 1)

    '                    End If
    '                    'Gajanan [22-Feb-2019] -- End

    '                Next

    '                If intAssignedEmpId <= 0 Then
    '                    If dtIdentityTran.Rows.Count > 0 Then
    '                        'Gajanan [22-Feb-2019] -- Start
    '                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                        'objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                        'objIdentitytran._DataList = dtIdentityTran
    '                        ''S.SANDEEP [04 JUN 2015] -- START
    '                        ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        ''If objIdentitytran.InsertUpdateDelete_IdentityTran Then
    '                        'If objIdentitytran.ImportEmployeeIdentity(User._Object._Userunkid) Then
    '                        '    'S.SANDEEP [04 JUN 2015] -- END
    '                        '    dtMRow.Item("image") = imgAccept
    '                        '    dtMRow.Item("message") = ""
    '                        '    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                        '    dtMRow.Item("objStatus") = 1
    '                        '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                        'Else
    '                        '    dtMRow.Item("image") = imgError
    '                        '    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                        '    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                        '    dtMRow.Item("objStatus") = 2
    '                        '    objError.Text = CStr(Val(objError.Text) + 1)
    '                        'End If
    '                        If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                            If objAIdInfoTran.ImportEmployeeIdentity(dtIdentityTran, User._Object._Userunkid) Then
    '                                dtMRow.Item("image") = imgAccept
    '                                dtMRow.Item("message") = ""
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                dtMRow.Item("objStatus") = 1
    '                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                            Else
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                            End If
    '                        Else
    '                            objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                            objIdentitytran._DataList = dtIdentityTran
    '                            If objIdentitytran.ImportEmployeeIdentity(User._Object._Userunkid) Then
    '                                dtMRow.Item("image") = imgAccept
    '                                dtMRow.Item("message") = ""
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                dtMRow.Item("objStatus") = 1
    '                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                            Else
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                            End If
    '                        End If
    '                        'Gajanan [22-Feb-2019] -- End
    '                    End If
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
    '    Finally
    '        btnFilter.Enabled = True
    '    End Try
    'End Sub
    'Private Sub Import_Data()
    '    Try
    '        btnFilter.Enabled = False
    '        If mdt_ImportData_Others.Rows.Count <= 0 Then
    '            Exit Sub
    '        End If
    '        If mdicEmpAdded.Keys.Count <= 0 Then
    '            Exit Sub
    '        End If
    '        Dim objCMaster As New clsCommon_Master
    '        Dim objIdentitytran As clsIdentity_tran = Nothing
    '        Dim dtIdentityTran As New DataTable
    '        Dim mdtTran As New DataTable
    '        Dim intIdentityTypeId As Integer = -1

    '        Dim dtApprovedEmp, dtPendingEmp As DataTable

    '        'Gajanan [27-May-2019] -- Start
    '        Dim EmpList As New List(Of String)
    '        'Gajanan [27-May-2019] -- End


    '        'Dim dvApprovedEmp, dvPendingEmp As DataView
    '        'dvApprovedEmp = Nothing : dvPendingEmp = Nothing
    '        'dtApprovedEmp = Nothing : dtPendingEmp = Nothing
    '        'If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True).Count > 0 Then
    '        '    dtApprovedEmp = mdt_ImportData_Others.Select("objIsApproved=True").CopyToDataTable()
    '        '    dvApprovedEmp = mdt_ImportData_Others.DefaultView
    '        '    dvApprovedEmp.RowFilter = "objIsApproved=True"
    '        'End If
    '        'If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False).Count > 0 Then
    '        '    dtPendingEmp = mdt_ImportData_Others.Select("objIsApproved=False").CopyToDataTable()
    '        '    dvPendingEmp = mdt_ImportData_Others.DefaultView
    '        '    dvPendingEmp.RowFilter = "objIsApproved=False"
    '        'End If

    '        Dim pRow As IEnumerable(Of DataRow) = Nothing : Dim aRow As IEnumerable(Of DataRow) = Nothing
    '        If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False).Count > 0 Then
    '            pRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False)
    '        End If
    '        If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True).Count > 0 Then
    '            aRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True)
    '        End If

    '        'PENDING EMPLOYEE - DATA WILL GO DIRECTLY INTO MAIN TABLE
    '        If pRow IsNot Nothing AndAlso pRow.Count > 0 Then
    '            For Each dtMRow As DataRow In pRow

    '                Application.DoEvents()

    '                If mdicEmpAdded.ContainsKey(dtMRow.Item("employeecode").ToString.Trim) = False Then Continue For
    '                objIdentitytran = New clsIdentity_tran
    '                objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                dtIdentityTran = objIdentitytran._DataList.Copy
    '                Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable
    '                If dtFilter.Rows.Count > 0 Then
    '                    Dim intAssignedEmpId As Integer = -1
    '                    For Each dtRow As DataRow In dtFilter.Rows
    '                        dtMRow("identityno") = dtRow.Item(cboIdentityNo.Text).ToString.Trim
    '                        dtMRow("isdefault") = CBool(dtRow.Item(cboIsDefault.Text))
    '                        dtMRow("issued_place") = dtRow.Item(cboPlaceofIssue.Text).ToString.Trim
    '                        dtMRow("dl_class") = dtRow.Item(cboDLClass.Text).ToString.Trim

    '                        Dim dtERow As DataRow = dtIdentityTran.NewRow
    '                        If dtRow.Item(cboIdentityType.Text).ToString.Trim.Length > 0 Then
    '                            intIdentityTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, dtRow.Item(cboIdentityType.Text).ToString.Trim)
    '                            If intIdentityTypeId <= 0 Then
    '                                If Not objCMaster Is Nothing Then objCMaster = Nothing
    '                                objCMaster = New clsCommon_Master
    '                                If dtRow.Item(cboIdentityType.Text).ToString.Trim.Length > 5 Then
    '                                    objCMaster._Alias = dtRow.Item(cboIdentityType.Text).ToString.Substring(0, 4).Trim
    '                                Else
    '                                    objCMaster._Alias = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                                End If
    '                                objCMaster._Code = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                                objCMaster._Isactive = True
    '                                objCMaster._Mastertype = clsCommon_Master.enCommonMaster.IDENTITY_TYPES
    '                                objCMaster._Name = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                                If objCMaster.Insert = False Then
    '                                    dtMRow.Item("image") = imgWarring
    '                                    dtMRow.Item("message") = objCMaster._Message
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                    dtMRow.Item("objStatus") = 0
    '                                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                                End If
    '                                dtMRow("identitytype") = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                                intIdentityTypeId = objCMaster._Masterunkid
    '                            End If
    '                        End If
    '                        intAssignedEmpId = objIdentitytran.GetEmployeeIdentityUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), intIdentityTypeId, dtRow.Item(cboIdentityNo.Text).ToString.Trim)

    '                        If intAssignedEmpId <= 0 Then
    '                            dtERow.Item("identitytranunkid") = -1
    '                            dtERow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                            dtERow.Item("idtypeunkid") = intIdentityTypeId
    '                            dtERow.Item("identity_no") = dtRow.Item(cboIdentityNo.Text).ToString.Trim
    '                            dtERow.Item("isdefault") = CBool(dtRow.Item(cboIsDefault.Text))
    '                            If cboCountry.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboCountry.Text).ToString.Trim.Length > 0 Then
    '                                    Dim objCountry As New clsMasterData
    '                                    dtERow.Item("countryunkid") = objCountry.GetCountryUnkId(dtRow.Item(cboCountry.Text).ToString.Trim)
    '                                    dtMRow("country") = dtRow.Item(cboCountry.Text).ToString.Trim
    '                                Else
    '                                    dtERow.Item("countryunkid") = 0
    '                                End If
    '                            Else
    '                                dtERow.Item("countryunkid") = 0
    '                            End If
    '                            If cboPlaceofIssue.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboPlaceofIssue.Text).ToString Is Nothing OrElse dtRow.Item(cboPlaceofIssue.Text).ToString = "" Then
    '                                        dtERow.Item("issued_place") = ""
    '                                    Else
    '                                        dtERow.Item("issued_place") = dtRow.Item(cboPlaceofIssue.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("issued_place") = ""
    '                                End If
    '                            Else
    '                                dtERow.Item("issued_place") = ""
    '                            End If
    '                            If cboDLClass.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboDLClass.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboDLClass.Text).ToString.Trim Is Nothing OrElse dtRow.Item(cboDLClass.Text).ToString.Trim = "" Then
    '                                        dtERow.Item("dl_class") = ""
    '                                    Else
    '                                        dtERow.Item("dl_class") = dtRow.Item(cboDLClass.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("dl_class") = ""
    '                                End If
    '                            Else
    '                                dtERow.Item("dl_class") = ""
    '                            End If
    '                            If cboIssueDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("issue_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                        dtMRow("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("issue_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("issue_date") = DBNull.Value
    '                            End If
    '                            If cboExpiryDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboExpiryDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("expiry_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                        dtMRow("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("expiry_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("expiry_date") = DBNull.Value
    '                            End If
    '                            dtERow.Item("AUD") = "A"
    '                            dtERow.Item("GUID") = Guid.NewGuid.ToString
    '                            dtIdentityTran.Rows.Add(dtERow)
    '                        Else
    '                            dtMRow.Item("image") = imgWarring
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 0
    '                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                        End If
    '                    Next
    '                    If intAssignedEmpId <= 0 Then
    '                        If dtIdentityTran.Rows.Count > 0 Then
    '                            objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                            objIdentitytran._DataList = dtIdentityTran
    '                            If objIdentitytran.ImportEmployeeIdentity(User._Object._Userunkid) Then
    '                                dtMRow.Item("image") = imgAccept
    '                                dtMRow.Item("message") = ""
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                dtMRow.Item("objStatus") = 1
    '                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                            Else
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            Next
    '        End If

    '        'APPROVED EMPLOYEE - DATA WILL GO DIRECTLY INTO MAIN TABLE ONLY IF SKIP APPROVAL FLOW IS ON ELSE DATA WILL GO TO APPROVAL TABLE
    '        If aRow IsNot Nothing AndAlso aRow.Count > 0 Then
    '            For Each dtMRow As DataRow In aRow
    '                Application.DoEvents()

    '                If mdicEmpAdded.ContainsKey(dtMRow.Item("employeecode").ToString.Trim) = False Then Continue For
    '                If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                    objAIdInfoTran = New clsIdentity_Approval_tran
    '                    objAIdInfoTran._Employeeunkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                    dtIdentityTran = objAIdInfoTran._DataTable.Copy
    '                Else
    '                    objIdentitytran = New clsIdentity_tran
    '                    objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                    dtIdentityTran = objIdentitytran._DataList.Copy
    '                End If
    '                Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable
    '                If dtFilter.Rows.Count > 0 Then
    '                    Dim intAssignedEmpId As Integer = -1
    '                    For Each dtRow As DataRow In dtFilter.Rows

    '                        dtMRow("identityno") = dtRow.Item(cboIdentityNo.Text).ToString.Trim
    '                        dtMRow("isdefault") = CBool(dtRow.Item(cboIsDefault.Text))
    '                        dtMRow("issued_place") = dtRow.Item(cboPlaceofIssue.Text).ToString.Trim
    '                        dtMRow("dl_class") = dtRow.Item(cboDLClass.Text).ToString.Trim

    '                        Dim dtERow As DataRow = dtIdentityTran.NewRow
    '                        If dtRow.Item(cboIdentityType.Text).ToString.Trim.Length > 0 Then
    '                            intIdentityTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, dtRow.Item(cboIdentityType.Text).ToString.Trim)
    '                            If intIdentityTypeId <= 0 Then
    '                                If Not objCMaster Is Nothing Then objCMaster = Nothing
    '                                objCMaster = New clsCommon_Master
    '                                If dtRow.Item(cboIdentityType.Text).ToString.Trim.Length > 5 Then
    '                                    objCMaster._Alias = dtRow.Item(cboIdentityType.Text).ToString.Substring(0, 4).Trim
    '                                Else
    '                                    objCMaster._Alias = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                                End If
    '                                objCMaster._Code = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                                objCMaster._Isactive = True
    '                                objCMaster._Mastertype = clsCommon_Master.enCommonMaster.IDENTITY_TYPES
    '                                objCMaster._Name = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                                If objCMaster.Insert = False Then
    '                                    dtMRow.Item("image") = imgWarring
    '                                    dtMRow.Item("message") = objCMaster._Message
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                    dtMRow.Item("objStatus") = 0
    '                                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                                End If
    '                                intIdentityTypeId = objCMaster._Masterunkid
    '                                dtMRow("identitytype") = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                            Else
    '                                dtMRow("identitytype") = dtRow.Item(cboIdentityType.Text).ToString.Trim
    '                            End If
    '                        End If
    '                        If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                            intAssignedEmpId = objAIdInfoTran.GetEmployeeIdentityUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), intIdentityTypeId)
    '                        Else
    '                            intAssignedEmpId = objIdentitytran.GetEmployeeIdentityUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), intIdentityTypeId, dtRow.Item(cboIdentityNo.Text).ToString.Trim)
    '                        End If
    '                        If intAssignedEmpId <= 0 Then
    '                            dtERow.Item("identitytranunkid") = -1
    '                            dtERow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                            dtERow.Item("idtypeunkid") = intIdentityTypeId
    '                            dtERow.Item("identity_no") = dtRow.Item(cboIdentityNo.Text).ToString.Trim
    '                            dtERow.Item("isdefault") = CBool(dtRow.Item(cboIsDefault.Text))
    '                            If cboCountry.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboCountry.Text).ToString.Trim.Length > 0 Then
    '                                    Dim objCountry As New clsMasterData
    '                                    dtERow.Item("countryunkid") = objCountry.GetCountryUnkId(dtRow.Item(cboCountry.Text).ToString.Trim)
    '                                    dtMRow("country") = dtRow.Item(cboCountry.Text).ToString.Trim
    '                                Else
    '                                    dtERow.Item("countryunkid") = 0
    '                                End If
    '                            Else
    '                                dtERow.Item("countryunkid") = 0
    '                            End If
    '                            If cboPlaceofIssue.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboPlaceofIssue.Text).ToString Is Nothing OrElse dtRow.Item(cboPlaceofIssue.Text).ToString = "" Then
    '                                        dtERow.Item("issued_place") = ""
    '                                    Else
    '                                        dtERow.Item("issued_place") = dtRow.Item(cboPlaceofIssue.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("issued_place") = ""
    '                                End If
    '                            Else
    '                                dtERow.Item("issued_place") = ""
    '                            End If
    '                            If cboDLClass.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboDLClass.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboDLClass.Text).ToString.Trim Is Nothing OrElse dtRow.Item(cboDLClass.Text).ToString.Trim = "" Then
    '                                        dtERow.Item("dl_class") = ""
    '                                    Else
    '                                        dtERow.Item("dl_class") = dtRow.Item(cboDLClass.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("dl_class") = ""
    '                                End If
    '                            Else
    '                                dtERow.Item("dl_class") = ""
    '                            End If
    '                            If cboIssueDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("issue_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                        dtMRow("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("issue_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("issue_date") = DBNull.Value
    '                            End If
    '                            If cboExpiryDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboExpiryDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("expiry_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                        dtMRow("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("expiry_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("expiry_date") = DBNull.Value
    '                            End If
    '                            dtERow.Item("AUD") = "A"
    '                            If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                                dtERow.Item("tranguid") = Guid.NewGuid.ToString()
    '                                dtERow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
    '                                dtERow.Item("mappingunkid") = 0
    '                                dtERow.Item("approvalremark") = ""
    '                                dtERow.Item("isfinal") = False
    '                                dtERow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
    '                                dtERow.Item("loginemployeeunkid") = 0
    '                                dtERow.Item("isvoid") = False
    '                                dtERow.Item("voidreason") = ""
    '                                dtERow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                                dtERow.Item("audittype") = enAuditType.ADD
    '                                dtERow.Item("audituserunkid") = User._Object._Userunkid
    '                                dtERow.Item("ip") = getIP()
    '                                dtERow.Item("host") = getHostName()
    '                                dtERow.Item("form_name") = mstrModuleName
    '                                dtERow.Item("isweb") = False
    '                                dtERow.Item("isprocessed") = False
    '                                dtERow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.ADDED
    '                                dtERow.Item("identities") = dtRow.Item(cboIdentityType.Text).ToString
    '                                dtERow.Item("country") = dtRow.Item(cboCountry.Text).ToString
    '                                dtERow.Item("serial_no") = dtRow.Item(cboSerialNo.Text).ToString
    '                            Else
    '                                dtERow.Item("GUID") = Guid.NewGuid.ToString
    '                            End If
    '                            dtIdentityTran.Rows.Add(dtERow)
    '                        Else
    '                            dtMRow.Item("image") = imgWarring

    '                            'Gajanan [27-May-2019] -- Start              
    '                            'dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
    '                            If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 24, "Employee Identity Aready Exist In Approval Process")
    '                            Else
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
    '                            End If
    '                            'Gajanan [27-May-2019] -- End

    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 0
    '                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                        End If
    '                    Next

    '                    If intAssignedEmpId <= 0 Then
    '                        If dtIdentityTran.Rows.Count > 0 Then
    '                            If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then


    '                                'Gajanan [27-May-2019] -- Start              
    '                                objApprovalData = New clsEmployeeDataApproval
    '                                'Gajanan [27-May-2019] -- End


    '                                'Gajanan [17-April-2019] -- Start
    '                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                                If objApprovalData.IsApproverPresent(enScreenName.frmIdentityInfoList, FinancialYear._Object._DatabaseName, _
    '                                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
    '                                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
    '                                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(dtMRow("employeeid").ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

    '                                    dtMRow.Item("image") = imgWarring
    '                                    dtMRow.Item("message") = objApprovalData._Message
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
    '                                    dtMRow.Item("objStatus") = 2
    '                                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                                    Continue For
    '                                End If
    '                                'Gajanan [17-April-2019] -- End



    '                                'S.SANDEEP |26-APR-2019| -- START
    '                                'If objAIdInfoTran.ImportEmployeeIdentity(dtIdentityTran, User._Object._Userunkid) Then
    '                                If objAIdInfoTran.ImportEmployeeIdentity(dtIdentityTran, User._Object._Userunkid, Company._Object._Companyunkid) Then
    '                                    'S.SANDEEP |26-APR-2019| -- END
    '                                    dtMRow.Item("image") = imgAccept
    '                                    dtMRow.Item("message") = ""
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                    dtMRow.Item("objStatus") = 1
    '                                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)


    '                                    'Gajanan [27-May-2019] -- Start   

    '                                    If EmpList.Contains(dtMRow("employeeid").ToString()) = False Then
    '                                        EmpList.Add(dtMRow("employeeid").ToString())
    '                                    End If

    '                                    'objApprovalData = New clsEmployeeDataApproval
    '                                    'objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
    '                                    '                        ConfigParameter._Object._UserAccessModeSetting, _
    '                                    '                        Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
    '                                    '                        CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
    '                                    '                        enScreenName.frmIdentityInfoList, ConfigParameter._Object._EmployeeAsOnDate, _
    '                                    '                        User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
    '                                    '                        User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , dtMRow("employeeid").ToString(), , , _
    '                                    '                         "employeeunkid= " & dtMRow("employeeid").ToString, dtIdentityTran, , )
    '                                    'Gajanan [27-May-2019] -- End

    '                                Else
    '                                    dtMRow.Item("image") = imgError
    '                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                    dtMRow.Item("objStatus") = 2
    '                                    objError.Text = CStr(Val(objError.Text) + 1)
    '                                End If
    '                            Else
    '                                objIdentitytran._EmployeeUnkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                                objIdentitytran._DataList = dtIdentityTran
    '                                If objIdentitytran.ImportEmployeeIdentity(User._Object._Userunkid) Then
    '                                    dtMRow.Item("image") = imgAccept
    '                                    dtMRow.Item("message") = ""
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                    dtMRow.Item("objStatus") = 1
    '                                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                                Else
    '                                    dtMRow.Item("image") = imgError
    '                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                    dtMRow.Item("objStatus") = 2
    '                                    objError.Text = CStr(Val(objError.Text) + 1)
    '                                End If
    '                            End If
    '                        End If
    '                    End If

    '                End If
    '            Next




    '            'Gajanan [27-May-2019] -- Start
    '            If IdentityApprovalFlowVal Is Nothing Then
    '                objApprovalData = New clsEmployeeDataApproval
    '                If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 10, "Fail") & "'").Count > 0 Then
    '                    If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
    '                        Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
    '                        mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 10, "Fail") & "'"
    '                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
    '                                                                   ConfigParameter._Object._UserAccessModeSetting, _
    '                                                                   Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
    '                                                                   enUserPriviledge.AllowToApproveRejectEmployeeIdentities, _
    '                                                                   enScreenName.frmIdentityInfoList, ConfigParameter._Object._EmployeeAsOnDate, _
    '                                                                   User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
    '                                                                   User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
    '                                                                   User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
    '                                                                   ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
    '                    End If
    '                End If
    '            End If
    '            'Gajanan [27-May-2019] -- End


    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
           
            Dim objCMaster As New clsCommon_Master
            Dim objIdentitytran As clsIdentity_tran = Nothing
            Dim dtIdentityTran As New DataTable
            Dim mdtTran As New DataTable
            Dim intIdentityTypeId As Integer = -1

            Dim dtApprovedEmp, dtPendingEmp As DataTable

            Dim EmpList As New List(Of String)

            Dim pRow As IEnumerable(Of DataRow) = Nothing : Dim aRow As IEnumerable(Of DataRow) = Nothing
            If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False).Count > 0 Then
                pRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False)
            End If
            If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True).Count > 0 Then
                aRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True)
            End If

            'PENDING EMPLOYEE - DATA WILL GO DIRECTLY INTO MAIN TABLE
            If pRow IsNot Nothing AndAlso pRow.Count > 0 Then
                For Each dtMRow As DataRow In pRow

                    Application.DoEvents()

                    'S.SANDEEP |06-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    If dtMRow("message").ToString() <> "" Then Continue For
                    'S.SANDEEP |06-SEP-2019| -- END


                objIdentitytran = New clsIdentity_tran
                    objIdentitytran._EmployeeUnkid = CInt(dtMRow.Item("employeeid").ToString.Trim)
                dtIdentityTran = objIdentitytran._DataList.Copy


                    Dim intAssignedEmpId As Integer = -1

                    dtMRow("identityno") = dtMRow.Item(cboIdentityNo.Text).ToString.Trim
                    dtMRow("isdefault") = CBool(dtMRow.Item(cboIsDefault.Text))
                    If cboPlaceofIssue.Text.Trim.Length > 0 Then
                        If dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
                            dtMRow("PlaceofIssue") = dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim
                        Else
                            dtMRow("PlaceofIssue") = ""
                        End If
                    Else
                        dtMRow("PlaceofIssue") = ""
                    End If

                    If cboDLClass.Text.Trim.Length > 0 Then
                        If dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
                    dtMRow("dlclass") = dtMRow.Item(cboDLClass.Text).ToString.Trim
                        Else
                            dtMRow("dlclass") = ""
                        End If
                    Else
                        dtMRow("dlclass") = ""
                    End If

                        Dim dtERow As DataRow = dtIdentityTran.NewRow
                    If dtMRow.Item(cboIdentityType.Text).ToString.Trim.Length > 0 Then
                        intIdentityTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, dtMRow.Item(cboIdentityType.Text).ToString.Trim)
                            If intIdentityTypeId <= 0 Then
                                If Not objCMaster Is Nothing Then objCMaster = Nothing
                                objCMaster = New clsCommon_Master
                            If dtMRow.Item(cboIdentityType.Text).ToString.Trim.Length > 5 Then
                                objCMaster._Alias = dtMRow.Item(cboIdentityType.Text).ToString.Substring(0, 4).Trim
                                Else
                                objCMaster._Alias = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                                End If
                            objCMaster._Code = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                                objCMaster._Isactive = True
                                objCMaster._Mastertype = clsCommon_Master.enCommonMaster.IDENTITY_TYPES
                            objCMaster._Name = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                                If objCMaster.Insert = False Then
                                    dtMRow.Item("image") = imgWarring
                                    dtMRow.Item("message") = objCMaster._Message
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 0
                                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                End If
                            dtMRow("identitytype") = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                                intIdentityTypeId = objCMaster._Masterunkid
                            End If
                        End If
                    intAssignedEmpId = objIdentitytran.GetEmployeeIdentityUnkid(CInt(dtMRow.Item("employeeid").ToString.Trim), intIdentityTypeId, dtMRow.Item(cboIdentityNo.Text).ToString.Trim)

                        If intAssignedEmpId <= 0 Then
                            dtERow.Item("identitytranunkid") = -1
                        dtERow.Item("employeeunkid") = CInt(dtMRow("employeeid").ToString.Trim)
                            dtERow.Item("idtypeunkid") = intIdentityTypeId
                        dtERow.Item("identity_no") = dtMRow.Item(cboIdentityNo.Text).ToString.Trim
                        dtERow.Item("isdefault") = CBool(dtMRow.Item(cboIsDefault.Text))
                            If cboCountry.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboCountry.Text).ToString.Trim.Length > 0 Then
                                    Dim objCountry As New clsMasterData
                                dtERow.Item("countryunkid") = objCountry.GetCountryUnkId(dtMRow.Item(cboCountry.Text).ToString.Trim)
                                dtMRow("country") = dtMRow.Item(cboCountry.Text).ToString.Trim
                                Else
                                    dtERow.Item("countryunkid") = 0
                                End If
                            Else
                                dtERow.Item("countryunkid") = 0
                            End If


                            If cboPlaceofIssue.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboPlaceofIssue.Text).ToString Is Nothing OrElse dtMRow.Item(cboPlaceofIssue.Text).ToString = "" Then
                                        dtERow.Item("issued_place") = ""
                                    Else
                                    dtERow.Item("issued_place") = dtMRow.Item(cboPlaceofIssue.Text)
                                    End If
                                Else
                                    dtERow.Item("issued_place") = ""
                                End If
                            Else
                                dtERow.Item("issued_place") = ""
                            End If


                            If cboDLClass.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboDLClass.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboDLClass.Text).ToString.Trim Is Nothing OrElse dtMRow.Item(cboDLClass.Text).ToString.Trim = "" Then
                                        dtERow.Item("dl_class") = ""
                                    Else
                                    dtERow.Item("dl_class") = dtMRow.Item(cboDLClass.Text)
                                    End If
                                Else
                                    dtERow.Item("dl_class") = ""
                                End If
                            Else
                                dtERow.Item("dl_class") = ""
                            End If
                            If cboIssueDate.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                        dtERow.Item("issue_date") = DBNull.Value
                                    Else
                                    dtERow.Item("issue_date") = dtMRow.Item(cboIssueDate.Text)
                                    dtMRow("issuedate") = dtMRow.Item(cboIssueDate.Text)
                                    End If
                                Else
                                    dtERow.Item("issue_date") = DBNull.Value
                                End If
                            Else
                                dtERow.Item("issue_date") = DBNull.Value
                            End If
                            If cboExpiryDate.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboExpiryDate.Text).ToString = "0000-00-00" Then
                                        dtERow.Item("expiry_date") = DBNull.Value
                                    Else
                                    dtERow.Item("expiry_date") = dtMRow.Item(cboExpiryDate.Text)
                                    dtMRow("expirydate") = dtMRow.Item(cboExpiryDate.Text)
                                    End If
                                Else
                                    dtERow.Item("expiry_date") = DBNull.Value
                                End If
                            Else
                                dtERow.Item("expiry_date") = DBNull.Value
                            End If
                            dtERow.Item("AUD") = "A"
                            dtERow.Item("GUID") = Guid.NewGuid.ToString
                            dtIdentityTran.Rows.Add(dtERow)
                        Else
                            dtMRow.Item("image") = imgWarring
                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                            dtMRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        End If


                    If intAssignedEmpId <= 0 Then
                        If dtIdentityTran.Rows.Count > 0 Then
                            objIdentitytran._EmployeeUnkid = CInt(dtMRow("employeeid").ToString.Trim)
                            objIdentitytran._DataList = dtIdentityTran
                            If objIdentitytran.ImportEmployeeIdentity(User._Object._Userunkid) Then
                                dtMRow.Item("image") = imgAccept
                                dtMRow.Item("message") = ""
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
                                dtMRow.Item("objStatus") = 1
                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            Else
                                dtMRow.Item("image") = imgError
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                            End If
                        End If
                    End If


            Next
            End If

            'APPROVED EMPLOYEE - DATA WILL GO DIRECTLY INTO MAIN TABLE ONLY IF SKIP APPROVAL FLOW IS ON ELSE DATA WILL GO TO APPROVAL TABLE
            If aRow IsNot Nothing AndAlso aRow.Count > 0 Then
                For Each dtMRow As DataRow In aRow
                    Application.DoEvents()

                    'S.SANDEEP |06-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    If dtMRow("message").ToString() <> "" Then Continue For
                    'S.SANDEEP |06-SEP-2019| -- END


                If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
                    objAIdInfoTran = New clsIdentity_Approval_tran
                        objAIdInfoTran._Employeeunkid = CInt(dtMRow.Item("employeeid").ToString.Trim)
                    dtIdentityTran = objAIdInfoTran._DataTable.Copy
                Else
                    objIdentitytran = New clsIdentity_tran
                        objIdentitytran._EmployeeUnkid = CInt(dtMRow.Item("employeeid").ToString.Trim)
                    dtIdentityTran = objIdentitytran._DataList.Copy
                End If


                    Dim intAssignedEmpId As Integer = -1

                    dtMRow("identityno") = dtMRow.Item(cboIdentityNo.Text).ToString.Trim
                    dtMRow("isdefault") = CBool(dtMRow.Item(cboIsDefault.Text))
                    If cboPlaceofIssue.Text.Trim.Length > 0 Then
                        If dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
                    dtMRow("PlaceofIssue") = dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim
                        Else
                            dtMRow("PlaceofIssue") = ""
                        End If
                    Else
                        dtMRow("PlaceofIssue") = ""
                    End If

                    If cboDLClass.Text.Trim.Length > 0 Then
                        If dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
                    dtMRow("dlclass") = dtMRow.Item(cboDLClass.Text).ToString.Trim
                        Else
                            dtMRow("dlclass") = ""
                        End If
                    Else
                        dtMRow("dlclass") = ""
                    End If



                        Dim dtERow As DataRow = dtIdentityTran.NewRow
                    If dtMRow.Item(cboIdentityType.Text).ToString.Trim.Length > 0 Then
                        intIdentityTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, dtMRow.Item(cboIdentityType.Text).ToString.Trim)
                            If intIdentityTypeId <= 0 Then
                                If Not objCMaster Is Nothing Then objCMaster = Nothing
                                objCMaster = New clsCommon_Master
                            If dtMRow.Item(cboIdentityType.Text).ToString.Trim.Length > 5 Then
                                objCMaster._Alias = dtMRow.Item(cboIdentityType.Text).ToString.Substring(0, 4).Trim
                                Else
                                objCMaster._Alias = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                                End If
                            objCMaster._Code = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                                objCMaster._Isactive = True
                                objCMaster._Mastertype = clsCommon_Master.enCommonMaster.IDENTITY_TYPES
                            objCMaster._Name = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                                If objCMaster.Insert = False Then
                                    dtMRow.Item("image") = imgWarring
                                    dtMRow.Item("message") = objCMaster._Message
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 0
                                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                End If
                                intIdentityTypeId = objCMaster._Masterunkid
                            dtMRow("identitytype") = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                        Else
                            dtMRow("identitytype") = dtMRow.Item(cboIdentityType.Text).ToString.Trim
                            End If
                        End If
                        If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
                        intAssignedEmpId = objAIdInfoTran.GetEmployeeIdentityUnkid(CInt(dtMRow.Item("employeeid").ToString.Trim), intIdentityTypeId)
                        Else
                        intAssignedEmpId = objIdentitytran.GetEmployeeIdentityUnkid(CInt(dtMRow.Item("employeeid").ToString.Trim), intIdentityTypeId, dtMRow.Item(cboIdentityNo.Text).ToString.Trim)
                        End If
                        If intAssignedEmpId <= 0 Then
                            dtERow.Item("identitytranunkid") = -1
                        dtERow.Item("employeeunkid") = CInt(dtMRow("employeeid").ToString.Trim)
                            dtERow.Item("idtypeunkid") = intIdentityTypeId
                        dtERow.Item("identity_no") = dtMRow.Item(cboIdentityNo.Text).ToString.Trim
                        dtERow.Item("isdefault") = CBool(dtMRow.Item(cboIsDefault.Text))
                            If cboCountry.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboCountry.Text).ToString.Trim.Length > 0 Then
                                    Dim objCountry As New clsMasterData
                                dtERow.Item("countryunkid") = objCountry.GetCountryUnkId(dtMRow.Item(cboCountry.Text).ToString.Trim)
                                dtMRow("country") = dtMRow.Item(cboCountry.Text).ToString.Trim
                                Else
                                    dtERow.Item("countryunkid") = 0
                                End If
                            Else
                                dtERow.Item("countryunkid") = 0
                            End If
                            If cboPlaceofIssue.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboPlaceofIssue.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboPlaceofIssue.Text).ToString Is Nothing OrElse dtMRow.Item(cboPlaceofIssue.Text).ToString = "" Then
                                        dtERow.Item("issued_place") = ""
                                    Else
                                    dtERow.Item("issued_place") = dtMRow.Item(cboPlaceofIssue.Text)
                                    End If
                                Else
                                    dtERow.Item("issued_place") = ""
                                End If
                            Else
                                dtERow.Item("issued_place") = ""
                            End If
                            If cboDLClass.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboDLClass.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboDLClass.Text).ToString.Trim Is Nothing OrElse dtMRow.Item(cboDLClass.Text).ToString.Trim = "" Then
                                        dtERow.Item("dl_class") = ""
                                    Else
                                    dtERow.Item("dl_class") = dtMRow.Item(cboDLClass.Text)
                                    End If
                                Else
                                    dtERow.Item("dl_class") = ""
                                End If
                            Else
                                dtERow.Item("dl_class") = ""
                            End If
                            If cboIssueDate.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                        dtERow.Item("issue_date") = DBNull.Value
                                    Else
                                    dtERow.Item("issue_date") = dtMRow.Item(cboIssueDate.Text)
                                    dtMRow("IssueDate") = dtMRow.Item(cboIssueDate.Text)
                                    End If
                                Else
                                    dtERow.Item("issue_date") = DBNull.Value
                                End If
                            Else
                                dtERow.Item("issue_date") = DBNull.Value
                            End If
                            If cboExpiryDate.Text.Trim.Length > 0 Then
                            If dtMRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
                                If dtMRow.Item(cboExpiryDate.Text).ToString = "0000-00-00" Then
                                        dtERow.Item("expiry_date") = DBNull.Value
                                    Else
                                    dtERow.Item("expiry_date") = dtMRow.Item(cboExpiryDate.Text)
                                    dtMRow("ExpiryDate") = dtMRow.Item(cboExpiryDate.Text)
                                    End If
                                Else
                                    dtERow.Item("expiry_date") = DBNull.Value
                                End If
                            Else
                                dtERow.Item("expiry_date") = DBNull.Value
                            End If
                            dtERow.Item("AUD") = "A"
                            If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
                                dtERow.Item("tranguid") = Guid.NewGuid.ToString()
                                dtERow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                                dtERow.Item("mappingunkid") = 0
                                dtERow.Item("approvalremark") = ""
                                dtERow.Item("isfinal") = False
                                dtERow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                                dtERow.Item("loginemployeeunkid") = 0
                                dtERow.Item("isvoid") = False
                                dtERow.Item("voidreason") = ""
                                dtERow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                                dtERow.Item("audittype") = enAuditType.ADD
                                dtERow.Item("audituserunkid") = User._Object._Userunkid
                                dtERow.Item("ip") = getIP()
                                dtERow.Item("host") = getHostName()
                                dtERow.Item("form_name") = mstrModuleName
                                dtERow.Item("isweb") = False
                                dtERow.Item("isprocessed") = False
                                dtERow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.ADDED
                            dtERow.Item("identities") = dtMRow.Item(cboIdentityType.Text).ToString
                            If cboCountry.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboCountry.Text).ToString.Trim.Length > 0 Then
                            dtERow.Item("country") = dtMRow.Item(cboCountry.Text).ToString
                                Else
                                    dtERow.Item("country") = ""
                                End If
                            Else
                                dtERow.Item("country") = ""
                            End If
                            If cboSerialNo.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboSerialNo.Text).ToString.Trim.Length > 0 Then
                            dtERow.Item("serial_no") = dtMRow.Item(cboSerialNo.Text).ToString
                            Else
                                    dtERow.Item("serial_no") = ""
                                End If
                            Else
                                dtERow.Item("serial_no") = ""
                            End If                        
                        Else
                            dtERow.Item("GUID") = Guid.NewGuid.ToString
                            End If
                            dtIdentityTran.Rows.Add(dtERow)
                        Else
                            dtMRow.Item("image") = imgWarring

                        'Gajanan [27-May-2019] -- Start              
                        'dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
                        If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 24, "Employee Identity Aready Exist In Approval Process")
                        Else
                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
                        End If
                        'Gajanan [27-May-2019] -- End

                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                            dtMRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        End If

                    If intAssignedEmpId <= 0 Then
                        If dtIdentityTran.Rows.Count > 0 Then
                            If IdentityApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then


                                objApprovalData = New clsEmployeeDataApproval

                                    If objApprovalData.IsApproverPresent(enScreenName.frmIdentityInfoList, FinancialYear._Object._DatabaseName, _
                                                                          ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                                          FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
                                                                          User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(dtMRow("employeeid").ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                                        dtMRow.Item("image") = imgWarring
                                        dtMRow.Item("message") = objApprovalData._Message
                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                        dtMRow.Item("objStatus") = 2
                                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                        Continue For
                                    End If

                                If objAIdInfoTran.ImportEmployeeIdentity(dtIdentityTran, User._Object._Userunkid, Company._Object._Companyunkid) Then
                                    dtMRow.Item("image") = imgAccept
                                    dtMRow.Item("message") = ""
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
                                    dtMRow.Item("objStatus") = 1
                                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)



                                    If EmpList.Contains(dtMRow("employeeid").ToString()) = False Then
                                        EmpList.Add(dtMRow("employeeid").ToString())
                                    End If

                                Else
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                End If
                            Else
                                objIdentitytran._EmployeeUnkid = CInt(dtMRow("employeeid").ToString.Trim)
                            objIdentitytran._DataList = dtIdentityTran
                            If objIdentitytran.ImportEmployeeIdentity(User._Object._Userunkid) Then
                                dtMRow.Item("image") = imgAccept
                                dtMRow.Item("message") = ""
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
                                dtMRow.Item("objStatus") = 1
                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            Else
                                dtMRow.Item("image") = imgError
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                            End If
                        End If
                    End If
                End If


                Next




                'Gajanan [27-May-2019] -- Start
                If IdentityApprovalFlowVal Is Nothing Then
                    objApprovalData = New clsEmployeeDataApproval
                    If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 10, "Fail") & "'").Count > 0 Then
                        If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                            Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                            mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 10, "Fail") & "'"
                            objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                       enUserPriviledge.AllowToApproveRejectEmployeeIdentities, _
                                                                       enScreenName.frmIdentityInfoList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                       User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                       User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                                       User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                                       ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                End If
                    End If
                End If
                'Gajanan [27-May-2019] -- End


            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    'Gajanan [22-Feb-2019] -- End
    'Gajanan [27-May-2019] -- End


    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known

            'For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
            '    For Each ctrl As Control In gbFiledMapping.Controls
            '        If TypeOf ctrl Is ComboBox Then
            '            CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
            '        End If
            '    Next
            'Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    If TypeOf ctrl Is ComboBox Then
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
                    End If
                Next

                If dtColumns.ColumnName = Language.getMessage(mstrModuleName, 13, "EmployeeCode") Then
                    dtColumns.Caption = "EmployeeCode"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 14, "IdentityType") Then
                    dtColumns.Caption = "IdentityType"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 15, "IdentityNo") Then
                    dtColumns.Caption = "IdentityNo"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 16, "IsDefault") Then
                    dtColumns.Caption = "IsDefault"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 17, "IdentityType") Then
                    dtColumns.Caption = "IdentityType"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 18, "Country") Then
                    dtColumns.Caption = "Country"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 19, "SerialNo") Then
                    dtColumns.Caption = "SerialNo"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 20, "PlaceofIssue") Then
                    dtColumns.Caption = "PlaceofIssue"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 21, "DLClass") Then
                    dtColumns.Caption = "DLClass"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 22, "IssueDate") Then
                    dtColumns.Caption = "IssueDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 23, "ExpiryDate") Then
                    dtColumns.Caption = "ExpiryDate"
                End If
            Next
            mds_ImportData.AcceptChanges()

            'Gajanan (24 Nov 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    dtTable.Columns.Remove("employeeid")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Membership Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim dtExTable As New DataTable("DataList")
            With dtExTable
                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB

                '.Columns.Add("EmployeeCode", System.Type.GetType("System.String"))
                '.Columns.Add("IdentityType", System.Type.GetType("System.String"))
                '.Columns.Add("IdentityNo", System.Type.GetType("System.String"))
                '.Columns.Add("IsDefault", System.Type.GetType("System.String"))
                '.Columns.Add("Country", System.Type.GetType("System.String"))
                '.Columns.Add("SerialNo", System.Type.GetType("System.String"))
                '.Columns.Add("PlaceofIssue", System.Type.GetType("System.String"))
                '.Columns.Add("DLClass", System.Type.GetType("System.String"))
                '.Columns.Add("IssueDate", System.Type.GetType("System.String"))
                '.Columns.Add("ExpiryDate", System.Type.GetType("System.String"))

                .Columns.Add(Language.getMessage(mstrModuleName, 13, "EmployeeCode"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 14, "IdentityType"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 15, "IdentityNo"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 16, "IsDefault"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 18, "Country"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 19, "SerialNo"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 20, "PlaceofIssue"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 21, "DLClass"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 22, "IssueDate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 23, "ExpiryDate"), System.Type.GetType("System.String"))
                'Gajanan (24 Nov 2018) -- End

            End With
            Dim dsList As New DataSet
            dsList.Tables.Add(dtExTable.Copy)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, dsList)
            End If
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 67, "Template Exported Successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.WizImportIdentity.CancelText = Language._Object.getCaption(Me.WizImportIdentity.Name & "_CancelText" , Me.WizImportIdentity.CancelText)
			Me.WizImportIdentity.NextText = Language._Object.getCaption(Me.WizImportIdentity.Name & "_NextText" , Me.WizImportIdentity.NextText)
			Me.WizImportIdentity.BackText = Language._Object.getCaption(Me.WizImportIdentity.Name & "_BackText" , Me.WizImportIdentity.BackText)
			Me.WizImportIdentity.FinishText = Language._Object.getCaption(Me.WizImportIdentity.Name & "_FinishText" , Me.WizImportIdentity.FinishText)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblIdentityType.Text = Language._Object.getCaption(Me.lblIdentityType.Name, Me.lblIdentityType.Text)
            Me.lblIsDefault.Text = Language._Object.getCaption(Me.lblIsDefault.Name, Me.lblIsDefault.Text)
            Me.lblIdentityNo.Text = Language._Object.getCaption(Me.lblIdentityNo.Name, Me.lblIdentityNo.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.LblSerialNo.Text = Language._Object.getCaption(Me.LblSerialNo.Name, Me.LblSerialNo.Text)
            Me.LblCountry.Text = Language._Object.getCaption(Me.LblCountry.Name, Me.LblCountry.Text)
            Me.lblExpiryDate.Text = Language._Object.getCaption(Me.lblExpiryDate.Name, Me.lblExpiryDate.Text)
            Me.lblIssueDate.Text = Language._Object.getCaption(Me.lblIssueDate.Name, Me.lblIssueDate.Text)
            Me.LblDLClass.Text = Language._Object.getCaption(Me.LblDLClass.Name, Me.LblDLClass.Text)
            Me.LblPlaceofIssue.Text = Language._Object.getCaption(Me.LblPlaceofIssue.Name, Me.LblPlaceofIssue.Text)
			Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code cannot be blank. Please set the Employee Code to import employee identities.")
            Language.setMessage(mstrModuleName, 2, "Identity Type cannot be blank. Please set the Identity Type to import employee identities.")
            Language.setMessage(mstrModuleName, 3, "Identity No cannot be blank. Please set the Identity No to import employee identities.")
            Language.setMessage(mstrModuleName, 5, "Is Default cannot be blank. Please set the Is Default to import employee identities.")
            Language.setMessage(mstrModuleName, 6, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 7, "Employee Identity Aready Exist")
            Language.setMessage(mstrModuleName, 9, "Invalid Data")
            Language.setMessage(mstrModuleName, 10, "Fail")
            Language.setMessage(mstrModuleName, 11, "Success")
            Language.setMessage(mstrModuleName, 12, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 13, "EmployeeCode")
			Language.setMessage(mstrModuleName, 14, "IdentityType")
			Language.setMessage(mstrModuleName, 15, "IdentityNo")
			Language.setMessage(mstrModuleName, 16, "IsDefault")
            Language.setMessage(mstrModuleName, 17, "Employee Not Found.")
			Language.setMessage(mstrModuleName, 18, "Country")
			Language.setMessage(mstrModuleName, 19, "SerialNo")
			Language.setMessage(mstrModuleName, 20, "PlaceofIssue")
			Language.setMessage(mstrModuleName, 21, "DLClass")
			Language.setMessage(mstrModuleName, 22, "IssueDate")
			Language.setMessage(mstrModuleName, 23, "ExpiryDate")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 67, "Template Exported Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class