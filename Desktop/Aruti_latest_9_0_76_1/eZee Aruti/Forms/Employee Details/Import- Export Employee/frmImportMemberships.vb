﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportMemberships

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportMemberships"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim mdicEmpAdded As New Dictionary(Of String, Integer)

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'S.SANDEEP |22-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Training requisition UAT]
    Private objAMemInfoTran As New clsMembership_Approval_Tran
    Private Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Private MembershipApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmMembershipInfoList)))
    Private mdtApprovalMemTran As DataTable
    'S.SANDEEP |22-MAY-2019| -- END
#End Region

#Region " From's Events "

    Private Sub frmImportMemberships_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End

            'S.SANDEEP [ 02 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkCopyPreviousEDSlab.Checked = False
            'S.SANDEEP [ 02 JAN 2013 ] -- END

            txtFilePath.BackColor = GUI.ColorComp

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                objlblSign7.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                lblEffPeriod.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                cboEffectivePeriod.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                chkCopyPreviousEDSlab.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                chkOverwrite.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                chkOverwritePrevEDSlabHeads.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportMemberships_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " eZee Wizard "

    Private Sub WizImportMembership_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizImportMembership.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizImportMembership.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportMembership_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub WizImportMembership_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizImportMembership.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizImportMembership.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()

                Case WizImportMembership.Pages.IndexOf(wizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    'S.SANDEEP [19-JUL-2018] -- START
                                    Case cboIssueDate.Name.ToUpper, cboStartDate.Name.ToUpper, cboExpiryDate.Name.ToUpper
                                        'Case "CBOISSUEDATE", "CBOSTARTDATE", "CBOEXPIRYDATE"
                                        'S.SANDEEP [19-JUL-2018] -- END
                                        Continue For
                                    Case Else
                                        'S.SANDEEP [ 15 April 2013 ] -- START
                                        'ENHANCEMENT : LICENSE CHANGES

                                        'S.SANDEEP [19-JUL-2018] -- START
                                        If CType(ctrl, ComboBox).Name.ToUpper = cboEffectivePeriod.Name.ToUpper Then
                                            'If CType(ctrl, ComboBox).Name.ToUpper = "CBOEFFECTIVEPERIOD" Then
                                            'S.SANDEEP [19-JUL-2018] -- END
                                            If ConfigParameter._Object._IsArutiDemo = False Then
                                                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                                                    Continue For
                                                End If
                                            End If
                                        End If
                                        'S.SANDEEP [ 15 April 2013 ] -- END
                                        If CType(ctrl, ComboBox).Text = "" Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If

                Case WizImportMembership.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportMembership_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Dim objEmployee As New clsEmployee_Master
        Try
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("displayname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("employeeid", System.Type.GetType("System.Int32"))
            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))


            'Gajanan [27-May-2019] -- Start     
            mdt_ImportData_Others.Columns.Add("MembershipCategory", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("MembershipName", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("MembershipNo", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("IssueDate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("StartDate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("ExpiryDate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("EffectivePeriod", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- End


            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
            'S.SANDEEP |22-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Training requisition UAT]
            mdt_ImportData_Others.Columns.Add("objIsApproved", GetType(System.Boolean))
            'S.SANDEEP |22-MAY-2019| -- END

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData_Others.Columns("MembershipCategory").ExtendedProperties.Add("col", "MembershipCategory")
            mdt_ImportData_Others.Columns("MembershipName").ExtendedProperties.Add("col", "MembershipName")
            mdt_ImportData_Others.Columns("MembershipNo").ExtendedProperties.Add("col", "MembershipNo")
            mdt_ImportData_Others.Columns("IssueDate").ExtendedProperties.Add("col", "IssueDate")
            mdt_ImportData_Others.Columns("StartDate").ExtendedProperties.Add("col", "StartDate")
            mdt_ImportData_Others.Columns("ExpiryDate").ExtendedProperties.Add("col", "ExpiryDate")
            mdt_ImportData_Others.Columns("EffectivePeriod").ExtendedProperties.Add("col", "EffectivePeriod")
            'Gajanan [27-May-2019] -- End

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            Dim intEmpId As Integer = -1


            'For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
            '    blnIsNotThrown = CheckInvalidData(dtRow)

            '    If blnIsNotThrown = False Then Exit Sub

            '    'If mdicEmpAdded.ContainsKey(dtRow.Item(cboEmployeeCode.Text).ToString.Trim) Then Continue For

            '    'S.SANDEEP [ 02 JAN 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim)
            '    drNewRow = mdt_ImportData_Others.NewRow
            '    'S.SANDEEP |22-MAY-2019| -- START
            '    'ISSUE/ENHANCEMENT : [Training requisition UAT]
            '    'intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim)
            '    Dim blnIsApproved As Boolean = False
            '    intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim, blnIsApproved)
            '    'S.SANDEEP |22-MAY-2019| -- END
            '    If intEmpId <= 0 Then
            '        drNewRow.Item("displayname") = dtRow.Item(cboName.Text).ToString.Trim
            '        drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
            '        drNewRow.Item("image") = imgError
            '        drNewRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Employee Not Found.")
            '        drNewRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
            '        drNewRow.Item("objStatus") = 2
            '        objError.Text = CStr(Val(objError.Text) + 1)
            '    Else
            '        drNewRow.Item("displayname") = dtRow.Item(cboName.Text).ToString.Trim
            '        drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
            '        drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
            '        drNewRow.Item("message") = ""
            '        drNewRow.Item("status") = ""
            '        drNewRow.Item("objStatus") = ""
            '        drNewRow.Item("employeeid") = intEmpId.ToString
            '        drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
            '        drNewRow.Item("message") = ""
            '        drNewRow.Item("status") = ""
            '        drNewRow.Item("objStatus") = ""
            '        'S.SANDEEP |22-MAY-2019| -- START
            '        'ISSUE/ENHANCEMENT : [Training requisition UAT]
            '        drNewRow.Item("objIsApproved") = blnIsApproved
            '        'S.SANDEEP |22-MAY-2019| -- END

            '        'mdicEmpAdded.Add(dtRow.Item(cboEmployeeCode.Text).ToString.Trim, intEmpId)
            '        objTotal.Text = CStr(Val(objTotal.Text) + 1)
            '    End If





            '    intEmpId = -1
            '    mdt_ImportData_Others.Rows.Add(drNewRow)

            '    'S.SANDEEP [ 02 JAN 2013 ] -- END
            'Next

            'If blnIsNotThrown = True Then
            '    colhEmployee.DataPropertyName = "displayname"
            '    colhMembership.DataPropertyName = "MembershipName"
            '    colhMessage.DataPropertyName = "message"
            '    objcolhImage.DataPropertyName = "image"
            '    colhStatus.DataPropertyName = "status"
            '    objdgcolhEmployeeId.DataPropertyName = "employeeid"
            '    objcolhstatus.DataPropertyName = "objStatus"
            '    dgData.AutoGenerateColumns = False
            '    dvGriddata = New DataView(mdt_ImportData_Others)
            '    dgData.DataSource = dvGriddata
            'End If



            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows

                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                Dim blnIsApproved As Boolean = False
                intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim, blnIsApproved)

                If intEmpId <= 0 Then
                    drNewRow.Item("displayname") = dtRow.Item(cboName.Text).ToString.Trim
                    drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                    drNewRow.Item("image") = imgError
                    drNewRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Employee Not Found.")
                    drNewRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                    drNewRow.Item("objStatus") = 2
                    'S.SANDEEP |06-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    drNewRow.Item("objIsApproved") = False
                    'S.SANDEEP |06-SEP-2019| -- END
                    objError.Text = CStr(Val(objError.Text) + 1)
                Else
                    drNewRow.Item("displayname") = dtRow.Item(cboName.Text).ToString.Trim
                    drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                    drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                    drNewRow.Item("message") = ""
                    drNewRow.Item("status") = ""
                    drNewRow.Item("objStatus") = ""
                    drNewRow.Item("employeeid") = intEmpId.ToString
                    drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                    drNewRow.Item("message") = ""
                    drNewRow.Item("status") = ""
                    drNewRow.Item("objStatus") = ""
                    drNewRow.Item("objIsApproved") = blnIsApproved
                    objTotal.Text = CStr(Val(objTotal.Text) + 1)
                End If

                If cboMembershipCategory.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("MembershipCategory") = dtRow.Item(cboMembershipCategory.Text).ToString.Trim
                Else
                    drNewRow.Item("MembershipCategory") = ""
                End If

                If cboMembershipName.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("MembershipName") = dtRow.Item(cboMembershipName.Text).ToString.Trim
                Else
                    drNewRow.Item("MembershipName") = ""
                End If

                If cboMembershipNo.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("MembershipNo") = dtRow.Item(cboMembershipNo.Text).ToString.Trim
                Else
                    drNewRow.Item("MembershipNo") = ""
                End If

                If cboIssueDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("IssueDate") = dtRow.Item(cboIssueDate.Text).ToString.Trim
                Else
                    drNewRow.Item("IssueDate") = ""
                End If

                If cboStartDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("StartDate") = dtRow.Item(cboStartDate.Text).ToString.Trim
                Else
                    drNewRow.Item("StartDate") = ""
                End If

                If cboExpiryDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("ExpiryDate") = dtRow.Item(cboExpiryDate.Text).ToString.Trim
                Else
                    drNewRow.Item("ExpiryDate") = ""
                End If

                If cboExpiryDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("EffectivePeriod") = dtRow.Item(cboEffectivePeriod.Text).ToString.Trim
                Else
                    drNewRow.Item("EffectivePeriod") = ""
                End If

                intEmpId = -1
                mdt_ImportData_Others.Rows.Add(drNewRow)
            Next


            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "displayname"
                colhMembershipCategory.DataPropertyName = "MembershipCategory"
                colhMembershipName.DataPropertyName = "MembershipName"
                colhMembershipNo.DataPropertyName = "MembershipNo"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objdgcolhEmployeeId.DataPropertyName = "employeeid"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizImportMembership.BackEnabled = False
            WizImportMembership.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee Code cannot be blank. Please set the Employee Code to import employee membership(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Name cannot be blank. Please set the Name to import employee membership(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboMembershipCategory.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Membership Category cannot be blank. Please set the Membership Category to import employee membership(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboMembershipName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Membership  cannot be blank. Please set the Membership to import employee membership(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboMembershipNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Membership No. cannot be blank. Please set the Membership No. to import employee membership(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP [ 02 JAN 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If .Item(cboEffectivePeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Effective Period cannot be blank. Please set the Effective Period to import employee membership(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP [ 02 JAN 2013 ] -- END
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP |22-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Training requisition UAT]
    'Private Sub Import_Data()
    '    Try
    '        btnFilter.Enabled = False

    '        If mdt_ImportData_Others.Rows.Count <= 0 Then
    '            Exit Sub
    '        End If

    '        'S.SANDEEP [ 02 JAN 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        If mdicEmpAdded.Keys.Count <= 0 Then
    '            Exit Sub
    '        End If
    '        'S.SANDEEP [ 02 JAN 2013 ] -- END

    '        Dim objCMaster As New clsCommon_Master
    '        Dim objMMaster As New clsmembership_master
    '        Dim objMembershipTran As clsMembershipTran
    '        'S.SANDEEP [ 02 JAN 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Dim objCPeriod As clscommom_period_Tran
    '        Dim iPeriodId As Integer = 0
    '        'S.SANDEEP [ 02 JAN 2013 ] -- END

    '        Dim dtMemTran As New DataTable
    '        Dim mdtTran As New DataTable

    '        Dim intMCatId As Integer = -1
    '        Dim intMembershipId As Integer = -1

    '        For Each dtMRow As DataRow In mdt_ImportData_Others.Rows
    '            If mdicEmpAdded.ContainsKey(dtMRow.Item("employeecode").ToString.Trim) = False Then Continue For
    '            objMembershipTran = New clsMembershipTran
    '            objMembershipTran._EmployeeUnkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '            dtMemTran = objMembershipTran._DataList.Copy

    '            Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable

    '            If dtFilter.Rows.Count > 0 Then
    '                Dim intAssignedEmpId As Integer = -1
    '                For Each dtRow As DataRow In dtFilter.Rows
    '                    Dim dtERow As DataRow = dtMemTran.NewRow

    '                    'S.SANDEEP [ 02 JAN 2013 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    iPeriodId = 0
    '                    objCPeriod = New clscommom_period_Tran

    '                    If dtRow.Item(cboMembershipCategory.Text).ToString.Trim.Length > 0 Then
    '                        intMCatId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, dtRow.Item(cboMembershipCategory.Text).ToString.Trim)
    '                        If intMCatId <= 0 Then
    '                            dtMRow.Item("image") = imgError
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Membership Category not found in system.")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 2
    '                            objError.Text = CStr(Val(objError.Text) + 1)
    '                            'S.SANDEEP [ 05 JUL 2014 ] -- Start
    '                            'Continue For
    '                            Exit Sub
    '                            'S.SANDEEP [ 05 JUL 2014 ] -- End 
    '                        End If
    '                    End If

    '                    If dtRow.Item(cboMembershipName.Text).ToString.Trim.Length > 0 Then
    '                        intMembershipId = objMMaster.GetMembershipUnkid(dtRow.Item(cboMembershipName.Text).ToString.Trim)
    '                        If intMembershipId <= 0 Then
    '                            dtMRow.Item("image") = imgError
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Membership not found in system.")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 2
    '                            objError.Text = CStr(Val(objError.Text) + 1)
    '                            'S.SANDEEP [ 05 JUL 2014 ] -- Start
    '                            'Continue For
    '                            Exit Sub
    '                            'S.SANDEEP [ 05 JUL 2014 ] -- End 
    '                        End If
    '                    End If

    '                    If dtRow.Item(cboEffectivePeriod.Text).ToString.Trim.Length > 0 Then
    '                        iPeriodId = objCPeriod.GetPeriodByName(dtRow.Item(cboEffectivePeriod.Text).ToString.Trim, enModuleReference.Payroll)
    '                        If iPeriodId <= 0 Then
    '                            dtMRow.Item("image") = imgError
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Effective Period not found in system.")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 2
    '                            objError.Text = CStr(Val(objError.Text) + 1)
    '                            'S.SANDEEP [ 05 JUL 2014 ] -- Start
    '                            'Continue For
    '                            Exit Sub
    '                            'S.SANDEEP [ 05 JUL 2014 ] -- End 
    '                        End If

    '                        'Shani (14-Nov-2016) -- Start
    '                        'Enhancement - 
    '                        objCPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
    '                        If objCPeriod._Statusid = enStatusType.Close Then
    '                            dtMRow.Item("image") = imgError
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Sorry,You cannot import this membership. Reason period assigned to this memebership is already close.")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 2
    '                            objError.Text = CStr(Val(objError.Text) + 1)
    '                            Exit Sub
    '                        End If
    '                        'Shani (14-Nov-2016) -- End


    '                    End If
    '                    'S.SANDEEP [ 02 JAN 2013 ] -- END


    '                    '/////////////// --- CHECKING IF ALREADY ASSIGNED --- ///////////////  -- START
    '                    intAssignedEmpId = objMembershipTran.GetEmployeeMembershipUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), dtRow.Item(cboMembershipName.Text).ToString.Trim)
    '                    '/////////////// --- CHECKING IF ALREADY ASSIGNED --- ///////////////  -- END

    '                    If intAssignedEmpId <= 0 Then

    '                        'S.SANDEEP [ 02 JAN 2013 ] -- START
    '                        'ENHANCEMENT : TRA CHANGES
    '                        'ISSUE : (NOW NO INSERTION FORM IMPORT WILL TAKE PLACE DUE TO LINK BETWEEN ED AND MEMBERSHIP)
    '                        ''********************************* MEMBERSHIP CATEGORY *********************************
    '                        'If dtRow.Item(cboMembershipCategory.Text).ToString.Trim.Length > 0 Then
    '                        '    intMCatId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, dtRow.Item(cboMembershipCategory.Text).ToString.Trim)

    '                        '    If intMCatId <= 0 Then
    '                        '        If Not objCMaster Is Nothing Then objCMaster = Nothing
    '                        '        objCMaster = New clsCommon_Master

    '                        '        'If dtRow.Item(cboMembershipCategory.Text).ToString.Trim.Length > 5 Then
    '                        '        '    objCMaster._Alias = dtRow.Item(cboMembershipCategory.Text).ToString.Substring(0, 4).Trim
    '                        '        'Else
    '                        '        '    objCMaster._Alias = dtRow.Item(cboMembershipCategory.Text).ToString.Trim
    '                        '        'End If

    '                        '        objCMaster._Code = dtRow.Item(cboMembershipCategory.Text).ToString.Trim
    '                        '        objCMaster._Isactive = True
    '                        '        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY
    '                        '        objCMaster._Name = dtRow.Item(cboMembershipCategory.Text).ToString.Trim

    '                        '        If objCMaster.Insert Then
    '                        '            intMCatId = objCMaster._Masterunkid
    '                        'Else
    '                        '            intMCatId = -1
    '                        'End If
    '                        '    End If
    '                        'End If
    '                        ''********************************* MEMBERSHIP CATEGORY *********************************

    '                        ''********************************* MEMBERSHIP *********************************
    '                        'If dtRow.Item(cboMembership.Text).ToString.Trim.Length > 0 Then
    '                        '    intMembershipId = objMMaster.GetMembershipUnkid(dtRow.Item(cboMembership.Text).ToString.Trim)

    '                        '    If intMembershipId <= 0 Then
    '                        '        If Not objMMaster Is Nothing Then objMMaster = Nothing
    '                        '        objMMaster = New clsmembership_master

    '                        '        objMMaster._Isactive = True
    '                        '        objMMaster._Membershipcategoryunkid = intMCatId
    '                        '        objMMaster._Membershipcode = dtRow.Item(cboMembership.Text).ToString.Trim
    '                        '        objMMaster._Membershipname = dtRow.Item(cboMembership.Text).ToString.Trim

    '                        '        If objMMaster.Insert Then
    '                        '            intMembershipId = objMMaster._Membershipunkid
    '                        '        Else
    '                        '            intMembershipId = -1
    '                        '        End If
    '                        '    End If
    '                        'End If
    '                        ''********************************* MEMBERSHIP *********************************
    '                        'S.SANDEEP [ 02 JAN 2013 ] -- END


    '                        dtERow.Item("membershiptranunkid") = -1
    '                        dtERow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                        dtERow.Item("membership_categoryunkid") = intMCatId
    '                        dtERow.Item("membershipunkid") = intMembershipId
    '                        dtERow.Item("membershipno") = dtRow.Item(cboMembershipNo.Text).ToString.Trim

    '                        If cboIssueDate.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                    dtERow.Item("issue_date") = DBNull.Value
    '                                Else
    '                                    dtERow.Item("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                End If
    '                            Else
    '                                dtERow.Item("issue_date") = DBNull.Value
    '                            End If
    '                        Else
    '                            dtERow.Item("issue_date") = DBNull.Value
    '                        End If

    '                        If cboStartDate.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboStartDate.Text).ToString.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                    dtERow.Item("start_date") = DBNull.Value
    '                                Else
    '                                    dtERow.Item("start_date") = dtRow.Item(cboStartDate.Text)
    '                                End If
    '                            Else
    '                                dtERow.Item("start_date") = DBNull.Value
    '                            End If
    '                        Else
    '                            dtERow.Item("start_date") = DBNull.Value
    '                        End If

    '                        If cboExpiryDate.Text.Trim.Length > 0 Then
    '                            If dtRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                    dtERow.Item("expiry_date") = DBNull.Value
    '                                Else
    '                                    dtERow.Item("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                End If
    '                            Else
    '                                dtERow.Item("expiry_date") = DBNull.Value
    '                            End If
    '                        Else
    '                            dtERow.Item("expiry_date") = DBNull.Value
    '                        End If


    '                        dtERow.Item("remark") = ""
    '                        dtERow.Item("isactive") = True
    '                        dtERow.Item("AUD") = "A"
    '                        dtERow.Item("GUID") = Guid.NewGuid.ToString
    '                        'S.SANDEEP [ 02 JAN 2013 ] -- START
    '                        'ENHANCEMENT : TRA CHANGES
    '                        objMMaster._Membershipunkid = intMembershipId
    '                        If IsDBNull(objMMaster._ETransHead_Id) = False Then
    '                            dtERow.Item("emptrnheadid") = objMMaster._ETransHead_Id
    '                        Else
    '                            dtERow.Item("emptrnheadid") = 0
    '                        End If
    '                        If IsDBNull(objMMaster._CTransHead_Id) = False Then
    '                            dtERow.Item("cotrnheadid") = objMMaster._CTransHead_Id
    '                        Else
    '                            dtERow.Item("cotrnheadid") = 0
    '                        End If
    '                        dtERow.Item("effetiveperiodid") = iPeriodId
    '                        dtERow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
    '                        dtERow.Item("overwriteslab") = chkOverwritePrevEDSlabHeads.Checked
    '                        dtERow.Item("overwritehead") = chkOverwrite.Checked
    '                        'S.SANDEEP [ 02 JAN 2013 ] -- END

    '                        dtMemTran.Rows.Add(dtERow)

    '                        intAssignedEmpId = -1
    '                    Else
    '                        dtMRow.Item("image") = imgWarring
    '                        dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Membership Aready Exist")
    '                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                        dtMRow.Item("objStatus") = 0
    '                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                    End If
    '                Next
    '                If intAssignedEmpId <= 0 Then
    '                    If dtMemTran.Rows.Count > 0 Then
    '                        objMembershipTran._EmployeeUnkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                        objMembershipTran._DataList = dtMemTran
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'If objMembershipTran.InsertUpdateDelete_MembershipTran Then
    '                        If objMembershipTran.InsertUpdateDelete_MembershipTran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True) Then
    '                            'Sohail (21 Aug 2015) -- End
    '                            dtMRow.Item("image") = imgAccept
    '                            dtMRow.Item("message") = ""
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                            dtMRow.Item("objStatus") = 1
    '                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                        Else
    '                            dtMRow.Item("image") = imgError
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 2
    '                            objError.Text = CStr(Val(objError.Text) + 1)
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
    '    Finally
    '        btnFilter.Enabled = True
    '    End Try
    'End Sub


    'Gajanan [27-May-2019] -- Start              



    'Private Sub Import_Data()
    '    Try
    '        btnFilter.Enabled = False

    '        If mdt_ImportData_Others.Rows.Count <= 0 Then
    '            Exit Sub
    '        End If

    '        If mdicEmpAdded.Keys.Count <= 0 Then
    '            Exit Sub
    '        End If

    '        Dim objCMaster As New clsCommon_Master
    '        Dim objMMaster As New clsmembership_master
    '        'S.SANDEEP |22-MAY-2019| -- START
    '        'ISSUE/ENHANCEMENT : [Training requisition UAT]
    '        'Dim objMembershipTran As clsMembershipTran
    '        Dim objMembershipTran As clsMembershipTran = Nothing
    '        'S.SANDEEP |22-MAY-2019| -- END
    '        Dim objCPeriod As clscommom_period_Tran
    '        Dim iPeriodId As Integer = 0

    '        Dim dtMemTran As New DataTable
    '        Dim mdtTran As New DataTable

    '        Dim intMCatId As Integer = -1
    '        Dim intMembershipId As Integer = -1

    '        Dim pRow As IEnumerable(Of DataRow) = Nothing : Dim aRow As IEnumerable(Of DataRow) = Nothing
    '        If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False).Count > 0 Then
    '            pRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False)
    '        End If
    '        If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True).Count > 0 Then
    '            aRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True)
    '        End If

    '        If pRow IsNot Nothing AndAlso pRow.Count > 0 Then
    '            For Each dtMRow As DataRow In pRow
    '                Application.DoEvents()
    '                If mdicEmpAdded.ContainsKey(dtMRow.Item("employeecode").ToString.Trim) = False Then Continue For
    '                objMembershipTran = New clsMembershipTran
    '                objMembershipTran._EmployeeUnkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                dtMemTran = objMembershipTran._DataList.Copy
    '                Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable

    '                If dtFilter.Rows.Count > 0 Then
    '                    Dim intAssignedEmpId As Integer = -1
    '                    For Each dtRow As DataRow In dtFilter.Rows
    '                        Dim dtERow As DataRow = dtMemTran.NewRow
    '                        iPeriodId = 0
    '                        objCPeriod = New clscommom_period_Tran
    '                        If dtRow.Item(cboMembershipCategory.Text).ToString.Trim.Length > 0 Then
    '                            intMCatId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, dtRow.Item(cboMembershipCategory.Text).ToString.Trim)
    '                            If intMCatId <= 0 Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Membership Category not found in system.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                        If dtRow.Item(cboMembershipName.Text).ToString.Trim.Length > 0 Then
    '                            intMembershipId = objMMaster.GetMembershipUnkid(dtRow.Item(cboMembershipName.Text).ToString.Trim)
    '                            If intMembershipId <= 0 Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Membership not found in system.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                        If dtRow.Item(cboEffectivePeriod.Text).ToString.Trim.Length > 0 Then
    '                            iPeriodId = objCPeriod.GetPeriodByName(dtRow.Item(cboEffectivePeriod.Text).ToString.Trim, enModuleReference.Payroll)
    '                            If iPeriodId <= 0 Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Effective Period not found in system.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                            objCPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
    '                            If objCPeriod._Statusid = enStatusType.Close Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Sorry,You cannot import this membership. Reason period assigned to this memebership is already close.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                        End If

    '                        intAssignedEmpId = objMembershipTran.GetEmployeeMembershipUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), dtRow.Item(cboMembershipName.Text).ToString.Trim)

    '                        If intAssignedEmpId <= 0 Then
    '                            dtERow.Item("membershiptranunkid") = -1
    '                            dtERow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                            dtERow.Item("membership_categoryunkid") = intMCatId
    '                            dtERow.Item("membershipunkid") = intMembershipId
    '                            dtERow.Item("membershipno") = dtRow.Item(cboMembershipNo.Text).ToString.Trim
    '                            If cboIssueDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("issue_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("issue_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("issue_date") = DBNull.Value
    '                            End If
    '                            If cboStartDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboStartDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("start_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("start_date") = dtRow.Item(cboStartDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("start_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("start_date") = DBNull.Value
    '                            End If
    '                            If cboExpiryDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("expiry_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("expiry_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("expiry_date") = DBNull.Value
    '                            End If
    '                            dtERow.Item("remark") = ""
    '                            dtERow.Item("isactive") = True
    '                            dtERow.Item("AUD") = "A"
    '                            dtERow.Item("GUID") = Guid.NewGuid.ToString
    '                            objMMaster._Membershipunkid = intMembershipId
    '                            If IsDBNull(objMMaster._ETransHead_Id) = False Then
    '                                dtERow.Item("emptrnheadid") = objMMaster._ETransHead_Id
    '                            Else
    '                                dtERow.Item("emptrnheadid") = 0
    '                            End If
    '                            If IsDBNull(objMMaster._CTransHead_Id) = False Then
    '                                dtERow.Item("cotrnheadid") = objMMaster._CTransHead_Id
    '                            Else
    '                                dtERow.Item("cotrnheadid") = 0
    '                            End If
    '                            dtERow.Item("effetiveperiodid") = iPeriodId
    '                            dtERow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
    '                            dtERow.Item("overwriteslab") = chkOverwritePrevEDSlabHeads.Checked
    '                            dtERow.Item("overwritehead") = chkOverwrite.Checked
    '                            dtMemTran.Rows.Add(dtERow)
    '                            intAssignedEmpId = -1
    '                        Else
    '                            dtMRow.Item("image") = imgWarring
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Membership Aready Exist")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 0
    '                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                        End If
    '                    Next
    '                    If intAssignedEmpId <= 0 Then
    '                        If dtMemTran.Rows.Count > 0 Then
    '                            objMembershipTran._EmployeeUnkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                            objMembershipTran._DataList = dtMemTran
    '                            If objMembershipTran.InsertUpdateDelete_MembershipTran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True) Then
    '                                dtMRow.Item("image") = imgAccept
    '                                dtMRow.Item("message") = ""
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                dtMRow.Item("objStatus") = 1
    '                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                            Else
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            Next
    '        End If

    '        If aRow IsNot Nothing AndAlso aRow.Count > 0 Then
    '            For Each dtMRow As DataRow In aRow
    '                Application.DoEvents()
    '                If mdicEmpAdded.ContainsKey(dtMRow.Item("employeecode").ToString.Trim) = False Then Continue For
    '                If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                    objAMemInfoTran = New clsMembership_Approval_Tran
    '                    objAMemInfoTran._Employeeunkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                    dtMemTran = objAMemInfoTran._DataTable.Copy
    '                Else
    '                    objMembershipTran = New clsMembershipTran
    '                    objMembershipTran._EmployeeUnkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
    '                    dtMemTran = objMembershipTran._DataList.Copy
    '                End If

    '                Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable
    '                If dtFilter.Rows.Count > 0 Then
    '                    Dim intAssignedEmpId As Integer = -1
    '                    For Each dtRow As DataRow In dtFilter.Rows
    '                        Dim dtERow As DataRow = dtMemTran.NewRow
    '                        iPeriodId = 0
    '                        objCPeriod = New clscommom_period_Tran

    '                        If dtRow.Item(cboMembershipCategory.Text).ToString.Trim.Length > 0 Then
    '                            intMCatId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, dtRow.Item(cboMembershipCategory.Text).ToString.Trim)
    '                            If intMCatId <= 0 Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Membership Category not found in system.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                        If dtRow.Item(cboMembershipName.Text).ToString.Trim.Length > 0 Then
    '                            intMembershipId = objMMaster.GetMembershipUnkid(dtRow.Item(cboMembershipName.Text).ToString.Trim)
    '                            If intMembershipId <= 0 Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Membership not found in system.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                        If dtRow.Item(cboEffectivePeriod.Text).ToString.Trim.Length > 0 Then
    '                            iPeriodId = objCPeriod.GetPeriodByName(dtRow.Item(cboEffectivePeriod.Text).ToString.Trim, enModuleReference.Payroll)
    '                            If iPeriodId <= 0 Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Effective Period not found in system.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                            objCPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
    '                            If objCPeriod._Statusid = enStatusType.Close Then
    '                                dtMRow.Item("image") = imgError
    '                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Sorry,You cannot import this membership. Reason period assigned to this memebership is already close.")
    '                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                dtMRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                        If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                            intAssignedEmpId = objAMemInfoTran.GetEmployeeMembershipUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), dtRow.Item(cboMembershipName.Text).ToString.Trim)
    '                        Else
    '                            intAssignedEmpId = objMembershipTran.GetEmployeeMembershipUnkid(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), dtRow.Item(cboMembershipName.Text).ToString.Trim)
    '                        End If
    '                        If intAssignedEmpId <= 0 Then
    '                            dtERow.Item("membershiptranunkid") = -1
    '                            dtERow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                            dtERow.Item("membership_categoryunkid") = intMCatId
    '                            dtERow.Item("membershipunkid") = intMembershipId
    '                            dtERow.Item("membershipno") = dtRow.Item(cboMembershipNo.Text).ToString.Trim
    '                            If cboIssueDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("issue_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("issue_date") = dtRow.Item(cboIssueDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("issue_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("issue_date") = DBNull.Value
    '                            End If
    '                            If cboStartDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboStartDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("start_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("start_date") = dtRow.Item(cboStartDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("start_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("start_date") = DBNull.Value
    '                            End If
    '                            If cboExpiryDate.Text.Trim.Length > 0 Then
    '                                If dtRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
    '                                    If dtRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
    '                                        dtERow.Item("expiry_date") = DBNull.Value
    '                                    Else
    '                                        dtERow.Item("expiry_date") = dtRow.Item(cboExpiryDate.Text)
    '                                    End If
    '                                Else
    '                                    dtERow.Item("expiry_date") = DBNull.Value
    '                                End If
    '                            Else
    '                                dtERow.Item("expiry_date") = DBNull.Value
    '                            End If
    '                            dtERow.Item("remark") = ""
    '                            dtERow.Item("isactive") = True
    '                            dtERow.Item("AUD") = "A"
    '                            objMMaster._Membershipunkid = intMembershipId
    '                            If IsDBNull(objMMaster._ETransHead_Id) = False Then
    '                                dtERow.Item("emptrnheadid") = objMMaster._ETransHead_Id
    '                            Else
    '                                dtERow.Item("emptrnheadid") = 0
    '                            End If
    '                            If IsDBNull(objMMaster._CTransHead_Id) = False Then
    '                                dtERow.Item("cotrnheadid") = objMMaster._CTransHead_Id
    '                            Else
    '                                dtERow.Item("cotrnheadid") = 0
    '                            End If
    '                            dtERow.Item("effetiveperiodid") = iPeriodId
    '                            dtERow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
    '                            dtERow.Item("overwriteslab") = chkOverwritePrevEDSlabHeads.Checked
    '                            dtERow.Item("overwritehead") = chkOverwrite.Checked
    '                            dtERow.Item("GUID") = Guid.NewGuid.ToString
    '                            If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
    '                                dtERow.Item("tranguid") = Guid.NewGuid.ToString()
    '                                dtERow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
    '                                dtERow.Item("mappingunkid") = 0
    '                                dtERow.Item("approvalremark") = ""
    '                                dtERow.Item("isfinal") = False
    '                                dtERow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
    '                                dtERow.Item("loginemployeeunkid") = 0
    '                                dtERow.Item("isvoid") = False
    '                                dtERow.Item("voidreason") = ""
    '                                dtERow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                                dtERow.Item("audittype") = enAuditType.ADD
    '                                dtERow.Item("audituserunkid") = User._Object._Userunkid
    '                                dtERow.Item("ip") = getIP()
    '                                dtERow.Item("host") = getHostName()
    '                                dtERow.Item("form_name") = mstrModuleName
    '                                dtERow.Item("isweb") = False
    '                                dtERow.Item("isprocessed") = False
    '                                dtERow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.ADDED
    '                                dtERow.Item("operationtype") = Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information")
    '                                dtERow.Item("membership_category") = dtRow.Item(cboMembershipCategory.Text)
    '                                dtERow.Item("membershipname") = dtRow.Item(cboMembershipName.Text)
    '                            End If
    '                            dtMemTran.Rows.Add(dtERow)
    '                        Else
    '                            dtMRow.Item("image") = imgWarring
    '                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Membership Aready Exist")
    '                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                            dtMRow.Item("objStatus") = 0
    '                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                        End If
    '                    Next
    '                    If intAssignedEmpId <= 0 Then
    '                        If dtMemTran.Rows.Count > 0 Then
    '                            If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then

    '                                'Gajanan [17-April-2019] -- Start
    '                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                                objApprovalData = New clsEmployeeDataApproval

    '                                If objApprovalData.IsApproverPresent(enScreenName.frmMembershipInfoList, FinancialYear._Object._DatabaseName, _
    '                                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
    '                                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
    '                                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(dtMRow("employeeid").ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then


    '                                    dtMRow.Item("image") = imgWarring
    '                                    dtMRow.Item("message") = objApprovalData._Message
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                    dtMRow.Item("objStatus") = 2
    '                                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
    '                                    Continue For
    '                                End If
    '                                'Gajanan [17-April-2019] -- End



    '                                If objAMemInfoTran.InsertUpdateDelete_MembershipTran(dtMemTran, Nothing, User._Object._Userunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp) Then
    '                                    dtMRow.Item("image") = imgAccept
    '                                    dtMRow.Item("message") = ""
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                    dtMRow.Item("objStatus") = 1
    '                                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)


    '                                    objApprovalData = New clsEmployeeDataApproval
    '                                    objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
    '                                                            ConfigParameter._Object._UserAccessModeSetting, _
    '                                                            Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
    '                                                            CInt(enUserPriviledge.AllowToApproveRejectEmployeeMemberships), _
    '                                                            enScreenName.frmMembershipInfoList, ConfigParameter._Object._EmployeeAsOnDate, _
    '                                                            User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
    '                                                            User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , dtMRow("employeeid").ToString(), , , _
    '                                                             "employeeunkid= " & dtMRow("employeeid").ToString, dtMemTran, , )
    '                                Else
    '                                    dtMRow.Item("image") = imgError
    '                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                    dtMRow.Item("objStatus") = 2
    '                                    objError.Text = CStr(Val(objError.Text) + 1)
    '                                End If
    '                            Else
    '                                If dtMemTran.Rows.Count > 0 Then
    '                                    objMembershipTran._EmployeeUnkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
    '                                    objMembershipTran._DataList = dtMemTran
    '                                    If objMembershipTran.InsertUpdateDelete_MembershipTran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True) Then
    '                                        dtMRow.Item("image") = imgAccept
    '                                        dtMRow.Item("message") = ""
    '                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
    '                                        dtMRow.Item("objStatus") = 1
    '                                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '                                    Else
    '                                        dtMRow.Item("image") = imgError
    '                                        dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
    '                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
    '                                        dtMRow.Item("objStatus") = 2
    '                                        objError.Text = CStr(Val(objError.Text) + 1)
    '                                    End If
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            Next
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
    '    Finally
    '        btnFilter.Enabled = True
    '    End Try
    'End Sub


    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objCMaster As New clsCommon_Master
            Dim objMMaster As New clsmembership_master
            Dim objMembershipTran As clsMembershipTran = Nothing
            Dim objCPeriod As clscommom_period_Tran
            Dim iPeriodId As Integer = 0

            Dim dtMemTran As New DataTable
            Dim mdtTran As New DataTable

            Dim intMCatId As Integer = -1
            Dim intMembershipId As Integer = -1

            Dim EmpList As New List(Of String)

            Dim pRow As IEnumerable(Of DataRow) = Nothing : Dim aRow As IEnumerable(Of DataRow) = Nothing
            If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False).Count > 0 Then
                pRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = False)
            End If
            If mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True).Count > 0 Then
                aRow = mdt_ImportData_Others.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("objIsApproved") = True)
            End If

            If pRow IsNot Nothing AndAlso pRow.Count > 0 Then
                For Each dtMRow As DataRow In pRow
                    Application.DoEvents()
                    
                    'S.SANDEEP |06-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    If dtMRow("message").ToString() <> "" Then Continue For
                    'S.SANDEEP |06-SEP-2019| -- END

                objMembershipTran = New clsMembershipTran

                    objMembershipTran._EmployeeUnkid = CInt(dtMRow.Item("employeeid").ToString.Trim)
                dtMemTran = objMembershipTran._DataList.Copy
                Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable

                If dtFilter.Rows.Count > 0 Then
                    Dim intAssignedEmpId As Integer = -1

                        Dim dtERow As DataRow = dtMemTran.NewRow
                        iPeriodId = 0
                        objCPeriod = New clscommom_period_Tran
                        If dtMRow.Item(cboMembershipCategory.Text).ToString.Trim.Length > 0 Then
                            intMCatId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, dtMRow.Item(cboMembershipCategory.Text).ToString.Trim)
                            If intMCatId <= 0 Then
                                dtMRow.Item("image") = imgError
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Membership Category not found in system.")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit Sub
                            End If
                        End If
                        If dtMRow.Item(cboMembershipName.Text).ToString.Trim.Length > 0 Then
                            intMembershipId = objMMaster.GetMembershipUnkid(dtMRow.Item(cboMembershipName.Text).ToString.Trim)
                            If intMembershipId <= 0 Then
                                dtMRow.Item("image") = imgError
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Membership not found in system.")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit Sub
                            End If

                        End If
                        If dtMRow.Item(cboEffectivePeriod.Text).ToString.Trim.Length > 0 Then
                            iPeriodId = objCPeriod.GetPeriodByName(dtMRow.Item(cboEffectivePeriod.Text).ToString.Trim, enModuleReference.Payroll)
                            If iPeriodId <= 0 Then
                                dtMRow.Item("image") = imgError
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Effective Period not found in system.")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit Sub
                            End If
                            objCPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
                            If objCPeriod._Statusid = enStatusType.Close Then
                                dtMRow.Item("image") = imgError
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Sorry,You cannot import this membership. Reason period assigned to this memebership is already close.")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit Sub
                            End If
                        End If

                        intAssignedEmpId = objMembershipTran.GetEmployeeMembershipUnkid(CInt(dtMRow.Item("employeeid").ToString), dtMRow.Item(cboMembershipName.Text).ToString.Trim)

                        If intAssignedEmpId <= 0 Then
                                dtERow.Item("membershiptranunkid") = -1
                            dtERow.Item("employeeunkid") = CInt(dtMRow.Item("employeeid").ToString)
                                dtERow.Item("membership_categoryunkid") = intMCatId
                                dtERow.Item("membershipunkid") = intMembershipId
                            dtERow.Item("membershipno") = dtMRow.Item(cboMembershipNo.Text).ToString.Trim
                                If cboIssueDate.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
                                    If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                            dtERow.Item("issue_date") = DBNull.Value
                                        Else
                                        dtERow.Item("issue_date") = dtMRow.Item(cboIssueDate.Text)
                                        End If
                                    Else
                                        dtERow.Item("issue_date") = DBNull.Value
                                    End If
                                Else
                                    dtERow.Item("issue_date") = DBNull.Value
                                End If
                                If cboStartDate.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboStartDate.Text).ToString.Trim.Length > 0 Then
                                    If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                            dtERow.Item("start_date") = DBNull.Value
                                        Else
                                        dtERow.Item("start_date") = dtMRow.Item(cboStartDate.Text)
                                        End If
                                    Else
                                        dtERow.Item("start_date") = DBNull.Value
                                    End If
                                Else
                                    dtERow.Item("start_date") = DBNull.Value
                                End If
                                If cboExpiryDate.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
                                    If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                            dtERow.Item("expiry_date") = DBNull.Value
                                        Else
                                        dtERow.Item("expiry_date") = dtMRow.Item(cboExpiryDate.Text)
                                        End If
                                    Else
                                        dtERow.Item("expiry_date") = DBNull.Value
                                    End If
                                Else
                                    dtERow.Item("expiry_date") = DBNull.Value
                                End If
                                dtERow.Item("remark") = ""
                                dtERow.Item("isactive") = True
                                dtERow.Item("AUD") = "A"
                                dtERow.Item("GUID") = Guid.NewGuid.ToString
                                objMMaster._Membershipunkid = intMembershipId
                                If IsDBNull(objMMaster._ETransHead_Id) = False Then
                                    dtERow.Item("emptrnheadid") = objMMaster._ETransHead_Id
                                Else
                                    dtERow.Item("emptrnheadid") = 0
                                End If
                                If IsDBNull(objMMaster._CTransHead_Id) = False Then
                                    dtERow.Item("cotrnheadid") = objMMaster._CTransHead_Id
                                Else
                                    dtERow.Item("cotrnheadid") = 0
                                End If
                                dtERow.Item("effetiveperiodid") = iPeriodId
                                dtERow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
                                dtERow.Item("overwriteslab") = chkOverwritePrevEDSlabHeads.Checked
                                dtERow.Item("overwritehead") = chkOverwrite.Checked
                                dtMemTran.Rows.Add(dtERow)
                                intAssignedEmpId = -1
                            Else
                                dtMRow.Item("image") = imgWarring
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Membership Aready Exist")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 0
                                objWarning.Text = CStr(Val(objWarning.Text) + 1)
                            End If



                        If intAssignedEmpId <= 0 Then
                            If dtMemTran.Rows.Count > 0 Then
                                objMembershipTran._EmployeeUnkid = CInt(dtMRow.Item("employeeid").ToString)
                                objMembershipTran._DataList = dtMemTran
                                If objMembershipTran.InsertUpdateDelete_MembershipTran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True) Then
                                    dtMRow.Item("image") = imgAccept
                                    dtMRow.Item("message") = ""
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
                                    dtMRow.Item("objStatus") = 1
                                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                                Else
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                End If
                            End If
                        End If
                    End If
                Next
            End If


            If aRow IsNot Nothing AndAlso aRow.Count > 0 Then
                For Each dtMRow As DataRow In aRow
                    Application.DoEvents()

                    'S.SANDEEP |06-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    If dtMRow("message").ToString() <> "" Then Continue For
                    'S.SANDEEP |06-SEP-2019| -- END

                    If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
                        objAMemInfoTran = New clsMembership_Approval_Tran
                        objAMemInfoTran._Employeeunkid = CInt(dtMRow.Item("employeeid").ToString)
                        dtMemTran = objAMemInfoTran._DataTable.Copy
                    Else
                        objMembershipTran = New clsMembershipTran
                        objMembershipTran._EmployeeUnkid = CInt(dtMRow.Item("employeeid").ToString)
                        dtMemTran = objMembershipTran._DataList.Copy
                    End If

                    Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable
                    If dtFilter.Rows.Count > 0 Then
                        Dim intAssignedEmpId As Integer = -1


                            Dim dtERow As DataRow = dtMemTran.NewRow
                            iPeriodId = 0
                            objCPeriod = New clscommom_period_Tran

                        If dtMRow.Item(cboMembershipCategory.Text).ToString.Trim.Length > 0 Then
                            intMCatId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, dtMRow.Item(cboMembershipCategory.Text).ToString.Trim)
                                If intMCatId <= 0 Then
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Membership Category not found in system.")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                                End If
                            End If
                        If dtMRow.Item(cboMembershipName.Text).ToString.Trim.Length > 0 Then
                            intMembershipId = objMMaster.GetMembershipUnkid(dtMRow.Item(cboMembershipName.Text).ToString.Trim)
                                If intMembershipId <= 0 Then
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Membership not found in system.")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                                End If
                            End If
                        If dtMRow.Item(cboEffectivePeriod.Text).ToString.Trim.Length > 0 Then
                            iPeriodId = objCPeriod.GetPeriodByName(dtMRow.Item(cboEffectivePeriod.Text).ToString.Trim, enModuleReference.Payroll)
                                If iPeriodId <= 0 Then
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Effective Period not found in system.")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                                End If
                                objCPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
                                If objCPeriod._Statusid = enStatusType.Close Then
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Sorry,You cannot import this membership. Reason period assigned to this memebership is already close.")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                    dtMRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                                End If
                            End If
                            If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
                            intAssignedEmpId = objAMemInfoTran.GetEmployeeMembershipUnkid(CInt(dtMRow.Item("employeeid").ToString), dtMRow.Item(cboMembershipName.Text).ToString.Trim)
                            Else
                            intAssignedEmpId = objMembershipTran.GetEmployeeMembershipUnkid(CInt(dtMRow.Item("employeeid").ToString), dtMRow.Item(cboMembershipName.Text).ToString.Trim)
                            End If
                            If intAssignedEmpId <= 0 Then
                            dtERow.Item("membershiptranunkid") = -1
                            dtERow.Item("employeeunkid") = CInt(dtMRow.Item("employeeid").ToString)
                            dtERow.Item("membership_categoryunkid") = intMCatId
                            dtERow.Item("membershipunkid") = intMembershipId
                            dtERow.Item("membershipno") = dtMRow.Item(cboMembershipNo.Text).ToString.Trim
                            If cboIssueDate.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboIssueDate.Text).ToString.Trim.Length > 0 Then
                                    If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                        dtERow.Item("issue_date") = DBNull.Value
                                    Else
                                        dtERow.Item("issue_date") = dtMRow.Item(cboIssueDate.Text)
                                    End If
                                Else
                                    dtERow.Item("issue_date") = DBNull.Value
                                End If
                            Else
                                dtERow.Item("issue_date") = DBNull.Value
                            End If
                            If cboStartDate.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboStartDate.Text).ToString.Trim.Length > 0 Then
                                    If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                        dtERow.Item("start_date") = DBNull.Value
                                    Else
                                        dtERow.Item("start_date") = dtMRow.Item(cboStartDate.Text)
                                    End If
                                Else
                                    dtERow.Item("start_date") = DBNull.Value
                                End If
                            Else
                                dtERow.Item("start_date") = DBNull.Value
                            End If
                            If cboExpiryDate.Text.Trim.Length > 0 Then
                                If dtMRow.Item(cboExpiryDate.Text).ToString.Trim.Length > 0 Then
                                    If dtMRow.Item(cboIssueDate.Text).ToString = "0000-00-00" Then
                                        dtERow.Item("expiry_date") = DBNull.Value
                                    Else
                                        dtERow.Item("expiry_date") = dtMRow.Item(cboExpiryDate.Text)
                                    End If
                                Else
                                    dtERow.Item("expiry_date") = DBNull.Value
                                End If
                            Else
                                dtERow.Item("expiry_date") = DBNull.Value
                            End If
                            dtERow.Item("remark") = ""
                            dtERow.Item("isactive") = True
                            dtERow.Item("AUD") = "A"
                            objMMaster._Membershipunkid = intMembershipId
                            If IsDBNull(objMMaster._ETransHead_Id) = False Then
                                dtERow.Item("emptrnheadid") = objMMaster._ETransHead_Id
                            Else
                                dtERow.Item("emptrnheadid") = 0
                            End If
                            If IsDBNull(objMMaster._CTransHead_Id) = False Then
                                dtERow.Item("cotrnheadid") = objMMaster._CTransHead_Id
                            Else
                                dtERow.Item("cotrnheadid") = 0
                            End If
                            dtERow.Item("effetiveperiodid") = iPeriodId
                            dtERow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
                            dtERow.Item("overwriteslab") = chkOverwritePrevEDSlabHeads.Checked
                            dtERow.Item("overwritehead") = chkOverwrite.Checked
                                dtERow.Item("GUID") = Guid.NewGuid.ToString
                                If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then
                                    dtERow.Item("tranguid") = Guid.NewGuid.ToString()
                                    dtERow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                                    dtERow.Item("mappingunkid") = 0
                                    dtERow.Item("approvalremark") = ""
                                    dtERow.Item("isfinal") = False
                                    dtERow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                                    dtERow.Item("loginemployeeunkid") = 0
                                    dtERow.Item("isvoid") = False
                                    dtERow.Item("voidreason") = ""
                                    dtERow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                                    dtERow.Item("audittype") = enAuditType.ADD
                                    dtERow.Item("audituserunkid") = User._Object._Userunkid
                                    dtERow.Item("ip") = getIP()
                                    dtERow.Item("host") = getHostName()
                                    dtERow.Item("form_name") = mstrModuleName
                                    dtERow.Item("isweb") = False
                                    dtERow.Item("isprocessed") = False
                                    dtERow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.ADDED
                                    dtERow.Item("operationtype") = Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information")
                                dtERow.Item("membership_category") = dtMRow.Item(cboMembershipCategory.Text)
                                dtERow.Item("membershipname") = dtMRow.Item(cboMembershipName.Text)
                                End If
                            dtMemTran.Rows.Add(dtERow)
                        Else
                            dtMRow.Item("image") = imgWarring
                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Membership Aready Exist")
                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                            dtMRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        End If


                    If intAssignedEmpId <= 0 Then
                        If dtMemTran.Rows.Count > 0 Then
                                If MembershipApprovalFlowVal Is Nothing AndAlso CBool(dtMRow("objIsApproved")) = True Then

                                    objApprovalData = New clsEmployeeDataApproval

                                    If objApprovalData.IsApproverPresent(enScreenName.frmMembershipInfoList, FinancialYear._Object._DatabaseName, _
                                                                          ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                                          FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
                                                                          User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(dtMRow("employeeid").ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then


                                        dtMRow.Item("image") = imgWarring
                                        dtMRow.Item("message") = objApprovalData._Message
                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                        dtMRow.Item("objStatus") = 2
                                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                        Continue For
                                    End If



                                    If objAMemInfoTran.InsertUpdateDelete_MembershipTran(dtMemTran, Nothing, User._Object._Userunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp) Then
                                        dtMRow.Item("image") = imgAccept
                                        dtMRow.Item("message") = ""
                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
                                        dtMRow.Item("objStatus") = 1
                                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)

                                        If EmpList.Contains(dtMRow("employeeid").ToString()) = False Then
                                            EmpList.Add(dtMRow("employeeid").ToString())
                                        End If
                                    Else
                                        dtMRow.Item("image") = imgError
                                        dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                        dtMRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                    End If
                                Else
                                    If dtMemTran.Rows.Count > 0 Then
                                        objMembershipTran._EmployeeUnkid = CInt(dtMRow.Item("employeeid").ToString)
                            objMembershipTran._DataList = dtMemTran
                            If objMembershipTran.InsertUpdateDelete_MembershipTran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True) Then
                                dtMRow.Item("image") = imgAccept
                                dtMRow.Item("message") = ""
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
                                dtMRow.Item("objStatus") = 1
                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            Else
                                dtMRow.Item("image") = imgError
                                dtMRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Data")
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                                dtMRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                            End If
                        End If
                    End If
                End If
                        End If
                    End If
            Next


                If MembershipApprovalFlowVal Is Nothing Then
                    objApprovalData = New clsEmployeeDataApproval
                    If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 10, "Fail") & "'").Count > 0 Then
                        If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                            Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                            mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 10, "Fail") & "'"
                            objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                       enUserPriviledge.AllowToApproveRejectEmployeeMemberships, _
                                                                       enScreenName.frmMembershipInfoList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                       User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                       User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                                       User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                                       ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    'Gajanan [27-May-2019] -- End


    'S.SANDEEP |22-MAY-2019| -- END

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    If TypeOf ctrl Is ComboBox Then
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
                    End If
                Next


                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known

                If dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 7, "EmployeeCode") Then
                    dtColumns.Caption = "EmployeeCode"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 8, "Displayname") Then
                    dtColumns.Caption = "Displayname"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 9, "MembershipCategory") Then
                    dtColumns.Caption = "MembershipCategory"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 10, "MembershipName") Then
                    dtColumns.Caption = "MembershipName"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 11, "Employee_Code") Then
                    dtColumns.Caption = "Employee_Code"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 12, "IssueDate") Then
                    dtColumns.Caption = "IssueDate"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 13, "StartDate") Then
                    dtColumns.Caption = "StartDate"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 14, "ExpiryDate") Then
                    dtColumns.Caption = "ExpiryDate"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsMembershipTran", 15, "EffectivePeriod") Then
                    dtColumns.Caption = "EffectivePeriod"
                End If
                'Gajanan (24 Nov 2018) -- End

               
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    dtTable.Columns.Remove("employeeid")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Membership Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 02 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = True Then
                chkOverwritePrevEDSlabHeads.Enabled = True
                chkOverwrite.Enabled = True
            Else
                chkOverwritePrevEDSlabHeads.Enabled = False
                chkOverwrite.Enabled = False
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 02 JAN 2013 ] -- END


    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    'Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
    '    Try
    '        Dim dtExTable As New DataTable("DataList")
    '        With dtExTable
    '            .Columns.Add("EmployeeCode", System.Type.GetType("System.String"))
    '            .Columns.Add("Displayname", System.Type.GetType("System.String"))
    '            .Columns.Add("MembershipCategory", System.Type.GetType("System.String"))
    '            .Columns.Add("MembershipName", System.Type.GetType("System.String"))
    '            .Columns.Add("MembershipNo", System.Type.GetType("System.String"))
    '            .Columns.Add("EffectivePeriod", System.Type.GetType("System.String"))
    '            .Columns.Add("IssueDate", System.Type.GetType("System.String"))
    '            .Columns.Add("StartDate", System.Type.GetType("System.String"))
    '            .Columns.Add("ExpiryDate", System.Type.GetType("System.String"))
    '        End With
    '        Dim dsList As New DataSet
    '        dsList.Tables.Add(dtExTable.Copy)
    '        Dim dlgSaveFile As New SaveFileDialog
    '        dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
    '        If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            OpenXML_Export(dlgSaveFile.FileName, dsList)
    '        End If
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 67, "Template Exported Successfully."), enMsgBoxStyle.Information)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.WizImportMembership.CancelText = Language._Object.getCaption(Me.WizImportMembership.Name & "_CancelText", Me.WizImportMembership.CancelText)
            Me.WizImportMembership.NextText = Language._Object.getCaption(Me.WizImportMembership.Name & "_NextText", Me.WizImportMembership.NextText)
            Me.WizImportMembership.BackText = Language._Object.getCaption(Me.WizImportMembership.Name & "_BackText", Me.WizImportMembership.BackText)
            Me.WizImportMembership.FinishText = Language._Object.getCaption(Me.WizImportMembership.Name & "_FinishText", Me.WizImportMembership.FinishText)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblMembershipNo.Text = Language._Object.getCaption(Me.lblMembershipNo.Name, Me.lblMembershipNo.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.lblMembershipCategory.Text = Language._Object.getCaption(Me.lblMembershipCategory.Name, Me.lblMembershipCategory.Text)
            Me.lblExpiryDate.Text = Language._Object.getCaption(Me.lblExpiryDate.Name, Me.lblExpiryDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblIssueDate.Text = Language._Object.getCaption(Me.lblIssueDate.Name, Me.lblIssueDate.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lblEffPeriod.Text = Language._Object.getCaption(Me.lblEffPeriod.Name, Me.lblEffPeriod.Text)
            Me.chkOverwrite.Text = Language._Object.getCaption(Me.chkOverwrite.Name, Me.chkOverwrite.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code cannot be blank. Please set the Employee Code to import employee membership(s).")
            Language.setMessage(mstrModuleName, 2, "Name cannot be blank. Please set the Name to import employee membership(s).")
            Language.setMessage(mstrModuleName, 3, "Membership Category cannot be blank. Please set the Membership Category to import employee membership(s).")
            Language.setMessage(mstrModuleName, 4, "Membership  cannot be blank. Please set the Membership to import employee membership(s).")
            Language.setMessage(mstrModuleName, 5, "Membership No. cannot be blank. Please set the Membership No. to import employee membership(s).")
            Language.setMessage(mstrModuleName, 6, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 7, "Employee Membership Aready Exist")
            Language.setMessage(mstrModuleName, 8, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 9, "Invalid Data")
            Language.setMessage(mstrModuleName, 10, "Fail")
            Language.setMessage(mstrModuleName, 11, "Success")
            Language.setMessage(mstrModuleName, 12, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 13, "Effective Period cannot be blank. Please set the Effective Period to import employee membership(s).")
            Language.setMessage(mstrModuleName, 14, "Membership not found in system.")
            Language.setMessage(mstrModuleName, 15, "Membership Category not found in system.")
            Language.setMessage(mstrModuleName, 16, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 17, "Effective Period not found in system.")
            Language.setMessage(mstrModuleName, 18, "Sorry,You cannot import this membership. Reason period assigned to this memebership is already close.")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class