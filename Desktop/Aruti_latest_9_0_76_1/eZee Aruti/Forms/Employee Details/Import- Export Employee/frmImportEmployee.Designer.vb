﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportEmployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportEmployee))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvImportInfo = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOthername = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSurname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhappointeddate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShift = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradelevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostCenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSalaray = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.gbFileInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtAppointdate = New System.Windows.Forms.DateTimePicker
        Me.lblAppointdate = New System.Windows.Forms.Label
        Me.objbtnAddSalary = New eZee.Common.eZeeGradientButton
        Me.txtSalary = New eZee.TextBox.NumericTextBox
        Me.lblSalary = New System.Windows.Forms.Label
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.lblCostcenter = New System.Windows.Forms.Label
        Me.cboTranhead = New System.Windows.Forms.ComboBox
        Me.lblTranHead = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblGradeGrp = New System.Windows.Forms.Label
        Me.cboGradeGrp = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.objbtnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.radApplySelected = New System.Windows.Forms.RadioButton
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.lblDept = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.lblShift = New System.Windows.Forms.Label
        Me.cboEmpType = New System.Windows.Forms.ComboBox
        Me.lblEmpType = New System.Windows.Forms.Label
        Me.elMandatoryInfo = New eZee.Common.eZeeLine
        Me.objbtnOpenFile = New eZee.Common.eZeeGradientButton
        Me.txtFilePath = New System.Windows.Forms.TextBox
        Me.lblFileName = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.ofdlgOpen = New System.Windows.Forms.OpenFileDialog
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFileInfo.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvImportInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbFileInfo)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(846, 598)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgvImportInfo
        '
        Me.dgvImportInfo.AllowUserToAddRows = False
        Me.dgvImportInfo.AllowUserToDeleteRows = False
        Me.dgvImportInfo.AllowUserToResizeColumns = False
        Me.dgvImportInfo.AllowUserToResizeRows = False
        Me.dgvImportInfo.BackgroundColor = System.Drawing.Color.White
        Me.dgvImportInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvImportInfo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhEmployee, Me.dgcolhFirstName, Me.dgcolhOthername, Me.dgcolhSurname, Me.dgcolhappointeddate, Me.dgcolhEmpType, Me.dgcolhShift, Me.dgcolhDept, Me.dgcolhJob, Me.dgcolhGradeGrp, Me.dgcolhGrade, Me.dgcolhGradelevel, Me.dgcolhTranHead, Me.dgcolhCostCenter, Me.dgcolhSalaray})
        Me.dgvImportInfo.Location = New System.Drawing.Point(12, 241)
        Me.dgvImportInfo.MultiSelect = False
        Me.dgvImportInfo.Name = "dgvImportInfo"
        Me.dgvImportInfo.RowHeadersVisible = False
        Me.dgvImportInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvImportInfo.Size = New System.Drawing.Size(822, 296)
        Me.dgvImportInfo.TabIndex = 5
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Code"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 80
        '
        'dgcolhFirstName
        '
        Me.dgcolhFirstName.HeaderText = "First Name"
        Me.dgcolhFirstName.Name = "dgcolhFirstName"
        Me.dgcolhFirstName.ReadOnly = True
        Me.dgcolhFirstName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhFirstName.Width = 150
        '
        'dgcolhOthername
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.dgcolhOthername.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhOthername.HeaderText = "Other Name"
        Me.dgcolhOthername.Name = "dgcolhOthername"
        Me.dgcolhOthername.ReadOnly = True
        Me.dgcolhOthername.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOthername.Width = 120
        '
        'dgcolhSurname
        '
        Me.dgcolhSurname.HeaderText = "Surname"
        Me.dgcolhSurname.Name = "dgcolhSurname"
        Me.dgcolhSurname.ReadOnly = True
        Me.dgcolhSurname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSurname.Width = 135
        '
        'dgcolhappointeddate
        '
        Me.dgcolhappointeddate.HeaderText = "Appoint Date"
        Me.dgcolhappointeddate.Name = "dgcolhappointeddate"
        Me.dgcolhappointeddate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmpType
        '
        Me.dgcolhEmpType.HeaderText = "Employement Type"
        Me.dgcolhEmpType.Name = "dgcolhEmpType"
        Me.dgcolhEmpType.ReadOnly = True
        Me.dgcolhEmpType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpType.Width = 130
        '
        'dgcolhShift
        '
        Me.dgcolhShift.HeaderText = "Shift"
        Me.dgcolhShift.Name = "dgcolhShift"
        Me.dgcolhShift.ReadOnly = True
        Me.dgcolhShift.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDept
        '
        Me.dgcolhDept.HeaderText = "Department"
        Me.dgcolhDept.Name = "dgcolhDept"
        Me.dgcolhDept.ReadOnly = True
        Me.dgcolhDept.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGradeGrp
        '
        Me.dgcolhGradeGrp.HeaderText = "Grade Group"
        Me.dgcolhGradeGrp.Name = "dgcolhGradeGrp"
        Me.dgcolhGradeGrp.ReadOnly = True
        Me.dgcolhGradeGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGrade
        '
        Me.dgcolhGrade.HeaderText = "Grade"
        Me.dgcolhGrade.Name = "dgcolhGrade"
        Me.dgcolhGrade.ReadOnly = True
        Me.dgcolhGrade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGradelevel
        '
        Me.dgcolhGradelevel.HeaderText = "Grade Level"
        Me.dgcolhGradelevel.Name = "dgcolhGradelevel"
        Me.dgcolhGradelevel.ReadOnly = True
        Me.dgcolhGradelevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTranHead
        '
        Me.dgcolhTranHead.HeaderText = "Tran. Head"
        Me.dgcolhTranHead.Name = "dgcolhTranHead"
        Me.dgcolhTranHead.ReadOnly = True
        Me.dgcolhTranHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCostCenter
        '
        Me.dgcolhCostCenter.HeaderText = "Cost Center"
        Me.dgcolhCostCenter.Name = "dgcolhCostCenter"
        Me.dgcolhCostCenter.ReadOnly = True
        Me.dgcolhCostCenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSalaray
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F0"
        Me.dgcolhSalaray.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhSalaray.HeaderText = "Salary"
        Me.dgcolhSalaray.Name = "dgcolhSalaray"
        Me.dgcolhSalaray.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhSalaray.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'gbFileInfo
        '
        Me.gbFileInfo.BorderColor = System.Drawing.Color.Black
        Me.gbFileInfo.Checked = False
        Me.gbFileInfo.CollapseAllExceptThis = False
        Me.gbFileInfo.CollapsedHoverImage = Nothing
        Me.gbFileInfo.CollapsedNormalImage = Nothing
        Me.gbFileInfo.CollapsedPressedImage = Nothing
        Me.gbFileInfo.CollapseOnLoad = False
        Me.gbFileInfo.Controls.Add(Me.dtAppointdate)
        Me.gbFileInfo.Controls.Add(Me.lblAppointdate)
        Me.gbFileInfo.Controls.Add(Me.objbtnAddSalary)
        Me.gbFileInfo.Controls.Add(Me.txtSalary)
        Me.gbFileInfo.Controls.Add(Me.lblSalary)
        Me.gbFileInfo.Controls.Add(Me.cboCostcenter)
        Me.gbFileInfo.Controls.Add(Me.lblCostcenter)
        Me.gbFileInfo.Controls.Add(Me.cboTranhead)
        Me.gbFileInfo.Controls.Add(Me.lblTranHead)
        Me.gbFileInfo.Controls.Add(Me.cboGradeLevel)
        Me.gbFileInfo.Controls.Add(Me.lblGradeLevel)
        Me.gbFileInfo.Controls.Add(Me.cboGrade)
        Me.gbFileInfo.Controls.Add(Me.lblGrade)
        Me.gbFileInfo.Controls.Add(Me.lblGradeGrp)
        Me.gbFileInfo.Controls.Add(Me.cboGradeGrp)
        Me.gbFileInfo.Controls.Add(Me.lblJob)
        Me.gbFileInfo.Controls.Add(Me.cboJob)
        Me.gbFileInfo.Controls.Add(Me.objbtnSet)
        Me.gbFileInfo.Controls.Add(Me.objLine2)
        Me.gbFileInfo.Controls.Add(Me.objLine1)
        Me.gbFileInfo.Controls.Add(Me.radApplytoChecked)
        Me.gbFileInfo.Controls.Add(Me.radApplySelected)
        Me.gbFileInfo.Controls.Add(Me.radApplytoAll)
        Me.gbFileInfo.Controls.Add(Me.lblDept)
        Me.gbFileInfo.Controls.Add(Me.cboDepartment)
        Me.gbFileInfo.Controls.Add(Me.cboShift)
        Me.gbFileInfo.Controls.Add(Me.lblShift)
        Me.gbFileInfo.Controls.Add(Me.cboEmpType)
        Me.gbFileInfo.Controls.Add(Me.lblEmpType)
        Me.gbFileInfo.Controls.Add(Me.elMandatoryInfo)
        Me.gbFileInfo.Controls.Add(Me.objbtnOpenFile)
        Me.gbFileInfo.Controls.Add(Me.txtFilePath)
        Me.gbFileInfo.Controls.Add(Me.lblFileName)
        Me.gbFileInfo.ExpandedHoverImage = Nothing
        Me.gbFileInfo.ExpandedNormalImage = Nothing
        Me.gbFileInfo.ExpandedPressedImage = Nothing
        Me.gbFileInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFileInfo.HeaderHeight = 25
        Me.gbFileInfo.HeaderMessage = ""
        Me.gbFileInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFileInfo.HeightOnCollapse = 0
        Me.gbFileInfo.LeftTextSpace = 0
        Me.gbFileInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbFileInfo.Name = "gbFileInfo"
        Me.gbFileInfo.OpenHeight = 300
        Me.gbFileInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFileInfo.ShowBorder = True
        Me.gbFileInfo.ShowCheckBox = False
        Me.gbFileInfo.ShowCollapseButton = False
        Me.gbFileInfo.ShowDefaultBorderColor = True
        Me.gbFileInfo.ShowDownButton = False
        Me.gbFileInfo.ShowHeader = True
        Me.gbFileInfo.Size = New System.Drawing.Size(822, 223)
        Me.gbFileInfo.TabIndex = 2
        Me.gbFileInfo.Temp = 0
        Me.gbFileInfo.Text = "File Name"
        Me.gbFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtAppointdate
        '
        Me.dtAppointdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtAppointdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtAppointdate.Location = New System.Drawing.Point(696, 78)
        Me.dtAppointdate.Name = "dtAppointdate"
        Me.dtAppointdate.Size = New System.Drawing.Size(114, 21)
        Me.dtAppointdate.TabIndex = 49
        '
        'lblAppointdate
        '
        Me.lblAppointdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointdate.Location = New System.Drawing.Point(602, 80)
        Me.lblAppointdate.Name = "lblAppointdate"
        Me.lblAppointdate.Size = New System.Drawing.Size(88, 17)
        Me.lblAppointdate.TabIndex = 48
        Me.lblAppointdate.Text = "Appoint Date"
        Me.lblAppointdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddSalary
        '
        Me.objbtnAddSalary.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSalary.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSalary.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSalary.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSalary.BorderSelected = False
        Me.objbtnAddSalary.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSalary.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSalary.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSalary.Location = New System.Drawing.Point(567, 159)
        Me.objbtnAddSalary.Name = "objbtnAddSalary"
        Me.objbtnAddSalary.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSalary.TabIndex = 46
        '
        'txtSalary
        '
        Me.txtSalary.AllowNegative = True
        Me.txtSalary.DigitsInGroup = 0
        Me.txtSalary.Flags = 0
        Me.txtSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalary.Location = New System.Drawing.Point(420, 159)
        Me.txtSalary.MaxDecimalPlaces = 6
        Me.txtSalary.MaxWholeDigits = 21
        Me.txtSalary.Name = "txtSalary"
        Me.txtSalary.Prefix = ""
        Me.txtSalary.RangeMax = 1.7976931348623157E+308
        Me.txtSalary.RangeMin = -1.7976931348623157E+308
        Me.txtSalary.Size = New System.Drawing.Size(137, 21)
        Me.txtSalary.TabIndex = 34
        Me.txtSalary.Text = "0.00"
        Me.txtSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSalary
        '
        Me.lblSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalary.Location = New System.Drawing.Point(326, 161)
        Me.lblSalary.Name = "lblSalary"
        Me.lblSalary.Size = New System.Drawing.Size(88, 17)
        Me.lblSalary.TabIndex = 33
        Me.lblSalary.Text = "Salary"
        Me.lblSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostcenter
        '
        Me.cboCostcenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(139, 186)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(167, 21)
        Me.cboCostcenter.TabIndex = 31
        '
        'lblCostcenter
        '
        Me.lblCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostcenter.Location = New System.Drawing.Point(45, 188)
        Me.lblCostcenter.Name = "lblCostcenter"
        Me.lblCostcenter.Size = New System.Drawing.Size(88, 17)
        Me.lblCostcenter.TabIndex = 30
        Me.lblCostcenter.Text = "Cost Center"
        Me.lblCostcenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranhead
        '
        Me.cboTranhead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranhead.FormattingEnabled = True
        Me.cboTranhead.Location = New System.Drawing.Point(420, 186)
        Me.cboTranhead.Name = "cboTranhead"
        Me.cboTranhead.Size = New System.Drawing.Size(167, 21)
        Me.cboTranhead.TabIndex = 29
        '
        'lblTranHead
        '
        Me.lblTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranHead.Location = New System.Drawing.Point(326, 188)
        Me.lblTranHead.Name = "lblTranHead"
        Me.lblTranHead.Size = New System.Drawing.Size(88, 17)
        Me.lblTranHead.TabIndex = 28
        Me.lblTranHead.Text = "Tran.  Head"
        Me.lblTranHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(420, 132)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(167, 21)
        Me.cboGradeLevel.TabIndex = 27
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(326, 134)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(88, 17)
        Me.lblGradeLevel.TabIndex = 26
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(420, 105)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(167, 21)
        Me.cboGrade.TabIndex = 25
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(326, 107)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(88, 17)
        Me.lblGrade.TabIndex = 24
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeGrp
        '
        Me.lblGradeGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGrp.Location = New System.Drawing.Point(326, 80)
        Me.lblGradeGrp.Name = "lblGradeGrp"
        Me.lblGradeGrp.Size = New System.Drawing.Size(88, 17)
        Me.lblGradeGrp.TabIndex = 23
        Me.lblGradeGrp.Text = "Grade Group"
        Me.lblGradeGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeGrp
        '
        Me.cboGradeGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGrp.FormattingEnabled = True
        Me.cboGradeGrp.Location = New System.Drawing.Point(420, 78)
        Me.cboGradeGrp.Name = "cboGradeGrp"
        Me.cboGradeGrp.Size = New System.Drawing.Size(167, 21)
        Me.cboGradeGrp.TabIndex = 22
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(45, 161)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(88, 17)
        Me.lblJob.TabIndex = 21
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(139, 159)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(167, 21)
        Me.cboJob.TabIndex = 20
        '
        'objbtnSet
        '
        Me.objbtnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSet.BackColor = System.Drawing.Color.White
        Me.objbtnSet.BackgroundImage = CType(resources.GetObject("objbtnSet.BackgroundImage"), System.Drawing.Image)
        Me.objbtnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnSet.BorderColor = System.Drawing.Color.Empty
        Me.objbtnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnSet.FlatAppearance.BorderSize = 0
        Me.objbtnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSet.ForeColor = System.Drawing.Color.Black
        Me.objbtnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnSet.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Location = New System.Drawing.Point(696, 183)
        Me.objbtnSet.Name = "objbtnSet"
        Me.objbtnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Size = New System.Drawing.Size(114, 30)
        Me.objbtnSet.TabIndex = 18
        Me.objbtnSet.Text = "&Set"
        Me.objbtnSet.UseVisualStyleBackColor = False
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(593, 78)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(13, 142)
        Me.objLine2.TabIndex = 17
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(312, 78)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(13, 142)
        Me.objLine1.TabIndex = 16
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(612, 161)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(133, 17)
        Me.radApplytoChecked.TabIndex = 15
        Me.radApplytoChecked.TabStop = True
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = True
        '
        'radApplySelected
        '
        Me.radApplySelected.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplySelected.Location = New System.Drawing.Point(612, 134)
        Me.radApplySelected.Name = "radApplySelected"
        Me.radApplySelected.Size = New System.Drawing.Size(133, 17)
        Me.radApplySelected.TabIndex = 13
        Me.radApplySelected.TabStop = True
        Me.radApplySelected.Text = "Apply to Selected"
        Me.radApplySelected.UseVisualStyleBackColor = True
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(612, 107)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(133, 17)
        Me.radApplytoAll.TabIndex = 14
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = True
        '
        'lblDept
        '
        Me.lblDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDept.Location = New System.Drawing.Point(45, 134)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(88, 17)
        Me.lblDept.TabIndex = 12
        Me.lblDept.Text = "Department"
        Me.lblDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(139, 132)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(167, 21)
        Me.cboDepartment.TabIndex = 11
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(139, 105)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(167, 21)
        Me.cboShift.TabIndex = 10
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(45, 107)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(88, 17)
        Me.lblShift.TabIndex = 9
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmpType
        '
        Me.cboEmpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpType.FormattingEnabled = True
        Me.cboEmpType.Location = New System.Drawing.Point(139, 78)
        Me.cboEmpType.Name = "cboEmpType"
        Me.cboEmpType.Size = New System.Drawing.Size(167, 21)
        Me.cboEmpType.TabIndex = 8
        '
        'lblEmpType
        '
        Me.lblEmpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpType.Location = New System.Drawing.Point(45, 80)
        Me.lblEmpType.Name = "lblEmpType"
        Me.lblEmpType.Size = New System.Drawing.Size(88, 17)
        Me.lblEmpType.TabIndex = 7
        Me.lblEmpType.Text = "Emp. Type"
        Me.lblEmpType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elMandatoryInfo
        '
        Me.elMandatoryInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elMandatoryInfo.Location = New System.Drawing.Point(11, 58)
        Me.elMandatoryInfo.Name = "elMandatoryInfo"
        Me.elMandatoryInfo.Size = New System.Drawing.Size(644, 17)
        Me.elMandatoryInfo.TabIndex = 6
        Me.elMandatoryInfo.Text = "Mandatory Information"
        Me.elMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOpenFile
        '
        Me.objbtnOpenFile.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOpenFile.BorderSelected = False
        Me.objbtnOpenFile.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnOpenFile.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOpenFile.Location = New System.Drawing.Point(661, 34)
        Me.objbtnOpenFile.Name = "objbtnOpenFile"
        Me.objbtnOpenFile.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOpenFile.TabIndex = 5
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(139, 34)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(516, 21)
        Me.txtFilePath.TabIndex = 3
        '
        'lblFileName
        '
        Me.lblFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(45, 36)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(88, 17)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "File Name"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnImport)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 543)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(846, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(737, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(634, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'ofdlgOpen
        '
        Me.ofdlgOpen.FileName = "OpenFileDialog1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Trans. Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 130
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn3.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Head Type"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 135
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Type Of"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 135
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Shift"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Grade Group"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Grade"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Grade Level"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Tran. Head"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Cost Center"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        '
        'frmImportEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 598)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportEmployee"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Employee"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFileInfo.ResumeLayout(False)
        Me.gbFileInfo.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents gbFileInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents txtFilePath As System.Windows.Forms.TextBox
    Friend WithEvents elMandatoryInfo As eZee.Common.eZeeLine
    Friend WithEvents objbtnOpenFile As eZee.Common.eZeeGradientButton
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents lblDept As System.Windows.Forms.Label
    Friend WithEvents radApplySelected As System.Windows.Forms.RadioButton
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents cboEmpType As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpType As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnSet As eZee.Common.eZeeLightButton
    Friend WithEvents ofdlgOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents dgvImportInfo As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostcenter As System.Windows.Forms.Label
    Friend WithEvents cboTranhead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTranHead As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblGradeGrp As System.Windows.Forms.Label
    Friend WithEvents cboGradeGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents txtSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents lblSalary As System.Windows.Forms.Label
    Friend WithEvents objbtnAddSalary As eZee.Common.eZeeGradientButton
    Friend WithEvents dtAppointdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAppointdate As System.Windows.Forms.Label
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOthername As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSurname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhappointeddate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradelevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSalaray As eZee.Common.DataGridViewNumericTextBoxColumn
End Class
