﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpImportWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpImportWizard))
        Me.eZeeWizImportEmp = New eZee.Common.eZeeWizard
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lnkAllocationFormat = New System.Windows.Forms.LinkLabel
        Me.lblMessage2 = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.tabcMappingInfo = New System.Windows.Forms.TabControl
        Me.tabpMandatoryInfo = New System.Windows.Forms.TabPage
        Me.objlblSign21 = New System.Windows.Forms.Label
        Me.lblPolicy = New System.Windows.Forms.Label
        Me.cboPolicy = New System.Windows.Forms.ComboBox
        Me.lblTermReason = New System.Windows.Forms.Label
        Me.cboEOCReason = New System.Windows.Forms.ComboBox
        Me.objlblSign17 = New System.Windows.Forms.Label
        Me.lblPayPoint = New System.Windows.Forms.Label
        Me.cboPayPoint = New System.Windows.Forms.ComboBox
        Me.lblPayType = New System.Windows.Forms.Label
        Me.cboPayType = New System.Windows.Forms.ComboBox
        Me.objlblSign13 = New System.Windows.Forms.Label
        Me.lblEOC = New System.Windows.Forms.Label
        Me.cboEOC_Date = New System.Windows.Forms.ComboBox
        Me.objlblSign20 = New System.Windows.Forms.Label
        Me.lblRetirementdate = New System.Windows.Forms.Label
        Me.cboRetirementdate = New System.Windows.Forms.ComboBox
        Me.cboEmail = New System.Windows.Forms.ComboBox
        Me.lblEmail = New System.Windows.Forms.Label
        Me.objlblSign19 = New System.Windows.Forms.Label
        Me.cboTransHead = New System.Windows.Forms.ComboBox
        Me.lblTransHead = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.cboFirstName = New System.Windows.Forms.ComboBox
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.cboOtherName = New System.Windows.Forms.ComboBox
        Me.lblOtherName = New System.Windows.Forms.Label
        Me.cboSurname = New System.Windows.Forms.ComboBox
        Me.lblSurName = New System.Windows.Forms.Label
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.cboEmploymentType = New System.Windows.Forms.ComboBox
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.lblEmpType = New System.Windows.Forms.Label
        Me.lblShift = New System.Windows.Forms.Label
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.objlblSign5 = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.lblDept = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblCostcenter = New System.Windows.Forms.Label
        Me.objlblSign7 = New System.Windows.Forms.Label
        Me.objlblSign8 = New System.Windows.Forms.Label
        Me.objlblSign9 = New System.Windows.Forms.Label
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.objlblSign11 = New System.Windows.Forms.Label
        Me.objlblSign10 = New System.Windows.Forms.Label
        Me.objlblSign12 = New System.Windows.Forms.Label
        Me.cboGradeName = New System.Windows.Forms.ComboBox
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.lblGradeGrp = New System.Windows.Forms.Label
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.objlblSign18 = New System.Windows.Forms.Label
        Me.objgbSalaryGrade = New System.Windows.Forms.GroupBox
        Me.radDefinedSalary = New System.Windows.Forms.RadioButton
        Me.radGradedSalary = New System.Windows.Forms.RadioButton
        Me.cboSalary = New System.Windows.Forms.ComboBox
        Me.lblDisplayName = New System.Windows.Forms.Label
        Me.objlblSign14 = New System.Windows.Forms.Label
        Me.cboDisplayName = New System.Windows.Forms.ComboBox
        Me.lblSalary = New System.Windows.Forms.Label
        Me.cboBirthDate = New System.Windows.Forms.ComboBox
        Me.lblBirthDate = New System.Windows.Forms.Label
        Me.objlblSign16 = New System.Windows.Forms.Label
        Me.lblAppointdate = New System.Windows.Forms.Label
        Me.cboAppointedDate = New System.Windows.Forms.ComboBox
        Me.tabpOptionalMapping = New System.Windows.Forms.TabPage
        Me.pnlOptional = New System.Windows.Forms.Panel
        Me.cboSportHobbies = New System.Windows.Forms.ComboBox
        Me.lblSportHobbies = New System.Windows.Forms.Label
        Me.cboMedicalDisabilities = New System.Windows.Forms.ComboBox
        Me.lblMedicalDisabilities = New System.Windows.Forms.Label
        Me.cboAllergies = New System.Windows.Forms.ComboBox
        Me.lblAllergies = New System.Windows.Forms.Label
        Me.cboWeight = New System.Windows.Forms.ComboBox
        Me.cboHeight = New System.Windows.Forms.ComboBox
        Me.cboExtraTel = New System.Windows.Forms.ComboBox
        Me.lblWeight = New System.Windows.Forms.Label
        Me.lblHeight = New System.Windows.Forms.Label
        Me.lblExtraTel = New System.Windows.Forms.Label
        Me.cboTitle = New System.Windows.Forms.ComboBox
        Me.lblLanguage4 = New System.Windows.Forms.Label
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.cboLanguage4 = New System.Windows.Forms.ComboBox
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.lblLanguage3 = New System.Windows.Forms.Label
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.cboLanguage3 = New System.Windows.Forms.ComboBox
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.lblLanguage2 = New System.Windows.Forms.Label
        Me.lblUnit = New System.Windows.Forms.Label
        Me.cboLanguage2 = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.lblLanguage1 = New System.Windows.Forms.Label
        Me.cboTeam = New System.Windows.Forms.ComboBox
        Me.cboLanguage1 = New System.Windows.Forms.ComboBox
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.lblHair = New System.Windows.Forms.Label
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboHair = New System.Windows.Forms.ComboBox
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.lblReligion = New System.Windows.Forms.Label
        Me.cboLeavingDate = New System.Windows.Forms.ComboBox
        Me.cboReligion = New System.Windows.Forms.ComboBox
        Me.lblEmpTitle = New System.Windows.Forms.Label
        Me.lblEthinCity = New System.Windows.Forms.Label
        Me.lblTerminationDate = New System.Windows.Forms.Label
        Me.cboEthincity = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.lblNationality = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.cboNationality = New System.Windows.Forms.ComboBox
        Me.cboMaritalStatus = New System.Windows.Forms.ComboBox
        Me.lblEyeColor = New System.Windows.Forms.Label
        Me.lblMaritalStatus = New System.Windows.Forms.Label
        Me.cboEyeColor = New System.Windows.Forms.ComboBox
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.lblBloodGroup = New System.Windows.Forms.Label
        Me.cboMarriedDate = New System.Windows.Forms.ComboBox
        Me.cboBloodGroup = New System.Windows.Forms.ComboBox
        Me.cboDepartmentGroup = New System.Windows.Forms.ComboBox
        Me.lblComplexion = New System.Windows.Forms.Label
        Me.lblMarriedDate = New System.Windows.Forms.Label
        Me.cboComplexion = New System.Windows.Forms.ComboBox
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.lblClasses = New System.Windows.Forms.Label
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.cboClasses = New System.Windows.Forms.ComboBox
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.cboAddress1 = New System.Windows.Forms.ComboBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.radActiveEmployee = New System.Windows.Forms.RadioButton
        Me.radInactiveEmployee = New System.Windows.Forms.RadioButton
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.cboAddress2 = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.lblAddress1 = New System.Windows.Forms.Label
        Me.cboPincode = New System.Windows.Forms.ComboBox
        Me.lblPincode = New System.Windows.Forms.Label
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhlogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.chkAssignDefaulTranHeads = New System.Windows.Forms.CheckBox
        Me.eZeeWizImportEmp.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.tabcMappingInfo.SuspendLayout()
        Me.tabpMandatoryInfo.SuspendLayout()
        Me.objgbSalaryGrade.SuspendLayout()
        Me.tabpOptionalMapping.SuspendLayout()
        Me.pnlOptional.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeWizImportEmp
        '
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizImportEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizImportEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizImportEmp.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizImportEmp.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizImportEmp.Name = "eZeeWizImportEmp"
        Me.eZeeWizImportEmp.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizImportEmp.SaveEnabled = True
        Me.eZeeWizImportEmp.SaveText = "Save && Finish"
        Me.eZeeWizImportEmp.SaveVisible = False
        Me.eZeeWizImportEmp.SetSaveIndexBeforeFinishIndex = False
        Me.eZeeWizImportEmp.Size = New System.Drawing.Size(836, 502)
        Me.eZeeWizImportEmp.TabIndex = 0
        Me.eZeeWizImportEmp.WelcomeImage = Nothing
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.lnkAllocationFormat)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage2)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(836, 454)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'lnkAllocationFormat
        '
        Me.lnkAllocationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocationFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocationFormat.Location = New System.Drawing.Point(692, 156)
        Me.lnkAllocationFormat.Name = "lnkAllocationFormat"
        Me.lnkAllocationFormat.Size = New System.Drawing.Size(132, 22)
        Me.lnkAllocationFormat.TabIndex = 102
        Me.lnkAllocationFormat.TabStop = True
        Me.lnkAllocationFormat.Text = "Get Import Format"
        Me.lnkAllocationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMessage2
        '
        Me.lblMessage2.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage2.ForeColor = System.Drawing.Color.Maroon
        Me.lblMessage2.Location = New System.Drawing.Point(186, 249)
        Me.lblMessage2.Name = "lblMessage2"
        Me.lblMessage2.Size = New System.Drawing.Size(638, 136)
        Me.lblMessage2.TabIndex = 19
        Me.lblMessage2.Text = "1. Please set all date(s) columns in selected file as blank if not available."
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(188, 49)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(580, 55)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import 'Employee' records made from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(188, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(580, 23)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Employee Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(801, 186)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(189, 186)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(606, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(189, 157)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(228, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(836, 454)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFieldMapping.Controls.Add(Me.tabcMappingInfo)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.Controls.Add(Me.cboAddress1)
        Me.gbFieldMapping.Controls.Add(Me.GroupBox1)
        Me.gbFieldMapping.Controls.Add(Me.lblAddress2)
        Me.gbFieldMapping.Controls.Add(Me.cboCountry)
        Me.gbFieldMapping.Controls.Add(Me.lblCountry)
        Me.gbFieldMapping.Controls.Add(Me.cboAddress2)
        Me.gbFieldMapping.Controls.Add(Me.cboState)
        Me.gbFieldMapping.Controls.Add(Me.lblState)
        Me.gbFieldMapping.Controls.Add(Me.lblAddress1)
        Me.gbFieldMapping.Controls.Add(Me.cboPincode)
        Me.gbFieldMapping.Controls.Add(Me.lblPincode)
        Me.gbFieldMapping.Controls.Add(Me.cboCity)
        Me.gbFieldMapping.Controls.Add(Me.lblCity)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFieldMapping.Margin = New System.Windows.Forms.Padding(0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(672, 452)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(524, 25)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(142, 20)
        Me.lnkAutoMap.TabIndex = 106
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tabcMappingInfo
        '
        Me.tabcMappingInfo.Controls.Add(Me.tabpMandatoryInfo)
        Me.tabcMappingInfo.Controls.Add(Me.tabpOptionalMapping)
        Me.tabcMappingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcMappingInfo.Location = New System.Drawing.Point(3, 27)
        Me.tabcMappingInfo.Name = "tabcMappingInfo"
        Me.tabcMappingInfo.SelectedIndex = 0
        Me.tabcMappingInfo.Size = New System.Drawing.Size(666, 423)
        Me.tabcMappingInfo.TabIndex = 0
        '
        'tabpMandatoryInfo
        '
        Me.tabpMandatoryInfo.Controls.Add(Me.chkAssignDefaulTranHeads)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign21)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblPolicy)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboPolicy)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblTermReason)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboEOCReason)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign17)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblPayPoint)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboPayPoint)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblPayType)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboPayType)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign13)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblEOC)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboEOC_Date)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign20)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblRetirementdate)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboRetirementdate)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboEmail)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblEmail)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign19)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboTransHead)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblTransHead)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboEmployeeCode)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboGender)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblGender)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign3)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign2)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign1)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblEmployeeCode)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboFirstName)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblFirstName)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboOtherName)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblOtherName)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboSurname)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblSurName)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign4)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboEmploymentType)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboShift)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblEmpType)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblShift)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign6)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign5)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboDepartment)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboJob)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboCostcenter)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblDept)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblJob)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblCostcenter)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign7)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign8)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign9)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboGradeGroup)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign11)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign10)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign12)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboGradeName)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboGradeLevel)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblGradeGrp)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblGrade)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblGradeLevel)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign18)
        Me.tabpMandatoryInfo.Controls.Add(Me.objgbSalaryGrade)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboSalary)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblDisplayName)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign14)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboDisplayName)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblSalary)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboBirthDate)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblBirthDate)
        Me.tabpMandatoryInfo.Controls.Add(Me.objlblSign16)
        Me.tabpMandatoryInfo.Controls.Add(Me.lblAppointdate)
        Me.tabpMandatoryInfo.Controls.Add(Me.cboAppointedDate)
        Me.tabpMandatoryInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpMandatoryInfo.Name = "tabpMandatoryInfo"
        Me.tabpMandatoryInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpMandatoryInfo.Size = New System.Drawing.Size(658, 397)
        Me.tabpMandatoryInfo.TabIndex = 0
        Me.tabpMandatoryInfo.Text = "Mandatory Mapping"
        Me.tabpMandatoryInfo.UseVisualStyleBackColor = True
        '
        'objlblSign21
        '
        Me.objlblSign21.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign21.ForeColor = System.Drawing.Color.Red
        Me.objlblSign21.Location = New System.Drawing.Point(329, 345)
        Me.objlblSign21.Name = "objlblSign21"
        Me.objlblSign21.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign21.TabIndex = 74
        Me.objlblSign21.Text = "*"
        Me.objlblSign21.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblPolicy
        '
        Me.lblPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPolicy.Location = New System.Drawing.Point(345, 345)
        Me.lblPolicy.Name = "lblPolicy"
        Me.lblPolicy.Size = New System.Drawing.Size(96, 17)
        Me.lblPolicy.TabIndex = 72
        Me.lblPolicy.Text = "Policy"
        Me.lblPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPolicy
        '
        Me.cboPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPolicy.FormattingEnabled = True
        Me.cboPolicy.Location = New System.Drawing.Point(447, 343)
        Me.cboPolicy.Name = "cboPolicy"
        Me.cboPolicy.Size = New System.Drawing.Size(185, 21)
        Me.cboPolicy.TabIndex = 73
        '
        'lblTermReason
        '
        Me.lblTermReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTermReason.Location = New System.Drawing.Point(345, 264)
        Me.lblTermReason.Name = "lblTermReason"
        Me.lblTermReason.Size = New System.Drawing.Size(96, 17)
        Me.lblTermReason.TabIndex = 70
        Me.lblTermReason.Text = "EOC Reason"
        Me.lblTermReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEOCReason
        '
        Me.cboEOCReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEOCReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEOCReason.FormattingEnabled = True
        Me.cboEOCReason.Location = New System.Drawing.Point(447, 262)
        Me.cboEOCReason.Name = "cboEOCReason"
        Me.cboEOCReason.Size = New System.Drawing.Size(185, 21)
        Me.cboEOCReason.TabIndex = 71
        '
        'objlblSign17
        '
        Me.objlblSign17.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign17.ForeColor = System.Drawing.Color.Red
        Me.objlblSign17.Location = New System.Drawing.Point(329, 75)
        Me.objlblSign17.Name = "objlblSign17"
        Me.objlblSign17.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign17.TabIndex = 43
        Me.objlblSign17.Text = "*"
        Me.objlblSign17.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblPayPoint
        '
        Me.lblPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPoint.Location = New System.Drawing.Point(345, 318)
        Me.lblPayPoint.Name = "lblPayPoint"
        Me.lblPayPoint.Size = New System.Drawing.Size(96, 17)
        Me.lblPayPoint.TabIndex = 64
        Me.lblPayPoint.Text = "Pay Point"
        Me.lblPayPoint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPoint
        '
        Me.cboPayPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPoint.FormattingEnabled = True
        Me.cboPayPoint.Location = New System.Drawing.Point(447, 316)
        Me.cboPayPoint.Name = "cboPayPoint"
        Me.cboPayPoint.Size = New System.Drawing.Size(185, 21)
        Me.cboPayPoint.TabIndex = 65
        '
        'lblPayType
        '
        Me.lblPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayType.Location = New System.Drawing.Point(345, 291)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.Size = New System.Drawing.Size(96, 17)
        Me.lblPayType.TabIndex = 62
        Me.lblPayType.Text = "Pay Type"
        Me.lblPayType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayType
        '
        Me.cboPayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.Location = New System.Drawing.Point(447, 289)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(185, 21)
        Me.cboPayType.TabIndex = 63
        '
        'objlblSign13
        '
        Me.objlblSign13.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign13.ForeColor = System.Drawing.Color.Red
        Me.objlblSign13.Location = New System.Drawing.Point(329, 157)
        Me.objlblSign13.Name = "objlblSign13"
        Me.objlblSign13.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign13.TabIndex = 61
        Me.objlblSign13.Text = "*"
        Me.objlblSign13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblEOC
        '
        Me.lblEOC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEOC.Location = New System.Drawing.Point(345, 237)
        Me.lblEOC.Name = "lblEOC"
        Me.lblEOC.Size = New System.Drawing.Size(96, 17)
        Me.lblEOC.TabIndex = 59
        Me.lblEOC.Text = "EOC Date"
        Me.lblEOC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEOC_Date
        '
        Me.cboEOC_Date.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEOC_Date.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEOC_Date.FormattingEnabled = True
        Me.cboEOC_Date.Location = New System.Drawing.Point(447, 235)
        Me.cboEOC_Date.Name = "cboEOC_Date"
        Me.cboEOC_Date.Size = New System.Drawing.Size(185, 21)
        Me.cboEOC_Date.TabIndex = 60
        '
        'objlblSign20
        '
        Me.objlblSign20.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign20.ForeColor = System.Drawing.Color.Red
        Me.objlblSign20.Location = New System.Drawing.Point(329, 210)
        Me.objlblSign20.Name = "objlblSign20"
        Me.objlblSign20.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign20.TabIndex = 56
        Me.objlblSign20.Text = "*"
        Me.objlblSign20.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblRetirementdate
        '
        Me.lblRetirementdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetirementdate.Location = New System.Drawing.Point(345, 210)
        Me.lblRetirementdate.Name = "lblRetirementdate"
        Me.lblRetirementdate.Size = New System.Drawing.Size(96, 17)
        Me.lblRetirementdate.TabIndex = 57
        Me.lblRetirementdate.Text = "Retirement Date"
        Me.lblRetirementdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRetirementdate
        '
        Me.cboRetirementdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRetirementdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRetirementdate.FormattingEnabled = True
        Me.cboRetirementdate.Location = New System.Drawing.Point(447, 208)
        Me.cboRetirementdate.Name = "cboRetirementdate"
        Me.cboRetirementdate.Size = New System.Drawing.Size(185, 21)
        Me.cboRetirementdate.TabIndex = 58
        '
        'cboEmail
        '
        Me.cboEmail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmail.FormattingEnabled = True
        Me.cboEmail.Location = New System.Drawing.Point(447, 181)
        Me.cboEmail.Name = "cboEmail"
        Me.cboEmail.Size = New System.Drawing.Size(185, 21)
        Me.cboEmail.TabIndex = 55
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(345, 183)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(96, 17)
        Me.lblEmail.TabIndex = 54
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign19
        '
        Me.objlblSign19.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign19.ForeColor = System.Drawing.Color.Red
        Me.objlblSign19.Location = New System.Drawing.Point(329, 129)
        Me.objlblSign19.Name = "objlblSign19"
        Me.objlblSign19.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign19.TabIndex = 49
        Me.objlblSign19.Text = "*"
        Me.objlblSign19.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboTransHead
        '
        Me.cboTransHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransHead.FormattingEnabled = True
        Me.cboTransHead.Location = New System.Drawing.Point(447, 127)
        Me.cboTransHead.Name = "cboTransHead"
        Me.cboTransHead.Size = New System.Drawing.Size(185, 21)
        Me.cboTransHead.TabIndex = 51
        '
        'lblTransHead
        '
        Me.lblTransHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransHead.Location = New System.Drawing.Point(345, 129)
        Me.lblTransHead.Name = "lblTransHead"
        Me.lblTransHead.Size = New System.Drawing.Size(96, 17)
        Me.lblTransHead.TabIndex = 50
        Me.lblTransHead.Text = "Transaction Head"
        Me.lblTransHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(137, 19)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(185, 21)
        Me.cboEmployeeCode.TabIndex = 2
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(447, 154)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(185, 21)
        Me.cboGender.TabIndex = 53
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(345, 156)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(96, 17)
        Me.lblGender.TabIndex = 52
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(19, 75)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign3.TabIndex = 6
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign3.Visible = False
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(19, 48)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign2.TabIndex = 3
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(19, 21)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign1.TabIndex = 0
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(35, 21)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(96, 17)
        Me.lblEmployeeCode.TabIndex = 1
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFirstName
        '
        Me.cboFirstName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFirstName.FormattingEnabled = True
        Me.cboFirstName.Location = New System.Drawing.Point(137, 46)
        Me.cboFirstName.Name = "cboFirstName"
        Me.cboFirstName.Size = New System.Drawing.Size(185, 21)
        Me.cboFirstName.TabIndex = 5
        '
        'lblFirstName
        '
        Me.lblFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(35, 48)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(96, 17)
        Me.lblFirstName.TabIndex = 4
        Me.lblFirstName.Text = "First Name"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOtherName
        '
        Me.cboOtherName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherName.FormattingEnabled = True
        Me.cboOtherName.Location = New System.Drawing.Point(137, 73)
        Me.cboOtherName.Name = "cboOtherName"
        Me.cboOtherName.Size = New System.Drawing.Size(185, 21)
        Me.cboOtherName.TabIndex = 8
        '
        'lblOtherName
        '
        Me.lblOtherName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherName.Location = New System.Drawing.Point(35, 75)
        Me.lblOtherName.Name = "lblOtherName"
        Me.lblOtherName.Size = New System.Drawing.Size(96, 17)
        Me.lblOtherName.TabIndex = 7
        Me.lblOtherName.Text = "Other Name"
        Me.lblOtherName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSurname
        '
        Me.cboSurname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSurname.FormattingEnabled = True
        Me.cboSurname.Location = New System.Drawing.Point(137, 100)
        Me.cboSurname.Name = "cboSurname"
        Me.cboSurname.Size = New System.Drawing.Size(185, 21)
        Me.cboSurname.TabIndex = 11
        '
        'lblSurName
        '
        Me.lblSurName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurName.Location = New System.Drawing.Point(35, 102)
        Me.lblSurName.Name = "lblSurName"
        Me.lblSurName.Size = New System.Drawing.Size(96, 17)
        Me.lblSurName.TabIndex = 10
        Me.lblSurName.Text = "Surname"
        Me.lblSurName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(19, 102)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign4.TabIndex = 9
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmploymentType
        '
        Me.cboEmploymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmploymentType.FormattingEnabled = True
        Me.cboEmploymentType.Location = New System.Drawing.Point(137, 127)
        Me.cboEmploymentType.Name = "cboEmploymentType"
        Me.cboEmploymentType.Size = New System.Drawing.Size(185, 21)
        Me.cboEmploymentType.TabIndex = 14
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(137, 154)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(185, 21)
        Me.cboShift.TabIndex = 17
        '
        'lblEmpType
        '
        Me.lblEmpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpType.Location = New System.Drawing.Point(35, 129)
        Me.lblEmpType.Name = "lblEmpType"
        Me.lblEmpType.Size = New System.Drawing.Size(96, 17)
        Me.lblEmpType.TabIndex = 13
        Me.lblEmpType.Text = "Emp. Type"
        Me.lblEmpType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(35, 156)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(96, 17)
        Me.lblShift.TabIndex = 16
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(19, 156)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign6.TabIndex = 15
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign5
        '
        Me.objlblSign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign5.ForeColor = System.Drawing.Color.Red
        Me.objlblSign5.Location = New System.Drawing.Point(19, 129)
        Me.objlblSign5.Name = "objlblSign5"
        Me.objlblSign5.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign5.TabIndex = 12
        Me.objlblSign5.Text = "*"
        Me.objlblSign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(137, 181)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(185, 21)
        Me.cboDepartment.TabIndex = 20
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(137, 208)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(185, 21)
        Me.cboJob.TabIndex = 23
        '
        'cboCostcenter
        '
        Me.cboCostcenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(137, 235)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(185, 21)
        Me.cboCostcenter.TabIndex = 26
        '
        'lblDept
        '
        Me.lblDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDept.Location = New System.Drawing.Point(35, 183)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(96, 17)
        Me.lblDept.TabIndex = 19
        Me.lblDept.Text = "Department"
        Me.lblDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(35, 210)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(96, 17)
        Me.lblJob.TabIndex = 22
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCostcenter
        '
        Me.lblCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostcenter.Location = New System.Drawing.Point(35, 237)
        Me.lblCostcenter.Name = "lblCostcenter"
        Me.lblCostcenter.Size = New System.Drawing.Size(96, 17)
        Me.lblCostcenter.TabIndex = 25
        Me.lblCostcenter.Text = "Cost Center"
        Me.lblCostcenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign7
        '
        Me.objlblSign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign7.ForeColor = System.Drawing.Color.Red
        Me.objlblSign7.Location = New System.Drawing.Point(19, 183)
        Me.objlblSign7.Name = "objlblSign7"
        Me.objlblSign7.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign7.TabIndex = 18
        Me.objlblSign7.Text = "*"
        Me.objlblSign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign8
        '
        Me.objlblSign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign8.ForeColor = System.Drawing.Color.Red
        Me.objlblSign8.Location = New System.Drawing.Point(19, 210)
        Me.objlblSign8.Name = "objlblSign8"
        Me.objlblSign8.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign8.TabIndex = 21
        Me.objlblSign8.Text = "*"
        Me.objlblSign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign9
        '
        Me.objlblSign9.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign9.ForeColor = System.Drawing.Color.Red
        Me.objlblSign9.Location = New System.Drawing.Point(19, 237)
        Me.objlblSign9.Name = "objlblSign9"
        Me.objlblSign9.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign9.TabIndex = 24
        Me.objlblSign9.Text = "*"
        Me.objlblSign9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(137, 262)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(185, 21)
        Me.cboGradeGroup.TabIndex = 29
        '
        'objlblSign11
        '
        Me.objlblSign11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign11.ForeColor = System.Drawing.Color.Red
        Me.objlblSign11.Location = New System.Drawing.Point(19, 291)
        Me.objlblSign11.Name = "objlblSign11"
        Me.objlblSign11.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign11.TabIndex = 30
        Me.objlblSign11.Text = "*"
        Me.objlblSign11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign10
        '
        Me.objlblSign10.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign10.ForeColor = System.Drawing.Color.Red
        Me.objlblSign10.Location = New System.Drawing.Point(19, 264)
        Me.objlblSign10.Name = "objlblSign10"
        Me.objlblSign10.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign10.TabIndex = 27
        Me.objlblSign10.Text = "*"
        Me.objlblSign10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign12
        '
        Me.objlblSign12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign12.ForeColor = System.Drawing.Color.Red
        Me.objlblSign12.Location = New System.Drawing.Point(19, 318)
        Me.objlblSign12.Name = "objlblSign12"
        Me.objlblSign12.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign12.TabIndex = 33
        Me.objlblSign12.Text = "*"
        Me.objlblSign12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboGradeName
        '
        Me.cboGradeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeName.FormattingEnabled = True
        Me.cboGradeName.Location = New System.Drawing.Point(137, 289)
        Me.cboGradeName.Name = "cboGradeName"
        Me.cboGradeName.Size = New System.Drawing.Size(185, 21)
        Me.cboGradeName.TabIndex = 32
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(137, 316)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(185, 21)
        Me.cboGradeLevel.TabIndex = 35
        '
        'lblGradeGrp
        '
        Me.lblGradeGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGrp.Location = New System.Drawing.Point(35, 264)
        Me.lblGradeGrp.Name = "lblGradeGrp"
        Me.lblGradeGrp.Size = New System.Drawing.Size(96, 17)
        Me.lblGradeGrp.TabIndex = 28
        Me.lblGradeGrp.Text = "Grade Group"
        Me.lblGradeGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(35, 291)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(96, 17)
        Me.lblGrade.TabIndex = 31
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(35, 318)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(96, 17)
        Me.lblGradeLevel.TabIndex = 34
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign18
        '
        Me.objlblSign18.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign18.ForeColor = System.Drawing.Color.Red
        Me.objlblSign18.Location = New System.Drawing.Point(329, 102)
        Me.objlblSign18.Name = "objlblSign18"
        Me.objlblSign18.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign18.TabIndex = 46
        Me.objlblSign18.Text = "*"
        Me.objlblSign18.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objgbSalaryGrade
        '
        Me.objgbSalaryGrade.Controls.Add(Me.radDefinedSalary)
        Me.objgbSalaryGrade.Controls.Add(Me.radGradedSalary)
        Me.objgbSalaryGrade.Location = New System.Drawing.Point(17, 338)
        Me.objgbSalaryGrade.Name = "objgbSalaryGrade"
        Me.objgbSalaryGrade.Size = New System.Drawing.Size(305, 33)
        Me.objgbSalaryGrade.TabIndex = 36
        Me.objgbSalaryGrade.TabStop = False
        '
        'radDefinedSalary
        '
        Me.radDefinedSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDefinedSalary.Location = New System.Drawing.Point(21, 12)
        Me.radDefinedSalary.Name = "radDefinedSalary"
        Me.radDefinedSalary.Size = New System.Drawing.Size(133, 16)
        Me.radDefinedSalary.TabIndex = 0
        Me.radDefinedSalary.TabStop = True
        Me.radDefinedSalary.Text = "Defined Salary"
        Me.radDefinedSalary.UseVisualStyleBackColor = True
        '
        'radGradedSalary
        '
        Me.radGradedSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radGradedSalary.Location = New System.Drawing.Point(164, 12)
        Me.radGradedSalary.Name = "radGradedSalary"
        Me.radGradedSalary.Size = New System.Drawing.Size(121, 16)
        Me.radGradedSalary.TabIndex = 1
        Me.radGradedSalary.TabStop = True
        Me.radGradedSalary.Text = "Graded Salary"
        Me.radGradedSalary.UseVisualStyleBackColor = True
        '
        'cboSalary
        '
        Me.cboSalary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalary.FormattingEnabled = True
        Me.cboSalary.Location = New System.Drawing.Point(447, 19)
        Me.cboSalary.Name = "cboSalary"
        Me.cboSalary.Size = New System.Drawing.Size(185, 21)
        Me.cboSalary.TabIndex = 39
        '
        'lblDisplayName
        '
        Me.lblDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplayName.Location = New System.Drawing.Point(345, 102)
        Me.lblDisplayName.Name = "lblDisplayName"
        Me.lblDisplayName.Size = New System.Drawing.Size(96, 17)
        Me.lblDisplayName.TabIndex = 47
        Me.lblDisplayName.Text = "Display Name"
        Me.lblDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign14
        '
        Me.objlblSign14.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign14.ForeColor = System.Drawing.Color.Red
        Me.objlblSign14.Location = New System.Drawing.Point(329, 21)
        Me.objlblSign14.Name = "objlblSign14"
        Me.objlblSign14.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign14.TabIndex = 37
        Me.objlblSign14.Text = "*"
        Me.objlblSign14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboDisplayName
        '
        Me.cboDisplayName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisplayName.FormattingEnabled = True
        Me.cboDisplayName.Location = New System.Drawing.Point(447, 100)
        Me.cboDisplayName.Name = "cboDisplayName"
        Me.cboDisplayName.Size = New System.Drawing.Size(185, 21)
        Me.cboDisplayName.TabIndex = 48
        '
        'lblSalary
        '
        Me.lblSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalary.Location = New System.Drawing.Point(345, 21)
        Me.lblSalary.Name = "lblSalary"
        Me.lblSalary.Size = New System.Drawing.Size(96, 17)
        Me.lblSalary.TabIndex = 38
        Me.lblSalary.Text = "Salary"
        Me.lblSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBirthDate
        '
        Me.cboBirthDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBirthDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBirthDate.FormattingEnabled = True
        Me.cboBirthDate.Location = New System.Drawing.Point(447, 73)
        Me.cboBirthDate.Name = "cboBirthDate"
        Me.cboBirthDate.Size = New System.Drawing.Size(185, 21)
        Me.cboBirthDate.TabIndex = 45
        '
        'lblBirthDate
        '
        Me.lblBirthDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthDate.Location = New System.Drawing.Point(345, 75)
        Me.lblBirthDate.Name = "lblBirthDate"
        Me.lblBirthDate.Size = New System.Drawing.Size(96, 17)
        Me.lblBirthDate.TabIndex = 44
        Me.lblBirthDate.Text = "Birth Date"
        Me.lblBirthDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign16
        '
        Me.objlblSign16.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign16.ForeColor = System.Drawing.Color.Red
        Me.objlblSign16.Location = New System.Drawing.Point(329, 48)
        Me.objlblSign16.Name = "objlblSign16"
        Me.objlblSign16.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign16.TabIndex = 40
        Me.objlblSign16.Text = "*"
        Me.objlblSign16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblAppointdate
        '
        Me.lblAppointdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointdate.Location = New System.Drawing.Point(345, 48)
        Me.lblAppointdate.Name = "lblAppointdate"
        Me.lblAppointdate.Size = New System.Drawing.Size(96, 17)
        Me.lblAppointdate.TabIndex = 41
        Me.lblAppointdate.Text = "Appoint Date"
        Me.lblAppointdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAppointedDate
        '
        Me.cboAppointedDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppointedDate.FormattingEnabled = True
        Me.cboAppointedDate.Location = New System.Drawing.Point(447, 46)
        Me.cboAppointedDate.Name = "cboAppointedDate"
        Me.cboAppointedDate.Size = New System.Drawing.Size(185, 21)
        Me.cboAppointedDate.TabIndex = 42
        '
        'tabpOptionalMapping
        '
        Me.tabpOptionalMapping.Controls.Add(Me.pnlOptional)
        Me.tabpOptionalMapping.Location = New System.Drawing.Point(4, 22)
        Me.tabpOptionalMapping.Name = "tabpOptionalMapping"
        Me.tabpOptionalMapping.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOptionalMapping.Size = New System.Drawing.Size(658, 397)
        Me.tabpOptionalMapping.TabIndex = 1
        Me.tabpOptionalMapping.Text = "Optional Mapping"
        Me.tabpOptionalMapping.UseVisualStyleBackColor = True
        '
        'pnlOptional
        '
        Me.pnlOptional.AutoScroll = True
        Me.pnlOptional.Controls.Add(Me.cboSportHobbies)
        Me.pnlOptional.Controls.Add(Me.lblSportHobbies)
        Me.pnlOptional.Controls.Add(Me.cboMedicalDisabilities)
        Me.pnlOptional.Controls.Add(Me.lblMedicalDisabilities)
        Me.pnlOptional.Controls.Add(Me.cboAllergies)
        Me.pnlOptional.Controls.Add(Me.lblAllergies)
        Me.pnlOptional.Controls.Add(Me.cboWeight)
        Me.pnlOptional.Controls.Add(Me.cboHeight)
        Me.pnlOptional.Controls.Add(Me.cboExtraTel)
        Me.pnlOptional.Controls.Add(Me.lblWeight)
        Me.pnlOptional.Controls.Add(Me.lblHeight)
        Me.pnlOptional.Controls.Add(Me.lblExtraTel)
        Me.pnlOptional.Controls.Add(Me.cboTitle)
        Me.pnlOptional.Controls.Add(Me.lblLanguage4)
        Me.pnlOptional.Controls.Add(Me.lblUnitGroup)
        Me.pnlOptional.Controls.Add(Me.cboLanguage4)
        Me.pnlOptional.Controls.Add(Me.cboUnitGroup)
        Me.pnlOptional.Controls.Add(Me.lblLanguage3)
        Me.pnlOptional.Controls.Add(Me.cboUnit)
        Me.pnlOptional.Controls.Add(Me.cboLanguage3)
        Me.pnlOptional.Controls.Add(Me.lblSectionGroup)
        Me.pnlOptional.Controls.Add(Me.lblLanguage2)
        Me.pnlOptional.Controls.Add(Me.lblUnit)
        Me.pnlOptional.Controls.Add(Me.cboLanguage2)
        Me.pnlOptional.Controls.Add(Me.lblSection)
        Me.pnlOptional.Controls.Add(Me.lblLanguage1)
        Me.pnlOptional.Controls.Add(Me.cboTeam)
        Me.pnlOptional.Controls.Add(Me.cboLanguage1)
        Me.pnlOptional.Controls.Add(Me.cboSection)
        Me.pnlOptional.Controls.Add(Me.lblHair)
        Me.pnlOptional.Controls.Add(Me.lblTeam)
        Me.pnlOptional.Controls.Add(Me.cboHair)
        Me.pnlOptional.Controls.Add(Me.cboSectionGroup)
        Me.pnlOptional.Controls.Add(Me.lblReligion)
        Me.pnlOptional.Controls.Add(Me.cboLeavingDate)
        Me.pnlOptional.Controls.Add(Me.cboReligion)
        Me.pnlOptional.Controls.Add(Me.lblEmpTitle)
        Me.pnlOptional.Controls.Add(Me.lblEthinCity)
        Me.pnlOptional.Controls.Add(Me.lblTerminationDate)
        Me.pnlOptional.Controls.Add(Me.cboEthincity)
        Me.pnlOptional.Controls.Add(Me.lblBranch)
        Me.pnlOptional.Controls.Add(Me.lblNationality)
        Me.pnlOptional.Controls.Add(Me.cboBranch)
        Me.pnlOptional.Controls.Add(Me.cboNationality)
        Me.pnlOptional.Controls.Add(Me.cboMaritalStatus)
        Me.pnlOptional.Controls.Add(Me.lblEyeColor)
        Me.pnlOptional.Controls.Add(Me.lblMaritalStatus)
        Me.pnlOptional.Controls.Add(Me.cboEyeColor)
        Me.pnlOptional.Controls.Add(Me.lblDepartmentGroup)
        Me.pnlOptional.Controls.Add(Me.lblBloodGroup)
        Me.pnlOptional.Controls.Add(Me.cboMarriedDate)
        Me.pnlOptional.Controls.Add(Me.cboBloodGroup)
        Me.pnlOptional.Controls.Add(Me.cboDepartmentGroup)
        Me.pnlOptional.Controls.Add(Me.lblComplexion)
        Me.pnlOptional.Controls.Add(Me.lblMarriedDate)
        Me.pnlOptional.Controls.Add(Me.cboComplexion)
        Me.pnlOptional.Controls.Add(Me.cboJobGroup)
        Me.pnlOptional.Controls.Add(Me.lblClasses)
        Me.pnlOptional.Controls.Add(Me.lblJobGroup)
        Me.pnlOptional.Controls.Add(Me.cboClasses)
        Me.pnlOptional.Controls.Add(Me.cboClassGroup)
        Me.pnlOptional.Controls.Add(Me.lblClassGroup)
        Me.pnlOptional.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlOptional.Location = New System.Drawing.Point(3, 3)
        Me.pnlOptional.Name = "pnlOptional"
        Me.pnlOptional.Size = New System.Drawing.Size(652, 391)
        Me.pnlOptional.TabIndex = 114
        '
        'cboSportHobbies
        '
        Me.cboSportHobbies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSportHobbies.DropDownWidth = 180
        Me.cboSportHobbies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSportHobbies.FormattingEnabled = True
        Me.cboSportHobbies.Location = New System.Drawing.Point(137, 425)
        Me.cboSportHobbies.Name = "cboSportHobbies"
        Me.cboSportHobbies.Size = New System.Drawing.Size(185, 21)
        Me.cboSportHobbies.TabIndex = 127
        '
        'lblSportHobbies
        '
        Me.lblSportHobbies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSportHobbies.Location = New System.Drawing.Point(35, 427)
        Me.lblSportHobbies.Name = "lblSportHobbies"
        Me.lblSportHobbies.Size = New System.Drawing.Size(96, 17)
        Me.lblSportHobbies.TabIndex = 126
        Me.lblSportHobbies.Text = "Sport/Hobbies"
        Me.lblSportHobbies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMedicalDisabilities
        '
        Me.cboMedicalDisabilities.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedicalDisabilities.DropDownWidth = 180
        Me.cboMedicalDisabilities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedicalDisabilities.FormattingEnabled = True
        Me.cboMedicalDisabilities.Location = New System.Drawing.Point(434, 398)
        Me.cboMedicalDisabilities.Name = "cboMedicalDisabilities"
        Me.cboMedicalDisabilities.Size = New System.Drawing.Size(185, 21)
        Me.cboMedicalDisabilities.TabIndex = 125
        '
        'lblMedicalDisabilities
        '
        Me.lblMedicalDisabilities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalDisabilities.Location = New System.Drawing.Point(332, 400)
        Me.lblMedicalDisabilities.Name = "lblMedicalDisabilities"
        Me.lblMedicalDisabilities.Size = New System.Drawing.Size(96, 17)
        Me.lblMedicalDisabilities.TabIndex = 124
        Me.lblMedicalDisabilities.Text = "Medical Disabilities"
        Me.lblMedicalDisabilities.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllergies
        '
        Me.cboAllergies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllergies.DropDownWidth = 180
        Me.cboAllergies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllergies.FormattingEnabled = True
        Me.cboAllergies.Location = New System.Drawing.Point(434, 371)
        Me.cboAllergies.Name = "cboAllergies"
        Me.cboAllergies.Size = New System.Drawing.Size(185, 21)
        Me.cboAllergies.TabIndex = 123
        '
        'lblAllergies
        '
        Me.lblAllergies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllergies.Location = New System.Drawing.Point(332, 373)
        Me.lblAllergies.Name = "lblAllergies"
        Me.lblAllergies.Size = New System.Drawing.Size(96, 17)
        Me.lblAllergies.TabIndex = 122
        Me.lblAllergies.Text = "Allergies"
        Me.lblAllergies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWeight
        '
        Me.cboWeight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWeight.DropDownWidth = 180
        Me.cboWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWeight.FormattingEnabled = True
        Me.cboWeight.Location = New System.Drawing.Point(434, 344)
        Me.cboWeight.Name = "cboWeight"
        Me.cboWeight.Size = New System.Drawing.Size(185, 21)
        Me.cboWeight.TabIndex = 121
        '
        'cboHeight
        '
        Me.cboHeight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeight.DropDownWidth = 180
        Me.cboHeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeight.FormattingEnabled = True
        Me.cboHeight.Location = New System.Drawing.Point(434, 317)
        Me.cboHeight.Name = "cboHeight"
        Me.cboHeight.Size = New System.Drawing.Size(185, 21)
        Me.cboHeight.TabIndex = 120
        '
        'cboExtraTel
        '
        Me.cboExtraTel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExtraTel.DropDownWidth = 180
        Me.cboExtraTel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExtraTel.FormattingEnabled = True
        Me.cboExtraTel.Location = New System.Drawing.Point(434, 289)
        Me.cboExtraTel.Name = "cboExtraTel"
        Me.cboExtraTel.Size = New System.Drawing.Size(185, 21)
        Me.cboExtraTel.TabIndex = 119
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(332, 346)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(96, 17)
        Me.lblWeight.TabIndex = 118
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHeight
        '
        Me.lblHeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeight.Location = New System.Drawing.Point(332, 319)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(96, 17)
        Me.lblHeight.TabIndex = 116
        Me.lblHeight.Text = "Height"
        Me.lblHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExtraTel
        '
        Me.lblExtraTel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExtraTel.Location = New System.Drawing.Point(332, 291)
        Me.lblExtraTel.Name = "lblExtraTel"
        Me.lblExtraTel.Size = New System.Drawing.Size(96, 17)
        Me.lblExtraTel.TabIndex = 114
        Me.lblExtraTel.Text = "Tel. Ext"
        Me.lblExtraTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTitle
        '
        Me.cboTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTitle.FormattingEnabled = True
        Me.cboTitle.Location = New System.Drawing.Point(137, 19)
        Me.cboTitle.Name = "cboTitle"
        Me.cboTitle.Size = New System.Drawing.Size(185, 21)
        Me.cboTitle.TabIndex = 52
        '
        'lblLanguage4
        '
        Me.lblLanguage4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage4.Location = New System.Drawing.Point(332, 264)
        Me.lblLanguage4.Name = "lblLanguage4"
        Me.lblLanguage4.Size = New System.Drawing.Size(96, 17)
        Me.lblLanguage4.TabIndex = 112
        Me.lblLanguage4.Text = "Language4"
        Me.lblLanguage4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(35, 156)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(96, 17)
        Me.lblUnitGroup.TabIndex = 59
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLanguage4
        '
        Me.cboLanguage4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage4.DropDownWidth = 180
        Me.cboLanguage4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage4.FormattingEnabled = True
        Me.cboLanguage4.Location = New System.Drawing.Point(434, 262)
        Me.cboLanguage4.Name = "cboLanguage4"
        Me.cboLanguage4.Size = New System.Drawing.Size(185, 21)
        Me.cboLanguage4.TabIndex = 113
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(137, 154)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(185, 21)
        Me.cboUnitGroup.TabIndex = 60
        '
        'lblLanguage3
        '
        Me.lblLanguage3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage3.Location = New System.Drawing.Point(332, 237)
        Me.lblLanguage3.Name = "lblLanguage3"
        Me.lblLanguage3.Size = New System.Drawing.Size(96, 17)
        Me.lblLanguage3.TabIndex = 110
        Me.lblLanguage3.Text = "Language3"
        Me.lblLanguage3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnit
        '
        Me.cboUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Location = New System.Drawing.Point(137, 181)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(185, 21)
        Me.cboUnit.TabIndex = 62
        '
        'cboLanguage3
        '
        Me.cboLanguage3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage3.DropDownWidth = 180
        Me.cboLanguage3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage3.FormattingEnabled = True
        Me.cboLanguage3.Location = New System.Drawing.Point(434, 235)
        Me.cboLanguage3.Name = "cboLanguage3"
        Me.cboLanguage3.Size = New System.Drawing.Size(185, 21)
        Me.cboLanguage3.TabIndex = 111
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(35, 102)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(96, 17)
        Me.lblSectionGroup.TabIndex = 55
        Me.lblSectionGroup.Text = "Section Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLanguage2
        '
        Me.lblLanguage2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage2.Location = New System.Drawing.Point(332, 210)
        Me.lblLanguage2.Name = "lblLanguage2"
        Me.lblLanguage2.Size = New System.Drawing.Size(96, 17)
        Me.lblLanguage2.TabIndex = 108
        Me.lblLanguage2.Text = "Language2"
        Me.lblLanguage2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUnit
        '
        Me.lblUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnit.Location = New System.Drawing.Point(35, 183)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(96, 17)
        Me.lblUnit.TabIndex = 61
        Me.lblUnit.Text = "Unit"
        Me.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLanguage2
        '
        Me.cboLanguage2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage2.DropDownWidth = 180
        Me.cboLanguage2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage2.FormattingEnabled = True
        Me.cboLanguage2.Location = New System.Drawing.Point(434, 208)
        Me.cboLanguage2.Name = "cboLanguage2"
        Me.cboLanguage2.Size = New System.Drawing.Size(185, 21)
        Me.cboLanguage2.TabIndex = 109
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(35, 129)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(96, 17)
        Me.lblSection.TabIndex = 57
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLanguage1
        '
        Me.lblLanguage1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage1.Location = New System.Drawing.Point(332, 183)
        Me.lblLanguage1.Name = "lblLanguage1"
        Me.lblLanguage1.Size = New System.Drawing.Size(96, 17)
        Me.lblLanguage1.TabIndex = 106
        Me.lblLanguage1.Text = "Language1"
        Me.lblLanguage1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTeam
        '
        Me.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeam.FormattingEnabled = True
        Me.cboTeam.Location = New System.Drawing.Point(137, 208)
        Me.cboTeam.Name = "cboTeam"
        Me.cboTeam.Size = New System.Drawing.Size(185, 21)
        Me.cboTeam.TabIndex = 64
        '
        'cboLanguage1
        '
        Me.cboLanguage1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage1.DropDownWidth = 180
        Me.cboLanguage1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage1.FormattingEnabled = True
        Me.cboLanguage1.Location = New System.Drawing.Point(434, 181)
        Me.cboLanguage1.Name = "cboLanguage1"
        Me.cboLanguage1.Size = New System.Drawing.Size(185, 21)
        Me.cboLanguage1.TabIndex = 107
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(137, 127)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(185, 21)
        Me.cboSection.TabIndex = 58
        '
        'lblHair
        '
        Me.lblHair.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHair.Location = New System.Drawing.Point(332, 156)
        Me.lblHair.Name = "lblHair"
        Me.lblHair.Size = New System.Drawing.Size(96, 17)
        Me.lblHair.TabIndex = 104
        Me.lblHair.Text = "Hair"
        Me.lblHair.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(35, 210)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(96, 17)
        Me.lblTeam.TabIndex = 63
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHair
        '
        Me.cboHair.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHair.DropDownWidth = 180
        Me.cboHair.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHair.FormattingEnabled = True
        Me.cboHair.Location = New System.Drawing.Point(434, 154)
        Me.cboHair.Name = "cboHair"
        Me.cboHair.Size = New System.Drawing.Size(185, 21)
        Me.cboHair.TabIndex = 105
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(137, 100)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(185, 21)
        Me.cboSectionGroup.TabIndex = 56
        '
        'lblReligion
        '
        Me.lblReligion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReligion.Location = New System.Drawing.Point(332, 129)
        Me.lblReligion.Name = "lblReligion"
        Me.lblReligion.Size = New System.Drawing.Size(96, 17)
        Me.lblReligion.TabIndex = 102
        Me.lblReligion.Text = "Religion"
        Me.lblReligion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLeavingDate
        '
        Me.cboLeavingDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeavingDate.FormattingEnabled = True
        Me.cboLeavingDate.Location = New System.Drawing.Point(137, 317)
        Me.cboLeavingDate.Name = "cboLeavingDate"
        Me.cboLeavingDate.Size = New System.Drawing.Size(185, 21)
        Me.cboLeavingDate.TabIndex = 67
        '
        'cboReligion
        '
        Me.cboReligion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReligion.DropDownWidth = 180
        Me.cboReligion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReligion.FormattingEnabled = True
        Me.cboReligion.Location = New System.Drawing.Point(434, 127)
        Me.cboReligion.Name = "cboReligion"
        Me.cboReligion.Size = New System.Drawing.Size(185, 21)
        Me.cboReligion.TabIndex = 103
        '
        'lblEmpTitle
        '
        Me.lblEmpTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpTitle.Location = New System.Drawing.Point(35, 21)
        Me.lblEmpTitle.Name = "lblEmpTitle"
        Me.lblEmpTitle.Size = New System.Drawing.Size(96, 17)
        Me.lblEmpTitle.TabIndex = 51
        Me.lblEmpTitle.Text = "Title"
        Me.lblEmpTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEthinCity
        '
        Me.lblEthinCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEthinCity.Location = New System.Drawing.Point(332, 102)
        Me.lblEthinCity.Name = "lblEthinCity"
        Me.lblEthinCity.Size = New System.Drawing.Size(96, 17)
        Me.lblEthinCity.TabIndex = 100
        Me.lblEthinCity.Text = "Ethnicity"
        Me.lblEthinCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTerminationDate
        '
        Me.lblTerminationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerminationDate.Location = New System.Drawing.Point(35, 319)
        Me.lblTerminationDate.Name = "lblTerminationDate"
        Me.lblTerminationDate.Size = New System.Drawing.Size(96, 17)
        Me.lblTerminationDate.TabIndex = 66
        Me.lblTerminationDate.Text = "Leaving Date"
        Me.lblTerminationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEthincity
        '
        Me.cboEthincity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEthincity.DropDownWidth = 180
        Me.cboEthincity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEthincity.FormattingEnabled = True
        Me.cboEthincity.Location = New System.Drawing.Point(434, 100)
        Me.cboEthincity.Name = "cboEthincity"
        Me.cboEthincity.Size = New System.Drawing.Size(185, 21)
        Me.cboEthincity.TabIndex = 101
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(35, 48)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(96, 17)
        Me.lblBranch.TabIndex = 53
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNationality
        '
        Me.lblNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNationality.Location = New System.Drawing.Point(332, 75)
        Me.lblNationality.Name = "lblNationality"
        Me.lblNationality.Size = New System.Drawing.Size(96, 17)
        Me.lblNationality.TabIndex = 98
        Me.lblNationality.Text = "Nationality"
        Me.lblNationality.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(137, 46)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(185, 21)
        Me.cboBranch.TabIndex = 54
        '
        'cboNationality
        '
        Me.cboNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNationality.DropDownWidth = 180
        Me.cboNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNationality.FormattingEnabled = True
        Me.cboNationality.Location = New System.Drawing.Point(434, 73)
        Me.cboNationality.Name = "cboNationality"
        Me.cboNationality.Size = New System.Drawing.Size(185, 21)
        Me.cboNationality.TabIndex = 99
        '
        'cboMaritalStatus
        '
        Me.cboMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMaritalStatus.FormattingEnabled = True
        Me.cboMaritalStatus.Location = New System.Drawing.Point(137, 344)
        Me.cboMaritalStatus.Name = "cboMaritalStatus"
        Me.cboMaritalStatus.Size = New System.Drawing.Size(185, 21)
        Me.cboMaritalStatus.TabIndex = 71
        '
        'lblEyeColor
        '
        Me.lblEyeColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEyeColor.Location = New System.Drawing.Point(332, 48)
        Me.lblEyeColor.Name = "lblEyeColor"
        Me.lblEyeColor.Size = New System.Drawing.Size(96, 17)
        Me.lblEyeColor.TabIndex = 96
        Me.lblEyeColor.Text = "Eye Color"
        Me.lblEyeColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMaritalStatus
        '
        Me.lblMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaritalStatus.Location = New System.Drawing.Point(35, 346)
        Me.lblMaritalStatus.Name = "lblMaritalStatus"
        Me.lblMaritalStatus.Size = New System.Drawing.Size(96, 17)
        Me.lblMaritalStatus.TabIndex = 70
        Me.lblMaritalStatus.Text = "Marital Status"
        Me.lblMaritalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEyeColor
        '
        Me.cboEyeColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEyeColor.DropDownWidth = 180
        Me.cboEyeColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEyeColor.FormattingEnabled = True
        Me.cboEyeColor.Location = New System.Drawing.Point(434, 46)
        Me.cboEyeColor.Name = "cboEyeColor"
        Me.cboEyeColor.Size = New System.Drawing.Size(185, 21)
        Me.cboEyeColor.TabIndex = 97
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(35, 75)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(96, 17)
        Me.lblDepartmentGroup.TabIndex = 55
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBloodGroup
        '
        Me.lblBloodGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBloodGroup.Location = New System.Drawing.Point(332, 21)
        Me.lblBloodGroup.Name = "lblBloodGroup"
        Me.lblBloodGroup.Size = New System.Drawing.Size(96, 17)
        Me.lblBloodGroup.TabIndex = 94
        Me.lblBloodGroup.Text = "Blood Group"
        Me.lblBloodGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMarriedDate
        '
        Me.cboMarriedDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMarriedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMarriedDate.FormattingEnabled = True
        Me.cboMarriedDate.Location = New System.Drawing.Point(137, 371)
        Me.cboMarriedDate.Name = "cboMarriedDate"
        Me.cboMarriedDate.Size = New System.Drawing.Size(185, 21)
        Me.cboMarriedDate.TabIndex = 73
        '
        'cboBloodGroup
        '
        Me.cboBloodGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBloodGroup.DropDownWidth = 180
        Me.cboBloodGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBloodGroup.FormattingEnabled = True
        Me.cboBloodGroup.Location = New System.Drawing.Point(434, 19)
        Me.cboBloodGroup.Name = "cboBloodGroup"
        Me.cboBloodGroup.Size = New System.Drawing.Size(185, 21)
        Me.cboBloodGroup.TabIndex = 95
        '
        'cboDepartmentGroup
        '
        Me.cboDepartmentGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGroup.FormattingEnabled = True
        Me.cboDepartmentGroup.Location = New System.Drawing.Point(137, 73)
        Me.cboDepartmentGroup.Name = "cboDepartmentGroup"
        Me.cboDepartmentGroup.Size = New System.Drawing.Size(185, 21)
        Me.cboDepartmentGroup.TabIndex = 56
        '
        'lblComplexion
        '
        Me.lblComplexion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComplexion.Location = New System.Drawing.Point(35, 400)
        Me.lblComplexion.Name = "lblComplexion"
        Me.lblComplexion.Size = New System.Drawing.Size(96, 17)
        Me.lblComplexion.TabIndex = 92
        Me.lblComplexion.Text = "Complexion"
        Me.lblComplexion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMarriedDate
        '
        Me.lblMarriedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarriedDate.Location = New System.Drawing.Point(35, 373)
        Me.lblMarriedDate.Name = "lblMarriedDate"
        Me.lblMarriedDate.Size = New System.Drawing.Size(96, 17)
        Me.lblMarriedDate.TabIndex = 72
        Me.lblMarriedDate.Text = "Married Date"
        Me.lblMarriedDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboComplexion
        '
        Me.cboComplexion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComplexion.DropDownWidth = 180
        Me.cboComplexion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComplexion.FormattingEnabled = True
        Me.cboComplexion.Location = New System.Drawing.Point(137, 398)
        Me.cboComplexion.Name = "cboComplexion"
        Me.cboComplexion.Size = New System.Drawing.Size(185, 21)
        Me.cboComplexion.TabIndex = 93
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(137, 235)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(185, 21)
        Me.cboJobGroup.TabIndex = 87
        '
        'lblClasses
        '
        Me.lblClasses.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasses.Location = New System.Drawing.Point(35, 291)
        Me.lblClasses.Name = "lblClasses"
        Me.lblClasses.Size = New System.Drawing.Size(96, 17)
        Me.lblClasses.TabIndex = 90
        Me.lblClasses.Text = "Classes"
        Me.lblClasses.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(35, 237)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(96, 17)
        Me.lblJobGroup.TabIndex = 86
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClasses
        '
        Me.cboClasses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClasses.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClasses.FormattingEnabled = True
        Me.cboClasses.Location = New System.Drawing.Point(137, 289)
        Me.cboClasses.Name = "cboClasses"
        Me.cboClasses.Size = New System.Drawing.Size(185, 21)
        Me.cboClasses.TabIndex = 91
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(137, 262)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(185, 21)
        Me.cboClassGroup.TabIndex = 89
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(35, 264)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(96, 17)
        Me.lblClassGroup.TabIndex = 88
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(385, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(284, 19)
        Me.lblCaption.TabIndex = 67
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboAddress1
        '
        Me.cboAddress1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAddress1.FormattingEnabled = True
        Me.cboAddress1.Location = New System.Drawing.Point(252, 163)
        Me.cboAddress1.Name = "cboAddress1"
        Me.cboAddress1.Size = New System.Drawing.Size(185, 21)
        Me.cboAddress1.TabIndex = 83
        Me.cboAddress1.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radActiveEmployee)
        Me.GroupBox1.Controls.Add(Me.radInactiveEmployee)
        Me.GroupBox1.Location = New System.Drawing.Point(150, 219)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(287, 33)
        Me.GroupBox1.TabIndex = 65
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Visible = False
        '
        'radActiveEmployee
        '
        Me.radActiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radActiveEmployee.Location = New System.Drawing.Point(6, 12)
        Me.radActiveEmployee.Name = "radActiveEmployee"
        Me.radActiveEmployee.Size = New System.Drawing.Size(117, 16)
        Me.radActiveEmployee.TabIndex = 0
        Me.radActiveEmployee.TabStop = True
        Me.radActiveEmployee.Text = "Active Employee"
        Me.radActiveEmployee.UseVisualStyleBackColor = True
        '
        'radInactiveEmployee
        '
        Me.radInactiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInactiveEmployee.Location = New System.Drawing.Point(140, 12)
        Me.radInactiveEmployee.Name = "radInactiveEmployee"
        Me.radInactiveEmployee.Size = New System.Drawing.Size(125, 16)
        Me.radInactiveEmployee.TabIndex = 1
        Me.radInactiveEmployee.TabStop = True
        Me.radInactiveEmployee.Text = "Inactive Employee"
        Me.radInactiveEmployee.UseVisualStyleBackColor = True
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(150, 192)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(96, 17)
        Me.lblAddress2.TabIndex = 84
        Me.lblAddress2.Text = "Address2"
        Me.lblAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAddress2.Visible = False
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(252, 258)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(185, 21)
        Me.cboCountry.TabIndex = 75
        Me.cboCountry.Visible = False
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(150, 260)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(96, 17)
        Me.lblCountry.TabIndex = 74
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCountry.Visible = False
        '
        'cboAddress2
        '
        Me.cboAddress2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAddress2.FormattingEnabled = True
        Me.cboAddress2.Location = New System.Drawing.Point(252, 190)
        Me.cboAddress2.Name = "cboAddress2"
        Me.cboAddress2.Size = New System.Drawing.Size(185, 21)
        Me.cboAddress2.TabIndex = 85
        Me.cboAddress2.Visible = False
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(252, 285)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(185, 21)
        Me.cboState.TabIndex = 77
        Me.cboState.Visible = False
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(150, 287)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(96, 17)
        Me.lblState.TabIndex = 76
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblState.Visible = False
        '
        'lblAddress1
        '
        Me.lblAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress1.Location = New System.Drawing.Point(150, 165)
        Me.lblAddress1.Name = "lblAddress1"
        Me.lblAddress1.Size = New System.Drawing.Size(96, 17)
        Me.lblAddress1.TabIndex = 82
        Me.lblAddress1.Text = "Address1"
        Me.lblAddress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAddress1.Visible = False
        '
        'cboPincode
        '
        Me.cboPincode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPincode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPincode.FormattingEnabled = True
        Me.cboPincode.Location = New System.Drawing.Point(252, 339)
        Me.cboPincode.Name = "cboPincode"
        Me.cboPincode.Size = New System.Drawing.Size(185, 21)
        Me.cboPincode.TabIndex = 81
        Me.cboPincode.Visible = False
        '
        'lblPincode
        '
        Me.lblPincode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPincode.Location = New System.Drawing.Point(150, 341)
        Me.lblPincode.Name = "lblPincode"
        Me.lblPincode.Size = New System.Drawing.Size(96, 17)
        Me.lblPincode.TabIndex = 80
        Me.lblPincode.Text = "Pincode"
        Me.lblPincode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPincode.Visible = False
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(252, 312)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(185, 21)
        Me.cboCity.TabIndex = 79
        Me.cboCity.Visible = False
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(150, 314)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(96, 17)
        Me.lblCity.TabIndex = 78
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCity.Visible = False
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(836, 454)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 416)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 20
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhlogindate, Me.colhStatus, Me.colhMessage, Me.objcolhstatus, Me.objcolhDate})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(812, 341)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhlogindate
        '
        Me.colhlogindate.HeaderText = "Date"
        Me.colhlogindate.Name = "colhlogindate"
        Me.colhlogindate.ReadOnly = True
        Me.colhlogindate.Visible = False
        Me.colhlogindate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(812, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(690, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(576, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(690, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(622, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(735, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(576, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(735, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(622, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAssignDefaulTranHeads
        '
        Me.chkAssignDefaulTranHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAssignDefaulTranHeads.Location = New System.Drawing.Point(348, 370)
        Me.chkAssignDefaulTranHeads.Name = "chkAssignDefaulTranHeads"
        Me.chkAssignDefaulTranHeads.Size = New System.Drawing.Size(241, 17)
        Me.chkAssignDefaulTranHeads.TabIndex = 75
        Me.chkAssignDefaulTranHeads.Text = "Assign Default Transaction Heads"
        Me.chkAssignDefaulTranHeads.UseVisualStyleBackColor = True
        '
        'frmEmpImportWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(836, 502)
        Me.Controls.Add(Me.eZeeWizImportEmp)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpImportWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Employee"
        Me.eZeeWizImportEmp.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.tabcMappingInfo.ResumeLayout(False)
        Me.tabpMandatoryInfo.ResumeLayout(False)
        Me.objgbSalaryGrade.ResumeLayout(False)
        Me.tabpOptionalMapping.ResumeLayout(False)
        Me.pnlOptional.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.WizPageImporting.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeWizImportEmp As eZee.Common.eZeeWizard
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblMessage2 As System.Windows.Forms.Label
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhlogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objgbSalaryGrade As System.Windows.Forms.GroupBox
    Friend WithEvents radDefinedSalary As System.Windows.Forms.RadioButton
    Friend WithEvents radGradedSalary As System.Windows.Forms.RadioButton
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents cboAddress2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblAddress1 As System.Windows.Forms.Label
    Friend WithEvents cboAddress1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents lblPincode As System.Windows.Forms.Label
    Friend WithEvents cboPincode As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign18 As System.Windows.Forms.Label
    Friend WithEvents objlblSign17 As System.Windows.Forms.Label
    Friend WithEvents objlblSign14 As System.Windows.Forms.Label
    Friend WithEvents objlblSign16 As System.Windows.Forms.Label
    Friend WithEvents lblDisplayName As System.Windows.Forms.Label
    Friend WithEvents cboDisplayName As System.Windows.Forms.ComboBox
    Friend WithEvents cboBirthDate As System.Windows.Forms.ComboBox
    Friend WithEvents cboAppointedDate As System.Windows.Forms.ComboBox
    Friend WithEvents cboSalary As System.Windows.Forms.ComboBox
    Friend WithEvents lblBirthDate As System.Windows.Forms.Label
    Friend WithEvents lblAppointdate As System.Windows.Forms.Label
    Friend WithEvents lblSalary As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblGradeGrp As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign12 As System.Windows.Forms.Label
    Friend WithEvents objlblSign9 As System.Windows.Forms.Label
    Friend WithEvents objlblSign10 As System.Windows.Forms.Label
    Friend WithEvents objlblSign11 As System.Windows.Forms.Label
    Friend WithEvents objlblSign8 As System.Windows.Forms.Label
    Friend WithEvents objlblSign5 As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents objlblSign7 As System.Windows.Forms.Label
    Friend WithEvents lblCostcenter As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblDept As System.Windows.Forms.Label
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents lblEmpType As System.Windows.Forms.Label
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmploymentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherName As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboSurname As System.Windows.Forms.ComboBox
    Friend WithEvents cboOtherName As System.Windows.Forms.ComboBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents cboFirstName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents lblSurName As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents lblEmpTitle As System.Windows.Forms.Label
    Friend WithEvents cboTitle As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboTeam As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblTerminationDate As System.Windows.Forms.Label
    Friend WithEvents cboLeavingDate As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radActiveEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents radInactiveEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents lblMarriedDate As System.Windows.Forms.Label
    Friend WithEvents cboMarriedDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblMaritalStatus As System.Windows.Forms.Label
    Friend WithEvents cboMaritalStatus As System.Windows.Forms.ComboBox
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tabcMappingInfo As System.Windows.Forms.TabControl
    Friend WithEvents tabpMandatoryInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpOptionalMapping As System.Windows.Forms.TabPage
    Friend WithEvents objlblSign19 As System.Windows.Forms.Label
    Friend WithEvents cboTransHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransHead As System.Windows.Forms.Label
    Friend WithEvents lblClasses As System.Windows.Forms.Label
    Friend WithEvents cboClasses As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartmentGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents cboEmail As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents objlblSign20 As System.Windows.Forms.Label
    Friend WithEvents lblRetirementdate As System.Windows.Forms.Label
    Friend WithEvents cboRetirementdate As System.Windows.Forms.ComboBox
    Friend WithEvents lblEOC As System.Windows.Forms.Label
    Friend WithEvents cboEOC_Date As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign13 As System.Windows.Forms.Label
    Friend WithEvents lblPayType As System.Windows.Forms.Label
    Friend WithEvents cboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPoint As System.Windows.Forms.Label
    Friend WithEvents cboPayPoint As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAllocationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents lblTermReason As System.Windows.Forms.Label
    Friend WithEvents cboEOCReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblPolicy As System.Windows.Forms.Label
    Friend WithEvents cboPolicy As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign21 As System.Windows.Forms.Label
    Friend WithEvents lblHair As System.Windows.Forms.Label
    Friend WithEvents cboHair As System.Windows.Forms.ComboBox
    Friend WithEvents lblReligion As System.Windows.Forms.Label
    Friend WithEvents cboReligion As System.Windows.Forms.ComboBox
    Friend WithEvents lblEthinCity As System.Windows.Forms.Label
    Friend WithEvents cboEthincity As System.Windows.Forms.ComboBox
    Friend WithEvents lblNationality As System.Windows.Forms.Label
    Friend WithEvents cboNationality As System.Windows.Forms.ComboBox
    Friend WithEvents lblEyeColor As System.Windows.Forms.Label
    Friend WithEvents cboEyeColor As System.Windows.Forms.ComboBox
    Friend WithEvents lblBloodGroup As System.Windows.Forms.Label
    Friend WithEvents cboBloodGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblComplexion As System.Windows.Forms.Label
    Friend WithEvents cboComplexion As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage4 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage4 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage3 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage2 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage1 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage1 As System.Windows.Forms.ComboBox
    Friend WithEvents pnlOptional As System.Windows.Forms.Panel
    Friend WithEvents cboWeight As System.Windows.Forms.ComboBox
    Friend WithEvents cboHeight As System.Windows.Forms.ComboBox
    Friend WithEvents cboExtraTel As System.Windows.Forms.ComboBox
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblExtraTel As System.Windows.Forms.Label
    Friend WithEvents cboMedicalDisabilities As System.Windows.Forms.ComboBox
    Friend WithEvents lblMedicalDisabilities As System.Windows.Forms.Label
    Friend WithEvents cboAllergies As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllergies As System.Windows.Forms.Label
    Friend WithEvents cboSportHobbies As System.Windows.Forms.ComboBox
    Friend WithEvents lblSportHobbies As System.Windows.Forms.Label
    Friend WithEvents chkAssignDefaulTranHeads As System.Windows.Forms.CheckBox
End Class
