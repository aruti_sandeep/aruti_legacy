﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmBulkMovementUpdateWizard
    Private mstrModuleName As String = "frmBulkMovementUpdateWizard"

#Region " Private Variables "

    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region "Private Funcation(S)"

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next
            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboBranch.Items.Add(dtColumns.ColumnName)
                cboDepartmentGroup.Items.Add(dtColumns.ColumnName)
                cboDepartment.Items.Add(dtColumns.ColumnName)
                cboSectionGroup.Items.Add(dtColumns.ColumnName)
                cboSection.Items.Add(dtColumns.ColumnName)
                cboUnitGroup.Items.Add(dtColumns.ColumnName)
                cboUnit.Items.Add(dtColumns.ColumnName)
                cboTeam.Items.Add(dtColumns.ColumnName)
                cboClassGroup.Items.Add(dtColumns.ColumnName)
                cboClass.Items.Add(dtColumns.ColumnName)
                cboAllocationChangeReason.Items.Add(dtColumns.ColumnName)

                cboCostCenter.Items.Add(dtColumns.ColumnName)
                cboCostCenterChangeReason.Items.Add(dtColumns.ColumnName)

                cboJobGroup.Items.Add(dtColumns.ColumnName)
                cboJob.Items.Add(dtColumns.ColumnName)
                cboCategorizeChangeReason.Items.Add(dtColumns.ColumnName)

                cboEOCDate.Items.Add(dtColumns.ColumnName)
                cboLeavingDate.Items.Add(dtColumns.ColumnName)
                cboTerminationReason.Items.Add(dtColumns.ColumnName)
                cboIsExcludePayroll.Items.Add(dtColumns.ColumnName)

                cboRetirementDate.Items.Add(dtColumns.ColumnName)
                cboRetirementChangeReason.Items.Add(dtColumns.ColumnName)

                cboProbationFromDate.Items.Add(dtColumns.ColumnName)
                cboProbationToDate.Items.Add(dtColumns.ColumnName)
                cboProbationChangeReason.Items.Add(dtColumns.ColumnName)

                cboConfirmationDate.Items.Add(dtColumns.ColumnName)
                cboConfirmationChangeReason.Items.Add(dtColumns.ColumnName)

                cboSuspensionFromDate.Items.Add(dtColumns.ColumnName)
                cboSuspensionToDate.Items.Add(dtColumns.ColumnName)
                cboSuspensionChangeReason.Items.Add(dtColumns.ColumnName)

                cboWorkPermitNo.Items.Add(dtColumns.ColumnName)
                cboWorkPermitCountry.Items.Add(dtColumns.ColumnName)
                cboWorkPermitIssueDate.Items.Add(dtColumns.ColumnName)
                cboWorkPermitExpiryDate.Items.Add(dtColumns.ColumnName)
                cboWorkPermitChangeReason.Items.Add(dtColumns.ColumnName)
                cboWorkPermitIssuePlace.Items.Add(dtColumns.ColumnName)

                cboResidentPermitNo.Items.Add(dtColumns.ColumnName)
                cboResidentCountry.Items.Add(dtColumns.ColumnName)
                cboResidentIssueDate.Items.Add(dtColumns.ColumnName)
                cboResidentExpiryDate.Items.Add(dtColumns.ColumnName)
                cboResidentChangeReason.Items.Add(dtColumns.ColumnName)
                cboResidentIssuePlace.Items.Add(dtColumns.ColumnName)

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                cboExemptionFromDate.Items.Add(dtColumns.ColumnName)
                cboExemptionToDate.Items.Add(dtColumns.ColumnName)
                cboExemptionChangeReason.Items.Add(dtColumns.ColumnName)
                'Sohail (21 Oct 2019) -- End

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known


                If dtColumns.ColumnName = Language.getMessage(mstrModuleName, 210, "EmployeeCode") Then
                    dtColumns.Caption = "EmployeeCode"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 169, "Branch") Then
                    dtColumns.Caption = "Branch"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 170, "DepartmentGroup") Then
                    dtColumns.Caption = "DepartmentGroup"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 171, "Department") Then
                    dtColumns.Caption = "Department"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 172, "SectionGroup") Then
                    dtColumns.Caption = "SectionGroup"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 173, "Section") Then
                    dtColumns.Caption = "Section"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 174, "UnitGroup") Then
                    dtColumns.Caption = "UnitGroup"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 175, "Unit") Then
                    dtColumns.Caption = "Unit"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 176, "Team") Then
                    dtColumns.Caption = "Team"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 177, "ClassGroup") Then
                    dtColumns.Caption = "ClassGroup"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 178, "Class") Then
                    dtColumns.Caption = "Class"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 179, "AllocationChangeReason") Then
                    dtColumns.Caption = "AllocationChangeReason"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 180, "CostCenter") Then
                    dtColumns.Caption = "CostCenter"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 181, "CostCenterChangeReason") Then
                    dtColumns.Caption = "CostCenterChangeReason"


                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 182, "JobGroup") Then
                    dtColumns.Caption = "JobGroup"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 183, "Job") Then
                    dtColumns.Caption = "Job"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 184, "CategorizeChangeReason") Then
                    dtColumns.Caption = "CategorizeChangeReason"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 185, "EOCDate") Then
                    dtColumns.Caption = "EOCDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 186, "LeavingDate") Then
                    dtColumns.Caption = "LeavingDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 187, "TerminationReason") Then
                    dtColumns.Caption = "TerminationReason"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 188, "IsExcludePayroll") Then
                    dtColumns.Caption = "IsExcludePayroll"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 189, "RetirementDate") Then
                    dtColumns.Caption = "RetirementDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 190, "RetirementChangeReason") Then
                    dtColumns.Caption = "RetirementChangeReason"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 191, "ProbationFromDate") Then
                    dtColumns.Caption = "RetirementDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 192, "ProbationToDate") Then
                    dtColumns.Caption = "RetirementChangeReason"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 193, "ProbationChangeReason") Then
                    dtColumns.Caption = "RetirementDate"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 194, "ConfirmationDate") Then
                    dtColumns.Caption = "RetirementDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 195, "ConfirmationChangeReason") Then
                    dtColumns.Caption = "RetirementChangeReason"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 196, "SuspensionFromDate") Then
                    dtColumns.Caption = "SuspensionFromDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 197, "SuspensionToDate") Then
                    dtColumns.Caption = "SuspensionToDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 198, "SuspensionChangeReason") Then
                    dtColumns.Caption = "SuspensionChangeReason"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 199, "WorkPermitNo") Then
                    dtColumns.Caption = "WorkPermitNo"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 200, "WorkPermitCountry") Then
                    dtColumns.Caption = "WorkPermitCountry"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 201, "WorkPermitIssueDate") Then
                    dtColumns.Caption = "WorkPermitIssueDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 202, "WorkPermitExpiryDate") Then
                    dtColumns.Caption = "WorkPermitExpiryDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 203, "WorkPermitIssuePlace") Then
                    dtColumns.Caption = "WorkPermitIssuePlace"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 204, "WorkPermitChangeReason") Then
                    dtColumns.Caption = "WorkPermitChangeReason"

                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 205, "ResidentPermitNo") Then
                    dtColumns.Caption = "ResidentPermitNo"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 206, "ResidentCountry") Then
                    dtColumns.Caption = "ResidentCountry"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 207, "ResidentIssueDate") Then
                    dtColumns.Caption = "ResidentIssueDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 208, "ResidentIssuePlace") Then
                    dtColumns.Caption = "ResidentIssuePlace"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 209, "ResidentChangeReason") Then
                    dtColumns.Caption = "ResidentChangeReason"

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 275, "ExemptionFromDate") Then
                    dtColumns.Caption = "ExemptionFromDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 276, "ExemptionToDate") Then
                    dtColumns.Caption = "ExemptionToDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 277, "ExemptionChangeReason") Then
                    dtColumns.Caption = "ExemptionChangeReason"
                    'Sohail (21 Oct 2019) -- End

                End If

                'Gajanan (24 Nov 2018) -- End

            Next
            mds_ImportData.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True
            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("branch", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("departmentgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("deptname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("secgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("sections", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("unitgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("unit", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("team", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("clsgrp", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("class", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("allocationreason", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("costcenter", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("ccreason", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("jobgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("job", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("jobreason", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("eocdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("leavingdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("eocreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("isexcludepayroll", System.Type.GetType("System.Boolean")).DefaultValue = False

            mdt_ImportData_Others.Columns.Add("rdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("rtreason", System.Type.GetType("System.String")).DefaultValue = ""

            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            mdt_ImportData_Others.Columns.Add("prbfrmdate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("prbtodate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("prbreason", GetType(System.String)).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("cnfdate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("cnfreason", GetType(System.String)).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("suspfrmdate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("susptodate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("suspreason", GetType(System.String)).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("wpermitno", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("wpermitcountry", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("wpermitissuedate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("wpermitexpirydate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("wpermitissueplace", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("wpermitreason", GetType(System.String)).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("rpermitno", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("rpermitcountry", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("rpermitissuedate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("rpermitexpirydate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("rpermitissueplace", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("rpermitreason", GetType(System.String)).DefaultValue = ""
            'S.SANDEEP [14-JUN-2018] -- END


            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            mdt_ImportData_Others.Columns.Add("exemptionfrmdate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("exemptiontodate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("exemptionreason", GetType(System.String)).DefaultValue = ""

            'Sohail (21 Oct 2019) -- End

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow
                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

                If cboBranch.Enabled = True Then
                    drNewRow.Item("branch") = dtRow.Item(cboBranch.Text).ToString.Trim
                End If
                If cboDepartmentGroup.Enabled = True Then
                    drNewRow.Item("departmentgroup") = dtRow.Item(cboDepartmentGroup.Text).ToString.Trim
                End If
                If cboDepartment.Enabled = True Then
                    drNewRow.Item("deptname") = dtRow.Item(cboDepartment.Text).ToString.Trim
                End If
                If cboSectionGroup.Enabled = True Then
                    drNewRow.Item("secgroup") = dtRow.Item(cboSectionGroup.Text).ToString.Trim
                End If
                If cboSection.Enabled = True Then
                    drNewRow.Item("sections") = dtRow.Item(cboSection.Text).ToString.Trim
                End If
                If cboUnitGroup.Enabled = True Then
                    drNewRow.Item("unitgroup") = dtRow.Item(cboUnitGroup.Text).ToString.Trim
                End If
                If cboUnit.Enabled = True Then
                    drNewRow.Item("unit") = dtRow.Item(cboUnit.Text).ToString.Trim
                End If
                If cboTeam.Enabled = True Then
                    drNewRow.Item("team") = dtRow.Item(cboTeam.Text).ToString.Trim
                End If
                If cboClassGroup.Enabled = True Then
                    drNewRow.Item("clsgrp") = dtRow.Item(cboClassGroup.Text).ToString.Trim
                End If
                If cboClass.Enabled = True Then
                    drNewRow.Item("class") = dtRow.Item(cboClass.Text).ToString.Trim
                End If
                If cboAllocationChangeReason.Enabled = True Then
                    drNewRow.Item("allocationreason") = dtRow.Item(cboAllocationChangeReason.Text).ToString.Trim
                End If

                If cboCostCenter.Enabled = True Then
                    drNewRow.Item("costcenter") = dtRow.Item(cboCostCenter.Text).ToString.Trim
                End If
                If cboCostCenterChangeReason.Enabled = True Then
                    drNewRow.Item("ccreason") = dtRow.Item(cboCostCenterChangeReason.Text).ToString.Trim
                End If

                If cboJobGroup.Enabled = True Then
                    drNewRow.Item("jobgroup") = dtRow.Item(cboJobGroup.Text).ToString.Trim
                End If
                If cboJob.Enabled = True Then
                    drNewRow.Item("job") = dtRow.Item(cboJob.Text).ToString.Trim
                End If
                If cboCategorizeChangeReason.Enabled = True Then
                    drNewRow.Item("jobreason") = dtRow.Item(cboCategorizeChangeReason.Text).ToString.Trim
                End If

                If cboEOCDate.Enabled = True Then
                    drNewRow.Item("eocdate") = dtRow.Item(cboEOCDate.Text).ToString.Trim
                End If
                If cboLeavingDate.Enabled = True Then
                    drNewRow.Item("leavingdate") = dtRow.Item(cboLeavingDate.Text).ToString.Trim
                End If
                If cboTerminationReason.Enabled = True Then
                    drNewRow.Item("eocreason") = dtRow.Item(cboTerminationReason.Text).ToString.Trim
                End If
                If cboIsExcludePayroll.Enabled = True Then
                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'drNewRow.Item("isexcludepayroll") = dtRow.Item(cboIsExcludePayroll.Text).ToString.Trim
                    drNewRow.Item("isexcludepayroll") = CBool(dtRow.Item(cboIsExcludePayroll.Text).ToString.Trim)
                    'S.SANDEEP [20-JUN-2018] -- END
                End If

                If cboRetirementDate.Enabled = True Then
                    drNewRow.Item("rdate") = dtRow.Item(cboRetirementDate.Text).ToString.Trim
                    drNewRow.Item("rtreason") = dtRow.Item(cboRetirementChangeReason.Text).ToString.Trim
                End If

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                If cboProbationFromDate.Enabled = True Then
                    drNewRow.Item("prbfrmdate") = dtRow.Item(cboProbationFromDate.Text).ToString.Trim
                    drNewRow.Item("prbtodate") = dtRow.Item(cboProbationToDate.Text).ToString.Trim
                    drNewRow.Item("prbreason") = dtRow.Item(cboProbationChangeReason.Text).ToString.Trim
                End If

                If cboConfirmationDate.Enabled = True Then
                    drNewRow.Item("cnfdate") = dtRow.Item(cboConfirmationDate.Text).ToString.Trim
                    drNewRow.Item("cnfreason") = dtRow.Item(cboConfirmationChangeReason.Text).ToString.Trim
                End If

                If cboSuspensionFromDate.Enabled = True Then
                    drNewRow.Item("suspfrmdate") = dtRow.Item(cboSuspensionFromDate.Text).ToString.Trim
                    drNewRow.Item("susptodate") = dtRow.Item(cboSuspensionToDate.Text).ToString.Trim
                    drNewRow.Item("suspreason") = dtRow.Item(cboSuspensionChangeReason.Text).ToString.Trim
                End If

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                If cboExemptionFromDate.Enabled = True Then
                    drNewRow.Item("Exemptionfrmdate") = dtRow.Item(cboExemptionFromDate.Text).ToString.Trim
                    drNewRow.Item("Exemptiontodate") = dtRow.Item(cboExemptionToDate.Text).ToString.Trim
                    drNewRow.Item("Exemptionreason") = dtRow.Item(cboExemptionChangeReason.Text).ToString.Trim
                End If
                'Sohail (21 Oct 2019) -- End

                If cboWorkPermitNo.Enabled = True Then
                    drNewRow.Item("wpermitno") = dtRow.Item(cboWorkPermitNo.Text).ToString.Trim
                End If
                If cboWorkPermitCountry.Enabled = True Then
                    drNewRow.Item("wpermitcountry") = dtRow.Item(cboWorkPermitCountry.Text).ToString.Trim
                End If
                If cboWorkPermitIssueDate.Enabled = True Then
                    drNewRow.Item("wpermitissuedate") = dtRow.Item(cboWorkPermitIssueDate.Text).ToString.Trim
                End If
                If cboWorkPermitExpiryDate.Enabled = True Then
                    drNewRow.Item("wpermitexpirydate") = dtRow.Item(cboWorkPermitExpiryDate.Text).ToString.Trim
                End If
                If cboWorkPermitIssuePlace.Enabled = True Then
                    drNewRow.Item("wpermitissueplace") = dtRow.Item(cboWorkPermitIssuePlace.Text).ToString.Trim
                End If
                If cboWorkPermitChangeReason.Enabled = True Then
                    drNewRow.Item("wpermitreason") = dtRow.Item(cboWorkPermitChangeReason.Text).ToString.Trim
                End If

                If cboResidentPermitNo.Enabled = True Then
                    drNewRow.Item("rpermitno") = dtRow.Item(cboResidentPermitNo.Text).ToString.Trim
                End If
                If cboResidentCountry.Enabled = True Then
                    drNewRow.Item("rpermitcountry") = dtRow.Item(cboResidentCountry.Text).ToString.Trim
                End If
                If cboResidentIssueDate.Enabled = True Then
                    drNewRow.Item("rpermitissuedate") = dtRow.Item(cboResidentIssueDate.Text).ToString.Trim
                End If
                If cboResidentExpiryDate.Enabled = True Then
                    drNewRow.Item("rpermitexpirydate") = dtRow.Item(cboResidentExpiryDate.Text).ToString.Trim
                End If
                If cboResidentIssuePlace.Enabled = True Then
                    drNewRow.Item("rpermitissueplace") = dtRow.Item(cboResidentIssuePlace.Text).ToString.Trim
                End If
                If cboResidentChangeReason.Enabled = True Then
                    drNewRow.Item("rpermitreason") = dtRow.Item(cboResidentChangeReason.Text).ToString.Trim
                End If
                'S.SANDEEP [14-JUN-2018] -- END

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next


            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizUpdateEmployee.BackEnabled = False
            WizUpdateEmployee.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
            ezWait.Active = False
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboBranch.Enabled = True Then
                    If .Item(cboBranch.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Branch is mandatory information. Please select Branch to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboDepartmentGroup.Enabled = True Then
                    If .Item(cboDepartmentGroup.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Department Group is mandatory information. Please select Department Group to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboDepartment.Enabled = True Then
                    If .Item(cboDepartment.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Department is mandatory information. Please select Department to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboSectionGroup.Enabled = True Then
                    If .Item(cboSectionGroup.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sections Group is mandatory information. Please select Sections Group to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboSection.Enabled = True Then
                    If .Item(cboSection.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sections is mandatory information. Please select Sections to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboUnitGroup.Enabled = True Then
                    If .Item(cboUnitGroup.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Unit Group is mandatory information. Please select Unit Group to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboUnit.Enabled = True Then
                    If .Item(cboUnit.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Unit is mandatory information. Please select Unit to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboTeam.Enabled = True Then
                    If .Item(cboTeam.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Team is mandatory information. Please select Team to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboClassGroup.Enabled = True Then
                    If .Item(cboClassGroup.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Class Group is mandatory information. Please select Class Group to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboClass.Enabled = True Then
                    If .Item(cboClass.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Class is mandatory information. Please select Class to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboCostCenter.Enabled = True Then
                    If .Item(cboCostCenter.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If .Item(cboCostCenterChangeReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Cost Center Reason is mandatory information. Please select Cost Center Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboJobGroup.Enabled = True Then
                    If .Item(cboJobGroup.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Job Group is mandatory information. Please select Job Group to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboJob.Enabled = True Then
                    If .Item(cboJob.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Job is mandatory information. Please select Job to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboEOCDate.Enabled = True Then
                    If .Item(cboEOCDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "EOC Date is mandatory information. Please select EOC Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboEOCDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "EOC Date format is not correct. Please set correct EOC Date format  to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboLeavingDate.Enabled = True Then
                    If .Item(cboLeavingDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Leaving Date is mandatory information. Please select Leaving Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboLeavingDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Leaving Date format is not correct. Please set correct Leaving Date format  to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If

                End If
                If cboEOCDate.Enabled = True OrElse cboLeavingDate.Enabled = True Then
                    If .Item(cboTerminationReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "EOC Reason is mandatory information. Please select EOC Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If .Item(cboIsExcludePayroll.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Isexclude Payroll is mandatory information. Please select Isexclude Payroll to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    'If Boolean.Parse(.Item(cboIsexcludePayroll.Text).ToString) = False Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Isexclude Payroll is mandatory information. Please select Isexclude Payroll to continue."), enMsgBoxStyle.Information)
                    '    Return False
                    'End If
                    Dim blnIsexcludePayroll As Boolean = Nothing
                    If .Item(cboIsExcludePayroll.Text).ToString.Trim.Length > 0 Then
                        'S.SANDEEP [20-JUN-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#244}
                        'If Boolean.TryParse(.Item(cboIsExcludePayroll.Text).ToString, blnIsexcludePayroll) = False Then
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Isexclude Payroll is not vaild data."), enMsgBoxStyle.Information)
                        '    Return False
                        'End If
                        Select Case .Item(cboIsExcludePayroll.Text).ToString.ToUpper
                            Case "TRUE", "FALSE", "1", "0"
                            Case Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Isexclude Payroll is not vaild data."), enMsgBoxStyle.Information)
                            Return False
                        End Select
                        'S.SANDEEP [20-JUN-2018] -- END
                    End If
                End If

                If cboRetirementDate.Enabled = True Then
                    If .Item(cboRetirementDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Retirement Date is mandatory information. Please select Retirement Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboRetirementDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Retirement Date format is not correct. Please set correct Retirement Date format  to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                If cboProbationFromDate.Enabled = True Then
                    If .Item(cboProbationFromDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 68, "Probation From Date is mandatory information. Please select Probation From Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboProbationFromDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 69, "Probation From Date format is not correct. Please set correct Probation From Date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboProbationToDate.Enabled = True Then
                    If .Item(cboProbationToDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 70, "Probation To Date is mandatory information. Please select Probation To Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboProbationToDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 71, "Probation To Date format is not correct. Please set correct Probation To Date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboProbationChangeReason.Enabled = True Then
                    If .Item(cboProbationChangeReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 72, "Probation Change Reason is mandatory information. Please select Probation Change Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboConfirmationDate.Enabled = True Then
                    If .Item(cboConfirmationDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 73, "Confirmation Date is mandatory information. Please select Confirmation Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboConfirmationDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 74, "Confirmation Date format is not correct. Please set correct Confirmation Date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboConfirmationChangeReason.Enabled = True Then
                    If .Item(cboConfirmationChangeReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 75, "Confirmation Change Reason is mandatory information. Please select Confirmation Change Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboSuspensionFromDate.Enabled = True Then
                    If .Item(cboSuspensionFromDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 76, "Suspension From Date is mandatory information. Please select Suspension From Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboSuspensionFromDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 77, "Suspension From Date format is not correct. Please set correct Suspension From Date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboSuspensionToDate.Enabled = True Then
                    If .Item(cboSuspensionToDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 78, "Suspension To Date is mandatory information. Please select Suspension To Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboSuspensionToDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 79, "Suspension To Date format is not correct. Please set correct Suspension To Date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboSuspensionChangeReason.Enabled = True Then
                    If .Item(cboSuspensionChangeReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 80, "Suspension Change Reason is mandatory information. Please select Suspension Change Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                If cboExemptionFromDate.Enabled = True Then
                    If .Item(cboExemptionFromDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 278, "Exemption From Date is mandatory information. Please select Exemption From Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboExemptionFromDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 279, "Exemption From Date format is not correct. Please set correct Exemption From Date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboExemptionToDate.Enabled = True Then
                    If .Item(cboExemptionToDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 280, "Exemption To Date is mandatory information. Please select Exemption To Date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboExemptionToDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 281, "Exemption To Date format is not correct. Please set correct Exemption To Date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboExemptionChangeReason.Enabled = True Then
                    If .Item(cboExemptionChangeReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 282, "Exemption Change Reason is mandatory information. Please select Exemption Change Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                'Sohail (21 Oct 2019) -- End

                If cboWorkPermitNo.Enabled = True Then
                    If .Item(cboWorkPermitNo.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 81, "Workpermit number is mandatory information. Please select workpermit number to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboWorkPermitCountry.Enabled = True Then
                    If .Item(cboWorkPermitCountry.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 82, "Workpermit country is mandatory information. Please select workpermit country to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboWorkPermitIssueDate.Enabled = True Then
                    If .Item(cboWorkPermitIssueDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 83, "Workpermit issue date is mandatory information. Please select workpermit issue date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboWorkPermitIssueDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 84, "Workpermit issue date format is not correct. Please set correct workpermit issue date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboWorkPermitExpiryDate.Enabled = True Then
                    If .Item(cboWorkPermitExpiryDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 85, "Workpermit expiry date is mandatory information. Please select workpermit expiry date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboWorkPermitExpiryDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 86, "Workpermit expiry date format is not correct. Please set correct workpermit expiry date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboWorkPermitIssuePlace.Enabled = True Then
                    If .Item(cboWorkPermitIssuePlace.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 87, "Workpermit issue place is mandatory information. Please select workpermit issue place to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboWorkPermitChangeReason.Enabled = True Then
                    If .Item(cboWorkPermitChangeReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 88, "Workpermit Change Reason is mandatory information. Please select Workpermit Change Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboResidentPermitNo.Enabled = True Then
                    If .Item(cboResidentPermitNo.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 89, "Resident permit number is mandatory information. Please select resident permit number to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboResidentCountry.Enabled = True Then
                    If .Item(cboResidentCountry.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 90, "Resident permit country is mandatory information. Please select resident permit country to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboResidentIssueDate.Enabled = True Then
                    If .Item(cboResidentIssueDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 91, "Resident permit issue date is mandatory information. Please select resident permit issue date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboResidentIssueDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 92, "Resident permit issue date format is not correct. Please set correct resident permit issue date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboResidentExpiryDate.Enabled = True Then
                    If .Item(cboResidentExpiryDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 93, "Resident permit expiry date is mandatory information. Please select resident permit expiry date to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboResidentExpiryDate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 94, "Resident permit expiry date format is not correct. Please set correct resident permit expiry date format to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboResidentIssuePlace.Enabled = True Then
                    If .Item(cboResidentIssuePlace.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 95, "Resident permit issue place is mandatory information. Please select resident permit issue place to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboResidentChangeReason.Enabled = True Then
                    If .Item(cboResidentChangeReason.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 96, "Resident permit Change Reason is mandatory information. Please select resident permit Change Reason to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                'S.SANDEEP [14-JUN-2018] -- END
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            Dim mvt As New List(Of clsEmployeeMovmentApproval)
            Dim iValue As New Dictionary(Of clsEmployeeMovmentApproval.enMovementType, String)
            btnFilter.Enabled = False
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objEmp As New clsEmployee_Master
            Dim objReason As New clsAction_Reason
            Dim objcommon As New clsCommon_Master

            Dim objBranch As New clsStation : Dim objTeam As New clsTeams
            Dim objDeptGrp As New clsDepartmentGroup : Dim objDept As New clsDepartment
            Dim objSecGrp As New clsSectionGroup : Dim objSection As New clsSections
            Dim objUnitGrp As New clsUnitGroup : Dim objUnit As New clsUnits
            Dim objClsGrp As New clsClassGroup : Dim objClass As New clsClass
            Dim objEmpTransfer As New clsemployee_transfer_tran

            Dim objJobGrp As New clsJobGroup : Dim objJob As New clsJobs
            Dim objCategorize As New clsemployee_categorization_Tran

            Dim objCostCenter As New clscostcenter_master
            Dim objEmpCostCenterTran As New clsemployee_cctranhead_tran

            Dim objEDates As New clsemployee_dates_tran

            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            Dim objPermit As New clsemployee_workpermit_tran
            'S.SANDEEP [14-JUN-2018] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim objATransfers As New clsTransfer_Approval_Tran
            Dim objACategorization As New clsCategorization_Approval_Tran
            Dim objACCT As New clsCCTranhead_Approval_Tran
            Dim objADate As New clsDates_Approval_Tran
            Dim objAPermit As New clsPermit_Approval_Tran
            'S.SANDEEP [20-JUN-2018] -- END


            Dim iEmpId As Integer
            Dim iBranch As Integer : Dim iDeptGrp As Integer : Dim iDeptId As Integer
            Dim iSecGrp As Integer : Dim iSections As Integer : Dim iUnitGrp As Integer
            Dim iUnit As Integer : Dim iTeam As Integer : Dim iClsGrpId As Integer
            Dim iClsId As Integer : Dim iAllocationReasonId As Integer

            Dim iCostCntr As Integer : Dim iCCReasonId As Integer
            Dim iJobgrp As Integer : Dim iJobId As Integer : Dim iJobReasonId As Integer

            Dim iEOCReasonId As Integer = 0

            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            Dim iPermitId As Integer = 0
            'S.SANDEEP [14-JUN-2018] -- END

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                iEmpId = 0 : iCostCntr = 0 : iBranch = 0 : iDeptGrp = 0 : iDeptId = 0
                iSecGrp = 0 : iSections = 0 : iUnitGrp = 0 : iUnit = 0 : iTeam = 0 : iClsGrpId = 0
                iClsId = 0 : iAllocationReasonId = 0 : iJobgrp = 0 : iJobId = 0 : iJobReasonId = 0
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    iEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If iEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 28, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ Insert Employee Allocation  ---------------------------
                If chkBranch.Checked OrElse chkDepGroup.Checked OrElse chkDepartment.Checked OrElse _
                    chkSections.Checked OrElse chkSecGroup.Checked OrElse chkUnitGroup.Checked OrElse _
                    chkUnit.Checked OrElse chkTeam.Checked OrElse chkClassGrp.Checked OrElse _
                    chkClass.Checked Then

                    If dtRow.Item("branch").ToString.Trim.Length > 0 Then
                        iBranch = objBranch.GetStationUnkId(dtRow.Item("branch").ToString.Trim)
                        If iBranch <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 29, "Branch Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("departmentgroup").ToString.Trim.Length > 0 Then
                        iDeptGrp = CInt(objDeptGrp.GetDepartmentGrpUnkId(dtRow.Item("departmentgroup").ToString.Trim))
                        If iDeptGrp <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 30, "Department Group Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("deptname").ToString.Trim.Length > 0 Then
                        iDeptId = objDept.GetDepartmentUnkId(dtRow.Item("deptname").ToString)
                        If iDeptId <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 31, "Department Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("secgroup").ToString.Trim.Length > 0 Then
                        iSecGrp = objSecGrp.GetSectionGrpUnkId(dtRow.Item("secgroup").ToString)
                        If iSecGrp <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 32, "Section Group Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("sections").ToString.Trim.Length > 0 Then
                        iSections = objSection.GetSectionUnkId(dtRow.Item("sections").ToString)
                        If iSections <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 33, "Section Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("unitgroup").ToString.Trim.Length > 0 Then
                        iUnitGrp = objUnitGrp.GetUnitGrpUnkId(dtRow.Item("unitgroup").ToString)
                        If iUnitGrp <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 34, "Unit Group Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("unit").ToString.Trim.Length > 0 Then
                        iUnit = objUnit.GetUnitUnkId(dtRow.Item("unit").ToString)
                        If iUnit <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 35, "Unit Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("team").ToString.Trim.Length > 0 Then
                        iTeam = objTeam.GetTeamUnkId(dtRow.Item("team").ToString)
                        If iTeam <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 36, "Team Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("clsgrp").ToString.Trim.Length > 0 Then
                        iClsGrpId = objClsGrp.GetClassGrpUnkId(dtRow.Item("clsgrp").ToString)
                        If iClsGrpId <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 37, "Class Group Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("class").ToString.Trim.Length > 0 Then
                        iClsId = objClass.GetClassUnkId(dtRow.Item("class").ToString)
                        Dim dList As DataSet = objClass.getComboList("List", False, iClsGrpId)
                        iClsId = objClass.GetClassUnkId(dtRow.Item("class").ToString)

                        If iClsId > 0 Then
                            If dList.Tables(0).Select("classesunkid = " & iClsId).Count <= 0 Then
                                iClsId = 0
                            End If
                        End If

                        If iClsId <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 38, "Class Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    iAllocationReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TRANSFERS, dtRow.Item("allocationreason").ToString)
                    If iAllocationReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 39, "Allocation Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dtEmpTransfer As DataTable = objEmpTransfer.Get_Current_Allocation(dtpAllocEffDate.Value.Date, iEmpId).Tables(0)
                    If dtEmpTransfer.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtEmpTransfer.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpAllocEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 40, "Sorry, Transfer transaction for the selected effective date is already present for the employee in the selected file.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtEmpTransfer.Rows.Count > 0 Then
                        iBranch = CInt(IIf(iBranch > 0, iBranch, dtEmpTransfer.Rows(0)("stationunkid")))
                        iDeptGrp = CInt(IIf(iDeptGrp > 0, iDeptGrp, dtEmpTransfer.Rows(0)("deptgroupunkid")))
                        iDeptId = CInt(IIf(iDeptId > 0, iDeptId, dtEmpTransfer.Rows(0)("departmentunkid")))
                        iSecGrp = CInt(IIf(iSecGrp > 0, iSecGrp, dtEmpTransfer.Rows(0)("sectiongroupunkid")))
                        iSections = CInt(IIf(iSections > 0, iSections, dtEmpTransfer.Rows(0)("sectionunkid")))
                        iUnitGrp = CInt(IIf(iUnitGrp > 0, iUnitGrp, dtEmpTransfer.Rows(0)("unitgroupunkid")))
                        iUnit = CInt(IIf(iUnit > 0, iUnit, dtEmpTransfer.Rows(0)("unitunkid")))
                        iTeam = CInt(IIf(iTeam > 0, iTeam, dtEmpTransfer.Rows(0)("teamunkid")))
                        iClsGrpId = CInt(IIf(iClsGrpId > 0, iClsGrpId, dtEmpTransfer.Rows(0)("classgroupunkid")))
                        iClsId = CInt(IIf(iClsId > 0, iClsId, dtEmpTransfer.Rows(0)("classunkid")))
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    '    objEmpTransfer._Effectivedate = dtpAllocEffDate.Value
                    '    objEmpTransfer._Employeeunkid = iEmpId
                    '    objEmpTransfer._Stationunkid = iBranch 'CInt(IIf(iBranch > 0, iBranch, dtEmpTransfer.Rows(0)("stationunkid")))
                    '    objEmpTransfer._Deptgroupunkid = iDeptGrp 'CInt(IIf(iDeptGrp > 0, iDeptGrp, dtEmpTransfer.Rows(0)("deptgroupunkid")))
                    '    objEmpTransfer._Departmentunkid = iDeptId 'CInt(IIf(iDeptId > 0, iDeptId, dtEmpTransfer.Rows(0)("departmentunkid")))
                    '    objEmpTransfer._Sectiongroupunkid = iSecGrp 'CInt(IIf(iSecGrp > 0, iSecGrp, dtEmpTransfer.Rows(0)("sectiongroupunkid")))
                    '    objEmpTransfer._Sectionunkid = iSections 'CInt(IIf(iSections > 0, iSections, dtEmpTransfer.Rows(0)("sectionunkid")))
                    '    objEmpTransfer._Unitgroupunkid = iUnitGrp 'CInt(IIf(iUnitGrp > 0, iUnitGrp, dtEmpTransfer.Rows(0)("unitgroupunkid")))
                    '    objEmpTransfer._Unitunkid = iUnit 'CInt(IIf(iUnit > 0, iUnit, dtEmpTransfer.Rows(0)("unitunkid")))
                    '    objEmpTransfer._Teamunkid = iTeam 'CInt(IIf(iTeam > 0, iTeam, dtEmpTransfer.Rows(0)("teamunkid")))
                    '    objEmpTransfer._Classgroupunkid = iClsGrpId 'CInt(IIf(iClsGrpId > 0, iClsGrpId, dtEmpTransfer.Rows(0)("classgroupunkid")))
                    '    objEmpTransfer._Classunkid = iClsId 'CInt(IIf(iClsId > 0, iClsId, dtEmpTransfer.Rows(0)("classunkid")))
                    '    objEmpTransfer._Changereasonunkid = iAllocationReasonId
                    '    objEmpTransfer._Isvoid = False
                    '    objEmpTransfer._Statusunkid = 0
                    '    objEmpTransfer._Userunkid = User._Object._Userunkid
                    '    objEmpTransfer._Voiddatetime = Nothing
                    '    objEmpTransfer._Voidreason = ""
                    '    objEmpTransfer._Voiduserunkid = -1

                    '    If objEmpTransfer.Insert(Company._Object._Companyunkid, Nothing) Then
                    '        dtRow.Item("image") = imgAccept
                    '        dtRow.Item("message") = ""
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '        dtRow.Item("objStatus") = 1
                    '        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    '    Else
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = objEmpTransfer._Message
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '        dtRow.Item("objStatus") = 2
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '    End If
                    'End If


                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement



                    If objEmpTransfer.isExist(dtpAllocEffDate.Value.Date, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 211, "Sorry, transfer information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dsList As New DataSet
                    dsList = objEmpTransfer.Get_Current_Allocation(dtpAllocEffDate.Value.Date, iEmpId)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        If CInt(dsList.Tables(0).Rows(0)("stationunkid")) = iBranch AndAlso CInt(dsList.Tables(0).Rows(0)("deptgroupunkid")) = iDeptGrp _
                          AndAlso CInt(dsList.Tables(0).Rows(0)("departmentunkid")) = iDeptId AndAlso CInt(dsList.Tables(0).Rows(0)("sectiongroupunkid")) = iSecGrp _
                          AndAlso CInt(dsList.Tables(0).Rows(0)("sectionunkid")) = iSections AndAlso CInt(dsList.Tables(0).Rows(0)("unitgroupunkid")) = iUnitGrp _
                          AndAlso CInt(dsList.Tables(0).Rows(0)("unitunkid")) = iUnit AndAlso CInt(dsList.Tables(0).Rows(0)("teamunkid")) = iTeam _
                          AndAlso CInt(dsList.Tables(0).Rows(0)("classgroupunkid")) = iClsGrpId AndAlso CInt(dsList.Tables(0).Rows(0)("classunkid")) = iClsId Then

                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 212, "Sorry, transfer information is already present for the selected combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For

                        End If
                    End If
                    dsList = Nothing

                    If objEmpTransfer.isExist(dtpAllocEffDate.Value.Date, iBranch, iDeptGrp, _
                       iDeptId, iSecGrp, iSections, iUnitGrp, iUnit, iTeam, _
                        iClsGrpId, iClsId, iEmpId, -1, Nothing) Then

                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 213, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End




                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    objEmpTransfer._Effectivedate = dtpAllocEffDate.Value
                    objEmpTransfer._Employeeunkid = iEmpId
                    objEmpTransfer._Stationunkid = iBranch 'CInt(IIf(iBranch > 0, iBranch, dtEmpTransfer.Rows(0)("stationunkid")))
                    objEmpTransfer._Deptgroupunkid = iDeptGrp 'CInt(IIf(iDeptGrp > 0, iDeptGrp, dtEmpTransfer.Rows(0)("deptgroupunkid")))
                    objEmpTransfer._Departmentunkid = iDeptId 'CInt(IIf(iDeptId > 0, iDeptId, dtEmpTransfer.Rows(0)("departmentunkid")))
                    objEmpTransfer._Sectiongroupunkid = iSecGrp 'CInt(IIf(iSecGrp > 0, iSecGrp, dtEmpTransfer.Rows(0)("sectiongroupunkid")))
                    objEmpTransfer._Sectionunkid = iSections 'CInt(IIf(iSections > 0, iSections, dtEmpTransfer.Rows(0)("sectionunkid")))
                    objEmpTransfer._Unitgroupunkid = iUnitGrp 'CInt(IIf(iUnitGrp > 0, iUnitGrp, dtEmpTransfer.Rows(0)("unitgroupunkid")))
                    objEmpTransfer._Unitunkid = iUnit 'CInt(IIf(iUnit > 0, iUnit, dtEmpTransfer.Rows(0)("unitunkid")))
                    objEmpTransfer._Teamunkid = iTeam 'CInt(IIf(iTeam > 0, iTeam, dtEmpTransfer.Rows(0)("teamunkid")))
                    objEmpTransfer._Classgroupunkid = iClsGrpId 'CInt(IIf(iClsGrpId > 0, iClsGrpId, dtEmpTransfer.Rows(0)("classgroupunkid")))
                    objEmpTransfer._Classunkid = iClsId 'CInt(IIf(iClsId > 0, iClsId, dtEmpTransfer.Rows(0)("classunkid")))
                    objEmpTransfer._Changereasonunkid = iAllocationReasonId
                    objEmpTransfer._Isvoid = False
                    objEmpTransfer._Statusunkid = 0
                    objEmpTransfer._Userunkid = User._Object._Userunkid
                    objEmpTransfer._Voiddatetime = Nothing
                    objEmpTransfer._Voidreason = ""
                    objEmpTransfer._Voiduserunkid = -1



                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'If objEmpTransfer.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                        If objEmpTransfer.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            'Pinkal (12-Oct-2020) -- End
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEmpTransfer._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    Else

                        Dim strMsg As String = String.Empty
                        Dim objPMovement As New clsEmployeeMovmentApproval
                        'S.SANDEEP [18-SEP-2018] -- START                
                        'strMsg = objPMovement.IsTransferCategorizationApprovers(clsEmployeeMovmentApproval.enMovementType.TRANSFERS, _
                        '                                                        FinancialYear._Object._DatabaseName, _
                        '                                                        ConfigParameter._Object._UserAccessModeSetting, _
                        '                                                        Company._Object._Companyunkid, _
                        '                                                        FinancialYear._Object._YearUnkid, 1192, iBranch, iDeptGrp, iDeptId, iSecGrp, iSections, iUnitGrp, iUnit, iTeam, iClsGrpId, iClsId, -1, -1)
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.TRANSFERS, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1192, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, iEmpId, Nothing)
                        'S.SANDEEP [18-SEP-2018] -- END
                        
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                End If
                        objPMovement = Nothing

                        objATransfers._Audittype = enAuditType.ADD
                        objATransfers._Audituserunkid = User._Object._Userunkid
                        objATransfers._Effectivedate = dtpAllocEffDate.Value
                        objATransfers._Employeeunkid = iEmpId
                        objATransfers._Stationunkid = iBranch
                        objATransfers._Deptgroupunkid = iDeptGrp
                        objATransfers._Departmentunkid = iDeptId
                        objATransfers._Sectiongroupunkid = iSecGrp
                        objATransfers._Sectionunkid = iSections
                        objATransfers._Unitgroupunkid = iUnitGrp
                        objATransfers._Unitunkid = iUnit
                        objATransfers._Teamunkid = iTeam
                        objATransfers._Classgroupunkid = iClsGrpId
                        objATransfers._Classunkid = iClsId
                        objATransfers._Changereasonunkid = iAllocationReasonId
                        objATransfers._Isvoid = False
                        objATransfers._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objATransfers._Voiddatetime = Nothing
                        objATransfers._Voidreason = ""
                        objATransfers._Voiduserunkid = -1
                        objATransfers._Tranguid = Guid.NewGuid.ToString()
                        objATransfers._Transactiondate = Now
                        objATransfers._Remark = ""
                        objATransfers._Rehiretranunkid = 0
                        objATransfers._Mappingunkid = 0
                        objATransfers._Isweb = False
                        objATransfers._Isfinal = False
                        objATransfers._Ip = getIP()
                        objATransfers._Hostname = getHostName()
                        objATransfers._Form_Name = mstrModuleName
                        'S.SANDEEP |17-JAN-2019| -- START
                        objATransfers._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |17-JAN-2019| -- END


                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        Dim blnFlag As Boolean = False
                        blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 214, "Sorry, Information is already present in approval process with selected date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 215, "Sorry, Information is already present in approval process with selected date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then

                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 216, "Sorry, Information is already present in approval process with selected allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        'Gajanan [14-SEP-2019] -- End


                        'Gajanan [24-OCT-2019] -- Start    
                        'If objATransfers.Insert(Company._Object._Companyunkid, Nothing) Then


                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'If objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, ConfigParameter._Object._CreateADUserFromEmpMst) Then
                        If objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName) Then
                            'Pinkal (12-Oct-2020) -- End

                            'Gajanan [24-OCT-2019] -- End
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)

                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, iEmpId, Nothing))
                            mvt.Add(New clsEmployeeMovmentApproval(1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, iEmpId, Nothing, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END

                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.TRANSFERS) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.TRANSFERS, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.TRANSFERS) = iValue(clsEmployeeMovmentApproval.enMovementType.TRANSFERS) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, Nothing, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objATransfers._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    End If
                End If
                'S.SANDEEP [20-JUN-2018] -- END

                '------------------------------ Insert Employee Cost Center ---------------------------
                If chkCostCenter.Checked = True Then
                    If dtRow.Item("costcenter").ToString.Trim.Length > 0 Then
                        '-------------------Check Cost center--------------------
                        iCostCntr = objCostCenter.GetCostCenterUnkId(dtRow.Item("costcenter").ToString.Trim)
                        If iCostCntr <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 41, "Cost Center Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        '-----------------Check Cost Center Effective Date ---------------------------
                        Dim dtEmpCostCenter As DataTable = objEmpCostCenterTran.Get_Current_CostCenter_TranHeads(dtpCCEffDate.Value.Date, True, iEmpId).Tables(0)
                        If dtEmpCostCenter.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtEmpCostCenter.Rows(0)("effectivedate").ToString)) = eZeeDate.convertDate(dtpCCEffDate.Value.Date) Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 42, "Sorry, Cost Center transaction for the selected effective date is already present for the employee in the selected file.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        '-----------------Check Cost Center Reason ---------------------------
                        If dtRow.Item("ccreason").ToString.Trim.Length > 0 Then
                            iCCReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.COST_CENTER, dtRow.Item("ccreason").ToString)
                            If iCCReasonId <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 43, "Cost Center Reason Not Found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If

                        'S.SANDEEP [20-JUN-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#244}
                        'objEmpCostCenterTran._Employeeunkid = iEmpId
                        'objEmpCostCenterTran._Cctranheadvalueid = iCostCntr
                        'objEmpCostCenterTran._Changereasonunkid = iCCReasonId
                        'objEmpCostCenterTran._Effectivedate = dtpCCEffDate.Value
                        'objEmpCostCenterTran._Isvoid = False
                        'objEmpCostCenterTran._Rehiretranunkid = 0
                        'objEmpCostCenterTran._Statusunkid = 0
                        'objEmpCostCenterTran._Userunkid = User._Object._Userunkid
                        'objEmpCostCenterTran._Voiddatetime = Nothing
                        'objEmpCostCenterTran._Voidreason = ""

                        'If objEmpCostCenterTran.Insert(Nothing) Then
                        '    dtRow.Item("image") = imgAccept
                        '    dtRow.Item("message") = ""
                        '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        '    dtRow.Item("objStatus") = 1
                        '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                        'Else
                        '    dtRow.Item("image") = imgError
                        '    dtRow.Item("message") = objEmpCostCenterTran._Message
                        '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        '    dtRow.Item("objStatus") = 2
                        '    objError.Text = CStr(Val(objError.Text) + 1)
                        'End If



                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement

                        If objEmpCostCenterTran.isExist(dtpCCEffDate.Value.Date, -1, True, iEmpId, -1, Nothing) Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 217, "Sorry, cost center information is already present for the selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        Dim dsList As New DataSet
                        dsList = objEmpCostCenterTran.Get_Current_CostCenter_TranHeads(dtpCCEffDate.Value.Date, True, iEmpId)
                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            If CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid")) = iCostCntr Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 218, "Sorry, cost center information is already present for the selected effective date.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If

                        If objEmpCostCenterTran.isExist(dtpCCEffDate.Value.Date, iCostCntr, True, iEmpId, -1, Nothing) Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 219, "Sorry, cost center information is already present for the selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        'Gajanan [14-SEP-2019] -- End



                        If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                        objEmpCostCenterTran._Employeeunkid = iEmpId
                        objEmpCostCenterTran._Cctranheadvalueid = iCostCntr
                        objEmpCostCenterTran._Changereasonunkid = iCCReasonId
                        objEmpCostCenterTran._Effectivedate = dtpCCEffDate.Value
                        objEmpCostCenterTran._Isvoid = False
                        objEmpCostCenterTran._Rehiretranunkid = 0
                        objEmpCostCenterTran._Statusunkid = 0
                        objEmpCostCenterTran._Userunkid = User._Object._Userunkid
                        objEmpCostCenterTran._Voiddatetime = Nothing
                        objEmpCostCenterTran._Voidreason = ""

                        If objEmpCostCenterTran.Insert(Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objEmpCostCenterTran._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        Else

                            Dim objPMovement As New clsEmployeeMovmentApproval
                            Dim strMsg As String = String.Empty
                            strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.COSTCENTER, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1201, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, iEmpId)
                            If strMsg.Trim.Length > 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = strMsg
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                            objPMovement = Nothing

                            objACCT._Audittype = enAuditType.ADD
                            objACCT._Audituserunkid = User._Object._Userunkid
                            objACCT._Cctranheadvalueid = iCostCntr
                            objACCT._Changereasonunkid = iCCReasonId
                            objACCT._Effectivedate = dtpCCEffDate.Value
                            objACCT._Employeeunkid = iEmpId
                            objACCT._Isvoid = False
                            objACCT._Rehiretranunkid = 0
                            objACCT._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                            objACCT._Voiddatetime = Nothing
                            objACCT._Voidreason = ""
                            objACCT._Voiduserunkid = -1

                            objACCT._Tranguid = Guid.NewGuid.ToString()
                            objACCT._Transactiondate = Now
                            objACCT._Remark = ""
                            objACCT._Rehiretranunkid = 0
                            objACCT._Mappingunkid = 0
                            objACCT._Isweb = False
                            objACCT._Isfinal = False
                            objACCT._Ip = getIP()
                            objACCT._Hostname = getHostName()
                            objACCT._Form_Name = mstrModuleName
                            'S.SANDEEP |13-JUN-2019| -- START
                            'ISSUE/ENHANCEMENT : Left Out
                            objACCT._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                            'S.SANDEEP |13-JUN-2019| -- END


                            'Gajanan [14-SEP-2019] -- Start    
                            'Issue: Missing Isexist In Import Movement
                            Dim blnFlag As Boolean = False
                            blnFlag = objACCT.isExist(objACCT._Effectivedate, iCostCntr, True, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", "", Nothing, True)

                            'Gajanan [24-OCT-2019] -- Start    
                            If blnFlag Then
                                'Gajanan [24-OCT-2019] -- End
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 220, "Sorry, Information is already present in approval process with selected date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            End If

                            blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, True, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", "", Nothing, True)
                            If blnFlag Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 221, "Sorry, Information is already present in approval process with selected date.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If

                            blnFlag = objACCT.isExist(Nothing, iCostCntr, True, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", , Nothing, True)
                            If blnFlag Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 222, "Sorry, Information is already present in approval process with selected allocation combination.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If

                            'Gajanan [14-SEP-2019] -- End


                            'Pinkal (20-Dec-2024) -- Start
                            'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
                            'If objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED) Then
                            If objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED, Company._Object._Companyunkid) Then
                                'Pinkal (20-Dec-2024) -- End

                                dtRow.Item("image") = imgAccept
                                dtRow.Item("message") = ""
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                                dtRow.Item("objStatus") = 1
                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                                'S.SANDEEP |17-JAN-2019| -- START
                                'mvt.Add(New clsEmployeeMovmentApproval(1201, clsEmployeeMovmentApproval.enMovementType.COSTCENTER, iEmpId, Nothing))
                                mvt.Add(New clsEmployeeMovmentApproval(1201, clsEmployeeMovmentApproval.enMovementType.COSTCENTER, iEmpId, Nothing, clsEmployeeMovmentApproval.enOperationType.ADDED))
                                'S.SANDEEP |17-JAN-2019| -- END
                                If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.COSTCENTER) = False Then
                                    iValue.Add(clsEmployeeMovmentApproval.enMovementType.COSTCENTER, iEmpId.ToString())
                                Else
                                    iValue(clsEmployeeMovmentApproval.enMovementType.COSTCENTER) = iValue(clsEmployeeMovmentApproval.enMovementType.COSTCENTER) & "," & iEmpId.ToString()
                                End If
                                'Dim objPMovement As New clsEmployeeMovmentApproval
                                'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1201, clsEmployeeMovmentApproval.enMovementType.COSTCENTER, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, Nothing, 0, iEmpId.ToString(), "")
                                'objPMovement = Nothing

                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = objACCT._Message
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                            End If
                        End If
                        'S.SANDEEP [20-JUN-2018] -- END
                    End If
                End If
                '----------------------- Insert Recategorize Employee-------------------------------
                If chkJobGroup.Checked OrElse chkJob.Checked Then

                    If dtRow.Item("jobgroup").ToString.Trim.Length > 0 Then
                        iJobgrp = objJobGrp.GetJobGroupUnkId(dtRow.Item("jobgroup").ToString.Trim)
                        If iJobgrp <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 45, "Job Group Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("job").ToString.Trim.Length > 0 Then
                        iJobId = objJob.GetJobUnkId(dtRow.Item("job").ToString.Trim)
                        If iJobId <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 46, "Job Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    Dim dtEmpRecategorize As DataTable = objCategorize.Get_Current_Job(dtpCatEffDate.Value.Date, iEmpId).Tables(0)
                    If dtEmpRecategorize.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtEmpRecategorize.Rows(0)("effectivedate").ToString)) = eZeeDate.convertDate(dtpCatEffDate.Value.Date) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 47, "Sorry, Recategorization transaction for the selected effective date is already present for the employee in the selected file.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtRow.Item("jobreason").ToString.Trim.Length > 0 Then
                        iJobReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RECATEGORIZE, dtRow.Item("jobreason").ToString)
                        If iJobReasonId <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 48, "Recategorize Reason Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If



                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objCategorize._Effectivedate = dtpCatEffDate.Value.Date
                    'objCategorize._Employeeunkid = iEmpId

                    ''S.SANDEEP [10 FEB 2016] -- START
                    ''JOB GROUP IS NOT MANDATORY NOW
                    ''objCategorize._JobGroupunkid = CInt(IIf(iJobgrp > 0, iJobgrp, dtEmpRecategorize.Rows(0)("jobgroupunkid").ToString))
                    'objCategorize._JobGroupunkid = iJobgrp
                    ''S.SANDEEP [10 FEB 2016] -- END

                    'If dtEmpRecategorize.Rows.Count > 0 Then
                    '    iJobId = CInt(IIf(iJobId > 0, iJobId, dtEmpRecategorize.Rows(0)("jobunkid").ToString))
                    '    objCategorize._Gradeunkid = CInt(dtEmpRecategorize.Rows(0)("gradeunkid").ToString)
                    '    objCategorize._Gradelevelunkid = CInt(dtEmpRecategorize.Rows(0)("gradelevelunkid").ToString)
                    'Else
                    '    objCategorize._Gradeunkid = 0
                    '    objCategorize._Gradelevelunkid = 0
                    'End If
                    'objCategorize._Jobunkid = iJobId 'CInt(IIf(iJobId > 0, iJobId, dtEmpRecategorize.Rows(0)("jobunkid").ToString))
                    'objCategorize._Changereasonunkid = iJobReasonId
                    'objCategorize._Isvoid = False
                    'objCategorize._Statusunkid = 0
                    'objCategorize._Userunkid = User._Object._Userunkid
                    'objCategorize._Voiddatetime = Nothing
                    'objCategorize._Voidreason = ""
                    'objCategorize._Voiduserunkid = -1

                    'If objCategorize.Insert(Company._Object._Companyunkid, Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objCategorize._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If



                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement

                    If objCategorize.isExist(dtpCatEffDate.Value.Date, iEmpId, -1, -1, -1, -1, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 223, "Sorry, Re-Categorize information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dsList As New DataSet
                    dsList = objCategorize.Get_Current_Job(dtpCatEffDate.Value.Date, iEmpId)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        If CInt(dsList.Tables(0).Rows(0)("jobunkid")) = iJobId AndAlso CInt(dsList.Tables(0).Rows(0)("jobgroupunkid")) = iJobgrp Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 224, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                    dsList = Nothing

                    If objCategorize.isExist(dtpCatEffDate.Value.Date, iEmpId, iJobgrp, iJobId, -1, -1, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 225, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End


                    'Gajanan [29-Oct-2020] -- Start   
                    'Enhancement:Worked On Succession Module
                    If dtRow.Item("job").ToString.Trim.Length > 0 Then
                        If objJob.IsJobAssignAsKeyRoleToEmployee(iJobId, iEmpId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) Then
                            dtRow.Item("image") = imgWarring
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 283, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it can be assign to one and only one employee.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                            Continue For
                        End If
                    End If
                    'Gajanan [29-Oct-2020] -- End


                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

                    objCategorize._Effectivedate = dtpCatEffDate.Value.Date
                    objCategorize._Employeeunkid = iEmpId
                    objCategorize._JobGroupunkid = iJobgrp
                    If dtEmpRecategorize.Rows.Count > 0 Then
                        iJobId = CInt(IIf(iJobId > 0, iJobId, dtEmpRecategorize.Rows(0)("jobunkid").ToString))
                        objCategorize._Gradeunkid = CInt(dtEmpRecategorize.Rows(0)("gradeunkid").ToString)
                        objCategorize._Gradelevelunkid = CInt(dtEmpRecategorize.Rows(0)("gradelevelunkid").ToString)
                    Else
                        objCategorize._Gradeunkid = 0
                        objCategorize._Gradelevelunkid = 0
                    End If
                        objCategorize._Jobunkid = iJobId
                    objCategorize._Changereasonunkid = iJobReasonId
                    objCategorize._Isvoid = False
                    objCategorize._Statusunkid = 0
                    objCategorize._Userunkid = User._Object._Userunkid
                    objCategorize._Voiddatetime = Nothing
                    objCategorize._Voidreason = ""
                    objCategorize._Voiduserunkid = -1


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                        'If objCategorize.Insert(Company._Object._Companyunkid, Nothing) Then
                        If objCategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            'Pinkal (18-Aug-2018) -- End
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objCategorize._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    Else

                        Dim strMsg As String = String.Empty
                        Dim objPMovement As New clsEmployeeMovmentApproval

                        'S.SANDEEP [18-SEP-2018] -- START
                        'strMsg = objPMovement.IsTransferCategorizationApprovers(clsEmployeeMovmentApproval.enMovementType.TRANSFERS, _
                        '                                                         FinancialYear._Object._DatabaseName, _
                        '                                                         ConfigParameter._Object._UserAccessModeSetting, _
                        '                                                         Company._Object._Companyunkid, _
                        '                                                         FinancialYear._Object._YearUnkid, 1192, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, iJobgrp, iJobId)
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1193, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, iEmpId, Nothing)
                        'S.SANDEEP [18-SEP-2018] -- END

                        
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        objPMovement = Nothing

                        objACategorization._Audittype = enAuditType.ADD
                        objACategorization._Audituserunkid = User._Object._Userunkid
                        objACategorization._Effectivedate = dtpCatEffDate.Value.Date
                        objACategorization._Employeeunkid = iEmpId
                        objACategorization._Jobgroupunkid = iJobgrp
                        objACategorization._Jobunkid = iJobId
                        If dtEmpRecategorize.Rows.Count > 0 Then
                            iJobId = CInt(IIf(iJobId > 0, iJobId, dtEmpRecategorize.Rows(0)("jobunkid").ToString))
                            objACategorization._Gradeunkid = CInt(dtEmpRecategorize.Rows(0)("gradeunkid").ToString)
                            objACategorization._Gradelevelunkid = CInt(dtEmpRecategorize.Rows(0)("gradelevelunkid").ToString)
                        Else
                            objACategorization._Gradeunkid = 0
                            objACategorization._Gradelevelunkid = 0
                        End If
                        objACategorization._Changereasonunkid = iJobReasonId
                        objACategorization._Isvoid = False
                        objACategorization._Statusunkid = 0
                        objACategorization._Voiddatetime = Nothing
                        objACategorization._Voidreason = ""
                        objACategorization._Voiduserunkid = -1
                        objACategorization._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objACategorization._Tranguid = Guid.NewGuid.ToString()
                        objACategorization._Transactiondate = Now
                        objACategorization._Remark = ""
                        objACategorization._Rehiretranunkid = 0
                        objACategorization._Mappingunkid = 0
                        objACategorization._Isweb = False
                        objACategorization._Isfinal = False
                        objACategorization._Ip = getIP()
                        objACategorization._Hostname = getHostName()
                        objACategorization._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objACategorization._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END


                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        Dim blnFlag As Boolean = False
                        blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, _
                                                             objACategorization._Jobgroupunkid, objACategorization._Jobunkid, _
                                                             objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, _
                                                             clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 226, "Sorry, Information is already present in approval process with selected date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, _
                                                             0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 227, "Sorry, Information is already present in approval process with selected date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, _
                                                             objACategorization._Jobunkid, objACategorization._Gradeunkid, _
                                                             objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, _
                                                             "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 228, "Sorry, Information is already present in approval process with selected allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        'Gajanan [14-SEP-2019] -- End

                        'Gajanan [29-Oct-2020] -- Start   
                        'Enhancement:Worked On Succession Module
                        If dtRow.Item("job").ToString.Trim.Length > 0 Then
                            If objJob.IsJobAsKeyRoleToIntoApprovalFlow(iJobId, iEmpId) Then
                                dtRow.Item("image") = imgWarring
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 284, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it is already in approver.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                dtRow.Item("objStatus") = 0
                                objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                Continue For
                            End If
                        End If
                        'Gajanan [29-Oct-2020] -- End



                        'Gajanan [24-OCT-2019] -- Start    
                        'If objACategorization.Insert(Company._Object._Companyunkid, Nothing) Then


                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'If objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, ConfigParameter._Object._CreateADUserFromEmpMst) Then
                        If objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName) Then
                            'Pinkal (12-Oct-2020) -- End
                            'Gajanan [24-OCT-2019] -- End

                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, iEmpId, Nothing))
                            mvt.Add(New clsEmployeeMovmentApproval(1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, iEmpId, Nothing, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END
                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE) = iValue(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, Nothing, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objACategorization._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If

                    End If
                    'S.SANDEEP [20-JUN-2018] -- END

                End If
                '----------------------- Insert Employee Termination Date -------------------------------
                If chkEOCDate.Checked OrElse chkLeavingDate.Checked Then
                    If dtRow.Item("eocdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("eocdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 49, "EOC date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("leavingdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("leavingdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 50, "leaving Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    iEOCReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TERMINATION, dtRow.Item("eocreason").ToString.Trim)
                    If iEOCReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 51, "EOC Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtRow.Item("eocdate").ToString.Trim.Length > 0 Then
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                        'If objEmp.IsEmpTerminated(CDate(dtRow.Item("eocdate")).Date, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                        If objEmp.IsEmpTerminated(CDate(dtRow.Item("eocdate")).Date, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                            'Sohail (09 Oct 2019) -- End
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objEmp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("leavingdate").ToString.Trim.Length > 0 Then
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                        'If objEmp.IsEmpTerminated(CDate(dtRow.Item("leavingdate")), iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                        If objEmp.IsEmpTerminated(CDate(dtRow.Item("leavingdate")), iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                            'Sohail (09 Oct 2019) -- End
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objEmp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    Dim dtTermination As DataTable = objEDates.Get_Current_Dates(dtpTerEffDate.Value, enEmp_Dates_Transaction.DT_TERMINATION, iEmpId).Tables(0)
                    If dtTermination.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtTermination.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpTerEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 52, "Sorry, Termination transaction for the selected effective date is already present for the employee in the selected file.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objEDates._Effectivedate = dtpTerEffDate.Value.Date
                    'objEDates._Employeeunkid = iEmpId
                    'If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("eocdate").ToString.Trim.Length <= 0 Then
                    '    objEDates._Date1 = Nothing
                    'ElseIf dtRow.Item("eocdate").ToString.Trim.Length > 0 Then
                    '    objEDates._Date1 = CDate(dtRow.Item("eocdate")).Date
                    'ElseIf dtTermination.Rows.Count > 0 Then
                    '    objEDates._Date1 = CDate(dtTermination.Rows(0)("date1")).Date
                    'End If

                    'If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("leavingdate").ToString.Trim.Length <= 0 Then
                    '    objEDates._Date2 = Nothing
                    'ElseIf dtRow.Item("leavingdate").ToString.Trim.Length > 0 Then
                    '    objEDates._Date2 = CDate(dtRow.Item("leavingdate")).Date
                    'ElseIf dtTermination.Rows.Count > 0 Then
                    '    objEDates._Date2 = CDate(dtTermination.Rows(0)("date2")).Date
                    'End If

                    'objEDates._Isconfirmed = False
                    'objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION
                    'objEDates._Changereasonunkid = iEOCReasonId
                    'objEDates._Isvoid = False
                    'objEDates._Statusunkid = 0
                    'objEDates._Userunkid = User._Object._Userunkid
                    'objEDates._Voiddatetime = Nothing
                    'objEDates._Voidreason = ""
                    'objEDates._Voiduserunkid = -1
                    'objEDates._Actionreasonunkid = iEOCReasonId
                    'objEDates._Isexclude_payroll = CBool(dtRow.Item("isexcludepayroll").ToString)

                    'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEDates._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If


                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement

                    Dim Date1 As Date = Nothing
                    If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("eocdate").ToString.Trim.Length <= 0 Then
                        Date1 = Nothing
                    ElseIf dtRow.Item("eocdate").ToString.Trim.Length > 0 Then
                        Date1 = CDate(dtRow.Item("eocdate")).Date
                    ElseIf dtTermination.Rows.Count > 0 Then
                        Date1 = CDate(dtTermination.Rows(0)("date1")).Date
                    End If

                    Dim Date2 As Date = Nothing

                    If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("leavingdate").ToString.Trim.Length <= 0 Then
                        Date2 = Nothing
                    ElseIf dtRow.Item("leavingdate").ToString.Trim.Length > 0 Then
                        Date2 = CDate(dtRow.Item("leavingdate")).Date
                    ElseIf dtTermination.Rows.Count > 0 Then
                        Date2 = CDate(dtTermination.Rows(0)("date2")).Date
                    End If


                    If IsNothing(Date1) = False Then
                        Dim objEmployee As New clsEmployee_Master
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                        'If objEmployee.IsEmpTerminated(Date1, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                        If objEmployee.IsEmpTerminated(Date1, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                            'Sohail (09 Oct 2019) -- End
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objEmployee._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        objEmployee = Nothing
                    End If


                    If IsNothing(Date2) = False Then
                        Dim objEmployee As New clsEmployee_Master
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                        'If objEmployee.IsEmpTerminated(Date2, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                        If objEmployee.IsEmpTerminated(Date2, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                            'Sohail (09 Oct 2019) -- End
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objEmployee._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        objEmployee = Nothing
                    End If

                    Dim objTnALeaveTran As New clsTnALeaveTran
                    Dim objMData As New clsMasterData : Dim iPeriod As Integer = 0
                    iPeriod = objMData.getCurrentPeriodID(enModuleReference.Payroll, Date1, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                    If iPeriod <= 0 Then
                        objMData.getCurrentPeriodID(enModuleReference.Payroll, Date2, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                    End If
                    'Sohail (09 Oct 2019) -- Start
                    'NMB Enhancement # : privilege  Allow to change date on Closed Period on user master to change eoc / termination date in paid period or closed period.
                    'If iPeriod > 0 Then
                    If iPeriod > 0 AndAlso User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod = True Then
                        'Sohail (09 Oct 2019) -- End
                        Dim objPrd As New clscommom_period_Tran
                        objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriod
                        If objTnALeaveTran.IsPayrollProcessDone(iPeriod, iEmpId.ToString(), objPrd._End_Date, enModuleReference.Payroll) Then

                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 229, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If objEDates.IsPendingSalaryIncrement(iEmpId, Date1) = True Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 230, "Sorry, You cannot terminate this employee. Reason there are pending salary increment information present.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                    If dtpTerEffDate.Value.Date > Date1 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 237, "cannot be less then effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End


                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                        objEDates._Effectivedate = dtpTerEffDate.Value.Date
                        objEDates._Employeeunkid = iEmpId

                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        'If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("eocdate").ToString.Trim.Length <= 0 Then
                        '    objEDates._Date1 = Nothing
                        'ElseIf dtRow.Item("eocdate").ToString.Trim.Length > 0 Then
                        '    objEDates._Date1 = CDate(dtRow.Item("eocdate")).Date
                        'ElseIf dtTermination.Rows.Count > 0 Then
                        '    objEDates._Date1 = CDate(dtTermination.Rows(0)("date1")).Date
                        'End If

                        'If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("leavingdate").ToString.Trim.Length <= 0 Then
                        '    objEDates._Date2 = Nothing
                        'ElseIf dtRow.Item("leavingdate").ToString.Trim.Length > 0 Then
                        '    objEDates._Date2 = CDate(dtRow.Item("leavingdate")).Date
                        'ElseIf dtTermination.Rows.Count > 0 Then
                        '    objEDates._Date2 = CDate(dtTermination.Rows(0)("date2")).Date
                        'End If

                        objEDates._Date1 = Date1
                        objEDates._Date2 = Date2
                        'Gajanan [14-SEP-2019] -- End

                    objEDates._Isconfirmed = False
                    objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION
                    objEDates._Changereasonunkid = iEOCReasonId
                    objEDates._Isvoid = False
                    objEDates._Statusunkid = 0
                    objEDates._Userunkid = User._Object._Userunkid
                    objEDates._Voiddatetime = Nothing
                    objEDates._Voidreason = ""
                    objEDates._Voiduserunkid = -1
                    objEDates._Actionreasonunkid = iEOCReasonId
                    objEDates._Isexclude_payroll = CBool(dtRow.Item("isexcludepayroll").ToString)


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                        'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                        If objEDates.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            'Pinkal (18-Aug-2018) -- End
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEDates._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    Else

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1197
                        eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enEmp_Dates_Transaction.DT_TERMINATION, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objADate._Audittype = enAuditType.ADD
                        objADate._Audituserunkid = User._Object._Userunkid
                        objADate._Effectivedate = dtpTerEffDate.Value.Date
                        objADate._Employeeunkid = iEmpId
                        If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("eocdate").ToString.Trim.Length <= 0 Then
                            objADate._Date1 = Nothing
                        ElseIf dtRow.Item("eocdate").ToString.Trim.Length > 0 Then
                            objADate._Date1 = CDate(dtRow.Item("eocdate")).Date
                        ElseIf dtTermination.Rows.Count > 0 Then
                            objADate._Date1 = CDate(dtTermination.Rows(0)("date1")).Date
                        End If

                        If dtTermination.Rows.Count <= 0 AndAlso dtRow.Item("leavingdate").ToString.Trim.Length <= 0 Then
                            objEDates._Date2 = Nothing
                        ElseIf dtRow.Item("leavingdate").ToString.Trim.Length > 0 Then
                            objEDates._Date2 = CDate(dtRow.Item("leavingdate")).Date
                        ElseIf dtTermination.Rows.Count > 0 Then
                            objEDates._Date2 = CDate(dtTermination.Rows(0)("date2")).Date
                        End If

                        objADate._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION
                        objADate._Changereasonunkid = iEOCReasonId
                        objADate._Isvoid = False
                        objADate._Statusunkid = 0
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Actionreasonunkid = iEOCReasonId
                        objADate._Isexclude_Payroll = CBool(dtRow.Item("isexcludepayroll").ToString)
                        objADate._Isvoid = False
                        objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Tranguid = Guid.NewGuid.ToString()
                        objADate._Transactiondate = Now
                        objADate._Remark = ""
                        objADate._Rehiretranunkid = 0
                        objADate._Mappingunkid = 0
                        objADate._Isweb = False
                        objADate._Isfinal = False
                        objADate._Ip = getIP()
                        objADate._Hostname = getHostName()
                        objADate._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END


                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        If objEDates.isExist(dtpTerEffDate.Value.Date, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_TERMINATION), iEmpId, -1) Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 231, "Sorry, termination information is already present for the selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        If objEDates.isExist(Nothing, objADate._Date1, objADate._Date2, CInt(enEmp_Dates_Transaction.DT_TERMINATION), iEmpId, -1) Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 232, "Sorry, This termination information is already present.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        Dim blnFlag As Boolean = False

                        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, _
                                                   CInt(enEmp_Dates_Transaction.DT_TERMINATION), objADate._Employeeunkid, _
                                                   clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 233, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_TERMINATION), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 234, "Sorry, termination Information is already present in approval process with selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If


                        blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, CInt(enEmp_Dates_Transaction.DT_TERMINATION), objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 235, "Sorry, termination Information is already present in approval process with selected allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        'Gajanan [14-SEP-2019] -- End


                        'Gajanan [24-OCT-2019] -- Start    
                        'If objADate.Insert(Company._Object._Companyunkid, Nothing) Then
                        If objADate.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED) Then
                            'Gajanan [24-OCT-2019] -- End
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)

                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1197, clsEmployeeMovmentApproval.enMovementType.TERMINATION, iEmpId, enEmp_Dates_Transaction.DT_TERMINATION))
                            mvt.Add(New clsEmployeeMovmentApproval(1197, clsEmployeeMovmentApproval.enMovementType.TERMINATION, iEmpId, enEmp_Dates_Transaction.DT_TERMINATION, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END

                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.TERMINATION) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.TERMINATION, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.TERMINATION) = iValue(clsEmployeeMovmentApproval.enMovementType.TERMINATION) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'Dim intPrivilegeId As Integer = 0
                            'Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                            'intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, enEmp_Dates_Transaction.DT_TERMINATION, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If

                    End If
                    'S.SANDEEP [20-JUN-2018] -- END

                End If

                '----------------------- Insert Retirement  Date -------------------------------
                If chkRetirementDt.Checked Then
                    If dtRow.Item("rdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("rdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 53, "Retirement Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    iEOCReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RETIREMENTS, dtRow.Item("rtreason").ToString.Trim)
                    If iEOCReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 54, "EOC Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtRow.Item("rdate").ToString.Trim.Length > 0 Then
                        If objEmp.IsEmpTerminated(CDate(dtRow.Item("rdate")).Date, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objEmp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If


                    Dim dtRetirement As DataTable = objEDates.Get_Current_Dates(dtpRetrEffDate.Value, enEmp_Dates_Transaction.DT_RETIREMENT, iEmpId).Tables(0)
                    If dtRetirement.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtRetirement.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpTerEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 55, "Sorry, Retirement transaction for the selected effective date is already present for the employee in the selected file")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objEDates._Effectivedate = dtpTerEffDate.Value.Date
                    'objEDates._Employeeunkid = iEmpId
                    'If dtRetirement.Rows.Count <= 0 AndAlso dtRow.Item("rdate").ToString.Trim.Length <= 0 Then
                    '    objEDates._Date1 = Nothing
                    'ElseIf dtRow.Item("rdate").ToString.Trim.Length > 0 Then
                    '    objEDates._Date1 = CDate(dtRow.Item("rdate")).Date
                    'ElseIf dtRetirement.Rows.Count > 0 Then
                    '    objEDates._Date1 = CDate(dtRetirement.Rows(0)("date2")).Date
                    'End If

                    'objEDates._Date2 = Nothing
                    'objEDates._Isconfirmed = False
                    'objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT
                    'objEDates._Changereasonunkid = iEOCReasonId
                    'objEDates._Isvoid = False
                    'objEDates._Statusunkid = 0
                    'objEDates._Userunkid = User._Object._Userunkid
                    'objEDates._Voiddatetime = Nothing
                    'objEDates._Voidreason = ""
                    'objEDates._Voiduserunkid = -1
                    'objEDates._Actionreasonunkid = 0
                    'objEDates._Isexclude_payroll = False

                    'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEDates._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If


                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement
                    Dim Date1 As Date
                    If dtRetirement.Rows.Count <= 0 AndAlso dtRow.Item("rdate").ToString.Trim.Length <= 0 Then
                        Date1 = Nothing
                    ElseIf dtRow.Item("rdate").ToString.Trim.Length > 0 Then
                        Date1 = CDate(dtRow.Item("rdate")).Date
                    ElseIf dtRetirement.Rows.Count > 0 Then
                        Date1 = CDate(dtRetirement.Rows(0)("date2")).Date
                    End If
                    Dim Date2 As Date = Nothing


                    If objEDates.isExist(dtpRetrEffDate.Value.Date, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_RETIREMENT), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 236, "Sorry, retirement information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If objEDates.isExist(Nothing, Date1, Date2, CInt(enEmp_Dates_Transaction.DT_RETIREMENT), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 238, "Sorry, This retirement information is already present.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim objEmployee As New clsEmployee_Master
                    If objEmployee.IsEmpTerminated(Date1, iEmpId, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEmployee._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                    objEmployee = Nothing

                    If objEDates.IsPendingSalaryIncrement(iEmpId, Date1) = True Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 239, "Sorry, You cannot retire this employee. Reason there are pending salary increment information present.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtpRetrEffDate.Value.Date > Date1 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 237, "cannot be less then effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End


                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                        objEDates._Effectivedate = dtpRetrEffDate.Value.Date
                        objEDates._Employeeunkid = iEmpId

                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        'If dtRetirement.Rows.Count <= 0 AndAlso dtRow.Item("rdate").ToString.Trim.Length <= 0 Then
                        '    objEDates._Date1 = Nothing
                        'ElseIf dtRow.Item("rdate").ToString.Trim.Length > 0 Then
                        '    objEDates._Date1 = CDate(dtRow.Item("rdate")).Date
                        'ElseIf dtRetirement.Rows.Count > 0 Then
                        '    objEDates._Date1 = CDate(dtRetirement.Rows(0)("date2")).Date
                        'End If
                        objEDates._Date1 = Date1
                        'Gajanan [14-SEP-2019] -- End
                    objEDates._Date2 = Nothing
                    objEDates._Isconfirmed = False
                    objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT
                    objEDates._Changereasonunkid = iEOCReasonId
                    objEDates._Isvoid = False
                    objEDates._Statusunkid = 0
                    objEDates._Userunkid = User._Object._Userunkid
                    objEDates._Voiddatetime = Nothing
                    objEDates._Voidreason = ""
                    objEDates._Voiduserunkid = -1
                    objEDates._Actionreasonunkid = 0
                    objEDates._Isexclude_payroll = False


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                        'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                        If objEDates.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            'Pinkal (18-Aug-2018) -- End
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEDates._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    Else

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1198
                        eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enEmp_Dates_Transaction.DT_RETIREMENT, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objADate._Audittype = enAuditType.ADD
                        objADate._Audituserunkid = User._Object._Userunkid
                        objADate._Effectivedate = dtpRetrEffDate.Value.Date
                        objADate._Employeeunkid = iEmpId
                        If dtRetirement.Rows.Count <= 0 AndAlso dtRow.Item("rdate").ToString.Trim.Length <= 0 Then
                            objADate._Date1 = Nothing
                        ElseIf dtRow.Item("rdate").ToString.Trim.Length > 0 Then
                            objADate._Date1 = CDate(dtRow.Item("rdate")).Date
                        ElseIf dtRetirement.Rows.Count > 0 Then
                            objADate._Date1 = CDate(dtRetirement.Rows(0)("date2")).Date
                        End If
                        objADate._Date2 = Nothing
                        objADate._Isconfirmed = False
                        objADate._Datetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT
                        objADate._Changereasonunkid = iEOCReasonId
                        objADate._Isvoid = False
                        objADate._Statusunkid = 0
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Actionreasonunkid = 0
                        objADate._Isexclude_Payroll = False
                        objADate._Isvoid = False
                        objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Tranguid = Guid.NewGuid.ToString()
                        objADate._Transactiondate = Now
                        objADate._Remark = ""
                        objADate._Rehiretranunkid = 0
                        objADate._Mappingunkid = 0
                        objADate._Isweb = False
                        objADate._Isfinal = False
                        objADate._Ip = getIP()
                        objADate._Hostname = getHostName()
                        objADate._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END

                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement


                        Dim blnFlag As Boolean = False

                        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, _
                                                   CInt(enEmp_Dates_Transaction.DT_RETIREMENT), objADate._Employeeunkid, _
                                                   clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 240, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_RETIREMENT), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 241, "Sorry, retirement Information is already present in approval process with selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If


                        blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, CInt(enEmp_Dates_Transaction.DT_RETIREMENT), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 242, "Sorry, retirement Information is already present in approval process with selected allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        'Gajanan [14-SEP-2019] -- End


                        If objADate.Insert(Company._Object._Companyunkid, Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)

                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1198, clsEmployeeMovmentApproval.enMovementType.RETIREMENTS, iEmpId, enEmp_Dates_Transaction.DT_RETIREMENT))
                            mvt.Add(New clsEmployeeMovmentApproval(1198, clsEmployeeMovmentApproval.enMovementType.RETIREMENTS, iEmpId, enEmp_Dates_Transaction.DT_RETIREMENT, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END

                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.RETIREMENTS) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.RETIREMENTS, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.RETIREMENTS) = iValue(clsEmployeeMovmentApproval.enMovementType.RETIREMENTS) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'Dim intPrivilegeId As Integer = 0
                            'Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                            'intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, enEmp_Dates_Transaction.DT_RETIREMENT, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    End If
                    'S.SANDEEP [20-JUN-2018] -- END

                End If
                '----------------------- END ------------------------------- 

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                '----------------------- Insert Probation Date -------------------------------
                If chkProbationFromDt.Checked Then
                    If dtRow.Item("prbfrmdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("prbfrmdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 97, "Probation From Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("prbtodate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("prbtodate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 98, "Probation To Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If CDate(dtRow.Item("prbtodate")).Date < CDate(dtRow.Item("prbfrmdate")).Date Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 99, "Probation To Date cannot be less than Probation From Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim iReasonId As Integer = 0
                    iReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.PROBATION, dtRow.Item("prbreason").ToString.Trim)
                    If iReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 100, "Probation Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dtProbation As DataTable = objEDates.Get_Current_Dates(dtpPbEffDate.Value, enEmp_Dates_Transaction.DT_PROBATION, iEmpId).Tables(0)
                    If dtProbation.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtProbation.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpPbEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 101, "Sorry, Probation transaction for the selected effective date is already present for the employee in the selected file")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objEDates._Effectivedate = dtpPbEffDate.Value.Date
                    'objEDates._Employeeunkid = iEmpId
                    'objEDates._Date1 = CDate(dtRow.Item("prbfrmdate").ToString)
                    'objEDates._Date2 = CDate(dtRow.Item("prbtodate").ToString)
                    'objEDates._Isconfirmed = False
                    'objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_PROBATION
                    'objEDates._Changereasonunkid = iReasonId
                    'objEDates._Isvoid = False
                    'objEDates._Statusunkid = 0
                    'objEDates._Userunkid = User._Object._Userunkid
                    'objEDates._Voiddatetime = Nothing
                    'objEDates._Voidreason = ""
                    'objEDates._Voiduserunkid = -1
                    'objEDates._Actionreasonunkid = 0
                    'objEDates._Isexclude_payroll = False

                    'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEDates._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If

                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement


                    Dim Date1 As Date = CDate(dtRow.Item("prbfrmdate").ToString)
                    Dim Date2 As Date = CDate(dtRow.Item("prbfrmdate").ToString)


                    If objEDates.isExist(dtpPbEffDate.Value.Date, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_PROBATION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 243, "Sorry, porbation information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If objEDates.isExist(Nothing, Date1, Date2, CInt(enEmp_Dates_Transaction.DT_PROBATION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 244, "Sorry, This porbation information is already present.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtpPbEffDate.Value.Date > Date1 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 237, "cannot be less then effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End

                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                    objEDates._Effectivedate = dtpPbEffDate.Value.Date
                    objEDates._Employeeunkid = iEmpId
                    objEDates._Date1 = CDate(dtRow.Item("prbfrmdate").ToString)
                    objEDates._Date2 = CDate(dtRow.Item("prbtodate").ToString)
                    objEDates._Isconfirmed = False
                    objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_PROBATION
                    objEDates._Changereasonunkid = iReasonId
                    objEDates._Isvoid = False
                    objEDates._Statusunkid = 0
                    objEDates._Userunkid = User._Object._Userunkid
                    objEDates._Voiddatetime = Nothing
                    objEDates._Voidreason = ""
                    objEDates._Voiduserunkid = -1
                    objEDates._Actionreasonunkid = 0
                    objEDates._Isexclude_payroll = False


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                        'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                        If objEDates.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            'Pinkal (18-Aug-2018) -- End
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    Else

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1194
                        eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enEmp_Dates_Transaction.DT_PROBATION, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objADate._Audittype = enAuditType.ADD
                        objADate._Audituserunkid = User._Object._Userunkid
                        objADate._Effectivedate = dtpPbEffDate.Value.Date
                        objADate._Employeeunkid = iEmpId
                        objADate._Date1 = CDate(dtRow.Item("prbfrmdate").ToString)
                        objADate._Date2 = CDate(dtRow.Item("prbtodate").ToString)
                        objADate._Isconfirmed = False
                        objADate._Datetypeunkid = enEmp_Dates_Transaction.DT_PROBATION
                        objADate._Changereasonunkid = iReasonId
                        objADate._Isvoid = False
                        objADate._Statusunkid = 0
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Actionreasonunkid = 0
                        objADate._Isexclude_Payroll = False
                        objADate._Isvoid = False
                        objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Tranguid = Guid.NewGuid.ToString()
                        objADate._Transactiondate = Now
                        objADate._Remark = ""
                        objADate._Rehiretranunkid = 0
                        objADate._Mappingunkid = 0
                        objADate._Isweb = False
                        objADate._Isfinal = False
                        objADate._Ip = getIP()
                        objADate._Hostname = getHostName()
                        objADate._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END


                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        Dim blnFlag As Boolean = False
                        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, _
                                                   CInt(enEmp_Dates_Transaction.DT_PROBATION), objADate._Employeeunkid, _
                                                   clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)

                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 245, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_PROBATION), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 246, "Sorry, porbation Information is already present in approval process with selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        'Gajanan [14-SEP-2019] -- End

                        If objADate.Insert(Company._Object._Companyunkid, Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1194, clsEmployeeMovmentApproval.enMovementType.PROBATION, iEmpId, enEmp_Dates_Transaction.DT_PROBATION))
                            mvt.Add(New clsEmployeeMovmentApproval(1194, clsEmployeeMovmentApproval.enMovementType.PROBATION, iEmpId, enEmp_Dates_Transaction.DT_PROBATION, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END
                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.PROBATION) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.PROBATION, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.PROBATION) = iValue(clsEmployeeMovmentApproval.enMovementType.PROBATION) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'Dim intPrivilegeId As Integer = 0
                            'Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                            'intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, enEmp_Dates_Transaction.DT_PROBATION, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    End If
                    'S.SANDEEP [20-JUN-2018] -- END

                End If
                '----------------------- END -------------------------------

                '----------------------- Insert Suspension Date -------------------------------
                If chkSuspensionFrmDt.Checked Then
                    If dtRow.Item("suspfrmdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("suspfrmdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 102, "Suspension From Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("susptodate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("susptodate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 103, "Suspension To Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If CDate(dtRow.Item("susptodate")).Date < CDate(dtRow.Item("suspfrmdate")).Date Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 104, "Suspension To Date cannot be less than Suspension From Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim iReasonId As Integer = 0
                    iReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.SUSPENSION, dtRow.Item("suspreason").ToString.Trim)
                    If iReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 105, "Suspension Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dtSuspension As DataTable = objEDates.Get_Current_Dates(dtpSuspEffDate.Value, enEmp_Dates_Transaction.DT_SUSPENSION, iEmpId).Tables(0)

                    If dtSuspension.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtSuspension.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpSuspEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 106, "Sorry, Suspension transaction for the selected effective date is already present for the employee in the selected file")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objEDates._Effectivedate = dtpSuspEffDate.Value.Date
                    'objEDates._Employeeunkid = iEmpId
                    'objEDates._Date1 = CDate(dtRow.Item("suspfrmdate").ToString)
                    'objEDates._Date2 = CDate(dtRow.Item("susptodate").ToString)
                    'objEDates._Isconfirmed = False
                    'objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_SUSPENSION
                    'objEDates._Changereasonunkid = iReasonId
                    'objEDates._Isvoid = False
                    'objEDates._Statusunkid = 0
                    'objEDates._Userunkid = User._Object._Userunkid
                    'objEDates._Voiddatetime = Nothing
                    'objEDates._Voidreason = ""
                    'objEDates._Voiduserunkid = -1
                    'objEDates._Actionreasonunkid = 0
                    'objEDates._Isexclude_payroll = False

                    'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEDates._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If


                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement

                    Dim Date1 As Date = CDate(dtRow.Item("suspfrmdate").ToString)
                    Dim Date2 As Date = CDate(dtRow.Item("susptodate").ToString)

                    If objEDates.isExist(dtpSuspEffDate.Value.Date, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_SUSPENSION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 247, "Sorry, suspension information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If objEDates.isExist(Nothing, Date1, Date2, CInt(enEmp_Dates_Transaction.DT_SUSPENSION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 274, "Sorry, This suspension information is already present.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtpSuspEffDate.Value.Date > Date1 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 237, "cannot be less then effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End

                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    objEDates._Effectivedate = dtpSuspEffDate.Value.Date
                    objEDates._Employeeunkid = iEmpId
                    objEDates._Date1 = CDate(dtRow.Item("suspfrmdate").ToString)
                    objEDates._Date2 = CDate(dtRow.Item("susptodate").ToString)
                    objEDates._Isconfirmed = False
                    objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_SUSPENSION
                    objEDates._Changereasonunkid = iReasonId
                    objEDates._Isvoid = False
                    objEDates._Statusunkid = 0
                    objEDates._Userunkid = User._Object._Userunkid
                    objEDates._Voiddatetime = Nothing
                    objEDates._Voidreason = ""
                    objEDates._Voiduserunkid = -1
                    objEDates._Actionreasonunkid = 0
                    objEDates._Isexclude_payroll = False


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                        'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                        If objEDates.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            'Pinkal (18-Aug-2018) -- End
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    Else

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1196
                        eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enEmp_Dates_Transaction.DT_SUSPENSION, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objADate._Audittype = enAuditType.ADD
                        objADate._Audituserunkid = User._Object._Userunkid
                        objADate._Effectivedate = dtpSuspEffDate.Value.Date
                        objADate._Employeeunkid = iEmpId
                        objADate._Date1 = CDate(dtRow.Item("suspfrmdate").ToString)
                        objADate._Date2 = CDate(dtRow.Item("susptodate").ToString)
                        objADate._Isconfirmed = False
                        objADate._Datetypeunkid = enEmp_Dates_Transaction.DT_SUSPENSION
                        objADate._Changereasonunkid = iReasonId
                        objADate._Isvoid = False
                        objADate._Statusunkid = 0
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Actionreasonunkid = 0
                        objADate._Isexclude_Payroll = False
                        objADate._Isvoid = False
                        objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Tranguid = Guid.NewGuid.ToString()
                        objADate._Transactiondate = Now
                        objADate._Remark = ""
                        objADate._Rehiretranunkid = 0
                        objADate._Mappingunkid = 0
                        objADate._Isweb = False
                        objADate._Isfinal = False
                        objADate._Ip = getIP()
                        objADate._Hostname = getHostName()
                        objADate._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END


                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        Dim blnFlag As Boolean = False
                        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, _
                                                   CInt(enEmp_Dates_Transaction.DT_SUSPENSION), objADate._Employeeunkid, _
                                                   clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)

                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 248, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_SUSPENSION), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 249, "Sorry, suspension Information is already present in approval process with selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        'Gajanan [14-SEP-2019] -- End



                        If objADate.Insert(Company._Object._Companyunkid, Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1196, clsEmployeeMovmentApproval.enMovementType.SUSPENSION, iEmpId, enEmp_Dates_Transaction.DT_SUSPENSION))
                            mvt.Add(New clsEmployeeMovmentApproval(1196, clsEmployeeMovmentApproval.enMovementType.SUSPENSION, iEmpId, enEmp_Dates_Transaction.DT_SUSPENSION, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END
                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.SUSPENSION) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.PROBATION, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.SUSPENSION) = iValue(clsEmployeeMovmentApproval.enMovementType.SUSPENSION) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'Dim intPrivilegeId As Integer = 0
                            'Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                            'intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, enEmp_Dates_Transaction.DT_SUSPENSION, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                        'S.SANDEEP [20-JUN-2018] -- END
                    End If
                End If
                '----------------------- END -------------------------------

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                '----------------------- Insert Exemption Date -------------------------------
                If chkExemptionFrmDt.Checked Then
                    If dtRow.Item("Exemptionfrmdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("Exemptionfrmdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 102, "Exemption From Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If dtRow.Item("Exemptiontodate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("Exemptiontodate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 103, "Exemption To Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If CDate(dtRow.Item("Exemptiontodate")).Date < CDate(dtRow.Item("Exemptionfrmdate")).Date Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 104, "Exemption To Date cannot be less than Exemption From Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim iReasonId As Integer = 0
                    iReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.EXEMPTION, dtRow.Item("Exemptionreason").ToString.Trim)
                    If iReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 105, "Exemption Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dtExemption As DataTable = objEDates.Get_Current_Dates(dtpExemptionEffDate.Value, enEmp_Dates_Transaction.DT_EXEMPTION, iEmpId).Tables(0)

                    If dtExemption.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtExemption.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpExemptionEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 106, "Sorry, Exemption transaction for the selected effective date is already present for the employee in the selected file")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim Date1 As Date = CDate(dtRow.Item("Exemptionfrmdate").ToString)
                    Dim Date2 As Date = CDate(dtRow.Item("Exemptiontodate").ToString)

                    If objEDates.isExist(dtpExemptionEffDate.Value.Date, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_EXEMPTION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 247, "Sorry, Exemption information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If objEDates.isExist(Nothing, Date1, Date2, CInt(enEmp_Dates_Transaction.DT_EXEMPTION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 274, "Sorry, This Exemption information is already present.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtpExemptionEffDate.Value.Date > Date1 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 237, "cannot be less then effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                        objEDates._Effectivedate = dtpExemptionEffDate.Value.Date
                        objEDates._Employeeunkid = iEmpId
                        objEDates._Date1 = CDate(dtRow.Item("Exemptionfrmdate").ToString)
                        objEDates._Date2 = CDate(dtRow.Item("Exemptiontodate").ToString)
                        objEDates._Isconfirmed = False
                        objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_EXEMPTION
                        objEDates._Changereasonunkid = iReasonId
                        objEDates._Isvoid = False
                        objEDates._Statusunkid = 0
                        objEDates._Userunkid = User._Object._Userunkid
                        objEDates._Voiddatetime = Nothing
                        objEDates._Voidreason = ""
                        objEDates._Voiduserunkid = -1
                        objEDates._Actionreasonunkid = 0
                        objEDates._Isexclude_payroll = False


                        If objEDates.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    Else

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1196
                        eMovement = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enEmp_Dates_Transaction.DT_EXEMPTION, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objADate._Audittype = enAuditType.ADD
                        objADate._Audituserunkid = User._Object._Userunkid
                        objADate._Effectivedate = dtpExemptionEffDate.Value.Date
                        objADate._Employeeunkid = iEmpId
                        objADate._Date1 = CDate(dtRow.Item("Exemptionfrmdate").ToString)
                        objADate._Date2 = CDate(dtRow.Item("Exemptiontodate").ToString)
                        objADate._Isconfirmed = False
                        objADate._Datetypeunkid = enEmp_Dates_Transaction.DT_EXEMPTION
                        objADate._Changereasonunkid = iReasonId
                        objADate._Isvoid = False
                        objADate._Statusunkid = 0
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Actionreasonunkid = 0
                        objADate._Isexclude_Payroll = False
                        objADate._Isvoid = False
                        objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Tranguid = Guid.NewGuid.ToString()
                        objADate._Transactiondate = Now
                        objADate._Remark = ""
                        objADate._Rehiretranunkid = 0
                        objADate._Mappingunkid = 0
                        objADate._Isweb = False
                        objADate._Isfinal = False
                        objADate._Ip = getIP()
                        objADate._Hostname = getHostName()
                        objADate._Form_Name = mstrModuleName
                        objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED


                        Dim blnFlag As Boolean = False
                        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, _
                                                   CInt(enEmp_Dates_Transaction.DT_EXEMPTION), objADate._Employeeunkid, _
                                                   clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)

                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 248, "Sorry, Exemption Information is already present in approval process with selected effective date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_EXEMPTION), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 249, "Sorry, Exemption Information is already present in approval process with selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If



                        If objADate.Insert(Company._Object._Companyunkid, Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            mvt.Add(New clsEmployeeMovmentApproval(1196, clsEmployeeMovmentApproval.enMovementType.EXEMPTION, iEmpId, enEmp_Dates_Transaction.DT_EXEMPTION, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.EXEMPTION) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.PROBATION, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.EXEMPTION) = iValue(clsEmployeeMovmentApproval.enMovementType.EXEMPTION) & "," & iEmpId.ToString()
                            End If

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    End If
                End If
                '----------------------- END -------------------------------
                'Sohail (21 Oct 2019) -- End

                '----------------------- Insert Confirmation Date -------------------------------
                If chkConfirmationDt.Checked Then
                    If dtRow.Item("cnfdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("cnfdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 107, "Confirmation Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    Dim iReasonId As Integer = 0
                    'Hemant (11 May 2020) -- Start
                    'ISSUE#0004681: TRA: able to import the confirmation date due to Error "Reason Not found" while the same reason in System we put in import excel
                    'iReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.CONFIRMATION, dtRow.Item("prbreason").ToString.Trim)
                    iReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.CONFIRMATION, dtRow.Item("cnfreason").ToString.Trim)
                    'Hemant ( May 2020) -- End
                    If iReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 108, "Confirmation Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dtConfirm As DataTable = objEDates.Get_Current_Dates(dtpConEffDate.Value, enEmp_Dates_Transaction.DT_CONFIRMATION, iEmpId).Tables(0)
                    If dtConfirm.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtConfirm.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpConEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 109, "Sorry, Confirmation transaction for the selected effective date is already present for the employee in the selected file")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objEDates._Effectivedate = dtpConEffDate.Value.Date
                    'objEDates._Employeeunkid = iEmpId
                    'objEDates._Date1 = CDate(dtRow.Item("cnfdate").ToString)
                    'objEDates._Date2 = Nothing
                    'objEDates._Isconfirmed = False
                    'objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_CONFIRMATION
                    'objEDates._Changereasonunkid = iReasonId
                    'objEDates._Isvoid = False
                    'objEDates._Statusunkid = 0
                    'objEDates._Userunkid = User._Object._Userunkid
                    'objEDates._Voiddatetime = Nothing
                    'objEDates._Voidreason = ""
                    'objEDates._Voiduserunkid = -1
                    'objEDates._Actionreasonunkid = 0
                    'objEDates._Isexclude_payroll = False

                    'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEDates._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If

                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement
                    Dim Date1 As Date = CDate(dtRow.Item("cnfdate").ToString)
                    Dim Date2 As Date = Nothing

                    If objEDates.isExist(dtpConEffDate.Value.Date, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_CONFIRMATION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 250, "Sorry, confirmation information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If objEDates.isExist(Nothing, Date1, Date2, CInt(enEmp_Dates_Transaction.DT_CONFIRMATION), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 251, "Sorry, This retirement information is already present.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If dtpConEffDate.Value.Date > Date1 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 237, "cannot be less then effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End


                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    objEDates._Effectivedate = dtpConEffDate.Value.Date
                    objEDates._Employeeunkid = iEmpId
                    objEDates._Date1 = CDate(dtRow.Item("cnfdate").ToString)
                    objEDates._Date2 = Nothing
                    objEDates._Isconfirmed = False
                    objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_CONFIRMATION
                    objEDates._Changereasonunkid = iReasonId
                    objEDates._Isvoid = False
                    objEDates._Statusunkid = 0
                    objEDates._Userunkid = User._Object._Userunkid
                    objEDates._Voiddatetime = Nothing
                    objEDates._Voidreason = ""
                    objEDates._Voiduserunkid = -1
                    objEDates._Actionreasonunkid = 0
                    objEDates._Isexclude_payroll = False


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                        'If objEDates.Insert(Company._Object._Companyunkid, Nothing) Then
                        If objEDates.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing) Then
                            'Pinkal (18-Aug-2018) -- End
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    Else

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1195
                        eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enEmp_Dates_Transaction.DT_CONFIRMATION, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objADate._Audittype = enAuditType.ADD
                        objADate._Audituserunkid = User._Object._Userunkid
                        objADate._Effectivedate = dtpConEffDate.Value.Date
                        objADate._Employeeunkid = iEmpId
                        objADate._Date1 = CDate(dtRow.Item("cnfdate").ToString)
                        objADate._Date2 = Nothing
                        objADate._Isconfirmed = False
                        objADate._Datetypeunkid = enEmp_Dates_Transaction.DT_CONFIRMATION
                        objADate._Changereasonunkid = iReasonId
                        objADate._Isvoid = False
                        objADate._Statusunkid = 0
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Actionreasonunkid = 0
                        objADate._Isexclude_Payroll = False
                        objADate._Isvoid = False
                        objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADate._Voiddatetime = Nothing
                        objADate._Voidreason = ""
                        objADate._Voiduserunkid = -1
                        objADate._Tranguid = Guid.NewGuid.ToString()
                        objADate._Transactiondate = Now
                        objADate._Remark = ""
                        objADate._Rehiretranunkid = 0
                        objADate._Mappingunkid = 0
                        objADate._Isweb = False
                        objADate._Isfinal = False
                        objADate._Ip = getIP()
                        objADate._Hostname = getHostName()
                        objADate._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END


                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        Dim blnFlag As Boolean = False
                        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, _
                                                   CInt(enEmp_Dates_Transaction.DT_CONFIRMATION), objADate._Employeeunkid, _
                                                   clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 252, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, CInt(enEmp_Dates_Transaction.DT_CONFIRMATION), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 253, "Sorry, confirmation Information is already present in approval process with selected effective date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If


                        blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, CInt(enEmp_Dates_Transaction.DT_CONFIRMATION), _
                                                   objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 254, "Sorry, confirmation Information is already present in approval process with selected allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        'Gajanan [14-SEP-2019] -- End



                        If objADate.Insert(Company._Object._Companyunkid, Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1195, clsEmployeeMovmentApproval.enMovementType.CONFIRMATION, iEmpId, enEmp_Dates_Transaction.DT_CONFIRMATION))
                            mvt.Add(New clsEmployeeMovmentApproval(1195, clsEmployeeMovmentApproval.enMovementType.CONFIRMATION, iEmpId, enEmp_Dates_Transaction.DT_CONFIRMATION, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END
                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.CONFIRMATION) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.CONFIRMATION, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.CONFIRMATION) = iValue(clsEmployeeMovmentApproval.enMovementType.CONFIRMATION) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'Dim intPrivilegeId As Integer = 0
                            'Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                            'intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, enEmp_Dates_Transaction.DT_CONFIRMATION, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objADate._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                End If
                    'S.SANDEEP [20-JUN-2018] -- END

                End If
                '----------------------- END -------------------------------

                '----------------------- Insert Work Permit Data -------------------------------
                If ChkWorkPermitNo.Checked = True _
                    OrElse chkWpCountry.Checked = True _
                    OrElse chkWpIssueDt.Checked = True _
                    OrElse chkWpExpiryDt.Checked = True _
                    OrElse chkWpIssuePlace.Checked = True Then

                    If dtRow.Item("wpermitno").ToString.Trim.Length <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 110, "Work Permit Number is not valid")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim iCountry As Integer : Dim objMaster As New clsMasterData
                    If chkWpCountry.Checked = True Then
                        If dtRow.Item("wpermitcountry").ToString.Trim.Length > 0 Then
                            iCountry = objMaster.GetCountryUnkId(dtRow.Item("wpermitcountry").ToString.Trim)
                            If iCountry <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 111, "Country not found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For                                
                            End If
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 111, "Country not found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkWpIssueDt.Checked = True Then
                        If dtRow.Item("wpermitissuedate").ToString.Trim.Length > 0 Then
                            If IsDate(dtRow.Item("wpermitissuedate")) = False Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 112, "Work Permit Issue Date is not vaild")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 112, "Work Permit Issue Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkWpExpiryDt.Checked = True Then
                        If dtRow.Item("wpermitexpirydate").ToString.Trim.Length > 0 Then
                            If IsDate(dtRow.Item("wpermitexpirydate")) = False Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 113, "Work Permit Expiry Date is not vaild")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 113, "Work Permit Expiry Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkWpIssueDt.Checked = True AndAlso chkWpExpiryDt.Checked = True Then
                        If CDate(dtRow.Item("wpermitexpirydate")).Date < CDate(dtRow.Item("wpermitissuedate")).Date Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 114, "Work permit expiry date cannot be less than Work permit issue Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkWpIssuePlace.Checked = True Then
                        If dtRow.Item("wpermitissueplace").ToString.Trim.Length <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 115, "Issue Place not found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    Dim iReasonId As Integer = 0
                    iReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.WORK_PERMIT, dtRow.Item("wpermitreason").ToString.Trim)
                    If iReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 116, "Work Permit Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dtWorkPermit As DataTable = objPermit.Get_Current_WorkPermit(dtpWpEffDate.Value, False, iEmpId).Tables(0)
                    If dtWorkPermit.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtWorkPermit.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpWpEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 117, "Sorry, Work Permit transaction for the selected effective date is already present for the employee in the selected file")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objPermit._Effectivedate = dtpWpEffDate.Value.Date
                    'objPermit._Employeeunkid = iEmpId
                    'objPermit._Changereasonunkid = iReasonId
                    'If dtRow("wpermitexpirydate").ToString.Trim.Length <= 0 Then
                    '    objPermit._Expiry_Date = Nothing
                    'Else
                    '    objPermit._Expiry_Date = CDate(dtRow("wpermitexpirydate"))
                    'End If
                    'objPermit._Isfromemployee = False
                    'objPermit._IsResidentPermit = False
                    'If dtRow("wpermitissuedate").ToString.Trim.Length <= 0 Then
                    '    objPermit._Issue_Date = Nothing
                    'Else
                    '    objPermit._Issue_Date = CDate(dtRow("wpermitissuedate"))
                    'End If
                    'objPermit._Issue_Place = dtRow("wpermitissueplace").ToString()
                    'objPermit._Isvoid = False
                    'objPermit._Rehiretranunkid = 0
                    'objPermit._Work_Permit_No = dtRow("wpermitno").ToString()
                    'objPermit._Workcountryunkid = iCountry
                    'objPermit._Userunkid = User._Object._Userunkid
                    'objPermit._Voiddatetime = Nothing
                    'objPermit._Voidreason = ""
                    'objPermit._Voiduserunkid = -1
                    'objPermit._Statusunkid = 0

                    'If objPermit.Insert(Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEDates._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If

                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement
                    Dim wpermitexpirydate As Date = Nothing
                    Dim wpermitissuedate As Date = Nothing

                    If dtRow("wpermitexpirydate").ToString.Trim.Length <= 0 Then
                        wpermitexpirydate = Nothing
                    Else
                        wpermitexpirydate = CDate(dtRow("wpermitexpirydate"))
                    End If
                    If dtRow("wpermitissuedate").ToString.Trim.Length <= 0 Then
                        wpermitissuedate = Nothing
                    Else
                        wpermitissuedate = CDate(dtRow("wpermitissuedate"))
                    End If

                    If IsNothing(wpermitexpirydate) = False AndAlso IsNothing(wpermitissuedate) = False Then
                        If wpermitexpirydate <= wpermitissuedate Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 255, "Sorry, expiry date cannot be less or equal to issue date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If objPermit.isExist(False, dtpWpEffDate.Value.Date, , iEmpId, -1, Nothing) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 256, "Sorry, work permit information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                    Dim dsList As New DataSet
                    dsList = objPermit.Get_Current_WorkPermit(dtpWpEffDate.Value.Date, False, iEmpId)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        If dsList.Tables(0).Rows(0)("work_permit_no").ToString().Trim = dtRow("wpermitno").ToString() AndAlso _
                            CInt(dsList.Tables(0).Rows(0)("workcountryunkid")) = iCountry Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 257, "Sorry, work permit no is already present for the selected employee.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                    dsList = Nothing


                    If objPermit.isExist(False, Nothing, dtRow("wpermitno").ToString(), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 258, "Sorry, work permit no is already present for the selected employee.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End



                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    objPermit._Effectivedate = dtpWpEffDate.Value.Date
                    objPermit._Employeeunkid = iEmpId
                    objPermit._Changereasonunkid = iReasonId
                    If dtRow("wpermitexpirydate").ToString.Trim.Length <= 0 Then
                        objPermit._Expiry_Date = Nothing
                    Else
                        objPermit._Expiry_Date = CDate(dtRow("wpermitexpirydate"))
                    End If
                    objPermit._Isfromemployee = False
                    objPermit._IsResidentPermit = False
                    If dtRow("wpermitissuedate").ToString.Trim.Length <= 0 Then
                        objPermit._Issue_Date = Nothing
                    Else
                        objPermit._Issue_Date = CDate(dtRow("wpermitissuedate"))
                    End If
                    objPermit._Issue_Place = dtRow("wpermitissueplace").ToString()
                    objPermit._Isvoid = False
                    objPermit._Rehiretranunkid = 0
                    objPermit._Work_Permit_No = dtRow("wpermitno").ToString()
                    objPermit._Workcountryunkid = iCountry
                    objPermit._Userunkid = User._Object._Userunkid
                    objPermit._Voiddatetime = Nothing
                    objPermit._Voidreason = ""
                    objPermit._Voiduserunkid = -1
                    objPermit._Statusunkid = 0

                    If objPermit.Insert(Nothing) Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEDates._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    Else
                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1199
                        eMovement = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objAPermit._Audittype = enAuditType.ADD
                        objAPermit._Audituserunkid = User._Object._Userunkid
                        objAPermit._Effectivedate = dtpWpEffDate.Value.Date
                        objAPermit._Employeeunkid = iEmpId
                        objAPermit._Changereasonunkid = iReasonId
                        If dtRow("wpermitexpirydate").ToString.Trim.Length <= 0 Then
                            objAPermit._Expiry_Date = Nothing
                        Else
                            objAPermit._Expiry_Date = CDate(dtRow("wpermitexpirydate"))
                        End If
                        objAPermit._Isresidentpermit = False
                        If dtRow("wpermitissuedate").ToString.Trim.Length <= 0 Then
                            objAPermit._Issue_Date = Nothing
                        Else
                            objAPermit._Issue_Date = CDate(dtRow("wpermitissuedate"))
                        End If
                        objAPermit._Issue_Place = dtRow("wpermitissueplace").ToString()
                        objAPermit._Isvoid = False
                        objAPermit._Voidreason = ""
                        objAPermit._Voiduserunkid = -1
                        objAPermit._Work_Permit_No = dtRow("wpermitno").ToString()
                        objAPermit._Workcountryunkid = iCountry
                        objAPermit._Isresidentpermit = False
                        objAPermit._Isvoid = False
                        objAPermit._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objAPermit._Voidreason = ""
                        objAPermit._Voiduserunkid = -1
                        objAPermit._Tranguid = Guid.NewGuid.ToString()
                        objAPermit._Transactiondate = Now
                        objAPermit._Remark = ""
                        objAPermit._Rehiretranunkid = 0
                        objAPermit._Mappingunkid = 0
                        objAPermit._Isweb = False
                        objAPermit._Isfinal = False
                        objAPermit._Ip = getIP()
                        objAPermit._Hostname = getHostName()
                        objAPermit._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objAPermit._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END


                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        Dim blnFlag As Boolean = False
                        blnFlag = objAPermit.isExist(False, clsEmployeeMovmentApproval.enOperationType.ADDED, objAPermit._Effectivedate, _
                                                     objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 259, "Sorry, Work Permit Information is already present in approval process with selected date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objAPermit.isExist(False, clsEmployeeMovmentApproval.enOperationType.ADDED, objAPermit._Effectivedate, _
                                                     "", objAPermit._Employeeunkid, "", Nothing, True)

                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 260, "Sorry, Work Permit Information is already present in approval process with selected date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objAPermit.isExist(False, clsEmployeeMovmentApproval.enOperationType.ADDED, Nothing, _
                                                     objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 261, "Sorry, Work Permit Information is already present in approval process with selected allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        'Gajanan [14-SEP-2019] -- End



                        'Pinkal (20-Dec-2024) -- Start
                        'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
                        'If objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED) Then
                        If objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED, Company._Object._Companyunkid) Then
                            'Pinkal (20-Dec-2024) -- End
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1199, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT, iEmpId, Nothing))
                            mvt.Add(New clsEmployeeMovmentApproval(1199, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT, iEmpId, Nothing, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END
                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.WORKPERMIT) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.WORKPERMIT, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.WORKPERMIT) = iValue(clsEmployeeMovmentApproval.enMovementType.WORKPERMIT) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'Dim intPrivilegeId As Integer = 0
                            'Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                            'intPrivilegeId = 1199
                            'eMovement = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, Nothing, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objAPermit._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    End If
                    'S.SANDEEP [20-JUN-2018] -- END
                End If
                '----------------------- END -------------------------------

                '----------------------- Resident Permit Data -------------------------------
                If ChkResidentPermitNo.Checked = True _
                    OrElse chkRpCountry.Checked = True _
                    OrElse chkRpIssueDt.Checked = True _
                    OrElse chkRpExpiryDt.Checked = True _
                    OrElse chkRpIssuePlace.Checked = True Then

                    If dtRow.Item("rpermitno").ToString.Trim.Length <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 118, "Resident Permit Number is not valid")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim iCountry As Integer : Dim objMaster As New clsMasterData
                    If chkRpCountry.Checked = True Then
                        If dtRow.Item("rpermitcountry").ToString.Trim.Length > 0 Then
                            iCountry = objMaster.GetCountryUnkId(dtRow.Item("rpermitcountry").ToString.Trim)
                            If iCountry <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 111, "Country not found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 111, "Country not found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkRpIssueDt.Checked = True Then
                        If dtRow.Item("rpermitissuedate").ToString.Trim.Length > 0 Then
                            If IsDate(dtRow.Item("rpermitissuedate")) = False Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 119, "Resident Permit Issue Date is not vaild")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 119, "Resident Permit Issue Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkRpExpiryDt.Checked = True Then
                        If dtRow.Item("rpermitexpirydate").ToString.Trim.Length > 0 Then
                            If IsDate(dtRow.Item("rpermitexpirydate")) = False Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 120, "Resident Permit Expiry Date is not vaild")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 120, "Resident Permit Expiry Date is not vaild")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkRpIssueDt.Checked = True AndAlso chkRpExpiryDt.Checked = True Then
                        If CDate(dtRow.Item("rpermitexpirydate")).Date < CDate(dtRow.Item("rpermitissuedate")).Date Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 121, "Resident permit expiry date cannot be less than Resident permit issue Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If chkRpIssuePlace.Checked = True Then
                        If dtRow.Item("rpermitissueplace").ToString.Trim.Length <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 115, "Issue Place not found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    Dim iReasonId As Integer = 0
                    iReasonId = objcommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RESIDENT_PERMIT, dtRow.Item("rpermitreason").ToString.Trim)
                    If iReasonId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 122, "Resident Permit Reason Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    Dim dtResidentPermit As DataTable = objPermit.Get_Current_WorkPermit(dtpRpEffDate.Value, True, iEmpId).Tables(0)
                    If dtResidentPermit.Rows.Count > 0 AndAlso eZeeDate.convertDate(CDate(dtResidentPermit.Rows(0)("effectivedate"))) = eZeeDate.convertDate(dtpRpEffDate.Value) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 123, "Sorry, Resident Permit transaction for the selected effective date is already present for the employee in the selected file")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'objPermit._Effectivedate = dtpRpEffDate.Value.Date
                    'objPermit._Employeeunkid = iEmpId
                    'objPermit._Changereasonunkid = iReasonId
                    'If dtRow("rpermitexpirydate").ToString.Trim.Length <= 0 Then
                    '    objPermit._Expiry_Date = Nothing
                    'Else
                    '    objPermit._Expiry_Date = CDate(dtRow("rpermitexpirydate"))
                    'End If
                    'objPermit._Isfromemployee = False
                    'objPermit._IsResidentPermit = True
                    'If dtRow("rpermitissuedate").ToString.Trim.Length <= 0 Then
                    '    objPermit._Issue_Date = Nothing
                    'Else
                    '    objPermit._Issue_Date = CDate(dtRow("rpermitissuedate"))
                    'End If
                    'objPermit._Issue_Place = dtRow("rpermitissueplace").ToString()
                    'objPermit._Isvoid = False
                    'objPermit._Rehiretranunkid = 0
                    'objPermit._Work_Permit_No = dtRow("rpermitno").ToString()
                    'objPermit._Workcountryunkid = iCountry
                    'objPermit._Userunkid = User._Object._Userunkid
                    'objPermit._Voiddatetime = Nothing
                    'objPermit._Voidreason = ""
                    'objPermit._Voiduserunkid = -1
                    'objPermit._Statusunkid = 0

                    'If objPermit.Insert(Nothing) Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEDates._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If


                    'Gajanan [14-SEP-2019] -- Start    
                    'Issue: Missing Isexist In Import Movement
                    Dim rpermitexpirydate As Date = Nothing
                    Dim rpermitissuedate As Date = Nothing


                    If dtRow("rpermitexpirydate").ToString.Trim.Length <= 0 Then
                        rpermitexpirydate = Nothing
                    Else
                        rpermitexpirydate = CDate(dtRow("rpermitexpirydate"))
                    End If
                    If dtRow("rpermitissuedate").ToString.Trim.Length <= 0 Then
                        rpermitissuedate = Nothing
                    Else
                        rpermitissuedate = CDate(dtRow("rpermitissuedate"))
                    End If

                    If IsNothing(rpermitexpirydate) = False AndAlso IsNothing(rpermitissuedate) = False Then
                        If rpermitexpirydate <= rpermitissuedate Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 262, "Sorry, expiry date cannot be less or equal to issue date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If

                    If objPermit.isExist(False, dtpWpEffDate.Value.Date, , iEmpId, -1, Nothing) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 263, "Sorry, work permit information is already present for the selected effective date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                    Dim dsList As New DataSet
                    dsList = objPermit.Get_Current_WorkPermit(dtpWpEffDate.Value.Date, False, iEmpId)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        If dsList.Tables(0).Rows(0)("work_permit_no").ToString().Trim = dtRow("wpermitno").ToString() AndAlso _
                            CInt(dsList.Tables(0).Rows(0)("workcountryunkid")) = iCountry Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 264, "Sorry, Resident permit no is already present for the selected employee.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                    dsList = Nothing


                    If objPermit.isExist(False, Nothing, dtRow("wpermitno").ToString(), iEmpId, -1) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 265, "Sorry, work Resident no is already present for the selected employee.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [14-SEP-2019] -- End



                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                        objPermit._Effectivedate = dtpRpEffDate.Value.Date
                    objPermit._Employeeunkid = iEmpId
                    objPermit._Changereasonunkid = iReasonId
                    If dtRow("rpermitexpirydate").ToString.Trim.Length <= 0 Then
                        objPermit._Expiry_Date = Nothing
                    Else
                        objPermit._Expiry_Date = CDate(dtRow("rpermitexpirydate"))
                    End If
                    objPermit._Isfromemployee = False
                    objPermit._IsResidentPermit = True
                    If dtRow("rpermitissuedate").ToString.Trim.Length <= 0 Then
                        objPermit._Issue_Date = Nothing
                    Else
                        objPermit._Issue_Date = CDate(dtRow("rpermitissuedate"))
                    End If
                    objPermit._Issue_Place = dtRow("rpermitissueplace").ToString()
                    objPermit._Isvoid = False
                    objPermit._Rehiretranunkid = 0
                    objPermit._Work_Permit_No = dtRow("rpermitno").ToString()
                    objPermit._Workcountryunkid = iCountry
                    objPermit._Userunkid = User._Object._Userunkid
                    objPermit._Voiddatetime = Nothing
                    objPermit._Voidreason = ""
                    objPermit._Voiduserunkid = -1
                    objPermit._Statusunkid = 0

                    If objPermit.Insert(Nothing) Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEDates._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    Else

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        intPrivilegeId = 1200
                        eMovement = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT

                        Dim strMsg As String = String.Empty
                        strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, iEmpId)
                        If strMsg.Trim.Length > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = strMsg
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                        objPMovement = Nothing

                        objAPermit._Audittype = enAuditType.ADD
                        objAPermit._Audituserunkid = User._Object._Userunkid
                        objPermit._Effectivedate = dtpRpEffDate.Value.Date
                        objPermit._Employeeunkid = iEmpId
                        objPermit._Changereasonunkid = iReasonId
                        If dtRow("rpermitexpirydate").ToString.Trim.Length <= 0 Then
                            objAPermit._Expiry_Date = Nothing
                        Else
                            objAPermit._Expiry_Date = CDate(dtRow("rpermitexpirydate"))
                        End If
                        objAPermit._Isresidentpermit = True
                        If dtRow("rpermitissuedate").ToString.Trim.Length <= 0 Then
                            objAPermit._Issue_Date = Nothing
                        Else
                            objAPermit._Issue_Date = CDate(dtRow("rpermitissuedate"))
                        End If
                        objAPermit._Issue_Place = dtRow("rpermitissueplace").ToString()
                        objAPermit._Isvoid = False
                        objAPermit._Rehiretranunkid = 0
                        objAPermit._Work_Permit_No = dtRow("rpermitno").ToString()
                        objAPermit._Workcountryunkid = iCountry
                        objAPermit._Isvoid = False
                        objAPermit._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objAPermit._Voidreason = ""
                        objAPermit._Voiduserunkid = -1
                        objAPermit._Tranguid = Guid.NewGuid.ToString()
                        objAPermit._Transactiondate = Now
                        objAPermit._Remark = ""
                        objAPermit._Rehiretranunkid = 0
                        objAPermit._Mappingunkid = 0
                        objAPermit._Isweb = False
                        objAPermit._Isfinal = False
                        objAPermit._Ip = getIP()
                        objAPermit._Hostname = getHostName()
                        objAPermit._Form_Name = mstrModuleName
                        'S.SANDEEP |13-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : Left Out
                        objAPermit._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'S.SANDEEP |13-JUN-2019| -- END

                        'Gajanan [14-SEP-2019] -- Start    
                        'Issue: Missing Isexist In Import Movement
                        Dim blnFlag As Boolean = False
                        blnFlag = objAPermit.isExist(True, clsEmployeeMovmentApproval.enOperationType.ADDED, objAPermit._Effectivedate, _
                                                     objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 266, "Sorry, Resident Permit Information is already present in approval process with selected date and allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                        blnFlag = objAPermit.isExist(True, clsEmployeeMovmentApproval.enOperationType.ADDED, objAPermit._Effectivedate, _
                                                     "", objAPermit._Employeeunkid, "", Nothing, True)

                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 267, "Sorry, Resident Permit Information is already present in approval process with selected date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                     

                        blnFlag = objAPermit.isExist(True, clsEmployeeMovmentApproval.enOperationType.ADDED, Nothing, _
                                                     objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                        If blnFlag Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 268, "Sorry, Work Resident Information is already present in approval process with selected allocation combination.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                        'Gajanan [14-SEP-2019] -- End



                        'Pinkal (20-Dec-2024) -- Start
                        'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
                        'If objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED) Then
                        If objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED, Company._Object._Companyunkid) Then
                            'Pinkal (20-Dec-2024) -- End
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 1, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            'S.SANDEEP |17-JAN-2019| -- START
                            'mvt.Add(New clsEmployeeMovmentApproval(1200, clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, iEmpId, Nothing))
                            mvt.Add(New clsEmployeeMovmentApproval(1200, clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, iEmpId, Nothing, clsEmployeeMovmentApproval.enOperationType.ADDED))
                            'S.SANDEEP |17-JAN-2019| -- END
                            If iValue.ContainsKey(clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT) = False Then
                                iValue.Add(clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, iEmpId.ToString())
                            Else
                                iValue(clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT) = iValue(clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT) & "," & iEmpId.ToString()
                            End If
                            'Dim objPMovement As New clsEmployeeMovmentApproval
                            'Dim intPrivilegeId As Integer = 0
                            'Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                            'intPrivilegeId = 1200
                            'eMovement = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, True, Nothing, 0, iEmpId.ToString(), "")
                            'objPMovement = Nothing

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objAPermit._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    End If
                    'S.SANDEEP [20-JUN-2018] -- END
                End If
                '----------------------- END -------------------------------
                'S.SANDEEP [14-JUN-2018] -- END
            Next

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If mvt.Count > 0 Then
                Dim lst As New List(Of Integer)()
                Dim objPMovement As New clsEmployeeMovmentApproval
                For Each item In mvt
                    Dim itype = item
                    If lst.FindIndex(Function(x) x = CInt(itype._MovementType)) < 0 Then
                        lst.Add(item._MovementType)
                        If iValue.ContainsKey(itype._MovementType) Then
                            Dim blnIsResident As Boolean = False
                            If itype._MovementType = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT Then blnIsResident = True
                            'S.SANDEEP |17-JAN-2019| -- START
                            'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, itype._PrivilegeId, itype._MovementType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, blnIsResident, itype._DateType, 0, iValue(itype._MovementType), "")
                            objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, itype._PrivilegeId, itype._MovementType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, clsEmployeeMovmentApproval.enOperationType.ADDED, blnIsResident, itype._DateType, 0, iValue(itype._MovementType), "")
                            'S.SANDEEP |17-JAN-2019| -- END
                            Exit For
                        End If
                    End If
                Next
                objPMovement = Nothing
            End If
            'S.SANDEEP [20-JUN-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub GroupBoxOperation(ByVal objGrp As eZee.Common.eZeeCollapsibleContainer)
        Try
            For Each cntr As Control In fpnlData.Controls
                If TypeOf cntr Is eZee.Common.eZeeCollapsibleContainer Then
                    If cntr.Name <> objGrp.Name Then
                        If CType(cntr, eZee.Common.eZeeCollapsibleContainer).Checked = True Then
                            CType(cntr, eZee.Common.eZeeCollapsibleContainer).Checked = False
                            Call GroupControlOpration(CType(cntr, eZee.Common.eZeeCollapsibleContainer))
                        End If
                    End If
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GroupBoxOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub GroupControlOpration(ByVal objGrp As eZee.Common.eZeeCollapsibleContainer)
        Try
            For Each cntr As Control In objGrp.Controls
                If TypeOf cntr Is System.Windows.Forms.CheckBox Then
                    CType(cntr, System.Windows.Forms.CheckBox).Checked = False
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GroupControlOpration", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " From's Events "

    Private Sub frmBulkMovementUpdateWizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp

            objlblAllocationCaption.Text = Language.getMessage(mstrModuleName, 60, "Note: Please take a note that effective date will get set for selected  allocations and it will be set for all employees.")
            objlblCCCaption.Text = Language.getMessage(mstrModuleName, 61, "Note: Please take a note that effective date will get set for costcenter and it will be set for all employees.")
            objlblEOCCaption.Text = Language.getMessage(mstrModuleName, 62, "Note: Please take a note that effective date will get set for selected eoc and leaving date operation and it will be set for all employees.")
            objlblRetireCaption.Text = Language.getMessage(mstrModuleName, 63, "Note: Please take a note that effective date will get set for selected retirement date operation and it will be set for all employees.")
            objlbJobCaption.Text = Language.getMessage(mstrModuleName, 64, "Note: Please take a note that effective date will get set for selected job operation and it will be set for all employees.")
            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            objlblProbationCaption.Text = Language.getMessage(mstrModuleName, 269, "Note: Please take a note that effective date will get set for selected probation start,end date operation and it will be set for all employees.")
            objlblConfirmationCaption.Text = Language.getMessage(mstrModuleName, 270, "Note: Please take a note that effective date will get set for selected confirmation date operation and it will be set for all employees.")
            objlblSuspensionCaption.Text = Language.getMessage(mstrModuleName, 271, "Note: Please take a note that effective date will get set for selected suspension start,end date operation and it will be set for all employees.")
            objlblWorkPermitCaption.Text = Language.getMessage(mstrModuleName, 272, "Note: Please take a note that effective date will get set for selected work permit operation and it will be set for all employees.")
            objlblResidentPermitCaption.Text = Language.getMessage(mstrModuleName, 273, "Note: Please take a note that effective date will get set for selected resident permit operation and it will be set for all employees.")
            'S.SANDEEP [14-JUN-2018] -- END
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objlblExemptionCaption.Text = Language.getMessage(mstrModuleName, 271, "Note: Please take a note that effective date will get set for selected Exemption start,end date operation and it will be set for all employees.")
            'Sohail (21 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBulkMovementUpdateWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub WizUpdateEmployee_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizUpdateEmployee.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizUpdateEmployee.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizUpdateEmployee_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub WizUpdateEmployee_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizUpdateEmployee.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizUpdateEmployee.Pages.IndexOf(wizPageFile)

                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 58, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    If chkBranch.Checked = False AndAlso chkDepGroup.Checked = False AndAlso chkDepartment.Checked = False AndAlso _
                        chkSecGroup.Checked = False AndAlso chkSections.Checked = False AndAlso _
                        chkUnitGroup.Checked = False AndAlso chkUnit.Checked = False AndAlso chkTeam.Checked = False AndAlso _
                        chkClassGrp.Checked = False AndAlso chkClass.Checked = False AndAlso chkCostCenter.Checked = False AndAlso _
                        chkJobGroup.Checked = False AndAlso chkJob.Checked = False AndAlso chkEOCDate.Checked = False _
                        AndAlso chkLeavingDate.Checked = False AndAlso chkRetirementDt.Checked = False AndAlso chkProbationFromDt.Checked = False _
                        AndAlso chkProbationToDt.Checked = False AndAlso chkConfirmationDt.Checked = False _
                        AndAlso chkSuspensionFrmDt.Checked = False AndAlso chkSuspensionToDt.Checked = False _
                        AndAlso ChkWorkPermitNo.Checked = False AndAlso chkWpCountry.Checked = False AndAlso chkWpIssueDt.Checked = False _
                        AndAlso chkWpExpiryDt.Checked = False AndAlso chkWpIssuePlace.Checked = False AndAlso ChkResidentPermitNo.Checked = False _
                        AndAlso chkRpCountry.Checked = False AndAlso chkRpIssueDt.Checked = False AndAlso chkRpExpiryDt.Checked = False _
                        AndAlso chkExemptionFrmDt.Checked = False AndAlso chkExemptionToDt.Checked = False _
                        AndAlso chkRpIssuePlace.Checked = False Then
                        'Sohail (21 Oct 2019) -- [chkExemptionFrmDt, chkExemptionToDt]

                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 66, "No update information selected to update. Please select atleast one update information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 56, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case WizUpdateEmployee.Pages.IndexOf(wizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox AndAlso ctrl.Enabled = True Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 57, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case WizUpdateEmployee.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizUpdateEmployee_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button Event "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Menu Event(s) "

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = CType(dgData.DataSource, DataView).ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Update Employee Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox(S) Event"

    Private Sub chkBranch_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUnitGroup.CheckedChanged, chkUnit.CheckedChanged, chkTeam.CheckedChanged, chkSections.CheckedChanged, chkSecGroup.CheckedChanged, chkDepGroup.CheckedChanged, chkDepartment.CheckedChanged, chkClassGrp.CheckedChanged, chkClass.CheckedChanged, chkBranch.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkBranch.Name.ToUpper
                    If chkBranch.Checked = True Then
                        objlblSign13.Enabled = True : lblBranch.Enabled = True : cboBranch.Enabled = True
                    Else
                        objlblSign13.Enabled = False : lblBranch.Enabled = False : cboBranch.Enabled = False
                    End If
                Case chkDepGroup.Name.ToUpper
                    If chkDepGroup.Checked = True Then
                        objlblSign14.Enabled = True : lblDepGroup.Enabled = True : cboDepartmentGroup.Enabled = True
                    Else
                        objlblSign14.Enabled = False : lblDepGroup.Enabled = False : cboDepartmentGroup.Enabled = False
                    End If
                Case chkDepartment.Name.ToUpper
                    If chkDepartment.Checked = True Then
                        objlblSign5.Enabled = True : lblDepartment.Enabled = True : cboDepartment.Enabled = True
                    Else
                        objlblSign5.Enabled = False : lblDepartment.Enabled = False : cboDepartment.Enabled = False
                    End If
                Case chkSecGroup.Name.ToUpper
                    If chkSecGroup.Checked = True Then
                        objlblSign15.Enabled = True : lblSecGroup.Enabled = True : cboSectionGroup.Enabled = True
                    Else
                        objlblSign15.Enabled = False : lblSecGroup.Enabled = False : cboSectionGroup.Enabled = False
                    End If
                Case chkSections.Name.ToUpper
                    If chkSections.Checked = True Then
                        objlblSign16.Enabled = True : lblSections.Enabled = True : cboSection.Enabled = True
                    Else
                        objlblSign16.Enabled = False : lblSections.Enabled = False : cboSection.Enabled = False
                    End If
                Case chkUnitGroup.Name.ToUpper
                    If chkUnitGroup.Checked = True Then
                        objlblSign17.Enabled = True : lblUnitGroup.Enabled = True : cboUnitGroup.Enabled = True
                    Else
                        objlblSign17.Enabled = False : lblUnitGroup.Enabled = False : cboUnitGroup.Enabled = False
                    End If
                Case chkUnit.Name.ToUpper
                    If chkUnit.Checked = True Then
                        objlblSign18.Enabled = True : lblUnit.Enabled = True : cboUnit.Enabled = True
                    Else
                        objlblSign18.Enabled = False : lblUnit.Enabled = False : cboUnit.Enabled = False
                    End If
                Case chkTeam.Name.ToUpper
                    If chkTeam.Checked = True Then
                        objlblSign19.Enabled = True : lblTeam.Enabled = True : cboTeam.Enabled = True
                    Else
                        objlblSign19.Enabled = False : lblTeam.Enabled = False : cboTeam.Enabled = False
                    End If
                Case chkClassGrp.Name.ToUpper
                    If chkClassGrp.Checked = True Then
                        objlblSign7.Enabled = True : lblClassGrp.Enabled = True : cboClassGroup.Enabled = True
                    Else
                        objlblSign7.Enabled = False : lblClassGrp.Enabled = False : cboClassGroup.Enabled = False
                    End If
                Case chkClass.Name.ToUpper
                    If chkClass.Checked = True Then
                        objlblSign8.Enabled = True : lblClass.Enabled = True : cboClass.Enabled = True
                    Else
                        objlblSign8.Enabled = False : lblClass.Enabled = False : cboClass.Enabled = False
                    End If
            End Select

            If chkBranch.Checked OrElse chkDepGroup.Checked OrElse chkDepartment.Checked OrElse chkSections.Checked OrElse chkSecGroup.Checked OrElse chkUnitGroup.Checked OrElse chkUnit.Checked OrElse chkTeam.Checked OrElse chkClassGrp.Checked OrElse chkClass.Checked Then
                chkAllocationChangeReson.Checked = True
                objlblSign23.Enabled = True : lblAllocationReason.Enabled = True : cboAllocationChangeReason.Enabled = True
            Else
                chkAllocationChangeReson.Checked = False
                objlblSign23.Enabled = False : lblAllocationReason.Enabled = False : cboAllocationChangeReason.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkBranch_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkCostCenter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCostCenter.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkCostCenter.Name.ToUpper
                    If chkCostCenter.Checked = True Then
                        objlblSign21.Enabled = True : lblCostCenter.Enabled = True : cboCostCenter.Enabled = True
                        objlblSign25.Enabled = True : lblCCReason.Enabled = True : cboCostCenterChangeReason.Enabled = True
                    Else
                        objlblSign21.Enabled = False : lblCostCenter.Enabled = False : cboCostCenter.Enabled = False
                        objlblSign25.Enabled = False : lblCCReason.Enabled = False : cboCostCenterChangeReason.Enabled = False
                    End If
            End Select
            chkCcChangeReason.Checked = chkCostCenter.Checked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCostCenter_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkRetirementDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRetirementDt.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkRetirementDt.Name.ToUpper
                    If chkRetirementDt.Checked = True Then
                        objlblSign4.Enabled = True : lblRetirementDt.Enabled = True : cboRetirementDate.Enabled = True
                        objlblSign26.Enabled = True : lblRtReason.Enabled = True : cboRetirementChangeReason.Enabled = True
                    Else
                        objlblSign4.Enabled = False : lblRetirementDt.Enabled = False : cboRetirementDate.Enabled = False
                        objlblSign26.Enabled = False : lblRtReason.Enabled = False : cboRetirementChangeReason.Enabled = False
                    End If
            End Select
            chkRtChangeReason.Checked = chkRetirementDt.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkRetirementDt_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkJobGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkJobGroup.CheckedChanged, chkJob.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkJob.Name.ToUpper
                    If chkJob.Checked = True Then
                        objlblSign6.Enabled = True : lblJob.Enabled = True : cboJob.Enabled = True
                    Else
                        objlblSign6.Enabled = False : lblJob.Enabled = False : cboJob.Enabled = False
                    End If
                Case chkJobGroup.Name.ToUpper
                    If chkJobGroup.Checked = True Then
                        objlblSign20.Enabled = True : lblJobGroup.Enabled = True : cboJobGroup.Enabled = True
                    Else
                        objlblSign20.Enabled = False : lblJobGroup.Enabled = False : cboJobGroup.Enabled = False
                    End If
            End Select

            If chkJobGroup.Checked OrElse chkJob.Checked Then
                'Gajanan [24-OCT-2019] -- Start    
                chkJob.Checked = True : chkJob.Enabled = False
                'Gajanan [24-OCT-2019] -- End
                chkCateChangeReason.Checked = True
                objlblSign24.Enabled = True : lblJobReason.Enabled = True : cboCategorizeChangeReason.Enabled = True
            Else
                chkCateChangeReason.Checked = False
                objlblSign24.Enabled = False : lblJobReason.Enabled = False : cboCategorizeChangeReason.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkJobGroup_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkEOCDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLeavingDate.CheckedChanged, chkEOCDate.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkEOCDate.Name.ToUpper
                    If chkEOCDate.Checked = True Then
                        objlblSign11.Enabled = True : lblECODate.Enabled = True : cboEOCDate.Enabled = True
                    Else
                        objlblSign11.Enabled = False : lblECODate.Enabled = False : cboEOCDate.Enabled = False
                    End If
                Case chkLeavingDate.Name.ToUpper
                    If chkLeavingDate.Checked = True Then
                        objlblSign12.Enabled = True : lblLeavingDate.Enabled = True : cboLeavingDate.Enabled = True
                    Else
                        objlblSign12.Enabled = False : lblLeavingDate.Enabled = False : cboLeavingDate.Enabled = False
                    End If
            End Select
            If chkEOCDate.Checked OrElse chkLeavingDate.Checked Then
                chkEOCReason.Checked = True
                chkIsexcludePayroll.Checked = True
                objlblSign22.Enabled = True : lblEOCReason.Enabled = True : cboTerminationReason.Enabled = True
                objlblSign27.Enabled = True : lblIsexcludePayroll.Enabled = True : cboIsExcludePayroll.Enabled = True
            Else
                chkEOCReason.Checked = False
                chkIsexcludePayroll.Checked = False
                objlblSign22.Enabled = False : lblEOCReason.Enabled = False : cboTerminationReason.Enabled = False
                objlblSign27.Enabled = False : lblIsexcludePayroll.Enabled = False : cboIsExcludePayroll.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkEOCDate_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub chkProbationFromDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkProbationFromDt.CheckedChanged, chkProbationToDt.CheckedChanged, chkPbChangeReason.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkProbationFromDt.Name.ToUpper
                    chkProbationToDt.Checked = chkProbationFromDt.Checked
                    chkPbChangeReason.Checked = chkProbationFromDt.Checked
                    If chkProbationFromDt.Checked = True Then
                        objlblSign28.Enabled = True : lblProbationFromDt.Enabled = True : cboProbationFromDate.Enabled = True
                    Else
                        objlblSign28.Enabled = False : lblProbationFromDt.Enabled = False : cboProbationFromDate.Enabled = False
                    End If
                Case chkProbationToDt.Name.ToUpper
                    If chkProbationToDt.Checked = True Then
                        objlblSign30.Enabled = True : lblProbationToDt.Enabled = True : cboProbationToDate.Enabled = True
                    Else
                        objlblSign30.Enabled = False : lblProbationToDt.Enabled = False : cboProbationToDate.Enabled = False
                    End If
                Case chkPbChangeReason.Name.ToUpper
                    If chkPbChangeReason.Checked = True Then
                        objlblSign29.Enabled = True : lblPbReason.Enabled = True : cboProbationChangeReason.Enabled = True
                    Else
                        objlblSign29.Enabled = False : lblPbReason.Enabled = False : cboProbationChangeReason.Enabled = False
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkProbationFromDt_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkConfirmationDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConfirmationDt.CheckedChanged
        Try
            chkConfChangeReason.Checked = chkConfirmationDt.Checked
            If chkConfirmationDt.Checked = True Then
                objlblSign31.Enabled = True : lbConfirmationDt.Enabled = True : cboConfirmationDate.Enabled = True
            Else
                objlblSign31.Enabled = False : lbConfirmationDt.Enabled = False : cboConfirmationDate.Enabled = False
            End If
            If chkConfChangeReason.Checked = True Then
                objlblSign32.Enabled = True : lblConfirmationReason.Enabled = True : cboConfirmationChangeReason.Enabled = True
            Else
                objlblSign32.Enabled = False : lblConfirmationReason.Enabled = False : cboConfirmationChangeReason.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkConfirmationDt_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkSuspensionFrmDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSuspensionFrmDt.CheckedChanged, chkSuspensionToDt.CheckedChanged, chkSuspChangeReason.CheckedChanged
        'Sohail (23 Dec 2020) - [chkSuspensionToDt, chkSuspChangeReason]
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkSuspensionFrmDt.Name.ToUpper
                    chkSuspensionToDt.Checked = chkSuspensionFrmDt.Checked
                    chkSuspChangeReason.Checked = chkSuspensionFrmDt.Checked
                    If chkSuspensionFrmDt.Checked = True Then
                        objlblSign33.Enabled = True : lblSuspFromDt.Enabled = True : cboSuspensionFromDate.Enabled = True
                    Else
                        objlblSign33.Enabled = False : lblSuspFromDt.Enabled = False : cboSuspensionFromDate.Enabled = False
                    End If
                    'Sohail (23 Dec 2020) -- Start
                    'G & C # OLD-223 : - Inactive fields when doing Bulk Employee Exemption & Suspension.
                    'Case chkSuspensionToDt.Name.ToUpper
                    'If chkSuspensionToDt.Checked = True Then
                Case chkSuspChangeReason.Name.ToUpper
                    'Sohail (23 Dec 2020) -- End
                    If chkSuspChangeReason.Checked = True Then
                        objlblSign34.Enabled = True : lblSuspensionReason.Enabled = True : cboSuspensionChangeReason.Enabled = True
                    Else
                        objlblSign34.Enabled = False : lblSuspensionReason.Enabled = False : cboSuspensionChangeReason.Enabled = False
                    End If
                    'Sohail (23 Dec 2020) -- Start
                    'G & C # OLD-223 : - Inactive fields when doing Bulk Employee Exemption & Suspension.
                    'Case chkSuspChangeReason.Name.ToUpper
                    'If chkSuspChangeReason.Checked = True Then
                Case chkSuspensionToDt.Name.ToUpper
                    'Sohail (23 Dec 2020) -- End
                    If chkSuspensionToDt.Checked = True Then
                        objlblSign35.Enabled = True : lblSuspToDt.Enabled = True : cboSuspensionToDate.Enabled = True
                    Else
                        objlblSign35.Enabled = False : lblSuspToDt.Enabled = False : cboSuspensionToDate.Enabled = False
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSuspensionFrmDt_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private Sub chkExemptionFrmDts_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExemptionFrmDt.CheckedChanged, chkExemptionToDt.CheckedChanged, chkExemptionChangeReason.CheckedChanged
        'Sohail (23 Dec 2020) - [chkExemptionToDt, chkExemptionChangeReason]
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case chkExemptionFrmDt.Name.ToUpper
                    chkExemptionToDt.Checked = chkExemptionFrmDt.Checked
                    chkExemptionChangeReason.Checked = chkExemptionFrmDt.Checked
                    If chkExemptionFrmDt.Checked = True Then
                        objlblSign49.Enabled = True : lblExemptionFromDt.Enabled = True : cboExemptionFromDate.Enabled = True
                    Else
                        objlblSign49.Enabled = False : lblExemptionFromDt.Enabled = False : cboExemptionFromDate.Enabled = False
                    End If
                Case chkExemptionToDt.Name.ToUpper
                    If chkExemptionToDt.Checked = True Then
                        'Sohail (23 Dec 2020) -- Start
                        'G & C # OLD-223 : - Inactive fields when doing Bulk Employee Exemption & Suspension.
                        'objlblSign51.Enabled = True : lblExemptionReason.Enabled = True : cboExemptionChangeReason.Enabled = True
                        objlblSign51.Enabled = True : lblExemptionToDt.Enabled = True : cboExemptionToDate.Enabled = True
                        'Sohail (23 Dec 2020) -- End
                    Else
                        'Sohail (23 Dec 2020) -- Start
                        'G & C # OLD-223 : - Inactive fields when doing Bulk Employee Exemption & Suspension.
                        'objlblSign51.Enabled = False : lblExemptionReason.Enabled = False : cboExemptionChangeReason.Enabled = False
                        objlblSign51.Enabled = False : lblExemptionToDt.Enabled = False : cboExemptionToDate.Enabled = False
                        'Sohail (23 Dec 2020) -- End
                    End If
                Case chkExemptionChangeReason.Name.ToUpper
                    If chkExemptionChangeReason.Checked = True Then
                        'Sohail (23 Dec 2020) -- Start
                        'G & C # OLD-223 : - Inactive fields when doing Bulk Employee Exemption & Suspension.
                        'objlblSign50.Enabled = True : lblExemptionToDt.Enabled = True : cboExemptionToDate.Enabled = True
                        objlblSign50.Enabled = True : lblExemptionReason.Enabled = True : cboExemptionChangeReason.Enabled = True
                        'Sohail (23 Dec 2020) -- End
                    Else
                        'Sohail (23 Dec 2020) -- Start
                        'G & C # OLD-223 : - Inactive fields when doing Bulk Employee Exemption & Suspension.
                        'objlblSign50.Enabled = False : lblExemptionToDt.Enabled = False : cboExemptionToDate.Enabled = False
                        objlblSign50.Enabled = False : lblExemptionReason.Enabled = False : cboExemptionChangeReason.Enabled = False
                        'Sohail (23 Dec 2020) -- End
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExemptionFrmDts_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (21 Oct 2019) -- End

    Private Sub ChkWorkPermitNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkWorkPermitNo.CheckedChanged, chkWpCountry.CheckedChanged, chkWpIssueDt.CheckedChanged, chkWpExpiryDt.CheckedChanged, chkWpIssuePlace.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case ChkWorkPermitNo.Name.ToUpper
                    If ChkWorkPermitNo.Checked = True Then
                        objlblSign36.Enabled = True : lblWPno.Enabled = True : cboWorkPermitNo.Enabled = True
                    Else
                        objlblSign36.Enabled = False : lblWPno.Enabled = False : cboWorkPermitNo.Enabled = False
                    End If
                Case chkWpCountry.Name.ToUpper
                    If chkWpCountry.Checked = True Then
                        objlblSign40.Enabled = True : lblWPCountry.Enabled = True : cboWorkPermitCountry.Enabled = True
                    Else
                        objlblSign40.Enabled = False : lblWPCountry.Enabled = False : cboWorkPermitCountry.Enabled = False
                    End If
                Case chkWpIssueDt.Name.ToUpper
                    If chkWpIssueDt.Checked = True Then
                        objlblSign38.Enabled = True : lblWPIssueDt.Enabled = True : cboWorkPermitIssueDate.Enabled = True
                    Else
                        objlblSign38.Enabled = False : lblWPIssueDt.Enabled = False : cboWorkPermitIssueDate.Enabled = False
                    End If
                Case chkWpExpiryDt.Name.ToUpper
                    If chkWpExpiryDt.Checked = True Then
                        objlblSign39.Enabled = True : lblWPExpiryDate.Enabled = True : cboWorkPermitExpiryDate.Enabled = True
                    Else
                        objlblSign39.Enabled = False : lblWPExpiryDate.Enabled = False : cboWorkPermitExpiryDate.Enabled = False
                    End If

                Case chkWpIssuePlace.Name.ToUpper
                    If chkWpIssuePlace.Checked = True Then
                        objlblSign47.Enabled = True : lblWpIssuePlace.Enabled = True : cboWorkPermitIssuePlace.Enabled = True
                    Else
                        objlblSign47.Enabled = False : lblWpIssuePlace.Enabled = False : cboWorkPermitIssuePlace.Enabled = False
                    End If
            End Select

            If ChkWorkPermitNo.Checked = True OrElse chkWpCountry.Checked = True OrElse chkWpIssueDt.Checked = True OrElse chkWpExpiryDt.Checked = True OrElse chkWpIssuePlace.Checked = True Then
                chkWpChangeReason.Checked = True
                objlblSign37.Enabled = True : lblWPReason.Enabled = True : cboWorkPermitChangeReason.Enabled = True
            Else
                chkWpChangeReason.Checked = False
                objlblSign37.Enabled = False : lblWPReason.Enabled = False : cboWorkPermitChangeReason.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ChkWorkPermitNo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ChkResidentPermitNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkResidentPermitNo.CheckedChanged, chkRpCountry.CheckedChanged, chkRpIssueDt.CheckedChanged, chkRpExpiryDt.CheckedChanged, chkRpIssuePlace.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
                Case ChkResidentPermitNo.Name.ToUpper
                    If ChkResidentPermitNo.Checked = True Then
                        objlblSign41.Enabled = True : lblRPno.Enabled = True : cboResidentPermitNo.Enabled = True
                    Else
                        objlblSign41.Enabled = False : lblRPno.Enabled = False : cboResidentPermitNo.Enabled = False
                    End If
                Case chkRpCountry.Name.ToUpper
                    If chkRpCountry.Checked = True Then
                        objlblSign46.Enabled = True : lblRPCountry.Enabled = True : cboResidentCountry.Enabled = True
                    Else
                        objlblSign46.Enabled = False : lblRPCountry.Enabled = False : cboResidentCountry.Enabled = False
                    End If
                Case chkRpIssueDt.Name.ToUpper
                    If chkRpIssueDt.Checked = True Then
                        objlblSign45.Enabled = True : lblRPIssueDate.Enabled = True : cboResidentIssueDate.Enabled = True
                    Else
                        objlblSign45.Enabled = False : lblRPIssueDate.Enabled = False : cboResidentIssueDate.Enabled = False
                    End If
                Case chkRpExpiryDt.Name.ToUpper
                    If chkRpExpiryDt.Checked = True Then
                        objlblSign44.Enabled = True : lblRPExpiryDate.Enabled = True : cboResidentExpiryDate.Enabled = True
                    Else
                        objlblSign44.Enabled = False : lblRPExpiryDate.Enabled = False : cboResidentExpiryDate.Enabled = False
                    End If
                Case chkRpIssuePlace.Name.ToUpper
                    If chkRpIssuePlace.Checked = True Then
                        objlblSign48.Enabled = True : lblRpIssuePlace.Enabled = True : cboResidentIssuePlace.Enabled = True
                    Else
                        objlblSign48.Enabled = False : lblRpIssuePlace.Enabled = False : cboResidentIssuePlace.Enabled = False
                    End If
            End Select
            If ChkResidentPermitNo.Checked = True OrElse chkRpCountry.Checked = True OrElse chkRpIssueDt.Checked = True OrElse chkRpExpiryDt.Checked = True OrElse chkRpIssuePlace.Checked = True Then
                chkRpChangeReason.Checked = True
                objlblSign42.Enabled = True : lblRPReason.Enabled = True : cboResidentChangeReason.Enabled = True
            Else
                chkRpChangeReason.Checked = False
                objlblSign42.Enabled = False : lblRPReason.Enabled = False : cboResidentChangeReason.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ChkResidentPermitNo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub gbProbationInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbProbationInfo.CheckedChanged
        Try
            dtpPbEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbProbationInfo) : Exit Sub
            chkPbChangeReason.Enabled = False
            chkProbationToDt.Enabled = False
            GroupBoxOperation(gbProbationInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbProbationInfo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub gbSuspensionInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbSuspensionInfo.CheckedChanged
        Try
            dtpSuspEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbSuspensionInfo) : Exit Sub
            chkSuspChangeReason.Enabled = False
            chkSuspensionToDt.Enabled = False
            GroupBoxOperation(gbSuspensionInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbSuspensionInfo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private Sub gbExemptionInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbExemptionInfo.CheckedChanged
        Try
            dtpExemptionEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbExemptionInfo) : Exit Sub
            chkExemptionChangeReason.Enabled = False
            chkExemptionToDt.Enabled = False
            GroupBoxOperation(gbExemptionInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbExemptionInfo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (21 Oct 2019) -- End

    Private Sub gbConfirmationInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbConfirmationInfo.CheckedChanged
        Try
            dtpConEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbConfirmationInfo) : Exit Sub
            chkConfChangeReason.Enabled = False
            GroupBoxOperation(gbConfirmationInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbConfirmationInfo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub gbWorkPermitInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbWorkPermitInfo.CheckedChanged
        Try
            dtpWpEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbWorkPermitInfo) : Exit Sub
            chkWpChangeReason.Enabled = False
            ChkWorkPermitNo.Enabled = False
            ChkWorkPermitNo.Checked = gbWorkPermitInfo.Checked
            GroupBoxOperation(gbWorkPermitInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbWorkPermitInfo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub gbResidentPermitInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbResidentPermitInfo.CheckedChanged
        Try
            dtpRpEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbResidentPermitInfo) : Exit Sub
            chkRpChangeReason.Enabled = False
            ChkResidentPermitNo.Enabled = False
            ChkResidentPermitNo.Checked = gbResidentPermitInfo.Checked
            GroupBoxOperation(gbResidentPermitInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbResidentPermitInfo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

    Private Sub gbUpdateAllocation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbUpdateAllocation.CheckedChanged
        Try
            dtpAllocEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbUpdateAllocation) : Exit Sub
            chkAllocationChangeReson.Enabled = False
            GroupBoxOperation(gbUpdateAllocation)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbUpdateAllocation_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gbCostCenter_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbCostCenter.CheckedChanged
        Try
            dtpCCEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbCostCenter) : Exit Sub
            chkCcChangeReason.Enabled = False
            GroupBoxOperation(gbCostCenter)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbCostCenter_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gbCategorization_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbCategorization.CheckedChanged
        Try
            dtpCatEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbCategorization) : Exit Sub
            chkCateChangeReason.Enabled = False
            GroupBoxOperation(gbCategorization)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbCategorization_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gbRetirementInfo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbRetirementInfo.CheckedChanged
        Try
            dtpRetrEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbRetirementInfo) : Exit Sub
            chkRtChangeReason.Enabled = False
            GroupBoxOperation(gbRetirementInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbRetirementInfo_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gbEOCInfo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbEOCInfo.CheckedChanged
        Try
            dtpTerEffDate.Value = Now.Date
            If CType(sender, System.Windows.Forms.CheckBox).Checked = False Then GroupControlOpration(gbEOCInfo) : Exit Sub
            chkEOCReason.Enabled = False
            chkIsexcludePayroll.Enabled = False
            GroupBoxOperation(gbEOCInfo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbEOCInfo_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Control(S) Event"
    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Enabled = True Then
                            If cr.Name <> cmb.Name Then
                                If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 65, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    cmb.SelectedIndex = -1
                                    cmb.Select()
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Link Event(s) "

    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked, lnkCostCenterFormat.LinkClicked, lnkCategorizeFormat.LinkClicked, lnkTerminationFormat.LinkClicked, lnkRetirementFormat.LinkClicked, lnkProbationFormat.LinkClicked, lnkConfirmationFormat.LinkClicked, lnkSuspensionFormat.LinkClicked, lnkWorkPermitFormat.LinkClicked, lnkResidentPermitFormat.LinkClicked, lnkExemptionFormat.LinkClicked
        'Sohail (21 Oct 2019) - [lnkExemptionFormat]
        Try
            Dim dtExTable As New DataTable("DataList")
            dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 210, "EmployeeCode"), System.Type.GetType("System.String"))
            Select Case CType(sender, LinkLabel).Name.ToUpper
                Case lnkAllocationFormat.Name.ToUpper
                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known 

                    'dtExTable.Columns.Add("Branch", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("DepartmentGroup", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("Department", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("SectionGroup", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("Section", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("UnitGroup", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("Unit", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("Team", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("ClassGroup", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("Class", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("AllocationChangeReason", System.Type.GetType("System.String"))


                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 169, "Branch"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 170, "DepartmentGroup"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 171, "Department"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 172, "SectionGroup"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 173, "Section"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 174, "UnitGroup"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 175, "Unit"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 176, "Team"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 177, "ClassGroup"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 178, "Class"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 179, "AllocationChangeReason"), System.Type.GetType("System.String"))

                    'Gajanan (24 Nov 2018) -- End
                Case lnkCostCenterFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known
                    'dtExTable.Columns.Add("CostCenter", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("CostCenterChangeReason", System.Type.GetType("System.String"))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 180, "CostCenter"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 181, "CostCenterChangeReason"), System.Type.GetType("System.String"))
                    'Gajanan (24 Nov 2018) -- End


                Case lnkCategorizeFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known

                    'dtExTable.Columns.Add("JobGroup", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("Job", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("CategorizeChangeReason", System.Type.GetType("System.String"))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 182, "JobGroup"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 183, "Job"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 184, "CategorizeChangeReason"), System.Type.GetType("System.String"))

                    'Gajanan (24 Nov 2018) -- End

                Case lnkTerminationFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known

                    'dtExTable.Columns.Add("EOCDate", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("LeavingDate", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("TerminationReason", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("IsExcludePayroll", System.Type.GetType("System.String"))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 185, "EOCDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 186, "LeavingDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 187, "TerminationReason"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 188, "IsExcludePayroll"), System.Type.GetType("System.String"))
                    'Gajanan (24 Nov 2018) -- End

                Case lnkRetirementFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known

                    'dtExTable.Columns.Add("RetirementDate", System.Type.GetType("System.String"))
                    'dtExTable.Columns.Add("RetirementChangeReason", System.Type.GetType("System.String"))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 189, "RetirementDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 190, "RetirementChangeReason"), System.Type.GetType("System.String"))
                    'Gajanan (24 Nov 2018) -- End


                    'S.SANDEEP [14-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {NMB}
                Case lnkProbationFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known


                    'dtExTable.Columns.Add("ProbationFromDate", GetType(System.String))
                    'dtExTable.Columns.Add("ProbationToDate", GetType(System.String))
                    'dtExTable.Columns.Add("ProbationChangeReason", GetType(System.String))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 191, "ProbationFromDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 192, "ProbationToDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 193, "ProbationChangeReason"), System.Type.GetType("System.String"))
                    'Gajanan (24 Nov 2018) -- End

                Case lnkConfirmationFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known


                    'dtExTable.Columns.Add("ConfirmationDate", GetType(System.String))
                    'dtExTable.Columns.Add("ConfirmationChangeReason", GetType(System.String))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 194, "ConfirmationDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 195, "ConfirmationChangeReason"), System.Type.GetType("System.String"))
                    'Gajanan (24 Nov 2018) -- End

                Case lnkSuspensionFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known


                    'dtExTable.Columns.Add("SuspensionFromDate", GetType(System.String))
                    'dtExTable.Columns.Add("SuspensionToDate", GetType(System.String))
                    'dtExTable.Columns.Add("SuspensionChangeReason", GetType(System.String))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 196, "SuspensionFromDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 197, "SuspensionToDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 198, "SuspensionChangeReason"), System.Type.GetType("System.String"))

                    'Gajanan (24 Nov 2018) -- End

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case lnkExemptionFormat.Name.ToUpper

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 196, "ExemptionFromDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 197, "ExemptionToDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 198, "ExemptionChangeReason"), System.Type.GetType("System.String"))
                    'Sohail (21 Oct 2019) -- End

                Case lnkWorkPermitFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known


                    'dtExTable.Columns.Add("WorkPermitNo", GetType(System.String))
                    'dtExTable.Columns.Add("WorkPermitCountry", GetType(System.String))
                    'dtExTable.Columns.Add("WorkPermitIssueDate", GetType(System.String))
                    'dtExTable.Columns.Add("WorkPermitExpiryDate", GetType(System.String))
                    'dtExTable.Columns.Add("WorkPermitIssuePlace", GetType(System.String))
                    'dtExTable.Columns.Add("WorkPermitChangeReason", GetType(System.String))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 199, "WorkPermitNo"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 200, "WorkPermitCountry"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 201, "WorkPermitIssueDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 202, "WorkPermitExpiryDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 203, "WorkPermitIssuePlace"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 204, "WorkPermitChangeReason"), System.Type.GetType("System.String"))

                    'Gajanan (24 Nov 2018) -- End

                Case lnkResidentPermitFormat.Name.ToUpper

                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known


                    'dtExTable.Columns.Add("ResidentPermitNo", GetType(System.String))
                    'dtExTable.Columns.Add("ResidentCountry", GetType(System.String))
                    'dtExTable.Columns.Add("ResidentIssueDate", GetType(System.String))
                    'dtExTable.Columns.Add("ResidentExpiryDate", GetType(System.String))
                    'dtExTable.Columns.Add("ResidentIssuePlace", GetType(System.String))
                    'dtExTable.Columns.Add("ResidentChangeReason", GetType(System.String))

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 205, "ResidentPermitNo"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 206, "ResidentCountry"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 207, "ResidentIssueDate"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 208, "ResidentIssuePlace"), System.Type.GetType("System.String"))
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 209, "ResidentChangeReason"), System.Type.GetType("System.String"))

                    'S.SANDEEP [14-JUN-2018] -- END
                    'Gajanan (24 Nov 2018) -- End

            End Select

            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'Dim IExcel As New ExcelData : Dim dsList As New DataSet
            Dim dsList As New DataSet
            'S.SANDEEP [12-Jan-2018] -- END

            dsList.Tables.Add(dtExTable.Copy)

            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"

            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(dlgSaveFile.FileName, dsList)
                OpenXML_Export(dlgSaveFile.FileName, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
            End If
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 67, "Template Exported Successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known

                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault

                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 127, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 124, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 125, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next

            lstCombos = (From p In objpnlCtrls.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) And (p).Enabled = True Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)


                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known

                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault

                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In objpnlCtrls.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 127, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 124, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 125, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbUpdateAllocation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbUpdateAllocation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbCostCenter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCostCenter.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbCategorization.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCategorization.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEOCInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEOCInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbRetirementInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRetirementInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbProbationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbProbationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbConfirmationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbConfirmationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSuspensionInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSuspensionInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbWorkPermitInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbWorkPermitInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbResidentPermitInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbResidentPermitInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbExemptionInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExemptionInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonSave.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.WizUpdateEmployee.CancelText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_CancelText", Me.WizUpdateEmployee.CancelText)
            Me.WizUpdateEmployee.NextText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_NextText", Me.WizUpdateEmployee.NextText)
            Me.WizUpdateEmployee.BackText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_BackText", Me.WizUpdateEmployee.BackText)
            Me.WizUpdateEmployee.FinishText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_FinishText", Me.WizUpdateEmployee.FinishText)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.gbUpdateAllocation.Text = Language._Object.getCaption(Me.gbUpdateAllocation.Name, Me.gbUpdateAllocation.Text)
            Me.chkBranch.Text = Language._Object.getCaption(Me.chkBranch.Name, Me.chkBranch.Text)
            Me.chkDepartment.Text = Language._Object.getCaption(Me.chkDepartment.Name, Me.chkDepartment.Text)
            Me.chkDepGroup.Text = Language._Object.getCaption(Me.chkDepGroup.Name, Me.chkDepGroup.Text)
            Me.chkSecGroup.Text = Language._Object.getCaption(Me.chkSecGroup.Name, Me.chkSecGroup.Text)
            Me.chkSections.Text = Language._Object.getCaption(Me.chkSections.Name, Me.chkSections.Text)
            Me.chkUnitGroup.Text = Language._Object.getCaption(Me.chkUnitGroup.Name, Me.chkUnitGroup.Text)
            Me.chkClassGrp.Text = Language._Object.getCaption(Me.chkClassGrp.Name, Me.chkClassGrp.Text)
            Me.chkUnit.Text = Language._Object.getCaption(Me.chkUnit.Name, Me.chkUnit.Text)
            Me.chkTeam.Text = Language._Object.getCaption(Me.chkTeam.Name, Me.chkTeam.Text)
            Me.chkClass.Text = Language._Object.getCaption(Me.chkClass.Name, Me.chkClass.Text)
            Me.gbCostCenter.Text = Language._Object.getCaption(Me.gbCostCenter.Name, Me.gbCostCenter.Text)
            Me.chkCostCenter.Text = Language._Object.getCaption(Me.chkCostCenter.Name, Me.chkCostCenter.Text)
            Me.gbCategorization.Text = Language._Object.getCaption(Me.gbCategorization.Name, Me.gbCategorization.Text)
            Me.chkJobGroup.Text = Language._Object.getCaption(Me.chkJobGroup.Name, Me.chkJobGroup.Text)
            Me.chkJob.Text = Language._Object.getCaption(Me.chkJob.Name, Me.chkJob.Text)
            Me.gbEOCInfo.Text = Language._Object.getCaption(Me.gbEOCInfo.Name, Me.gbEOCInfo.Text)
            Me.chkEOCDate.Text = Language._Object.getCaption(Me.chkEOCDate.Name, Me.chkEOCDate.Text)
            Me.chkEOCReason.Text = Language._Object.getCaption(Me.chkEOCReason.Name, Me.chkEOCReason.Text)
            Me.chkLeavingDate.Text = Language._Object.getCaption(Me.chkLeavingDate.Name, Me.chkLeavingDate.Text)
            Me.gbRetirementInfo.Text = Language._Object.getCaption(Me.gbRetirementInfo.Name, Me.gbRetirementInfo.Text)
            Me.chkRetirementDt.Text = Language._Object.getCaption(Me.chkRetirementDt.Name, Me.chkRetirementDt.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
            Me.lblEOCReason.Text = Language._Object.getCaption(Me.lblEOCReason.Name, Me.lblEOCReason.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblSections.Text = Language._Object.getCaption(Me.lblSections.Name, Me.lblSections.Text)
            Me.lblSecGroup.Text = Language._Object.getCaption(Me.lblSecGroup.Name, Me.lblSecGroup.Text)
            Me.lblDepGroup.Text = Language._Object.getCaption(Me.lblDepGroup.Name, Me.lblDepGroup.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblLeavingDate.Text = Language._Object.getCaption(Me.lblLeavingDate.Name, Me.lblLeavingDate.Text)
            Me.lblECODate.Text = Language._Object.getCaption(Me.lblECODate.Name, Me.lblECODate.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblClassGrp.Text = Language._Object.getCaption(Me.lblClassGrp.Name, Me.lblClassGrp.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblRetirementDt.Text = Language._Object.getCaption(Me.lblRetirementDt.Name, Me.lblRetirementDt.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.chkAllocationChangeReson.Text = Language._Object.getCaption(Me.chkAllocationChangeReson.Name, Me.chkAllocationChangeReson.Text)
            Me.chkCcChangeReason.Text = Language._Object.getCaption(Me.chkCcChangeReason.Name, Me.chkCcChangeReason.Text)
            Me.chkCateChangeReason.Text = Language._Object.getCaption(Me.chkCateChangeReason.Name, Me.chkCateChangeReason.Text)
            Me.chkRtChangeReason.Text = Language._Object.getCaption(Me.chkRtChangeReason.Name, Me.chkRtChangeReason.Text)
            Me.lblAllocationReason.Text = Language._Object.getCaption(Me.lblAllocationReason.Name, Me.lblAllocationReason.Text)
            Me.lblCCReason.Text = Language._Object.getCaption(Me.lblCCReason.Name, Me.lblCCReason.Text)
            Me.lblJobReason.Text = Language._Object.getCaption(Me.lblJobReason.Name, Me.lblJobReason.Text)
            Me.lblRtReason.Text = Language._Object.getCaption(Me.lblRtReason.Name, Me.lblRtReason.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.chkIsexcludePayroll.Text = Language._Object.getCaption(Me.chkIsexcludePayroll.Name, Me.chkIsexcludePayroll.Text)
            Me.lblIsexcludePayroll.Text = Language._Object.getCaption(Me.lblIsexcludePayroll.Name, Me.lblIsexcludePayroll.Text)
            Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
            Me.lnkCostCenterFormat.Text = Language._Object.getCaption(Me.lnkCostCenterFormat.Name, Me.lnkCostCenterFormat.Text)
            Me.lnkCategorizeFormat.Text = Language._Object.getCaption(Me.lnkCategorizeFormat.Name, Me.lnkCategorizeFormat.Text)
            Me.lnkTerminationFormat.Text = Language._Object.getCaption(Me.lnkTerminationFormat.Name, Me.lnkTerminationFormat.Text)
            Me.lnkRetirementFormat.Text = Language._Object.getCaption(Me.lnkRetirementFormat.Name, Me.lnkRetirementFormat.Text)
			Me.gbProbationInfo.Text = Language._Object.getCaption(Me.gbProbationInfo.Name, Me.gbProbationInfo.Text)
			Me.lnkProbationFormat.Text = Language._Object.getCaption(Me.lnkProbationFormat.Name, Me.lnkProbationFormat.Text)
			Me.chkPbChangeReason.Text = Language._Object.getCaption(Me.chkPbChangeReason.Name, Me.chkPbChangeReason.Text)
			Me.chkProbationFromDt.Text = Language._Object.getCaption(Me.chkProbationFromDt.Name, Me.chkProbationFromDt.Text)
			Me.chkProbationToDt.Text = Language._Object.getCaption(Me.chkProbationToDt.Name, Me.chkProbationToDt.Text)
			Me.gbConfirmationInfo.Text = Language._Object.getCaption(Me.gbConfirmationInfo.Name, Me.gbConfirmationInfo.Text)
			Me.lnkConfirmationFormat.Text = Language._Object.getCaption(Me.lnkConfirmationFormat.Name, Me.lnkConfirmationFormat.Text)
			Me.chkConfChangeReason.Text = Language._Object.getCaption(Me.chkConfChangeReason.Name, Me.chkConfChangeReason.Text)
			Me.chkConfirmationDt.Text = Language._Object.getCaption(Me.chkConfirmationDt.Name, Me.chkConfirmationDt.Text)
			Me.gbSuspensionInfo.Text = Language._Object.getCaption(Me.gbSuspensionInfo.Name, Me.gbSuspensionInfo.Text)
			Me.chkSuspensionToDt.Text = Language._Object.getCaption(Me.chkSuspensionToDt.Name, Me.chkSuspensionToDt.Text)
			Me.lnkSuspensionFormat.Text = Language._Object.getCaption(Me.lnkSuspensionFormat.Name, Me.lnkSuspensionFormat.Text)
			Me.chkSuspChangeReason.Text = Language._Object.getCaption(Me.chkSuspChangeReason.Name, Me.chkSuspChangeReason.Text)
			Me.chkSuspensionFrmDt.Text = Language._Object.getCaption(Me.chkSuspensionFrmDt.Name, Me.chkSuspensionFrmDt.Text)
			Me.gbWorkPermitInfo.Text = Language._Object.getCaption(Me.gbWorkPermitInfo.Name, Me.gbWorkPermitInfo.Text)
			Me.chkWpIssueDt.Text = Language._Object.getCaption(Me.chkWpIssueDt.Name, Me.chkWpIssueDt.Text)
			Me.lnkWorkPermitFormat.Text = Language._Object.getCaption(Me.lnkWorkPermitFormat.Name, Me.lnkWorkPermitFormat.Text)
			Me.chkWpChangeReason.Text = Language._Object.getCaption(Me.chkWpChangeReason.Name, Me.chkWpChangeReason.Text)
			Me.ChkWorkPermitNo.Text = Language._Object.getCaption(Me.ChkWorkPermitNo.Name, Me.ChkWorkPermitNo.Text)
			Me.chkWpExpiryDt.Text = Language._Object.getCaption(Me.chkWpExpiryDt.Name, Me.chkWpExpiryDt.Text)
			Me.chkWpCountry.Text = Language._Object.getCaption(Me.chkWpCountry.Name, Me.chkWpCountry.Text)
			Me.gbResidentPermitInfo.Text = Language._Object.getCaption(Me.gbResidentPermitInfo.Name, Me.gbResidentPermitInfo.Text)
			Me.chkRpCountry.Text = Language._Object.getCaption(Me.chkRpCountry.Name, Me.chkRpCountry.Text)
			Me.chkRpExpiryDt.Text = Language._Object.getCaption(Me.chkRpExpiryDt.Name, Me.chkRpExpiryDt.Text)
			Me.chkRpIssueDt.Text = Language._Object.getCaption(Me.chkRpIssueDt.Name, Me.chkRpIssueDt.Text)
			Me.lnkResidentPermitFormat.Text = Language._Object.getCaption(Me.lnkResidentPermitFormat.Name, Me.lnkResidentPermitFormat.Text)
			Me.chkRpChangeReason.Text = Language._Object.getCaption(Me.chkRpChangeReason.Name, Me.chkRpChangeReason.Text)
			Me.ChkResidentPermitNo.Text = Language._Object.getCaption(Me.ChkResidentPermitNo.Name, Me.ChkResidentPermitNo.Text)
			Me.lblProbationFromDt.Text = Language._Object.getCaption(Me.lblProbationFromDt.Name, Me.lblProbationFromDt.Text)
			Me.lblPbReason.Text = Language._Object.getCaption(Me.lblPbReason.Name, Me.lblPbReason.Text)
			Me.EZeeLine2.Text = Language._Object.getCaption(Me.EZeeLine2.Name, Me.EZeeLine2.Text)
			Me.lblProbationToDt.Text = Language._Object.getCaption(Me.lblProbationToDt.Name, Me.lblProbationToDt.Text)
			Me.lbConfirmationDt.Text = Language._Object.getCaption(Me.lbConfirmationDt.Name, Me.lbConfirmationDt.Text)
			Me.lblConfirmationReason.Text = Language._Object.getCaption(Me.lblConfirmationReason.Name, Me.lblConfirmationReason.Text)
			Me.EZeeLine3.Text = Language._Object.getCaption(Me.EZeeLine3.Name, Me.EZeeLine3.Text)
			Me.lblSuspToDt.Text = Language._Object.getCaption(Me.lblSuspToDt.Name, Me.lblSuspToDt.Text)
			Me.lblSuspFromDt.Text = Language._Object.getCaption(Me.lblSuspFromDt.Name, Me.lblSuspFromDt.Text)
			Me.lblSuspensionReason.Text = Language._Object.getCaption(Me.lblSuspensionReason.Name, Me.lblSuspensionReason.Text)
			Me.EZeeLine4.Text = Language._Object.getCaption(Me.EZeeLine4.Name, Me.EZeeLine4.Text)
			Me.lblWPIssueDt.Text = Language._Object.getCaption(Me.lblWPIssueDt.Name, Me.lblWPIssueDt.Text)
			Me.lblWPno.Text = Language._Object.getCaption(Me.lblWPno.Name, Me.lblWPno.Text)
			Me.lblWPReason.Text = Language._Object.getCaption(Me.lblWPReason.Name, Me.lblWPReason.Text)
			Me.EZeeLine5.Text = Language._Object.getCaption(Me.EZeeLine5.Name, Me.EZeeLine5.Text)
			Me.lblWPExpiryDate.Text = Language._Object.getCaption(Me.lblWPExpiryDate.Name, Me.lblWPExpiryDate.Text)
			Me.lblWPCountry.Text = Language._Object.getCaption(Me.lblWPCountry.Name, Me.lblWPCountry.Text)
			Me.lblRPCountry.Text = Language._Object.getCaption(Me.lblRPCountry.Name, Me.lblRPCountry.Text)
			Me.lblRPExpiryDate.Text = Language._Object.getCaption(Me.lblRPExpiryDate.Name, Me.lblRPExpiryDate.Text)
			Me.lblRPIssueDate.Text = Language._Object.getCaption(Me.lblRPIssueDate.Name, Me.lblRPIssueDate.Text)
			Me.lblRPno.Text = Language._Object.getCaption(Me.lblRPno.Name, Me.lblRPno.Text)
			Me.lblRPReason.Text = Language._Object.getCaption(Me.lblRPReason.Name, Me.lblRPReason.Text)
			Me.EZeeLine6.Text = Language._Object.getCaption(Me.EZeeLine6.Name, Me.EZeeLine6.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
			Me.chkWpIssuePlace.Text = Language._Object.getCaption(Me.chkWpIssuePlace.Name, Me.chkWpIssuePlace.Text)
			Me.chkRpIssuePlace.Text = Language._Object.getCaption(Me.chkRpIssuePlace.Name, Me.chkRpIssuePlace.Text)
			Me.lblWpIssuePlace.Text = Language._Object.getCaption(Me.lblWpIssuePlace.Name, Me.lblWpIssuePlace.Text)
			Me.lblRpIssuePlace.Text = Language._Object.getCaption(Me.lblRpIssuePlace.Name, Me.lblRpIssuePlace.Text)
			Me.gbExemptionInfo.Text = Language._Object.getCaption(Me.gbExemptionInfo.Name, Me.gbExemptionInfo.Text)
			Me.chkExemptionToDt.Text = Language._Object.getCaption(Me.chkExemptionToDt.Name, Me.chkExemptionToDt.Text)
			Me.lnkExemptionFormat.Text = Language._Object.getCaption(Me.lnkExemptionFormat.Name, Me.lnkExemptionFormat.Text)
			Me.chkExemptionChangeReason.Text = Language._Object.getCaption(Me.chkExemptionChangeReason.Name, Me.chkExemptionChangeReason.Text)
			Me.chkExemptionFrmDt.Text = Language._Object.getCaption(Me.chkExemptionFrmDt.Name, Me.chkExemptionFrmDt.Text)
			Me.lblExemptionToDt.Text = Language._Object.getCaption(Me.lblExemptionToDt.Name, Me.lblExemptionToDt.Text)
			Me.lblExemptionFromDt.Text = Language._Object.getCaption(Me.lblExemptionFromDt.Name, Me.lblExemptionFromDt.Text)
			Me.lblExemptionReason.Text = Language._Object.getCaption(Me.lblExemptionReason.Name, Me.lblExemptionReason.Text)
			Me.EZeeLine7.Text = Language._Object.getCaption(Me.EZeeLine7.Name, Me.EZeeLine7.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Success")
            Language.setMessage(mstrModuleName, 2, "Fail")
            Language.setMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue.")
            Language.setMessage(mstrModuleName, 4, "Branch is mandatory information. Please select Branch to continue.")
            Language.setMessage(mstrModuleName, 5, "Department Group is mandatory information. Please select Department Group to continue.")
            Language.setMessage(mstrModuleName, 6, "Department is mandatory information. Please select Department to continue.")
            Language.setMessage(mstrModuleName, 7, "Sections Group is mandatory information. Please select Sections Group to continue.")
            Language.setMessage(mstrModuleName, 8, "Sections is mandatory information. Please select Sections to continue.")
            Language.setMessage(mstrModuleName, 9, "Unit Group is mandatory information. Please select Unit Group to continue.")
            Language.setMessage(mstrModuleName, 10, "Unit is mandatory information. Please select Unit to continue.")
            Language.setMessage(mstrModuleName, 11, "Team is mandatory information. Please select Team to continue.")
            Language.setMessage(mstrModuleName, 12, "Class Group is mandatory information. Please select Class Group to continue.")
            Language.setMessage(mstrModuleName, 13, "Class is mandatory information. Please select Class to continue.")
            Language.setMessage(mstrModuleName, 14, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Language.setMessage(mstrModuleName, 15, "Cost Center Reason is mandatory information. Please select Cost Center Reason to continue.")
            Language.setMessage(mstrModuleName, 16, "Job Group is mandatory information. Please select Job Group to continue.")
            Language.setMessage(mstrModuleName, 17, "Job is mandatory information. Please select Job to continue.")
            Language.setMessage(mstrModuleName, 18, "EOC Date is mandatory information. Please select EOC Date to continue.")
            Language.setMessage(mstrModuleName, 19, "EOC Date format is not correct. Please set correct EOC Date format  to continue.")
            Language.setMessage(mstrModuleName, 20, "Leaving Date is mandatory information. Please select Leaving Date to continue.")
            Language.setMessage(mstrModuleName, 21, "Leaving Date format is not correct. Please set correct Leaving Date format  to continue.")
            Language.setMessage(mstrModuleName, 22, "EOC Reason is mandatory information. Please select EOC Reason to continue.")
            Language.setMessage(mstrModuleName, 23, "Isexclude Payroll is mandatory information. Please select Isexclude Payroll to continue.")
            Language.setMessage(mstrModuleName, 25, "Isexclude Payroll is not vaild data.")
            Language.setMessage(mstrModuleName, 26, "Retirement Date is mandatory information. Please select Retirement Date to continue.")
            Language.setMessage(mstrModuleName, 27, "Retirement Date format is not correct. Please set correct Retirement Date format  to continue.")
            Language.setMessage(mstrModuleName, 28, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 29, "Branch Not Found.")
            Language.setMessage(mstrModuleName, 30, "Department Group Not Found.")
            Language.setMessage(mstrModuleName, 31, "Department Not Found.")
            Language.setMessage(mstrModuleName, 32, "Section Group Not Found.")
            Language.setMessage(mstrModuleName, 33, "Section Not Found.")
            Language.setMessage(mstrModuleName, 34, "Unit Group Not Found.")
            Language.setMessage(mstrModuleName, 35, "Unit Not Found.")
            Language.setMessage(mstrModuleName, 36, "Team Not Found.")
            Language.setMessage(mstrModuleName, 37, "Class Group Not Found.")
            Language.setMessage(mstrModuleName, 38, "Class Not Found.")
            Language.setMessage(mstrModuleName, 39, "Allocation Reason Not Found.")
            Language.setMessage(mstrModuleName, 40, "Sorry, Transfer transaction for the selected effective date is already present for the employee in the selected file.")
            Language.setMessage(mstrModuleName, 41, "Cost Center Not Found.")
            Language.setMessage(mstrModuleName, 42, "Sorry, Cost Center transaction for the selected effective date is already present for the employee in the selected file.")
            Language.setMessage(mstrModuleName, 43, "Cost Center Reason Not Found.")
            Language.setMessage(mstrModuleName, 45, "Job Group Not Found.")
            Language.setMessage(mstrModuleName, 46, "Job Not Found.")
            Language.setMessage(mstrModuleName, 47, "Sorry, Recategorization transaction for the selected effective date is already present for the employee in the selected file.")
            Language.setMessage(mstrModuleName, 48, "Recategorize Reason Not Found.")
            Language.setMessage(mstrModuleName, 49, "EOC date is not vaild")
            Language.setMessage(mstrModuleName, 50, "leaving Date is not vaild")
            Language.setMessage(mstrModuleName, 51, "EOC Reason Not Found.")
            Language.setMessage(mstrModuleName, 52, "Sorry, Termination transaction for the selected effective date is already present for the employee in the selected file.")
            Language.setMessage(mstrModuleName, 53, "Retirement Date is not vaild")
            Language.setMessage(mstrModuleName, 54, "EOC Reason Not Found.")
            Language.setMessage(mstrModuleName, 55, "Sorry, Retirement transaction for the selected effective date is already present for the employee in the selected file")
            Language.setMessage(mstrModuleName, 56, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 57, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 58, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 60, "Note: Please take a note that effective date will get set for selected  allocations and it will be set for all employees.")
            Language.setMessage(mstrModuleName, 61, "Note: Please take a note that effective date will get set for costcenter and it will be set for all employees.")
            Language.setMessage(mstrModuleName, 62, "Note: Please take a note that effective date will get set for selected eoc and leaving date operation and it will be set for all employees.")
            Language.setMessage(mstrModuleName, 63, "Note: Please take a note that effective date will get set for selected retirement date operation and it will be set for all employees.")
            Language.setMessage(mstrModuleName, 64, "Note: Please take a note that effective date will get set for selected job operation and it will be set for all employees.")
            Language.setMessage(mstrModuleName, 65, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 66, "No update information selected to update. Please select atleast one update information.")
            Language.setMessage(mstrModuleName, 67, "Template Exported Successfully.")
			Language.setMessage(mstrModuleName, 68, "Probation From Date is mandatory information. Please select Probation From Date to continue.")
			Language.setMessage(mstrModuleName, 69, "Probation From Date format is not correct. Please set correct Probation From Date format to continue.")
			Language.setMessage(mstrModuleName, 70, "Probation To Date is mandatory information. Please select Probation To Date to continue.")
			Language.setMessage(mstrModuleName, 71, "Probation To Date format is not correct. Please set correct Probation To Date format to continue.")
			Language.setMessage(mstrModuleName, 72, "Probation Change Reason is mandatory information. Please select Probation Change Reason to continue.")
			Language.setMessage(mstrModuleName, 73, "Confirmation Date is mandatory information. Please select Confirmation Date to continue.")
			Language.setMessage(mstrModuleName, 74, "Confirmation Date format is not correct. Please set correct Confirmation Date format to continue.")
			Language.setMessage(mstrModuleName, 75, "Confirmation Change Reason is mandatory information. Please select Confirmation Change Reason to continue.")
			Language.setMessage(mstrModuleName, 76, "Suspension From Date is mandatory information. Please select Suspension From Date to continue.")
			Language.setMessage(mstrModuleName, 77, "Suspension From Date format is not correct. Please set correct Suspension From Date format to continue.")
			Language.setMessage(mstrModuleName, 78, "Suspension To Date is mandatory information. Please select Suspension To Date to continue.")
			Language.setMessage(mstrModuleName, 79, "Suspension To Date format is not correct. Please set correct Suspension To Date format to continue.")
			Language.setMessage(mstrModuleName, 80, "Suspension Change Reason is mandatory information. Please select Suspension Change Reason to continue.")
			Language.setMessage(mstrModuleName, 81, "Workpermit number is mandatory information. Please select workpermit number to continue.")
			Language.setMessage(mstrModuleName, 82, "Workpermit country is mandatory information. Please select workpermit country to continue.")
			Language.setMessage(mstrModuleName, 83, "Workpermit issue date is mandatory information. Please select workpermit issue date to continue.")
			Language.setMessage(mstrModuleName, 84, "Workpermit issue date format is not correct. Please set correct workpermit issue date format to continue.")
			Language.setMessage(mstrModuleName, 85, "Workpermit expiry date is mandatory information. Please select workpermit expiry date to continue.")
			Language.setMessage(mstrModuleName, 86, "Workpermit expiry date format is not correct. Please set correct workpermit expiry date format to continue.")
			Language.setMessage(mstrModuleName, 87, "Workpermit issue place is mandatory information. Please select workpermit issue place to continue.")
			Language.setMessage(mstrModuleName, 88, "Workpermit Change Reason is mandatory information. Please select Workpermit Change Reason to continue.")
			Language.setMessage(mstrModuleName, 89, "Resident permit number is mandatory information. Please select resident permit number to continue.")
			Language.setMessage(mstrModuleName, 90, "Resident permit country is mandatory information. Please select resident permit country to continue.")
			Language.setMessage(mstrModuleName, 91, "Resident permit issue date is mandatory information. Please select resident permit issue date to continue.")
			Language.setMessage(mstrModuleName, 92, "Resident permit issue date format is not correct. Please set correct resident permit issue date format to continue.")
			Language.setMessage(mstrModuleName, 93, "Resident permit expiry date is mandatory information. Please select resident permit expiry date to continue.")
			Language.setMessage(mstrModuleName, 94, "Resident permit expiry date format is not correct. Please set correct resident permit expiry date format to continue.")
			Language.setMessage(mstrModuleName, 95, "Resident permit issue place is mandatory information. Please select resident permit issue place to continue.")
			Language.setMessage(mstrModuleName, 96, "Resident permit Change Reason is mandatory information. Please select resident permit Change Reason to continue.")
			Language.setMessage(mstrModuleName, 97, "Probation From Date is not vaild")
			Language.setMessage(mstrModuleName, 98, "Probation To Date is not vaild")
			Language.setMessage(mstrModuleName, 99, "Probation To Date cannot be less than Probation From Date")
			Language.setMessage(mstrModuleName, 100, "Probation Reason Not Found.")
			Language.setMessage(mstrModuleName, 101, "Sorry, Probation transaction for the selected effective date is already present for the employee in the selected file")
			Language.setMessage(mstrModuleName, 102, "Suspension From Date is not vaild")
			Language.setMessage(mstrModuleName, 103, "Suspension To Date is not vaild")
			Language.setMessage(mstrModuleName, 104, "Suspension To Date cannot be less than Suspension From Date")
			Language.setMessage(mstrModuleName, 105, "Suspension Reason Not Found.")
			Language.setMessage(mstrModuleName, 106, "Sorry, Suspension transaction for the selected effective date is already present for the employee in the selected file")
			Language.setMessage(mstrModuleName, 107, "Confirmation Date is not vaild")
			Language.setMessage(mstrModuleName, 108, "Confirmation Reason Not Found.")
			Language.setMessage(mstrModuleName, 109, "Sorry, Confirmation transaction for the selected effective date is already present for the employee in the selected file")
			Language.setMessage(mstrModuleName, 110, "Work Permit Number is not valid")
			Language.setMessage(mstrModuleName, 111, "Country not found.")
			Language.setMessage(mstrModuleName, 112, "Work Permit Issue Date is not vaild")
			Language.setMessage(mstrModuleName, 113, "Work Permit Expiry Date is not vaild")
			Language.setMessage(mstrModuleName, 114, "Work permit expiry date cannot be less than Work permit issue Date")
			Language.setMessage(mstrModuleName, 115, "Issue Place not found.")
			Language.setMessage(mstrModuleName, 116, "Work Permit Reason Not Found.")
			Language.setMessage(mstrModuleName, 117, "Sorry, Work Permit transaction for the selected effective date is already present for the employee in the selected file")
			Language.setMessage(mstrModuleName, 118, "Resident Permit Number is not valid")
			Language.setMessage(mstrModuleName, 119, "Resident Permit Issue Date is not vaild")
			Language.setMessage(mstrModuleName, 120, "Resident Permit Expiry Date is not vaild")
			Language.setMessage(mstrModuleName, 121, "Resident permit expiry date cannot be less than Resident permit issue Date")
			Language.setMessage(mstrModuleName, 122, "Resident Permit Reason Not Found.")
			Language.setMessage(mstrModuleName, 123, "Sorry, Resident Permit transaction for the selected effective date is already present for the employee in the selected file")
			Language.setMessage(mstrModuleName, 124, " Fields From File.")
			Language.setMessage(mstrModuleName, 125, "Please select different Field.")
			Language.setMessage(mstrModuleName, 126, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 127, "is already Mapped with")
			Language.setMessage(mstrModuleName, 169, "Branch")
			Language.setMessage(mstrModuleName, 170, "DepartmentGroup")
			Language.setMessage(mstrModuleName, 171, "Department")
			Language.setMessage(mstrModuleName, 172, "SectionGroup")
			Language.setMessage(mstrModuleName, 173, "Section")
			Language.setMessage(mstrModuleName, 174, "UnitGroup")
			Language.setMessage(mstrModuleName, 175, "Unit")
			Language.setMessage(mstrModuleName, 176, "Team")
			Language.setMessage(mstrModuleName, 177, "ClassGroup")
			Language.setMessage(mstrModuleName, 178, "Class")
			Language.setMessage(mstrModuleName, 179, "AllocationChangeReason")
			Language.setMessage(mstrModuleName, 180, "CostCenter")
			Language.setMessage(mstrModuleName, 181, "CostCenterChangeReason")
			Language.setMessage(mstrModuleName, 182, "JobGroup")
			Language.setMessage(mstrModuleName, 183, "Job")
			Language.setMessage(mstrModuleName, 184, "CategorizeChangeReason")
			Language.setMessage(mstrModuleName, 185, "EOCDate")
			Language.setMessage(mstrModuleName, 186, "LeavingDate")
			Language.setMessage(mstrModuleName, 187, "TerminationReason")
			Language.setMessage(mstrModuleName, 188, "IsExcludePayroll")
			Language.setMessage(mstrModuleName, 189, "RetirementDate")
			Language.setMessage(mstrModuleName, 190, "RetirementChangeReason")
			Language.setMessage(mstrModuleName, 191, "ProbationFromDate")
			Language.setMessage(mstrModuleName, 192, "ProbationToDate")
			Language.setMessage(mstrModuleName, 193, "ProbationChangeReason")
			Language.setMessage(mstrModuleName, 194, "ConfirmationDate")
			Language.setMessage(mstrModuleName, 195, "ConfirmationChangeReason")
			Language.setMessage(mstrModuleName, 196, "SuspensionFromDate")
			Language.setMessage(mstrModuleName, 197, "SuspensionToDate")
			Language.setMessage(mstrModuleName, 198, "SuspensionChangeReason")
			Language.setMessage(mstrModuleName, 199, "WorkPermitNo")
			Language.setMessage(mstrModuleName, 200, "WorkPermitCountry")
			Language.setMessage(mstrModuleName, 201, "WorkPermitIssueDate")
			Language.setMessage(mstrModuleName, 202, "WorkPermitExpiryDate")
			Language.setMessage(mstrModuleName, 203, "WorkPermitIssuePlace")
			Language.setMessage(mstrModuleName, 204, "WorkPermitChangeReason")
			Language.setMessage(mstrModuleName, 205, "ResidentPermitNo")
			Language.setMessage(mstrModuleName, 206, "ResidentCountry")
			Language.setMessage(mstrModuleName, 207, "ResidentIssueDate")
			Language.setMessage(mstrModuleName, 208, "ResidentIssuePlace")
			Language.setMessage(mstrModuleName, 209, "ResidentChangeReason")
			Language.setMessage(mstrModuleName, 210, "EmployeeCode")
			Language.setMessage(mstrModuleName, 211, "Sorry, transfer information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 212, "Sorry, transfer information is already present for the selected combination.")
			Language.setMessage(mstrModuleName, 213, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
			Language.setMessage(mstrModuleName, 214, "Sorry, Information is already present in approval process with selected date and allocation combination.")
			Language.setMessage(mstrModuleName, 215, "Sorry, Information is already present in approval process with selected date.")
			Language.setMessage(mstrModuleName, 216, "Sorry, Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 217, "Sorry, cost center information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 218, "Sorry, cost center information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 219, "Sorry, cost center information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 220, "Sorry, Information is already present in approval process with selected date and allocation combination.")
			Language.setMessage(mstrModuleName, 221, "Sorry, Information is already present in approval process with selected date.")
			Language.setMessage(mstrModuleName, 222, "Sorry, Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 223, "Sorry, Re-Categorize information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 224, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
			Language.setMessage(mstrModuleName, 225, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
			Language.setMessage(mstrModuleName, 226, "Sorry, Information is already present in approval process with selected date and allocation combination.")
			Language.setMessage(mstrModuleName, 227, "Sorry, Information is already present in approval process with selected date.")
			Language.setMessage(mstrModuleName, 228, "Sorry, Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 229, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period.")
			Language.setMessage(mstrModuleName, 230, "Sorry, You cannot terminate this employee. Reason there are pending salary increment information present.")
			Language.setMessage(mstrModuleName, 231, "Sorry, termination information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 232, "Sorry, This termination information is already present.")
			Language.setMessage(mstrModuleName, 233, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 234, "Sorry, termination Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 235, "Sorry, termination Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 236, "Sorry, retirement information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 237, "cannot be less then effective date.")
			Language.setMessage(mstrModuleName, 238, "Sorry, This retirement information is already present.")
			Language.setMessage(mstrModuleName, 239, "Sorry, You cannot retire this employee. Reason there are pending salary increment information present.")
			Language.setMessage(mstrModuleName, 240, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 241, "Sorry, retirement Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 242, "Sorry, retirement Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 243, "Sorry, porbation information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 244, "Sorry, This porbation information is already present.")
			Language.setMessage(mstrModuleName, 245, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 246, "Sorry, porbation Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 247, "Sorry, suspension information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 248, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 249, "Sorry, suspension Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 250, "Sorry, confirmation information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 251, "Sorry, This retirement information is already present.")
			Language.setMessage(mstrModuleName, 252, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 253, "Sorry, confirmation Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 254, "Sorry, confirmation Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 255, "Sorry, expiry date cannot be less or equal to issue date.")
			Language.setMessage(mstrModuleName, 256, "Sorry, work permit information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 257, "Sorry, work permit no is already present for the selected employee.")
			Language.setMessage(mstrModuleName, 258, "Sorry, work permit no is already present for the selected employee.")
			Language.setMessage(mstrModuleName, 259, "Sorry, Work Permit Information is already present in approval process with selected date and allocation combination.")
			Language.setMessage(mstrModuleName, 260, "Sorry, Work Permit Information is already present in approval process with selected date.")
			Language.setMessage(mstrModuleName, 261, "Sorry, Work Permit Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 262, "Sorry, expiry date cannot be less or equal to issue date.")
			Language.setMessage(mstrModuleName, 263, "Sorry, work permit information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 264, "Sorry, Resident permit no is already present for the selected employee.")
			Language.setMessage(mstrModuleName, 265, "Sorry, work Resident no is already present for the selected employee.")
			Language.setMessage(mstrModuleName, 266, "Sorry, Resident Permit Information is already present in approval process with selected date and allocation combination.")
			Language.setMessage(mstrModuleName, 267, "Sorry, Resident Permit Information is already present in approval process with selected date.")
			Language.setMessage(mstrModuleName, 268, "Sorry, Work Resident Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 269, "Note: Please take a note that effective date will get set for selected probation start,end date operation and it will be set for all employees.")
			Language.setMessage(mstrModuleName, 270, "Note: Please take a note that effective date will get set for selected confirmation date operation and it will be set for all employees.")
			Language.setMessage(mstrModuleName, 271, "Note: Please take a note that effective date will get set for selected suspension start,end date operation and it will be set for all employees.")
			Language.setMessage(mstrModuleName, 272, "Note: Please take a note that effective date will get set for selected work permit operation and it will be set for all employees.")
			Language.setMessage(mstrModuleName, 273, "Note: Please take a note that effective date will get set for selected resident permit operation and it will be set for all employees.")
			Language.setMessage(mstrModuleName, 274, "Sorry, This suspension information is already present.")
			Language.setMessage(mstrModuleName, 275, "ExemptionFromDate")
			Language.setMessage(mstrModuleName, 276, "ExemptionToDate")
			Language.setMessage(mstrModuleName, 277, "ExemptionChangeReason")
			Language.setMessage(mstrModuleName, 278, "Exemption From Date is mandatory information. Please select Exemption From Date to continue.")
			Language.setMessage(mstrModuleName, 279, "Exemption From Date format is not correct. Please set correct Exemption From Date format to continue.")
			Language.setMessage(mstrModuleName, 280, "Exemption To Date is mandatory information. Please select Exemption To Date to continue.")
			Language.setMessage(mstrModuleName, 281, "Exemption To Date format is not correct. Please set correct Exemption To Date format to continue.")
			Language.setMessage(mstrModuleName, 282, "Exemption Change Reason is mandatory information. Please select Exemption Change Reason to continue.")
			Language.setMessage(mstrModuleName, 102, "Exemption From Date is not vaild")
			Language.setMessage(mstrModuleName, 103, "Exemption To Date is not vaild")
			Language.setMessage(mstrModuleName, 104, "Exemption To Date cannot be less than Exemption From Date")
			Language.setMessage(mstrModuleName, 105, "Exemption Reason Not Found.")
			Language.setMessage(mstrModuleName, 106, "Sorry, Exemption transaction for the selected effective date is already present for the employee in the selected file")
			Language.setMessage(mstrModuleName, 196, "ExemptionFromDate")
			Language.setMessage(mstrModuleName, 197, "ExemptionToDate")
			Language.setMessage(mstrModuleName, 198, "ExemptionChangeReason")
			Language.setMessage(mstrModuleName, 247, "Sorry, Exemption information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 248, "Sorry, Exemption Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 249, "Sorry, Exemption Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 271, "Note: Please take a note that effective date will get set for selected Exemption start,end date operation and it will be set for all employees.")
			Language.setMessage(mstrModuleName, 274, "Sorry, This Exemption information is already present.")
            Language.setMessage(mstrModuleName, 283, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it can be assign to one and only one employee.")
            Language.setMessage(mstrModuleName, 284, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it is already in approver.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class