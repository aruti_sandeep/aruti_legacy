﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBenefitCoverage_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBenefitCoverage_List))
        Me.pnlCoveragInfo = New System.Windows.Forms.Panel
        Me.lvBenefitCoverage = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitPlan = New System.Windows.Forms.ColumnHeader
        Me.colhTransactionHead = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpID = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitGroup = New System.Windows.Forms.ColumnHeader
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radShowAll = New System.Windows.Forms.RadioButton
        Me.radShowVoid = New System.Windows.Forms.RadioButton
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.cboTransactionHead = New System.Windows.Forms.ComboBox
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.lblBenefitPlan = New System.Windows.Forms.Label
        Me.cboBenefitPlan = New System.Windows.Forms.ComboBox
        Me.lblBenefitType = New System.Windows.Forms.Label
        Me.cboBenefitGroup = New System.Windows.Forms.ComboBox
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtDBBenefitInPercent = New eZee.TextBox.NumericTextBox
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpAssignDate = New System.Windows.Forms.DateTimePicker
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblBenefitInAmount = New System.Windows.Forms.Label
        Me.lblBenefitsInPercent = New System.Windows.Forms.Label
        Me.txtBenefitAmount = New eZee.TextBox.NumericTextBox
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.radShowActive = New System.Windows.Forms.RadioButton
        Me.pnlCoveragInfo.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCoveragInfo
        '
        Me.pnlCoveragInfo.Controls.Add(Me.lvBenefitCoverage)
        Me.pnlCoveragInfo.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlCoveragInfo.Controls.Add(Me.objFooter)
        Me.pnlCoveragInfo.Controls.Add(Me.txtDBBenefitInPercent)
        Me.pnlCoveragInfo.Controls.Add(Me.lblStartDate)
        Me.pnlCoveragInfo.Controls.Add(Me.dtpAssignDate)
        Me.pnlCoveragInfo.Controls.Add(Me.dtpEndDate)
        Me.pnlCoveragInfo.Controls.Add(Me.lblEndDate)
        Me.pnlCoveragInfo.Controls.Add(Me.lblBenefitInAmount)
        Me.pnlCoveragInfo.Controls.Add(Me.lblBenefitsInPercent)
        Me.pnlCoveragInfo.Controls.Add(Me.txtBenefitAmount)
        Me.pnlCoveragInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCoveragInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlCoveragInfo.Name = "pnlCoveragInfo"
        Me.pnlCoveragInfo.Size = New System.Drawing.Size(703, 419)
        Me.pnlCoveragInfo.TabIndex = 0
        '
        'lvBenefitCoverage
        '
        Me.lvBenefitCoverage.BackColorOnChecked = True
        Me.lvBenefitCoverage.ColumnHeaders = Nothing
        Me.lvBenefitCoverage.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhBenefitPlan, Me.colhTransactionHead, Me.objcolhEmpID, Me.colhBenefitGroup})
        Me.lvBenefitCoverage.CompulsoryColumns = ""
        Me.lvBenefitCoverage.FullRowSelect = True
        Me.lvBenefitCoverage.GridLines = True
        Me.lvBenefitCoverage.GroupingColumn = Nothing
        Me.lvBenefitCoverage.HideSelection = False
        Me.lvBenefitCoverage.Location = New System.Drawing.Point(12, 156)
        Me.lvBenefitCoverage.MinColumnWidth = 50
        Me.lvBenefitCoverage.MultiSelect = False
        Me.lvBenefitCoverage.Name = "lvBenefitCoverage"
        Me.lvBenefitCoverage.OptionalColumns = ""
        Me.lvBenefitCoverage.ShowMoreItem = False
        Me.lvBenefitCoverage.ShowSaveItem = False
        Me.lvBenefitCoverage.ShowSelectAll = True
        Me.lvBenefitCoverage.ShowSizeAllColumnsToFit = True
        Me.lvBenefitCoverage.Size = New System.Drawing.Size(679, 203)
        Me.lvBenefitCoverage.Sortable = True
        Me.lvBenefitCoverage.TabIndex = 4
        Me.lvBenefitCoverage.UseCompatibleStateImageBehavior = False
        Me.lvBenefitCoverage.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 200
        '
        'colhBenefitPlan
        '
        Me.colhBenefitPlan.Tag = "colhBenefitPlan"
        Me.colhBenefitPlan.Text = "Benefit Plan"
        Me.colhBenefitPlan.Width = 150
        '
        'colhTransactionHead
        '
        Me.colhTransactionHead.Tag = "colhTransactionHead"
        Me.colhTransactionHead.Text = "Transaction Head"
        Me.colhTransactionHead.Width = 160
        '
        'objcolhEmpID
        '
        Me.objcolhEmpID.Tag = "objcolhEmpID"
        Me.objcolhEmpID.Text = ""
        Me.objcolhEmpID.Width = 0
        '
        'colhBenefitGroup
        '
        Me.colhBenefitGroup.Tag = "colhBenefitGroup"
        Me.colhBenefitGroup.Text = "Benefit Group"
        Me.colhBenefitGroup.Width = 150
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radShowActive)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radShowAll)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radShowVoid)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.EZeeStraightLine1)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkAllocation)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchTranHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboTransactionHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblTransactionHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBenefitPlan)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBenefitPlan)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBenefitType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBenefitGroup)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.EZeeStraightLine2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnReset)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearch)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployee)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 64)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 90
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(679, 87)
        Me.EZeeCollapsibleContainer1.TabIndex = 0
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radShowAll
        '
        Me.radShowAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowAll.Location = New System.Drawing.Point(490, 65)
        Me.radShowAll.Name = "radShowAll"
        Me.radShowAll.Size = New System.Drawing.Size(106, 17)
        Me.radShowAll.TabIndex = 76
        Me.radShowAll.TabStop = True
        Me.radShowAll.Text = "Show All"
        Me.radShowAll.UseVisualStyleBackColor = True
        '
        'radShowVoid
        '
        Me.radShowVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowVoid.Location = New System.Drawing.Point(269, 65)
        Me.radShowVoid.Name = "radShowVoid"
        Me.radShowVoid.Size = New System.Drawing.Size(106, 17)
        Me.radShowVoid.TabIndex = 76
        Me.radShowVoid.TabStop = True
        Me.radShowVoid.Text = "Show Void"
        Me.radShowVoid.UseVisualStyleBackColor = True
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(260, 59)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(416, 3)
        Me.EZeeStraightLine1.TabIndex = 75
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(542, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 73
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(229, 60)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 71
        '
        'cboTransactionHead
        '
        Me.cboTransactionHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionHead.FormattingEnabled = True
        Me.cboTransactionHead.Location = New System.Drawing.Point(88, 60)
        Me.cboTransactionHead.Name = "cboTransactionHead"
        Me.cboTransactionHead.Size = New System.Drawing.Size(135, 21)
        Me.cboTransactionHead.TabIndex = 70
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(8, 63)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(74, 15)
        Me.lblTransactionHead.TabIndex = 69
        Me.lblTransactionHead.Text = "Trans. Head"
        '
        'lblBenefitPlan
        '
        Me.lblBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitPlan.Location = New System.Drawing.Point(487, 35)
        Me.lblBenefitPlan.Name = "lblBenefitPlan"
        Me.lblBenefitPlan.Size = New System.Drawing.Size(39, 15)
        Me.lblBenefitPlan.TabIndex = 6
        Me.lblBenefitPlan.Text = "Plan"
        Me.lblBenefitPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBenefitPlan
        '
        Me.cboBenefitPlan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitPlan.FormattingEnabled = True
        Me.cboBenefitPlan.Location = New System.Drawing.Point(532, 32)
        Me.cboBenefitPlan.Name = "cboBenefitPlan"
        Me.cboBenefitPlan.Size = New System.Drawing.Size(135, 21)
        Me.cboBenefitPlan.TabIndex = 7
        '
        'lblBenefitType
        '
        Me.lblBenefitType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitType.Location = New System.Drawing.Point(266, 35)
        Me.lblBenefitType.Name = "lblBenefitType"
        Me.lblBenefitType.Size = New System.Drawing.Size(74, 15)
        Me.lblBenefitType.TabIndex = 2
        Me.lblBenefitType.Text = "Benefit Group"
        Me.lblBenefitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBenefitGroup
        '
        Me.cboBenefitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitGroup.FormattingEnabled = True
        Me.cboBenefitGroup.Location = New System.Drawing.Point(346, 32)
        Me.cboBenefitGroup.Name = "cboBenefitGroup"
        Me.cboBenefitGroup.Size = New System.Drawing.Size(135, 21)
        Me.cboBenefitGroup.TabIndex = 3
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(256, 25)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(6, 61)
        Me.EZeeStraightLine2.TabIndex = 5
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(652, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(229, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(629, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(88, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(135, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 364)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(703, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(502, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 72
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(405, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 71
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(309, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 70
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(598, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtDBBenefitInPercent
        '
        Me.txtDBBenefitInPercent.AllowNegative = True
        Me.txtDBBenefitInPercent.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDBBenefitInPercent.DigitsInGroup = 0
        Me.txtDBBenefitInPercent.Flags = 0
        Me.txtDBBenefitInPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBBenefitInPercent.Location = New System.Drawing.Point(302, 99)
        Me.txtDBBenefitInPercent.MaxDecimalPlaces = 6
        Me.txtDBBenefitInPercent.MaxWholeDigits = 21
        Me.txtDBBenefitInPercent.Name = "txtDBBenefitInPercent"
        Me.txtDBBenefitInPercent.Prefix = ""
        Me.txtDBBenefitInPercent.RangeMax = 1.7976931348623157E+308
        Me.txtDBBenefitInPercent.RangeMin = -1.7976931348623157E+308
        Me.txtDBBenefitInPercent.Size = New System.Drawing.Size(40, 21)
        Me.txtDBBenefitInPercent.TabIndex = 9
        Me.txtDBBenefitInPercent.Text = "0"
        Me.txtDBBenefitInPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDBBenefitInPercent.Visible = False
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(229, 89)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(67, 15)
        Me.lblStartDate.TabIndex = 12
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblStartDate.Visible = False
        '
        'dtpAssignDate
        '
        Me.dtpAssignDate.Checked = False
        Me.dtpAssignDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssignDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssignDate.Location = New System.Drawing.Point(299, 86)
        Me.dtpAssignDate.Name = "dtpAssignDate"
        Me.dtpAssignDate.ShowCheckBox = True
        Me.dtpAssignDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpAssignDate.TabIndex = 13
        Me.dtpAssignDate.Visible = False
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(455, 86)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpEndDate.TabIndex = 15
        Me.dtpEndDate.Visible = False
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(400, 89)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(51, 15)
        Me.lblEndDate.TabIndex = 14
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEndDate.Visible = False
        '
        'lblBenefitInAmount
        '
        Me.lblBenefitInAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitInAmount.Location = New System.Drawing.Point(348, 102)
        Me.lblBenefitInAmount.Name = "lblBenefitInAmount"
        Me.lblBenefitInAmount.Size = New System.Drawing.Size(49, 15)
        Me.lblBenefitInAmount.TabIndex = 10
        Me.lblBenefitInAmount.Text = "Amount"
        Me.lblBenefitInAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBenefitInAmount.Visible = False
        '
        'lblBenefitsInPercent
        '
        Me.lblBenefitsInPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitsInPercent.Location = New System.Drawing.Point(229, 102)
        Me.lblBenefitsInPercent.Name = "lblBenefitsInPercent"
        Me.lblBenefitsInPercent.Size = New System.Drawing.Size(67, 15)
        Me.lblBenefitsInPercent.TabIndex = 8
        Me.lblBenefitsInPercent.Text = "Benefit in %"
        Me.lblBenefitsInPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBenefitsInPercent.Visible = False
        '
        'txtBenefitAmount
        '
        Me.txtBenefitAmount.AllowNegative = True
        Me.txtBenefitAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 196608})
        Me.txtBenefitAmount.DigitsInGroup = 0
        Me.txtBenefitAmount.Flags = 0
        Me.txtBenefitAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenefitAmount.Location = New System.Drawing.Point(403, 99)
        Me.txtBenefitAmount.MaxDecimalPlaces = 6
        Me.txtBenefitAmount.MaxWholeDigits = 21
        Me.txtBenefitAmount.Name = "txtBenefitAmount"
        Me.txtBenefitAmount.Prefix = ""
        Me.txtBenefitAmount.RangeMax = 1.7976931348623157E+308
        Me.txtBenefitAmount.RangeMin = -1.7976931348623157E+308
        Me.txtBenefitAmount.Size = New System.Drawing.Size(74, 21)
        Me.txtBenefitAmount.TabIndex = 11
        Me.txtBenefitAmount.Text = "0.000"
        Me.txtBenefitAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtBenefitAmount.Visible = False
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(703, 58)
        Me.EZeeHeader1.TabIndex = 1
        Me.EZeeHeader1.Title = "Benefit Coverage List"
        '
        'radShowActive
        '
        Me.radShowActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowActive.Location = New System.Drawing.Point(381, 65)
        Me.radShowActive.Name = "radShowActive"
        Me.radShowActive.Size = New System.Drawing.Size(106, 17)
        Me.radShowActive.TabIndex = 78
        Me.radShowActive.TabStop = True
        Me.radShowActive.Text = "Show Active"
        Me.radShowActive.UseVisualStyleBackColor = True
        '
        'frmBenefitCoverage_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(703, 419)
        Me.Controls.Add(Me.EZeeHeader1)
        Me.Controls.Add(Me.pnlCoveragInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBenefitCoverage_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Benefit Coverage List"
        Me.pnlCoveragInfo.ResumeLayout(False)
        Me.pnlCoveragInfo.PerformLayout()
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlCoveragInfo As System.Windows.Forms.Panel
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpAssignDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblBenefitsInPercent As System.Windows.Forms.Label
    Friend WithEvents txtBenefitAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBenefitInAmount As System.Windows.Forms.Label
    Friend WithEvents lvBenefitCoverage As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitPlan As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblBenefitType As System.Windows.Forms.Label
    Friend WithEvents cboBenefitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitPlan As System.Windows.Forms.Label
    Friend WithEvents cboBenefitPlan As System.Windows.Forms.ComboBox
    Friend WithEvents txtDBBenefitInPercent As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTransactionHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents colhTransactionHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objcolhEmpID As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents radShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents radShowVoid As System.Windows.Forms.RadioButton
    Friend WithEvents colhBenefitGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents radShowActive As System.Windows.Forms.RadioButton
End Class
