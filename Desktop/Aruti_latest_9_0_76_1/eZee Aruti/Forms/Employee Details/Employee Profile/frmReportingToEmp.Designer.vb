﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportingToEmp
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportingToEmp))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSetReportingTo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRemoveReportingTo = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvReportingEmp = New eZee.Common.eZeeListView(Me.components)
        Me.colhECode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhJobGrp = New System.Windows.Forms.ColumnHeader
        Me.colhJobName = New System.Windows.Forms.ColumnHeader
        Me.objcolhemployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhreporttoemployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhishierarchy = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnViewreportingList = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSaveInfo = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtEmployeeCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.lnReportingInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.colhEffectiveDate = New System.Windows.Forms.ColumnHeader
        Me.pnlMain.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.dtpEffectiveDate)
        Me.pnlMain.Controls.Add(Me.lblEffectiveDate)
        Me.pnlMain.Controls.Add(Me.lnkAllocation)
        Me.pnlMain.Controls.Add(Me.btnOperation)
        Me.pnlMain.Controls.Add(Me.btnDelete)
        Me.pnlMain.Controls.Add(Me.btnAdd)
        Me.pnlMain.Controls.Add(Me.lvReportingEmp)
        Me.pnlMain.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlMain.Controls.Add(Me.cboEmployee)
        Me.pnlMain.Controls.Add(Me.Label1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.txtEmployee)
        Me.pnlMain.Controls.Add(Me.lblEmployee)
        Me.pnlMain.Controls.Add(Me.txtEmployeeCode)
        Me.pnlMain.Controls.Add(Me.lblEmployeeCode)
        Me.pnlMain.Controls.Add(Me.lnReportingInfo)
        Me.pnlMain.Controls.Add(Me.lnEmployeeName)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(685, 439)
        Me.pnlMain.TabIndex = 0
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(598, 70)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(69, 13)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(344, 117)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(118, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 24
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetReportingTo, Me.mnuRemoveReportingTo})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(207, 48)
        '
        'mnuSetReportingTo
        '
        Me.mnuSetReportingTo.Name = "mnuSetReportingTo"
        Me.mnuSetReportingTo.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetReportingTo.Text = "Set As Main Reporting To"
        '
        'mnuRemoveReportingTo
        '
        Me.mnuRemoveReportingTo.Name = "mnuRemoveReportingTo"
        Me.mnuRemoveReportingTo.Size = New System.Drawing.Size(206, 22)
        Me.mnuRemoveReportingTo.Text = "Remove Reporting To"
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(574, 117)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(100, 30)
        Me.btnDelete.TabIndex = 123
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(468, 117)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(100, 30)
        Me.btnAdd.TabIndex = 121
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lvReportingEmp
        '
        Me.lvReportingEmp.BackColorOnChecked = True
        Me.lvReportingEmp.ColumnHeaders = Nothing
        Me.lvReportingEmp.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEffectiveDate, Me.colhECode, Me.colhEmployee, Me.colhJobGrp, Me.colhJobName, Me.objcolhemployeeunkid, Me.objcolhreporttoemployeeunkid, Me.objcolhishierarchy, Me.objcolhGUID})
        Me.lvReportingEmp.CompulsoryColumns = ""
        Me.lvReportingEmp.FullRowSelect = True
        Me.lvReportingEmp.GridLines = True
        Me.lvReportingEmp.GroupingColumn = Nothing
        Me.lvReportingEmp.HideSelection = False
        Me.lvReportingEmp.Location = New System.Drawing.Point(15, 153)
        Me.lvReportingEmp.MinColumnWidth = 50
        Me.lvReportingEmp.MultiSelect = False
        Me.lvReportingEmp.Name = "lvReportingEmp"
        Me.lvReportingEmp.OptionalColumns = ""
        Me.lvReportingEmp.ShowMoreItem = False
        Me.lvReportingEmp.ShowSaveItem = False
        Me.lvReportingEmp.ShowSelectAll = True
        Me.lvReportingEmp.ShowSizeAllColumnsToFit = True
        Me.lvReportingEmp.Size = New System.Drawing.Size(659, 230)
        Me.lvReportingEmp.Sortable = True
        Me.lvReportingEmp.TabIndex = 120
        Me.lvReportingEmp.UseCompatibleStateImageBehavior = False
        Me.lvReportingEmp.View = System.Windows.Forms.View.Details
        '
        'colhECode
        '
        Me.colhECode.DisplayIndex = 1
        Me.colhECode.Text = "Employee Code"
        Me.colhECode.Width = 95
        '
        'colhEmployee
        '
        Me.colhEmployee.DisplayIndex = 2
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 245
        '
        'colhJobGrp
        '
        Me.colhJobGrp.DisplayIndex = 3
        Me.colhJobGrp.Text = "Job Grp"
        Me.colhJobGrp.Width = 110
        '
        'colhJobName
        '
        Me.colhJobName.DisplayIndex = 4
        Me.colhJobName.Text = "Job Name"
        Me.colhJobName.Width = 200
        '
        'objcolhemployeeunkid
        '
        Me.objcolhemployeeunkid.DisplayIndex = 5
        Me.objcolhemployeeunkid.Text = "objcolhemployeeunkid"
        Me.objcolhemployeeunkid.Width = 0
        '
        'objcolhreporttoemployeeunkid
        '
        Me.objcolhreporttoemployeeunkid.DisplayIndex = 6
        Me.objcolhreporttoemployeeunkid.Tag = "objcolhreporttoemployeeunkid"
        Me.objcolhreporttoemployeeunkid.Text = "objcolhreporttoemployeeunkid"
        Me.objcolhreporttoemployeeunkid.Width = 0
        '
        'objcolhishierarchy
        '
        Me.objcolhishierarchy.DisplayIndex = 7
        Me.objcolhishierarchy.Tag = "objcolhishierarchy"
        Me.objcolhishierarchy.Text = "objcolhishierarchy"
        Me.objcolhishierarchy.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.DisplayIndex = 8
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = "objcolhGUID"
        Me.objcolhGUID.Width = 0
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(653, 90)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 118
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(315, 90)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(332, 21)
        Me.cboEmployee.TabIndex = 117
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(243, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 15)
        Me.Label1.TabIndex = 116
        Me.Label1.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnViewreportingList)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSaveInfo)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 389)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(685, 50)
        Me.objFooter.TabIndex = 115
        '
        'btnViewreportingList
        '
        Me.btnViewreportingList.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewreportingList.BackColor = System.Drawing.Color.White
        Me.btnViewreportingList.BackgroundImage = CType(resources.GetObject("btnViewreportingList.BackgroundImage"), System.Drawing.Image)
        Me.btnViewreportingList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewreportingList.BorderColor = System.Drawing.Color.Empty
        Me.btnViewreportingList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewreportingList.FlatAppearance.BorderSize = 0
        Me.btnViewreportingList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewreportingList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewreportingList.ForeColor = System.Drawing.Color.Black
        Me.btnViewreportingList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewreportingList.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewreportingList.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewreportingList.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewreportingList.Location = New System.Drawing.Point(15, 11)
        Me.btnViewreportingList.Name = "btnViewreportingList"
        Me.btnViewreportingList.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewreportingList.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewreportingList.Size = New System.Drawing.Size(119, 30)
        Me.btnViewreportingList.TabIndex = 24
        Me.btnViewreportingList.Text = "&View Reporting To"
        Me.btnViewreportingList.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(580, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 23
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSaveInfo
        '
        Me.btnSaveInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveInfo.BackColor = System.Drawing.Color.White
        Me.btnSaveInfo.BackgroundImage = CType(resources.GetObject("btnSaveInfo.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveInfo.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveInfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveInfo.FlatAppearance.BorderSize = 0
        Me.btnSaveInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveInfo.ForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveInfo.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Location = New System.Drawing.Point(481, 11)
        Me.btnSaveInfo.Name = "btnSaveInfo"
        Me.btnSaveInfo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Size = New System.Drawing.Size(93, 30)
        Me.btnSaveInfo.TabIndex = 5
        Me.btnSaveInfo.Text = "&Save"
        Me.btnSaveInfo.UseVisualStyleBackColor = True
        '
        'txtEmployee
        '
        Me.txtEmployee.BackColor = System.Drawing.Color.White
        Me.txtEmployee.Flags = 0
        Me.txtEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployee.Location = New System.Drawing.Point(315, 41)
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.ReadOnly = True
        Me.txtEmployee.Size = New System.Drawing.Size(359, 21)
        Me.txtEmployee.TabIndex = 114
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(243, 44)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(66, 15)
        Me.lblEmployee.TabIndex = 113
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeeCode
        '
        Me.txtEmployeeCode.BackColor = System.Drawing.Color.White
        Me.txtEmployeeCode.Flags = 0
        Me.txtEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeCode.Location = New System.Drawing.Point(109, 41)
        Me.txtEmployeeCode.Name = "txtEmployeeCode"
        Me.txtEmployeeCode.ReadOnly = True
        Me.txtEmployeeCode.Size = New System.Drawing.Size(128, 21)
        Me.txtEmployeeCode.TabIndex = 112
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(12, 44)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(91, 15)
        Me.lblEmployeeCode.TabIndex = 111
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnReportingInfo
        '
        Me.lnReportingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnReportingInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnReportingInfo.Location = New System.Drawing.Point(12, 70)
        Me.lnReportingInfo.Name = "lnReportingInfo"
        Me.lnReportingInfo.Size = New System.Drawing.Size(580, 17)
        Me.lnReportingInfo.TabIndex = 110
        Me.lnReportingInfo.Text = "Reporting Info"
        Me.lnReportingInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(12, 15)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(662, 17)
        Me.lnEmployeeName.TabIndex = 109
        Me.lnEmployeeName.Text = "Employee Info"
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(109, 96)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpEffectiveDate.TabIndex = 237
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 99)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(83, 15)
        Me.lblEffectiveDate.TabIndex = 236
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'colhEffectiveDate
        '
        Me.colhEffectiveDate.DisplayIndex = 0
        Me.colhEffectiveDate.Text = "Effective Date"
        Me.colhEffectiveDate.Width = 100
        '
        'frmReportingToEmp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 439)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportingToEmp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reporting to Employee(s)"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.cmnuOperation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lnReportingInfo As eZee.Common.eZeeLine
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents txtEmployeeCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents txtEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSaveInfo As eZee.Common.eZeeLightButton
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSetReportingTo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRemoveReportingTo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lvReportingEmp As eZee.Common.eZeeListView
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents colhECode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhemployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhreporttoemployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhishierarchy As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnViewreportingList As eZee.Common.eZeeLightButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents colhEffectiveDate As System.Windows.Forms.ColumnHeader
End Class
