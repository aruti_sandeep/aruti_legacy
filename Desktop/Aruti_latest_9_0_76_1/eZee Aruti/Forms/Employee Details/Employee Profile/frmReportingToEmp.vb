﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmReportingToEmp

#Region " Private Varaibles "

    Private objEmployee As clsEmployee_Master
    Private ReadOnly mstrModuleName As String = "frmReportingToEmp"
    Private mblnCancel As Boolean = True
    Private mintEmployeeUnkid As Integer = -1
    Private dtEmployee As DataTable = Nothing
    Private objReportTo As clsReportingToEmployee
    Private mdtReport As DataTable


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mstrEmpDisplayName As String = ""
    'Pinkal (18-Aug-2018) -- End


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Private mdtOwnReportTolist As DataTable
    Private oldReporttoEmployeeid As Integer = 0
    'Gajanan [24-OCT-2019] -- End

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End


    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mblnIsEmpApproved As Boolean = False
    'Pinkal (04-Apr-2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUnkId As Integer) As Boolean
        Try
            mintEmployeeUnkid = intUnkId

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmReportingToEmp_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsReportingToEmployee.SetMessages()
            objfrm._Other_ModuleNames = "clsReportingToEmployee"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmReportingToEmp_LanguageClick", mstrModuleName)
        Finally
            If objfrm IsNot Nothing Then objfrm.Dispose()
        End Try
    End Sub

    Private Sub frmReportingToEmp_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmployee = New clsEmployee_Master
        objReportTo = New clsReportingToEmployee
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
'Gajanan [24-OCT-2019] -- Start   
'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            btnViewreportingList.Visible = False
'Gajanan [24-OCT-2019] -- End
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = mintEmployeeUnkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintEmployeeUnkid
            'S.SANDEEP [04 JUN 2015] -- END

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            dtpEffectiveDate.MinDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'Hemant (03 Feb 2023) -- End

            Call Fill_Info()


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'objReportTo._EmployeeUnkid = mintEmployeeUnkid
            'mdtReport = objReportTo._RDataTable
            objReportTo._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , True) = mintEmployeeUnkid
            'Hemant (03 Feb 2023) -- [blnIsAllData:=True]
            mdtReport = objReportTo._RDataTable.Copy()
            'Pinkal (18-Aug-2018) -- End


            lvReportingEmp.GridLines = False
            If mdtReport.Rows.Count > 0 Then


                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                'Hemant (03 Feb 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                'Dim drrow() As DataRow = mdtReport.Select("ishierarchy = " & True)
                Dim dtCurrentDate As String = mdtReport.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("effectivedate") <= Format(ConfigParameter._Object._CurrentDateAndTime.Date, "yyyyMMdd")).OrderByDescending(Function(x) x.Field(Of String)("effectivedate")).Select(Function(x) x.Field(Of String)("effectivedate")).Take(1).ToList.SingleOrDefault
                Dim drrow() As DataRow = mdtReport.Select("ishierarchy = true AND effectivedate = '" & dtCurrentDate & "' ")
                'Hemant (03 Feb 2023) -- End

                If drrow.Length > 0 Then
                    oldReporttoEmployeeid = CInt(drrow(0)("reporttoemployeeunkid"))
                End If
                'Gajanan [24-OCT-2019] -- End

                Call Fill_Data()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmReportingToEmp_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_Info()
        Try
            txtEmployeeCode.Text = objEmployee._Employeecode
            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            mstrEmpDisplayName = objEmployee._Displayname
            'Pinkal (18-Aug-2018) -- End


            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            mblnIsEmpApproved = objEmployee._Isapproved
            'Pinkal (04-Apr-2020) -- End


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB


            'dtEmployee = objReportTo.Get_ReportingToEmp("List", _
            '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                            mintEmployeeUnkid, True)
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "ename"
            '    .DataSource = dtEmployee
            'End With
            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim lvItem As ListViewItem
        Try
            With lvReportingEmp
                .Items.Clear()
                For Each dtRow As DataRow In mdtReport.Rows
                    If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                        lvItem = New ListViewItem
                        'Hemant (03 Feb 2023) -- Start
                        'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                        'lvItem.Text = dtRow.Item("ecode").ToString
                        If Not IsDBNull(dtRow.Item("effectivedate")) Then
                            lvItem.Text = Date.ParseExact(dtRow.Item("effectivedate").ToString, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToShortDateString
                        Else
                            lvItem.Text = ""
                        End If
                        lvItem.SubItems.Add(dtRow.Item("ecode").ToString)
                        'Hemant (03 Feb 2023) -- End
                        lvItem.SubItems.Add(dtRow.Item("ename").ToString)
                        lvItem.SubItems.Add(dtRow.Item("job_group").ToString)
                        lvItem.SubItems.Add(dtRow.Item("job_name").ToString)
                        lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                        lvItem.SubItems.Add(dtRow.Item("reporttoemployeeunkid").ToString)
                        lvItem.SubItems.Add(dtRow.Item("ishierarchy").ToString)
                        lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                        
                        If CBool(dtRow.Item("ishierarchy")) = True Then
                            lvItem.BackColor = Color.Gray
                            lvItem.ForeColor = Color.White
                            lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                        End If
                        lvItem.Tag = dtRow.Item("reporttounkid")

                        lvReportingEmp.Items.Add(lvItem)
                    End If
                Next
            End With
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
            lvItem = Nothing
        End Try
    End Sub


    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataTable
        Dim objEmployee As New clsEmployee_Master
        Try

            dsCombos = objReportTo.Get_ReportingToEmp("List", _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      mintEmployeeUnkid, FinancialYear._Object._DatabaseName, True, mstrAdvanceFilter)
            'Gajanan [11-Dec-2019] -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "ename"
                .DataSource = dsCombos
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnSaveInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Try
            Dim blnFlag As Boolean = False
            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            'Dim oTable As List(Of Date) = mdtReport.AsEnumerable().Select(Function(x) x.Field(Of Date)("effectivedate").ToString()).Distinct().ToList()
            Dim lstDates As List(Of String) = mdtReport.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of String)("effectivedate")).Distinct().ToList()
            For Each dtDate As String In lstDates.ToArray
                Dim drDate() As DataRow = mdtReport.Select("ishierarchy = true AND effectivedate = '" & dtDate & "'  AND AUD <> 'D' ")
                If drDate.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Please set at least one employee as default reporting to on Date :") & Date.ParseExact(dtDate, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToShortDateString, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim drRowIshierarchyList() As DataRow = mdtReport.Select("ishierarchy = true AND effectivedate = '" & dtDate & "' AND AUD <> 'D'")


                If drRowIshierarchyList.Count >= 1 Then
                    If objReportTo.IsReportToValid(CInt(drRowIshierarchyList(0)("employeeunkid").ToString()), CInt(drRowIshierarchyList(0)("reporttoemployeeunkid").ToString()), eZeeDate.convertDate(dtDate), mdtOwnReportTolist, Nothing) = False Then
                        'Hemant (03 Feb 2023) -- [eZeeDate.convertDate(dtCurrentDate)]
                        btnViewreportingList.Visible = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you can not set this employee as default reporting to, Reason: Selected reporting to employee is already reporting directly/indirectly to above selected employee."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            Next

            Dim dtCurrentDate As String = mdtReport.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("effectivedate") <= Format(ConfigParameter._Object._CurrentDateAndTime, "yyyyMMdd")).OrderByDescending(Function(x) x.Field(Of String)("effectivedate")).Select(Function(x) x.Field(Of String)("effectivedate")).Take(1).ToList.SingleOrDefault
            'Hemant (03 Feb 2023) -- End


            Dim dtRow() As DataRow = mdtReport.Select("ishierarchy = false AND effectivedate = '" & dtCurrentDate & "' AND AUD <> 'D'")
            'Hemant (03 Feb 2023) -- [AND effectivedate = '" & CDate(dtCurrentDate.Date) & "']
            Dim dCnt() As DataRow = mdtReport.Select("AUD <> 'D' AND effectivedate = '" & dtCurrentDate & "'")
            'Hemant (03 Feb 2023) -- [AND effectivedate = '" & CDate(dtCurrentDate.Date) & "']

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change  
      
            Dim dtRowIshierarchy() As DataRow = mdtReport.Select("ishierarchy = true AND effectivedate = '" & dtCurrentDate & "' AND AUD <> 'D'")
            'Hemant (03 Feb 2023) -- [AND effectivedate = '" & CDate(dtCurrentDate.Date) & "']

            If dtRowIshierarchy.Count >= 1 Then
                If objReportTo.IsReportToValid(CInt(dtRowIshierarchy(0)("employeeunkid").ToString()), CInt(dtRowIshierarchy(0)("reporttoemployeeunkid").ToString()), eZeeDate.convertDate(dtCurrentDate), mdtOwnReportTolist, Nothing) = False Then
                    'Hemant (03 Feb 2023) -- [eZeeDate.convertDate(dtCurrentDate)]
                    btnViewreportingList.Visible = True
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you can not set this employee as default reporting to, Reason: Selected reporting to employee is already reporting directly/indirectly to above selected employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            'Gajanan [24-OCT-2019] -- End

            'Pinkal (20-Mar-2023) -- Start
            'A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube
            mdtReport = New DataView(mdtReport, "", "ishierarchy desc,effectivedate desc", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (20-Mar-2023) -- End

            If dtRow.Length > 0 Then
                If dCnt.Length = dtRow.Length Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Please set at least one employee as default reporting to."), enMsgBoxStyle.Information)
                    Exit Sub
                Else

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'objReportTo._EmployeeUnkid = mintEmployeeUnkid
                    objReportTo._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , True) = mintEmployeeUnkid
                    'Hemant (03 Feb 2023) -- [blnIsAllData:=True]
                    'Pinkal (18-Aug-2018) -- End

                    objReportTo._RDataTable = mdtReport.Copy()
					'Gajanan [24-OCT-2019] -- Start   
					'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                    objReportTo._Ip = getIP()
                    objReportTo._Host = getHostName()
                    objReportTo._Form_Name = mstrModuleName
                    objReportTo._Isweb = False
                    objReportTo._Audituserunkid = User._Object._Userunkid
                    objReportTo._Audittype = 2
					'Gajanan [24-OCT-2019] -- End


                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objReportTo.InsertUpdateDelete() = True Then

                    'Hemant (03 Feb 2023) -- Start
                    'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                    'If objReportTo.InsertUpdateDelete(ConfigParameter._Object._CreateADUserFromEmpMst) = True Then
                    Dim xnewReporttoEmployeeid As Integer = -1
                    If dtRowIshierarchy IsNot Nothing AndAlso dtRowIshierarchy.Length > 0 Then
                        xnewReporttoEmployeeid = CInt(dtRowIshierarchy(0)("reporttoemployeeunkid"))
                    End If

                    'Pinkal (20-Mar-2023) -- Start
                    'A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube.
                    'If objReportTo.InsertUpdateDelete(ConfigParameter._Object._CreateADUserFromEmpMst, oldReporttoEmployeeid, xnewReporttoEmployeeid, , mintEmployeeUnkid) = True Then
                    If objReportTo.InsertUpdateDelete(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, oldReporttoEmployeeid, xnewReporttoEmployeeid, , mintEmployeeUnkid) = True Then
                        'Pinkal (20-Mar-2023) -- End
                        'Hemant (03 Feb 2023) -- End
                        'Pinkal (18-Aug-2018) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Information successfully saved."), enMsgBoxStyle.Information)
                        blnFlag = True
                    End If
                End If
            Else
                If mdtReport.Rows.Count > 0 Then

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'objReportTo._EmployeeUnkid = mintEmployeeUnkid
                    objReportTo._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , True) = mintEmployeeUnkid
                    'Hemant (03 Feb 2023) -- [blnIsAllData:=True]
                    'Pinkal (18-Aug-2018) -- End

                    objReportTo._RDataTable = mdtReport.Copy()
                    'Gajanan [24-OCT-2019] -- Start   
                    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                    objReportTo._RDataTable = mdtReport.Copy()
                    objReportTo._Ip = getIP()
                    objReportTo._Host = getHostName()
                    objReportTo._Form_Name = mstrModuleName
                    objReportTo._Isweb = False
                    objReportTo._Audituserunkid = User._Object._Userunkid
                    objReportTo._Audittype = 2
                    'Gajanan [24-OCT-2019] -- End





                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objReportTo.InsertUpdateDelete() = True Then

                    'Pinkal (15-Feb-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objReportTo.InsertUpdateDelete(ConfigParameter._Object._CreateADUserFromEmpMst, oldReporttoEmployeeid, CInt(dtRowIshierarchy(0)("reporttoemployeeunkid"))) = True Then

                    Dim xnewReporttoEmployeeid As Integer = -1
                    If dtRowIshierarchy IsNot Nothing AndAlso dtRowIshierarchy.Length > 0 Then
                        xnewReporttoEmployeeid = CInt(dtRowIshierarchy(0)("reporttoemployeeunkid"))
                    End If

                    'Pinkal (20-Mar-2023) -- Start
                    'A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube.
                    'If objReportTo.InsertUpdateDelete(ConfigParameter._Object._CreateADUserFromEmpMst, oldReporttoEmployeeid, xnewReporttoEmployeeid, , mintEmployeeUnkid) = True Then
                    If objReportTo.InsertUpdateDelete(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, oldReporttoEmployeeid, xnewReporttoEmployeeid, , mintEmployeeUnkid) = True Then
                        'Pinkal (20-Mar-2023) -- End

                        'Hemant (03 Feb 2023) -- [mintEmployeeUnkid]
                        'Pinkal (15-Feb-2020) -- End
                        'Pinkal (18-Aug-2018) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Information successfully saved."), enMsgBoxStyle.Information)
                        blnFlag = True
                    End If
                End If
            End If
            If blnFlag = True Then
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If objReportTo._Message.Trim.Length > 0 Then
                Call eZeeMsgBox.Show(objReportTo._Message, CType(MsgBoxStyle.Information, enMsgBoxStyle))
            Else
            Call DisplayError.Show("-1", ex.Message, "btnSaveInfo_Click", mstrModuleName)
            End If
            'Pinkal (18-Aug-2018) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If mdtReport.Rows.Count > 0 Then
                Dim dtTemp() As DataRow = mdtReport.Select("AUD <> ''")
                If dtTemp.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You have some unsaved information. If you close the window the data will be lost.") & vbCrLf & _
                                                          Language.getMessage(mstrModuleName, 7, "Do you wish to continue closing this window?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Me.Close()
                    Else
                        Exit Sub
                    End If
                Else
                    Me.Close()
                End If
            Else
                Me.Close()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "ecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub
            End If

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Exit Sub
            End If
            'Hemant (03 Feb 2023) -- End

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            'Dim dTemp() As DataRow = mdtReport.Select("reporttoemployeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' AND AUD <> 'D'")
            'If dTemp.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry you cannot add same employee again to the list."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            Dim dTemp() As DataRow = mdtReport.Select("reporttoemployeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' AND effectivedate = '" & Format(dtpEffectiveDate.Value.Date, "yyyyMMdd") & "' AND AUD <> 'D'")
            If dTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry you cannot add same employee again for same effective date to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Hemant (03 Feb 2023) -- End
            

            Dim dRow As DataRow
            dRow = mdtReport.NewRow

            Dim dtTemp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")

            With dRow
                .Item("reporttounkid") = -1
                .Item("employeeunkid") = mintEmployeeUnkid
                .Item("reporttoemployeeunkid") = CInt(cboEmployee.SelectedValue)

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                .Item("EmpDisplayName") = mstrEmpDisplayName
                'Pinkal (18-Aug-2018) -- End

                .Item("ishierarchy") = False
                .Item("userunkid") = User._Object._Userunkid
                .Item("isvoid") = False
                .Item("voiduserunkid") = -1
                .Item("voiddatetime") = DBNull.Value
                .Item("voidreason") = ""


                'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                .Item("IsEmpApproved") = mblnIsEmpApproved
                'Pinkal (04-Apr-2020) -- End

                'Hemant (03 Feb 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                .Item("effectivedate") = Format(dtpEffectiveDate.Value.Date, "yyyyMMdd")
                'Hemant (03 Feb 2023) -- End

                If dtTemp.Length > 0 Then
                    .Item("ecode") = dtTemp(0)("ecode")
                    .Item("ename") = dtTemp(0)("ename")
                    .Item("job_group") = dtTemp(0)("job_group")
                    .Item("job_name") = dtTemp(0)("job_name")
                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    .Item("ReportingToEmpDisplayName") = dtTemp(0)("ReportingToEmpDisplayName")
                    'Pinkal (18-Aug-2018) -- End


                    'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                    .Item("IsReportingToEmpApproved") = dtTemp(0)("IsReportingToEmpApproved")
                    'Pinkal (04-Apr-2020) -- End

                End If
                .Item("AUD") = "A"
                .Item("GUID") = Guid.NewGuid.ToString
            End With
            mdtReport.Rows.Add(dRow)
            Call Fill_Data()
            cboEmployee.SelectedValue = 0
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvReportingEmp.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvReportingEmp.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtReport.Select("GUID = '" & lvReportingEmp.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtReport.Select("reporttounkid = '" & CInt(lvReportingEmp.SelectedItems(0).Tag) & "'")
                End If

                'Hemant (03 Feb 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                Dim dtDate As Date = CDate(lvReportingEmp.SelectedItems(0).SubItems(colhEffectiveDate.Index).Text)
                If CDate(lvReportingEmp.SelectedItems(0).SubItems(colhEffectiveDate.Index).Text) < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot delete this entry."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Hemant (03 Feb 2023) -- End


                If drTemp.Length > 0 Then
                    If CInt(drTemp(0)("reporttounkid")) > 0 Then
                        Dim frm As New frmReasonSelection

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If

                    Else
                        drTemp(0).Item("AUD") = "D"
                    End If
                End If
            End If
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Private Sub btnViewreportingList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewreportingList.Click
        Try

            If IsNothing(mdtOwnReportTolist) = False AndAlso mdtOwnReportTolist.Rows.Count > 0 Then
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, "", frmReasonSelection.endisplayType.ReportingTolist, mdtOwnReportTolist)
            End If

            btnViewreportingList.Visible = False
            mdtOwnReportTolist = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnViewreportingList_Click", mstrModuleName)
        End Try
    End Sub
    'Gajanan [24-OCT-2019] -- End


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region " Control's Events "

    Private Sub mnuSetReportingTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetReportingTo.Click
        Try
            If lvReportingEmp.SelectedItems.Count > 0 Then


                'Hemant (03 Feb 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                'Dim dTemp() As DataRow = mdtReport.Select("ishierarchy = true AND AUD <> 'D'")
                Dim dTemp() As DataRow = mdtReport.Select("ishierarchy = true AND effectivedate = '" & Format(CDate(lvReportingEmp.SelectedItems(0).SubItems(colhEffectiveDate.Index).Text), "yyyyMMdd") & "' AND AUD <> 'D'")
                'Hemant (03 Feb 2023) -- End

                If dTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot set more than one employee as default reporting to."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim drTemp As DataRow()
                If CInt(lvReportingEmp.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtReport.Select("GUID = '" & lvReportingEmp.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtReport.Select("reporttounkid = '" & CInt(lvReportingEmp.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    drTemp(0)("ishierarchy") = True
                    If IsDBNull(drTemp(0)("AUD")) Or CStr(drTemp(0)("AUD")).ToString.Trim = "" Then
                        drTemp(0)("AUD") = "U"
                    End If
                    mdtReport.AcceptChanges()
                    lvReportingEmp.SelectedItems(0).BackColor = Color.Gray
                    lvReportingEmp.SelectedItems(0).ForeColor = Color.White
                    lvReportingEmp.SelectedItems(0).Font = New Font(Me.Font, FontStyle.Bold)
                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    lvReportingEmp_SelectedIndexChanged(New Object(), New EventArgs())
                    lvReportingEmp.Focus()
                    'Pinkal (18-Aug-2018) -- End
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuSetReportingTo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuRemoveReportingTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRemoveReportingTo.Click
        Try
            If lvReportingEmp.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvReportingEmp.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtReport.Select("GUID = '" & lvReportingEmp.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtReport.Select("reporttounkid = '" & CInt(lvReportingEmp.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    drTemp(0)("ishierarchy") = False
                    If IsDBNull(drTemp(0)("AUD")) Or CStr(drTemp(0)("AUD")).ToString.Trim = "" Then
                        drTemp(0)("AUD") = "U"
                    End If
                    mdtReport.AcceptChanges()
                    lvReportingEmp.SelectedItems(0).ForeColor = Color.Black

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    lvReportingEmp.SelectedItems(0).BackColor = Color.White
                    lvReportingEmp.SelectedItems(0).Font = New Font(Me.Font, FontStyle.Regular)
                    lvReportingEmp_SelectedIndexChanged(New Object(), New EventArgs())
                    'Pinkal (18-Aug-2018) -- End

                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuRemoveReportingTo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvReportingEmp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvReportingEmp.SelectedIndexChanged
        Try
            If lvReportingEmp.SelectedItems.Count > 0 Then
                If lvReportingEmp.SelectedItems(0).BackColor = Color.Gray Then
                    mnuSetReportingTo.Enabled = False : mnuRemoveReportingTo.Enabled = True
                Else
                    mnuSetReportingTo.Enabled = True : mnuRemoveReportingTo.Enabled = False
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lvReportingEmp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveInfo.GradientBackColor = GUI._ButttonBackColor
			Me.btnSaveInfo.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnViewreportingList.GradientBackColor = GUI._ButttonBackColor
			Me.btnViewreportingList.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lnReportingInfo.Text = Language._Object.getCaption(Me.lnReportingInfo.Name, Me.lnReportingInfo.Text)
			Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSaveInfo.Text = Language._Object.getCaption(Me.btnSaveInfo.Name, Me.btnSaveInfo.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuSetReportingTo.Text = Language._Object.getCaption(Me.mnuSetReportingTo.Name, Me.mnuSetReportingTo.Text)
			Me.mnuRemoveReportingTo.Text = Language._Object.getCaption(Me.mnuRemoveReportingTo.Name, Me.mnuRemoveReportingTo.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.colhECode.Text = Language._Object.getCaption(CStr(Me.colhECode.Tag), Me.colhECode.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhJobGrp.Text = Language._Object.getCaption(CStr(Me.colhJobGrp.Tag), Me.colhJobGrp.Text)
			Me.colhJobName.Text = Language._Object.getCaption(CStr(Me.colhJobName.Tag), Me.colhJobName.Text)
			Me.btnViewreportingList.Text = Language._Object.getCaption(Me.btnViewreportingList.Name, Me.btnViewreportingList.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
			
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry you cannot add same employee again to the list.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Please set at least one employee as default reporting to.")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot set more than one employee as default reporting to.")
			Language.setMessage(mstrModuleName, 4, "Information successfully saved.")
			Language.setMessage(mstrModuleName, 5, "You have some unsaved information. If you close the window the data will be lost.")
			Language.setMessage(mstrModuleName, 6, "Employee is mandatory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 7, "Do you wish to continue closing this window?")
			Language.setMessage(mstrModuleName, 8, "Sorry, you can not set this employee as default reporting to, Reason: Selected reporting to employee is already reporting directly/indirectly to above selected employee.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Language.setMessage(mstrModuleName, 10, "Sorry you cannot add same employee again for same effective date to the list.")
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class