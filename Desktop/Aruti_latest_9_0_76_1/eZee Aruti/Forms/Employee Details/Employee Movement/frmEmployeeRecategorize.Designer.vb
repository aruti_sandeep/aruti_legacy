﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeRecategorize
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeRecategorize))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbHeadInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboStation = New System.Windows.Forms.ComboBox
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.lblBranch = New System.Windows.Forms.Label
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.objbtnSearchGrade = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchJobGrp = New eZee.Common.eZeeGradientButton
        Me.lblAllocationGrade = New System.Windows.Forms.Label
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.cboAllocationGrade = New System.Windows.Forms.ComboBox
        Me.objbtnSearchBranch = New eZee.Common.eZeeGradientButton
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDeptGrp = New eZee.Common.eZeeGradientButton
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.objbtnSearchDepartment = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchClassGrp = New eZee.Common.eZeeGradientButton
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.objbtnSearchClass = New eZee.Common.eZeeGradientButton
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchUnits = New eZee.Common.eZeeGradientButton
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.objbtnSearchTeam = New eZee.Common.eZeeGradientButton
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchUnitGrp = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchSecGroup = New eZee.Common.eZeeGradientButton
        Me.lblUnits = New System.Windows.Forms.Label
        Me.objbtnSearchSection = New eZee.Common.eZeeGradientButton
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.cboChangeReason = New System.Windows.Forms.ComboBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhViewPending = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhOperationType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChangeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrecategorizeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFromEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrehiretranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhtranguid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOperationTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lblAppointmentdate = New System.Windows.Forms.Label
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.lblChangeReason = New System.Windows.Forms.Label
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.objSearchReason = New eZee.Common.eZeeGradientButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblPendingData = New System.Windows.Forms.Label
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlGrade = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1.SuspendLayout()
        Me.gbHeadInformation.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlData.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlGrade.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbHeadInformation)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(731, 586)
        Me.Panel1.TabIndex = 1
        '
        'gbHeadInformation
        '
        Me.gbHeadInformation.BorderColor = System.Drawing.Color.Black
        Me.gbHeadInformation.Checked = False
        Me.gbHeadInformation.CollapseAllExceptThis = False
        Me.gbHeadInformation.CollapsedHoverImage = Nothing
        Me.gbHeadInformation.CollapsedNormalImage = Nothing
        Me.gbHeadInformation.CollapsedPressedImage = Nothing
        Me.gbHeadInformation.CollapseOnLoad = False
        Me.gbHeadInformation.Controls.Add(Me.FlowLayoutPanel1)
        Me.gbHeadInformation.Controls.Add(Me.lnkAllocation)
        Me.gbHeadInformation.Controls.Add(Me.objelLine1)
        Me.gbHeadInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbHeadInformation.Controls.Add(Me.dtpEffectiveDate)
        Me.gbHeadInformation.Controls.Add(Me.cboEmployee)
        Me.gbHeadInformation.Controls.Add(Me.lblEffectiveDate)
        Me.gbHeadInformation.Controls.Add(Me.lblEmployee)
        Me.gbHeadInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbHeadInformation.ExpandedHoverImage = Nothing
        Me.gbHeadInformation.ExpandedNormalImage = Nothing
        Me.gbHeadInformation.ExpandedPressedImage = Nothing
        Me.gbHeadInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHeadInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHeadInformation.HeaderHeight = 25
        Me.gbHeadInformation.HeaderMessage = ""
        Me.gbHeadInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbHeadInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHeadInformation.HeightOnCollapse = 0
        Me.gbHeadInformation.LeftTextSpace = 0
        Me.gbHeadInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbHeadInformation.Name = "gbHeadInformation"
        Me.gbHeadInformation.OpenHeight = 300
        Me.gbHeadInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHeadInformation.ShowBorder = True
        Me.gbHeadInformation.ShowCheckBox = False
        Me.gbHeadInformation.ShowCollapseButton = False
        Me.gbHeadInformation.ShowDefaultBorderColor = True
        Me.gbHeadInformation.ShowDownButton = False
        Me.gbHeadInformation.ShowHeader = True
        Me.gbHeadInformation.Size = New System.Drawing.Size(731, 536)
        Me.gbHeadInformation.TabIndex = 0
        Me.gbHeadInformation.Temp = 0
        Me.gbHeadInformation.Text = "Re-Categorize Information"
        Me.gbHeadInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.gbFilterCriteria)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(4, 64)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(723, 466)
        Me.FlowLayoutPanel1.TabIndex = 237
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = CType(resources.GetObject("gbFilterCriteria.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbFilterCriteria.CollapsedNormalImage = CType(resources.GetObject("gbFilterCriteria.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbFilterCriteria.CollapsedPressedImage = CType(resources.GetObject("gbFilterCriteria.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbFilterCriteria.CollapseOnLoad = True
        Me.gbFilterCriteria.Controls.Add(Me.cboStation)
        Me.gbFilterCriteria.Controls.Add(Me.cboTeams)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnitGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchGrade)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchJobGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocationGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocationGrade)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchDeptGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartmentGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchClassGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartmentGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblClass)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnitGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchUnits)
        Me.gbFilterCriteria.Controls.Add(Me.lblSectionGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTeam)
        Me.gbFilterCriteria.Controls.Add(Me.cboSectionGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchUnitGrp)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchSecGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnits)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchSection)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnits)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.lblTeam)
        Me.gbFilterCriteria.Controls.Add(Me.cboSections)
        Me.gbFilterCriteria.ExpandedHoverImage = CType(resources.GetObject("gbFilterCriteria.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbFilterCriteria.ExpandedNormalImage = CType(resources.GetObject("gbFilterCriteria.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbFilterCriteria.ExpandedPressedImage = CType(resources.GetObject("gbFilterCriteria.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 202
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = True
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(718, 201)
        Me.gbFilterCriteria.TabIndex = 236
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Job"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboStation
        '
        Me.cboStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStation.DropDownWidth = 260
        Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStation.FormattingEnabled = True
        Me.cboStation.Location = New System.Drawing.Point(102, 35)
        Me.cboStation.Name = "cboStation"
        Me.cboStation.Size = New System.Drawing.Size(211, 21)
        Me.cboStation.TabIndex = 199
        '
        'cboTeams
        '
        Me.cboTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeams.DropDownWidth = 260
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(455, 89)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(211, 21)
        Me.cboTeams.TabIndex = 221
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(360, 37)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(87, 15)
        Me.lblUnitGroup.TabIndex = 214
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(10, 38)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(86, 15)
        Me.lblBranch.TabIndex = 198
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(10, 173)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(86, 15)
        Me.lblJobGroup.TabIndex = 192
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchGrade
        '
        Me.objbtnSearchGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGrade.BorderSelected = False
        Me.objbtnSearchGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGrade.Location = New System.Drawing.Point(676, 170)
        Me.objbtnSearchGrade.Name = "objbtnSearchGrade"
        Me.objbtnSearchGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGrade.TabIndex = 231
        '
        'objbtnSearchJobGrp
        '
        Me.objbtnSearchJobGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobGrp.BorderSelected = False
        Me.objbtnSearchJobGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobGrp.Location = New System.Drawing.Point(319, 170)
        Me.objbtnSearchJobGrp.Name = "objbtnSearchJobGrp"
        Me.objbtnSearchJobGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobGrp.TabIndex = 194
        '
        'lblAllocationGrade
        '
        Me.lblAllocationGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocationGrade.Location = New System.Drawing.Point(360, 173)
        Me.lblAllocationGrade.Name = "lblAllocationGrade"
        Me.lblAllocationGrade.Size = New System.Drawing.Size(87, 15)
        Me.lblAllocationGrade.TabIndex = 229
        Me.lblAllocationGrade.Text = "Grade"
        Me.lblAllocationGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.DropDownWidth = 400
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(102, 170)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(211, 21)
        Me.cboJobGroup.TabIndex = 193
        '
        'cboAllocationGrade
        '
        Me.cboAllocationGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocationGrade.DropDownWidth = 260
        Me.cboAllocationGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocationGrade.FormattingEnabled = True
        Me.cboAllocationGrade.Location = New System.Drawing.Point(455, 170)
        Me.cboAllocationGrade.Name = "cboAllocationGrade"
        Me.cboAllocationGrade.Size = New System.Drawing.Size(211, 21)
        Me.cboAllocationGrade.TabIndex = 230
        '
        'objbtnSearchBranch
        '
        Me.objbtnSearchBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBranch.BorderSelected = False
        Me.objbtnSearchBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBranch.Location = New System.Drawing.Point(319, 35)
        Me.objbtnSearchBranch.Name = "objbtnSearchBranch"
        Me.objbtnSearchBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBranch.TabIndex = 200
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.DropDownWidth = 260
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(455, 116)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(211, 21)
        Me.cboClassGroup.TabIndex = 224
        '
        'objbtnSearchDeptGrp
        '
        Me.objbtnSearchDeptGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeptGrp.BorderSelected = False
        Me.objbtnSearchDeptGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeptGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeptGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeptGrp.Location = New System.Drawing.Point(319, 62)
        Me.objbtnSearchDeptGrp.Name = "objbtnSearchDeptGrp"
        Me.objbtnSearchDeptGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeptGrp.TabIndex = 203
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGrp.DropDownWidth = 260
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(102, 62)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(211, 21)
        Me.cboDepartmentGrp.TabIndex = 202
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(360, 119)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(87, 15)
        Me.lblClassGroup.TabIndex = 223
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchDepartment
        '
        Me.objbtnSearchDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDepartment.BorderSelected = False
        Me.objbtnSearchDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDepartment.Location = New System.Drawing.Point(319, 89)
        Me.objbtnSearchDepartment.Name = "objbtnSearchDepartment"
        Me.objbtnSearchDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDepartment.TabIndex = 206
        '
        'objbtnSearchClassGrp
        '
        Me.objbtnSearchClassGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClassGrp.BorderSelected = False
        Me.objbtnSearchClassGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClassGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClassGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClassGrp.Location = New System.Drawing.Point(676, 116)
        Me.objbtnSearchClassGrp.Name = "objbtnSearchClassGrp"
        Me.objbtnSearchClassGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClassGrp.TabIndex = 225
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(10, 65)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(86, 15)
        Me.lblDepartmentGroup.TabIndex = 201
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchClass
        '
        Me.objbtnSearchClass.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClass.BorderSelected = False
        Me.objbtnSearchClass.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClass.Location = New System.Drawing.Point(676, 143)
        Me.objbtnSearchClass.Name = "objbtnSearchClass"
        Me.objbtnSearchClass.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClass.TabIndex = 228
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 260
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(102, 89)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(211, 21)
        Me.cboDepartment.TabIndex = 205
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(360, 146)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(87, 15)
        Me.lblClass.TabIndex = 226
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(10, 92)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(86, 15)
        Me.lblDepartment.TabIndex = 204
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.DropDownWidth = 260
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(455, 143)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(211, 21)
        Me.cboClass.TabIndex = 227
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.DropDownWidth = 260
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(455, 35)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(211, 21)
        Me.cboUnitGroup.TabIndex = 215
        '
        'objbtnSearchUnits
        '
        Me.objbtnSearchUnits.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnits.BorderSelected = False
        Me.objbtnSearchUnits.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnits.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnits.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnits.Location = New System.Drawing.Point(676, 62)
        Me.objbtnSearchUnits.Name = "objbtnSearchUnits"
        Me.objbtnSearchUnits.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnits.TabIndex = 219
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(10, 119)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(86, 15)
        Me.lblSectionGroup.TabIndex = 208
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTeam
        '
        Me.objbtnSearchTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTeam.BorderSelected = False
        Me.objbtnSearchTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTeam.Location = New System.Drawing.Point(676, 89)
        Me.objbtnSearchTeam.Name = "objbtnSearchTeam"
        Me.objbtnSearchTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTeam.TabIndex = 222
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.DropDownWidth = 260
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(102, 116)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(211, 21)
        Me.cboSectionGroup.TabIndex = 209
        '
        'objbtnSearchUnitGrp
        '
        Me.objbtnSearchUnitGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnitGrp.BorderSelected = False
        Me.objbtnSearchUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnitGrp.Location = New System.Drawing.Point(676, 35)
        Me.objbtnSearchUnitGrp.Name = "objbtnSearchUnitGrp"
        Me.objbtnSearchUnitGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnitGrp.TabIndex = 216
        '
        'objbtnSearchSecGroup
        '
        Me.objbtnSearchSecGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSecGroup.BorderSelected = False
        Me.objbtnSearchSecGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSecGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSecGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSecGroup.Location = New System.Drawing.Point(319, 116)
        Me.objbtnSearchSecGroup.Name = "objbtnSearchSecGroup"
        Me.objbtnSearchSecGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSecGroup.TabIndex = 210
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(360, 65)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(87, 15)
        Me.lblUnits.TabIndex = 217
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSection
        '
        Me.objbtnSearchSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSection.BorderSelected = False
        Me.objbtnSearchSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSection.Location = New System.Drawing.Point(319, 143)
        Me.objbtnSearchSection.Name = "objbtnSearchSection"
        Me.objbtnSearchSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSection.TabIndex = 213
        '
        'cboUnits
        '
        Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnits.DropDownWidth = 260
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.Location = New System.Drawing.Point(455, 62)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(211, 21)
        Me.cboUnits.TabIndex = 218
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(10, 146)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(86, 15)
        Me.lblSection.TabIndex = 211
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(360, 92)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(87, 15)
        Me.lblTeam.TabIndex = 220
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSections
        '
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.DropDownWidth = 260
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(102, 143)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(211, 21)
        Me.cboSections.TabIndex = 212
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.EZeeLine1)
        Me.Panel2.Controls.Add(Me.cboJob)
        Me.Panel2.Controls.Add(Me.lblJob)
        Me.Panel2.Controls.Add(Me.objbtnSearchJob)
        Me.Panel2.Controls.Add(Me.cboChangeReason)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Controls.Add(Me.pnl1)
        Me.Panel2.Controls.Add(Me.pnlData)
        Me.Panel2.Controls.Add(Me.lblChangeReason)
        Me.Panel2.Controls.Add(Me.objbtnAddReason)
        Me.Panel2.Controls.Add(Me.objSearchReason)
        Me.Panel2.Location = New System.Drawing.Point(3, 210)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(717, 253)
        Me.Panel2.TabIndex = 238
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(6, 1)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(707, 8)
        Me.EZeeLine1.TabIndex = 180
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 400
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(105, 12)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(582, 21)
        Me.cboJob.TabIndex = 10
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(13, 15)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(84, 15)
        Me.lblJob.TabIndex = 9
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(691, 12)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 11
        '
        'cboChangeReason
        '
        Me.cboChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeReason.DropDownWidth = 400
        Me.cboChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeReason.FormattingEnabled = True
        Me.cboChangeReason.Location = New System.Drawing.Point(105, 39)
        Me.cboChangeReason.Name = "cboChangeReason"
        Me.cboChangeReason.Size = New System.Drawing.Size(192, 21)
        Me.cboChangeReason.TabIndex = 176
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(590, 37)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(110, 30)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.dgvHistory)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(8, 73)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(705, 172)
        Me.pnl1.TabIndex = 173
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhViewPending, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhOperationType, Me.dgcolhChangeDate, Me.dgcolhJobGroup, Me.dgcolhJob, Me.dgcolhReason, Me.objdgcolhrecategorizeunkid, Me.objdgcolhFromEmp, Me.objdgcolhAppointdate, Me.objdgcolhrehiretranunkid, Me.objdgcolhtranguid, Me.objdgcolhOperationTypeId})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(705, 172)
        Me.dgvHistory.TabIndex = 0
        '
        'objdgcolhViewPending
        '
        Me.objdgcolhViewPending.Frozen = True
        Me.objdgcolhViewPending.HeaderText = ""
        Me.objdgcolhViewPending.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.objdgcolhViewPending.Name = "objdgcolhViewPending"
        Me.objdgcolhViewPending.ReadOnly = True
        Me.objdgcolhViewPending.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhViewPending.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhOperationType
        '
        Me.objdgcolhOperationType.Frozen = True
        Me.objdgcolhOperationType.HeaderText = "Operation Type"
        Me.objdgcolhOperationType.Name = "objdgcolhOperationType"
        Me.objdgcolhOperationType.ReadOnly = True
        Me.objdgcolhOperationType.Visible = False
        '
        'dgcolhChangeDate
        '
        Me.dgcolhChangeDate.Frozen = True
        Me.dgcolhChangeDate.HeaderText = "Effective Date"
        Me.dgcolhChangeDate.Name = "dgcolhChangeDate"
        Me.dgcolhChangeDate.ReadOnly = True
        Me.dgcolhChangeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJobGroup
        '
        Me.dgcolhJobGroup.Frozen = True
        Me.dgcolhJobGroup.HeaderText = "Job Group"
        Me.dgcolhJobGroup.Name = "dgcolhJobGroup"
        Me.dgcolhJobGroup.ReadOnly = True
        Me.dgcolhJobGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhJobGroup.Visible = False
        Me.dgcolhJobGroup.Width = 200
        '
        'dgcolhJob
        '
        Me.dgcolhJob.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhJob.Frozen = True
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhJob.Width = 304
        '
        'dgcolhReason
        '
        Me.dgcolhReason.Frozen = True
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhReason.Width = 250
        '
        'objdgcolhrecategorizeunkid
        '
        Me.objdgcolhrecategorizeunkid.Frozen = True
        Me.objdgcolhrecategorizeunkid.HeaderText = "objdgcolhrecategorizeunkid"
        Me.objdgcolhrecategorizeunkid.Name = "objdgcolhrecategorizeunkid"
        Me.objdgcolhrecategorizeunkid.ReadOnly = True
        Me.objdgcolhrecategorizeunkid.Visible = False
        '
        'objdgcolhFromEmp
        '
        Me.objdgcolhFromEmp.Frozen = True
        Me.objdgcolhFromEmp.HeaderText = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.Name = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.ReadOnly = True
        Me.objdgcolhFromEmp.Visible = False
        '
        'objdgcolhAppointdate
        '
        Me.objdgcolhAppointdate.Frozen = True
        Me.objdgcolhAppointdate.HeaderText = "objdgcolhAppointdate"
        Me.objdgcolhAppointdate.Name = "objdgcolhAppointdate"
        Me.objdgcolhAppointdate.ReadOnly = True
        Me.objdgcolhAppointdate.Visible = False
        '
        'objdgcolhrehiretranunkid
        '
        Me.objdgcolhrehiretranunkid.Frozen = True
        Me.objdgcolhrehiretranunkid.HeaderText = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.Name = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.ReadOnly = True
        Me.objdgcolhrehiretranunkid.Visible = False
        '
        'objdgcolhtranguid
        '
        Me.objdgcolhtranguid.Frozen = True
        Me.objdgcolhtranguid.HeaderText = "objdgcolhtranguid"
        Me.objdgcolhtranguid.Name = "objdgcolhtranguid"
        Me.objdgcolhtranguid.ReadOnly = True
        Me.objdgcolhtranguid.Visible = False
        '
        'objdgcolhOperationTypeId
        '
        Me.objdgcolhOperationTypeId.HeaderText = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.Name = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.ReadOnly = True
        Me.objdgcolhOperationTypeId.Visible = False
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.lblAppointmentdate)
        Me.pnlData.Controls.Add(Me.txtDate)
        Me.pnlData.Location = New System.Drawing.Point(361, 37)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(223, 25)
        Me.pnlData.TabIndex = 190
        '
        'lblAppointmentdate
        '
        Me.lblAppointmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentdate.Location = New System.Drawing.Point(3, 5)
        Me.lblAppointmentdate.Name = "lblAppointmentdate"
        Me.lblAppointmentdate.Size = New System.Drawing.Size(79, 15)
        Me.lblAppointmentdate.TabIndex = 183
        Me.lblAppointmentdate.Text = "Appoint. Date"
        Me.lblAppointmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.Color.White
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(95, 2)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(125, 21)
        Me.txtDate.TabIndex = 184
        '
        'lblChangeReason
        '
        Me.lblChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChangeReason.Location = New System.Drawing.Point(13, 42)
        Me.lblChangeReason.Name = "lblChangeReason"
        Me.lblChangeReason.Size = New System.Drawing.Size(84, 15)
        Me.lblChangeReason.TabIndex = 175
        Me.lblChangeReason.Text = "Change Reason"
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(303, 39)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 189
        '
        'objSearchReason
        '
        Me.objSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchReason.BorderSelected = False
        Me.objSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchReason.Location = New System.Drawing.Point(331, 39)
        Me.objSearchReason.Name = "objSearchReason"
        Me.objSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objSearchReason.TabIndex = 177
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(651, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(76, 13)
        Me.lnkAllocation.TabIndex = 233
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(12, 30)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(707, 0)
        Me.objelLine1.TabIndex = 5
        Me.objelLine1.Text = "Filter Job"
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objelLine1.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(698, 38)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(109, 37)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(114, 21)
        Me.dtpEffectiveDate.TabIndex = 1
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(307, 37)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(385, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 40)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(91, 15)
        Me.lblEffectiveDate.TabIndex = 0
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(229, 40)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(72, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblPendingData)
        Me.objFooter.Controls.Add(Me.objlblCaption)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.pnlGrade)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 536)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(731, 50)
        Me.objFooter.TabIndex = 1
        '
        'lblPendingData
        '
        Me.lblPendingData.BackColor = System.Drawing.Color.PowderBlue
        Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendingData.ForeColor = System.Drawing.Color.Black
        Me.lblPendingData.Location = New System.Drawing.Point(430, 18)
        Me.lblPendingData.Name = "lblPendingData"
        Me.lblPendingData.Size = New System.Drawing.Size(155, 17)
        Me.lblPendingData.TabIndex = 180
        Me.lblPendingData.Text = "Pending Approval"
        Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.Color.Orange
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Black
        Me.objlblCaption.Location = New System.Drawing.Point(12, 18)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(155, 17)
        Me.objlblCaption.TabIndex = 8
        Me.objlblCaption.Text = "Employee Rehired"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(626, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlGrade
        '
        Me.pnlGrade.Controls.Add(Me.Label1)
        Me.pnlGrade.Controls.Add(Me.cboGradeLevel)
        Me.pnlGrade.Controls.Add(Me.lblGrade)
        Me.pnlGrade.Controls.Add(Me.cboGrade)
        Me.pnlGrade.Location = New System.Drawing.Point(595, 18)
        Me.pnlGrade.Name = "pnlGrade"
        Me.pnlGrade.Size = New System.Drawing.Size(25, 21)
        Me.pnlGrade.TabIndex = 179
        Me.pnlGrade.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 15)
        Me.Label1.TabIndex = 181
        Me.Label1.Text = "Grade Level"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.Visible = False
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboGradeLevel.DropDownWidth = 200
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(91, -1)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(282, 21)
        Me.cboGradeLevel.TabIndex = 182
        Me.cboGradeLevel.Visible = False
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(22, 32)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(79, 15)
        Me.lblGrade.TabIndex = 180
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboGrade.DropDownWidth = 200
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(107, 30)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(105, 21)
        Me.cboGrade.TabIndex = 180
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Operation Type"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Effective Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Job Group"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 304
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "Reason"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.Frozen = True
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhrecategorizeunkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.Frozen = True
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhFromEmp"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.Frozen = True
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhAppointdate"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.Frozen = True
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhrehiretranunkid"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.Frozen = True
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhtranguid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhOperationTypeId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'frmEmployeeRecategorize
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 586)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeRecategorize"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Re-Categorize Employee"
        Me.Panel1.ResumeLayout(False)
        Me.gbHeadInformation.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.pnl1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.pnlGrade.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbHeadInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents objSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents cboChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblChangeReason As System.Windows.Forms.Label
    Friend WithEvents pnlGrade As System.Windows.Forms.Panel
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblAppointmentdate As System.Windows.Forms.Label
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchJobGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDeptGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSection As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchSecGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchClassGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchClass As eZee.Common.eZeeGradientButton
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchUnits As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents lblAllocationGrade As System.Windows.Forms.Label
    Friend WithEvents cboAllocationGrade As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
    Friend WithEvents objdgcolhViewPending As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhOperationType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChangeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrecategorizeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFromEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrehiretranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhtranguid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOperationTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents Panel2 As System.Windows.Forms.Panel

End Class
