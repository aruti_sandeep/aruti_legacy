﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeOtherDetails

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeOtherDetails"
    Private objECCT As clsemployee_cctranhead_tran
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mstrEmployeeCode As String = String.Empty
    Private mblnIsCostCenter As Boolean = False
    Private xMasterType As Integer = 0
    Private mdtAppointmentDate As Date = Nothing

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private objACCT As clsCCTranhead_Approval_Tran
    'S.SANDEEP [20-JUN-2018] -- END

    'S.SANDEEP [09-AUG-2018] -- START
    Private imgInfo As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.information)
    'S.SANDEEP [09-AUG-2018] -- END

    'S.SANDEEP [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'S.SANDEEP [15-NOV-2018] -- END



    'S.SANDEEP |17-JAN-2019| -- START
    Private imgView As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.doc_view)
    'S.SANDEEP |17-JAN-2019| -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    Private mintEmployeeID As Integer = -1
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _IsCostCenter() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCostCenter = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objCC As New clscostcenter_master
        Dim objTrnHead As New clsTransactionHead
        Dim dsCombos As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, True)
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB


            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                        User._Object._Userunkid, _
            '                                        FinancialYear._Object._YearUnkid, _
            '                                        Company._Object._Companyunkid, _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        ConfigParameter._Object._UserAccessModeSetting, _
            '                                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''S.SANDEEP [04 JUN 2015] -- END

            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Emp")
            '    .SelectedValue = 0
            '    .Text = ""
            'End With

            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End

            dsCombos = objCMaster.getComboList(CType(xMasterType, clsCommon_Master.enCommonMaster), True, "List")
            With cboChangeReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            If mblnIsCostCenter = True Then
                dsCombos = objCC.getComboList("CostCenter", True)
                With cboCCTrnHead
                    .ValueMember = "costcenterunkid"
                    .DisplayMember = "costcentername"
                    .DataSource = dsCombos.Tables("CostCenter")
                    .SelectedValue = 0
                End With
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsCombos = objTrnHead.getComboList("TranHead", True, 0, 0, enTypeOf.Salary)
                dsCombos = objTrnHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, enTypeOf.Salary)
                'Sohail (21 Aug 2015) -- End
                With cboCCTrnHead
                    .ValueMember = "tranheadunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("TranHead")
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose() : objEmployee = Nothing : objCMaster = Nothing
            objCC = Nothing : objTrnHead = Nothing
        End Try
    End Sub

    Private Sub Set_Form_Controls()
        Try
            objlblCaption.Text = "" : objgbInformation.Text = "" : Me.Text = ""
            If mblnIsCostCenter = True Then
                Me.Text = Language.getMessage(mstrModuleName, 1, "Cost Center Information")
                objlblCaption.Text = Language.getMessage(mstrModuleName, 2, "Cost Center")
                objgbInformation.Text = Language.getMessage(mstrModuleName, 1, "Cost Center Information")
                xMasterType = clsCommon_Master.enCommonMaster.COST_CENTER
            Else
                Me.Text = Language.getMessage(mstrModuleName, 3, "Transaction Head Information")
                objlblCaption.Text = Language.getMessage(mstrModuleName, 4, "Transaction Head")
                objgbInformation.Text = Language.getMessage(mstrModuleName, 3, "Transaction Head Information")
            End If
            objdgcolhValue.HeaderText = objlblCaption.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            dsData = objECCT.GetList("List", mblnIsCostCenter, CInt(cboEmployee.SelectedValue))

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)

            '
            'S.SANDEEP |17-JAN-2019| -- START
            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.Int32)
                .ColumnName = "operationtypeid"
                .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
            End With
            dsData.Tables(0).Columns.Add(dcol)

            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "OperationType"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)
            'S.SANDEEP |17-JAN-2019| -- END


            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim dsPending As New DataSet
                dsPending = objACCT.GetList("List", mblnIsCostCenter, CInt(cboEmployee.SelectedValue))
                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsData.Tables(0).ImportRow(row)
                    Next
                End If
            End If
            'S.SANDEEP [20-JUN-2018] -- END


            dgvHistory.AutoGenerateColumns = False

            dgcolhChangeDate.DataPropertyName = "EffDate"
            objdgcolhValue.DataPropertyName = "DispValue"
            dgcolhReason.DataPropertyName = "CReason"
            objdgcolhFromEmp.DataPropertyName = "isfromemployee"
            objdgcolhdetailtranunkid.DataPropertyName = "cctranheadunkid"
            objdgcolhAppointDate.DataPropertyName = "adate"
            'S.SANDEEP [14 APR 2015] -- START
            objdgcolhrehiretranunkid.DataPropertyName = "rehiretranunkid"
            'ISSUE/ENHANCEMENT : {Ref#244}
            objdgcolhtranguid.DataPropertyName = "tranguid"
            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            objdgcolhOperationTypeId.DataPropertyName = "operationtypeid"
            objdgcolhOperationType.DataPropertyName = "OperationType"
            'S.SANDEEP |17-JAN-2019| -- END
            dgvHistory.DataSource = dsData.Tables(0)

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Details()
        Try
            Dim dsList As New DataSet
            dsList = objECCT.Get_Current_CostCenter_TranHeads(Now.Date, mblnIsCostCenter, CInt(cboEmployee.SelectedValue))
            If dsList.Tables(0).Rows.Count > 0 Then
                cboCCTrnHead.SelectedValue = dsList.Tables(0).Rows(0).Item("cctranheadvalueid")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Details", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidDetails() As Boolean
        Try

            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboCCTrnHead.SelectedValue) <= 0 Then
                If mblnIsCostCenter = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Cost center is mandatory information. Please select cost center to continue."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Transaction head is mandatory information. Please select transaction head to continue."), enMsgBoxStyle.Information)
                End If
                cboCCTrnHead.Focus()
                Return False
            End If

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue."), enMsgBoxStyle.Information)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtpEffectiveDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If objECCT.isExist(dtpEffectiveDate.Value.Date, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, CInt(cboEmployee.SelectedValue)) Then
                If mblnIsCostCenter = False Then
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), enMsgBoxStyle.Information)
                End If
                Return False
            End If
            'Gajanan [11-Dec-2019] -- End




            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim objPMovement As New clsEmployeeMovmentApproval
                Dim strMsg As String = String.Empty
                strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.COSTCENTER, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1201, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, CInt(cboEmployee.SelectedValue))
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing

                'S.SANDEEP |16-JAN-2019| -- START
                If objECCT.isExist(dtpEffectiveDate.Value.Date, -1, mblnIsCostCenter, CInt(cboEmployee.SelectedValue), mintTransactionId) Then 'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    If mblnIsCostCenter = False Then
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    End If
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objECCT.Get_Current_CostCenter_TranHeads(dtpEffectiveDate.Value.Date, mblnIsCostCenter, CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid")) = CInt(cboCCTrnHead.SelectedValue) Then
                        If mblnIsCostCenter = False Then
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Else
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        End If
                        Return False
                    End If
                End If

                If objECCT.isExist(dtpEffectiveDate.Value.Date, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, CInt(cboEmployee.SelectedValue), mintTransactionId) Then  'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    If mblnIsCostCenter = False Then
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    End If
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END


            End If
            'S.SANDEEP [20-JUN-2018] -- END
            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidDetails", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objECCT._Cctranheadvalueid = CInt(cboCCTrnHead.SelectedValue)
            'objECCT._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objECCT._Effectivedate = dtpEffectiveDate.Value
            'objECCT._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objECCT._Istransactionhead = Not mblnIsCostCenter
            'objECCT._Isvoid = False
            'objECCT._Rehiretranunkid = 0
            'objECCT._Statusunkid = 0
            'objECCT._Userunkid = User._Object._Userunkid
            'objECCT._Voiddatetime = Nothing
            'objECCT._Voidreason = ""
            'objECCT._Voiduserunkid = -1
            'S.SANDEEP [15-NOV-2018] -- START
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
                'S.SANDEEP [15-NOV-2018] -- END
            objECCT._Cctranheadvalueid = CInt(cboCCTrnHead.SelectedValue)
            objECCT._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            objECCT._Effectivedate = dtpEffectiveDate.Value
            objECCT._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objECCT._Istransactionhead = Not mblnIsCostCenter
            objECCT._Isvoid = False
            objECCT._Rehiretranunkid = 0
            objECCT._Statusunkid = 0
            objECCT._Userunkid = User._Object._Userunkid
            objECCT._Voiddatetime = Nothing
            objECCT._Voidreason = ""
            objECCT._Voiduserunkid = -1
            Else

                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                objACCT = New clsCCTranhead_Approval_Tran
                'Gajanan [11-Dec-2019] -- End

                objACCT._Audittype = enAuditType.ADD
                objACCT._Audituserunkid = User._Object._Userunkid

                objACCT._Cctranheadvalueid = CInt(cboCCTrnHead.SelectedValue)
                objACCT._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objACCT._Effectivedate = dtpEffectiveDate.Value
                objACCT._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objACCT._Istransactionhead = Not mblnIsCostCenter
                objACCT._Isvoid = False
                objACCT._Rehiretranunkid = 0
                objACCT._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objACCT._Voiddatetime = Nothing
                objACCT._Voidreason = ""
                objACCT._Voiduserunkid = -1

                objACCT._Tranguid = Guid.NewGuid.ToString()
                objACCT._Transactiondate = Now
                objACCT._Remark = ""
                objACCT._Rehiretranunkid = 0
                objACCT._Mappingunkid = 0
                objACCT._Isweb = False
                objACCT._Isfinal = False
                objACCT._Ip = getIP()
                objACCT._Hostname = getHostName()
                objACCT._Form_Name = mstrModuleName

                'S.SANDEEP |17-JAN-2019| -- START
                If mAction = enAction.EDIT_ONE Then
                    objACCT._Cctranheadunkid = mintTransactionId
                    objACCT._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objACCT._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END
            End If



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetEditValue()
        Try
            dtpEffectiveDate.Value = objECCT._Effectivedate
            cboEmployee.SelectedValue = objECCT._Employeeunkid
            cboChangeReason.SelectedValue = objECCT._Changereasonunkid
            cboCCTrnHead.SelectedValue = objECCT._Cctranheadvalueid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetEditValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEffectiveDate.Checked = False
            cboChangeReason.SelectedValue = 0
            cboCCTrnHead.SelectedValue = 0
            txtDate.Text = ""
            pnlData.Visible = False
            mdtAppointmentDate = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowtoChangeCostCenter

            'S.SANDEEP |17-JAN-2019| -- START
            'objdgcolhEdit.Visible = User._Object.Privilege._EditEmployeeCostCenter
            If User._Object.Privilege._EditEmployeeCostCenter = False Then
                objdgcolhEdit.Image = imgBlank
            End If
            'S.SANDEEP |17-JAN-2019| -- END

            objdgcolhDelete.Visible = User._Object.Privilege._DeleteEmployeeCostCenter
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master

        Try
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeOtherDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objECCT = New clsemployee_cctranhead_tran
        'S.SANDEEP [20-JUN-2018] -- START
        'ISSUE/ENHANCEMENT : {Ref#244}
        objACCT = New clsCCTranhead_Approval_Tran
        'S.SANDEEP [20-JUN-2018] -- END

        Try
            Call Set_Form_Controls()
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If mintEmployeeID > 0 Then
                cboEmployee.SelectedValue = mintEmployeeID
            End If
            'Gajanan [11-Dec-2019] -- End


            pnlData.Visible = False
            'S.SANDEEP [14 APR 2015] -- START
            objlblRehire.Visible = False

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            lblPendingData.Visible = False
            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP [14 APR 2015] -- END
            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            'btnSave.Enabled = User._Object.Privilege._AllowtoChangeCostCenter
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End
            'Shani (08-Dec-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeOtherDetails_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_cctranhead_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_cctranhead_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidDetails() = False Then Exit Sub
            Call SetValue()
            If mAction = enAction.EDIT_ONE Then
                'S.SANDEEP |17-JAN-2019| -- START

                'blnFlag = objECCT.Update()
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                blnFlag = objECCT.Update()


                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    'Pinkal (20-Dec-2024) -- Start
                    'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
                    '   blnFlag = objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.EDITED)
                    blnFlag = objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.EDITED, Company._Object._Companyunkid)
                    'Pinkal (20-Dec-2024) -- End



                    If blnFlag = False AndAlso objACCT._Message <> "" Then
                        eZeeMsgBox.Show(objACCT._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                'S.SANDEEP |17-JAN-2019| -- END


            Else
                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'blnFlag = objECCT.Insert()
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                blnFlag = objECCT.Insert()
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmEmployeeBenefitCoverage
                        objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
                    End If
                Else

                    'S.SANDEEP |17-JAN-2019| -- START

                    'blnFlag = objECCT.Update()
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                        blnFlag = objECCT.Update()
                    Else
                        'Check For Entry is Available In Approval For Delete
                        blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                        If blnFlag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If


                        blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                        If blnFlag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                        blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                        If blnFlag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If


                        'Check For Entry is Available In Approval For Edit

                        blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                        If blnFlag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If


                        blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                        If blnFlag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                        blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                        If blnFlag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP |17-JAN-2019| -- END

                        'Pinkal (20-Dec-2024) -- Start
                        'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
                        'blnFlag = objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED)
                        blnFlag = objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED, Company._Object._Companyunkid)
                        'Pinkal (20-Dec-2024) -- End



                    If blnFlag = False AndAlso objACCT._Message <> "" Then
                        eZeeMsgBox.Show(objACCT._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    End If

                End If
                'S.SANDEEP [20-JUN-2018] -- END
            End If
            If blnFlag = False AndAlso objECCT._Message <> "" Then
                eZeeMsgBox.Show(objECCT._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                Call Fill_Grid()
                Call Set_Details()
                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'S.SANDEEP [15-NOV-2018] -- START
                'S.SANDEEP |17-JAN-2019| -- Start
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
                    'S.SANDEEP |17-JAN-2019| -- END
                    'S.SANDEEP [15-NOV-2018] -- END
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    'S.SANDEEP |17-JAN-2019| -- START
                    'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1201, clsEmployeeMovmentApproval.enMovementType.COSTCENTER, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If mAction = enAction.EDIT_ONE Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1201, clsEmployeeMovmentApproval.enMovementType.COSTCENTER, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, eOperType, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    'S.SANDEEP |17-JAN-2019| -- END
                    objPMovement = Nothing
                End If
                'S.SANDEEP [20-JUN-2018] -- END
            End If
            Call ClearControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'S.SANDEEP [15-NOV-2018] -- START
            'S.SANDEEP |17-JAN-2019| -- Start
            'mintTransactionId = 0
            'S.SANDEEP |17-JAN-2019| -- End

            'S.SANDEEP [15-NOV-2018] -- END
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objSearchReason.Click, objbtnSearchCCTrnHead.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objSearchReason.Name.ToUpper
                    xCbo = cboChangeReason
                Case objbtnSearchCCTrnHead.Name.ToUpper
                    xCbo = cboCCTrnHead
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, xMasterType, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(CType(xMasterType, clsCommon_Master.enCommonMaster), True, "List")
                With cboChangeReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region " Combobox Event(s) "


    'Pinkal (09-Apr-2015) -- Start
    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

    'Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboEmployee.KeyDown
    '    Try
    '        If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboEmployee.ValueMember
    '                .DisplayMember = cboEmployee.DisplayMember
    '                .DataSource = CType(cboEmployee.DataSource, DataTable)
    '                .CodeMember = "employeecode"
    '                .SelectedText = cboEmployee.Text
    '            End With
    '            If frm.DisplayDialog Then
    '                cboEmployee.SelectedValue = frm.SelectedValue
    '                If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
    '            Else
    '                cboEmployee.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (09-Apr-2015) -- End

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim xtmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                If xtmp.Length > 0 Then
                    mstrEmployeeCode = CStr(xtmp(0).Item("employeecode"))
                End If
                'S.SANDEEP [14 APR 2015] -- START
                objlblRehire.Visible = False
                'S.SANDEEP [14 APR 2015] -- END

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If btnSave.Enabled = False Then btnSave.Enabled = True
                If objdgcolhEdit.Visible = False Then objdgcolhEdit.Visible = True
                If objdgcolhDelete.Visible = False Then objdgcolhDelete.Visible = True
                'S.SANDEEP [04-AUG-2017] -- END

                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                If mAction = enAction.EDIT_ONE Then
                    If objECCT._Cctranheadunkid > 0 Then objECCT._Cctranheadunkid = 0
                    mAction = enAction.ADD_CONTINUE
                End If
                'S.SANDEEP [07-Feb-2018] -- END

                Call ClearControls()
                Call Fill_Grid()
                Call Set_Details()
                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If ConfigParameter._Object._IsIncludeInactiveEmp Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date) = CInt(cboEmployee.SelectedValue)
                    If objEmp._Empl_Enddate <> Nothing Then
                        If objEmp._Empl_Enddate <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_From_Date <> Nothing Then
                        If objEmp._Termination_From_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_To_Date <> Nothing Then
                        If objEmp._Termination_To_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    objEmp = Nothing
                End If
                'S.SANDEEP [04-AUG-2017] -- END
            Else
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            mdtAppointmentDate = Nothing
            txtDate.Text = ""
            pnlData.Visible = False
            Select Case e.ColumnIndex
                'S.SANDEEP [09-AUG-2018] -- START
                Case objdgcolhViewPending.Index
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhViewPending.Index).Value Is imgInfo Then
                        Dim frm As New frmViewMovementApproval
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim intMovementId As Integer = 0
                        Dim intPrivilegeId As Integer = 0
                        If mblnIsCostCenter Then
                            intMovementId = clsEmployeeMovmentApproval.enMovementType.COSTCENTER
                            intPrivilegeId = 1201
                        End If
                        'S.SANDEEP |17-JAN-2019| -- START
                        'frm.displayDialog(User._Object._Userunkid, 0, intPrivilegeId, CType(intMovementId, clsEmployeeMovmentApproval.enMovementType), Nothing, False, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                        If mAction = enAction.EDIT_ONE Then
                            eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                        Else
                            eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                        End If
                        frm.displayDialog(User._Object._Userunkid, 0, intPrivilegeId, CType(intMovementId, clsEmployeeMovmentApproval.enMovementType), Nothing, False, eOperType, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        'S.SANDEEP |17-JAN-2019| -- END
                        If frm IsNot Nothing Then frm.Dispose()
                        Exit Sub
                    End If
                    'S.SANDEEP [09-AUG-2018] -- END

                Case objdgcolhEdit.Index
                    'S.SANDEEP |17-JAN-2019| -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgView Then
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value) > 0 Then
                            Dim dr As DataRow() = CType(dgvHistory.DataSource, DataTable).Select(objdgcolhdetailtranunkid.DataPropertyName & " = " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value) & " AND " & objdgcolhtranguid.DataPropertyName & "= ''")
                            If dr.Length > 0 Then
                                Dim index As Integer = CType(dgvHistory.DataSource, DataTable).Rows.IndexOf(dr(0))
                                dgvHistory.Rows(index).Selected = True
                                dgvHistory.FirstDisplayedScrollingRowIndex = index
                            End If
                        End If
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                    mAction = enAction.EDIT_ONE
                    'S.SANDEEP [14 APR 2015] -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub
                    'S.SANDEEP [14 APR 2015] -- END

                    objECCT._Cctranheadunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value)

                    'S.SANDEEP [15-NOV-2018] -- START
                    mintTransactionId = objECCT._Cctranheadunkid
                    'S.SANDEEP [15-NOV-2018] -- END

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objACCT.isExist(Nothing, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True, mintTransactionId)
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP |17-JAN-2019| -- END
                    End If

                    Call SetEditValue()
                    If CBool(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhFromEmp.Index).Value) = True Then
                        mdtAppointmentDate = eZeeDate.convertDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAppointDate.Index).Value.ToString)
                        txtDate.Text = mdtAppointmentDate.Date.ToShortDateString
                        pnlData.Visible = True
                    End If
                Case objdgcolhDelete.Index
                    mAction = enAction.ADD_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objACCT.isExist(Nothing, 0, mblnIsCostCenter, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True, CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value))
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objECCT._Isvoid = True
                    objECCT._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objECCT._Voidreason = xStrVoidReason
                    objECCT._Voiduserunkid = User._Object._Userunkid
                    objECCT._Userunkid = User._Object._Userunkid
                    'S.SANDEEP |17-JAN-2019| -- START


                    'If objECCT.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value)) = False Then
                    '    If objECCT._Message <> "" Then
                    '        eZeeMsgBox.Show(objECCT._Message, enMsgBoxStyle.Information)
                    '    End If
                    '    Exit Sub
                    'End If
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        objACCT._Isvoid = False
                        objACCT._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objACCT._Voidreason = xStrVoidReason
                        objACCT._Voiduserunkid = -1
                        objACCT._Audituserunkid = User._Object._Userunkid
                        objACCT._Isweb = False
                        objACCT._Ip = getIP()
                        objACCT._Hostname = getHostName()
                        objACCT._Form_Name = mstrModuleName

                        'Pinkal (20-Dec-2024) -- Start
                        'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
                        'If objACCT.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED, Nothing) = False Then
                        If objACCT.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED, Company._Object._Companyunkid, Nothing) = False Then
                            'Pinkal (20-Dec-2024) -- End

                            If objACCT._Message <> "" Then
                                eZeeMsgBox.Show(objACCT._Message, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If
                    Else
                    If objECCT.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdetailtranunkid.Index).Value)) = False Then
                        If objECCT._Message <> "" Then
                            eZeeMsgBox.Show(objECCT._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    Call Fill_Grid()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHistory.DataBindingComplete
        Try
            For Each xdgvr As DataGridViewRow In dgvHistory.Rows

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                '    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                'End If
                If CStr(xdgvr.Cells(objdgcolhtranguid.Index).Value).Trim.Length > 0 Then
                    xdgvr.DefaultCellStyle.BackColor = Color.PowderBlue
                    xdgvr.DefaultCellStyle.ForeColor = Color.Black
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    'S.SANDEEP [09-AUG-2018] -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).Value = imgInfo
                    'S.SANDEEP [09-AUG-2018] -- END
                    lblPendingData.Visible = True

                    'S.SANDEEP |17-JAN-2019| -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).ToolTipText = xdgvr.Cells(objdgcolhOperationType.Index).Value.ToString
                    If CInt(xdgvr.Cells(objdgcolhOperationTypeId.Index).Value) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        xdgvr.Cells(objdgcolhEdit.Index).Value = imgView
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                Else
                If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                End If
                End If
                'S.SANDEEP [20-JUN-2018] -- END


                'S.SANDEEP [14 APR 2015] -- START
                If CInt(xdgvr.Cells(objdgcolhrehiretranunkid.Index).Value) > 0 Then
                    For xCellIdx As Integer = 0 To xdgvr.Cells.Count - 1
                        If xCellIdx > 1 Then
                            xdgvr.Cells(xCellIdx).Style.BackColor = Color.Orange
                            xdgvr.Cells(xCellIdx).Style.ForeColor = Color.Black
                        End If
                    Next
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    objlblRehire.Visible = True
                End If
                'S.SANDEEP [14 APR 2015] -- END

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.objgbInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.Name, Me.lblAppointmentdate.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.dgcolhChangeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChangeDate.Name, Me.dgcolhChangeDate.HeaderText)
            Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Cost Center Information")
            Language.setMessage(mstrModuleName, 2, "Cost Center")
            Language.setMessage(mstrModuleName, 3, "Transaction Head Information")
            Language.setMessage(mstrModuleName, 4, "Transaction Head")
            Language.setMessage(mstrModuleName, 5, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Cost center is mandatory information. Please select cost center to continue.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Transaction head is mandatory information. Please select transaction head to continue.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue.")
            Language.setMessage(mstrModuleName, 10, "Sorry, This is system generate entry. Please set effective date as employee appointment date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class