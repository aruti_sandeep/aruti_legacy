﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmployeeMovementLog_List

#Region " Private Variables "
    Private objEmployee As clsEmployee_Master
    Private ReadOnly mstrModuleName As String = "frmEmployeeMovementLog_List"
#End Region

#Region " Private Functions "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim intIndex As Integer = 0
        Try

            dsList = objEmployee.GetMovement_Log(CInt(cboEmployee.SelectedValue), "Log")

            If CInt(cboDepartment.SelectedValue) > 0 Then
                StrSearching &= "AND DeptId = " & CInt(cboDepartment.SelectedValue) & " "
            End If

            If CInt(cboClass.SelectedValue) > 0 Then
                StrSearching &= "AND ClassId = " & CInt(cboClass.SelectedValue) & " "
            End If

            If CInt(cboGrade.SelectedValue) > 0 Then
                StrSearching &= "AND GradeId = " & CInt(cboGrade.SelectedValue) & " "
            End If

            If CInt(cboJob.SelectedValue) > 0 Then
                StrSearching &= "AND JobId = " & CInt(cboJob.SelectedValue) & " "
            End If

            If CInt(cboSections.SelectedValue) > 0 Then
                StrSearching &= "AND SecId = " & CInt(cboSections.SelectedValue) & " "
            End If

            If CInt(cboUnits.SelectedValue) > 0 Then
                StrSearching &= "AND UnitId = " & CInt(cboUnits.SelectedValue) & " "
            End If

            If dtpDate.Checked = True Then
                StrSearching &= "AND Date = '" & eZeeDate.convertDate(dtpDate.Value) & "'" & " "
            End If



            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables("Log"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("Log")
            End If


            lvEmployeeMovementLog.LVItems = Nothing
            If dtTable.Rows.Count > 0 Then
                lvEmployeeMovementLog.LVItems = New ListViewItem(dtTable.Rows.Count - 1) {}
            End If

            Dim lvItem As ListViewItem

            lvEmployeeMovementLog.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("Branch").ToString)
                lvItem.SubItems.Add(dtRow.Item("Dept").ToString)
                lvItem.SubItems.Add(dtRow.Item("Section").ToString)
                lvItem.SubItems.Add(dtRow.Item("Job").ToString)
                lvItem.SubItems.Add(dtRow.Item("UserName").ToString)
                lvItem.SubItems.Add(dtRow.Item("Unit").ToString)
                lvItem.SubItems.Add(dtRow.Item("Grade").ToString)
                lvItem.SubItems.Add(dtRow.Item("GLevel").ToString)
                lvItem.SubItems.Add(Format(CDec(dtRow.Item("Scale")), GUI.fmtCurrency).ToString)
                lvItem.SubItems.Add(dtRow.Item("Class").ToString)
                lvItem.SubItems.Add(dtRow.Item("CostCenter").ToString)

                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dtRow.Item("DeptGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("SectionGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("UnitGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("Team").ToString)
                lvItem.SubItems.Add(dtRow.Item("GradeGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("JobGroup").ToString)
                lvItem.SubItems.Add(dtRow.Item("ClassGrp").ToString)
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                'Anjan (02 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                lvItem.SubItems.Add(dtRow.Item("AllocationReason").ToString)
                'Anjan (02 Mar 2012)-End 


                'S.SANDEEP [ 17 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Appointed").ToString).ToShortDateString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("ConfDt").ToString).ToShortDateString)

                If dtRow.Item("SFD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("SFD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("STD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("STD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("PFD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("PFD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("PTD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("PTD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("EOC").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EOC").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("Leaving").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Leaving").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Retirement").ToString).ToShortDateString)

                If dtRow.Item("Reinstatement").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Reinstatement").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If
                'S.SANDEEP [ 17 DEC 2012 ] -- END


                lvEmployeeMovementLog.LVItems(intIndex) = lvItem
                intIndex += 1
                lvItem = Nothing
            Next

            If intIndex > 0 Then
                lvEmployeeMovementLog.VirtualListSize = intIndex
            Else
                lvEmployeeMovementLog.VirtualListSize = 0
            End If

            lvEmployeeMovementLog.Invalidate()
            lvEmployeeMovementLog.Reset()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objDepartment As New clsDepartment
        Dim objJobs As New clsJobs
        Dim objUnit As New clsUnits
        Dim objSection As New clsSections
        Dim objGrade As New clsGrade
        Dim objClass As New clsClass
        Dim dsCombos As New DataSet
        Try
            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
                .SelectedValue = 0
            End With

            dsCombos = objJobs.getComboList("Jobs", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Jobs")
                .SelectedValue = 0
            End With

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
                .SelectedValue = 0
            End With

            dsCombos = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
                .SelectedValue = 0
            End With

            dsCombos = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Grade")
                .SelectedValue = 0
            End With

            dsCombos = objClass.getComboList("Class", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Class")
                .SelectedValue = 0
            End With


            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsCombos = objEmployee.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 17 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, )
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmployee.GetEmployeeList("Emp", True)
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 17 DEC 2012 ] -- END

            
            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objDepartment = Nothing
            objJobs = Nothing
            objUnit = Nothing
            objSection = Nothing
            objGrade = Nothing
            objClass = Nothing
            dsCombos = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeMovementLog_List_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeMovementLog_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployee = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            lvEmployeeMovementLog.CreateHeaderItems()
            Call FillCombo()
            'Call FillList()
            lvEmployeeMovementLog.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeMovementLog_List_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeMovementLog_List_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmployee = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboClass.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboGrade.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboUnits.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhBranch.Text = Language._Object.getCaption(CStr(Me.colhBranch.Tag), Me.colhBranch.Text)
			Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
			Me.colhUnit.Text = Language._Object.getCaption(CStr(Me.colhUnit.Tag), Me.colhUnit.Text)
			Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
			Me.colhGrade.Text = Language._Object.getCaption(CStr(Me.colhGrade.Tag), Me.colhGrade.Text)
			Me.colhScale.Text = Language._Object.getCaption(CStr(Me.colhScale.Tag), Me.colhScale.Text)
			Me.colhClass.Text = Language._Object.getCaption(CStr(Me.colhClass.Tag), Me.colhClass.Text)
			Me.colhCostCenter.Text = Language._Object.getCaption(CStr(Me.colhCostCenter.Tag), Me.colhCostCenter.Text)
			Me.colhUser.Text = Language._Object.getCaption(CStr(Me.colhUser.Tag), Me.colhUser.Text)
			Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
			Me.colhGradeLevel.Text = Language._Object.getCaption(CStr(Me.colhGradeLevel.Tag), Me.colhGradeLevel.Text)
			Me.colhDeptGrp.Text = Language._Object.getCaption(CStr(Me.colhDeptGrp.Tag), Me.colhDeptGrp.Text)
			Me.colhSectionGrp.Text = Language._Object.getCaption(CStr(Me.colhSectionGrp.Tag), Me.colhSectionGrp.Text)
			Me.colhUnitGrp.Text = Language._Object.getCaption(CStr(Me.colhUnitGrp.Tag), Me.colhUnitGrp.Text)
			Me.colhTeam.Text = Language._Object.getCaption(CStr(Me.colhTeam.Tag), Me.colhTeam.Text)
			Me.colhGradeGrp.Text = Language._Object.getCaption(CStr(Me.colhGradeGrp.Tag), Me.colhGradeGrp.Text)
			Me.colhJobGrp.Text = Language._Object.getCaption(CStr(Me.colhJobGrp.Tag), Me.colhJobGrp.Text)
			Me.colhClassGrp.Text = Language._Object.getCaption(CStr(Me.colhClassGrp.Tag), Me.colhClassGrp.Text)
			Me.colhAllocationReason.Text = Language._Object.getCaption(CStr(Me.colhAllocationReason.Tag), Me.colhAllocationReason.Text)
			Me.colhAppointedDate.Text = Language._Object.getCaption(CStr(Me.colhAppointedDate.Tag), Me.colhAppointedDate.Text)
			Me.colhConfirmationDate.Text = Language._Object.getCaption(CStr(Me.colhConfirmationDate.Tag), Me.colhConfirmationDate.Text)
			Me.colhSuspensionFrom.Text = Language._Object.getCaption(CStr(Me.colhSuspensionFrom.Tag), Me.colhSuspensionFrom.Text)
			Me.colhSuspensionTo.Text = Language._Object.getCaption(CStr(Me.colhSuspensionTo.Tag), Me.colhSuspensionTo.Text)
			Me.colhProbationFrom.Text = Language._Object.getCaption(CStr(Me.colhProbationFrom.Tag), Me.colhProbationFrom.Text)
			Me.colhProbationTo.Text = Language._Object.getCaption(CStr(Me.colhProbationTo.Tag), Me.colhProbationTo.Text)
			Me.colhEOCDate.Text = Language._Object.getCaption(CStr(Me.colhEOCDate.Tag), Me.colhEOCDate.Text)
			Me.colhLeavingDate.Text = Language._Object.getCaption(CStr(Me.colhLeavingDate.Tag), Me.colhLeavingDate.Text)
			Me.colhRetirementDate.Text = Language._Object.getCaption(CStr(Me.colhRetirementDate.Tag), Me.colhRetirementDate.Text)
			Me.colhReinstatementDate.Text = Language._Object.getCaption(CStr(Me.colhReinstatementDate.Tag), Me.colhReinstatementDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class