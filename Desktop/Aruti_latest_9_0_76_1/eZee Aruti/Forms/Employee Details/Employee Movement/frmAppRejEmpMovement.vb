﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmAppRejEmpMovement

#Region " Private Varaibles "

    Private objApprovalTran As New clsEmployeeMovmentApproval
    Private ReadOnly mstrModuleName As String = "frmAppRejEmpMovement"
    Private mdvData As DataView
    Private intPrivilegeId As Integer = 0
    Private eFillType As clsEmployeeMovmentApproval.enMovementType
    Private eDtType As enEmp_Dates_Transaction
    Private blnIsResidentPermit As Boolean
    'S.SANDEEP [09-AUG-2018] -- START
    Private mstrDefaultSearchParameter As String = ""
    'S.SANDEEP [09-AUG-2018] -- END

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                If CInt(.SelectedValue) <= 0 Then
                    .Text = ""
                End If
            End With

            dsList = objApprovalTran.getMovementList(User._Object._Userunkid, "List", True)
            With cboMovement
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP |17-JAN-2019| -- START
            dsList = objApprovalTran.getOperationTypeList("List", True)
            With cboOperationType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP |17-JAN-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Function GetFilterString(ByVal strCriteria As String) As String
        Dim strFilter As String = String.Empty
        Try
            If dgvData.RowCount > 0 Then
                If CInt(cboMovement.SelectedValue) > 0 Then
                    Dim vrow As DataRowView = CType(cboMovement.SelectedItem, DataRowView)
                    Dim row As DataRow = vrow.Row
                    Dim eFillType As clsEmployeeMovmentApproval.enMovementType
                    eFillType = CType(row.Item("Id"), clsEmployeeMovmentApproval.enMovementType)

                    strFilter = dgcolhecode.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                dgcolhename.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                dgcolhEffDate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                dgcolhCReason.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                    Select Case eFillType
                        Case clsEmployeeMovmentApproval.enMovementType.CONFIRMATION, clsEmployeeMovmentApproval.enMovementType.PROBATION, clsEmployeeMovmentApproval.enMovementType.RETIREMENTS, clsEmployeeMovmentApproval.enMovementType.SUSPENSION, clsEmployeeMovmentApproval.enMovementType.TERMINATION, clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                            'Sohail (21 Oct 2019) - [EXEMPTION]
                            strFilter &= dgcolhdDate1.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhdDate2.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                        Case clsEmployeeMovmentApproval.enMovementType.COSTCENTER
                            'Gajanan [17-Nov-2018] -- START
                            'strFilter &= dgcolhDispValue.Visible = True
                            strFilter &= dgcolhDispValue.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "
                            'Gajanan [17-Nov-2018] -- End


                        Case clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE
                            strFilter &= dgcolhJobGroup.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhJob.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                        Case clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                            strFilter &= dgcolhwork_permit_no.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhissue_place.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhIDate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhExDate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhCountry.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                        Case clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                            strFilter &= dgcolhbranch.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhdeptgroup.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhdept.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhsecgroup.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhsection.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhunitgrp.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhunit.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhteam.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhclassgrp.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         dgcolhclass.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "
                    End Select
                End If
            End If
            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(0, strFilter.Length - 4)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFilterString", mstrModuleName)
        Finally
        End Try
        Return strFilter
    End Function

    Private Sub FillGrid()
        Try
            If txtApprover.Text.Trim.Length > 0 Then
                If CInt(cboMovement.SelectedValue) <= 0 OrElse CInt(cboOperationType.SelectedValue) <= 0 Then Exit Sub
    			'Gajanan [9-NOV-2019] -- Add {OrElse CInt(cboOperationType.SelectedValue) <= 0}   

                'S.SANDEEP |17-JAN-2019| -- START
                'Dim dt As DataTable = objApprovalTran.GetEmployeeApprovalData(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, User._Object._Userunkid, CInt(txtLevel.Tag), False, eFillType, ConfigParameter._Object._EmployeeAsOnDate, eDtType, blnIsResidentPermit, Nothing)
                Dim dt As DataTable = objApprovalTran.GetEmployeeApprovalData(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, User._Object._Userunkid, CInt(txtLevel.Tag), False, eFillType, ConfigParameter._Object._EmployeeAsOnDate, eDtType, blnIsResidentPermit, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), Nothing)
                'S.SANDEEP |17-JAN-2019| -- END


                dgvData.AutoGenerateColumns = False

                objdgcolhicheck.DataPropertyName = "icheck"
                dgcolhecode.DataPropertyName = "ecode"
                dgcolhename.DataPropertyName = "ename"
                dgcolhEffDate.DataPropertyName = "EffDate"
                dgcolhCReason.DataPropertyName = "CReason"

                dgcolhbranch.DataPropertyName = "branch"
                dgcolhdeptgroup.DataPropertyName = "deptgroup"
                dgcolhdept.DataPropertyName = "dept"
                dgcolhsecgroup.DataPropertyName = "secgroup"
                dgcolhsection.DataPropertyName = "section"
                dgcolhunitgrp.DataPropertyName = "unitgrp"
                dgcolhunit.DataPropertyName = "unit"
                dgcolhteam.DataPropertyName = "team"
                dgcolhclassgrp.DataPropertyName = "classgrp"
                dgcolhclass.DataPropertyName = "class"

                dgcolhJobGroup.DataPropertyName = "JobGroup"
                dgcolhJob.DataPropertyName = "Job"

                dgcolhdDate1.DataPropertyName = "dDate1"
                dgcolhdDate2.DataPropertyName = "dDate2"

                dgcolhwork_permit_no.DataPropertyName = "work_permit_no"
                dgcolhissue_place.DataPropertyName = "issue_place"
                dgcolhIDate.DataPropertyName = "IDate"
                dgcolhExDate.DataPropertyName = "EDate"
                dgcolhCountry.DataPropertyName = "Country"

                dgcolhDispValue.DataPropertyName = "DispValue"

                'S.SANDEEP [09-AUG-2018] -- START
                'mdvData = dt.DefaultView
                mstrDefaultSearchParameter = ""
                If User._Object._EmployeeUnkid > 0 Then
                    mstrDefaultSearchParameter = "employeeunkid <> " & User._Object._EmployeeUnkid
                End If

            'S.SANDEEP |17-JAN-2019| -- START

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    mstrDefaultSearchParameter = "employeeunkid  =  " & cboEmployee.SelectedValue.ToString()
                End If

            'S.SANDEEP |17-JAN-2019| -- END

                mdvData = dt.DefaultView
                mdvData.RowFilter = mstrDefaultSearchParameter
                'S.SANDEEP [09-AUG-2018] -- END

                dgvData.DataSource = mdvData

                Select Case eFillType
                    Case clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                        dgcolhdDate1.Visible = True
                        dgcolhdDate2.Visible = False
                        dgcolhdDate1.HeaderText = Language.getMessage(mstrModuleName, 102, "Confirmation Date")

                    Case clsEmployeeMovmentApproval.enMovementType.PROBATION
                        dgcolhdDate1.Visible = True
                        dgcolhdDate2.Visible = True
                        dgcolhdDate1.HeaderText = Language.getMessage(mstrModuleName, 100, "Start Date")
                        dgcolhdDate2.HeaderText = Language.getMessage(mstrModuleName, 101, "End Date")

                    Case clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                        dgcolhdDate1.Visible = True
                        dgcolhdDate2.Visible = False
                        dgcolhdDate1.HeaderText = Language.getMessage(mstrModuleName, 103, "Retirement Date")

                    Case clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                        dgcolhdDate1.Visible = True
                        dgcolhdDate2.Visible = True
                        dgcolhdDate1.HeaderText = Language.getMessage(mstrModuleName, 100, "Start Date")
                        dgcolhdDate2.HeaderText = Language.getMessage(mstrModuleName, 101, "End Date")

                    Case clsEmployeeMovmentApproval.enMovementType.TERMINATION
                        dgcolhdDate1.Visible = True
                        dgcolhdDate2.Visible = True
                        dgcolhdDate1.HeaderText = Language.getMessage(mstrModuleName, 104, "EOC Date")
                        dgcolhdDate2.HeaderText = Language.getMessage(mstrModuleName, 105, "Leaving Date")

                    Case clsEmployeeMovmentApproval.enMovementType.COSTCENTER
                        dgcolhDispValue.Visible = True

                    Case clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE
                        dgcolhJobGroup.Visible = True
                        dgcolhJob.Visible = True

                    Case clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                        dgcolhwork_permit_no.Visible = True
                        dgcolhissue_place.Visible = True
                        dgcolhIDate.Visible = True
                        dgcolhExDate.Visible = True
                        dgcolhCountry.Visible = True

                    Case clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                        dgcolhbranch.Visible = True
                        dgcolhdeptgroup.Visible = True
                        dgcolhdept.Visible = True
                        dgcolhsecgroup.Visible = True
                        dgcolhsection.Visible = True
                        dgcolhunitgrp.Visible = True
                        dgcolhunit.Visible = True
                        dgcolhteam.Visible = True
                        dgcolhclassgrp.Visible = True
                        dgcolhclass.Visible = True

                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                        dgcolhdDate1.Visible = True
                        dgcolhdDate2.Visible = True
                        dgcolhdDate1.HeaderText = Language.getMessage(mstrModuleName, 100, "Start Date")
                        dgcolhdDate2.HeaderText = Language.getMessage(mstrModuleName, 101, "End Date")
                        'Sohail (21 Oct 2019) -- End

                End Select
    			'Gajanan [9-NOV-2019] -- Start   
    			'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
                objbtnSearch.ShowResult(dgvData.RowCount.ToString)
    			'Gajanan [9-NOV-2019] -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetApproverInfo()
        Dim objUMapping As New clsemp_appUsermapping
        Dim dsInfo As New DataSet
        Try
            dsInfo = objUMapping.GetList(enEmpApproverType.APPR_MOVEMENT, "List", True, " AND hremp_appusermapping.isactive = 1 ", User._Object._Userunkid, Nothing)
            If dsInfo.Tables("List").Rows.Count > 0 Then
                txtApprover.Text = dsInfo.Tables("List").Rows(0)("User").ToString()
                txtApprover.Tag = CInt(dsInfo.Tables("List").Rows(0)("mappingunkid"))

                txtLevel.Text = dsInfo.Tables("List").Rows(0)("Level").ToString()
                txtLevel.Tag = CInt(dsInfo.Tables("List").Rows(0)("priority"))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetApproverInfo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Sub SetGridColums()
        Try
            dgcolhdDate1.Visible = False : dgcolhdDate2.Visible = False
            dgcolhDispValue.Visible = False : dgcolhJobGroup.Visible = False
            dgcolhJob.Visible = False : dgcolhwork_permit_no.Visible = False
            dgcolhissue_place.Visible = False : dgcolhIDate.Visible = False
            dgcolhExDate.Visible = False : dgcolhCountry.Visible = False
            dgcolhbranch.Visible = False : dgcolhdeptgroup.Visible = False
            dgcolhdept.Visible = False : dgcolhsecgroup.Visible = False
            dgcolhsection.Visible = False : dgcolhunitgrp.Visible = False
            dgcolhunit.Visible = False : dgcolhteam.Visible = False
            dgcolhclassgrp.Visible = False : dgcolhclass.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColums", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAppRejEmpMovement_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApprovalTran = New clsEmployeeMovmentApproval
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call SetGridColums()

            Call OtherSettings()
            Call SetApproverInfo()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppRejEmpMovement_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAppRejEmpMovement_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objApprovalTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeMovmentApproval.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeMovmentApproval"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Try
            If mdvData IsNot Nothing Then
                Dim dtTable As DataTable = mdvData.ToTable()
                If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objdgcolhicheck.DataPropertyName) = True).Count() <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If


                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to approve employee(s) without remark?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    Case btnReject.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue."), enMsgBoxStyle.Information)
                            txtRemark.Focus()
                            Exit Sub
                        End If


                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set

                        'S.SANDEEP |05-JUN-2023| -- START
                        'ISSUE/ENHANCEMENT : A1X-933,934
                        'If CInt(cboOperationType.SelectedValue) = clsEmployeeMovmentApproval.enOperationType.ADDED AndAlso CType(cboMovement.SelectedValue, enUserPriviledge) = enUserPriviledge.AllowToApproveRejectEmployeeExemption Then
                        If CInt(cboOperationType.SelectedValue) = clsEmployeeMovmentApproval.enOperationType.ADDED AndAlso CType(cboMovement.SelectedValue, enUserPriviledge) = enUserPriviledge.AllowToApproveRejectEmployeeExemption AndAlso eFillType = clsEmployeeMovmentApproval.enMovementType.EXEMPTION Then
                            'S.SANDEEP |05-JUN-2023| -- END
                            If dtTable.Select("icheck = 1 AND leaveissueunkid > 0 ").Length > 0 Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Leave is arleady Issued, Are you sure you want to reject Employee exemotion for this") & " " & dtTable.Select("icheck = 1 AND leaveissueunkid > 0 ")(0).Item("CReason").ToString & "?" & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 6, "If YES employee will be considered in Payroll pocess for those unpaid leave days."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                    Exit Sub
                                End If
                            End If
                        End If


                        'Gajanan [9-NOV-2019] -- ADD Missing Condition 
                        '{CType(cboMovement.SelectedValue, enUserPriviledge) = enUserPriviledge.AllowToApproveRejectEmployeeExemption   

                        'Sohail (21 Oct 2019) -- End

                End Select

                Dim dtAppr As DataTable : Dim drtemp() As DataRow = Nothing : Dim strCheckedEmpIds As String = ""

                'S.SANDEEP |17-JAN-2019| -- START
                'dtAppr = objApprovalTran.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, eFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, eDtType, blnIsResidentPermit, Nothing, CInt(txtLevel.Tag), False)
                dtAppr = objApprovalTran.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, eFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, eDtType, blnIsResidentPermit, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), Nothing, CInt(txtLevel.Tag), False)
                'S.SANDEEP |17-JAN-2019| -- END


                If dtAppr.Rows.Count > 0 Then
                    strCheckedEmpIds = String.Join(",", dtAppr.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
                Else
                    strCheckedEmpIds = ""
                End If

                If strCheckedEmpIds.Trim.Length > 0 Then
                    drtemp = dtTable.Select("employeeunkid not in (" & strCheckedEmpIds & ") AND icheck = true ")
                Else
                    drtemp = dtTable.Select("icheck = true ")
                End If
                If drtemp.Length > 0 Then
                    For index As Integer = 0 To drtemp.Length - 1
                        drtemp(index)("isfinal") = True
                    Next
                End If
                dtTable.AcceptChanges()
                dtTable = New DataView(dtTable, "icheck = true", "", DataViewRowState.CurrentRows).ToTable

                Dim mblnIsRejection As Boolean = False
                Dim iStatusunkid As clsEmployee_Master.EmpApprovalStatus
                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
                    Case btnReject.Name
                        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
                        mblnIsRejection = True
                End Select

                strCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())

                'S.SANDEEP |05-JUN-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-933,934
                Dim blnIsEnforceAttachmentOn As Boolean = False
                Select Case eFillType
                    Case clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                        blnIsEnforceAttachmentOn = ConfigParameter._Object._MakeAttachmentMandatoryOnTransfer
                    Case clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE
                        blnIsEnforceAttachmentOn = ConfigParameter._Object._MakeAttachmentMandatoryOnRecategorize
                End Select
                'S.SANDEEP |0-JUN-2023| -- END


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objApprovalTran.InsertAll(eFillType, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), dtTable, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, False, iStatusunkid, txtRemark.Text, CInt(txtApprover.Tag), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then
                If objApprovalTran.InsertAll(eFillType, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), dtTable, User._Object._Userunkid _
                                                      , ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, False, iStatusunkid, txtRemark.Text, CInt(txtApprover.Tag) _
                                                      , Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                      , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP, blnIsEnforceAttachmentOn, ConfigParameter._Object._Document_Path) Then
                    'S.SANDEEP |05-JUN-2023| -- START {blnIsEnforceAttachmentOn,ConfigParameter._Object._Document_Path} -- END

                    'Pinkal (12-Oct-2020) -- End

                    'S.SANDEEP |17-JAN-2019| -- START
                    'If mblnIsRejection = False Then
                    '    objApprovalTran.SendNotification(2, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, blnIsResidentPermit, eDtType, CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text)
                    'Else
                    '    objApprovalTran.SendNotification(3, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, blnIsResidentPermit, eDtType, CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text, ConfigParameter._Object._RejectTransferUserNotification, ConfigParameter._Object._RejectReCategorizationUserNotification, ConfigParameter._Object._RejectProbationUserNotification, ConfigParameter._Object._RejectConfirmationUserNotification, ConfigParameter._Object._RejectSuspensionUserNotification, ConfigParameter._Object._RejectTerminationUserNotification, ConfigParameter._Object._RejectRetirementUserNotification, ConfigParameter._Object._RejectWorkPermitUserNotification, ConfigParameter._Object._RejectResidentPermitUserNotification, ConfigParameter._Object._RejectCostCenterPermitUserNotification)
                    'End If

                    If mblnIsRejection = False Then
                        objApprovalTran.SendNotification(2, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), blnIsResidentPermit, eDtType, CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text)
                    Else
                        objApprovalTran.SendNotification(3, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), blnIsResidentPermit, eDtType, CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text, ConfigParameter._Object._RejectTransferUserNotification, ConfigParameter._Object._RejectReCategorizationUserNotification, ConfigParameter._Object._RejectProbationUserNotification, ConfigParameter._Object._RejectConfirmationUserNotification, ConfigParameter._Object._RejectSuspensionUserNotification, ConfigParameter._Object._RejectTerminationUserNotification, ConfigParameter._Object._RejectRetirementUserNotification, ConfigParameter._Object._RejectWorkPermitUserNotification, ConfigParameter._Object._RejectResidentPermitUserNotification, ConfigParameter._Object._RejectCostCenterPermitUserNotification, ConfigParameter._Object._RejectExemptionUserNotification)
                        'Sohail (21 Oct 2019) - [_RejectExemptionUserNotification]
                    End If
                    Call FillGrid()
                Else
                    If objApprovalTran._Message.Trim.Length > 0 Then
                        eZeeMsgBox.Show(objApprovalTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        Finally
            'Call FillGrid()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mdvData.Table.Rows.Clear()
            objbtnReset.ShowResult(dgvData.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If CInt(cboMovement.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Movement Type is mandatory information. Please select movement type to continue"), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [14-AUG-2018] -- END
            'S.SANDEEP |17-JAN-2019| -- START


            If CInt(cboOperationType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Opration Type is mandatory information. Please select Opration type to continue"), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |17-JAN-2019| -- END

            
            Call SetGridColums()
            Call FillGrid()

            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            'objbtnSearch.ShowResult(dgvData.RowCount.ToString)
            'Gajanan [9-NOV-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnShowMyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowMyReport.Click
        Dim frm As New frmViewMovementApproval
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'S.SANDEEP |17-JAN-2019| -- START
            'frm.displayDialog(User._Object._Userunkid, CInt(txtLevel.Tag), intPrivilegeId, eFillType, eDtType, blnIsResidentPermit) 'S.SANDEEP [09-AUG-2018] -- START -- END
            frm.displayDialog(User._Object._Userunkid, CInt(txtLevel.Tag), intPrivilegeId, eFillType, eDtType, blnIsResidentPermit, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType))
            'S.SANDEEP |17-JAN-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnShowMyReport_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboMovement_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMovement.SelectedIndexChanged
        Try
            If CInt(cboMovement.SelectedValue) > 0 Then
                Dim vrow As DataRowView = CType(cboMovement.SelectedItem, DataRowView)
                Dim row As DataRow = vrow.Row

                intPrivilegeId = CInt(row.Item("PrivilegeId"))
                eFillType = CType(row.Item("Id"), clsEmployeeMovmentApproval.enMovementType)
                eDtType = CType(row.Item("DatesId"), enEmp_Dates_Transaction)
                blnIsResidentPermit = CBool(row.Item("IsResident"))
                btnShowMyReport.Enabled = True
            Else
                intPrivilegeId = 0
                btnShowMyReport.Enabled = False
            End If

            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            Call FillGrid()
            'Gajanan [9-NOV-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMovement_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim str As String = ""
        Try
            If mdvData Is Nothing Then Exit Sub
            If (txtSearch.Text.Length > 0) Then
                str = GetFilterString(txtSearch.Text)
            End If
            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            'mdvData.RowFilter = str
            If str.Trim.Length > 0 Then

                If mstrDefaultSearchParameter.Trim.Length > 0 Then
                mdvData.RowFilter = mstrDefaultSearchParameter & " AND (" & str & ") "
            Else
                    mdvData.RowFilter = str
                End If
            Else
                mdvData.RowFilter = mstrDefaultSearchParameter
            End If
            'S.SANDEEP [09-AUG-2018] -- END
            objbtnSearch.ShowResult(dgvData.RowCount.ToString())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            If mdvData Is Nothing Then Exit Sub
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In mdvData
                dr.Item(objdgcolhicheck.DataPropertyName) = CBool(objchkAll.CheckState)
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Datagrid Event(s) "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnShowMyReport.GradientBackColor = GUI._ButttonBackColor
            Me.btnShowMyReport.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnReject.GradientBackColor = GUI._ButttonBackColor
            Me.btnReject.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
            Me.lblLoggedInUser.Text = Language._Object.getCaption(Me.lblLoggedInUser.Name, Me.lblLoggedInUser.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnShowMyReport.Text = Language._Object.getCaption(Me.btnShowMyReport.Name, Me.btnShowMyReport.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblMovement.Text = Language._Object.getCaption(Me.lblMovement.Name, Me.lblMovement.Text)
            Me.dgcolhecode.HeaderText = Language._Object.getCaption(Me.dgcolhecode.Name, Me.dgcolhecode.HeaderText)
            Me.dgcolhename.HeaderText = Language._Object.getCaption(Me.dgcolhename.Name, Me.dgcolhename.HeaderText)
            Me.dgcolhEffDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffDate.Name, Me.dgcolhEffDate.HeaderText)
            Me.dgcolhbranch.HeaderText = Language._Object.getCaption(Me.dgcolhbranch.Name, Me.dgcolhbranch.HeaderText)
            Me.dgcolhdeptgroup.HeaderText = Language._Object.getCaption(Me.dgcolhdeptgroup.Name, Me.dgcolhdeptgroup.HeaderText)
            Me.dgcolhdept.HeaderText = Language._Object.getCaption(Me.dgcolhdept.Name, Me.dgcolhdept.HeaderText)
            Me.dgcolhsecgroup.HeaderText = Language._Object.getCaption(Me.dgcolhsecgroup.Name, Me.dgcolhsecgroup.HeaderText)
            Me.dgcolhsection.HeaderText = Language._Object.getCaption(Me.dgcolhsection.Name, Me.dgcolhsection.HeaderText)
            Me.dgcolhunitgrp.HeaderText = Language._Object.getCaption(Me.dgcolhunitgrp.Name, Me.dgcolhunitgrp.HeaderText)
            Me.dgcolhunit.HeaderText = Language._Object.getCaption(Me.dgcolhunit.Name, Me.dgcolhunit.HeaderText)
            Me.dgcolhteam.HeaderText = Language._Object.getCaption(Me.dgcolhteam.Name, Me.dgcolhteam.HeaderText)
            Me.dgcolhclassgrp.HeaderText = Language._Object.getCaption(Me.dgcolhclassgrp.Name, Me.dgcolhclassgrp.HeaderText)
            Me.dgcolhclass.HeaderText = Language._Object.getCaption(Me.dgcolhclass.Name, Me.dgcolhclass.HeaderText)
            Me.dgcolhCReason.HeaderText = Language._Object.getCaption(Me.dgcolhCReason.Name, Me.dgcolhCReason.HeaderText)
            Me.dgcolhJobGroup.HeaderText = Language._Object.getCaption(Me.dgcolhJobGroup.Name, Me.dgcolhJobGroup.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhdDate1.HeaderText = Language._Object.getCaption(Me.dgcolhdDate1.Name, Me.dgcolhdDate1.HeaderText)
            Me.dgcolhdDate2.HeaderText = Language._Object.getCaption(Me.dgcolhdDate2.Name, Me.dgcolhdDate2.HeaderText)
            Me.dgcolhwork_permit_no.HeaderText = Language._Object.getCaption(Me.dgcolhwork_permit_no.Name, Me.dgcolhwork_permit_no.HeaderText)
            Me.dgcolhissue_place.HeaderText = Language._Object.getCaption(Me.dgcolhissue_place.Name, Me.dgcolhissue_place.HeaderText)
            Me.dgcolhIDate.HeaderText = Language._Object.getCaption(Me.dgcolhIDate.Name, Me.dgcolhIDate.HeaderText)
            Me.dgcolhExDate.HeaderText = Language._Object.getCaption(Me.dgcolhExDate.Name, Me.dgcolhExDate.HeaderText)
            Me.dgcolhCountry.HeaderText = Language._Object.getCaption(Me.dgcolhCountry.Name, Me.dgcolhCountry.HeaderText)
            Me.dgcolhDispValue.HeaderText = Language._Object.getCaption(Me.dgcolhDispValue.Name, Me.dgcolhDispValue.HeaderText)
	    Me.lblSelectOperationType.Text = Language._Object.getCaption(Me.lblSelectOperationType.Name, Me.lblSelectOperationType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to approve employee(s) without remark?")
            Language.setMessage(mstrModuleName, 4, "Movement Type is mandatory information. Please select movement type to continue")
			Language.setMessage(mstrModuleName, 5, "Leave is arleady Issued, Are you sure you want to reject Employee exemotion for this")
			Language.setMessage(mstrModuleName, 6, "If YES employee will be considered in Payroll pocess for those unpaid leave days.")
            Language.setMessage(mstrModuleName, 100, "Start Date")
            Language.setMessage(mstrModuleName, 101, "End Date")
            Language.setMessage(mstrModuleName, 102, "Confirmation Date")
            Language.setMessage(mstrModuleName, 103, "Retirement Date")
            Language.setMessage(mstrModuleName, 104, "EOC Date")
            Language.setMessage(mstrModuleName, 105, "Leaving Date")
			Language.setMessage(mstrModuleName, 106, "Opration Type is mandatory information. Please select Opration type to continue")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class