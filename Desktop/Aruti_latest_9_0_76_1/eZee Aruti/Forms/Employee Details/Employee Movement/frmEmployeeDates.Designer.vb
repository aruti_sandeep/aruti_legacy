﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeDates
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeDates))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbDatesInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpActualDate = New System.Windows.Forms.DateTimePicker
        Me.lnkSetSuspension = New System.Windows.Forms.LinkLabel
        Me.LblActualDate = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.txtPaymentDate = New System.Windows.Forms.TextBox
        Me.lblPaymentDate = New System.Windows.Forms.Label
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lblAppointmentdate = New System.Windows.Forms.Label
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.chkExclude = New System.Windows.Forms.CheckBox
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.objlblStartFrom = New System.Windows.Forms.Label
        Me.objSearchReason = New eZee.Common.eZeeGradientButton
        Me.lblReason = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhViewPending = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhOperationType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChangeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdatetranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFromEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActionReasonId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhExclude = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrehiretranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhallowopr = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhtranguid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOperationTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objlblEndTo = New System.Windows.Forms.Label
        Me.cboChangeReason = New System.Windows.Forms.ComboBox
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblPendingData = New System.Windows.Forms.Label
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSalaryChange = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBenefits = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTransfers = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRecategorization = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblEmplReason = New System.Windows.Forms.Label
        Me.cboEndEmplReason = New System.Windows.Forms.ComboBox
        Me.objbtnAddEndReason = New eZee.Common.eZeeGradientButton
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objgbDatesInformation.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbDatesInformation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(642, 426)
        Me.pnlMain.TabIndex = 1
        '
        'objgbDatesInformation
        '
        Me.objgbDatesInformation.BorderColor = System.Drawing.Color.Black
        Me.objgbDatesInformation.Checked = False
        Me.objgbDatesInformation.CollapseAllExceptThis = False
        Me.objgbDatesInformation.CollapsedHoverImage = Nothing
        Me.objgbDatesInformation.CollapsedNormalImage = Nothing
        Me.objgbDatesInformation.CollapsedPressedImage = Nothing
        Me.objgbDatesInformation.CollapseOnLoad = False
        Me.objgbDatesInformation.Controls.Add(Me.dtpActualDate)
        Me.objgbDatesInformation.Controls.Add(Me.lnkSetSuspension)
        Me.objgbDatesInformation.Controls.Add(Me.LblActualDate)
        Me.objgbDatesInformation.Controls.Add(Me.lnkAllocation)
        Me.objgbDatesInformation.Controls.Add(Me.txtPaymentDate)
        Me.objgbDatesInformation.Controls.Add(Me.lblPaymentDate)
        Me.objgbDatesInformation.Controls.Add(Me.pnlData)
        Me.objgbDatesInformation.Controls.Add(Me.chkExclude)
        Me.objgbDatesInformation.Controls.Add(Me.objbtnAddReason)
        Me.objgbDatesInformation.Controls.Add(Me.objelLine1)
        Me.objgbDatesInformation.Controls.Add(Me.dtpStartDate)
        Me.objgbDatesInformation.Controls.Add(Me.objlblStartFrom)
        Me.objgbDatesInformation.Controls.Add(Me.objSearchReason)
        Me.objgbDatesInformation.Controls.Add(Me.lblReason)
        Me.objgbDatesInformation.Controls.Add(Me.btnSave)
        Me.objgbDatesInformation.Controls.Add(Me.pnl1)
        Me.objgbDatesInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.objgbDatesInformation.Controls.Add(Me.objlblEndTo)
        Me.objgbDatesInformation.Controls.Add(Me.cboChangeReason)
        Me.objgbDatesInformation.Controls.Add(Me.dtpEffectiveDate)
        Me.objgbDatesInformation.Controls.Add(Me.cboEmployee)
        Me.objgbDatesInformation.Controls.Add(Me.dtpEndDate)
        Me.objgbDatesInformation.Controls.Add(Me.lblEffectiveDate)
        Me.objgbDatesInformation.Controls.Add(Me.lblEmployee)
        Me.objgbDatesInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objgbDatesInformation.ExpandedHoverImage = Nothing
        Me.objgbDatesInformation.ExpandedNormalImage = Nothing
        Me.objgbDatesInformation.ExpandedPressedImage = Nothing
        Me.objgbDatesInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbDatesInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbDatesInformation.HeaderHeight = 25
        Me.objgbDatesInformation.HeaderMessage = ""
        Me.objgbDatesInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbDatesInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbDatesInformation.HeightOnCollapse = 0
        Me.objgbDatesInformation.LeftTextSpace = 0
        Me.objgbDatesInformation.Location = New System.Drawing.Point(0, 0)
        Me.objgbDatesInformation.Name = "objgbDatesInformation"
        Me.objgbDatesInformation.OpenHeight = 300
        Me.objgbDatesInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbDatesInformation.ShowBorder = True
        Me.objgbDatesInformation.ShowCheckBox = False
        Me.objgbDatesInformation.ShowCollapseButton = False
        Me.objgbDatesInformation.ShowDefaultBorderColor = True
        Me.objgbDatesInformation.ShowDownButton = False
        Me.objgbDatesInformation.ShowHeader = True
        Me.objgbDatesInformation.Size = New System.Drawing.Size(642, 376)
        Me.objgbDatesInformation.TabIndex = 0
        Me.objgbDatesInformation.Temp = 0
        Me.objgbDatesInformation.Text = "Dates Information"
        Me.objgbDatesInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpActualDate
        '
        Me.dtpActualDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpActualDate.Checked = False
        Me.dtpActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpActualDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpActualDate.Location = New System.Drawing.Point(476, 92)
        Me.dtpActualDate.Name = "dtpActualDate"
        Me.dtpActualDate.ShowCheckBox = True
        Me.dtpActualDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpActualDate.TabIndex = 237
        '
        'lnkSetSuspension
        '
        Me.lnkSetSuspension.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetSuspension.Location = New System.Drawing.Point(213, 72)
        Me.lnkSetSuspension.Name = "lnkSetSuspension"
        Me.lnkSetSuspension.Size = New System.Drawing.Size(389, 13)
        Me.lnkSetSuspension.TabIndex = 239
        Me.lnkSetSuspension.TabStop = True
        Me.lnkSetSuspension.Text = "Set Suspension For Descipline"
        Me.lnkSetSuspension.Visible = False
        '
        'LblActualDate
        '
        Me.LblActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblActualDate.Location = New System.Drawing.Point(391, 95)
        Me.LblActualDate.Name = "LblActualDate"
        Me.LblActualDate.Size = New System.Drawing.Size(79, 15)
        Me.LblActualDate.TabIndex = 236
        Me.LblActualDate.Text = "Actual Date"
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(562, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(76, 13)
        Me.lnkAllocation.TabIndex = 234
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'txtPaymentDate
        '
        Me.txtPaymentDate.BackColor = System.Drawing.Color.White
        Me.txtPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaymentDate.Location = New System.Drawing.Point(291, 91)
        Me.txtPaymentDate.Name = "txtPaymentDate"
        Me.txtPaymentDate.ReadOnly = True
        Me.txtPaymentDate.Size = New System.Drawing.Size(94, 21)
        Me.txtPaymentDate.TabIndex = 215
        '
        'lblPaymentDate
        '
        Me.lblPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentDate.Location = New System.Drawing.Point(209, 94)
        Me.lblPaymentDate.Name = "lblPaymentDate"
        Me.lblPaymentDate.Size = New System.Drawing.Size(79, 15)
        Me.lblPaymentDate.TabIndex = 214
        Me.lblPaymentDate.Text = "Payment Date"
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.lblAppointmentdate)
        Me.pnlData.Controls.Add(Me.txtDate)
        Me.pnlData.Location = New System.Drawing.Point(345, 135)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(167, 25)
        Me.pnlData.TabIndex = 187
        '
        'lblAppointmentdate
        '
        Me.lblAppointmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentdate.Location = New System.Drawing.Point(3, 5)
        Me.lblAppointmentdate.Name = "lblAppointmentdate"
        Me.lblAppointmentdate.Size = New System.Drawing.Size(61, 15)
        Me.lblAppointmentdate.TabIndex = 183
        Me.lblAppointmentdate.Text = "Appt. Date"
        Me.lblAppointmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.Color.White
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(70, 2)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(94, 21)
        Me.txtDate.TabIndex = 184
        '
        'chkExclude
        '
        Me.chkExclude.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkExclude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExclude.Location = New System.Drawing.Point(216, 67)
        Me.chkExclude.Name = "chkExclude"
        Me.chkExclude.Size = New System.Drawing.Size(412, 17)
        Me.chkExclude.TabIndex = 212
        Me.chkExclude.Text = "Exclude From Payroll Process for EOC Date / Leaving Date"
        Me.chkExclude.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkExclude.UseVisualStyleBackColor = True
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(291, 137)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 207
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(12, 117)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(618, 9)
        Me.objelLine1.TabIndex = 205
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(101, 64)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpStartDate.TabIndex = 189
        '
        'objlblStartFrom
        '
        Me.objlblStartFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblStartFrom.Location = New System.Drawing.Point(12, 67)
        Me.objlblStartFrom.Name = "objlblStartFrom"
        Me.objlblStartFrom.Size = New System.Drawing.Size(83, 15)
        Me.objlblStartFrom.TabIndex = 188
        Me.objlblStartFrom.Text = "Start Date"
        Me.objlblStartFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objSearchReason
        '
        Me.objSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchReason.BorderSelected = False
        Me.objSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchReason.Location = New System.Drawing.Point(318, 137)
        Me.objSearchReason.Name = "objSearchReason"
        Me.objSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objSearchReason.TabIndex = 204
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(12, 140)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(83, 15)
        Me.lblReason.TabIndex = 202
        Me.lblReason.Text = "Reason"
        Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(518, 132)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(111, 30)
        Me.btnSave.TabIndex = 37
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.dgvHistory)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(12, 178)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(618, 192)
        Me.pnl1.TabIndex = 173
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhViewPending, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhOperationType, Me.dgcolhChangeDate, Me.objdgcolhStartDate, Me.objdgcolhEndDate, Me.dgcolhReason, Me.objdgcolhdatetranunkid, Me.objdgcolhFromEmp, Me.objdgcolhActionReasonId, Me.objdgcolhExclude, Me.objdgcolhAppointdate, Me.objdgcolhrehiretranunkid, Me.objdgcolhallowopr, Me.objdgcolhtranguid, Me.objdgcolhOperationTypeId})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.MultiSelect = False
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(618, 192)
        Me.dgvHistory.TabIndex = 0
        '
        'objdgcolhViewPending
        '
        Me.objdgcolhViewPending.Frozen = True
        Me.objdgcolhViewPending.HeaderText = ""
        Me.objdgcolhViewPending.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.objdgcolhViewPending.Name = "objdgcolhViewPending"
        Me.objdgcolhViewPending.ReadOnly = True
        Me.objdgcolhViewPending.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhViewPending.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhOperationType
        '
        Me.objdgcolhOperationType.Frozen = True
        Me.objdgcolhOperationType.HeaderText = "Operation Type"
        Me.objdgcolhOperationType.Name = "objdgcolhOperationType"
        Me.objdgcolhOperationType.ReadOnly = True
        Me.objdgcolhOperationType.Visible = False
        '
        'dgcolhChangeDate
        '
        Me.dgcolhChangeDate.Frozen = True
        Me.dgcolhChangeDate.HeaderText = "Effective Date"
        Me.dgcolhChangeDate.Name = "dgcolhChangeDate"
        Me.dgcolhChangeDate.ReadOnly = True
        Me.dgcolhChangeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhStartDate
        '
        Me.objdgcolhStartDate.Frozen = True
        Me.objdgcolhStartDate.HeaderText = ""
        Me.objdgcolhStartDate.Name = "objdgcolhStartDate"
        Me.objdgcolhStartDate.ReadOnly = True
        Me.objdgcolhStartDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhStartDate.Width = 120
        '
        'objdgcolhEndDate
        '
        Me.objdgcolhEndDate.Frozen = True
        Me.objdgcolhEndDate.HeaderText = ""
        Me.objdgcolhEndDate.Name = "objdgcolhEndDate"
        Me.objdgcolhEndDate.ReadOnly = True
        Me.objdgcolhEndDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEndDate.Width = 120
        '
        'dgcolhReason
        '
        Me.dgcolhReason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhReason.Frozen = True
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhReason.Width = 225
        '
        'objdgcolhdatetranunkid
        '
        Me.objdgcolhdatetranunkid.Frozen = True
        Me.objdgcolhdatetranunkid.HeaderText = "objdgcolhdatetranunkid"
        Me.objdgcolhdatetranunkid.Name = "objdgcolhdatetranunkid"
        Me.objdgcolhdatetranunkid.ReadOnly = True
        Me.objdgcolhdatetranunkid.Visible = False
        '
        'objdgcolhFromEmp
        '
        Me.objdgcolhFromEmp.Frozen = True
        Me.objdgcolhFromEmp.HeaderText = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.Name = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.ReadOnly = True
        Me.objdgcolhFromEmp.Visible = False
        '
        'objdgcolhActionReasonId
        '
        Me.objdgcolhActionReasonId.Frozen = True
        Me.objdgcolhActionReasonId.HeaderText = "objdgcolhActionReasonId"
        Me.objdgcolhActionReasonId.Name = "objdgcolhActionReasonId"
        Me.objdgcolhActionReasonId.ReadOnly = True
        Me.objdgcolhActionReasonId.Visible = False
        '
        'objdgcolhExclude
        '
        Me.objdgcolhExclude.Frozen = True
        Me.objdgcolhExclude.HeaderText = "objdgcolhExclude"
        Me.objdgcolhExclude.Name = "objdgcolhExclude"
        Me.objdgcolhExclude.ReadOnly = True
        Me.objdgcolhExclude.Visible = False
        '
        'objdgcolhAppointdate
        '
        Me.objdgcolhAppointdate.Frozen = True
        Me.objdgcolhAppointdate.HeaderText = "objdgcolhAppointdate"
        Me.objdgcolhAppointdate.Name = "objdgcolhAppointdate"
        Me.objdgcolhAppointdate.ReadOnly = True
        Me.objdgcolhAppointdate.Visible = False
        '
        'objdgcolhrehiretranunkid
        '
        Me.objdgcolhrehiretranunkid.Frozen = True
        Me.objdgcolhrehiretranunkid.HeaderText = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.Name = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.ReadOnly = True
        Me.objdgcolhrehiretranunkid.Visible = False
        '
        'objdgcolhallowopr
        '
        Me.objdgcolhallowopr.Frozen = True
        Me.objdgcolhallowopr.HeaderText = "objdgcolhallowopr"
        Me.objdgcolhallowopr.Name = "objdgcolhallowopr"
        Me.objdgcolhallowopr.ReadOnly = True
        Me.objdgcolhallowopr.Visible = False
        '
        'objdgcolhtranguid
        '
        Me.objdgcolhtranguid.Frozen = True
        Me.objdgcolhtranguid.HeaderText = "objdgcolhtranguid"
        Me.objdgcolhtranguid.Name = "objdgcolhtranguid"
        Me.objdgcolhtranguid.ReadOnly = True
        Me.objdgcolhtranguid.Visible = False
        '
        'objdgcolhOperationTypeId
        '
        Me.objdgcolhOperationTypeId.HeaderText = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.Name = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.ReadOnly = True
        Me.objdgcolhOperationTypeId.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(608, 37)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'objlblEndTo
        '
        Me.objlblEndTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEndTo.Location = New System.Drawing.Point(12, 94)
        Me.objlblEndTo.Name = "objlblEndTo"
        Me.objlblEndTo.Size = New System.Drawing.Size(83, 15)
        Me.objlblEndTo.TabIndex = 190
        Me.objlblEndTo.Text = "End Date"
        Me.objlblEndTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChangeReason
        '
        Me.cboChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeReason.DropDownWidth = 400
        Me.cboChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeReason.FormattingEnabled = True
        Me.cboChangeReason.Location = New System.Drawing.Point(101, 137)
        Me.cboChangeReason.Name = "cboChangeReason"
        Me.cboChangeReason.Size = New System.Drawing.Size(184, 21)
        Me.cboChangeReason.TabIndex = 203
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(101, 37)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpEffectiveDate.TabIndex = 1
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(291, 37)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(311, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(101, 91)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpEndDate.TabIndex = 191
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 40)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(83, 15)
        Me.lblEffectiveDate.TabIndex = 0
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(213, 40)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(72, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblPendingData)
        Me.objFooter.Controls.Add(Me.objlblCaption)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.lblEmplReason)
        Me.objFooter.Controls.Add(Me.cboEndEmplReason)
        Me.objFooter.Controls.Add(Me.objbtnAddEndReason)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 376)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(642, 50)
        Me.objFooter.TabIndex = 1
        '
        'lblPendingData
        '
        Me.lblPendingData.BackColor = System.Drawing.Color.PowderBlue
        Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendingData.ForeColor = System.Drawing.Color.Black
        Me.lblPendingData.Location = New System.Drawing.Point(375, 18)
        Me.lblPendingData.Name = "lblPendingData"
        Me.lblPendingData.Size = New System.Drawing.Size(155, 17)
        Me.lblPendingData.TabIndex = 90
        Me.lblPendingData.Text = "Pending Approval"
        Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblPendingData.Visible = False
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.Color.Orange
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Black
        Me.objlblCaption.Location = New System.Drawing.Point(121, 18)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(155, 17)
        Me.objlblCaption.TabIndex = 89
        Me.objlblCaption.Text = "Employee Rehired"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 11)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(103, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperations.TabIndex = 88
        Me.btnOperations.Text = "&Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSalaryChange, Me.mnuBenefits, Me.mnuTransfers, Me.mnuRecategorization})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(170, 92)
        '
        'mnuSalaryChange
        '
        Me.mnuSalaryChange.Name = "mnuSalaryChange"
        Me.mnuSalaryChange.Size = New System.Drawing.Size(169, 22)
        Me.mnuSalaryChange.Tag = "mnuSalaryChange"
        Me.mnuSalaryChange.Text = "Salary Change"
        '
        'mnuBenefits
        '
        Me.mnuBenefits.Name = "mnuBenefits"
        Me.mnuBenefits.Size = New System.Drawing.Size(169, 22)
        Me.mnuBenefits.Tag = "mnuBenefits"
        Me.mnuBenefits.Text = "Benefits"
        '
        'mnuTransfers
        '
        Me.mnuTransfers.Name = "mnuTransfers"
        Me.mnuTransfers.Size = New System.Drawing.Size(169, 22)
        Me.mnuTransfers.Tag = "mnuTransfers"
        Me.mnuTransfers.Text = "Transfers"
        '
        'mnuRecategorization
        '
        Me.mnuRecategorization.Name = "mnuRecategorization"
        Me.mnuRecategorization.Size = New System.Drawing.Size(169, 22)
        Me.mnuRecategorization.Tag = "mnuRecategorization"
        Me.mnuRecategorization.Text = "Re-Categorization"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(536, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblEmplReason
        '
        Me.lblEmplReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmplReason.Location = New System.Drawing.Point(288, 3)
        Me.lblEmplReason.Name = "lblEmplReason"
        Me.lblEmplReason.Size = New System.Drawing.Size(72, 15)
        Me.lblEmplReason.TabIndex = 209
        Me.lblEmplReason.Text = "EOC Reason"
        Me.lblEmplReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEmplReason.Visible = False
        '
        'cboEndEmplReason
        '
        Me.cboEndEmplReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEndEmplReason.DropDownWidth = 120
        Me.cboEndEmplReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEndEmplReason.FormattingEnabled = True
        Me.cboEndEmplReason.Location = New System.Drawing.Point(291, 26)
        Me.cboEndEmplReason.Name = "cboEndEmplReason"
        Me.cboEndEmplReason.Size = New System.Drawing.Size(33, 21)
        Me.cboEndEmplReason.TabIndex = 210
        Me.cboEndEmplReason.Visible = False
        '
        'objbtnAddEndReason
        '
        Me.objbtnAddEndReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddEndReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddEndReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddEndReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddEndReason.BorderSelected = False
        Me.objbtnAddEndReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddEndReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddEndReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddEndReason.Location = New System.Drawing.Point(330, 26)
        Me.objbtnAddEndReason.Name = "objbtnAddEndReason"
        Me.objbtnAddEndReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddEndReason.TabIndex = 211
        Me.objbtnAddEndReason.Visible = False
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(12, 167)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(618, 8)
        Me.objelLine2.TabIndex = 37
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Effective Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = ""
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = ""
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Reason"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolhdatetranunkid"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        Me.DataGridViewTextBoxColumn5.Width = 225
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.Frozen = True
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhFromEmp"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.Frozen = True
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhActionReasonId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.Frozen = True
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhExclude"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.Frozen = True
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhAppointdate"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.Frozen = True
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhrehiretranunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.Frozen = True
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhrehiretranunkid"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.Frozen = True
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhallowopr"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.Frozen = True
        Me.DataGridViewTextBoxColumn13.HeaderText = "objdgcolhtranguid"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhOperationTypeId"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'frmEmployeeDates
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 426)
        Me.Controls.Add(Me.objelLine2)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeDates"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Dates"
        Me.pnlMain.ResumeLayout(False)
        Me.objgbDatesInformation.ResumeLayout(False)
        Me.objgbDatesInformation.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.pnl1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objgbDatesInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblStartFrom As System.Windows.Forms.Label
    Friend WithEvents objlblEndTo As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents cboChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents objSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddEndReason As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEndEmplReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmplReason As System.Windows.Forms.Label
    Friend WithEvents chkExclude As System.Windows.Forms.CheckBox
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSalaryChange As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBenefits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTransfers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecategorization As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblAppointmentdate As System.Windows.Forms.Label
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
    Friend WithEvents objdgcolhViewPending As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhOperationType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChangeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhdatetranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFromEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActionReasonId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhExclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrehiretranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhallowopr As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhtranguid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOperationTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtPaymentDate As System.Windows.Forms.TextBox
    Friend WithEvents lblPaymentDate As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents dtpActualDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblActualDate As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkSetSuspension As System.Windows.Forms.LinkLabel
End Class
