﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpSubstantive_Job
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpSubstantive_Job))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbSubstantiveInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.objlblStartFrom = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblRemark = New System.Windows.Forms.Label
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSubstantivePost = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhsubstantivetranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhJobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrehiretranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objlblEndTo = New System.Windows.Forms.Label
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.objgbSubstantiveInformation.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbSubstantiveInformation)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(640, 454)
        Me.pnlMain.TabIndex = 0
        '
        'objgbSubstantiveInformation
        '
        Me.objgbSubstantiveInformation.BorderColor = System.Drawing.Color.Black
        Me.objgbSubstantiveInformation.Checked = False
        Me.objgbSubstantiveInformation.CollapseAllExceptThis = False
        Me.objgbSubstantiveInformation.CollapsedHoverImage = Nothing
        Me.objgbSubstantiveInformation.CollapsedNormalImage = Nothing
        Me.objgbSubstantiveInformation.CollapsedPressedImage = Nothing
        Me.objgbSubstantiveInformation.CollapseOnLoad = False
        Me.objgbSubstantiveInformation.Controls.Add(Me.cboJob)
        Me.objgbSubstantiveInformation.Controls.Add(Me.lblJob)
        Me.objgbSubstantiveInformation.Controls.Add(Me.objbtnSearchJob)
        Me.objgbSubstantiveInformation.Controls.Add(Me.txtRemark)
        Me.objgbSubstantiveInformation.Controls.Add(Me.objelLine1)
        Me.objgbSubstantiveInformation.Controls.Add(Me.dtpStartDate)
        Me.objgbSubstantiveInformation.Controls.Add(Me.objlblStartFrom)
        Me.objgbSubstantiveInformation.Controls.Add(Me.btnClose)
        Me.objgbSubstantiveInformation.Controls.Add(Me.lblRemark)
        Me.objgbSubstantiveInformation.Controls.Add(Me.pnl1)
        Me.objgbSubstantiveInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.objgbSubstantiveInformation.Controls.Add(Me.objlblEndTo)
        Me.objgbSubstantiveInformation.Controls.Add(Me.dtpEffectiveDate)
        Me.objgbSubstantiveInformation.Controls.Add(Me.cboEmployee)
        Me.objgbSubstantiveInformation.Controls.Add(Me.dtpEndDate)
        Me.objgbSubstantiveInformation.Controls.Add(Me.btnSave)
        Me.objgbSubstantiveInformation.Controls.Add(Me.lblEffectiveDate)
        Me.objgbSubstantiveInformation.Controls.Add(Me.lblEmployee)
        Me.objgbSubstantiveInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objgbSubstantiveInformation.ExpandedHoverImage = Nothing
        Me.objgbSubstantiveInformation.ExpandedNormalImage = Nothing
        Me.objgbSubstantiveInformation.ExpandedPressedImage = Nothing
        Me.objgbSubstantiveInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbSubstantiveInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbSubstantiveInformation.HeaderHeight = 25
        Me.objgbSubstantiveInformation.HeaderMessage = ""
        Me.objgbSubstantiveInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbSubstantiveInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbSubstantiveInformation.HeightOnCollapse = 0
        Me.objgbSubstantiveInformation.LeftTextSpace = 0
        Me.objgbSubstantiveInformation.Location = New System.Drawing.Point(0, 0)
        Me.objgbSubstantiveInformation.Name = "objgbSubstantiveInformation"
        Me.objgbSubstantiveInformation.OpenHeight = 300
        Me.objgbSubstantiveInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbSubstantiveInformation.ShowBorder = True
        Me.objgbSubstantiveInformation.ShowCheckBox = False
        Me.objgbSubstantiveInformation.ShowCollapseButton = False
        Me.objgbSubstantiveInformation.ShowDefaultBorderColor = True
        Me.objgbSubstantiveInformation.ShowDownButton = False
        Me.objgbSubstantiveInformation.ShowHeader = True
        Me.objgbSubstantiveInformation.Size = New System.Drawing.Size(640, 454)
        Me.objgbSubstantiveInformation.TabIndex = 1
        Me.objgbSubstantiveInformation.Temp = 0
        Me.objgbSubstantiveInformation.Text = "Substantive Post Information"
        Me.objgbSubstantiveInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboJob.DropDownWidth = 400
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(291, 59)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(311, 21)
        Me.cboJob.TabIndex = 244
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(213, 62)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(72, 15)
        Me.lblJob.TabIndex = 243
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(609, 59)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 245
        '
        'txtRemark
        '
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(291, 89)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(339, 32)
        Me.txtRemark.TabIndex = 241
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 164)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(618, 4)
        Me.objelLine1.TabIndex = 205
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(101, 59)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpStartDate.TabIndex = 189
        '
        'objlblStartFrom
        '
        Me.objlblStartFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblStartFrom.Location = New System.Drawing.Point(12, 62)
        Me.objlblStartFrom.Name = "objlblStartFrom"
        Me.objlblStartFrom.Size = New System.Drawing.Size(83, 15)
        Me.objlblStartFrom.TabIndex = 188
        Me.objlblStartFrom.Text = "Start Date"
        Me.objlblStartFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(540, 129)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(213, 89)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(72, 15)
        Me.lblRemark.TabIndex = 202
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.dgvHistory)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(12, 171)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(618, 274)
        Me.pnl1.TabIndex = 173
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhEffectiveDate, Me.dgcolhStartDate, Me.dgcolhEndDate, Me.dgcolhSubstantivePost, Me.dgcolhRemark, Me.objdgcolhsubstantivetranunkid, Me.objdgcolhEmployeeId, Me.objdgcolhJobId, Me.objdgcolhrehiretranunkid})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.MultiSelect = False
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(618, 274)
        Me.dgvHistory.TabIndex = 0
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhEffectiveDate
        '
        Me.dgcolhEffectiveDate.Frozen = True
        Me.dgcolhEffectiveDate.HeaderText = "Effective Date"
        Me.dgcolhEffectiveDate.Name = "dgcolhEffectiveDate"
        Me.dgcolhEffectiveDate.ReadOnly = True
        Me.dgcolhEffectiveDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStartDate
        '
        Me.dgcolhStartDate.HeaderText = "Start Date"
        Me.dgcolhStartDate.Name = "dgcolhStartDate"
        Me.dgcolhStartDate.ReadOnly = True
        Me.dgcolhStartDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStartDate.Width = 120
        '
        'dgcolhEndDate
        '
        Me.dgcolhEndDate.HeaderText = "End Date"
        Me.dgcolhEndDate.Name = "dgcolhEndDate"
        Me.dgcolhEndDate.ReadOnly = True
        Me.dgcolhEndDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEndDate.Width = 120
        '
        'dgcolhSubstantivePost
        '
        Me.dgcolhSubstantivePost.HeaderText = "Substantive Post"
        Me.dgcolhSubstantivePost.Name = "dgcolhSubstantivePost"
        Me.dgcolhSubstantivePost.ReadOnly = True
        Me.dgcolhSubstantivePost.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSubstantivePost.Width = 150
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRemark.Width = 150
        '
        'objdgcolhsubstantivetranunkid
        '
        Me.objdgcolhsubstantivetranunkid.HeaderText = "Substantivetranunkid"
        Me.objdgcolhsubstantivetranunkid.Name = "objdgcolhsubstantivetranunkid"
        Me.objdgcolhsubstantivetranunkid.ReadOnly = True
        Me.objdgcolhsubstantivetranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhsubstantivetranunkid.Visible = False
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "EmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objdgcolhJobId
        '
        Me.objdgcolhJobId.HeaderText = "JobId"
        Me.objdgcolhJobId.Name = "objdgcolhJobId"
        Me.objdgcolhJobId.ReadOnly = True
        Me.objdgcolhJobId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhJobId.Visible = False
        '
        'objdgcolhrehiretranunkid
        '
        Me.objdgcolhrehiretranunkid.HeaderText = "Rehiretranunkid"
        Me.objdgcolhrehiretranunkid.Name = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.ReadOnly = True
        Me.objdgcolhrehiretranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhrehiretranunkid.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(609, 32)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'objlblEndTo
        '
        Me.objlblEndTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEndTo.Location = New System.Drawing.Point(12, 89)
        Me.objlblEndTo.Name = "objlblEndTo"
        Me.objlblEndTo.Size = New System.Drawing.Size(83, 15)
        Me.objlblEndTo.TabIndex = 190
        Me.objlblEndTo.Text = "End Date"
        Me.objlblEndTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(101, 32)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpEffectiveDate.TabIndex = 1
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(291, 32)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(311, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(101, 86)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpEndDate.TabIndex = 191
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(423, 129)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(111, 30)
        Me.btnSave.TabIndex = 37
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 35)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(83, 15)
        Me.lblEffectiveDate.TabIndex = 0
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(213, 35)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(72, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'frmEmpSubstantive_Job
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 454)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpSubstantive_Job"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Substantive Job"
        Me.pnlMain.ResumeLayout(False)
        Me.objgbSubstantiveInformation.ResumeLayout(False)
        Me.objgbSubstantiveInformation.PerformLayout()
        Me.pnl1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objgbSubstantiveInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblStartFrom As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblEndTo As System.Windows.Forms.Label
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSubstantivePost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhsubstantivetranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhJobId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrehiretranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
