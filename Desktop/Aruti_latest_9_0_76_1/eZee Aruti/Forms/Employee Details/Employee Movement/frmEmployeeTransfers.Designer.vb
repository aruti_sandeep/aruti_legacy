﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeTransfers
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeTransfers))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbTransferInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lblAppointmentdate = New System.Windows.Forms.Label
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhViewPending = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhOperationType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChangeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartmentGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSecGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUnitGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUnit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTeam = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClass = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhtransferunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFromEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRehiretranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhtranguid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOperationTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchClassGrp = New eZee.Common.eZeeGradientButton
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.objbtnSearchClass = New eZee.Common.eZeeGradientButton
        Me.lblClass = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.elLine1 = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboStation = New System.Windows.Forms.ComboBox
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblSection = New System.Windows.Forms.Label
        Me.objbtnSearchUnits = New eZee.Common.eZeeGradientButton
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTeam = New eZee.Common.eZeeGradientButton
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.objbtnSearchUnitGrp = New eZee.Common.eZeeGradientButton
        Me.lblUnits = New System.Windows.Forms.Label
        Me.objbtnSearchSection = New eZee.Common.eZeeGradientButton
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDepartment = New eZee.Common.eZeeGradientButton
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.objbtnSearchSecGroup = New eZee.Common.eZeeGradientButton
        Me.lblBranch = New System.Windows.Forms.Label
        Me.objbtnSearchDeptGrp = New eZee.Common.eZeeGradientButton
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchBranch = New eZee.Common.eZeeGradientButton
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblPendingData = New System.Windows.Forms.Label
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objSearchReason = New eZee.Common.eZeeGradientButton
        Me.cboChangeReason = New System.Windows.Forms.ComboBox
        Me.lblChangeReason = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.pnlMain.SuspendLayout()
        Me.gbTransferInformation.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbTransferInformation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(696, 529)
        Me.pnlMain.TabIndex = 0
        '
        'gbTransferInformation
        '
        Me.gbTransferInformation.BorderColor = System.Drawing.Color.Black
        Me.gbTransferInformation.Checked = False
        Me.gbTransferInformation.CollapseAllExceptThis = False
        Me.gbTransferInformation.CollapsedHoverImage = Nothing
        Me.gbTransferInformation.CollapsedNormalImage = Nothing
        Me.gbTransferInformation.CollapsedPressedImage = Nothing
        Me.gbTransferInformation.CollapseOnLoad = False
        Me.gbTransferInformation.Controls.Add(Me.lnkAllocation)
        Me.gbTransferInformation.Controls.Add(Me.objbtnAddReason)
        Me.gbTransferInformation.Controls.Add(Me.pnlData)
        Me.gbTransferInformation.Controls.Add(Me.btnSave)
        Me.gbTransferInformation.Controls.Add(Me.cboClassGroup)
        Me.gbTransferInformation.Controls.Add(Me.lblClassGroup)
        Me.gbTransferInformation.Controls.Add(Me.pnl1)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchClassGrp)
        Me.gbTransferInformation.Controls.Add(Me.objelLine2)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchClass)
        Me.gbTransferInformation.Controls.Add(Me.lblClass)
        Me.gbTransferInformation.Controls.Add(Me.cboClass)
        Me.gbTransferInformation.Controls.Add(Me.elLine1)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbTransferInformation.Controls.Add(Me.dtpEffectiveDate)
        Me.gbTransferInformation.Controls.Add(Me.cboEmployee)
        Me.gbTransferInformation.Controls.Add(Me.lblEffectiveDate)
        Me.gbTransferInformation.Controls.Add(Me.lblEmployee)
        Me.gbTransferInformation.Controls.Add(Me.cboStation)
        Me.gbTransferInformation.Controls.Add(Me.cboSections)
        Me.gbTransferInformation.Controls.Add(Me.lblDepartment)
        Me.gbTransferInformation.Controls.Add(Me.lblSection)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchUnits)
        Me.gbTransferInformation.Controls.Add(Me.cboDepartment)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchTeam)
        Me.gbTransferInformation.Controls.Add(Me.lblDepartmentGroup)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchUnitGrp)
        Me.gbTransferInformation.Controls.Add(Me.lblUnits)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchSection)
        Me.gbTransferInformation.Controls.Add(Me.cboUnits)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchDepartment)
        Me.gbTransferInformation.Controls.Add(Me.cboDepartmentGrp)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchSecGroup)
        Me.gbTransferInformation.Controls.Add(Me.lblBranch)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchDeptGrp)
        Me.gbTransferInformation.Controls.Add(Me.cboSectionGroup)
        Me.gbTransferInformation.Controls.Add(Me.objbtnSearchBranch)
        Me.gbTransferInformation.Controls.Add(Me.lblSectionGroup)
        Me.gbTransferInformation.Controls.Add(Me.lblTeam)
        Me.gbTransferInformation.Controls.Add(Me.cboUnitGroup)
        Me.gbTransferInformation.Controls.Add(Me.cboTeams)
        Me.gbTransferInformation.Controls.Add(Me.lblUnitGroup)
        Me.gbTransferInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTransferInformation.ExpandedHoverImage = Nothing
        Me.gbTransferInformation.ExpandedNormalImage = Nothing
        Me.gbTransferInformation.ExpandedPressedImage = Nothing
        Me.gbTransferInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTransferInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTransferInformation.HeaderHeight = 25
        Me.gbTransferInformation.HeaderMessage = ""
        Me.gbTransferInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbTransferInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTransferInformation.HeightOnCollapse = 0
        Me.gbTransferInformation.LeftTextSpace = 0
        Me.gbTransferInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbTransferInformation.Name = "gbTransferInformation"
        Me.gbTransferInformation.OpenHeight = 300
        Me.gbTransferInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTransferInformation.ShowBorder = True
        Me.gbTransferInformation.ShowCheckBox = False
        Me.gbTransferInformation.ShowCollapseButton = False
        Me.gbTransferInformation.ShowDefaultBorderColor = True
        Me.gbTransferInformation.ShowDownButton = False
        Me.gbTransferInformation.ShowHeader = True
        Me.gbTransferInformation.Size = New System.Drawing.Size(696, 479)
        Me.gbTransferInformation.TabIndex = 0
        Me.gbTransferInformation.Temp = 0
        Me.gbTransferInformation.Text = "Transfer Information"
        Me.gbTransferInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(326, 235)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 188
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.lblAppointmentdate)
        Me.pnlData.Controls.Add(Me.txtDate)
        Me.pnlData.Location = New System.Drawing.Point(378, 233)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(204, 25)
        Me.pnlData.TabIndex = 186
        '
        'lblAppointmentdate
        '
        Me.lblAppointmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentdate.Location = New System.Drawing.Point(3, 5)
        Me.lblAppointmentdate.Name = "lblAppointmentdate"
        Me.lblAppointmentdate.Size = New System.Drawing.Size(79, 15)
        Me.lblAppointmentdate.TabIndex = 183
        Me.lblAppointmentdate.Text = "Appoint. Date"
        Me.lblAppointmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.Color.White
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(88, 2)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(112, 21)
        Me.txtDate.TabIndex = 184
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(583, 230)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(101, 30)
        Me.btnSave.TabIndex = 37
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.DropDownWidth = 260
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(445, 163)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(211, 21)
        Me.cboClassGroup.TabIndex = 31
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(357, 166)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(82, 15)
        Me.lblClassGroup.TabIndex = 30
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.dgvHistory)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(12, 266)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(672, 207)
        Me.pnl1.TabIndex = 173
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhViewPending, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhOperationType, Me.dgcolhChangeDate, Me.dgcolhBranch, Me.dgcolhDepartmentGrp, Me.dgcolhDepartment, Me.dgcolhSecGroup, Me.dgcolhSection, Me.dgcolhUnitGrp, Me.dgcolhUnit, Me.dgcolhTeam, Me.dgcolhClsGrp, Me.dgcolhClass, Me.dgcolhReason, Me.objdgcolhtransferunkid, Me.objdgcolhFromEmp, Me.objdgcolhAppointdate, Me.objdgcolhRehiretranunkid, Me.objdgcolhtranguid, Me.objdgcolhOperationTypeId})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.MultiSelect = False
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(672, 207)
        Me.dgvHistory.TabIndex = 0
        '
        'objdgcolhViewPending
        '
        Me.objdgcolhViewPending.Frozen = True
        Me.objdgcolhViewPending.HeaderText = ""
        Me.objdgcolhViewPending.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.objdgcolhViewPending.Name = "objdgcolhViewPending"
        Me.objdgcolhViewPending.ReadOnly = True
        Me.objdgcolhViewPending.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhViewPending.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhOperationType
        '
        Me.objdgcolhOperationType.Frozen = True
        Me.objdgcolhOperationType.HeaderText = "Operation Type"
        Me.objdgcolhOperationType.Name = "objdgcolhOperationType"
        Me.objdgcolhOperationType.ReadOnly = True
        Me.objdgcolhOperationType.Visible = False
        Me.objdgcolhOperationType.Width = 110
        '
        'dgcolhChangeDate
        '
        Me.dgcolhChangeDate.HeaderText = "Effective Date"
        Me.dgcolhChangeDate.Name = "dgcolhChangeDate"
        Me.dgcolhChangeDate.ReadOnly = True
        Me.dgcolhChangeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhBranch
        '
        Me.dgcolhBranch.HeaderText = "Branch"
        Me.dgcolhBranch.Name = "dgcolhBranch"
        Me.dgcolhBranch.ReadOnly = True
        Me.dgcolhBranch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDepartmentGrp
        '
        Me.dgcolhDepartmentGrp.HeaderText = "Dept. Group"
        Me.dgcolhDepartmentGrp.Name = "dgcolhDepartmentGrp"
        Me.dgcolhDepartmentGrp.ReadOnly = True
        Me.dgcolhDepartmentGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDepartment
        '
        Me.dgcolhDepartment.HeaderText = "Department"
        Me.dgcolhDepartment.Name = "dgcolhDepartment"
        Me.dgcolhDepartment.ReadOnly = True
        Me.dgcolhDepartment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSecGroup
        '
        Me.dgcolhSecGroup.HeaderText = "Sec. Group"
        Me.dgcolhSecGroup.Name = "dgcolhSecGroup"
        Me.dgcolhSecGroup.ReadOnly = True
        Me.dgcolhSecGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSection
        '
        Me.dgcolhSection.HeaderText = "Sections"
        Me.dgcolhSection.Name = "dgcolhSection"
        Me.dgcolhSection.ReadOnly = True
        Me.dgcolhSection.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhUnitGrp
        '
        Me.dgcolhUnitGrp.HeaderText = "Unit Group"
        Me.dgcolhUnitGrp.Name = "dgcolhUnitGrp"
        Me.dgcolhUnitGrp.ReadOnly = True
        Me.dgcolhUnitGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhUnit
        '
        Me.dgcolhUnit.HeaderText = "Unit"
        Me.dgcolhUnit.Name = "dgcolhUnit"
        Me.dgcolhUnit.ReadOnly = True
        Me.dgcolhUnit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTeam
        '
        Me.dgcolhTeam.HeaderText = "Team"
        Me.dgcolhTeam.Name = "dgcolhTeam"
        Me.dgcolhTeam.ReadOnly = True
        Me.dgcolhTeam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhClsGrp
        '
        Me.dgcolhClsGrp.HeaderText = "Class Group"
        Me.dgcolhClsGrp.Name = "dgcolhClsGrp"
        Me.dgcolhClsGrp.ReadOnly = True
        Me.dgcolhClsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhClass
        '
        Me.dgcolhClass.HeaderText = "Class"
        Me.dgcolhClass.Name = "dgcolhClass"
        Me.dgcolhClass.ReadOnly = True
        Me.dgcolhClass.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhReason
        '
        Me.dgcolhReason.HeaderText = "Change Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.Width = 150
        '
        'objdgcolhtransferunkid
        '
        Me.objdgcolhtransferunkid.HeaderText = "objdgcolhtransferunkid"
        Me.objdgcolhtransferunkid.Name = "objdgcolhtransferunkid"
        Me.objdgcolhtransferunkid.ReadOnly = True
        Me.objdgcolhtransferunkid.Visible = False
        '
        'objdgcolhFromEmp
        '
        Me.objdgcolhFromEmp.HeaderText = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.Name = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.ReadOnly = True
        Me.objdgcolhFromEmp.Visible = False
        '
        'objdgcolhAppointdate
        '
        Me.objdgcolhAppointdate.HeaderText = "dgcolhAppointdate"
        Me.objdgcolhAppointdate.Name = "objdgcolhAppointdate"
        Me.objdgcolhAppointdate.ReadOnly = True
        Me.objdgcolhAppointdate.Visible = False
        '
        'objdgcolhRehiretranunkid
        '
        Me.objdgcolhRehiretranunkid.HeaderText = "objdgcolhRehiretranunkid"
        Me.objdgcolhRehiretranunkid.Name = "objdgcolhRehiretranunkid"
        Me.objdgcolhRehiretranunkid.ReadOnly = True
        Me.objdgcolhRehiretranunkid.Visible = False
        '
        'objdgcolhtranguid
        '
        Me.objdgcolhtranguid.HeaderText = "objdgcolhtranguid"
        Me.objdgcolhtranguid.Name = "objdgcolhtranguid"
        Me.objdgcolhtranguid.ReadOnly = True
        Me.objdgcolhtranguid.Visible = False
        '
        'objdgcolhOperationTypeId
        '
        Me.objdgcolhOperationTypeId.HeaderText = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.Name = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.ReadOnly = True
        Me.objdgcolhOperationTypeId.Visible = False
        '
        'objbtnSearchClassGrp
        '
        Me.objbtnSearchClassGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClassGrp.BorderSelected = False
        Me.objbtnSearchClassGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClassGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClassGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClassGrp.Location = New System.Drawing.Point(662, 163)
        Me.objbtnSearchClassGrp.Name = "objbtnSearchClassGrp"
        Me.objbtnSearchClassGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClassGrp.TabIndex = 32
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(12, 214)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(672, 13)
        Me.objelLine2.TabIndex = 36
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchClass
        '
        Me.objbtnSearchClass.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClass.BorderSelected = False
        Me.objbtnSearchClass.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClass.Location = New System.Drawing.Point(662, 190)
        Me.objbtnSearchClass.Name = "objbtnSearchClass"
        Me.objbtnSearchClass.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClass.TabIndex = 35
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(357, 193)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(82, 15)
        Me.lblClass.TabIndex = 33
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.DropDownWidth = 260
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(445, 190)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(211, 21)
        Me.cboClass.TabIndex = 34
        '
        'elLine1
        '
        Me.elLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine1.Location = New System.Drawing.Point(12, 63)
        Me.elLine1.Name = "elLine1"
        Me.elLine1.Size = New System.Drawing.Size(672, 14)
        Me.elLine1.TabIndex = 5
        Me.elLine1.Text = "Allocations"
        Me.elLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(662, 37)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(109, 37)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(114, 21)
        Me.dtpEffectiveDate.TabIndex = 1
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(307, 37)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(349, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 40)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(91, 15)
        Me.lblEffectiveDate.TabIndex = 0
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(229, 40)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(72, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'cboStation
        '
        Me.cboStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStation.DropDownWidth = 260
        Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStation.FormattingEnabled = True
        Me.cboStation.Location = New System.Drawing.Point(109, 82)
        Me.cboStation.Name = "cboStation"
        Me.cboStation.Size = New System.Drawing.Size(211, 21)
        Me.cboStation.TabIndex = 7
        '
        'cboSections
        '
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.DropDownWidth = 260
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(109, 190)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(211, 21)
        Me.cboSections.TabIndex = 19
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(12, 139)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(91, 15)
        Me.lblDepartment.TabIndex = 12
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(12, 193)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(91, 15)
        Me.lblSection.TabIndex = 18
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUnits
        '
        Me.objbtnSearchUnits.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnits.BorderSelected = False
        Me.objbtnSearchUnits.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnits.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnits.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnits.Location = New System.Drawing.Point(662, 109)
        Me.objbtnSearchUnits.Name = "objbtnSearchUnits"
        Me.objbtnSearchUnits.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnits.TabIndex = 26
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 260
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(109, 136)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(211, 21)
        Me.cboDepartment.TabIndex = 13
        '
        'objbtnSearchTeam
        '
        Me.objbtnSearchTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTeam.BorderSelected = False
        Me.objbtnSearchTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTeam.Location = New System.Drawing.Point(662, 136)
        Me.objbtnSearchTeam.Name = "objbtnSearchTeam"
        Me.objbtnSearchTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTeam.TabIndex = 29
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(12, 112)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(91, 15)
        Me.lblDepartmentGroup.TabIndex = 9
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUnitGrp
        '
        Me.objbtnSearchUnitGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnitGrp.BorderSelected = False
        Me.objbtnSearchUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnitGrp.Location = New System.Drawing.Point(663, 82)
        Me.objbtnSearchUnitGrp.Name = "objbtnSearchUnitGrp"
        Me.objbtnSearchUnitGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnitGrp.TabIndex = 23
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(357, 112)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(82, 15)
        Me.lblUnits.TabIndex = 24
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSection
        '
        Me.objbtnSearchSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSection.BorderSelected = False
        Me.objbtnSearchSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSection.Location = New System.Drawing.Point(326, 190)
        Me.objbtnSearchSection.Name = "objbtnSearchSection"
        Me.objbtnSearchSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSection.TabIndex = 20
        '
        'cboUnits
        '
        Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnits.DropDownWidth = 260
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.Location = New System.Drawing.Point(445, 109)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(211, 21)
        Me.cboUnits.TabIndex = 25
        '
        'objbtnSearchDepartment
        '
        Me.objbtnSearchDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDepartment.BorderSelected = False
        Me.objbtnSearchDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDepartment.Location = New System.Drawing.Point(326, 136)
        Me.objbtnSearchDepartment.Name = "objbtnSearchDepartment"
        Me.objbtnSearchDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDepartment.TabIndex = 14
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGrp.DropDownWidth = 260
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(109, 109)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(211, 21)
        Me.cboDepartmentGrp.TabIndex = 10
        '
        'objbtnSearchSecGroup
        '
        Me.objbtnSearchSecGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSecGroup.BorderSelected = False
        Me.objbtnSearchSecGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSecGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSecGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSecGroup.Location = New System.Drawing.Point(326, 163)
        Me.objbtnSearchSecGroup.Name = "objbtnSearchSecGroup"
        Me.objbtnSearchSecGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSecGroup.TabIndex = 17
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(12, 85)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(91, 15)
        Me.lblBranch.TabIndex = 6
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchDeptGrp
        '
        Me.objbtnSearchDeptGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeptGrp.BorderSelected = False
        Me.objbtnSearchDeptGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeptGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeptGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeptGrp.Location = New System.Drawing.Point(326, 109)
        Me.objbtnSearchDeptGrp.Name = "objbtnSearchDeptGrp"
        Me.objbtnSearchDeptGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeptGrp.TabIndex = 11
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.DropDownWidth = 260
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(109, 163)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(211, 21)
        Me.cboSectionGroup.TabIndex = 16
        '
        'objbtnSearchBranch
        '
        Me.objbtnSearchBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBranch.BorderSelected = False
        Me.objbtnSearchBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBranch.Location = New System.Drawing.Point(326, 82)
        Me.objbtnSearchBranch.Name = "objbtnSearchBranch"
        Me.objbtnSearchBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBranch.TabIndex = 8
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(12, 166)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(91, 15)
        Me.lblSectionGroup.TabIndex = 15
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(357, 139)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(82, 15)
        Me.lblTeam.TabIndex = 27
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.DropDownWidth = 260
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(445, 82)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(211, 21)
        Me.cboUnitGroup.TabIndex = 22
        '
        'cboTeams
        '
        Me.cboTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeams.DropDownWidth = 260
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(445, 136)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(211, 21)
        Me.cboTeams.TabIndex = 28
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(357, 85)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(82, 15)
        Me.lblUnitGroup.TabIndex = 21
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblPendingData)
        Me.objFooter.Controls.Add(Me.objlblCaption)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 479)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(696, 50)
        Me.objFooter.TabIndex = 1
        '
        'lblPendingData
        '
        Me.lblPendingData.BackColor = System.Drawing.Color.PowderBlue
        Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendingData.ForeColor = System.Drawing.Color.Black
        Me.lblPendingData.Location = New System.Drawing.Point(430, 19)
        Me.lblPendingData.Name = "lblPendingData"
        Me.lblPendingData.Size = New System.Drawing.Size(155, 17)
        Me.lblPendingData.TabIndex = 181
        Me.lblPendingData.Text = "Pending Approval"
        Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.Color.Orange
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Black
        Me.objlblCaption.Location = New System.Drawing.Point(12, 19)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(155, 17)
        Me.objlblCaption.TabIndex = 7
        Me.objlblCaption.Text = "Employee Rehired"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(591, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objSearchReason
        '
        Me.objSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchReason.BorderSelected = False
        Me.objSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchReason.Location = New System.Drawing.Point(353, 235)
        Me.objSearchReason.Name = "objSearchReason"
        Me.objSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objSearchReason.TabIndex = 180
        '
        'cboChangeReason
        '
        Me.cboChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeReason.DropDownWidth = 400
        Me.cboChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeReason.FormattingEnabled = True
        Me.cboChangeReason.Location = New System.Drawing.Point(109, 235)
        Me.cboChangeReason.Name = "cboChangeReason"
        Me.cboChangeReason.Size = New System.Drawing.Size(211, 21)
        Me.cboChangeReason.TabIndex = 179
        '
        'lblChangeReason
        '
        Me.lblChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChangeReason.Location = New System.Drawing.Point(12, 238)
        Me.lblChangeReason.Name = "lblChangeReason"
        Me.lblChangeReason.Size = New System.Drawing.Size(91, 15)
        Me.lblChangeReason.TabIndex = 178
        Me.lblChangeReason.Text = "Change Reason"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Effective Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 110
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Branch"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Dept. Group"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Sec. Group"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Sections"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Unit Group"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Team"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Class Group"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Class"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Change Reason"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Width = 150
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "objdgcolhtransferunkid"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Visible = False
        Me.DataGridViewTextBoxColumn13.Width = 150
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhFromEmp"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "dgcolhAppointdate"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhRehiretranunkid"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objdgcolhtranguid"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "objdgcolhtranguid"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "objdgcolhOperationTypeId"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(616, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(76, 13)
        Me.lnkAllocation.TabIndex = 234
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'frmEmployeeTransfers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(696, 529)
        Me.Controls.Add(Me.objSearchReason)
        Me.Controls.Add(Me.cboChangeReason)
        Me.Controls.Add(Me.lblChangeReason)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeTransfers"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Transfers"
        Me.pnlMain.ResumeLayout(False)
        Me.gbTransferInformation.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.pnl1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchUnits As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchSection As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchSecGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDeptGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents gbTransferInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents elLine1 As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchClass As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchClassGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents cboChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblChangeReason As System.Windows.Forms.Label
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents lblAppointmentdate As System.Windows.Forms.Label
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhViewPending As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhOperationType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChangeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDepartmentGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSecGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUnitGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTeam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhtransferunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFromEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRehiretranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhtranguid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOperationTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
End Class
