﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMovementMigration
#Region " Private Variables "

    Private Const mstrModuleName As String = "frmMovementMigration"
    Dim mdtDatatable As New DataTable
    Dim mintTransactionId As Integer = 0
    Private menAction As enAction = enAction.ADD_ONE
    Private mintEmployeeUnkid As Integer = -1
    Private mblnCancel As Boolean = True
    Private lstApprover As List(Of DataRow)
    Private lstOldApprover As List(Of DataRow)

    Dim objMovementMigration As clsMovementMigration
    Private mblnIsapprovalFlowOn As Boolean = False

    Dim mintOperationType As Integer = clsEmployeeMovmentApproval.enOperationType.ADDED


    'Gajanan [26-OCT-2019] -- Start    
    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
    Dim screenType As String() = Nothing
    Dim CurrentScreen As Integer = 0
    'Gajanan [26-OCT-2019] -- End

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region "Page Event"
    Private Sub frmMovementMigration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call CreateTable()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMovementMigration_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef Dt As DataTable, ByVal intUnkId As Integer, _
                                  ByVal intOperationType As clsEmployeeMovmentApproval.enOperationType, _
                                  ByVal blnIsapprovalFlowOn As Boolean, _
                                  Optional ByVal intEmployeeUnkid As Integer = -1 _
                                  ) As Boolean
        Try
            mintTransactionId = intUnkId
            mintEmployeeUnkid = intEmployeeUnkid
            mintOperationType = intOperationType
            mblnIsapprovalFlowOn = blnIsapprovalFlowOn
            Me.ShowDialog()
            Dt = mdtDatatable
            Return mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Method "
    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim objMaster As New clsMasterData
        Dim objEmployee As New clsEmployee_Master
        Try
            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , , True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsFill.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
            End With


            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False
            End If


            dsFill = Nothing
            Dim dtTable As DataTable = Nothing

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsFill = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsFill = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End

            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                dtTable = New DataView(dsFill.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsFill.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            dsFill = Nothing
            dsFill = objMaster.GetMovementMigrationScreenType(True, "MigrationScreen")


            'Gajanan [26-OCT-2019] -- Start    
            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
            With cboScreenType
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = dsFill.Tables("MigrationScreen")
                .SelectedValue = 0
            End With

            If IsNothing(dsFill.Tables(0)) = False AndAlso dsFill.Tables(0).Rows.Count > 0 Then
                screenType = dsFill.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("id") > 0).Select(Function(x) x.Field(Of Integer)("id").ToString()).ToArray()
            End If

            If IsNothing(screenType) = False AndAlso screenType.Length > 0 Then
                cboScreenType.SelectedValue = screenType(0).ToString()
                CurrentScreen = CInt(screenType(0))
                btnSave.Visible = False
            End If

            If CurrentScreen = CInt(screenType(screenType.Length - 1)) Then
                btnSave.Visible = True
                btnRetain.Visible = False
            Else
                btnSave.Visible = False
                btnRetain.Visible = True
            End If
            'Gajanan [2-NOV-2019] -- Start    
            FillGrid(CurrentScreen, True)
            'Gajanan [2-NOV-2019] -- End

            'Gajanan [26-OCT-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetApproverPanel()
        Try
            cboOldLevel.Enabled = False
            pnlExpenseType.Visible = False
            pnlPerformanceApproverType.Visible = False
            pnlAssesmentApprovalOT.Visible = False

            pnlNewLevel.Enabled = True
            pnlOldLevel.Enabled = True

            cboOldApprover.DataSource = Nothing
            cboOldLevel.DataSource = Nothing

            cboNewApprover.DataSource = Nothing
            cboNewLevel.DataSource = Nothing

            If cboEmployee.SelectedValue > 0 AndAlso cboScreenType.SelectedValue > 0 Then
                cboOldApprover.Enabled = True
                cboNewApprover.Enabled = True
                cboNewLevel.Enabled = True
            Else
                cboOldApprover.Enabled = False
                cboNewApprover.Enabled = False
                cboNewLevel.Enabled = False
            End If

            pnlNewLevel.Visible = True
            pnlOldLevel.Visible = True

            radAssessor.Checked = False
            radReviewer.Checked = False


            'Gajanan [26-OCT-2019] -- Start    
            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
            If IsNothing(cboScreenType.SelectedValue) = False Then
                lblScreenTypeApprover.Text = cboScreenType.Text + " " + Language.getMessage(mstrModuleName, 22, "Approver List")
            End If
            'Gajanan [26-OCT-2019] -- End





        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetApproverPanel", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateTable()
        Try
            Dim objMovementMigration As New clsMovementMigration
            mdtDatatable = objMovementMigration._DataList.Clone()
            'Dim dcol As New DataColumn

            'dcol.DataType = System.Type.GetType("System.String")
            'dcol.ColumnName = "ScreenType"
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.String")
            'dcol.ColumnName = "OldApprover"
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.String")
            'dcol.ColumnName = "NewApprover"
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Boolean")
            'dcol.ColumnName = "isSkip"
            'dcol.DefaultValue = True
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "OldApproverId"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "NewApproverId"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "transactionid"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "employeeunkid"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "assesoroprationtype"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "moduletype"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "screentype"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "approverunkid"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Int32")
            'dcol.ColumnName = "oldapproverunkid"
            'dcol.DefaultValue = 0
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Boolean")
            'dcol.ColumnName = "isAssesor"
            'dcol.DefaultValue = False
            'mdtDatatable.Columns.Add(dcol)

            'dcol = New DataColumn
            'dcol.DataType = System.Type.GetType("System.Boolean")
            'dcol.ColumnName = "isExternal"
            'dcol.DefaultValue = False
            'mdtDatatable.Columns.Add(dcol)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateTable", mstrModuleName)
        End Try
    End Sub

    Private Function SetColValue(ByVal dr As DataRow, ByVal screenType As enApprovalMigration, Optional ByVal isReviewer As Boolean = False) As Boolean
        Try
            Dim drow As DataRow = mdtDatatable.NewRow()

            Select Case screenType
                Case enApprovalMigration.Leave
                    drow("ScreenType") = Language.getMessage(mstrModuleName, 1, "Leave")
                    drow("OldApproverId") = dr("approverunkid")
                    drow("NewApproverId") = dr("approverunkid")
                    drow("OldApprover") = dr("employeename")
                    drow("NewApprover") = dr("employeename")
                    drow("IsActive") = dr("IsActive")

                Case enApprovalMigration.Loan
                    drow("ScreenType") = Language.getMessage(mstrModuleName, 2, "Loan")
                    drow("OldApproverId") = dr("lnapproverunkid")
                    drow("NewApproverId") = dr("lnapproverunkid")
                    drow("OldApprover") = dr("employeename")
                    drow("NewApprover") = dr("employeename")
                    'drow("IsActive") = dr("IsActive")
                    drow("IsActive") = True

                Case enApprovalMigration.BudgetTimesheet
                    drow("ScreenType") = Language.getMessage(mstrModuleName, 3, "Budget Timesheet")
                    drow("OldApproverId") = dr("tsapproverunkid")
                    drow("NewApproverId") = dr("tsapproverunkid")
                    drow("OldApprover") = dr("employeename")
                    drow("NewApprover") = dr("employeename")
                    'drow("IsActive") = dr("IsActive")
                    drow("IsActive") = True

                Case enApprovalMigration.Claim
                    drow("ScreenType") = Language.getMessage(mstrModuleName, 4, "Claim") & " (" & dr("expensetype") & ")"
                    drow("OldApproverId") = dr("approverunkid")
                    drow("NewApproverId") = dr("approverunkid")
                    drow("OldApprover") = dr("ApproverName")
                    drow("NewApprover") = dr("ApproverName")
                    drow("IsActive") = dr("IsActive")

                Case enApprovalMigration.Performance

                    drow("ScreenType") = Language.getMessage(mstrModuleName, 5, "Performance") & " (" & dr("ApproverType") & ")"
                    drow("OldApproverId") = dr("assessormasterunkid")
                    drow("NewApproverId") = dr("assessormasterunkid")
                    drow("OldApprover") = dr("Employee")
                    drow("NewApprover") = dr("Employee")
                    drow("isReviewer") = isReviewer
                    drow("isExternal") = dr("isexternalapprover")
                    'drow("IsActive") = dr("IsActive")
                    drow("IsActive") = True

                Case enApprovalMigration.ReportingTo
                    drow("ScreenType") = Language.getMessage(mstrModuleName, 6, "Reporting To")
                    drow("OldApproverId") = dr("reporttoemployeeunkid")
                    drow("NewApproverId") = dr("reporttoemployeeunkid")
                    drow("OldApprover") = dr("ename")
                    drow("NewApprover") = dr("ename")

                    'S.SANDEEP |04-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                Case enApprovalMigration.OTRequisition
                    drow("ScreenType") = Language.getMessage("clsMasterData", 1039, "OT Requisition")
                    drow("OldApproverId") = dr("tnamappingunkid")
                    drow("NewApproverId") = dr("tnamappingunkid")
                    drow("OldApprover") = dr("employeename")
                    drow("NewApprover") = dr("employeename")
                    'drow("IsActive") = dr("IsActive")
                    drow("IsActive") = True
                    'S.SANDEEP |04-MAR-2020| -- END

            End Select

            drow("moduletype") = CInt(screenType)
            drow("employeeunkid") = CInt(cboEmployee.SelectedValue)
            mdtDatatable.Rows.Add(drow)
            mdtDatatable.AcceptChanges()
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColValue", mstrModuleName)
        End Try
    End Function

    Private Sub GetApproverLevel(ByVal sender As Object, ByVal intApproverID As Integer, ByVal blnExternalApprover As Boolean)

        Try
            Dim objLeaveApprover As New clsleaveapprover_master
            Dim objLoanapprover As New clsLoanApprover_master
            Dim objBugetTimeSheetapprover As New clstsapprover_master
            Dim objExpenseapprover As New clsExpenseApprover_Master
            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
            Dim objOTRequisitionApprover As New clsTnaapprover_master
            'S.SANDEEP |04-MAR-2020| -- END

            'Get Current Approver Detail
            If CType(sender, ComboBox).Name = cboNewApprover.Name Then
                Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                    Case enApprovalMigration.Leave
                        objLeaveApprover._Approverunkid = GetApproverId(lstApprover, True, enApprovalMigration.Leave)
                    Case enApprovalMigration.Loan
                        objLoanapprover._lnApproverunkid = GetApproverId(lstApprover, True, enApprovalMigration.Loan)
                    Case enApprovalMigration.BudgetTimesheet
                        objBugetTimeSheetapprover._Tsapproverunkid(Nothing) = GetApproverId(lstApprover, True, enApprovalMigration.BudgetTimesheet)
                    Case enApprovalMigration.Claim
                        objExpenseapprover._crApproverunkid = GetApproverId(lstApprover, True, enApprovalMigration.Claim)
                        'S.SANDEEP |04-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                    Case enApprovalMigration.OTRequisition
                        objOTRequisitionApprover._Tnamappingunkid = GetApproverId(lstApprover, True, enApprovalMigration.OTRequisition)
                        'S.SANDEEP |04-MAR-2020| -- END
                End Select
            ElseIf CType(sender, ComboBox).Name = cboOldApprover.Name Then
                Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                    Case enApprovalMigration.Leave
                        objLeaveApprover._Approverunkid = cboOldApprover.SelectedValue
                    Case enApprovalMigration.Loan
                        objLoanapprover._lnApproverunkid = cboOldApprover.SelectedValue
                    Case enApprovalMigration.BudgetTimesheet
                        objBugetTimeSheetapprover._Tslevelunkid = cboOldApprover.SelectedValue
                    Case enApprovalMigration.Claim
                        objExpenseapprover._crApproverunkid = cboOldApprover.SelectedValue
                        'S.SANDEEP |04-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                    Case enApprovalMigration.OTRequisition
                        objOTRequisitionApprover._Tnamappingunkid = cboOldApprover.SelectedValue
                        'S.SANDEEP |04-MAR-2020| -- END
                End Select
            End If

            'Get Selected Approver Level
            Dim dtList As DataTable = Nothing
            Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                Case enApprovalMigration.Leave
                    dtList = objLeaveApprover.GetLevelFromLeaveApprover(objLeaveApprover._leaveapproverunkid, objLeaveApprover._Isexternalapprover).Tables(0)
                Case enApprovalMigration.Loan
                    dtList = objLoanapprover.GetLevelFromLoanApprover(objLoanapprover._ApproverEmpunkid, objLoanapprover._IsExternalApprover).Tables(0)
                Case enApprovalMigration.BudgetTimesheet
                    dtList = objBugetTimeSheetapprover.GetLevelFromTimesheetApprover(objBugetTimeSheetapprover._Employeeapproverunkid, objBugetTimeSheetapprover._Isexternalapprover).Tables(0)
                Case enApprovalMigration.Claim

                    'Pinkal (18-Jun-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                    'dtList = objExpenseapprover.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), objExpenseapprover._crApproverunkid, objExpenseapprover._Employeeunkid).Tables(0)
                    dtList = objExpenseapprover.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), objExpenseapprover._crApproverunkid, objExpenseapprover._Employeeunkid, objExpenseapprover._Isexternalapprover).Tables(0)
                    'Pinkal (18-Jun-2020) -- End

                    'S.SANDEEP |04-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                Case enApprovalMigration.OTRequisition
                    dtList = objOTRequisitionApprover.GetOTApproverLevels(objOTRequisitionApprover._ApproverEmployeeId, objOTRequisitionApprover._IsExternalApprover, True).Tables(0)
                    'S.SANDEEP |04-MAR-2020| -- END
            End Select

            'Assign Dataset To Approver Level
            'If Request Come From Old Approver Set It's Current Level
            'Else Assign Dataset To Combobox

            If IsNothing(dtList) = False Then
                If CType(sender, ComboBox).Name = cboNewApprover.Name Then
                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    cboNewLevel.DataSource = Nothing
                    'Gajanan [26-OCT-2019] -- End

                    Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                        Case enApprovalMigration.Leave
                            cboNewLevel.DisplayMember = "levelname"
                            cboNewLevel.ValueMember = "levelunkid"
                        Case enApprovalMigration.Loan
                            cboNewLevel.DisplayMember = "name"
                            cboNewLevel.ValueMember = "lnlevelunkid"
                        Case enApprovalMigration.BudgetTimesheet
                            cboNewLevel.DisplayMember = "name"
                            cboNewLevel.ValueMember = "tslevelunkid"
                        Case enApprovalMigration.Claim
                            cboNewLevel.DisplayMember = "NAME"
                            cboNewLevel.ValueMember = "Id"
                            'S.SANDEEP |04-MAR-2020| -- START
                            'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                        Case enApprovalMigration.OTRequisition
                            cboNewLevel.DisplayMember = "NAME"
                            cboNewLevel.ValueMember = "Id"
                            'S.SANDEEP |04-MAR-2020| -- END
                    End Select


                    If IsNothing(dtList) = False Then
                        dtList = AddSelectToDatatable(dtList, cboNewLevel)
                        cboNewLevel.DataSource = dtList
                    End If

                ElseIf CType(sender, ComboBox).Name = cboOldApprover.Name Then
                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    cboOldLevel.DataSource = Nothing
                    'Gajanan [26-OCT-2019] -- End

                    Dim Levelid As Integer = 0
                    Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                        Case enApprovalMigration.Leave
                            cboOldLevel.DisplayMember = "levelname"
                            cboOldLevel.ValueMember = "levelunkid"
                            Levelid = objLeaveApprover._Levelunkid
                        Case enApprovalMigration.Loan
                            cboOldLevel.DisplayMember = "name"
                            cboOldLevel.ValueMember = "lnlevelunkid"
                            Levelid = objLoanapprover._lnLevelunkid
                        Case enApprovalMigration.BudgetTimesheet
                            cboOldLevel.DisplayMember = "name"
                            cboOldLevel.ValueMember = "tslevelunkid"
                            Levelid = objBugetTimeSheetapprover._Tslevelunkid
                        Case enApprovalMigration.Claim
                            cboOldLevel.DisplayMember = "NAME"
                            cboOldLevel.ValueMember = "Id"
                            Levelid = objExpenseapprover._crLevelunkid
                            'S.SANDEEP |04-MAR-2020| -- START
                            'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                        Case enApprovalMigration.OTRequisition
                            cboOldLevel.DisplayMember = "NAME"
                            cboOldLevel.ValueMember = "Id"
                            Levelid = objOTRequisitionApprover._Tnalevelunkid
                            'S.SANDEEP |04-MAR-2020| -- END
                    End Select

                    cboOldLevel.DataSource = dtList
                    cboOldLevel.SelectedValue = Levelid

                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveApproverLevel", mstrModuleName)
        End Try
    End Sub

    Private Sub SetNewApproverCombo(ByVal dt As DataTable)
        Try
            cboOldApprover.DataSource = Nothing
            Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                Case enApprovalMigration.Leave
                    With cboOldApprover
                        .ValueMember = "approverunkid"
                        .DisplayMember = "employeename"
                    End With

                Case enApprovalMigration.Loan
                    With cboOldApprover
                        .ValueMember = "lnapproverunkid"
                        .DisplayMember = "employeename"
                    End With

                Case enApprovalMigration.Claim
                    With cboOldApprover
                        .ValueMember = "approverunkid"
                        .DisplayMember = "ApproverName"
                    End With


                Case enApprovalMigration.BudgetTimesheet
                    With cboOldApprover
                        .ValueMember = "tsapproverunkid"
                        .DisplayMember = "employeename"
                    End With

                Case enApprovalMigration.Performance
                    With cboOldApprover
                        .ValueMember = "assessormasterunkid"
                        .DisplayMember = "Employee"
                    End With

                Case enApprovalMigration.ReportingTo
                    With cboOldApprover
                        .ValueMember = "reporttoemployeeunkid"
                        .DisplayMember = "ename"
                    End With
                    'S.SANDEEP |04-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                Case enApprovalMigration.OTRequisition
                    With cboOldApprover
                        .ValueMember = "tnamappingunkid"
                        .DisplayMember = "employeename"
                    End With
                    'S.SANDEEP |04-MAR-2020| -- END
            End Select

            If IsNothing(dt) = False Then
                dt = AddSelectToDatatable(dt, cboOldApprover)
            End If
            cboOldApprover.SelectedValue = 0
            cboOldApprover.DataSource = dt

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetNewApproverCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearApproverAndLevelCombo()
        Try
            cboOldApprover.DataSource = Nothing
            cboOldLevel.DataSource = Nothing
            cboNewApprover.DataSource = Nothing
            cboNewLevel.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearApproverAndLevelCombo", mstrModuleName)
        End Try

    End Sub

    Private Function AddSelectToDatatable(ByVal dt As DataTable, ByVal cbo As ComboBox) As DataTable
        Try
            If dt.AsEnumerable.Where(Function(x) CInt(x.Field(Of Integer)(cbo.ValueMember.ToString())) <= 0).Count <= 0 Then
                Dim drow As DataRow = dt.NewRow
                drow(cbo.ValueMember.ToString()) = 0
                drow(cbo.DisplayMember.ToString()) = Language.getMessage(mstrModuleName, 14, "Select")
                dt.Rows.Add(drow)
                dt.DefaultView.Sort = cbo.ValueMember.ToString()
                dt.AcceptChanges()
            End If
            Return dt
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddSelectToDatatable", mstrModuleName)
        End Try
        Return dt
    End Function

    Private Function GetApproverId(ByVal lstApprover As List(Of DataRow), ByVal getApproverlevel As Boolean, ByVal screenType As enApprovalMigration) As Integer
        Try
            Dim intApproverId As Integer = 0

            If IsNothing(lstApprover) = False Then
                Select Case screenType
                    Case enApprovalMigration.Leave
                        If getApproverlevel Then
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("leaveapproverunkid")) = CInt(cboNewApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("approverunkid")).FirstOrDefault()
                        Else
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("leaveapproverunkid")) = CInt(cboNewApprover.SelectedValue) And CInt(x.Field(Of Integer)("levelunkid")) = CInt(cboNewLevel.SelectedValue)).Select(Function(x) x.Field(Of Integer)("approverunkid")).FirstOrDefault()
                        End If
                    Case enApprovalMigration.Loan
                        If getApproverlevel Then
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("approverempunkid")) = CInt(cboNewApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("lnapproverunkid")).FirstOrDefault()
                        Else
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("approverempunkid")) = CInt(cboNewApprover.SelectedValue) And CInt(x.Field(Of Integer)("lnlevelunkid")) = CInt(cboNewLevel.SelectedValue)).Select(Function(x) x.Field(Of Integer)("lnapproverunkid")).FirstOrDefault()
                        End If

                    Case enApprovalMigration.BudgetTimesheet
                        If getApproverlevel Then
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("employeeapproverunkid")) = CInt(cboNewApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("tsapproverunkid")).FirstOrDefault()
                        Else
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("employeeapproverunkid")) = CInt(cboNewApprover.SelectedValue) And CInt(x.Field(Of Integer)("tslevelunkid")) = CInt(cboNewLevel.SelectedValue)).Select(Function(x) x.Field(Of Integer)("tsapproverunkid")).FirstOrDefault()
                        End If

                    Case enApprovalMigration.Claim
                        If getApproverlevel Then
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("employeeunkid")) = CInt(cboNewApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("crapproverunkid")).FirstOrDefault()
                        Else
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("employeeunkid")) = CInt(cboNewApprover.SelectedValue) And CInt(x.Field(Of Integer)("crlevelunkid")) = CInt(cboNewLevel.SelectedValue)).Select(Function(x) x.Field(Of Integer)("crapproverunkid")).FirstOrDefault()
                        End If

                    Case enApprovalMigration.Performance

                        If getApproverlevel Then
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("employeeunkid")) = CInt(cboNewApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("assessormasterunkid")).FirstOrDefault()
                        Else
                            Dim blnReviewerFlag As Boolean = False
                            If radAssessor.Checked = True Then
                                blnReviewerFlag = False
                            ElseIf radReviewer.Checked = True Then
                                blnReviewerFlag = True
                            End If
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("assessormasterunkid")) = CInt(cboOldApprover.SelectedValue) And x.Field(Of Boolean)("isreviewer") = blnReviewerFlag).Select(Function(x) x.Field(Of Integer)("employeeunkid")).FirstOrDefault()
                        End If
                        'S.SANDEEP |04-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                    Case enApprovalMigration.OTRequisition
                        If getApproverlevel Then
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("approveremployeeunkid")) = CInt(cboNewApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("tnamappingunkid")).FirstOrDefault()
                        Else
                            intApproverId = lstApprover.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("approveremployeeunkid")) = CInt(cboNewApprover.SelectedValue) And CInt(x.Field(Of Integer)("tnalevelunkid")) = CInt(cboNewLevel.SelectedValue)).Select(Function(x) x.Field(Of Integer)("tnamappingunkid")).FirstOrDefault()
                        End If
                        'S.SANDEEP |04-MAR-2020| -- END
                End Select
            End If


            Return intApproverId
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverId", mstrModuleName)
        End Try
    End Function

    Private Function UpdateRowColor(ByVal dgr As DataGridViewRow) As Boolean
        Try
            dgr.Cells(colhdgNewApprover.Index).Style.ForeColor = Color.Blue
            lblnote.Visible = True
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateRowColor", mstrModuleName)
        End Try
    End Function

    Private Function isValidate(ByVal mintScreenType As Integer) As Boolean
        Try


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboScreenType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Screen type is mandatory information. Please select Screen to continue."), enMsgBoxStyle.Information)
                cboScreenType.Focus()
                Return False
            End If

            Select Case CType(mintScreenType, enApprovalMigration)
                Case enApprovalMigration.Claim
                    If CInt(cboExCategory.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Expense Type is mandatory information. Please select Expense Type to continue."), enMsgBoxStyle.Information)
                        cboExCategory.Focus()
                        Return False
                    End If
            End Select

            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, From Approver is mandatory information. Please select From Approver to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, To Approver is mandatory information. Please select To Approver to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            Select Case CType(mintScreenType, enApprovalMigration)
                Case enApprovalMigration.Performance
                    If radAssessor.Checked = False AndAlso radReviewer.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Please select atleast one approver type in order to do continue process."), enMsgBoxStyle.Information)
                        radAssessor.Focus()
                        Return False
                    End If

                    If radOverWriteAssessment.Checked = False AndAlso radVoidAssessment.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, Please select atleast one operation type in order to do continue process."), enMsgBoxStyle.Information)
                        radAssessor.Focus()
                        Return False
                    End If

                Case enApprovalMigration.ReportingTo
					'Gajanan [24-OCT-2019] -- Start   
					'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                    Dim objReportTo As New clsReportingToEmployee

                    If objReportTo.IsReportToValid(CInt(cboOldApprover.SelectedValue), CInt(cboNewApprover.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Nothing, Nothing) = False Then
                        'Hemant (03 Feb 2023) -- [eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)]
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you can not set this employee as default reporting to, Reason: Selected reporting to employee is already reporting directly/indirectly to above selected employee."), enMsgBoxStyle.Information)
                        Return False
                    End If
					'Gajanan [24-OCT-2019] -- End

                Case Else
                    If CInt(cboOldLevel.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, From Approver Level is mandatory information. Please select From Approver to continue."), enMsgBoxStyle.Information)
                        cboEmployee.Focus()
                        Return False
                    End If

                    If CInt(cboNewLevel.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, To Approver Level is mandatory information. Please select To Approver to continue."), enMsgBoxStyle.Information)
                        cboEmployee.Focus()
                        Return False
                    End If
            End Select


            'Gajanan [26-OCT-2019] -- Start    
            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  



            'Select Case CType(mintScreenType, enApprovalMigration)
            '    Case enApprovalMigration.Claim
            '        If CInt(cboExCategory.SelectedValue) <= 0 Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Expense Type is mandatory information. Please select Expense Type to continue."), enMsgBoxStyle.Information)
            '            cboExCategory.Focus()
            '            Return False
            '        End If
            'End Select
            'Gajanan [26-OCT-2019] -- End
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isValidate", mstrModuleName)
        End Try
    End Function

    Private Function SetSkipAll(ByVal dr As DataRow) As Boolean
        Try
            dr("isskip") = True
            dr("NewApproverId") = dr("OldApproverId")
            dr("NewApprover") = dr("OldApprover")
            dr("oldapproverunkid") = 0
            dr("approverunkid") = 0
            dr("isReviewer") = False
            dr("ExpenseType") = 0
            dr("isExternal") = False
            dr.AcceptChanges()
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColValue", mstrModuleName)
        End Try
    End Function



    'Gajanan [26-OCT-2019] -- Start    
    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
    Private Function FillGrid(ByVal xScreenType As enApprovalMigration, Optional ByVal isLoad As Boolean = False)
        'Gajanan [2-NOV-2019] -- Add [isLoad]    

        Try
            If cboEmployee.SelectedValue > 0 Then

                'Gajanan [2-NOV-2019] -- Start    
                If isLoad Then
                    'Gajanan [2-NOV-2019] -- End
                CreateTable()

                Dim objapprover As New clsleaveapprover_master
                Dim objLoanapprover As New clsLoanApprover_master
                Dim objBugetTimeSheetapprover As New clstsapprover_master
                Dim objExpenseapprover As New clsExpenseApprover_Master
                Dim objAssesmentApprover As New clsAssessor_tran
                Dim objReportingToEmployee As New clsReportingToEmployee
                    'S.SANDEEP |04-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                    Dim objOTApprover As New clsTnaapprover_master
                    'S.SANDEEP |04-MAR-2020| -- END

                Dim dtOldApproverList As DataTable = Nothing

                'Leave -- Start    
                If xScreenType = enApprovalMigration.Leave Then
                    'Gajanan [02-June-2020] -- Start
                    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

                    'dtOldApproverList = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                    '                                                          , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                    '                                                          , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), Nothing, False)

                    dtOldApproverList = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                              , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), Nothing, False)
                    'Gajanan [02-June-2020] -- End

                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Leave))
                    End If
                End If

                'Loan -- Start   
                    'Gajanan [2-NOV-2019] -- Start 
                    'If xScreenType = enApprovalMigration.Loan Then
            
                    dtOldApproverList = objLoanapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, -1, FinancialYear._Object._YearUnkid, _
                                                                            Company._Object._Companyunkid, CInt(cboEmployee.SelectedValue), _
                                                                            ConfigParameter._Object._IsLoanApprover_ForLoanScheme, -1, "")
                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Loan))
                    End If
                    'End If  'Gajanan [2-NOV-2019] -- End


                'TimeSheet-- Start    
                    'Gajanan [2-NOV-2019] -- Start 
                    'If xScreenType = enApprovalMigration.BudgetTimesheet Then
                   

                    'Gajanan [02-June-2020] -- Start
                    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

                    'dtOldApproverList = objBugetTimeSheetapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                    '                                                        Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                                                        ConfigParameter._Object._IsIncludeInactiveEmp, -1, cboEmployee.SelectedValue)


                    dtOldApproverList = objBugetTimeSheetapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                                            Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                            True, -1, cboEmployee.SelectedValue)
                    'Gajanan [02-June-2020] -- End



                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.BudgetTimesheet))
                    End If
                    'End If  'Gajanan [2-NOV-2019] -- End

                'Claim -- Start  
                    'Gajanan [2-NOV-2019] -- Start 
                    'If xScreenType = enApprovalMigration.Claim Then
                   
                    'Gajanan [02-June-2020] -- Start
                    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.


                    'dtOldApproverList = objExpenseapprover.GetApproverFromEmployeeId(CInt(cboEmployee.SelectedValue)).Tables(0)
                    dtOldApproverList = objExpenseapprover.GetApproverFromEmployeeId(FinancialYear._Object._DatabaseName, _
                                                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                     True, _
                                                                                     CInt(cboEmployee.SelectedValue)).Tables(0)
                    'Gajanan [02-June-2020] -- End

                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Claim))
                    End If
                    'End If  'Gajanan [2-NOV-2019] -- End

                'Reporting To -- Start   

                    'Gajanan [2-NOV-2019] -- Start 
                    'If xScreenType = enApprovalMigration.ReportingTo Then
                    'Gajanan [2-NOV-2019] -- End
                    objReportingToEmployee._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                    dtOldApproverList = objReportingToEmployee._RDataTable.Copy()
                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                        dtOldApproverList = New DataView(dtOldApproverList, "ishierarchy = true", "", DataViewRowState.CurrentRows).ToTable()
                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.ReportingTo))
                    End If
                    'End If

                'Performance Assesment -- Start  
                    'Gajanan [2-NOV-2019] -- Start 
                    'If xScreenType = enApprovalMigration.Performance Then
                 
                    dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_ASSESSOR, CInt(cboEmployee.SelectedValue), _
                                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    FinancialYear._Object._DatabaseName).Tables(0)
                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then

                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Performance, False))
                    End If

                    dtOldApproverList = Nothing
                    dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_REVIEWER, CInt(cboEmployee.SelectedValue), _
                                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    FinancialYear._Object._DatabaseName).Tables(0)

                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Performance, True))
                    End If


                    'OT REQUISITION -- START 
                    'S.SANDEEP |04-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                    dtOldApproverList = objOTApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                             , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp, -1, cboEmployee.SelectedValue, -1, Nothing, False, "", True)
                    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.OTRequisition))
                    End If
                    'S.SANDEEP |04-MAR-2020| -- END
                    'OT REQUISITION -- END

                End If  'Gajanan [2-NOV-2019] -- End



                    'Gajanan [2-NOV-2019] -- Start    
                    Dim dtView As DataView = mdtDatatable.DefaultView
                    dtView.RowFilter = "moduletype = " & xScreenType & ""
                    'Gajanan [2-NOV-2019] -- End


                dgvApproverMigration.AutoGenerateColumns = False
                colhdgScreenType.DataPropertyName = "ScreenType"
                colhdgOldApprover.DataPropertyName = "OldApprover"
                colhdgNewApprover.DataPropertyName = "NewApprover"
                objcolhdgIsSkip.DataPropertyName = "isSkip"
                objcolhNewApproverId.DataPropertyName = "NewApproverId"
                objcolhOldApproverId.DataPropertyName = "OldApproverId"
                objcolhtransactionid.DataPropertyName = "transactionid"
                objcolhemployeeunkid.DataPropertyName = "employeeunkid"
                objcolhassesoroprationtype.DataPropertyName = "assesoroprationtype"
                objcolhmoduletype.DataPropertyName = "moduletype"
                objcolhscreentype.DataPropertyName = "screentype"
                'Gajanan [04-June-2020] -- Start
                objcolhisactive.DataPropertyName = "IsActive"
                'Gajanan [04-June-2020] -- End
                    'Gajanan [2-NOV-2019] -- Start    
                    'dgvApproverMigration.DataSource = mdtDatatable
                    dgvApproverMigration.DataSource = dtView
                    'Gajanan [2-NOV-2019] -- End

                'Gajanan [04-June-2020] -- Start
                For Each dr As DataGridViewRow In dgvApproverMigration.Rows
                    If CBool(dr.Cells(objcolhisactive.Index).Value) = False Then
                        dr.Cells(colhdgOldApprover.Index).Style.ForeColor = Color.Red
                        lblapprovernote.Visible = True
                    End If
                Next
                'Gajanan [04-June-2020] -- End

            Else
                dgvApproverMigration.DataSource = Nothing
            End If 'Gajanan [2-NOV-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Function

    Private Function createEmptycomboDt() As DataTable
        Try
            Dim dt As New DataTable
            dt.Columns.Add("name")
            dt.Columns.Add("id")

            Dim drow As DataRow = dt.NewRow
            drow("name") = Language.getMessage(mstrModuleName, 14, "Select")
            drow("id") = 0
            dt.Rows.Add(drow)

            Return dt
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Function

    'Gajanan [26-OCT-2019] -- End




#End Region

#Region " Buttons Method "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        'Gajanan [26-OCT-2019] -- Start    
        'Enhancement:Worked On NMB Approver Migration Enforcement Comment  

        'mdtDatatable.AsEnumerable().ToList().ForEach(Function(dr) SetSkipAll(dr))
        'mblnCancel = False

        If mdtDatatable.AsEnumerable.Where(Function(x) x.Field(Of Boolean)("isSkip") = False).Count > 0 Then
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Are you sure you want to close this screen ? if you close this screen then all transection which were done will not saved.Do you still want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                mblnCancel = False
        Me.Close()
            End If
        Else
            mblnCancel = False
            Me.Close()
        End If
        'Gajanan [26-OCT-2019] -- End    


    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, _
                                                                                                                objbtnSearchFromApprover.Click, _
                                                                                                                objbtnSearchToApprover.Click, _
                                                                                                                objbtnSearchToApproverLevel.Click

        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objbtnSearchFromApprover.Name.ToUpper
                    xCbo = cboOldApprover
                Case objbtnSearchToApprover.Name.ToUpper
                    xCbo = cboNewApprover
                Case objbtnSearchToApproverLevel.Name.ToUpper
                    xCbo = cboNewLevel
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Dim blnflag As Boolean = False
            'objMovementMigration = New clsMovementMigration

            'blnflag = objMovementMigration.Insert(mdtDatatable, mintTransactionId, CInt(cboEmployee.SelectedValue), _
            '                                      CInt(enApprovalMigrationScreenType.TRANSFER), False, _
            '                                      ConfigParameter._Object._EmployeeAsOnDate, _
            '                                      ConfigParameter._Object._PaymentApprovalwithLeaveApproval, User._Object._Userunkid, _
            '                                      mstrModuleName, CInt(clsEmployeeMovmentApproval.enOperationType.ADDED), Nothing)

            'If blnflag = False AndAlso objMovementMigration._Message <> "" Then
            '    eZeeMsgBox.Show(objMovementMigration._Message, enMsgBoxStyle.Information)
            '    Exit Sub
            'End If


            'Gajanan [26-OCT-2019] -- Start    
            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  


            If mdtDatatable.AsEnumerable.Where(Function(x) x.Field(Of Boolean)("isSkip") = False).Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Are you sure you want to save this data ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    mblnCancel = True
                    Me.Close()
                End If
            Else
            mblnCancel = True
            Me.Close()
            End If
            'Gajanan [26-OCT-2019] -- End

            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Try

            If isValidate(cboScreenType.SelectedValue) = False Then Exit Sub



            Dim drow As DataRow() = mdtDatatable.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue) _
                                                                                     And x.Field(Of Integer)("OldApproverId") = CInt(cboOldApprover.SelectedValue) _
                                                                                     And x.Field(Of Integer)("moduletype") = CInt(cboScreenType.SelectedValue)).ToArray()


            If IsNothing(drow) = False AndAlso drow.Length > 0 Then
                Dim mintApprovalOnSameAssignCount As Integer = 0
                Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                    Case enApprovalMigration.Claim
                        mintApprovalOnSameAssignCount = mdtDatatable.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue) _
                                                                                    And x.Field(Of Integer)("moduletype") = CInt(cboScreenType.SelectedValue) _
                                                                                    And x.Field(Of Integer)("ExpenseType") = CInt(cboExCategory.SelectedValue) _
                                                                                    And x.Field(Of Integer)("approverunkid") = CInt(cboNewApprover.SelectedValue)).Count

                    Case enApprovalMigration.Performance
                        mintApprovalOnSameAssignCount = mdtDatatable.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue) _
                                                                                   And x.Field(Of Integer)("moduletype") = CInt(cboScreenType.SelectedValue) _
                                                                                   And x.Field(Of Boolean)("isReviewer") = If(radAssessor.Checked, False, True) _
                                                                                   And x.Field(Of Integer)("approverunkid") = CInt(cboNewApprover.SelectedValue)).Count

                    Case Else
                        mintApprovalOnSameAssignCount = mdtDatatable.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue) _
                                                                                                          And x.Field(Of Integer)("moduletype") = CInt(cboScreenType.SelectedValue) _
                                                                                                           And x.Field(Of Integer)("approverunkid") = CInt(cboNewApprover.SelectedValue)).Count

                End Select

                If mintApprovalOnSameAssignCount > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Selected employee is already binded with the selected to approver."), enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    drow(0).Item("NewApprover") = cboNewApprover.Text.ToString()

                    Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)
                        Case enApprovalMigration.Leave
                            drow(0).Item("NewApproverId") = GetApproverId(lstApprover, False, enApprovalMigration.Leave)
                            drow(0).Item("approverunkid") = CInt(cboNewApprover.SelectedValue)

                        Case enApprovalMigration.Loan
                            drow(0).Item("NewApproverId") = GetApproverId(lstApprover, False, enApprovalMigration.Loan)
                            drow(0).Item("approverunkid") = CInt(cboNewApprover.SelectedValue)

                        Case enApprovalMigration.BudgetTimesheet
                            drow(0).Item("NewApproverId") = GetApproverId(lstApprover, False, enApprovalMigration.BudgetTimesheet)
                            drow(0).Item("approverunkid") = CInt(cboNewApprover.SelectedValue)

                        Case enApprovalMigration.Claim
                            drow(0).Item("NewApproverId") = GetApproverId(lstApprover, False, enApprovalMigration.Claim)
                            drow(0).Item("approverunkid") = CInt(cboNewApprover.SelectedValue)
                            drow(0).Item("ExpenseType") = CInt(cboExCategory.SelectedValue)

                        Case enApprovalMigration.Performance
                            drow(0).Item("NewApproverId") = GetApproverId(lstApprover, True, enApprovalMigration.Performance)

                            drow(0).Item("oldapproverunkid") = GetApproverId(lstOldApprover, False, enApprovalMigration.Performance)
                            drow(0).Item("approverunkid") = CInt(cboNewApprover.SelectedValue)

                            If radOverWriteAssessment.Checked Then
                                drow(0).Item("assesoroprationtype") = CInt(clsAssessor_tran.enOperationType.Overwrite)
                            Else
                                drow(0).Item("assesoroprationtype") = CInt(clsAssessor_tran.enOperationType.Void)
                            End If

                        Case enApprovalMigration.ReportingTo
                            drow(0).Item("NewApproverId") = CInt(cboNewApprover.SelectedValue)
                            'S.SANDEEP |04-MAR-2020| -- START
                            'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                        Case enApprovalMigration.OTRequisition
                            drow(0).Item("NewApproverId") = GetApproverId(lstApprover, False, enApprovalMigration.OTRequisition)
                            drow(0).Item("approverunkid") = CInt(cboNewApprover.SelectedValue)
                            'S.SANDEEP |04-MAR-2020| -- END
                        Case Else

                    End Select

                End If
                If CInt(drow(0).Item("OldApproverId")) <> CInt(cboNewApprover.SelectedValue) Then
                    drow(0).Item("isskip") = False
                End If
                mdtDatatable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSet_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub btnRetain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetain.Click
        Try
            If cboScreenType.Items.Count <> cboScreenType.SelectedIndex Then
                cboScreenType.SelectedIndex += 1
                CurrentScreen = cboScreenType.SelectedValue
            End If

            If CurrentScreen = CInt(screenType(screenType.Length - 1)) Then
                btnRetain.Visible = False
                btnSave.Visible = True
            End If

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            mstrAdvanceFilter = ""
            'Gajanan [11-Dec-2019] -- End
            FillGrid(cboScreenType.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRetain_Click", mstrModuleName)
        End Try
    End Sub


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            cboOldApprover_SelectedIndexChanged(cboNewApprover, Nothing)
            'FillEmployeeCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region " ComboBox Method "
    Private Sub cboScreenType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboScreenType.SelectedIndexChanged
        Try
            ResetApproverPanel()

            If cboScreenType.SelectedValue > 0 Then

                FillGrid(cboScreenType.SelectedValue)

                Select Case cboScreenType.SelectedValue
                    Case enApprovalMigration.Claim
                        pnlExpenseType.Visible = True

                    Case enApprovalMigration.Performance
                        pnlNewLevel.Visible = False
                        pnlOldLevel.Visible = False
                        pnlPerformanceApproverType.Visible = True
                        pnlAssesmentApprovalOT.Visible = True
                        radAssessor.Checked = True
                    Case enApprovalMigration.ReportingTo
                        pnlNewLevel.Visible = False
                        pnlOldLevel.Visible = False

                End Select

                If CInt(cboScreenType.SelectedValue) = CInt(enApprovalMigration.Leave) Then
                    pnlPerformanceApproverType.Visible = True
                    pnlPerformanceApproverType.Visible = False
                ElseIf cboScreenType.SelectedItem.ToString().ToUpper() = "CLAIM" Then
                    pnlPerformanceApproverType.Visible = True
                    pnlPerformanceApproverType.Visible = False
                End If

                If cboScreenType.SelectedValue > 0 Then

                    Dim dtOldApproverList As DataTable = Nothing
                    Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)

                        Case enApprovalMigration.Leave
                            Dim objapprover As New clsleaveapprover_master
                            'Gajanan [04-June-2020] -- Start
                            'dtOldApproverList = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                            '                                              , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                            '                                              , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), Nothing, False)
                            dtOldApproverList = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                              , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                          , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), Nothing, False)
                            'Gajanan [04-June-2020] -- End


                        Case enApprovalMigration.Loan
                            Dim objLoanapprover As New clsLoanApprover_master

                            dtOldApproverList = objLoanapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, -1, FinancialYear._Object._YearUnkid, _
                                                                            Company._Object._Companyunkid, CInt(cboEmployee.SelectedValue), _
                                                                            ConfigParameter._Object._IsLoanApprover_ForLoanScheme, -1, "")


                        Case enApprovalMigration.BudgetTimesheet
                            Dim objBugetTimeSheetapprover As New clstsapprover_master


                            'Gajanan [02-June-2020] -- Start
                            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.



                            'dtOldApproverList = objBugetTimeSheetapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                            '                                                        Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                                                        ConfigParameter._Object._IsIncludeInactiveEmp, -1, cboEmployee.SelectedValue)


                            dtOldApproverList = objBugetTimeSheetapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                                                    Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    True, -1, cboEmployee.SelectedValue)

                            'Gajanan [02-June-2020] -- End


                        Case enApprovalMigration.ReportingTo
                            Dim objReportingToEmployee As New clsReportingToEmployee
                            objReportingToEmployee._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                            dtOldApproverList = objReportingToEmployee._RDataTable.Copy()
                            dtOldApproverList = New DataView(dtOldApproverList, "ishierarchy = true", "", DataViewRowState.CurrentRows).ToTable()


                            'Gajanan [26-OCT-2019] -- Start    
                            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                        Case enApprovalMigration.Claim
                            Dim dt As DataTable = createEmptycomboDt()
                            cboOldApprover.ValueMember = "id"
                            cboOldApprover.DisplayMember = "name"
                            cboOldApprover.DataSource = dt
                            cboOldApprover.SelectedValue = 0
                            'Gajanan [26-OCT-2019] -- End

                            'S.SANDEEP |04-MAR-2020| -- START
                            'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                        Case enApprovalMigration.OTRequisition
                            Dim objOTApprover As New clsTnaapprover_master

                            'Gajanan [02-June-2020] -- Start
                            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                            'dtOldApproverList = objOTApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, -1, cboEmployee.SelectedValue)
                            dtOldApproverList = objOTApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, -1, cboEmployee.SelectedValue)
                            'Gajanan [02-June-2020] -- End


                            'S.SANDEEP |04-MAR-2020| -- END
                    End Select

                    If IsNothing(dtOldApproverList) = False Then
                        SetNewApproverCombo(dtOldApproverList)
                    End If
                Else
                    ClearApproverAndLevelCombo()
                End If


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboScreenType_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            ResetApproverPanel()
            cboScreenType.SelectedIndex = 0

            'Gajanan [26-OCT-2019] -- Start    
            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  

            'If cboEmployee.SelectedValue > 0 Then
            '    CreateTable()

            '    Dim objapprover As New clsleaveapprover_master
            '    Dim objLoanapprover As New clsLoanApprover_master
            '    Dim objBugetTimeSheetapprover As New clstsapprover_master
            '    Dim objExpenseapprover As New clsExpenseApprover_Master
            '    Dim objAssesmentApprover As New clsAssessor_tran
            '    Dim objReportingToEmployee As New clsReportingToEmployee


            '    'Leave -- Start    
            '    Dim dtOldApproverList As DataTable = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                                              , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
            '                                                              , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), Nothing, False)
            '    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
            '        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Leave))
            '    End If



            '    'Loan -- Start    
            '    dtOldApproverList = Nothing

            '    dtOldApproverList = objLoanapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, -1, FinancialYear._Object._YearUnkid, _
            '                                                            Company._Object._Companyunkid, CInt(cboEmployee.SelectedValue), _
            '                                                            ConfigParameter._Object._IsLoanApprover_ForLoanScheme, -1, "")
            '    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
            '        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Loan))
            '    End If


            '    'TimeSheet-- Start    
            '    dtOldApproverList = Nothing
            '    dtOldApproverList = objBugetTimeSheetapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
            '                                                            Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                            ConfigParameter._Object._IsIncludeInactiveEmp, -1, cboEmployee.SelectedValue)
            '    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
            '        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.BudgetTimesheet))
            '    End If

            '    'Claim -- Start    
            '    dtOldApproverList = Nothing
            '    dtOldApproverList = objExpenseapprover.GetApproverFromEmployeeId(CInt(cboEmployee.SelectedValue)).Tables(0)
            '    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
            '        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Claim))
            '    End If

            '    'Reporting To -- Start    
            '    dtOldApproverList = Nothing
            '    objReportingToEmployee._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            '    dtOldApproverList = objReportingToEmployee._RDataTable.Copy()
            '    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
            '        dtOldApproverList = New DataView(dtOldApproverList, "ishierarchy = true", "", DataViewRowState.CurrentRows).ToTable()
            '        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.ReportingTo))
            '    End If

            '    'Performance Assesment -- Start   
            '    dtOldApproverList = Nothing
            '    dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_ASSESSOR, CInt(cboEmployee.SelectedValue), _
            '                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                    FinancialYear._Object._DatabaseName).Tables(0)
            '    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then

            '        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Performance, False))
            '    End If

            '    dtOldApproverList = Nothing
            '    dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_REVIEWER, CInt(cboEmployee.SelectedValue), _
            '                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                    FinancialYear._Object._DatabaseName).Tables(0)

            '    If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
            '        dtOldApproverList.AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, enApprovalMigration.Performance, True))
            '    End If

            '    dgvApproverMigration.AutoGenerateColumns = False
            '    colhdgScreenType.DataPropertyName = "ScreenType"
            '    colhdgOldApprover.DataPropertyName = "OldApprover"
            '    colhdgNewApprover.DataPropertyName = "NewApprover"
            '    objcolhdgIsSkip.DataPropertyName = "isSkip"
            '    objcolhNewApproverId.DataPropertyName = "NewApproverId"
            '    objcolhOldApproverId.DataPropertyName = "OldApproverId"
            '    objcolhtransactionid.DataPropertyName = "transactionid"
            '    objcolhemployeeunkid.DataPropertyName = "employeeunkid"
            '    objcolhassesoroprationtype.DataPropertyName = "assesoroprationtype"
            '    objcolhmoduletype.DataPropertyName = "moduletype"
            '    objcolhscreentype.DataPropertyName = "screentype"
            '    dgvApproverMigration.DataSource = mdtDatatable

            'Else
            '    dgvApproverMigration.DataSource = Nothing
            'End If

            'Gajanan [26-OCT-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNewApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Try
            If cboScreenType.SelectedValue > 0 AndAlso IsNothing(cboNewApprover.DataSource) = False Then
                'Select CType(cboScreenType.SelectedValue, enApprovalMigration)
                '    Case enApprovalMigration.Claim
                '        GetLeaveApproverLevel(sender, CInt(cboNewApprover.SelectedValue), CBool(CType(cboNewApprover.SelectedItem, DataRowView).Item("ExAppr")))
                '    Case Else
                'End Select
                If CInt(cboNewApprover.SelectedValue) > 0 Then

                    If CInt(cboScreenType.SelectedValue) <> enApprovalMigration.ReportingTo Then
                        GetApproverLevel(sender, CInt(cboNewApprover.SelectedValue), CBool(CType(cboNewApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                    End If

                Else
                    cboNewLevel.DataSource = Nothing
                    Dim dt As DataTable = createEmptycomboDt()
                    cboNewLevel.ValueMember = "id"
                    cboNewLevel.DisplayMember = "name"
                    cboNewLevel.DataSource = dt
                    cboNewLevel.SelectedValue = 0
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboOldApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        Try
            If cboScreenType.SelectedValue > 0 AndAlso IsNothing(cboOldApprover.DataSource) = False AndAlso CInt(cboOldApprover.SelectedValue) > 0 Then
                Dim dtNewApproverList As DataTable = Nothing
                Select Case CType(cboScreenType.SelectedValue, enApprovalMigration)

                    Case enApprovalMigration.Leave

                        GetApproverLevel(sender, CInt(cboOldApprover.SelectedValue), _
                                              CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))

                        Dim objapprover As New clsleaveapprover_master

                        If CInt(cboOldApprover.SelectedValue) > 0 Then
                            'Gajanan [04-June-2020] -- Start

                            'Dim dtOldApproverList As DataTable = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                            '                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                            '                                                        , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), Nothing, False)

                            Dim dtOldApproverList As DataTable = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                                    , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), Nothing, False)
                            'Gajanan [04-June-2020] -- End

                            Dim strApproverUnkids As String = ""
                            If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                                strApproverUnkids = String.Join(",", dtOldApproverList.AsEnumerable().Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("approverunkid").ToString()).ToArray())
                            End If



                            'Gajanan [11-Dec-2019] -- Start   
                            'Enhancement:Worked On December Cut Over Enhancement For NMB


                            'dtNewApproverList = objapprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                            '                                                         , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                            '                                                         , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                            '                                                         , True, True, True, False, -1, Nothing _
                            '                                                         , "", "", 0).Tables(0)

                            '<THIS METHOD IS CALLED WITHOUT EMPLOYEE, AS USER CAN DECIDE TO TAKE THAT EMPLOYEE TO ANY OTHER APPROVER/LEVEL>
                            dtNewApproverList = objapprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                     , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                     , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                     , True, True, True, False, -1, Nothing _
                                                         , If(mstrAdvanceFilter.Trim.Length > 0, " and " & mstrAdvanceFilter, mstrAdvanceFilter), "", 0).Tables(0)

                            'Gajanan [11-Dec-2019] -- End


                            If lstApprover Is Nothing Then lstApprover = New List(Of DataRow)

                            Dim strApproverUnkidsBasedOnEmployee As String = ""
                            '<THIS METHOD IS CALLED DUE TO IF PARTUCLAR EMPLOYEE FALLS IN OTHER LEVEL AND IT SHOULD NOT COME TO SAME APPROVER AGAIN FOR DIFFERENT LEVEL>
                            strApproverUnkidsBasedOnEmployee = objapprover.GetLeaveApproverUnkid(CInt(cboEmployee.SelectedValue), False)
                            If strApproverUnkidsBasedOnEmployee.Length > 0 Then
                                If strApproverUnkids.Length > 0 Then
                                    strApproverUnkids = strApproverUnkids & "," & strApproverUnkidsBasedOnEmployee
                                Else
                                    strApproverUnkids = strApproverUnkidsBasedOnEmployee
                                End If
                            End If

                            dtNewApproverList = New DataView(dtNewApproverList, "approverunkid NOT IN(" & strApproverUnkids & ") ", "", DataViewRowState.CurrentRows).ToTable()

                            lstApprover = dtNewApproverList.AsEnumerable().ToList()

                            dtNewApproverList = dtNewApproverList.DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")


                            'Gajanan [26-OCT-2019] -- Start    
                            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                            cboNewApprover.DataSource = Nothing
                            'Gajanan [26-OCT-2019] -- End

                            With cboNewApprover
                                .ValueMember = "leaveapproverunkid"
                                .DisplayMember = "name"
                            End With
                        End If


                    Case enApprovalMigration.Loan
                        GetApproverLevel(sender, CInt(cboOldApprover.SelectedValue), _
                                              CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                        Dim objLoanapprover As New clsLoanApprover_master

                        Dim dtOldApproverList As DataTable = objLoanapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, -1, FinancialYear._Object._YearUnkid, _
                                                                        Company._Object._Companyunkid, CInt(cboEmployee.SelectedValue), _
                                                                        ConfigParameter._Object._IsLoanApprover_ForLoanScheme, -1, "")

                        Dim strApproverUnkids As String = ""
                        If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                            strApproverUnkids = String.Join(",", dtOldApproverList.AsEnumerable().Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("lnapproverunkid").ToString()).ToArray())
                        End If



                        'Gajanan [11-Dec-2019] -- Start   
                        'Enhancement:Worked On December Cut Over Enhancement For NMB


                        'dtNewApproverList = objLoanapprover.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                        '                                                            FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                        '                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                        '                                                            ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, False, "", 0).Tables("List")


                        dtNewApproverList = objLoanapprover.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                                                    FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                                   ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, False, mstrAdvanceFilter, 0).Tables("List")
                        'Gajanan [11-Dec-2019] -- End


                        If lstApprover Is Nothing Then lstApprover = New List(Of DataRow)
                        Dim strApproverUnkidsBasedOnEmployee As String = ""
                        strApproverUnkidsBasedOnEmployee = objLoanapprover.GetApproverUnkid(CInt(cboEmployee.SelectedValue), False, Nothing)
                        If strApproverUnkidsBasedOnEmployee.Length > 0 Then
                            If strApproverUnkids.Length > 0 Then
                                strApproverUnkids = strApproverUnkids & "," & strApproverUnkidsBasedOnEmployee
                            Else
                                strApproverUnkids = strApproverUnkidsBasedOnEmployee
                            End If
                        End If

                        dtNewApproverList = New DataView(dtNewApproverList, "lnapproverunkid NOT IN(" & strApproverUnkids & ") and  approverempunkid NOT IN(" & cboEmployee.SelectedValue & ") and isexternalapprover =false", "", DataViewRowState.CurrentRows).ToTable()
                        lstApprover = dtNewApproverList.AsEnumerable().ToList()

                        dtNewApproverList = New DataView(dtNewApproverList, "", "", DataViewRowState.CurrentRows).ToTable(True, "approverempunkid", "ApproverName", "isexternalapprover")

                        'Gajanan [26-OCT-2019] -- Start    
                        'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                        cboNewApprover.DataSource = Nothing
                        'Gajanan [26-OCT-2019] -- End
                        With cboNewApprover
                            .ValueMember = "approverempunkid"
                            .DisplayMember = "ApproverName"
                        End With



                    Case enApprovalMigration.BudgetTimesheet
                        GetApproverLevel(sender, CInt(cboOldApprover.SelectedValue), _
                                              CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                        Dim objBugetTimeSheetapprover As New clstsapprover_master

                        Dim dtOldApproverList As DataTable = objBugetTimeSheetapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                                                Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                ConfigParameter._Object._IsIncludeInactiveEmp, -1, cboEmployee.SelectedValue)

                        Dim strApproverUnkids As String = ""
                        If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                            strApproverUnkids = String.Join(",", dtOldApproverList.AsEnumerable().Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("tsapproverunkid").ToString()).ToArray())
                        End If


                        'Gajanan [11-Dec-2019] -- Start   
                        'Enhancement:Worked On December Cut Over Enhancement For NMB


                        'dtNewApproverList = objBugetTimeSheetapprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                        '                                                                      FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                        '                                                                      ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                        '                                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, -1, Nothing, "", "", 0).Tables("List")

                        dtNewApproverList = objBugetTimeSheetapprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                                                              FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                                              ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                                                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, -1, Nothing, If(mstrAdvanceFilter.Trim.Length > 0, " and " & mstrAdvanceFilter, mstrAdvanceFilter), "", 0).Tables("List")
                        'Gajanan [11-Dec-2019] -- End



                        If lstApprover Is Nothing Then lstApprover = New List(Of DataRow)

                        Dim strApproverUnkidsBasedOnEmployee As String = ""

                        'selected employee is approver or not if it's approver then get it's id and discard it from to approver list
                        strApproverUnkidsBasedOnEmployee = objBugetTimeSheetapprover.GetTimesheetApproverEmployeeId(CInt(cboEmployee.SelectedValue), False)

                        If strApproverUnkidsBasedOnEmployee.Length > 0 Then
                            If strApproverUnkids.Length > 0 Then
                                strApproverUnkids = strApproverUnkids & "," & strApproverUnkidsBasedOnEmployee
                            Else
                                strApproverUnkids = strApproverUnkidsBasedOnEmployee
                            End If
                        End If

                        dtNewApproverList = New DataView(dtNewApproverList, "tsapproverunkid NOT IN(" & strApproverUnkids & ")", "", DataViewRowState.CurrentRows).ToTable()
                        lstApprover = dtNewApproverList.AsEnumerable().ToList()
                        dtNewApproverList = New DataView(dtNewApproverList, "", "", DataViewRowState.CurrentRows).ToTable(True, "employeeapproverunkid", "name", "isexternalapprover")

                        'Gajanan [26-OCT-2019] -- Start    
                        'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                        cboNewApprover.DataSource = Nothing
                        'Gajanan [26-OCT-2019] -- End

                        With cboNewApprover
                            .ValueMember = "employeeapproverunkid"
                            .DisplayMember = "name"
                        End With


                    Case enApprovalMigration.Claim

                        GetApproverLevel(sender, CInt(cboOldApprover.SelectedValue), _
                                              CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                        Dim objExpenseapprover As New clsExpenseApprover_Master


                        'Gajanan [02-June-2020] -- Start
                        'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                        'Dim dtOldApproverList As DataTable = objExpenseapprover.GetApproverFromEmployeeId(CInt(cboEmployee.SelectedValue), CInt(cboExCategory.SelectedValue)).Tables(0)
                        Dim dtOldApproverList As DataTable = objExpenseapprover.GetApproverFromEmployeeId(FinancialYear._Object._DatabaseName, _
                                                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                                          True, _
                                                                                                          CInt(cboEmployee.SelectedValue), CInt(cboExCategory.SelectedValue)).Tables(0)
                        'Gajanan [02-June-2020] -- End

                        Dim strApproverUnkids As String = ""
                        If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                            strApproverUnkids = String.Join(",", dtOldApproverList.AsEnumerable().Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("approverunkid").ToString()).ToArray())
                        End If


                        'Gajanan [11-Dec-2019] -- Start   
                        'Enhancement:Worked On December Cut Over Enhancement For NMB


                        'dtNewApproverList = objExpenseapprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                        '                                                                      FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                        '                                                                      ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                        '                                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "", True).Tables("List")

                        dtNewApproverList = objExpenseapprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                                                              FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                                              ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                                                                             True, ConfigParameter._Object._IsIncludeInactiveEmp, True, mstrAdvanceFilter, True).Tables("List")
                        'Gajanan [11-Dec-2019] -- End


                        If lstApprover Is Nothing Then lstApprover = New List(Of DataRow)


                        Dim strApproverUnkidsBasedOnEmployee As String = ""
                        strApproverUnkidsBasedOnEmployee = objExpenseapprover.GetClaimApproverUnkid(CInt(cboExCategory.SelectedValue), (CInt(cboEmployee.SelectedValue)), False)
                        If strApproverUnkidsBasedOnEmployee.Length > 0 Then
                            If strApproverUnkids.Length > 0 Then
                                strApproverUnkids = strApproverUnkids & "," & strApproverUnkidsBasedOnEmployee
                            Else
                                strApproverUnkids = strApproverUnkidsBasedOnEmployee
                            End If
                        End If



                        dtNewApproverList = New DataView(dtNewApproverList, "expensetypeid = " & CInt(cboExCategory.SelectedValue) & " and  crapproverunkid NOT IN(" & strApproverUnkids & ")", "", DataViewRowState.CurrentRows).ToTable()
                        lstApprover = dtNewApproverList.AsEnumerable().ToList()
                        dtNewApproverList = New DataView(dtNewApproverList, "", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "isexternalapprover", "ename")

                        'Gajanan [26-OCT-2019] -- Start    
                        'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                        cboNewApprover.DataSource = Nothing
                        'Gajanan [26-OCT-2019] -- End

                        With cboNewApprover
                            .ValueMember = "employeeunkid"
                            .DisplayMember = "ename"
                        End With

                    Case enApprovalMigration.Performance
                        Dim objAssesmentApprover As New clsAssessor_tran
                        Dim dtOldApproverList As DataTable = Nothing



                        If radAssessor.Checked Then
                            dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_ASSESSOR, CInt(cboEmployee.SelectedValue), _
                                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                            FinancialYear._Object._DatabaseName).Tables(0)

                        ElseIf radReviewer.Checked Then

                            dtOldApproverList = Nothing
                            dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_REVIEWER, CInt(cboEmployee.SelectedValue), _
                                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                            FinancialYear._Object._DatabaseName).Tables(0)


                        End If
                        Dim strApproverUnkids As String = ""
                        If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                            strApproverUnkids = String.Join(",", dtOldApproverList.AsEnumerable().Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("assessormasterunkid").ToString()).ToArray())
                        End If

                        Dim objAssessor As New clsAssessor
                        Dim blnReviewerFlag As Boolean = False
                        If radAssessor.Checked = True Then
                            blnReviewerFlag = False
                        ElseIf radReviewer.Checked = True Then
                            blnReviewerFlag = True
                        End If
                        Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE


                        dtNewApproverList = objAssessor.GetList(FinancialYear._Object._DatabaseName, _
                                        User._Object._Userunkid, _
                                        FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        ConfigParameter._Object._UserAccessModeSetting, True, _
                                        ConfigParameter._Object._IsIncludeInactiveEmp, "List", blnReviewerFlag, strVisibleTypeIds.ToString, True, mstrAdvanceFilter).Tables("List")

                        Dim strApproverUnkidsBasedOnEmployee As String = ""
                        strApproverUnkidsBasedOnEmployee = objAssesmentApprover.Get_AssignedEmp(CInt(cboEmployee.SelectedValue), blnReviewerFlag, False, strVisibleTypeIds)
                        If strApproverUnkidsBasedOnEmployee.Length > 0 Then
                            If strApproverUnkids.Length > 0 Then
                                strApproverUnkids = strApproverUnkids & "," & strApproverUnkidsBasedOnEmployee
                            Else
                                strApproverUnkids = strApproverUnkidsBasedOnEmployee
                            End If
                        End If

                        dtNewApproverList = New DataView(dtNewApproverList, "assessormasterunkid NOT IN(" & strApproverUnkids & ") ", "", DataViewRowState.CurrentRows).ToTable()

                        lstApprover = dtNewApproverList.AsEnumerable().ToList()

                        'Gajanan [26-OCT-2019] -- Start    
                        'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                        cboNewApprover.DataSource = Nothing
                        'Gajanan [26-OCT-2019] -- End

                        With cboNewApprover
                            .ValueMember = "employeeunkid"
                            .DisplayMember = "assessorname"
                        End With

                    Case enApprovalMigration.ReportingTo
                        Dim objReportingToEmployee As New clsReportingToEmployee
                        Dim dtOldApproverList As DataTable = Nothing

                        If CInt(cboOldApprover.SelectedValue) > 0 Then
                            objReportingToEmployee._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                            dtOldApproverList = objReportingToEmployee._RDataTable.Copy()

                            Dim strApproverUnkids As String = ""
                            If IsNothing(dtOldApproverList) = False AndAlso dtOldApproverList.Rows.Count > 0 Then
                                strApproverUnkids = String.Join(",", dtOldApproverList.AsEnumerable().Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("reporttoemployeeunkid").ToString()).ToArray())
                            End If

                            'Gajanan [11-Dec-2019] -- Start   
                            'Enhancement:Worked On December Cut Over Enhancement For NMB


                            'dtNewApproverList = objReportingToEmployee.Get_ReportingToEmp("List", _
                            '                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                CInt(cboEmployee.SelectedValue), _
                            '                True)

                            dtNewApproverList = objReportingToEmployee.Get_ReportingToEmp("List", _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, _
                                            True, mstrAdvanceFilter)
                            'Gajanan [11-Dec-2019] -- End

                            If strApproverUnkids.Length > 0 Then
                                strApproverUnkids &= "," & cboEmployee.SelectedValue.ToString()
                            Else
                                strApproverUnkids = cboEmployee.SelectedValue
                            End If
                            dtNewApproverList = New DataView(dtNewApproverList, "employeeunkid NOT IN(" & strApproverUnkids & ")", "", DataViewRowState.CurrentRows).ToTable()

                            'Gajanan [26-OCT-2019] -- Start    
                            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                            cboNewApprover.DataSource = Nothing
                            'Gajanan [26-OCT-2019] -- End
                            With cboNewApprover
                                .ValueMember = "employeeunkid"
                                .DisplayMember = "ename"
                            End With
                        End If

                        'S.SANDEEP |04-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                    Case enApprovalMigration.OTRequisition
                        GetApproverLevel(sender, CInt(cboOldApprover.SelectedValue), _
                                              CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))

                        Dim objOTRequisitionApprover As New clsTnaapprover_master

                        Dim strApproverUnkids As String = ""
                        If IsNothing(CType(cboOldApprover.DataSource, DataTable)) = False AndAlso (CType(cboOldApprover.DataSource, DataTable).Rows.Count > 0) Then
                            strApproverUnkids = String.Join(",", (CType(cboOldApprover.DataSource, DataTable).AsEnumerable().Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("tnamappingunkid").ToString()).ToArray()))
                        End If

                        '<THIS METHOD IS CALLED WITHOUT EMPLOYEE, AS USER CAN DECIDE TO TAKE THAT EMPLOYEE TO ANY OTHER APPROVER/LEVEL>
                        dtNewApproverList = objOTRequisitionApprover.GetList("List", _
                                                                             FinancialYear._Object._DatabaseName, _
                                                                             enTnAApproverType.OT_Requisition_Approver, _
                                                                             ConfigParameter._Object._EmployeeAsOnDate, _
                                                                             False, True, Nothing, mstrAdvanceFilter, 0, _
                                                                             CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isotcap_hod"))).Tables(0)

                        If lstApprover Is Nothing Then lstApprover = New List(Of DataRow)

                        Dim strApproverUnkidsBasedOnEmployee As String = ""
                        '<THIS METHOD IS CALLED DUE TO IF PARTUCLAR EMPLOYEE FALLS IN OTHER LEVEL AND IT SHOULD NOT COME TO SAME APPROVER AGAIN FOR DIFFERENT LEVEL>
                        strApproverUnkidsBasedOnEmployee = objOTRequisitionApprover.GetTnAApproverUnkid(CInt(cboEmployee.SelectedValue), False)
                        If strApproverUnkidsBasedOnEmployee.Length > 0 Then
                            If strApproverUnkids.Length > 0 Then
                                strApproverUnkids = strApproverUnkids & "," & strApproverUnkidsBasedOnEmployee
                            Else
                                strApproverUnkids = strApproverUnkidsBasedOnEmployee
                            End If
                        End If

                        dtNewApproverList = New DataView(dtNewApproverList, "tnamappingunkid NOT IN(" & strApproverUnkids & ") ", "", DataViewRowState.CurrentRows).ToTable()

                        lstApprover = dtNewApproverList.AsEnumerable().ToList()

                        dtNewApproverList = dtNewApproverList.DefaultView.ToTable(True, "approveremployeeunkid", "name", "isexternalapprover", "isotcap_hod")

                        cboNewApprover.DataSource = Nothing

                        With cboNewApprover
                            .ValueMember = "approveremployeeunkid"
                            .DisplayMember = "name"
                        End With

                        'S.SANDEEP |04-MAR-2020| -- END
                End Select


                If IsNothing(dtNewApproverList) = False Then
                    dtNewApproverList = AddSelectToDatatable(dtNewApproverList, cboNewApprover)
                    cboNewApprover.SelectedValue = 0
                    cboNewApprover.DataSource = dtNewApproverList
                End If
                'Gajanan [26-OCT-2019] -- Start    
                'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
            Else

                cboNewApprover.DataSource = Nothing
                Dim dt As DataTable = createEmptycomboDt()
                cboNewApprover.ValueMember = "id"
                cboNewApprover.DisplayMember = "name"
                cboNewApprover.DataSource = dt
                cboNewApprover.SelectedValue = 0

                cboOldLevel.DataSource = Nothing
                cboOldLevel.ValueMember = "id"
                cboOldLevel.DisplayMember = "name"
                cboOldLevel.DataSource = dt.Copy()
                cboOldLevel.SelectedValue = 0
                'Gajanan [26-OCT-2019] -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboExCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            ClearApproverAndLevelCombo()
            If cboExCategory.SelectedValue > 0 Then

                Dim dtOldApproverList As DataTable = Nothing
                Dim objExpenseapprover As New clsExpenseApprover_Master

                'Gajanan [02-June-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dtOldApproverList = objExpenseapprover.GetApproverFromEmployeeId(CInt(cboEmployee.SelectedValue), CInt(cboExCategory.SelectedValue)).Tables(0)
                dtOldApproverList = objExpenseapprover.GetApproverFromEmployeeId(FinancialYear._Object._DatabaseName, _
                                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                 True, _
                                                                                 CInt(cboEmployee.SelectedValue), CInt(cboExCategory.SelectedValue)).Tables(0)
                'Gajanan [02-June-2020] -- End

                lstOldApprover = dtOldApproverList.AsEnumerable().ToList()
                If IsNothing(dtOldApproverList) = False Then
                    SetNewApproverCombo(dtOldApproverList)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "RadioButton Method"
    Private Sub radAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAssessor.CheckedChanged, radReviewer.CheckedChanged
        Try
            If cboScreenType.SelectedValue > 0 Then
                Dim dtOldApproverList As DataTable = Nothing
                Dim objAssesmentApprover As New clsAssessor_tran
                'Dim objAssesmentApprover As New clsAssessor


                ClearApproverAndLevelCombo()
                Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE
                If radAssessor.Checked Then
                    dtOldApproverList = Nothing
                    dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_ASSESSOR, CInt(cboEmployee.SelectedValue), _
                                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    FinancialYear._Object._DatabaseName).Tables(0)



                ElseIf radReviewer.Checked Then

                    dtOldApproverList = Nothing
                    dtOldApproverList = objAssesmentApprover.GetEmployeeAssessorReviewer(enApprFilterTypeId.APRL_REVIEWER, CInt(cboEmployee.SelectedValue), _
                                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    FinancialYear._Object._DatabaseName).Tables(0)




                End If


                If IsNothing(dtOldApproverList) = False Then
                    lstOldApprover = dtOldApproverList.AsEnumerable.ToList()
                    SetNewApproverCombo(dtOldApproverList)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAssessor_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Gridview Method"
    Private Sub dgOldApproverEmp_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvApproverMigration.DataBindingComplete
        Try
            lblnote.Visible = False
            dgvApproverMigration.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CInt(x.Cells(objcolhOldApproverId.Index).Value) <> CInt(x.Cells(objcolhNewApproverId.Index).Value)).ToList().ForEach(Function(x) UpdateRowColor(x))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOldApproverEmp_DataBindingComplete", mstrModuleName)
        End Try
    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSet.GradientBackColor = GUI._ButttonBackColor
            Me.btnSet.GradientForeColor = GUI._ButttonFontColor

            Me.btnRetain.GradientBackColor = GUI._ButttonBackColor
            Me.btnRetain.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
            Me.radReviewer.Text = Language._Object.getCaption(Me.radReviewer.Name, Me.radReviewer.Text)
            Me.radAssessor.Text = Language._Object.getCaption(Me.radAssessor.Name, Me.radAssessor.Text)
            Me.lblNewApprover.Text = Language._Object.getCaption(Me.lblNewApprover.Name, Me.lblNewApprover.Text)
            Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.lblNewLevel.Text = Language._Object.getCaption(Me.lblNewLevel.Name, Me.lblNewLevel.Text)
            Me.radOverWriteAssessment.Text = Language._Object.getCaption(Me.radOverWriteAssessment.Name, Me.radOverWriteAssessment.Text)
            Me.radVoidAssessment.Text = Language._Object.getCaption(Me.radVoidAssessment.Name, Me.radVoidAssessment.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.btnSet.Text = Language._Object.getCaption(Me.btnSet.Name, Me.btnSet.Text)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.lblnote.Text = Language._Object.getCaption(Me.lblnote.Name, Me.lblnote.Text)
            Me.btnRetain.Text = Language._Object.getCaption(Me.btnRetain.Name, Me.btnRetain.Text)
            Me.colhdgScreenType.HeaderText = Language._Object.getCaption(Me.colhdgScreenType.Name, Me.colhdgScreenType.HeaderText)
            Me.colhdgOldApprover.HeaderText = Language._Object.getCaption(Me.colhdgOldApprover.Name, Me.colhdgOldApprover.HeaderText)
            Me.colhdgNewApprover.HeaderText = Language._Object.getCaption(Me.colhdgNewApprover.Name, Me.colhdgNewApprover.HeaderText)
            Me.lblScreenTypeApprover.Text = Language._Object.getCaption(Me.lblScreenTypeApprover.Name, Me.lblScreenTypeApprover.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Leave")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "Budget Timesheet")
            Language.setMessage(mstrModuleName, 4, "Claim")
            Language.setMessage(mstrModuleName, 5, "Performance")
            Language.setMessage(mstrModuleName, 6, "Reporting To")
            Language.setMessage(mstrModuleName, 10, "Sorry, From Approver Level is mandatory information. Please select From Approver to continue.")
            Language.setMessage(mstrModuleName, 11, "Sorry, To Approver Level is mandatory information. Please select To Approver to continue.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Expense Type is mandatory information. Please select Expense Type to continue.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Selected employee is already binded with the selected to approver.")
            Language.setMessage(mstrModuleName, 14, "Select")
            Language.setMessage(mstrModuleName, 15, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Screen type is mandatory information. Please select Screen to continue.")
            Language.setMessage(mstrModuleName, 17, "Sorry, From Approver is mandatory information. Please select From Approver to continue.")
            Language.setMessage(mstrModuleName, 18, "Sorry, To Approver is mandatory information. Please select To Approver to continue.")
            Language.setMessage(mstrModuleName, 19, "Sorry, Please select atleast one approver type in order to do continue process.")
            Language.setMessage(mstrModuleName, 20, "Sorry, Please select atleast one operation type in order to do continue process.")
            Language.setMessage(mstrModuleName, 21, "Are you sure you want to close this screen ? if you close this screen then all transection which were done will not saved.Do you still want to continue?")
            Language.setMessage(mstrModuleName, 22, "Approver List")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
