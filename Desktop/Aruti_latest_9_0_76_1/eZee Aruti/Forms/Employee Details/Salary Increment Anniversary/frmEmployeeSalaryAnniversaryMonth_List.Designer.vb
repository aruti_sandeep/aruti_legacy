﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSalaryAnniversaryMonth_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSalaryAnniversaryMonth_List))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowAllAsOnDate = New System.Windows.Forms.CheckBox
        Me.nudToMonth = New System.Windows.Forms.NumericUpDown
        Me.lblAnniversaryMonthTo = New System.Windows.Forms.Label
        Me.nudFromMonth = New System.Windows.Forms.NumericUpDown
        Me.lblAnniversaryMonthFrom = New System.Windows.Forms.Label
        Me.dtpAsOnDate = New System.Windows.Forms.DateTimePicker
        Me.lblAsOnDate = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.dgvSalaryAnniversaryMonthList = New System.Windows.Forms.DataGridView
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.objcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAnniversaryMonth = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppointDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhConfirmationDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpSalaryAnniversaryunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSalaryAnniversaryMonthList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 351)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(863, 55)
        Me.objFooter.TabIndex = 21
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(651, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(548, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 3
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(754, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowAllAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.nudToMonth)
        Me.gbFilterCriteria.Controls.Add(Me.lblAnniversaryMonthTo)
        Me.gbFilterCriteria.Controls.Add(Me.nudFromMonth)
        Me.gbFilterCriteria.Controls.Add(Me.lblAnniversaryMonthFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(839, 63)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowAllAsOnDate
        '
        Me.chkShowAllAsOnDate.AutoSize = True
        Me.chkShowAllAsOnDate.Checked = True
        Me.chkShowAllAsOnDate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowAllAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowAllAsOnDate.Location = New System.Drawing.Point(708, 35)
        Me.chkShowAllAsOnDate.Name = "chkShowAllAsOnDate"
        Me.chkShowAllAsOnDate.Size = New System.Drawing.Size(124, 17)
        Me.chkShowAllAsOnDate.TabIndex = 4
        Me.chkShowAllAsOnDate.Text = "Show All As On Date"
        Me.chkShowAllAsOnDate.UseVisualStyleBackColor = True
        '
        'nudToMonth
        '
        Me.nudToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToMonth.Location = New System.Drawing.Point(649, 33)
        Me.nudToMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudToMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudToMonth.Name = "nudToMonth"
        Me.nudToMonth.Size = New System.Drawing.Size(50, 21)
        Me.nudToMonth.TabIndex = 3
        Me.nudToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudToMonth.Value = New Decimal(New Integer() {12, 0, 0, 0})
        '
        'lblAnniversaryMonthTo
        '
        Me.lblAnniversaryMonthTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnniversaryMonthTo.Location = New System.Drawing.Point(614, 36)
        Me.lblAnniversaryMonthTo.Name = "lblAnniversaryMonthTo"
        Me.lblAnniversaryMonthTo.Size = New System.Drawing.Size(29, 15)
        Me.lblAnniversaryMonthTo.TabIndex = 246
        Me.lblAnniversaryMonthTo.Text = "To"
        Me.lblAnniversaryMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'nudFromMonth
        '
        Me.nudFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromMonth.Location = New System.Drawing.Point(558, 33)
        Me.nudFromMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudFromMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudFromMonth.Name = "nudFromMonth"
        Me.nudFromMonth.Size = New System.Drawing.Size(50, 21)
        Me.nudFromMonth.TabIndex = 2
        Me.nudFromMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudFromMonth.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblAnniversaryMonthFrom
        '
        Me.lblAnniversaryMonthFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnniversaryMonthFrom.Location = New System.Drawing.Point(423, 36)
        Me.lblAnniversaryMonthFrom.Name = "lblAnniversaryMonthFrom"
        Me.lblAnniversaryMonthFrom.Size = New System.Drawing.Size(129, 15)
        Me.lblAnniversaryMonthFrom.TabIndex = 153
        Me.lblAnniversaryMonthFrom.Text = "Anniversary Month From"
        Me.lblAnniversaryMonthFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAsOnDate
        '
        Me.dtpAsOnDate.Checked = False
        Me.dtpAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsOnDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsOnDate.Location = New System.Drawing.Point(314, 33)
        Me.dtpAsOnDate.Name = "dtpAsOnDate"
        Me.dtpAsOnDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpAsOnDate.TabIndex = 1
        '
        'lblAsOnDate
        '
        Me.lblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnDate.Location = New System.Drawing.Point(239, 36)
        Me.lblAsOnDate.Name = "lblAsOnDate"
        Me.lblAsOnDate.Size = New System.Drawing.Size(69, 15)
        Me.lblAsOnDate.TabIndex = 243
        Me.lblAsOnDate.Text = "As On Date"
        Me.lblAsOnDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(813, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(55, 15)
        Me.lblEmployee.TabIndex = 238
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(789, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 250
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(75, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(158, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'dgvSalaryAnniversaryMonthList
        '
        Me.dgvSalaryAnniversaryMonthList.AllowUserToAddRows = False
        Me.dgvSalaryAnniversaryMonthList.AllowUserToDeleteRows = False
        Me.dgvSalaryAnniversaryMonthList.AllowUserToResizeRows = False
        Me.dgvSalaryAnniversaryMonthList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvSalaryAnniversaryMonthList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvSalaryAnniversaryMonthList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSalaryAnniversaryMonthList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhSelect, Me.dgcolhEmployeeCode, Me.dgcolhEmployeeName, Me.dgcolhEffectiveDate, Me.dgcolhAnniversaryMonth, Me.dgcolhAppointDate, Me.dgcolhConfirmationDate, Me.objdgcolhEmployeeUnkId, Me.objdgcolhEmpSalaryAnniversaryunkid})
        Me.dgvSalaryAnniversaryMonthList.Location = New System.Drawing.Point(12, 81)
        Me.dgvSalaryAnniversaryMonthList.Name = "dgvSalaryAnniversaryMonthList"
        Me.dgvSalaryAnniversaryMonthList.RowHeadersVisible = False
        Me.dgvSalaryAnniversaryMonthList.Size = New System.Drawing.Size(839, 264)
        Me.dgvSalaryAnniversaryMonthList.TabIndex = 0
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(22, 86)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 23
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'objcolhSelect
        '
        DataGridViewCellStyle1.NullValue = False
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.objcolhSelect.DefaultCellStyle = DataGridViewCellStyle1
        Me.objcolhSelect.HeaderText = ""
        Me.objcolhSelect.Name = "objcolhSelect"
        Me.objcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhSelect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhSelect.Width = 30
        '
        'dgcolhEmployeeCode
        '
        Me.dgcolhEmployeeCode.HeaderText = "Employee Code"
        Me.dgcolhEmployeeCode.Name = "dgcolhEmployeeCode"
        Me.dgcolhEmployeeCode.ReadOnly = True
        Me.dgcolhEmployeeCode.Width = 115
        '
        'dgcolhEmployeeName
        '
        Me.dgcolhEmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEmployeeName.HeaderText = "Employee Name"
        Me.dgcolhEmployeeName.Name = "dgcolhEmployeeName"
        Me.dgcolhEmployeeName.ReadOnly = True
        '
        'dgcolhEffectiveDate
        '
        Me.dgcolhEffectiveDate.HeaderText = "Effective Date"
        Me.dgcolhEffectiveDate.Name = "dgcolhEffectiveDate"
        Me.dgcolhEffectiveDate.ReadOnly = True
        Me.dgcolhEffectiveDate.Width = 120
        '
        'dgcolhAnniversaryMonth
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAnniversaryMonth.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhAnniversaryMonth.HeaderText = "Anniversary Month"
        Me.dgcolhAnniversaryMonth.Name = "dgcolhAnniversaryMonth"
        Me.dgcolhAnniversaryMonth.ReadOnly = True
        Me.dgcolhAnniversaryMonth.Width = 125
        '
        'dgcolhAppointDate
        '
        Me.dgcolhAppointDate.HeaderText = "Appoint Date"
        Me.dgcolhAppointDate.Name = "dgcolhAppointDate"
        Me.dgcolhAppointDate.Width = 120
        '
        'dgcolhConfirmationDate
        '
        Me.dgcolhConfirmationDate.HeaderText = "Confirmation Date"
        Me.dgcolhConfirmationDate.Name = "dgcolhConfirmationDate"
        Me.dgcolhConfirmationDate.Width = 120
        '
        'objdgcolhEmployeeUnkId
        '
        Me.objdgcolhEmployeeUnkId.HeaderText = "Employee Id"
        Me.objdgcolhEmployeeUnkId.Name = "objdgcolhEmployeeUnkId"
        Me.objdgcolhEmployeeUnkId.ReadOnly = True
        Me.objdgcolhEmployeeUnkId.Visible = False
        Me.objdgcolhEmployeeUnkId.Width = 5
        '
        'objdgcolhEmpSalaryAnniversaryunkid
        '
        Me.objdgcolhEmpSalaryAnniversaryunkid.HeaderText = "Employee Salary Anniversary Id"
        Me.objdgcolhEmpSalaryAnniversaryunkid.Name = "objdgcolhEmpSalaryAnniversaryunkid"
        Me.objdgcolhEmpSalaryAnniversaryunkid.Visible = False
        Me.objdgcolhEmpSalaryAnniversaryunkid.Width = 5
        '
        'frmEmployeeSalaryAnniversaryMonth_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 406)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.dgvSalaryAnniversaryMonthList)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeSalaryAnniversaryMonth_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Salary Anniversary Month List"
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSalaryAnniversaryMonthList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents dtpAsOnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAsOnDate As System.Windows.Forms.Label
    Friend WithEvents lblAnniversaryMonthFrom As System.Windows.Forms.Label
    Friend WithEvents nudFromMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblAnniversaryMonthTo As System.Windows.Forms.Label
    Friend WithEvents nudToMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents dgvSalaryAnniversaryMonthList As System.Windows.Forms.DataGridView
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowAllAsOnDate As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAnniversaryMonth As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppointDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhConfirmationDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpSalaryAnniversaryunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
