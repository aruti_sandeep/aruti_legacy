﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeSalaryIncrementAnniversary

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmEmployeeSalaryIncrementAnniversary"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objEmpSalAnniversary As clsEmployee_salary_anniversary_tran
    Private mstrAdvanceFilter As String = ""
    Private mstrSearchText As String = "" 'Sohail (22 Sep 2016)
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction

            Me.ShowDialog()


            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Function "

    Private Sub FillEmployeeCombo()
        Dim dsCombos As DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpAsOnDate.Value.Date, dtpAsOnDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , , , True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim strInnerFilter As String = ""
        Dim strOuterFilter As String = ""
        Try
            If dtpFromAppointment.Checked = True AndAlso dtpToAppointment.Checked = True Then
                If dtpFromAppointment.Value.Date > dtpToAppointment.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Appointment To Date should be greater than Appointment From Date."), enMsgBoxStyle.Information)
                    dtpFromAppointment.Focus()
                    Exit Sub
                End If
            End If

            If dtpFromConfirm.Checked = True AndAlso dtpToConfirm.Checked = True Then
                If dtpFromConfirm.Value.Date > dtpToConfirm.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Confirmation To Date should be greater than Confirmation From Date."), enMsgBoxStyle.Information)
                    dtpFromConfirm.Focus()
                    Exit Sub
                End If
            End If

            If nudFromMonth.Value > nudToMonth.Value Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Appointment To Month should be greater than Appointment From Month."), enMsgBoxStyle.Information)
                nudFromMonth.Focus()
                Exit Sub
            End If

            strInnerFilter &= " MONTH(hremployee_master.appointeddate) BETWEEN " & nudFromMonth.Value & " AND " & nudToMonth.Value & " " & _
                              " AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= " & eZeeDate.convertDate(dtpEffectiveDate.Value.Date) & " "

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strInnerFilter &= " AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If chkNeverAssignedOnly.Checked = True Then
                strInnerFilter &= " AND hremployee_salary_anniversary_tran.employeeunkid IS NULL "
            End If

            If dtpFromAppointment.Checked = True AndAlso dtpToAppointment.Checked = True Then
                strInnerFilter &= " AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN '" & eZeeDate.convertDate(dtpFromAppointment.Value.Date) & "' AND '" & eZeeDate.convertDate(dtpToAppointment.Value.Date) & "' "
            ElseIf dtpFromAppointment.Checked = True Then
                strInnerFilter &= " AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) >= '" & eZeeDate.convertDate(dtpFromAppointment.Value.Date) & "' "
            ElseIf dtpToAppointment.Checked = True Then
                strInnerFilter &= " AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= '" & eZeeDate.convertDate(dtpToAppointment.Value.Date) & "' "
            End If

            If dtpFromConfirm.Checked = True AndAlso dtpToConfirm.Checked = True Then
                strInnerFilter &= " AND CONVERT(CHAR(8), hremployee_master.confirmation_date, 112) BETWEEN '" & eZeeDate.convertDate(dtpFromConfirm.Value.Date) & "' AND '" & eZeeDate.convertDate(dtpToConfirm.Value.Date) & "' "
            ElseIf dtpFromConfirm.Checked = True Then
                strInnerFilter &= " AND CONVERT(CHAR(8), hremployee_master.confirmation_date, 112) >= '" & eZeeDate.convertDate(dtpFromConfirm.Value.Date) & "' "
            ElseIf dtpToConfirm.Checked = True Then
                strInnerFilter &= " AND CONVERT(CHAR(8), hremployee_master.confirmation_date, 112) <= '" & eZeeDate.convertDate(dtpToConfirm.Value.Date) & "' "
            End If

            If mstrAdvanceFilter.Trim <> "" Then
                strInnerFilter &= " AND " & mstrAdvanceFilter
            End If

            'Sohail (22 Sep 2016) -- Start
            'Issue - 63.1 - Employees were not coming in list on assign screen once annivarsary transaction deleted.
            'dsList = objEmpSalAnniversary.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpAsOnDate.Value.Date, dtpAsOnDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, strInnerFilter, "", " A.employeename ASC ", True)
            dsList = objEmpSalAnniversary.GetListForAssignment(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpAsOnDate.Value.Date, dtpAsOnDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, strInnerFilter, "", " A.employeename ASC ", True)
            'Sohail (22 Sep 2016) -- End

            dgvSalaryAnniversary.AutoGenerateColumns = False

            objcolhCheckAll.DataPropertyName = "IsChecked"
            objcolhEmployeeUnkId.DataPropertyName = "employeeunkid"
            colhEmployeeCode.DataPropertyName = "employeecode"
            colhEmployeeName.DataPropertyName = "employeename"
            colhAppointmentDate.DataPropertyName = "appointeddate"
            colhEffectiveDate.DataPropertyName = "effectivedate"
            colhAnniversaryMonth.DataPropertyName = "anniversarymonth"
            colhNewEffectiveDate.DataPropertyName = "neweffectivedate"
            colhNewAnniversaryMonth.DataPropertyName = "newanniversarymonth"

            colhAppointmentDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            colhEffectiveDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            colhNewEffectiveDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern

            dgvSalaryAnniversary.DataSource = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvSalaryAnniversary.RowCount.ToString)
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidated", mstrModuleName)
        End Try
    End Function

    'Sohail (22 Sep 2016) -- Start
    'Issue - 63.1 - Employees were not coming in list on assign screen once annivarsary transaction deleted.
    Private Sub SetDefaultSearchText()
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 11, "Type to Search")
            With cboEmployee
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Sep 2016) -- End
#End Region

#Region " Form's Events "

    Private Sub frmEmployeeSalaryIncrementAnniversary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpSalAnniversary = New clsEmployee_salary_anniversary_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Call SetVisibility()

            Call FillEmployeeCombo()
            Call SetDefaultSearchText() 'Sohail (22 Sep 2016)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSalaryIncrementAnniversary_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeSalaryIncrementAnniversary_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpSalAnniversary = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsEmployee_salary_anniversary_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_salary_anniversary_tran"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                    cboEmployee.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    'Sohail (22 Sep 2016) -- Start
    'Issue - 63.1 - Employees were not coming in list on assign screen once annivarsary transaction deleted.
    'Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
    '    Try
    '        If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Sep 2016) -- End
#End Region

#Region " DataGrid's Event "

    Private Sub dgvSalaryAnniversary_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvSalaryAnniversary.CellValidating
        Try
            If e.RowIndex = -1 Then Exit Sub

            If e.ColumnIndex = colhNewAnniversaryMonth.Index Then
                Dim i As Integer = 0
                If e.FormattedValue.ToString.Trim <> "" Then
                    If Integer.TryParse(e.FormattedValue.ToString, i) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter the correct Anniversary Month."), enMsgBoxStyle.Information)
                        e.Cancel = True
                    ElseIf i < 1 OrElse i > 12 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Anniversary Month should be between 1 and 12."), enMsgBoxStyle.Information)
                        e.Cancel = True
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSalaryAnniversary_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvSalaryAnniversary_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvSalaryAnniversary.DataError

    End Sub

    Private Sub dgvSalaryAnniversary_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvSalaryAnniversary.EditingControlShowing
        Try
            If dgvSalaryAnniversary.CurrentCell.ColumnIndex = colhNewAnniversaryMonth.Index AndAlso TypeOf e.Control Is TextBox Then
                Dim tb As TextBox
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
            Else
                Dim tb As TextBox
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSalaryAnniversary_EditingControlShowing", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApplytoChecked_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplytoChecked.Click
        Try
            If CType(dgvSalaryAnniversary.DataSource, DataTable).Select("IsChecked = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please tick atleast one transaction."), enMsgBoxStyle.Information)
                dgvSalaryAnniversary.Focus()
                Exit Sub
            End If

            For Each dtRow As DataRow In CType(dgvSalaryAnniversary.DataSource, DataTable).Select("IsChecked = 1 ")

                If dtRow.Item("appointeddatestring").ToString > eZeeDate.convertDate(dtpEffectiveDate.Value.Date) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, New Effective Date should be greater than Appointment Date for this employee") & " " & dtRow.Item("employeecode").ToString & " : " & dtRow.Item("employeename").ToString, enMsgBoxStyle.Information)
                    Exit Sub
                ElseIf IsDBNull(dtRow.Item("effectivedate")) = True Then
                    dtRow.Item("neweffectivedate") = dtRow.Item("appointeddate")
                ElseIf dtRow.Item("effectivedatestring").ToString >= eZeeDate.convertDate(dtpEffectiveDate.Value.Date) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, New Effective Date should not be Same As Current Effective Date for this employee") & " " & dtRow.Item("employeecode").ToString & " : " & dtRow.Item("employeename").ToString, enMsgBoxStyle.Information)
                    Exit Sub
                ElseIf dtpEffectiveDate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, New Effective Date should not be greater than current financial year end date."), enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    dtRow.Item("neweffectivedate") = dtpEffectiveDate.Value.Date
                End If

                If radAppointmentMonth.Checked = True Then
                    dtRow.Item("newanniversarymonth") = Month(CDate(dtRow.Item("appointeddate")))
                ElseIf radConfirmationMonth.Checked = True Then
                    dtRow.Item("newanniversarymonth") = Month(CDate(dtRow.Item("confirmation_date")))
                Else
                    dtRow.Item("newanniversarymonth") = nudDefinedMonth.Value
                End If

                dtRow.Item("userunkid") = User._Object._Userunkid
                dtRow.Item("isvoid") = False
                dtRow.Item("voiduserunkid") = -1
                dtRow.Item("voiddatetime") = DBNull.Value
                dtRow.Item("voidreason") = ""

                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                If radAppointmentMonth.Checked = True Then
                    dtRow.Item("anniversarymonthby") = enSalaryAnniversaryMonth.APPOINTMENT_MONTH
                ElseIf radConfirmationMonth.Checked = True Then
                    dtRow.Item("anniversarymonthby") = enSalaryAnniversaryMonth.CONFIRMATION_MONTH
                ElseIf radDefinedMonth.Checked = True Then
                    dtRow.Item("anniversarymonthby") = enSalaryAnniversaryMonth.DEFINED_MONTH
                End If
                'S.SANDEEP [25 OCT 2016] -- END

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApplytoChecked_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dtTable As DataTable
        Dim blnFlag As Boolean = False
        Try
            dtTable = New DataView(CType(dgvSalaryAnniversary.DataSource, DataTable), "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable

            If dtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please tick atleast one transaction."), enMsgBoxStyle.Information)
                dgvSalaryAnniversary.Focus()
                Exit Sub
            ElseIf dtTable.Select("neweffectivedate IS NULL OR newanniversarymonth IS NULL").Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please set New Effective Date and New Anniversary Month for all ticked transactions."), enMsgBoxStyle.Information)
                dgvSalaryAnniversary.Focus()
                Exit Sub
            End If

            blnFlag = objEmpSalAnniversary.InsertUpdateDelete(dtTable, ConfigParameter._Object._CurrentDateAndTime)

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    Call objbtnReset.PerformClick()
                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.Text = ""
            cboEmployee.SelectedValue = 0
            chkNeverAssignedOnly.Checked = False
            mstrAdvanceFilter = ""

            Call FillEmployeeCombo()

            Call FillList()
            Call SetDefaultSearchText() 'Sohail (22 Sep 2016)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Date Control's Events "

    Private Sub dtpAsOnDate_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpAsOnDate.Validated
        Try
            Call FillEmployeeCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAsOnDate_Validated", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " RadioButton's Events "
    Private Sub radAppointmentMonth_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAppointmentMonth.CheckedChanged, _
                                                                                                                radConfirmationMonth.CheckedChanged, _
                                                                                                                radDefinedMonth.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case radAppointmentMonth.Name.ToUpper, radConfirmationMonth.Name.ToUpper
                    nudDefinedMonth.Enabled = False
                Case radDefinedMonth.Name.ToUpper
                    nudDefinedMonth.Enabled = True
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAppointmentMonth_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Checkbox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dgvSalaryAnniversary.RowCount > 0 Then
                For Each dRow As DataGridViewRow In dgvSalaryAnniversary.Rows
                    dRow.Cells(objcolhCheckAll.Index).Value = chkSelectAll.Checked
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbMonthAssignment.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMonthAssignment.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApplytoChecked.GradientBackColor = GUI._ButttonBackColor
            Me.btnApplytoChecked.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
            Me.lblToConfirm.Text = Language._Object.getCaption(Me.lblToConfirm.Name, Me.lblToConfirm.Text)
            Me.lblFromConfirm.Text = Language._Object.getCaption(Me.lblFromConfirm.Name, Me.lblFromConfirm.Text)
            Me.lblToAppointment.Text = Language._Object.getCaption(Me.lblToAppointment.Name, Me.lblToAppointment.Text)
            Me.lblFromAppointment.Text = Language._Object.getCaption(Me.lblFromAppointment.Name, Me.lblFromAppointment.Text)
            Me.lblToMonth.Text = Language._Object.getCaption(Me.lblToMonth.Name, Me.lblToMonth.Text)
            Me.lblFromMonth.Text = Language._Object.getCaption(Me.lblFromMonth.Name, Me.lblFromMonth.Text)
            Me.gbMonthAssignment.Text = Language._Object.getCaption(Me.gbMonthAssignment.Name, Me.gbMonthAssignment.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.radAppointmentMonth.Text = Language._Object.getCaption(Me.radAppointmentMonth.Name, Me.radAppointmentMonth.Text)
            Me.radDefinedMonth.Text = Language._Object.getCaption(Me.radDefinedMonth.Name, Me.radDefinedMonth.Text)
            Me.btnApplytoChecked.Text = Language._Object.getCaption(Me.btnApplytoChecked.Name, Me.btnApplytoChecked.Text)
            Me.colhEmployeeCode.HeaderText = Language._Object.getCaption(Me.colhEmployeeCode.Name, Me.colhEmployeeCode.HeaderText)
            Me.colhEmployeeName.HeaderText = Language._Object.getCaption(Me.colhEmployeeName.Name, Me.colhEmployeeName.HeaderText)
            Me.colhAppointmentDate.HeaderText = Language._Object.getCaption(Me.colhAppointmentDate.Name, Me.colhAppointmentDate.HeaderText)
            Me.colhEffectiveDate.HeaderText = Language._Object.getCaption(Me.colhEffectiveDate.Name, Me.colhEffectiveDate.HeaderText)
            Me.colhAnniversaryMonth.HeaderText = Language._Object.getCaption(Me.colhAnniversaryMonth.Name, Me.colhAnniversaryMonth.HeaderText)
            Me.colhNewEffectiveDate.HeaderText = Language._Object.getCaption(Me.colhNewEffectiveDate.Name, Me.colhNewEffectiveDate.HeaderText)
            Me.colhNewAnniversaryMonth.HeaderText = Language._Object.getCaption(Me.colhNewAnniversaryMonth.Name, Me.colhNewAnniversaryMonth.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.chkNeverAssignedOnly.Text = Language._Object.getCaption(Me.chkNeverAssignedOnly.Name, Me.chkNeverAssignedOnly.Text)
            Me.radConfirmationMonth.Text = Language._Object.getCaption(Me.radConfirmationMonth.Name, Me.radConfirmationMonth.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Appointment To Date should be greater than Appointment From Date.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Confirmation To Date should be greater than Confirmation From Date.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Appointment To Month should be greater than Appointment From Month.")
            Language.setMessage(mstrModuleName, 4, "Please tick atleast one transaction.")
            Language.setMessage(mstrModuleName, 5, "Sorry, New Effective Date should be greater than Appointment Date for this employee")
            Language.setMessage(mstrModuleName, 6, "Sorry, New Effective Date should not be Same As Current Effective Date for this employee")
            Language.setMessage(mstrModuleName, 7, "Please enter the correct Anniversary Month.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Anniversary Month should be between 1 and 12.")
            Language.setMessage(mstrModuleName, 9, "Please set New Effective Date and New Anniversary Month for all ticked transactions.")
            Language.setMessage(mstrModuleName, 10, "Sorry, New Effective Date should not be greater than current financial year end date.")
            Language.setMessage(mstrModuleName, 11, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class