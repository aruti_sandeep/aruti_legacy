﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustom_Report_View
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCustom_Report_View))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlInformation = New System.Windows.Forms.Panel
        Me.tblSidePanel = New System.Windows.Forms.TableLayoutPanel
        Me.objlblProperty = New System.Windows.Forms.Label
        Me.lvTemplate = New System.Windows.Forms.ListView
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.btnPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.btnCopyTemplate = New System.Windows.Forms.ToolStripMenuItem
        Me.btnExportTemplate = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmOperations = New System.Windows.Forms.ToolStrip
        Me.tsmibtnNew = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiDelete = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiEdit = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiCopy = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiExport = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiImport = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.eghMain = New eZee.Common.eZeeGradientHeader
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.tblSidePanel.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.tsmOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.pnlInformation)
        Me.pnlMain.Controls.Add(Me.tblSidePanel)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 58)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(778, 453)
        Me.pnlMain.TabIndex = 3
        '
        'pnlInformation
        '
        Me.pnlInformation.BackColor = System.Drawing.Color.White
        Me.pnlInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlInformation.Location = New System.Drawing.Point(197, 0)
        Me.pnlInformation.Name = "pnlInformation"
        Me.pnlInformation.Size = New System.Drawing.Size(581, 453)
        Me.pnlInformation.TabIndex = 1
        '
        'tblSidePanel
        '
        Me.tblSidePanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.tblSidePanel.ColumnCount = 1
        Me.tblSidePanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblSidePanel.Controls.Add(Me.objlblProperty, 0, 0)
        Me.tblSidePanel.Controls.Add(Me.lvTemplate, 0, 1)
        Me.tblSidePanel.Controls.Add(Me.tsmOperations, 0, 2)
        Me.tblSidePanel.Dock = System.Windows.Forms.DockStyle.Left
        Me.tblSidePanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblSidePanel.Location = New System.Drawing.Point(0, 0)
        Me.tblSidePanel.Name = "tblSidePanel"
        Me.tblSidePanel.RowCount = 3
        Me.tblSidePanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tblSidePanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblSidePanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tblSidePanel.Size = New System.Drawing.Size(197, 453)
        Me.tblSidePanel.TabIndex = 2
        '
        'objlblProperty
        '
        Me.objlblProperty.BackColor = System.Drawing.Color.Silver
        Me.objlblProperty.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblProperty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProperty.Location = New System.Drawing.Point(3, 3)
        Me.objlblProperty.Margin = New System.Windows.Forms.Padding(0)
        Me.objlblProperty.Name = "objlblProperty"
        Me.objlblProperty.Size = New System.Drawing.Size(191, 25)
        Me.objlblProperty.TabIndex = 81
        Me.objlblProperty.Text = "Property"
        Me.objlblProperty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvTemplate
        '
        Me.lvTemplate.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvTemplate.ContextMenuStrip = Me.ContextMenuStrip1
        Me.lvTemplate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lvTemplate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTemplate.FullRowSelect = True
        Me.lvTemplate.HideSelection = False
        Me.lvTemplate.Location = New System.Drawing.Point(6, 34)
        Me.lvTemplate.Name = "lvTemplate"
        Me.lvTemplate.Size = New System.Drawing.Size(185, 378)
        Me.lvTemplate.TabIndex = 0
        Me.lvTemplate.UseCompatibleStateImageBehavior = False
        Me.lvTemplate.View = System.Windows.Forms.View.List
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnEdit, Me.btnPreview, Me.btnCopyTemplate, Me.btnExportTemplate})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(161, 92)
        '
        'btnEdit
        '
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(160, 22)
        Me.btnEdit.Text = "Edit Template"
        '
        'btnPreview
        '
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(160, 22)
        Me.btnPreview.Text = "Preview"
        '
        'btnCopyTemplate
        '
        Me.btnCopyTemplate.Name = "btnCopyTemplate"
        Me.btnCopyTemplate.Size = New System.Drawing.Size(160, 22)
        Me.btnCopyTemplate.Text = "Copy Template"
        '
        'btnExportTemplate
        '
        Me.btnExportTemplate.Name = "btnExportTemplate"
        Me.btnExportTemplate.Size = New System.Drawing.Size(160, 22)
        Me.btnExportTemplate.Text = "Export Template"
        '
        'tsmOperations
        '
        Me.tsmOperations.BackColor = System.Drawing.Color.White
        Me.tsmOperations.CanOverflow = False
        Me.tsmOperations.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tsmOperations.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsmOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmibtnNew, Me.ToolStripSeparator1, Me.tsmiDelete, Me.ToolStripSeparator2, Me.tsmiEdit, Me.ToolStripSeparator3, Me.tsmiCopy, Me.ToolStripSeparator4, Me.tsmiExport, Me.ToolStripSeparator5, Me.ToolStripButton1, Me.ToolStripSeparator7, Me.tsmiImport, Me.ToolStripSeparator6})
        Me.tsmOperations.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.tsmOperations.Location = New System.Drawing.Point(3, 418)
        Me.tsmOperations.Name = "tsmOperations"
        Me.tsmOperations.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tsmOperations.Size = New System.Drawing.Size(191, 32)
        Me.tsmOperations.TabIndex = 82
        Me.tsmOperations.Text = "ToolStrip1"
        '
        'tsmibtnNew
        '
        Me.tsmibtnNew.AutoSize = False
        Me.tsmibtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsmibtnNew.Image = Global.Aruti.Main.My.Resources.Resources.Add_Green_16
        Me.tsmibtnNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsmibtnNew.Name = "tsmibtnNew"
        Me.tsmibtnNew.Size = New System.Drawing.Size(48, 29)
        Me.tsmibtnNew.Text = "&New"
        Me.tsmibtnNew.ToolTipText = "New Template"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.AutoSize = False
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 24)
        '
        'tsmiDelete
        '
        Me.tsmiDelete.AutoSize = False
        Me.tsmiDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsmiDelete.Image = Global.Aruti.Main.My.Resources.Resources.delete_20
        Me.tsmiDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsmiDelete.Name = "tsmiDelete"
        Me.tsmiDelete.Size = New System.Drawing.Size(48, 29)
        Me.tsmiDelete.Text = "Delete"
        Me.tsmiDelete.ToolTipText = "Delete Template"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.AutoSize = False
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 24)
        '
        'tsmiEdit
        '
        Me.tsmiEdit.AutoSize = False
        Me.tsmiEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsmiEdit.Image = Global.Aruti.Main.My.Resources.Resources.Edit_16
        Me.tsmiEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsmiEdit.Name = "tsmiEdit"
        Me.tsmiEdit.Size = New System.Drawing.Size(48, 29)
        Me.tsmiEdit.Text = "&Edit"
        Me.tsmiEdit.ToolTipText = "Edit Template"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.AutoSize = False
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 24)
        Me.ToolStripSeparator3.Visible = False
        '
        'tsmiCopy
        '
        Me.tsmiCopy.AutoSize = False
        Me.tsmiCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsmiCopy.Image = Global.Aruti.Main.My.Resources.Resources.CopyRoom_16
        Me.tsmiCopy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsmiCopy.Name = "tsmiCopy"
        Me.tsmiCopy.Size = New System.Drawing.Size(48, 29)
        Me.tsmiCopy.Text = "&Copy"
        Me.tsmiCopy.ToolTipText = "Copy Template"
        Me.tsmiCopy.Visible = False
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.AutoSize = False
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 24)
        Me.ToolStripSeparator4.Visible = False
        '
        'tsmiExport
        '
        Me.tsmiExport.AutoSize = False
        Me.tsmiExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsmiExport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsmiExport.Name = "tsmiExport"
        Me.tsmiExport.Size = New System.Drawing.Size(29, 29)
        Me.tsmiExport.Text = "Export Template"
        Me.tsmiExport.Visible = False
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.AutoSize = False
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 24)
        Me.ToolStripSeparator5.Visible = False
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.AutoSize = False
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(29, 29)
        Me.ToolStripButton1.Text = "Import Template"
        Me.ToolStripButton1.Visible = False
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.AutoSize = False
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 24)
        Me.ToolStripSeparator7.Visible = False
        '
        'tsmiImport
        '
        Me.tsmiImport.AutoSize = False
        Me.tsmiImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsmiImport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsmiImport.Name = "tsmiImport"
        Me.tsmiImport.Size = New System.Drawing.Size(29, 29)
        Me.tsmiImport.Text = "Import Template"
        Me.tsmiImport.Visible = False
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.AutoSize = False
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 24)
        Me.ToolStripSeparator6.Visible = False
        '
        'eghMain
        '
        Me.eghMain.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.eghMain.BoundrySize = 15
        Me.eghMain.Color1 = System.Drawing.Color.DimGray
        Me.eghMain.Color2 = System.Drawing.Color.White
        Me.eghMain.Dock = System.Windows.Forms.DockStyle.Top
        Me.eghMain.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eghMain.Icon = Nothing
        Me.eghMain.Image = Nothing
        Me.eghMain.Location = New System.Drawing.Point(0, 0)
        Me.eghMain.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.eghMain.Message = "Welcome to Aruti Custom Report"
        Me.eghMain.MessageFontStyle = System.Drawing.FontStyle.Regular
        Me.eghMain.Name = "eghMain"
        Me.eghMain.Size = New System.Drawing.Size(778, 58)
        Me.eghMain.TabIndex = 2
        Me.eghMain.TextStartPosition = New System.Drawing.Point(15, 20)
        Me.eghMain.Title = "Aruti Custom Report"
        Me.eghMain.TitleFontStyle = System.Drawing.FontStyle.Bold
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "ManageFolios_16.png")
        Me.ImageList1.Images.SetKeyName(1, "New_16.png")
        '
        'frmCustom_Report_View
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 511)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.eghMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmCustom_Report_View"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlMain.ResumeLayout(False)
        Me.tblSidePanel.ResumeLayout(False)
        Me.tblSidePanel.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.tsmOperations.ResumeLayout(False)
        Me.tsmOperations.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents pnlInformation As System.Windows.Forms.Panel
    Friend WithEvents tblSidePanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblProperty As System.Windows.Forms.Label
    Friend WithEvents lvTemplate As System.Windows.Forms.ListView
    Friend WithEvents tsmOperations As System.Windows.Forms.ToolStrip
    Friend WithEvents tsmibtnNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiCopy As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiExport As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiImport As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents eghMain As eZee.Common.eZeeGradientHeader
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnCopyTemplate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExportTemplate As System.Windows.Forms.ToolStripMenuItem
End Class
