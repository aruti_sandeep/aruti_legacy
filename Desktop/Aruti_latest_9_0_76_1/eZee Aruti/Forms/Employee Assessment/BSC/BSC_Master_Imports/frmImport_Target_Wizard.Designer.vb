﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImport_Target_Wizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImport_Target_Wizard))
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.TargetWizard = New eZee.Common.eZeeWizard
        Me.wizPageFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblValue = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.wizPageData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhKPICode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.wizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboTargetName = New System.Windows.Forms.ComboBox
        Me.lblTargetName = New System.Windows.Forms.Label
        Me.objlblSign8 = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboWeight = New System.Windows.Forms.ComboBox
        Me.objlblSign7 = New System.Windows.Forms.Label
        Me.lblWeight = New System.Windows.Forms.Label
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.cboTargetCode = New System.Windows.Forms.ComboBox
        Me.lblTargetCode = New System.Windows.Forms.Label
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.cboObjetiveCode = New System.Windows.Forms.ComboBox
        Me.lblObjectiveCode = New System.Windows.Forms.Label
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.cboKPICode = New System.Windows.Forms.ComboBox
        Me.lblKPI_Code = New System.Windows.Forms.Label
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.EZeeLightButton1 = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeLightButton2 = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeLightButton3 = New eZee.Common.eZeeLightButton(Me.components)
        Me.cmsFilter.SuspendLayout()
        Me.TargetWizard.SuspendLayout()
        Me.wizPageFile.SuspendLayout()
        Me.wizPageData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.wizPageMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'TargetWizard
        '
        Me.TargetWizard.Controls.Add(Me.wizPageMapping)
        Me.TargetWizard.Controls.Add(Me.wizPageFile)
        Me.TargetWizard.Controls.Add(Me.wizPageData)
        Me.TargetWizard.Controls.Add(Me.objbuttonBack)
        Me.TargetWizard.Controls.Add(Me.objbuttonCancel)
        Me.TargetWizard.Controls.Add(Me.objbuttonNext)
        Me.TargetWizard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TargetWizard.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.TargetWizard.Location = New System.Drawing.Point(0, 0)
        Me.TargetWizard.Name = "TargetWizard"
        Me.TargetWizard.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizPageFile, Me.wizPageMapping, Me.wizPageData})
        Me.TargetWizard.SaveEnabled = True
        Me.TargetWizard.SaveText = "Save && Finish"
        Me.TargetWizard.SaveVisible = False
        Me.TargetWizard.SetSaveIndexBeforeFinishIndex = False
        Me.TargetWizard.Size = New System.Drawing.Size(583, 369)
        Me.TargetWizard.TabIndex = 6
        Me.TargetWizard.WelcomeImage = Nothing
        '
        'wizPageFile
        '
        Me.wizPageFile.Controls.Add(Me.lblValue)
        Me.wizPageFile.Controls.Add(Me.btnOpenFile)
        Me.wizPageFile.Controls.Add(Me.txtFilePath)
        Me.wizPageFile.Controls.Add(Me.lblSelectfile)
        Me.wizPageFile.Controls.Add(Me.lblTitle)
        Me.wizPageFile.Location = New System.Drawing.Point(0, 0)
        Me.wizPageFile.Name = "wizPageFile"
        Me.wizPageFile.Size = New System.Drawing.Size(583, 321)
        Me.wizPageFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageFile.TabIndex = 7
        '
        'lblValue
        '
        Me.lblValue.BackColor = System.Drawing.Color.Transparent
        Me.lblValue.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValue.ForeColor = System.Drawing.Color.Maroon
        Me.lblValue.Location = New System.Drawing.Point(172, 251)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(399, 55)
        Me.lblValue.TabIndex = 24
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(547, 164)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(24, 20)
        Me.btnOpenFile.TabIndex = 21
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(176, 164)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(365, 21)
        Me.txtFilePath.TabIndex = 20
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 141)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(143, 17)
        Me.lblSelectfile.TabIndex = 19
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(172, 21)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(399, 23)
        Me.lblTitle.TabIndex = 18
        Me.lblTitle.Text = "Employee Targets Import Wizard"
        '
        'wizPageData
        '
        Me.wizPageData.Controls.Add(Me.dgData)
        Me.wizPageData.Controls.Add(Me.btnFilter)
        Me.wizPageData.Controls.Add(Me.pnlInfo)
        Me.wizPageData.Location = New System.Drawing.Point(0, 0)
        Me.wizPageData.Name = "wizPageData"
        Me.wizPageData.Size = New System.Drawing.Size(583, 321)
        Me.wizPageData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizPageData.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.dgcolhCode, Me.dgcolhKPICode, Me.dgcolhName, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(559, 212)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        Me.colhEmployee.Width = 90
        '
        'dgcolhCode
        '
        Me.dgcolhCode.HeaderText = "Obj. Code"
        Me.dgcolhCode.Name = "dgcolhCode"
        Me.dgcolhCode.ReadOnly = True
        Me.dgcolhCode.Width = 80
        '
        'dgcolhKPICode
        '
        Me.dgcolhKPICode.HeaderText = "KPI Code"
        Me.dgcolhKPICode.Name = "dgcolhKPICode"
        Me.dgcolhKPICode.ReadOnly = True
        Me.dgcolhKPICode.Width = 70
        '
        'dgcolhName
        '
        Me.dgcolhName.HeaderText = "Target"
        Me.dgcolhName.Name = "dgcolhName"
        Me.dgcolhName.ReadOnly = True
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.Width = 80
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 287)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(91, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(559, 51)
        Me.pnlInfo.TabIndex = 3
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(437, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(323, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(437, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(369, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(482, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(323, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(482, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(369, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(331, 333)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(499, 333)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(415, 333)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'wizPageMapping
        '
        Me.wizPageMapping.Controls.Add(Me.gbFiledMapping)
        Me.wizPageMapping.Controls.Add(Me.EZeeLightButton1)
        Me.wizPageMapping.Controls.Add(Me.EZeeLightButton2)
        Me.wizPageMapping.Controls.Add(Me.EZeeLightButton3)
        Me.wizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.wizPageMapping.Name = "wizPageMapping"
        Me.wizPageMapping.Size = New System.Drawing.Size(583, 321)
        Me.wizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageMapping.TabIndex = 8
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.cboTargetName)
        Me.gbFiledMapping.Controls.Add(Me.lblTargetName)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign8)
        Me.gbFiledMapping.Controls.Add(Me.cboPeriod)
        Me.gbFiledMapping.Controls.Add(Me.lblPeriod)
        Me.gbFiledMapping.Controls.Add(Me.cboWeight)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign7)
        Me.gbFiledMapping.Controls.Add(Me.lblWeight)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign6)
        Me.gbFiledMapping.Controls.Add(Me.cboTargetCode)
        Me.gbFiledMapping.Controls.Add(Me.lblTargetCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign4)
        Me.gbFiledMapping.Controls.Add(Me.cboObjetiveCode)
        Me.gbFiledMapping.Controls.Add(Me.lblObjectiveCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign3)
        Me.gbFiledMapping.Controls.Add(Me.cboKPICode)
        Me.gbFiledMapping.Controls.Add(Me.lblKPI_Code)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign2)
        Me.gbFiledMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(419, 321)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTargetName
        '
        Me.cboTargetName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTargetName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTargetName.FormattingEnabled = True
        Me.cboTargetName.Location = New System.Drawing.Point(185, 185)
        Me.cboTargetName.Name = "cboTargetName"
        Me.cboTargetName.Size = New System.Drawing.Size(186, 21)
        Me.cboTargetName.TabIndex = 22
        '
        'lblTargetName
        '
        Me.lblTargetName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTargetName.Location = New System.Drawing.Point(82, 187)
        Me.lblTargetName.Name = "lblTargetName"
        Me.lblTargetName.Size = New System.Drawing.Size(97, 17)
        Me.lblTargetName.TabIndex = 21
        Me.lblTargetName.Text = "Target Name"
        Me.lblTargetName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign8
        '
        Me.objlblSign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign8.ForeColor = System.Drawing.Color.Red
        Me.objlblSign8.Location = New System.Drawing.Point(59, 187)
        Me.objlblSign8.Name = "objlblSign8"
        Me.objlblSign8.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign8.TabIndex = 20
        Me.objlblSign8.Text = "*"
        Me.objlblSign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(185, 212)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(186, 21)
        Me.cboPeriod.TabIndex = 15
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(82, 214)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(97, 17)
        Me.lblPeriod.TabIndex = 14
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWeight
        '
        Me.cboWeight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWeight.FormattingEnabled = True
        Me.cboWeight.Location = New System.Drawing.Point(185, 239)
        Me.cboWeight.Name = "cboWeight"
        Me.cboWeight.Size = New System.Drawing.Size(186, 21)
        Me.cboWeight.TabIndex = 18
        '
        'objlblSign7
        '
        Me.objlblSign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign7.ForeColor = System.Drawing.Color.Red
        Me.objlblSign7.Location = New System.Drawing.Point(59, 214)
        Me.objlblSign7.Name = "objlblSign7"
        Me.objlblSign7.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign7.TabIndex = 13
        Me.objlblSign7.Text = "*"
        Me.objlblSign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(82, 241)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(97, 17)
        Me.lblWeight.TabIndex = 17
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(59, 241)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign6.TabIndex = 16
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboTargetCode
        '
        Me.cboTargetCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTargetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTargetCode.FormattingEnabled = True
        Me.cboTargetCode.Location = New System.Drawing.Point(185, 158)
        Me.cboTargetCode.Name = "cboTargetCode"
        Me.cboTargetCode.Size = New System.Drawing.Size(186, 21)
        Me.cboTargetCode.TabIndex = 12
        '
        'lblTargetCode
        '
        Me.lblTargetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTargetCode.Location = New System.Drawing.Point(82, 160)
        Me.lblTargetCode.Name = "lblTargetCode"
        Me.lblTargetCode.Size = New System.Drawing.Size(97, 17)
        Me.lblTargetCode.TabIndex = 11
        Me.lblTargetCode.Text = "Target Code"
        Me.lblTargetCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(59, 160)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign4.TabIndex = 10
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboObjetiveCode
        '
        Me.cboObjetiveCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObjetiveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObjetiveCode.FormattingEnabled = True
        Me.cboObjetiveCode.Location = New System.Drawing.Point(185, 104)
        Me.cboObjetiveCode.Name = "cboObjetiveCode"
        Me.cboObjetiveCode.Size = New System.Drawing.Size(186, 21)
        Me.cboObjetiveCode.TabIndex = 6
        '
        'lblObjectiveCode
        '
        Me.lblObjectiveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObjectiveCode.Location = New System.Drawing.Point(82, 106)
        Me.lblObjectiveCode.Name = "lblObjectiveCode"
        Me.lblObjectiveCode.Size = New System.Drawing.Size(97, 17)
        Me.lblObjectiveCode.TabIndex = 5
        Me.lblObjectiveCode.Text = "Objective Code"
        Me.lblObjectiveCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(59, 106)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign3.TabIndex = 4
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboKPICode
        '
        Me.cboKPICode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKPICode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboKPICode.FormattingEnabled = True
        Me.cboKPICode.Location = New System.Drawing.Point(185, 131)
        Me.cboKPICode.Name = "cboKPICode"
        Me.cboKPICode.Size = New System.Drawing.Size(186, 21)
        Me.cboKPICode.TabIndex = 9
        '
        'lblKPI_Code
        '
        Me.lblKPI_Code.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKPI_Code.Location = New System.Drawing.Point(82, 133)
        Me.lblKPI_Code.Name = "lblKPI_Code"
        Me.lblKPI_Code.Size = New System.Drawing.Size(97, 17)
        Me.lblKPI_Code.TabIndex = 8
        Me.lblKPI_Code.Text = "KPI Code"
        Me.lblKPI_Code.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(59, 133)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign2.TabIndex = 7
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(185, 77)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(186, 21)
        Me.cboEmployeeCode.TabIndex = 3
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(82, 79)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(97, 17)
        Me.lblEmployeeCode.TabIndex = 2
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(59, 79)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign1.TabIndex = 1
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(182, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(234, 19)
        Me.lblCaption.TabIndex = 0
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'EZeeLightButton1
        '
        Me.EZeeLightButton1.BackColor = System.Drawing.Color.White
        Me.EZeeLightButton1.BackgroundImage = CType(resources.GetObject("EZeeLightButton1.BackgroundImage"), System.Drawing.Image)
        Me.EZeeLightButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.EZeeLightButton1.BorderColor = System.Drawing.Color.Empty
        Me.EZeeLightButton1.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.EZeeLightButton1.Enabled = False
        Me.EZeeLightButton1.FlatAppearance.BorderSize = 0
        Me.EZeeLightButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EZeeLightButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLightButton1.ForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.EZeeLightButton1.GradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton1.HoverGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.Location = New System.Drawing.Point(331, 333)
        Me.EZeeLightButton1.Name = "EZeeLightButton1"
        Me.EZeeLightButton1.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton1.PressedGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.Size = New System.Drawing.Size(77, 29)
        Me.EZeeLightButton1.TabIndex = 6
        Me.EZeeLightButton1.Text = "Back"
        Me.EZeeLightButton1.UseVisualStyleBackColor = False
        '
        'EZeeLightButton2
        '
        Me.EZeeLightButton2.BackColor = System.Drawing.Color.White
        Me.EZeeLightButton2.BackgroundImage = CType(resources.GetObject("EZeeLightButton2.BackgroundImage"), System.Drawing.Image)
        Me.EZeeLightButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.EZeeLightButton2.BorderColor = System.Drawing.Color.Empty
        Me.EZeeLightButton2.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.EZeeLightButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.EZeeLightButton2.FlatAppearance.BorderSize = 0
        Me.EZeeLightButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EZeeLightButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLightButton2.ForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.EZeeLightButton2.GradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton2.HoverGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.Location = New System.Drawing.Point(499, 333)
        Me.EZeeLightButton2.Name = "EZeeLightButton2"
        Me.EZeeLightButton2.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton2.PressedGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.Size = New System.Drawing.Size(77, 29)
        Me.EZeeLightButton2.TabIndex = 5
        Me.EZeeLightButton2.Text = "Cancel"
        Me.EZeeLightButton2.UseVisualStyleBackColor = False
        '
        'EZeeLightButton3
        '
        Me.EZeeLightButton3.BackColor = System.Drawing.Color.White
        Me.EZeeLightButton3.BackgroundImage = CType(resources.GetObject("EZeeLightButton3.BackgroundImage"), System.Drawing.Image)
        Me.EZeeLightButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.EZeeLightButton3.BorderColor = System.Drawing.Color.Empty
        Me.EZeeLightButton3.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.EZeeLightButton3.FlatAppearance.BorderSize = 0
        Me.EZeeLightButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EZeeLightButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLightButton3.ForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.EZeeLightButton3.GradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton3.HoverGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.Location = New System.Drawing.Point(415, 333)
        Me.EZeeLightButton3.Name = "EZeeLightButton3"
        Me.EZeeLightButton3.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton3.PressedGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.Size = New System.Drawing.Size(77, 29)
        Me.EZeeLightButton3.TabIndex = 4
        Me.EZeeLightButton3.Text = "Next"
        Me.EZeeLightButton3.UseVisualStyleBackColor = False
        '
        'frmImport_Target_Wizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(583, 369)
        Me.Controls.Add(Me.TargetWizard)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImport_Target_Wizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Target Wizard"
        Me.cmsFilter.ResumeLayout(False)
        Me.TargetWizard.ResumeLayout(False)
        Me.wizPageFile.ResumeLayout(False)
        Me.wizPageFile.PerformLayout()
        Me.wizPageData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.wizPageMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TargetWizard As eZee.Common.eZeeWizard
    Friend WithEvents wizPageFile As eZee.Common.eZeeWizardPage
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents wizPageData As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents wizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboWeight As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign7 As System.Windows.Forms.Label
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents cboTargetCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblTargetCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents cboObjetiveCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblObjectiveCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents cboKPICode As System.Windows.Forms.ComboBox
    Friend WithEvents lblKPI_Code As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents EZeeLightButton1 As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeLightButton2 As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeLightButton3 As eZee.Common.eZeeLightButton
    Friend WithEvents cboTargetName As System.Windows.Forms.ComboBox
    Friend WithEvents lblTargetName As System.Windows.Forms.Label
    Friend WithEvents objlblSign8 As System.Windows.Forms.Label
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhKPICode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents lblValue As System.Windows.Forms.Label
End Class
