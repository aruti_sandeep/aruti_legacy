﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalVoidOwnerGoals

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmGlobalVoidOwnerGoals"
    Private mdtFinal As DataTable = Nothing
    Private cmnuDelete As ContextMenuStrip
    Private objFMaster As New clsAssess_Field_Master(True)
    Private iOwnerRefId As Integer = 0
    Private mintLinkedFieldId As Integer = 0

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Try
            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objMap = Nothing

            If User._Object.Privilege._AllowtoViewAllocationGoalsList = False Then Exit Sub

            Dim objOwrField1 = New clsassess_owrfield1_master
            dtFinal = objOwrField1.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            objOwrField1 = Nothing

            mdtFinal = New DataView(dtFinal, "", "Field1Id DESC, Field1 ASC", DataViewRowState.CurrentRows).ToTable

            mdtFinal.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            mdtFinal.Columns("ischeck").SetOrdinal(0)


            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split("|")
            End If

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvData.Columns.Contains(iColName) = True Then Continue For

                If dCol.ColumnName = "ischeck" Then
                    Dim dgvCol As New DataGridViewCheckBoxColumn()
                    dgvCol.Name = iColName
                    dgvCol.Width = 25
                    dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dgvCol.Frozen = True
                    dgvCol.HeaderText = ""
                    dgvCol.DataPropertyName = dCol.ColumnName
                    dgvCol.Resizable = DataGridViewTriState.False
                    dgvData.Columns.Add(dgvCol)
                Else
                    Dim dgvCol As New DataGridViewTextBoxColumn()
                    dgvCol.Name = iColName
                    dgvCol.Width = 110
                    dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dgvCol.ReadOnly = True
                    dgvCol.DataPropertyName = dCol.ColumnName
                    dgvCol.HeaderText = dCol.Caption
                    If dCol.Caption.Length <= 0 Then
                        dgvCol.Visible = False
                    Else
                        If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                            dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                            If iPlan IsNot Nothing Then
                                If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                    dgvCol.Visible = False
                                End If
                            End If
                        End If
                    End If

                    'S.SANDEEP [01 JUL 2015] -- START
                    If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                        dgvCol.Visible = False
                    End If
                    'S.SANDEEP [01 JUL 2015] -- END

                    dgvData.Columns.Add(dgvCol)
                End If
            Next
            dgvData.DataSource = mdtFinal

            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.FINAL_COMMITTED Then
                    btnVoid.Enabled = False
                    objChkAll.Enabled = False
                    dgvRow.Cells("objischeck").ReadOnly = True
                Else
                    btnVoid.Enabled = True
                    objChkAll.Enabled = True
                    dgvRow.Cells("objischeck").ReadOnly = False
                End If
            Next

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue), 0)
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 5, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_MenuItems()
        Dim dsList As New DataSet
        Try
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            If dsList.Tables(0).Rows.Count > 0 Then
                cmnuDelete.Items.Clear()
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim objbtnDel As New ToolStripMenuItem
                    objbtnDel.Name = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnDel.Text = Language.getMessage(mstrModuleName, 2, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnDel.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuDelete.Items.Add(objbtnDel)
                    AddHandler objbtnDel.Click, AddressOf objbtnDel_Click
                Next
            End If
            btnVoid.SplitButtonMenu = cmnuDelete
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub VoidCheckedData(ByVal intFieldTypeId As Integer, ByVal strVoidReasion As String, ByVal drow() As DataRow)
        Try
            For iRdx As Integer = 0 To drow.Length - 1
                Select Case intFieldTypeId
                    Case 1  'OWNER FIELD 1 UNKID

                        Dim objOwrField1 As New clsassess_owrfield1_master
                        If CInt(drow(iRdx).Item("owrfield1unkid")) > 0 Then
                            objOwrField1._Isvoid = True
                            objOwrField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField1._Voidreason = strVoidReasion
                            objOwrField1._Voiduserunkid = User._Object._Userunkid
                            objOwrField1.Delete(CInt(drow(iRdx).Item("owrfield1unkid")))
                        End If
                        objOwrField1 = Nothing

                    Case 2  'OWNER FIELD 2 UNKID

                        Dim objOwrField2 As New clsassess_owrfield2_master
                        If CInt(drow(iRdx).Item("owrfield2unkid")) > 0 Then
                            objOwrField2._Isvoid = True
                            objOwrField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField2._Voidreason = strVoidReasion
                            objOwrField2._Voiduserunkid = User._Object._Userunkid
                            objOwrField2.Delete(CInt(drow(iRdx).Item("owrfield2unkid")))
                        End If
                        objOwrField2 = Nothing

                    Case 3  'OWNER FIELD 3 UNKID

                        Dim objOwrField3 As New clsassess_owrfield3_master
                        If CInt(drow(iRdx).Item("owrfield3unkid")) > 0 Then
                            objOwrField3._Isvoid = True
                            objOwrField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField3._Voidreason = strVoidReasion
                            objOwrField3._Voiduserunkid = User._Object._Userunkid
                            objOwrField3.Delete(CInt(drow(iRdx).Item("owrfield3unkid")))
                        End If
                        objOwrField3 = Nothing

                    Case 4  'OWNER FIELD 4 UNKID

                        Dim objOwrField4 As New clsassess_owrfield4_master
                        If CInt(drow(iRdx).Item("owrfield4unkid")) > 0 Then
                            objOwrField4._Isvoid = True
                            objOwrField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField4._Voidreason = strVoidReasion
                            objOwrField4._Voiduserunkid = User._Object._Userunkid
                            objOwrField4.Delete(CInt(drow(iRdx).Item("owrfield4unkid")))
                        End If
                        objOwrField4 = Nothing

                    Case 5  'OWNER FIELD 5 UNKID

                        Dim objOwrField5 As New clsassess_owrfield5_master
                        If CInt(drow(iRdx).Item("owrfield5unkid")) > 0 Then
                            objOwrField5._Isvoid = True
                            objOwrField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField5._Voidreason = strVoidReasion
                            objOwrField5._Voiduserunkid = User._Object._Userunkid
                            objOwrField5.Delete(CInt(drow(iRdx).Item("owrfield5unkid")))
                        End If
                        objOwrField5 = Nothing
                End Select
            Next
            Call Fill_Grid()
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            objChkAll.CheckState = CheckState.Unchecked
            If dgvData.RowCount <= 0 Then
                objChkAll.Enabled = False
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VoidCheckedData", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmGlobalVoidOwnerGoals_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmnuDelete = New ContextMenuStrip()
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call Generate_MenuItems()
            Call objbtnReset_Click(New Object, New EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalVoidOwnerGoals_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_owrfield1_master.SetMessages()
            clsassess_owrfield2_master.SetMessages()
            clsassess_owrfield3_master.SetMessages()
            clsassess_owrfield4_master.SetMessages()
            clsassess_owrfield5_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_owrfield1_master,clsassess_owrfield2_master,clsassess_owrfield3_master,clsassess_owrfield4_master,clsassess_owrfield5_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboOwnerType.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call Fill_Grid()
            If dgvData.RowCount <= 0 Then
                objChkAll.Enabled = False
            End If
            objbtnSearch.ShowResult(dgvData.RowCount)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboOwner.SelectedValue = 0
            dgvData.DataSource = Nothing
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            objChkAll.CheckState = CheckState.Unchecked
            objChkAll.Enabled = False
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            objbtnReset.ShowResult(dgvData.RowCount)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchField1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOwner.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboOwner.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboOwner.ValueMember
                .DisplayMember = cboOwner.DisplayMember
                .DataSource = cboOwner.DataSource
                If .DisplayDialog = True Then
                    cboOwner.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub objbtnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmReasonSelection
        Try
            Dim dtmp() As DataRow = mdtFinal.Select("ischeck=true")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please tick atleast one owner goal to delete it."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim iVoidReason As String = String.Empty

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iMsg As String = Language.getMessage(mstrModuleName, 7, "You are about to remove the information from owner level, this will delete all child linked to this parent") & vbCrLf & _
                                 Language.getMessage(mstrModuleName, 8, "Are you sure you want to delete?")

            If eZeeMsgBox.Show(iMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
            If iVoidReason.Length <= 0 Then
                Exit Sub
            End If

            Call VoidCheckedData(CInt(sender.tag.ToString.Split("|")(1)), iVoidReason, dtmp)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " CheckBox Events "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            For Each row As DataRow In mdtFinal.Rows
                row.Item("ischeck") = CBool(objChkAll.CheckState)
                row.AcceptChanges()
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim iExecOrder As Integer = 0
                Dim objMapping As New clsAssess_Field_Mapping
                mintLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
                objMapping = Nothing
                Dim dsFields As New DataSet
                dsFields = objFMaster.Get_Field_Mapping("List", , False)
                If dsFields.Tables(0).Rows.Count > 0 Then
                    Dim dtmp() As DataRow = dsFields.Tables(0).Select("fieldunkid = '" & mintLinkedFieldId & "'")
                    If dtmp.Length > 0 Then
                        Select Case dtmp(0).Item("ExOrder")
                            Case 1
                                Dim objCoyField1 As New clsassess_coyfield1_master
                                iOwnerRefId = objCoyField1.GetOwnerRefId
                                objCoyField1 = Nothing
                            Case 2
                                Dim objCoyField2 As New clsassess_coyfield2_master
                                iOwnerRefId = objCoyField2.GetOwnerRefId
                                objCoyField2 = Nothing
                            Case 3
                                Dim objCoyField3 As New clsassess_coyfield3_master
                                iOwnerRefId = objCoyField3.GetOwnerRefId
                                objCoyField3 = Nothing
                            Case 4
                                Dim objCoyField4 As New clsassess_coyfield4_master
                                iOwnerRefId = objCoyField4.GetOwnerRefId
                                objCoyField4 = Nothing
                            Case 5
                                Dim objCoyField5 As New clsassess_coyfield5_master
                                iOwnerRefId = objCoyField5.GetOwnerRefId
                                objCoyField5 = Nothing
                        End Select
                    End If
                    Dim dsList As New DataSet : Dim objMData As New clsMasterData
                    dsList = objMData.GetEAllocation_Notification("List")
                    Dim dtTable As DataTable = Nothing
                    If iOwnerRefId > 0 Then
                        dtTable = New DataView(dsList.Tables(0), "Id IN(0," & iOwnerRefId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
                    End If
                    With cboOwnerType
                        .ValueMember = "Id"
                        .DisplayMember = "Name"
                        .DataSource = dtTable
                        .SelectedValue = IIf(iOwnerRefId <= 0, enAllocation.BRANCH, iOwnerRefId)
                    End With
                    objMData = Nothing
                End If
            Else
                cboOwnerType.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboOwnerType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOwnerType.SelectedIndexChanged
        Try
            Dim dList As New DataSet
            cboOwner.DataSource = Nothing
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "stationunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0).Copy
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "deptgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "departmentunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "sectiongroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "sectionunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "unitgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "unitunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "teamunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "jobgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "jobunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "classgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "classesunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOwnerType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
            Me.lblCaption2.Text = Language._Object.getCaption(Me.lblCaption2.Name, Me.lblCaption2.Text)
            Me.lblOwnerType.Text = Language._Object.getCaption(Me.lblOwnerType.Name, Me.lblOwnerType.Text)
            Me.lblOwner.Text = Language._Object.getCaption(Me.lblOwner.Name, Me.lblOwner.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please tick atleast one owner goal to delete it.")
            Language.setMessage(mstrModuleName, 2, "Delete")
            Language.setMessage(mstrModuleName, 3, "Sorry, No data defined for asseessment caption settings screen.")
            Language.setMessage(mstrModuleName, 4, "Sorry no field is mapped with the selected period.")
            Language.setMessage(mstrModuleName, 5, "Total Weight Assigned :")
            Language.setMessage(mstrModuleName, 6, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue.")
            Language.setMessage(mstrModuleName, 7, "You are about to remove the information from owner level, this will delete all child linked to this parent")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to delete?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmGlobalVoidEmpGoals"
'    Private mblnIsCancel As Boolean = True
'    Private mdtFinal As DataTable = Nothing
'    Private objFMaster As New clsAssess_Field_Master(True)
'    Private cmnuDelete As ContextMenuStrip

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Dim dsList As New DataSet
'        Dim objPeriod As New clscommom_period_Tran
'        Dim objEmp As New clsEmployee_Master
'        Try

'            dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsList.Tables(0)
'                .SelectedValue = 0
'            End With

'            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
'            With cboPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables(0)
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_Grid()
'        Dim dtFinal As DataTable
'        Dim iSearch As String = String.Empty
'        Try

'            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim objMap As New clsAssess_Field_Mapping
'            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If
'            objMap = Nothing

'            If User._Object.Privilege._AllowtoViewEmployeeGoalsList = False Then Exit Sub
'            Dim objEmpField1 = New clsassess_empfield1_master
'            dtFinal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
'            objEmpField1 = Nothing
'            mdtFinal = New DataView(dtFinal, "", "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable

'            mdtFinal.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
'            mdtFinal.Columns("ischeck").SetOrdinal(0)

'            dgvData.AutoGenerateColumns = False
'            Dim iColName As String = String.Empty

'            Dim iPlan() As String = Nothing
'            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
'                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split(CChar("|"))
'            End If

'            For Each dCol As DataColumn In mdtFinal.Columns
'                iColName = "" : iColName = "obj" & dCol.ColumnName
'                If dgvData.Columns.Contains(iColName) = True Then Continue For
'                If dCol.ColumnName = "ischeck" Then
'                    Dim dgvCol As New DataGridViewCheckBoxColumn()
'                    dgvCol.Name = iColName
'                    dgvCol.Width = 25
'                    dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
'                    dgvCol.Frozen = True
'                    dgvCol.HeaderText = ""
'                    dgvCol.DataPropertyName = dCol.ColumnName
'                    dgvCol.Resizable = DataGridViewTriState.False
'                    dgvData.Columns.Add(dgvCol)
'                Else
'                    Dim dgvCol As New DataGridViewTextBoxColumn()
'                    dgvCol.Name = iColName
'                    dgvCol.Width = 120
'                    dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
'                    dgvCol.ReadOnly = True
'                    dgvCol.DataPropertyName = dCol.ColumnName
'                    dgvCol.HeaderText = dCol.Caption
'                    If dCol.Caption.Length <= 0 Then
'                        dgvCol.Visible = False
'                    Else
'                        If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
'                            dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, CInt(mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
'                            If iPlan IsNot Nothing Then
'                                If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
'                                    dgvCol.Visible = False
'                                End If
'                            End If
'                        End If
'                    End If
'                    If dCol.ColumnName = "Emp" Then
'                        dgvCol.Visible = False
'                    End If
'                    If dCol.ColumnName = "OPeriod" Then
'                        dgvCol.Visible = False
'                    End If
'                    If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
'                        If dCol.ColumnName.StartsWith("Owr") Then
'                            dgvCol.Visible = False
'                        End If
'                    End If
'                    dgvData.Columns.Add(dgvCol)
'                End If
'            Next
'            dgvData.DataSource = mdtFinal

'            For Each dgvRow As DataGridViewRow In dgvData.Rows
'                If CBool(dgvRow.Cells("objisfinal").Value) = True Then
'                    btnVoid.Enabled = False
'                    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
'                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
'                    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Blue
'                    objChkAll.Enabled = False
'                    dgvRow.Cells("objischeck").ReadOnly = True
'                ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
'                    btnVoid.Enabled = False
'                    dgvRow.DefaultCellStyle.ForeColor = Color.Green
'                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
'                    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Green
'                    objChkAll.Enabled = False
'                    dgvRow.Cells("objischeck").ReadOnly = True
'                Else
'                    btnVoid.Enabled = True
'                    dgvRow.DefaultCellStyle.ForeColor = Color.Black
'                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
'                    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Black
'                    objChkAll.Enabled = True
'                    dgvRow.Cells("objischeck").ReadOnly = False
'                End If
'            Next

'            Dim objFMapping As New clsAssess_Field_Mapping
'            Dim xTotalWeight As Double = 0
'            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
'            If xTotalWeight > 0 Then
'                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 3, "Total Weight Assigned :") & " " & xTotalWeight.ToString
'            Else
'                objlblTotalWeight.Text = ""
'            End If
'            objFMapping = Nothing
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Generate_MenuItems()
'        Dim dsList As New DataSet
'        Try
'            dsList = objFMaster.Get_Field_Mapping("List", , True)
'            If dsList.Tables(0).Rows.Count > 0 Then
'                cmnuDelete.Items.Clear()
'                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
'                    Dim objbtnDel As New ToolStripMenuItem
'                    objbtnDel.Name = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
'                    objbtnDel.Text = Language.getMessage(mstrModuleName, 6, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
'                    objbtnDel.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
'                    cmnuDelete.Items.Add(objbtnDel)
'                    AddHandler objbtnDel.Click, AddressOf objbtnDel_Click
'                Next
'            End If
'            btnVoid.SplitButtonMenu = cmnuDelete
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub VoidCheckedData(ByVal intFieldTypeId As Integer, ByVal strVoidReasion As String, ByVal drow() As DataRow)
'        Try
'            For iRdx As Integer = 0 To drow.Length - 1
'                Select Case intFieldTypeId
'                    Case 1  'EMP FIELD 1 UNKID

'                        Dim objEmpField1 As New clsassess_empfield1_master
'                        If CInt(drow(iRdx).Item("empfield1unkid")) > 0 Then
'                            objEmpField1._Isvoid = True
'                            objEmpField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                            objEmpField1._Voidreason = strVoidReasion
'                            objEmpField1._Voiduserunkid = User._Object._Userunkid
'                            objEmpField1.Delete(CInt(drow(iRdx).Item("empfield1unkid")))
'                        End If
'                        objEmpField1 = Nothing

'                    Case 2  'EMP FIELD 2 UNKID

'                        Dim objEmpField2 As New clsassess_empfield2_master
'                        If CInt(drow(iRdx).Item("empfield2unkid")) > 0 Then
'                            objEmpField2._Isvoid = True
'                            objEmpField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                            objEmpField2._Voidreason = strVoidReasion
'                            objEmpField2._Voiduserunkid = User._Object._Userunkid
'                            objEmpField2.Delete(CInt(drow(iRdx).Item("empfield2unkid")))
'                        End If
'                        objEmpField2 = Nothing

'                    Case 3  'EMP FIELD 3 UNKID

'                        Dim objEmpField3 As New clsassess_empfield3_master
'                        If CInt(drow(iRdx).Item("empfield3unkid")) > 0 Then
'                            objEmpField3._Isvoid = True
'                            objEmpField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                            objEmpField3._Voidreason = strVoidReasion
'                            objEmpField3._Voiduserunkid = User._Object._Userunkid
'                            objEmpField3.Delete(CInt(drow(iRdx).Item("empfield3unkid")))
'                        End If
'                        objEmpField3 = Nothing

'                    Case 4  'EMP FIELD 4 UNKID

'                        Dim objEmpField4 As New clsassess_empfield4_master
'                        If CInt(drow(iRdx).Item("empfield4unkid")) > 0 Then
'                            objEmpField4._Isvoid = True
'                            objEmpField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                            objEmpField4._Voidreason = strVoidReasion
'                            objEmpField4._Voiduserunkid = User._Object._Userunkid
'                            objEmpField4.Delete(CInt(drow(iRdx).Item("empfield4unkid")))
'                        End If
'                        objEmpField4 = Nothing

'                    Case 5  'EMP FIELD 5 UNKID

'                        Dim objEmpField5 As New clsassess_empfield5_master
'                        If CInt(drow(iRdx).Item("empfield5unkid")) > 0 Then
'                            objEmpField5._Isvoid = True
'                            objEmpField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                            objEmpField5._Voidreason = strVoidReasion
'                            objEmpField5._Voiduserunkid = User._Object._Userunkid
'                            objEmpField5.Delete(CInt(drow(iRdx).Item("empfield5unkid")))
'                        End If
'                        objEmpField5 = Nothing

'                End Select
'            Next
'            Call Fill_Grid()
'            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
'            objChkAll.CheckState = CheckState.Unchecked
'            If dgvData.RowCount <= 0 Then
'                objChkAll.Enabled = False
'            End If
'            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "VoidCheckedData", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Form's Event "

'    Private Sub frmGlobalVoidEmpGoals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        cmnuDelete = New ContextMenuStrip()
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            Call FillCombo()
'            Call Generate_MenuItems()
'            Call objbtnReset_Click(New Object, New EventArgs)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmGlobalVoidEmpGoals_Load", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsassess_empfield1_master.SetMessages()
'            clsassess_empfield2_master.SetMessages()
'            clsassess_empfield3_master.SetMessages()
'            clsassess_empfield4_master.SetMessages()
'            clsassess_empfield5_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsassess_empfield1_master,clsassess_empfield2_master,clsassess_empfield3_master,clsassess_empfield4_master,clsassess_empfield5_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            If CInt(cboEmployee.SelectedValue) <= 0 Or _
'                   CInt(cboPeriod.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee and Period are mandatory information. Please select Employee and Period to continue."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If
'            Call Fill_Grid()
'            If dgvData.RowCount <= 0 Then
'                objChkAll.Enabled = False
'            End If
'            objbtnSearch.ShowResult(dgvData.RowCount)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            cboPeriod.SelectedValue = 0
'            cboEmployee.SelectedValue = 0
'            dgvData.DataSource = Nothing
'            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
'            objChkAll.CheckState = CheckState.Unchecked
'            objChkAll.Enabled = False
'            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
'            objbtnReset.ShowResult(dgvData.RowCount)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmp.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            With frm
'                .ValueMember = cboEmployee.ValueMember
'                .DisplayMember = cboEmployee.DisplayMember
'                .CodeMember = "employeecode"
'                .DataSource = CType(cboEmployee.DataSource, DataTable)
'                If .DisplayDialog = True Then
'                    cboEmployee.SelectedValue = .SelectedValue
'                End If
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'#End Region

'#Region " Context Menu Events "

'    Private Sub objbtnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Dim frm As New frmReasonSelection
'        Try
'            Dim dtmp() As DataRow = mdtFinal.Select("ischeck=true")
'            If dtmp.Length <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Please tick atleast one employee goal to delete it."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim iVoidReason As String = String.Empty

'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            Dim iMsg As String = Language.getMessage(mstrModuleName, 7, "You are about to remove the information from employee level, this will delete all child linked to this parent") & vbCrLf & _
'                                 Language.getMessage(mstrModuleName, 8, "Are you sure you want to delete?")

'            If eZeeMsgBox.Show(iMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

'            frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
'            If iVoidReason.Length <= 0 Then
'                Exit Sub
'            End If

'            Call VoidCheckedData(CInt(sender.tag.ToString.Split("|")(1)), iVoidReason, dtmp)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnDel_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'#End Region

'#Region " CheckBox Events "

'    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
'        Try
'            For Each row As DataRow In mdtFinal.Rows
'                row.Item("ischeck") = CBool(objChkAll.CheckState)
'                row.AcceptChanges()
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Private Sub OtherSettings()
'        Try
'            Me.SuspendLayout()

'            Call SetLanguage()

'            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
'            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


'            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
'            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

'            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
'            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


'            Me.ResumeLayout()
'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetLanguage()
'        Try
'            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
'            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
'            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
'            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Sorry, No data defined for asseessment caption settings screen.")
'            Language.setMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period.")
'            Language.setMessage(mstrModuleName, 3, "Total Weight Assigned :")
'            Language.setMessage(mstrModuleName, 4, "Employee and Period are mandatory information. Please select Employee and Period to continue.")

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>