﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalVoidOwnerGoals
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalVoidOwnerGoals))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objSPC1 = New System.Windows.Forms.SplitContainer
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.lblCaption2 = New System.Windows.Forms.Label
        Me.objpnl2 = New System.Windows.Forms.Panel
        Me.objpnl1 = New System.Windows.Forms.Panel
        Me.objlblTotalWeight = New System.Windows.Forms.Label
        Me.btnVoid = New eZee.Common.eZeeSplitButton
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblOwnerType = New System.Windows.Forms.Label
        Me.cboOwnerType = New System.Windows.Forms.ComboBox
        Me.lblOwner = New System.Windows.Forms.Label
        Me.cboOwner = New System.Windows.Forms.ComboBox
        Me.objbtnSearchOwner = New eZee.Common.eZeeGradientButton
        Me.pnlMain.SuspendLayout()
        Me.objSPC1.Panel1.SuspendLayout()
        Me.objSPC1.Panel2.SuspendLayout()
        Me.objSPC1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objSPC1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(879, 418)
        Me.pnlMain.TabIndex = 0
        '
        'objSPC1
        '
        Me.objSPC1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSPC1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSPC1.IsSplitterFixed = True
        Me.objSPC1.Location = New System.Drawing.Point(0, 0)
        Me.objSPC1.Name = "objSPC1"
        Me.objSPC1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSPC1.Panel1
        '
        Me.objSPC1.Panel1.Controls.Add(Me.gbFilterCriteria)
        '
        'objSPC1.Panel2
        '
        Me.objSPC1.Panel2.Controls.Add(Me.objChkAll)
        Me.objSPC1.Panel2.Controls.Add(Me.dgvData)
        Me.objSPC1.Size = New System.Drawing.Size(879, 418)
        Me.objSPC1.SplitterDistance = 69
        Me.objSPC1.SplitterWidth = 2
        Me.objSPC1.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchOwner)
        Me.gbFilterCriteria.Controls.Add(Me.cboOwner)
        Me.gbFilterCriteria.Controls.Add(Me.lblOwner)
        Me.gbFilterCriteria.Controls.Add(Me.cboOwnerType)
        Me.gbFilterCriteria.Controls.Add(Me.lblOwnerType)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(879, 69)
        Me.gbFilterCriteria.TabIndex = 477
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(64, 17)
        Me.lblPeriod.TabIndex = 476
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(78, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(166, 21)
        Me.cboPeriod.TabIndex = 477
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(852, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(829, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(7, 5)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 478
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(879, 347)
        Me.dgvData.TabIndex = 477
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblCaption1)
        Me.objFooter.Controls.Add(Me.lblCaption2)
        Me.objFooter.Controls.Add(Me.objpnl2)
        Me.objFooter.Controls.Add(Me.objpnl1)
        Me.objFooter.Controls.Add(Me.objlblTotalWeight)
        Me.objFooter.Controls.Add(Me.btnVoid)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 418)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(879, 55)
        Me.objFooter.TabIndex = 476
        '
        'lblCaption1
        '
        Me.lblCaption1.Location = New System.Drawing.Point(53, 8)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(166, 17)
        Me.lblCaption1.TabIndex = 480
        Me.lblCaption1.Text = "Submitted for Approval(s)."
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption2
        '
        Me.lblCaption2.Location = New System.Drawing.Point(53, 29)
        Me.lblCaption2.Name = "lblCaption2"
        Me.lblCaption2.Size = New System.Drawing.Size(166, 17)
        Me.lblCaption2.TabIndex = 481
        Me.lblCaption2.Text = "Finally Approved"
        Me.lblCaption2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnl2
        '
        Me.objpnl2.BackColor = System.Drawing.Color.Blue
        Me.objpnl2.Location = New System.Drawing.Point(12, 29)
        Me.objpnl2.Name = "objpnl2"
        Me.objpnl2.Size = New System.Drawing.Size(35, 17)
        Me.objpnl2.TabIndex = 479
        '
        'objpnl1
        '
        Me.objpnl1.BackColor = System.Drawing.Color.Green
        Me.objpnl1.Location = New System.Drawing.Point(12, 8)
        Me.objpnl1.Name = "objpnl1"
        Me.objpnl1.Size = New System.Drawing.Size(35, 17)
        Me.objpnl1.TabIndex = 478
        '
        'objlblTotalWeight
        '
        Me.objlblTotalWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalWeight.Location = New System.Drawing.Point(347, 22)
        Me.objlblTotalWeight.Name = "objlblTotalWeight"
        Me.objlblTotalWeight.Size = New System.Drawing.Size(314, 13)
        Me.objlblTotalWeight.TabIndex = 477
        Me.objlblTotalWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Location = New System.Drawing.Point(667, 13)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.ShowDefaultBorderColor = True
        Me.btnVoid.Size = New System.Drawing.Size(97, 30)
        Me.btnVoid.SplitButtonMenu = Nothing
        Me.btnVoid.TabIndex = 7
        Me.btnVoid.Text = "&Void"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(770, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblOwnerType
        '
        Me.lblOwnerType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOwnerType.Location = New System.Drawing.Point(259, 37)
        Me.lblOwnerType.Name = "lblOwnerType"
        Me.lblOwnerType.Size = New System.Drawing.Size(79, 17)
        Me.lblOwnerType.TabIndex = 477
        Me.lblOwnerType.Text = "Owner Type"
        Me.lblOwnerType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOwnerType
        '
        Me.cboOwnerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwnerType.DropDownWidth = 200
        Me.cboOwnerType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwnerType.FormattingEnabled = True
        Me.cboOwnerType.Location = New System.Drawing.Point(344, 35)
        Me.cboOwnerType.Name = "cboOwnerType"
        Me.cboOwnerType.Size = New System.Drawing.Size(166, 21)
        Me.cboOwnerType.TabIndex = 483
        '
        'lblOwner
        '
        Me.lblOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOwner.Location = New System.Drawing.Point(523, 37)
        Me.lblOwner.Name = "lblOwner"
        Me.lblOwner.Size = New System.Drawing.Size(62, 17)
        Me.lblOwner.TabIndex = 484
        Me.lblOwner.Text = "Owner"
        Me.lblOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOwner
        '
        Me.cboOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwner.DropDownWidth = 200
        Me.cboOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwner.FormattingEnabled = True
        Me.cboOwner.Location = New System.Drawing.Point(591, 35)
        Me.cboOwner.Name = "cboOwner"
        Me.cboOwner.Size = New System.Drawing.Size(232, 21)
        Me.cboOwner.TabIndex = 485
        '
        'objbtnSearchOwner
        '
        Me.objbtnSearchOwner.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOwner.BorderSelected = False
        Me.objbtnSearchOwner.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOwner.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOwner.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOwner.Location = New System.Drawing.Point(829, 35)
        Me.objbtnSearchOwner.Name = "objbtnSearchOwner"
        Me.objbtnSearchOwner.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOwner.TabIndex = 482
        '
        'frmGlobalVoidOwnerGoals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(879, 473)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalVoidOwnerGoals"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Void Owner Goals"
        Me.pnlMain.ResumeLayout(False)
        Me.objSPC1.Panel1.ResumeLayout(False)
        Me.objSPC1.Panel2.ResumeLayout(False)
        Me.objSPC1.Panel2.PerformLayout()
        Me.objSPC1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objSPC1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnVoid As eZee.Common.eZeeSplitButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objlblTotalWeight As System.Windows.Forms.Label
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents lblCaption2 As System.Windows.Forms.Label
    Friend WithEvents objpnl2 As System.Windows.Forms.Panel
    Friend WithEvents objpnl1 As System.Windows.Forms.Panel
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lblOwnerType As System.Windows.Forms.Label
    Friend WithEvents cboOwnerType As System.Windows.Forms.ComboBox
    Friend WithEvents lblOwner As System.Windows.Forms.Label
    Friend WithEvents cboOwner As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchOwner As eZee.Common.eZeeGradientButton
End Class
