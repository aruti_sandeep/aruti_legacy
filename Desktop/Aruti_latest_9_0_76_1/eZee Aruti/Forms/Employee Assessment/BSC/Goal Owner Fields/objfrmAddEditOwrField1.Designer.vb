﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmAddEditOwrField1
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(objfrmAddEditOwrField1))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.txtPercent = New eZee.TextBox.NumericTextBox
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvOwner = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objtabcRemarks = New System.Windows.Forms.TabControl
        Me.objtabpRemark1 = New System.Windows.Forms.TabPage
        Me.txtRemark1 = New System.Windows.Forms.TextBox
        Me.objtabpRemark2 = New System.Windows.Forms.TabPage
        Me.txtRemark2 = New System.Windows.Forms.TextBox
        Me.objtabpRemark3 = New System.Windows.Forms.TabPage
        Me.txtRemark3 = New System.Windows.Forms.TextBox
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.lblWeight = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtPeriod = New System.Windows.Forms.TextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchOwner = New eZee.Common.eZeeGradientButton
        Me.lblOwner = New System.Windows.Forms.Label
        Me.cboOwner = New System.Windows.Forms.ComboBox
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.lblGoalOwner = New System.Windows.Forms.Label
        Me.objlblOwrField1 = New System.Windows.Forms.Label
        Me.objtxtOwrField1 = New System.Windows.Forms.TextBox
        Me.objbtnSearchField1 = New eZee.Common.eZeeGradientButton
        Me.objlblField1 = New System.Windows.Forms.Label
        Me.cboFieldValue1 = New System.Windows.Forms.ComboBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvOwner, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objtabcRemarks.SuspendLayout()
        Me.objtabpRemark1.SuspendLayout()
        Me.objtabpRemark2.SuspendLayout()
        Me.objtabpRemark3.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objpnlData)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.txtPeriod)
        Me.pnlMain.Controls.Add(Me.lblPeriod)
        Me.pnlMain.Controls.Add(Me.objbtnSearchOwner)
        Me.pnlMain.Controls.Add(Me.lblOwner)
        Me.pnlMain.Controls.Add(Me.cboOwner)
        Me.pnlMain.Controls.Add(Me.cboAllocations)
        Me.pnlMain.Controls.Add(Me.lblGoalOwner)
        Me.pnlMain.Controls.Add(Me.objlblOwrField1)
        Me.pnlMain.Controls.Add(Me.objtxtOwrField1)
        Me.pnlMain.Controls.Add(Me.objbtnSearchField1)
        Me.pnlMain.Controls.Add(Me.objlblField1)
        Me.pnlMain.Controls.Add(Me.cboFieldValue1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(779, 386)
        Me.pnlMain.TabIndex = 0
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.txtPercent)
        Me.objpnlData.Controls.Add(Me.tblpAssessorEmployee)
        Me.objpnlData.Controls.Add(Me.lblPercentage)
        Me.objpnlData.Controls.Add(Me.dtpStartDate)
        Me.objpnlData.Controls.Add(Me.lblEndDate)
        Me.objpnlData.Controls.Add(Me.lblStartDate)
        Me.objpnlData.Controls.Add(Me.dtpEndDate)
        Me.objpnlData.Controls.Add(Me.cboStatus)
        Me.objpnlData.Controls.Add(Me.lblStatus)
        Me.objpnlData.Controls.Add(Me.objtabcRemarks)
        Me.objpnlData.Controls.Add(Me.txtWeight)
        Me.objpnlData.Controls.Add(Me.lblWeight)
        Me.objpnlData.Location = New System.Drawing.Point(304, 3)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(463, 326)
        Me.objpnlData.TabIndex = 479
        '
        'txtPercent
        '
        Me.txtPercent.AllowNegative = False
        Me.txtPercent.BackColor = System.Drawing.SystemColors.Window
        Me.txtPercent.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtPercent.DigitsInGroup = 0
        Me.txtPercent.Flags = 65536
        Me.txtPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercent.Location = New System.Drawing.Point(169, 114)
        Me.txtPercent.MaxDecimalPlaces = 2
        Me.txtPercent.MaxWholeDigits = 9
        Me.txtPercent.Name = "txtPercent"
        Me.txtPercent.Prefix = ""
        Me.txtPercent.RangeMax = 1.7976931348623157E+308
        Me.txtPercent.RangeMin = -1.7976931348623157E+308
        Me.txtPercent.ReadOnly = True
        Me.txtPercent.Size = New System.Drawing.Size(54, 21)
        Me.txtPercent.TabIndex = 488
        Me.txtPercent.Text = "0.00"
        Me.txtPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(3, 141)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(459, 183)
        Me.tblpAssessorEmployee.TabIndex = 489
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill


        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(453, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvOwner)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(453, 151)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvOwner
        '
        Me.dgvOwner.AllowUserToAddRows = False
        Me.dgvOwner.AllowUserToDeleteRows = False
        Me.dgvOwner.AllowUserToResizeColumns = False
        Me.dgvOwner.AllowUserToResizeRows = False
        Me.dgvOwner.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvOwner.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvOwner.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvOwner.ColumnHeadersHeight = 21
        Me.dgvOwner.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvOwner.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvOwner.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOwner.Location = New System.Drawing.Point(0, 0)
        Me.dgvOwner.MultiSelect = False
        Me.dgvOwner.Name = "dgvOwner"
        Me.dgvOwner.RowHeadersVisible = False
        Me.dgvOwner.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOwner.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOwner.Size = New System.Drawing.Size(453, 151)
        Me.dgvOwner.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.Frozen = True
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(136, 94)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(87, 17)
        Me.lblPercentage.TabIndex = 487
        Me.lblPercentage.Text = "% Completed"
        Me.lblPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(6, 26)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpStartDate.TabIndex = 481
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(124, 5)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(93, 17)
        Me.lblEndDate.TabIndex = 484
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(6, 6)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(102, 17)
        Me.lblStartDate.TabIndex = 483
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(118, 26)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpEndDate.TabIndex = 482
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboStatus.Enabled = False
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(6, 70)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(217, 21)
        Me.cboStatus.TabIndex = 479
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(6, 50)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(211, 17)
        Me.lblStatus.TabIndex = 480
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objtabcRemarks
        '
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark1)
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark2)
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark3)
        Me.objtabcRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objtabcRemarks.Location = New System.Drawing.Point(229, 6)
        Me.objtabcRemarks.Name = "objtabcRemarks"
        Me.objtabcRemarks.SelectedIndex = 0
        Me.objtabcRemarks.Size = New System.Drawing.Size(233, 129)
        Me.objtabcRemarks.TabIndex = 478
        '
        'objtabpRemark1
        '
        Me.objtabpRemark1.Controls.Add(Me.txtRemark1)
        Me.objtabpRemark1.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark1.Name = "objtabpRemark1"
        Me.objtabpRemark1.Padding = New System.Windows.Forms.Padding(3)
        Me.objtabpRemark1.Size = New System.Drawing.Size(225, 103)
        Me.objtabpRemark1.TabIndex = 0
        Me.objtabpRemark1.Tag = "objtabpRemark1"
        Me.objtabpRemark1.UseVisualStyleBackColor = True
        '
        'txtRemark1
        '
        Me.txtRemark1.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark1.Dock = System.Windows.Forms.DockStyle.Fill

        Me.txtRemark1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

        Me.txtRemark1.Location = New System.Drawing.Point(3, 3)
        Me.txtRemark1.Multiline = True
        Me.txtRemark1.Name = "txtRemark1"
        Me.txtRemark1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark1.Size = New System.Drawing.Size(219, 97)
        Me.txtRemark1.TabIndex = 443
        '
        'objtabpRemark2
        '
        Me.objtabpRemark2.Controls.Add(Me.txtRemark2)
        Me.objtabpRemark2.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark2.Name = "objtabpRemark2"
        Me.objtabpRemark2.Padding = New System.Windows.Forms.Padding(3)
        Me.objtabpRemark2.Size = New System.Drawing.Size(225, 103)
        Me.objtabpRemark2.TabIndex = 1
        Me.objtabpRemark2.Tag = "objtabpRemark2"
        Me.objtabpRemark2.UseVisualStyleBackColor = True
        '
        'txtRemark2
        '
        Me.txtRemark2.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark2.Dock = System.Windows.Forms.DockStyle.Fill

        Me.txtRemark2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

        Me.txtRemark2.Location = New System.Drawing.Point(3, 3)
        Me.txtRemark2.Multiline = True
        Me.txtRemark2.Name = "txtRemark2"
        Me.txtRemark2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark2.Size = New System.Drawing.Size(219, 97)
        Me.txtRemark2.TabIndex = 444
        '
        'objtabpRemark3
        '
        Me.objtabpRemark3.Controls.Add(Me.txtRemark3)
        Me.objtabpRemark3.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark3.Name = "objtabpRemark3"
        Me.objtabpRemark3.Padding = New System.Windows.Forms.Padding(3)
        Me.objtabpRemark3.Size = New System.Drawing.Size(225, 103)
        Me.objtabpRemark3.TabIndex = 2
        Me.objtabpRemark3.Tag = "objtabpRemark3"
        Me.objtabpRemark3.UseVisualStyleBackColor = True
        '
        'txtRemark3
        '
        Me.txtRemark3.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark3.Dock = System.Windows.Forms.DockStyle.Fill

        Me.txtRemark3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

        Me.txtRemark3.Location = New System.Drawing.Point(3, 3)
        Me.txtRemark3.Multiline = True
        Me.txtRemark3.Name = "txtRemark3"
        Me.txtRemark3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark3.Size = New System.Drawing.Size(219, 97)
        Me.txtRemark3.TabIndex = 444
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(6, 114)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 9
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.Size = New System.Drawing.Size(57, 21)
        Me.txtWeight.TabIndex = 467
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(9, 94)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(54, 17)
        Me.lblWeight.TabIndex = 466
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 331)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(779, 55)
        Me.objFooter.TabIndex = 446
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(573, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(673, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtPeriod
        '
        Me.txtPeriod.BackColor = System.Drawing.Color.White
        Me.txtPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriod.Location = New System.Drawing.Point(12, 29)
        Me.txtPeriod.Name = "txtPeriod"
        Me.txtPeriod.ReadOnly = True
        Me.txtPeriod.Size = New System.Drawing.Size(259, 21)
        Me.txtPeriod.TabIndex = 482
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 9)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(253, 17)
        Me.lblPeriod.TabIndex = 481
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOwner
        '
        Me.objbtnSearchOwner.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOwner.BorderSelected = False
        Me.objbtnSearchOwner.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOwner.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOwner.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOwner.Location = New System.Drawing.Point(277, 117)
        Me.objbtnSearchOwner.Name = "objbtnSearchOwner"
        Me.objbtnSearchOwner.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOwner.TabIndex = 477
        '
        'lblOwner
        '
        Me.lblOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOwner.Location = New System.Drawing.Point(12, 97)
        Me.lblOwner.Name = "lblOwner"
        Me.lblOwner.Size = New System.Drawing.Size(253, 17)
        Me.lblOwner.TabIndex = 476
        Me.lblOwner.Text = "Owner"
        Me.lblOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOwner
        '
        Me.cboOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwner.DropDownWidth = 145
        Me.cboOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwner.FormattingEnabled = True
        Me.cboOwner.Location = New System.Drawing.Point(12, 117)
        Me.cboOwner.Name = "cboOwner"
        Me.cboOwner.Size = New System.Drawing.Size(259, 21)
        Me.cboOwner.TabIndex = 475
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(12, 73)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(259, 21)
        Me.cboAllocations.TabIndex = 73
        '
        'lblGoalOwner
        '
        Me.lblGoalOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalOwner.Location = New System.Drawing.Point(12, 53)
        Me.lblGoalOwner.Name = "lblGoalOwner"
        Me.lblGoalOwner.Size = New System.Drawing.Size(253, 17)
        Me.lblGoalOwner.TabIndex = 74
        Me.lblGoalOwner.Text = "Owner Category"
        Me.lblGoalOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblOwrField1
        '
        Me.objlblOwrField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOwrField1.Location = New System.Drawing.Point(12, 185)
        Me.objlblOwrField1.Name = "objlblOwrField1"
        Me.objlblOwrField1.Size = New System.Drawing.Size(253, 17)
        Me.objlblOwrField1.TabIndex = 474
        Me.objlblOwrField1.Text = "#Caption"
        '
        'objtxtOwrField1
        '
        Me.objtxtOwrField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objtxtOwrField1.Location = New System.Drawing.Point(12, 205)
        Me.objtxtOwrField1.Multiline = True
        Me.objtxtOwrField1.Name = "objtxtOwrField1"
        Me.objtxtOwrField1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.objtxtOwrField1.Size = New System.Drawing.Size(259, 122)
        Me.objtxtOwrField1.TabIndex = 473
        '
        'objbtnSearchField1
        '
        Me.objbtnSearchField1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField1.BorderSelected = False
        Me.objbtnSearchField1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField1.Location = New System.Drawing.Point(277, 161)
        Me.objbtnSearchField1.Name = "objbtnSearchField1"
        Me.objbtnSearchField1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField1.TabIndex = 468
        '
        'objlblField1
        '
        Me.objlblField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField1.Location = New System.Drawing.Point(12, 141)
        Me.objlblField1.Name = "objlblField1"
        Me.objlblField1.Size = New System.Drawing.Size(253, 17)
        Me.objlblField1.TabIndex = 465
        Me.objlblField1.Text = "#Caption"
        Me.objlblField1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue1
        '
        Me.cboFieldValue1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue1.FormattingEnabled = True
        Me.cboFieldValue1.Location = New System.Drawing.Point(12, 161)
        Me.cboFieldValue1.Name = "cboFieldValue1"
        Me.cboFieldValue1.Size = New System.Drawing.Size(259, 21)
        Me.cboFieldValue1.TabIndex = 464
        '
        'objfrmAddEditOwrField1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 386)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmAddEditOwrField1"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objpnlData.ResumeLayout(False)
        Me.objpnlData.PerformLayout()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvOwner, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objtabcRemarks.ResumeLayout(False)
        Me.objtabpRemark1.ResumeLayout(False)
        Me.objtabpRemark1.PerformLayout()
        Me.objtabpRemark2.ResumeLayout(False)
        Me.objtabpRemark2.PerformLayout()
        Me.objtabpRemark3.ResumeLayout(False)
        Me.objtabpRemark3.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchField1 As eZee.Common.eZeeGradientButton
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents objlblField1 As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue1 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblOwrField1 As System.Windows.Forms.Label
    Friend WithEvents objtxtOwrField1 As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSearchOwner As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOwner As System.Windows.Forms.Label
    Friend WithEvents cboOwner As System.Windows.Forms.ComboBox
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents lblGoalOwner As System.Windows.Forms.Label
    Friend WithEvents objtabcRemarks As System.Windows.Forms.TabControl
    Friend WithEvents objtabpRemark1 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark1 As System.Windows.Forms.TextBox
    Friend WithEvents objtabpRemark2 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark2 As System.Windows.Forms.TextBox
    Friend WithEvents objtabpRemark3 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark3 As System.Windows.Forms.TextBox
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtPercent As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtPeriod As System.Windows.Forms.TextBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvOwner As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
