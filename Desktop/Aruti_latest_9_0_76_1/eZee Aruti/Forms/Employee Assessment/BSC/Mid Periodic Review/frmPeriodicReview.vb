﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPeriodicReview

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmPeriodicReview"
    Private mdtEmployee As DataTable
    Private mdView As DataView
    Private mstrAdvanceFilter As String = String.Empty
    Private objPriodicReview As clsassess_periodic_review

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try
            'RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim objEmpField1 As New clsassess_empfield1_master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mdtEmployee = objEmpField1.GetEmployee_ForPeriodicReview(CInt(cboPeriod.SelectedValue), mstrAdvanceFilter)
            mdtEmployee = objEmpField1.GetEmployee_ForPeriodicReview(FinancialYear._Object._DatabaseName, _
                                                                     User._Object._Userunkid, _
                                                                     FinancialYear._Object._YearUnkid, _
                                                                     Company._Object._Companyunkid, _
                                                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                     CInt(cboPeriod.SelectedValue), mstrAdvanceFilter)
            'S.SANDEEP [04 JUN 2015] -- END

            mdView = mdtEmployee.DefaultView
            dgvData.AutoGenerateColumns = False
            dgcolhECode.DataPropertyName = "employeecode"
            dgcolhName.DataPropertyName = "employeename"
            objdgcolhEmail.DataPropertyName = "email"
            objdgcolhCheck.DataPropertyName = "ischeck"
            objdgEmpId.DataPropertyName = "employeeunkid"
            dgvData.DataSource = mdView
            objEmpField1 = Nothing
            If dgvData.RowCount > 0 Then
                lnkAllocation.Enabled = True : objbtnReset.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmPeriodicReview_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objPriodicReview = Nothing
    End Sub

    Private Sub frmPeriodicReview_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPriodicReview = New clsassess_periodic_review
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeriodicReview_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_periodic_review.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_periodic_review"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrAdvanceFilter = ""
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        Try
            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, No data found to open for periodic review."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtEmployee.AcceptChanges()
            Dim xRow() As DataRow = Nothing
            xRow = mdtEmployee.Select("ischeck=true")
            If xRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Please check atleast one employee for periodic review."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            xRow = mdtEmployee.Select("ischeck=true AND email = ''")
            If xRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, there are some of the checked employee(s) which does not have emails defined.") & vbCrLf & _
                                Language.getMessage(mstrModuleName, 4, "Due to this notitification will be sent to those employee(s) for periodic review and will be highlighted with red."), enMsgBoxStyle.Information)
                Dim lRows As IEnumerable(Of DataGridViewRow) = From row As DataGridViewRow In dgvData.Rows Select row
                For i As Integer = 0 To xRow.Length - 1
                    Dim xLoopId As Integer = i
                    Dim rows = lRows.Cast(Of DataGridViewRow)().Where(Function(row) row.Cells(dgcolhECode.Index).Value.ToString().Equals(xRow(xLoopId).Item("employeecode")))
                    If rows.Count > 0 Then
                        Dim idx As Integer = rows(0).Index
                        If idx >= 0 Then
                            dgvData.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                            dgvData.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                            dgvData.Rows(idx).DefaultCellStyle.SelectionBackColor = Color.White
                        End If
                    End If
                Next
            End If
            xRow = mdtEmployee.Select("ischeck=true AND email <> ''")
            If xRow.Length > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Do you wish to contrinue?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If

                Dim iUnlockComments As String = ""

                iUnlockComments = InputBox(Language.getMessage(mstrModuleName, 6, "Enter reason to unlock for periodic review."), Me.Text)
                If iUnlockComments.Trim.Length > 0 Then
                    For xRdx As Integer = 0 To xRow.Length - 1

                        'S.SANDEEP |27-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                        'Dim xMsg As String = ""
                        'xMsg = (New clsassess_empfield1_master).IsGoalsAssignedSubEmployee(CInt(cboPeriod.SelectedValue), CInt(xRow(xRdx)("employeeunkid")))
                        'If xMsg.Trim.Length > 0 Then
                        '    Dim msg As String = Language.getMessage(mstrModuleName, 200, "For Checked Employee : ") & " " & "[" & xRow(xRdx).Item("employeename").ToString & "]." & vbCrLf & _
                        '    xMsg
                        '    eZeeMsgBox.Show(msg, enMsgBoxStyle.Information)
                        '    Exit Sub
                        'End If
                        'S.SANDEEP |27-NOV-2020| -- END

                        Dim objEStatusTran As New clsassess_empstatus_tran
                        objEStatusTran._Assessoremployeeunkid = 0
                        objEStatusTran._Assessormasterunkid = 0
                        objEStatusTran._Commtents = iUnlockComments
                        objEStatusTran._Employeeunkid = xRow(xRdx).Item("employeeunkid")
                        objEStatusTran._Isunlock = True
                        objEStatusTran._Loginemployeeunkid = 0
                        objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                        objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                        objEStatusTran._Statustypeid = enObjective_Status.PERIODIC_REVIEW
                        objEStatusTran._Userunkid = User._Object._Userunkid
                        If objEStatusTran.Insert() = False Then
                            eZeeMsgBox.Show(objEStatusTran._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        objEStatusTran = Nothing
                    Next
                    Dim iChkItemEmails As String = String.Empty
                    Dim selectedTags As List(Of String) = (From p In mdtEmployee.AsEnumerable() Where p.Field(Of Boolean)("ischeck") = True And p.Field(Of String)("email") <> "" Select (p.Item("email").ToString)).Distinct().ToList()
                    iChkItemEmails = String.Join(",", selectedTags.ToArray())
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPriodicReview.SendPeriodicNotification(iChkItemEmails, CInt(cboPeriod.SelectedValue))
                    objPriodicReview.SendPeriodicNotification(iChkItemEmails, CInt(cboPeriod.SelectedValue), FinancialYear._Object._DatabaseName)
                    'Sohail (21 Aug 2015) -- End
                    Call FillGrid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpen_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Call FillGrid()
            Else
                dgvData.DataSource = Nothing : mdtEmployee = Nothing : mdView = Nothing
                lnkAllocation.Enabled = False : objbtnReset.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call FillGrid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Textbox Event "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = dgvData.Rows(dgvData.RowCount - 1).Index Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhECode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            mdView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event "

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
            If e.ColumnIndex = objdgcolhCheck.Index Then
                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = mdView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If mdView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In mdView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnOpen.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpen.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnOpen.Text = Language._Object.getCaption(Me.btnOpen.Name, Me.btnOpen.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.dgcolhECode.HeaderText = Language._Object.getCaption(Me.dgcolhECode.Name, Me.dgcolhECode.HeaderText)
			Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, No data found to open for periodic review.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Please check atleast one employee for periodic review.")
			Language.setMessage(mstrModuleName, 3, "Sorry, there are some of the checked employee(s) which does not have emails defined.")
			Language.setMessage(mstrModuleName, 4, "Due to this notitification will be sent to those employee(s) for periodic review and will be highlighted with red.")
			Language.setMessage(mstrModuleName, 5, "Do you wish to contrinue?")
			Language.setMessage(mstrModuleName, 6, "Enter reason to unlock for periodic review.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class