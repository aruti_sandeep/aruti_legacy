﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportEmployeeGoalsWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportEmployeeGoalsWizard))
        Me.ezeeWizardImportEmployeeGoal = New eZee.Common.eZeeWizard
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlGoalMapping = New System.Windows.Forms.Panel
        Me.objlblASign13 = New System.Windows.Forms.Label
        Me.cboUomType = New System.Windows.Forms.ComboBox
        Me.lblUOMType = New System.Windows.Forms.Label
        Me.objlblASign12 = New System.Windows.Forms.Label
        Me.cboGoalValue = New System.Windows.Forms.ComboBox
        Me.lblGoalValue = New System.Windows.Forms.Label
        Me.objlblASign11 = New System.Windows.Forms.Label
        Me.cboGoalType = New System.Windows.Forms.ComboBox
        Me.lblGoalType = New System.Windows.Forms.Label
        Me.objlblASign10 = New System.Windows.Forms.Label
        Me.objlblASign9 = New System.Windows.Forms.Label
        Me.objlblASign8 = New System.Windows.Forms.Label
        Me.objlblASign7 = New System.Windows.Forms.Label
        Me.objlblASign6 = New System.Windows.Forms.Label
        Me.objlblASign3 = New System.Windows.Forms.Label
        Me.lblField5 = New System.Windows.Forms.Label
        Me.lblField4 = New System.Windows.Forms.Label
        Me.lblField3 = New System.Windows.Forms.Label
        Me.lblField2 = New System.Windows.Forms.Label
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.cboField_1 = New System.Windows.Forms.ComboBox
        Me.lblField1 = New System.Windows.Forms.Label
        Me.objlblASign5 = New System.Windows.Forms.Label
        Me.cboField_5 = New System.Windows.Forms.ComboBox
        Me.cboField_4 = New System.Windows.Forms.ComboBox
        Me.cboField_3 = New System.Windows.Forms.ComboBox
        Me.cboField_2 = New System.Windows.Forms.ComboBox
        Me.cboWeight = New System.Windows.Forms.ComboBox
        Me.lblWeight = New System.Windows.Forms.Label
        Me.cboPerspective_Name = New System.Windows.Forms.ComboBox
        Me.lblPerspective = New System.Windows.Forms.Label
        Me.objlblASign2 = New System.Windows.Forms.Label
        Me.cboEmployee_Code = New System.Windows.Forms.ComboBox
        Me.cboPeriod_Name = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.lblPeriodCode = New System.Windows.Forms.Label
        Me.objlblASign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.objlblOtherNote = New System.Windows.Forms.Label
        Me.chkImportWithApprovedStatus = New System.Windows.Forms.CheckBox
        Me.objlblAdditionalNote = New System.Windows.Forms.Label
        Me.lnkGenerateFile = New System.Windows.Forms.LinkLabel
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.objlblGoalApproval = New System.Windows.Forms.Label
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhlogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboStartDate = New System.Windows.Forms.ComboBox
        Me.lblStDate = New System.Windows.Forms.Label
        Me.cboEndDate = New System.Windows.Forms.ComboBox
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.ezeeWizardImportEmployeeGoal.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.pnlGoalMapping.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ezeeWizardImportEmployeeGoal
        '
        Me.ezeeWizardImportEmployeeGoal.Controls.Add(Me.WizPageSelectFile)
        Me.ezeeWizardImportEmployeeGoal.Controls.Add(Me.WizPageMapping)
        Me.ezeeWizardImportEmployeeGoal.Controls.Add(Me.WizPageImporting)
        Me.ezeeWizardImportEmployeeGoal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ezeeWizardImportEmployeeGoal.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.ezeeWizardImportEmployeeGoal.Location = New System.Drawing.Point(0, 0)
        Me.ezeeWizardImportEmployeeGoal.Name = "ezeeWizardImportEmployeeGoal"
        Me.ezeeWizardImportEmployeeGoal.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.ezeeWizardImportEmployeeGoal.SaveEnabled = True
        Me.ezeeWizardImportEmployeeGoal.SaveText = "Save && Finish"
        Me.ezeeWizardImportEmployeeGoal.SaveVisible = False
        Me.ezeeWizardImportEmployeeGoal.SetSaveIndexBeforeFinishIndex = False
        Me.ezeeWizardImportEmployeeGoal.Size = New System.Drawing.Size(698, 480)
        Me.ezeeWizardImportEmployeeGoal.TabIndex = 3
        Me.ezeeWizardImportEmployeeGoal.WelcomeImage = Nothing
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(698, 432)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.pnlGoalMapping)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(534, 431)
        Me.gbFieldMapping.TabIndex = 1
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlGoalMapping
        '
        Me.pnlGoalMapping.Controls.Add(Me.cboEndDate)
        Me.pnlGoalMapping.Controls.Add(Me.lblEndDate)
        Me.pnlGoalMapping.Controls.Add(Me.cboStartDate)
        Me.pnlGoalMapping.Controls.Add(Me.lblStDate)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign13)
        Me.pnlGoalMapping.Controls.Add(Me.cboUomType)
        Me.pnlGoalMapping.Controls.Add(Me.lblUOMType)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign12)
        Me.pnlGoalMapping.Controls.Add(Me.cboGoalValue)
        Me.pnlGoalMapping.Controls.Add(Me.lblGoalValue)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign11)
        Me.pnlGoalMapping.Controls.Add(Me.cboGoalType)
        Me.pnlGoalMapping.Controls.Add(Me.lblGoalType)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign10)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign9)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign8)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign7)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign6)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign3)
        Me.pnlGoalMapping.Controls.Add(Me.lblField5)
        Me.pnlGoalMapping.Controls.Add(Me.lblField4)
        Me.pnlGoalMapping.Controls.Add(Me.lblField3)
        Me.pnlGoalMapping.Controls.Add(Me.lblField2)
        Me.pnlGoalMapping.Controls.Add(Me.lnkAutoMap)
        Me.pnlGoalMapping.Controls.Add(Me.cboField_1)
        Me.pnlGoalMapping.Controls.Add(Me.lblField1)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign5)
        Me.pnlGoalMapping.Controls.Add(Me.cboField_5)
        Me.pnlGoalMapping.Controls.Add(Me.cboField_4)
        Me.pnlGoalMapping.Controls.Add(Me.cboField_3)
        Me.pnlGoalMapping.Controls.Add(Me.cboField_2)
        Me.pnlGoalMapping.Controls.Add(Me.cboWeight)
        Me.pnlGoalMapping.Controls.Add(Me.lblWeight)
        Me.pnlGoalMapping.Controls.Add(Me.cboPerspective_Name)
        Me.pnlGoalMapping.Controls.Add(Me.lblPerspective)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign2)
        Me.pnlGoalMapping.Controls.Add(Me.cboEmployee_Code)
        Me.pnlGoalMapping.Controls.Add(Me.cboPeriod_Name)
        Me.pnlGoalMapping.Controls.Add(Me.lblEmployeeCode)
        Me.pnlGoalMapping.Controls.Add(Me.lblPeriodCode)
        Me.pnlGoalMapping.Controls.Add(Me.objlblASign1)
        Me.pnlGoalMapping.Location = New System.Drawing.Point(2, 26)
        Me.pnlGoalMapping.Name = "pnlGoalMapping"
        Me.pnlGoalMapping.Size = New System.Drawing.Size(529, 403)
        Me.pnlGoalMapping.TabIndex = 1
        '
        'objlblASign13
        '
        Me.objlblASign13.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign13.ForeColor = System.Drawing.Color.Red
        Me.objlblASign13.Location = New System.Drawing.Point(29, 309)
        Me.objlblASign13.Name = "objlblASign13"
        Me.objlblASign13.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign13.TabIndex = 80
        Me.objlblASign13.Text = "*"
        Me.objlblASign13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboUomType
        '
        Me.cboUomType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUomType.DropDownWidth = 190
        Me.cboUomType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUomType.FormattingEnabled = True
        Me.cboUomType.Location = New System.Drawing.Point(172, 307)
        Me.cboUomType.Name = "cboUomType"
        Me.cboUomType.Size = New System.Drawing.Size(244, 21)
        Me.cboUomType.TabIndex = 79
        '
        'lblUOMType
        '
        Me.lblUOMType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUOMType.Location = New System.Drawing.Point(45, 309)
        Me.lblUOMType.Name = "lblUOMType"
        Me.lblUOMType.Size = New System.Drawing.Size(121, 16)
        Me.lblUOMType.TabIndex = 78
        Me.lblUOMType.Text = "UoM Type"
        Me.lblUOMType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign12
        '
        Me.objlblASign12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign12.ForeColor = System.Drawing.Color.Red
        Me.objlblASign12.Location = New System.Drawing.Point(29, 282)
        Me.objlblASign12.Name = "objlblASign12"
        Me.objlblASign12.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign12.TabIndex = 77
        Me.objlblASign12.Text = "*"
        Me.objlblASign12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboGoalValue
        '
        Me.cboGoalValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGoalValue.DropDownWidth = 190
        Me.cboGoalValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGoalValue.FormattingEnabled = True
        Me.cboGoalValue.Location = New System.Drawing.Point(172, 280)
        Me.cboGoalValue.Name = "cboGoalValue"
        Me.cboGoalValue.Size = New System.Drawing.Size(244, 21)
        Me.cboGoalValue.TabIndex = 76
        '
        'lblGoalValue
        '
        Me.lblGoalValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalValue.Location = New System.Drawing.Point(45, 282)
        Me.lblGoalValue.Name = "lblGoalValue"
        Me.lblGoalValue.Size = New System.Drawing.Size(121, 16)
        Me.lblGoalValue.TabIndex = 75
        Me.lblGoalValue.Text = "Goal Value"
        Me.lblGoalValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign11
        '
        Me.objlblASign11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign11.ForeColor = System.Drawing.Color.Red
        Me.objlblASign11.Location = New System.Drawing.Point(29, 255)
        Me.objlblASign11.Name = "objlblASign11"
        Me.objlblASign11.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign11.TabIndex = 74
        Me.objlblASign11.Text = "*"
        Me.objlblASign11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboGoalType
        '
        Me.cboGoalType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGoalType.DropDownWidth = 190
        Me.cboGoalType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGoalType.FormattingEnabled = True
        Me.cboGoalType.Location = New System.Drawing.Point(172, 253)
        Me.cboGoalType.Name = "cboGoalType"
        Me.cboGoalType.Size = New System.Drawing.Size(244, 21)
        Me.cboGoalType.TabIndex = 73
        '
        'lblGoalType
        '
        Me.lblGoalType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalType.Location = New System.Drawing.Point(45, 255)
        Me.lblGoalType.Name = "lblGoalType"
        Me.lblGoalType.Size = New System.Drawing.Size(121, 16)
        Me.lblGoalType.TabIndex = 72
        Me.lblGoalType.Text = "Goal Type"
        Me.lblGoalType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign10
        '
        Me.objlblASign10.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign10.ForeColor = System.Drawing.Color.Red
        Me.objlblASign10.Location = New System.Drawing.Point(29, 228)
        Me.objlblASign10.Name = "objlblASign10"
        Me.objlblASign10.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign10.TabIndex = 71
        Me.objlblASign10.Text = "*"
        Me.objlblASign10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign9
        '
        Me.objlblASign9.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign9.ForeColor = System.Drawing.Color.Red
        Me.objlblASign9.Location = New System.Drawing.Point(29, 201)
        Me.objlblASign9.Name = "objlblASign9"
        Me.objlblASign9.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign9.TabIndex = 70
        Me.objlblASign9.Text = "*"
        Me.objlblASign9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign8
        '
        Me.objlblASign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign8.ForeColor = System.Drawing.Color.Red
        Me.objlblASign8.Location = New System.Drawing.Point(29, 174)
        Me.objlblASign8.Name = "objlblASign8"
        Me.objlblASign8.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign8.TabIndex = 69
        Me.objlblASign8.Text = "*"
        Me.objlblASign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign7
        '
        Me.objlblASign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign7.ForeColor = System.Drawing.Color.Red
        Me.objlblASign7.Location = New System.Drawing.Point(29, 147)
        Me.objlblASign7.Name = "objlblASign7"
        Me.objlblASign7.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign7.TabIndex = 68
        Me.objlblASign7.Text = "*"
        Me.objlblASign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign6
        '
        Me.objlblASign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign6.ForeColor = System.Drawing.Color.Red
        Me.objlblASign6.Location = New System.Drawing.Point(29, 120)
        Me.objlblASign6.Name = "objlblASign6"
        Me.objlblASign6.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign6.TabIndex = 67
        Me.objlblASign6.Text = "*"
        Me.objlblASign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign3
        '
        Me.objlblASign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign3.ForeColor = System.Drawing.Color.Red
        Me.objlblASign3.Location = New System.Drawing.Point(29, 66)
        Me.objlblASign3.Name = "objlblASign3"
        Me.objlblASign3.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign3.TabIndex = 65
        Me.objlblASign3.Text = "*"
        Me.objlblASign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblField5
        '
        Me.lblField5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblField5.Location = New System.Drawing.Point(45, 201)
        Me.lblField5.Name = "lblField5"
        Me.lblField5.Size = New System.Drawing.Size(121, 16)
        Me.lblField5.TabIndex = 64
        Me.lblField5.Text = "Field 5"
        Me.lblField5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblField4
        '
        Me.lblField4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblField4.Location = New System.Drawing.Point(45, 174)
        Me.lblField4.Name = "lblField4"
        Me.lblField4.Size = New System.Drawing.Size(121, 16)
        Me.lblField4.TabIndex = 63
        Me.lblField4.Text = "Field 4"
        Me.lblField4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblField3
        '
        Me.lblField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblField3.Location = New System.Drawing.Point(45, 147)
        Me.lblField3.Name = "lblField3"
        Me.lblField3.Size = New System.Drawing.Size(121, 16)
        Me.lblField3.TabIndex = 62
        Me.lblField3.Text = "Field 3"
        Me.lblField3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblField2
        '
        Me.lblField2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblField2.Location = New System.Drawing.Point(45, 120)
        Me.lblField2.Name = "lblField2"
        Me.lblField2.Size = New System.Drawing.Size(121, 16)
        Me.lblField2.TabIndex = 61
        Me.lblField2.Text = "Field 2"
        Me.lblField2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(422, 377)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(98, 14)
        Me.lnkAutoMap.TabIndex = 60
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboField_1
        '
        Me.cboField_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboField_1.DropDownWidth = 190
        Me.cboField_1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboField_1.FormattingEnabled = True
        Me.cboField_1.Location = New System.Drawing.Point(172, 91)
        Me.cboField_1.Name = "cboField_1"
        Me.cboField_1.Size = New System.Drawing.Size(244, 21)
        Me.cboField_1.TabIndex = 12
        '
        'lblField1
        '
        Me.lblField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblField1.Location = New System.Drawing.Point(45, 93)
        Me.lblField1.Name = "lblField1"
        Me.lblField1.Size = New System.Drawing.Size(121, 16)
        Me.lblField1.TabIndex = 11
        Me.lblField1.Text = "Field 1"
        Me.lblField1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign5
        '
        Me.objlblASign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign5.ForeColor = System.Drawing.Color.Red
        Me.objlblASign5.Location = New System.Drawing.Point(29, 93)
        Me.objlblASign5.Name = "objlblASign5"
        Me.objlblASign5.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign5.TabIndex = 10
        Me.objlblASign5.Text = "*"
        Me.objlblASign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboField_5
        '
        Me.cboField_5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboField_5.DropDownWidth = 190
        Me.cboField_5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboField_5.FormattingEnabled = True
        Me.cboField_5.Location = New System.Drawing.Point(172, 199)
        Me.cboField_5.Name = "cboField_5"
        Me.cboField_5.Size = New System.Drawing.Size(244, 21)
        Me.cboField_5.TabIndex = 27
        '
        'cboField_4
        '
        Me.cboField_4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboField_4.DropDownWidth = 190
        Me.cboField_4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboField_4.FormattingEnabled = True
        Me.cboField_4.Location = New System.Drawing.Point(172, 172)
        Me.cboField_4.Name = "cboField_4"
        Me.cboField_4.Size = New System.Drawing.Size(244, 21)
        Me.cboField_4.TabIndex = 24
        '
        'cboField_3
        '
        Me.cboField_3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboField_3.DropDownWidth = 190
        Me.cboField_3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboField_3.FormattingEnabled = True
        Me.cboField_3.Location = New System.Drawing.Point(172, 145)
        Me.cboField_3.Name = "cboField_3"
        Me.cboField_3.Size = New System.Drawing.Size(244, 21)
        Me.cboField_3.TabIndex = 21
        '
        'cboField_2
        '
        Me.cboField_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboField_2.DropDownWidth = 190
        Me.cboField_2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboField_2.FormattingEnabled = True
        Me.cboField_2.Location = New System.Drawing.Point(172, 118)
        Me.cboField_2.Name = "cboField_2"
        Me.cboField_2.Size = New System.Drawing.Size(244, 21)
        Me.cboField_2.TabIndex = 18
        '
        'cboWeight
        '
        Me.cboWeight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWeight.DropDownWidth = 190
        Me.cboWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWeight.FormattingEnabled = True
        Me.cboWeight.Location = New System.Drawing.Point(172, 226)
        Me.cboWeight.Name = "cboWeight"
        Me.cboWeight.Size = New System.Drawing.Size(244, 21)
        Me.cboWeight.TabIndex = 15
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(45, 228)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(121, 16)
        Me.lblWeight.TabIndex = 14
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPerspective_Name
        '
        Me.cboPerspective_Name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPerspective_Name.DropDownWidth = 190
        Me.cboPerspective_Name.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPerspective_Name.FormattingEnabled = True
        Me.cboPerspective_Name.Location = New System.Drawing.Point(172, 64)
        Me.cboPerspective_Name.Name = "cboPerspective_Name"
        Me.cboPerspective_Name.Size = New System.Drawing.Size(244, 21)
        Me.cboPerspective_Name.TabIndex = 9
        '
        'lblPerspective
        '
        Me.lblPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerspective.Location = New System.Drawing.Point(45, 66)
        Me.lblPerspective.Name = "lblPerspective"
        Me.lblPerspective.Size = New System.Drawing.Size(121, 16)
        Me.lblPerspective.TabIndex = 8
        Me.lblPerspective.Text = "Perspective Name"
        Me.lblPerspective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign2
        '
        Me.objlblASign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign2.ForeColor = System.Drawing.Color.Red
        Me.objlblASign2.Location = New System.Drawing.Point(29, 39)
        Me.objlblASign2.Name = "objlblASign2"
        Me.objlblASign2.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign2.TabIndex = 7
        Me.objlblASign2.Text = "*"
        Me.objlblASign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmployee_Code
        '
        Me.cboEmployee_Code.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee_Code.DropDownWidth = 190
        Me.cboEmployee_Code.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee_Code.FormattingEnabled = True
        Me.cboEmployee_Code.Location = New System.Drawing.Point(172, 10)
        Me.cboEmployee_Code.Name = "cboEmployee_Code"
        Me.cboEmployee_Code.Size = New System.Drawing.Size(244, 21)
        Me.cboEmployee_Code.TabIndex = 2
        '
        'cboPeriod_Name
        '
        Me.cboPeriod_Name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod_Name.DropDownWidth = 190
        Me.cboPeriod_Name.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod_Name.FormattingEnabled = True
        Me.cboPeriod_Name.Location = New System.Drawing.Point(172, 37)
        Me.cboPeriod_Name.Name = "cboPeriod_Name"
        Me.cboPeriod_Name.Size = New System.Drawing.Size(244, 21)
        Me.cboPeriod_Name.TabIndex = 6
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(45, 12)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(121, 16)
        Me.lblEmployeeCode.TabIndex = 1
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriodCode
        '
        Me.lblPeriodCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodCode.Location = New System.Drawing.Point(45, 39)
        Me.lblPeriodCode.Name = "lblPeriodCode"
        Me.lblPeriodCode.Size = New System.Drawing.Size(121, 16)
        Me.lblPeriodCode.TabIndex = 5
        Me.lblPeriodCode.Text = "Period Code"
        Me.lblPeriodCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign1
        '
        Me.objlblASign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign1.ForeColor = System.Drawing.Color.Red
        Me.objlblASign1.Location = New System.Drawing.Point(29, 12)
        Me.objlblASign1.Name = "objlblASign1"
        Me.objlblASign1.Size = New System.Drawing.Size(10, 16)
        Me.objlblASign1.TabIndex = 0
        Me.objlblASign1.Text = "*"
        Me.objlblASign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(246, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(284, 19)
        Me.lblCaption.TabIndex = 323
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.objlblOtherNote)
        Me.WizPageSelectFile.Controls.Add(Me.chkImportWithApprovedStatus)
        Me.WizPageSelectFile.Controls.Add(Me.objlblAdditionalNote)
        Me.WizPageSelectFile.Controls.Add(Me.lnkGenerateFile)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(698, 432)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'objlblOtherNote
        '
        Me.objlblOtherNote.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOtherNote.ForeColor = System.Drawing.Color.Blue
        Me.objlblOtherNote.Location = New System.Drawing.Point(187, 235)
        Me.objlblOtherNote.Name = "objlblOtherNote"
        Me.objlblOtherNote.Size = New System.Drawing.Size(499, 165)
        Me.objlblOtherNote.TabIndex = 331
        '
        'chkImportWithApprovedStatus
        '
        Me.chkImportWithApprovedStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkImportWithApprovedStatus.Location = New System.Drawing.Point(187, 403)
        Me.chkImportWithApprovedStatus.Name = "chkImportWithApprovedStatus"
        Me.chkImportWithApprovedStatus.Size = New System.Drawing.Size(282, 17)
        Me.chkImportWithApprovedStatus.TabIndex = 330
        Me.chkImportWithApprovedStatus.Text = "Import With Approved Status"
        Me.chkImportWithApprovedStatus.UseVisualStyleBackColor = True
        '
        'objlblAdditionalNote
        '
        Me.objlblAdditionalNote.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAdditionalNote.ForeColor = System.Drawing.Color.DarkRed
        Me.objlblAdditionalNote.Location = New System.Drawing.Point(187, 115)
        Me.objlblAdditionalNote.Name = "objlblAdditionalNote"
        Me.objlblAdditionalNote.Size = New System.Drawing.Size(499, 120)
        Me.objlblAdditionalNote.TabIndex = 329
        '
        'lnkGenerateFile
        '
        Me.lnkGenerateFile.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkGenerateFile.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkGenerateFile.Location = New System.Drawing.Point(551, 403)
        Me.lnkGenerateFile.Name = "lnkGenerateFile"
        Me.lnkGenerateFile.Size = New System.Drawing.Size(135, 17)
        Me.lnkGenerateFile.TabIndex = 328
        Me.lnkGenerateFile.TabStop = True
        Me.lnkGenerateFile.Text = "Get File Format"
        Me.lnkGenerateFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(663, 91)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 21)
        Me.btnOpenFile.TabIndex = 22
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(187, 91)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(470, 21)
        Me.txtFilePath.TabIndex = 21
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(184, 74)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(228, 14)
        Me.lblSelectfile.TabIndex = 20
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(184, 40)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(426, 34)
        Me.lblMessage.TabIndex = 19
        Me.lblMessage.Text = "This wizard will import Employee 'Goals' records made from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(183, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(427, 23)
        Me.lblTitle.TabIndex = 18
        Me.lblTitle.Text = "Employee Goal Import Wizard"
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(698, 432)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 396)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(91, 31)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 24
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.objlblGoalApproval)
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 15)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(674, 51)
        Me.pnlInfo.TabIndex = 21
        '
        'objlblGoalApproval
        '
        Me.objlblGoalApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.objlblGoalApproval.Location = New System.Drawing.Point(56, 7)
        Me.objlblGoalApproval.Name = "objlblGoalApproval"
        Me.objlblGoalApproval.Size = New System.Drawing.Size(376, 35)
        Me.objlblGoalApproval.TabIndex = 25
        Me.objlblGoalApproval.Text = "Please Wait, Approving Goals Now"
        Me.objlblGoalApproval.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objlblGoalApproval.Visible = False
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(552, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(438, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(552, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(484, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(597, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(438, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(597, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(484, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhlogindate, Me.colhStatus, Me.colhMessage, Me.objcolhstatus, Me.objcolhDate})
        Me.dgData.Location = New System.Drawing.Point(12, 72)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(674, 318)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhlogindate
        '
        Me.colhlogindate.HeaderText = "Date"
        Me.colhlogindate.Name = "colhlogindate"
        Me.colhlogindate.ReadOnly = True
        Me.colhlogindate.Visible = False
        Me.colhlogindate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 250
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Message"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 250
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objcolhstatus"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objcolhDate"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'cboStartDate
        '
        Me.cboStartDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStartDate.DropDownWidth = 190
        Me.cboStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStartDate.FormattingEnabled = True
        Me.cboStartDate.Location = New System.Drawing.Point(172, 334)
        Me.cboStartDate.Name = "cboStartDate"
        Me.cboStartDate.Size = New System.Drawing.Size(244, 21)
        Me.cboStartDate.TabIndex = 82
        '
        'lblStDate
        '
        Me.lblStDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStDate.Location = New System.Drawing.Point(45, 336)
        Me.lblStDate.Name = "lblStDate"
        Me.lblStDate.Size = New System.Drawing.Size(121, 16)
        Me.lblStDate.TabIndex = 81
        Me.lblStDate.Text = "Start Date"
        Me.lblStDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEndDate
        '
        Me.cboEndDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEndDate.DropDownWidth = 190
        Me.cboEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEndDate.FormattingEnabled = True
        Me.cboEndDate.Location = New System.Drawing.Point(172, 361)
        Me.cboEndDate.Name = "cboEndDate"
        Me.cboEndDate.Size = New System.Drawing.Size(244, 21)
        Me.cboEndDate.TabIndex = 84
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(45, 363)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(121, 16)
        Me.lblEndDate.TabIndex = 83
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmImportEmployeeGoalsWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(698, 480)
        Me.Controls.Add(Me.ezeeWizardImportEmployeeGoal)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportEmployeeGoalsWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Employee Goals Wizard"
        Me.ezeeWizardImportEmployeeGoal.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.pnlGoalMapping.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.WizPageImporting.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ezeeWizardImportEmployeeGoal As eZee.Common.eZeeWizard
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents lnkGenerateFile As System.Windows.Forms.LinkLabel
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents pnlGoalMapping As System.Windows.Forms.Panel
    Friend WithEvents lblField5 As System.Windows.Forms.Label
    Friend WithEvents lblField4 As System.Windows.Forms.Label
    Friend WithEvents lblField3 As System.Windows.Forms.Label
    Friend WithEvents lblField2 As System.Windows.Forms.Label
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents cboField_1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblField1 As System.Windows.Forms.Label
    Friend WithEvents objlblASign5 As System.Windows.Forms.Label
    Friend WithEvents cboField_5 As System.Windows.Forms.ComboBox
    Friend WithEvents cboField_4 As System.Windows.Forms.ComboBox
    Friend WithEvents cboField_3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboField_2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboWeight As System.Windows.Forms.ComboBox
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents cboPerspective_Name As System.Windows.Forms.ComboBox
    Friend WithEvents lblPerspective As System.Windows.Forms.Label
    Friend WithEvents objlblASign2 As System.Windows.Forms.Label
    Friend WithEvents cboEmployee_Code As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod_Name As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents lblPeriodCode As System.Windows.Forms.Label
    Friend WithEvents objlblASign1 As System.Windows.Forms.Label
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhlogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents objlblASign10 As System.Windows.Forms.Label
    Friend WithEvents objlblASign9 As System.Windows.Forms.Label
    Friend WithEvents objlblASign8 As System.Windows.Forms.Label
    Friend WithEvents objlblASign7 As System.Windows.Forms.Label
    Friend WithEvents objlblASign6 As System.Windows.Forms.Label
    Friend WithEvents objlblASign3 As System.Windows.Forms.Label
    Friend WithEvents objlblAdditionalNote As System.Windows.Forms.Label
    Friend WithEvents chkImportWithApprovedStatus As System.Windows.Forms.CheckBox
    Friend WithEvents objlblGoalApproval As System.Windows.Forms.Label
    Friend WithEvents objlblOtherNote As System.Windows.Forms.Label
    Friend WithEvents objlblASign12 As System.Windows.Forms.Label
    Friend WithEvents cboGoalValue As System.Windows.Forms.ComboBox
    Friend WithEvents lblGoalValue As System.Windows.Forms.Label
    Friend WithEvents objlblASign11 As System.Windows.Forms.Label
    Friend WithEvents cboGoalType As System.Windows.Forms.ComboBox
    Friend WithEvents lblGoalType As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblASign13 As System.Windows.Forms.Label
    Friend WithEvents cboUomType As System.Windows.Forms.ComboBox
    Friend WithEvents lblUOMType As System.Windows.Forms.Label
    Friend WithEvents cboEndDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents cboStartDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblStDate As System.Windows.Forms.Label
End Class
