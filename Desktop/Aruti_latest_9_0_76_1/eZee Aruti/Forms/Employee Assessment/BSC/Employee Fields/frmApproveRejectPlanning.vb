﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApproveRejectPlanning

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApproveRejectPlanning"
    Private mintEmployeeId As Integer = -1
    Private mintPeriodId As Integer = -1
    Private mblnIsFinalSaved As Boolean = False
    Private mblnIsCancel As Boolean = True
    Private xDataTable As DataTable = Nothing
    'S.SANDEEP [29 JAN 2015] -- START
    Private xdtEmpCmpt As DataTable = Nothing
    'S.SANDEEP [29 JAN 2015] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal strName As String, _
                                  ByVal intEmployeeId As Integer, _
                                  ByVal intPeriodId As Integer, _
                                  ByVal iFinal As Boolean, ByVal iDSource As DataTable, _
                                  Optional ByVal idtEmpCmpt As DataTable = Nothing) As Boolean 'S.SANDEEP [29 JAN 2015] -- START {} -- END

        Try
            objlblCaption.Text = strName
            mintEmployeeId = intEmployeeId
            mintPeriodId = intPeriodId

            If iFinal = True Then btnFinalSave.Enabled = False

            xDataTable = iDSource.Copy

            Call Fill_Grid(iDSource)

            'S.SANDEEP [29 JAN 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = False Then
                tbcPlanningView.TabPages.Remove(tabpCompetencies)
            Else
                xdtEmpCmpt = idtEmpCmpt.Copy
                Fill_Competancy_List(xdtEmpCmpt)
            End If
            'S.SANDEEP [29 JAN 2015] -- END

            Me.ShowDialog()
            Return Not mblnIsCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_Grid(ByVal iDataSource As DataTable)
        Try
            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty
            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split(CChar("|"))
            End If
            Dim objFMaster As New clsAssess_Field_Master(True)

            For Each dCol As DataColumn In iDataSource.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvData.Columns.Contains(iColName) = True Then Continue For
                Dim dgvCol As New DataGridViewTextBoxColumn()
                dgvCol.Name = iColName
                dgvCol.Width = 120
                dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.ReadOnly = True
                dgvCol.DataPropertyName = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If iDataSource.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, CInt(iDataSource.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, iDataSource.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If
                If dCol.ColumnName = "Emp" Then
                    dgvCol.Visible = False
                End If
                If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    If dCol.ColumnName.StartsWith("Owr") Then
                        dgvCol.Visible = False
                    End If
                End If

                'S.SANDEEP [01 JUL 2015] -- START
                If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [01 JUL 2015] -- END

                dgvData.Columns.Add(dgvCol)
            Next
            dgvData.DataSource = iDataSource
            dgvData.ColumnHeadersHeight = 35
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Get_Assessor(ByRef iAssessorEmpId As Integer, ByRef sAssessorName As String) As Integer
        Dim iAssessor As Integer = 0
        Try
            Dim dLst As New DataSet

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'Dim objBSC As New clsBSC_Analysis_Master
            'dLst = objBSC.getAssessorComboList("List", False, False, User._Object._Userunkid)
            Dim objEvalutionMst As New clsevaluation_analysis_master
            dLst = objEvalutionMst.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        True, False, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False, mintEmployeeId) 'S.SANDEEP [27 DEC 2016] -- START {clsAssessor.enARVisibilityTypeId.VISIBLE}-- END
            'Shani(01-MAR-2016) -- End

            If dLst.Tables(0).Rows.Count > 0 Then
                iAssessor = CInt(dLst.Tables(0).Rows(0).Item("Id"))
                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'iAssessorEmpId = objBSC.GetAssessorEmpId(iAssessor)
                iAssessorEmpId = objEvalutionMst.GetAssessorEmpId(iAssessor)
                'Shani(01-MAR-2016) -- End
                sAssessorName = CStr(dLst.Tables(0).Rows(0).Item("Name"))
            End If

            objEvalutionMst = Nothing 'Shani(01-MAR-2016) --> objBSC = Nothing

            Return iAssessor

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Assessor", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP [29 JAN 2015] -- START
    Private Sub Fill_Competancy_List(ByVal xTable As DataTable)
        Try
            For Each dRow As DataRow In xTable.Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dRow.Item("competencies").ToString
                lvItem.SubItems.Add(dRow.Item("assigned_weight").ToString)
                'S.SANDEEP |10-AUG-2021| -- START
                'If CBool(dRow.Item("isfinal")) = True Or CInt(dRow.Item("opstatusid")) = enObjective_Status.FINAL_SAVE Then
                '    lvItem.ForeColor = Color.Blue
                'ElseIf CInt(dRow.Item("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                '    lvItem.ForeColor = Color.Green
                'End If
                If CBool(dRow.Item("isfinal")) = True Or CInt(dRow.Item("opstatusid")) = enObjective_Status.FINAL_SAVE Then
                    If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                        If ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE) Then
                            lvItem.ForeColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                        Else
                    lvItem.ForeColor = Color.Blue
                        End If
                    Else
                    lvItem.ForeColor = Color.Blue
                    End If
                ElseIf CInt(dRow.Item("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                    If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                        If ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.SUBMIT_APPROVAL) Then
                            lvItem.ForeColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                        Else
                            lvItem.ForeColor = Color.Green
                        End If
                    Else
                    lvItem.ForeColor = Color.Green
                End If
                End If
                'S.SANDEEP |10-AUG-2021| -- END
                lvAssignedCompetencies.Items.Add(lvItem)
            Next
            If lvAssignedCompetencies.Items.Count > 10 Then
                colhCompetencies.Width = 760 - 20
            Else
                colhCompetencies.Width = 760
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Competancy_List", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [29 JAN 2015] -- END

#End Region

#Region " Form's Events "

    Private Sub frmApproveRejectPlanning_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveRejectPlanning_Load", mstrModuleName)
        Finally
        End Try
    End Sub
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        iAssessorMasterId = 0 : iAssessorEmpId = 0
        Try
            'S.SANDEEP |28-MAR-2019| -- START
            If txtComments.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Comments are mandatory information. Please provide comments in order to continue"), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |28-MAR-2019| -- END

            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            If iAssessorMasterId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You are about to Approve this BSC Planning for the selected employee, if you follow this process then this employee won't be able to modify or delete this transaction.." & vbCrLf & "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub


            Dim objEmpField1 As New clsassess_empfield1_master
            Dim xRow() As DataRow = Nothing
            If xDataTable IsNot Nothing Then
                xRow = xDataTable.Select("OwnerIds <> ''")
            End If
            Dim blnFlag As Boolean = False
            If xRow.Length > 0 Then
                'S.SANDEEP [23 APR 2015] -- START
                'blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This approving process will transfer the goals to your subordinate employee(s), Those employee(s) who has already committed their goals will not be affected by this process. Do you wish to continue?"), _
                                   CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If

                'Pinkal (07-Dec-2020) -- Start
                'Enhancement NMB - Cascading PM UAT Comments.
                'blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow)
                blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
                'Pinkal (07-Dec-2020) -- End

                'S.SANDEEP [23 APR 2015] -- END
            End If

            'S.SANDEEP |08-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
            'Dim objGrp As New clsGroup_Master(True)
            'If objGrp._Groupname.ToString.ToUpper() = "NMB PLC" Then
            '    If xRow.Length <= 0 Then
            '        xRow = xDataTable.DefaultView.ToTable(True, "employeeunkid").Select("employeeunkid >0 ")

            '        'Pinkal (07-Dec-2020) -- Start
            '        'Enhancement NMB - Cascading PM UAT Comments.
            '        'blnFlag = objassess_Empfield1Mst.Transfer_Goals_Subordinate_Employee(xRow)
            '        blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, True)
            '        'Pinkal (07-Dec-2020) -- End


            '    End If
            'End If
            'objGrp = Nothing
            'S.SANDEEP |08-DEC-2020| -- END

            Dim objStatus As New clsassess_empstatus_tran
            'S.SANDEEP [07-OCT-2017] -- START
            'ISSUE/ENHANCEMENT : IMPORT GOALS
            'objStatus._Assessoremployeeunkid = iAssessorMasterId
            'objStatus._Assessormasterunkid = iAssessorEmpId
            objStatus._Assessoremployeeunkid = iAssessorEmpId
            objStatus._Assessormasterunkid = iAssessorMasterId
            'S.SANDEEP [07-OCT-2017] -- END
            objStatus._Commtents = txtComments.Text
            objStatus._Employeeunkid = mintEmployeeId
            objStatus._Periodunkid = mintPeriodId
            objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatus._Statustypeid = enObjective_Status.FINAL_SAVE
            objStatus._Isunlock = False
            objStatus._Userunkid = User._Object._Userunkid
            If objStatus.Insert(, ConfigParameter._Object._Self_Assign_Competencies) = False Then 'S.SANDEEP [29 JAN 2015] -- START {_Self_Assign_Competencies} -- END
                eZeeMsgBox.Show(objStatus._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEmpField1.Send_Notification_Employee(mintEmployeeId, sAssessorName, "", True, False)
                objEmpField1.Send_Notification_Employee(mintEmployeeId, sAssessorName, "", True, False, Company._Object._Companyunkid)
                'Sohail (30 Nov 2017) -- End
                objEmpField1 = Nothing
            End If
            objStatus = Nothing

            mblnIsCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFinalSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOpen_Changes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen_Changes.Click
        Try
            'S.SANDEEP |28-MAR-2019| -- START
            If txtComments.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Comments are mandatory information. Please provide comments in order to continue"), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |28-MAR-2019| -- END

            Dim iAssessorMasterId, iAssessorEmpId As Integer
            Dim sAssessorName As String = ""

            iAssessorMasterId = 0 : iAssessorEmpId = 0

            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)

            If iAssessorMasterId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You are about to Disapprove this BSC Planning for the selected employee, Due to this employee will be able to modify or delete this." & vbCrLf & "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objStatus As New clsassess_empstatus_tran
            objStatus._Assessoremployeeunkid = iAssessorMasterId
            objStatus._Assessormasterunkid = iAssessorEmpId
            objStatus._Commtents = txtComments.Text
            objStatus._Employeeunkid = mintEmployeeId
            objStatus._Periodunkid = mintPeriodId
            objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatus._Statustypeid = enObjective_Status.OPEN_CHANGES
            objStatus._Isunlock = False
            objStatus._Userunkid = User._Object._Userunkid

            If objStatus.Insert(, ConfigParameter._Object._Self_Assign_Competencies) = False Then 'S.SANDEEP [29 JAN 2015] -- START {_Self_Assign_Competencies} -- END
                eZeeMsgBox.Show(objStatus._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim objEmpField1 As New clsassess_empfield1_master
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEmpField1.Send_Notification_Employee(mintEmployeeId, sAssessorName, txtComments.Text, False, False)
                objEmpField1.Send_Notification_Employee(mintEmployeeId, sAssessorName, txtComments.Text, False, False, Company._Object._Companyunkid)
                'Sohail (30 Nov 2017) -- End
                objEmpField1 = Nothing
            End If
            objStatus = Nothing
            mblnIsCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpen_Changes_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'S.SANDEEP [29 JAN 2015] -- START
#Region " DataGrid Event(s) "

    Private Sub dgvData_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                'S.SANDEEP |10-AUG-2021| -- START
                'If CBool(dgvRow.Cells("objisfinal").Value) = True Then
                '    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                'ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
                '    dgvRow.DefaultCellStyle.ForeColor = Color.Green
                'End If
                If CBool(dgvRow.Cells("objisfinal").Value) = True Then
                    If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                        If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                            dgvRow.DefaultCellStyle.ForeColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                        Else
                    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                        End If
                    Else
                    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                    End If
                ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
                    If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                        If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                            dgvRow.DefaultCellStyle.ForeColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                        Else
                    dgvRow.DefaultCellStyle.ForeColor = Color.Green
                End If
                    Else
                        dgvRow.DefaultCellStyle.ForeColor = Color.Green
                    End If
                End If
                'S.SANDEEP |10-AUG-2021| -- END
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [29 JAN 2015] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnFinalSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFinalSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnOpen_Changes.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpen_Changes.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblComment.Text = Language._Object.getCaption(Me.lblComment.Name, Me.lblComment.Text)
			Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.Name, Me.btnFinalSave.Text)
			Me.btnOpen_Changes.Text = Language._Object.getCaption(Me.btnOpen_Changes.Name, Me.btnOpen_Changes.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tabpBSC.Text = Language._Object.getCaption(Me.tabpBSC.Name, Me.tabpBSC.Text)
            Me.tabpCompetencies.Text = Language._Object.getCaption(Me.tabpCompetencies.Name, Me.tabpCompetencies.Text)
            Me.colhCompetencies.Text = Language._Object.getCaption(CStr(Me.colhCompetencies.Tag), Me.colhCompetencies.Text)
            Me.colhWeight.Text = Language._Object.getCaption(CStr(Me.colhWeight.Tag), Me.colhWeight.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, You cannot perform this operation there is no Assessor mapped with your account.")
			Language.setMessage(mstrModuleName, 2, "You are about to Approve this BSC Planning for the selected employee, if you follow this process then this employee won't be able to modify or delete this transaction.." & vbCrLf & "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 3, "You are about to Disapprove this BSC Planning for the selected employee, Due to this employee will be able to modify or delete this." & vbCrLf & "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 4, "This approving process will transfer the goals to your subordinate employee(s), Those employee(s) who has already committed their goals will not be affected by this process. Do you wish to continue?")
            Language.setMessage(mstrModuleName, 5, "Sorry, Comments are mandatory information. Please provide comments in order to continue")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class