﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUnlockEmployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUnlockEmployee))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objlblPeriodDates = New System.Windows.Forms.LinkLabel
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.gbOperation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudNextDays = New System.Windows.Forms.NumericUpDown
        Me.lblSetDays = New System.Windows.Forms.Label
        Me.lnkUpdate = New System.Windows.Forms.LinkLabel
        Me.radApplyToAll = New System.Windows.Forms.RadioButton
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.radApplyTochecked = New System.Windows.Forms.RadioButton
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLockDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGraceDays = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNextLockDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLockUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objlblValue = New System.Windows.Forms.LinkLabel
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblLockType = New System.Windows.Forms.Label
        Me.cboLockType = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblUnlockPeriod = New System.Windows.Forms.Label
        Me.cboUnlockPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbgWorker = New System.ComponentModel.BackgroundWorker
        Me.pnlMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbOperation.SuspendLayout()
        CType(Me.nudNextDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.Panel1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(687, 498)
        Me.pnlMain.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objlblPeriodDates)
        Me.Panel1.Controls.Add(Me.txtRemark)
        Me.Panel1.Controls.Add(Me.lblRemark)
        Me.Panel1.Controls.Add(Me.gbOperation)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.Panel1.Controls.Add(Me.gbFilterCriteria)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(687, 498)
        Me.Panel1.TabIndex = 481
        '
        'objlblPeriodDates
        '
        Me.objlblPeriodDates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblPeriodDates.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlblPeriodDates.Location = New System.Drawing.Point(7, 100)
        Me.objlblPeriodDates.Name = "objlblPeriodDates"
        Me.objlblPeriodDates.Size = New System.Drawing.Size(345, 17)
        Me.objlblPeriodDates.TabIndex = 502
        Me.objlblPeriodDates.TabStop = True
        Me.objlblPeriodDates.Text = "########"
        Me.objlblPeriodDates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Location = New System.Drawing.Point(106, 381)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(578, 59)
        Me.txtRemark.TabIndex = 501
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(14, 384)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(86, 17)
        Me.lblRemark.TabIndex = 500
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbOperation
        '
        Me.gbOperation.BorderColor = System.Drawing.Color.Black
        Me.gbOperation.Checked = False
        Me.gbOperation.CollapseAllExceptThis = False
        Me.gbOperation.CollapsedHoverImage = Nothing
        Me.gbOperation.CollapsedNormalImage = Nothing
        Me.gbOperation.CollapsedPressedImage = Nothing
        Me.gbOperation.CollapseOnLoad = False
        Me.gbOperation.Controls.Add(Me.nudNextDays)
        Me.gbOperation.Controls.Add(Me.lblSetDays)
        Me.gbOperation.Controls.Add(Me.lnkUpdate)
        Me.gbOperation.Controls.Add(Me.radApplyToAll)
        Me.gbOperation.Controls.Add(Me.objStLine1)
        Me.gbOperation.Controls.Add(Me.radApplyTochecked)
        Me.gbOperation.ExpandedHoverImage = Nothing
        Me.gbOperation.ExpandedNormalImage = Nothing
        Me.gbOperation.ExpandedPressedImage = Nothing
        Me.gbOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOperation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOperation.HeaderHeight = 25
        Me.gbOperation.HeaderMessage = ""
        Me.gbOperation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbOperation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOperation.HeightOnCollapse = 0
        Me.gbOperation.LeftTextSpace = 0
        Me.gbOperation.Location = New System.Drawing.Point(356, 3)
        Me.gbOperation.Name = "gbOperation"
        Me.gbOperation.OpenHeight = 300
        Me.gbOperation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOperation.ShowBorder = True
        Me.gbOperation.ShowCheckBox = False
        Me.gbOperation.ShowCollapseButton = False
        Me.gbOperation.ShowDefaultBorderColor = True
        Me.gbOperation.ShowDownButton = False
        Me.gbOperation.ShowHeader = True
        Me.gbOperation.Size = New System.Drawing.Size(328, 94)
        Me.gbOperation.TabIndex = 496
        Me.gbOperation.Temp = 0
        Me.gbOperation.Text = "Set Information"
        Me.gbOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudNextDays
        '
        Me.nudNextDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudNextDays.Location = New System.Drawing.Point(11, 57)
        Me.nudNextDays.Name = "nudNextDays"
        Me.nudNextDays.Size = New System.Drawing.Size(61, 21)
        Me.nudNextDays.TabIndex = 497
        Me.nudNextDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblSetDays
        '
        Me.lblSetDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSetDays.Location = New System.Drawing.Point(8, 34)
        Me.lblSetDays.Name = "lblSetDays"
        Me.lblSetDays.Size = New System.Drawing.Size(132, 17)
        Me.lblSetDays.TabIndex = 498
        Me.lblSetDays.Text = "Set Next Lock Days"
        Me.lblSetDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkUpdate
        '
        Me.lnkUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkUpdate.AutoSize = True
        Me.lnkUpdate.BackColor = System.Drawing.Color.Transparent
        Me.lnkUpdate.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkUpdate.Location = New System.Drawing.Point(273, 6)
        Me.lnkUpdate.Name = "lnkUpdate"
        Me.lnkUpdate.Size = New System.Drawing.Size(48, 13)
        Me.lnkUpdate.TabIndex = 8
        Me.lnkUpdate.TabStop = True
        Me.lnkUpdate.Text = "Update"
        '
        'radApplyToAll
        '
        Me.radApplyToAll.Location = New System.Drawing.Point(178, 60)
        Me.radApplyToAll.Name = "radApplyToAll"
        Me.radApplyToAll.Size = New System.Drawing.Size(136, 17)
        Me.radApplyToAll.TabIndex = 9
        Me.radApplyToAll.TabStop = True
        Me.radApplyToAll.Text = "Apply To All"
        Me.radApplyToAll.UseVisualStyleBackColor = True
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(160, 24)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(3, 69)
        Me.objStLine1.TabIndex = 9
        '
        'radApplyTochecked
        '
        Me.radApplyTochecked.Location = New System.Drawing.Point(178, 34)
        Me.radApplyTochecked.Name = "radApplyTochecked"
        Me.radApplyTochecked.Size = New System.Drawing.Size(136, 17)
        Me.radApplyTochecked.TabIndex = 8
        Me.radApplyTochecked.TabStop = True
        Me.radApplyTochecked.Text = "Apply To Checked"
        Me.radApplyTochecked.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.chkSelectAll)
        Me.Panel2.Controls.Add(Me.dgvData)
        Me.Panel2.Location = New System.Drawing.Point(3, 122)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(681, 253)
        Me.Panel2.TabIndex = 480
        '
        'chkSelectAll
        '
        Me.chkSelectAll.BackColor = System.Drawing.Color.Transparent
        Me.chkSelectAll.Location = New System.Drawing.Point(7, 6)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(13, 13)
        Me.chkSelectAll.TabIndex = 7
        Me.chkSelectAll.UseVisualStyleBackColor = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhCheck, Me.colhEmployee, Me.dgcolhLockDate, Me.dgcolhGraceDays, Me.dgcolhNextLockDate, Me.objdgcolhLockUnkid, Me.objdgcolhEmpId, Me.dgcolhMessage})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(681, 253)
        Me.dgvData.TabIndex = 6
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Frozen = True
        Me.objcolhCheck.HeaderText = ""
        Me.objcolhCheck.Name = "objcolhCheck"
        Me.objcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhCheck.Width = 25
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        Me.colhEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhLockDate
        '
        Me.dgcolhLockDate.HeaderText = "Locked Date"
        Me.dgcolhLockDate.Name = "dgcolhLockDate"
        Me.dgcolhLockDate.ReadOnly = True
        Me.dgcolhLockDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGraceDays
        '
        Me.dgcolhGraceDays.HeaderText = "Days"
        Me.dgcolhGraceDays.Name = "dgcolhGraceDays"
        Me.dgcolhGraceDays.ReadOnly = True
        Me.dgcolhGraceDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhGraceDays.Width = 50
        '
        'dgcolhNextLockDate
        '
        Me.dgcolhNextLockDate.HeaderText = "Next Lock Date"
        Me.dgcolhNextLockDate.Name = "dgcolhNextLockDate"
        Me.dgcolhNextLockDate.ReadOnly = True
        Me.dgcolhNextLockDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhLockUnkid
        '
        Me.objdgcolhLockUnkid.HeaderText = "objdgcolhLockUnkid"
        Me.objdgcolhLockUnkid.Name = "objdgcolhLockUnkid"
        Me.objdgcolhLockUnkid.ReadOnly = True
        Me.objdgcolhLockUnkid.Visible = False
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.Visible = False
        '
        'dgcolhMessage
        '
        Me.dgcolhMessage.HeaderText = "Message"
        Me.dgcolhMessage.Name = "dgcolhMessage"
        Me.dgcolhMessage.ReadOnly = True
        Me.dgcolhMessage.Width = 130
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objlblValue)
        Me.EZeeFooter1.Controls.Add(Me.btnProcess)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 443)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(687, 55)
        Me.EZeeFooter1.TabIndex = 479
        '
        'objlblValue
        '
        Me.objlblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblValue.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlblValue.Location = New System.Drawing.Point(8, 19)
        Me.objlblValue.Name = "objlblValue"
        Me.objlblValue.Size = New System.Drawing.Size(295, 17)
        Me.objlblValue.TabIndex = 3
        Me.objlblValue.TabStop = True
        Me.objlblValue.Text = "########"
        Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(475, 13)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(97, 30)
        Me.btnProcess.TabIndex = 9
        Me.btnProcess.Text = "&Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(578, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblLockType)
        Me.gbFilterCriteria.Controls.Add(Me.cboLockType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnlockPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnlockPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(349, 94)
        Me.gbFilterCriteria.TabIndex = 478
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLockType
        '
        Me.lblLockType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLockType.Location = New System.Drawing.Point(8, 64)
        Me.lblLockType.Name = "lblLockType"
        Me.lblLockType.Size = New System.Drawing.Size(92, 17)
        Me.lblLockType.TabIndex = 485
        Me.lblLockType.Text = "Select Lock Type"
        Me.lblLockType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLockType
        '
        Me.cboLockType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLockType.DropDownWidth = 250
        Me.cboLockType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLockType.FormattingEnabled = True
        Me.cboLockType.Location = New System.Drawing.Point(106, 62)
        Me.cboLockType.Name = "cboLockType"
        Me.cboLockType.Size = New System.Drawing.Size(232, 21)
        Me.cboLockType.TabIndex = 486
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(323, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'lblUnlockPeriod
        '
        Me.lblUnlockPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnlockPeriod.Location = New System.Drawing.Point(8, 37)
        Me.lblUnlockPeriod.Name = "lblUnlockPeriod"
        Me.lblUnlockPeriod.Size = New System.Drawing.Size(92, 17)
        Me.lblUnlockPeriod.TabIndex = 476
        Me.lblUnlockPeriod.Text = "Select Period"
        Me.lblUnlockPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnlockPeriod
        '
        Me.cboUnlockPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnlockPeriod.DropDownWidth = 250
        Me.cboUnlockPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnlockPeriod.FormattingEnabled = True
        Me.cboUnlockPeriod.Location = New System.Drawing.Point(106, 35)
        Me.cboUnlockPeriod.Name = "cboUnlockPeriod"
        Me.cboUnlockPeriod.Size = New System.Drawing.Size(232, 21)
        Me.cboUnlockPeriod.TabIndex = 477
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(297, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'objbgWorker
        '
        Me.objbgWorker.WorkerReportsProgress = True
        Me.objbgWorker.WorkerSupportsCancellation = True
        '
        'frmUnlockEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 498)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUnlockEmployee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Unlock Employee(s)"
        Me.pnlMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbOperation.ResumeLayout(False)
        Me.gbOperation.PerformLayout()
        CType(Me.nudNextDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objbgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objlblValue As System.Windows.Forms.LinkLabel
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblUnlockPeriod As System.Windows.Forms.Label
    Friend WithEvents cboUnlockPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblLockType As System.Windows.Forms.Label
    Friend WithEvents cboLockType As System.Windows.Forms.ComboBox
    Friend WithEvents gbOperation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkUpdate As System.Windows.Forms.LinkLabel
    Friend WithEvents radApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents radApplyTochecked As System.Windows.Forms.RadioButton
    Friend WithEvents lblSetDays As System.Windows.Forms.Label
    Friend WithEvents nudNextDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents objcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLockDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGraceDays As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNextLockDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLockUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblPeriodDates As System.Windows.Forms.LinkLabel
End Class
