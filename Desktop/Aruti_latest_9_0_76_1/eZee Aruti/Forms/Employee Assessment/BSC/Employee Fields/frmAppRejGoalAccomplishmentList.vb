﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region


Public Class frmAppRejGoalAccomplishmentList

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmAppRejGoalAccomplishmentList"
    Private mdtFinal As DataTable = Nothing

    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    'Private mstrApprovedRemark As String = ""
    'Private mstrRejectedRemark As String = ""
    'Private dtFinal As DataTable = Nothing
    'Shani(24-JAN-2017) -- End

#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Try
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With



            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

            dsList = objMData.Get_Goal_Accomplishement_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsList As DataSet = Nothing
        Try

            'Shani(24-JAN-2017) -- Start
            'Enhancement : 
            dsList = (New clsassess_empfield1_master).GetAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), _
                                                                                    CInt(cboPeriod.SelectedValue), _
                                                                                    CInt(cboStatus.SelectedValue), , _
                                                                                    "List")

            If dsList Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Record not found in system."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            mdtFinal = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            'Shani(24-JAN-2017) -- End

            If mdtFinal Is Nothing Then Exit Sub

            'Shani(24-JAN-2017) -- Start
            'Enhancement : 
            'dgvData.AutoGenerateColumns = False
            'Dim iColName As String = String.Empty
            'Dim dgvCol As DataGridViewColumn = Nothing
            'For Each dCol As DataColumn In mdtFinal.Columns
            '    iColName = "" : iColName = "obj" & dCol.ColumnName
            '    If dgvData.Columns.Contains(iColName) = True Then Continue For
            '    dgvCol = New DataGridViewTextBoxColumn()
            '    dgvCol.Name = iColName
            '    dgvCol.Width = 160
            '    dgvCol.SortMode = DataGridViewColumnSortMode.Automatic
            '    dgvCol.ReadOnly = True
            '    dgvCol.DataPropertyName = dCol.ColumnName
            '    dgvCol.HeaderText = dCol.Caption
            '    dgvCol.Visible = False
            '    Select Case dCol.ColumnName
            '        Case "Field1", "Field2", "Field3", "Field4", "Field5"
            '            If dCol.ColumnName = CStr("Field" & intLinkedFld.ToString) Then
            '                dgvCol.Visible = True
            '            End If
            '        Case "CStatus", "pct_complete", "vuGoalAccomplishmentStatus"
            '            dgvCol.Visible = True
            '    End Select
            '    dgvData.Columns.Add(dgvCol)
            'Next
            'dgvData.DataSource = mdtFinal

            ''Call SetVisibility()

            'objdgcolhApprove.DefaultCellStyle.SelectionBackColor = Color.White
            'objdgcolhApprove.DefaultCellStyle.SelectionForeColor = Color.White

            'objdgcolhReject.DefaultCellStyle.SelectionBackColor = Color.White
            'objdgcolhReject.DefaultCellStyle.SelectionForeColor = Color.White

            'For Each dgvRow As DataGridViewRow In dgvData.Rows
            '    If CBool(dgvRow.Cells("objisfinal").Value) = True Then
            '        dgvRow.DefaultCellStyle.ForeColor = Color.Blue
            '        If dgvData.Columns("objvuRemark").Visible = False Then dgvData.Columns("objvuRemark").Visible = True
            '        objdgcolhApprove.Visible = True
            '        objdgcolhReject.Visible = True
            '    ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
            '        dgvRow.DefaultCellStyle.ForeColor = Color.Green
            '        objdgcolhApprove.Visible = False
            '        objdgcolhReject.Visible = False
            '    Else
            '        objdgcolhApprove.Visible = False
            '        objdgcolhReject.Visible = False
            '    End If
            '    dgvRow.Cells("objvuProgress").Style.SelectionForeColor = Color.White
            '    dgvRow.Cells("objvuProgress").Style.SelectionBackColor = Color.White
            'Next

            'Get_Map_FieldName
            Dim strFiledName As String = (New clsAssess_Field_Mapping).Get_Map_FieldName(CInt(cboPeriod.SelectedValue))

            dgvcolhFiledData.HeaderText = strFiledName
            dgvData.AutoGenerateColumns = False
            objchkSelect.DataPropertyName = "ischeck"
            dgvcolhFiledData.DataPropertyName = "field_data"
            dgvcolhStatus.DataPropertyName = "Goal_status"
            dgvcolhComplatePer.DataPropertyName = "per_comp"
            dgvcolhProgressRemark.DataPropertyName = "Goal_Remark"
            dgvcolhAccomplishedStatus.DataPropertyName = "accomplished_status"
            objdgvcolhAccomplishmentStatusid.DataPropertyName = "Accomplished_statusId"
            objdgvcolhfiledunkid.DataPropertyName = "empfieldunkid"
            objdgvcolhfiledUpdatetranunkid.DataPropertyName = "empupdatetranunkid"
            objdgvcolhIsGrp.DataPropertyName = "IsGrp"
            dgvData.DataSource = mdtFinal

            Call SetGridColor()
            'For Each dgvRow As DataGridViewRow In dgvData.Rows
            '    If CBool(dgvRow.Cells("objisfinal").Value) = True Then
            '        dgvRow.DefaultCellStyle.ForeColor = Color.Blue
            '        If dgvData.Columns("objvuRemark").Visible = False Then dgvData.Columns("objvuRemark").Visible = True
            '        objdgcolhApprove.Visible = True
            '        objdgcolhReject.Visible = True
            '    ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
            '        dgvRow.DefaultCellStyle.ForeColor = Color.Green
            '        objdgcolhApprove.Visible = False
            '        objdgcolhReject.Visible = False
            '    Else
            '        objdgcolhApprove.Visible = False
            '        objdgcolhReject.Visible = False
            '    End If
            '    dgvRow.Cells("objvuProgress").Style.SelectionForeColor = Color.White
            '    dgvRow.Cells("objvuProgress").Style.SelectionBackColor = Color.White
            'Next
            'Shani(24-JAN-2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Get_Assessor(ByVal mintemployeeunkid As Integer, ByRef iAssessorEmpId As Integer, ByRef sAssessorName As String) As Integer
        Dim iAssessor As Integer = 0
        Try
            Dim dLst As New DataSet
            Dim objEvalutionMst As New clsevaluation_analysis_master

            dLst = objEvalutionMst.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        True, False, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False, mintemployeeunkid) 'S.SANDEEP [27 DEC 2016] -- START {clsAssessor.enARVisibilityTypeId.VISIBLE}-- END



            If dLst.Tables(0).Rows.Count > 0 Then
                iAssessor = CInt(dLst.Tables(0).Rows(0).Item("Id"))
                iAssessorEmpId = objEvalutionMst.GetAssessorEmpId(iAssessor)
                sAssessorName = CStr(dLst.Tables(0).Rows(0).Item("Name"))
            End If

            objEvalutionMst = Nothing

            Return iAssessor

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Assessor", mstrModuleName)
        End Try
    End Function

    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    Private Sub SetGridColor()
        Try

            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim pCell As New clsMergeCell
            'Dim img As New Bitmap(15, 15, 15, Imaging.PixelFormat.Format48bppRgb, System.IntPtr.Size)

            For Each dgRow As DataGridViewRow In dgvData.Rows
                If CBool(dgRow.Cells(objdgvcolhIsGrp.Index).Value) = True Then
                    dgRow.DefaultCellStyle = dgvcsHeader
                    dgRow.Cells(objchkSelect.Index).ReadOnly = True
                    dgRow.Cells(dgvcolhComplatePer.Index).Style.ForeColor = Color.Gray
                    dgRow.Cells(dgvcolhComplatePer.Index).Style.SelectionForeColor = Color.Gray
                    pCell.MakeMerge(dgvData, dgRow.Index, objchkSelect.Index, objchkSelect.Index, Color.Gray, Color.Gray, "", "", picStayView.Image)
                Else
                    dgRow.Cells(dgvcolhStatus.Index).Value = ""
                    If CInt(dgRow.Cells(objdgvcolhAccomplishmentStatusid.Index).Value) <> clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending Then
                        pCell.MakeMerge(dgvData, dgRow.Index, objchkSelect.Index, objchkSelect.Index, Color.White, Color.White, "", "", picStayView.Image)
                        dgRow.Cells(objchkSelect.Index).ReadOnly = True
                    End If
                End If
            Next
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub
    'Shani(24-JAN-2017) -- End

#End Region

#Region " Form's Event "

    Private Sub frmAppRejGoalAccomplishmentList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Call Generate_MenuItems()
            'Call SetFormViewSetting()
            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppRejGoalAccomplishmentList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim iSearch As String = String.Empty
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
                   CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee and Period are mandatory information. Please select Employee and Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If (New clsAssess_Field_Master).IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If (New clsAssess_Field_Mapping).isExist(CInt(cboPeriod.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Shani(24-JAN-2017) -- Start
            'Enhancement : 
            'dtFinal = (New clsassess_empfield1_master).GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", , True)

            'If CInt(cboStatus.SelectedValue) > 0 Then
            '    iSearch &= "AND vuGoalAccomplishmentStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
            'End If

            'If iSearch.Trim.Length > 0 Then
            '    iSearch = iSearch.Substring(3)
            '    mdtFinal = New DataView(dtFinal, iSearch, "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            'Else
            '    mdtFinal = New DataView(dtFinal, "", "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            'End If
            'Shani(24-JAN-2017) -- End
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboStatus.SelectedValue = 0
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmp.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = cboEmployee.DataSource
                If .DisplayDialog = True Then
                    cboEmployee.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmp_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Shani(24-JAN-2017) -- Start
    'Enhancement : 


    Private Sub btnAccomplishedApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccomplishedApprove.Click
        Dim strMessage As String = ""
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        Dim objEUpdateProgress As New clsassess_empupdate_tran
        Try
            Me.Cursor = Cursors.WaitCursor
            If mdtFinal Is Nothing Then Exit Sub

            'S.SANDEEP |30-MAR-2019| -- START
            If mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Accomplished_statusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "No pending progress update to approve."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Integer)("IsGrp") = 0).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please select") & " " & dgvcolhFiledData.HeaderText & " " & Language.getMessage(mstrModuleName, 16, "to proceed."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |30-MAR-2019| -- END

            If txtRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Remark is mendtroy inforamation,please enter remark first."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            iAssessorMasterId = 0 : iAssessorEmpId = 0
            iAssessorMasterId = Get_Assessor(CInt(cboEmployee.SelectedValue), iAssessorEmpId, sAssessorName)
            If iAssessorMasterId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |25-MAR-2019| -- START
            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "are you sure appoved this goal accomplishment?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to approve this progress update?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub
            'S.SANDEEP |25-MAR-2019| -- END

            For Each dRow As DataRow In mdtFinal.Select("ischeck = TRUE AND IsGrp = FALSE")
                If CInt(dRow.Item("empupdatetranunkid")) <= 0 Then Exit Sub
                Dim mintEmpUpdateTranunkid As Integer = CInt(dRow.Item("empupdatetranunkid"))

                If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
                    dgvData.Rows(mdtFinal.Rows.IndexOf(dRow)).DefaultCellStyle.SelectionBackColor = Color.Red
                    strMessage &= " * " & Language.getMessage(mstrModuleName, 6, "Sorry you can not approved this goal,reason goal alrady approved status") & vbCrLf
                End If

                If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
                    dgvData.Rows(mdtFinal.Rows.IndexOf(dRow)).DefaultCellStyle.SelectionBackColor = Color.Red
                    strMessage &= " * " & Language.getMessage(mstrModuleName, 7, "Sorry you can not approved this goal,reason goal alrady rejected status") & vbCrLf
                End If

                objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
                objEUpdateProgress._Pct_Completed = CDec(dRow.Item("per_comp"))
                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
                objEUpdateProgress._GoalAccomplishmentRemark = txtRemark.Text
                If objEUpdateProgress.Update() = False Then
                    dgvData.Rows(mdtFinal.Rows.IndexOf(dRow)).DefaultCellStyle.BackColor = Color.Red
                    strMessage &= " * " & objEUpdateProgress._Message & vbCrLf
                End If
            Next


            Dim eList = _
            mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True AndAlso x.Field(Of Int32)("IsGrp") = 0). _
            Select(Function(x) x.Field(Of Integer)("empfieldunkid")).ToList()

            Dim eGoal = _
            From G In mdtFinal Where (G.Field(Of Int32)("IsGrp") = 1) Join P In eList _
            On G.Field(Of Integer)("empfieldunkid") Equals P Select G.Field(Of String)("field_data")

            If strMessage.Trim.Length > 0 Then
                eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                Exit Sub
            Else
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
                '                                              txtRemark.Text, True, cboPeriod.Text, enLogin_Mode.DESKTOP, 0, _
                '                                              User._Object._Userunkid)
                'objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
                '                                              txtRemark.Text, True, cboPeriod.Text, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, _
                '                                              User._Object._Userunkid)

                objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
                                                              txtRemark.Text, True, cboPeriod.Text, Company._Object._Companyunkid, eGoal.ToList(), enLogin_Mode.DESKTOP, 0, _
                                                              User._Object._Userunkid)

                'Sohail (30 Nov 2017) -- End

                Call Fill_Grid()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAccomplishedApprove_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnAccomplishedReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccomplishedReject.Click
        Dim strMessage As String = ""
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        Dim objEUpdateProgress As New clsassess_empupdate_tran
        Try
            Me.Cursor = Cursors.WaitCursor
            If mdtFinal Is Nothing Then Exit Sub

            'S.SANDEEP |30-MAR-2019| -- START
            If mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Accomplished_statusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "No pending progress update to reject."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Integer)("IsGrp") = 0).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please select") & " " & dgvcolhFiledData.HeaderText & " " & Language.getMessage(mstrModuleName, 16, "to proceed."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |30-MAR-2019| -- END

            If txtRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Remark is mendtroy inforamation,please enter remark first."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            iAssessorMasterId = 0 : iAssessorEmpId = 0
            iAssessorMasterId = Get_Assessor(CInt(cboEmployee.SelectedValue), iAssessorEmpId, sAssessorName)
            If iAssessorMasterId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP |25-MAR-2019| -- START
            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "are you sure appoved this goal accomplishment?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to reject this progress update?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub
            'S.SANDEEP |25-MAR-2019| -- END

            For Each dRow As DataRow In mdtFinal.Select("ischeck = TRUE AND IsGrp = FALSE")
                If CInt(dRow.Item("empupdatetranunkid")) <= 0 Then Exit Sub
                Dim mintEmpUpdateTranunkid As Integer = CInt(dRow.Item("empupdatetranunkid"))

                If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
                    dgvData.Rows(mdtFinal.Rows.IndexOf(dRow)).DefaultCellStyle.SelectionBackColor = Color.Red
                    strMessage &= " * " & Language.getMessage(mstrModuleName, 6, "Sorry you can not approved this goal,reason goal alrady approved status") & vbCrLf
                End If

                If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
                    dgvData.Rows(mdtFinal.Rows.IndexOf(dRow)).DefaultCellStyle.SelectionBackColor = Color.Red
                    strMessage &= " * " & Language.getMessage(mstrModuleName, 7, "Sorry you can not approved this goal,reason goal alrady rejected status") & vbCrLf
                End If

                objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
                objEUpdateProgress._Pct_Completed = CDec(dRow.Item("per_comp"))
                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)
                objEUpdateProgress._GoalAccomplishmentRemark = txtRemark.Text
                If objEUpdateProgress.Update() = False Then
                    dgvData.Rows(mdtFinal.Rows.IndexOf(dRow)).DefaultCellStyle.BackColor = Color.Red
                    strMessage &= " * " & objEUpdateProgress._Message & vbCrLf
                End If
            Next

            Dim eList = _
            mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True AndAlso x.Field(Of Int32)("IsGrp") = 0). _
            Select(Function(x) x.Field(Of Integer)("empfieldunkid")).ToList()

            Dim eGoal = _
            From G In mdtFinal Where (G.Field(Of Int32)("IsGrp") = 1) Join P In eList _
            On G.Field(Of Integer)("empfieldunkid") Equals P Select G.Field(Of String)("field_data")



            If strMessage.Trim.Length > 0 Then
                eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                Exit Sub
            Else
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
                '                                              txtRemark.Text, False, cboPeriod.Text, enLogin_Mode.DESKTOP, 0, _
                '                                              User._Object._Userunkid)
                'objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
                '                                              txtRemark.Text, False, cboPeriod.Text, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, _
                '                                              User._Object._Userunkid)

                objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
                                                              txtRemark.Text, False, cboPeriod.Text, Company._Object._Companyunkid, eGoal.ToList(), enLogin_Mode.DESKTOP, 0, _
                                                              User._Object._Userunkid)

                'Sohail (30 Nov 2017) -- End

                Call Fill_Grid()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAccomplishedReject_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Shani(24-JAN-2017) -- End

#End Region

#Region " Controls Events "
    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub
            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = objchkSelect.Index Then
                If CBool(dgvData.Rows(e.RowIndex).Cells(objchkSelect.Index).Value) = True Then
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).ReadOnly = False
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Style.BackColor = Color.LightGreen
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Style.SelectionBackColor = Color.LightGreen
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Style.SelectionForeColor = Color.Black
                Else
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).ReadOnly = True
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Style.BackColor = Color.White
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Style.SelectionBackColor = dgvData.Rows(e.RowIndex).Cells(dgvcolhFiledData.Index).Style.SelectionBackColor
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Style.SelectionForeColor = dgvData.Rows(e.RowIndex).Cells(dgvcolhFiledData.Index).Style.SelectionForeColor
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Value = mdtFinal.Rows(e.RowIndex)("org_per_comp")
                End If
            End If
            'If e.ColumnIndex = objdgcolhApprove.Index Then
            '    Me.Cursor = Cursors.WaitCursor
            '    If CInt(dgvData.Rows(e.RowIndex).Cells("objempupdatetranunkid").Value) <= 0 Then Exit Sub
            '    Dim mintEmpUpdateTranunkid As Integer = CInt(dgvData.Rows(e.RowIndex).Cells("objempupdatetranunkid").Value)

            '    If mdtFinal Is Nothing Then Exit Sub
            '    Dim drow() As DataRow = mdtFinal.Select("empupdatetranunkid ='" & mintEmpUpdateTranunkid & "'")
            '    If drow.Length <= 0 Then Exit Sub

            '    If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry you can not approved this goal,reason goal alrady approved status"), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry you can not approved this goal,reason goal alrady rejected status"), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "are you sure appoved this goal accomplishment?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

            '    Dim strRemark As String = ""
            '    If mstrApprovedRemark.Trim.Length <= 0 Then
            '        Dim frm As New frmRemark
            '        frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 10, "Goal Accomplishment Approve's Remarks")
            '        If frm.displayDialog(strRemark, enArutiApplicatinType.Aruti_Payroll) = False Then frm.Dispose() : Exit Sub
            '        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "are you Apply this remark all approve goals..?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then mstrApprovedRemark = strRemark
            '    Else
            '        strRemark = mstrApprovedRemark
            '    End If

            '    Dim iAssessorMasterId, iAssessorEmpId As Integer
            '    Dim sAssessorName As String = ""
            '    iAssessorMasterId = 0 : iAssessorEmpId = 0
            '    iAssessorMasterId = Get_Assessor(CInt(cboEmployee.SelectedValue), iAssessorEmpId, sAssessorName)

            '    If iAssessorMasterId <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    Dim objEUpdateProgress As New clsassess_empupdate_tran
            '    objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
            '    objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
            '    objEUpdateProgress._GoalAccomplishmentRemark = strRemark
            '    If objEUpdateProgress.Update() Then
            '        objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
            '                                                      strRemark, True, mintEmpUpdateTranunkid, enLogin_Mode.DESKTOP, 0, _
            '                                                      User._Object._Userunkid)
            '        drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved
            '        drow(0).Item("vuGoalAccomplishmentStatus") = Language.getMessage("clsMasterData", 758, "Approved")
            '        mdtFinal.AcceptChanges()
            '        Call Fill_Grid()
            '    Else
            '        eZeeMsgBox.Show(objEUpdateProgress._Message, enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'ElseIf e.ColumnIndex = objdgcolhReject.Index Then
            '    Me.Cursor = Cursors.WaitCursor

            '    If CInt(dgvData.Rows(e.RowIndex).Cells("objempupdatetranunkid").Value) <= 0 Then Exit Sub
            '    Dim mintEmpUpdateTranunkid As Integer = CInt(dgvData.Rows(e.RowIndex).Cells("objempupdatetranunkid").Value)

            '    If mdtFinal Is Nothing Then Exit Sub
            '    Dim drow() As DataRow = mdtFinal.Select("empupdatetranunkid ='" & mintEmpUpdateTranunkid & "'")
            '    If drow.Length <= 0 Then Exit Sub

            '    If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry you can not reject this goal,reason goal alrady approved status"), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you can not reject this goal,reason goal alrady rejected status"), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "are you sure rejected this goal accomplishment?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub


            '    Dim strRemark As String = ""
            '    If mstrRejectedRemark.Trim.Length <= 0 Then
            '        Dim frm As New frmRemark
            '        frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 11, "Goal Accomplishment Reject's Remarks")
            '        If frm.displayDialog(strRemark, enArutiApplicatinType.Aruti_Payroll) = False Then frm.Dispose() : Exit Sub
            '        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "are you Apply this remark all approve goals..?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then mstrRejectedRemark = strRemark
            '    Else
            '        strRemark = mstrRejectedRemark
            '    End If


            '    Dim iAssessorMasterId, iAssessorEmpId As Integer
            '    Dim sAssessorName As String = ""
            '    iAssessorMasterId = 0 : iAssessorEmpId = 0
            '    iAssessorMasterId = Get_Assessor(CInt(cboEmployee.SelectedValue), iAssessorEmpId, sAssessorName)

            '    If iAssessorMasterId <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    Dim objEUpdateProgress As New clsassess_empupdate_tran
            '    objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
            '    objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)
            '    objEUpdateProgress._GoalAccomplishmentRemark = strRemark
            '    If objEUpdateProgress.Update() Then
            '        objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, _
            '                                                      strRemark, False, mintEmpUpdateTranunkid, enLogin_Mode.DESKTOP, 0, _
            '                                                      User._Object._Userunkid)

            '        If drow.Length > 0 Then
            '            drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected
            '            drow(0).Item("vuGoalAccomplishmentStatus") = Language.getMessage("clsMasterData", 759, "Rejected")
            '        End If
            '        mdtFinal.AcceptChanges()
            '        Call Fill_Grid()
            '    Else
            '        eZeeMsgBox.Show(objEUpdateProgress._Message, enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError
        e.Cancel = True
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.ColumnIndex = dgvcolhComplatePer.Index Then
                If dgvData.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red Then
                    dgvData.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
                End If
                If CDec(dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Value) > 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
                    dgvData.Rows(e.RowIndex).Cells(dgvcolhComplatePer.Index).Value = mdtFinal.Rows(e.RowIndex)("org_per_comp")
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Shani(24-JAN-2017) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAccomplishedApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnAccomplishedApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnAccomplishedReject.GradientBackColor = GUI._ButttonBackColor
            Me.btnAccomplishedReject.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnAccomplishedApprove.Text = Language._Object.getCaption(Me.btnAccomplishedApprove.Name, Me.btnAccomplishedApprove.Text)
            Me.btnAccomplishedReject.Text = Language._Object.getCaption(Me.btnAccomplishedReject.Name, Me.btnAccomplishedReject.Text)
            Me.dgvcolhFiledData.HeaderText = Language._Object.getCaption(Me.dgvcolhFiledData.Name, Me.dgvcolhFiledData.HeaderText)
            Me.dgvcolhStatus.HeaderText = Language._Object.getCaption(Me.dgvcolhStatus.Name, Me.dgvcolhStatus.HeaderText)
            Me.dgvcolhComplatePer.HeaderText = Language._Object.getCaption(Me.dgvcolhComplatePer.Name, Me.dgvcolhComplatePer.HeaderText)
            Me.dgvcolhProgressRemark.HeaderText = Language._Object.getCaption(Me.dgvcolhProgressRemark.Name, Me.dgvcolhProgressRemark.HeaderText)
            Me.dgvcolhAccomplishedStatus.HeaderText = Language._Object.getCaption(Me.dgvcolhAccomplishedStatus.Name, Me.dgvcolhAccomplishedStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee and Period are mandatory information. Please select Employee and Period to continue.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to approve this progress update?")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to reject this progress update?")
            Language.setMessage(mstrModuleName, 4, "Sorry, No data defined for asseessment caption settings screen.")
            Language.setMessage(mstrModuleName, 5, "Sorry no field is mapped with the selected period.")
            Language.setMessage(mstrModuleName, 6, "Sorry you can not approved this goal,reason goal alrady approved status")
            Language.setMessage(mstrModuleName, 7, "Sorry you can not approved this goal,reason goal alrady rejected status")
            Language.setMessage(mstrModuleName, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account.")
            Language.setMessage(mstrModuleName, 14, "Record not found in system.")
            Language.setMessage(mstrModuleName, 15, "Please select")
            Language.setMessage(mstrModuleName, 16, "to proceed.")
            Language.setMessage(mstrModuleName, 17, "No pending progress update to approve.")
            Language.setMessage(mstrModuleName, 18, "No pending progress update to reject.")
            Language.setMessage(mstrModuleName, 27, "Remark is mendtroy inforamation,please enter remark first.")
            Language.setMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


   
End Class