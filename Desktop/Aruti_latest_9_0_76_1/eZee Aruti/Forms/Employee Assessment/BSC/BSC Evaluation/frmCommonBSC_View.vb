﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCommonBSC_View

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCommonBSC_View"
    Private mdsList As DataSet
    Private mintEmployeeId As Integer = -1
    Private mintYearId As Integer = -1
    Private mintPeriodId As Integer = -1
    Private objBSCMaster As clsBSC_Analysis_Master
    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsOnlyView As Boolean = False
    Private mblnIsFinalSaved As Boolean = False
    'S.SANDEEP [ 10 APR 2013 ] -- END

#End Region

#Region " Display Dialog "

    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function displayDialog(ByVal strName As String, _
    '                              ByVal intEmployeeId As Integer, _
    '                              ByVal intYearId As Integer, _
    '                              ByVal intPeriodId As Integer) As Boolean

    Public Function displayDialog(ByVal strName As String, _
                                  ByVal intEmployeeId As Integer, _
                                  ByVal intYearId As Integer, _
                                  ByVal intPeriodId As Integer, _
                                  ByVal blnFlag As Boolean, Optional ByVal iFinal As Boolean = False) As Boolean
        'S.SANDEEP [ 10 APR 2013 ] -- END

        Try
            objlblCaption.Text = strName
            mintEmployeeId = intEmployeeId
            mintYearId = intYearId
            mintPeriodId = intPeriodId
            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIsOnlyView = blnFlag
            If iFinal = True Then btnFinalSave.Enabled = False
            'S.SANDEEP [ 10 APR 2013 ] -- END

            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillAssessmentResult()
        Try

            RemoveHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

            If mdsList.Tables(0).Columns.Count > 0 Then
                Dim dtTable As New DataTable("Data")
                dtTable = mdsList.Tables(0).Copy
                dtTable.Rows.Clear()
                Dim mdecValue() As Decimal
                Dim mdecValueTotal() As Decimal : mdecValueTotal = New Decimal(mdsList.Tables(0).Columns.Count) {}
                Dim mdicIsNew As New Dictionary(Of Integer, String)
                For Each dtRow As DataRow In mdsList.Tables(0).Rows
                    mdecValue = New Decimal(mdsList.Tables(0).Columns.Count) {}
                    If mdicIsNew.ContainsKey(CInt(dtRow.Item("PId"))) Then Continue For
                    mdicIsNew.Add(CInt(dtRow.Item("PId")), CStr(dtRow.Item("perspective")))
                    Dim dtTemp() As DataRow = mdsList.Tables(0).Select("PId = '" & CInt(dtRow.Item("PId")) & "'")
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp.Length - 1
                            For j As Integer = 0 To dtTemp(i).ItemArray.Length - 1
                                Select Case j
                                    'S.SANDEEP [ 04 JULY 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    'Case Is >= 12

                                    'S.SANDEEP [ 28 DEC 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    'Case Is > 12
                                    Case Is > 11
                                        'S.SANDEEP [ 28 DEC 2012 ] -- END


                                        'S.SANDEEP [ 04 JULY 2012 ] -- END
                                        If j Mod 2 = 0 Or j Mod 2 >= 0 Then
                                            If IsDBNull(dtTemp(i)(j)) = False Then
                                                If IsNumeric(dtTemp(i)(j)) Then
                                                    mdecValue(j) = mdecValue(j) + CDec(dtTemp(i)(j))
                                                    mdecValueTotal(j) = mdecValueTotal(j) + CDec(dtTemp(i)(j))
                                                End If
                                            End If
                                        End If
                                End Select
                            Next
                            dtTable.ImportRow(dtTemp(i))
                        Next
                    End If

                    Dim dRow As DataRow = dtTable.NewRow

                    For i As Integer = 0 To mdecValue.Length - 1
                        If mdecValue(i) <= 0 Then
                            Select Case i
                                Case 0, 1, 2, 3, 4
                                    dRow.Item(i) = -2
                                Case 5
                                    dRow.Item(i) = mdicIsNew(CInt(dtRow.Item("PId")))
                                Case 6
                                    dRow.Item(i) = "Group Total : "
                            End Select
                        Else
                            dRow.Item(i) = mdecValue(i)
                        End If
                    Next
                    dtTable.Rows.Add(dRow)
                Next
                Dim drRow As DataRow = dtTable.NewRow

                For i As Integer = 0 To mdecValueTotal.Length - 1
                    If mdecValueTotal(i) <= 0 Then
                        Select Case i
                            Case 0, 1, 2, 3, 4
                                drRow.Item(i) = -1
                            Case 6
                                drRow.Item(i) = "Grand Total : "                            
                        End Select
                    Else
                        drRow.Item(i) = mdecValueTotal(i)
                    End If
                Next

                dtTable.Rows.Add(drRow)

                Dim strColumns As String = String.Empty
                Dim iCntColumns As Integer = 0

                For Each dCol As DataColumn In dtTable.Columns
                    Select Case iCntColumns
                        Case 0 To 5
                            lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
                        Case 6 To 9
                            lvAssessments.Columns.Add(dCol.Caption, 150, HorizontalAlignment.Left)
                        Case 10, 11
                            lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
                        Case Is >= 12
                            'S.SANDEEP [ 10 APR 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
                            If mblnIsOnlyView = True Then
                                lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
                            Else
                                If iCntColumns > 12 Then
                                    lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
                                Else
                            lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
                                End If
                            End If
                            'S.SANDEEP [ 10 APR 2013 ] -- END
                    End Select
                    strColumns &= "," & dCol.ColumnName
                    iCntColumns += 1
                Next

                strColumns = Mid(strColumns, 2)
                Dim strColumnName() As String = strColumns.Split(CChar(","))

                For Each dtRow As DataRow In dtTable.Rows
                    Dim lvItem As New ListViewItem
                    For i As Integer = 0 To iCntColumns - 1
                        Select Case i
                            Case 0
                                lvItem.Text = dtRow.Item(strColumnName(i)).ToString
                            Case Else
                                lvItem.SubItems.Add(dtRow.Item(strColumnName(i)).ToString)
                        End Select
                    Next
                    lvAssessments.Items.Add(lvItem)
                Next

                lvAssessments.GroupingColumn = lvAssessments.Columns(5)
                lvAssessments.DisplayGroups(True)

                For Each lvItem As ListViewItem In lvAssessments.Items
                    If CInt(lvItem.SubItems(0).Text) = -2 Then
                        lvItem.BackColor = Color.SteelBlue
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    ElseIf CInt(lvItem.SubItems(0).Text) = -1 Then
                        lvItem.BackColor = Color.Maroon
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    End If
                Next

            End If


            'If mdsList.Tables(0).Columns.Count > 0 Then
            '    Dim dtTable As New DataTable("Data")
            '    dtTable = mdsList.Tables(0).Copy
            '    dtTable.Rows.Clear()
            '    Dim mdecValue() As Decimal
            '    Dim mdecValueTotal() As Decimal : mdecValueTotal = New Decimal(mdsList.Tables(0).Columns.Count) {}
            '    Dim mdicIsNew As New Dictionary(Of Integer, String)
            '    For Each dtRow As DataRow In mdsList.Tables(0).Rows
            '        mdecValue = New Decimal(mdsList.Tables(0).Columns.Count) {}
            '        If mdicIsNew.ContainsKey(CInt(dtRow.Item("assessitemunkid"))) Then Continue For
            '        mdicIsNew.Add(CInt(dtRow.Item("assessitemunkid")), CStr(dtRow.Item("MainGrp")))
            '        Dim dtTemp() As DataRow = mdsList.Tables(0).Select("assessitemunkid = '" & CInt(dtRow.Item("assessitemunkid")) & "'")
            '        If dtTemp.Length > 0 Then
            '            For i As Integer = 0 To dtTemp.Length - 1
            '                For j As Integer = 0 To dtTemp(i).ItemArray.Length - 1
            '                    Select Case j
            '                        Case 0, 1, 3, 4
            '                        Case Else
            '                            If IsDBNull(dtTemp(i)(j)) = False Then
            '                                If IsNumeric(dtTemp(i)(j)) Then
            '                                    mdecValue(j) = mdecValue(j) + CDec(dtTemp(i)(j))
            '                                    mdecValueTotal(j) = mdecValueTotal(j) + CDec(dtTemp(i)(j))
            '                                End If
            '                            End If
            '                    End Select
            '                Next
            '                dtTable.ImportRow(dtTemp(i))
            '            Next
            '        End If

            '        Dim dRow As DataRow = dtTable.NewRow

            '        For i As Integer = 0 To mdecValue.Length - 1
            '            If mdecValue(i) <= 0 Then
            '                Select Case i
            '                    Case 0
            '                        dRow.Item(i) = mdicIsNew(CInt(dtRow.Item("assessitemunkid")))
            '                    Case 1
            '                        dRow.Item(i) = "Group Total : "
            '                    Case 3, 4
            '                        dRow.Item(i) = "-1"
            '                End Select
            '            Else
            '                dRow.Item(i) = mdecValue(i)
            '            End If
            '        Next
            '        dtTable.Rows.Add(dRow)
            '    Next

            '    Dim drRow As DataRow = dtTable.NewRow

            '    For i As Integer = 0 To mdecValueTotal.Length - 1
            '        If mdecValueTotal(i) <= 0 Then
            '            Select Case i
            '                Case 0
            '                    drRow.Item(i) = ""
            '                Case 1
            '                    drRow.Item(i) = "Grand Total : "
            '                Case 3, 4
            '                    drRow.Item(i) = "-2"
            '            End Select
            '        Else
            '            drRow.Item(i) = mdecValueTotal(i)
            '        End If
            '    Next

            '    dtTable.Rows.Add(drRow)

            '    Dim strColumns As String = String.Empty
            '    Dim iCntColumns As Integer = 0
            '    For Each dCol As DataColumn In dtTable.Columns
            '        Select Case iCntColumns
            '            Case 0, 3, 4
            '                lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
            '            Case 1
            '                lvAssessments.Columns.Add(dCol.Caption, 500, HorizontalAlignment.Left)
            '            Case Else
            '                lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
            '        End Select
            '        strColumns &= "," & dCol.ColumnName
            '        iCntColumns += 1
            '    Next

            '    strColumns = Mid(strColumns, 2)
            '    Dim strColumnName() As String = strColumns.Split(CChar(","))

            '    For Each dtRow As DataRow In dtTable.Rows
            '        Dim lvItem As New ListViewItem
            '        For i As Integer = 0 To iCntColumns - 1
            '            Select Case i
            '                Case 0
            '                    lvItem.Text = dtRow.Item(strColumnName(i)).ToString
            '                Case Else
            '                    lvItem.SubItems.Add(dtRow.Item(strColumnName(i)).ToString)
            '            End Select
            '        Next
            '        lvAssessments.Items.Add(lvItem)
            '    Next

            '    lvAssessments.GroupingColumn = lvAssessments.Columns(0)
            '    lvAssessments.DisplayGroups(True)

            '    For Each lvItem As ListViewItem In lvAssessments.Items
            '        If CInt(lvItem.SubItems(4).Text) = -1 Then
            '            lvItem.BackColor = Color.SteelBlue
            '            lvItem.ForeColor = Color.White
            '            lvItem.Font = New Font(Me.Font, FontStyle.Bold)
            '        ElseIf CInt(lvItem.SubItems(4).Text) = -2 Then
            '            lvItem.BackColor = Color.Maroon
            '            lvItem.ForeColor = Color.White
            '            lvItem.Font = New Font(Me.Font, FontStyle.Bold)
            '        End If
            '    Next
            'End If

            AddHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAssessmentResult", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Get_Assessor(ByRef iAssessorEmpId As Integer, ByRef sAssessorName As String) As Integer
        Dim iAssessor As Integer = 0
        Try
            Dim dLst As New DataSet
            Dim objBSC As New clsBSC_Analysis_Master
            dLst = objBSC.getAssessorComboList("List", False, False, User._Object._Userunkid)

            If dLst.Tables(0).Rows.Count > 0 Then
                iAssessor = CInt(dLst.Tables(0).Rows(0).Item("Id"))
                iAssessorEmpId = objBSC.GetAssessorEmpId(iAssessor)
                sAssessorName = CStr(dLst.Tables(0).Rows(0).Item("Name"))
            End If

            objBSC = Nothing

            Return iAssessor

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Assessor", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 10 APR 2013 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmCommonAssess_View_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mblnIsOnlyView = True Then
                pnlResultInfo.BringToFront()
                pnlResultInfo.Dock = DockStyle.Fill
                btnFinalSave.Visible = False : btnOpen_Close.Visible = False
            Else
                pnlResultInfo.Size = New Size(863, 327)
                btnFinalSave.Visible = True : btnOpen_Close.Visible = True
            End If
            'S.SANDEEP [ 10 APR 2013 ] -- END

            Call FillAssessmentResult()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonAssess_View_Shown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCommonAssess_View_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBSCMaster = New clsBSC_Analysis_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            mdsList = objBSCMaster.GetAssessmentResultView(mintEmployeeId, mintYearId, mintPeriodId)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub btnFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Try
            Dim iAssessorMasterId, iAssessorEmpId As Integer
            Dim sAssessorName As String = ""
            iAssessorMasterId = 0 : iAssessorEmpId = 0

            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)

            If iAssessorMasterId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "You are about to Approve this BSC Planning for the selected employee, if you follow this process then this employee won't be able to modify or delete this transaction.." & vbCrLf & "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objStatusTran As New clsobjective_status_tran

            objStatusTran._Assessoremployeeunkid = iAssessorMasterId
            objStatusTran._Assessormasterunkid = iAssessorEmpId
            objStatusTran._Commtents = txtComments.Text
            objStatusTran._Employeeunkid = mintEmployeeId
            objStatusTran._Periodunkid = mintPeriodId
            objStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatusTran._Statustypeid = enObjective_Status.FINAL_SAVE
            objStatusTran._Yearunkid = mintYearId
            objStatusTran._Isunlock = False

            'S.SANDEEP [ 23 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objStatusTran._Userunkid = 0
            objStatusTran._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 23 JULY 2013 ] -- END

            If objStatusTran.Insert(User._Object._Userunkid, True) = False Then 'S.SANDEEP [ 13 JUL 2014 ] -- Start -- End 
                eZeeMsgBox.Show(objStatusTran._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim objObjective As New clsObjective_Master
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objObjective.Send_Notification_Employee(mintEmployeeId, sAssessorName, "", True, False)
                objObjective.Send_Notification_Employee(mintEmployeeId, sAssessorName, "", True, False, Company._Object._Companyunkid)
                'Sohail (30 Nov 2017) -- End
                objObjective = Nothing
            End If
            objStatusTran = Nothing

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFinalSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOpen_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen_Close.Click
        Try
            Dim iAssessorMasterId, iAssessorEmpId As Integer
            Dim sAssessorName As String = ""

            iAssessorMasterId = 0 : iAssessorEmpId = 0

            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)

            If iAssessorMasterId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You are about to Disapprove this BSC Planning for the selected employee, Due to this employee will be able to modify or delete this." & vbCrLf & "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objStatusTran As New clsobjective_status_tran

            objStatusTran._Assessoremployeeunkid = iAssessorMasterId
            objStatusTran._Assessormasterunkid = iAssessorEmpId
            objStatusTran._Commtents = txtComments.Text
            objStatusTran._Employeeunkid = mintEmployeeId
            objStatusTran._Periodunkid = mintPeriodId
            objStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
            objStatusTran._Yearunkid = mintYearId
            objStatusTran._Isunlock = False
            'S.SANDEEP [ 23 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objStatusTran._Userunkid = 0
            objStatusTran._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 23 JULY 2013 ] -- END

            If objStatusTran.Insert(User._Object._Userunkid, True) = False Then 'S.SANDEEP [ 13 JUL 2014 ] -- Start -- End 
                eZeeMsgBox.Show(objStatusTran._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim objObjective As New clsObjective_Master
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objObjective.Send_Notification_Employee(mintEmployeeId, sAssessorName, "", False, False)
                objObjective.Send_Notification_Employee(mintEmployeeId, sAssessorName, "", False, False, Company._Object._Companyunkid)
                'Sohail (30 Nov 2017) -- End
                objObjective = Nothing
            End If
            objStatusTran = Nothing

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpen_Close_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 10 APR 2013 ] -- END

#End Region

#Region " Control's Events "

    Private Sub lvAssessments_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvAssessments.ItemSelectionChanged
        Try
            If lvAssessments.SelectedItems.Count > 0 Then
                If lvAssessments.SelectedItems(0).SubItems(3).Text = "-1" Or _
                   lvAssessments.SelectedItems(0).SubItems(3).Text = "-2" Then
                    e.Item.Selected = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssessments_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnFinalSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFinalSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnOpen_Close.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpen_Close.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblComment.Text = Language._Object.getCaption(Me.lblComment.Name, Me.lblComment.Text)
			Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.Name, Me.btnFinalSave.Text)
			Me.btnOpen_Close.Text = Language._Object.getCaption(Me.btnOpen_Close.Name, Me.btnOpen_Close.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "You are about to Approve this BSC Planning for the selected employee, if you follow this process then this employee won't be able to modify or delete this transaction.." & vbCrLf & "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot perform this operation there is no Assessor mapped with your account.")
			Language.setMessage(mstrModuleName, 3, "You are about to Disapprove this BSC Planning for the selected employee, Due to this employee will be able to modify or delete this." & vbCrLf & "Do you wish to continue?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class