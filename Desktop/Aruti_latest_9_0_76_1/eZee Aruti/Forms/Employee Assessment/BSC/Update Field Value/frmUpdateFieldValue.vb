﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmUpdateFieldValue

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmUpdateFieldValue"
    Private mdtFinal As DataTable = Nothing
    Private mintOwrEmplId As Integer = 0
    Private mintPeriodId As Integer = 0
    Private mintFieldUnkid As Integer = 0
    Private mintFieldTypeId As Integer = 0
    Private mintLinkedFld As Integer = 0
    Private mintTableFieldTranUnkId As Integer = 0
    Private eType As clsAssess_Field_Mapping.enWeightCheckType
    Private mstrDefinedGoal As String = ""
    Private mblnCancel As Boolean = True
    Private objEUpdateProgress As clsassess_empupdate_tran
    Private objOUpdateProgress As clsassess_owrupdate_tran
    Private mintEmpUpdateTranunkid As Integer = 0
    Private mintOwrUpdateTranunkid As Integer = 0
    Private mdtPeriodStartDate As Date = Nothing
    'S.SANDEEP |18-JAN-2019| -- START
    Private mintGoalTypeId As Integer = 0
    Private mdecGoalValue As Decimal = 0
    'S.SANDEEP |18-JAN-2019| -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private mstrUoMType As String = String.Empty
    'S.SANDEEP |12-FEB-2019| -- END

'S.SANDEEP |25-FEB-2019| -- START
'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
Private mdblPercentage As Double = 0
'S.SANDEEP |25-FEB-2019| -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFieldId As Integer, _
                                  ByVal iOwrEmplId As Integer, _
                                  ByVal iPeriodId As Integer, _
                                  ByVal iEmployeeName As String, _
                                  ByVal iPeriodName As String, _
                                  ByVal iFieldTranUnkid As Integer, _
                                  ByVal eUpdateType As clsAssess_Field_Mapping.enWeightCheckType, _
                                  ByVal iDefinedGoal As String, _
                                  ByVal intGoalTypeId As Integer, _
                                  ByVal decGoalValue As Decimal, _
                                  ByVal strUoMType As String) As Boolean 'S.SANDEEP |18-JAN-2019| -- START {intGoalTypeId,decGoalValue} -- END
        'S.SANDEEP |12-FEB-2019| -- START {strUoMType} -- END
        Try
            mintFieldUnkid = intFieldId
            mintOwrEmplId = iOwrEmplId
            mintPeriodId = iPeriodId
            mintTableFieldTranUnkId = iFieldTranUnkid
            mstrDefinedGoal = iDefinedGoal
            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            mstrUoMType = strUoMType
            'S.SANDEEP |12-FEB-2019| -- END

            'S.SANDEEP |18-JAN-2019| -- START
            mintGoalTypeId = intGoalTypeId
            mdecGoalValue = decGoalValue
            Select Case mintGoalTypeId
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'Case enGoalType.GT_QUALITATIVE
                '    objlnkGoalType.Text = Language.getMessage("clsMasterData", 843, "Qualitative") & " | " & Language.getMessage(mstrModuleName, 102, "Value") & " : 100 "
                'Case enGoalType.GT_QUANTITATIVE
                '    objlnkGoalType.Text = Language.getMessage("clsMasterData", 844, "Quantitative") & " | " & Language.getMessage(mstrModuleName, 102, "Value") & " : " & Format(mdecGoalValue, GUI.fmtCurrency)
                Case enGoalType.GT_QUALITATIVE
                    objlnkGoalType.Text = Language.getMessage("clsMasterData", 843, "Qualitative") & " | " & Language.getMessage(mstrModuleName, 102, "Value") & " : 100 " & mstrUoMType
                Case enGoalType.GT_QUANTITATIVE
                    objlnkGoalType.Text = Language.getMessage("clsMasterData", 844, "Quantitative") & " | " & Language.getMessage(mstrModuleName, 102, "Value") & " : " & Format(mdecGoalValue, GUI.fmtCurrency) & " " & mstrUoMType
                    'S.SANDEEP |12-FEB-2019| -- END
            End Select
            'S.SANDEEP |18-JAN-2019| -- END
            eType = eUpdateType

            txtPeriod.Text = iPeriodName
            txtGoals.Text = iDefinedGoal

            Select Case eUpdateType
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                    lblEmployee.Visible = False : txtEmployeeName.Visible = False
                    txtOwner.Text = iEmployeeName
                    'S.SANDEEP |23-DEC-2020| -- START
                    'ISSUE/ENHANCEMENT : DON'T SHOW DISAPPROVED DATA BY DEFAULT {UPDATE PROGRESS}
                    chkIncludeDisapproved.Visible = False : chkIncludeDisapproved.Checked = False
                    'S.SANDEEP |23-DEC-2020| -- END
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                    lblOwner.Visible = False : txtOwner.Visible = False
                    txtEmployeeName.Text = iEmployeeName
                    'S.SANDEEP |23-DEC-2020| -- START
                    'ISSUE/ENHANCEMENT : DON'T SHOW DISAPPROVED DATA BY DEFAULT {UPDATE PROGRESS}
                    chkIncludeDisapproved.Visible = True : chkIncludeDisapproved.Checked = False
                    'S.SANDEEP |23-DEC-2020| -- END
            End Select

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(mintPeriodId)
            mintLinkedFld = objFMapping.Get_Map_FieldId(mintPeriodId)

            Dim objFMaster As New clsAssess_Field_Master
            Dim mintExOrder As Integer = objFMaster.Get_Field_ExOrder(mintLinkedFld)

            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMapping = Nothing : objFMaster = Nothing

            Dim objEvaluation As New clsevaluation_analysis_master
            If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, mintOwrEmplId, mintPeriodId) = True Then
                objdgcolhEdit.Visible = False
                objdgcolhDelete.Visible = False
                btnSave.Enabled = False
            End If
            objEvaluation = Nothing

            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = mintPeriodId
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodId
            'Sohail (21 Aug 2015) -- End
            mdtPeriodStartDate = objPrd._Start_Date
            objPrd = Nothing

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP |18-JAN-2019| -- START
            With cboChangeBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 101, "Percentage"))
                .Items.Add(Language.getMessage(mstrModuleName, 102, "Value"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            dsList = objEUpdateProgress.GetCalculationMode("List", False)
            With cboCalculationType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = clsassess_empupdate_tran.enCalcMode.Increasing
            End With
            'S.SANDEEP |24-APR-2020| -- END
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objMData = Nothing
        End Try
    End Sub

    Private Function IsValidInfo() As Boolean
        Try
            If dtpChangeDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Date is mandatory information. Please set date to continue."), enMsgBoxStyle.Information)
                dtpChangeDate.Focus()
                Return False
            End If

            If dtpChangeDate.Value.Date < mdtPeriodStartDate.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Date cannot be less than period start date."), enMsgBoxStyle.Information)
                dtpChangeDate.Focus()
                Return False
            End If

            If dtpChangeDate.Value.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Date cannot be greater than today date."), enMsgBoxStyle.Information)
                dtpChangeDate.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Change Status is mandatory information. Please select change status to continue."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If

            If txtTotalPercentage.Enabled = True AndAlso txtTotalPercentage.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, % completed is mandatory information. Please enter % completed to continue."), enMsgBoxStyle.Information)
                txtNewValue.Focus()
                Return False
            End If

            'S.SANDEEP |08-JAN-2019| -- START
            'If txtPercent.Decimal > 100 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
            '    txtPercent.Focus()
            '    Return False
            'End If
            'S.SANDEEP |08-JAN-2019| -- END
            

            'Shani(24-JAN-2017) -- Start
            'Enhancement : 
            ''Shani (26-Sep-2016) -- Start
            ''Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'If mintEmpUpdateTranunkid <= 0 Then 'INSERT
            '    If ConfigParameter._Object._GoalsAccomplishedRequiresApproval Then
            '        If CType(dgvHistory.DataSource, DataTable).Select("approvalstatusunkid = " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending).Count > 0 Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding "), enMsgBoxStyle.Information)
            '            Return False
            '        End If
            '    End If
            'End If
            ''Shani (26-Sep-2016) -- End
            'Shani(24-JAN-2017) -- End


            'S.SANDEEP |18-JAN-2019| -- START
            If cboChangeBy.Enabled = True Then
                If cboChangeBy.SelectedIndex = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 200, "Sorry, change by is mandatory information. Please select change by to continue."), enMsgBoxStyle.Information)
                    cboChangeBy.Focus()
                    Return False
                ElseIf cboChangeBy.SelectedIndex = 1 AndAlso txtTotalPercentage.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 201, "Sorry, % completed is mandatory information. Please enter % completed to continue."), enMsgBoxStyle.Information)
                    txtNewValue.Focus()
                    Return False
                ElseIf cboChangeBy.SelectedIndex = 2 AndAlso txtTotalValue.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 202, "Sorry, total value is mandatory information. Please total value to continue."), enMsgBoxStyle.Information)
                    txtNewValue.Focus()
                    Return False
                End If
            End If
            If txtTotalPercentage.Enabled = True AndAlso txtTotalPercentage.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 201, "Sorry, % completed is mandatory information. Please enter % completed to continue."), enMsgBoxStyle.Information)
                txtNewValue.Focus()
                Return False
            End If
            If CInt(cboChangeBy.SelectedIndex) = 1 Then
                Dim dblMaxValue As Double = 0
                dblMaxValue = 100
                If dblMaxValue > 0 AndAlso (CDec(txtNewPercentage.Text) > dblMaxValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 203, "Sorry, Percentage Completed cannot be greater than 100."), enMsgBoxStyle.Information)
                    txtNewPercentage.Focus()
                    Return False
                End If
                If dblMaxValue > 0 AndAlso (CDec(txtTotalPercentage.Text) > dblMaxValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 203, "Sorry, Percentage Completed cannot be greater than 100."), enMsgBoxStyle.Information)
                    txtNewPercentage.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP [15-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : {#32}
            If mintEmpUpdateTranunkid <= 0 Then 'INSERT
                If ConfigParameter._Object._GoalsAccomplishedRequiresApproval Then
                    If CType(dgvHistory.DataSource, DataTable).Select("approvalstatusunkid = " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending).Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding "), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End If
            'S.SANDEEP [15-Feb-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidInfo", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub SetTagValue(ByVal dtTab As DataTable, ByVal blnIsEdit As Boolean)
        Try
            If dtTab.Rows.Count > 0 Then
                Dim iMaxTranId As Integer = 0
                'S.SANDEEP |05-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'iMaxTranId = Convert.ToInt32(dtTab.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)))
                If IsDBNull(dtTab.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))) = False Then
                iMaxTranId = Convert.ToInt32(dtTab.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)))
                End If
                'S.SANDEEP |05-MAR-2019| -- END
                If blnIsEdit Then
                    Dim iSecondLastTranId As Integer = 0
                    Dim iList As List(Of Integer) = dtTab.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid")).ToList()
                    If iList.Count > 0 Then
                        Dim dr() As DataRow = Nothing
                        If iList.Min() = iMaxTranId Then
                            dr = dtTab.Select("empupdatetranunkid = " & iMaxTranId)
                            If dr.Length > 0 Then
                                cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                                cboChangeBy.Enabled = False
                                cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
                                Select Case cboChangeBy.SelectedIndex
                                    Case 1
                                        txtNewPercentage.Tag = 0
                                    Case 2
                                        txtNewValue.Tag = 0
                                End Select
                            End If
                        Else
                            Try
                                iSecondLastTranId = iList.Item(iList.IndexOf(iMaxTranId) + 1)
                            Catch ex As Exception
                                iSecondLastTranId = iMaxTranId
                            End Try
                            dr = dtTab.Select("empupdatetranunkid = " & iSecondLastTranId)
                            If dr.Length > 0 Then
                                cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                                cboChangeBy.Enabled = False
                                cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
                                Select Case cboChangeBy.SelectedIndex
                                    Case 1
                                        txtNewPercentage.Tag = CDec(dr(0)("pct_complete"))
                                    Case 2
                                        txtNewValue.Tag = CDec(dr(0)("finalvalue"))
                                End Select
                            End If
                        End If
                    Else
                        txtNewPercentage.Tag = 0
                        txtNewValue.Tag = 0
                    End If
                Else
                    If iMaxTranId > 0 Then
                        Dim dr() As DataRow = dtTab.Select("empupdatetranunkid = " & iMaxTranId)
                        If dr.Length > 0 Then
                            cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                            cboChangeBy.Enabled = False
                            Select Case cboChangeBy.SelectedIndex
                                Case 1
                                    txtNewPercentage.Tag = CDec(dr(0)("pct_complete"))
                                Case 2
                                    txtNewValue.Tag = CDec(dr(0)("finalvalue"))
                            End Select
                        Else
                            txtNewPercentage.Tag = 0
                            txtNewValue.Tag = 0
                        End If
                    Else
                        txtNewPercentage.Tag = 0
                        txtNewValue.Tag = 0
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetTagValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_History()
        Dim dtViewTable As DataTable = Nothing
        Try
            Select Case eType
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                    dtViewTable = objOUpdateProgress.GetList("List", mintOwrEmplId, mintPeriodId, mintTableFieldTranUnkId).Tables(0)
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                    dtViewTable = objEUpdateProgress.GetList("List", mintOwrEmplId, mintPeriodId, mintTableFieldTranUnkId).Tables(0)

                    'S.SANDEEP |23-DEC-2020| -- START
                    'ISSUE/ENHANCEMENT : DON'T SHOW DISAPPROVED DATA BY DEFAULT {UPDATE PROGRESS}
                    If chkIncludeDisapproved.Checked = False Then
                        Dim dt As DataTable = New DataView(dtViewTable, "approvalstatusunkid <> " & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected), "", DataViewRowState.CurrentRows).ToTable
                        dtViewTable = dt
                    End If
                    'S.SANDEEP |23-DEC-2020| -- END


                    'S.SANDEEP |18-JAN-2019| -- START
                    'Dim iMaxTranId As Integer = 0
                    'If dtViewTable.Rows.Count > 0 Then
                    '    iMaxTranId = Convert.ToInt32(dtViewTable.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)))
                    'End If
                    'If iMaxTranId > 0 Then
                    '    Dim iSecondLastTranId As Integer = 0
                    '    Dim iList As List(Of Integer) = dtViewTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid")).ToList()
                    '    If iList.Count > 0 Then
                    '        Dim dr() As DataRow = Nothing
                    '        If iList.Min() = iMaxTranId Then
                    '            dr = dtViewTable.Select("empupdatetranunkid = " & iMaxTranId)
                    '            If dr.Length > 0 Then
                            'cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                            'cboChangeBy.Enabled = False
                    '                Select Case cboChangeBy.SelectedIndex
                    '                    Case 1
                    '                        txtNewPercentage.Tag = 0
                    '                    Case 2
                    '                        txtNewValue.Tag = 0
                    '                End Select
                    '            End If
                    '        Else
                    '            Try
                    '                iSecondLastTranId = iList.Item(iList.IndexOf(iMaxTranId) + 1)
                    '            Catch ex As Exception
                    '                iSecondLastTranId = iMaxTranId
                    '            End Try
                    '            dr = dtViewTable.Select("empupdatetranunkid = " & iSecondLastTranId)

                    '            If dr.Length > 0 Then
                    '                cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                    '                cboChangeBy.Enabled = False
                            'Select Case cboChangeBy.SelectedIndex
                            '   Case 1
                    '                        txtNewPercentage.Tag = CDec(dr(0)("pct_complete"))
                            '   Case 2
                    '                        txtNewValue.Tag = CDec(dr(0)("finalvalue"))
                            'End Select
                    '            End If

                    '        End If
                    '    Else
                    '        txtNewPercentage.Tag = 0
                    '        txtNewValue.Tag = 0
                    '    End If
                    'Else
                    '    txtNewPercentage.Tag = 0
                    '    txtNewValue.Tag = 0
                    'End If
                    Call SetTagValue(dtViewTable, False)
                    'S.SANDEEP |18-JAN-2019| -- END
            End Select

            If dtViewTable IsNot Nothing Then
                dgvHistory.AutoGenerateColumns = False
                dgcolhDate.DataPropertyName = "ddate"
                dgcolhPercent.DataPropertyName = "pct_completed"
                dgcolhRemark.DataPropertyName = "remark"
                dgcolhStatus.DataPropertyName = "dstatus"
                dgcolhLastValue.DataPropertyName = "dfinalvalue"

                Select Case eType
                    Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                        objdgcolhUnkid.DataPropertyName = "owrupdatetranunkid"
                    Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                        objdgcolhUnkid.DataPropertyName = "empupdatetranunkid"
                End Select

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                objdgcolhAccomplishedStatusId.DataPropertyName = "approvalstatusunkid"
                dgcolhAccomplishedStatus.DataPropertyName = "GoalAccomplishmentStatus"
                If ConfigParameter._Object._GoalsAccomplishedRequiresApproval = False Then
                    dgcolhAccomplishedStatus.Visible = False
                End If
                'Shani (26-Sep-2016) -- End


                dgvHistory.DataSource = dtViewTable

                dgcolhPercent.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_History", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Select Case eType
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                    'txtPercent.Decimal = objOUpdateProgress._Pct_Completed
                    txtRemark.Text = objOUpdateProgress._Remark
                    cboStatus.SelectedValue = objOUpdateProgress._Statusunkid
                    dtpChangeDate.Value = objOUpdateProgress._Updatedate
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    dtpChangeDate.Checked = True
                    'Shani (26-Sep-2016) -- End
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                    'txtPercent.Decimal = objEUpdateProgress._Pct_Completed
                    txtRemark.Text = objEUpdateProgress._Remark
                    cboStatus.SelectedValue = objEUpdateProgress._Statusunkid
                    dtpChangeDate.Value = objEUpdateProgress._Updatedate
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    dtpChangeDate.Checked = True
                    'Shani (26-Sep-2016) -- End

                    'S.SANDEEP |18-JAN-2019| -- START
                    cboChangeBy.SelectedIndex = objEUpdateProgress._ChangeById
                    Select Case objEUpdateProgress._ChangeById
                        Case 1  'PERCENTAGE
                            txtTotalPercentage.Text = Format(objEUpdateProgress._FinalValue, GUI.fmtCurrency).ToString
                            'txtNewPercentage.Text = Format(objEUpdateProgress._Pct_Completed, GUI.fmtCurrency).ToString
                        Case 2  'VALUE
                            txtTotalValue.Text = Format(objEUpdateProgress._FinalValue, GUI.fmtCurrency).ToString
                            'txtNewValue.Text = Format(objEUpdateProgress._FinalValue, GUI.fmtCurrency).ToString
                    End Select
                    'S.SANDEEP |18-JAN-2019| -- END

                    'S.SANDEEP |24-APR-2020| -- START
                    'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                    cboCalculationType.SelectedValue = objEUpdateProgress._CalcModeId
                    'S.SANDEEP |24-APR-2020| -- END

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            Select Case eType
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                    objOUpdateProgress._Owrfieldtypeid = mintFieldTypeId
                    objOUpdateProgress._Owrfieldunkid = mintTableFieldTranUnkId
                    objOUpdateProgress._Fieldunkid = mintFieldUnkid
                    objOUpdateProgress._Ownerunkid = mintOwrEmplId
                    objOUpdateProgress._Isvoid = False
                    objOUpdateProgress._Pct_Completed = CDec(mdblPercentage)
                    objOUpdateProgress._Periodunkid = mintPeriodId
                    objOUpdateProgress._Remark = txtRemark.Text
                    objOUpdateProgress._Statusunkid = CInt(cboStatus.SelectedValue)
                    objOUpdateProgress._Updatedate = dtpChangeDate.Value
                    objOUpdateProgress._Userunkid = User._Object._Userunkid
                    objOUpdateProgress._Voiddatetime = Nothing
                    objOUpdateProgress._Voidreason = ""
                    objOUpdateProgress._Voiduserunkid = -1

                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                    objEUpdateProgress._Empfieldtypeid = mintFieldTypeId
                    objEUpdateProgress._Empfieldunkid = mintTableFieldTranUnkId
                    objEUpdateProgress._Employeeunkid = mintOwrEmplId
                    objEUpdateProgress._Fieldunkid = mintFieldUnkid
                    objEUpdateProgress._Isvoid = False
                    objEUpdateProgress._Pct_Completed = CDec(mdblPercentage)
                    objEUpdateProgress._Periodunkid = mintPeriodId
                    objEUpdateProgress._Remark = txtRemark.Text
                    objEUpdateProgress._Statusunkid = CInt(cboStatus.SelectedValue)
                    objEUpdateProgress._Updatedate = dtpChangeDate.Value
                    objEUpdateProgress._Userunkid = User._Object._Userunkid
                    objEUpdateProgress._Voiddatetime = Nothing
                    objEUpdateProgress._Voidreason = ""
                    objEUpdateProgress._Voiduserunkid = -1
                    'S.SANDEEP |24-APR-2020| -- START
                    'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                    objEUpdateProgress._CalcModeId = CInt(cboCalculationType.SelectedValue)
                    'S.SANDEEP |24-APR-2020| -- END
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    If ConfigParameter._Object._GoalsAccomplishedRequiresApproval Then
                        'If User._Object.Privilege._AllowToApproveGoalsAccomplishment = False Then
                        '    objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
                        'Else
                        '    objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
                        'End If
                        objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
                    Else
                        objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
                    End If
                    objEUpdateProgress._GoalAccomplishmentRemark = ""
                    'Shani (26-Sep-2016) -- End

                    'S.SANDEEP |18-JAN-2019| -- START
                    objEUpdateProgress._ChangeById = cboChangeBy.SelectedIndex
                    If cboChangeBy.SelectedIndex = 1 Then
                        objEUpdateProgress._Pct_Completed = CDec(mdblPercentage)
                        objEUpdateProgress._FinalValue = txtTotalPercentage.Decimal
                    ElseIf cboChangeBy.SelectedIndex = 2 Then
                        objEUpdateProgress._Pct_Completed = CDec(mdblPercentage)
                        objEUpdateProgress._FinalValue = txtTotalValue.Decimal
                    End If
                    'S.SANDEEP |18-JAN-2019| -- END
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            dtpChangeDate.Checked = False
            mintEmpUpdateTranunkid = 0
            cboStatus.SelectedValue = 0
            'txtPercent.Decimal = 0
            txtRemark.Text = ""
            'S.SANDEEP |18-JAN-2019| -- START
            txtNewValue.Decimal = 0
            txtTotalPercentage.Decimal = 0
            txtTotalValue.Decimal = 0
            'S.SANDEEP |18-JAN-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Clear_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmUpdateFieldValue_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEUpdateProgress = New clsassess_empupdate_tran
        objOUpdateProgress = New clsassess_owrupdate_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call Fill_History()
            'S.SANDEEP |18-JAN-2019| -- START
            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                cboChangeBy.Enabled = False
                cboChangeBy.SelectedIndex = 1
            End If
            'S.SANDEEP |18-JAN-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUpdateFieldValue_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmUpdateFieldValue_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_empupdate_tran.SetMessages()
            clsassess_owrupdate_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_empupdate_tran,clsassess_owrupdate_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidInfo() = False Then Exit Sub

            If txtRemark.Text.Trim.Length <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you have not entered the remark. Would you like to save the changes without remark?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            Call SetValue()
            Select Case eType
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                    If mintOwrUpdateTranunkid <= 0 Then 'INSERT
                        blnFlag = objOUpdateProgress.Insert()
                    ElseIf mintEmpUpdateTranunkid > 0 Then  'UPDATE
                        blnFlag = objOUpdateProgress.Update()
                    End If
                Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                    If mintEmpUpdateTranunkid <= 0 Then 'INSERT
                        blnFlag = objEUpdateProgress.Insert()
                    ElseIf mintEmpUpdateTranunkid > 0 Then  'UPDATE
                        blnFlag = objEUpdateProgress.Update()
                    End If
            End Select

            If blnFlag = True Then

                'Shani(24-JAN-2017) -- Start
                'Enhancement : 
                ''Shani (26-Sep-2016) -- Start
                ''Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'If ConfigParameter._Object._GoalsAccomplishedRequiresApproval Then
                '    Select Case eType
                '        Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                '            If mintEmpUpdateTranunkid <= 0 Then 'INSERT
                '                objEUpdateProgress.Send_Notification_Assessor(mintOwrEmplId, mintPeriodId, _
                '                                                              FinancialYear._Object._YearUnkid, _
                '                                                              FinancialYear._Object._FinancialYear_Name, _
                '                                                              FinancialYear._Object._DatabaseName, objEUpdateProgress._Empupdatetranunkid, _
                '                                                              Company._Object._Companyunkid, _
                '                                                              ConfigParameter._Object._ArutiSelfServiceURL, _
                '                                                              enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
                '            End If
                '    End Select

                'End If
                ''Shani (26-Sep-2016) -- End
                'Shani(24-JAN-2017) -- End

                Call Clear_Controls()
                Call Fill_History()
            Else
                Select Case eType
                    Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                        If objOUpdateProgress._Message <> "" Then
                            eZeeMsgBox.Show(objOUpdateProgress._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                        If objEUpdateProgress._Message <> "" Then
                            eZeeMsgBox.Show(objEUpdateProgress._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                End Select
                
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'S.SANDEEP |18-JAN-2019| -- START
#Region " Combobox Event(s) "

    Private Sub cboChangeBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChangeBy.SelectedIndexChanged
        Try
            Select Case cboChangeBy.SelectedIndex
                Case 0  'SELECT
                    txtNewPercentage.Enabled = False
                    txtNewValue.Enabled = False
                    txtTotalPercentage.Enabled = False
                    txtTotalValue.Enabled = False
                Case 1  'PERCENTAGE
                    txtNewPercentage.Enabled = True
                    txtNewValue.Enabled = False
                    txtTotalPercentage.Enabled = True
                    txtTotalValue.Enabled = False
                Case 2 'VALUE
                    txtNewPercentage.Enabled = False
                    txtNewValue.Enabled = True
                    txtTotalPercentage.Enabled = False
                    txtTotalValue.Enabled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChangeBy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub cboChangeBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChangeBy.SelectedIndexChanged
    '    Try
    '        Select Case cboChangeBy.SelectedIndex
    '            Case 0  '
    '                txtChangeBy.Enabled = False
    '                objlblCaption1.Text = ""
    '                objlblCaption2.Text = ""
    '                objlblCaption3.Text = ""
    '                objlblCaption4.Text = ""
    '            Case 1  '
    '                txtChangeBy.Enabled = True
    '                objlblCaption1.Text = Language.getMessage(mstrModuleName, 202, "Current") & " " & cboChangeBy.Text
    '                objlblCaption2.Text = Language.getMessage(mstrModuleName, 203, "Final") & " " & cboChangeBy.Items.Item(cboChangeBy.SelectedIndex + 1).ToString()
    '                objlblCaption3.Text = Language.getMessage(mstrModuleName, 201, "Last") & " " & cboChangeBy.Items.Item(cboChangeBy.SelectedIndex + 1).ToString()
    '                objlblCaption4.Text = Language.getMessage(mstrModuleName, 202, "Current") & " " & cboChangeBy.Items.Item(cboChangeBy.SelectedIndex + 1).ToString()
    '            Case 2  '
    '                txtChangeBy.Enabled = True
    '                objlblCaption1.Text = Language.getMessage(mstrModuleName, 202, "Current") & " " & cboChangeBy.Text
    '                objlblCaption2.Text = Language.getMessage(mstrModuleName, 203, "Final") & " " & cboChangeBy.Items.Item(cboChangeBy.SelectedIndex - 1).ToString()
    '                objlblCaption3.Text = Language.getMessage(mstrModuleName, 201, "Last") & " " & cboChangeBy.Items.Item(cboChangeBy.SelectedIndex - 1).ToString()
    '                objlblCaption4.Text = Language.getMessage(mstrModuleName, 202, "Current") & " " & cboChangeBy.Items.Item(cboChangeBy.SelectedIndex - 1).ToString()
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboChangeBy_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

#End Region

#Region " Textbox Event(s) "

    'Private Sub txtChangeBy_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtChangeBy.TextChanged
    '    Try
    '        If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '            If CDbl(txtChangeBy.Decimal) > CDbl(100) Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            Else
    '                txtCurrentValue.Text = txtChangeBy.Text
    '            End If
    '        ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
    '            Select Case cboChangeBy.SelectedIndex
    '                Case 1  'PERCENTAGE
    '                    txtCurrentValue.Text = Format(CDbl((mdecGoalValue * txtChangeBy.Decimal) / 100), GUI.fmtCurrency)
    '                Case 2  'VALUE
    '                    txtCurrentValue.Text = Format(CDbl((txtChangeBy.Decimal * 100) / mdecGoalValue), GUI.fmtCurrency)
    '            End Select
    '        End If

    '        If cboChangeBy.SelectedIndex = 1 Then
    '            txtFinalValue.Text = Format(CDec(txtChangeBy.Decimal + txtLastValue_Pct.Decimal), GUI.fmtCurrency)
    '        ElseIf cboChangeBy.SelectedIndex = 2 Then
    '            txtFinalValue.Text = Format(CDec(txtLastValue_Pct.Decimal + txtCurrentValue.Decimal), GUI.fmtCurrency)
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtChangeBy_TextChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub txtTotalPercentage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalPercentage.TextChanged
        Try
            RemoveHandler txtNewPercentage.TextChanged, AddressOf txtNewPercentage_Pct_TextChanged
            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                If CDbl(txtTotalPercentage.Decimal) > CDbl(100) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            txtNewPercentage.Decimal = txtTotalPercentage.Decimal - CDec(txtNewPercentage.Tag)
            mdblPercentage = txtTotalPercentage.Decimal
            AddHandler txtNewPercentage.TextChanged, AddressOf txtNewPercentage_Pct_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTotalPercentage_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtNewPercentage_Pct_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewPercentage.TextChanged
        Try
            RemoveHandler txtTotalPercentage.TextChanged, AddressOf txtTotalPercentage_TextChanged
            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                If CDbl(txtNewPercentage.Decimal) > CDbl(100) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            txtTotalPercentage.Decimal = txtNewPercentage.Decimal + CDec(txtNewPercentage.Tag)
            mdblPercentage = txtTotalPercentage.Decimal
            AddHandler txtTotalPercentage.TextChanged, AddressOf txtTotalPercentage_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewPercentage_Pct_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtTotalValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalValue.TextChanged
        Try
            RemoveHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
            txtNewValue.Decimal = txtTotalValue.Decimal - CDec(txtNewValue.Tag)
            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            'mdblPercentage = CDbl((txtTotalValue.Decimal * 100) / CDec(IIf(mdecGoalValue <= 0, 1, mdecGoalValue)))
            If CInt(cboCalculationType.SelectedValue) = clsassess_empupdate_tran.enCalcMode.Increasing Then
            mdblPercentage = CDbl((txtTotalValue.Decimal * 100) / CDec(IIf(mdecGoalValue <= 0, 1, mdecGoalValue)))
            Else
                mdblPercentage = CDbl((mdecGoalValue * 100) / CDec(IIf(txtTotalValue.Decimal <= 0, 1, txtTotalValue.Decimal)))
            End If
            'S.SANDEEP |24-APR-2020| -- END
            AddHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTotalValue_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtNewValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewValue.TextChanged
        Try
            RemoveHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged
            txtTotalValue.Decimal = txtNewValue.Decimal + CDec(txtNewValue.Tag)
            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER
            'mdblPercentage = CDbl((txtTotalValue.Decimal * 100) / CDec(IIf(mdecGoalValue <= 0, 1, mdecGoalValue)))
            If CInt(cboCalculationType.SelectedValue) = clsassess_empupdate_tran.enCalcMode.Increasing Then
            mdblPercentage = CDbl((txtTotalValue.Decimal * 100) / CDec(IIf(mdecGoalValue <= 0, 1, mdecGoalValue)))
            Else
                mdblPercentage = CDbl((mdecGoalValue * 100) / CDec(IIf(txtTotalValue.Decimal <= 0, 1, txtTotalValue.Decimal)))
            End If
            'S.SANDEEP |24-APR-2020| -- END
            AddHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewValue_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP |18-JAN-2019| -- END


    'S.SANDEEP |23-DEC-2020| -- START
    'ISSUE/ENHANCEMENT : DON'T SHOW DISAPPROVED DATA BY DEFAULT {UPDATE PROGRESS}
    Private Sub chkIncludeDisapproved_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeDisapproved.CheckedChanged
        Try
            Fill_History()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |23-DEC-2020| -- END

#Region " DataGrid Events "

    Private Sub dgvHistory_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index
                    Select Case eType
                        Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                            mintOwrUpdateTranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhUnkid.Index).Value)
                            objOUpdateProgress._Owrupdatetranunkid = mintOwrUpdateTranunkid
                        Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                            'Shani (26-Sep-2016) -- Start
                            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                            If ConfigParameter._Object._GoalsAccomplishedRequiresApproval Then
                                If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAccomplishedStatusId.Index).Value) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You can not edit this recoed.reason: this record is approved "), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                                If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAccomplishedStatusId.Index).Value) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You can not edit this recoed.reason: this record is Rejected "), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                            'Shani (26-Sep-2016) -- End

                            If dgvHistory.DataSource IsNot Nothing Then
                                If CType(dgvHistory.DataSource, DataTable).Select(objdgcolhUnkid.DataPropertyName & " > " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhUnkid.Index).Value)).Length > 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 204, "Sorry, you cannot edit this information. Reason there is another information present which is greater than selected information."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If

                            mintEmpUpdateTranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhUnkid.Index).Value)
                            objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
                    End Select
                    Call SetTagValue(CType(dgvHistory.DataSource, DataTable), True)
                    Call GetValue()
                Case objdgcolhDelete.Index
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    If ConfigParameter._Object._GoalsAccomplishedRequiresApproval Then
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAccomplishedStatusId.Index).Value) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You can not edit this recoed.reason: this record is approved "), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAccomplishedStatusId.Index).Value) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You can not edit this recoed.reason: this record is Rejected "), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'Shani (26-Sep-2016) -- End

                    If dgvHistory.DataSource IsNot Nothing Then
                        If CType(dgvHistory.DataSource, DataTable).Select(objdgcolhUnkid.DataPropertyName & " > " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhUnkid.Index).Value)).Length > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 205, "Sorry, you cannot delete this information. Reason there is another information present which is greater than selected information."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    Dim frm As New frmReasonSelection
                    Dim iVoidReason As String = String.Empty

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete this progress for the selected employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                    frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)

                    If iVoidReason.Length <= 0 Then
                        Exit Sub
                    End If

                    frm.Dispose()
                    Select Case eType
                        Case clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL
                            objOUpdateProgress._Isvoid = True
                            objOUpdateProgress._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOUpdateProgress._Voidreason = iVoidReason
                            objOUpdateProgress._Voiduserunkid = User._Object._Userunkid

                            If objOUpdateProgress.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhUnkid.Index).Value)) = True Then
                                Call Fill_History()
                            End If
                        Case clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL
                            objEUpdateProgress._Isvoid = True
                            objEUpdateProgress._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEUpdateProgress._Voidreason = iVoidReason
                            objEUpdateProgress._Voiduserunkid = User._Object._Userunkid

                            If objEUpdateProgress.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhUnkid.Index).Value)) = True Then
                                Call Fill_History()
                            End If
                    End Select

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblGoals.Text = Language._Object.getCaption(Me.lblGoals.Name, Me.lblGoals.Text)
            Me.lblOwner.Text = Language._Object.getCaption(Me.lblOwner.Name, Me.lblOwner.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.lblCaption2.Text = Language._Object.getCaption(Me.lblCaption2.Name, Me.lblCaption2.Text)
            Me.lblChangeBy.Text = Language._Object.getCaption(Me.lblChangeBy.Name, Me.lblChangeBy.Text)
            Me.lblCaption3.Text = Language._Object.getCaption(Me.lblCaption3.Name, Me.lblCaption3.Text)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.lblCaption4.Text = Language._Object.getCaption(Me.lblCaption4.Name, Me.lblCaption4.Text)
            Me.dgcolhDate.HeaderText = Language._Object.getCaption(Me.dgcolhDate.Name, Me.dgcolhDate.HeaderText)
            Me.dgcolhPercent.HeaderText = Language._Object.getCaption(Me.dgcolhPercent.Name, Me.dgcolhPercent.HeaderText)
            Me.dgcolhLastValue.HeaderText = Language._Object.getCaption(Me.dgcolhLastValue.Name, Me.dgcolhLastValue.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)
            Me.dgcolhAccomplishedStatus.HeaderText = Language._Object.getCaption(Me.dgcolhAccomplishedStatus.Name, Me.dgcolhAccomplishedStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 843, "Qualitative")
            Language.setMessage("clsMasterData", 844, "Quantitative")
            Language.setMessage(mstrModuleName, 1, "Sorry, Date is mandatory information. Please set date to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Date cannot be less than period start date.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Change Status is mandatory information. Please select change status to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, % completed is mandatory information. Please enter % completed to continue.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you have not entered the remark. Would you like to save the changes without remark?")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete this progress for the selected employee?")
            Language.setMessage(mstrModuleName, 8, "Sorry, Date cannot be greater than today date.")
            Language.setMessage(mstrModuleName, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding")
            Language.setMessage(mstrModuleName, 10, "Sorry, You can not edit this recoed.reason: this record is approved")
            Language.setMessage(mstrModuleName, 11, "Sorry, You can not edit this recoed.reason: this record is Rejected")
            Language.setMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue.")
            Language.setMessage(mstrModuleName, 100, "Select")
            Language.setMessage(mstrModuleName, 101, "Percentage")
            Language.setMessage(mstrModuleName, 102, "Value")
            Language.setMessage(mstrModuleName, 200, "Sorry, change by is mandatory information. Please select change by to continue.")
            Language.setMessage(mstrModuleName, 201, "Sorry, % completed is mandatory information. Please enter % completed to continue.")
            Language.setMessage(mstrModuleName, 202, "Sorry, total value is mandatory information. Please total value to continue.")
            Language.setMessage(mstrModuleName, 203, "Sorry, Percentage Completed cannot be greater than 100.")
            Language.setMessage(mstrModuleName, 204, "Sorry, you cannot edit this information. Reason there is another information present which is greater than selected information.")
            Language.setMessage(mstrModuleName, 205, "Sorry, you cannot delete this information. Reason there is another information present which is greater than selected information.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class