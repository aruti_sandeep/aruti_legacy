﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class objfrmAddEditCoyField1

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "objfrmAddEditCoyField1"
    Private mblnCancel As Boolean = True
    Private mintCoyField1Unkid As Integer = 0
    Private objCoyField1 As clsassess_coyfield1_master
    Private objCoyOwner As clsassess_coyowner_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtOwner As DataTable
    Private mintFieldUnkid As Integer
    Private objFieldMaster As New clsAssess_Field_Master(True)
    Private mdicFieldData As New Dictionary(Of Integer, String)
    Private objWSetting As New clsWeight_Setting(True)
    Private mintLinkedFieldId As Integer = -1
    Private mintSelectedPeriodId As Integer = -1
    Private iOwnerRefId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intFieldId As Integer, ByVal iPeriodId As Integer) As Boolean
        Try
            mintCoyField1Unkid = intUnkId
            mintFieldUnkid = intFieldId
            mintSelectedPeriodId = iPeriodId
            menAction = eAction

            objtabcRemarks.Enabled = False
            objpnlData.Enabled = False

            Me.ShowDialog()

            intUnkId = mintCoyField1Unkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Set_Form_Information()
        Try
            Me.Text = Language.getMessage(mstrModuleName, 10, "Add/Edit Company") & " " & objFieldMaster._Field1_Caption & " " & _
                      Language.getMessage(mstrModuleName, 11, "Information")
            objlblField1.Text = objFieldMaster._Field1_Caption
            txtFieldValue1.Tag = objFieldMaster._Field1Unkid

            If mintFieldUnkid = mintLinkedFieldId Then
                If objFieldMaster._Field6_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark1)
                Else
                    objtabpRemark1.Text = objFieldMaster._Field6_Caption
                    txtRemark1.Tag = objFieldMaster._Field6Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark2)
                Else
                    objtabpRemark2.Text = objFieldMaster._Field7_Caption
                    txtRemark2.Tag = objFieldMaster._Field7Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark3)
                Else
                    objtabpRemark3.Text = objFieldMaster._Field8_Caption
                    txtRemark3.Tag = objFieldMaster._Field8Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicFieldData.Keys.Count > 0 Then objtabcRemarks.Enabled = True
            Else
                objtabcRemarks.Enabled = False : objpnlData.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Information", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'dsList = objMData.Get_BSC_Perspective("List", True)
            Dim objPerspective As New clsassess_perspective_master
            dsList = objPerspective.getComboList("List", True)
            objPerspective = Nothing
            'S.SANDEEP [ 05 NOV 2014 ] -- END
            With cboPerspective
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintSelectedPeriodId
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtFieldValue1.BackColor = GUI.ColorComp
            cboAllocations.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            cboPerspective.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)
                If mdtOwner.Rows.Count > 0 Then
                    Dim dRow As DataRow() = mdtOwner.Select("allocationid = '" & CInt(dtRow.Item(StrIdColName)) & "' AND AUD <> 'D'")
                    If dRow.Length > 0 Then
                        lvItem.Checked = True
                    End If
                End If
                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 7 Then
                objcolhAllocations.Width = 235 - 35
            Else
                objcolhAllocations.Width = 235
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub GoalOwnerOperation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            If mdtOwner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwner.Select("allocationid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("coyfieldunkid") = mintCoyField1Unkid
                        dRow.Item("allocationid") = iTagUnkid
                        dRow.Item("coyfieldtypeid") = enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwner.Rows.Add(dRow)
                    End If
                    mdtOwner.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GoalOwnerOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If dtpEndDate.Checked = True Then
                objCoyField1._Enddate = dtpEndDate.Value
            Else
                objCoyField1._Enddate = Nothing
            End If
            objCoyField1._Field_Data = txtFieldValue1.Text
            objCoyField1._Fieldunkid = mintFieldUnkid
            objCoyField1._Isvoid = False
            objCoyField1._Periodunkid = CInt(cboPeriod.SelectedValue)
            objCoyField1._Perspectiveunkid = CInt(cboPerspective.SelectedValue)
            If dtpStartDate.Checked = True Then
                objCoyField1._Startdate = dtpStartDate.Value
            Else
                objCoyField1._Startdate = Nothing
            End If
            objCoyField1._Userunkid = User._Object._Userunkid
            objCoyField1._Voiddatetime = Nothing
            objCoyField1._Voidreason = ""
            objCoyField1._Voiduserunkid = 0
            objCoyField1._Weight = txtWeight.Decimal
            objCoyField1._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD1
            If mintFieldUnkid = mintLinkedFieldId Then
                objCoyField1._Ownerrefid = CInt(cboAllocations.SelectedValue)
                objCoyField1._Statusunkid = CInt(cboStatus.SelectedValue)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objCoyField1._Enddate <> Nothing Then
                dtpEndDate.Value = objCoyField1._Enddate
                dtpEndDate.Checked = True
            End If
            txtFieldValue1.Text = objCoyField1._Field_Data
            cboAllocations.SelectedValue = IIf(objCoyField1._Ownerrefid <= 0, 1, objCoyField1._Ownerrefid)
            cboPerspective.SelectedValue = objCoyField1._Perspectiveunkid
            If objCoyField1._Startdate <> Nothing Then
                dtpStartDate.Value = objCoyField1._Startdate
                dtpStartDate.Checked = True
            End If
            cboStatus.SelectedValue = IIf(objCoyField1._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objCoyField1._Statusunkid)
            txtWeight.Decimal = CDec(objCoyField1._Weight)
            If menAction = enAction.EDIT_ONE Then
                Dim objCoyInfoField As New clsassess_coyinfofield_tran
                mdicFieldData = objCoyInfoField.Get_Data(mintCoyField1Unkid, enWeight_Types.WEIGHT_FIELD1)
                If mdicFieldData.Keys.Count > 0 Then
                    If mdicFieldData.ContainsKey(CInt(txtRemark1.Tag)) Then
                        txtRemark1.Text = mdicFieldData(CInt(txtRemark1.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark2.Tag)) Then
                        txtRemark2.Text = mdicFieldData(CInt(txtRemark2.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark3.Tag)) Then
                        txtRemark3.Text = mdicFieldData(CInt(txtRemark3.Tag))
                    End If
                End If
                objCoyInfoField = Nothing
            End If


            iOwnerRefId = objCoyField1.GetOwnerRefId
            If iOwnerRefId > 0 Then
                cboAllocations.SelectedValue = iOwnerRefId : cboAllocations.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            If CInt(cboPerspective.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Perspective is mandatory information. Please select Perspective to continue."), enMsgBoxStyle.Information)
                cboPerspective.Focus()
                Return False
            End If

            If txtFieldValue1.Text.Trim.Length <= 0 Then
                Dim iMsg As String = String.Empty
                iMsg = Language.getMessage(mstrModuleName, 2, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName, 3, " is mandatory information. Please provide ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName, 4, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                txtFieldValue1.Focus()
                Return False
            End If

            If mintFieldUnkid = mintLinkedFieldId Then

                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Weight is mandatory information. Please provide Weight to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If

                If txtWeight.Decimal > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    Dim iMsg As String = ""
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_COMPANY_LEVEL, enWeight_Types.WEIGHT_FIELD1, txtWeight.Decimal, mintSelectedPeriodId, mintLinkedFieldId, 0, 0, menAction, mintCoyField1Unkid)
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                        txtWeight.Focus()
                        Return False
                    End If
                End If
                

                'If txtWeight.Decimal > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100."), enMsgBoxStyle.Information)
                '    txtWeight.Focus()
                '    Return False
                'End If

                If CInt(cboStatus.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Status is mandatory information. Please select Status to continue."), enMsgBoxStyle.Information)
                    cboStatus.Focus()
                    Return False
                End If
                If lvAllocation.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Goal Owner mandatory information. Please check atleast one Goal Owner to continue."), enMsgBoxStyle.Information)
                    lvAllocation.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmAddEditCoyField1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCoyField1 = New clsassess_coyfield1_master
        objCoyOwner = New clsassess_coyowner_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call SetColor()
            Call Set_Form_Information()
            If menAction = enAction.EDIT_ONE Then
                objCoyField1._Coyfield1unkid = mintCoyField1Unkid
                cboAllocations.Enabled = False
            End If
            mdtOwner = objCoyOwner.Get_Data(mintCoyField1Unkid, enWeight_Types.WEIGHT_FIELD1)
            Call FillCombo()
            Call Fill_Data()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCoyField1_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAddEditCoyField2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCoyField1 = Nothing : objCoyOwner = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCoyField1_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_coyfield1_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_coyfield1_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

            Me.Text = Language.getMessage(mstrModuleName, 10, "Add/Edit Company") & " " & objFieldMaster._Field1_Caption & " " & _
                      Language.getMessage(mstrModuleName, 11, "Information")

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim iblnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub
            Call SetValue()
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objCoyField1.Update(mdtOwner, mdicFieldData)
            Else
                iblnFlag = objCoyField1.Insert(mdtOwner, mdicFieldData)
            End If
            If iblnFlag = False Then
                If objCoyField1._Message <> "" Then
                    eZeeMsgBox.Show(objCoyField1._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, problem in saving Company Goals."), enMsgBoxStyle.Information)
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Company Goals are saved successfully."), enMsgBoxStyle.Information)
                If menAction = enAction.ADD_CONTINUE Then
                    objCoyField1 = New clsassess_coyfield1_master
                    objCoyOwner = New clsassess_coyowner_tran
                    mdtOwner.Rows.Clear()
                    objchkAll.Checked = False
                    Call GetValue()
                    txtRemark1.Text = "" : txtRemark2.Text = "" : txtRemark3.Text = ""
                    If mintSelectedPeriodId > 0 Then
                        cboPeriod.SelectedValue = mintSelectedPeriodId
                    End If
                Else
                    Call btnClose_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [27-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : PROVIDED SEARCHING, AS PACRA HAVING MANY PERSPECTIVES
    Private Sub objbtnSearchPerspective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPerspective.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboPerspective.ValueMember
                .DisplayMember = cboPerspective.DisplayMember
                .DataSource = CType(cboPerspective.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPerspective.SelectedValue = .SelectedValue
                    cboPerspective.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPerspective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [27-MAR-2017] -- END

#End Region

#Region " Controls Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then mintSelectedPeriodId = CInt(cboPeriod.SelectedValue)
            Dim objMapping As New clsAssess_Field_Mapping
            mintLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            If mintLinkedFieldId = mintFieldUnkid Then
                objpnlData.Enabled = True
                Call Set_Form_Information()
            Else
                objtabcRemarks.Enabled = False
                objpnlData.Enabled = False
            End If
            objMapping = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            objcolhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
                lvFoundItem.EnsureVisible()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = objchkAll.Checked
                Call GoalOwnerOperation(CInt(LItem.Tag), LItem.Checked)
            Next
            If iOwnerRefId <= 0 Then
                If lvAllocation.CheckedItems.Count <= 0 Then
                    cboAllocations.Enabled = True
                Else
                    cboAllocations.Enabled = False
                End If
            End If
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                cboAllocations.Enabled = True
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                cboAllocations.Enabled = False
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                cboAllocations.Enabled = False
                objchkAll.CheckState = CheckState.Checked
            End If
            Call GoalOwnerOperation(CInt(e.Item.Tag), e.Item.Checked)
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark1.TextChanged
        Try
            mdicFieldData(CInt(txtRemark1.Tag)) = txtRemark1.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark1_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark2.TextChanged
        Try
            mdicFieldData(CInt(txtRemark2.Tag)) = txtRemark2.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark2_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark3.TextChanged
        Try
            mdicFieldData(CInt(txtRemark3.Tag)) = txtRemark3.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark3_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblGoalOwner.Text = Language._Object.getCaption(Me.lblGoalOwner.Name, Me.lblGoalOwner.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Perspective is mandatory information. Please select Perspective to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry,")
			Language.setMessage(mstrModuleName, 3, " is mandatory information. Please provide")
			Language.setMessage(mstrModuleName, 4, " to continue.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Status is mandatory information. Please select Status to continue.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Goal Owner mandatory information. Please check atleast one Goal Owner to continue.")
			Language.setMessage(mstrModuleName, 7, "Sorry, problem in saving Company Goals.")
			Language.setMessage(mstrModuleName, 8, "Company Goals are saved successfully.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Weight is mandatory information. Please provide Weight to continue.")
            Language.setMessage(mstrModuleName, 10, "Add/Edit Company")
			Language.setMessage(mstrModuleName, 11, "Information")
			Language.setMessage(mstrModuleName, 12, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class