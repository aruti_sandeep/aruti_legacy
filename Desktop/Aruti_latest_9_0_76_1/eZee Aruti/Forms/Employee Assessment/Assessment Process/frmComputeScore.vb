﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

#End Region

Public Class frmComputeScore

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmComputeScore"
    Private Thread1 As Threading.Thread
    Private ThreadSt As Threading.ThreadStart
    Private objComputeScore As New clsComputeScore_master
    Private objCompute As New clsassess_computation_master
    Private mdtView As DataView
    Private mstrAdavanceFilter As String = ""
    Private dsTable As New DataSet
    Private dsScore As DataSet = Nothing
    Private dtFormula As DataTable = Nothing
    Private blnThread As Boolean = False

    Private mblnIsVoidProcess As Boolean = False
    Private mstrEmpids As String = String.Empty
    Private dtComputeVariable As DataTable
    Private xStringMatcingIn As String = String.Empty
    Private decBscWeight As Decimal = 0
    Private xStrGrps As String = String.Empty
    Private inttotalCount As Integer = 0
    Private intCount As Integer = 0
    Private strEmpAssGrps As String = String.Empty
    Private objAssessGrp As New clsassess_group_master
    Private xFinalTotalScore As Decimal = 0
    Private strMessage As String = ""
    Private dtComputeTran As DataTable = Nothing
    Private blnSummary As Boolean = False
    Private intComputeMstId As Integer = 0
    Private xTotalScore As Decimal = 0
    Private objEvalution As New clsevaluation_analysis_master
    Private mblnCancelWorker As Boolean = False

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Try

            dsCombo = (New clscommom_period_Tran).getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            'dsCombo = objComputeScore.GetAssessMode("List", True, ConfigParameter._Object._IsCompanyNeedReviewer)
            dsCombo = objComputeScore.GetAssessMode("List", True, ConfigParameter._Object._IsCompanyNeedReviewer, ConfigParameter._Object._IsCalibrationSettingActive)
            'S.SANDEEP |16-AUG-2019| -- END
            With cboSelectMode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges 
    Private Sub SetVisibility()
        Try
            btnProcess.Enabled = User._Object.Privilege._AllowToPerformComputeScoreProcess
            btnVoid.Enabled = User._Object.Privilege._AllowToPerformVoidComputedScore
            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            btnSubmit.Visible = False
            'S.SANDEEP |16-AUG-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End



    Private Sub FillList()

        Try
            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            'dsTable = objComputeScore.GetList(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                  "Emp", CInt(cboPeriod.SelectedValue), _
            '                                  chkIncludeUncommitedAssessment.Checked, mstrAdavanceFilter)
            Dim blnIsForCalibrationSubmit = False
            If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.SUBMIT_CALIBRATION Then
                blnIsForCalibrationSubmit = True
            End If
            dsTable = objComputeScore.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                              "Emp", CInt(cboPeriod.SelectedValue), _
                                              chkIncludeUncommitedAssessment.Checked, mstrAdavanceFilter, _
                                              blnIsForCalibrationSubmit, _
                                              CBool(IIf(ConfigParameter._Object._IsCompanyNeedReviewer = True, True, False)))
            'S.SANDEEP |16-AUG-2019| -- END

            Dim dCol As New DataColumn
            dCol.ColumnName = "message"
            dCol.DataType = Type.GetType("System.String")
            dCol.DefaultValue = ""
            dsTable.Tables(0).Columns.Add(dCol)

            mdtView = dsTable.Tables(0).DefaultView

            dgvAEmployee.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgchkEmpProcess.DataPropertyName = "E_IsProcess"
            objdgchkASRProcess.DataPropertyName = "A_IsProcess"
            objdgchkREVProcess.DataPropertyName = "R_IsProcess"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolheEMstID.DataPropertyName = "E_MstID"
            objdgcolheAMstID.DataPropertyName = "A_MstID"
            objdgcolheRMstID.DataPropertyName = "R_MstID"
            objdgcolhMessage.DataPropertyName = "message"
            'S.SANDEEP |10-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            objdgcolhiRead.DataPropertyName = "iRead"
            objdgcolhiMsg.DataPropertyName = "iMsg"
            'S.SANDEEP |10-OCT-2019| -- END

            dgvAEmployee.DataSource = mdtView

            If ConfigParameter._Object._IsCompanyNeedReviewer = False Then
                objdgchkREVProcess.Visible = False
            End If

            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            'If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.SUBMIT_CALIBRATION Then
            '    'S.SANDEEP |22-NOV-2019| -- START
            '    'ISSUE/ENHANCEMENT : Calibration Issues
            '    '    objdgcolhECheck.Visible = False : objchkEmployee.Visible = False
            '    '    lnkAllocation.Visible = False
            '    'Else
            '    '    objdgcolhECheck.Visible = True : objchkEmployee.Visible = True
            '    '    lnkAllocation.Visible = True
                '    lnkAllocation.Visible = False
                'Else
                '    lnkAllocation.Visible = True
            '    'S.SANDEEP |22-NOV-2019| -- END
            'End If
            'S.SANDEEP |16-AUG-2019| -- END

            'S.SANDEEP |10-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            Dim dgvRows As IEnumerable(Of DataGridViewRow) = dgvAEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CInt(x.Cells(objdgcolhiRead.Index).Value) = 1)
            If dgvRows IsNot Nothing AndAlso dgvRows.Count > 0 Then
                For Each dgvRow In dgvRows
                    dgvRow.Cells(objdgcolhMessage.Index).Value = dgvRow.Cells(objdgcolhiMsg.Index).Value.ToString()
                    dgvRow.DefaultCellStyle.ForeColor = Color.Red
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Red
                    dgvRow.DefaultCellStyle.Font = New Font(dgvAEmployee.Font, FontStyle.Bold)
                    dgvRow.Cells(objdgcolhECheck.Index).ToolTipText = dgvRow.Cells(objdgcolhiMsg.Index).Value.ToString()
                    dgvRow.Cells(dgcolhEcode.Index).ToolTipText = dgvRow.Cells(objdgcolhiMsg.Index).Value.ToString()
                    dgvRow.Cells(dgcolhEName.Index).ToolTipText = dgvRow.Cells(objdgcolhiMsg.Index).Value.ToString()
                    dgvRow.Cells(objdgcolhMessage.Index).ToolTipText = dgvRow.Cells(objdgcolhiMsg.Index).Value.ToString()
                    dgvRow.ReadOnly = True
                Next
            End If
            'S.SANDEEP |10-OCT-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub ApplyFilter()
        Dim strFilter As String = ""
        Try
            If mdtView Is Nothing Then Exit Sub

            'If chkIncludeUncommitedAssessment.Enabled Then
            '    Select Case CInt(cboSelectMode.SelectedValue)
            '        Case clsComputeScore_master.enAssessMode.SELF_ASSESSMENT
            '            strFilter &= "AND E_Commited = '" & chkIncludeUncommitedAssessment.Checked & "' "
            '        Case clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT
            '            strFilter &= "AND A_Commited = '" & chkIncludeUncommitedAssessment.Checked & "' "
            '        Case clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT
            '            strFilter &= "AND R_Commited = '" & chkIncludeUncommitedAssessment.Checked & "' "
            '        Case clsComputeScore_master.enAssessMode.ALL_ASSESSMENT
            '            strFilter &= "AND E_Commited = '" & chkIncludeUncommitedAssessment.Checked & "' "
            '            strFilter &= "AND A_Commited = '" & chkIncludeUncommitedAssessment.Checked & "' "
            '            strFilter &= "AND R_Commited = '" & chkIncludeUncommitedAssessment.Checked & "' "
            '    End Select
            'End If

            If txtSearchEmp.Text.Trim.Length > 0 Then
                strFilter &= "employeecode LIKE '%" & txtSearchEmp.Text.Trim & "%' OR employeename LIKE '%" & txtSearchEmp.Text.Trim & "%' "
            End If
            'strFilter = strFilter.Substring(3)

            mdtView.RowFilter = strFilter
            dgvAEmployee.DataSource = mdtView

            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If Me.dgvAEmployee.IsCurrentCellDirty Then
                Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            Dim intRowCount = dgvAEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells("objdgcolhECheck").Value) = True).Count

            If intRowCount = dgvAEmployee.RowCount Then
                objchkEmployee.CheckState = CheckState.Checked
            ElseIf intRowCount <= 0 Then
                objchkEmployee.CheckState = CheckState.Unchecked
            Else
                objchkEmployee.CheckState = CheckState.Indeterminate
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFilterString", mstrModuleName)
        End Try
    End Sub

    Private Function Is_Vaild_Data() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            If CInt(cboSelectMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Process Mode is mandatory information. Please select Process Mode to continue."), enMsgBoxStyle.Information)
                cboSelectMode.Focus()
                Return False
            End If

            If mdtView.Table.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isCheck") = True).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one of the employee to compute score process."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Vaild_Data", mstrModuleName)
        End Try
    End Function

    Private Sub ComputeScore()
        Try
            For Each dRow As DataRow In dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True)
                If mblnCancelWorker = True Then Exit For
                dsTable.Tables(0).AcceptChanges()
                Try
                    dgvAEmployee.Rows(dsTable.Tables(0).Rows.IndexOf(dRow)).Selected = True
                    dgvAEmployee.FirstDisplayedScrollingRowIndex = dsTable.Tables(0).Rows.IndexOf(dRow) - 10
                Catch ex As Exception
                End Try
                mdtView = dsTable.Tables(0).DefaultView

                strEmpAssGrps = ""
                For Each strGrp In xStrGrps.Split(CChar(","))
                    Dim strgrpid As String = strGrp
                    If dtFormula.AsEnumerable().Where(Function(x) x.Field(Of String)("formula_string").Contains(strgrpid)).Count > 0 Then
                        strEmpAssGrps = objAssessGrp.GetCSV_AssessGroupIds(CInt(dRow.Item("employeeunkid")), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboPeriod.SelectedValue))
                        Exit For
                    End If
                Next

                '**************Set Class Property Start***************
                objComputeScore._Employeeunkid = CInt(dRow.Item("employeeunkid"))
                objComputeScore._Periodunkid = CInt(cboPeriod.SelectedValue)
                objComputeScore._Scoretypeid = ConfigParameter._Object._ScoringOptionId
                objComputeScore._Userunkid = User._Object._Userunkid
                objComputeScore._Finaloverallscore = xFinalTotalScore
                objComputeScore._Computationdate = Now
                '**************Set Class Property END ***************
                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.SELF_ASSESSMENT OrElse _
                        CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then

                    If objComputeScore.isExist(CInt(dRow("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.SELF_ASSESSMENT) = True OrElse CInt(dRow.Item("E_Analysisid")) <= 0 Then
                        If CInt(dRow.Item("E_Analysisid")) <= 0 Then
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 4, "Self Assessment is not present")
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Compute score process already done")
                        End If

                    Else
                        dtComputeTran = objComputeScore.GetTranData(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.SELF_ASSESSMENT, strMessage, "List")
                        If dtComputeTran Is Nothing AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                            Continue For
                        End If

                        '************* SELF BSC SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If

                        '************* SELF GE SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If

                        '************* SELF OVERALL SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        xTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If
                        objComputeScore._Analysisunkid = objEvalution.GetAnalusisUnkid(CInt(dRow.Item("employeeunkid")), enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                        objComputeScore._Assessmodeid = clsComputeScore_master.enAssessMode.SELF_ASSESSMENT

                        If dtComputeTran IsNot Nothing AndAlso dtComputeTran.Rows.Count > 0 Then
                            If objComputeScore.Insert(dtComputeTran) = False Then
                                dRow.Item("message") = objComputeScore._Message
                                Continue For
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                            End If
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Computation formula does not exist. Please define computation formula ")
                            Continue For
                        End If
                    End If
                End If

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT OrElse _
                        CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                    If objComputeScore.isExist(CInt(dRow("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.APPRAISER_ASSESSMENT) = True OrElse CInt(dRow.Item("A_Analysisid")) <= 0 Then
                        If CInt(dRow.Item("A_Analysisid")) <= 0 Then
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Assessor Assessment is not present")
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Compute score process already done")
                        End If
                    Else
                        dtComputeTran = objComputeScore.GetTranData(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT, strMessage, "List")
                        If dtComputeTran Is Nothing AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                            Continue For
                        End If

                        '************* ASSESSOR BSC SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If

                        '************* ASSESSOR GE SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If

                        '************* ASSESSOR OVERALL SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        'S.SANDEEP |26-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                        'If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.ASR_OVERALL_SCORE).Count > 0 Then
                            'S.SANDEEP |26-AUG-2019| -- END
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        xTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If

                        objComputeScore._Analysisunkid = objEvalution.GetAnalusisUnkid(CInt(dRow.Item("employeeunkid")), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                        objComputeScore._Assessmodeid = clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT
                        If dtComputeTran IsNot Nothing AndAlso dtComputeTran.Rows.Count > 0 Then
                            If objComputeScore.Insert(dtComputeTran) = False Then
                                dRow.Item("message") = objComputeScore._Message
                                Continue For
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                            End If
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Computation formula does not exist. Please define computation formula ")
                            Continue For
                        End If
                    End If
                End If

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT OrElse _
                        CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then

                    If objComputeScore.isExist(CInt(dRow("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT) = True OrElse CInt(dRow.Item("R_Analysisid")) <= 0 Then
                        If CInt(dRow.Item("R_Analysisid")) <= 0 Then
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Reviewer Assessment is not present")
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Compute score process already done")
                        End If
                    Else
                        dtComputeTran = objComputeScore.GetTranData(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT, strMessage, "List")
                        If dtComputeTran Is Nothing AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                            Continue For
                        End If

                        '************* REVIEWER BSC SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If

                        '************* REVIEWER GE SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If

                        '************* REVIEWER OVERALL SCORE*******************************************
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        'S.SANDEEP |26-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                        'If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.REV_OVERALL_SCORE).Count > 0 Then
                            'S.SANDEEP |26-AUG-2019| -- END
                            blnSummary = True
                        End If

                        xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.REV_OVERALL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        xTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.REV_OVERALL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        If intComputeMstId > 0 Then
                            Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                        End If


                        objComputeScore._Analysisunkid = objEvalution.GetAnalusisUnkid(CInt(dRow.Item("employeeunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                        objComputeScore._Assessmodeid = clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT

                        If dtComputeTran IsNot Nothing AndAlso dtComputeTran.Rows.Count > 0 Then
                            If objComputeScore.Insert(dtComputeTran) = False Then
                                dRow.Item("message") = objComputeScore._Message
                                Continue For
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                            End If
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Computation formula does not exist. Please define computation formula ")
                            'Continue For
                        End If
                    End If
                End If

                Dim bln As Boolean = False
                Dim strComputationFrm As String = ""
                If dtFormula IsNot Nothing AndAlso dtFormula.Rows.Count > 0 Then
                    Dim drRow = dtFormula.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.FINAL_RESULT_SCORE)
                    If drRow.Count > 0 Then
                        strComputationFrm = drRow(0).Item("computation_formula").ToString
                        Dim objCompute_tran As New clsassess_computation_tran
                        objCompute_tran._Computationunkid = CInt(drRow(0).Item("computationunkid"))
                        Dim dtComputTran As DataTable = objCompute_tran._DataTable
                        objCompute_tran = Nothing
                        xFinalTotalScore = 0
                        Dim intScore As Decimal = 0
                        For Each dtRow As DataRow In dtComputTran.Rows
                            If CInt(dtRow("computation_typeid")) > 1000 Then
                                Select Case CInt(dtRow("computation_typeid"))
                                    Case 10001
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE)
                                    Case 10002
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE)
                                    Case 10003
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE)
                                    Case 10004
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE)
                                    Case 10005
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE)
                                    Case 10006
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE)
                                    Case 10007
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.EMP_OVERALL_SCORE)
                                    Case 10008
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.ASR_OVERALL_SCORE)
                                    Case 10009
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.REV_OVERALL_SCORE)
                                End Select
                                Dim strtype As String = "#" & dtRow("computation_typeid").ToString & "#"
                                strComputationFrm = strComputationFrm.Replace(strtype, intScore.ToString)
                                bln = True
                            Else
                                bln = False
                                Exit For
                            End If
                        Next
                    End If
                End If
                If bln Then
                    xFinalTotalScore = (New clsFomulaEvaluate).Eval(strComputationFrm)
                End If

                If bln = False Then
                    If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                        blnSummary = True
                    End If

                    xFinalTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)

                    If xFinalTotalScore <= 0 Then
                        xFinalTotalScore += GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    End If

                    If xFinalTotalScore <= 0 Then
                        xFinalTotalScore += GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    End If
                End If

                If objComputeScore.Update_Final_score(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), xFinalTotalScore) = False Then
                    dRow.Item("message") = objComputeScore._Message
                    Continue For
                End If

                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 40
                xFinalTotalScore = 0
                If dtFormula IsNot Nothing AndAlso dtFormula.Rows.Count > 0 Then
                    Dim drRow = dtFormula.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.AVG_FINAL_RESULT_SCORE)
                    If drRow.Count > 0 Then
                        'UpdateAvgFinalScore
                        blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If
                        strComputationFrm = drRow(0).Item("computation_formula").ToString
                        Dim objCompute_tran As New clsassess_computation_tran
                        objCompute_tran._Computationunkid = CInt(drRow(0).Item("computationunkid"))
                        Dim dtComputTran As DataTable = objCompute_tran._DataTable
                        objCompute_tran = Nothing
                        Dim intScore As Decimal = 0
                        For Each dtRow As DataRow In dtComputTran.Rows
                            If CInt(dtRow("computation_typeid")) > 1000 Then
                                Select Case CInt(dtRow("computation_typeid"))
                                    Case 10001
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE)
                                    Case 10002
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE)
                                    Case 10003
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE)
                                    Case 10004
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE)
                                    Case 10005
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE)
                                    Case 10006
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE)
                                    Case 10007
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.EMP_OVERALL_SCORE)
                                    Case 10008
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.ASR_OVERALL_SCORE)
                                    Case 10009
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.REV_OVERALL_SCORE)
                                    Case 100011
                                        intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.FINAL_RESULT_SCORE)
                                End Select
                                Dim strtype As String = "#" & dtRow("computation_typeid").ToString & "#"
                                strComputationFrm = strComputationFrm.Replace(strtype, intScore.ToString)
                            End If
                        Next
                        xFinalTotalScore = objCompute.Get_All_Formula_Expression(strComputationFrm, 0, CInt(dRow.Item("employeeunkid")), ConfigParameter._Object._ScoringOptionId, 0, CType(cboSelectMode.SelectedValue, enAssessmentMode), 0, "", ConfigParameter._Object._Self_Assign_Competencies)
                        If objComputeScore.UpdateAvgFinalScore(CInt(drRow(0)("formula_typeid")), CInt(cboPeriod.SelectedValue), CInt(dRow.Item("employeeunkid")), CInt(drRow(0)("computationunkid")), xFinalTotalScore) = False Then
                            dRow.Item("message") = objComputeScore._Message
                            Continue For
                        End If
                    End If
                End If
                'S.SANDEEP [29-NOV-2017] -- END

                intCount += 1
                bgwCmputeProcess.ReportProgress(intCount)

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ComputeScore", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub VoidComputedScores()
        Dim bln As Boolean = True
        Try
            inttotalCount = dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsCheck") = True).Count
            intCount = 0 : strMessage = ""
            For Each dRow As DataRow In dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsCheck") = True)
                If mblnCancelWorker = True Then Exit For
                dsTable.Tables(0).AcceptChanges()
                mdtView = dsTable.Tables(0).DefaultView
                Try
                    dgvAEmployee.Rows(dsTable.Tables(0).Rows.IndexOf(dRow)).Selected = True
                    dgvAEmployee.FirstDisplayedScrollingRowIndex = dsTable.Tables(0).Rows.IndexOf(dRow) - 10
                Catch ex As Exception
                End Try

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.SELF_ASSESSMENT OrElse CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                    If CBool(dRow("E_IsProcess")) = False Then
                        dRow("message") = Language.getMessage(mstrModuleName, 11, "Self score computation is not present")
                    Else
                        bln = objComputeScore.Delete(CInt(dRow.Item("E_MstID").ToString), strMessage)
                        If bln = False AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                        End If
                    End If
                End If

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT OrElse CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                    If CBool(dRow("A_IsProcess")) = False Then
                        dRow("message") = Language.getMessage(mstrModuleName, 12, "Assessor score computation is not present")
                    Else
                        bln = objComputeScore.Delete(CInt(dRow.Item("A_MstID").ToString), strMessage)
                        If bln = False AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                        End If
                    End If
                End If

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT OrElse CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                    If CBool(dRow("R_IsProcess")) = False Then
                        dRow("message") = Language.getMessage(mstrModuleName, 13, "Reviewer score computation is not present")
                    Else
                        bln = objComputeScore.Delete(CInt(dRow.Item("R_MstID").ToString), strMessage)
                        If bln = False AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                        End If
                    End If
                End If

                intCount += 1
                bgwCmputeProcess.ReportProgress(intCount)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VoidComputedScores", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub EmployeeComputeScoreProcess()
        Dim dtComputeTran As DataTable = Nothing
        Dim xTotalScore As Decimal = 0
        Dim xFinalTotalScore As Decimal = 0
        Dim inttotalCount As Integer = 0
        Dim intCount As Integer = 0
        Dim blnSummary As Boolean = False
        Dim intComputeMstId As Integer = 0
        Dim strMessage As String = ""
        Dim decBscWeight As Decimal = 0
        Dim strEmpAssGrps As String = ""
        Dim objAssessGrp As New clsassess_group_master
        Dim dtComputeVariable As DataTable = Nothing
        Dim objEvalution As New clsevaluation_analysis_master
        Dim mstrEmpids As String = ""
        Try
            If dsTable Is Nothing Then Exit Sub
            If dsTable.Tables(0).Rows.Count <= 0 Then Exit Sub


            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            'dsScore = objComputeScore.GetAssessmentScore(CInt(cboPeriod.SelectedValue), mstrEmpids, "List")
            dsScore = objComputeScore.GetAssessmentScore(CInt(cboPeriod.SelectedValue), mstrEmpids, ConfigParameter._Object._IsUseAgreedScore, "List")
            'Shani (23-Nov123-2016-2016) -- End


            dtFormula = objCompute.GetFormulaStrings(CInt(cboPeriod.SelectedValue))

            mstrEmpids = String.Join(",", dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isCheck") = True).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())

            If dsScore IsNot Nothing AndAlso dsScore.Tables(0).Rows.Count > 0 Then

                dtComputeVariable = objCompute.Get_Computation_TranVariables(CInt(cboPeriod.SelectedValue), -1)

                Dim xStringMatcingIn As String = enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'objCompute.Get_All_Compution_value(CInt(cboPeriod.SelectedValue), _
                '                                   ConfigParameter._Object._ScoringOptionId, _
                '                                   mstrEmpids, _
                '                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                objCompute.Get_All_Compution_value(CInt(cboPeriod.SelectedValue), _
                                                   ConfigParameter._Object._ScoringOptionId, _
                                                   mstrEmpids, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsUseAgreedScore, _
                                                   ConfigParameter._Object._Self_Assign_Competencies)
                'Shani (23-Nov-2016) -- End

                Dim objFMapping As New clsAssess_Field_Mapping
                Dim xMappingUnkid As Integer = objFMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))

                If xMappingUnkid > 0 Then
                    objFMapping._Mappingunkid = xMappingUnkid
                    decBscWeight = objFMapping._Weight
                End If
                objFMapping = Nothing

                Dim xStrGrps As String = "#" & enAssess_Computation_Types.CMP_OVERALL_WEIGHT & "#," & _
                                         "#" & enAssess_Computation_Types.CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT & "#," & _
                                         "#" & enAssess_Computation_Types.CMP_CATEGORY_ALL_GROUP_COUNT & "#"

                inttotalCount = dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count
                intCount = 0
                'Shani [16-Jan-2017] -- START
                'Issue : TRA Compute Process Take Time 
                'frmNewMDI.tmrReminder.Stop()
                'frmNewMDI.Enabled = False
                'Shani [16-Jan-2017] -- END
                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                For Each dRow As DataRow In dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True)
                    '************* Starting Process Start*******************************************
                    intCount = intCount + 1
                    lblProcess.Text = "Total Record " & intCount & " of " & inttotalCount
                    dsTable.Tables(0).AcceptChanges()
                    Try
                        dgvAEmployee.Rows(dsTable.Tables(0).Rows.IndexOf(dRow)).Selected = True
                        dgvAEmployee.FirstDisplayedScrollingRowIndex = dsTable.Tables(0).Rows.IndexOf(dRow) - 10
                    Catch ex As Exception
                    End Try
                    mdtView = dsTable.Tables(0).DefaultView
                    Application.DoEvents()


                    'Shani (20-Sep-2016) -- Start
                    'If dtFormula.AsEnumerable().Where(Function(x) x.Field(Of String)("formula_string").Contains("#" & xStrGrps & "#")).Count > 0 Then
                    '    strEmpAssGrps = objAssessGrp.GetCSV_AssessGroupIds(CInt(dRow.Item("employeeunkid")), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    'End If
                    For Each strGrp In xStrGrps.Split(CChar(","))
                        Dim strgrpid As String = strGrp
                        If dtFormula.AsEnumerable().Where(Function(x) x.Field(Of String)("formula_string").Contains(strgrpid)).Count > 0 Then
                            'S.SANDEEP [19 DEC 2016] -- START
                            'strEmpAssGrps = objAssessGrp.GetCSV_AssessGroupIds(CInt(dRow.Item("employeeunkid")), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                            strEmpAssGrps = objAssessGrp.GetCSV_AssessGroupIds(CInt(dRow.Item("employeeunkid")), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboPeriod.SelectedValue))
                            'S.SANDEEP [19 DEC 2016] -- END
                            Exit For
                        End If
                    Next
                    'Shani (20-Sep-2016) -- End

                    '************* Starting Process End *******************************************

                    '************* Final OVERALL SCORE Start*******************************************
                    blnSummary = False : xFinalTotalScore = 0 : intComputeMstId = 0

                    'Shani [16-Jan-2017] -- START
                    'Issue : TRA Compute Process Take Time 
                    'If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                    '    blnSummary = True
                    'End If

                    'xFinalTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    'xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)

                    'If xFinalTotalScore <= 0 Then
                    '    xFinalTotalScore += GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    '    xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    'End If

                    'If xFinalTotalScore <= 0 Then
                    '    xFinalTotalScore += GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    '    xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                    'End If
                    'Shani [16-Jan-2017] -- END

                    '************* Final OVERALL SCORE End*******************************************

                    '**************Set Class Property Start***************
                    objComputeScore._Employeeunkid = CInt(dRow.Item("employeeunkid"))
                    objComputeScore._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objComputeScore._Scoretypeid = ConfigParameter._Object._ScoringOptionId
                    objComputeScore._Userunkid = User._Object._Userunkid
                    objComputeScore._Finaloverallscore = xFinalTotalScore
                    objComputeScore._Computationdate = Now
                    '**************Set Class Property END ***************

                    If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.SELF_ASSESSMENT OrElse _
                        CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then

                        If objComputeScore.isExist(CInt(dRow("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.SELF_ASSESSMENT) = True OrElse CInt(dRow.Item("E_Analysisid")) <= 0 Then
                            If CInt(dRow.Item("E_Analysisid")) <= 0 Then
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 4, "Self Assessment is not present")
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Compute score process already done")
                            End If

                        Else
                            dtComputeTran = objComputeScore.GetTranData(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.SELF_ASSESSMENT, strMessage, "List")
                            If dtComputeTran Is Nothing AndAlso strMessage <> "" Then
                                dRow.Item("message") = strMessage
                                Continue For
                            End If

                            '************* SELF BSC SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE).Count > 0 Then
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If

                            '************* SELF GE SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE).Count > 0 Then
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If

                            '************* SELF OVERALL SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            xTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If
                            objComputeScore._Analysisunkid = objEvalution.GetAnalusisUnkid(CInt(dRow.Item("employeeunkid")), enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                            objComputeScore._Assessmodeid = clsComputeScore_master.enAssessMode.SELF_ASSESSMENT

                            If dtComputeTran IsNot Nothing AndAlso dtComputeTran.Rows.Count > 0 Then
                                If objComputeScore.Insert(dtComputeTran) = False Then
                                    dRow.Item("message") = objComputeScore._Message
                                    Continue For
                                Else
                                    dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                                End If
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Computation formula does not exist. Please define computation formula ")
                                Continue For
                            End If
                        End If
                    End If

                    If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT OrElse _
                        CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                        If objComputeScore.isExist(CInt(dRow("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.APPRAISER_ASSESSMENT) = True OrElse CInt(dRow.Item("A_Analysisid")) <= 0 Then
                            If CInt(dRow.Item("A_Analysisid")) <= 0 Then
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Assessor Assessment is not present")
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Compute score process already done")
                            End If
                        Else
                            dtComputeTran = objComputeScore.GetTranData(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT, strMessage, "List")
                            If dtComputeTran Is Nothing AndAlso strMessage <> "" Then
                                dRow.Item("message") = strMessage
                                Continue For
                            End If

                            '************* ASSESSOR BSC SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE).Count > 0 Then
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If

                            '************* ASSESSOR GE SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE).Count > 0 Then
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If

                            '************* ASSESSOR OVERALL SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            'S.SANDEEP |26-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                            'If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.ASR_OVERALL_SCORE).Count > 0 Then
                                'S.SANDEEP |26-AUG-2019| -- END
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            xTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If

                            objComputeScore._Analysisunkid = objEvalution.GetAnalusisUnkid(CInt(dRow.Item("employeeunkid")), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                            objComputeScore._Assessmodeid = clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT
                            If dtComputeTran IsNot Nothing AndAlso dtComputeTran.Rows.Count > 0 Then
                                If objComputeScore.Insert(dtComputeTran) = False Then
                                    dRow.Item("message") = objComputeScore._Message
                                    Continue For
                                Else
                                    dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                                End If
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Computation formula does not exist. Please define computation formula ")
                                Continue For
                            End If
                        End If
                    End If

                    If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT OrElse _
                        CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then

                        If objComputeScore.isExist(CInt(dRow("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT) = True OrElse CInt(dRow.Item("R_Analysisid")) <= 0 Then
                            If CInt(dRow.Item("R_Analysisid")) <= 0 Then
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Reviewer Assessment is not present")
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Compute score process already done")
                            End If
                        Else
                            dtComputeTran = objComputeScore.GetTranData(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT, strMessage, "List")
                            If dtComputeTran Is Nothing AndAlso strMessage <> "" Then
                                dRow.Item("message") = strMessage
                                Continue For
                            End If

                            '************* REVIEWER BSC SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE).Count > 0 Then
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If

                            '************* REVIEWER GE SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE).Count > 0 Then
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If

                            '************* REVIEWER OVERALL SCORE*******************************************
                            blnSummary = False : xTotalScore = 0 : intComputeMstId = 0
                            'S.SANDEEP |26-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                            'If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                            If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.REV_OVERALL_SCORE).Count > 0 Then
                                'S.SANDEEP |26-AUG-2019| -- END
                                blnSummary = True
                            End If

                            xTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.REV_OVERALL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            xTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.REV_OVERALL_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            If intComputeMstId > 0 Then
                                Call Insert_Update_Delete_Tran(dtComputeTran, intComputeMstId, xTotalScore)
                            End If


                            objComputeScore._Analysisunkid = objEvalution.GetAnalusisUnkid(CInt(dRow.Item("employeeunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                            objComputeScore._Assessmodeid = clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT

                            If dtComputeTran IsNot Nothing AndAlso dtComputeTran.Rows.Count > 0 Then
                                If objComputeScore.Insert(dtComputeTran) = False Then
                                    dRow.Item("message") = objComputeScore._Message
                                    Continue For
                                Else
                                    dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                                End If
                            Else
                                dRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Computation formula does not exist. Please define computation formula ")
                                Continue For
                            End If
                        End If
                    End If

                    'Shani [16-Jan-2017] -- START
                    'Issue : TRA Compute Process Take Time 
                    Dim bln As Boolean = False
                    Dim strComputationFrm As String = ""
                    If dtFormula IsNot Nothing AndAlso dtFormula.Rows.Count > 0 Then
                        Dim drRow = dtFormula.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.FINAL_RESULT_SCORE)
                        If drRow.Count > 0 Then
                            strComputationFrm = drRow(0).Item("computation_formula").ToString
                            Dim objCompute_tran As New clsassess_computation_tran
                            objCompute_tran._Computationunkid = CInt(drRow(0).Item("computationunkid"))
                            Dim dtComputTran As DataTable = objCompute_tran._DataTable
                            objCompute_tran = Nothing
                            xFinalTotalScore = 0
                            Dim intScore As Decimal = 0
                            For Each dtRow As DataRow In dtComputTran.Rows
                                If CInt(dtRow("computation_typeid")) > 1000 Then
                                    Select Case CInt(dtRow("computation_typeid"))
                                        Case 10001
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE)
                                        Case 10002
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE)
                                        Case 10003
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE)
                                        Case 10004
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE)
                                        Case 10005
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE)
                                        Case 10006
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE)
                                        Case 10007
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.EMP_OVERALL_SCORE)
                                        Case 10008
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.ASR_OVERALL_SCORE)
                                        Case 10009
                                            intScore = objComputeScore.Get_Formula_value(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssess_Computation_Formulas.REV_OVERALL_SCORE)
                                    End Select
                                    Dim strtype As String = "#" & dtRow("computation_typeid").ToString & "#"
                                    strComputationFrm = strComputationFrm.Replace(strtype, intScore.ToString)
                                    bln = True
                                Else
                                    bln = False
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    If bln Then
                        xFinalTotalScore = (New clsFomulaEvaluate).Eval(strComputationFrm)
                    End If

                    If bln = False Then
                        If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.EMP_OVERALL_SCORE).Count > 0 Then
                            blnSummary = True
                        End If

                        xFinalTotalScore = GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.SELF_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)

                        If xFinalTotalScore <= 0 Then
                            xFinalTotalScore += GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.APPRAISER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        End If

                        If xFinalTotalScore <= 0 Then
                            xFinalTotalScore += GetScore(True, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                            xFinalTotalScore += GetScore(False, CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._ScoringOptionId, blnSummary, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, enAssessmentMode.REVIEWER_ASSESSMENT, decBscWeight, strEmpAssGrps, intComputeMstId)
                        End If
                    End If

                    If objComputeScore.Update_Final_score(CInt(dRow.Item("employeeunkid")), CInt(cboPeriod.SelectedValue), xFinalTotalScore) = False Then
                        dRow.Item("message") = objComputeScore._Message
                        Continue For
                    End If

                    'Shani [16-Jan-2017] -- END


                    If blnThread = True Then
                        Exit For
                    End If
                Next
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Successful computation score Process done"), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EmployeeComputeScoreProcess", mstrModuleName)
        Finally
            'Shani [16-Jan-2017] -- START
            'Issue : TRA Compute Process Take Time 
            'AddHandler frmNewMDI.tmrReminder.Tick, AddressOf frmNewMDI.tmrReminder_Tick
            'Shani [16-Jan-2017] -- END
            btnStop.Visible = False
            btnClose.Visible = True
            Try
                Thread1.Interrupt()
            Catch ex As Exception
            End Try
            'Shani [16-Jan-2017] -- START
            'Issue : TRA Compute Process Take Time 
            'frmNewMDI.Enabled = True
            'frmNewMDI.tmrReminder.Start()

            'S.SANDEEP [22-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
            'AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'S.SANDEEP [22-MAR-2017] -- END



            'Shani [16-Jan-2017] -- END
        End Try
    End Sub

    Private Function GetScore(ByVal xIsBSC As Boolean, _
                              ByVal xEmpId As Integer, _
                              ByVal xPeriodId As Integer, _
                              ByVal xScoreOptId As Integer, _
                              ByVal blnSummary As Boolean, _
                              ByVal enFormula As enAssess_Computation_Formulas, _
                              ByVal enAssMode As enAssessmentMode, _
                              ByVal decBSC_OverAllWeight As Decimal, _
                              ByVal strArrGrps As String, _
                              ByRef intComputeMstId As Integer) As Decimal
        Dim xScore As Decimal = 0
        Dim xTotalScore As Decimal = 0
        Dim xFormulaRef As String = ""
        Dim dRow() As DataRow = Nothing
        Dim xStrFormula As String = ""
        Try
            intComputeMstId = 0
            If dtFormula IsNot Nothing AndAlso dtFormula.Rows.Count > 0 Then
                Dim drRow = dtFormula.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enFormula)
                If drRow.Count > 0 Then
                    xStrFormula = drRow(0).Item("formula_string").ToString
                    intComputeMstId = CInt(drRow(0).Item("computationunkid"))
                End If

            End If

            ''S.SANDEEP |26-AUG-2019| -- START
            ''ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            Dim blnISummryFormulaUsed As Boolean = False
            'If blnSummary = False Then
            '    Dim objCmpTran As New clsassess_computation_tran
            '    Dim dtran As New DataTable
            '    objCmpTran._Computationunkid = intComputeMstId
            '    dtran = objCmpTran._DataTable.Copy()
            '    If dtran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("computation_typeid").ToString().StartsWith("1000") = True).Count > 0 Then
            '        For Each iRow As DataRow In dtran.Rows
            '            Dim dtmp() As DataRow = dtFormula.Select("formula_typeid = '" & iRow("computation_typeid").ToString.Replace("1000", "") & "'")
            '            If dtmp.Length > 0 Then
            '                If dtComputeVariable.AsEnumerable().Where(Function(x) xStringMatcingIn.Split(CChar(",")).Contains(x.Field(Of Integer)("computation_typeid").ToString) And x.Field(Of Integer)("formula_typeid") = CInt(dtmp(0)("formula_typeid"))).Count > 0 Then
            '                    blnSummary = True
            '                End If
            '            End If
            '        Next
            '    End If
            '    objCmpTran = Nothing
            'End If
            'S.SANDEEP |26-AUG-2019| -- END

            'S.SANDEEP |09-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
            'If dsScore IsNot Nothing AndAlso dsScore.Tables(0).Rows.Count > 0 AndAlso xStrFormula.Trim.Length > 0 Then

            '    If xIsBSC Then
            '        dRow = dsScore.Tables(0).Select("employeeunkid = " & xEmpId & " AND assessmodeid = " & enAssMode & " AND assessgroupunkid <= 0 ")
            '    Else
            '        dRow = dsScore.Tables(0).Select("employeeunkid = " & xEmpId & " AND assessmodeid = " & enAssMode & " AND assessgroupunkid > 0 ")
            '    End If

            '    Dim xitemsids = String.Join(",", dRow.AsEnumerable().Select(Function(x) x.Field(Of Integer)("iItemUnkid").ToString).ToArray)
            '    For Each xRow As DataRow In dRow
            '        If xIsBSC Then
            '            If blnSummary = True Then

            '                'Shani (23-Nov-2016) -- Start
            '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                'xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, 0, enAssMode, decBSC_OverAllWeight, strArrGrps)
            '                xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, 0, enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies)
            '                'Shani (23-Nov123-2016-2016) -- End


            '                xTotalScore = xTotalScore + xScore
            '                Exit For
            '            Else

            '                'Shani (23-Nov-2016) -- Start
            '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                'xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps)

            '                'S.SANDEEP |26-AUG-2019| -- START
            '                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            '                'xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies)
            '                xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, , blnISummryFormulaUsed)
            '                'S.SANDEEP |26-AUG-2019| -- END


            '                'Shani (23-Nov123-2016-2016) -- End


            '                xTotalScore = xTotalScore + xScore

            '                'S.SANDEEP |26-AUG-2019| -- START
            '                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            '                If blnISummryFormulaUsed Then
            '                    Exit For
            '                End If
            '                'S.SANDEEP |26-AUG-2019| -- END

            '            End If
            '        Else
            '            If blnSummary = True Then

            '                'Shani (23-Nov-2016) -- Start
            '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                'xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, 0, enAssMode, decBSC_OverAllWeight, strArrGrps, CInt(xRow("assessgroupunkid")))
            '                xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, 0, enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, CInt(xRow("assessgroupunkid")))
            '                'Shani (23-Nov123-2016-2016) -- End
            '                xTotalScore = xTotalScore + xScore
            '                Exit For
            '            Else

            '                'Shani (23-Nov-2016) -- Start
            '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                'xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps, CInt(xRow("assessgroupunkid")))

            '                'S.SANDEEP |26-AUG-2019| -- START
            '                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            '                'xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, CInt(xRow("assessgroupunkid")))
            '                xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, CInt(xRow("assessgroupunkid")), blnISummryFormulaUsed)
            '                'S.SANDEEP |26-AUG-2019| -- END

            '                'Shani (23-Nov123-2016-2016) -- End


            '                xTotalScore = xTotalScore + xScore

            '                'S.SANDEEP |26-AUG-2019| -- START
            '                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            '                If blnISummryFormulaUsed Then
            '                    Exit For
            '                End If
            '                'S.SANDEEP |26-AUG-2019| -- END

            '            End If
            '        End If

            '    Next
            'End If

            If dsScore IsNot Nothing AndAlso dsScore.Tables(0).Rows.Count > 0 AndAlso xStrFormula.Trim.Length > 0 Then
                If xIsBSC Then
                    dRow = dsScore.Tables(0).Select("employeeunkid = " & xEmpId & " AND assessmodeid = " & enAssMode & " AND assessgroupunkid <= 0 ")
                Else
                    dRow = dsScore.Tables(0).Select("employeeunkid = " & xEmpId & " AND assessmodeid = " & enAssMode & " AND assessgroupunkid > 0 ")
                End If
                Dim xitemsids = String.Join(",", dRow.AsEnumerable().Select(Function(x) x.Field(Of Integer)("iItemUnkid").ToString).ToArray)
                For Each xRow As DataRow In dRow
                    If xIsBSC Then
                        If enFormula = enAssess_Computation_Formulas.FINAL_RESULT_SCORE Then
                            xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, 0, enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, , blnISummryFormulaUsed)
                        Else
                        xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, , blnISummryFormulaUsed)
                        End If
                            xTotalScore = xTotalScore + xScore
                            If blnISummryFormulaUsed Then
                                Exit For
                            End If
                    Else
                        If enFormula = enAssess_Computation_Formulas.FINAL_RESULT_SCORE Then
                            xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, 0, enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, CInt(xRow("assessgroupunkid")), blnISummryFormulaUsed)
                        Else
                        xScore = objCompute.Get_All_Formula_Expression(xStrFormula, CInt(xRow.Item("iItemUnkid")), xEmpId, xScoreOptId, CDec(xRow.Item("iScore")), enAssMode, decBSC_OverAllWeight, strArrGrps, ConfigParameter._Object._Self_Assign_Competencies, CInt(xRow("assessgroupunkid")), blnISummryFormulaUsed)
                        End If
                        xTotalScore = xTotalScore + xScore
                        If blnISummryFormulaUsed Then
                            Exit For
                        End If
                    End If
                Next
            End If
            'S.SANDEEP |09-DEC-2019| -- END


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetScore; Module Name: " & mstrModuleName)
        End Try
        Return xTotalScore
    End Function

    Private Sub Insert_Update_Delete_Tran(ByVal dtTran As DataTable, ByVal intComputeMstId As Integer, ByVal decFormulaValue As Decimal)
        Try
            Dim xRow = dtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("computationunkid") = intComputeMstId)
            If xRow.Count > 0 Then
                xRow(0).Item("formula_value") = decFormulaValue
                xRow(0).Item("AUD") = "U"
            Else
                Dim dtRow As DataRow = dtTran.NewRow
                dtRow("computescoretranunkid") = -1
                dtRow("computescoremasterunkid") = -1
                dtRow("computationunkid") = intComputeMstId
                dtRow("formula_value") = decFormulaValue
                dtRow("AUD") = "A"
                dtRow("isvoid") = False
                dtTran.Rows.Add(dtRow)
            End If
            dtTran.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update_Delete_Tran", mstrModuleName)
        End Try
    End Sub

    Private Sub VoidComputeProcess()
        Dim i As Integer = 0
        Dim strMessage As String = ""
        Dim bln As Boolean = True
        Try

            Dim intTotalRecord As Integer = dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsCheck") = True).Count


            For Each dRow As DataRow In dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsCheck") = True)
                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                i = i + 1
                lblProcess.Text = "Total Record" & i & " Of " & intTotalRecord
                dsTable.Tables(0).AcceptChanges()
                mdtView = dsTable.Tables(0).DefaultView
                Try
                    dgvAEmployee.Rows(dsTable.Tables(0).Rows.IndexOf(dRow)).Selected = True
                    dgvAEmployee.FirstDisplayedScrollingRowIndex = dsTable.Tables(0).Rows.IndexOf(dRow) - 10
                Catch ex As Exception
                End Try
                Application.DoEvents()
                strMessage = "" : bln = True

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.SELF_ASSESSMENT OrElse CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                    If CBool(dRow("E_IsProcess")) = False Then
                        dRow("message") = Language.getMessage(mstrModuleName, 11, "Self score computation is not present")
                    Else
                        bln = objComputeScore.Delete(CInt(dRow.Item("E_MstID").ToString), strMessage)
                        If bln = False AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                        End If
                    End If
                End If

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT OrElse CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                    If CBool(dRow("A_IsProcess")) = False Then
                        dRow("message") = Language.getMessage(mstrModuleName, 12, "Assessor score computation is not present")
                    Else
                        bln = objComputeScore.Delete(CInt(dRow.Item("A_MstID").ToString), strMessage)
                        If bln = False AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                        End If
                    End If
                End If

                If CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT OrElse CInt(cboSelectMode.SelectedValue) = clsComputeScore_master.enAssessMode.ALL_ASSESSMENT Then
                    If CBool(dRow("R_IsProcess")) = False Then
                        dRow("message") = Language.getMessage(mstrModuleName, 13, "Reviewer score computation is not present")
                    Else
                        bln = objComputeScore.Delete(CInt(dRow.Item("R_MstID").ToString), strMessage)
                        If bln = False AndAlso strMessage <> "" Then
                            dRow.Item("message") = strMessage
                        Else
                            dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                        End If
                    End If
                End If

                'Shani (20-Sep-2016) -- Start
                'Select Case CInt(cboSelectMode.SelectedValue)
                '    Case clsComputeScore_master.enAssessMode.SELF_ASSESSMENT
                '        If CBool(dRow("E_IsProcess")) = False Then
                '            dRow("message") = Language.getMessage(mstrModuleName, 11, "Self score computation is not present")
                '        Else
                '            bln = objComputeScore.Delete(CInt(dRow.Item("E_MstID").ToString), strMessage)
                '            If bln = False AndAlso strMessage <> "" Then
                '                dRow.Item("message") = strMessage
                '            Else
                '                dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                '            End If
                '        End If
                '    Case clsComputeScore_master.enAssessMode.APPRAISER_ASSESSMENT
                '        If CBool(dRow("A_IsProcess")) = False Then
                '            dRow("message") = Language.getMessage(mstrModuleName, 12, "Assessor score computation is not present")
                '        Else
                '            bln = objComputeScore.Delete(CInt(dRow.Item("A_MstID").ToString), strMessage)
                '            If bln = False AndAlso strMessage <> "" Then
                '                dRow.Item("message") = strMessage
                '            Else
                '                dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                '            End If
                '        End If
                '    Case clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT
                '        If CBool(dRow("R_IsProcess")) = False Then
                '            dRow("message") = Language.getMessage(mstrModuleName, 13, "Reviewer score computation is not present")
                '        Else
                '            bln = objComputeScore.Delete(CInt(dRow.Item("R_MstID").ToString), strMessage)
                '            If bln = False AndAlso strMessage <> "" Then
                '                dRow.Item("message") = strMessage
                '            Else
                '                dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                '            End If
                '        End If
                '    Case clsComputeScore_master.enAssessMode.ALL_ASSESSMENT
                '        If CBool(dRow("E_IsProcess")) = False OrElse CBool(dRow("A_IsProcess")) = False OrElse CBool(dRow("R_IsProcess")) = False Then
                '            dRow("message") = Language.getMessage(mstrModuleName, 14, "Score computation is not done")
                '        Else
                '            bln = objComputeScore.Delete(CInt(dRow.Item("E_MstID").ToString), strMessage)
                '            If bln = True Then bln = objComputeScore.Delete(CInt(dRow.Item("A_MstID").ToString), strMessage)
                '            If bln = True Then bln = objComputeScore.Delete(CInt(dRow.Item("R_MstID").ToString), strMessage)

                '            If bln = False AndAlso strMessage <> "" Then
                '                dRow.Item("message") = strMessage
                '            Else
                '                dRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Successful")
                '            End If
                '        End If

                'End Select
                'Shani (20-Sep-2016) -- End

                If blnThread = True Then
                    Exit For
                End If
                AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            Next
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Successful void computaion score"), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VoidComputeProcess", mstrModuleName)
        Finally
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            btnStop.Visible = False
            btnClose.Visible = True
            Try
                Thread1.Interrupt()
            Catch ex As Exception
            End Try
        End Try
    End Sub

#End Region

#Region "Form's Event"

    Private Sub frmComputeScore_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsexternal_assessor_master.SetMessages()
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmComputeScore_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmComputeScore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            Call Fill_Combo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmComputeScore_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Event(S)"

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            Call FillList()
            objbtnSearch.ShowResult(dgvAEmployee.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrAdavanceFilter = ""
            cboPeriod.SelectedValue = 0
            cboSelectMode.SelectedValue = 0
            chkIncludeUncommitedAssessment.Checked = False
            dgvAEmployee.DataSource = Nothing
            mdtView = Nothing
            objbtnReset.ShowResult(dgvAEmployee.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            If Is_Vaild_Data() = False Then Exit Sub
            Me.gbComputeScoreProcess.Enabled = False
            Me.ControlBox = False
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are You sure you want to start score computation process"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkCancel, enMsgBoxStyle)) = Windows.Forms.DialogResult.OK Then
                If dsTable Is Nothing Then Exit Sub
                If dsTable.Tables(0).Rows.Count <= 0 Then Exit Sub
                mstrEmpids = String.Join(",", dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isCheck") = True).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
                dsScore = objComputeScore.GetAssessmentScore(CInt(cboPeriod.SelectedValue), mstrEmpids, ConfigParameter._Object._IsUseAgreedScore, "List")
                dtFormula = objCompute.GetFormulaStrings(CInt(cboPeriod.SelectedValue))

                If dsScore IsNot Nothing AndAlso dsScore.Tables(0).Rows.Count > 0 Then
                    dtComputeVariable = objCompute.Get_Computation_TranVariables(CInt(cboPeriod.SelectedValue), -1)
                End If

                xStringMatcingIn = enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                   enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                   enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO & "," & _
                                   enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                   enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                   enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO

                objCompute.Get_All_Compution_value(CInt(cboPeriod.SelectedValue), _
                                                       ConfigParameter._Object._ScoringOptionId, _
                                                       mstrEmpids, _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsUseAgreedScore, _
                                                       ConfigParameter._Object._Self_Assign_Competencies)

                Dim objFMapping As New clsAssess_Field_Mapping
                Dim xMappingUnkid As Integer = objFMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))

                If xMappingUnkid > 0 Then
                    objFMapping._Mappingunkid = xMappingUnkid
                    decBscWeight = objFMapping._Weight
                End If
                objFMapping = Nothing

                xStrGrps = "#" & enAssess_Computation_Types.CMP_OVERALL_WEIGHT & "#," & _
                           "#" & enAssess_Computation_Types.CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT & "#," & _
                           "#" & enAssess_Computation_Types.CMP_CATEGORY_ALL_GROUP_COUNT & "#"

                inttotalCount = dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count
                intCount = 0

                mblnIsVoidProcess = False

                objdgchkEmpProcess.Visible = False
                objdgchkASRProcess.Visible = False
                objdgchkREVProcess.Visible = False
                'S.SANDEEP |10-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                'objdgcolhMessage.Visible = True
                'S.SANDEEP |10-OCT-2019| -- END
                btnClose.Visible = False
                btnStop.Visible = True
                btnProcess.Enabled = False
                btnVoid.Enabled = False

                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

                bgwCmputeProcess.RunWorkerAsync()

                'objdgchkEmpProcess.Visible = False
                'objdgchkASRProcess.Visible = False
                'objdgchkREVProcess.Visible = False
                'objdgcolhMessage.Visible = True
                'btnClose.Visible = False
                'btnStop.Visible = True
                'txtSearchEmp.Text = ""
                'btnProcess.Enabled = False
                'btnVoid.Enabled = False
                ''Dim strEmpids As String = String.Join(",", dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isCheck") = True).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
                ''thread1 = New System.Threading.Thread(AddressOf EmployeeComputeScoreProcess)
                ''thread1.Start()
                'ThreadSt = New ThreadStart(AddressOf EmployeeComputeScoreProcess)
                'Thread1 = New Thread(ThreadSt)
                'Thread1.IsBackground = True
                'Thread1.Start()
                ''Call EmployeeComputeScoreProcess(strEmpids)
            End If
        Catch ex As Exception
            Me.gbComputeScoreProcess.Enabled = True
            Me.ControlBox = True
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        Finally
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        End Try
    End Sub

    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        Dim sVoidReason As String = String.Empty
        Try
            If Is_Vaild_Data() = False Then Exit Sub
            Me.gbComputeScoreProcess.Enabled = False
            Me.ControlBox = False
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Are You sure you want to void computed score"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkCancel, enMsgBoxStyle)) = Windows.Forms.DialogResult.OK Then
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                If sVoidReason.Trim.Length <= 0 Then Exit Sub

                objComputeScore._Isvoid = True
                objComputeScore._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objComputeScore._Voidreason = sVoidReason
                objComputeScore._Voiduserunkid = User._Object._Userunkid

                objdgchkEmpProcess.Visible = False
                objdgchkASRProcess.Visible = False
                objdgchkREVProcess.Visible = False
                'S.SANDEEP |10-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                'objdgcolhMessage.Visible = True
                'S.SANDEEP |10-OCT-2019| -- END
                txtSearchEmp.Text = ""

                btnStop.Visible = True
                btnClose.Visible = False
                btnProcess.Enabled = False
                btnVoid.Enabled = False

                'ThreadSt = New ThreadStart(AddressOf VoidComputeProcess)
                'Thread1 = New Thread(ThreadSt)
                'Thread1.IsBackground = True
                'Thread1.Start()

                mblnIsVoidProcess = True
                Call bgwCmputeProcess.RunWorkerAsync()

            End If
        Catch ex As Exception
            Me.gbComputeScoreProcess.Enabled = True
            Me.ControlBox = True
            DisplayError.Show("-1", ex.Message, "btnVoid_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Thread1 IsNot Nothing AndAlso Thread1.ThreadState = ThreadState.Background Then
                eZeeMsgBox.Show("Process Is Current Work you Can not close this")
            Else
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnStop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStop.Click
        Try
            If eZeeMsgBox.Show("Are you sure want stop current computation process", CType(enMsgBoxStyle.OkCancel + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.OK Then
                'If Thread1.ThreadState = Threading.ThreadState.Background Then
                '    blnThread = True
                '    btnStop.Enabled = False
                'End If
                mblnCancelWorker = True
                bgwCmputeProcess.CancelAsync()
            End If
        Catch ex As Exception

        End Try
    End Sub

    'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            'Dim objSCA As New clsScoreCalibrationApproval
            ''S.SANDEEP |22-NOV-2019| -- START
            ''ISSUE/ENHANCEMENT : Calibration Issues
            ''mstrEmpids = String.Join(",", dsTable.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
            'mstrEmpids = String.Join(",", dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
            ''S.SANDEEP |22-NOV-2019| -- END
            'objSCA.SendSubmitNotification(FinancialYear._Object._DatabaseName, _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                              Company._Object._Companyunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              enUserPriviledge.AllowtoCalibrateProvisionalScore, _
            '                              User._Object._Userunkid, _
            '                              CInt(cboPeriod.SelectedValue), _
            '                              cboPeriod.Text, _
            '                              ConfigParameter._Object._EmployeeAsOnDate, _
            '                              mstrEmpids, _
            '                              mstrModuleName, _
            '                              enLogin_Mode.DESKTOP, _
            '                              Company._Object._Senderaddress, _
            '                              ConfigParameter._Object._ArutiSelfServiceURL, _
            '                              User._Object._Username, Nothing)
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Submitted successfully for Calibration process."), enMsgBoxStyle.Information)
            Dim objSCA As New clsScoreCalibrationApproval
            mstrEmpids = String.Join(",", dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
            If mstrEmpids.Length > 0 Then
                Dim dtUser As New DataTable
                If dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count > 0 Then
                    dtUser = dsTable.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).CopyToDataTable().DefaultView.ToTable(True, "UName", "UEmail", "UId")
                End If
                objSCA.SendSubmitNotification(Company._Object._Companyunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          enUserPriviledge.AllowtoCalibrateProvisionalScore, _
                                          User._Object._Userunkid, _
                                          CInt(cboPeriod.SelectedValue), _
                                          cboPeriod.Text, _
                                          mstrModuleName, _
                                          enLogin_Mode.DESKTOP, _
                                          Company._Object._Senderaddress, _
                                              mstrEmpids, _
                                          ConfigParameter._Object._ArutiSelfServiceURL, _
                                              User._Object._Username, _
                                              dtUser, _
                                              Nothing)

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Submitted successfully for Calibration process."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSubmit_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |16-AUG-2019| -- END

#End Region

#Region "Control Event(S)"

    'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    Private Sub cboSelectMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSelectMode.SelectedIndexChanged
        Try
            Select Case CInt(cboSelectMode.SelectedValue)
                Case clsComputeScore_master.enAssessMode.SUBMIT_CALIBRATION
                    btnProcess.Visible = False : btnVoid.Visible = False : btnStop.Visible = False
                    btnSubmit.Visible = True
                Case Else
                    btnProcess.Visible = True : btnVoid.Visible = True
                    btnSubmit.Visible = False
            End Select
            dgvAEmployee.DataSource = Nothing
            If dsTable IsNot Nothing AndAlso dsTable.Tables.Count > 0 Then dsTable.Tables(0).Rows.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSelectMode_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |16-AUG-2019| -- END

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdavanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Call ApplyFilter()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each gvRow As DataGridViewRow In dgvAEmployee.Rows
                'S.SANDEEP |10-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If CBool(gvRow.Cells(objdgcolhiRead.Index).Value) = True Then Continue For
                'S.SANDEEP |10-OCT-2019| -- END
                gvRow.Cells(objdgcolhECheck.Index).Value = objchkEmployee.Checked
            Next
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
        Try
            If e.ColumnIndex = objdgcolhECheck.Index Then
                RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim intRowCount = dgvAEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells("objdgcolhECheck").Value) = True).Count

                If intRowCount = dgvAEmployee.Rows.Count Then
                    objchkEmployee.CheckState = CheckState.Checked
                ElseIf intRowCount <= 0 Then
                    objchkEmployee.CheckState = CheckState.Unchecked
                Else
                    objchkEmployee.CheckState = CheckState.Indeterminate
                End If

                AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboSelectMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSelectMode.SelectedIndexChanged
    '    Try
    '        chkIncludeUncommitedAssessment.Enabled = CInt(cboSelectMode.SelectedValue) > 0
    '        Call ApplyFilter()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboSelectMode_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub chkIncludeUncommitedAssessment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeUncommitedAssessment.CheckedChanged
    '    Try
    '        Call ApplyFilter()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkIncludeUncommitedAssessment_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Background Worker Event(s) "

    Private Sub bgwCmputeProcess_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwCmputeProcess.DoWork
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            If mblnIsVoidProcess Then   'VOID COMPUTED SCORES PROCESS
                Call VoidComputedScores()
            Else                        'PERFORM COMPUTATION SCORES PROCESS
                Call ComputeScore()
            End If
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgwCmputeProcess_DoWork", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub bgwCmputeProcess_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwCmputeProcess.ProgressChanged
        Try
            lblProcess.Text = intCount.ToString & " / " & inttotalCount.ToString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgwCmputeProcess_ProgressChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub bgwCmputeProcess_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwCmputeProcess.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            If mblnIsVoidProcess Then   'VOID COMPUTED SCORES PROCESS
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Successful void computaion score"), enMsgBoxStyle.Information)
            Else                        'PERFORM COMPUTATION SCORES PROCESS
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Successful computation score Process done"), enMsgBoxStyle.Information)
            End If
            Me.gbComputeScoreProcess.Enabled = True
            Me.ControlBox = True
            btnStop.Visible = False : btnClose.Visible = True
            mblnCancelWorker = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgwCmputeProcess_RunWorkerCompleted", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbComputeScoreProcess.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbComputeScoreProcess.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnStop.GradientBackColor = GUI._ButttonBackColor
            Me.btnStop.GradientForeColor = GUI._ButttonFontColor

            Me.btnSubmit.GradientBackColor = GUI._ButttonBackColor
            Me.btnSubmit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.chkIncludeUncommitedAssessment.Text = Language._Object.getCaption(Me.chkIncludeUncommitedAssessment.Name, Me.chkIncludeUncommitedAssessment.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.lblSelectMode.Text = Language._Object.getCaption(Me.lblSelectMode.Name, Me.lblSelectMode.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.lblProcess.Text = Language._Object.getCaption(Me.lblProcess.Name, Me.lblProcess.Text)
            Me.btnStop.Text = Language._Object.getCaption(Me.btnStop.Name, Me.btnStop.Text)
            Me.gbComputeScoreProcess.Text = Language._Object.getCaption(Me.gbComputeScoreProcess.Name, Me.gbComputeScoreProcess.Text)
            Me.btnSubmit.Text = Language._Object.getCaption(Me.btnSubmit.Name, Me.btnSubmit.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 2, "Process Mode is mandatory information. Please select Process Mode to continue.")
            Language.setMessage(mstrModuleName, 3, "Please check atleast one of the employee to compute score process.")
            Language.setMessage(mstrModuleName, 4, "Self Assessment is not present")
            Language.setMessage(mstrModuleName, 5, "Compute score process already done")
            Language.setMessage(mstrModuleName, 6, "Successful")
            Language.setMessage(mstrModuleName, 7, "Assessor Assessment is not present")
            Language.setMessage(mstrModuleName, 8, "Reviewer Assessment is not present")
            Language.setMessage(mstrModuleName, 9, "Are You sure you want to start score computation process")
            Language.setMessage(mstrModuleName, 10, "Are You sure you want to void computed score")
            Language.setMessage(mstrModuleName, 11, "Self score computation is not present")
            Language.setMessage(mstrModuleName, 12, "Assessor score computation is not present")
            Language.setMessage(mstrModuleName, 13, "Reviewer score computation is not present")
            Language.setMessage(mstrModuleName, 15, "Computation formula does not exist. Please define computation formula")
            Language.setMessage(mstrModuleName, 16, "Successful void computaion score")
            Language.setMessage(mstrModuleName, 17, "Successful computation score Process done")
            Language.setMessage(mstrModuleName, 18, "Submitted successfully for Calibration process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class