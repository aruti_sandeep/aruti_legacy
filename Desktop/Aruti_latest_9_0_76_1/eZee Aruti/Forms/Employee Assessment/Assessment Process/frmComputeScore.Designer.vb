﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComputeScore
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComputeScore))
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnVoid = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.chkIncludeUncommitedAssessment = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblSelectMode = New System.Windows.Forms.Label
        Me.cboSelectMode = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSubmit = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblProcess = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnStop = New eZee.Common.eZeeLightButton(Me.components)
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgchkEmpProcess = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgchkASRProcess = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgchkREVProcess = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolheEMstID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolheAMstID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolheRMstID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhiRead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhiMsg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbComputeScoreProcess = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.bgwCmputeProcess = New System.ComponentModel.BackgroundWorker
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbComputeScoreProcess.SuspendLayout()
        Me.SuspendLayout()
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(630, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 10
        Me.objbtnReset.TabStop = False
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(12, 10)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(88, 30)
        Me.btnProcess.TabIndex = 2
        Me.btnProcess.Text = "&Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoid.FlatAppearance.BorderSize = 0
        Me.btnVoid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Location = New System.Drawing.Point(106, 10)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Size = New System.Drawing.Size(88, 30)
        Me.btnVoid.TabIndex = 0
        Me.btnVoid.Text = "&Void"
        Me.btnVoid.UseVisualStyleBackColor = True
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(605, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 9
        Me.objbtnSearch.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(505, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(94, 15)
        Me.lnkAllocation.TabIndex = 14
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkIncludeUncommitedAssessment
        '
        Me.chkIncludeUncommitedAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeUncommitedAssessment.Location = New System.Drawing.Point(21, 17)
        Me.chkIncludeUncommitedAssessment.Name = "chkIncludeUncommitedAssessment"
        Me.chkIncludeUncommitedAssessment.Size = New System.Drawing.Size(10, 17)
        Me.chkIncludeUncommitedAssessment.TabIndex = 12
        Me.chkIncludeUncommitedAssessment.Text = "Include Uncommitted Assessment For Score Processing"
        Me.chkIncludeUncommitedAssessment.UseVisualStyleBackColor = True
        Me.chkIncludeUncommitedAssessment.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(66, 16)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(80, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(267, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'lblSelectMode
        '
        Me.lblSelectMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectMode.Location = New System.Drawing.Point(353, 36)
        Me.lblSelectMode.Name = "lblSelectMode"
        Me.lblSelectMode.Size = New System.Drawing.Size(84, 16)
        Me.lblSelectMode.TabIndex = 307
        Me.lblSelectMode.Text = "Process Mode"
        Me.lblSelectMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSelectMode
        '
        Me.cboSelectMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSelectMode.DropDownWidth = 200
        Me.cboSelectMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSelectMode.FormattingEnabled = True
        Me.cboSelectMode.Location = New System.Drawing.Point(443, 34)
        Me.cboSelectMode.Name = "cboSelectMode"
        Me.cboSelectMode.Size = New System.Drawing.Size(202, 21)
        Me.cboSelectMode.TabIndex = 308
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSubmit)
        Me.objFooter.Controls.Add(Me.lblProcess)
        Me.objFooter.Controls.Add(Me.btnVoid)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Controls.Add(Me.btnStop)
        Me.objFooter.Controls.Add(Me.chkIncludeUncommitedAssessment)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 489)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(657, 50)
        Me.objFooter.TabIndex = 16
        '
        'btnSubmit
        '
        Me.btnSubmit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSubmit.BackColor = System.Drawing.Color.White
        Me.btnSubmit.BackgroundImage = CType(resources.GetObject("btnSubmit.BackgroundImage"), System.Drawing.Image)
        Me.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSubmit.BorderColor = System.Drawing.Color.Empty
        Me.btnSubmit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSubmit.FlatAppearance.BorderSize = 0
        Me.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmit.ForeColor = System.Drawing.Color.Black
        Me.btnSubmit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSubmit.GradientForeColor = System.Drawing.Color.Black
        Me.btnSubmit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmit.Location = New System.Drawing.Point(12, 10)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmit.Size = New System.Drawing.Size(182, 30)
        Me.btnSubmit.TabIndex = 311
        Me.btnSubmit.Text = "&Submit for Calibration"
        Me.btnSubmit.UseVisualStyleBackColor = True
        '
        'lblProcess
        '
        Me.lblProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcess.Location = New System.Drawing.Point(304, 17)
        Me.lblProcess.Name = "lblProcess"
        Me.lblProcess.Size = New System.Drawing.Size(247, 16)
        Me.lblProcess.TabIndex = 309
        Me.lblProcess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(557, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(88, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStop.BackColor = System.Drawing.Color.White
        Me.btnStop.BackgroundImage = CType(resources.GetObject("btnStop.BackgroundImage"), System.Drawing.Image)
        Me.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStop.BorderColor = System.Drawing.Color.Empty
        Me.btnStop.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStop.FlatAppearance.BorderSize = 0
        Me.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStop.ForeColor = System.Drawing.Color.Black
        Me.btnStop.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStop.GradientForeColor = System.Drawing.Color.Black
        Me.btnStop.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Location = New System.Drawing.Point(557, 10)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Size = New System.Drawing.Size(88, 30)
        Me.btnStop.TabIndex = 310
        Me.btnStop.Text = "&Stop"
        Me.btnStop.UseVisualStyleBackColor = True
        Me.btnStop.Visible = False
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(3, 61)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(651, 425)
        Me.tblpAssessorEmployee.TabIndex = 305
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(645, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(645, 393)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.ColumnHeadersHeight = 21
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgchkEmpProcess, Me.objdgchkASRProcess, Me.objdgchkREVProcess, Me.objdgcolhMessage, Me.objdgcolhEmpId, Me.objdgcolheEMstID, Me.objdgcolheAMstID, Me.objdgcolheRMstID, Me.objdgcolhiRead, Me.objdgcolhiMsg})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(645, 393)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.Frozen = True
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgchkEmpProcess
        '
        Me.objdgchkEmpProcess.HeaderText = "Self"
        Me.objdgchkEmpProcess.Name = "objdgchkEmpProcess"
        Me.objdgchkEmpProcess.ReadOnly = True
        Me.objdgchkEmpProcess.Width = 75
        '
        'objdgchkASRProcess
        '
        Me.objdgchkASRProcess.HeaderText = "Assessor"
        Me.objdgchkASRProcess.Name = "objdgchkASRProcess"
        Me.objdgchkASRProcess.ReadOnly = True
        Me.objdgchkASRProcess.Width = 75
        '
        'objdgchkREVProcess
        '
        Me.objdgchkREVProcess.HeaderText = "Reviewer"
        Me.objdgchkREVProcess.Name = "objdgchkREVProcess"
        Me.objdgchkREVProcess.ReadOnly = True
        Me.objdgchkREVProcess.Width = 75
        '
        'objdgcolhMessage
        '
        Me.objdgcolhMessage.HeaderText = "Message"
        Me.objdgcolhMessage.Name = "objdgcolhMessage"
        Me.objdgcolhMessage.ReadOnly = True
        Me.objdgcolhMessage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhMessage.Width = 150
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolheEMstID
        '
        Me.objdgcolheEMstID.HeaderText = "objdgcolheEMstID"
        Me.objdgcolheEMstID.Name = "objdgcolheEMstID"
        Me.objdgcolheEMstID.ReadOnly = True
        Me.objdgcolheEMstID.Visible = False
        '
        'objdgcolheAMstID
        '
        Me.objdgcolheAMstID.HeaderText = "objdgcolheAMstID"
        Me.objdgcolheAMstID.Name = "objdgcolheAMstID"
        Me.objdgcolheAMstID.ReadOnly = True
        Me.objdgcolheAMstID.Visible = False
        '
        'objdgcolheRMstID
        '
        Me.objdgcolheRMstID.HeaderText = "objdgcolheRMstID"
        Me.objdgcolheRMstID.Name = "objdgcolheRMstID"
        Me.objdgcolheRMstID.ReadOnly = True
        Me.objdgcolheRMstID.Visible = False
        '
        'objdgcolhiRead
        '
        Me.objdgcolhiRead.HeaderText = "objdgcolhiRead"
        Me.objdgcolhiRead.Name = "objdgcolhiRead"
        Me.objdgcolhiRead.ReadOnly = True
        Me.objdgcolhiRead.Visible = False
        '
        'objdgcolhiMsg
        '
        Me.objdgcolhiMsg.HeaderText = "objdgcolhiMsg"
        Me.objdgcolhiMsg.Name = "objdgcolhiMsg"
        Me.objdgcolhiMsg.ReadOnly = True
        Me.objdgcolhiMsg.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objdgcolheEMstID"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolheAMstID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolheRMstID"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'gbComputeScoreProcess
        '
        Me.gbComputeScoreProcess.BorderColor = System.Drawing.Color.Black
        Me.gbComputeScoreProcess.Checked = False
        Me.gbComputeScoreProcess.CollapseAllExceptThis = False
        Me.gbComputeScoreProcess.CollapsedHoverImage = Nothing
        Me.gbComputeScoreProcess.CollapsedNormalImage = Nothing
        Me.gbComputeScoreProcess.CollapsedPressedImage = Nothing
        Me.gbComputeScoreProcess.CollapseOnLoad = False
        Me.gbComputeScoreProcess.Controls.Add(Me.lnkAllocation)
        Me.gbComputeScoreProcess.Controls.Add(Me.objbtnReset)
        Me.gbComputeScoreProcess.Controls.Add(Me.objbtnSearch)
        Me.gbComputeScoreProcess.Controls.Add(Me.cboSelectMode)
        Me.gbComputeScoreProcess.Controls.Add(Me.cboPeriod)
        Me.gbComputeScoreProcess.Controls.Add(Me.lblSelectMode)
        Me.gbComputeScoreProcess.Controls.Add(Me.lblPeriod)
        Me.gbComputeScoreProcess.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbComputeScoreProcess.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbComputeScoreProcess.ExpandedHoverImage = Nothing
        Me.gbComputeScoreProcess.ExpandedNormalImage = Nothing
        Me.gbComputeScoreProcess.ExpandedPressedImage = Nothing
        Me.gbComputeScoreProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gbComputeScoreProcess.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbComputeScoreProcess.HeaderHeight = 25
        Me.gbComputeScoreProcess.HeaderMessage = ""
        Me.gbComputeScoreProcess.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbComputeScoreProcess.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbComputeScoreProcess.HeightOnCollapse = 0
        Me.gbComputeScoreProcess.LeftTextSpace = 0
        Me.gbComputeScoreProcess.Location = New System.Drawing.Point(0, 0)
        Me.gbComputeScoreProcess.Name = "gbComputeScoreProcess"
        Me.gbComputeScoreProcess.OpenHeight = 300
        Me.gbComputeScoreProcess.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbComputeScoreProcess.ShowBorder = True
        Me.gbComputeScoreProcess.ShowCheckBox = False
        Me.gbComputeScoreProcess.ShowCollapseButton = False
        Me.gbComputeScoreProcess.ShowDefaultBorderColor = True
        Me.gbComputeScoreProcess.ShowDownButton = False
        Me.gbComputeScoreProcess.ShowHeader = True
        Me.gbComputeScoreProcess.Size = New System.Drawing.Size(657, 489)
        Me.gbComputeScoreProcess.TabIndex = 309
        Me.gbComputeScoreProcess.Temp = 0
        Me.gbComputeScoreProcess.Text = "Compute Score Process"
        Me.gbComputeScoreProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bgwCmputeProcess
        '
        Me.bgwCmputeProcess.WorkerReportsProgress = True
        Me.bgwCmputeProcess.WorkerSupportsCancellation = True
        '
        'frmComputeScore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(657, 539)
        Me.Controls.Add(Me.gbComputeScoreProcess)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmComputeScore"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Compute Score"
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbComputeScoreProcess.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents chkIncludeUncommitedAssessment As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnVoid As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblSelectMode As System.Windows.Forms.Label
    Friend WithEvents cboSelectMode As System.Windows.Forms.ComboBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents lblProcess As System.Windows.Forms.Label
    Friend WithEvents btnStop As eZee.Common.eZeeLightButton
    Friend WithEvents gbComputeScoreProcess As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents bgwCmputeProcess As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnSubmit As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgchkEmpProcess As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgchkASRProcess As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgchkREVProcess As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolheEMstID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolheAMstID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolheRMstID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhiRead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhiMsg As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
