﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPerformanceEvaluation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPerformanceEvaluation))
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objSPC = New System.Windows.Forms.SplitContainer
        Me.gbBSCEvaluation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkViewApproveRejectRemark = New System.Windows.Forms.LinkLabel
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.lnkCopyScore = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.radExternalAssessor = New System.Windows.Forms.RadioButton
        Me.lblAssessor = New System.Windows.Forms.Label
        Me.radInternalAssessor = New System.Windows.Forms.RadioButton
        Me.lblReviewer = New System.Windows.Forms.Label
        Me.objbtnSearchAssessor = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchReviewer = New eZee.Common.eZeeGradientButton
        Me.cboAssessor = New System.Windows.Forms.ComboBox
        Me.lblAssessDate = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.dtpAssessdate = New System.Windows.Forms.DateTimePicker
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboReviewer = New System.Windows.Forms.ComboBox
        Me.tblpContainer = New System.Windows.Forms.TableLayoutPanel
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.objpnlContainer = New System.Windows.Forms.Panel
        Me.gbRemarks = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblRvrCmts = New System.Windows.Forms.Label
        Me.txtReviewerCmts = New System.Windows.Forms.TextBox
        Me.txtAssessorCmts = New System.Windows.Forms.TextBox
        Me.objlblAsrCmts = New System.Windows.Forms.Label
        Me.objlnkCloseRemark = New System.Windows.Forms.LinkLabel
        Me.objgbDescription = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.objlnkCloseDesc = New System.Windows.Forms.LinkLabel
        Me.objgbScoreGuide = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlnkClose = New System.Windows.Forms.LinkLabel
        Me.lvScaleList = New eZee.Common.eZeeListView(Me.components)
        Me.colhScale = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objpnlCItems = New System.Windows.Forms.Panel
        Me.dgvItems = New System.Windows.Forms.DataGridView
        Me.objdgcolhAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objpnlGE = New System.Windows.Forms.Panel
        Me.dgvGE = New System.Windows.Forms.DataGridView
        Me.objdgcolhGECollapse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolheval_itemGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhInfo = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhGEWeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGEScore = New System.Windows.Forms.DataGridViewLinkColumn
        Me.dgcolheselfGE = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objdgcolhedisplayGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolheremarkGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaselfGE = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objdgcolhadisplayGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolharemarkGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaAgreedScoreGE = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhrselfGE = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objdgcolhrdisplayGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhrremarkGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhscalemasterunkidGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhcompetenciesunkidGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhassessgroupunkidGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrpGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsPGrpGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpIdGE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objpnlBSC = New System.Windows.Forms.Panel
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.dgvBSC = New System.Windows.Forms.DataGridView
        Me.objpnlInstruction = New System.Windows.Forms.Panel
        Me.txtInstruction = New System.Windows.Forms.TextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.tblpInformation = New System.Windows.Forms.TableLayoutPanel
        Me.objlblValue4 = New System.Windows.Forms.Label
        Me.objlblValue3 = New System.Windows.Forms.Label
        Me.objlblValue2 = New System.Windows.Forms.Label
        Me.objlblValue1 = New System.Windows.Forms.Label
        Me.objbtnBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSaveCommit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCCollaps = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBSCField8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCompleted = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGoalType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGoalvalue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBSCScore = New System.Windows.Forms.DataGridViewLinkColumn
        Me.dgcolhBSCWeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolheselfBSC = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolheremarkBSC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaselfBSC = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolharemarkBSC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAgreedScoreBSC = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhrselfBSC = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhrremarkBSC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrpBSC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpIdBSC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhScaleMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objSPC.Panel1.SuspendLayout()
        Me.objSPC.Panel2.SuspendLayout()
        Me.objSPC.SuspendLayout()
        Me.gbBSCEvaluation.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.tblpContainer.SuspendLayout()
        Me.objpnlContainer.SuspendLayout()
        Me.gbRemarks.SuspendLayout()
        Me.objgbDescription.SuspendLayout()
        Me.objgbScoreGuide.SuspendLayout()
        Me.objpnlCItems.SuspendLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objpnlGE.SuspendLayout()
        CType(Me.dgvGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objpnlBSC.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBSC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objpnlInstruction.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.tblpInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objSPC)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1018, 652)
        Me.pnlMain.TabIndex = 0
        '
        'objSPC
        '
        Me.objSPC.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSPC.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSPC.IsSplitterFixed = True
        Me.objSPC.Location = New System.Drawing.Point(0, 0)
        Me.objSPC.Name = "objSPC"
        Me.objSPC.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSPC.Panel1
        '
        Me.objSPC.Panel1.Controls.Add(Me.gbBSCEvaluation)
        '
        'objSPC.Panel2
        '
        Me.objSPC.Panel2.Controls.Add(Me.tblpContainer)
        Me.objSPC.Size = New System.Drawing.Size(1018, 602)
        Me.objSPC.SplitterDistance = 64
        Me.objSPC.SplitterWidth = 2
        Me.objSPC.TabIndex = 1
        '
        'gbBSCEvaluation
        '
        Me.gbBSCEvaluation.BorderColor = System.Drawing.Color.Black
        Me.gbBSCEvaluation.Checked = False
        Me.gbBSCEvaluation.CollapseAllExceptThis = False
        Me.gbBSCEvaluation.CollapsedHoverImage = Nothing
        Me.gbBSCEvaluation.CollapsedNormalImage = Nothing
        Me.gbBSCEvaluation.CollapsedPressedImage = Nothing
        Me.gbBSCEvaluation.CollapseOnLoad = False
        Me.gbBSCEvaluation.Controls.Add(Me.lnkViewApproveRejectRemark)
        Me.gbBSCEvaluation.Controls.Add(Me.FlowLayoutPanel1)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnReset)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearch)
        Me.gbBSCEvaluation.Controls.Add(Me.cboEmployee)
        Me.gbBSCEvaluation.Controls.Add(Me.radExternalAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.lblAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.radInternalAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.lblReviewer)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearchAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearchReviewer)
        Me.gbBSCEvaluation.Controls.Add(Me.cboAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.lblAssessDate)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbBSCEvaluation.Controls.Add(Me.lblEmployee)
        Me.gbBSCEvaluation.Controls.Add(Me.lblPeriod)
        Me.gbBSCEvaluation.Controls.Add(Me.dtpAssessdate)
        Me.gbBSCEvaluation.Controls.Add(Me.cboPeriod)
        Me.gbBSCEvaluation.Controls.Add(Me.cboReviewer)
        Me.gbBSCEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbBSCEvaluation.ExpandedHoverImage = Nothing
        Me.gbBSCEvaluation.ExpandedNormalImage = Nothing
        Me.gbBSCEvaluation.ExpandedPressedImage = Nothing
        Me.gbBSCEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBSCEvaluation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBSCEvaluation.HeaderHeight = 25
        Me.gbBSCEvaluation.HeaderMessage = ""
        Me.gbBSCEvaluation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBSCEvaluation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBSCEvaluation.HeightOnCollapse = 0
        Me.gbBSCEvaluation.LeftTextSpace = 0
        Me.gbBSCEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.gbBSCEvaluation.Name = "gbBSCEvaluation"
        Me.gbBSCEvaluation.OpenHeight = 300
        Me.gbBSCEvaluation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBSCEvaluation.ShowBorder = True
        Me.gbBSCEvaluation.ShowCheckBox = False
        Me.gbBSCEvaluation.ShowCollapseButton = False
        Me.gbBSCEvaluation.ShowDefaultBorderColor = True
        Me.gbBSCEvaluation.ShowDownButton = False
        Me.gbBSCEvaluation.ShowHeader = True
        Me.gbBSCEvaluation.Size = New System.Drawing.Size(1018, 64)
        Me.gbBSCEvaluation.TabIndex = 39
        Me.gbBSCEvaluation.Temp = 0
        Me.gbBSCEvaluation.Text = "Evaluation Info"
        Me.gbBSCEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkViewApproveRejectRemark
        '
        Me.lnkViewApproveRejectRemark.BackColor = System.Drawing.Color.Transparent
        Me.lnkViewApproveRejectRemark.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkViewApproveRejectRemark.LinkColor = System.Drawing.Color.Maroon
        Me.lnkViewApproveRejectRemark.Location = New System.Drawing.Point(749, 4)
        Me.lnkViewApproveRejectRemark.Name = "lnkViewApproveRejectRemark"
        Me.lnkViewApproveRejectRemark.Size = New System.Drawing.Size(210, 17)
        Me.lnkViewApproveRejectRemark.TabIndex = 388
        Me.lnkViewApproveRejectRemark.TabStop = True
        Me.lnkViewApproveRejectRemark.Text = "Approve/Reject Assessment"
        Me.lnkViewApproveRejectRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkViewApproveRejectRemark.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.lnkCopyScore)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(782, 4)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(177, 17)
        Me.FlowLayoutPanel1.TabIndex = 390
        '
        'lnkCopyScore
        '
        Me.lnkCopyScore.BackColor = System.Drawing.Color.Transparent
        Me.lnkCopyScore.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkCopyScore.Location = New System.Drawing.Point(3, 0)
        Me.lnkCopyScore.Name = "lnkCopyScore"
        Me.lnkCopyScore.Size = New System.Drawing.Size(171, 17)
        Me.lnkCopyScore.TabIndex = 387
        Me.lnkCopyScore.TabStop = True
        Me.lnkCopyScore.Text = "Copy Assessor Score"
        Me.lnkCopyScore.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnReset.BorderSelected = False
        Me.objbtnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objbtnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnReset.Location = New System.Drawing.Point(992, 2)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnReset.TabIndex = 221
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(965, 2)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 222
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(355, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(249, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'radExternalAssessor
        '
        Me.radExternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radExternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExternalAssessor.Location = New System.Drawing.Point(294, 4)
        Me.radExternalAssessor.Name = "radExternalAssessor"
        Me.radExternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radExternalAssessor.TabIndex = 378
        Me.radExternalAssessor.TabStop = True
        Me.radExternalAssessor.Text = "External Assessor"
        Me.radExternalAssessor.UseVisualStyleBackColor = False
        Me.radExternalAssessor.Visible = False
        '
        'lblAssessor
        '
        Me.lblAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessor.Location = New System.Drawing.Point(8, 36)
        Me.lblAssessor.Name = "lblAssessor"
        Me.lblAssessor.Size = New System.Drawing.Size(59, 15)
        Me.lblAssessor.TabIndex = 343
        Me.lblAssessor.Text = "Assessor"
        Me.lblAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radInternalAssessor
        '
        Me.radInternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radInternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radInternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInternalAssessor.Location = New System.Drawing.Point(161, 4)
        Me.radInternalAssessor.Name = "radInternalAssessor"
        Me.radInternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radInternalAssessor.TabIndex = 378
        Me.radInternalAssessor.TabStop = True
        Me.radInternalAssessor.Text = "Internal Assessor"
        Me.radInternalAssessor.UseVisualStyleBackColor = False
        Me.radInternalAssessor.Visible = False
        '
        'lblReviewer
        '
        Me.lblReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReviewer.Location = New System.Drawing.Point(8, 36)
        Me.lblReviewer.Name = "lblReviewer"
        Me.lblReviewer.Size = New System.Drawing.Size(59, 15)
        Me.lblReviewer.TabIndex = 347
        Me.lblReviewer.Text = "Reviewer"
        Me.lblReviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchAssessor
        '
        Me.objbtnSearchAssessor.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAssessor.BorderSelected = False
        Me.objbtnSearchAssessor.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAssessor.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAssessor.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAssessor.Location = New System.Drawing.Point(264, 33)
        Me.objbtnSearchAssessor.Name = "objbtnSearchAssessor"
        Me.objbtnSearchAssessor.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAssessor.TabIndex = 345
        '
        'objbtnSearchReviewer
        '
        Me.objbtnSearchReviewer.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReviewer.BorderSelected = False
        Me.objbtnSearchReviewer.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReviewer.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReviewer.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReviewer.Location = New System.Drawing.Point(264, 33)
        Me.objbtnSearchReviewer.Name = "objbtnSearchReviewer"
        Me.objbtnSearchReviewer.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReviewer.TabIndex = 385
        '
        'cboAssessor
        '
        Me.cboAssessor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessor.DropDownWidth = 350
        Me.cboAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessor.FormattingEnabled = True
        Me.cboAssessor.Location = New System.Drawing.Point(73, 33)
        Me.cboAssessor.Name = "cboAssessor"
        Me.cboAssessor.Size = New System.Drawing.Size(185, 21)
        Me.cboAssessor.TabIndex = 344
        '
        'lblAssessDate
        '
        Me.lblAssessDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessDate.Location = New System.Drawing.Point(855, 36)
        Me.lblAssessDate.Name = "lblAssessDate"
        Me.lblAssessDate.Size = New System.Drawing.Size(47, 15)
        Me.lblAssessDate.TabIndex = 5
        Me.lblAssessDate.Text = "Date"
        Me.lblAssessDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(610, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(291, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(58, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(637, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(51, 15)
        Me.lblPeriod.TabIndex = 7
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAssessdate
        '
        Me.dtpAssessdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssessdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssessdate.Location = New System.Drawing.Point(908, 33)
        Me.dtpAssessdate.Name = "dtpAssessdate"
        Me.dtpAssessdate.Size = New System.Drawing.Size(98, 21)
        Me.dtpAssessdate.TabIndex = 6
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(694, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(153, 21)
        Me.cboPeriod.TabIndex = 8
        '
        'cboReviewer
        '
        Me.cboReviewer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReviewer.DropDownWidth = 350
        Me.cboReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReviewer.FormattingEnabled = True
        Me.cboReviewer.Location = New System.Drawing.Point(73, 33)
        Me.cboReviewer.Name = "cboReviewer"
        Me.cboReviewer.Size = New System.Drawing.Size(185, 21)
        Me.cboReviewer.TabIndex = 384
        '
        'tblpContainer
        '
        Me.tblpContainer.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tblpContainer.ColumnCount = 1
        Me.tblpContainer.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpContainer.Controls.Add(Me.objlblCaption, 0, 0)
        Me.tblpContainer.Controls.Add(Me.objpnlContainer, 0, 1)
        Me.tblpContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblpContainer.Location = New System.Drawing.Point(0, 0)
        Me.tblpContainer.Name = "tblpContainer"
        Me.tblpContainer.RowCount = 2
        Me.tblpContainer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23.0!))
        Me.tblpContainer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpContainer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblpContainer.Size = New System.Drawing.Size(1018, 536)
        Me.tblpContainer.TabIndex = 0
        '
        'objlblCaption
        '
        Me.objlblCaption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(4, 1)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(1010, 23)
        Me.objlblCaption.TabIndex = 0
        Me.objlblCaption.Text = "#value"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objpnlContainer
        '
        Me.objpnlContainer.Controls.Add(Me.gbRemarks)
        Me.objpnlContainer.Controls.Add(Me.objgbDescription)
        Me.objpnlContainer.Controls.Add(Me.objgbScoreGuide)
        Me.objpnlContainer.Controls.Add(Me.objpnlCItems)
        Me.objpnlContainer.Controls.Add(Me.objpnlGE)
        Me.objpnlContainer.Controls.Add(Me.objpnlBSC)
        Me.objpnlContainer.Controls.Add(Me.objpnlInstruction)
        Me.objpnlContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlContainer.Location = New System.Drawing.Point(1, 25)
        Me.objpnlContainer.Margin = New System.Windows.Forms.Padding(0)
        Me.objpnlContainer.Name = "objpnlContainer"
        Me.objpnlContainer.Size = New System.Drawing.Size(1016, 510)
        Me.objpnlContainer.TabIndex = 1
        '
        'gbRemarks
        '
        Me.gbRemarks.BorderColor = System.Drawing.Color.Black
        Me.gbRemarks.Checked = False
        Me.gbRemarks.CollapseAllExceptThis = False
        Me.gbRemarks.CollapsedHoverImage = Nothing
        Me.gbRemarks.CollapsedNormalImage = Nothing
        Me.gbRemarks.CollapsedPressedImage = Nothing
        Me.gbRemarks.CollapseOnLoad = False
        Me.gbRemarks.Controls.Add(Me.objlblRvrCmts)
        Me.gbRemarks.Controls.Add(Me.txtReviewerCmts)
        Me.gbRemarks.Controls.Add(Me.txtAssessorCmts)
        Me.gbRemarks.Controls.Add(Me.objlblAsrCmts)
        Me.gbRemarks.Controls.Add(Me.objlnkCloseRemark)
        Me.gbRemarks.ExpandedHoverImage = Nothing
        Me.gbRemarks.ExpandedNormalImage = Nothing
        Me.gbRemarks.ExpandedPressedImage = Nothing
        Me.gbRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRemarks.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRemarks.HeaderHeight = 25
        Me.gbRemarks.HeaderMessage = ""
        Me.gbRemarks.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbRemarks.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRemarks.HeightOnCollapse = 0
        Me.gbRemarks.LeftTextSpace = 0
        Me.gbRemarks.Location = New System.Drawing.Point(14, 163)
        Me.gbRemarks.Name = "gbRemarks"
        Me.gbRemarks.OpenHeight = 300
        Me.gbRemarks.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRemarks.ShowBorder = True
        Me.gbRemarks.ShowCheckBox = False
        Me.gbRemarks.ShowCollapseButton = False
        Me.gbRemarks.ShowDefaultBorderColor = True
        Me.gbRemarks.ShowDownButton = False
        Me.gbRemarks.ShowHeader = True
        Me.gbRemarks.Size = New System.Drawing.Size(516, 299)
        Me.gbRemarks.TabIndex = 393
        Me.gbRemarks.Temp = 0
        Me.gbRemarks.Text = "Assessment Comments By Employee"
        Me.gbRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbRemarks.Visible = False
        '
        'objlblRvrCmts
        '
        Me.objlblRvrCmts.Location = New System.Drawing.Point(7, 164)
        Me.objlblRvrCmts.Name = "objlblRvrCmts"
        Me.objlblRvrCmts.Size = New System.Drawing.Size(502, 16)
        Me.objlblRvrCmts.TabIndex = 496
        Me.objlblRvrCmts.Text = "#Value"
        '
        'txtReviewerCmts
        '
        Me.txtReviewerCmts.BackColor = System.Drawing.Color.White
        Me.txtReviewerCmts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReviewerCmts.Location = New System.Drawing.Point(10, 185)
        Me.txtReviewerCmts.Multiline = True
        Me.txtReviewerCmts.Name = "txtReviewerCmts"
        Me.txtReviewerCmts.ReadOnly = True
        Me.txtReviewerCmts.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtReviewerCmts.Size = New System.Drawing.Size(499, 104)
        Me.txtReviewerCmts.TabIndex = 495
        '
        'txtAssessorCmts
        '
        Me.txtAssessorCmts.BackColor = System.Drawing.Color.White
        Me.txtAssessorCmts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAssessorCmts.Location = New System.Drawing.Point(10, 49)
        Me.txtAssessorCmts.Multiline = True
        Me.txtAssessorCmts.Name = "txtAssessorCmts"
        Me.txtAssessorCmts.ReadOnly = True
        Me.txtAssessorCmts.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAssessorCmts.Size = New System.Drawing.Size(499, 104)
        Me.txtAssessorCmts.TabIndex = 494
        '
        'objlblAsrCmts
        '
        Me.objlblAsrCmts.Location = New System.Drawing.Point(7, 30)
        Me.objlblAsrCmts.Name = "objlblAsrCmts"
        Me.objlblAsrCmts.Size = New System.Drawing.Size(502, 16)
        Me.objlblAsrCmts.TabIndex = 493
        Me.objlblAsrCmts.Text = "#Value"
        '
        'objlnkCloseRemark
        '
        Me.objlnkCloseRemark.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlnkCloseRemark.AutoSize = True
        Me.objlnkCloseRemark.BackColor = System.Drawing.Color.Transparent
        Me.objlnkCloseRemark.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkCloseRemark.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.objlnkCloseRemark.LinkColor = System.Drawing.Color.Blue
        Me.objlnkCloseRemark.Location = New System.Drawing.Point(495, 6)
        Me.objlnkCloseRemark.Name = "objlnkCloseRemark"
        Me.objlnkCloseRemark.Size = New System.Drawing.Size(15, 14)
        Me.objlnkCloseRemark.TabIndex = 489
        Me.objlnkCloseRemark.TabStop = True
        Me.objlnkCloseRemark.Text = "X"
        Me.objlnkCloseRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objgbDescription
        '
        Me.objgbDescription.BorderColor = System.Drawing.Color.Black
        Me.objgbDescription.Checked = False
        Me.objgbDescription.CollapseAllExceptThis = False
        Me.objgbDescription.CollapsedHoverImage = Nothing
        Me.objgbDescription.CollapsedNormalImage = Nothing
        Me.objgbDescription.CollapsedPressedImage = Nothing
        Me.objgbDescription.CollapseOnLoad = False
        Me.objgbDescription.Controls.Add(Me.txtDescription)
        Me.objgbDescription.Controls.Add(Me.objlnkCloseDesc)
        Me.objgbDescription.ExpandedHoverImage = Nothing
        Me.objgbDescription.ExpandedNormalImage = Nothing
        Me.objgbDescription.ExpandedPressedImage = Nothing
        Me.objgbDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbDescription.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbDescription.HeaderHeight = 25
        Me.objgbDescription.HeaderMessage = ""
        Me.objgbDescription.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbDescription.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbDescription.HeightOnCollapse = 0
        Me.objgbDescription.LeftTextSpace = 0
        Me.objgbDescription.Location = New System.Drawing.Point(539, 163)
        Me.objgbDescription.Name = "objgbDescription"
        Me.objgbDescription.OpenHeight = 300
        Me.objgbDescription.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbDescription.ShowBorder = True
        Me.objgbDescription.ShowCheckBox = False
        Me.objgbDescription.ShowCollapseButton = False
        Me.objgbDescription.ShowDefaultBorderColor = True
        Me.objgbDescription.ShowDownButton = False
        Me.objgbDescription.ShowHeader = True
        Me.objgbDescription.Size = New System.Drawing.Size(366, 132)
        Me.objgbDescription.TabIndex = 392
        Me.objgbDescription.Temp = 0
        Me.objgbDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.objgbDescription.Visible = False
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.Color.White
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Location = New System.Drawing.Point(2, 26)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(362, 104)
        Me.txtDescription.TabIndex = 491
        '
        'objlnkCloseDesc
        '
        Me.objlnkCloseDesc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlnkCloseDesc.AutoSize = True
        Me.objlnkCloseDesc.BackColor = System.Drawing.Color.Transparent
        Me.objlnkCloseDesc.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkCloseDesc.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.objlnkCloseDesc.LinkColor = System.Drawing.Color.Blue
        Me.objlnkCloseDesc.Location = New System.Drawing.Point(345, 6)
        Me.objlnkCloseDesc.Name = "objlnkCloseDesc"
        Me.objlnkCloseDesc.Size = New System.Drawing.Size(15, 14)
        Me.objlnkCloseDesc.TabIndex = 489
        Me.objlnkCloseDesc.TabStop = True
        Me.objlnkCloseDesc.Text = "X"
        Me.objlnkCloseDesc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objgbScoreGuide
        '
        Me.objgbScoreGuide.BorderColor = System.Drawing.Color.Black
        Me.objgbScoreGuide.Checked = False
        Me.objgbScoreGuide.CollapseAllExceptThis = False
        Me.objgbScoreGuide.CollapsedHoverImage = Nothing
        Me.objgbScoreGuide.CollapsedNormalImage = Nothing
        Me.objgbScoreGuide.CollapsedPressedImage = Nothing
        Me.objgbScoreGuide.CollapseOnLoad = False
        Me.objgbScoreGuide.Controls.Add(Me.objlnkClose)
        Me.objgbScoreGuide.Controls.Add(Me.lvScaleList)
        Me.objgbScoreGuide.ExpandedHoverImage = Nothing
        Me.objgbScoreGuide.ExpandedNormalImage = Nothing
        Me.objgbScoreGuide.ExpandedPressedImage = Nothing
        Me.objgbScoreGuide.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbScoreGuide.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbScoreGuide.HeaderHeight = 25
        Me.objgbScoreGuide.HeaderMessage = ""
        Me.objgbScoreGuide.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbScoreGuide.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbScoreGuide.HeightOnCollapse = 0
        Me.objgbScoreGuide.LeftTextSpace = 0
        Me.objgbScoreGuide.Location = New System.Drawing.Point(539, 296)
        Me.objgbScoreGuide.Name = "objgbScoreGuide"
        Me.objgbScoreGuide.OpenHeight = 300
        Me.objgbScoreGuide.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbScoreGuide.ShowBorder = True
        Me.objgbScoreGuide.ShowCheckBox = False
        Me.objgbScoreGuide.ShowCollapseButton = False
        Me.objgbScoreGuide.ShowDefaultBorderColor = True
        Me.objgbScoreGuide.ShowDownButton = False
        Me.objgbScoreGuide.ShowHeader = True
        Me.objgbScoreGuide.Size = New System.Drawing.Size(473, 209)
        Me.objgbScoreGuide.TabIndex = 389
        Me.objgbScoreGuide.Temp = 0
        Me.objgbScoreGuide.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.objgbScoreGuide.Visible = False
        '
        'objlnkClose
        '
        Me.objlnkClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlnkClose.AutoSize = True
        Me.objlnkClose.BackColor = System.Drawing.Color.Transparent
        Me.objlnkClose.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkClose.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.objlnkClose.LinkColor = System.Drawing.Color.Blue
        Me.objlnkClose.Location = New System.Drawing.Point(452, 6)
        Me.objlnkClose.Name = "objlnkClose"
        Me.objlnkClose.Size = New System.Drawing.Size(15, 14)
        Me.objlnkClose.TabIndex = 489
        Me.objlnkClose.TabStop = True
        Me.objlnkClose.Text = "X"
        Me.objlnkClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lvScaleList
        '
        Me.lvScaleList.BackColorOnChecked = False
        Me.lvScaleList.ColumnHeaders = Nothing
        Me.lvScaleList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhScale, Me.colhDescription})
        Me.lvScaleList.CompulsoryColumns = ""
        Me.lvScaleList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvScaleList.FullRowSelect = True
        Me.lvScaleList.GridLines = True
        Me.lvScaleList.GroupingColumn = Nothing
        Me.lvScaleList.HideSelection = False
        Me.lvScaleList.Location = New System.Drawing.Point(2, 26)
        Me.lvScaleList.MinColumnWidth = 50
        Me.lvScaleList.MultiSelect = False
        Me.lvScaleList.Name = "lvScaleList"
        Me.lvScaleList.OptionalColumns = ""
        Me.lvScaleList.ShowMoreItem = False
        Me.lvScaleList.ShowSaveItem = False
        Me.lvScaleList.ShowSelectAll = True
        Me.lvScaleList.ShowSizeAllColumnsToFit = True
        Me.lvScaleList.Size = New System.Drawing.Size(469, 181)
        Me.lvScaleList.Sortable = True
        Me.lvScaleList.TabIndex = 488
        Me.lvScaleList.UseCompatibleStateImageBehavior = False
        Me.lvScaleList.View = System.Windows.Forms.View.Details
        '
        'colhScale
        '
        Me.colhScale.Tag = "colhScale"
        Me.colhScale.Text = "Scale"
        Me.colhScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhScale.Width = 70
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 395
        '
        'objpnlCItems
        '
        Me.objpnlCItems.Controls.Add(Me.dgvItems)
        Me.objpnlCItems.Location = New System.Drawing.Point(2, 135)
        Me.objpnlCItems.Name = "objpnlCItems"
        Me.objpnlCItems.Size = New System.Drawing.Size(1012, 26)
        Me.objpnlCItems.TabIndex = 391
        Me.objpnlCItems.Visible = False
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToResizeRows = False
        Me.dgvItems.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhAdd, Me.objdgcolhEdit, Me.objdgcolhDelete})
        Me.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvItems.Location = New System.Drawing.Point(0, 0)
        Me.dgvItems.MultiSelect = False
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.RowHeadersVisible = False
        Me.dgvItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvItems.Size = New System.Drawing.Size(1012, 26)
        Me.dgvItems.TabIndex = 2
        '
        'objdgcolhAdd
        '
        Me.objdgcolhAdd.Frozen = True
        Me.objdgcolhAdd.HeaderText = ""
        Me.objdgcolhAdd.Image = Global.Aruti.Main.My.Resources.Resources.add_16
        Me.objdgcolhAdd.Name = "objdgcolhAdd"
        Me.objdgcolhAdd.ReadOnly = True
        Me.objdgcolhAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAdd.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objpnlGE
        '
        Me.objpnlGE.Controls.Add(Me.dgvGE)
        Me.objpnlGE.Location = New System.Drawing.Point(2, 87)
        Me.objpnlGE.Name = "objpnlGE"
        Me.objpnlGE.Size = New System.Drawing.Size(1012, 42)
        Me.objpnlGE.TabIndex = 390
        Me.objpnlGE.Visible = False
        '
        'dgvGE
        '
        Me.dgvGE.AllowUserToAddRows = False
        Me.dgvGE.AllowUserToDeleteRows = False
        Me.dgvGE.AllowUserToResizeColumns = False
        Me.dgvGE.AllowUserToResizeRows = False
        Me.dgvGE.BackgroundColor = System.Drawing.Color.White
        Me.dgvGE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvGE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvGE.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhGECollapse, Me.dgcolheval_itemGE, Me.objdgcolhInfo, Me.dgcolhGEWeight, Me.dgcolhGEScore, Me.dgcolheselfGE, Me.objdgcolhedisplayGE, Me.dgcolheremarkGE, Me.dgcolhaselfGE, Me.objdgcolhadisplayGE, Me.dgcolharemarkGE, Me.dgcolhaAgreedScoreGE, Me.dgcolhrselfGE, Me.objdgcolhrdisplayGE, Me.dgcolhrremarkGE, Me.objdgcolhscalemasterunkidGE, Me.objdgcolhcompetenciesunkidGE, Me.objdgcolhassessgroupunkidGE, Me.objdgcolhIsGrpGE, Me.objdgcolhIsPGrpGE, Me.objdgcolhGrpIdGE})
        Me.dgvGE.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvGE.Location = New System.Drawing.Point(0, 0)
        Me.dgvGE.MultiSelect = False
        Me.dgvGE.Name = "dgvGE"
        Me.dgvGE.RowHeadersVisible = False
        Me.dgvGE.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvGE.Size = New System.Drawing.Size(1012, 42)
        Me.dgvGE.TabIndex = 1
        '
        'objdgcolhGECollapse
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhGECollapse.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhGECollapse.Frozen = True
        Me.objdgcolhGECollapse.HeaderText = ""
        Me.objdgcolhGECollapse.Name = "objdgcolhGECollapse"
        Me.objdgcolhGECollapse.ReadOnly = True
        Me.objdgcolhGECollapse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGECollapse.Width = 25
        '
        'dgcolheval_itemGE
        '
        Me.dgcolheval_itemGE.HeaderText = "Items"
        Me.dgcolheval_itemGE.Name = "dgcolheval_itemGE"
        Me.dgcolheval_itemGE.ReadOnly = True
        Me.dgcolheval_itemGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolheval_itemGE.Width = 800
        '
        'objdgcolhInfo
        '
        Me.objdgcolhInfo.HeaderText = ""
        Me.objdgcolhInfo.Name = "objdgcolhInfo"
        Me.objdgcolhInfo.ReadOnly = True
        Me.objdgcolhInfo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhInfo.Width = 25
        '
        'dgcolhGEWeight
        '
        Me.dgcolhGEWeight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.dgcolhGEWeight.HeaderText = "Weight"
        Me.dgcolhGEWeight.Name = "dgcolhGEWeight"
        Me.dgcolhGEWeight.ReadOnly = True
        Me.dgcolhGEWeight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhGEWeight.Width = 47
        '
        'dgcolhGEScore
        '
        Me.dgcolhGEScore.HeaderText = "Score Guide"
        Me.dgcolhGEScore.Name = "dgcolhGEScore"
        Me.dgcolhGEScore.ReadOnly = True
        Me.dgcolhGEScore.Width = 80
        '
        'dgcolheselfGE
        '
        Me.dgcolheselfGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgcolheselfGE.DecimalLength = 2
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F2"
        Me.dgcolheselfGE.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolheselfGE.HeaderText = "Self Score"
        Me.dgcolheselfGE.Name = "dgcolheselfGE"
        Me.dgcolheselfGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolheselfGE.Width = 61
        '
        'objdgcolhedisplayGE
        '
        Me.objdgcolhedisplayGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.objdgcolhedisplayGE.DefaultCellStyle = DataGridViewCellStyle3
        Me.objdgcolhedisplayGE.HeaderText = "Final Score"
        Me.objdgcolhedisplayGE.Name = "objdgcolhedisplayGE"
        Me.objdgcolhedisplayGE.ReadOnly = True
        Me.objdgcolhedisplayGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhedisplayGE.Width = 65
        '
        'dgcolheremarkGE
        '
        Me.dgcolheremarkGE.HeaderText = "Self Remark"
        Me.dgcolheremarkGE.Name = "dgcolheremarkGE"
        Me.dgcolheremarkGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolheremarkGE.Width = 200
        '
        'dgcolhaselfGE
        '
        Me.dgcolhaselfGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgcolhaselfGE.DecimalLength = 2
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F2"
        Me.dgcolhaselfGE.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhaselfGE.HeaderText = "Assessor Score"
        Me.dgcolhaselfGE.Name = "dgcolhaselfGE"
        Me.dgcolhaselfGE.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhaselfGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhaselfGE.Width = 86
        '
        'objdgcolhadisplayGE
        '
        Me.objdgcolhadisplayGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.objdgcolhadisplayGE.DefaultCellStyle = DataGridViewCellStyle5
        Me.objdgcolhadisplayGE.HeaderText = "Final Score"
        Me.objdgcolhadisplayGE.Name = "objdgcolhadisplayGE"
        Me.objdgcolhadisplayGE.ReadOnly = True
        Me.objdgcolhadisplayGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhadisplayGE.Width = 65
        '
        'dgcolharemarkGE
        '
        Me.dgcolharemarkGE.HeaderText = "Assessor Remark"
        Me.dgcolharemarkGE.Name = "dgcolharemarkGE"
        Me.dgcolharemarkGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolharemarkGE.Width = 200
        '
        'dgcolhaAgreedScoreGE
        '
        Me.dgcolhaAgreedScoreGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgcolhaAgreedScoreGE.DecimalLength = 2
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "F2"
        Me.dgcolhaAgreedScoreGE.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhaAgreedScoreGE.HeaderText = "Agreed Score"
        Me.dgcolhaAgreedScoreGE.Name = "dgcolhaAgreedScoreGE"
        Me.dgcolhaAgreedScoreGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhaAgreedScoreGE.Width = 78
        '
        'dgcolhrselfGE
        '
        Me.dgcolhrselfGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgcolhrselfGE.DecimalLength = 2
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "F2"
        Me.dgcolhrselfGE.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgcolhrselfGE.HeaderText = "Reviewer Self"
        Me.dgcolhrselfGE.Name = "dgcolhrselfGE"
        Me.dgcolhrselfGE.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhrselfGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhrselfGE.Width = 79
        '
        'objdgcolhrdisplayGE
        '
        Me.objdgcolhrdisplayGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.objdgcolhrdisplayGE.DefaultCellStyle = DataGridViewCellStyle8
        Me.objdgcolhrdisplayGE.HeaderText = "Final Score"
        Me.objdgcolhrdisplayGE.Name = "objdgcolhrdisplayGE"
        Me.objdgcolhrdisplayGE.ReadOnly = True
        Me.objdgcolhrdisplayGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhrdisplayGE.Width = 65
        '
        'dgcolhrremarkGE
        '
        Me.dgcolhrremarkGE.HeaderText = "Reviewer Remark"
        Me.dgcolhrremarkGE.Name = "dgcolhrremarkGE"
        Me.dgcolhrremarkGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhrremarkGE.Width = 200
        '
        'objdgcolhscalemasterunkidGE
        '
        Me.objdgcolhscalemasterunkidGE.HeaderText = "objdgcolhscalemasterunkid"
        Me.objdgcolhscalemasterunkidGE.Name = "objdgcolhscalemasterunkidGE"
        Me.objdgcolhscalemasterunkidGE.ReadOnly = True
        Me.objdgcolhscalemasterunkidGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhscalemasterunkidGE.Visible = False
        '
        'objdgcolhcompetenciesunkidGE
        '
        Me.objdgcolhcompetenciesunkidGE.HeaderText = "objdgcolhcompetenciesunkid"
        Me.objdgcolhcompetenciesunkidGE.Name = "objdgcolhcompetenciesunkidGE"
        Me.objdgcolhcompetenciesunkidGE.ReadOnly = True
        Me.objdgcolhcompetenciesunkidGE.Visible = False
        '
        'objdgcolhassessgroupunkidGE
        '
        Me.objdgcolhassessgroupunkidGE.HeaderText = "dgcolhassessgroupunkid"
        Me.objdgcolhassessgroupunkidGE.Name = "objdgcolhassessgroupunkidGE"
        Me.objdgcolhassessgroupunkidGE.ReadOnly = True
        Me.objdgcolhassessgroupunkidGE.Visible = False
        '
        'objdgcolhIsGrpGE
        '
        Me.objdgcolhIsGrpGE.HeaderText = "dgcolhIsGrp"
        Me.objdgcolhIsGrpGE.Name = "objdgcolhIsGrpGE"
        Me.objdgcolhIsGrpGE.ReadOnly = True
        Me.objdgcolhIsGrpGE.Visible = False
        '
        'objdgcolhIsPGrpGE
        '
        Me.objdgcolhIsPGrpGE.HeaderText = "dgcolhIsPGrp"
        Me.objdgcolhIsPGrpGE.Name = "objdgcolhIsPGrpGE"
        Me.objdgcolhIsPGrpGE.ReadOnly = True
        Me.objdgcolhIsPGrpGE.Visible = False
        '
        'objdgcolhGrpIdGE
        '
        Me.objdgcolhGrpIdGE.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpIdGE.Name = "objdgcolhGrpIdGE"
        Me.objdgcolhGrpIdGE.ReadOnly = True
        Me.objdgcolhGrpIdGE.Visible = False
        '
        'objpnlBSC
        '
        Me.objpnlBSC.Controls.Add(Me.picStayView)
        Me.objpnlBSC.Controls.Add(Me.dgvBSC)
        Me.objpnlBSC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlBSC.Location = New System.Drawing.Point(2, 41)
        Me.objpnlBSC.Name = "objpnlBSC"
        Me.objpnlBSC.Size = New System.Drawing.Size(1012, 40)
        Me.objpnlBSC.TabIndex = 388
        Me.objpnlBSC.Visible = False
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(927, 380)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 35
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'dgvBSC
        '
        Me.dgvBSC.AllowUserToAddRows = False
        Me.dgvBSC.AllowUserToDeleteRows = False
        Me.dgvBSC.AllowUserToResizeRows = False
        Me.dgvBSC.BackgroundColor = System.Drawing.Color.White
        Me.dgvBSC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvBSC.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhBSCCollaps, Me.objdgcolhBSCField1, Me.objdgcolhBSCField2, Me.objdgcolhBSCField3, Me.objdgcolhBSCField4, Me.objdgcolhBSCField5, Me.objdgcolhBSCField6, Me.objdgcolhBSCField7, Me.objdgcolhBSCField8, Me.dgcolhSDate, Me.dgcolhEDate, Me.dgcolhCompleted, Me.dgcolhStatus, Me.dgcolhGoalType, Me.dgcolhGoalvalue, Me.dgcolhBSCScore, Me.dgcolhBSCWeight, Me.dgcolheselfBSC, Me.dgcolheremarkBSC, Me.dgcolhaselfBSC, Me.dgcolharemarkBSC, Me.dgcolhAgreedScoreBSC, Me.dgcolhrselfBSC, Me.dgcolhrremarkBSC, Me.objdgcolhIsGrpBSC, Me.objdgcolhGrpIdBSC, Me.objdgcolhScaleMasterId})
        Me.dgvBSC.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBSC.Location = New System.Drawing.Point(0, 0)
        Me.dgvBSC.MultiSelect = False
        Me.dgvBSC.Name = "dgvBSC"
        Me.dgvBSC.RowHeadersVisible = False
        Me.dgvBSC.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvBSC.Size = New System.Drawing.Size(1012, 40)
        Me.dgvBSC.TabIndex = 0
        '
        'objpnlInstruction
        '
        Me.objpnlInstruction.Controls.Add(Me.txtInstruction)
        Me.objpnlInstruction.Location = New System.Drawing.Point(3, 3)
        Me.objpnlInstruction.Name = "objpnlInstruction"
        Me.objpnlInstruction.Size = New System.Drawing.Size(1009, 32)
        Me.objpnlInstruction.TabIndex = 41
        '
        'txtInstruction
        '
        Me.txtInstruction.BackColor = System.Drawing.Color.White
        Me.txtInstruction.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtInstruction.Location = New System.Drawing.Point(0, 0)
        Me.txtInstruction.Multiline = True
        Me.txtInstruction.Name = "txtInstruction"
        Me.txtInstruction.ReadOnly = True
        Me.txtInstruction.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInstruction.ShortcutsEnabled = False
        Me.txtInstruction.Size = New System.Drawing.Size(1009, 32)
        Me.txtInstruction.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.tblpInformation)
        Me.objFooter.Controls.Add(Me.objbtnBack)
        Me.objFooter.Controls.Add(Me.objbtnNext)
        Me.objFooter.Controls.Add(Me.btnSaveCommit)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 602)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1018, 50)
        Me.objFooter.TabIndex = 3
        '
        'tblpInformation
        '
        Me.tblpInformation.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tblpInformation.ColumnCount = 4
        Me.tblpInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tblpInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tblpInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tblpInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tblpInformation.Controls.Add(Me.objlblValue4, 3, 0)
        Me.tblpInformation.Controls.Add(Me.objlblValue3, 2, 0)
        Me.tblpInformation.Controls.Add(Me.objlblValue2, 1, 0)
        Me.tblpInformation.Controls.Add(Me.objlblValue1, 0, 0)
        Me.tblpInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpInformation.Location = New System.Drawing.Point(83, 15)
        Me.tblpInformation.Name = "tblpInformation"
        Me.tblpInformation.RowCount = 1
        Me.tblpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpInformation.Size = New System.Drawing.Size(619, 21)
        Me.tblpInformation.TabIndex = 390
        '
        'objlblValue4
        '
        Me.objlblValue4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblValue4.Location = New System.Drawing.Point(466, 1)
        Me.objlblValue4.Name = "objlblValue4"
        Me.objlblValue4.Size = New System.Drawing.Size(149, 19)
        Me.objlblValue4.TabIndex = 393
        Me.objlblValue4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblValue3
        '
        Me.objlblValue3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblValue3.Location = New System.Drawing.Point(312, 1)
        Me.objlblValue3.Name = "objlblValue3"
        Me.objlblValue3.Size = New System.Drawing.Size(147, 19)
        Me.objlblValue3.TabIndex = 392
        Me.objlblValue3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblValue2
        '
        Me.objlblValue2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblValue2.Location = New System.Drawing.Point(158, 1)
        Me.objlblValue2.Name = "objlblValue2"
        Me.objlblValue2.Size = New System.Drawing.Size(147, 19)
        Me.objlblValue2.TabIndex = 391
        Me.objlblValue2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblValue1
        '
        Me.objlblValue1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblValue1.Location = New System.Drawing.Point(4, 1)
        Me.objlblValue1.Name = "objlblValue1"
        Me.objlblValue1.Size = New System.Drawing.Size(147, 19)
        Me.objlblValue1.TabIndex = 390
        Me.objlblValue1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnBack
        '
        Me.objbtnBack.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnBack.BackColor = System.Drawing.Color.White
        Me.objbtnBack.BackgroundImage = CType(resources.GetObject("objbtnBack.BackgroundImage"), System.Drawing.Image)
        Me.objbtnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnBack.BorderColor = System.Drawing.Color.Empty
        Me.objbtnBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnBack.FlatAppearance.BorderSize = 0
        Me.objbtnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnBack.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnBack.ForeColor = System.Drawing.Color.Black
        Me.objbtnBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBack.Image = Global.Aruti.Main.My.Resources.Resources.left_arrow_16
        Me.objbtnBack.Location = New System.Drawing.Point(11, 10)
        Me.objbtnBack.Name = "objbtnBack"
        Me.objbtnBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBack.Size = New System.Drawing.Size(30, 30)
        Me.objbtnBack.TabIndex = 3
        Me.objbtnBack.UseVisualStyleBackColor = True
        '
        'objbtnNext
        '
        Me.objbtnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnNext.BackColor = System.Drawing.Color.White
        Me.objbtnNext.BackgroundImage = CType(resources.GetObject("objbtnNext.BackgroundImage"), System.Drawing.Image)
        Me.objbtnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnNext.BorderColor = System.Drawing.Color.Empty
        Me.objbtnNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnNext.FlatAppearance.BorderSize = 0
        Me.objbtnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnNext.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnNext.ForeColor = System.Drawing.Color.Black
        Me.objbtnNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.objbtnNext.Location = New System.Drawing.Point(47, 10)
        Me.objbtnNext.Name = "objbtnNext"
        Me.objbtnNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.Size = New System.Drawing.Size(30, 30)
        Me.objbtnNext.TabIndex = 4
        Me.objbtnNext.UseVisualStyleBackColor = True
        '
        'btnSaveCommit
        '
        Me.btnSaveCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveCommit.BackColor = System.Drawing.Color.White
        Me.btnSaveCommit.BackgroundImage = CType(resources.GetObject("btnSaveCommit.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveCommit.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveCommit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveCommit.FlatAppearance.BorderSize = 0
        Me.btnSaveCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveCommit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveCommit.ForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveCommit.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Location = New System.Drawing.Point(708, 11)
        Me.btnSaveCommit.Name = "btnSaveCommit"
        Me.btnSaveCommit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Size = New System.Drawing.Size(110, 30)
        Me.btnSaveCommit.TabIndex = 0
        Me.btnSaveCommit.Text = "S&ave && Commit"
        Me.btnSaveCommit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(824, 11)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(88, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(918, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(88, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 25
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Items"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 800
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.DataGridViewTextBoxColumn3.HeaderText = "Weight"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridViewTextBoxColumn4.HeaderText = "Final Score"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Self Remark"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 200
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridViewTextBoxColumn6.HeaderText = "Final Score"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Assessor Remark"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle18
        Me.DataGridViewTextBoxColumn8.HeaderText = "Final Score"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Reviewer Remark"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 200
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhscalemasterunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhcompetenciesunkid"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "dgcolhassessgroupunkid"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "dgcolhIsGrp"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "dgcolhIsPGrp"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn16.DefaultCellStyle = DataGridViewCellStyle19
        Me.DataGridViewTextBoxColumn16.HeaderText = ""
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Width = 25
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = ""
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Width = 150
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = ""
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Width = 150
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = ""
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Width = 150
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = ""
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn20.Width = 150
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = ""
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn21.Width = 150
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = ""
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn22.Width = 150
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = ""
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn23.Width = 150
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = ""
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn24.Width = 150
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "Start Date"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "End Date"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "% Completed"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "Wgt."
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn29.Width = 35
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn30.Width = 200
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn31.Width = 200
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn32.Width = 200
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "objdgcolhScaleMasterId"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.Visible = False
        '
        'objdgcolhBSCCollaps
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhBSCCollaps.DefaultCellStyle = DataGridViewCellStyle9
        Me.objdgcolhBSCCollaps.HeaderText = ""
        Me.objdgcolhBSCCollaps.Name = "objdgcolhBSCCollaps"
        Me.objdgcolhBSCCollaps.ReadOnly = True
        Me.objdgcolhBSCCollaps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCCollaps.Width = 25
        '
        'objdgcolhBSCField1
        '
        Me.objdgcolhBSCField1.HeaderText = ""
        Me.objdgcolhBSCField1.Name = "objdgcolhBSCField1"
        Me.objdgcolhBSCField1.ReadOnly = True
        Me.objdgcolhBSCField1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField1.Width = 150
        '
        'objdgcolhBSCField2
        '
        Me.objdgcolhBSCField2.HeaderText = ""
        Me.objdgcolhBSCField2.Name = "objdgcolhBSCField2"
        Me.objdgcolhBSCField2.ReadOnly = True
        Me.objdgcolhBSCField2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField2.Width = 150
        '
        'objdgcolhBSCField3
        '
        Me.objdgcolhBSCField3.HeaderText = ""
        Me.objdgcolhBSCField3.Name = "objdgcolhBSCField3"
        Me.objdgcolhBSCField3.ReadOnly = True
        Me.objdgcolhBSCField3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField3.Width = 150
        '
        'objdgcolhBSCField4
        '
        Me.objdgcolhBSCField4.HeaderText = ""
        Me.objdgcolhBSCField4.Name = "objdgcolhBSCField4"
        Me.objdgcolhBSCField4.ReadOnly = True
        Me.objdgcolhBSCField4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField4.Width = 150
        '
        'objdgcolhBSCField5
        '
        Me.objdgcolhBSCField5.HeaderText = ""
        Me.objdgcolhBSCField5.Name = "objdgcolhBSCField5"
        Me.objdgcolhBSCField5.ReadOnly = True
        Me.objdgcolhBSCField5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField5.Width = 150
        '
        'objdgcolhBSCField6
        '
        Me.objdgcolhBSCField6.HeaderText = ""
        Me.objdgcolhBSCField6.Name = "objdgcolhBSCField6"
        Me.objdgcolhBSCField6.ReadOnly = True
        Me.objdgcolhBSCField6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField6.Width = 150
        '
        'objdgcolhBSCField7
        '
        Me.objdgcolhBSCField7.HeaderText = ""
        Me.objdgcolhBSCField7.Name = "objdgcolhBSCField7"
        Me.objdgcolhBSCField7.ReadOnly = True
        Me.objdgcolhBSCField7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField7.Width = 150
        '
        'objdgcolhBSCField8
        '
        Me.objdgcolhBSCField8.HeaderText = ""
        Me.objdgcolhBSCField8.Name = "objdgcolhBSCField8"
        Me.objdgcolhBSCField8.ReadOnly = True
        Me.objdgcolhBSCField8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBSCField8.Width = 150
        '
        'dgcolhSDate
        '
        Me.dgcolhSDate.HeaderText = "Start Date"
        Me.dgcolhSDate.Name = "dgcolhSDate"
        Me.dgcolhSDate.ReadOnly = True
        Me.dgcolhSDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEDate
        '
        Me.dgcolhEDate.HeaderText = "End Date"
        Me.dgcolhEDate.Name = "dgcolhEDate"
        Me.dgcolhEDate.ReadOnly = True
        Me.dgcolhEDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCompleted
        '
        Me.dgcolhCompleted.HeaderText = "% Completed"
        Me.dgcolhCompleted.Name = "dgcolhCompleted"
        Me.dgcolhCompleted.ReadOnly = True
        Me.dgcolhCompleted.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGoalType
        '
        Me.dgcolhGoalType.HeaderText = "Goal Type"
        Me.dgcolhGoalType.Name = "dgcolhGoalType"
        Me.dgcolhGoalType.ReadOnly = True
        Me.dgcolhGoalType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGoalvalue
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhGoalvalue.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgcolhGoalvalue.HeaderText = "Goal Value"
        Me.dgcolhGoalvalue.Name = "dgcolhGoalvalue"
        Me.dgcolhGoalvalue.ReadOnly = True
        Me.dgcolhGoalvalue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhBSCScore
        '
        Me.dgcolhBSCScore.HeaderText = "Score Guide"
        Me.dgcolhBSCScore.Name = "dgcolhBSCScore"
        Me.dgcolhBSCScore.ReadOnly = True
        '
        'dgcolhBSCWeight
        '
        Me.dgcolhBSCWeight.HeaderText = "Wgt."
        Me.dgcolhBSCWeight.Name = "dgcolhBSCWeight"
        Me.dgcolhBSCWeight.ReadOnly = True
        Me.dgcolhBSCWeight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBSCWeight.Width = 35
        '
        'dgcolheselfBSC
        '
        Me.dgcolheselfBSC.DecimalLength = 2
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "F2"
        Me.dgcolheselfBSC.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgcolheselfBSC.HeaderText = "Result"
        Me.dgcolheselfBSC.Name = "dgcolheselfBSC"
        Me.dgcolheselfBSC.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolheselfBSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolheselfBSC.Width = 35
        '
        'dgcolheremarkBSC
        '
        Me.dgcolheremarkBSC.HeaderText = "Remark"
        Me.dgcolheremarkBSC.Name = "dgcolheremarkBSC"
        Me.dgcolheremarkBSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolheremarkBSC.Width = 200
        '
        'dgcolhaselfBSC
        '
        Me.dgcolhaselfBSC.DecimalLength = 2
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "F2"
        Me.dgcolhaselfBSC.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgcolhaselfBSC.HeaderText = "Result"
        Me.dgcolhaselfBSC.Name = "dgcolhaselfBSC"
        Me.dgcolhaselfBSC.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhaselfBSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhaselfBSC.Width = 35
        '
        'dgcolharemarkBSC
        '
        Me.dgcolharemarkBSC.HeaderText = "Remark"
        Me.dgcolharemarkBSC.Name = "dgcolharemarkBSC"
        Me.dgcolharemarkBSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolharemarkBSC.Width = 200
        '
        'dgcolhAgreedScoreBSC
        '
        Me.dgcolhAgreedScoreBSC.DecimalLength = 2
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "F2"
        Me.dgcolhAgreedScoreBSC.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgcolhAgreedScoreBSC.HeaderText = "Agreed Score"
        Me.dgcolhAgreedScoreBSC.Name = "dgcolhAgreedScoreBSC"
        Me.dgcolhAgreedScoreBSC.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAgreedScoreBSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAgreedScoreBSC.Width = 60
        '
        'dgcolhrselfBSC
        '
        Me.dgcolhrselfBSC.DecimalLength = 2
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "F2"
        Me.dgcolhrselfBSC.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgcolhrselfBSC.HeaderText = "Result"
        Me.dgcolhrselfBSC.Name = "dgcolhrselfBSC"
        Me.dgcolhrselfBSC.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhrselfBSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhrselfBSC.Width = 35
        '
        'dgcolhrremarkBSC
        '
        Me.dgcolhrremarkBSC.HeaderText = "Remark"
        Me.dgcolhrremarkBSC.Name = "dgcolhrremarkBSC"
        Me.dgcolhrremarkBSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhrremarkBSC.Width = 200
        '
        'objdgcolhIsGrpBSC
        '
        Me.objdgcolhIsGrpBSC.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrpBSC.Name = "objdgcolhIsGrpBSC"
        Me.objdgcolhIsGrpBSC.ReadOnly = True
        Me.objdgcolhIsGrpBSC.Visible = False
        '
        'objdgcolhGrpIdBSC
        '
        Me.objdgcolhGrpIdBSC.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpIdBSC.Name = "objdgcolhGrpIdBSC"
        Me.objdgcolhGrpIdBSC.ReadOnly = True
        Me.objdgcolhGrpIdBSC.Visible = False
        '
        'objdgcolhScaleMasterId
        '
        Me.objdgcolhScaleMasterId.HeaderText = "objdgcolhScaleMasterId"
        Me.objdgcolhScaleMasterId.Name = "objdgcolhScaleMasterId"
        Me.objdgcolhScaleMasterId.Visible = False
        '
        'frmPerformanceEvaluation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 652)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPerformanceEvaluation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Performance Evaluation"
        Me.pnlMain.ResumeLayout(False)
        Me.objSPC.Panel1.ResumeLayout(False)
        Me.objSPC.Panel2.ResumeLayout(False)
        Me.objSPC.ResumeLayout(False)
        Me.gbBSCEvaluation.ResumeLayout(False)
        Me.gbBSCEvaluation.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.tblpContainer.ResumeLayout(False)
        Me.objpnlContainer.ResumeLayout(False)
        Me.gbRemarks.ResumeLayout(False)
        Me.gbRemarks.PerformLayout()
        Me.objgbDescription.ResumeLayout(False)
        Me.objgbDescription.PerformLayout()
        Me.objgbScoreGuide.ResumeLayout(False)
        Me.objgbScoreGuide.PerformLayout()
        Me.objpnlCItems.ResumeLayout(False)
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objpnlGE.ResumeLayout(False)
        CType(Me.dgvGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objpnlBSC.ResumeLayout(False)
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBSC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objpnlInstruction.ResumeLayout(False)
        Me.objpnlInstruction.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.tblpInformation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objSPC As System.Windows.Forms.SplitContainer
    Friend WithEvents gbBSCEvaluation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents radExternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents lblAssessor As System.Windows.Forms.Label
    Friend WithEvents radInternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents lblReviewer As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAssessor As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchReviewer As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAssessor As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpAssessdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboReviewer As System.Windows.Forms.ComboBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents tblpInformation As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblValue4 As System.Windows.Forms.Label
    Friend WithEvents objlblValue3 As System.Windows.Forms.Label
    Friend WithEvents objlblValue2 As System.Windows.Forms.Label
    Friend WithEvents objlblValue1 As System.Windows.Forms.Label
    Friend WithEvents objbtnBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnNext As eZee.Common.eZeeLightButton
    Friend WithEvents btnSaveCommit As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tblpContainer As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objpnlContainer As System.Windows.Forms.Panel
    Friend WithEvents objpnlInstruction As System.Windows.Forms.Panel
    Friend WithEvents txtInstruction As System.Windows.Forms.TextBox
    Friend WithEvents objpnlBSC As System.Windows.Forms.Panel
    Friend WithEvents objgbScoreGuide As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlnkClose As System.Windows.Forms.LinkLabel
    Friend WithEvents lvScaleList As eZee.Common.eZeeListView
    Friend WithEvents colhScale As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents dgvBSC As System.Windows.Forms.DataGridView
    Friend WithEvents objpnlGE As System.Windows.Forms.Panel
    Friend WithEvents dgvGE As System.Windows.Forms.DataGridView
    Friend WithEvents objpnlCItems As System.Windows.Forms.Panel
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents lnkCopyScore As System.Windows.Forms.LinkLabel
    Friend WithEvents objgbDescription As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents objlnkCloseDesc As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGECollapse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolheval_itemGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhInfo As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhGEWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGEScore As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents dgcolheselfGE As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objdgcolhedisplayGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolheremarkGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaselfGE As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objdgcolhadisplayGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolharemarkGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaAgreedScoreGE As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhrselfGE As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objdgcolhrdisplayGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhrremarkGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhscalemasterunkidGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhcompetenciesunkidGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhassessgroupunkidGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrpGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsPGrpGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpIdGE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkViewApproveRejectRemark As System.Windows.Forms.LinkLabel
    Friend WithEvents gbRemarks As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlnkCloseRemark As System.Windows.Forms.LinkLabel
    Friend WithEvents objlblRvrCmts As System.Windows.Forms.Label
    Friend WithEvents txtReviewerCmts As System.Windows.Forms.TextBox
    Friend WithEvents txtAssessorCmts As System.Windows.Forms.TextBox
    Friend WithEvents objlblAsrCmts As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents objdgcolhBSCCollaps As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBSCField8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCompleted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGoalType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGoalvalue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBSCScore As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents dgcolhBSCWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolheselfBSC As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolheremarkBSC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaselfBSC As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolharemarkBSC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAgreedScoreBSC As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhrselfBSC As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhrremarkBSC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrpBSC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpIdBSC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhScaleMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
