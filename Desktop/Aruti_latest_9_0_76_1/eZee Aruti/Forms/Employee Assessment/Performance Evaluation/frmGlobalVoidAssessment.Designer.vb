﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalVoidAssessment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalVoidAssessment))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbView = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkCopy = New System.Windows.Forms.LinkLabel
        Me.objlnkClose = New System.Windows.Forms.LinkLabel
        Me.lvError = New System.Windows.Forms.ListView
        Me.colhEName = New System.Windows.Forms.ColumnHeader
        Me.colhAssessmentType = New System.Windows.Forms.ColumnHeader
        Me.objcolhError = New System.Windows.Forms.ColumnHeader
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhiRead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhiMsg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblAssessmentPeriods = New System.Windows.Forms.Label
        Me.gbOperation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeCommitted = New System.Windows.Forms.CheckBox
        Me.chkReviewerAssessment = New System.Windows.Forms.CheckBox
        Me.chkAssessorAssessment = New System.Windows.Forms.CheckBox
        Me.chkSelfAssessment = New System.Windows.Forms.CheckBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objgbView.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbOperation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbView)
        Me.pnlMain.Controls.Add(Me.tblpAssessorEmployee)
        Me.pnlMain.Controls.Add(Me.gbFilter)
        Me.pnlMain.Controls.Add(Me.gbOperation)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(462, 494)
        Me.pnlMain.TabIndex = 0
        '
        'objgbView
        '
        Me.objgbView.BorderColor = System.Drawing.Color.Black
        Me.objgbView.Checked = False
        Me.objgbView.CollapseAllExceptThis = False
        Me.objgbView.CollapsedHoverImage = Nothing
        Me.objgbView.CollapsedNormalImage = Nothing
        Me.objgbView.CollapsedPressedImage = Nothing
        Me.objgbView.CollapseOnLoad = False
        Me.objgbView.Controls.Add(Me.lnkCopy)
        Me.objgbView.Controls.Add(Me.objlnkClose)
        Me.objgbView.Controls.Add(Me.lvError)
        Me.objgbView.ExpandedHoverImage = Nothing
        Me.objgbView.ExpandedNormalImage = Nothing
        Me.objgbView.ExpandedPressedImage = Nothing
        Me.objgbView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbView.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbView.HeaderHeight = 25
        Me.objgbView.HeaderMessage = ""
        Me.objgbView.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.objgbView.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbView.HeightOnCollapse = 0
        Me.objgbView.LeftTextSpace = 0
        Me.objgbView.Location = New System.Drawing.Point(29, 122)
        Me.objgbView.Name = "objgbView"
        Me.objgbView.OpenHeight = 300
        Me.objgbView.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbView.ShowBorder = True
        Me.objgbView.ShowCheckBox = False
        Me.objgbView.ShowCollapseButton = False
        Me.objgbView.ShowDefaultBorderColor = True
        Me.objgbView.ShowDownButton = False
        Me.objgbView.ShowHeader = True
        Me.objgbView.Size = New System.Drawing.Size(402, 209)
        Me.objgbView.TabIndex = 119
        Me.objgbView.Temp = 0
        Me.objgbView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.objgbView.Visible = False
        '
        'lnkCopy
        '
        Me.lnkCopy.BackColor = System.Drawing.Color.Transparent
        Me.lnkCopy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCopy.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkCopy.Location = New System.Drawing.Point(8, 4)
        Me.lnkCopy.Name = "lnkCopy"
        Me.lnkCopy.Size = New System.Drawing.Size(113, 17)
        Me.lnkCopy.TabIndex = 246
        Me.lnkCopy.TabStop = True
        Me.lnkCopy.Text = "Copy Error"
        Me.lnkCopy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlnkClose
        '
        Me.objlnkClose.AutoSize = True
        Me.objlnkClose.BackColor = System.Drawing.Color.Transparent
        Me.objlnkClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkClose.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkClose.Location = New System.Drawing.Point(381, 6)
        Me.objlnkClose.Name = "objlnkClose"
        Me.objlnkClose.Size = New System.Drawing.Size(14, 13)
        Me.objlnkClose.TabIndex = 244
        Me.objlnkClose.TabStop = True
        Me.objlnkClose.Text = "X"
        Me.objlnkClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvError
        '
        Me.lvError.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEName, Me.colhAssessmentType, Me.objcolhError})
        Me.lvError.Location = New System.Drawing.Point(3, 26)
        Me.lvError.Name = "lvError"
        Me.lvError.Size = New System.Drawing.Size(396, 181)
        Me.lvError.TabIndex = 106
        Me.lvError.UseCompatibleStateImageBehavior = False
        Me.lvError.View = System.Windows.Forms.View.Details
        '
        'colhEName
        '
        Me.colhEName.Tag = "colhEName"
        Me.colhEName.Text = "Employee"
        Me.colhEName.Width = 267
        '
        'colhAssessmentType
        '
        Me.colhAssessmentType.Tag = "colhAssessmentType"
        Me.colhAssessmentType.Text = "Assessment Mode"
        Me.colhAssessmentType.Width = 120
        '
        'objcolhError
        '
        Me.objcolhError.Tag = "objcolhError"
        Me.objcolhError.Text = "Error"
        Me.objcolhError.Width = 0
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(2, 128)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(458, 310)
        Me.tblpAssessorEmployee.TabIndex = 304
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(452, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(452, 278)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 21
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId, Me.objdgcolhiRead, Me.dgcolhiMsg})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(452, 278)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolhiRead
        '
        Me.objdgcolhiRead.HeaderText = "objdgcolhiRead"
        Me.objdgcolhiRead.Name = "objdgcolhiRead"
        Me.objdgcolhiRead.ReadOnly = True
        Me.objdgcolhiRead.Visible = False
        '
        'dgcolhiMsg
        '
        Me.dgcolhiMsg.HeaderText = "Message"
        Me.dgcolhiMsg.Name = "dgcolhiMsg"
        Me.dgcolhiMsg.ReadOnly = True
        Me.dgcolhiMsg.Width = 120
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.objbtnReset)
        Me.gbFilter.Controls.Add(Me.lnkAllocation)
        Me.gbFilter.Controls.Add(Me.objbtnSearch)
        Me.gbFilter.Controls.Add(Me.cboPeriod)
        Me.gbFilter.Controls.Add(Me.lblAssessmentPeriods)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(2, 2)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(458, 67)
        Me.gbFilter.TabIndex = 118
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(432, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 23)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(299, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(105, 17)
        Me.lnkAllocation.TabIndex = 242
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(407, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 23)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(68, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(380, 21)
        Me.cboPeriod.TabIndex = 8
        '
        'lblAssessmentPeriods
        '
        Me.lblAssessmentPeriods.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentPeriods.Location = New System.Drawing.Point(12, 36)
        Me.lblAssessmentPeriods.Name = "lblAssessmentPeriods"
        Me.lblAssessmentPeriods.Size = New System.Drawing.Size(50, 15)
        Me.lblAssessmentPeriods.TabIndex = 223
        Me.lblAssessmentPeriods.Text = "Period"
        Me.lblAssessmentPeriods.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbOperation
        '
        Me.gbOperation.BorderColor = System.Drawing.Color.Black
        Me.gbOperation.Checked = False
        Me.gbOperation.CollapseAllExceptThis = False
        Me.gbOperation.CollapsedHoverImage = Nothing
        Me.gbOperation.CollapsedNormalImage = Nothing
        Me.gbOperation.CollapsedPressedImage = Nothing
        Me.gbOperation.CollapseOnLoad = False
        Me.gbOperation.Controls.Add(Me.chkIncludeCommitted)
        Me.gbOperation.Controls.Add(Me.chkReviewerAssessment)
        Me.gbOperation.Controls.Add(Me.chkAssessorAssessment)
        Me.gbOperation.Controls.Add(Me.chkSelfAssessment)
        Me.gbOperation.ExpandedHoverImage = Nothing
        Me.gbOperation.ExpandedNormalImage = Nothing
        Me.gbOperation.ExpandedPressedImage = Nothing
        Me.gbOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOperation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOperation.HeaderHeight = 25
        Me.gbOperation.HeaderMessage = ""
        Me.gbOperation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOperation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOperation.HeightOnCollapse = 0
        Me.gbOperation.LeftTextSpace = 0
        Me.gbOperation.Location = New System.Drawing.Point(2, 70)
        Me.gbOperation.Name = "gbOperation"
        Me.gbOperation.OpenHeight = 300
        Me.gbOperation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOperation.ShowBorder = True
        Me.gbOperation.ShowCheckBox = False
        Me.gbOperation.ShowCollapseButton = False
        Me.gbOperation.ShowDefaultBorderColor = True
        Me.gbOperation.ShowDownButton = False
        Me.gbOperation.ShowHeader = True
        Me.gbOperation.Size = New System.Drawing.Size(458, 57)
        Me.gbOperation.TabIndex = 243
        Me.gbOperation.Temp = 0
        Me.gbOperation.Text = "Delete Selected"
        Me.gbOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeCommitted
        '
        Me.chkIncludeCommitted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeCommitted.Location = New System.Drawing.Point(15, 61)
        Me.chkIncludeCommitted.Name = "chkIncludeCommitted"
        Me.chkIncludeCommitted.Size = New System.Drawing.Size(433, 17)
        Me.chkIncludeCommitted.TabIndex = 228
        Me.chkIncludeCommitted.Text = "Include Committed Evaluation for deletion"
        Me.chkIncludeCommitted.UseVisualStyleBackColor = True
        Me.chkIncludeCommitted.Visible = False
        '
        'chkReviewerAssessment
        '
        Me.chkReviewerAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkReviewerAssessment.Location = New System.Drawing.Point(305, 33)
        Me.chkReviewerAssessment.Name = "chkReviewerAssessment"
        Me.chkReviewerAssessment.Size = New System.Drawing.Size(143, 17)
        Me.chkReviewerAssessment.TabIndex = 227
        Me.chkReviewerAssessment.Text = "Reviewer Evaluation"
        Me.chkReviewerAssessment.UseVisualStyleBackColor = True
        '
        'chkAssessorAssessment
        '
        Me.chkAssessorAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAssessorAssessment.Location = New System.Drawing.Point(156, 33)
        Me.chkAssessorAssessment.Name = "chkAssessorAssessment"
        Me.chkAssessorAssessment.Size = New System.Drawing.Size(143, 17)
        Me.chkAssessorAssessment.TabIndex = 226
        Me.chkAssessorAssessment.Text = "Assessor Evaluation"
        Me.chkAssessorAssessment.UseVisualStyleBackColor = True
        '
        'chkSelfAssessment
        '
        Me.chkSelfAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelfAssessment.Location = New System.Drawing.Point(15, 33)
        Me.chkSelfAssessment.Name = "chkSelfAssessment"
        Me.chkSelfAssessment.Size = New System.Drawing.Size(134, 17)
        Me.chkSelfAssessment.TabIndex = 225
        Me.chkSelfAssessment.Text = "Self Evaluation"
        Me.chkSelfAssessment.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.Label1)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 439)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(462, 55)
        Me.objFooter.TabIndex = 118
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(245, 39)
        Me.Label1.TabIndex = 244
        Me.Label1.Text = "Score processed for the selected period, checked mode and employee will also be v" & _
            "oided."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(261, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 72
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(357, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'frmGlobalVoidAssessment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 494)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalVoidAssessment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Globally Delete Evaluation"
        Me.pnlMain.ResumeLayout(False)
        Me.objgbView.ResumeLayout(False)
        Me.objgbView.PerformLayout()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilter.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbOperation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessmentPeriods As System.Windows.Forms.Label
    Friend WithEvents gbOperation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents chkIncludeCommitted As System.Windows.Forms.CheckBox
    Friend WithEvents chkReviewerAssessment As System.Windows.Forms.CheckBox
    Friend WithEvents chkAssessorAssessment As System.Windows.Forms.CheckBox
    Friend WithEvents chkSelfAssessment As System.Windows.Forms.CheckBox
    Friend WithEvents lvError As System.Windows.Forms.ListView
    Friend WithEvents colhEName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAssessmentType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objgbView As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlnkClose As System.Windows.Forms.LinkLabel
    Friend WithEvents objcolhError As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkCopy As System.Windows.Forms.LinkLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhiRead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhiMsg As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
