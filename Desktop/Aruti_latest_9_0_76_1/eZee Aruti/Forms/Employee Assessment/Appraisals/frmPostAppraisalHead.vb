﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPostAppraisalHead

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPostAppraisalHead"
    Private objED As clsEarningDeduction
    Private mdtTable As DataTable
    Private mdtEmpInfo As DataTable
    Private mstrCSVEmpIds As String = String.Empty
    Private mstrCSVTranIds As String = String.Empty
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
    Private objFinalEmp As clsAppraisal_Final_Emp

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dtTab As DataTable) As Boolean
        Try
            mdtEmpInfo = dtTab
            Me.ShowDialog()
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboTrnHead.BackColor = GUI.ColorComp
            txtAmount.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim dsList As DataSet = Nothing
        Dim objTrnFormula As New clsTranheadFormulaTran
        Dim objTranHead As New clsTransactionHead
        Dim strHeadName As String = ""
        Try
            If CInt(cboTrnHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Transaction Head. Transaction Head is mandatory information."), enMsgBoxStyle.Information)
                cboTrnHead.Focus()
                Return False
            End If

            dsList = objTrnFormula.getFlateRateHeadFromFormula(CInt(cboTrnHead.SelectedValue), "TranHead")
            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                If CInt(dsRow.Item("defaultvalue_id")) <> enTranHeadFormula_DefaultValue.COMPUTED_VALUE Then Continue For
                objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
                strHeadName = objTranHead._Trnheadname
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "The Formula of this Transaction Head contains ") & strHeadName & _
                                   Language.getMessage(mstrModuleName, 3, " Tranasaction Head (type of Flate Rate).") & vbCrLf & _
                                   Language.getMessage(mstrModuleName, 4, "If you do not assign ") & strHeadName & _
                                   Language.getMessage(mstrModuleName, 5, " Transaction Head, its value will be cosidered as Zero.") & vbCrLf & vbCrLf & _
                                   Language.getMessage(mstrModuleName, 6, "Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            Next

            If objTranHead._Formulaid.Trim = "" AndAlso (objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            Dim objPdata As New clscommom_period_Tran
            objPdata._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            Dim objProcessed As New clsTnALeaveTran
            If objProcessed.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), mstrCSVEmpIds, objPdata._End_Date.Date) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot assign this transaction head. Reason : Payroll is already processed for the current period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot assign this transaction head. Reason : Payroll is already processed for the current period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 14, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Return False
            End If
            objProcessed = Nothing

            If txtAmount.Enabled = True AndAlso txtAmount.Decimal <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "The transaction head you have selected is of Flat Rate Type.") & " " & _
                                   Language.getMessage(mstrModuleName, 5, " Transaction Head, its value will be cosidered as Zero.") & vbCrLf & _
                                   Language.getMessage(mstrModuleName, 6, "Do you want to proceed?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    txtAmount.Focus()
                    Return False
                End If
            End If

            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
            'S.SANDEEP [18-JUL-2018] -- START
            'objTrnFormula = Nothing : objTranHead = Nothing : dsList.Dispose()
            objTrnFormula = Nothing : objTranHead = Nothing : If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [18-JUL-2018] -- END
        End Try
        Return True
    End Function

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objCostCenter As New clscostcenter_master
        Dim dsList As New DataSet
        Try
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, -1, , , , , , , 1)
            With cboTrnHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("TranHead")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            mstrCSVTranIds = String.Join(",", dsList.Tables("TranHead").AsEnumerable().Where(Function(x) x.Field(Of Integer)("tranheadunkid") > 0).Select(Function(x) x.Field(Of Integer)("tranheadunkid").ToString()).ToArray())


            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)

            Dim intFirstPeriodId As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            Dim mdtPeriod As DataTable
            If intFirstPeriodId > 0 Then
                mdtPeriod = New DataView(dsList.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtPeriod = New DataView(dsList.Tables("Period"), "periodunkid = 0", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = mdtPeriod
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            dsList = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("CostCenter")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue(ByVal intEmpId As Integer)
        Dim objTHead As New clsTransactionHead
        Try
            mdtTable = objED._DataSource
            mdtTable.Rows.Clear()
            objTHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            objED._Periodunkid = CInt(cboPeriod.SelectedValue)
            objED._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objED._Employeeunkid(FinancialYear._Object._DatabaseName) = intEmpId
            objED._Trnheadtype_Id = objTHead._Trnheadtype_Id
            objED._Typeof_Id = objTHead._Typeof_id
            objED._Calctype_Id = objTHead._Calctype_Id
            objED._Formula = objTHead._Formula
            objED._FormulaId = objTHead._Formulaid
            objED._Userunkid = User._Object._Userunkid
            If txtAmount.Text.Length > 0 Then
                objED._Amount = txtAmount.Decimal
            Else
                objED._Amount = Nothing
            End If
            If objED._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
                objED._Isdeduct = False
            ElseIf objED._Trnheadtype_Id = enTranHeadType.DeductionForEmployee Or objED._Trnheadtype_Id = enTranHeadType.EmployeesStatutoryDeductions Then
                objED._Isdeduct = True
            Else
                objED._Isdeduct = Nothing
            End If
            If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                objED._Isapproved = True
                objED._Approveruserunkid = User._Object._Userunkid
            Else
                objED._Isapproved = False
            End If
            objED._CostCenterUnkID = CInt(cboCostCenter.SelectedValue)


            Dim drED As DataRow
            mdtTable.Rows.Clear()
            drED = mdtTable.NewRow
            drED.Item("edunkid") = -1
            drED.Item("employeeunkid") = intEmpId
            drED.Item("tranheadunkid") = CInt(cboTrnHead.SelectedValue)
            drED.Item("trnheadname") = ""
            drED.Item("batchtransactionunkid") = -1
            If txtAmount.Text.Length > 0 Then
                drED.Item("amount") = txtAmount.Decimal
            Else
                drED.Item("amount") = 0
            End If
            drED.Item("currencyid") = 0
            drED.Item("vendorid") = 0
            drED.Item("userunkid") = User._Object._Userunkid
            drED.Item("isvoid") = False
            drED.Item("voiduserunkid") = -1
            drED.Item("voiddatetime") = DBNull.Value
            drED.Item("voidreason") = ""
            If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                drED.Item("isapproved") = True
                drED.Item("approveruserunkid") = User._Object._Userunkid
            Else
                drED.Item("isapproved") = False
                drED.Item("approveruserunkid") = -1
            End If
            drED.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            drED.Item("start_date") = eZeeDate.convertDate(mdtPeriodStartDate)
            drED.Item("end_date") = eZeeDate.convertDate(mdtPeriodEndDate)
            drED.Item("costcenterunkid") = CInt(cboCostCenter.SelectedValue)
            drED.Item("AUD") = "A"

            mdtTable.Rows.Add(drED)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
            objTHead = Nothing
        End Try
    End Sub

    Private Sub FillList(ByVal dtStartDate As Date, ByVal dtEndDate As Date)
        Dim dsList As New DataSet
        Try
            'lvED.Items.Clear()
            'dsList = objED.GetList(FinancialYear._Object._DatabaseName, _
            '                       User._Object._Userunkid, _
            '                       FinancialYear._Object._YearUnkid, _
            '                       Company._Object._Companyunkid, _
            '                       dtStartDate, dtEndDate, _
            '                       ConfigParameter._Object._UserAccessModeSetting, True, _
            '                       False, "List", True, _
            '                       mstrCSVEmpIds, "", CInt(cboPeriod.SelectedValue))

            'If dsList.Tables("List").Rows.Count > 0 Then
            '    Dim dt As DataTable = dsList.Tables("List").Select("tranheadunkid IN (" & mstrCSVTranIds & ")").CopyToDataTable()
            '    lvED.BeginUpdate()
            '    For Each row As DataRow In dt.Rows
            '        Dim lvitem As New ListViewItem

            '        lvitem.Text = row("employeename").ToString()    'colhEmployee
            '        lvitem.SubItems.Add(row("trnheadname").ToString()) 'colhTransactionHead
            '        lvitem.SubItems.Add(row("amount").ToString()) 'colhAmount
            '        lvitem.SubItems.Add(row("periodunkid").ToString()) 'objcolhPeriodId
            '        lvitem.SubItems.Add(row("employeeunkid").ToString()) 'objcolhEmployeeId
            '        lvitem.SubItems.Add(row("period_name").ToString()) 'objcolhPeriod

            '        lvitem.Tag = row("edunkid").ToString()

            '        lvED.Items.Add(lvitem)
            '    Next

            '    lvED.GroupingColumn = objcolhPeriod
            '    lvED.DisplayGroups(True)

            '    lvED.EndUpdate()
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillEmployee()
        Try
            dgvEmpData.AutoGenerateColumns = False
            dgcolhemployeecode.DataPropertyName = "employeecode"
            dgcolhEmployee.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolhshortlistunkid.DataPropertyName = "shortlistunkid"
            objdgcolhedunkid.DataPropertyName = "edunkid"
            dgvEmpData.DataSource = mdtEmpInfo

            mstrCSVEmpIds = String.Join(",", mdtEmpInfo.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPostAppraisalHead_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objED = New clsEarningDeduction
        objFinalEmp = New clsAppraisal_Final_Emp
        Try
            lnkProgress.Text = ""
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            SetColor()
            FillCombo()
            FillEmployee()
            Call FillList(mdtPeriodStartDate, mdtPeriodEndDate)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPostAppraisalHead_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPostAppraisalHead_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objED = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEarningDeduction.SetMessages()
            objfrm._Other_ModuleNames = "clsEarningDeduction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        lnkProgress.Text = ""
        Dim intCurr, intTotal As Integer
        Me.Enabled = False
        Try
            intCurr = 0 : intTotal = dgvEmpData.RowCount
            If Validation() = False Then Exit Sub
            Dim eId As List(Of Integer) = mstrCSVEmpIds.Split(","c).[Select](AddressOf Integer.Parse).ToList()
            For Each ival In eId
                Call SetValue(ival)
                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
                'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, ival.ToString(), mdtTable, False, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, Nothing, ival.ToString(), mdtTable, False, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                'Sohail (15 Dec 2018) -- End
                If blnFlag = False And objED._Message <> "" Then
                    eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                    Exit For
                End If
                If objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) > 0 Then
                    Dim dr() As DataRow = mdtEmpInfo.Select("employeeunkid = '" & ival & "'")
                    If dr.Length > 0 Then
                        dr(0)("edunkid") = objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName)
                    End If
                    mdtEmpInfo.AcceptChanges()
                    objFinalEmp.UpdateEarningDeductionId(ival, CInt(dr(0)("shortlistunkid")), CInt(dr(0)("edunkid")), CInt(cboPeriod.SelectedValue), False, User._Object._Userunkid, Nothing)
                End If
                intCurr += 1
                lnkProgress.Text = Language.getMessage(mstrModuleName, 11, "Processed") & " " & intCurr.ToString & " / " & intTotal.ToString
                Application.DoEvents()
            Next
            If blnFlag = True Then
                Call FillList(mdtPeriodStartDate, mdtPeriodEndDate)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Head assigned successfully for the selected period and employee(s)."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Enabled = True

        End Try
    End Sub

    'S.SANDEEP [18-JUL-2018] -- START
    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboTrnHead
                objfrm.DataSource = CType(.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "Code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [18-JUL-2018] -- END

#End Region

#Region " Checkbox Event(s) "

    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboTrnHead_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTrnHead.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboTrnHead.ValueMember
                    .DisplayMember = cboTrnHead.DisplayMember
                    .DataSource = CType(cboTrnHead.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboTrnHead.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboTrnHead.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHead_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboTrnHead_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrnHead.SelectedIndexChanged
        Try
            If CInt(cboTrnHead.SelectedValue) > 0 Then
                Dim objTHead As New clsTransactionHead
                objTHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
                If objTHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    txtAmount.Enabled = True
                End If
                objTHead = Nothing
            Else
                txtAmount.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHead_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            Else
                lvED.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

#End Region

#Region " Datagridview Event(s) "



#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbPostPerformanceHeads.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPostPerformanceHeads.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
            Me.gbPostPerformanceHeads.Text = Language._Object.getCaption(Me.gbPostPerformanceHeads.Name, Me.gbPostPerformanceHeads.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhTransactionHead.Text = Language._Object.getCaption(CStr(Me.colhTransactionHead.Tag), Me.colhTransactionHead.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.dgcolhemployeecode.HeaderText = Language._Object.getCaption(Me.dgcolhemployeecode.Name, Me.dgcolhemployeecode.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lnkProgress.Text = Language._Object.getCaption(Me.lnkProgress.Name, Me.lnkProgress.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Transaction Head. Transaction Head is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "The Formula of this Transaction Head contains")
            Language.setMessage(mstrModuleName, 3, " Tranasaction Head (type of Flate Rate).")
            Language.setMessage(mstrModuleName, 4, "If you do not assign")
            Language.setMessage(mstrModuleName, 5, " Transaction Head, its value will be cosidered as Zero.")
            Language.setMessage(mstrModuleName, 6, "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 7, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form.")
            Language.setMessage(mstrModuleName, 8, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot assign this transaction head. Reason : Payroll is already processed for the current period.")
            Language.setMessage(mstrModuleName, 10, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
            Language.setMessage(mstrModuleName, 11, "Processed")
            Language.setMessage(mstrModuleName, 12, "The transaction head you have selected is of Flat Rate Type.")
			Language.setMessage(mstrModuleName, 13, "Head assigned successfully for the selected period and employee(s).")
            Language.setMessage(mstrModuleName, 14, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class