﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonAssess_View
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonAssess_View))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.objlblCaption1 = New System.Windows.Forms.Label
        Me.objcolon1 = New System.Windows.Forms.Label
        Me.lblAssessGroup = New System.Windows.Forms.Label
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.objlblColon = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.tabcAssessments = New System.Windows.Forms.TabControl
        Me.tabpAssessmentResults = New System.Windows.Forms.TabPage
        Me.gbAssessmentInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlResultInfo = New System.Windows.Forms.Panel
        Me.lvAssessments = New eZee.Common.eZeeListView(Me.components)
        Me.tabpAssessmentRemarks = New System.Windows.Forms.TabPage
        Me.tabcRemarks = New eZee.Common.eZeeTabControlEx
        Me.tabpSelf = New System.Windows.Forms.TabPage
        Me.lvImprovement = New eZee.Common.eZeeListView(Me.components)
        Me.colhI_Improvement = New System.Windows.Forms.ColumnHeader
        Me.colhI_ActivityReq = New System.Windows.Forms.ColumnHeader
        Me.colhI_Support = New System.Windows.Forms.ColumnHeader
        Me.colhI_Course = New System.Windows.Forms.ColumnHeader
        Me.colhI_Timeframe = New System.Windows.Forms.ColumnHeader
        Me.colhI_OtherTraning = New System.Windows.Forms.ColumnHeader
        Me.tabpAppriasers = New System.Windows.Forms.TabPage
        Me.lvPersonalDevelop = New eZee.Common.eZeeListView(Me.components)
        Me.colhP_Development = New System.Windows.Forms.ColumnHeader
        Me.colhP_ActivityReq = New System.Windows.Forms.ColumnHeader
        Me.colhP_Support = New System.Windows.Forms.ColumnHeader
        Me.colhP_Course = New System.Windows.Forms.ColumnHeader
        Me.colhP_Timeframe = New System.Windows.Forms.ColumnHeader
        Me.colhP_OtherTraning = New System.Windows.Forms.ColumnHeader
        Me.objcolhAppraiser = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.tabcAssessments.SuspendLayout()
        Me.tabpAssessmentResults.SuspendLayout()
        Me.gbAssessmentInfo.SuspendLayout()
        Me.pnlResultInfo.SuspendLayout()
        Me.tabpAssessmentRemarks.SuspendLayout()
        Me.tabcRemarks.SuspendLayout()
        Me.tabpSelf.SuspendLayout()
        Me.tabpAppriasers.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.SplitContainer1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(858, 483)
        Me.pnlMain.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.objlblCaption1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.objcolon1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblAssessGroup)
        Me.SplitContainer1.Panel1.Controls.Add(Me.objlblCaption)
        Me.SplitContainer1.Panel1.Controls.Add(Me.objlblColon)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblEmployee)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.tabcAssessments)
        Me.SplitContainer1.Size = New System.Drawing.Size(858, 428)
        Me.SplitContainer1.SplitterDistance = 34
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 15
        '
        'objlblCaption1
        '
        Me.objlblCaption1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objlblCaption1.Location = New System.Drawing.Point(543, 8)
        Me.objlblCaption1.Name = "objlblCaption1"
        Me.objlblCaption1.Size = New System.Drawing.Size(303, 15)
        Me.objlblCaption1.TabIndex = 5
        Me.objlblCaption1.Text = "##Value"
        Me.objlblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objcolon1
        '
        Me.objcolon1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objcolon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objcolon1.Location = New System.Drawing.Point(527, 9)
        Me.objcolon1.Name = "objcolon1"
        Me.objcolon1.Size = New System.Drawing.Size(10, 13)
        Me.objcolon1.TabIndex = 4
        Me.objcolon1.Text = ":"
        Me.objcolon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessGroup
        '
        Me.lblAssessGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblAssessGroup.Location = New System.Drawing.Point(412, 8)
        Me.lblAssessGroup.Name = "lblAssessGroup"
        Me.lblAssessGroup.Size = New System.Drawing.Size(109, 15)
        Me.lblAssessGroup.TabIndex = 3
        Me.lblAssessGroup.Text = "Assessment Group"
        Me.lblAssessGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCaption
        '
        Me.objlblCaption.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objlblCaption.Location = New System.Drawing.Point(116, 8)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(289, 15)
        Me.objlblCaption.TabIndex = 2
        Me.objlblCaption.Text = "##Value"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblColon
        '
        Me.objlblColon.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objlblColon.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColon.Location = New System.Drawing.Point(100, 9)
        Me.objlblColon.Name = "objlblColon"
        Me.objlblColon.Size = New System.Drawing.Size(10, 13)
        Me.objlblColon.TabIndex = 1
        Me.objlblColon.Text = ":"
        Me.objlblColon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblEmployee.Location = New System.Drawing.Point(12, 8)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(82, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcAssessments
        '
        Me.tabcAssessments.Controls.Add(Me.tabpAssessmentResults)
        Me.tabcAssessments.Controls.Add(Me.tabpAssessmentRemarks)
        Me.tabcAssessments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcAssessments.Location = New System.Drawing.Point(0, 0)
        Me.tabcAssessments.Name = "tabcAssessments"
        Me.tabcAssessments.SelectedIndex = 0
        Me.tabcAssessments.Size = New System.Drawing.Size(854, 388)
        Me.tabcAssessments.TabIndex = 15
        '
        'tabpAssessmentResults
        '
        Me.tabpAssessmentResults.Controls.Add(Me.gbAssessmentInfo)
        Me.tabpAssessmentResults.Location = New System.Drawing.Point(4, 22)
        Me.tabpAssessmentResults.Name = "tabpAssessmentResults"
        Me.tabpAssessmentResults.Size = New System.Drawing.Size(846, 362)
        Me.tabpAssessmentResults.TabIndex = 0
        Me.tabpAssessmentResults.Text = "Assessment Results"
        Me.tabpAssessmentResults.UseVisualStyleBackColor = True
        '
        'gbAssessmentInfo
        '
        Me.gbAssessmentInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAssessmentInfo.Checked = False
        Me.gbAssessmentInfo.CollapseAllExceptThis = False
        Me.gbAssessmentInfo.CollapsedHoverImage = Nothing
        Me.gbAssessmentInfo.CollapsedNormalImage = Nothing
        Me.gbAssessmentInfo.CollapsedPressedImage = Nothing
        Me.gbAssessmentInfo.CollapseOnLoad = False
        Me.gbAssessmentInfo.Controls.Add(Me.pnlResultInfo)
        Me.gbAssessmentInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssessmentInfo.ExpandedHoverImage = Nothing
        Me.gbAssessmentInfo.ExpandedNormalImage = Nothing
        Me.gbAssessmentInfo.ExpandedPressedImage = Nothing
        Me.gbAssessmentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssessmentInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssessmentInfo.HeaderHeight = 25
        Me.gbAssessmentInfo.HeaderMessage = ""
        Me.gbAssessmentInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAssessmentInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssessmentInfo.HeightOnCollapse = 0
        Me.gbAssessmentInfo.LeftTextSpace = 0
        Me.gbAssessmentInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbAssessmentInfo.Name = "gbAssessmentInfo"
        Me.gbAssessmentInfo.OpenHeight = 300
        Me.gbAssessmentInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssessmentInfo.ShowBorder = True
        Me.gbAssessmentInfo.ShowCheckBox = False
        Me.gbAssessmentInfo.ShowCollapseButton = False
        Me.gbAssessmentInfo.ShowDefaultBorderColor = True
        Me.gbAssessmentInfo.ShowDownButton = False
        Me.gbAssessmentInfo.ShowHeader = True
        Me.gbAssessmentInfo.Size = New System.Drawing.Size(846, 362)
        Me.gbAssessmentInfo.TabIndex = 16
        Me.gbAssessmentInfo.Temp = 0
        Me.gbAssessmentInfo.Text = "Assessment Results"
        Me.gbAssessmentInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlResultInfo
        '
        Me.pnlResultInfo.Controls.Add(Me.lvAssessments)
        Me.pnlResultInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlResultInfo.Location = New System.Drawing.Point(2, 26)
        Me.pnlResultInfo.Name = "pnlResultInfo"
        Me.pnlResultInfo.Size = New System.Drawing.Size(842, 334)
        Me.pnlResultInfo.TabIndex = 15
        '
        'lvAssessments
        '
        Me.lvAssessments.BackColorOnChecked = False
        Me.lvAssessments.ColumnHeaders = Nothing
        Me.lvAssessments.CompulsoryColumns = ""
        Me.lvAssessments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAssessments.FullRowSelect = True
        Me.lvAssessments.GridLines = True
        Me.lvAssessments.GroupingColumn = Nothing
        Me.lvAssessments.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAssessments.HideSelection = False
        Me.lvAssessments.Location = New System.Drawing.Point(0, 0)
        Me.lvAssessments.MinColumnWidth = 50
        Me.lvAssessments.MultiSelect = False
        Me.lvAssessments.Name = "lvAssessments"
        Me.lvAssessments.OptionalColumns = ""
        Me.lvAssessments.ShowMoreItem = False
        Me.lvAssessments.ShowSaveItem = False
        Me.lvAssessments.ShowSelectAll = True
        Me.lvAssessments.ShowSizeAllColumnsToFit = True
        Me.lvAssessments.Size = New System.Drawing.Size(842, 334)
        Me.lvAssessments.Sortable = True
        Me.lvAssessments.TabIndex = 14
        Me.lvAssessments.UseCompatibleStateImageBehavior = False
        Me.lvAssessments.View = System.Windows.Forms.View.Details
        '
        'tabpAssessmentRemarks
        '
        Me.tabpAssessmentRemarks.Controls.Add(Me.tabcRemarks)
        Me.tabpAssessmentRemarks.Location = New System.Drawing.Point(4, 22)
        Me.tabpAssessmentRemarks.Name = "tabpAssessmentRemarks"
        Me.tabpAssessmentRemarks.Size = New System.Drawing.Size(846, 362)
        Me.tabpAssessmentRemarks.TabIndex = 1
        Me.tabpAssessmentRemarks.Text = "Assessment Remarks"
        Me.tabpAssessmentRemarks.UseVisualStyleBackColor = True
        '
        'tabcRemarks
        '
        Me.tabcRemarks.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.tabcRemarks.Controls.Add(Me.tabpSelf)
        Me.tabcRemarks.Controls.Add(Me.tabpAppriasers)
        Me.tabcRemarks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcRemarks.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.tabcRemarks.HeaderAlignment = System.Drawing.ContentAlignment.MiddleCenter
        Me.tabcRemarks.HeaderBackColor = System.Drawing.Color.DarkGray
        Me.tabcRemarks.HeaderBorderColor = System.Drawing.Color.WhiteSmoke
        Me.tabcRemarks.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcRemarks.HeaderPadding = New System.Windows.Forms.Padding(0)
        Me.tabcRemarks.HeaderSelectedBackColor = System.Drawing.Color.LightGray
        Me.tabcRemarks.ItemSize = New System.Drawing.Size(32, 100)
        Me.tabcRemarks.Location = New System.Drawing.Point(0, 0)
        Me.tabcRemarks.Multiline = True
        Me.tabcRemarks.Name = "tabcRemarks"
        Me.tabcRemarks.SelectedIndex = 0
        Me.tabcRemarks.Size = New System.Drawing.Size(846, 362)
        Me.tabcRemarks.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tabcRemarks.TabIndex = 18
        '
        'tabpSelf
        '
        Me.tabpSelf.Controls.Add(Me.lvImprovement)
        Me.tabpSelf.Location = New System.Drawing.Point(104, 4)
        Me.tabpSelf.Name = "tabpSelf"
        Me.tabpSelf.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpSelf.Size = New System.Drawing.Size(738, 354)
        Me.tabpSelf.TabIndex = 0
        Me.tabpSelf.Text = "Self"
        Me.tabpSelf.UseVisualStyleBackColor = True
        '
        'lvImprovement
        '
        Me.lvImprovement.BackColorOnChecked = False
        Me.lvImprovement.ColumnHeaders = Nothing
        Me.lvImprovement.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhI_Improvement, Me.colhI_ActivityReq, Me.colhI_Support, Me.colhI_Course, Me.colhI_Timeframe, Me.colhI_OtherTraning})
        Me.lvImprovement.CompulsoryColumns = ""
        Me.lvImprovement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvImprovement.FullRowSelect = True
        Me.lvImprovement.GridLines = True
        Me.lvImprovement.GroupingColumn = Nothing
        Me.lvImprovement.HideSelection = False
        Me.lvImprovement.Location = New System.Drawing.Point(3, 3)
        Me.lvImprovement.MinColumnWidth = 50
        Me.lvImprovement.MultiSelect = False
        Me.lvImprovement.Name = "lvImprovement"
        Me.lvImprovement.OptionalColumns = ""
        Me.lvImprovement.ShowMoreItem = False
        Me.lvImprovement.ShowSaveItem = False
        Me.lvImprovement.ShowSelectAll = True
        Me.lvImprovement.ShowSizeAllColumnsToFit = True
        Me.lvImprovement.Size = New System.Drawing.Size(732, 348)
        Me.lvImprovement.Sortable = True
        Me.lvImprovement.TabIndex = 339
        Me.lvImprovement.UseCompatibleStateImageBehavior = False
        Me.lvImprovement.View = System.Windows.Forms.View.Details
        '
        'colhI_Improvement
        '
        Me.colhI_Improvement.Tag = "colhI_Improvement"
        Me.colhI_Improvement.Text = "Major Area for Improvement"
        Me.colhI_Improvement.Width = 160
        '
        'colhI_ActivityReq
        '
        Me.colhI_ActivityReq.Tag = "colhI_ActivityReq"
        Me.colhI_ActivityReq.Text = "Activity/Action Required to Meet Need"
        Me.colhI_ActivityReq.Width = 200
        '
        'colhI_Support
        '
        Me.colhI_Support.Tag = "colhI_Support"
        Me.colhI_Support.Text = "Support required from Assessor"
        Me.colhI_Support.Width = 180
        '
        'colhI_Course
        '
        Me.colhI_Course.Tag = "colhI_Course"
        Me.colhI_Course.Text = "Training or Learning Objective"
        Me.colhI_Course.Width = 160
        '
        'colhI_Timeframe
        '
        Me.colhI_Timeframe.Tag = "colhI_Timeframe"
        Me.colhI_Timeframe.Text = "Timeframe"
        Me.colhI_Timeframe.Width = 90
        '
        'colhI_OtherTraning
        '
        Me.colhI_OtherTraning.Tag = "colhI_OtherTraning"
        Me.colhI_OtherTraning.Text = "Other Training"
        Me.colhI_OtherTraning.Width = 160
        '
        'tabpAppriasers
        '
        Me.tabpAppriasers.Controls.Add(Me.lvPersonalDevelop)
        Me.tabpAppriasers.Location = New System.Drawing.Point(104, 4)
        Me.tabpAppriasers.Name = "tabpAppriasers"
        Me.tabpAppriasers.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpAppriasers.Size = New System.Drawing.Size(738, 354)
        Me.tabpAppriasers.TabIndex = 1
        Me.tabpAppriasers.Text = "Assessor's"
        Me.tabpAppriasers.UseVisualStyleBackColor = True
        '
        'lvPersonalDevelop
        '
        Me.lvPersonalDevelop.BackColorOnChecked = False
        Me.lvPersonalDevelop.ColumnHeaders = Nothing
        Me.lvPersonalDevelop.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhP_Development, Me.colhP_ActivityReq, Me.colhP_Support, Me.colhP_Course, Me.colhP_Timeframe, Me.colhP_OtherTraning, Me.objcolhAppraiser})
        Me.lvPersonalDevelop.CompulsoryColumns = ""
        Me.lvPersonalDevelop.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPersonalDevelop.FullRowSelect = True
        Me.lvPersonalDevelop.GridLines = True
        Me.lvPersonalDevelop.GroupingColumn = Nothing
        Me.lvPersonalDevelop.HideSelection = False
        Me.lvPersonalDevelop.Location = New System.Drawing.Point(3, 3)
        Me.lvPersonalDevelop.MinColumnWidth = 50
        Me.lvPersonalDevelop.MultiSelect = False
        Me.lvPersonalDevelop.Name = "lvPersonalDevelop"
        Me.lvPersonalDevelop.OptionalColumns = ""
        Me.lvPersonalDevelop.ShowMoreItem = False
        Me.lvPersonalDevelop.ShowSaveItem = False
        Me.lvPersonalDevelop.ShowSelectAll = True
        Me.lvPersonalDevelop.ShowSizeAllColumnsToFit = True
        Me.lvPersonalDevelop.Size = New System.Drawing.Size(732, 348)
        Me.lvPersonalDevelop.Sortable = True
        Me.lvPersonalDevelop.TabIndex = 340
        Me.lvPersonalDevelop.UseCompatibleStateImageBehavior = False
        Me.lvPersonalDevelop.View = System.Windows.Forms.View.Details
        '
        'colhP_Development
        '
        Me.colhP_Development.Tag = "colhP_Development"
        Me.colhP_Development.Text = "Major Area for Development"
        Me.colhP_Development.Width = 160
        '
        'colhP_ActivityReq
        '
        Me.colhP_ActivityReq.Tag = "colhP_ActivityReq"
        Me.colhP_ActivityReq.Text = "Activity/Action Required to Meet Need"
        Me.colhP_ActivityReq.Width = 200
        '
        'colhP_Support
        '
        Me.colhP_Support.Tag = "colhP_Support"
        Me.colhP_Support.Text = "Support required from Assessor"
        Me.colhP_Support.Width = 180
        '
        'colhP_Course
        '
        Me.colhP_Course.Tag = "colhP_Course"
        Me.colhP_Course.Text = "Training or Learning Objective"
        Me.colhP_Course.Width = 160
        '
        'colhP_Timeframe
        '
        Me.colhP_Timeframe.Tag = "colhP_Timeframe"
        Me.colhP_Timeframe.Text = "Timeframe"
        Me.colhP_Timeframe.Width = 90
        '
        'colhP_OtherTraning
        '
        Me.colhP_OtherTraning.Tag = "colhP_OtherTraning"
        Me.colhP_OtherTraning.Text = "Other Training"
        Me.colhP_OtherTraning.Width = 160
        '
        'objcolhAppraiser
        '
        Me.objcolhAppraiser.Tag = "objcolhAppraiser"
        Me.objcolhAppraiser.Text = ""
        Me.objcolhAppraiser.Width = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 428)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(858, 55)
        Me.objFooter.TabIndex = 13
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(749, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmCommonAssess_View
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(858, 483)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonAssess_View"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessments"
        Me.pnlMain.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.tabcAssessments.ResumeLayout(False)
        Me.tabpAssessmentResults.ResumeLayout(False)
        Me.gbAssessmentInfo.ResumeLayout(False)
        Me.pnlResultInfo.ResumeLayout(False)
        Me.tabpAssessmentRemarks.ResumeLayout(False)
        Me.tabcRemarks.ResumeLayout(False)
        Me.tabpSelf.ResumeLayout(False)
        Me.tabpAppriasers.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvAssessments As eZee.Common.eZeeListView
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objlblCaption1 As System.Windows.Forms.Label
    Friend WithEvents objcolon1 As System.Windows.Forms.Label
    Friend WithEvents lblAssessGroup As System.Windows.Forms.Label
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objlblColon As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents tabcAssessments As System.Windows.Forms.TabControl
    Friend WithEvents tabpAssessmentResults As System.Windows.Forms.TabPage
    Friend WithEvents tabpAssessmentRemarks As System.Windows.Forms.TabPage
    Friend WithEvents gbAssessmentInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlResultInfo As System.Windows.Forms.Panel
    Friend WithEvents lvImprovement As eZee.Common.eZeeListView
    Friend WithEvents colhI_Improvement As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_ActivityReq As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Support As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Course As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Timeframe As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_OtherTraning As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvPersonalDevelop As eZee.Common.eZeeListView
    Friend WithEvents colhP_Development As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_ActivityReq As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Support As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Course As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Timeframe As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_OtherTraning As System.Windows.Forms.ColumnHeader
    Friend WithEvents tabcRemarks As eZee.Common.eZeeTabControlEx
    Friend WithEvents tabpSelf As System.Windows.Forms.TabPage
    Friend WithEvents tabpAppriasers As System.Windows.Forms.TabPage
    Friend WithEvents objcolhAppraiser As System.Windows.Forms.ColumnHeader
End Class
