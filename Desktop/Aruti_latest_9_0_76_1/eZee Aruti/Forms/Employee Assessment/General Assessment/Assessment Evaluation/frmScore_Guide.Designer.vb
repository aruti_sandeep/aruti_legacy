﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScore_Guide
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScore_Guide))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.rtbScoreGuide = New System.Windows.Forms.RichTextBox
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.Controls.Add(Me.rtbScoreGuide)
        Me.pnlMain.Location = New System.Drawing.Point(1, 1)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(423, 196)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOk)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 198)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(425, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(316, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'rtbScoreGuide
        '
        Me.rtbScoreGuide.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rtbScoreGuide.DetectUrls = False
        Me.rtbScoreGuide.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbScoreGuide.Location = New System.Drawing.Point(0, 0)
        Me.rtbScoreGuide.Name = "rtbScoreGuide"
        Me.rtbScoreGuide.ReadOnly = True
        Me.rtbScoreGuide.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.rtbScoreGuide.Size = New System.Drawing.Size(423, 196)
        Me.rtbScoreGuide.TabIndex = 0
        Me.rtbScoreGuide.Text = ""
        '
        'frmScore_Guide
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(425, 253)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmScore_Guide"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Score Guide"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents rtbScoreGuide As System.Windows.Forms.RichTextBox
End Class
