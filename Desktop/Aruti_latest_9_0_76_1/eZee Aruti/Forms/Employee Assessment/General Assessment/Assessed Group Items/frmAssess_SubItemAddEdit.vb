﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssess_SubItemAddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssess_SubItemAddEdit"
    Private mblnCancel As Boolean = True
    Private objSubItemmaster As clsassess_subitem_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAssess_SubItemunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintAssess_SubItemunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintAssess_SubItemunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmAssess_SubItemAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSubItemmaster = New clsassess_subitem_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call SetColor()

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objSubItemmaster._Assess_Subitemunkid = mintAssess_SubItemunkid


              'Sohail (28 Jan 2012) -start
                Dim objAssessItem As New clsassess_item_master
                objAssessItem._Assessitemunkid = objSubItemmaster._Assessitemunkid
                cboGroup.SelectedValue = objAssessItem._Assessgroupunkid
                cboGroup.Enabled = False : objbtnAddGroup.Enabled = False
                cboYear.Enabled = False : cboPeriod.Enabled = False 'S.SANDEEP [ 28 DEC 2012 ] -- START
                objAssessItem = Nothing
               'Sohail (28 Jan 2012) End
            End If

            Call GetValue()

            txtItemCode.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssess_SubItemAddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAssess_SubItemAddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssess_SubItemAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssess_SubItemAddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssess_SubItemAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssess_SubItemAddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objSubItemmaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_subitem_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_subitem_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsVaild() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objSubItemmaster.Update()
            Else
                blnFlag = objSubItemmaster.Insert()
            End If

            If blnFlag = False And objSubItemmaster._Message <> "" Then
                eZeeMsgBox.Show(objSubItemmaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objSubItemmaster = Nothing
                    objSubItemmaster = New clsassess_subitem_master
                    Call GetValue()
                    cboAssessmentItem.Select()
                Else
                    mintAssess_SubItemunkid = objSubItemmaster._Assess_Subitemunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If

            Call objFrmLangPopup.displayDialog(txtAssessmentName.Text, objSubItemmaster._Name1, objSubItemmaster._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnAddAssessItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddAssessItem.Click
        Dim frm As New frmAssessmentItems_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim intRefId As Integer = -1

            frm.displayDialog(intRefId, enAction.ADD_ONE)

            If intRefId > 0 Then
                Dim dsCombo As New DataSet
                Dim objAssessItem As New clsassess_item_master
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombo = objAssessItem.getListForCombo("List", True)
                dsCombo = objAssessItem.getListForCombo("List", True, CInt(cboGroup.SelectedValue), , CInt(cboPeriod.SelectedValue))
                'S.SANDEEP [ 28 DEC 2012 ] -- END
                With cboAssessmentItem
                    .ValueMember = "assessitemunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsCombo.Dispose() : objAssessItem = Nothing
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddAssessItem_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchItems.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboAssessmentItem.ValueMember
                .DisplayMember = cboAssessmentItem.DisplayMember
                .DataSource = CType(cboAssessmentItem.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboAssessmentItem.SelectedValue = frm.SelectedValue
                cboAssessmentItem.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchItems_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboAssessmentItem.BackColor = GUI.ColorComp
            cboAssessmentItem.BackColor = GUI.ColorComp
            txtItemCode.BackColor = GUI.ColorComp
            txtAssessmentName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboGroup.BackColor = GUI.ColorComp 'Sohail (28 Jan 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtItemCode.Text = objSubItemmaster._Code
            txtAssessmentName.Text = objSubItemmaster._Name
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objAItem As New clsassess_item_master
            objAItem._Assessitemunkid = objSubItemmaster._Assessitemunkid
            cboYear.SelectedValue = objAItem._Yearunkid
            cboPeriod.SelectedValue = objAItem._Periodunkid
            'S.SANDEEP [ 28 DEC 2012 ] -- END
            cboAssessmentItem.SelectedValue = objSubItemmaster._Assessitemunkid
            txtDescription.Text = objSubItemmaster._Description
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objSubItemmaster._Code = txtItemCode.Text.Trim
            objSubItemmaster._Name = txtAssessmentName.Text.Trim
            objSubItemmaster._Assessitemunkid = CInt(cboAssessmentItem.SelectedValue)
            objSubItemmaster._Description = txtDescription.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        'Sohail (28 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objAssessItem As New clsassess_item_master
        Dim objAssessGroup As New clsassess_group_master
        'Sohail (28 Jan 2012) -- End
        Try
            'Sohail (28 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objAssessItem.getListForCombo("List", True)
            'With cboAssessmentItem
            '    .ValueMember = "assessitemunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("List")
            '    .SelectedValue = 0
            'End With
            dsCombo = objAssessGroup.getListForCombo("Group", True)
            cboGroup.ValueMember = "assessgroupunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsCombo.Tables(0)
            'Sohail (28 Jan 2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objMData As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objMData.getComboListPAYYEAR("List", True, , , , True)
            dsCombo = objMData.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "List", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
            End With
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objAssessGroup = Nothing ': objAssessItem = Nothing  'Sohail (28 Jan 2012)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'objbtnAddAssessItem.Enabled = User._Object.Privilege._AddAssessmentItem
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Function IsVaild() As Boolean
        Try
            If CInt(cboAssessmentItem.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Assessment Item is compulsory information.Please Select Assessment Item."), enMsgBoxStyle.Information)
                cboAssessmentItem.Focus()
                Return False
            ElseIf Trim(txtItemCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sub Item Code cannot be blank. Sub Item Code is required information."), enMsgBoxStyle.Information)
                txtItemCode.Focus()
                Return False
            ElseIf Trim(txtAssessmentName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sub Item Name cannot be blank. Sub Item Name is required information."), enMsgBoxStyle.Information)
                txtAssessmentName.Focus()
                Return False
            End If

            If CInt(cboYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Year is mandatory information. Please select Year to continue."), enMsgBoxStyle.Information)
                cboYear.Focus()
                Return False
            End If

            If CInt(cboYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsVaild", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

    'Sohail (28 Jan 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran
            Dim dsList As New DataSet

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboGroup.SelectedIndexChanged
        Dim objAssessItem As New clsassess_item_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objAssessItem.getListForCombo("AssessItem", True, CInt(cboGroup.SelectedValue), , CInt(cboPeriod.SelectedValue))
            With cboAssessmentItem
                .ValueMember = "assessitemunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("AssessItem")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Other Control's Events "
    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim objfrmGroup_AddEdit As New frmGroup_AddEdit
            If User._Object._Isrighttoleft = True Then
                objfrmGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmGroup_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmGroup_AddEdit)
            End If
            If objfrmGroup_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillCombo()
                cboGroup.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (28 Jan 2012) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAssessSubItems.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAssessSubItems.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAssessSubItems.Text = Language._Object.getCaption(Me.gbAssessSubItems.Name, Me.gbAssessSubItems.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.tabpName.Text = Language._Object.getCaption(Me.tabpName.Name, Me.tabpName.Text)
			Me.tabpDescription.Text = Language._Object.getCaption(Me.tabpDescription.Name, Me.tabpDescription.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Assessment Item is compulsory information.Please Select Assessment Item.")
			Language.setMessage(mstrModuleName, 2, "Sub Item Code cannot be blank. Sub Item Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Sub Item Name cannot be blank. Sub Item Name is required information.")
			Language.setMessage(mstrModuleName, 4, "Year is mandatory information. Please select Year to continue.")
			Language.setMessage(mstrModuleName, 5, "Period is mandatory information. Please select Period to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class