﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportAssessorAssessment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportAssessorAssessment))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.WizImportAssessorAssessment = New eZee.Common.eZeeWizard
        Me.wizPageFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.wizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblSign12 = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboAssessorCode = New System.Windows.Forms.ComboBox
        Me.LblAssessorCode = New System.Windows.Forms.Label
        Me.objlblSign13 = New System.Windows.Forms.Label
        Me.objlblSign11 = New System.Windows.Forms.Label
        Me.LblTrainingLearningObjective = New System.Windows.Forms.Label
        Me.cboTrainingLearningObjective = New System.Windows.Forms.ComboBox
        Me.objlblSign10 = New System.Windows.Forms.Label
        Me.objlblSign9 = New System.Windows.Forms.Label
        Me.objlblSign8 = New System.Windows.Forms.Label
        Me.objlblSign7 = New System.Windows.Forms.Label
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.objlblSign5 = New System.Windows.Forms.Label
        Me.LblSupportRequired = New System.Windows.Forms.Label
        Me.cboSupportRequired = New System.Windows.Forms.ComboBox
        Me.LblActivity = New System.Windows.Forms.Label
        Me.cboActivity = New System.Windows.Forms.ComboBox
        Me.LblImprovement = New System.Windows.Forms.Label
        Me.lblTimeframe = New System.Windows.Forms.Label
        Me.cboTimeFrame = New System.Windows.Forms.ComboBox
        Me.lblOtherTraining = New System.Windows.Forms.Label
        Me.cboOtherTraining = New System.Windows.Forms.ComboBox
        Me.cboImprovement = New System.Windows.Forms.ComboBox
        Me.LblResult = New System.Windows.Forms.Label
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.lblIsDefault = New System.Windows.Forms.Label
        Me.cboAssessmentSubItemCode = New System.Windows.Forms.ComboBox
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.lblAssessmentItemCode = New System.Windows.Forms.Label
        Me.cboAssessmentItemCode = New System.Windows.Forms.ComboBox
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.lblAssessmentGrpCode = New System.Windows.Forms.Label
        Me.cboAssessmentGrpCode = New System.Windows.Forms.ComboBox
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.wizPageData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhAssessorCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGroupCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhItemCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSubItemCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhResult = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.WizImportAssessorAssessment.SuspendLayout()
        Me.wizPageFile.SuspendLayout()
        Me.wizPageMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.wizPageData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(763, 458)
        Me.pnlMainInfo.TabIndex = 0
        '
        'WizImportAssessorAssessment
        '
        Me.WizImportAssessorAssessment.Controls.Add(Me.wizPageMapping)
        Me.WizImportAssessorAssessment.Controls.Add(Me.wizPageFile)
        Me.WizImportAssessorAssessment.Controls.Add(Me.wizPageData)
        Me.WizImportAssessorAssessment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizImportAssessorAssessment.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.WizImportAssessorAssessment.Location = New System.Drawing.Point(0, 0)
        Me.WizImportAssessorAssessment.Name = "WizImportAssessorAssessment"
        Me.WizImportAssessorAssessment.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizPageFile, Me.wizPageMapping, Me.wizPageData})
        Me.WizImportAssessorAssessment.SaveEnabled = True
        Me.WizImportAssessorAssessment.SaveText = "Save && Finish"
        Me.WizImportAssessorAssessment.SaveVisible = False
        Me.WizImportAssessorAssessment.SetSaveIndexBeforeFinishIndex = False
        Me.WizImportAssessorAssessment.Size = New System.Drawing.Size(763, 458)
        Me.WizImportAssessorAssessment.TabIndex = 1
        Me.WizImportAssessorAssessment.WelcomeImage = Nothing
        '
        'wizPageFile
        '
        Me.wizPageFile.Controls.Add(Me.btnOpenFile)
        Me.wizPageFile.Controls.Add(Me.txtFilePath)
        Me.wizPageFile.Controls.Add(Me.lblSelectfile)
        Me.wizPageFile.Controls.Add(Me.lblTitle)
        Me.wizPageFile.Location = New System.Drawing.Point(0, 0)
        Me.wizPageFile.Name = "wizPageFile"
        Me.wizPageFile.Size = New System.Drawing.Size(763, 410)
        Me.wizPageFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageFile.TabIndex = 7
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(723, 196)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(28, 21)
        Me.btnOpenFile.TabIndex = 21
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(180, 196)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(537, 21)
        Me.txtFilePath.TabIndex = 20
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 174)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(143, 17)
        Me.lblSelectfile.TabIndex = 19
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(172, 21)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(552, 23)
        Me.lblTitle.TabIndex = 18
        Me.lblTitle.Text = "Assessor Assessment Import Wizard"
        '
        'wizPageMapping
        '
        Me.wizPageMapping.Controls.Add(Me.gbFiledMapping)
        Me.wizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.wizPageMapping.Name = "wizPageMapping"
        Me.wizPageMapping.Size = New System.Drawing.Size(763, 410)
        Me.wizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageMapping.TabIndex = 8
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.objlblSign12)
        Me.gbFiledMapping.Controls.Add(Me.lblPeriod)
        Me.gbFiledMapping.Controls.Add(Me.cboPeriod)
        Me.gbFiledMapping.Controls.Add(Me.cboAssessorCode)
        Me.gbFiledMapping.Controls.Add(Me.LblAssessorCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign13)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign11)
        Me.gbFiledMapping.Controls.Add(Me.LblTrainingLearningObjective)
        Me.gbFiledMapping.Controls.Add(Me.cboTrainingLearningObjective)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign10)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign9)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign8)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign7)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign6)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign5)
        Me.gbFiledMapping.Controls.Add(Me.LblSupportRequired)
        Me.gbFiledMapping.Controls.Add(Me.cboSupportRequired)
        Me.gbFiledMapping.Controls.Add(Me.LblActivity)
        Me.gbFiledMapping.Controls.Add(Me.cboActivity)
        Me.gbFiledMapping.Controls.Add(Me.LblImprovement)
        Me.gbFiledMapping.Controls.Add(Me.lblTimeframe)
        Me.gbFiledMapping.Controls.Add(Me.cboTimeFrame)
        Me.gbFiledMapping.Controls.Add(Me.lblOtherTraining)
        Me.gbFiledMapping.Controls.Add(Me.cboOtherTraining)
        Me.gbFiledMapping.Controls.Add(Me.cboImprovement)
        Me.gbFiledMapping.Controls.Add(Me.LblResult)
        Me.gbFiledMapping.Controls.Add(Me.cboResult)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign4)
        Me.gbFiledMapping.Controls.Add(Me.lblIsDefault)
        Me.gbFiledMapping.Controls.Add(Me.cboAssessmentSubItemCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign3)
        Me.gbFiledMapping.Controls.Add(Me.lblAssessmentItemCode)
        Me.gbFiledMapping.Controls.Add(Me.cboAssessmentItemCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign2)
        Me.gbFiledMapping.Controls.Add(Me.lblAssessmentGrpCode)
        Me.gbFiledMapping.Controls.Add(Me.cboAssessmentGrpCode)
        Me.gbFiledMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(598, 410)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign12
        '
        Me.objlblSign12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign12.ForeColor = System.Drawing.Color.Red
        Me.objlblSign12.Location = New System.Drawing.Point(84, 45)
        Me.objlblSign12.Name = "objlblSign12"
        Me.objlblSign12.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign12.TabIndex = 120
        Me.objlblSign12.Text = "*"
        Me.objlblSign12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(102, 45)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(166, 17)
        Me.lblPeriod.TabIndex = 118
        Me.lblPeriod.Text = "Assessment Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(274, 43)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(203, 21)
        Me.cboPeriod.TabIndex = 119
        '
        'cboAssessorCode
        '
        Me.cboAssessorCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessorCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessorCode.FormattingEnabled = True
        Me.cboAssessorCode.Location = New System.Drawing.Point(274, 70)
        Me.cboAssessorCode.Name = "cboAssessorCode"
        Me.cboAssessorCode.Size = New System.Drawing.Size(203, 21)
        Me.cboAssessorCode.TabIndex = 116
        '
        'LblAssessorCode
        '
        Me.LblAssessorCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAssessorCode.Location = New System.Drawing.Point(102, 72)
        Me.LblAssessorCode.Name = "LblAssessorCode"
        Me.LblAssessorCode.Size = New System.Drawing.Size(164, 17)
        Me.LblAssessorCode.TabIndex = 115
        Me.LblAssessorCode.Text = "Assessor Code"
        Me.LblAssessorCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign13
        '
        Me.objlblSign13.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign13.ForeColor = System.Drawing.Color.Red
        Me.objlblSign13.Location = New System.Drawing.Point(84, 72)
        Me.objlblSign13.Name = "objlblSign13"
        Me.objlblSign13.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign13.TabIndex = 114
        Me.objlblSign13.Text = "*"
        Me.objlblSign13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign11
        '
        Me.objlblSign11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign11.ForeColor = System.Drawing.Color.Red
        Me.objlblSign11.Location = New System.Drawing.Point(84, 369)
        Me.objlblSign11.Name = "objlblSign11"
        Me.objlblSign11.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign11.TabIndex = 109
        Me.objlblSign11.Text = "*"
        Me.objlblSign11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign11.Visible = False
        '
        'LblTrainingLearningObjective
        '
        Me.LblTrainingLearningObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTrainingLearningObjective.Location = New System.Drawing.Point(102, 369)
        Me.LblTrainingLearningObjective.Name = "LblTrainingLearningObjective"
        Me.LblTrainingLearningObjective.Size = New System.Drawing.Size(164, 17)
        Me.LblTrainingLearningObjective.TabIndex = 107
        Me.LblTrainingLearningObjective.Text = "Training Or Learning Objective "
        Me.LblTrainingLearningObjective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainingLearningObjective
        '
        Me.cboTrainingLearningObjective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingLearningObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingLearningObjective.FormattingEnabled = True
        Me.cboTrainingLearningObjective.Location = New System.Drawing.Point(274, 367)
        Me.cboTrainingLearningObjective.Name = "cboTrainingLearningObjective"
        Me.cboTrainingLearningObjective.Size = New System.Drawing.Size(203, 21)
        Me.cboTrainingLearningObjective.TabIndex = 108
        '
        'objlblSign10
        '
        Me.objlblSign10.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign10.ForeColor = System.Drawing.Color.Red
        Me.objlblSign10.Location = New System.Drawing.Point(84, 342)
        Me.objlblSign10.Name = "objlblSign10"
        Me.objlblSign10.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign10.TabIndex = 106
        Me.objlblSign10.Text = "*"
        Me.objlblSign10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign10.Visible = False
        '
        'objlblSign9
        '
        Me.objlblSign9.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign9.ForeColor = System.Drawing.Color.Red
        Me.objlblSign9.Location = New System.Drawing.Point(84, 315)
        Me.objlblSign9.Name = "objlblSign9"
        Me.objlblSign9.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign9.TabIndex = 105
        Me.objlblSign9.Text = "*"
        Me.objlblSign9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign9.Visible = False
        '
        'objlblSign8
        '
        Me.objlblSign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign8.ForeColor = System.Drawing.Color.Red
        Me.objlblSign8.Location = New System.Drawing.Point(84, 288)
        Me.objlblSign8.Name = "objlblSign8"
        Me.objlblSign8.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign8.TabIndex = 104
        Me.objlblSign8.Text = "*"
        Me.objlblSign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign8.Visible = False
        '
        'objlblSign7
        '
        Me.objlblSign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign7.ForeColor = System.Drawing.Color.Red
        Me.objlblSign7.Location = New System.Drawing.Point(84, 261)
        Me.objlblSign7.Name = "objlblSign7"
        Me.objlblSign7.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign7.TabIndex = 103
        Me.objlblSign7.Text = "*"
        Me.objlblSign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign7.Visible = False
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(84, 234)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign6.TabIndex = 102
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign6.Visible = False
        '
        'objlblSign5
        '
        Me.objlblSign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign5.ForeColor = System.Drawing.Color.Red
        Me.objlblSign5.Location = New System.Drawing.Point(84, 207)
        Me.objlblSign5.Name = "objlblSign5"
        Me.objlblSign5.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign5.TabIndex = 101
        Me.objlblSign5.Text = "*"
        Me.objlblSign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'LblSupportRequired
        '
        Me.LblSupportRequired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSupportRequired.Location = New System.Drawing.Point(102, 288)
        Me.LblSupportRequired.Name = "LblSupportRequired"
        Me.LblSupportRequired.Size = New System.Drawing.Size(164, 17)
        Me.LblSupportRequired.TabIndex = 98
        Me.LblSupportRequired.Text = "Support Required"
        Me.LblSupportRequired.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSupportRequired
        '
        Me.cboSupportRequired.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSupportRequired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSupportRequired.FormattingEnabled = True
        Me.cboSupportRequired.Location = New System.Drawing.Point(274, 286)
        Me.cboSupportRequired.Name = "cboSupportRequired"
        Me.cboSupportRequired.Size = New System.Drawing.Size(203, 21)
        Me.cboSupportRequired.TabIndex = 99
        '
        'LblActivity
        '
        Me.LblActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblActivity.Location = New System.Drawing.Point(102, 261)
        Me.LblActivity.Name = "LblActivity"
        Me.LblActivity.Size = New System.Drawing.Size(164, 17)
        Me.LblActivity.TabIndex = 96
        Me.LblActivity.Text = "Activity"
        Me.LblActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboActivity
        '
        Me.cboActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivity.FormattingEnabled = True
        Me.cboActivity.Location = New System.Drawing.Point(274, 259)
        Me.cboActivity.Name = "cboActivity"
        Me.cboActivity.Size = New System.Drawing.Size(203, 21)
        Me.cboActivity.TabIndex = 97
        '
        'LblImprovement
        '
        Me.LblImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImprovement.Location = New System.Drawing.Point(102, 234)
        Me.LblImprovement.Name = "LblImprovement"
        Me.LblImprovement.Size = New System.Drawing.Size(164, 17)
        Me.LblImprovement.TabIndex = 88
        Me.LblImprovement.Text = "Improvement"
        Me.LblImprovement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTimeframe
        '
        Me.lblTimeframe.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTimeframe.Location = New System.Drawing.Point(102, 342)
        Me.lblTimeframe.Name = "lblTimeframe"
        Me.lblTimeframe.Size = New System.Drawing.Size(164, 17)
        Me.lblTimeframe.TabIndex = 94
        Me.lblTimeframe.Text = "Time Frame"
        Me.lblTimeframe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTimeFrame
        '
        Me.cboTimeFrame.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTimeFrame.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTimeFrame.FormattingEnabled = True
        Me.cboTimeFrame.Location = New System.Drawing.Point(274, 340)
        Me.cboTimeFrame.Name = "cboTimeFrame"
        Me.cboTimeFrame.Size = New System.Drawing.Size(203, 21)
        Me.cboTimeFrame.TabIndex = 95
        '
        'lblOtherTraining
        '
        Me.lblOtherTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherTraining.Location = New System.Drawing.Point(102, 315)
        Me.lblOtherTraining.Name = "lblOtherTraining"
        Me.lblOtherTraining.Size = New System.Drawing.Size(164, 17)
        Me.lblOtherTraining.TabIndex = 90
        Me.lblOtherTraining.Text = "Other Training"
        Me.lblOtherTraining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOtherTraining
        '
        Me.cboOtherTraining.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherTraining.FormattingEnabled = True
        Me.cboOtherTraining.Location = New System.Drawing.Point(274, 313)
        Me.cboOtherTraining.Name = "cboOtherTraining"
        Me.cboOtherTraining.Size = New System.Drawing.Size(203, 21)
        Me.cboOtherTraining.TabIndex = 91
        '
        'cboImprovement
        '
        Me.cboImprovement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImprovement.FormattingEnabled = True
        Me.cboImprovement.Location = New System.Drawing.Point(274, 232)
        Me.cboImprovement.Name = "cboImprovement"
        Me.cboImprovement.Size = New System.Drawing.Size(203, 21)
        Me.cboImprovement.TabIndex = 89
        '
        'LblResult
        '
        Me.LblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblResult.Location = New System.Drawing.Point(102, 207)
        Me.LblResult.Name = "LblResult"
        Me.LblResult.Size = New System.Drawing.Size(164, 17)
        Me.LblResult.TabIndex = 86
        Me.LblResult.Text = "Result"
        Me.LblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(274, 205)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(203, 21)
        Me.cboResult.TabIndex = 87
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(84, 180)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign4.TabIndex = 81
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign4.Visible = False
        '
        'lblIsDefault
        '
        Me.lblIsDefault.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsDefault.Location = New System.Drawing.Point(102, 180)
        Me.lblIsDefault.Name = "lblIsDefault"
        Me.lblIsDefault.Size = New System.Drawing.Size(164, 17)
        Me.lblIsDefault.TabIndex = 82
        Me.lblIsDefault.Text = "Assessment Sub Item Code"
        Me.lblIsDefault.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAssessmentSubItemCode
        '
        Me.cboAssessmentSubItemCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessmentSubItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessmentSubItemCode.FormattingEnabled = True
        Me.cboAssessmentSubItemCode.Location = New System.Drawing.Point(274, 178)
        Me.cboAssessmentSubItemCode.Name = "cboAssessmentSubItemCode"
        Me.cboAssessmentSubItemCode.Size = New System.Drawing.Size(203, 21)
        Me.cboAssessmentSubItemCode.TabIndex = 83
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(84, 153)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign3.TabIndex = 75
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblAssessmentItemCode
        '
        Me.lblAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItemCode.Location = New System.Drawing.Point(102, 153)
        Me.lblAssessmentItemCode.Name = "lblAssessmentItemCode"
        Me.lblAssessmentItemCode.Size = New System.Drawing.Size(164, 17)
        Me.lblAssessmentItemCode.TabIndex = 76
        Me.lblAssessmentItemCode.Text = "Assessment Item Code"
        Me.lblAssessmentItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAssessmentItemCode
        '
        Me.cboAssessmentItemCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessmentItemCode.FormattingEnabled = True
        Me.cboAssessmentItemCode.Location = New System.Drawing.Point(274, 151)
        Me.cboAssessmentItemCode.Name = "cboAssessmentItemCode"
        Me.cboAssessmentItemCode.Size = New System.Drawing.Size(203, 21)
        Me.cboAssessmentItemCode.TabIndex = 77
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(84, 126)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign2.TabIndex = 72
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblAssessmentGrpCode
        '
        Me.lblAssessmentGrpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentGrpCode.Location = New System.Drawing.Point(102, 126)
        Me.lblAssessmentGrpCode.Name = "lblAssessmentGrpCode"
        Me.lblAssessmentGrpCode.Size = New System.Drawing.Size(164, 17)
        Me.lblAssessmentGrpCode.TabIndex = 73
        Me.lblAssessmentGrpCode.Text = "Assessment Group Code"
        Me.lblAssessmentGrpCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAssessmentGrpCode
        '
        Me.cboAssessmentGrpCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessmentGrpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessmentGrpCode.FormattingEnabled = True
        Me.cboAssessmentGrpCode.Location = New System.Drawing.Point(274, 124)
        Me.cboAssessmentGrpCode.Name = "cboAssessmentGrpCode"
        Me.cboAssessmentGrpCode.Size = New System.Drawing.Size(203, 21)
        Me.cboAssessmentGrpCode.TabIndex = 74
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(274, 97)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(203, 21)
        Me.cboEmployeeCode.TabIndex = 71
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(102, 99)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(164, 17)
        Me.lblEmployeeCode.TabIndex = 70
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(84, 99)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign1.TabIndex = 69
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(360, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(234, 19)
        Me.lblCaption.TabIndex = 68
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'wizPageData
        '
        Me.wizPageData.Controls.Add(Me.dgData)
        Me.wizPageData.Controls.Add(Me.btnFilter)
        Me.wizPageData.Controls.Add(Me.pnlInfo)
        Me.wizPageData.Location = New System.Drawing.Point(0, 0)
        Me.wizPageData.Name = "wizPageData"
        Me.wizPageData.Size = New System.Drawing.Size(763, 410)
        Me.wizPageData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizPageData.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhAssessorCode, Me.colhEmployee, Me.colhGroupCode, Me.colhItemCode, Me.colhSubItemCode, Me.colhResult, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(741, 298)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 25
        '
        'colhAssessorCode
        '
        Me.colhAssessorCode.HeaderText = "Assessor Code"
        Me.colhAssessorCode.Name = "colhAssessorCode"
        Me.colhAssessorCode.ReadOnly = True
        Me.colhAssessorCode.Width = 90
        '
        'colhEmployee
        '
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        Me.colhEmployee.Width = 80
        '
        'colhGroupCode
        '
        Me.colhGroupCode.HeaderText = "Group Code"
        Me.colhGroupCode.Name = "colhGroupCode"
        Me.colhGroupCode.ReadOnly = True
        Me.colhGroupCode.Width = 80
        '
        'colhItemCode
        '
        Me.colhItemCode.HeaderText = "Item Code"
        Me.colhItemCode.Name = "colhItemCode"
        Me.colhItemCode.ReadOnly = True
        Me.colhItemCode.Width = 80
        '
        'colhSubItemCode
        '
        Me.colhSubItemCode.HeaderText = "Sub Item Code"
        Me.colhSubItemCode.Name = "colhSubItemCode"
        Me.colhSubItemCode.ReadOnly = True
        '
        'colhResult
        '
        Me.colhResult.HeaderText = "Result"
        Me.colhResult.Name = "colhResult"
        Me.colhResult.ReadOnly = True
        Me.colhResult.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.Width = 80
        '
        'colhMessage
        '
        Me.colhMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 373)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(187, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(186, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(186, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(741, 51)
        Me.pnlInfo.TabIndex = 3
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(619, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(505, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(619, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(551, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(664, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(505, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(664, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(551, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmImportAssessorAssessment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 458)
        Me.Controls.Add(Me.WizImportAssessorAssessment)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportAssessorAssessment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Assessor Assessment"
        Me.WizImportAssessorAssessment.ResumeLayout(False)
        Me.wizPageFile.ResumeLayout(False)
        Me.wizPageFile.PerformLayout()
        Me.wizPageMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.wizPageData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents WizImportAssessorAssessment As eZee.Common.eZeeWizard
    Friend WithEvents wizPageData As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentGrpCode As System.Windows.Forms.Label
    Friend WithEvents cboAssessmentGrpCode As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents lblIsDefault As System.Windows.Forms.Label
    Friend WithEvents cboAssessmentSubItemCode As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentItemCode As System.Windows.Forms.Label
    Friend WithEvents cboAssessmentItemCode As System.Windows.Forms.ComboBox
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LblImprovement As System.Windows.Forms.Label
    Friend WithEvents cboImprovement As System.Windows.Forms.ComboBox
    Friend WithEvents LblResult As System.Windows.Forms.Label
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents lblTimeframe As System.Windows.Forms.Label
    Friend WithEvents cboTimeFrame As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherTraining As System.Windows.Forms.Label
    Friend WithEvents cboOtherTraining As System.Windows.Forms.ComboBox
    Friend WithEvents LblSupportRequired As System.Windows.Forms.Label
    Friend WithEvents cboSupportRequired As System.Windows.Forms.ComboBox
    Friend WithEvents LblActivity As System.Windows.Forms.Label
    Friend WithEvents cboActivity As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign5 As System.Windows.Forms.Label
    Friend WithEvents objlblSign8 As System.Windows.Forms.Label
    Friend WithEvents objlblSign7 As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents objlblSign10 As System.Windows.Forms.Label
    Friend WithEvents objlblSign9 As System.Windows.Forms.Label
    Friend WithEvents objlblSign11 As System.Windows.Forms.Label
    Friend WithEvents LblTrainingLearningObjective As System.Windows.Forms.Label
    Friend WithEvents cboTrainingLearningObjective As System.Windows.Forms.ComboBox
    Friend WithEvents cboAssessorCode As System.Windows.Forms.ComboBox
    Friend WithEvents LblAssessorCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign13 As System.Windows.Forms.Label
    Friend WithEvents objlblSign12 As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhAssessorCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGroupCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhItemCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSubItemCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhResult As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
