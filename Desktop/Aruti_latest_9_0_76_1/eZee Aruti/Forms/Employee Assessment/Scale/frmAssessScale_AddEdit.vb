﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmAssessScale_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessScale_AddEdit"
    Private mintScaleTranId As Integer = 0
    Private objScaleMaster As clsAssessment_Scale
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintScaleTranId = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintScaleTranId

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtScale.BackColor = GUI.ColorComp
            cboScaleGroup.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            'S.SANDEEP [11-OCT-2018] -- START
            txtScoreFrom.BackColor = GUI.ColorOptional
            txtScoreTo.BackColor = GUI.ColorOptional
            'S.SANDEEP [11-OCT-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtScale.Decimal = objScaleMaster._Scale
            txtDescription.Text = objScaleMaster._Description
            cboScaleGroup.SelectedValue = objScaleMaster._Scalemasterunkid
            cboPeriod.SelectedValue = objScaleMaster._Periodunkid
            'S.SANDEEP [11-OCT-2018] -- START
            txtScoreFrom.Decimal = objScaleMaster._PercentFrom
            txtScoreTo.Decimal = objScaleMaster._PercentTo
            'S.SANDEEP [11-OCT-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objScaleMaster._Scale = txtScale.Decimal
            objScaleMaster._Scalemasterunkid = CInt(cboScaleGroup.SelectedValue)
            objScaleMaster._Description = txtDescription.Text
            objScaleMaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            'S.SANDEEP [11-OCT-2018] -- START
            objScaleMaster._PercentFrom = CDec(txtScoreFrom.Decimal)
            objScaleMaster._PercentTo = CDec(txtScoreTo.Decimal)
            'S.SANDEEP [11-OCT-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboScaleGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Scale Group is mandatory information. Please select Scale Group to continue."), enMsgBoxStyle.Information)
                cboScaleGroup.Focus()
                Return False
            End If

            If txtScoreFrom.Decimal > 0 Or txtScoreTo.Decimal > 0 Then
                If txtScoreTo.Decimal <= txtScoreFrom.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Percentage To cannot be less or euqal to Perecentage from."), enMsgBoxStyle.Information)
                    Return False
                End If
                Dim iMsg As String = ""
                iMsg = objScaleMaster.IsValidPercentageValue(txtScoreFrom.Decimal, CInt(cboPeriod.SelectedValue), CInt(cboScaleGroup.SelectedValue))
                If iMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            'If IsNumeric(txtScale.Decimal) = False Or txtScale.Decimal = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Scale is mandatory information. Please provide Scale to continue."), enMsgBoxStyle.Information)
            '    txtScale.Focus()
            '    Return False
            'End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Combo()
        Dim objCMaster As New clsCommon_Master
        Dim objCPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP, True, "List")
            With cboScaleGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmScale_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objScaleMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmScale_AddEdit_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmScale_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objScaleMaster = New clsAssessment_Scale
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objScaleMaster._Scaletranunkid = mintScaleTranId
            End If
            Call Fill_Combo()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmScale_AddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessment_Scale.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessment_Scale"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

    'S.SANDEEP [11-OCT-2018] -- START
#Region " Combobox Events "

    Private Sub cboScaleGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboScaleGroup.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            objlblLastRating.Text = objScaleMaster.getLastPecentageDefined(CInt(cboPeriod.SelectedValue), CInt(cboScaleGroup.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboScaleGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [11-OCT-2018] -- END

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objScaleMaster.Update()
            Else
                blnFlag = objScaleMaster.Insert()
            End If

            If blnFlag = False AndAlso objScaleMaster._Message <> "" Then
                eZeeMsgBox.Show(objScaleMaster._Message, enMsgBoxStyle.Information)
            End If
            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objScaleMaster = Nothing
                    objScaleMaster = New clsAssessment_Scale
                    cboScaleGroup.Tag = CInt(cboScaleGroup.SelectedValue)
                    cboPeriod.Tag = CInt(cboPeriod.SelectedValue)
                    Call GetValue()
                    cboPeriod.SelectedValue = CInt(cboPeriod.Tag)
                    cboPeriod.Select()

                    cboScaleGroup.SelectedValue = CInt(cboScaleGroup.Tag)
                    txtScale.Focus()
                Else
                    mintScaleTranId = objScaleMaster._Scaletranunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboScaleGroup.ValueMember
                .DisplayMember = cboScaleGroup.DisplayMember
                .DataSource = CType(cboScaleGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboScaleGroup.SelectedValue = .SelectedValue
                    cboScaleGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCategory.Click
        Dim frm As New frmCommonMaster
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim iRefId As Integer = -1
            frm.displayDialog(iRefId, clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP, enAction.ADD_ONE)
            If iRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP, True, "List")
                With cboScaleGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = iRefId
                End With
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbScaleInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbScaleInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbScaleInfo.Text = Language._Object.getCaption(Me.gbScaleInfo.Name, Me.gbScaleInfo.Text)
			Me.lblScale.Text = Language._Object.getCaption(Me.lblScale.Name, Me.lblScale.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Scale is mandatory information. Please provide Scale to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Scale Group is mandatory information. Please select Scale Group to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class