﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMigration

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmReviewer_AddEdit"
    Private objAssessor As clsAssessor
    Private objAssessorTran As clsAssessor_tran
    Private dtOldEmp As DataTable
    Private dtNewEmp As DataTable
    Private dtAssignedEmp As DataTable

    Private dgOldView As DataView
    Private dgNewView As DataView
    Private dgAssignedEmp As DataView

#End Region

#Region " Private Function / Procedure "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If radAssessor.Checked = True Then
            '    dsCombo = objAssessor.GetList("List", False, True)
            'ElseIf radReviewer.Checked = True Then
            '    dsCombo = objAssessor.GetList("List", True, True)
            'End If

            Dim blnReviewerFlag As Boolean = False
            If radAssessor.Checked = True Then
                blnReviewerFlag = False
            ElseIf radReviewer.Checked = True Then
                blnReviewerFlag = True
            End If

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE
            'S.SANDEEP [27 DEC 2016] -- END
            dsCombo = objAssessor.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, "List", blnReviewerFlag, strVisibleTypeIds.ToString, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboFrom
                .ValueMember = "assessormasterunkid"
                .DisplayMember = "assessorname"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assessor_Emp(Optional ByVal isMigrate As Boolean = False)
        Try
            If isMigrate = False Then
                If cboFrom.DataSource IsNot Nothing Then
                    Dim itmp() As DataRow = CType(cboFrom.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboFrom.SelectedValue) & "'")
                    If itmp.Length > 0 Then
                        'Shani(29-Nov-2016) -- Start 
                        'Issue : TRA Fix for assessor migration.
                        'Shani(01-MAR-2016) -- Start
                        'Enhancement :PA External Approver Flow
                        'objAssessorTran._AssessorEmployeeId = CInt(itmp(0).Item("employeeunkid"))
                        Dim eval As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA
                        If radAssessor.Checked Then
                            eval = clsAssessor.enAssessorType.ASSESSOR
                        ElseIf radReviewer.Checked Then
                            eval = clsAssessor.enAssessorType.REVIEWER
                        End If

                        'Shani (07-Dec-2016) -- Start
                        'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
                        objAssessorTran._EmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
                        'Shani (07-Dec-2016) -- End
                        objAssessorTran._AssessorEmployeeId(CBool(itmp(0).Item("isexternalapprover")), eval) = CInt(itmp(0).Item("employeeunkid"))
                        'Shani(01-MAR-2016) -- End
                        'Shani(29-Nov-2016) --  End 

                        dtOldEmp = objAssessorTran._DataTable.Copy
                    End If
                End If
            End If

            If radAssessor.Checked = True Then
                dtOldEmp = New DataView(dtOldEmp, "isreviewer = 0", "", DataViewRowState.CurrentRows).ToTable
            ElseIf radReviewer.Checked = True Then
                dtOldEmp = New DataView(dtOldEmp, "isreviewer = 1", "", DataViewRowState.CurrentRows).ToTable
            End If


            dgOldView = dtOldEmp.DefaultView
            dgvOldEmp.AutoGenerateColumns = False
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "name"
            objdgcolhOldMasterunkid.DataPropertyName = "assessormasterunkid"
            objdgcolhOldTranId.DataPropertyName = "assessortranunkid"
            dgvOldEmp.DataSource = dgOldView

            If dtNewEmp Is Nothing Then
                dtNewEmp = dtOldEmp.Clone
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Assessor_Emp", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Migrated_Emp()
        Try
            dgNewView = dtNewEmp.DefaultView
            dgvNewEmp.AutoGenerateColumns = False
            objdgcolhNewEmpId.DataPropertyName = "employeeunkid"
            objdgcolhNewChek.DataPropertyName = "ischeck"
            dgcolhNewCode.DataPropertyName = "employeecode"
            dgcolhNewEmp.DataPropertyName = "name"
            objdgcolhNewMasterunkid.DataPropertyName = "assessormasterunkid"
            objdgcolhNewTranId.DataPropertyName = "assessortranunkid"
            dgvNewEmp.DataSource = dgNewView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Migrated_Emp", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assigned_Emp()
        Try
            If cboTo.DataSource IsNot Nothing Then
                Dim itmp() As DataRow = CType(cboTo.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboTo.SelectedValue) & "'")
                If itmp.Length > 0 Then

                    'Shani(29-Nov-2016) -- Start 
                    'Issue : TRA Fix for assessor migration.
                    'Shani(01-MAR-2016) -- Start
                    'Enhancement:PA External Approver Flow
                    'objAssessorTran._AssessorEmployeeId = CInt(itmp(0).Item("employeeunkid"))
                    Dim eval As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA
                    If radAssessor.Checked Then
                        eval = clsAssessor.enAssessorType.ASSESSOR
                    ElseIf radReviewer.Checked Then
                        eval = clsAssessor.enAssessorType.REVIEWER
                    End If

                    'Shani (07-Dec-2016) -- Start
                    'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
                    objAssessorTran._EmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
                    'Shani (07-Dec-2016) -- End
                    objAssessorTran._AssessorEmployeeId(CBool(itmp(0).Item("isexternalapprover")), eval) = CInt(itmp(0).Item("employeeunkid"))
                    'Shani(01-MAR-2016)-- End
                    'Shani(29-Nov-2016) -- End


                    dtAssignedEmp = objAssessorTran._DataTable.Copy
                    If radAssessor.Checked = True Then
                        dtAssignedEmp = New DataView(dtAssignedEmp, "isreviewer = 0", "", DataViewRowState.CurrentRows).ToTable
                    ElseIf radReviewer.Checked = True Then
                        dtAssignedEmp = New DataView(dtAssignedEmp, "isreviewer = 1", "", DataViewRowState.CurrentRows).ToTable
                    End If
                    dgAssignedEmp = dtAssignedEmp.DefaultView
                    dgvAssigned.AutoGenerateColumns = False
                    objdgcolhAEmpId.DataPropertyName = "employeeunkid"
                    dgcolhACode.DataPropertyName = "employeecode"
                    dgcolhAName.DataPropertyName = "name"
                    dgvAssigned.DataSource = dgAssignedEmp
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmMigration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessor = New clsAssessor
        objAssessorTran = New clsAssessor_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            radAssessor.Checked = True
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnSave.Enabled = User._Object.Privilege._AllowToMigrateAssessor_Reviewer
            btnSave.Enabled = User._Object.Privilege._AllowtoPerformAssessorReviewerMigration
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMigration_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessor_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessor_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchOldValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldValue.Click
        Dim frm As New frmCommonSearch
        Try
            If cboFrom.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboFrom.ValueMember
                .DisplayMember = cboFrom.DisplayMember
                .DataSource = CType(cboFrom.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboFrom.SelectedValue = frm.SelectedValue
                cboFrom.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldValue_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchNewValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewValue.Click
        Dim frm As New frmCommonSearch
        Try
            If cboTo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboTo.ValueMember
                .DisplayMember = cboTo.DisplayMember
                .DataSource = CType(cboTo.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboTo.SelectedValue = frm.SelectedValue
                cboTo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewValue_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Me.Cursor = Cursors.WaitCursor
        Try
            If radAssessor.Checked = True Then
                If CInt(cboFrom.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select From Assessor in order to migrate data."), enMsgBoxStyle.Information)
                    cboFrom.Focus()
                    Exit Sub
                End If

                If CInt(cboTo.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select To Assessor in order to migrate data."), enMsgBoxStyle.Information)
                    cboTo.Focus()
                    Exit Sub
                End If
            ElseIf radReviewer.Checked = True Then
                If CInt(cboFrom.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select From Assessor in order to migrate data."), enMsgBoxStyle.Information)
                    cboFrom.Focus()
                    Exit Sub
                End If

                If CInt(cboTo.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select To Assessor in order to migrate data."), enMsgBoxStyle.Information)
                    cboTo.Focus()
                    Exit Sub
                End If
            End If
            Dim dTemp() As DataRow = dtOldEmp.Select("ischeck = true")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Check atleast one employee to migrate."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim blnFlag As Boolean = False

            'S.SANDEEP [ 28 OCT 2013 ] -- START
            Dim iEmployeeId As Integer = 0
            Dim itmp() As DataRow = CType(cboTo.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboTo.SelectedValue) & "'")
            If itmp.Length > 0 Then
                iEmployeeId = CInt(itmp(0).Item("employeeunkid"))
            End If
            'S.SANDEEP [ 28 OCT 2013 ] -- END
            For i As Integer = 0 To dTemp.Length - 1
                'S.SANDEEP [ 28 OCT 2013 ] -- START
                If iEmployeeId = CInt(dTemp(i)("employeeunkid")) Then
                    If radAssessor.Checked = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot make same employee as his own assessor and will not be added to migration list."), enMsgBoxStyle.Information)
                    ElseIf radReviewer.Checked = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot make same employee as his own reviever and will not be added to migration list."), enMsgBoxStyle.Information)
                    End If
                    Continue For
                End If
                'S.SANDEEP [ 28 OCT 2013 ] -- END

                'S.SANDEEP [27 DEC 2016] -- START
                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                'If objAssessor.IsEmpMapped(CInt(cboTo.SelectedValue), CInt(dTemp(i)("employeeunkid"))) Then
                If objAssessor.IsEmpMapped(CInt(cboTo.SelectedValue), CInt(dTemp(i)("employeeunkid")), clsAssessor.enARVisibilityTypeId.VISIBLE) Then
                    'S.SANDEEP [27 DEC 2016] -- END

                    If blnFlag = False Then
                        If radAssessor.Checked = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Some of the checked employee is already linked with the selected approver and will not be added to the migration list."), enMsgBoxStyle.Information)
                        ElseIf radReviewer.Checked = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Some of the checked employee is already linked with the selected reviewer and will not be added to the migration list."), enMsgBoxStyle.Information)
                        End If
                        blnFlag = True
                        Continue For
                    Else
                        Continue For
                    End If
                End If
                dTemp(i)("ischeck") = False
                dtNewEmp.ImportRow(dTemp(i))
                dtOldEmp.Rows.Remove(dTemp(i))
            Next
            Call Fill_Migrated_Emp()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try
            Dim dTemp() As DataRow = dtNewEmp.Select("ischeck = true")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please Check atleast one employee to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            For i As Integer = 0 To dTemp.Length - 1
                dTemp(i)("ischeck") = False
                dtOldEmp.ImportRow(dTemp(i))
                dtNewEmp.Rows.Remove(dTemp(i))
            Next
            Call Fill_Assessor_Emp(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'Dim itmp() As DataRow = CType(cboFrom.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboFrom.SelectedValue) & "'")
            'Dim iEmpId As Integer = 0
            'If itmp.Length > 0 Then
            '    iEmpId = CInt(itmp(0).Item("employeeunkid"))
            'End If


            ''Shani(01-MAR-2016) -- Start
            ''Enhancement:PA External Approver Flow
            ''If objAssessorTran.Perform_Migration(CInt(cboFrom.SelectedValue), dtNewEmp, CInt(cboTo.SelectedValue), CBool(IIf(radAssessor.Checked = True, True, False)), iEmpId, User._Object._Userunkid) = False Then
            'If objAssessorTran.Perform_Migration(CInt(cboFrom.SelectedValue), dtNewEmp, CInt(cboTo.SelectedValue), CBool(IIf(radAssessor.Checked = True, True, False)), iEmpId, User._Object._Userunkid, CBool(CType(cboFrom.SelectedItem, DataRowView).Item("isexternalapprover")), ConfigParameter._Object._EmployeeAsOnDate) = False Then
            '    'Shani (07-Dec-2016) --ADD-- [ConfigParameter._Object._EmployeeAsOnDate]

            '    'Shani(01-MAR-2016)-- End
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Problem in migration process."), enMsgBoxStyle.Information)
            '    Exit Sub
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Migration process completed successfully."), enMsgBoxStyle.Information)
            '    dtNewEmp.Rows.Clear()
            '    dtOldEmp.Rows.Clear()
            '    dtAssignedEmp.Rows.Clear()
            '    Call FillCombo()
            '    Call Fill_Assessor_Emp()
            '    Call Fill_Assigned_Emp()
            '    Call Fill_Migrated_Emp()
            'End If
            If dtNewEmp IsNot Nothing Then

                If radOverWriteAssessment.Checked = False AndAlso radVoidAssessment.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Please select atleast one operation type in order to do migration process."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim itmp() As DataRow = Nothing
                itmp = CType(cboFrom.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboFrom.SelectedValue) & "'")
                Dim iEmpId, iToArEmpId As Integer : iEmpId = 0 : iToArEmpId = 0
                If itmp.Length > 0 Then
                    iEmpId = CInt(itmp(0).Item("employeeunkid"))
                End If

                itmp = CType(cboFrom.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboTo.SelectedValue) & "'")
                If itmp.Length > 0 Then
                    iToArEmpId = CInt(itmp(0).Item("employeeunkid"))
                End If

                If objAssessorTran.Perform_Migration(CInt(cboFrom.SelectedValue), _
                                                     dtNewEmp, _
                                                     CInt(cboTo.SelectedValue), _
                                                     CBool(IIf(radAssessor.Checked = True, True, False)), _
                                                     iEmpId, _
                                                     User._Object._Userunkid, _
                                                     CBool(CType(cboFrom.SelectedItem, DataRowView).Item("isexternalapprover")), _
                                                     ConfigParameter._Object._EmployeeAsOnDate, _
                                                     CType(IIf(radOverWriteAssessment.Checked = True, clsAssessor_tran.enOperationType.Overwrite, clsAssessor_tran.enOperationType.Void), clsAssessor_tran.enOperationType), _
                                                     CType(IIf(radAssessor.Checked = True, enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT), enAssessmentMode), _
                                                     iToArEmpId, _
                                                     mstrModuleName) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Problem in migration process."), enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Migration process completed successfully."), enMsgBoxStyle.Information)
                    dtNewEmp.Rows.Clear()
                    dtOldEmp.Rows.Clear()
                    dtAssignedEmp.Rows.Clear()
                    Call FillCombo()
                    Call Fill_Assessor_Emp()
                    Call Fill_Assigned_Emp()
                    Call Fill_Migrated_Emp()
                End If
            End If
            'S.SANDEEP [27 DEC 2016] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub txtOldEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOldEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvOldEmp.Rows.Count > 0 Then
                        If dgvOldEmp.SelectedRows(0).Index = dgvOldEmp.Rows(dgvOldEmp.RowCount - 1).Index Then Exit Sub
                        dgvOldEmp.Rows(dgvOldEmp.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dtOldEmp.Rows.Count > 0 Then
                        If dgvOldEmp.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvOldEmp.Rows(dgvOldEmp.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtOldEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtOldEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOldEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtOldEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtOldEmp.Text & "%' OR " & _
                            dgcolhEcode.DataPropertyName & " LIKE '%" & txtOldEmp.Text & "%'"
                objchkOldEmp.Checked = False
                objchkOldEmp.Enabled = False
            Else
                objchkOldEmp.Enabled = True
            End If

            dgOldView.RowFilter = strSearch

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNewEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvNewEmp.Rows.Count > 0 Then
                        If dgvNewEmp.SelectedRows(0).Index = dgvNewEmp.Rows(dgvNewEmp.RowCount - 1).Index Then Exit Sub
                        dgvNewEmp.Rows(dgvNewEmp.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvNewEmp.Rows.Count > 0 Then
                        If dgvNewEmp.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvNewEmp.Rows(dgvNewEmp.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtNewEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtNewEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhNewEmp.DataPropertyName & " LIKE '%" & txtNewEmp.Text & "%' OR " & _
                            dgcolhNewCode.DataPropertyName & " LIKE '%" & txtNewEmp.Text & "%'"
                objchkNewEmp.Checked = False
                objchkNewEmp.Enabled = False
            Else
                objchkNewEmp.Enabled = True
            End If

            dgNewView.RowFilter = strSearch

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtNewEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssignedEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAssignedEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAssigned.Rows.Count > 0 Then
                        If dgvAssigned.SelectedRows(0).Index = dgvAssigned.Rows(dgvAssigned.RowCount - 1).Index Then Exit Sub
                        dgvAssigned.Rows(dgvAssigned.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAssigned.Rows.Count > 0 Then
                        If dgvAssigned.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAssigned.Rows(dgvAssigned.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssignedEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssignedEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAssignedEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtAssignedEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhAName.DataPropertyName & " LIKE '%" & txtAssignedEmp.Text & "%' OR " & _
                            dgcolhACode.DataPropertyName & " LIKE '%" & txtAssignedEmp.Text & "%'"
            End If

            dgAssignedEmp.RowFilter = strSearch

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtNewEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFrom.SelectedIndexChanged
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim dsCombo As New DataSet
            Dim dtTable As DataTable

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If radAssessor.Checked = True Then
            '    dsCombo = objAssessor.GetList("List", False, True)
            'ElseIf radReviewer.Checked = True Then
            '    dsCombo = objAssessor.GetList("List", True, True)
            'End If

            'If CInt(cboFrom.SelectedValue) <= 0 Then
            '    dtTable = New DataView(dsCombo.Tables(0), "assessormasterunkid IN(" & cboFrom.SelectedValue.ToString & ")", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsCombo.Tables(0), "assessormasterunkid NOT IN(" & cboFrom.SelectedValue.ToString & ")", "", DataViewRowState.CurrentRows).ToTable
            'End If

            Dim blnReviewerFlag As Boolean = False
            Dim StrSearch As String = ""
            If radAssessor.Checked = True Then
                blnReviewerFlag = False
            ElseIf radReviewer.Checked = True Then
                blnReviewerFlag = True
            End If

            If CInt(cboFrom.SelectedValue) <= 0 Then
                StrSearch = "hrassessor_master.assessormasterunkid IN(" & cboFrom.SelectedValue.ToString & ")"
            Else
                StrSearch = "hrassessor_master.assessormasterunkid NOT IN(" & cboFrom.SelectedValue.ToString & ")"
            End If

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE
            'S.SANDEEP [27 DEC 2016] -- END

            dsCombo = objAssessor.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, "List", blnReviewerFlag, strVisibleTypeIds.ToString, True, StrSearch)

            dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [04 JUN 2015] -- END

            With cboTo
                .ValueMember = "assessormasterunkid"
                .DisplayMember = "assessorname"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            Call Fill_Assessor_Emp()

            If dtNewEmp IsNot Nothing Then dtNewEmp.Rows.Clear()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFrom_SelectedIndexChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub radReviewer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radReviewer.CheckedChanged
        Try
            If radReviewer.Checked = True Then
                lblFromValue.Text = Language.getMessage(mstrModuleName, 3, "From Reviewer")
                lblToValue.Text = Language.getMessage(mstrModuleName, 4, "To Reviewer")
                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radReviewer_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAssessor.CheckedChanged
        Try
            If radAssessor.Checked = True Then
                lblFromValue.Text = Language.getMessage(mstrModuleName, 1, "From Assessor")
                lblToValue.Text = Language.getMessage(mstrModuleName, 2, "To Assessor")
                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTo.SelectedIndexChanged
        Try
            Call Fill_Assigned_Emp()
            If dtNewEmp IsNot Nothing Then
                Dim dicDataRow As New Dictionary(Of Integer, DataRow)
                For i As Integer = 0 To dtAssignedEmp.Rows.Count - 1
                    Dim dRow() As DataRow = dtNewEmp.Select("employeeunkid = '" & CInt(dtAssignedEmp.Rows(i)("employeeunkid")) & "'")
                    If dRow.Length > 0 Then
                        If dicDataRow.ContainsKey(i) = False Then
                            dicDataRow.Add(i, dRow(0))
                        End If
                    End If
                Next

                If dicDataRow.Keys.Count > 0 Then
                    For Each SKey As Integer In dicDataRow.Keys
                        dtOldEmp.ImportRow(CType(dicDataRow(SKey), DataRow))
                        dtNewEmp.Rows.Remove(CType(dicDataRow(SKey), DataRow))
                    Next
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTo_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkOldEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkOldEmp.CheckedChanged
        Try
            RemoveHandler dgvOldEmp.CellContentClick, AddressOf dgvOldEmp_CellContentClick
            For Each dr As DataRow In dgOldView.Table.Rows
                dr.Item("IsCheck") = CBool(objchkOldEmp.CheckState)
            Next
            dgOldView.Table.AcceptChanges()
            AddHandler dgvOldEmp.CellContentClick, AddressOf dgvOldEmp_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkOldEmp_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvOldEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOldEmp.CellContentClick, dgvOldEmp.CellContentDoubleClick
        Try
            RemoveHandler objchkOldEmp.CheckedChanged, AddressOf objchkOldEmp_CheckedChanged
            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvOldEmp.IsCurrentCellDirty Then
                    Me.dgvOldEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dgOldView.ToTable.Select("IsCheck=True")

                If drRow.Length > 0 Then
                    If dgOldView.ToTable.Rows.Count = drRow.Length Then
                        objchkOldEmp.CheckState = CheckState.Checked
                    Else
                        objchkOldEmp.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkOldEmp.CheckState = CheckState.Unchecked
                End If

            End If
            AddHandler objchkOldEmp.CheckedChanged, AddressOf objchkOldEmp_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOldEmp_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkNewEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNewEmp.CheckedChanged
        Try
            RemoveHandler dgvNewEmp.CellContentClick, AddressOf dgvNewEmp_CellContentClick
            For Each dr As DataRow In dgNewView.Table.Rows
                dr.Item("IsCheck") = CBool(objchkNewEmp.CheckState)
            Next
            dgNewView.Table.AcceptChanges()
            AddHandler dgvNewEmp.CellContentClick, AddressOf dgvNewEmp_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkNewEmp_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvNewEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNewEmp.CellContentClick, dgvNewEmp.CellDoubleClick
        Try
            RemoveHandler objchkNewEmp.CheckedChanged, AddressOf objchkNewEmp_CheckedChanged
            If e.ColumnIndex = objdgcolhNewChek.Index Then

                If Me.dgvNewEmp.IsCurrentCellDirty Then
                    Me.dgvNewEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dgNewView.ToTable.Select("IsCheck=True")

                If drRow.Length > 0 Then
                    If dgNewView.ToTable.Rows.Count = drRow.Length Then
                        objchkNewEmp.CheckState = CheckState.Checked
                    Else
                        objchkNewEmp.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkNewEmp.CheckState = CheckState.Unchecked
                End If

            End If
            AddHandler objchkNewEmp.CheckedChanged, AddressOf objchkNewEmp_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvNewEmp_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbMigration.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMigration.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUnAssign.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbMigration.Text = Language._Object.getCaption(Me.gbMigration.Name, Me.gbMigration.Text)
            Me.radAssessor.Text = Language._Object.getCaption(Me.radAssessor.Name, Me.radAssessor.Text)
            Me.radReviewer.Text = Language._Object.getCaption(Me.radReviewer.Name, Me.radReviewer.Text)
            Me.lblToValue.Text = Language._Object.getCaption(Me.lblToValue.Name, Me.lblToValue.Text)
            Me.lblFromValue.Text = Language._Object.getCaption(Me.lblFromValue.Name, Me.lblFromValue.Text)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.tabpMigrated.Text = Language._Object.getCaption(Me.tabpMigrated.Name, Me.tabpMigrated.Text)
            Me.tabpAssigned.Text = Language._Object.getCaption(Me.tabpAssigned.Name, Me.tabpAssigned.Text)
            Me.dgcolhACode.HeaderText = Language._Object.getCaption(Me.dgcolhACode.Name, Me.dgcolhACode.HeaderText)
            Me.dgcolhAName.HeaderText = Language._Object.getCaption(Me.dgcolhAName.Name, Me.dgcolhAName.HeaderText)
            Me.dgcolhNewCode.HeaderText = Language._Object.getCaption(Me.dgcolhNewCode.Name, Me.dgcolhNewCode.HeaderText)
            Me.dgcolhNewEmp.HeaderText = Language._Object.getCaption(Me.dgcolhNewEmp.Name, Me.dgcolhNewEmp.HeaderText)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.radVoidAssessment.Text = Language._Object.getCaption(Me.radVoidAssessment.Name, Me.radVoidAssessment.Text)
            Me.radOverWriteAssessment.Text = Language._Object.getCaption(Me.radOverWriteAssessment.Name, Me.radOverWriteAssessment.Text)
            Me.elOperationType.Text = Language._Object.getCaption(Me.elOperationType.Name, Me.elOperationType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Assessor")
            Language.setMessage(mstrModuleName, 2, "To Assessor")
            Language.setMessage(mstrModuleName, 3, "From Reviewer")
            Language.setMessage(mstrModuleName, 4, "To Reviewer")
            Language.setMessage(mstrModuleName, 5, "Please select To Assessor in order to migrate data.")
            Language.setMessage(mstrModuleName, 6, "Please select To Assessor in order to migrate data.")
            Language.setMessage(mstrModuleName, 7, "Please select From Assessor in order to migrate data.")
            Language.setMessage(mstrModuleName, 8, "Please select From Assessor in order to migrate data.")
            Language.setMessage(mstrModuleName, 9, "Please Check atleast one employee to migrate.")
            Language.setMessage(mstrModuleName, 10, "Please Check atleast one employee to unassign.")
            Language.setMessage(mstrModuleName, 11, "Sorry, Some of the checked employee is already linked with the selected approver and will not be added to the migration list.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Some of the checked employee is already linked with the selected reviewer and will not be added to the migration list.")
            Language.setMessage(mstrModuleName, 13, "Problem in migration process.")
            Language.setMessage(mstrModuleName, 14, "Migration process completed successfully.")
            Language.setMessage(mstrModuleName, 15, "Sorry, you cannot make same employee as his own assessor and will not be added to migration list.")
            Language.setMessage(mstrModuleName, 16, "Sorry, you cannot make same employee as his own reviever and will not be added to migration list.")
            Language.setMessage(mstrModuleName, 17, "Sorry, Please select atleast one operation type in order to do migration process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class frmMigration

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmReviewer_AddEdit"
'    Private objAssessor As clsAssessor
'    Private objAssessorTran As clsAssessor_tran
'    Private dtOldEmp As DataTable
'    Private dtNewEmp As DataTable
'    Private dtAssignedEmp As DataTable

'    Private dgOldView As DataView
'    Private dgNewView As DataView
'    Private dgAssignedEmp As DataView

'#End Region

'#Region " Private Function / Procedure "

'    Private Sub FillCombo()
'        Dim dsCombo As New DataSet
'        Try
'            If radAssessor.Checked = True Then
'                dsCombo = objAssessor.GetList("List", False, True)
'            ElseIf radReviewer.Checked = True Then
'                dsCombo = objAssessor.GetList("List", True, True)
'            End If

'            With cboFrom
'                .ValueMember = "assessormasterunkid"
'                .DisplayMember = "assessorname"
'                .DataSource = dsCombo.Tables("List")
'                .SelectedValue = 0
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_Assessor_Emp(Optional ByVal isMigrate As Boolean = False)
'        Try
'            If isMigrate = False Then
'                objAssessorTran._Assessormasterunkid = CInt(cboFrom.SelectedValue)
'                dtOldEmp = objAssessorTran.GetData().Tables(0)

'                dtOldEmp.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

'                For i As Integer = 0 To dtOldEmp.Rows.Count - 1
'                    dtOldEmp.Rows(i)("IsCheck") = False
'                Next
'            End If


'            dgOldView = dtOldEmp.DefaultView


'            dgvOldEmp.AutoGenerateColumns = False
'            objdgcolhEmpId.DataPropertyName = "employeeunkid"
'            objdgcolhECheck.DataPropertyName = "IsCheck"
'            dgcolhEcode.DataPropertyName = "employeecode"
'            dgcolhEName.DataPropertyName = "employeename"
'            objdgcolhOldMasterunkid.DataPropertyName = "assessormasterunkid"
'            objdgcolhOldTranId.DataPropertyName = "assessortranunkid"
'            dgvOldEmp.DataSource = dgOldView

'            If dtNewEmp Is Nothing Then
'                dtNewEmp = dtOldEmp.Clone
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_Assessor_Emp", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_Migrated_Emp()
'        Try
'            dgNewView = dtNewEmp.DefaultView

'            dgvNewEmp.AutoGenerateColumns = False
'            objdgcolhNewEmpId.DataPropertyName = "employeeunkid"
'            objdgcolhNewChek.DataPropertyName = "IsCheck"
'            dgcolhNewCode.DataPropertyName = "employeecode"
'            dgcolhNewEmp.DataPropertyName = "employeename"
'            objdgcolhNewMasterunkid.DataPropertyName = "assessormasterunkid"
'            objdgcolhNewTranId.DataPropertyName = "assessortranunkid"
'            dgvNewEmp.DataSource = dgNewView

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_Migrated_Emp", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_Assigned_Emp()
'        Try
'            objAssessorTran._Assessormasterunkid = CInt(cboTo.SelectedValue)
'            dtAssignedEmp = objAssessorTran.GetData().Tables(0)
'            dgAssignedEmp = dtAssignedEmp.DefaultView

'            dgvAssigned.AutoGenerateColumns = False
'            objdgcolhAEmpId.DataPropertyName = "employeeunkid"
'            dgcolhACode.DataPropertyName = "employeecode"
'            dgcolhAName.DataPropertyName = "employeename"
'            dgvAssigned.DataSource = dgAssignedEmp

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmMigration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objAssessor = New clsAssessor
'        objAssessorTran = New clsAssessor_tran
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            radAssessor.Checked = True
'            'S.SANDEEP [ 16 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'            btnSave.Enabled = User._Object.Privilege._AllowToMigrateAssessor_Reviewer
'            'S.SANDEEP [ 16 MAY 2012 ] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmMigration_Load", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub objbtnSearchOldValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldValue.Click
'        Dim frm As New frmCommonSearch
'        Try

'            If cboFrom.DataSource Is Nothing Then Exit Sub

'            With frm
'                .ValueMember = cboFrom.ValueMember
'                .DisplayMember = cboFrom.DisplayMember
'                .DataSource = CType(cboFrom.DataSource, DataTable)
'            End With
'            If frm.DisplayDialog Then
'                cboFrom.SelectedValue = frm.SelectedValue
'                cboFrom.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchOldValue_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnSearchNewValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewValue.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If cboTo.DataSource Is Nothing Then Exit Sub

'            With frm
'                .ValueMember = cboTo.ValueMember
'                .DisplayMember = cboTo.DisplayMember
'                .DataSource = CType(cboTo.DataSource, DataTable)
'            End With
'            If frm.DisplayDialog Then
'                cboTo.SelectedValue = frm.SelectedValue
'                cboTo.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchNewValue_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
'        Try
'            Me.Cursor = Cursors.WaitCursor
'            If radAssessor.Checked = True Then
'                If CInt(cboFrom.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select From Assessor in order to migrate data."), enMsgBoxStyle.Information)
'                    cboFrom.Focus()
'                    Exit Sub
'                End If

'                If CInt(cboTo.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select To Assessor in order to migrate data."), enMsgBoxStyle.Information)
'                    cboTo.Focus()
'                    Exit Sub
'                End If
'            ElseIf radReviewer.Checked = True Then
'                If CInt(cboFrom.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select From Assessor in order to migrate data."), enMsgBoxStyle.Information)
'                    cboFrom.Focus()
'                    Exit Sub
'                End If

'                If CInt(cboTo.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select To Assessor in order to migrate data."), enMsgBoxStyle.Information)
'                    cboTo.Focus()
'                    Exit Sub
'                End If
'            End If

'            Dim dTemp() As DataRow = dtOldEmp.Select("IsCheck = true")

'            If dTemp.Length <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Check atleast one employee to migrate."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If
'            Dim blnFlag As Boolean = False

'            For i As Integer = 0 To dTemp.Length - 1
'                If objAssessorTran.IsEmployeeExists(CInt(dTemp(i)("employeeunkid")), CInt(cboTo.SelectedValue)) = True Then
'                    If blnFlag = False Then
'                        If radAssessor.Checked = True Then
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Some of the checked employee is already binded with the selected approver and will not be added to the migration list."), enMsgBoxStyle.Information)
'                        ElseIf radAssessor.Checked = True Then
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Some of the checked employee is already binded with the selected reviewer and will not be added to the migration list."), enMsgBoxStyle.Information)
'                        End If
'                        blnFlag = True
'                        Continue For
'                    Else
'                        Continue For
'                    End If
'                End If
'                dTemp(i)("IsCheck") = False
'                dtNewEmp.ImportRow(dTemp(i))
'                dtOldEmp.Rows.Remove(dTemp(i))
'            Next

'            Call Fill_Migrated_Emp()


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
'        Finally
'            Me.Cursor = Cursors.Default
'        End Try
'    End Sub

'    Private Sub objbtnUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
'        Try
'            Dim dTemp() As DataRow = dtNewEmp.Select("IsCheck = true")

'            If dTemp.Length <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please Check atleast one employee to unassigned."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If


'            For i As Integer = 0 To dTemp.Length - 1
'                dTemp(i)("IsCheck") = False
'                dtOldEmp.ImportRow(dTemp(i))
'                dtNewEmp.Rows.Remove(dTemp(i))
'            Next

'            Call Fill_Assessor_Emp(True)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnUnAssign_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Try
'            If objAssessorTran.Perform_Migration(dtNewEmp, CInt(cboTo.SelectedValue)) = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Problem in migration process."), enMsgBoxStyle.Information)
'            Else
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Migration process completed successfully."), enMsgBoxStyle.Information)
'                dtNewEmp.Rows.Clear()
'                dtOldEmp.Rows.Clear()
'                dtAssignedEmp.Rows.Clear()
'            End If
'            Call FillCombo()
'            Call Fill_Assessor_Emp()
'            Call Fill_Assigned_Emp()
'            Call Fill_Migrated_Emp()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Controls Events "

'    Private Sub txtOldEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOldEmp.KeyDown
'        Try
'            Select Case e.KeyCode
'                Case Windows.Forms.Keys.Down
'                    If dgvOldEmp.Rows.Count > 0 Then
'                        If dgvOldEmp.SelectedRows(0).Index = dgvOldEmp.Rows(dgvOldEmp.RowCount - 1).Index Then Exit Sub
'                        dgvOldEmp.Rows(dgvOldEmp.SelectedRows(0).Index + 1).Selected = True
'                    End If
'                Case Windows.Forms.Keys.Up
'                    If dtOldEmp.Rows.Count > 0 Then
'                        If dgvOldEmp.SelectedRows(0).Index = 0 Then Exit Sub
'                        dgvOldEmp.Rows(dgvOldEmp.SelectedRows(0).Index - 1).Selected = True
'                    End If
'            End Select
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtOldEmp_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtOldEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOldEmp.TextChanged
'        Dim strSearch As String = ""
'        Try
'            If txtOldEmp.Text.Trim.Length > 0 Then
'                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtOldEmp.Text & "%' OR " & _
'                            dgcolhEcode.DataPropertyName & " LIKE '%" & txtOldEmp.Text & "%'"
'                objchkOldEmp.Checked = False
'                objchkOldEmp.Enabled = False
'            Else
'                objchkOldEmp.Enabled = True
'            End If

'            dgOldView.RowFilter = strSearch

'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtNewEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNewEmp.KeyDown
'        Try
'            Select Case e.KeyCode
'                Case Windows.Forms.Keys.Down
'                    If dgvNewEmp.Rows.Count > 0 Then
'                        If dgvNewEmp.SelectedRows(0).Index = dgvNewEmp.Rows(dgvNewEmp.RowCount - 1).Index Then Exit Sub
'                        dgvNewEmp.Rows(dgvNewEmp.SelectedRows(0).Index + 1).Selected = True
'                    End If
'                Case Windows.Forms.Keys.Up
'                    If dgvNewEmp.Rows.Count > 0 Then
'                        If dgvNewEmp.SelectedRows(0).Index = 0 Then Exit Sub
'                        dgvNewEmp.Rows(dgvNewEmp.SelectedRows(0).Index - 1).Selected = True
'                    End If
'            End Select
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtNewEmp_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtNewEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewEmp.TextChanged
'        Dim strSearch As String = ""
'        Try
'            If txtNewEmp.Text.Trim.Length > 0 Then
'                strSearch = dgcolhNewEmp.DataPropertyName & " LIKE '%" & txtNewEmp.Text & "%' OR " & _
'                            dgcolhNewCode.DataPropertyName & " LIKE '%" & txtNewEmp.Text & "%'"
'                objchkNewEmp.Checked = False
'                objchkNewEmp.Enabled = False
'            Else
'                objchkNewEmp.Enabled = True
'            End If

'            dgNewView.RowFilter = strSearch

'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtNewEmp_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtAssignedEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAssignedEmp.KeyDown
'        Try
'            Select Case e.KeyCode
'                Case Windows.Forms.Keys.Down
'                    If dgvAssigned.Rows.Count > 0 Then
'                        If dgvAssigned.SelectedRows(0).Index = dgvAssigned.Rows(dgvAssigned.RowCount - 1).Index Then Exit Sub
'                        dgvAssigned.Rows(dgvAssigned.SelectedRows(0).Index + 1).Selected = True
'                    End If
'                Case Windows.Forms.Keys.Up
'                    If dgvAssigned.Rows.Count > 0 Then
'                        If dgvAssigned.SelectedRows(0).Index = 0 Then Exit Sub
'                        dgvAssigned.Rows(dgvAssigned.SelectedRows(0).Index - 1).Selected = True
'                    End If
'            End Select
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtAssignedEmp_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtAssignedEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAssignedEmp.TextChanged
'        Dim strSearch As String = ""
'        Try
'            If txtAssignedEmp.Text.Trim.Length > 0 Then
'                strSearch = dgcolhAName.DataPropertyName & " LIKE '%" & txtAssignedEmp.Text & "%' OR " & _
'                            dgcolhACode.DataPropertyName & " LIKE '%" & txtAssignedEmp.Text & "%'"
'            End If

'            dgAssignedEmp.RowFilter = strSearch

'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtNewEmp_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFrom.SelectedIndexChanged
'        Try
'            Me.Cursor = Cursors.WaitCursor

'            Dim dsCombo As New DataSet
'            Dim dtTable As DataTable
'            If radAssessor.Checked = True Then
'                dsCombo = objAssessor.GetList("List", False, True)
'            ElseIf radReviewer.Checked = True Then
'                dsCombo = objAssessor.GetList("List", True, True)
'            End If

'            If CInt(cboFrom.SelectedValue) <= 0 Then
'                dtTable = New DataView(dsCombo.Tables(0), "assessormasterunkid IN(" & cboFrom.SelectedValue.ToString & ")", "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtTable = New DataView(dsCombo.Tables(0), "assessormasterunkid NOT IN(" & cboFrom.SelectedValue.ToString & ")", "", DataViewRowState.CurrentRows).ToTable
'            End If


'            With cboTo
'                .ValueMember = "assessormasterunkid"
'                .DisplayMember = "assessorname"
'                .DataSource = dtTable
'                .SelectedValue = 0
'            End With

'            Call Fill_Assessor_Emp()

'            If dtNewEmp IsNot Nothing Then dtNewEmp.Rows.Clear()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboFrom_SelectedIndexChanged", mstrModuleName)
'        Finally
'            Me.Cursor = Cursors.Default
'        End Try
'    End Sub

'    Private Sub radReviewer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radReviewer.CheckedChanged
'        Try
'            If radReviewer.Checked = True Then
'                lblFromValue.Text = Language.getMessage(mstrModuleName, 3, "From Reviewer")
'                lblToValue.Text = Language.getMessage(mstrModuleName, 4, "To Reviewer")
'                Call FillCombo()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "radReviewer_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub radAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAssessor.CheckedChanged
'        Try
'            If radAssessor.Checked = True Then
'                lblFromValue.Text = Language.getMessage(mstrModuleName, 1, "From Assessor")
'                lblToValue.Text = Language.getMessage(mstrModuleName, 2, "To Assessor")
'                Call FillCombo()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "radAssessor_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub cboTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTo.SelectedIndexChanged
'        Try
'            Call Fill_Assigned_Emp()
'            If dtNewEmp IsNot Nothing Then
'                Dim dicDataRow As New Dictionary(Of Integer, DataRow)
'                For i As Integer = 0 To dtAssignedEmp.Rows.Count - 1
'                    Dim dRow() As DataRow = dtNewEmp.Select("employeeunkid = '" & CInt(dtAssignedEmp.Rows(i)("employeeunkid")) & "'")
'                    If dRow.Length > 0 Then
'                        If dicDataRow.ContainsKey(i) = False Then
'                            dicDataRow.Add(i, dRow(0))
'                        End If
'                    End If
'                Next

'                If dicDataRow.Keys.Count > 0 Then
'                    For Each SKey As Integer In dicDataRow.Keys
'                        dtOldEmp.ImportRow(CType(dicDataRow(SKey), DataRow))
'                        dtNewEmp.Rows.Remove(CType(dicDataRow(SKey), DataRow))
'                    Next
'                End If

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboTo_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objchkOldEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkOldEmp.CheckedChanged
'        Try
'            RemoveHandler dgvOldEmp.CellContentClick, AddressOf dgvOldEmp_CellContentClick
'            For Each dr As DataRow In dgOldView.Table.Rows
'                dr.Item("IsCheck") = CBool(objchkOldEmp.CheckState)
'            Next
'            dgOldView.Table.AcceptChanges()
'            AddHandler dgvOldEmp.CellContentClick, AddressOf dgvOldEmp_CellContentClick
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkOldEmp_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub dgvOldEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOldEmp.CellContentClick, dgvOldEmp.CellContentDoubleClick
'        Try
'            RemoveHandler objchkOldEmp.CheckedChanged, AddressOf objchkOldEmp_CheckedChanged
'            If e.ColumnIndex = objdgcolhECheck.Index Then

'                If Me.dgvOldEmp.IsCurrentCellDirty Then
'                    Me.dgvOldEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
'                End If

'                Dim drRow As DataRow() = dgOldView.ToTable.Select("IsCheck=True")

'                If drRow.Length > 0 Then
'                    If dgOldView.ToTable.Rows.Count = drRow.Length Then
'                        objchkOldEmp.CheckState = CheckState.Checked
'                    Else
'                        objchkOldEmp.CheckState = CheckState.Indeterminate
'                    End If
'                Else
'                    objchkOldEmp.CheckState = CheckState.Unchecked
'                End If

'            End If
'            AddHandler objchkOldEmp.CheckedChanged, AddressOf objchkOldEmp_CheckedChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvOldEmp_CellContentClick", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objchkNewEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNewEmp.CheckedChanged
'        Try
'            RemoveHandler dgvNewEmp.CellContentClick, AddressOf dgvNewEmp_CellContentClick
'            For Each dr As DataRow In dgNewView.Table.Rows
'                dr.Item("IsCheck") = CBool(objchkNewEmp.CheckState)
'            Next
'            dgNewView.Table.AcceptChanges()
'            AddHandler dgvNewEmp.CellContentClick, AddressOf dgvNewEmp_CellContentClick
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkNewEmp_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub dgvNewEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNewEmp.CellContentClick, dgvNewEmp.CellDoubleClick
'        Try
'            RemoveHandler objchkNewEmp.CheckedChanged, AddressOf objchkNewEmp_CheckedChanged
'            If e.ColumnIndex = objdgcolhNewChek.Index Then

'                If Me.dgvNewEmp.IsCurrentCellDirty Then
'                    Me.dgvNewEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
'                End If

'                Dim drRow As DataRow() = dgNewView.ToTable.Select("IsCheck=True")

'                If drRow.Length > 0 Then
'                    If dgNewView.ToTable.Rows.Count = drRow.Length Then
'                        objchkNewEmp.CheckState = CheckState.Checked
'                    Else
'                        objchkNewEmp.CheckState = CheckState.Indeterminate
'                    End If
'                Else
'                    objchkNewEmp.CheckState = CheckState.Unchecked
'                End If

'            End If
'            AddHandler objchkNewEmp.CheckedChanged, AddressOf objchkNewEmp_CheckedChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvNewEmp_CellContentClick", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'End Class