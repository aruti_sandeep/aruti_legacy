﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCloseAssessmentPeriod

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCloseAssessmentPeriod"
    Private mdtYearStartDate As DateTime
    Private mdtYearEndDate As DateTime
    Private mdtCurrPeriodStartDate As DateTime
    Private mdtCurrPeriodEndDate As DateTime
    Private mdtNextPeriodStartDate As DateTime
    Private mdtNextPeriodEndDate As DateTime
    Private WithEvents objBackupDatabase As New eZeeDatabase
    Private mDicTransfer As Dictionary(Of Integer, String)
    Private mDicList As Dictionary(Of Integer, String)

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPayYearClose.BackColor = GUI.ColorComp
            cboPayPeriodClose.BackColor = GUI.ColorComp
            cboPayPeriodOpen.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objYear As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            With cboPayYearClose
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Year")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Try
            mDicList = New Dictionary(Of Integer, String)
            mDicList.Add(clsDataTransfer.enCFInformation.CF_FIELD_MAPPING, Language.getMessage(mstrModuleName, 501, "Field Mapping"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_ASSESSMENT_RATIOS, Language.getMessage(mstrModuleName, 502, "Assessment Ratios"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_ASSESSMENT_SCALES, Language.getMessage(mstrModuleName, 503, "Assessment Scales"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_ASSESSMENT_SCALE_MAPPING, Language.getMessage(mstrModuleName, 504, "Assessment Scale Mapping"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_COMPANY_GOALS, Language.getMessage(mstrModuleName, 505, "Company Goals"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_ALLOCATION_GOALS, Language.getMessage(mstrModuleName, 506, "Allocation Goals"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_EMPLOYEE_GOALS, Language.getMessage(mstrModuleName, 507, "Employee Goals"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_COMPETENCIES, Language.getMessage(mstrModuleName, 508, "Competencies"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_ASSIGNED_COMPETENCIES, Language.getMessage(mstrModuleName, 509, "Assigned Competencies"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_COMPUTATION_FORMULAS, Language.getMessage(mstrModuleName, 510, "Computation Formulas"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_CUSTOM_ITEMS, Language.getMessage(mstrModuleName, 511, "Custom Items"))
            mDicList.Add(clsDataTransfer.enCFInformation.CF_PLANNED_CUSTOM_ITEMS, Language.getMessage(mstrModuleName, 512, "Planned Custom Items"))

            lvTranferList.Items.Clear()

            For Each iKey As Integer In mDicList.Keys
                Dim lvItem As New ListViewItem()

                lvItem.Text = mDicList(iKey).ToString()
                lvItem.Tag = iKey.ToString()
                lvItem.Checked = True

                lvTranferList.Items.Add(lvItem)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPayYearClose.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Year. Year is mandatory information."), enMsgBoxStyle.Information)
                cboPayYearClose.Focus()
                Return False
            End If

            If CInt(cboPayPeriodClose.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Period to close. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriodClose.Focus()
                Return False
            End If

            If cboPayPeriodOpen.Visible = True AndAlso CInt(cboPayPeriodOpen.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period to be open. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriodOpen.Focus()
                Return False
            End If

            If chkNoDataTransfer.Checked = False Then
                If lvTranferList.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select atleast one information in order to carry forward data and close period."), enMsgBoxStyle.Information)
                    lvTranferList.Focus()
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Close_Period(ByVal intCurrentPid As Integer, ByVal objPeriod As clscommom_period_Tran) As Boolean
        Dim blnFlag As Boolean = False
        Try
            RemoveHandler frmNewMDI.tmrReminder.Tick, AddressOf frmNewMDI.tmrReminder_Tick 'S.SANDEEP [17-Jan-2018] -- START {#0001881} -- END

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to close the current period?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Return False
            End If

            Dim objAppSettings As New clsApplicationSettings
            Dim strFinalPath As String = String.Empty
            Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Assessment Period Backup"
            If objAppSettings._IsClient = 0 Then
                If Not System.IO.Directory.Exists(StrFilePath) Then
                    System.IO.Directory.CreateDirectory(StrFilePath)
                End If
                objAppSettings = Nothing

                For i As Integer = 0 To 2
                    If i = 0 Then
                        objlblValue.Text = Language.getMessage(mstrModuleName, 12, "Taking Database backup :") & " " & FinancialYear._Object._ConfigDatabaseName
                        Application.DoEvents()
                        eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
                        If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName) Then
                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
                        End If
                        strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
                    ElseIf i = 2 Then 'this database has to be always to be taken at last and i number has to be higher in order
                        objlblValue.Text = Language.getMessage(mstrModuleName, 12, "Taking Database backup :") & " " & FinancialYear._Object._DatabaseName
                        Application.DoEvents()
                        eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                        If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._DatabaseName) Then
                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
                        End If
                        strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
                    ElseIf i = 1 Then
                        Dim objBkUp As New clsBackup
                        If objBkUp.Is_DatabasePresent("arutiimages") Then
                            objlblValue.Text = Language.getMessage(mstrModuleName, 12, "Taking Database backup :") & " " & "arutiimages "
                            Application.DoEvents()
                            eZeeDatabase.change_database("arutiimages")
                            If Not System.IO.Directory.Exists(StrFilePath & "\" & "arutiimages") Then
                                System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
                            End If
                            strFinalPath = StrFilePath & "\" & "arutiimages"
                        Else
                            strFinalPath = ""
                        End If
                        objBkUp = Nothing
                    End If

                    If strFinalPath <> "" Then

                        strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)

                        Dim objBackup As New clsBackup
                        If System.IO.File.Exists(strFinalPath) Then

                            objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
                            objBackup._Backup_Path = strFinalPath
                            If i = 0 Then
                                objBackup._Companyunkid = -1
                                objBackup._Yearunkid = -1
                                objBackup._Isconfiguration = True
                            ElseIf i = 1 Then
                                objBackup._Companyunkid = Company._Object._Companyunkid
                                objBackup._Yearunkid = FinancialYear._Object._YearUnkid
                                objBackup._Isconfiguration = False
                            End If
                            objBackup._Userunkid = User._Object._Userunkid

                            Call objBackup.Insert()
                        End If

                    End If
                Next
            Else
                objAppSettings = Nothing
            End If

            Cursor.Current = Cursors.WaitCursor

            If chkNoDataTransfer.Checked = False Then
                Dim objTransfer As New clsDataTransfer
                If objTransfer.Perform_Transfer_Process(CInt(cboPayPeriodClose.SelectedValue), CInt(cboPayPeriodOpen.SelectedValue), mDicTransfer, User._Object._Userunkid, objlblValue) = False Then Exit Function
            End If

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intCurrentPid
            objPeriod._IsAssessmentPeriod = True
            objPeriod._Statusid = enStatusType.Close
            objPeriod._Closeuserunkid = User._Object._Userunkid
            objPeriod._Prescribed_interest_rate = 0

            blnFlag = objPeriod.Update()
            If blnFlag Then
                Call FillCombo()
            End If
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Close_Period", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
            objlblValue.Text = ""
            AddHandler frmNewMDI.tmrReminder.Tick, AddressOf frmNewMDI.tmrReminder_Tick 'S.SANDEEP [17-Jan-2018] -- START {#0001881} -- END
        End Try
    End Function

#End Region

#Region " Form's Evnets "

    Private Sub frmCloseAssessmentPeriod_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCloseAssessmentPeriod_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCloseAssessmentPeriod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            btnClosePeriod.Enabled = User._Object.Privilege._AllowtoCloseAssessmentPeriod
            Call OtherSettings()
            Call SetColor()
            Call FillCombo()
            Call FillList()
            lvUnAssessedEmpList.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCloseAssessmentPeriod_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClosePeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClosePeriod.Click
        Me.Enabled = False
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet : Dim dtTable As DataTable
        Try
            'RemoveHandler frmNewMDI.tmrReminder.Tick, AddressOf frmNewMDI.tmrReminder_Tick 'S.SANDEEP [17-Jan-2018] -- START {#0001881} -- END
            If IsValid() = False Then Exit Sub

            mDicTransfer = New Dictionary(Of Integer, String)
            For Each lvItem As ListViewItem In lvTranferList.CheckedItems
                mDicTransfer.Add(CInt(lvItem.Tag), lvItem.Text)
            Next

            dsList = objPeriod.GetList("Period", enModuleReference.Assessment, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open, , True)
            dtTable = New DataView(dsList.Tables("Period"), "end_date < '" & eZeeDate.convertDate(mdtCurrPeriodEndDate) & "'", "end_date", DataViewRowState.CurrentRows).ToTable

            If dtTable.Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Previous Period [") & dtTable.Rows(0).Item("period_name").ToString & Language.getMessage(mstrModuleName, 6, "] is still Open. Please Close Previous Period first."), enMsgBoxStyle.Information)
                cboPayPeriodClose.Focus()
                Exit Try
            End If

            If cboPayPeriodOpen.Visible = False AndAlso CInt(cboPayPeriodClose.SelectedValue) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, there is no period found in") & " " & FinancialYear._Object._FinancialYear_Name & " " & Language.getMessage(mstrModuleName, 10, "year to open. Please create new period to close this and open new one."), enMsgBoxStyle.Information)
                cboPayPeriodOpen.Focus()
                Exit Try
            End If

            If lvUnAssessedEmpList.Items.Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There are some unassessed employees, for the current period. If you close the current period you can not make or change any transaction regarding employee's General/BSC Evaluation for current period." & vbCrLf & _
                                              "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            If Close_Period(CInt(cboPayPeriodClose.SelectedValue), objPeriod) = False Then
                If objPeriod._Message <> "" Then
                    eZeeMsgBox.Show(objPeriod._Message, enMsgBoxStyle.Information)
                End If
                Exit Sub
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Period closed successfully."), enMsgBoxStyle.Information)
            End If
            
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClosePeriod_Click", mstrModuleName)
        Finally
            Me.Enabled = True
            'AddHandler frmNewMDI.tmrReminder.Tick, AddressOf frmNewMDI.tmrReminder_Tick 'S.SANDEEP [17-Jan-2018] -- START {#0001881} -- END
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub chkNoDataTransfer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoDataTransfer.CheckedChanged
        Try
            If chkNoDataTransfer.Checked = True Then
                For Each lvItem As ListViewItem In lvTranferList.Items
                    lvItem.Checked = False
                Next
                lvTranferList.Enabled = False
            Else
                lvTranferList.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNoDataTransfer_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboPayYearClose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYearClose.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Dim dtTable As DataTable

        Try

            mdtYearStartDate = FinancialYear._Object._Database_Start_Date
            mdtYearEndDate = FinancialYear._Object._Database_End_Date
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboPayYearClose.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            dtTable = New DataView(dsCombo.Tables("Period"), "end_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable

            With cboPayPeriodClose
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYearClose_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dtTable = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriodClose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriodClose.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Dim objAssess As New clsassess_analysis_master
        Dim lvItem As ListViewItem
        Dim dTable As DataTable = Nothing
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodClose.SelectedValue)
            mdtCurrPeriodStartDate = objPeriod._Start_Date
            mdtCurrPeriodEndDate = objPeriod._End_Date

            dsList = objPeriod.GetNextPeriod("NextPeriod", enModuleReference.Assessment, CInt(cboPayPeriodClose.SelectedValue), CInt(cboPayYearClose.SelectedValue))

            If dsList.Tables(0).Rows.Count <= 0 Then
                If CInt(cboPayPeriodClose.SelectedValue) > 0 Then
                    dsList = objPeriod.GetList("NextPeriod", enModuleReference.Assessment, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open, False, True)
                    Dim dt As DataTable = New DataView(dsList.Tables(0).Copy, "end_date > " & eZeeDate.convertDate(objPeriod._End_Date) & " AND periodunkid <> " & CInt(cboPayPeriodClose.SelectedValue), "end_date ASC", DataViewRowState.CurrentRows).ToTable
                    dsList.Tables(0).Rows.Clear()
                    If dt.Rows.Count > 0 Then
                        dsList.Tables("NextPeriod").ImportRow(dt.Rows(0))
                    End If
                End If
            End If

            With cboPayPeriodOpen
                .ValueMember = "periodunkid"
                .DisplayMember = "period_name"
                .DataSource = dsList.Tables("NextPeriod")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            If dsList.Tables(0).Rows.Count <= 0 Then
                lblPayPeriodOpen.Visible = False
                cboPayPeriodOpen.Visible = False
            Else
                lblPayPeriodOpen.Visible = True
                cboPayPeriodOpen.Visible = True
            End If

            lvUnAssessedEmpList.Items.Clear()
            If CInt(cboPayPeriodClose.SelectedValue) <= 0 Then Exit Try
            dsList = objAssess.Get_Unassessed_Emp(CInt(cboPayPeriodClose.SelectedValue))

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            '<TODO : MAKE CHANGES FOR BSC WHEN LIC. CHANGES MERGED>
            If ConfigParameter._Object._IsArutiDemo = False Then
                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then 'Anjan (09 Mar 2012)-Start
                    dTable = New DataView(dsList.Tables(0), "Id = 1", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

            If dTable Is Nothing Then dTable = dsList.Tables(0)

            For Each dtRow As DataRow In dTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("Assess_Type").ToString
                lvItem.SubItems.Add(dtRow.Item("Code").ToString)
                lvItem.SubItems.Add(dtRow.Item("Ename").ToString)

                lvUnAssessedEmpList.Items.Add(lvItem)
            Next
            If lvUnAssessedEmpList.Items.Count > 5 Then
                colhEmployee.Width = 172 - 20
            Else
                colhEmployee.Width = 172
            End If

            lvUnAssessedEmpList.GroupingColumn = objcolhGrp
            lvUnAssessedEmpList.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriodClose_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
            objAssess = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriodOpen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriodOpen.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodOpen.SelectedValue)
            mdtNextPeriodStartDate = objPeriod._Start_Date
            mdtNextPeriodEndDate = objPeriod._End_Date
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriodOpen_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbClosePeriodInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbClosePeriodInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClosePeriod.GradientBackColor = GUI._ButttonBackColor
            Me.btnClosePeriod.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbClosePeriodInfo.Text = Language._Object.getCaption(Me.gbClosePeriodInfo.Name, Me.gbClosePeriodInfo.Text)
            Me.lblNoteNo2.Text = Language._Object.getCaption(Me.lblNoteNo2.Name, Me.lblNoteNo2.Text)
            Me.lblNote2.Text = Language._Object.getCaption(Me.lblNote2.Name, Me.lblNote2.Text)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
            Me.lnEmployeeList.Text = Language._Object.getCaption(Me.lnEmployeeList.Name, Me.lnEmployeeList.Text)
            Me.lblPayPeriodOpen.Text = Language._Object.getCaption(Me.lblPayPeriodOpen.Name, Me.lblPayPeriodOpen.Text)
            Me.lblPayYearClose.Text = Language._Object.getCaption(Me.lblPayYearClose.Name, Me.lblPayYearClose.Text)
            Me.lblPayPeriodClose.Text = Language._Object.getCaption(Me.lblPayPeriodClose.Name, Me.lblPayPeriodClose.Text)
            Me.btnClosePeriod.Text = Language._Object.getCaption(Me.btnClosePeriod.Name, Me.btnClosePeriod.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.pbProgress.Text = Language._Object.getCaption(Me.pbProgress.Name, Me.pbProgress.Text)
            Me.lnlNoteNo3.Text = Language._Object.getCaption(Me.lnlNoteNo3.Name, Me.lnlNoteNo3.Text)
            Me.lnlNote3.Text = Language._Object.getCaption(Me.lnlNote3.Name, Me.lnlNote3.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Year. Year is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Please select Pay Period to close. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Please select Pay Period to be open. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Previous Period [")
            Language.setMessage(mstrModuleName, 5, "There are some unassessed employees, for the current period. If you close the current period you can not make or change any transaction regarding employee's General/BSC Evaluation for current period." & vbCrLf & _
                                                       "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 6, "] is still Open. Please Close Previous Period first.")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to close the current period?")
            Language.setMessage(mstrModuleName, 8, "Period closed successfully.")
            Language.setMessage(mstrModuleName, 9, "Sorry, there is no period found in")
            Language.setMessage(mstrModuleName, 10, "year to open. Please create new period to close this and open new one.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

'S.SANDEEP [18-AUG-2017] | ************************** CLOSE PERIOD DESIGNED ******************************> START
'Public Class frmCloseAssessmentPeriod

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmCloseAssessmentPeriod"
'    Private mdtYearStartDate As DateTime
'    Private mdtYearEndDate As DateTime
'    Private mdtCurrPeriodStartDate As DateTime
'    Private mdtCurrPeriodEndDate As DateTime
'    Private mdtNextPeriodStartDate As DateTime
'    Private mdtNextPeriodEndDate As DateTime
'    Private WithEvents objBackupDatabase As New eZeeDatabase

'#End Region

'#Region " Private Methods "

'    Private Sub SetColor()
'        Try
'            cboPayYearClose.BackColor = GUI.ColorComp
'            cboPayPeriodClose.BackColor = GUI.ColorComp
'            cboPayPeriodOpen.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim objYear As New clsMasterData
'        Dim dsCombo As DataSet
'        Try

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombo = objYear.getComboListPAYYEAR("Year", True, , , , True)
'            dsCombo = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboPayYearClose
'                .ValueMember = "Id"
'                .DisplayMember = "name"
'                .DataSource = dsCombo.Tables("Year")
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetVisibility()
'        Try

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try
'    End Sub

'    Private Function IsValid() As Boolean
'        Try
'            If CInt(cboPayYearClose.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Year. Year is mandatory information."), enMsgBoxStyle.Information)
'                cboPayYearClose.Focus()
'                Return False
'            End If

'            If CInt(cboPayPeriodClose.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Period to close. Pay Period is mandatory information."), enMsgBoxStyle.Information)
'                cboPayPeriodClose.Focus()
'                Return False
'            End If

'            If cboPayPeriodOpen.Visible = True AndAlso CInt(cboPayPeriodOpen.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period to be open. Pay Period is mandatory information."), enMsgBoxStyle.Information)
'                cboPayPeriodOpen.Focus()
'                Return False
'            End If
'            Return True
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
'        End Try
'    End Function

'    Private Function Close_Period(ByVal intCurrentPid As Integer, ByVal objPeriod As clscommom_period_Tran) As Boolean
'        Dim blnFlag As Boolean = False
'        Try
'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to close the current period?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
'                Return False
'            End If

'            Dim objAppSettings As New clsApplicationSettings
'            Dim strFinalPath As String = String.Empty
'            Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Assessment Period Backup"
'            If objAppSettings._IsClient = 0 Then
'                If Not System.IO.Directory.Exists(StrFilePath) Then
'                    System.IO.Directory.CreateDirectory(StrFilePath)
'                End If
'                objAppSettings = Nothing

'                'Dim objGSettings As New clsGeneralSettings
'                'objGSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
'                'objBackupDatabase.ServerName = objGSettings._ServerName
'                'objGSettings = Nothing

'                For i As Integer = 0 To 2

'                    'Shani (24-May-2016) -- Start
'                    'Application.DoEvents()
'                    'Shani (24-May-2016) -- End
'                    If i = 0 Then
'                        eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
'                        If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName) Then
'                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
'                        End If
'                        strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
'                    ElseIf i = 2 Then 'this database has to be always to be taken at last and i number has to be higher in order
'                        eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
'                        If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._DatabaseName) Then
'                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
'                        End If
'                        strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
'                    ElseIf i = 1 Then
'                        Dim objBkUp As New clsBackup
'                        If objBkUp.Is_DatabasePresent("arutiimages") Then
'                            eZeeDatabase.change_database("arutiimages")
'                            If Not System.IO.Directory.Exists(StrFilePath & "\" & "arutiimages") Then
'                                System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
'                            End If
'                            strFinalPath = StrFilePath & "\" & "arutiimages"
'                        Else
'                            strFinalPath = ""
'                        End If
'                        objBkUp = Nothing
'                    End If

'                    If strFinalPath <> "" Then

'                        strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)

'                        Dim objBackup As New clsBackup
'                        If System.IO.File.Exists(strFinalPath) Then

'                            objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
'                            objBackup._Backup_Path = strFinalPath
'                            If i = 0 Then
'                                objBackup._Companyunkid = -1
'                                objBackup._Yearunkid = -1
'                                objBackup._Isconfiguration = True
'                            ElseIf i = 1 Then
'                                objBackup._Companyunkid = Company._Object._Companyunkid
'                                objBackup._Yearunkid = FinancialYear._Object._YearUnkid
'                                objBackup._Isconfiguration = False
'                            End If
'                            objBackup._Userunkid = User._Object._Userunkid

'                            Call objBackup.Insert()
'                        End If

'                    End If
'                Next
'            Else
'                objAppSettings = Nothing
'            End If

'            Cursor.Current = Cursors.WaitCursor

'            Dim objTransfer As New clsDataTransfer
'            objTransfer._OldPeriodId = CInt(cboPayPeriodClose.SelectedValue)
'            objTransfer._NewPeriodId = CInt(cboPayPeriodOpen.SelectedValue)
'            objTransfer._xPB = pbProgress

'            'Shani (05-Sep-2016) -- Start
'            'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
'            'If objTransfer.Perform_Transfer_Process() = False Then Exit Function
'            If objTransfer.Perform_Transfer_Process(User._Object._Userunkid) = False Then Exit Function
'            'Shani (05-Sep-2016) -- End


'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = intCurrentPid
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intCurrentPid
'            'Sohail (21 Aug 2015) -- End

'            'S.SANDEEP [01-FEB-2017] -- START
'            'ISSUE/ENHANCEMENT : CCK !! HAVING SAME DATES FOR DIFFERENT PERIODS
'            objPeriod._IsAssessmentPeriod = True
'            'S.SANDEEP [01-FEB-2017] -- END

'            objPeriod._Statusid = enStatusType.Close
'            objPeriod._Closeuserunkid = User._Object._Userunkid
'            objPeriod._Prescribed_interest_rate = 0 'Sohail (30 Oct 2015)

'            blnFlag = objPeriod.Update()
'            If blnFlag Then
'                Call FillCombo()
'            End If
'            Return blnFlag
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Close_Period", mstrModuleName)
'        Finally
'            Cursor.Current = Cursors.Default
'        End Try
'    End Function

'#End Region

'#Region " Form's Evnets "

'    Private Sub frmCloseAssessmentPeriod_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            If e.KeyCode = Keys.Return Then
'                SendKeys.Send("{TAB}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmCloseAssessmentPeriod_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmCloseAssessmentPeriod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        Try
'            Call Set_Logo(Me, gApplicationType)

'            Language.setLanguage(Me.Name)
'            Call OtherSettings()

'            Call SetVisibility()
'            Call SetColor()
'            Call FillCombo()
'            lvUnAssessedEmpList.Items.Clear()
'            'S.SANDEEP [28 MAY 2015] -- START
'            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'            btnClosePeriod.Enabled = User._Object.Privilege._AllowtoCloseAssessmentPeriod
'            'S.SANDEEP [28 MAY 2015] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmCloseAssessmentPeriod_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClosePeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClosePeriod.Click
'        'S.SANDEEP [21 JAN 2015] -- START
'        Me.Enabled = False
'        'S.SANDEEP [21 JAN 2015] -- END

'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsList As DataSet : Dim dtTable As DataTable
'        Try
'            RemoveHandler frmNewMDI.tmrReminder.Tick, AddressOf frmNewMDI.tmrReminder_Tick
'            'S.SANDEEP [ 09 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If IsValid() = False Then Exit Sub
'            'S.SANDEEP [ 09 AUG 2013 ] -- END

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsList = objPeriod.GetList("Period", enModuleReference.Assessment, True, enStatusType.Open, , True)
'            dsList = objPeriod.GetList("Period", enModuleReference.Assessment, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open, , True)
'            'Sohail (21 Aug 2015) -- End
'            dtTable = New DataView(dsList.Tables("Period"), "end_date < '" & eZeeDate.convertDate(mdtCurrPeriodEndDate) & "'", "end_date", DataViewRowState.CurrentRows).ToTable

'            If dtTable.Rows.Count > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Previous Period [") & dtTable.Rows(0).Item("period_name").ToString & Language.getMessage(mstrModuleName, 6, "] is still Open. Please Close Previous Period first."), enMsgBoxStyle.Information)
'                cboPayPeriodClose.Focus()
'                Exit Try
'            End If

'            'Shani(06-Feb-2016) -- Start
'            'PA Changes Given By Glory for CCBRT
'            If cboPayPeriodOpen.Visible = False AndAlso CInt(cboPayPeriodClose.SelectedValue) > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, there is no period found in") & " " & FinancialYear._Object._FinancialYear_Name & " " & Language.getMessage(mstrModuleName, 10, "year to open. Please create new period to close this and open new one."), enMsgBoxStyle.Information)
'                cboPayPeriodOpen.Focus()
'                Exit Try
'            End If
'            'Shani(06-Feb-2016) -- End


'            'S.SANDEEP [21 MAY 2015] -- START
'            'If lvUnAssessedEmpList.Items.Count > 0 Then
'            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There are some unassessed employee(s), for the current period. If you close the current period you can not make or change any transaction regarding employee's General/BSC Evaluation for current period." & vbCrLf & _
'            '                                  "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'            '        Dim objTransfer As New clsDataTransfer
'            '        objTransfer._OldPeriodId = CInt(cboPayPeriodClose.SelectedValue)
'            '        objTransfer._NewPeriodId = CInt(cboPayPeriodOpen.SelectedValue)
'            '        objTransfer._xPB = pbProgress
'            '        If objTransfer.Perform_Transfer_Process() = False Then Exit Sub

'            '        If Close_Period(CInt(cboPayPeriodClose.SelectedValue), objPeriod) = False AndAlso objPeriod._Message <> "" Then
'            '            eZeeMsgBox.Show(objPeriod._Message, enMsgBoxStyle.Information)
'            '            Exit Sub
'            '        End If
'            '    End If
'            'Else
'            '    If Close_Period(CInt(cboPayPeriodClose.SelectedValue), objPeriod) = False AndAlso objPeriod._Message <> "" Then
'            '        eZeeMsgBox.Show(objPeriod._Message, enMsgBoxStyle.Information)
'            '        Exit Sub
'            '    End If
'            'End If

'            If lvUnAssessedEmpList.Items.Count > 0 Then
'                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There are some unassessed employees, for the current period. If you close the current period you can not make or change any transaction regarding employee's General/BSC Evaluation for current period." & vbCrLf & _
'                                              "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
'            End If

'            'Shani(26-APR-2016) -- Start
'            'Enhancement : PA Audit Trails
'            'If Close_Period(CInt(cboPayPeriodClose.SelectedValue), objPeriod) = False AndAlso objPeriod._Message <> "" Then
'            '    eZeeMsgBox.Show(objPeriod._Message, enMsgBoxStyle.Information)
'            '    Exit Sub
'            'Else
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Period closed successfully."), enMsgBoxStyle.Information)
'            'End If
'            If Close_Period(CInt(cboPayPeriodClose.SelectedValue), objPeriod) = False Then
'                If objPeriod._Message <> "" Then
'                    eZeeMsgBox.Show(objPeriod._Message, enMsgBoxStyle.Information)
'                End If
'                Exit Sub
'            Else
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Period closed successfully."), enMsgBoxStyle.Information)
'            End If
'            'Shani(26-APR-2016) -- End

'            'S.SANDEEP [21 MAY 2015] -- END
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnClosePeriod_Click", mstrModuleName)
'            'S.SANDEEP [21 JAN 2015] -- START
'        Finally
'            Me.Enabled = True
'            AddHandler frmNewMDI.tmrReminder.Tick, AddressOf frmNewMDI.tmrReminder_Tick
'            'S.SANDEEP [21 JAN 2015] -- END
'        End Try
'    End Sub

'#End Region

'#Region " ComboBox's Events "

'    Private Sub cboPayYearClose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYearClose.SelectedIndexChanged
'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsCombo As DataSet
'        Dim dtTable As DataTable

'        Try

'            mdtYearStartDate = FinancialYear._Object._Database_Start_Date
'            mdtYearEndDate = FinancialYear._Object._Database_End_Date

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboPayYearClose.SelectedValue), "Period", True, enStatusType.Open)
'            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboPayYearClose.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
'            'Sohail (21 Aug 2015) -- End
'            dtTable = New DataView(dsCombo.Tables("Period"), "end_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable

'            With cboPayPeriodClose
'                .ValueMember = "periodunkid"
'                .DisplayMember = "Name"
'                .DataSource = dtTable
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPayYearClose_SelectedIndexChanged", mstrModuleName)
'        Finally
'            objPeriod = Nothing
'            dtTable = Nothing
'            dsCombo = Nothing
'        End Try
'    End Sub

'    Private Sub cboPayPeriodClose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriodClose.SelectedIndexChanged
'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsList As DataSet = Nothing
'        Dim objAssess As New clsassess_analysis_master
'        Dim lvItem As ListViewItem
'        Dim dTable As DataTable = Nothing
'        Try
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = CInt(cboPayPeriodClose.SelectedValue)
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodClose.SelectedValue)
'            'Sohail (21 Aug 2015) -- End
'            mdtCurrPeriodStartDate = objPeriod._Start_Date
'            mdtCurrPeriodEndDate = objPeriod._End_Date

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsList = objPeriod.GetNextPeriod("NextPeriod", enModuleReference.Assessment, CInt(cboPayPeriodClose.SelectedValue), CInt(cboPayYearClose.SelectedValue))
'            dsList = objPeriod.GetNextPeriod("NextPeriod", enModuleReference.Assessment, CInt(cboPayPeriodClose.SelectedValue), CInt(cboPayYearClose.SelectedValue))
'            'Sohail (21 Aug 2015) -- End


'            'Shani(06-Feb-2016) -- Start
'            'PA Changes Given By CCBRT
'            If dsList.Tables(0).Rows.Count <= 0 Then
'                If CInt(cboPayPeriodClose.SelectedValue) > 0 Then
'                    dsList = objPeriod.GetList("NextPeriod", enModuleReference.Assessment, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open, False, True)
'                    Dim dt As DataTable = New DataView(dsList.Tables(0).Copy, "end_date > " & eZeeDate.convertDate(objPeriod._End_Date) & " AND periodunkid <> " & CInt(cboPayPeriodClose.SelectedValue), "end_date ASC", DataViewRowState.CurrentRows).ToTable
'                    dsList.Tables(0).Rows.Clear()
'                    If dt.Rows.Count > 0 Then
'                        dsList.Tables("NextPeriod").ImportRow(dt.Rows(0))
'                    End If
'                End If
'            End If
'            'Shani(06-Feb-2016) -- End


'            With cboPayPeriodOpen
'                .ValueMember = "periodunkid"
'                .DisplayMember = "period_name"
'                .DataSource = dsList.Tables("NextPeriod")
'                If .Items.Count > 0 Then .SelectedIndex = 0
'            End With

'            'S.SANDEEP [ 30 MAY 2014 ] -- START
'            'If mdtCurrPeriodEndDate = FinancialYear._Object._Database_End_Date Then
'            '    lblPayPeriodOpen.Visible = False
'            '    cboPayPeriodOpen.Visible = False
'            'End If
'            If dsList.Tables(0).Rows.Count <= 0 Then
'                lblPayPeriodOpen.Visible = False
'                cboPayPeriodOpen.Visible = False
'            Else
'                lblPayPeriodOpen.Visible = True
'                cboPayPeriodOpen.Visible = True
'            End If
'            'S.SANDEEP [ 30 MAY 2014 ] -- END

'            lvUnAssessedEmpList.Items.Clear()
'            If CInt(cboPayPeriodClose.SelectedValue) <= 0 Then Exit Try
'            dsList = objAssess.Get_Unassessed_Emp(CInt(cboPayPeriodClose.SelectedValue))

'            'S.SANDEEP [ 15 April 2013 ] -- START
'            'ENHANCEMENT : LICENSE CHANGES
'            '<TODO : MAKE CHANGES FOR BSC WHEN LIC. CHANGES MERGED>
'            If ConfigParameter._Object._IsArutiDemo = False Then
'                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then 'Anjan (09 Mar 2012)-Start
'                    dTable = New DataView(dsList.Tables(0), "Id = 1", "", DataViewRowState.CurrentRows).ToTable
'                End If
'            End If
'            'S.SANDEEP [ 15 April 2013 ] -- END

'            If dTable Is Nothing Then dTable = dsList.Tables(0)

'            For Each dtRow As DataRow In dTable.Rows
'                lvItem = New ListViewItem
'                lvItem.Text = dtRow.Item("Assess_Type").ToString
'                lvItem.SubItems.Add(dtRow.Item("Code").ToString)
'                lvItem.SubItems.Add(dtRow.Item("Ename").ToString)

'                lvUnAssessedEmpList.Items.Add(lvItem)
'            Next
'            If lvUnAssessedEmpList.Items.Count > 5 Then
'                colhEmployee.Width = 172 - 20
'            Else
'                colhEmployee.Width = 172
'            End If

'            lvUnAssessedEmpList.GroupingColumn = objcolhGrp
'            lvUnAssessedEmpList.DisplayGroups(True)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPayPeriodClose_SelectedIndexChanged", mstrModuleName)
'        Finally
'            objPeriod = Nothing
'            dsList = Nothing
'            objAssess = Nothing
'        End Try
'    End Sub

'    Private Sub cboPayPeriodOpen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriodOpen.SelectedIndexChanged
'        Dim objPeriod As New clscommom_period_Tran
'        Try
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = CInt(cboPayPeriodOpen.SelectedValue)
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodOpen.SelectedValue)
'            'Sohail (21 Aug 2015) -- End
'            mdtNextPeriodStartDate = objPeriod._Start_Date
'            mdtNextPeriodEndDate = objPeriod._End_Date
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPayPeriodOpen_SelectedIndexChanged", mstrModuleName)
'        Finally
'            objPeriod = Nothing
'        End Try
'    End Sub

'#End Region

'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Private Sub OtherSettings()
'        Try
'            Me.SuspendLayout()

'            Call SetLanguage()

'            Me.gbClosePeriodInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
'            Me.gbClosePeriodInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


'            Me.btnClosePeriod.GradientBackColor = GUI._ButttonBackColor
'            Me.btnClosePeriod.GradientForeColor = GUI._ButttonFontColor

'            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
'            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


'            Me.ResumeLayout()
'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetLanguage()
'        Try
'            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'            Me.gbClosePeriodInfo.Text = Language._Object.getCaption(Me.gbClosePeriodInfo.Name, Me.gbClosePeriodInfo.Text)
'            Me.lblNoteNo2.Text = Language._Object.getCaption(Me.lblNoteNo2.Name, Me.lblNoteNo2.Text)
'            Me.lblNote2.Text = Language._Object.getCaption(Me.lblNote2.Name, Me.lblNote2.Text)
'            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
'            Me.lnEmployeeList.Text = Language._Object.getCaption(Me.lnEmployeeList.Name, Me.lnEmployeeList.Text)
'            Me.lblPayPeriodOpen.Text = Language._Object.getCaption(Me.lblPayPeriodOpen.Name, Me.lblPayPeriodOpen.Text)
'            Me.lblPayYearClose.Text = Language._Object.getCaption(Me.lblPayYearClose.Name, Me.lblPayYearClose.Text)
'            Me.lblPayPeriodClose.Text = Language._Object.getCaption(Me.lblPayPeriodClose.Name, Me.lblPayPeriodClose.Text)
'            Me.btnClosePeriod.Text = Language._Object.getCaption(Me.btnClosePeriod.Name, Me.btnClosePeriod.Text)
'            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
'            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
'            Me.pbProgress.Text = Language._Object.getCaption(Me.pbProgress.Name, Me.pbProgress.Text)
'            Me.lnlNoteNo3.Text = Language._Object.getCaption(Me.lnlNoteNo3.Name, Me.lnlNoteNo3.Text)
'            Me.lnlNote3.Text = Language._Object.getCaption(Me.lnlNote3.Name, Me.lnlNote3.Text)

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Please select Year. Year is mandatory information.")
'            Language.setMessage(mstrModuleName, 2, "Please select Pay Period to close. Pay Period is mandatory information.")
'            Language.setMessage(mstrModuleName, 3, "Please select Pay Period to be open. Pay Period is mandatory information.")
'            Language.setMessage(mstrModuleName, 4, "Sorry, Previous Period [")
'            Language.setMessage(mstrModuleName, 5, "There are some unassessed employees, for the current period. If you close the current period you can not make or change any transaction regarding employee's General/BSC Evaluation for current period." & vbCrLf & _
'                                                       "Do you wish to continue?")
'            Language.setMessage(mstrModuleName, 6, "] is still Open. Please Close Previous Period first.")
'            Language.setMessage(mstrModuleName, 7, "Are you sure you want to close the current period?")
'            Language.setMessage(mstrModuleName, 8, "Period closed successfully.")
'            Language.setMessage(mstrModuleName, 9, "Sorry, there is no period found in")
'            Language.setMessage(mstrModuleName, 10, "year to open. Please create new period to close this and open new one.")

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>

'End Class