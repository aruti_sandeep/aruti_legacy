﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessorReviewerAddEdit

#Region " Private Variables "

    Private mstrModuleName As String = ""
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    Private mdtEmployee As DataTable
    Private mblnCancel As Boolean = True
    Private mintAssessorMasterId As Integer = -1
    Private mblnIsAssessorMode As Boolean = False
    Private objAssessorMaster As New clsAssessor
    Private objAssessorTran As New clsAssessor_tran
    Private dtEmpView As DataView
    Private mdtAssessor As DataTable
    Private mdtReviewer As DataTable
    Private dtAseView As DataView
    Private dtRevView As DataView

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal isAssessor As Boolean, ByVal iAssessorId As Integer) As Boolean
        Try
            mintAssessorMasterId = iAssessorId
            menAction = eAction
            mblnIsAssessorMode = isAssessor

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'If isAssessor = True Then
            '    mstrModuleName = "frmAssessor_AddEdit"
            '    Me.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Assessor")
            '    gbInfo.Text = Language.getMessage(mstrModuleName, 2, "Assessor Information")
            '    lblApproverName.Text = Language.getMessage(mstrModuleName, 3, "Assessor")
            'Else
            '    mstrModuleName = "frmReviewer_AddEdit"
            '    Me.Text = Language.getMessage(mstrModuleName, 4, "Add/Edit Reviewer")
            '    gbInfo.Text = Language.getMessage(mstrModuleName, 5, "Reviewer Information")
            '    lblApproverName.Text = Language.getMessage(mstrModuleName, 6, "Reviewer")
            'End If
            'Shani(01-MAR-2016)-- End
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objUsr As New clsUserAddEdit
        Dim objPswd As New clsPassowdOptions
        Dim objEmp As New clsEmployee_Master
        Try
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'If objPswd._IsEmployeeAsUser Then
            '    If mblnIsAssessorMode = True Then
            '        dsCombo = objUsr.getComboList("User", True, False, True, Company._Object._Companyunkid, 270, FinancialYear._Object._YearUnkid)
            '    Else
            '        dsCombo = objUsr.getComboList("User", True, False, True, Company._Object._Companyunkid, 506, FinancialYear._Object._YearUnkid)
            '    End If
            'Else
            '    If mblnIsAssessorMode = True Then
            '        dsCombo = objUsr.getComboList("User", True, False, False, , 270)
            '    Else
            '        dsCombo = objUsr.getComboList("User", True, False, False, , 506)
            '    End If
            'End If

            'S.SANDEEP [01 DEC 2015] -- START
            'If mblnIsAssessorMode = True Then
            '    dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, 270, FinancialYear._Object._YearUnkid)
            'Else
            '    dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, 506, FinancialYear._Object._YearUnkid)
            'End If

            Dim intPrivilege As Integer = 0

            If mblnIsAssessorMode = True Then
                If menAction <> enAction.EDIT_ONE Then
                    intPrivilege = 863
                Else
                    intPrivilege = 864
                End If
            Else
                If menAction <> enAction.EDIT_ONE Then
                    intPrivilege = 867
                Else
                    intPrivilege = 868
                End If
            End If

            'Nilay (01-Mar-2016) -- Start
            'dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, intPrivilege, FinancialYear._Object._YearUnkid)

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, intPrivilege, FinancialYear._Object._YearUnkid, True)
            dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, intPrivilege.ToString(), FinancialYear._Object._YearUnkid, True)
            'S.SANDEEP [20-JUN-2018] -- END

            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [01 DEC 2015] -- END


            'S.SANDEEP [10 AUG 2015] -- END



            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("User")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction <> enAction.EDIT_ONE Then
            '    dsCombo = objEmp.GetList("List", True, , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEmp.GetList("List", True)
            'End If

            'Dim blnInActiveEmp As Boolean = False
            'If menAction <> enAction.EDIT_ONE Then
            '    blnInActiveEmp = False
            'Else
            '    blnInActiveEmp = True
            'End If
            'dsCombo = objEmp.GetList(FinancialYear._Object._DatabaseName, _
            '                            User._Object._Userunkid, _
            '                            FinancialYear._Object._YearUnkid, _
            '                            Company._Object._Companyunkid, _
            '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                            ConfigParameter._Object._UserAccessModeSetting, _
            '                            True, blnInActiveEmp, "List", _
            '                            ConfigParameter._Object._ShowFirstAppointmentDate)
            'mdtEmployee = dsCombo.Tables("List")
            'mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            objPswd = Nothing : objUsr = Nothing : objEmp = Nothing
        End Try
    End Sub

    Private Sub Fill_Employee()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dEmp As DataTable = Nothing
            Dim sFilter As String = String.Empty
            Dim mstrIds As String = String.Empty
            Dim blnInActiveEmp As Boolean = False

            If menAction <> enAction.EDIT_ONE Then
                blnInActiveEmp = False
            Else
                blnInActiveEmp = True
            End If

            If txtName.Tag IsNot Nothing Then

                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'mstrIds = objAssessorTran.Get_AssignedEmp(CInt(txtName.Tag), mblnIsAssessorMode)

                'S.SANDEEP [27 DEC 2016] -- START
                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                'mstrIds = objAssessorTran.Get_AssignedEmp(CInt(txtName.Tag), mblnIsAssessorMode, objchkIsExternalEntity.Checked)
                Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE
                If menAction <> enAction.EDIT_ONE Then
                    mstrIds = objAssessorTran.Get_AssignedEmp(0, mblnIsAssessorMode, objchkIsExternalEntity.Checked, strVisibleTypeIds.ToString)
                Else
                    mstrIds = objAssessorTran.Get_AssignedEmp(CInt(txtName.Tag), mblnIsAssessorMode, objchkIsExternalEntity.Checked, strVisibleTypeIds.ToString)
                End If
                'S.SANDEEP [27 DEC 2016] -- END

                'Shani(01-MAR-2016) -- End

            End If

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'If txtName.Tag IsNot Nothing AndAlso mstrIds.Trim.Length > 0 Then
            '    sFilter &= "AND hremployee_master.employeeunkid NOT IN(" & txtName.Tag.ToString & "," & mstrIds & ")"
            'Else
            '    sFilter &= "AND hremployee_master.employeeunkid NOT IN(" & txtName.Tag.ToString & ")"
            'End If

            'Shani (07-Dec-2016) -- Start
            'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
            'If objchkIsExternalEntity.Checked = False Then
            '    sFilter &= "AND hremployee_master.employeeunkid NOT IN(" & txtName.Tag.ToString & ")"
            'End If

            'If mstrIds.Trim.Length > 0 Then
            '    sFilter &= "AND hremployee_master.employeeunkid NOT IN(" & mstrIds & ")"
            'End If

            ''Shani(01-MAR-2016)-- End

            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    sFilter &= "AND " & mstrAdvanceFilter & " "
            'End If

            If objchkIsExternalEntity.Checked = False Then
                sFilter &= "AND employeeunkid NOT IN(" & txtName.Tag.ToString & ")"
            End If

            If mstrIds.Trim.Length > 0 Then
                sFilter &= "AND employeeunkid NOT IN(" & mstrIds & ")"
            End If

            'Shani (07-Dec-2016) -- End

            'If mstrIds.Trim.Length > 0 Then
            '    sFilter &= "AND hremployee_master.employeeunkid not in ( " & mstrIds.Trim & " ) "
            'End If

            If sFilter.Trim.Length > 0 Then
                sFilter = sFilter.Trim.Substring(3)
            End If

            'Shani (07-Dec-2016) -- Start
            'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
            'dEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, _
            '                      User._Object._Userunkid, _
            '                      FinancialYear._Object._YearUnkid, _
            '                      Company._Object._Companyunkid, _
            '                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                      ConfigParameter._Object._UserAccessModeSetting, _
            '                      True, blnInActiveEmp, "List", _
            '                      ConfigParameter._Object._ShowFirstAppointmentDate, , , sFilter).Tables(0)

            Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job
            Dim dsEmp As DataSet = Nothing
            dsEmp = objEmp.GetListForDynamicField(StrCheck_Fields, _
                                                 FinancialYear._Object._DatabaseName, _
                                  User._Object._Userunkid, _
                                  FinancialYear._Object._YearUnkid, _
                                  Company._Object._Companyunkid, _
                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, blnInActiveEmp, "List", , , _
                                                 mstrAdvanceFilter, _
                                                 ConfigParameter._Object._ShowFirstAppointmentDate)

            dEmp = dsEmp.Tables(0).Clone

            If dsEmp.Tables(0).Rows.Count > 0 Then
                Dim dr = dsEmp.Tables(0).Select(sFilter)
                If dr.Length > 0 Then
                    dEmp = dr.CopyToDataTable
                End If
            End If
            Dim strcolName As String = ""
            strcolName = IIf(Language.getMessage("clsEmployee_Master", 42, "Code") = "", "Code", Language.getMessage("clsEmployee_Master", 42, "Code")).ToString
            dEmp.Columns(strcolName).ColumnName = "employeecode"

            strcolName = IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ToString
            dEmp.Columns(strcolName).ColumnName = "name"

            strcolName = IIf(Language.getMessage("clsEmployee_Master", 118, "Job") = "", "Job", Language.getMessage("clsEmployee_Master", 118, "Job")).ToString
            dEmp.Columns(strcolName).ColumnName = "job_name"

            strcolName = IIf(Language.getMessage("clsEmployee_Master", 120, "Department") = "", "Department", Language.getMessage("clsEmployee_Master", 120, "Department")).ToString
            dEmp.Columns(strcolName).ColumnName = "DeptName"
            'Shani (07-Dec-2016) -- End

            dEmp.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            mdtEmployee = dEmp.Copy

            dtEmpView = dEmp.DefaultView

            dgvAEmployee.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "name"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvAEmployee.DataSource = dtEmpView

            If txtName.Tag IsNot Nothing Then
                If mdtAssessor IsNot Nothing Then
                    mdtAssessor.Rows.Clear()
                End If

                If mdtReviewer IsNot Nothing Then
                    mdtReviewer.Rows.Clear()
                End If


                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'objAssessorTran._AssessorEmployeeId = CInt(txtName.Tag)

                'Shani (07-Dec-2016) -- Start
                'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
                objAssessorTran._EmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
                'Shani (07-Dec-2016) -- End
                objAssessorTran._AssessorEmployeeId(objchkIsExternalEntity.Checked) = CInt(txtName.Tag)
                'Shani(01-MAR-2016)-- End

                mdtAssessor = New DataView(objAssessorTran._DataTable, "isreviewer = false", "", DataViewRowState.CurrentRows).ToTable
                mdtReviewer = New DataView(objAssessorTran._DataTable, "isreviewer = true", "", DataViewRowState.CurrentRows).ToTable

                If mblnIsAssessorMode = True Then
                    objchkReviewer.Enabled = False
                    dgvReviewer.ReadOnly = True
                    btnDeleteR.Enabled = False
                Else
                    objchkAssessor.Enabled = False
                    dgvAssessor.ReadOnly = True
                    btnDeleteA.Enabled = False
                End If
                Call Fill_Assigned_Employee()
            End If

            'If mdtEmployee.Rows.Count > 0 Then
            '    Dim dEmp As DataTable = Nothing
            '    Dim sFilter As String = String.Empty
            '    Dim mstrIds As String = String.Empty

            '    If mstrAdvanceFilter.Trim.Length > 0 Then
            '        sFilter &= "AND " & mstrAdvanceFilter
            '    End If

            '    If txtName.Tag IsNot Nothing Then
            '        'S.SANDEEP [ 01 JAN 2015 ] -- START
            '        'mstrIds = objAssessorTran.Get_AssignedEmp(CInt(txtName.Tag))
            '        mstrIds = objAssessorTran.Get_AssignedEmp(CInt(txtName.Tag), mblnIsAssessorMode)
            '        'S.SANDEEP [ 01 JAN 2015 ] -- END
            '    End If

            '    If txtName.Tag IsNot Nothing AndAlso mstrIds.Trim.Length > 0 Then
            '        sFilter &= "AND employeeunkid NOT IN(" & txtName.Tag.ToString & "," & mstrIds & ")"
            '    Else
            '        sFilter &= "AND employeeunkid NOT IN(" & txtName.Tag.ToString & ")"
            '    End If
            '    If sFilter.Trim.Length > 0 Then
            '        sFilter = sFilter.Substring(3)
            '        dEmp = New DataView(mdtEmployee, sFilter, "", DataViewRowState.CurrentRows).ToTable
            '    Else
            '        dEmp = New DataView(mdtEmployee, "", "", DataViewRowState.CurrentRows).ToTable
            '    End If

            '    dtEmpView = dEmp.DefaultView

            '    dgvAEmployee.AutoGenerateColumns = False
            '    objdgcolhECheck.DataPropertyName = "ischeck"
            '    dgcolhEcode.DataPropertyName = "employeecode"
            '    dgcolhEName.DataPropertyName = "name"
            '    objdgcolhEmpId.DataPropertyName = "employeeunkid"
            '    dgvAEmployee.DataSource = dtEmpView

            '    If txtName.Tag IsNot Nothing Then
            '        If mdtAssessor IsNot Nothing Then
            '            mdtAssessor.Rows.Clear()
            '        End If
            '        If mdtReviewer IsNot Nothing Then
            '            mdtReviewer.Rows.Clear()
            '        End If
            '        objAssessorTran._AssessorEmployeeId = CInt(txtName.Tag)
            '        mdtAssessor = New DataView(objAssessorTran._DataTable, "isreviewer = false", "", DataViewRowState.CurrentRows).ToTable
            '        mdtReviewer = New DataView(objAssessorTran._DataTable, "isreviewer = true", "", DataViewRowState.CurrentRows).ToTable

            '        If mblnIsAssessorMode = True Then
            '            objchkReviewer.Enabled = False
            '            dgvReviewer.ReadOnly = True
            '            btnDeleteR.Enabled = False
            '        Else
            '            objchkAssessor.Enabled = False
            '            dgvAssessor.ReadOnly = True
            '            btnDeleteA.Enabled = False
            '        End If
            '        Call Fill_Assigned_Employee()
            '    End If
            'End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assigned_Employee()
        Try
            dtAseView = mdtAssessor.DefaultView
            dtAseView.RowFilter = " AUD <> 'D' "
            dgvAssessor.AutoGenerateColumns = False
            objdgcolhaCheck.DataPropertyName = "ischeck"
            objdgcolhaCode.DataPropertyName = "employeecode"
            objdgcolhaEmp.DataPropertyName = "name"
            dgcolhaDepartment.DataPropertyName = "Department"
            dgcolhaJob.DataPropertyName = "Job"
            objdgcolhaEmpId.DataPropertyName = "employeeunkid"
            objdgcolhAMasterId.DataPropertyName = "assessormasterunkid"
            objdgcolhATranId.DataPropertyName = "assessortranunkid"
            dgvAssessor.DataSource = dtAseView


            dtRevView = mdtReviewer.DefaultView
            dtRevView.RowFilter = " AUD <> 'D' "
            dgvReviewer.AutoGenerateColumns = False
            objdgcolhrCheck.DataPropertyName = "ischeck"
            objdgcolhrCode.DataPropertyName = "employeecode"
            objdgcolhrEmp.DataPropertyName = "name"
            dgcolhrDepartment.DataPropertyName = "Department"
            dgcolhrJob.DataPropertyName = "Job"
            objdgcolhrEmpId.DataPropertyName = "employeeunkid"
            objdgcolhRMasterId.DataPropertyName = "assessormasterunkid"
            objdgcolhRTranId.DataPropertyName = "assessortranunkid"
            dgvReviewer.DataSource = dtRevView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Assigned_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        Try
            Dim mdtRow As DataRow = Nothing
            Dim dtmp() As DataRow = Nothing
            If mblnIsAssessorMode = True Then
                dtmp = mdtAssessor.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")

                If dtmp.Length <= 0 Then
                    mdtRow = mdtAssessor.NewRow
                    mdtRow.Item("assessortranunkid") = -1
                    mdtRow.Item("assessormasterunkid") = mintAssessorMasterId
                    mdtRow.Item("stationunkid") = 0
                    'Shani (07-Dec-2016) -- Start
                    'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
                    'mdtRow.Item("departmentunkid") = dRow.Item("departmentunkid")
                    mdtRow.Item("departmentunkid") = 0
                    'Shani (07-Dec-2016) -- End
                    mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                    mdtRow.Item("isvoid") = False
                    mdtRow.Item("voiddatetime") = DBNull.Value
                    mdtRow.Item("voiduserunkid") = -1
                    mdtRow.Item("voidreason") = ""
                    mdtRow.Item("ischeck") = False
                    mdtRow.Item("employeecode") = dRow.Item("employeecode")
                    mdtRow.Item("name") = dRow.Item("name")
                    mdtRow.Item("department") = dRow.Item("DeptName")
                    mdtRow.Item("job") = dRow.Item("job_name")
                    mdtRow.Item("AUD") = "A"
                    mdtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtRow.Item("isreviewer") = mblnIsAssessorMode
                    If txtName.Tag IsNot Nothing Then
                        mdtRow.Item("arId") = CInt(txtName.Tag)
                    Else
                        mdtRow.Item("arId") = 0
                    End If
                    'S.SANDEEP [27 DEC 2016] -- START
                    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                    mdtRow.Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.VISIBLE
                    'S.SANDEEP [27 DEC 2016] -- END
                    mdtAssessor.Rows.Add(mdtRow)
                End If

            Else
                'S.SANDEEP [23 JUL 2015] -- START
                'dtmp = mdtAssessor.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
                dtmp = mdtReviewer.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
                'S.SANDEEP [23 JUL 2015] -- END
                If dtmp.Length <= 0 Then
                    mdtRow = mdtReviewer.NewRow
                    mdtRow.Item("assessortranunkid") = -1
                    mdtRow.Item("assessormasterunkid") = mintAssessorMasterId
                    mdtRow.Item("stationunkid") = 0
                    'Shani (07-Dec-2016) -- Start
                    'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
                    'mdtRow.Item("departmentunkid") = dRow.Item("departmentunkid")
                    mdtRow.Item("departmentunkid") = 0
                    'Shani (07-Dec-2016) -- End
                    mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                    mdtRow.Item("isvoid") = False
                    mdtRow.Item("voiddatetime") = DBNull.Value
                    mdtRow.Item("voiduserunkid") = -1
                    mdtRow.Item("voidreason") = ""
                    mdtRow.Item("ischeck") = False
                    mdtRow.Item("employeecode") = dRow.Item("employeecode")
                    mdtRow.Item("name") = dRow.Item("name")
                    mdtRow.Item("department") = dRow.Item("DeptName")
                    mdtRow.Item("job") = dRow.Item("job_name")
                    mdtRow.Item("AUD") = "A"
                    mdtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtRow.Item("isreviewer") = mblnIsAssessorMode
                    If txtName.Tag IsNot Nothing Then
                        mdtRow.Item("arId") = CInt(txtName.Tag)
                    Else
                        mdtRow.Item("arId") = 0
                    End If
                    'S.SANDEEP [27 DEC 2016] -- START
                    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                    mdtRow.Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.VISIBLE
                    'S.SANDEEP [27 DEC 2016] -- END
                    mdtReviewer.Rows.Add(mdtRow)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid_Data() As Boolean
        Try
            If txtName.Text.Trim.Length <= 0 Then
                If mblnIsAssessorMode = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Assessor is mandatory information. Please provide assessor to continue."), enMsgBoxStyle.Information)
                    Return False
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Reviewer is mandatory information. Please provide reviewer to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'If CInt(cboUser.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "User is mandatory information. Please provide user to continue"), enMsgBoxStyle.Information)
            '    cboUser.Focus()
            '    Return False
            'End If
            If CInt(cboUser.SelectedValue) <= 0 AndAlso objchkIsExternalEntity.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "User is mandatory information. Please provide user to continue"), enMsgBoxStyle.Information)
                cboUser.Focus()
                Return False
            End If
            'Shani(01-MAR-2016)-- End

            If CInt(cboUser.SelectedValue) <> objAssessorMaster._MappedUserId Then
                Dim sMessage As String = String.Empty

                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'If objAssessorTran.IsUserMapped(mblnIsAssessorMode, CInt(cboUser.SelectedValue), sMessage ) = True Then

                'S.SANDEEP [22-JUN-2017] -- START
                If objAssessorTran.IsUserMapped(mblnIsAssessorMode, CInt(cboUser.SelectedValue), sMessage, objchkIsExternalEntity.Checked, clsAssessor.enARVisibilityTypeId.VISIBLE) = True Then
                    'If objAssessorTran.IsUserMapped(mblnIsAssessorMode, CInt(cboUser.SelectedValue), sMessage, objchkIsExternalEntity.Checked) = True Then
                    'S.SANDEEP [22-JUN-2017] -- END
                    'Shani(01-MAR-2016)-- End
                    eZeeMsgBox.Show(sMessage, enMsgBoxStyle.Information)

                    'Shani(01-MAR-2016) -- Start
                    'Enhancement :PA External Approver Flow
                    'cboUser.Focus()
                    If objchkIsExternalEntity.Checked Then
                        txtName.Focus()
                    Else
                        cboUser.Focus()
                    End If
                    'Shani(01-MAR-2016)-- End

                    Return False
                End If
            End If

            Dim dtmp() As DataRow = Nothing
            If mblnIsAssessorMode = True Then
                mdtAssessor.AcceptChanges()
                dtmp = mdtAssessor.Select("AUD <> 'D'")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Assessor access is mandatory. Please provide assessor access to save."), enMsgBoxStyle.Information)
                    Return False
                End If
            Else
                mdtReviewer.AcceptChanges()
                dtmp = mdtReviewer.Select("AUD <> 'D'")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Reviewer access is mandatory. Please provide reviewer access to save."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid_Data", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objAssessorMaster._EmployeeId = CInt(txtName.Tag)

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'objAssessorMaster._MappedUserId = CInt(cboUser.SelectedValue)
            If objchkIsExternalEntity.Checked = True Then
                objAssessorMaster._MappedUserId = CInt(txtName.Tag)
            Else
                objAssessorMaster._MappedUserId = CInt(cboUser.SelectedValue)
            End If
            objAssessorMaster._ExternalAssessorReviewer = objchkIsExternalEntity.Checked
            'Shani(01-MAR-2016) -- End

            objAssessorMaster._Userunkid = User._Object._Userunkid
            If mintAssessorMasterId > 0 Then
                objAssessorMaster._Isreviewer = objAssessorMaster._Isreviewer
                objAssessorMaster._Isvoid = objAssessorMaster._Isvoid
                objAssessorMaster._Voiddatetime = objAssessorMaster._Voiddatetime
                objAssessorMaster._Voidreason = objAssessorMaster._Voidreason
                objAssessorMaster._Voiduserunkid = objAssessorMaster._Voiduserunkid
            Else
                objAssessorMaster._Isreviewer = Not mblnIsAssessorMode
                objAssessorMaster._Isvoid = False
                objAssessorMaster._Voiddatetime = Nothing
                objAssessorMaster._Voidreason = ""
                objAssessorMaster._Voiduserunkid = -1
            End If

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            objAssessorMaster._Visibletypeid = clsAssessor.enARVisibilityTypeId.VISIBLE
            'S.SANDEEP [27 DEC 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            objchkIsExternalEntity.Checked = objAssessorMaster._ExternalAssessorReviewer
            'Shani(01-MAR-2016)-- End
            txtName.Tag = objAssessorMaster._EmployeeId
            cboUser.SelectedValue = objAssessorMaster._MappedUserId
            txtName.Text = objAssessorMaster._EmployeeName
            If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAssessorReviewerAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessorMaster = New clsAssessor
        objAssessorTran = New clsAssessor_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            If mblnIsAssessorMode = True Then
                mstrModuleName = "frmAssessor_AddEdit"
                Me.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Assessor")
                gbInfo.Text = Language.getMessage(mstrModuleName, 2, "Assessor Information")
                lblApproverName.Text = Language.getMessage(mstrModuleName, 3, "Assessor")
                objchkIsExternalEntity.Text = Language.getMessage(mstrModuleName, 19, "Make External Assessor")
            Else
                mstrModuleName = "frmReviewer_AddEdit"
                Me.Text = Language.getMessage(mstrModuleName, 4, "Add/Edit Reviewer")
                gbInfo.Text = Language.getMessage(mstrModuleName, 5, "Reviewer Information")
                lblApproverName.Text = Language.getMessage(mstrModuleName, 6, "Reviewer")
                objchkIsExternalEntity.Text = Language.getMessage(mstrModuleName, 20, "Make External Reviewer")
            End If
            'Shani(01-MAR-2016)-- End

            Call Fill_Combo()
            If menAction <> enAction.EDIT_ONE Then
                If mblnIsAssessorMode = True Then
                    tabcAssignedEmployee.TabPages.Remove(tabpReviewer)
                Else
                    tabcAssignedEmployee.TabPages.Remove(tabpAssessor)
                End If
            Else
                objAssessorMaster._Assessormasterunkid = mintAssessorMasterId
                objbtnSearchEmployee.Enabled = False
                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                objchkIsExternalEntity.Checked = False
                'Shani(01-MAR-2016)-- End

                'S.SANDEEP [15-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002013}
                objchkIsExternalEntity.Enabled = False
                'S.SANDEEP [15-Feb-2018] -- END

            End If
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessorReviewerAddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessor.SetMessages()
            clsAssessor_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessor,clsAssessor_tran"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'With frm
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "name"
            '    .CodeMember = "employeecode"
            '    .DataSource = mdtEmployee
            'End With

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'Dim objEmployee As New clsEmployee_Master
            'Dim dsList As DataSet
            'dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     ConfigParameter._Object._UserAccessModeSetting, _
            '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False)


            'With frm
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .CodeMember = "employeecode"
            '    .DataSource = dsList.Tables("Employee")
            'End With
            ''S.SANDEEP [04 JUN 2015] -- END

            If objchkIsExternalEntity.Checked = False Then
                Dim objEmployee As New clsEmployee_Master
                Dim dsList As DataSet
                dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False)


                With frm
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .CodeMember = "employeecode"
                    .DataSource = dsList.Tables("Employee")
                End With
            Else
                Dim objUser As New clsUserAddEdit
                Dim strPrivilegeIds As String = ""
                If mblnIsAssessorMode = True Then
                    strPrivilegeIds = "863"
                Else
                    strPrivilegeIds = "867"
                End If

                Dim dsList As DataSet = objUser.GetExternalApproverList("List", Company._Object._Companyunkid, _
                                                                        FinancialYear._Object._YearUnkid, _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        strPrivilegeIds)

                'frm.DataSource = dsList.Tables("List")
                'frm.ValueMember = "userunkid"
                'frm.DisplayMember = "Name"
                'frm.CodeMember = "username"
                With frm
                    .ValueMember = "userunkid"
                    .DisplayMember = "Name"
                    .CodeMember = "username"
                    .DataSource = dsList.Tables("List")
                End With
            End If

            If frm.DisplayDialog Then
                txtName.Text = frm.SelectedAlias & " - " & frm.SelectedText
                txtName.Tag = frm.SelectedValue

                If objchkIsExternalEntity.Checked = False Then
                    Dim objOption As New clsPassowdOptions
                    If objOption._IsEmployeeAsUser Then
                        Dim objUser As New clsUserAddEdit
                        Dim mintUserID As Integer = objUser.Return_UserId(CInt(frm.SelectedValue), Company._Object._Companyunkid)
                        Dim drRow() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = " & mintUserID)
                        If drRow.Length > 0 Then
                            cboUser.SelectedValue = mintUserID
                        Else
                            cboUser.SelectedValue = 0
                        End If
                    End If
                End If
            End If
            'Shani(01-MAR-2016)-- End

            If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboUser.ValueMember
                .CodeMember = cboUser.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If dtEmpView IsNot Nothing Then
                Dim drCheck() As DataRow = Nothing
                drCheck = dtEmpView.ToTable.Select("ischeck=true")
                Dim iblFlag As Boolean = False
                If drCheck.Length > 0 Then
                    If mblnIsAssessorMode = True Then
                        For i As Integer = 0 To drCheck.Length - 1
                            Call Add_DataRow(drCheck(i))
                        Next
                    Else
                        For i As Integer = 0 To drCheck.Length - 1
                            If objAssessorMaster.IsMappedToReviewer(CInt(drCheck(i).Item("employeeunkid"))) = False Then
                                Call Add_DataRow(drCheck(i))
                            Else
                                iblFlag = True
                            End If
                        Next
                    End If
                End If
                If iblFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Some of the checked employee(s) are already assigned with some of the reviewer, And will be added to selected reviewer."), enMsgBoxStyle.Information)
                End If
                Call Fill_Assigned_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteA.Click
        Try
            If dtAseView Is Nothing Then Exit Sub

            Dim sVoidReason As String = String.Empty
            Dim dtmp() As DataRow = dtAseView.Table.Select("ischeck=true")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Check atleast one employee to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim blnFlag As Boolean = False
            For i As Integer = 0 To dtmp.Length - 1

                'S.SANDEEP [30 DEC 2016] -- START
                'ENHANCEMENT : VOLTAMP EMAILING REPORT
                'assessormasterunkid
                'If objAssessorTran.isUsed(True, CInt(dtmp(i).Item("employeeunkid")), CInt(dtmp(i).Item("arId"))) = True Then
                If objAssessorTran.isUsed(True, CInt(dtmp(i).Item("employeeunkid")), CInt(dtmp(i).Item("assessormasterunkid"))) = True Then
                    'S.SANDEEP [30 DEC 2016] -- END


                    If blnFlag = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Assessment of some these checked employee(s) has already been done." & vbCrLf & _
                                                               "Due to this some employee(s) will not be unassigned from this assessor." & vbCrLf & _
                                                               "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit For
                        Else
                            blnFlag = True
                            Continue For
                        End If
                    End If
                Else
                    If CInt(dtmp(i).Item("assessortranunkid")) > 0 Then
                        If sVoidReason.Trim.Length <= 0 Then
                            Dim frm As New frmReasonSelection
                            If User._Object._Isrighttoleft = True Then
                                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                frm.RightToLeftLayout = True
                                Call Language.ctlRightToLeftlayOut(frm)
                            End If
                            frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                            If sVoidReason.Trim.Length <= 0 Then Exit Sub
                        End If
                        dtmp(i).Item("isvoid") = True
                        dtmp(i).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtmp(i).Item("voiduserunkid") = User._Object._Userunkid
                        dtmp(i).Item("voidreason") = sVoidReason
                        dtmp(i).Item("ischeck") = False
                        dtmp(i).Item("AUD") = "D"
                        'S.SANDEEP [30 DEC 2016] -- START
                        'ENHANCEMENT : VOLTAMP EMAILING REPORT
                        dtmp(i).Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.NOT_VISIBLE
                        'S.SANDEEP [30 DEC 2016] -- END
                    Else
                        dtmp(i).Item("ischeck") = False
                        dtmp(i).Item("AUD") = "D"
                        'S.SANDEEP [30 DEC 2016] -- START
                        'ENHANCEMENT : VOLTAMP EMAILING REPORT
                        dtmp(i).Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.NOT_VISIBLE
                        'S.SANDEEP [30 DEC 2016] -- END
                    End If
                End If
            Next
            Call Fill_Assigned_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteA_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteR.Click
        Try
            If dtRevView Is Nothing Then Exit Sub

            Dim dtmp() As DataRow = dtRevView.Table.Select("ischeck=true")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Check atleast one employee to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim blnFlag As Boolean = False
            Dim sVoidReason As String = String.Empty
            For i As Integer = 0 To dtmp.Length - 1
                'S.SANDEEP [30 DEC 2016] -- START
                'ENHANCEMENT : VOLTAMP EMAILING REPORT
                'assessormasterunkid
                'If objAssessorTran.isUsed(True, CInt(dtmp(i).Item("employeeunkid")), CInt(dtmp(i).Item("arId"))) = True Then
                If objAssessorTran.isUsed(True, CInt(dtmp(i).Item("employeeunkid")), CInt(dtmp(i).Item("assessormasterunkid"))) = True Then
                    'S.SANDEEP [30 DEC 2016] -- END
                    If blnFlag = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Assessment of some these checked employee(s) has already been done." & vbCrLf & _
                                                               "Due to this some employee(s) will not be unassigned from this assessor." & vbCrLf & _
                                                               "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit For
                        Else
                            blnFlag = True
                            Continue For
                        End If
                    End If
                Else
                    If CInt(dtmp(i).Item("assessortranunkid")) > 0 Then
                        If sVoidReason.Trim.Length <= 0 Then
                            Dim frm As New frmReasonSelection
                            If User._Object._Isrighttoleft = True Then
                                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                frm.RightToLeftLayout = True
                                Call Language.ctlRightToLeftlayOut(frm)
                            End If
                            frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                            If sVoidReason.Trim.Length <= 0 Then Exit Sub
                        End If
                        dtmp(i).Item("isvoid") = True
                        dtmp(i).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtmp(i).Item("voiduserunkid") = User._Object._Userunkid
                        dtmp(i).Item("voidreason") = sVoidReason
                        dtmp(i).Item("ischeck") = False
                        dtmp(i).Item("AUD") = "D"
                        'S.SANDEEP [30 DEC 2016] -- START
                        'ENHANCEMENT : VOLTAMP EMAILING REPORT
                        dtmp(i).Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.NOT_VISIBLE
                        'S.SANDEEP [30 DEC 2016] -- END
                    Else
                        dtmp(i).Item("ischeck") = False
                        dtmp(i).Item("AUD") = "D"
                        'S.SANDEEP [30 DEC 2016] -- START
                        'ENHANCEMENT : VOLTAMP EMAILING REPORT
                        dtmp(i).Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.NOT_VISIBLE
                        'S.SANDEEP [30 DEC 2016] -- END
                    End If
                End If
            Next
            Call Fill_Assigned_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteR_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Valid_Data() = False Then Exit Sub
            Call SetValue()
            If menAction <> enAction.EDIT_ONE Then
                If objAssessorMaster.Insert(mdtAssessor, mdtReviewer) = True Then
                    If mblnIsAssessorMode = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Reviewer defined successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Assessor defined successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    End If
                    'Shani(01-MAR-2016) -- Start
                    'Enhancement :PA External Approver Flow
                    'Me.Close()
                    mblnCancel = False
                    Me.Close()
                Else
                    eZeeMsgBox.Show(objAssessorMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                    'Shani(01-MAR-2016) -- End
                End If
            Else
                If objAssessorMaster.Update(mdtAssessor, mdtReviewer) = True Then
                    If mblnIsAssessorMode = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Reviewer updated successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Assessor updated successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    End If
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call Fill_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dtEmpView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtEmpView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtEmpView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In dtEmpView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtaSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtaSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAssessor.Rows.Count > 0 Then
                        If dgvAssessor.SelectedRows(0).Index = dgvAssessor.Rows(dgvAssessor.RowCount - 1).Index Then Exit Sub
                        dgvAssessor.Rows(dgvAssessor.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAssessor.Rows.Count > 0 Then
                        If dgvAssessor.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAssessor.Rows(dgvAssessor.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtaSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtaSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtaSearch.Text.Trim.Length > 0 Then
                strSearch = objdgcolhaCode.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            objdgcolhaEmp.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaDepartment.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaJob.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' "
            End If
            dtAseView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAssessor_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAssessor.CellContentClick
        Try
            RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAssessor.IsCurrentCellDirty Then
                    Me.dgvAssessor.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtAseView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtAseView.ToTable.Rows.Count = drRow.Length Then
                        objchkAssessor.CheckState = CheckState.Checked
                    Else
                        objchkAssessor.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAssessor.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssessor_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAssessor.CheckedChanged
        Try
            RemoveHandler dgvAssessor.CellContentClick, AddressOf dgvAssessor_CellContentClick
            For Each dr As DataRowView In dtAseView
                dr.Item("ischeck") = CBool(objchkAssessor.CheckState)
            Next
            dgvAssessor.Refresh()
            AddHandler dgvAssessor.CellContentClick, AddressOf dgvAssessor_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtrSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtrSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvReviewer.Rows.Count > 0 Then
                        If dgvReviewer.SelectedRows(0).Index = dgvReviewer.Rows(dgvReviewer.RowCount - 1).Index Then Exit Sub
                        dgvReviewer.Rows(dgvReviewer.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvReviewer.Rows.Count > 0 Then
                        If dgvReviewer.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvReviewer.Rows(dgvReviewer.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtrSearch_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtrSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtrSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtrSearch.Text.Trim.Length > 0 Then
                strSearch = objdgcolhrCode.DataPropertyName & " LIKE '%" & txtrSearch.Text & "%' OR " & _
                            objdgcolhrEmp.DataPropertyName & " LIKE '%" & txtrSearch.Text & "%' OR " & _
                            dgcolhrDepartment.DataPropertyName & " LIKE '%" & txtrSearch.Text & "%' OR " & _
                            dgcolhrJob.DataPropertyName & " LIKE '%" & txtrSearch.Text & "%' "
            End If
            dtRevView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtrSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkReviewer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkReviewer.CheckedChanged
        Try
            RemoveHandler dgvReviewer.CellContentClick, AddressOf dgvReviewer_CellContentClick
            For Each dr As DataRowView In dtRevView
                dr.Item("ischeck") = CBool(objchkReviewer.CheckState)
            Next
            dgvReviewer.Refresh()
            AddHandler dgvReviewer.CellContentClick, AddressOf dgvReviewer_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkReviewer_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvReviewer_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReviewer.CellContentClick
        Try
            RemoveHandler objchkReviewer.CheckedChanged, AddressOf objchkReviewer_CheckedChanged
            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvReviewer.IsCurrentCellDirty Then
                    Me.dgvReviewer.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtRevView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtRevView.ToTable.Rows.Count = drRow.Length Then
                        objchkReviewer.CheckState = CheckState.Checked
                    Else
                        objchkReviewer.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkReviewer.CheckState = CheckState.Unchecked
                End If
            End If
            RemoveHandler objchkReviewer.CheckedChanged, AddressOf objchkReviewer_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvReviewer_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani(01-MAR-2016) -- Start
    'Enhancement  :PA External Approver Flow
    Private Sub objchkIsExternalEntity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkIsExternalEntity.CheckedChanged
        Try
            LblUser.Visible = Not objchkIsExternalEntity.Checked
            cboUser.Visible = Not objchkIsExternalEntity.Checked
            objbtnSearchUser.Visible = Not objchkIsExternalEntity.Checked
            If objchkIsExternalEntity.Checked = False Then
                cboUser.SelectedValue = 0
            End If
            If menAction <> enAction.EDIT_ONE Then
                txtName.Text = ""
                txtName.Tag = Nothing
                mstrEmployeeIDs = ""
                dgvAEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkIsExternalEntity_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Shani(01-MAR-2016)-- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbAssignedEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssignedEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteA.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteA.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteR.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteR.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbAssignedEmployee.Text = Language._Object.getCaption(Me.gbAssignedEmployee.Name, Me.gbAssignedEmployee.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.Name, Me.lblApproverName.Text)
            Me.LblUser.Text = Language._Object.getCaption(Me.LblUser.Name, Me.LblUser.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.tabpAssessor.Text = Language._Object.getCaption(Me.tabpAssessor.Name, Me.tabpAssessor.Text)
            Me.tabpReviewer.Text = Language._Object.getCaption(Me.tabpReviewer.Name, Me.tabpReviewer.Text)
            Me.btnDeleteA.Text = Language._Object.getCaption(Me.btnDeleteA.Name, Me.btnDeleteA.Text)
            Me.btnDeleteR.Text = Language._Object.getCaption(Me.btnDeleteR.Name, Me.btnDeleteR.Text)
            Me.dgcolhaDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhaDepartment.Name, Me.dgcolhaDepartment.HeaderText)
            Me.dgcolhaJob.HeaderText = Language._Object.getCaption(Me.dgcolhaJob.Name, Me.dgcolhaJob.HeaderText)
            Me.dgcolhrDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhrDepartment.Name, Me.dgcolhrDepartment.HeaderText)
            Me.dgcolhrJob.HeaderText = Language._Object.getCaption(Me.dgcolhrJob.Name, Me.dgcolhrJob.HeaderText)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsEmployee_Master", 42, "Code")
            Language.setMessage("clsEmployee_Master", 46, "Employee Name")
            Language.setMessage("clsEmployee_Master", 118, "Job")
            Language.setMessage("clsEmployee_Master", 120, "Department")
            Language.setMessage(mstrModuleName, 1, "Add/Edit Assessor")
            Language.setMessage(mstrModuleName, 2, "Assessor Information")
            Language.setMessage(mstrModuleName, 3, "Assessor")
            Language.setMessage(mstrModuleName, 4, "Add/Edit Reviewer")
            Language.setMessage(mstrModuleName, 5, "Reviewer Information")
            Language.setMessage(mstrModuleName, 6, "Reviewer")
            Language.setMessage(mstrModuleName, 7, "Please Check atleast one employee to unassign.")
            Language.setMessage(mstrModuleName, 8, "Assessment of some these checked employee(s")
            Language.setMessage(mstrModuleName, 9, "Assessor is mandatory information. Please provide assessor to continue.")
            Language.setMessage(mstrModuleName, 10, "Reviewer is mandatory information. Please provide reviewer to continue.")
            Language.setMessage(mstrModuleName, 11, "User is mandatory information. Please provide user to continue")
            Language.setMessage(mstrModuleName, 12, "Assessor access is mandatory. Please provide assessor access to save.")
            Language.setMessage(mstrModuleName, 13, "Reviewer access is mandatory. Please provide reviewer access to save.")
            Language.setMessage(mstrModuleName, 14, "Assessor defined successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 15, "Reviewer updated successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 16, "Assessor updated successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 17, "Reviewer defined successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 18, "Sorry, Some of the checked employee(s) are already assigned with some of the reviewer, And will be added to selected reviewer.")
            Language.setMessage(mstrModuleName, 19, "Make External Assessor")
            Language.setMessage(mstrModuleName, 20, "Make External Reviewer")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class