﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessor_Access

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAssessor_Access"
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrEmployeeId As String = String.Empty
    Private objAssessor_Tran As clsAssessor_tran
    Private mintAssessorId As Integer = -1
    Private mblnCancel As Boolean = True
    Dim dsAcessList As DataSet = Nothing
    Private mintReviewerEmpId As Integer = -1
    Private dtReviewer As DataTable


    'Pinkal (12-Jun-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    'Pinkal (12-Jun-2012) -- End

#End Region

#Region "Display Dialog"

    Public Function displayDialog(ByVal eAction As enAction, Optional ByVal intAssessorId As Integer = -1) As Boolean
        Try
            mintAssessorId = intAssessorId
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    'Private Sub FillEmployee()
    '    Dim objEmployee As New clsEmployee_Master
    '    Dim objStation As New clsStation
    '    Dim objDept As New clsDepartment
    '    Try
    '        tvAccess.Nodes.Clear()


    '        Dim dsStation As DataSet = objStation.GetList("StationList", True)
    '        Dim dsDepartment As DataSet = objDept.GetList("Department", True)
    '        'Sohail (06 Jan 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'Dim dsEmployee As DataSet = objEmployee.GetList("Employee", False, False)
    '        Dim dsEmployee As DataSet
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            dsEmployee = objEmployee.GetList("Employee", False, False, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
    '        Else
    '            dsEmployee = objEmployee.GetList("Employee", False, False)
    '        End If
    '        'Sohail (06 Jan 2012) -- End
    '        Dim dtEmp As DataTable
    '        If mintAssessorId > 0 Then
    '            'Sandeep ( 17 JAN 2011 ) -- START
    '            'dtEmp = New DataView(dsEmployee.Tables(0), "employeeunkid NOT IN ('" & mintAssessorId & "')", "", DataViewRowState.CurrentRows).ToTable
    '            Dim objAssessMaster As New clsassess_analysis_master
    '            Dim intAEmpId As Integer = -1
    '            intAEmpId = objAssessMaster.GetAssessorEmpId(mintAssessorId)

    '            'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '            'dtEmp = New DataView(dsEmployee.Tables(0), "employeeunkid NOT IN ('" & intAEmpId & "')", "", DataViewRowState.CurrentRows).ToTable
    '            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '                dtEmp = New DataView(dsEmployee.Tables(0), "employeeunkid NOT IN ('" & intAEmpId & "') AND isactive = 1 ", "", DataViewRowState.CurrentRows).ToTable
    '            Else
                'dtEmp = New DataView(dsEmployee.Tables(0), "employeeunkid NOT IN ('" & intAEmpId & "')", "", DataViewRowState.CurrentRows).ToTable
    '            End If
    '            'S.SANDEEP [ 29 JUNE 2011 ] -- END 


    '            'Sandeep ( 17 JAN 2011 ) -- END 
    '        Else
    '            'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '            'dtEmp = New DataView(dsEmployee.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
    '            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '                dtEmp = New DataView(dsEmployee.Tables(0), "isactive = 1 ", "", DataViewRowState.CurrentRows).ToTable
    '            Else
                'dtEmp = New DataView(dsEmployee.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
    '        End If
    '            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
    '        End If

    '        For Each drDept As DataRow In dsDepartment.Tables("Department").Rows
    '            Dim tvNodeDepartment As TreeNode

    '            tvNodeDepartment = tvAccess.Nodes.Add(drDept("departmentunkid").ToString, drDept("name").ToString)

    '            Dim dtEmployee As DataTable = New DataView(dtEmp, "departmentunkid=" & CInt(drDept("departmentunkid")), "", DataViewRowState.CurrentRows).ToTable

    '            For Each drEmployee As DataRow In dtEmployee.Rows
    '                tvNodeDepartment.Nodes.Add(drEmployee("employeeunkid").ToString, drEmployee("name").ToString)
    '            Next

    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
    '    Finally
    '        objEmployee = Nothing
    '    End Try
    'End Sub
    Private Sub FillEmployee()
        Try
            If CInt(cboAssessor.SelectedValue) > 0 Then
                Dim dMRow() As DataRow = CType(cboAssessor.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboAssessor.SelectedValue) & "'")
                If dMRow.Length > 0 Then
                    mintReviewerEmpId = CInt(dMRow(0)("employeeunkid"))
                    Dim objAssessor As New clsAssessor
                    Dim dsList As DataSet = objAssessor.GetList("List", False, False)
                    Dim dATemp() As DataRow = dsList.Tables(0).Select("employeeunkid = '" & CInt(dMRow(0)("employeeunkid")) & "' AND isreviewer = 0")


                    'Pinkal (12-Jun-2012) -- Start
                    'Enhancement : TRA Changes
                    'dtReviewer = objAssessor.GetReviewerAccess("Reviewer", CInt(dATemp(0)("employeeunkid")), True, CInt(cboAssessor.SelectedValue), True)
                    dtReviewer = objAssessor.GetReviewerAccess("Reviewer", CInt(dATemp(0)("employeeunkid")), True, CInt(cboAssessor.SelectedValue), True, mstrAdvanceFilter)
                    'Pinkal (12-Jun-2012) -- End


                    dgvReviewer.AutoGenerateColumns = False

                    objdgcolhCheck.DataPropertyName = "IsCheck"
                    dgcolhEmployee.DataPropertyName = "Employee"
                    objdgcolhEGrpId.DataPropertyName = "GrpId"
                    objdgcolhEmpId.DataPropertyName = "EmpId"

                    dgvReviewer.DataSource = dtReviewer

                    'Pinkal (12-Jun-2012) -- Start
                    'Enhancement : TRA Changes
                    SetCheckBoxValue()
                    'Pinkal (12-Jun-2012) -- End


                End If
            Else
                If dtReviewer IsNot Nothing Then
                    dtReviewer.Rows.Clear()
                    SetCheckBoxValue()
                End If
                dgvReviewer.DataSource = Nothing
            End If
            Call SetRevieweGridStyle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetRevieweGridStyle()
        Try


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'For Each dgvRow As DataGridViewRow In dgvReviewer.Rows
            '    If CBool(dgvRow.Cells(objdgcolhEIsGrp.Index).Value) = True Then
            '        If dgvRow.Cells(objdgcolhECollapse.Index).Value Is Nothing Then
            '            dgvRow.Cells(objdgcolhECollapse.Index).Value = "-"
            '            dgvRow.DefaultCellStyle.ForeColor = Color.White
            '            dgvRow.DefaultCellStyle.BackColor = Color.Gray
            '        End If
            '    Else
            '        dgvRow.Cells(objdgcolhECollapse.Index).Value = ""
            '    End If
            'Next


            'Pinkal (12-Jun-2012) -- Start
            'Enhancement : TRA Changes

            'For Each dgvRow As DataGridViewRow In dgvReviewer.Rows
            '    If CBool(dgvRow.Cells(objdgcolhEIsGrp.Index).Value) = True Then
            '        If dgvRow.Cells(objdgcolhECollapse.Index).Value Is Nothing Then
            '            dgvRow.Cells(objdgcolhECollapse.Index).Value = "+"
            '            dgvRow.DefaultCellStyle.ForeColor = Color.White
            '            dgvRow.DefaultCellStyle.BackColor = Color.Gray
            '        End If
            '    Else
            '        dgvRow.Cells(objdgcolhECollapse.Index).Value = ""
            '        dgvRow.Visible = False
            '    End If
            'Next

            'Pinkal (12-Jun-2012) -- End

            'Pinkal (06-Feb-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRevieweGridStyle", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 23 JAN 2012 ] -- END


    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objAssessor As New clsAssessor
        Dim dtTable As DataTable
        Try
            dsList = objAssessor.GetList("Assessor", False)
            Dim dtRow As DataRow
            dtRow = dsList.Tables("Assessor").NewRow

            dtRow.Item("assessormasterunkid") = 0
            dtRow.Item("employeeunkid") = 0
            dtRow.Item("assessorname") = Language.getMessage(mstrModuleName, 3, "Select")

            dsList.Tables("Assessor").Rows.Add(dtRow)

            dtRow = Nothing

            dtTable = New DataView(dsList.Tables("Assessor"), "", "assessormasterunkid", DataViewRowState.CurrentRows).ToTable

            With cboAssessor
                .ValueMember = "assessormasterunkid"
                .DisplayMember = "assessorname"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckForChildren(ByVal nodeSource As TreeNode)
        If nodeSource.Nodes Is Nothing Then Exit Sub

        Try
            For Each nNode As TreeNode In nodeSource.Nodes
                nNode.Checked = nodeSource.Checked
                Call CheckForChildren(nNode)
            Next

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "CheckChildren", mstrModuleName)
        End Try
    End Sub

    Private Sub GetEmployee()
        Try
            'For Each tvDepartment As TreeNode In tvAccess.Nodes
            '    For Each trEmployee As TreeNode In tvDepartment.Nodes
            '        If trEmployee.Checked Then
            '            mstrEmployeeId &= "," & trEmployee.Name
            '        End If
            '    Next
            'Next
            'mstrEmployeeId = Mid(mstrEmployeeId, 2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            Call GetEmployee()
            objAssessor_Tran._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
            objAssessor_Tran._EmployeeIds = mstrEmployeeId
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'Private Sub SetAssessorData()

    '    Dim mintEmpNode As Integer = 0
    '    Try
    '        If dsAcessList.Tables("List").Rows.Count > 0 Then

    '            For Each trDept As TreeNode In tvAccess.Nodes '--------------------- START DEPARTMENT LOOP

    '                mintEmpNode = 0

    '                For i = 0 To dsAcessList.Tables("List").Rows.Count - 1


    '                    If CInt(trDept.Name) = CInt(dsAcessList.Tables("List").Rows(i)("departmentunkid")) And trDept.Text = dsAcessList.Tables("List").Rows(i)("departmentname").ToString Then

    '                        For Each tremployee As TreeNode In trDept.Nodes '--------------------- START EMPLOYEE LOOP

    '                            If CInt(tremployee.Name) = CInt(dsAcessList.Tables("List").Rows(i)("employeeunkid")) And tremployee.Text = dsAcessList.Tables("List").Rows(i)("employeename").ToString Then
    '                                tremployee.Checked = True
    '                                mintEmpNode += 1
    '                            End If

    '                        Next                                            '--------------------- END EMPLOYEE LOOP

    '                        If trDept.Nodes.Count = mintEmpNode Then trDept.Checked = True

    '                    End If

    '                Next

    '            Next                                                    '--------------------- END DEPARTMENT LOOP

    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetAssessorData", mstrModuleName)
    '    End Try
    'End Sub


    'Pinkal (12-Jun-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dtReviewer.Select("ischeck = true")

            RemoveHandler objSelectAll.CheckedChanged, AddressOf objSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                objSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvReviewer.Rows.Count Then
                objSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvReviewer.Rows.Count Then
                objSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objSelectAll.CheckedChanged, AddressOf objSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub
    'Pinkal (12-Jun-2012) -- End


#End Region

#Region " Form's Events "

    Private Sub frmAssessor_Access_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAssessor_Tran = New clsAssessor_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call FillCombo()
            Call FillEmployee()
            If menAction = enAction.EDIT_ONE Then
                objAssessor_Tran._Assessormasterunkid = mintAssessorId
                'dsAcessList = objAssessor_Tran.GetData()
                'RemoveHandler tvAccess.AfterCheck, AddressOf tvAccess_AfterCheck
                cboAssessor.SelectedValue = mintAssessorId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessor_Access_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm._Other_ModuleNames = "clsAssessor_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim objfrm As New frmCommonSearch
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Dim dtList As DataTable = CType(cboAssessor.DataSource, DataTable)
            With objfrm
                .DataSource = dtList
                .ValueMember = cboAssessor.ValueMember
                .DisplayMember = cboAssessor.DisplayMember
            End With
            If objfrm.DisplayDialog() Then
                cboAssessor.SelectedValue = objfrm.SelectedValue.ToString
            End If
            cboAssessor.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Dim blnFlag As Boolean = False
    '    Try
    '        SetValue()
    '        If mstrEmployeeId.Length = 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select at least one Employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            tvAccess.Select()
    '            Exit Sub
    '        End If
    '        blnFlag = objAssessor_Tran.Insert()
    '        If blnFlag = True Then
    '            mblnCancel = False
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Information saved successfully."), enMsgBoxStyle.Information)
    '            btnClose_Click(sender, e)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try


            'Pinkal (12-Jun-2012) -- Start
            'Enhancement : TRA Changes
            'Dim dTemp() As DataRow = dtReviewer.Select("IsCheck=true AND IsGrp=false")
            Dim dTemp() As DataRow = dtReviewer.Select("IsCheck=true")
            'Pinkal (12-Jun-2012) -- End


            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Check atleast one employee to set access."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim mstrEmpIds As String = ""

            For i As Integer = 0 To dTemp.Length - 1
                mstrEmpIds &= "," & dTemp(i)("EmpId").ToString
            Next

            If mstrEmpIds.Trim.Length > 0 Then
                mstrEmpIds = Mid(mstrEmpIds, 2)
            End If

            objAssessor_Tran._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
            objAssessor_Tran._EmployeeIds = mstrEmpIds

            'S.SANDEEP [ 18 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If objAssessor_Tran.Insert(dtReviewer, enAssessmentMode.REVIEWER_ASSESSMENT, mintReviewerEmpId) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Information Successfully saved."), enMsgBoxStyle.Information)
            '    cboAssessor.SelectedValue = 0
            'Else
            '    eZeeMsgBox.Show(objAssessor_Tran._Message, enMsgBoxStyle.Information)
            'End If

            If objAssessor_Tran.Insert(dtReviewer, enAssessmentMode.APPRAISER_ASSESSMENT, mintReviewerEmpId) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Information Successfully saved."), enMsgBoxStyle.Information)
                cboAssessor.SelectedValue = 0
            Else
                eZeeMsgBox.Show(objAssessor_Tran._Message, enMsgBoxStyle.Information)
            End If
            'S.SANDEEP [ 18 APRIL 2012 ] -- END


            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 23 JAN 2012 ] -- END


    
    'Pinkal (12-Jun-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            FillEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Jun-2012) -- End


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub tvAccess_AfterCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
        Try
            CheckForChildren(e.Node)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tvAccess_AfterCheck", mstrModuleName)
        End Try

    End Sub

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            'If CInt(cboAssessor.SelectedValue) > 0 Then
            '    mintAssessorId = CInt(cboAssessor.SelectedValue)
            '    tvAccess.Enabled = True
            '    chkSelectAll.Enabled = True

            '    If menAction = enAction.EDIT_ONE Then
            '        SetAssessorData()
            '        AddHandler tvAccess.AfterCheck, AddressOf tvAccess_AfterCheck
            '    Else
            '        Call FillEmployee()
            '    End If

            'Else
            '    tvAccess.Enabled = False
            '    chkSelectAll.Enabled = False
            'End If
            mstrAdvanceFilter = ""
            Call FillEmployee()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub dgvReviewer_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReviewer.CellContentClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If dgvReviewer.IsCurrentCellDirty Then
                dgvReviewer.CommitEdit(DataGridViewDataErrorContexts.Commit)
                dtReviewer.AcceptChanges()
            End If

            SetCheckBoxValue()

            'Pinkal (12-Jun-2012) -- Start
            'Enhancement : TRA Changes

            'If CBool(dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhEIsGrp.Index).Value) = True Then
            '    Select Case CInt(e.ColumnIndex)
            '        Case 0
            '            If dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhECollapse.Index).Value Is "-" Then
            '                dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhECollapse.Index).Value = "+"
            '            Else
            '                dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhECollapse.Index).Value = "-"
            '            End If

            '            For i = e.RowIndex + 1 To dgvReviewer.RowCount - 1
            '                If CInt(dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhEGrpId.Index).Value) = CInt(dgvReviewer.Rows(i).Cells(objdgcolhEGrpId.Index).Value) Then
            '                    If dgvReviewer.Rows(i).Visible = False Then
            '                        dgvReviewer.Rows(i).Visible = True
            '                    Else
            '                        dgvReviewer.Rows(i).Visible = False
            '                    End If
            '                Else
            '                    Exit For
            '                End If
            '            Next
            '        Case 1
            '            For i = e.RowIndex + 1 To dgvReviewer.RowCount - 1
            '                Dim blnFlg As Boolean = CBool(dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
            '                If CInt(dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhEGrpId.Index).Value) = CInt(dgvReviewer.Rows(i).Cells(objdgcolhEGrpId.Index).Value) Then
            '                    dgvReviewer.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
            '                Else
            '                    Exit For
            '                End If
            '            Next
            '    End Select
            'End If

            dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = CBool(dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)

            'Pinkal (12-Jun-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvReviewer_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes

    'Private Sub dgvReviewer_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvReviewer.DataBindingComplete
    '    Try
    '        'Call SetRevieweGridStyle()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvReviewer_DataBindingComplete", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Pinkal (06-Feb-2012) -- End


    'S.SANDEEP [ 23 JAN 2012 ] -- END

#End Region


    'Pinkal (12-Jun-2012) -- Start
    'Enhancement : TRA Changes

#Region "LinkLabel Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                FillEmployee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSelectAll.CheckedChanged
        Try

            For Each dr As DataRow In dtReviewer.Rows
                RemoveHandler dgvReviewer.CellContentClick, AddressOf dgvReviewer_CellContentClick
                dr("ischeck") = objSelectAll.Checked
                AddHandler dgvReviewer.CellContentClick, AddressOf dgvReviewer_CellContentClick
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (12-Jun-2012) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAssessorAccess.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAssessorAccess.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAssessorAccess.Text = Language._Object.getCaption(Me.gbAssessorAccess.Name, Me.gbAssessorAccess.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

 



End Class