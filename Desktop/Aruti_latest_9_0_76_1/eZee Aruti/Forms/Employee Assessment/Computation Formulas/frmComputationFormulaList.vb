﻿Option Strict On
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmComputationFormulaList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmComputationFormulaList"
    Private objComputation As clsassess_computation_master

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objMaster.getComboListForAssessmentComputationFunction("List", True)
            With cboFormulaFor
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnNew.Enabled = User._Object.Privilege._AllowtoAddComputationFormula
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteComputationFormula
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim iSearch As String = String.Empty
        Dim mdtTable As DataTable
        Dim dsList As New DataSet
        Try
            dsList = objComputation.GetList("List")
            lvFormulaList.Items.Clear()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                iSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If

            If CInt(cboFormulaFor.SelectedValue) > 0 Then
                iSearch &= "AND formula_typeid = '" & CInt(cboFormulaFor.SelectedValue) & "' "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'If iSearch.Trim.Length > 0 Then
            '    iSearch = iSearch.Substring(3)
            '    mdtTable = New DataView(dsList.Tables(0), iSearch, "Formula_Type", DataViewRowState.CurrentRows).ToTable
            'Else
            '    mdtTable = New DataView(dsList.Tables(0), "", "Formula_Type", DataViewRowState.CurrentRows).ToTable
            'End If
            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtTable = New DataView(dsList.Tables(0), iSearch, "start_date,Formula_Type", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsList.Tables(0), "", "start_date,Formula_Type", DataViewRowState.CurrentRows).ToTable
            End If
            'S.SANDEEP [04 JUN 2015] -- END



            For Each dRow As DataRow In mdtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dRow.Item("Formula_Type").ToString
                lvItem.SubItems.Add(dRow.Item("Formula_Defined").ToString)
                lvItem.SubItems.Add(dRow.Item("PName").ToString)
                lvItem.SubItems.Add(dRow.Item("PStatId").ToString)
                lvItem.SubItems.Add(dRow.Item("computation_formula").ToString)
                'Shani (24-May-2016) -- Stat
                lvItem.SubItems.Add(dRow.Item("formula_typeid").ToString)
                'Shani (24-May-2016) -- End

                If CInt(dRow.Item("PStatId")) = enStatusType.Close Then
                    lvItem.ForeColor = Color.Gray
                End If

                lvItem.Tag = dRow.Item("computationunkid")

                lvFormulaList.Items.Add(lvItem)
            Next

            lvFormulaList.GroupingColumn = objcolhPeriodName
            lvFormulaList.DisplayGroups(True)

            If lvFormulaList.Items.Count >= 2 Then
                colhFormulaDefined.Width = 485 - 20
            Else
                colhFormulaDefined.Width = 485
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmComputationFormulaList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objComputation = New clsassess_computation_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            Call SetVisibility()

            If lvFormulaList.Items.Count > 0 Then lvFormulaList.Items(0).Selected = True
            lvFormulaList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmComputationFormulaList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmComputationFormulaList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvFormulaList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmComputationFormulaList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmComputationFormulaList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objComputation = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_computation_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_computation_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmComputationFormula_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE, "") Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmComputationFormula_AddEdit
        Try
            If lvFormulaList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select formula from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvFormulaList.Select()
                Exit Sub
            End If

            'Shani (24-May-2016) -- Start
            'If objComputation.isUsed(lvFormulaList.SelectedItems(0).SubItems(objcolhFormula.Index).Text) = True Then
            If objComputation.isUsed(CInt(lvFormulaList.SelectedItems(0).Tag), CInt(lvFormulaList.SelectedItems(0).SubItems(objcolhFormulaTypeId.Index).Text)) = True Then
                'Shani (24-May-2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot edit this formula its already used in computation score."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(CInt(lvFormulaList.SelectedItems(0).Tag), enAction.EDIT_ONE, lvFormulaList.SelectedItems(0).SubItems(colhFormulaDefined.Index).Text) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvFormulaList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select formula from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvFormulaList.Select()
                Exit Sub
            End If


            'Shani (24-May-2016) -- Start
            'If objComputation.isUsed(lvFormulaList.SelectedItems(0).SubItems(objcolhFormula.Index).Text) = True Then
            If objComputation.isUsed(CInt(lvFormulaList.SelectedItems(0).Tag), CInt(lvFormulaList.SelectedItems(0).SubItems(objcolhFormulaTypeId.Index).Text)) = True Then
                'Shani (24-May-2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this formula its already used in compution score."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this formula?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                Dim xStrVoidReason = String.Empty

                frm.displayDialog(enVoidCategoryType.ASSESSMENT, xStrVoidReason)

                If xStrVoidReason.Trim.Length <= 0 Then Exit Sub

                objComputation._Isvoid = True
                objComputation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objComputation._Voidreason = xStrVoidReason
                objComputation._Voiduserunkid = User._Object._Userunkid

                If objComputation.Delete(CInt(lvFormulaList.SelectedItems(0).Tag), User._Object._Userunkid) = True Then
                    lvFormulaList.SelectedItems(0).Remove()
                End If
            End If
            lvFormulaList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
            Call objbtnSearch.ShowResult(CStr(lvFormulaList.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0 : cboFormulaFor.SelectedValue = 0
            lvFormulaList.Items.Clear()
            Call objbtnSearch.ShowResult(CStr(lvFormulaList.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Listview Event(s) "

    Private Sub lvFormulaList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvFormulaList.SelectedIndexChanged
        Try
            If lvFormulaList.SelectedItems.Count <= 0 Then Exit Sub

            If lvFormulaList.SelectedItems(0).ForeColor = Color.Gray Then
                btnEdit.Enabled = False : btnDelete.Enabled = False
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True
            End If

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvFormulaList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblFormulaFor.Text = Language._Object.getCaption(Me.lblFormulaFor.Name, Me.lblFormulaFor.Text)
            Me.colhFormulaType.Text = Language._Object.getCaption(CStr(Me.colhFormulaType.Tag), Me.colhFormulaType.Text)
            Me.colhFormulaDefined.Text = Language._Object.getCaption(CStr(Me.colhFormulaDefined.Tag), Me.colhFormulaDefined.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select formula from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this formula?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot edit this formula its already used in computation score.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot delete this formula its already used in compution score.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class