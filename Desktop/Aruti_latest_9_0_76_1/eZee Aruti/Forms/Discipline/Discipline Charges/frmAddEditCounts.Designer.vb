﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddEditCounts
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddEditCounts))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbChargeCount = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddContraryto = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchContraryto = New eZee.Common.eZeeGradientButton
        Me.cboContraryto = New System.Windows.Forms.ComboBox
        Me.lblContraryto = New System.Windows.Forms.Label
        Me.txtIncident = New eZee.TextBox.AlphanumericTextBox
        Me.chkAssignResponse = New System.Windows.Forms.CheckBox
        Me.lblIncident = New System.Windows.Forms.Label
        Me.cboOffenceCategory = New System.Windows.Forms.ComboBox
        Me.objbtnAddDisciplinetype = New eZee.Common.eZeeGradientButton
        Me.lblOffenceCategory = New System.Windows.Forms.Label
        Me.objbtnSearchOffence = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnScanAttachDoc = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlResponse = New System.Windows.Forms.Panel
        Me.objbtnAddResponseType = New eZee.Common.eZeeGradientButton
        Me.dtpResponseDate = New System.Windows.Forms.DateTimePicker
        Me.cboResponseType = New System.Windows.Forms.ComboBox
        Me.lblResponse = New System.Windows.Forms.Label
        Me.txtResponse = New System.Windows.Forms.TextBox
        Me.lblReponseDate = New System.Windows.Forms.Label
        Me.lblResponseType = New System.Windows.Forms.Label
        Me.objbtnSearchResponseType = New eZee.Common.eZeeGradientButton
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.pnlMain.SuspendLayout()
        Me.gbChargeCount.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlResponse.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbChargeCount)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.pnlResponse)
        Me.pnlMain.Controls.Add(Me.objLine1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(541, 313)
        Me.pnlMain.TabIndex = 0
        '
        'gbChargeCount
        '
        Me.gbChargeCount.BorderColor = System.Drawing.Color.Black
        Me.gbChargeCount.Checked = False
        Me.gbChargeCount.CollapseAllExceptThis = False
        Me.gbChargeCount.CollapsedHoverImage = Nothing
        Me.gbChargeCount.CollapsedNormalImage = Nothing
        Me.gbChargeCount.CollapsedPressedImage = Nothing
        Me.gbChargeCount.CollapseOnLoad = False
        Me.gbChargeCount.Controls.Add(Me.objbtnAddContraryto)
        Me.gbChargeCount.Controls.Add(Me.objbtnSearchContraryto)
        Me.gbChargeCount.Controls.Add(Me.cboContraryto)
        Me.gbChargeCount.Controls.Add(Me.lblContraryto)
        Me.gbChargeCount.Controls.Add(Me.txtIncident)
        Me.gbChargeCount.Controls.Add(Me.chkAssignResponse)
        Me.gbChargeCount.Controls.Add(Me.lblIncident)
        Me.gbChargeCount.Controls.Add(Me.cboOffenceCategory)
        Me.gbChargeCount.Controls.Add(Me.objbtnAddDisciplinetype)
        Me.gbChargeCount.Controls.Add(Me.lblOffenceCategory)
        Me.gbChargeCount.Controls.Add(Me.objbtnSearchOffence)
        Me.gbChargeCount.Controls.Add(Me.objbtnSearchCategory)
        Me.gbChargeCount.Controls.Add(Me.cboDisciplineType)
        Me.gbChargeCount.Controls.Add(Me.lblDisciplineType)
        Me.gbChargeCount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbChargeCount.ExpandedHoverImage = Nothing
        Me.gbChargeCount.ExpandedNormalImage = Nothing
        Me.gbChargeCount.ExpandedPressedImage = Nothing
        Me.gbChargeCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbChargeCount.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbChargeCount.HeaderHeight = 25
        Me.gbChargeCount.HeaderMessage = ""
        Me.gbChargeCount.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbChargeCount.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbChargeCount.HeightOnCollapse = 0
        Me.gbChargeCount.LeftTextSpace = 0
        Me.gbChargeCount.Location = New System.Drawing.Point(0, 0)
        Me.gbChargeCount.Name = "gbChargeCount"
        Me.gbChargeCount.OpenHeight = 300
        Me.gbChargeCount.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbChargeCount.ShowBorder = True
        Me.gbChargeCount.ShowCheckBox = False
        Me.gbChargeCount.ShowCollapseButton = False
        Me.gbChargeCount.ShowDefaultBorderColor = True
        Me.gbChargeCount.ShowDownButton = False
        Me.gbChargeCount.ShowHeader = True
        Me.gbChargeCount.Size = New System.Drawing.Size(541, 258)
        Me.gbChargeCount.TabIndex = 1
        Me.gbChargeCount.Temp = 0
        Me.gbChargeCount.Text = "Count Information"
        Me.gbChargeCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddContraryto
        '
        Me.objbtnAddContraryto.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddContraryto.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddContraryto.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddContraryto.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddContraryto.BorderSelected = False
        Me.objbtnAddContraryto.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddContraryto.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddContraryto.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddContraryto.Location = New System.Drawing.Point(510, 90)
        Me.objbtnAddContraryto.Name = "objbtnAddContraryto"
        Me.objbtnAddContraryto.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddContraryto.TabIndex = 153
        '
        'objbtnSearchContraryto
        '
        Me.objbtnSearchContraryto.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchContraryto.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchContraryto.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchContraryto.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchContraryto.BorderSelected = False
        Me.objbtnSearchContraryto.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchContraryto.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchContraryto.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchContraryto.Location = New System.Drawing.Point(483, 90)
        Me.objbtnSearchContraryto.Name = "objbtnSearchContraryto"
        Me.objbtnSearchContraryto.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchContraryto.TabIndex = 154
        '
        'cboContraryto
        '
        Me.cboContraryto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContraryto.DropDownWidth = 500
        Me.cboContraryto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboContraryto.FormattingEnabled = True
        Me.cboContraryto.Location = New System.Drawing.Point(132, 90)
        Me.cboContraryto.Name = "cboContraryto"
        Me.cboContraryto.Size = New System.Drawing.Size(345, 21)
        Me.cboContraryto.TabIndex = 152
        '
        'lblContraryto
        '
        Me.lblContraryto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContraryto.Location = New System.Drawing.Point(12, 92)
        Me.lblContraryto.Name = "lblContraryto"
        Me.lblContraryto.Size = New System.Drawing.Size(114, 17)
        Me.lblContraryto.TabIndex = 151
        Me.lblContraryto.Text = "Contrary to"
        Me.lblContraryto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIncident
        '
        Me.txtIncident.BackColor = System.Drawing.SystemColors.Window
        Me.txtIncident.Flags = 0
        Me.txtIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncident.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtIncident.Location = New System.Drawing.Point(132, 117)
        Me.txtIncident.MaxLength = 2147483647
        Me.txtIncident.Multiline = True
        Me.txtIncident.Name = "txtIncident"
        Me.txtIncident.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIncident.Size = New System.Drawing.Size(345, 129)
        Me.txtIncident.TabIndex = 138
        '
        'chkAssignResponse
        '
        Me.chkAssignResponse.BackColor = System.Drawing.Color.Transparent
        Me.chkAssignResponse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAssignResponse.Location = New System.Drawing.Point(383, 4)
        Me.chkAssignResponse.Name = "chkAssignResponse"
        Me.chkAssignResponse.Size = New System.Drawing.Size(155, 17)
        Me.chkAssignResponse.TabIndex = 149
        Me.chkAssignResponse.Text = "Add With Response"
        Me.chkAssignResponse.UseVisualStyleBackColor = False
        Me.chkAssignResponse.Visible = False
        '
        'lblIncident
        '
        Me.lblIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncident.Location = New System.Drawing.Point(12, 121)
        Me.lblIncident.Name = "lblIncident"
        Me.lblIncident.Size = New System.Drawing.Size(114, 17)
        Me.lblIncident.TabIndex = 139
        Me.lblIncident.Text = "Incident Description"
        '
        'cboOffenceCategory
        '
        Me.cboOffenceCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOffenceCategory.DropDownWidth = 300
        Me.cboOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOffenceCategory.FormattingEnabled = True
        Me.cboOffenceCategory.Location = New System.Drawing.Point(132, 36)
        Me.cboOffenceCategory.Name = "cboOffenceCategory"
        Me.cboOffenceCategory.Size = New System.Drawing.Size(345, 21)
        Me.cboOffenceCategory.TabIndex = 132
        '
        'objbtnAddDisciplinetype
        '
        Me.objbtnAddDisciplinetype.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddDisciplinetype.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddDisciplinetype.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddDisciplinetype.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddDisciplinetype.BorderSelected = False
        Me.objbtnAddDisciplinetype.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddDisciplinetype.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddDisciplinetype.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddDisciplinetype.Location = New System.Drawing.Point(510, 63)
        Me.objbtnAddDisciplinetype.Name = "objbtnAddDisciplinetype"
        Me.objbtnAddDisciplinetype.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddDisciplinetype.TabIndex = 136
        '
        'lblOffenceCategory
        '
        Me.lblOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffenceCategory.Location = New System.Drawing.Point(12, 38)
        Me.lblOffenceCategory.Name = "lblOffenceCategory"
        Me.lblOffenceCategory.Size = New System.Drawing.Size(114, 17)
        Me.lblOffenceCategory.TabIndex = 131
        Me.lblOffenceCategory.Text = "Offence Category"
        Me.lblOffenceCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOffence
        '
        Me.objbtnSearchOffence.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOffence.BorderSelected = False
        Me.objbtnSearchOffence.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOffence.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOffence.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOffence.Location = New System.Drawing.Point(483, 63)
        Me.objbtnSearchOffence.Name = "objbtnSearchOffence"
        Me.objbtnSearchOffence.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOffence.TabIndex = 137
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(483, 36)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 133
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.DropDownWidth = 1000
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(132, 63)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(345, 21)
        Me.cboDisciplineType.TabIndex = 135
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(12, 65)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(114, 17)
        Me.lblDisciplineType.TabIndex = 134
        Me.lblDisciplineType.Text = "Offence Description"
        Me.lblDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnScanAttachDoc)
        Me.objFooter.Controls.Add(Me.objbtnProcess)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 258)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(541, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnScanAttachDoc
        '
        Me.btnScanAttachDoc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnScanAttachDoc.BackColor = System.Drawing.Color.White
        Me.btnScanAttachDoc.BackgroundImage = CType(resources.GetObject("btnScanAttachDoc.BackgroundImage"), System.Drawing.Image)
        Me.btnScanAttachDoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnScanAttachDoc.BorderColor = System.Drawing.Color.Empty
        Me.btnScanAttachDoc.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnScanAttachDoc.FlatAppearance.BorderSize = 0
        Me.btnScanAttachDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnScanAttachDoc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScanAttachDoc.ForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnScanAttachDoc.GradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Location = New System.Drawing.Point(12, 13)
        Me.btnScanAttachDoc.Name = "btnScanAttachDoc"
        Me.btnScanAttachDoc.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Size = New System.Drawing.Size(164, 30)
        Me.btnScanAttachDoc.TabIndex = 18
        Me.btnScanAttachDoc.Text = "&Browse"
        Me.btnScanAttachDoc.UseVisualStyleBackColor = True
        Me.btnScanAttachDoc.Visible = False
        '
        'objbtnProcess
        '
        Me.objbtnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnProcess.BackColor = System.Drawing.Color.White
        Me.objbtnProcess.BackgroundImage = CType(resources.GetObject("objbtnProcess.BackgroundImage"), System.Drawing.Image)
        Me.objbtnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnProcess.BorderColor = System.Drawing.Color.Empty
        Me.objbtnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnProcess.FlatAppearance.BorderSize = 0
        Me.objbtnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnProcess.ForeColor = System.Drawing.Color.Black
        Me.objbtnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnProcess.Location = New System.Drawing.Point(329, 13)
        Me.objbtnProcess.Name = "objbtnProcess"
        Me.objbtnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnProcess.Size = New System.Drawing.Size(97, 30)
        Me.objbtnProcess.TabIndex = 0
        Me.objbtnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(432, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlResponse
        '
        Me.pnlResponse.Controls.Add(Me.objbtnAddResponseType)
        Me.pnlResponse.Controls.Add(Me.dtpResponseDate)
        Me.pnlResponse.Controls.Add(Me.cboResponseType)
        Me.pnlResponse.Controls.Add(Me.lblResponse)
        Me.pnlResponse.Controls.Add(Me.txtResponse)
        Me.pnlResponse.Controls.Add(Me.lblReponseDate)
        Me.pnlResponse.Controls.Add(Me.lblResponseType)
        Me.pnlResponse.Controls.Add(Me.objbtnSearchResponseType)
        Me.pnlResponse.Location = New System.Drawing.Point(3, 93)
        Me.pnlResponse.Name = "pnlResponse"
        Me.pnlResponse.Size = New System.Drawing.Size(535, 126)
        Me.pnlResponse.TabIndex = 148
        '
        'objbtnAddResponseType
        '
        Me.objbtnAddResponseType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResponseType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResponseType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResponseType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResponseType.BorderSelected = False
        Me.objbtnAddResponseType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResponseType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResponseType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResponseType.Location = New System.Drawing.Point(507, 2)
        Me.objbtnAddResponseType.Name = "objbtnAddResponseType"
        Me.objbtnAddResponseType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResponseType.TabIndex = 147
        '
        'dtpResponseDate
        '
        Me.dtpResponseDate.Checked = False
        Me.dtpResponseDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpResponseDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpResponseDate.Location = New System.Drawing.Point(129, 2)
        Me.dtpResponseDate.Name = "dtpResponseDate"
        Me.dtpResponseDate.ShowCheckBox = True
        Me.dtpResponseDate.Size = New System.Drawing.Size(102, 21)
        Me.dtpResponseDate.TabIndex = 140
        '
        'cboResponseType
        '
        Me.cboResponseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResponseType.DropDownWidth = 300
        Me.cboResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResponseType.FormattingEnabled = True
        Me.cboResponseType.Location = New System.Drawing.Point(320, 2)
        Me.cboResponseType.Name = "cboResponseType"
        Me.cboResponseType.Size = New System.Drawing.Size(154, 21)
        Me.cboResponseType.TabIndex = 143
        '
        'lblResponse
        '
        Me.lblResponse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponse.Location = New System.Drawing.Point(9, 32)
        Me.lblResponse.Name = "lblResponse"
        Me.lblResponse.Size = New System.Drawing.Size(114, 17)
        Me.lblResponse.TabIndex = 144
        Me.lblResponse.Text = "Reponse Description"
        '
        'txtResponse
        '
        Me.txtResponse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResponse.Location = New System.Drawing.Point(129, 29)
        Me.txtResponse.Multiline = True
        Me.txtResponse.Name = "txtResponse"
        Me.txtResponse.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtResponse.Size = New System.Drawing.Size(345, 94)
        Me.txtResponse.TabIndex = 145
        '
        'lblReponseDate
        '
        Me.lblReponseDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReponseDate.Location = New System.Drawing.Point(9, 4)
        Me.lblReponseDate.Name = "lblReponseDate"
        Me.lblReponseDate.Size = New System.Drawing.Size(114, 17)
        Me.lblReponseDate.TabIndex = 141
        Me.lblReponseDate.Text = "Reponse Date"
        Me.lblReponseDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblResponseType
        '
        Me.lblResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponseType.Location = New System.Drawing.Point(237, 4)
        Me.lblResponseType.Name = "lblResponseType"
        Me.lblResponseType.Size = New System.Drawing.Size(77, 17)
        Me.lblResponseType.TabIndex = 142
        Me.lblResponseType.Text = "Reponse Type"
        Me.lblResponseType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchResponseType
        '
        Me.objbtnSearchResponseType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResponseType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResponseType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResponseType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResponseType.BorderSelected = False
        Me.objbtnSearchResponseType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResponseType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResponseType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResponseType.Location = New System.Drawing.Point(480, 2)
        Me.objbtnSearchResponseType.Name = "objbtnSearchResponseType"
        Me.objbtnSearchResponseType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResponseType.TabIndex = 146
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(3, 73)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(535, 17)
        Me.objLine1.TabIndex = 147
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmAddEditCounts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(541, 313)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAddEditCounts"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Count(s)"
        Me.pnlMain.ResumeLayout(False)
        Me.gbChargeCount.ResumeLayout(False)
        Me.gbChargeCount.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.pnlResponse.ResumeLayout(False)
        Me.pnlResponse.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents objbtnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboOffenceCategory As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOffenceCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnAddDisciplinetype As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchOffence As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents lblIncident As System.Windows.Forms.Label
    Friend WithEvents txtIncident As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbChargeCount As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpResponseDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReponseDate As System.Windows.Forms.Label
    Friend WithEvents txtResponse As System.Windows.Forms.TextBox
    Friend WithEvents lblResponse As System.Windows.Forms.Label
    Friend WithEvents cboResponseType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchResponseType As eZee.Common.eZeeGradientButton
    Friend WithEvents lblResponseType As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents pnlResponse As System.Windows.Forms.Panel
    Friend WithEvents chkAssignResponse As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnAddResponseType As eZee.Common.eZeeGradientButton
    Friend WithEvents btnScanAttachDoc As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAddContraryto As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchContraryto As eZee.Common.eZeeGradientButton
    Friend WithEvents cboContraryto As System.Windows.Forms.ComboBox
    Friend WithEvents lblContraryto As System.Windows.Forms.Label
End Class
