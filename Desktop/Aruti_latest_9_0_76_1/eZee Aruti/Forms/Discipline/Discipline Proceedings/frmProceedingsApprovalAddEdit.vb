﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmProceedingsApprovalAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmProceedingsApprovalAddEdit"
    Private objProceedingMaster As clsdiscipline_proceeding_master
    Private objMembersTran As clsdiscipline_members_tran
    Private objDisciplineFileMaster As clsDiscipline_file_master
    Private objDisciplineFileTran As clsDiscipline_file_tran
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtProceeding As DataTable
    Private mdtMemTran As DataTable
    Private mdtEmployee As DataTable
    Private mdtReference As DataTable
    Private mdtFileTran As DataTable
    Private mdtInvestigator As DataTable
    Private mdsCommittee As DataSet
    Private mintDisciplineProceedingMasterunkid As Integer = -1
    Private mintCommitteeMasterId As Integer = -1
    Private mintChargeCount As Integer = -1
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private dvProceeding As DataView
    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private mdtScanAttachDocument As DataTable
    'S.SANDEEP |11-NOV-2019| -- END
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, _
                                  ByVal intDisciplineProceedingMasterunkid As Integer) As Boolean
        Try
            menAction = eAction
            mintDisciplineProceedingMasterunkid = intDisciplineProceedingMasterunkid
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmProceedingsApprovalAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmProceedingsApprovalAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                'btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProceedingsApprovalAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProceedingsApprovalAddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsdiscipline_proceeding_master.SetMessages()
            objfrm._Other_ModuleNames = "clsdiscipline_proceeding_master"
            objfrm.displayDialog(Me)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProceedingsApprovalAddEdit_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmProceedingsApprovalAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objProceedingMaster = New clsdiscipline_proceeding_master
        objMembersTran = New clsdiscipline_members_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboReferenceNo.Enabled = False
                objbtnSearchReferenceNo.Enabled = False
                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'objProceedingMaster._DisciplineProceedingMasterunkid = mintDisciplineProceedingMasterunkid
                'mdtProceeding = objProceedingMaster._ProceedingTable
                'S.SANDEEP |11-NOV-2019| -- END
            End If

            objProceedingMaster._DisciplineProceedingMasterunkid = mintDisciplineProceedingMasterunkid
            mdtProceeding = objProceedingMaster._ProceedingTable
            objMembersTran._DisciplineProceedingMasterunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintDisciplineProceedingMasterunkid
            mdtMemTran = objMembersTran._MemTranTable

            Call GetValue()

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            Dim objAttachDocs As New clsScan_Attach_Documents
            'objAttachDocs.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
            objAttachDocs.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            Dim xRow = objAttachDocs._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES _
                                                                         And x.Field(Of Integer)("transactionunkid") = mintDisciplineProceedingMasterunkid _
                                                                         And x.Field(Of String)("form_name") = "'frmDisciplineProceedingList'")
            If xRow.Count > 0 Then
                mdtScanAttachDocument = xRow.CopyToDataTable()
            Else
                mdtScanAttachDocument = objAttachDocs._Datatable.Clone
            End If
            objAttachDocs = Nothing
            'S.SANDEEP |11-NOV-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProceedingsApprovalAddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objDscplnFileMaster As New clsDiscipline_file_master
            Dim objActionReason As New clsAction_Reason
            Dim objCommonMaster As New clsCommon_Master

            dsList = objDscplnFileMaster.GetComboList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , , 2, , )

            mdtEmployee = dsList.Tables("List").DefaultView.ToTable(True, "involved_employeeunkid", "employeecode", "employeename")
            RemoveHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged
            With cboEmployee
                .ValueMember = "involved_employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = mdtEmployee
                .SelectedValue = 0
            End With
            AddHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged

            mdtReference = dsList.Tables("List")
            RemoveHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtReference
                .SelectedValue = 0
            End With
            RemoveHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try

    End Sub

    Private Sub GetValueByPersonInvolved()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim objJob As New clsJobs
            Dim objDepartment As New clsDepartment

            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

            objJob._Jobunkid = objEmployee._Jobunkid
            objDepartment._Departmentunkid = objEmployee._Departmentunkid

            txtJobTitle.Text = objJob._Job_Name
            txtDepartment.Text = objDepartment._Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValuePersonInvolved", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValueByReferenceNo()
        Try
            Dim objDisciplineFileMaster As New clsDiscipline_file_master
            objDisciplineFileMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
            txtChargeDate.Text = CStr(CDate(objDisciplineFileMaster._Chargedate).Date)
            If objDisciplineFileMaster._Interdictiondate <> Nothing Then
                txtInterdictionDate.Text = CStr(CDate(objDisciplineFileMaster._Interdictiondate).Date)
            Else
                txtInterdictionDate.Text = ""
            End If
            txtChargeDescription.Text = objDisciplineFileMaster._Charge_Description
            objDisciplineFileMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValueByReferenceNo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterReferenceNo()
        Try
            Dim strCsvData As String = ""
            strCsvData = String.Join(",", mdtReference.AsEnumerable().Where(Function(x) x.Field(Of Integer)("involved_employeeunkid") = CInt(cboEmployee.SelectedValue)).Select(Function(y) y.Field(Of Integer)("disciplinefileunkid").ToString).ToArray())
            Dim mdtView As DataView = New DataView(mdtReference, "disciplinefileunkid IN (" & strCsvData & ") ", "", DataViewRowState.CurrentRows)
            RemoveHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtView.ToTable
                If mdtView.ToTable.Rows.Count = 1 Then
                    .SelectedValue = mdtView.ToTable.Rows(0).Item("disciplinefileunkid")
                    Call GetValueByReferenceNo()
                Else
                    .SelectedValue = 0
                    txtChargeDate.Text = ""
                    txtChargeDescription.Text = ""
                    txtInterdictionDate.Text = ""
                End If
            End With
            AddHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterReferenceNo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If menAction = enAction.EDIT_ONE Then
                objDisciplineFileMaster = New clsDiscipline_file_master
                If mdtProceeding IsNot Nothing Then
                    objDisciplineFileMaster._Disciplinefileunkid = CInt(mdtProceeding.Rows(0).Item("disciplinefileunkid"))
                    cboEmployee.SelectedValue = objDisciplineFileMaster._Involved_Employeeunkid
                    cboReferenceNo.SelectedValue = objDisciplineFileMaster._Disciplinefileunkid
                End If
            End If
            Call SetProceedingData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            'S.SANDEEP [24 OCT 2016] -- START
            'If dgvProceedingDetails.RowCount <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
            '    Return False
            'End If

            'If mdtProceeding IsNot Nothing Then
            '    mdtProceeding.AcceptChanges()
            '    If mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of String)("count") <> "" And x.Field(Of String)("AUD") <> "D").Count <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
            '        Return False
            '    End If
            'End If

            If menAction <> enAction.EDIT_ONE Then
                If dgvProceedingDetails.RowCount <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
                    Return False
                End If

                If mdtProceeding IsNot Nothing Then
                    mdtProceeding.AcceptChanges()
                    If ConfigParameter._Object._AddProceedingAgainstEachCount Then
                        If mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of String)("count") <> "" And x.Field(Of String)("AUD") <> "D").Count <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    Else
                        If mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Count <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                End If
            End If
            'S.SANDEEP [24 OCT 2016] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetProceedingData()
        Try
            dvProceeding = mdtProceeding.DefaultView

            If mdtProceeding.Rows.Count > 1 Then
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'dvProceeding.RowFilter = "AUD <> 'D' AND count <> ''"
                If ConfigParameter._Object._AddProceedingAgainstEachCount Then
                    dvProceeding.RowFilter = "AUD <> 'D' AND count <> ''"
                Else
                    dvProceeding.RowFilter = "AUD <> 'D' AND disciplinefileunkid > 0 "
                End If
                'S.SANDEEP |01-OCT-2019| -- END
            Else
                dvProceeding.RowFilter = "AUD <> 'D'"
            End If

            dgvProceedingDetails.AutoGenerateColumns = False

            dgcolhCount.DataPropertyName = "count"
            dgcolhProceedingDate.DataPropertyName = "proceeding_date"
            dgcolhProceedingDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhProceedingDetails.DataPropertyName = "proceeding_details"
            dgcolhDisciplineAction.DataPropertyName = "reason_action"
            dgcolhCommittee.DataPropertyName = "committee"
            dgcolhInvestigator.DataPropertyName = "investigator"
            dgcolhPIComments.DataPropertyName = "person_involved_comments"
            dgcolhCommentRemark.DataPropertyName = "remarks_comments"
            dgcolhApproved.DataPropertyName = "approved_status"
            dgcolhCountStatus.DataPropertyName = "status_name"
            objdgcolhGUID.DataPropertyName = "GUID"
            objcolhdisciplinefiletranunkid.DataPropertyName = "disciplinefiletranunkid"
            objdgcolhProceedingMastId.DataPropertyName = "disciplineproceedingmasterunkid"


            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            dgcolhPenaltyEffectiveDate.DataPropertyName = "penalty_effectivedate"
            dgcolhPenaltyExpiryDate.DataPropertyName = "penalty_expirydate"
            'Pinkal (19-Dec-2020) -- End


            dgvProceedingDetails.DataSource = mdtProceeding

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            If ConfigParameter._Object._AddProceedingAgainstEachCount = False Then
                dgcolhCount.Visible = False
                If dgvProceedingDetails.RowCount > 0 Then
                    If mdtProceeding.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("disciplinefileunkid")) > 0 And x.Field(Of String)("AUD") <> "D").Count > 0 Then
                        objdgcolhAdd.Visible = False
                    End If
                Else
                    objdgcolhAdd.Visible = True
                End If
            End If
            'S.SANDEEP |01-OCT-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetProceedingData", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetParameters()
        Try
            cboEmployee.SelectedValue = 0
            txtJobTitle.Text = ""
            txtDepartment.Text = ""
            cboReferenceNo.SelectedValue = 0
            txtChargeDate.Text = ""
            txtInterdictionDate.Text = ""
            txtChargeDescription.Text = ""
            objProceedingMaster._DisciplineProceedingMasterunkid = -1
            mdtProceeding = objProceedingMaster._ProceedingTable
            objMembersTran._DisciplineProceedingMasterunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = -1
            mdtMemTran = objMembersTran._MemTranTable
            dgvProceedingDetails.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetParameters", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub
            objProceedingMaster._Userunkid = User._Object._Userunkid
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran, _
            '                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False)
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran, _
            '                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, Company._Object._Companyunkid)

            If mdtScanAttachDocument IsNot Nothing Then 'S.SANDEEP |16-JAN-2020| -- START { ISSUE/ENHANCEMENT : OBJECT REFERENCE ERROR (mdtScanAttachDocument) } -- END
            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist() : Dim strError As String = ""
            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.DISCIPLINES) Select (p.Item("Name").ToString)).FirstOrDefault

            For Each dRow As DataRow In mdtScanAttachDocument.Rows
                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                    Dim intScanAttachRefId As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                            dRow("fileuniquename") = strFileName
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    Dim intScanattachrefid As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then strPath += "/"
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If clsFileUploadDownload.UpdateFile(CStr(dRow("filepath")), strPath, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Move(CStr(dRow("filepath")), strDocLocalPath)

                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If File.Exists(strDocLocalPath) Then
                                File.Delete(strDocLocalPath)
                            End If
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
            Next
            End If

            blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran, mdtScanAttachDocument, _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, Company._Object._Companyunkid, "frmDisciplineProceedingList", False, ConfigParameter._Object._AddProceedingAgainstEachCount) 'S.SANDEEP |25-MAR-2020| -- START {False} -- END
            'S.SANDEEP |11-NOV-2019| -- END

            'Sohail (30 Nov 2017) -- End
            If blnFlag = False AndAlso objProceedingMaster._Message <> "" Then
                eZeeMsgBox.Show(objProceedingMaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    Call ResetParameters()
                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .CodeMember = "employeecode"
                .DataSource = mdtEmployee
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchReferenceNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReferenceNo.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboReferenceNo.DisplayMember
                .ValueMember = cboReferenceNo.ValueMember
                .DataSource = CType(cboReferenceNo.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboReferenceNo.SelectedValue = frm.SelectedValue
                cboReferenceNo.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReferenceNo_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call GetValueByPersonInvolved()
                If menAction <> enAction.EDIT_ONE Then
                    cboReferenceNo.Enabled = True
                    objbtnSearchReferenceNo.Enabled = True
                End If
                Call FilterReferenceNo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged
        Try
            If CInt(cboReferenceNo.SelectedValue) > 0 Then
                Call GetValueByReferenceNo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then

                Dim frm As New frmCommonSearch

                With frm
                    .DisplayMember = cboEmployee.DisplayMember
                    .ValueMember = cboEmployee.ValueMember
                    .CodeMember = "employeecode"
                    .DataSource = mdtEmployee
                End With

                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString

                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReferenceNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboReferenceNo.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then

                Dim frm As New frmCommonSearch

                With frm
                    .DisplayMember = cboReferenceNo.DisplayMember
                    .ValueMember = cboReferenceNo.ValueMember
                    .DataSource = CType(cboReferenceNo.DataSource, DataTable)
                End With

                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString

                If frm.DisplayDialog Then
                    cboReferenceNo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboReferenceNo.Text = ""
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvProceedingDetails_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvProceedingDetails.CellClick
        Dim frm As New frmChargeProceedingDetails
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Select Case e.ColumnIndex
                Case objdgcolhAdd.Index

                    If CInt(cboReferenceNo.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please select reference number. Reference number is mandatory infomration."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    Dim objDisciFile As New clsDiscipline_file_master
                    objDisciFile._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
                    If ConfigParameter._Object._PreventDisciplineChargeunlessEmployeeNotified Then
                        If objDisciFile._Notification_Date = Nothing Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot add proceeding for this charge. Reason: Employee has not been notified for the selected charge."), enMsgBoxStyle.Information)
                            objDisciFile = Nothing
                            Exit Sub
                        End If
                    End If
                    objDisciFile = Nothing

                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    If ConfigParameter._Object._AddProceedingAgainstEachCount = False Then
                        Dim strMessage As String
                        strMessage = objProceedingMaster.IsPostingAllowed(CInt(cboReferenceNo.SelectedValue), 1, Nothing, False)
                        If strMessage.Trim.Length > 0 Then
                            eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END

                    Dim dtMemTran As DataTable = mdtMemTran.Clone
                    dtMemTran.Rows.Clear()
                    'S.SANDEEP |11-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If frm.displayDialog(enAction.ADD_CONTINUE, CInt(cboReferenceNo.SelectedValue), mintDisciplineProceedingMasterunkid, "", mdtProceeding, dtMemTran, False) Then
                    If frm.displayDialog(enAction.ADD_CONTINUE, CInt(cboReferenceNo.SelectedValue), mintDisciplineProceedingMasterunkid, "", mdtProceeding, dtMemTran, mdtScanAttachDocument, False) Then
                        'S.SANDEEP |11-NOV-2019| -- END
                        mdtMemTran.Merge(dtMemTran, True)
                        Call SetProceedingData()
                    End If

                Case objdgcolhEdit.Index

                    If dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub

                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If dgvProceedingDetails.Rows(e.RowIndex).Cells(dgcolhCount.Index).Value.ToString.Trim.Length <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, No Proceeding details is defined to perform edit operation."), enMsgBoxStyle.Information)
                    '    Exit Sub
                    'End If
                    If ConfigParameter._Object._AddProceedingAgainstEachCount Then
                        If dgvProceedingDetails.Rows(e.RowIndex).Cells(dgcolhCount.Index).Value.ToString.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, No Proceeding details is defined to perform edit operation."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END



                    'S.SANDEEP |11-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If frm.displayDialog(enAction.EDIT_ONE, CInt(cboReferenceNo.SelectedValue), mintDisciplineProceedingMasterunkid, dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString, mdtProceeding, mdtMemTran, False) Then
                    If frm.displayDialog(enAction.EDIT_ONE, CInt(cboReferenceNo.SelectedValue), mintDisciplineProceedingMasterunkid, dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString, mdtProceeding, mdtMemTran, mdtScanAttachDocument, False) Then
                        'S.SANDEEP |11-NOV-2019| -- END
                        Call SetProceedingData()
                    End If

                Case objdgcolhDelete.Index

                    If dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If dgvProceedingDetails.Rows(e.RowIndex).Cells(dgcolhCount.Index).Value.ToString.Trim.Length <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No Proceeding details is defined to perform delete operation."), enMsgBoxStyle.Information)
                    '    Exit Sub
                    'End If
                    If ConfigParameter._Object._AddProceedingAgainstEachCount Then
                        If dgvProceedingDetails.Rows(e.RowIndex).Cells(dgcolhCount.Index).Value.ToString.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No Proceeding details is defined to perform delete operation."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END

                    Dim strVoidReason As String = String.Empty
                    Dim dtRow() As DataRow = Nothing
                    If mintDisciplineProceedingMasterunkid > 0 Then
                        dtRow = mdtProceeding.Select("disciplineproceedingmasterunkid = '" & mintDisciplineProceedingMasterunkid & "' AND AUD <> 'D'")
                    Else
                        dtRow = mdtProceeding.Select("GUID = '" & dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                    End If
                    If mintDisciplineProceedingMasterunkid > 0 Then
                        Dim ofrm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            ofrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            ofrm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(ofrm)
                        End If
                        ofrm.displayDialog(enVoidCategoryType.DISCIPLINE, strVoidReason)
                        ofrm.Dispose()
                        If strVoidReason.Trim.Length <= 0 Then Exit Sub
                    End If
                    If strVoidReason.Trim.Length > 0 Then
                        dtRow(0).Item("isvoid") = True
                        dtRow(0).Item("voiduserunkid") = User._Object._Userunkid
                        dtRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtRow(0).Item("voidreason") = strVoidReason
                        dtRow(0).Item("AUD") = "D"
                        Dim dtMem As DataRow() = mdtMemTran.Select("disciplinefiletranunkid = '" & CInt(dtRow(0).Item("disciplinefiletranunkid")) & "'")
                        If dtMem.Length > 0 Then
                            For iRow As Integer = 0 To dtMem.Length - 1
                                dtMem(iRow).Item("isvoid") = True
                                dtMem(iRow).Item("voiduserunkid") = User._Object._Userunkid
                                dtMem(iRow).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                dtMem(iRow).Item("voidreason") = strVoidReason
                                dtMem(iRow).Item("AUD") = "D"
                            Next
                            mdtMemTran.AcceptChanges()
                        End If
                    Else
                        Dim dtMem As DataRow() = mdtMemTran.Select("disciplinefiletranunkid = '" & CInt(dtRow(0).Item("disciplinefiletranunkid")) & "'")
                        If dtMem.Length > 0 Then
                            For iRow As Integer = 0 To dtMem.Length - 1
                                mdtMemTran.Rows.Remove(dtMem(iRow))
                            Next
                        End If
                        mdtProceeding.Rows.Remove(dtRow(0))
                    End If
                    Call SetProceedingData()
                    'S.SANDEEP |11-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    If mdtScanAttachDocument IsNot Nothing AndAlso mdtScanAttachDocument.Rows.Count > 0 Then

                    End If
                    'S.SANDEEP |11-NOV-2019| -- END
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvProceedingDetails_CellClick", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnScanAttachDoc.GradientBackColor = GUI._ButttonBackColor
            Me.btnScanAttachDoc.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnScanAttachDoc.Text = Language._Object.getCaption(Me.btnScanAttachDoc.Name, Me.btnScanAttachDoc.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblChargeDescription.Text = Language._Object.getCaption(Me.lblChargeDescription.Name, Me.lblChargeDescription.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.Name, Me.lblJobTitle.Text)
            Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.elProceedingCountDetails.Text = Language._Object.getCaption(Me.elProceedingCountDetails.Name, Me.elProceedingCountDetails.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.dgcolhCount.HeaderText = Language._Object.getCaption(Me.dgcolhCount.Name, Me.dgcolhCount.HeaderText)
            Me.dgcolhProceedingDate.HeaderText = Language._Object.getCaption(Me.dgcolhProceedingDate.Name, Me.dgcolhProceedingDate.HeaderText)
            Me.dgcolhProceedingDetails.HeaderText = Language._Object.getCaption(Me.dgcolhProceedingDetails.Name, Me.dgcolhProceedingDetails.HeaderText)
            Me.dgcolhPIComments.HeaderText = Language._Object.getCaption(Me.dgcolhPIComments.Name, Me.dgcolhPIComments.HeaderText)
            Me.dgcolhCommentRemark.HeaderText = Language._Object.getCaption(Me.dgcolhCommentRemark.Name, Me.dgcolhCommentRemark.HeaderText)
            Me.dgcolhDisciplineAction.HeaderText = Language._Object.getCaption(Me.dgcolhDisciplineAction.Name, Me.dgcolhDisciplineAction.HeaderText)
            Me.dgcolhCommittee.HeaderText = Language._Object.getCaption(Me.dgcolhCommittee.Name, Me.dgcolhCommittee.HeaderText)
            Me.dgcolhInvestigator.HeaderText = Language._Object.getCaption(Me.dgcolhInvestigator.Name, Me.dgcolhInvestigator.HeaderText)
            Me.dgcolhApproved.HeaderText = Language._Object.getCaption(Me.dgcolhApproved.Name, Me.dgcolhApproved.HeaderText)
            Me.dgcolhCountStatus.HeaderText = Language._Object.getCaption(Me.dgcolhCountStatus.Name, Me.dgcolhCountStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please select reference number. Reference number is mandatory infomration.")
            Language.setMessage(mstrModuleName, 2, "Sorry, No Proceeding details is defined to perform edit operation.")
            Language.setMessage(mstrModuleName, 3, "Sorry, No Proceeding details is defined to perform delete operation.")
            Language.setMessage(mstrModuleName, 4, "Sorry, no proceeding information added into proceeding list.")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot add proceeding for this charge. Reason: Employee has not been notified for the selected charge.")
            Language.setMessage(mstrModuleName, 12, "Configuration Path does not Exist.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmProceedingsApprovalAddEdit"
'    Private objProceedingMaster As clsdiscipline_proceeding_master
'    Private objMembersTran As clsdiscipline_members_tran
'    Dim objDisciplineFileMaster As clsDiscipline_file_master
'    Private objDisciplineFileTran As clsDiscipline_file_tran
'    Private mblnCancel As Boolean = True
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mdtProceeding As DataTable
'    Private mdtMemTran As DataTable
'    Private mdtEmployee As DataTable
'    Private mdtReference As DataTable
'    Private mdtFileTran As DataTable
'    Private mdtInvestigator As DataTable
'    Private mdsCommittee As DataSet
'    Private mintDisciplineProceedingMasterunkid As Integer = -1
'    Private mintCommitteeMasterId As Integer = -1
'    Private mintChargeCount As Integer = -1

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByVal eAction As enAction, ByVal intDisciplineProceedingMasterunkid As Integer) As Boolean
'        Try
'            menAction = eAction
'            mintDisciplineProceedingMasterunkid = intDisciplineProceedingMasterunkid
'            Me.ShowDialog()
'            Return Not mblnCancel

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Form's Events "

'    Private Sub frmProceedingsApprovalAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        Me.Close()
'    End Sub

'    Private Sub frmProceedingsApprovalAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Keys.S Then
'                'btnSave_Click(sender, e)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmProceedingsApprovalAddEdit_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmProceedingsApprovalAddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsdiscipline_proceeding_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsdiscipline_proceeding_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmProceedingsApprovalAddEdit_LanguageClick", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmProceedingsApprovalAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objProceedingMaster = New clsdiscipline_proceeding_master
'        objMembersTran = New clsdiscipline_members_tran
'        Try
'            Call Set_Logo(Me, gApplicationType)

'            Language.setLanguage(Me.Name)
'            Call OtherSettings()

'            Call FillCombo()

'            If menAction = enAction.EDIT_ONE Then
'                cboEmployee.Enabled = False
'                objbtnSearchEmployee.Enabled = False
'                cboReferenceNo.Enabled = False
'                objbtnSearchReferenceNo.Enabled = False
'                objbtnAddCommittee.Enabled = False
'                objbtnAddInvestigator.Enabled = False
'                objProceedingMaster._DisciplineProceedingMasterunkid = mintDisciplineProceedingMasterunkid
'                mdtProceeding = objProceedingMaster._ProceedingTable
'            End If

'            objProceedingMaster._DisciplineProceedingMasterunkid = mintDisciplineProceedingMasterunkid
'            mdtProceeding = objProceedingMaster._ProceedingTable

'            objMembersTran._DisciplineProceedingMasterunkid = mintDisciplineProceedingMasterunkid
'            mdtMemTran = objMembersTran._MemTranTable

'            Call GetValue()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmProceedingsApprovalAddEdit_Load", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Try
'            Dim dsList As New DataSet
'            Dim objDscplnFileMaster As New clsDiscipline_file_master
'            Dim objActionReason As New clsAction_Reason
'            Dim objCommonMaster As New clsCommon_Master

'            dsList = objDscplnFileMaster.GetComboList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
'                                                      Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
'                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , , , , )

'            mdtEmployee = dsList.Tables("List").DefaultView.ToTable(True, "involved_employeeunkid", "employeecode", "employeename")

'            RemoveHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged
'            With cboEmployee
'                .ValueMember = "involved_employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = mdtEmployee
'                .SelectedValue = 0
'            End With
'            AddHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged

'            mdtReference = dsList.Tables("List")

'            RemoveHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
'            With cboReferenceNo
'                .ValueMember = "disciplinefileunkid"
'                .DisplayMember = "reference_no"
'                .DataSource = mdtReference
'                .SelectedValue = 0
'            End With
'            AddHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged

'            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
'            With cboCommittee
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 0
'            End With

'            dsList = objActionReason.getComboList("List", True, True)
'            With cboDisciplinaryAction
'                .DisplayMember = "name"
'                .ValueMember = "actionreasonunkid"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 0
'            End With

'            dsList = objProceedingMaster.getProceedingCountStatus("Count", True)
'            With cboCountStatus
'                .ValueMember = "id"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Count")
'                .SelectedValue = 0
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillComboCount()
'        Try
'            Dim dR As DataRow = mdtFileTran.NewRow
'            dR.Item("disciplinefiletranunkid") = 0
'            dR.Item("display_incident") = "Select"
'            mdtFileTran.Rows.InsertAt(dR, 0)

'            With cboChargesCount
'                .ValueMember = "disciplinefiletranunkid"
'                .DisplayMember = "display_incident"
'                .DataSource = mdtFileTran
'                .SelectedValue = 0
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillComboCount", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillChargesCountList()
'        Try
'            lvChargesCount.Items.Clear()

'            For Each dRow As DataRow In mdtProceeding.Rows
'                If dRow.Item("AUD").ToString <> "D" Then
'                    Dim lvitem As New ListViewItem
'                    lvitem.Text = dRow.Item("count").ToString
'                    lvitem.SubItems.Add(Format(dRow.Item("proceeding_date"), Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern))
'                    lvitem.SubItems.Add(dRow.Item("proceeding_details").ToString)
'                    lvitem.SubItems.Add(dRow.Item("reason_action").ToString)
'                    lvitem.SubItems.Add(dRow.Item("committee").ToString)
'                    lvitem.SubItems.Add(dRow.Item("investigator").ToString)
'                    lvitem.SubItems.Add(dRow.Item("person_involved_comments").ToString)
'                    If CBool(dRow.Item("isapproved")) = False Then
'                        lvitem.SubItems.Add(Language.getMessage(mstrModuleName, 9, "Pending"))
'                    ElseIf CBool(dRow.Item("isapproved")) = True Then
'                        lvitem.SubItems.Add(Language.getMessage(mstrModuleName, 10, "Approved"))
'                    End If
'                    If CInt(dRow.Item("count_status")) = clsdiscipline_proceeding_master.enProceedingCountStatus.Open Then
'                        lvitem.SubItems.Add(Language.getMessage(mstrModuleName, 7, "Open"))
'                    ElseIf CInt(dRow.Item("count_status")) = clsdiscipline_proceeding_master.enProceedingCountStatus.Close Then
'                        lvitem.SubItems.Add(Language.getMessage(mstrModuleName, 8, "Close"))
'                    End If
'                    lvitem.SubItems.Add(dRow.Item("GUID").ToString)
'                    lvitem.SubItems.Add(dRow.Item("disciplinefiletranunkid").ToString)
'                    'lvitem.SubItems.Add(dRow.Item("count_status").ToString)

'                    lvitem.Tag = dRow.Item("disciplineproceedingmasterunkid")

'                    lvChargesCount.Items.Add(lvitem)
'                End If

'            Next

'            If lvChargesCount.Items.Count > 0 Then
'                cboEmployee.Enabled = False
'                objbtnSearchEmployee.Enabled = False
'                cboReferenceNo.Enabled = False
'                objbtnSearchReferenceNo.Enabled = False
'            Else
'                cboEmployee.Enabled = True
'                objbtnSearchEmployee.Enabled = True
'                cboReferenceNo.Enabled = True
'                objbtnSearchReferenceNo.Enabled = True
'            End If

'            Call lvChargesCount_SelectedIndexChanged(lvChargesCount, New EventArgs())
'            Call ClearControls()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillChargesCountList", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub ClearControls()
'        Try
'            cboDisciplinaryAction.SelectedValue = 0
'            cboCountStatus.SelectedValue = 0
'            txtProceedingDetials.Text = ""
'            txtPersonInvolvedComments.Text = ""
'            txtRemarkCommnts.Text = ""
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValueByPersonInvolved()
'        Try
'            Dim objEmployee As New clsEmployee_Master
'            Dim objJob As New clsJobs
'            Dim objDepartment As New clsDepartment

'            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

'            objJob._Jobunkid = objEmployee._Jobunkid
'            objDepartment._Departmentunkid = objEmployee._Departmentunkid

'            txtJobTitle.Text = objJob._Job_Name
'            txtDepartment.Text = objDepartment._Name



'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValuePersonInvolved", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValueByReferenceNo()
'        Try
'            Dim objDisciplineFileMaster As New clsDiscipline_file_master
'            Dim objDisciplineFileTran As New clsDiscipline_file_tran
'            Dim objHearingScheduleMaster As New clshearing_schedule_master
'            Dim objHearingScheduleTran As New clshearing_schedule_Tran
'            Dim objCommonMaster As New clsCommon_Master

'            objDisciplineFileMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)

'            txtChargeDate.Text = CStr(CDate(objDisciplineFileMaster._Chargedate).Date)
'            txtInterdictionDate.Text = CStr(CDate(objDisciplineFileMaster._Interdictiondate).Date)
'            txtChargeDescription.Text = objDisciplineFileMaster._Charge_Description

'            '======================== Get Committee Name ============================
'            Dim intHearingScheduleMasterId As Integer = objHearingScheduleMaster.getHearingScheduleMasterId(CInt(cboReferenceNo.SelectedValue))
'            mintCommitteeMasterId = objHearingScheduleTran.getCommitteeMasterId(intHearingScheduleMasterId)
'            If mintCommitteeMasterId > 0 Then
'                cboCommittee.SelectedValue = mintCommitteeMasterId
'                cboCommittee.Enabled = False
'                objbtnSearchCommittee.Enabled = False
'                objbtnAddCommittee.Enabled = False
'            Else
'                cboCommittee.SelectedValue = 0
'                cboCommittee.Enabled = True
'                objbtnSearchCommittee.Enabled = True
'                objbtnAddCommittee.Enabled = True
'            End If

'            ''============================ Get Count =====================================
'            'objDisciplineFileTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
'            'mdtFileTran = objDisciplineFileTran._ChargesTable

'            'If mdtFileTran IsNot Nothing Then
'            '    Call FillComboCount()
'            'End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValueByReferenceNo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FilterReferenceNo()
'        Try
'            Dim strCsvData As String = ""
'            strCsvData = String.Join(",", mdtReference.AsEnumerable().Where(Function(x) x.Field(Of Integer)("involved_employeeunkid") = CInt(cboEmployee.SelectedValue)).Select(Function(y) y.Field(Of Integer)("disciplinefileunkid").ToString).ToArray())

'            Dim mdtView As DataView = New DataView(mdtReference, "disciplinefileunkid IN (" & strCsvData & ") ", "", DataViewRowState.CurrentRows)

'            RemoveHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
'            With cboReferenceNo
'                .ValueMember = "disciplinefileunkid"
'                .DisplayMember = "reference_no"
'                .DataSource = mdtView.ToTable
'                If mdtView.ToTable.Rows.Count = 1 Then
'                    .SelectedValue = mdtView.ToTable.Rows(0).Item("disciplinefileunkid")
'                    Call GetValueByReferenceNo()
'                Else
'                    .SelectedValue = 0
'                End If
'            End With
'            AddHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FilterReferenceNo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            If menAction = enAction.EDIT_ONE Then
'                objDisciplineFileMaster = New clsDiscipline_file_master
'                If mdtProceeding IsNot Nothing Then

'                    objDisciplineFileMaster._Disciplinefileunkid = CInt(mdtProceeding.Rows(0).Item("disciplinefileunkid"))
'                cboEmployee.SelectedValue = objDisciplineFileMaster._Involved_Employeeunkid
'                cboReferenceNo.SelectedValue = objDisciplineFileMaster._Disciplinefileunkid

'                    cboChargesCount.DropDownStyle = ComboBoxStyle.Simple
'                    cboChargesCount.SelectedValue = CInt(mdtProceeding.Rows(0).Item("disciplinefiletranunkid"))
'                dtpProceedingDate.Value = CDate(mdtProceeding.Rows(0).Item("proceeding_date")).Date
'                    cboCommittee.SelectedValue = CInt(mdtProceeding.Rows(0).Item("committeemasterunkid"))

'                    txtInvestigator.Text = CStr(mdtProceeding.Rows(0).Item("investigator"))
'                cboDisciplinaryAction.SelectedValue = CInt(mdtProceeding.Rows(0).Item("actionreasonunkid"))
'                cboCountStatus.SelectedValue = CInt(mdtProceeding.Rows(0).Item("count_status"))

'                txtProceedingDetials.Text = CStr(mdtProceeding.Rows(0).Item("proceeding_details"))
'                txtPersonInvolvedComments.Text = CStr(mdtProceeding.Rows(0).Item("person_involved_comments"))
'                txtRemarkCommnts.Text = CStr(mdtProceeding.Rows(0).Item("remarks_comments"))

'                Call FillChargesCountList()
'                    If lvChargesCount.Items.Count > 0 Then
'                        lvChargesCount.Items(0).Selected = True
'                    End If
'                End If

'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objProceedingMaster._Userunkid = User._Object._Userunkid

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Function IsValidate() As Boolean
'        Try
'            If CInt(cboChargesCount.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Select Count is mandatory information."), enMsgBoxStyle.Information)
'                cboChargesCount.Focus()
'                Return False

'            ElseIf CInt(cboDisciplinaryAction.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Disciplinary Action is mandatory information."), enMsgBoxStyle.Information)
'                cboDisciplinaryAction.Focus()
'                Return False

'            ElseIf CInt(cboCountStatus.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Count Status is mandatory information."), enMsgBoxStyle.Information)
'                cboCountStatus.Focus()
'                Return False

'            ElseIf txtProceedingDetials.Text.Trim = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Proceeding Deatils is mandatory information."), enMsgBoxStyle.Information)
'                txtProceedingDetials.Focus()
'                Return False

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
'        End Try
'        Return True
'    End Function

'    Private Sub ResetParameters()
'        Try
'            cboEmployee.SelectedValue = 0
'            txtJobTitle.Text = ""
'            txtDepartment.Text = ""
'            cboReferenceNo.SelectedValue = 0
'            txtChargeDate.Text = ""
'            txtInterdictionDate.Text = ""
'            txtChargeDescription.Text = ""
'            cboChargesCount.SelectedValue = 0
'            dtpProceedingDate.Value = ConfigParameter._Object._CurrentDateAndTime
'            txtInvestigator.Text = ""
'            cboDisciplinaryAction.SelectedValue = 0
'            cboCountStatus.SelectedValue = 0
'            txtProceedingDetials.Text = ""
'            txtPersonInvolvedComments.Text = ""
'            txtRemarkCommnts.Text = ""
'            lvChargesCount.Items.Clear()
'            objMembersTran._DisciplineProceedingMasterunkid = -1
'            mdtMemTran = objMembersTran._MemTranTable

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetParameters", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetMembers()
'        Try
'            Dim objCommittee As New clsDiscipline_Committee
'            mdsCommittee = objCommittee.GetList("List")
'            Dim strInvestigatorName As String = String.Join(", ", mdsCommittee.Tables("List").AsEnumerable().Where(Function(p) p.Field(Of Integer)("committeemasterunkid") = CInt(cboCommittee.SelectedValue)).Select(Function(x) x.Field(Of String)("investigator_name").ToString).ToArray())
'            txtInvestigator.Text = strInvestigatorName

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetMembers", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetMembersTable()
'        Try
'            Dim xRow = (From P In mdsCommittee.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("committeemasterunkid") = CInt(cboCommittee.SelectedValue)) Select P)
'            'Dim xRow = (From P In mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of Integer)("committeemasterunkid") = CInt(cboCommittee.SelectedValue)) Select P)
'            If xRow.Count > 0 Then
'                For Each r As DataRow In xRow

'                    Dim row As DataRow = mdtMemTran.NewRow
'                    row.Item("committeetranunkid") = r.Item("committeetranunkid")
'                    row.Item("committeemasterunkid") = r.Item("committeemasterunkid")
'                    row.Item("AUD") = "A"
'                    mdtMemTran.Rows.Add(row)
'                Next
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetMembersTable", mstrModuleName)
'        End Try
'    End Sub


'#End Region

'#Region " Button's Events "

'    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Try
'            If IsValidate() = False Then Exit Sub

'            If mdtProceeding.Rows.Count > 0 Then
'                If mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue) AndAlso CDate(x.Field(Of DateTime)("proceeding_date")).Date = CDate(dtpProceedingDate.Value).Date AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry,This Proceeding detail is already added in the list below for selected Count and Proceeding Date."), enMsgBoxStyle.Information)
'                    cboChargesCount.Focus()
'                    Exit Sub
'                End If
'            End If

'            Dim dRow As DataRow = mdtProceeding.NewRow

'            dRow.Item("count") = mintChargeCount
'            dRow.Item("disciplineproceedingmasterunkid") = mintDisciplineProceedingMasterunkid
'            dRow.Item("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)
'            dRow.Item("disciplinefileunkid") = CInt(cboReferenceNo.SelectedValue)
'            dRow.Item("committeemasterunkid") = mintCommitteeMasterId
'            dRow.Item("committee") = cboCommittee.Text
'            dRow.Item("investigator") = txtInvestigator.Text
'            dRow.Item("proceeding_date") = dtpProceedingDate.Value
'            dRow.Item("proceeding_details") = txtProceedingDetials.Text
'            dRow.Item("remarks_comments") = txtRemarkCommnts.Text
'            dRow.Item("person_involved_comments") = txtPersonInvolvedComments.Text
'            dRow.Item("actionreasonunkid") = CInt(cboDisciplinaryAction.SelectedValue)
'            dRow.Item("reason_action") = cboDisciplinaryAction.Text
'            dRow.Item("isapproved") = False 'CBool(User._Object.Privilege._AllowToApproveProceedingCounts)
'            dRow.Item("count_status") = CInt(cboCountStatus.SelectedValue)
'            dRow.Item("isvoid") = False
'            dRow.Item("voiduserunkid") = -1
'            dRow.Item("voiddatetime") = DBNull.Value
'            dRow.Item("voidreason") = ""
'            dRow.Item("AUD") = "A"
'            dRow.Item("GUID") = Guid.NewGuid.ToString

'            mdtProceeding.Rows.Add(dRow)

'            Dim xRow = (From P In mdsCommittee.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("committeemasterunkid") = CInt(cboCommittee.SelectedValue)) Select P)
'            If xRow.Count > 0 Then
'                For Each r As DataRow In xRow
'                    Dim row As DataRow = mdtMemTran.NewRow
'                    row.Item("disciplinememberstranunkid") = -1
'                    row.Item("disciplineproceedingmasterunkid") = -1
'                    row.Item("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)
'                    row.Item("committeetranunkid") = r.Item("committeetranunkid")
'                    row.Item("committeemasterunkid") = r.Item("committeemasterunkid")
'                    row.Item("committee") = cboCommittee.Text
'                    row.Item("csv_investigator") = txtInvestigator.Text
'                    row.Item("isvoid") = False
'                    row.Item("voiduserunkid") = -1
'                    row.Item("voiddatetime") = DBNull.Value
'                    row.Item("voidreason") = ""
'                    row.Item("AUD") = "A"
'                    row.Item("GUID") = Guid.NewGuid.ToString
'                    mdtMemTran.Rows.Add(row)
'                Next
'            End If

'            Call FillChargesCountList()



'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        Try
'            If lvChargesCount.SelectedItems.Count > 0 Then

'                Dim dRow As IEnumerable(Of DataRow)

'                If CInt(lvChargesCount.SelectedItems(0).Tag) > 0 Then
'                    dRow = (From row As DataRow In mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplineproceedingmasterunkid") = CInt(lvChargesCount.SelectedItems(0).Tag)) Select row)
'                Else
'                    dRow = (From row As DataRow In mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") = CStr(lvChargesCount.SelectedItems(0).SubItems(objcolhGUID.Index).Text)) Select row)
'                End If

'                If dRow.Count > 0 Then
'                    dRow(0).Item("count") = mintChargeCount
'                    dRow(0).Item("disciplineproceedingmasterunkid") = dRow(0).Item("disciplineproceedingmasterunkid")
'                    dRow(0).Item("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)
'                    dRow(0).Item("committeemasterunkid") = mintCommitteeMasterId
'                    dRow(0).Item("committee") = cboCommittee.Text
'                    dRow(0).Item("investigator") = txtInvestigator.Text
'                    dRow(0).Item("proceeding_date") = dtpProceedingDate.Value
'                    dRow(0).Item("proceeding_details") = txtProceedingDetials.Text
'                    dRow(0).Item("remarks_comments") = txtRemarkCommnts.Text
'                    dRow(0).Item("person_involved_comments") = txtPersonInvolvedComments.Text
'                    dRow(0).Item("actionreasonunkid") = CInt(cboDisciplinaryAction.SelectedValue)
'                    dRow(0).Item("reason_action") = cboDisciplinaryAction.Text
'                    dRow(0).Item("isapproved") = False
'                    dRow(0).Item("count_status") = CInt(cboCountStatus.SelectedValue)
'                    dRow(0).Item("isvoid") = False
'                    dRow(0).Item("voiduserunkid") = -1
'                    dRow(0).Item("voiddatetime") = DBNull.Value
'                    dRow(0).Item("voidreason") = ""
'                    dRow(0).Item("AUD") = IIf(dRow(0).Item("AUD").ToString <> "A", "U", dRow(0).Item("AUD").ToString)
'                    dRow(0).Item("GUID") = Guid.NewGuid.ToString
'                End If

'                mdtProceeding.AcceptChanges()
'If menAction <> enAction.EDIT_ONE Then
'                Dim xRow = (From P In mdsCommittee.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("committeemasterunkid") = CInt(cboCommittee.SelectedValue)) Select P)
'                If xRow.Count > 0 Then
'                    For Each r As DataRow In xRow
'                        Dim row As DataRow = mdtMemTran.NewRow
'                        row.Item("disciplinememberstranunkid") = r.Item("disciplinememberstranunkid")
'                        row.Item("disciplineproceedingmasterunkid") = r.Item("disciplineproceedingmasterunkid")
'                        row.Item("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)
'                        row.Item("committeetranunkid") = r.Item("committeetranunkid")
'                        row.Item("committeemasterunkid") = r.Item("committeemasterunkid")
'                        row.Item("committee") = cboCommittee.Text
'                        row.Item("csv_investigator") = txtInvestigator.Text
'                        row.Item("isvoid") = False
'                        row.Item("voiduserunkid") = -1
'                        row.Item("voiddatetime") = DBNull.Value
'                        row.Item("voidreason") = ""
'                        row.Item("AUD") = IIf(dRow(0).Item("AUD").ToString <> "A", "U", dRow(0).Item("AUD").ToString)
'                        row.Item("GUID") = Guid.NewGuid.ToString
'                        mdtMemTran.Rows.Add(row)
'                    Next
'                End If

'                End If



'                Call FillChargesCountList()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        Try
'            If lvChargesCount.SelectedItems.Count > 0 Then

'                Dim dRow As IEnumerable(Of DataRow)

'                If CInt(lvChargesCount.SelectedItems(0).Tag) > 0 Then
'                    dRow = (From row As DataRow In mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplineproceedingmasterunkid") = CInt(lvChargesCount.SelectedItems(0).Tag)) Select row)
'                Else
'                    dRow = (From row As DataRow In mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") = CStr(lvChargesCount.SelectedItems(0).SubItems(objcolhGUID.Index).Text)) Select row)
'                End If

'                If dRow.Count > 0 Then
'                    Dim mstrVoidReason As String = String.Empty
'                    If CInt(dRow(0).Item("disciplineproceedingmasterunkid")) > 0 Then
'                        Dim frm As New frmReasonSelection
'                        If User._Object._Isrighttoleft = True Then
'                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                            frm.RightToLeftLayout = True
'                            Call Language.ctlRightToLeftlayOut(frm)
'                        End If
'                        frm.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
'                        frm = Nothing
'                        If menAction = enAction.EDIT_ONE Then
'                            If mstrVoidReason.Trim.Length <= 0 Then Exit Sub
'                        End If
'                    End If
'                    dRow(0).Item("isvoid") = True
'                    dRow(0).Item("voiduserunkid") = User._Object._Userunkid
'                    dRow(0).Item("voidreason") = mstrVoidReason
'                    dRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
'                    dRow(0).Item("AUD") = "D"
'                End If

'                mdtProceeding.AcceptChanges()
'                Call FillChargesCountList()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Try
'            Dim blnFlag As Boolean = False
'            '[If IsValidate() = False Then Exit Sub
'            'Call SetMembersTable()
'            objProceedingMaster._Userunkid = User._Object._Userunkid

'            If mdtProceeding.Rows.Count > 0 Then

'                blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran)

'                If blnFlag = False AndAlso objProceedingMaster._Message <> "" Then
'                    eZeeMsgBox.Show(objProceedingMaster._Message, enMsgBoxStyle.Information)
'                    Exit Sub
'                End If

'                If blnFlag = True Then
'                    mblnCancel = False
'                    If menAction = enAction.ADD_CONTINUE Then
'                        Call ResetParameters()
'                    Else
'                        Me.Close()
'                    End If
'                End If
'            Else
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Add atleast one Count Proceedig Details."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Try
'            Dim frm As New frmCommonSearch

'            With frm
'                .DisplayMember = cboEmployee.DisplayMember
'                .ValueMember = cboEmployee.ValueMember
'                .CodeMember = "employeecode"
'                .DataSource = mdtEmployee
'            End With

'            If frm.DisplayDialog Then
'                cboEmployee.SelectedValue = frm.SelectedValue
'                cboEmployee.Focus()
'            End If

'            'If CInt(cboEmployee.SelectedValue) > 0 Then
'            '    Call FilterReferenceNo()
'            'End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchReferenceNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReferenceNo.Click
'        Try
'            Dim frm As New frmCommonSearch

'            With frm
'                .DisplayMember = cboReferenceNo.DisplayMember
'                .ValueMember = cboReferenceNo.ValueMember
'                .DataSource = CType(cboReferenceNo.DataSource, DataTable)
'            End With

'            If frm.DisplayDialog Then
'                cboReferenceNo.SelectedValue = frm.SelectedValue
'                cboReferenceNo.Focus()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchReferenceNo_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Private Sub objbtnSearchInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'    '    Try
'    '        Dim frm As New frmCommonSearch

'    '        With frm
'    '            .DisplayMember = cboInvestigator.DisplayMember
'    '            .ValueMember = cboInvestigator.ValueMember
'    '            .DataSource = CType(cboInvestigator.DataSource, DataTable)
'    '        End With

'    '        If frm.DisplayDialog Then
'    '            cboInvestigator.SelectedValue = frm.SelectedValue
'    '            cboInvestigator.Focus()
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "objbtnSearchInvestigator_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    Private Sub objbtnSearchDisciplinaryAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDisciplinaryAction.Click
'        Try
'            Dim frm As New frmCommonSearch

'            With frm
'                .DisplayMember = cboDisciplinaryAction.DisplayMember
'                .ValueMember = cboDisciplinaryAction.ValueMember
'                .CodeMember = "code"
'                .DataSource = CType(cboDisciplinaryAction.DataSource, DataTable)
'            End With

'            If frm.DisplayDialog Then
'                cboDisciplinaryAction.SelectedValue = frm.SelectedValue
'                cboDisciplinaryAction.Focus()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchDisciplinaryAction_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchCommittee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchCommittee.Click
'        Try
'            Dim frm As New frmCommonSearch

'            With frm
'                .DisplayMember = cboCommittee.DisplayMember
'                .ValueMember = cboCommittee.ValueMember
'                .CodeMember = "code"
'                .DataSource = CType(cboCommittee.DataSource, DataTable)
'            End With

'            If frm.DisplayDialog Then
'                cboCommittee.SelectedValue = frm.SelectedValue
'                cboCommittee.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchCommittee_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddCommittee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAddCommittee.Click
'        Dim frm As New frmCommonMaster
'        Try
'            Dim intMasterId As Integer = -1
'            frm.displayDialog(intMasterId, clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, enAction.ADD_ONE)
'            If intMasterId > 0 Then
'                Dim objCommonMaster As New clsCommon_Master
'                Dim dList As New DataSet
'                dList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
'                With cboCommittee
'                    .DisplayMember = "name"
'                    .ValueMember = "masterunkid"
'                    .DataSource = dList.Tables("List")
'                    .SelectedValue = intMasterId
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddCommittee_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInvestigator.Click
'        Dim frm As New frmCommittee_AddEdit
'        Try
'            Dim objCommittee As New clsDiscipline_Committee

'            Dim intCommitteMasterId As Integer = CInt(cboCommittee.SelectedValue)

'            frm.displayDialog(intCommitteMasterId, enAction.EDIT_ONE)

'            If intCommitteMasterId > 0 Then
'                Call GetMembers()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddInvestigator_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " ComboBox's Events "

'    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
'        Try
'            If CInt(cboEmployee.SelectedValue) > 0 Then
'                Call GetValueByPersonInvolved()
'                If menAction <> enAction.EDIT_ONE Then
'                    cboReferenceNo.Enabled = True
'                    objbtnSearchReferenceNo.Enabled = True
'                End If
'                Call FilterReferenceNo()

'                cboReferenceNo.SelectedValue = 0
'                txtChargeDate.Text = ""
'                txtInterdictionDate.Text = ""
'                txtChargeDescription.Text = ""
'                cboCommittee.SelectedValue = 0
'                objDisciplineFileTran = New clsDiscipline_file_tran
'                objDisciplineFileTran._Disciplinefileunkid = -1
'                mdtFileTran = objDisciplineFileTran._ChargesTable
'                Call FillComboCount()

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged
'        Try
'            If CInt(cboReferenceNo.SelectedValue) > 0 Then

'                'dtpProceedingDate.Value = ConfigParameter._Object._CurrentDateAndTime
'                'txtInvestigator.Text = ""
'                'txtProceedingDetials.Text = ""
'                'txtPersonInvolvedComments.Text = ""
'                'txtRemarkCommnts.Text = ""
'                'objProceedingMaster._DisciplineProceedingMasterunkid = -1
'                'mdtProceeding = objProceedingMaster._ProceedingTable
'                'Call FillChargesCountList()

'                Call GetValueByReferenceNo()
'            End If

'            '============================ Get Count =====================================

'            objDisciplineFileTran = New clsDiscipline_file_tran
'            objDisciplineFileTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
'            mdtFileTran = objDisciplineFileTran._ChargesTable
'            If mdtFileTran IsNot Nothing Then
'                Call FillComboCount()
'            End If
'            objDisciplineFileTran = Nothing

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboReferenceNo_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
'        Try
'            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then

'                Dim frm As New frmCommonSearch

'                With frm
'                    .DisplayMember = cboEmployee.DisplayMember
'                    .ValueMember = cboEmployee.ValueMember
'                    .CodeMember = "employeecode"
'                    .DataSource = mdtEmployee
'                End With

'                Dim ch As Char = Convert.ToChar(e.KeyChar)
'                frm.TypedText = ch.ToString

'                If frm.DisplayDialog Then
'                    cboEmployee.SelectedValue = frm.SelectedValue
'                    e.KeyChar = ChrW(Keys.ShiftKey)
'                Else
'                    cboEmployee.Text = ""
'                End If
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboReferenceNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboReferenceNo.KeyPress
'        Try
'            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then

'                Dim frm As New frmCommonSearch

'                With frm
'                    .DisplayMember = cboReferenceNo.DisplayMember
'                    .ValueMember = cboReferenceNo.ValueMember
'                    .DataSource = CType(cboReferenceNo.DataSource, DataTable)
'                End With

'                Dim ch As Char = Convert.ToChar(e.KeyChar)
'                frm.TypedText = ch.ToString

'                If frm.DisplayDialog Then
'                    cboReferenceNo.SelectedValue = frm.SelectedValue
'                    e.KeyChar = ChrW(Keys.ShiftKey)
'                Else
'                    cboReferenceNo.Text = ""
'                End If
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboReferenceNo_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboChargesCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChargesCount.SelectedIndexChanged
'        Try
'            'Dim dtTemp As DataTable
'            'Dim dtView As DataView = New DataView(mdtFileTran, "disciplinefiletranunkid = " & CInt(cboChargesCount.SelectedValue), "", DataViewRowState.CurrentRows)
'            'dtTemp = dtView.ToTable

'            If CInt(cboChargesCount.SelectedValue) > 0 Then
'                cboOffenceCategory.Items.Clear()
'                cboDisciplineType.Items.Clear()
'                Call ClearControls()

'                Dim datarow = (From P In mdtFileTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)) Select P)
'                If datarow.Count > 0 AndAlso datarow(0).Item("charge_category").ToString.Trim.Length > 0 Then
'                    Dim iCboItems() As ComboBoxValue = {New ComboBoxValue(CStr(datarow(0).Item("charge_category")), CInt(datarow(0).Item("offencecategoryunkid")))}
'                    With cboOffenceCategory
'                        .Items.Clear()
'                        .Items.AddRange(iCboItems)
'                        .SelectedIndex = 0
'                        .SelectedValue = CType(cboOffenceCategory.SelectedItem, ComboBoxValue).Value
'                    End With
'                End If
'                datarow = (From P In mdtFileTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)) Select P)
'                If datarow.Count > 0 AndAlso datarow(0).Item("charge_category").ToString.Trim.Length > 0 Then
'                    Dim iCboItems() As ComboBoxValue = {New ComboBoxValue(CStr(datarow(0).Item("charge_descr")), CInt(datarow(0).Item("offenceunkid")))}
'                    With cboDisciplineType
'                        .Items.Clear()
'                        .Items.AddRange(iCboItems)
'                        .SelectedIndex = 0
'                        .SelectedValue = CType(cboDisciplineType.SelectedItem, ComboBoxValue).Value
'                    End With
'                End If

'                txtSeverity.Text = CStr(CType(cboChargesCount.SelectedItem, DataRowView).Item("charge_severity"))
'                mintChargeCount = CInt(CType(cboChargesCount.SelectedItem, DataRowView).Item("charge_count"))
'            Else
'                cboOffenceCategory.Items.Clear()
'                cboDisciplineType.Items.Clear()
'                txtSeverity.Text = ""
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboChargesCount_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboCommittee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCommittee.SelectedIndexChanged
'        Try
'            If CInt(cboCommittee.SelectedValue) > 0 Then
'                mintCommitteeMasterId = CInt(cboCommittee.SelectedValue)
'                Call GetMembers()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboCommittee_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboChargesCount_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles cboChargesCount.KeyPress
'        Try
'            If cboChargesCount.DropDownStyle = ComboBoxStyle.Simple Then
'                e.Handled = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ComboBox_KeyPress", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub cboChargesCount_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cboChargesCount.KeyDown
'        Try
'            If cboChargesCount.DropDownStyle = ComboBoxStyle.Simple Then
'                If e.KeyCode = Keys.Right Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Home Or e.KeyCode = Keys.End Or e.KeyCode = Keys.Tab Then
'                    e.Handled = False
'                Else
'                    e.Handled = True
'                End If
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ComboBox_KeyDown", mstrModuleName)
'        Finally
'        End Try
'    End Sub


'    'Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
'    '    Try
'    '        Dim dtTemp As DataTable
'    '        Dim dtView As DataView = New DataView(CType(cboOffenceCategory.DataSource, DataTable), "offencecategoryunkid = " & CInt(cboOffenceCategory.SelectedValue), "", DataViewRowState.CurrentRows)
'    '        dtTemp = dtView.ToTable

'    '        Dim dR As DataRow = dtTemp.NewRow
'    '        dR.Item("offenceunkid") = 0
'    '        dR.Item("charge_descr") = "Select"
'    '        dtTemp.Rows.InsertAt(dR, 0)
'    '        With cboDisciplineType
'    '            .ValueMember = "offenceunkid"
'    '            .DisplayMember = "charge_descr"
'    '            .DataSource = dtTemp
'    '            .SelectedValue = 0
'    '        End With
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboOffenceCategory_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub cboDisciplineType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDisciplineType.SelectedIndexChanged
'    '    Try
'    '        'Dim dtTemp As DataTable
'    '        'Dim dtView As DataView = New DataView(CType(cboDisciplineType.DataSource, DataTable), "offenceunkid = " & CInt(cboDisciplineType.SelectedValue), "", DataViewRowState.CurrentRows)
'    '        'dtTemp = dtView.ToTable
'    '        'txtSeverity.Text = CStr(dtTemp.Rows(0).Item("charge_severity"))



'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboDisciplineType_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub

'#End Region

'#Region " Listview's Events "

'    Private Sub lvChargesCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvChargesCount.SelectedIndexChanged
'        Try
'            If lvChargesCount.SelectedItems.Count > 0 Then
'                btnAdd.Enabled = False
'                cboChargesCount.SelectedValue = CInt(lvChargesCount.SelectedItems(0).SubItems(objcolhdisciplinefiletranunkid.Index).Text)
'                dtpProceedingDate.Value = CDate(lvChargesCount.SelectedItems(0).SubItems(colhProceedingDate.Index).Text).Date
'                cboCommittee.Text = CStr(lvChargesCount.SelectedItems(0).SubItems(colhCommittee.Index).Text)
'                txtInvestigator.Text = CStr(lvChargesCount.SelectedItems(0).SubItems(colhInvestigator.Index).Text)
'                cboDisciplinaryAction.Text = CStr(lvChargesCount.SelectedItems(0).SubItems(colhDisciplineAction.Index).Text)
'                cboCountStatus.SelectedValue = CStr(lvChargesCount.SelectedItems(0).SubItems(colhCountStatus.Index).Text)
'                txtProceedingDetials.Text = CStr(lvChargesCount.SelectedItems(0).SubItems(colhProceedingDetails.Index).Text)
'                txtPersonInvolvedComments.Text = CStr(lvChargesCount.SelectedItems(0).SubItems(colhPIComments.Index).Text)
'                'objbtnAddInvestigator.Enabled = True
'            Else
'                btnAdd.Enabled = True
'                cboChargesCount.SelectedValue = 0
'                dtpProceedingDate.Value = CDate(ConfigParameter._Object._CurrentDateAndTime.ToShortDateString)
'                'cboCommittee.SelectedValue = 0
'                'txtInvestigator.Text = ""
'                cboDisciplinaryAction.SelectedValue = 0
'                cboCountStatus.SelectedValue = 0
'                txtProceedingDetials.Text = ""
'                txtPersonInvolvedComments.Text = ""
'                'objbtnAddInvestigator.Enabled = False
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvChargesCount_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region