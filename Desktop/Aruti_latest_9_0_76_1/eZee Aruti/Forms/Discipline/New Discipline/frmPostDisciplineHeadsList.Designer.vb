﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostDisciplineHeadsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPostDisciplineHeadsList))
        Me.objPchkall = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objSearchTransactionhead = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboExempApprovStatus = New System.Windows.Forms.ComboBox
        Me.lblApprovalStatus = New System.Windows.Forms.Label
        Me.lblState = New System.Windows.Forms.Label
        Me.cboTransactionHead = New System.Windows.Forms.ComboBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDisapprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvPostedHeads = New eZee.Common.eZeeListView(Me.components)
        Me.objECheck = New System.Windows.Forms.ColumnHeader
        Me.colhETName = New System.Windows.Forms.ColumnHeader
        Me.colhHeadType = New System.Windows.Forms.ColumnHeader
        Me.colhTypeOF = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhEStatus = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployee = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'objPchkall
        '
        Me.objPchkall.AutoSize = True
        Me.objPchkall.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objPchkall.Location = New System.Drawing.Point(20, 143)
        Me.objPchkall.Name = "objPchkall"
        Me.objPchkall.Size = New System.Drawing.Size(15, 14)
        Me.objPchkall.TabIndex = 9
        Me.objPchkall.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objSearchTransactionhead)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboExempApprovStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprovalStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblState)
        Me.gbFilterCriteria.Controls.Add(Me.cboTransactionHead)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(720, 69)
        Me.gbFilterCriteria.TabIndex = 10
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objSearchTransactionhead
        '
        Me.objSearchTransactionhead.BackColor = System.Drawing.Color.Transparent
        Me.objSearchTransactionhead.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchTransactionhead.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchTransactionhead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchTransactionhead.BorderSelected = False
        Me.objSearchTransactionhead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchTransactionhead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchTransactionhead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchTransactionhead.Location = New System.Drawing.Point(339, 33)
        Me.objSearchTransactionhead.Name = "objSearchTransactionhead"
        Me.objSearchTransactionhead.Size = New System.Drawing.Size(21, 21)
        Me.objSearchTransactionhead.TabIndex = 309
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(695, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 307
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(670, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 306
        Me.objbtnSearch.TabStop = False
        '
        'cboExempApprovStatus
        '
        Me.cboExempApprovStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExempApprovStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExempApprovStatus.FormattingEnabled = True
        Me.cboExempApprovStatus.Location = New System.Drawing.Point(432, 33)
        Me.cboExempApprovStatus.Name = "cboExempApprovStatus"
        Me.cboExempApprovStatus.Size = New System.Drawing.Size(138, 21)
        Me.cboExempApprovStatus.TabIndex = 303
        '
        'lblApprovalStatus
        '
        Me.lblApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalStatus.Location = New System.Drawing.Point(368, 36)
        Me.lblApprovalStatus.Name = "lblApprovalStatus"
        Me.lblApprovalStatus.Size = New System.Drawing.Size(58, 15)
        Me.lblApprovalStatus.TabIndex = 304
        Me.lblApprovalStatus.Text = "Status"
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(8, 36)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(101, 15)
        Me.lblState.TabIndex = 91
        Me.lblState.Text = "Transaction Head"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTransactionHead
        '
        Me.cboTransactionHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionHead.FormattingEnabled = True
        Me.cboTransactionHead.Location = New System.Drawing.Point(115, 33)
        Me.cboTransactionHead.Name = "cboTransactionHead"
        Me.cboTransactionHead.Size = New System.Drawing.Size(218, 21)
        Me.cboTransactionHead.TabIndex = 90
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(744, 58)
        Me.eZeeHeader.TabIndex = 86
        Me.eZeeHeader.Title = "Posted Disciplinary Heads"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDisapprove)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 373)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(744, 55)
        Me.objFooter.TabIndex = 87
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(12, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnApprove.TabIndex = 18
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(532, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 17
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(635, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDisapprove
        '
        Me.btnDisapprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisapprove.BackColor = System.Drawing.Color.White
        Me.btnDisapprove.BackgroundImage = CType(resources.GetObject("btnDisapprove.BackgroundImage"), System.Drawing.Image)
        Me.btnDisapprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDisapprove.BorderColor = System.Drawing.Color.Empty
        Me.btnDisapprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDisapprove.FlatAppearance.BorderSize = 0
        Me.btnDisapprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDisapprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisapprove.ForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDisapprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Location = New System.Drawing.Point(12, 13)
        Me.btnDisapprove.Name = "btnDisapprove"
        Me.btnDisapprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Size = New System.Drawing.Size(97, 30)
        Me.btnDisapprove.TabIndex = 19
        Me.btnDisapprove.Text = "&Disapprove"
        Me.btnDisapprove.UseVisualStyleBackColor = True
        '
        'lvPostedHeads
        '
        Me.lvPostedHeads.BackColorOnChecked = False
        Me.lvPostedHeads.CheckBoxes = True
        Me.lvPostedHeads.ColumnHeaders = Nothing
        Me.lvPostedHeads.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objECheck, Me.colhETName, Me.colhHeadType, Me.colhTypeOF, Me.colhAmount, Me.colhEStatus, Me.objcolhEmployee})
        Me.lvPostedHeads.CompulsoryColumns = ""
        Me.lvPostedHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPostedHeads.FullRowSelect = True
        Me.lvPostedHeads.GridLines = True
        Me.lvPostedHeads.GroupingColumn = Nothing
        Me.lvPostedHeads.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPostedHeads.HideSelection = False
        Me.lvPostedHeads.Location = New System.Drawing.Point(12, 139)
        Me.lvPostedHeads.MinColumnWidth = 50
        Me.lvPostedHeads.MultiSelect = False
        Me.lvPostedHeads.Name = "lvPostedHeads"
        Me.lvPostedHeads.OptionalColumns = ""
        Me.lvPostedHeads.ShowMoreItem = False
        Me.lvPostedHeads.ShowSaveItem = False
        Me.lvPostedHeads.ShowSelectAll = True
        Me.lvPostedHeads.ShowSizeAllColumnsToFit = True
        Me.lvPostedHeads.Size = New System.Drawing.Size(720, 228)
        Me.lvPostedHeads.Sortable = True
        Me.lvPostedHeads.TabIndex = 20
        Me.lvPostedHeads.UseCompatibleStateImageBehavior = False
        Me.lvPostedHeads.View = System.Windows.Forms.View.Details
        '
        'objECheck
        '
        Me.objECheck.Tag = "objECheck"
        Me.objECheck.Text = ""
        Me.objECheck.Width = 25
        '
        'colhETName
        '
        Me.colhETName.Tag = "colhETName"
        Me.colhETName.Text = "Transaction Head"
        Me.colhETName.Width = 180
        '
        'colhHeadType
        '
        Me.colhHeadType.Tag = "colhHeadType"
        Me.colhHeadType.Text = "Head Type"
        Me.colhHeadType.Width = 140
        '
        'colhTypeOF
        '
        Me.colhTypeOF.Tag = "colhTypeOF"
        Me.colhTypeOF.Text = "Type Of"
        Me.colhTypeOF.Width = 140
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 140
        '
        'colhEStatus
        '
        Me.colhEStatus.Tag = "colhEStatus"
        Me.colhEStatus.Text = "Status"
        Me.colhEStatus.Width = 90
        '
        'objcolhEmployee
        '
        Me.objcolhEmployee.Tag = "objcolhEmployee"
        Me.objcolhEmployee.Text = ""
        Me.objcolhEmployee.Width = 0
        '
        'frmPostDisciplineHeadsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(744, 428)
        Me.Controls.Add(Me.objPchkall)
        Me.Controls.Add(Me.lvPostedHeads)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPostDisciplineHeadsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Posted Disciplinary Heads List"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents objPchkall As System.Windows.Forms.CheckBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objSearchTransactionhead As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboExempApprovStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovalStatus As System.Windows.Forms.Label
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents cboTransactionHead As System.Windows.Forms.ComboBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDisapprove As eZee.Common.eZeeLightButton
    Friend WithEvents lvPostedHeads As eZee.Common.eZeeListView
    Friend WithEvents objECheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhETName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTypeOF As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
End Class
