﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineFiling
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineFiling))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.tabcDisciplineFile = New System.Windows.Forms.TabControl
        Me.tabpCharges = New System.Windows.Forms.TabPage
        Me.gbDiscipilneFiling = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tabcRemark = New System.Windows.Forms.TabControl
        Me.tabpRemark = New System.Windows.Forms.TabPage
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.tabpResponseRemark = New System.Windows.Forms.TabPage
        Me.txtResponseRemark = New eZee.TextBox.AlphanumericTextBox
        Me.dtpResponseDate = New System.Windows.Forms.DateTimePicker
        Me.lblResponsedate = New System.Windows.Forms.Label
        Me.objbtnSearchOffence = New eZee.Common.eZeeGradientButton
        Me.dtpInterdictiondate = New System.Windows.Forms.DateTimePicker
        Me.lblInterdictionDate = New System.Windows.Forms.Label
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.txtReferenceNo = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.lblOffenceCategory = New System.Windows.Forms.Label
        Me.cboOffenceCategory = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.dtpDisciplineDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.lblSeverity = New System.Windows.Forms.Label
        Me.txtPesonInvolved = New eZee.TextBox.AlphanumericTextBox
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.txtSeverity = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddDisciplinetype = New eZee.Common.eZeeGradientButton
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.tabpIncident = New System.Windows.Forms.TabPage
        Me.txtIncident = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtAgainstPerson = New eZee.TextBox.AlphanumericTextBox
        Me.lblAgainst = New System.Windows.Forms.Label
        Me.objbtnAgainstPerson = New eZee.Common.eZeeGradientButton
        Me.Panel1.SuspendLayout()
        Me.tabcDisciplineFile.SuspendLayout()
        Me.tabpCharges.SuspendLayout()
        Me.gbDiscipilneFiling.SuspendLayout()
        Me.tabcRemark.SuspendLayout()
        Me.tabpRemark.SuspendLayout()
        Me.tabpResponseRemark.SuspendLayout()
        Me.tabpIncident.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.tabcDisciplineFile)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(649, 414)
        Me.Panel1.TabIndex = 0
        '
        'tabcDisciplineFile
        '
        Me.tabcDisciplineFile.Controls.Add(Me.tabpCharges)
        Me.tabcDisciplineFile.Controls.Add(Me.tabpIncident)
        Me.tabcDisciplineFile.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcDisciplineFile.Location = New System.Drawing.Point(0, 0)
        Me.tabcDisciplineFile.Name = "tabcDisciplineFile"
        Me.tabcDisciplineFile.SelectedIndex = 0
        Me.tabcDisciplineFile.Size = New System.Drawing.Size(649, 359)
        Me.tabcDisciplineFile.TabIndex = 87
        '
        'tabpCharges
        '
        Me.tabpCharges.Controls.Add(Me.gbDiscipilneFiling)
        Me.tabpCharges.Location = New System.Drawing.Point(4, 22)
        Me.tabpCharges.Name = "tabpCharges"
        Me.tabpCharges.Size = New System.Drawing.Size(641, 333)
        Me.tabpCharges.TabIndex = 0
        Me.tabpCharges.Text = "Discipline Charges"
        Me.tabpCharges.UseVisualStyleBackColor = True
        '
        'gbDiscipilneFiling
        '
        Me.gbDiscipilneFiling.BorderColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.Checked = False
        Me.gbDiscipilneFiling.CollapseAllExceptThis = False
        Me.gbDiscipilneFiling.CollapsedHoverImage = Nothing
        Me.gbDiscipilneFiling.CollapsedNormalImage = Nothing
        Me.gbDiscipilneFiling.CollapsedPressedImage = Nothing
        Me.gbDiscipilneFiling.CollapseOnLoad = False
        Me.gbDiscipilneFiling.Controls.Add(Me.tabcRemark)
        Me.gbDiscipilneFiling.Controls.Add(Me.dtpResponseDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblResponsedate)
        Me.gbDiscipilneFiling.Controls.Add(Me.objbtnSearchOffence)
        Me.gbDiscipilneFiling.Controls.Add(Me.dtpInterdictiondate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblInterdictionDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblRefNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtReferenceNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.objbtnSearchCategory)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblOffenceCategory)
        Me.gbDiscipilneFiling.Controls.Add(Me.cboOffenceCategory)
        Me.gbDiscipilneFiling.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbDiscipilneFiling.Controls.Add(Me.dtpDisciplineDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDisciplineType)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblSeverity)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtPesonInvolved)
        Me.gbDiscipilneFiling.Controls.Add(Me.cboDisciplineType)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtSeverity)
        Me.gbDiscipilneFiling.Controls.Add(Me.objbtnAddDisciplinetype)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblPersonalInvolved)
        Me.gbDiscipilneFiling.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbDiscipilneFiling.ExpandedHoverImage = Nothing
        Me.gbDiscipilneFiling.ExpandedNormalImage = Nothing
        Me.gbDiscipilneFiling.ExpandedPressedImage = Nothing
        Me.gbDiscipilneFiling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDiscipilneFiling.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDiscipilneFiling.HeaderHeight = 25
        Me.gbDiscipilneFiling.HeaderMessage = ""
        Me.gbDiscipilneFiling.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDiscipilneFiling.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.HeightOnCollapse = 0
        Me.gbDiscipilneFiling.LeftTextSpace = 0
        Me.gbDiscipilneFiling.Location = New System.Drawing.Point(0, 0)
        Me.gbDiscipilneFiling.Name = "gbDiscipilneFiling"
        Me.gbDiscipilneFiling.OpenHeight = 300
        Me.gbDiscipilneFiling.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDiscipilneFiling.ShowBorder = True
        Me.gbDiscipilneFiling.ShowCheckBox = False
        Me.gbDiscipilneFiling.ShowCollapseButton = False
        Me.gbDiscipilneFiling.ShowDefaultBorderColor = True
        Me.gbDiscipilneFiling.ShowDownButton = False
        Me.gbDiscipilneFiling.ShowHeader = True
        Me.gbDiscipilneFiling.Size = New System.Drawing.Size(641, 333)
        Me.gbDiscipilneFiling.TabIndex = 86
        Me.gbDiscipilneFiling.Temp = 0
        Me.gbDiscipilneFiling.Text = "Disciplinary Charge Information"
        Me.gbDiscipilneFiling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcRemark
        '
        Me.tabcRemark.Controls.Add(Me.tabpRemark)
        Me.tabcRemark.Controls.Add(Me.tabpResponseRemark)
        Me.tabcRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcRemark.Location = New System.Drawing.Point(10, 200)
        Me.tabcRemark.Name = "tabcRemark"
        Me.tabcRemark.SelectedIndex = 0
        Me.tabcRemark.Size = New System.Drawing.Size(621, 130)
        Me.tabcRemark.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tabcRemark.TabIndex = 103
        '
        'tabpRemark
        '
        Me.tabpRemark.Controls.Add(Me.txtRemark)
        Me.tabpRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpRemark.Name = "tabpRemark"
        Me.tabpRemark.Size = New System.Drawing.Size(613, 104)
        Me.tabpRemark.TabIndex = 0
        Me.tabpRemark.Text = "Remark"
        Me.tabpRemark.UseVisualStyleBackColor = True
        '
        'txtRemark
        '
        Me.txtRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(613, 104)
        Me.txtRemark.TabIndex = 84
        '
        'tabpResponseRemark
        '
        Me.tabpResponseRemark.Controls.Add(Me.txtResponseRemark)
        Me.tabpResponseRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpResponseRemark.Name = "tabpResponseRemark"
        Me.tabpResponseRemark.Size = New System.Drawing.Size(613, 104)
        Me.tabpResponseRemark.TabIndex = 1
        Me.tabpResponseRemark.Text = "Response Remark"
        Me.tabpResponseRemark.UseVisualStyleBackColor = True
        '
        'txtResponseRemark
        '
        Me.txtResponseRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtResponseRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtResponseRemark.Flags = 0
        Me.txtResponseRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResponseRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtResponseRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtResponseRemark.Multiline = True
        Me.txtResponseRemark.Name = "txtResponseRemark"
        Me.txtResponseRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtResponseRemark.Size = New System.Drawing.Size(613, 104)
        Me.txtResponseRemark.TabIndex = 85
        '
        'dtpResponseDate
        '
        Me.dtpResponseDate.Checked = False
        Me.dtpResponseDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpResponseDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpResponseDate.Location = New System.Drawing.Point(339, 38)
        Me.dtpResponseDate.Name = "dtpResponseDate"
        Me.dtpResponseDate.ShowCheckBox = True
        Me.dtpResponseDate.Size = New System.Drawing.Size(111, 21)
        Me.dtpResponseDate.TabIndex = 102
        '
        'lblResponsedate
        '
        Me.lblResponsedate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponsedate.Location = New System.Drawing.Point(243, 41)
        Me.lblResponsedate.Name = "lblResponsedate"
        Me.lblResponsedate.Size = New System.Drawing.Size(91, 15)
        Me.lblResponsedate.TabIndex = 101
        Me.lblResponsedate.Text = "Response Date "
        Me.lblResponsedate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOffence
        '
        Me.objbtnSearchOffence.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOffence.BorderSelected = False
        Me.objbtnSearchOffence.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOffence.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOffence.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOffence.Location = New System.Drawing.Point(583, 119)
        Me.objbtnSearchOffence.Name = "objbtnSearchOffence"
        Me.objbtnSearchOffence.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOffence.TabIndex = 99
        '
        'dtpInterdictiondate
        '
        Me.dtpInterdictiondate.Checked = False
        Me.dtpInterdictiondate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInterdictiondate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInterdictiondate.Location = New System.Drawing.Point(115, 65)
        Me.dtpInterdictiondate.Name = "dtpInterdictiondate"
        Me.dtpInterdictiondate.ShowCheckBox = True
        Me.dtpInterdictiondate.Size = New System.Drawing.Size(123, 21)
        Me.dtpInterdictiondate.TabIndex = 98
        '
        'lblInterdictionDate
        '
        Me.lblInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterdictionDate.Location = New System.Drawing.Point(7, 68)
        Me.lblInterdictionDate.Name = "lblInterdictionDate"
        Me.lblInterdictionDate.Size = New System.Drawing.Size(102, 15)
        Me.lblInterdictionDate.TabIndex = 97
        Me.lblInterdictionDate.Text = "Interdiction Date "
        Me.lblInterdictionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(243, 68)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(91, 15)
        Me.lblRefNo.TabIndex = 96
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtReferenceNo.Flags = 0
        Me.txtReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceNo.Location = New System.Drawing.Point(339, 65)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.Size = New System.Drawing.Size(233, 21)
        Me.txtReferenceNo.TabIndex = 95
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(583, 92)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 93
        '
        'lblOffenceCategory
        '
        Me.lblOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffenceCategory.Location = New System.Drawing.Point(7, 95)
        Me.lblOffenceCategory.Name = "lblOffenceCategory"
        Me.lblOffenceCategory.Size = New System.Drawing.Size(102, 15)
        Me.lblOffenceCategory.TabIndex = 91
        Me.lblOffenceCategory.Text = "Offence Category"
        Me.lblOffenceCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOffenceCategory
        '
        Me.cboOffenceCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOffenceCategory.DropDownWidth = 300
        Me.cboOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOffenceCategory.FormattingEnabled = True
        Me.cboOffenceCategory.Location = New System.Drawing.Point(115, 92)
        Me.cboOffenceCategory.Name = "cboOffenceCategory"
        Me.cboOffenceCategory.Size = New System.Drawing.Size(462, 21)
        Me.cboOffenceCategory.TabIndex = 92
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(583, 173)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 87
        '
        'dtpDisciplineDate
        '
        Me.dtpDisciplineDate.Checked = False
        Me.dtpDisciplineDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDisciplineDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDisciplineDate.Location = New System.Drawing.Point(115, 38)
        Me.dtpDisciplineDate.Name = "dtpDisciplineDate"
        Me.dtpDisciplineDate.Size = New System.Drawing.Size(123, 21)
        Me.dtpDisciplineDate.TabIndex = 71
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(7, 41)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(102, 15)
        Me.lblDate.TabIndex = 72
        Me.lblDate.Text = "Charge Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(7, 122)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(102, 15)
        Me.lblDisciplineType.TabIndex = 73
        Me.lblDisciplineType.Text = "Offence"
        Me.lblDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSeverity
        '
        Me.lblSeverity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeverity.Location = New System.Drawing.Point(7, 149)
        Me.lblSeverity.Name = "lblSeverity"
        Me.lblSeverity.Size = New System.Drawing.Size(102, 15)
        Me.lblSeverity.TabIndex = 74
        Me.lblSeverity.Text = "Severity"
        Me.lblSeverity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPesonInvolved
        '
        Me.txtPesonInvolved.BackColor = System.Drawing.SystemColors.Window
        Me.txtPesonInvolved.Flags = 0
        Me.txtPesonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPesonInvolved.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPesonInvolved.Location = New System.Drawing.Point(115, 173)
        Me.txtPesonInvolved.Name = "txtPesonInvolved"
        Me.txtPesonInvolved.ReadOnly = True
        Me.txtPesonInvolved.Size = New System.Drawing.Size(462, 21)
        Me.txtPesonInvolved.TabIndex = 81
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.DropDownWidth = 1000
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(115, 119)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(462, 21)
        Me.cboDisciplineType.TabIndex = 75
        '
        'txtSeverity
        '
        Me.txtSeverity.BackColor = System.Drawing.SystemColors.Window
        Me.txtSeverity.Flags = 0
        Me.txtSeverity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSeverity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSeverity.Location = New System.Drawing.Point(115, 146)
        Me.txtSeverity.Name = "txtSeverity"
        Me.txtSeverity.ReadOnly = True
        Me.txtSeverity.Size = New System.Drawing.Size(56, 21)
        Me.txtSeverity.TabIndex = 76
        Me.txtSeverity.Text = "1"
        '
        'objbtnAddDisciplinetype
        '
        Me.objbtnAddDisciplinetype.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddDisciplinetype.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddDisciplinetype.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddDisciplinetype.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddDisciplinetype.BorderSelected = False
        Me.objbtnAddDisciplinetype.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddDisciplinetype.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddDisciplinetype.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddDisciplinetype.Location = New System.Drawing.Point(610, 119)
        Me.objbtnAddDisciplinetype.Name = "objbtnAddDisciplinetype"
        Me.objbtnAddDisciplinetype.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddDisciplinetype.TabIndex = 77
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(7, 176)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(102, 15)
        Me.lblPersonalInvolved.TabIndex = 78
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpIncident
        '
        Me.tabpIncident.Controls.Add(Me.txtIncident)
        Me.tabpIncident.Location = New System.Drawing.Point(4, 22)
        Me.tabpIncident.Name = "tabpIncident"
        Me.tabpIncident.Size = New System.Drawing.Size(641, 333)
        Me.tabpIncident.TabIndex = 1
        Me.tabpIncident.Text = "Incident"
        Me.tabpIncident.UseVisualStyleBackColor = True
        '
        'txtIncident
        '
        Me.txtIncident.BackColor = System.Drawing.SystemColors.Window
        Me.txtIncident.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtIncident.Flags = 0
        Me.txtIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncident.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtIncident.Location = New System.Drawing.Point(0, 0)
        Me.txtIncident.MaxLength = 2147483647
        Me.txtIncident.Multiline = True
        Me.txtIncident.Name = "txtIncident"
        Me.txtIncident.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIncident.Size = New System.Drawing.Size(641, 333)
        Me.txtIncident.TabIndex = 84
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.txtAgainstPerson)
        Me.objFooter.Controls.Add(Me.lblAgainst)
        Me.objFooter.Controls.Add(Me.objbtnAgainstPerson)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 359)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(649, 55)
        Me.objFooter.TabIndex = 85
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(435, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(538, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtAgainstPerson
        '
        Me.txtAgainstPerson.BackColor = System.Drawing.SystemColors.Window
        Me.txtAgainstPerson.Flags = 0
        Me.txtAgainstPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgainstPerson.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAgainstPerson.Location = New System.Drawing.Point(80, 22)
        Me.txtAgainstPerson.Name = "txtAgainstPerson"
        Me.txtAgainstPerson.ReadOnly = True
        Me.txtAgainstPerson.Size = New System.Drawing.Size(218, 21)
        Me.txtAgainstPerson.TabIndex = 82
        Me.txtAgainstPerson.Visible = False
        '
        'lblAgainst
        '
        Me.lblAgainst.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgainst.Location = New System.Drawing.Point(14, 25)
        Me.lblAgainst.Name = "lblAgainst"
        Me.lblAgainst.Size = New System.Drawing.Size(60, 15)
        Me.lblAgainst.TabIndex = 79
        Me.lblAgainst.Text = "Against"
        Me.lblAgainst.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAgainst.Visible = False
        '
        'objbtnAgainstPerson
        '
        Me.objbtnAgainstPerson.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAgainstPerson.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAgainstPerson.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAgainstPerson.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAgainstPerson.BorderSelected = False
        Me.objbtnAgainstPerson.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAgainstPerson.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnAgainstPerson.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAgainstPerson.Location = New System.Drawing.Point(304, 22)
        Me.objbtnAgainstPerson.Name = "objbtnAgainstPerson"
        Me.objbtnAgainstPerson.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAgainstPerson.TabIndex = 88
        Me.objbtnAgainstPerson.Visible = False
        '
        'frmDisciplineFiling
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(649, 414)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineFiling"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Filing Disciplinary Charge "
        Me.Panel1.ResumeLayout(False)
        Me.tabcDisciplineFile.ResumeLayout(False)
        Me.tabpCharges.ResumeLayout(False)
        Me.gbDiscipilneFiling.ResumeLayout(False)
        Me.gbDiscipilneFiling.PerformLayout()
        Me.tabcRemark.ResumeLayout(False)
        Me.tabpRemark.ResumeLayout(False)
        Me.tabpRemark.PerformLayout()
        Me.tabpResponseRemark.ResumeLayout(False)
        Me.tabpResponseRemark.PerformLayout()
        Me.tabpIncident.ResumeLayout(False)
        Me.tabpIncident.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objbtnAddDisciplinetype As eZee.Common.eZeeGradientButton
    Friend WithEvents txtSeverity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents dtpDisciplineDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblSeverity As System.Windows.Forms.Label
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents txtAgainstPerson As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPesonInvolved As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAgainst As System.Windows.Forms.Label
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbDiscipilneFiling As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnAgainstPerson As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOffenceCategory As System.Windows.Forms.Label
    Friend WithEvents cboOffenceCategory As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents tabcDisciplineFile As System.Windows.Forms.TabControl
    Friend WithEvents tabpCharges As System.Windows.Forms.TabPage
    Friend WithEvents tabpIncident As System.Windows.Forms.TabPage
    Friend WithEvents txtIncident As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblInterdictionDate As System.Windows.Forms.Label
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents txtReferenceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpInterdictiondate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchOffence As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpResponseDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblResponsedate As System.Windows.Forms.Label
    Friend WithEvents tabcRemark As System.Windows.Forms.TabControl
    Friend WithEvents tabpRemark As System.Windows.Forms.TabPage
    Friend WithEvents tabpResponseRemark As System.Windows.Forms.TabPage
    Friend WithEvents txtResponseRemark As eZee.TextBox.AlphanumericTextBox
End Class
