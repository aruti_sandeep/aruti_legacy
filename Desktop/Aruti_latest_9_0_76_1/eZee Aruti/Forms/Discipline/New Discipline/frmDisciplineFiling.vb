﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplineFiling


#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineFiling"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objDisciplineFile As clsDiscipline_File
    Private mintDisciplineFileUnkid As Integer = -1
    Private objEmployee As clsEmployee_Master
    Private dsEmployee As DataSet = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintDisciplineFileUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintDisciplineFileUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboDisciplineType.BackColor = GUI.ColorComp
            txtSeverity.BackColor = GUI.ColorComp
            txtAgainstPerson.BackColor = GUI.ColorOptional
            txtPesonInvolved.BackColor = GUI.ColorComp
            txtIncident.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            cboOffenceCategory.BackColor = GUI.ColorComp
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtResponseRemark.BackColor = GUI.ColorComp
            'S.SANDEEP [ 16 JAN 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objEmployee = New clsEmployee_Master
            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsEmployee = objEmployee.GetEmployeeList("List", False, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsEmployee = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsEmployee = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If
            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            
            'Anjan (17 Apr 2012)-End 
            If mintDisciplineFileUnkid > 0 Then
                dtpDisciplineDate.Value = objDisciplineFile._Trandate
                'FOR AGAINSTPERSON
                txtAgainstPerson.Tag = objDisciplineFile._Against_Employeeunkid

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(txtAgainstPerson.Tag)
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtAgainstPerson.Tag)
                'S.SANDEEP [04 JUN 2015] -- END

                If CInt(txtAgainstPerson.Tag) <= 0 Then
                    txtAgainstPerson.Text = ""
                Else
                    txtAgainstPerson.Text = objEmployee._Firstname & " " & objEmployee._Surname
                End If

                'FOR PERSON INVOLVED
                txtPesonInvolved.Tag = objDisciplineFile._Involved_Employeeunkid

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(txtPesonInvolved.Tag)
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtPesonInvolved.Tag)
                'S.SANDEEP [04 JUN 2015] -- END

                txtPesonInvolved.Text = objEmployee._Firstname & " " & objEmployee._Surname

                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                Dim objDisciplineType As New clsDisciplineType
                objDisciplineType._Disciplinetypeunkid = objDisciplineFile._Disciplinetypeunkid
                cboOffenceCategory.SelectedValue = objDisciplineType._Offencecategoryunkid
                objDisciplineType = Nothing
                cboDisciplineType.SelectedValue = objDisciplineFile._Disciplinetypeunkid
                'S.SANDEEP [ 20 MARCH 2012 ] -- END
            End If
            txtIncident.Text = objDisciplineFile._Incident
            txtRemark.Text = objDisciplineFile._Remark

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            If objDisciplineFile._Interdictdate <> Nothing Then
                dtpInterdictiondate.Value = objDisciplineFile._Interdictdate
            Else
                dtpInterdictiondate.Checked = False
            End If
            txtReferenceNo.Text = objDisciplineFile._Reference_No
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objDisciplineFile._Response_Date <> Nothing Then
                dtpResponseDate.Value = objDisciplineFile._Response_Date
            Else
                dtpResponseDate.Checked = False
            End If
            txtResponseRemark.Text = objDisciplineFile._Response_Remark
            'S.SANDEEP [ 16 JAN 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objDisciplineFile._Trandate = dtpDisciplineDate.Value.Date
            objDisciplineFile._Disciplinetypeunkid = CInt(cboDisciplineType.SelectedValue)
            objDisciplineFile._Against_Employeeunkid = CInt(txtAgainstPerson.Tag)
            objDisciplineFile._Involved_Employeeunkid = CInt(txtPesonInvolved.Tag)
            objDisciplineFile._Incident = txtIncident.Text.Trim
            objDisciplineFile._Remark = txtRemark.Text.Trim
            objDisciplineFile._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            If menAction = enAction.EDIT_ONE Then
                objDisciplineFile._Disciplinestatusunkid = objDisciplineFile._Disciplinestatusunkid
            Else
                objDisciplineFile._Disciplinestatusunkid = 2
            End If
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            If dtpInterdictiondate.Checked = True Then
                objDisciplineFile._Interdictdate = dtpInterdictiondate.Value
            Else
                objDisciplineFile._Interdictdate = Nothing
            End If
            objDisciplineFile._Reference_No = txtReferenceNo.Text
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dtpResponseDate.Checked = True Then
                objDisciplineFile._Response_Date = dtpResponseDate.Value
            Else
                objDisciplineFile._Response_Date = Nothing
            End If
            objDisciplineFile._Response_Remark = txtResponseRemark.Text
            'S.SANDEEP [ 16 JAN 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim objCMaster As New clsCommon_Master
        Try
            dsFill = objCMaster.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "List")
            With cboOffenceCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsFill.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboDisciplineType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Discipline Type is compulsory information.Please Select Discipline Type."), enMsgBoxStyle.Information)
                cboDisciplineType.Select()
                Return False
            ElseIf txtPesonInvolved.Tag Is Nothing Or CInt(txtPesonInvolved.Tag) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Person Involved is compulsory information.Please Select Person Involved."), enMsgBoxStyle.Information)
                txtPesonInvolved.Select()
                Return False
            ElseIf txtIncident.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Incident cannot be blank.Incident is required information."), enMsgBoxStyle.Information)
                txtIncident.Select()
                Return False
            End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            If dtpInterdictiondate.Checked = True Then
                Dim mdtOrgDate As Date = Nothing
                If objDisciplineFile.IsInterdictionDateExists(dtpInterdictiondate.Value, txtReferenceNo.Text, mdtOrgDate) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot set this inderdiction date. Reason : For this Reference No Interdiction date is already set to as :") & " (" & mdtOrgDate & ")", enMsgBoxStyle.Information)
                    dtpInterdictiondate.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dtpResponseDate.Checked = True Then
                If txtResponseRemark.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Reponse remark is mandatory information. Response remark cannot be blank."), enMsgBoxStyle.Information)
                    tabcRemark.SelectedTab = tabpResponseRemark
                    txtResponseRemark.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 16 JAN 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub SetVisibility()
        Try
            objbtnAddDisciplinetype.Enabled = User._Object.Privilege._AddDisciplineOffence
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            Dim objResoution As New clsDiscipline_Resolution
            Dim intValue As Integer = -1
            intValue = objResoution.IsCase_Can_Close(mintDisciplineFileUnkid)
            If intValue > 1 Then
                cboDisciplineType.Enabled = False : objbtnAddDisciplinetype.Enabled = False
                cboOffenceCategory.Enabled = False : objbtnSearchCategory.Enabled = False
                objbtnSearchEmployee.Enabled = False : txtIncident.ReadOnly = True
            End If
            objResoution = Nothing
            'S.SANDEEP [ 20 MARCH 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineFiling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDisciplineFile = New clsDiscipline_File
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            SetColor()

            If menAction = enAction.EDIT_ONE Then
                objDisciplineFile._Disciplinefileunkid = mintDisciplineFileUnkid
            End If

            FillCombo()
            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            tabcRemark.TabPages.Remove(tabpResponseRemark)
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            GetValue()
            'dtpDisciplineDate.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFiling_Load", mstrModuleName)
        End Try
    End Sub

    'Private Sub frmDisciplineFiling_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
    '    Try
    '        If Asc(e.KeyChar) = 13 Then
    '            SendKeys.Send("{TAB}")
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "frmDisciplineFiling_KeyPress", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub frmDisciplineFiling_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFiling_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineAnalysis_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDisciplineFile = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_File.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_File"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim strEmpMessage As String = String.Empty
        Try
            If Validation() Then
                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                If menAction <> enAction.EDIT_ONE Then
                    If dtpDisciplineDate.Value.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Selected Discipline file date is appropriate.Do you want to continue with this selected date?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                    End If
                End If
                'S.SANDEEP [ 20 MARCH 2012 ] -- END
                SetValue()
                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objDisciplineFile.Update()
                Else
                    blnFlag = objDisciplineFile.Insert()
                End If

                If blnFlag = False And objDisciplineFile._Message <> "" Then
                    eZeeMsgBox.Show(objDisciplineFile._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then
                    mblnCancel = False
                    If menAction = enAction.ADD_CONTINUE Then
                        objDisciplineFile = Nothing
                        objDisciplineFile = New clsDiscipline_File
                        Call GetValue()
                        dtpDisciplineDate.Select()
                    Else
                        mintDisciplineFileUnkid = objDisciplineFile._Disciplinefileunkid
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objCommon As New frmCommonSearch
        Dim dtFill As DataTable = Nothing
        Dim strSearching As String = ""
        Try
            If User._Object._Isrighttoleft = True Then
                objCommon.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCommon.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCommon)
            End If

            If CInt(txtAgainstPerson.Tag) > 0 Then
                strSearching &= "AND employeeunkid <> " & CInt(txtAgainstPerson.Tag) & " "
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtFill = New DataView(dsEmployee.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtFill = dsEmployee.Tables("List")
            End If
            objCommon.ValueMember = "employeeunkid"
            objCommon.DisplayMember = "employeename"
            objCommon.CodeMember = "employeecode"
            objCommon.DataSource = dtFill
            If objCommon.DisplayDialog Then
                txtPesonInvolved.Tag = objCommon.SelectedValue
                txtPesonInvolved.Text = objCommon.SelectedText
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objCommon.Dispose() : dtFill.Dispose() : strSearching = String.Empty
        End Try
    End Sub

    Private Sub objbtnAgainstPerson_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAgainstPerson.Click
        Dim objCommon As New frmCommonSearch
        Dim dtFill As DataTable = Nothing
        Dim strSearching As String = ""
        Try
            If User._Object._Isrighttoleft = True Then
                objCommon.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCommon.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCommon)
            End If

            If CInt(txtPesonInvolved.Tag) > 0 Then
                strSearching &= "AND employeeunkid <> " & CInt(txtPesonInvolved.Tag) & " "
            End If
            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtFill = New DataView(dsEmployee.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtFill = dsEmployee.Tables("List")
            End If
            objCommon.ValueMember = "employeeunkid"
            objCommon.DisplayMember = "employeename"
            objCommon.CodeMember = "employeecode"
            objCommon.DataSource = dtFill
            If objCommon.DisplayDialog Then
                txtAgainstPerson.Tag = objCommon.SelectedValue
                txtAgainstPerson.Text = objCommon.SelectedText
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAgainstPerson_Click", mstrModuleName)
        Finally
            objCommon.Dispose() : dtFill.Dispose() : strSearching = String.Empty
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnAddDisciplinetype_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDisciplinetype.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmDisciplineType_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objDisciplineType As New clsDisciplineType
                Dim dsList As New DataSet
                dsList = objDisciplineType.getComboList("DType", True)
                With cboDisciplineType
                    .ValueMember = "disciplinetypeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("DType")
                    .SelectedValue = intRefId
                End With
                objDisciplineType = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddDisciplinetype_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboOffenceCategory.ValueMember
                .DisplayMember = cboOffenceCategory.DisplayMember
                .DataSource = CType(cboOffenceCategory.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboOffenceCategory.SelectedValue = frm.SelectedValue
                cboOffenceCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

    'S.SANDEEP [ 01 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA DISCIPLINE CHANGES
    Private Sub objbtnSearchOffence_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOffence.Click
        Dim frm As New frmCommonSearch
        Try
            If cboDisciplineType.DataSource Is Nothing Then Exit Sub
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboDisciplineType.ValueMember
                .DisplayMember = cboDisciplineType.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboDisciplineType.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboDisciplineType.SelectedValue = frm.SelectedValue
                cboDisciplineType.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOffence_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 01 JUNE 2012 ] -- END

#End Region

#Region "Dropdown's Event"

    Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
        Try
            If CInt(cboOffenceCategory.SelectedValue) > 0 Then
                Dim dsFill As DataSet = Nothing
                Dim objDisciplineType As New clsDisciplineType
                dsFill = objDisciplineType.getComboList("DisciplineType", True, CInt(cboOffenceCategory.SelectedValue))
                cboDisciplineType.ValueMember = "disciplinetypeunkid"
                cboDisciplineType.DisplayMember = "name"
                cboDisciplineType.DataSource = dsFill.Tables("DisciplineType")
            Else
                cboDisciplineType.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOffenceCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboDisciplineType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDisciplineType.SelectedIndexChanged
        Try
            If CInt(cboDisciplineType.SelectedValue) > 0 Then
                Dim objDisicipline As New clsDisciplineType
                objDisicipline._Disciplinetypeunkid = CInt(cboDisciplineType.SelectedValue)
                txtSeverity.Text = objDisicipline._Severity.ToString
            Else
                txtSeverity.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDisciplineType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Controls Events "

    Private Sub dtpResponseDate_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dtpResponseDate.MouseUp
        If e.Button = Windows.Forms.MouseButtons.Left Then
            Call dtpResponseDate_ValueChanged(sender, New EventArgs())
        End If
    End Sub

    Private Sub dtpResponseDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpResponseDate.ValueChanged
        Try
            If dtpResponseDate.Checked = True Then
                If tabcRemark.TabPages.Contains(tabpResponseRemark) = False Then
                    tabcRemark.TabPages.Add(tabpResponseRemark)
                End If
            Else
                tabcRemark.TabPages.Remove(tabpResponseRemark)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "dtpResponseDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 16 JAN 2013 ] -- END



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbDiscipilneFiling.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDiscipilneFiling.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblSeverity.Text = Language._Object.getCaption(Me.lblSeverity.Name, Me.lblSeverity.Text)
            Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblAgainst.Text = Language._Object.getCaption(Me.lblAgainst.Name, Me.lblAgainst.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbDiscipilneFiling.Text = Language._Object.getCaption(Me.gbDiscipilneFiling.Name, Me.gbDiscipilneFiling.Text)
			Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.Name, Me.lblOffenceCategory.Text)
			Me.tabpCharges.Text = Language._Object.getCaption(Me.tabpCharges.Name, Me.tabpCharges.Text)
			Me.tabpIncident.Text = Language._Object.getCaption(Me.tabpIncident.Name, Me.tabpIncident.Text)
			Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
			Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
			Me.lblResponsedate.Text = Language._Object.getCaption(Me.lblResponsedate.Name, Me.lblResponsedate.Text)
			Me.tabpRemark.Text = Language._Object.getCaption(Me.tabpRemark.Name, Me.tabpRemark.Text)
			Me.tabpResponseRemark.Text = Language._Object.getCaption(Me.tabpResponseRemark.Name, Me.tabpResponseRemark.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Discipline Type is compulsory information.Please Select Discipline Type.")
            Language.setMessage(mstrModuleName, 2, "Person Involved is compulsory information.Please Select Person Involved.")
            Language.setMessage(mstrModuleName, 3, "Incident cannot be blank.Incident is required information.")
	    Language.setMessage(mstrModuleName, 4, "Sorry, you cannot set this inderdiction date. Reason : For this Reference No Interdiction date is already set to as :")
	    Language.setMessage(mstrModuleName, 5, "Selected Discipline file date is appropriate.Do you want to continue with this selected date?")
	    Language.setMessage(mstrModuleName, 6, "Sorry, Reponse remark is mandatory information. Response remark cannot be blank.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class