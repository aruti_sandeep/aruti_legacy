﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineResolutionList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineResolutionList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.lvDisciplineResolutions = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhDisciplinaryAction = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhRSteps = New System.Windows.Forms.ColumnHeader
        Me.colhApprovedStatus = New System.Windows.Forms.ColumnHeader
        Me.objcolhPersonInvolved = New System.Windows.Forms.ColumnHeader
        Me.objcolhFileUnkid = New System.Windows.Forms.ColumnHeader
        Me.colhIncident = New System.Windows.Forms.ColumnHeader
        Me.objcolhInvolvedEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsapproved = New System.Windows.Forms.ColumnHeader
        Me.objcolhDisciplineActionId = New System.Windows.Forms.ColumnHeader
        Me.objcolhDisciplineStatusId = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.cboApprovedStatus = New System.Windows.Forms.ComboBox
        Me.cboDisciplineStatus = New System.Windows.Forms.ComboBox
        Me.lblApprovedStatus = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtPersonInvolved = New eZee.TextBox.AlphanumericTextBox
        Me.lblPersonInvolved = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.dtpDisciplineDate = New System.Windows.Forms.DateTimePicker
        Me.cboDisciplineAction = New System.Windows.Forms.ComboBox
        Me.lblDisciplinaryAction = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblComplete = New System.Windows.Forms.Label
        Me.lblApproved = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.pnlGreen = New System.Windows.Forms.Panel
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objChkAll)
        Me.pnlMain.Controls.Add(Me.lvDisciplineResolutions)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(788, 452)
        Me.pnlMain.TabIndex = 0
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(20, 163)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 12
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'lvDisciplineResolutions
        '
        Me.lvDisciplineResolutions.BackColorOnChecked = False
        Me.lvDisciplineResolutions.ColumnHeaders = Nothing
        Me.lvDisciplineResolutions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhDate, Me.colhDisciplinaryAction, Me.colhStatus, Me.colhRSteps, Me.colhApprovedStatus, Me.objcolhPersonInvolved, Me.objcolhFileUnkid, Me.colhIncident, Me.objcolhInvolvedEmpId, Me.objcolhIsapproved, Me.objcolhDisciplineActionId, Me.objcolhDisciplineStatusId})
        Me.lvDisciplineResolutions.CompulsoryColumns = ""
        Me.lvDisciplineResolutions.FullRowSelect = True
        Me.lvDisciplineResolutions.GridLines = True
        Me.lvDisciplineResolutions.GroupingColumn = Nothing
        Me.lvDisciplineResolutions.HideSelection = False
        Me.lvDisciplineResolutions.Location = New System.Drawing.Point(12, 159)
        Me.lvDisciplineResolutions.MinColumnWidth = 50
        Me.lvDisciplineResolutions.MultiSelect = False
        Me.lvDisciplineResolutions.Name = "lvDisciplineResolutions"
        Me.lvDisciplineResolutions.OptionalColumns = ""
        Me.lvDisciplineResolutions.ShowMoreItem = False
        Me.lvDisciplineResolutions.ShowSaveItem = False
        Me.lvDisciplineResolutions.ShowSelectAll = True
        Me.lvDisciplineResolutions.ShowSizeAllColumnsToFit = True
        Me.lvDisciplineResolutions.Size = New System.Drawing.Size(764, 234)
        Me.lvDisciplineResolutions.Sortable = True
        Me.lvDisciplineResolutions.TabIndex = 11
        Me.lvDisciplineResolutions.UseCompatibleStateImageBehavior = False
        Me.lvDisciplineResolutions.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 80
        '
        'colhDisciplinaryAction
        '
        Me.colhDisciplinaryAction.DisplayIndex = 3
        Me.colhDisciplinaryAction.Tag = "colhDisciplinaryAction"
        Me.colhDisciplinaryAction.Text = "Disciplinary Penalties"
        Me.colhDisciplinaryAction.Width = 110
        '
        'colhStatus
        '
        Me.colhStatus.DisplayIndex = 4
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 90
        '
        'colhRSteps
        '
        Me.colhRSteps.DisplayIndex = 5
        Me.colhRSteps.Tag = "colhRSteps"
        Me.colhRSteps.Text = "Proceeding Steps"
        Me.colhRSteps.Width = 240
        '
        'colhApprovedStatus
        '
        Me.colhApprovedStatus.DisplayIndex = 6
        Me.colhApprovedStatus.Tag = "colhApprovedStatus"
        Me.colhApprovedStatus.Text = "A.Status"
        Me.colhApprovedStatus.Width = 80
        '
        'objcolhPersonInvolved
        '
        Me.objcolhPersonInvolved.DisplayIndex = 7
        Me.objcolhPersonInvolved.Tag = "objcolhPersonInvolved"
        Me.objcolhPersonInvolved.Text = "objcolhPersonInvolved"
        Me.objcolhPersonInvolved.Width = 0
        '
        'objcolhFileUnkid
        '
        Me.objcolhFileUnkid.DisplayIndex = 8
        Me.objcolhFileUnkid.Tag = "objcolhFileUnkid"
        Me.objcolhFileUnkid.Text = "objcolhFileUnkid"
        Me.objcolhFileUnkid.Width = 0
        '
        'colhIncident
        '
        Me.colhIncident.DisplayIndex = 2
        Me.colhIncident.Tag = "colhIncident"
        Me.colhIncident.Text = "Incident"
        Me.colhIncident.Width = 135
        '
        'objcolhInvolvedEmpId
        '
        Me.objcolhInvolvedEmpId.Tag = "objcolhInvolvedEmpId"
        Me.objcolhInvolvedEmpId.Text = ""
        Me.objcolhInvolvedEmpId.Width = 0
        '
        'objcolhIsapproved
        '
        Me.objcolhIsapproved.Tag = "objcolhIsapproved"
        Me.objcolhIsapproved.Text = "objcolhIsapproved"
        Me.objcolhIsapproved.Width = 0
        '
        'objcolhDisciplineActionId
        '
        Me.objcolhDisciplineActionId.Tag = "objcolhDisciplineActionId"
        Me.objcolhDisciplineActionId.Text = "objcolhDisciplineActionId"
        Me.objcolhDisciplineActionId.Width = 0
        '
        'objcolhDisciplineStatusId
        '
        Me.objcolhDisciplineStatusId.Tag = "objcolhDisciplineStatusId"
        Me.objcolhDisciplineStatusId.Text = ""
        Me.objcolhDisciplineStatusId.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.cboApprovedStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprovedStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.txtPersonInvolved)
        Me.gbFilterCriteria.Controls.Add(Me.lblPersonInvolved)
        Me.gbFilterCriteria.Controls.Add(Me.lblDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDisciplineDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineAction)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplinaryAction)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(764, 89)
        Me.gbFilterCriteria.TabIndex = 10
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(604, 33)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(153, 21)
        Me.cboDisciplineType.TabIndex = 120
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(519, 36)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(81, 15)
        Me.lblDisciplineType.TabIndex = 119
        Me.lblDisciplineType.Text = "Offences"
        Me.lblDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApprovedStatus
        '
        Me.cboApprovedStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprovedStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprovedStatus.FormattingEnabled = True
        Me.cboApprovedStatus.Location = New System.Drawing.Point(604, 60)
        Me.cboApprovedStatus.Name = "cboApprovedStatus"
        Me.cboApprovedStatus.Size = New System.Drawing.Size(153, 21)
        Me.cboApprovedStatus.TabIndex = 115
        '
        'cboDisciplineStatus
        '
        Me.cboDisciplineStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineStatus.FormattingEnabled = True
        Me.cboDisciplineStatus.Location = New System.Drawing.Point(416, 60)
        Me.cboDisciplineStatus.Name = "cboDisciplineStatus"
        Me.cboDisciplineStatus.Size = New System.Drawing.Size(97, 21)
        Me.cboDisciplineStatus.TabIndex = 112
        '
        'lblApprovedStatus
        '
        Me.lblApprovedStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedStatus.Location = New System.Drawing.Point(519, 63)
        Me.lblApprovedStatus.Name = "lblApprovedStatus"
        Me.lblApprovedStatus.Size = New System.Drawing.Size(81, 15)
        Me.lblApprovedStatus.TabIndex = 114
        Me.lblApprovedStatus.Text = "Appro. Status"
        Me.lblApprovedStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(416, 36)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(93, 15)
        Me.lblStatus.TabIndex = 99
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPersonInvolved
        '
        Me.txtPersonInvolved.Flags = 0
        Me.txtPersonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPersonInvolved.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPersonInvolved.Location = New System.Drawing.Point(125, 60)
        Me.txtPersonInvolved.Name = "txtPersonInvolved"
        Me.txtPersonInvolved.Size = New System.Drawing.Size(285, 21)
        Me.txtPersonInvolved.TabIndex = 106
        '
        'lblPersonInvolved
        '
        Me.lblPersonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonInvolved.Location = New System.Drawing.Point(5, 63)
        Me.lblPersonInvolved.Name = "lblPersonInvolved"
        Me.lblPersonInvolved.Size = New System.Drawing.Size(117, 15)
        Me.lblPersonInvolved.TabIndex = 105
        Me.lblPersonInvolved.Text = "Person Involved"
        Me.lblPersonInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(273, 36)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(38, 15)
        Me.lblDate.TabIndex = 93
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDisciplineDate
        '
        Me.dtpDisciplineDate.Checked = False
        Me.dtpDisciplineDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDisciplineDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDisciplineDate.Location = New System.Drawing.Point(314, 33)
        Me.dtpDisciplineDate.Name = "dtpDisciplineDate"
        Me.dtpDisciplineDate.ShowCheckBox = True
        Me.dtpDisciplineDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpDisciplineDate.TabIndex = 96
        '
        'cboDisciplineAction
        '
        Me.cboDisciplineAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineAction.FormattingEnabled = True
        Me.cboDisciplineAction.Location = New System.Drawing.Point(125, 34)
        Me.cboDisciplineAction.Name = "cboDisciplineAction"
        Me.cboDisciplineAction.Size = New System.Drawing.Size(142, 21)
        Me.cboDisciplineAction.TabIndex = 97
        '
        'lblDisciplinaryAction
        '
        Me.lblDisciplinaryAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplinaryAction.Location = New System.Drawing.Point(5, 36)
        Me.lblDisciplinaryAction.Name = "lblDisciplinaryAction"
        Me.lblDisciplinaryAction.Size = New System.Drawing.Size(117, 15)
        Me.lblDisciplinaryAction.TabIndex = 94
        Me.lblDisciplinaryAction.Text = "Disciplinary Penalities"
        Me.lblDisciplinaryAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(737, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(714, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(788, 58)
        Me.eZeeHeader.TabIndex = 9
        Me.eZeeHeader.Title = "Disciplinary Proceedings List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblComplete)
        Me.objFooter.Controls.Add(Me.lblApproved)
        Me.objFooter.Controls.Add(Me.Panel2)
        Me.objFooter.Controls.Add(Me.pnlGreen)
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 397)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(788, 55)
        Me.objFooter.TabIndex = 8
        '
        'lblComplete
        '
        Me.lblComplete.Location = New System.Drawing.Point(322, 21)
        Me.lblComplete.Name = "lblComplete"
        Me.lblComplete.Size = New System.Drawing.Size(93, 14)
        Me.lblComplete.TabIndex = 128
        Me.lblComplete.Text = "Completed Case"
        '
        'lblApproved
        '
        Me.lblApproved.Location = New System.Drawing.Point(180, 21)
        Me.lblApproved.Name = "lblApproved"
        Me.lblApproved.Size = New System.Drawing.Size(93, 14)
        Me.lblApproved.TabIndex = 128
        Me.lblApproved.Text = "Approved"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel2.Location = New System.Drawing.Point(279, 20)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(37, 17)
        Me.Panel2.TabIndex = 127
        '
        'pnlGreen
        '
        Me.pnlGreen.BackColor = System.Drawing.Color.SeaGreen
        Me.pnlGreen.Location = New System.Drawing.Point(137, 20)
        Me.pnlGreen.Name = "pnlGreen"
        Me.pnlGreen.Size = New System.Drawing.Size(37, 17)
        Me.pnlGreen.TabIndex = 126
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(12, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnApprove.TabIndex = 125
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(576, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 124
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(473, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 123
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(679, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmDisciplineResolutionList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(788, 452)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineResolutionList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Disciplinary Proceedings List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboDisciplineStatus As System.Windows.Forms.ComboBox
    Friend WithEvents txtPersonInvolved As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPersonInvolved As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpDisciplineDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboDisciplineAction As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplinaryAction As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lvDisciplineResolutions As eZee.Common.eZeeListView
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDisciplinaryAction As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRSteps As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPersonInvolved As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboApprovedStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovedStatus As System.Windows.Forms.Label
    Friend WithEvents colhApprovedStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents objcolhFileUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents colhIncident As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhInvolvedEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsapproved As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDisciplineActionId As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlGreen As System.Windows.Forms.Panel
    Friend WithEvents lblComplete As System.Windows.Forms.Label
    Friend WithEvents lblApproved As System.Windows.Forms.Label
    Friend WithEvents objcolhDisciplineStatusId As System.Windows.Forms.ColumnHeader
End Class
