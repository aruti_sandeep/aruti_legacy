﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region


Public Class frmImportFinalShortListingApplicants

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportFinalShortListingApplicants"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Dim objAccrue As clsleavebalance_tran
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmImportFinalShortListingApplicants_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objAccrue = New clsleavebalance_tran
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportFinalShortListingApplicants_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleavebalance_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleavebalance_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillGirdView()
        Try
            If dsList IsNot Nothing Then

                dsList.Tables(0).Columns.Add("isChecked", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("image", Type.GetType("System.Object"))
                dsList.Tables(0).Columns.Add("applicantunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("vacancyunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("AUD", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("error", Type.GetType("System.String"))

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim objApp As New clsApplicant_master
                    Dim objCommon As New clsCommon_Master
                    Dim objVacancy As New clsVacancy
                    Dim objFinalShortList As New clsshortlist_finalapplicant

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        dsList.Tables(0).Rows(i)("isChecked") = False
                        dsList.Tables(0).Rows(i)("AUD") = "A"
                        dsList.Tables(0).Rows(i)("error") = ""
                        dsList.Tables(0).Rows(i)("image") = New Drawing.Bitmap(1, 1).Clone
                        dsList.Tables(0).Rows(i)("applicantunkid") = objApp.GetApplicantIDByCode(dsList.Tables(0).Rows(i)("App.Code").ToString.Trim)

                        Dim xVacancyTitleId As Integer = 0
                        xVacancyTitleId = objCommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.VACANCY_MASTER, dsList.Tables(0).Rows(i)("vacancy").ToString.Trim)
                        Dim mstrFilter As String = "rcvacancy_master.vacancytitle =  " & xVacancyTitleId & " AND CONVERT(CHAR(8),rcvacancy_master.openingdate,112) = '" & _
                                                                eZeeDate.convertDate(CDate(dsList.Tables(0).Rows(i)("startdate")).Date) & "' AND CONVERT(CHAR(8),rcvacancy_master.closingdate,112) = '" & _
                                                                eZeeDate.convertDate(CDate(dsList.Tables(0).Rows(i)("enddate")).Date) & "'"

                        Dim dsVacancy As DataSet = objVacancy.GetList("List", True, mstrFilter)

                        If dsVacancy IsNot Nothing AndAlso dsVacancy.Tables(0).Rows.Count > 0 Then
                            dsList.Tables(0).Rows(i)("vacancyunkid") = CInt(dsVacancy.Tables(0).Rows(0)("vacancyunkid"))
                        Else
                            dsList.Tables(0).Rows(i)("vacancyunkid") = 0
                        End If

                        If IsDBNull(dsList.Tables(0).Rows(i)("startdate")) = False AndAlso dsList.Tables(0).Rows(i)("startdate").ToString().Trim().Length > 0 Then
                            dsList.Tables(0).Rows(i)("startdate") = CDate(dsList.Tables(0).Rows(i)("startdate")).ToShortDateString()
                        End If

                        If IsDBNull(dsList.Tables(0).Rows(i)("enddate")) = False AndAlso dsList.Tables(0).Rows(i)("enddate").ToString().Trim().Length > 0 Then
                            dsList.Tables(0).Rows(i)("enddate") = CDate(dsList.Tables(0).Rows(i)("enddate")).ToShortDateString()
                        End If

                    Next
                    objCommon = Nothing
                    objApp = Nothing
                End If

            End If

            dgFinalApplicants.AutoGenerateColumns = False

            dgFinalApplicants.DataSource = dsList.Tables(0)

            colhImage.DataPropertyName = "image"
            colhApplicantCode.DataPropertyName = "App.Code"
            colhVacancy.DataPropertyName = "vacancy"
            colhStartDate.DataPropertyName = "startdate"
            colhEndDate.DataPropertyName = "enddate"
            objcolhIscheck.DataPropertyName = "isChecked"
            colhError.DataPropertyName = "error"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then

                Cursor = Cursors.WaitCursor
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                dsList = OpenXML_Import(txtFilePath.Text)

                Dim strQuery As String = ""
                For i As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    dsList.Tables(0).Columns(i).ColumnName = dsList.Tables(0).Columns(i).ColumnName.Replace(" ", "_")
                    strQuery &= " AND " & dsList.Tables(0).Columns(i).ColumnName & " IS NULL "
                Next

                If strQuery.Trim.Length > 0 Then
                    strQuery = strQuery.Trim.Substring(4, strQuery.Trim.Length - 4)
                End If

                Dim drRow As DataRow() = dsList.Tables(0).Select(strQuery)

                If drRow.Length > 0 Then

                    For j As Integer = 0 To drRow.Length - 1
                        dsList.Tables(0).Rows.Remove(drRow(j))
                    Next
                    dsList.Tables(0).AcceptChanges()

                End If
                FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try
            Dim blnError As Boolean = False

            If dgFinalApplicants.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to import Final shortlisted applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            Else

                Me.Cursor = Cursors.WaitCursor

                btnImportData.Enabled = False
                btnExportError.Enabled = False

                Dim objShortList As New clsshortlist_finalapplicant
                Dim xCountFinalApplicants As Integer = 0
                Dim intIndex As Integer = 0

                For Each dr As DataRow In dsList.Tables(0).Rows

                    xCountFinalApplicants += 1

                    lnkFinalApplicantsProcess.Text = Language.getMessage(mstrModuleName, 13, "Final Applicants Processed : ") & xCountFinalApplicants & "/" & dsList.Tables(0).Rows.Count
                    Try
                        dgFinalApplicants.FirstDisplayedScrollingRowIndex = dsList.Tables(0).Rows.IndexOf(dr) - 16
                        Application.DoEvents()
                    Catch ex As Exception
                    End Try

                    If CInt(dr("applicantunkid")) <= 0 Then
                        dgFinalApplicants.Rows(intIndex).Cells(colhApplicantCode.Index).ErrorText = Language.getMessage(mstrModuleName, 5, "Invalid Applicant Code.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhApplicantCode.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhApplicantCode.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 5, "Invalid Applicant Code.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf CInt(dr("vacancyunkid")) <= 0 Then
                        dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText = Language.getMessage(mstrModuleName, 6, "Invalid Vacancy Name.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 6, "Invalid Vacancy Name.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf IsDBNull(dr("startdate")) Then
                        dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ErrorText = Language.getMessage(mstrModuleName, 2, "Start date is compulsory information.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 2, "Start date is compulsory information.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf IsDBNull(dr("enddate")) Then
                        dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ErrorText = Language.getMessage(mstrModuleName, 8, "End date is compulsory information.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 3, "End date is compulsory information.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf IsDBNull(dr("enddate")) = False AndAlso (CDate(dr("enddate")) < CDate(dr("startdate"))) Then
                        dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ErrorText = Language.getMessage(mstrModuleName, 4, "Invalid Start Date.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhStartDate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 4, "Invalid Start Date.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf CInt(dr("applicantunkid")) <= 0 Then
                        dgFinalApplicants.Rows(intIndex).Cells(colhApplicantCode.Index).ErrorText = Language.getMessage(mstrModuleName, 5, "Invalid Applicant Code.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhApplicantCode.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhApplicantCode.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 5, "Invalid Applicant Code.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf CInt(dr("vacancyunkid")) <= 0 Then
                        dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText = Language.getMessage(mstrModuleName, 6, "Invalid Vacancy Name.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 6, "Invalid Vacancy Name.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    End If

                    dr("isChecked") = True


                    Dim drRow() As DataRow = dsList.Tables(0).Select("applicantunkid = " & CInt(dr("applicantunkid")))

                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then

                        Dim dtTable As DataTable = drRow.CopyToDataTable()

                        If objShortList.Set_FinalApplicant(dtTable, CInt(dr("vacancyunkid"))) Then

                            If ConfigParameter._Object._SkipApprovalFlowForFinalApplicant Then

                                '/* START TO SUBMIT FOR APPROVAL AUTOMATICALLY
                                Dim mblnFlag As Boolean = objShortList.UpdateApplicantForSubmitForApproval(CInt(dr("vacancyunkid")), CInt(dr("applicantunkid")))

                                If mblnFlag = False Then
                                    dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                                    dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText
                                    dr("error") = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                                    dr("Image") = imgError
                                    blnError = True
                                    intIndex += 1
                                    Continue For
                                End If
                                '/* END TO SUBMIT FOR APPROVAL AUTOMATICALLY



                                '/* START TO APPROVE APPLICANT AUTOMATICALLY
                                If objShortList.ApproveFinalApplicant(CInt(dr("vacancyunkid")), dtTable) = False Then
                                    dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                                    dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText
                                    dr("error") = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                                    dr("Image") = imgError
                                    blnError = True
                                    intIndex += 1
                                    Continue For
                                End If
                                '/* END TO APPROVE APPLICANT AUTOMATICALLY

                            End If

                            If dtTable IsNot Nothing Then dtTable.Rows.Clear()
                            dtTable.Dispose()
                            dtTable = Nothing
                            GC.Collect()

                        Else

                            dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                            dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText
                            dr("error") = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                            dr("Image") = imgError
                            blnError = True
                            intIndex += 1
                            Continue For

                        End If  '  If drRow IsNot Nothing AndAlso drRow.Length > 0 Then

                    Else
                        dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                        dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ToolTipText = dgFinalApplicants.Rows(intIndex).Cells(colhVacancy.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    End If

                    dr("Image") = imgAccept
                    intIndex += 1

                    GC.Collect(0, GCCollectionMode.Forced)
                Next

                btnImportData.Enabled = True
                btnExportError.Enabled = True

                Dim drError As DataRow() = dsList.Tables(0).Select("error <> '' ")

                If drError.Length > 0 Then
                    If drError.Length < dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Some Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        btnExportError_Click(btnExportError, New EventArgs())
                        Exit Sub

                    ElseIf drError.Length = dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        btnExportError_Click(btnExportError, New EventArgs())
                        Exit Sub

                    End If
                End If

                If blnError = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Data successfully imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList.Dispose()
                    dsList = Nothing
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnImportData_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Private Sub btnGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim objSave As New SaveFileDialog
    '        objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
    '        If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            Dim dsList As DataSet = objAccrue.GetImportFileStructure(False, ConfigParameter._Object._LeaveAccrueTenureSetting)
    '            OpenXML_Export(objSave.FileName, dsList)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("1-", ex.Message, "btnGetFileFormat_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportError.Click
        Try

            If dgFinalApplicants.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no data to show error(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dsList IsNot Nothing Then
                Dim dvGriddata As DataView = dsList.Tables(0).DefaultView
                dvGriddata.RowFilter = "error <>''"
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("isChecked")
                        dtTable.Columns.Remove("applicantunkid")
                        dtTable.Columns.Remove("vacancyunkid")
                        dtTable.Columns.Remove("AUD")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Final ShortListing Applicants") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.lnkFinalApplicantsProcess.Text = Language._Object.getCaption(Me.lnkFinalApplicantsProcess.Name, Me.lnkFinalApplicantsProcess.Text)
            Me.colhImage.HeaderText = Language._Object.getCaption(Me.colhImage.Name, Me.colhImage.HeaderText)
            Me.colhApplicantCode.HeaderText = Language._Object.getCaption(Me.colhApplicantCode.Name, Me.colhApplicantCode.HeaderText)
            Me.colhVacancy.HeaderText = Language._Object.getCaption(Me.colhVacancy.Name, Me.colhVacancy.HeaderText)
            Me.colhStartDate.HeaderText = Language._Object.getCaption(Me.colhStartDate.Name, Me.colhStartDate.HeaderText)
            Me.colhEndDate.HeaderText = Language._Object.getCaption(Me.colhEndDate.Name, Me.colhEndDate.HeaderText)
            Me.colhError.HeaderText = Language._Object.getCaption(Me.colhError.Name, Me.colhError.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no data to import Final shortlisted applicant(s).")
            Language.setMessage(mstrModuleName, 2, "Start date is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "End date is compulsory information.")
            Language.setMessage(mstrModuleName, 4, "Invalid Start Date.")
            Language.setMessage(mstrModuleName, 5, "Invalid Applicant Code.")
            Language.setMessage(mstrModuleName, 6, "Invalid Vacancy Name.")
            Language.setMessage(mstrModuleName, 7, "There is no data to show error(s).")
            Language.setMessage(mstrModuleName, 8, "End date is compulsory information.")
            Language.setMessage(mstrModuleName, 9, "Final Applicant assignment process fail for this applicant.")
            Language.setMessage(mstrModuleName, 10, "Some Data did not import due to error.")
            Language.setMessage(mstrModuleName, 11, "Data did not import due to error.")
            Language.setMessage(mstrModuleName, 12, "Data successfully imported.")
            Language.setMessage(mstrModuleName, 13, "Final Applicants Processed :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class