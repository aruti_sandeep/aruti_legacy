﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDownloadAptitudeResult
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDownloadAptitudeResult))
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.dgAptitudeResult = New System.Windows.Forms.DataGridView
        Me.dgcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhFirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSurname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFinalGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhErrorMsg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objbtnImport = New eZee.Common.eZeeSplitButton
        Me.cmImportAptitudeResult = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnImportData = New System.Windows.Forms.ToolStripMenuItem
        Me.btnShowSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.btnShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.btnExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbVacancyFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchResult = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.LblVacancy = New System.Windows.Forms.Label
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.LblVacanyType = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgAptitudeResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmImportAptitudeResult.SuspendLayout()
        Me.gbVacancyFilterCriteria.SuspendLayout()
        CType(Me.objbtnSearchResult, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.dgAptitudeResult)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbVacancyFilterCriteria)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(802, 552)
        Me.pnlMain.TabIndex = 0
        '
        'dgAptitudeResult
        '
        Me.dgAptitudeResult.AllowUserToAddRows = False
        Me.dgAptitudeResult.AllowUserToDeleteRows = False
        Me.dgAptitudeResult.AllowUserToResizeRows = False
        Me.dgAptitudeResult.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgAptitudeResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgAptitudeResult.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgAptitudeResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgAptitudeResult.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhImage, Me.dgcolhFirstName, Me.dgcolhSurname, Me.dgcolhEmail, Me.dgcolhFinalGrade, Me.dgcolhErrorMsg})
        Me.dgAptitudeResult.Location = New System.Drawing.Point(3, 70)
        Me.dgAptitudeResult.Name = "dgAptitudeResult"
        Me.dgAptitudeResult.ReadOnly = True
        Me.dgAptitudeResult.RowHeadersVisible = False
        Me.dgAptitudeResult.RowHeadersWidth = 5
        Me.dgAptitudeResult.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgAptitudeResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgAptitudeResult.Size = New System.Drawing.Size(789, 421)
        Me.dgAptitudeResult.TabIndex = 289
        '
        'dgcolhImage
        '
        Me.dgcolhImage.Frozen = True
        Me.dgcolhImage.HeaderText = ""
        Me.dgcolhImage.Name = "dgcolhImage"
        Me.dgcolhImage.ReadOnly = True
        Me.dgcolhImage.Width = 30
        '
        'dgcolhFirstName
        '
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhFirstName.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgcolhFirstName.HeaderText = "First Name"
        Me.dgcolhFirstName.Name = "dgcolhFirstName"
        Me.dgcolhFirstName.ReadOnly = True
        Me.dgcolhFirstName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhFirstName.Width = 125
        '
        'dgcolhSurname
        '
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhSurname.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgcolhSurname.HeaderText = "Surname"
        Me.dgcolhSurname.Name = "dgcolhSurname"
        Me.dgcolhSurname.ReadOnly = True
        Me.dgcolhSurname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSurname.Width = 125
        '
        'dgcolhEmail
        '
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhEmail.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmail.Width = 200
        '
        'dgcolhFinalGrade
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.dgcolhFinalGrade.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgcolhFinalGrade.HeaderText = "Final Grade"
        Me.dgcolhFinalGrade.Name = "dgcolhFinalGrade"
        Me.dgcolhFinalGrade.ReadOnly = True
        Me.dgcolhFinalGrade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhFinalGrade.Width = 150
        '
        'dgcolhErrorMsg
        '
        Me.dgcolhErrorMsg.HeaderText = "Error"
        Me.dgcolhErrorMsg.Name = "dgcolhErrorMsg"
        Me.dgcolhErrorMsg.ReadOnly = True
        Me.dgcolhErrorMsg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhErrorMsg.Width = 175
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objbtnImport)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 497)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(802, 55)
        Me.objFooter.TabIndex = 15
        '
        'objbtnImport
        '
        Me.objbtnImport.BorderColor = System.Drawing.Color.Black
        Me.objbtnImport.ContextMenuStrip = Me.cmImportAptitudeResult
        Me.objbtnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnImport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnImport.Location = New System.Drawing.Point(579, 13)
        Me.objbtnImport.Name = "objbtnImport"
        Me.objbtnImport.ShowDefaultBorderColor = True
        Me.objbtnImport.Size = New System.Drawing.Size(110, 30)
        Me.objbtnImport.SplitButtonMenu = Me.cmImportAptitudeResult
        Me.objbtnImport.TabIndex = 5
        Me.objbtnImport.Text = "Import"
        '
        'cmImportAptitudeResult
        '
        Me.cmImportAptitudeResult.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnImportData, Me.btnShowSuccessful, Me.btnShowError, Me.btnExportError})
        Me.cmImportAptitudeResult.Name = "cmImportAccrueLeave"
        Me.cmImportAptitudeResult.Size = New System.Drawing.Size(162, 114)
        '
        'btnImportData
        '
        Me.btnImportData.Name = "btnImportData"
        Me.btnImportData.Size = New System.Drawing.Size(161, 22)
        Me.btnImportData.Text = "&Import Result"
        '
        'btnShowSuccessful
        '
        Me.btnShowSuccessful.Name = "btnShowSuccessful"
        Me.btnShowSuccessful.Size = New System.Drawing.Size(161, 22)
        Me.btnShowSuccessful.Text = "Show Successful"
        '
        'btnShowError
        '
        Me.btnShowError.Name = "btnShowError"
        Me.btnShowError.Size = New System.Drawing.Size(161, 22)
        Me.btnShowError.Text = "Show Error"
        '
        'btnExportError
        '
        Me.btnExportError.Name = "btnExportError"
        Me.btnExportError.Size = New System.Drawing.Size(161, 22)
        Me.btnExportError.Text = "Export Error"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(695, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbVacancyFilterCriteria
        '
        Me.gbVacancyFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbVacancyFilterCriteria.Checked = False
        Me.gbVacancyFilterCriteria.CollapseAllExceptThis = False
        Me.gbVacancyFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbVacancyFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbVacancyFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbVacancyFilterCriteria.CollapseOnLoad = False
        Me.gbVacancyFilterCriteria.Controls.Add(Me.objbtnSearchResult)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.LblVacancy)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.LblVacanyType)
        Me.gbVacancyFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbVacancyFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbVacancyFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbVacancyFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbVacancyFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbVacancyFilterCriteria.HeaderHeight = 25
        Me.gbVacancyFilterCriteria.HeaderMessage = ""
        Me.gbVacancyFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbVacancyFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbVacancyFilterCriteria.HeightOnCollapse = 0
        Me.gbVacancyFilterCriteria.LeftTextSpace = 0
        Me.gbVacancyFilterCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbVacancyFilterCriteria.Name = "gbVacancyFilterCriteria"
        Me.gbVacancyFilterCriteria.OpenHeight = 91
        Me.gbVacancyFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbVacancyFilterCriteria.ShowBorder = True
        Me.gbVacancyFilterCriteria.ShowCheckBox = False
        Me.gbVacancyFilterCriteria.ShowCollapseButton = False
        Me.gbVacancyFilterCriteria.ShowDefaultBorderColor = True
        Me.gbVacancyFilterCriteria.ShowDownButton = False
        Me.gbVacancyFilterCriteria.ShowHeader = True
        Me.gbVacancyFilterCriteria.Size = New System.Drawing.Size(790, 61)
        Me.gbVacancyFilterCriteria.TabIndex = 13
        Me.gbVacancyFilterCriteria.Temp = 0
        Me.gbVacancyFilterCriteria.Text = "Filter Criteria"
        Me.gbVacancyFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchResult
        '
        Me.objbtnSearchResult.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearchResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearchResult.Image = CType(resources.GetObject("objbtnSearchResult.Image"), System.Drawing.Image)
        Me.objbtnSearchResult.Location = New System.Drawing.Point(738, 0)
        Me.objbtnSearchResult.Name = "objbtnSearchResult"
        Me.objbtnSearchResult.ResultMessage = ""
        Me.objbtnSearchResult.SearchMessage = ""
        Me.objbtnSearchResult.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearchResult.TabIndex = 244
        Me.objbtnSearchResult.TabStop = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(763, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 242
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(763, 33)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 86
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 600
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(344, 33)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(413, 21)
        Me.cboVacancy.TabIndex = 237
        '
        'LblVacancy
        '
        Me.LblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVacancy.Location = New System.Drawing.Point(273, 35)
        Me.LblVacancy.Name = "LblVacancy"
        Me.LblVacancy.Size = New System.Drawing.Size(67, 17)
        Me.LblVacancy.TabIndex = 236
        Me.LblVacancy.Text = "Vacancy"
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 400
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(101, 33)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(158, 21)
        Me.cboVacancyType.TabIndex = 87
        '
        'LblVacanyType
        '
        Me.LblVacanyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVacanyType.Location = New System.Drawing.Point(8, 35)
        Me.LblVacanyType.Name = "LblVacanyType"
        Me.LblVacanyType.Size = New System.Drawing.Size(89, 17)
        Me.LblVacanyType.TabIndex = 85
        Me.LblVacanyType.Text = "Vacancy Type"
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Applicantunkid"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn2.HeaderText = "App. Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridViewTextBoxColumn3.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridViewTextBoxColumn4.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.Format = "N2"
        DataGridViewCellStyle18.NullValue = Nothing
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle18
        Me.DataGridViewTextBoxColumn5.HeaderText = "Vacancy"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Subject"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 220
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Send Date Time"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Body"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 275
        '
        'frmDownloadAptitudeResult
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(802, 552)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDownloadAptitudeResult"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Download Aptitude Result"
        Me.pnlMain.ResumeLayout(False)
        CType(Me.dgAptitudeResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmImportAptitudeResult.ResumeLayout(False)
        Me.gbVacancyFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnSearchResult, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents gbVacancyFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents LblVacancy As System.Windows.Forms.Label
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents LblVacanyType As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgAptitudeResult As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnImport As eZee.Common.eZeeSplitButton
    Friend WithEvents cmImportAptitudeResult As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnImportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objbtnSearchResult As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dgcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhFirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSurname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFinalGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhErrorMsg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnShowSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExportError As System.Windows.Forms.ToolStripMenuItem
End Class
