﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVacancy_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVacancy_AddEdit))
        Me.pnlVacancy = New System.Windows.Forms.Panel
        Me.tabcVacancy = New System.Windows.Forms.TabControl
        Me.tabpJobInfo = New System.Windows.Forms.TabPage
        Me.gbJob = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlJob = New System.Windows.Forms.Panel
        Me.chkIsBothInternalExternal = New System.Windows.Forms.CheckBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.objbtnAddVacancy = New eZee.Common.eZeeGradientButton
        Me.tabcRemarks = New System.Windows.Forms.TabControl
        Me.tabpOtherInfo = New System.Windows.Forms.TabPage
        Me.cboLevel = New System.Windows.Forms.ComboBox
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.objbtnSeachGrade = New eZee.Common.eZeeGradientButton
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.objbtnSeachGradeGroup = New eZee.Common.eZeeGradientButton
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.cboPayType = New System.Windows.Forms.ComboBox
        Me.lblPayType = New System.Windows.Forms.Label
        Me.objbtnAddPayType = New eZee.Common.eZeeGradientButton
        Me.lblShiftType = New System.Windows.Forms.Label
        Me.nudPosition = New System.Windows.Forms.NumericUpDown
        Me.objbtnAddShiftType = New eZee.Common.eZeeGradientButton
        Me.cboShiftType = New System.Windows.Forms.ComboBox
        Me.txtPayRangeFrom = New eZee.TextBox.NumericTextBox
        Me.lblPayRangeFrom = New System.Windows.Forms.Label
        Me.txtPayRangeTo = New eZee.TextBox.NumericTextBox
        Me.lblPayRangeTo = New System.Windows.Forms.Label
        Me.lblNoofPosition = New System.Windows.Forms.Label
        Me.tabpDuties = New System.Windows.Forms.TabPage
        Me.txtResponsibilitiesDuties = New eZee.TextBox.AlphanumericTextBox
        Me.tabpExperience = New System.Windows.Forms.TabPage
        Me.txtOtherWorkingExperience = New eZee.TextBox.AlphanumericTextBox
        Me.lblOtherWorkingExperience = New System.Windows.Forms.Label
        Me.tlsExperience = New System.Windows.Forms.ToolStrip
        Me.objtlbbtnExperienceBold = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnExperienceItalic = New System.Windows.Forms.ToolStripButton
        Me.txtWorkingExp = New eZee.TextBox.NumericTextBox
        Me.lblMonths = New System.Windows.Forms.Label
        Me.lblWorkExp = New System.Windows.Forms.Label
        Me.tabpRemark = New System.Windows.Forms.TabPage
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.objbtnSearchJobs = New eZee.Common.eZeeGradientButton
        Me.chkExportToWeb = New System.Windows.Forms.CheckBox
        Me.chkIsExternalVacancy = New System.Windows.Forms.CheckBox
        Me.objbtnAddJobDept = New eZee.Common.eZeeGradientButton
        Me.objbtnAddEmployment = New eZee.Common.eZeeGradientButton
        Me.objbtnAddJob = New eZee.Common.eZeeGradientButton
        Me.lblVacancyTitle = New System.Windows.Forms.Label
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.lblEmploymentType = New System.Windows.Forms.Label
        Me.cboEmploymentType = New System.Windows.Forms.ComboBox
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.dtInterviewClosingDate = New System.Windows.Forms.DateTimePicker
        Me.lblInterviewCloseDate = New System.Windows.Forms.Label
        Me.dtInterviewStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblInterviewStartDate = New System.Windows.Forms.Label
        Me.dtClosingDate = New System.Windows.Forms.DateTimePicker
        Me.lblClosingDate = New System.Windows.Forms.Label
        Me.dtOpeningDate = New System.Windows.Forms.DateTimePicker
        Me.lblJobOpenDate = New System.Windows.Forms.Label
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.cboStation = New System.Windows.Forms.ComboBox
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.lblUnits = New System.Windows.Forms.Label
        Me.cboJobDepartment = New System.Windows.Forms.ComboBox
        Me.lblJobDept = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.tabpInterview = New System.Windows.Forms.TabPage
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lvInterviewer = New System.Windows.Forms.ListView
        Me.colhLevel = New System.Windows.Forms.ColumnHeader
        Me.colhEmpName = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewType = New System.Windows.Forms.ColumnHeader
        Me.objcolhInterviewerunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhIntGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhInterviewtypeunkid = New System.Windows.Forms.ColumnHeader
        Me.gbInterviewer = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblLevelInfo = New System.Windows.Forms.Label
        Me.nudLevel = New System.Windows.Forms.NumericUpDown
        Me.lblLevel = New System.Windows.Forms.Label
        Me.EZeeLine3 = New eZee.Common.eZeeLine
        Me.objbtnAddInterviewType = New eZee.Common.eZeeGradientButton
        Me.lblInterViewType = New System.Windows.Forms.Label
        Me.cboInterviewType = New System.Windows.Forms.ComboBox
        Me.pnlOthers = New System.Windows.Forms.Panel
        Me.lblTrainerContactNo = New System.Windows.Forms.Label
        Me.txtOtherContactNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblOtherDepartment = New System.Windows.Forms.Label
        Me.txtOtherCompany = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtOtherInterviewerName = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.objColon3 = New System.Windows.Forms.Label
        Me.lblEmployeeDepartment = New System.Windows.Forms.Label
        Me.radOthers = New System.Windows.Forms.RadioButton
        Me.objColon2 = New System.Windows.Forms.Label
        Me.objColon1 = New System.Windows.Forms.Label
        Me.lblEmpContact = New System.Windows.Forms.Label
        Me.lblEmpCompany = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblEmployeeContactNo = New System.Windows.Forms.Label
        Me.lblEmployeeCompany = New System.Windows.Forms.Label
        Me.tabpAdvertise = New System.Windows.Forms.TabPage
        Me.gbAdvertisementAnalysis = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAdvertiser = New eZee.Common.eZeeGradientButton
        Me.objbtnAdvertiseCategory = New eZee.Common.eZeeGradientButton
        Me.btnAdDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtNotes = New eZee.TextBox.AlphanumericTextBox
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblNotes = New System.Windows.Forms.Label
        Me.lvAdvertisement = New System.Windows.Forms.ListView
        Me.colhAdvertiserName = New System.Windows.Forms.ColumnHeader
        Me.colhCosting = New System.Windows.Forms.ColumnHeader
        Me.colhBudget = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.colhNotes = New System.Windows.Forms.ColumnHeader
        Me.objcolhAdvertiseunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhCategoryunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhADGUID = New System.Windows.Forms.ColumnHeader
        Me.cboAdvertiser = New System.Windows.Forms.ComboBox
        Me.lblAgency = New System.Windows.Forms.Label
        Me.txtBudget = New eZee.TextBox.NumericTextBox
        Me.lblBudget = New System.Windows.Forms.Label
        Me.txtCosting = New eZee.TextBox.NumericTextBox
        Me.lblCosting = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.cboAdvertiseCategory = New System.Windows.Forms.ComboBox
        Me.lblAdvertiser = New System.Windows.Forms.Label
        Me.tabpOtherParameters = New System.Windows.Forms.TabPage
        Me.gbParametersInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tabcOtherInfo = New System.Windows.Forms.TabControl
        Me.tabpOtherJobQualification = New System.Windows.Forms.TabPage
        Me.txtOtherJobQualificationDesc = New eZee.TextBox.AlphanumericTextBox
        Me.tabpOtherJobSkills = New System.Windows.Forms.TabPage
        Me.txtOtherJobSkillsDesc = New eZee.TextBox.AlphanumericTextBox
        Me.tabpOtherJobLanguage = New System.Windows.Forms.TabPage
        Me.txtOtherJobLanguageDesc = New eZee.TextBox.AlphanumericTextBox
        Me.gbLanguage = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddLanguage = New eZee.Common.eZeeGradientButton
        Me.objtlsLanguage = New System.Windows.Forms.ToolStrip
        Me.objtlbbtnBoldLanguage = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnItalicLanguage = New System.Windows.Forms.ToolStripButton
        Me.pnlJobLanguage = New System.Windows.Forms.Panel
        Me.lvJobLanguage = New System.Windows.Forms.ListView
        Me.objColhJobLanguage = New System.Windows.Forms.ColumnHeader
        Me.gbJobQualification = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddQualification = New eZee.Common.eZeeGradientButton
        Me.objtlsQuali = New System.Windows.Forms.ToolStrip
        Me.objtlbbtnQualiBold = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnQualiItalic = New System.Windows.Forms.ToolStripButton
        Me.pnlJobQualification = New System.Windows.Forms.Panel
        Me.lvJobQualification = New System.Windows.Forms.ListView
        Me.objColhJobQualification = New System.Windows.Forms.ColumnHeader
        Me.gbSkill = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddSkills = New eZee.Common.eZeeGradientButton
        Me.objtlsSkill = New System.Windows.Forms.ToolStrip
        Me.objtlbbtnSkillBold = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnSkillItalic = New System.Windows.Forms.ToolStripButton
        Me.pnlJobSkill = New System.Windows.Forms.Panel
        Me.lvJobSkill = New System.Windows.Forms.ListView
        Me.objColhJobSkills = New System.Windows.Forms.ColumnHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblRemarkItalic = New System.Windows.Forms.Label
        Me.objlblRemarkBold = New System.Windows.Forms.Label
        Me.lblRemarkItalic = New System.Windows.Forms.Label
        Me.lblRemarkBold = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlVacancy.SuspendLayout()
        Me.tabcVacancy.SuspendLayout()
        Me.tabpJobInfo.SuspendLayout()
        Me.gbJob.SuspendLayout()
        Me.pnlJob.SuspendLayout()
        Me.tabcRemarks.SuspendLayout()
        Me.tabpOtherInfo.SuspendLayout()
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpDuties.SuspendLayout()
        Me.tabpExperience.SuspendLayout()
        Me.tlsExperience.SuspendLayout()
        Me.tabpRemark.SuspendLayout()
        Me.tabpInterview.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.gbInterviewer.SuspendLayout()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOthers.SuspendLayout()
        Me.tabpAdvertise.SuspendLayout()
        Me.gbAdvertisementAnalysis.SuspendLayout()
        Me.tabpOtherParameters.SuspendLayout()
        Me.gbParametersInfo.SuspendLayout()
        Me.tabcOtherInfo.SuspendLayout()
        Me.tabpOtherJobQualification.SuspendLayout()
        Me.tabpOtherJobSkills.SuspendLayout()
        Me.tabpOtherJobLanguage.SuspendLayout()
        Me.gbLanguage.SuspendLayout()
        Me.objtlsLanguage.SuspendLayout()
        Me.pnlJobLanguage.SuspendLayout()
        Me.gbJobQualification.SuspendLayout()
        Me.objtlsQuali.SuspendLayout()
        Me.pnlJobQualification.SuspendLayout()
        Me.gbSkill.SuspendLayout()
        Me.objtlsSkill.SuspendLayout()
        Me.pnlJobSkill.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlVacancy
        '
        Me.pnlVacancy.Controls.Add(Me.tabcVacancy)
        Me.pnlVacancy.Controls.Add(Me.EZeeFooter1)
        Me.pnlVacancy.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlVacancy.Location = New System.Drawing.Point(0, 0)
        Me.pnlVacancy.Name = "pnlVacancy"
        Me.pnlVacancy.Size = New System.Drawing.Size(910, 524)
        Me.pnlVacancy.TabIndex = 0
        '
        'tabcVacancy
        '
        Me.tabcVacancy.Controls.Add(Me.tabpJobInfo)
        Me.tabcVacancy.Controls.Add(Me.tabpInterview)
        Me.tabcVacancy.Controls.Add(Me.tabpAdvertise)
        Me.tabcVacancy.Controls.Add(Me.tabpOtherParameters)
        Me.tabcVacancy.Location = New System.Drawing.Point(2, 3)
        Me.tabcVacancy.Name = "tabcVacancy"
        Me.tabcVacancy.SelectedIndex = 0
        Me.tabcVacancy.Size = New System.Drawing.Size(905, 463)
        Me.tabcVacancy.TabIndex = 0
        '
        'tabpJobInfo
        '
        Me.tabpJobInfo.Controls.Add(Me.gbJob)
        Me.tabpJobInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpJobInfo.Name = "tabpJobInfo"
        Me.tabpJobInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpJobInfo.Size = New System.Drawing.Size(897, 437)
        Me.tabpJobInfo.TabIndex = 0
        Me.tabpJobInfo.Text = "Vacancy Info"
        Me.tabpJobInfo.UseVisualStyleBackColor = True
        '
        'gbJob
        '
        Me.gbJob.BorderColor = System.Drawing.Color.Black
        Me.gbJob.Checked = False
        Me.gbJob.CollapseAllExceptThis = False
        Me.gbJob.CollapsedHoverImage = Nothing
        Me.gbJob.CollapsedNormalImage = Nothing
        Me.gbJob.CollapsedPressedImage = Nothing
        Me.gbJob.CollapseOnLoad = False
        Me.gbJob.Controls.Add(Me.pnlJob)
        Me.gbJob.ExpandedHoverImage = Nothing
        Me.gbJob.ExpandedNormalImage = Nothing
        Me.gbJob.ExpandedPressedImage = Nothing
        Me.gbJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJob.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJob.HeaderHeight = 25
        Me.gbJob.HeaderMessage = ""
        Me.gbJob.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJob.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJob.HeightOnCollapse = 0
        Me.gbJob.LeftTextSpace = 0
        Me.gbJob.Location = New System.Drawing.Point(3, 3)
        Me.gbJob.Name = "gbJob"
        Me.gbJob.OpenHeight = 308
        Me.gbJob.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJob.ShowBorder = True
        Me.gbJob.ShowCheckBox = False
        Me.gbJob.ShowCollapseButton = False
        Me.gbJob.ShowDefaultBorderColor = True
        Me.gbJob.ShowDownButton = False
        Me.gbJob.ShowHeader = True
        Me.gbJob.Size = New System.Drawing.Size(888, 430)
        Me.gbJob.TabIndex = 0
        Me.gbJob.Temp = 0
        Me.gbJob.Text = "Vacancy"
        Me.gbJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlJob
        '
        Me.pnlJob.AutoScroll = True
        Me.pnlJob.Controls.Add(Me.chkIsBothInternalExternal)
        Me.pnlJob.Controls.Add(Me.lblCostCenter)
        Me.pnlJob.Controls.Add(Me.cboCostCenter)
        Me.pnlJob.Controls.Add(Me.lblClass)
        Me.pnlJob.Controls.Add(Me.cboClass)
        Me.pnlJob.Controls.Add(Me.lblClassGroup)
        Me.pnlJob.Controls.Add(Me.cboClassGroup)
        Me.pnlJob.Controls.Add(Me.cboTeams)
        Me.pnlJob.Controls.Add(Me.lblTeam)
        Me.pnlJob.Controls.Add(Me.cboUnitGroup)
        Me.pnlJob.Controls.Add(Me.lblUnitGroup)
        Me.pnlJob.Controls.Add(Me.cboSectionGroup)
        Me.pnlJob.Controls.Add(Me.lblSectionGroup)
        Me.pnlJob.Controls.Add(Me.objbtnAddVacancy)
        Me.pnlJob.Controls.Add(Me.tabcRemarks)
        Me.pnlJob.Controls.Add(Me.objbtnSearchVacancy)
        Me.pnlJob.Controls.Add(Me.cboVacancy)
        Me.pnlJob.Controls.Add(Me.objbtnSearchJobs)
        Me.pnlJob.Controls.Add(Me.chkExportToWeb)
        Me.pnlJob.Controls.Add(Me.chkIsExternalVacancy)
        Me.pnlJob.Controls.Add(Me.objbtnAddJobDept)
        Me.pnlJob.Controls.Add(Me.objbtnAddEmployment)
        Me.pnlJob.Controls.Add(Me.objbtnAddJob)
        Me.pnlJob.Controls.Add(Me.lblVacancyTitle)
        Me.pnlJob.Controls.Add(Me.EZeeLine1)
        Me.pnlJob.Controls.Add(Me.objelLine1)
        Me.pnlJob.Controls.Add(Me.lblEmploymentType)
        Me.pnlJob.Controls.Add(Me.cboEmploymentType)
        Me.pnlJob.Controls.Add(Me.cboJobGroup)
        Me.pnlJob.Controls.Add(Me.lblJobGroup)
        Me.pnlJob.Controls.Add(Me.lblSection)
        Me.pnlJob.Controls.Add(Me.cboSections)
        Me.pnlJob.Controls.Add(Me.dtInterviewClosingDate)
        Me.pnlJob.Controls.Add(Me.lblInterviewCloseDate)
        Me.pnlJob.Controls.Add(Me.dtInterviewStartDate)
        Me.pnlJob.Controls.Add(Me.lblInterviewStartDate)
        Me.pnlJob.Controls.Add(Me.dtClosingDate)
        Me.pnlJob.Controls.Add(Me.lblClosingDate)
        Me.pnlJob.Controls.Add(Me.dtOpeningDate)
        Me.pnlJob.Controls.Add(Me.lblJobOpenDate)
        Me.pnlJob.Controls.Add(Me.lblBranch)
        Me.pnlJob.Controls.Add(Me.cboDepartmentGrp)
        Me.pnlJob.Controls.Add(Me.cboStation)
        Me.pnlJob.Controls.Add(Me.lblDepartmentGroup)
        Me.pnlJob.Controls.Add(Me.cboUnits)
        Me.pnlJob.Controls.Add(Me.lblUnits)
        Me.pnlJob.Controls.Add(Me.cboJobDepartment)
        Me.pnlJob.Controls.Add(Me.lblJobDept)
        Me.pnlJob.Controls.Add(Me.cboJob)
        Me.pnlJob.Controls.Add(Me.lblJob)
        Me.pnlJob.Location = New System.Drawing.Point(3, 26)
        Me.pnlJob.Name = "pnlJob"
        Me.pnlJob.Size = New System.Drawing.Size(882, 401)
        Me.pnlJob.TabIndex = 50
        '
        'chkIsBothInternalExternal
        '
        Me.chkIsBothInternalExternal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsBothInternalExternal.Location = New System.Drawing.Point(278, 5)
        Me.chkIsBothInternalExternal.Name = "chkIsBothInternalExternal"
        Me.chkIsBothInternalExternal.Size = New System.Drawing.Size(211, 16)
        Me.chkIsBothInternalExternal.TabIndex = 448
        Me.chkIsBothInternalExternal.Text = "Both Internal and External Vacancy"
        Me.chkIsBothInternalExternal.UseVisualStyleBackColor = True
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(563, 136)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(97, 16)
        Me.lblCostCenter.TabIndex = 447
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.DropDownWidth = 270
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(666, 136)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(121, 21)
        Me.cboCostCenter.TabIndex = 446
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(297, 163)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(97, 16)
        Me.lblClass.TabIndex = 445
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.DropDownWidth = 270
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(399, 163)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(121, 21)
        Me.cboClass.TabIndex = 444
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(297, 136)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(97, 16)
        Me.lblClassGroup.TabIndex = 443
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.DropDownWidth = 270
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(399, 136)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboClassGroup.TabIndex = 442
        '
        'cboTeams
        '
        Me.cboTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeams.DropDownWidth = 270
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(399, 110)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(121, 21)
        Me.cboTeams.TabIndex = 440
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(297, 110)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(97, 16)
        Me.lblTeam.TabIndex = 441
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.DropDownWidth = 270
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(399, 56)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboUnitGroup.TabIndex = 438
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(297, 56)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(97, 16)
        Me.lblUnitGroup.TabIndex = 439
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.DropDownWidth = 270
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(124, 136)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboSectionGroup.TabIndex = 436
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(11, 136)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(104, 16)
        Me.lblSectionGroup.TabIndex = 437
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddVacancy
        '
        Me.objbtnAddVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddVacancy.BorderSelected = False
        Me.objbtnAddVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddVacancy.Location = New System.Drawing.Point(533, 27)
        Me.objbtnAddVacancy.Name = "objbtnAddVacancy"
        Me.objbtnAddVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddVacancy.TabIndex = 433
        '
        'tabcRemarks
        '
        Me.tabcRemarks.Controls.Add(Me.tabpOtherInfo)
        Me.tabcRemarks.Controls.Add(Me.tabpDuties)
        Me.tabcRemarks.Controls.Add(Me.tabpExperience)
        Me.tabcRemarks.Controls.Add(Me.tabpRemark)
        Me.tabcRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcRemarks.Location = New System.Drawing.Point(3, 260)
        Me.tabcRemarks.Multiline = True
        Me.tabcRemarks.Name = "tabcRemarks"
        Me.tabcRemarks.SelectedIndex = 0
        Me.tabcRemarks.Size = New System.Drawing.Size(859, 140)
        Me.tabcRemarks.TabIndex = 432
        '
        'tabpOtherInfo
        '
        Me.tabpOtherInfo.Controls.Add(Me.cboLevel)
        Me.tabpOtherInfo.Controls.Add(Me.lblGradeLevel)
        Me.tabpOtherInfo.Controls.Add(Me.objbtnSeachGrade)
        Me.tabpOtherInfo.Controls.Add(Me.lblGradeGroup)
        Me.tabpOtherInfo.Controls.Add(Me.cboGrade)
        Me.tabpOtherInfo.Controls.Add(Me.lblGrade)
        Me.tabpOtherInfo.Controls.Add(Me.objbtnSeachGradeGroup)
        Me.tabpOtherInfo.Controls.Add(Me.cboGradeGroup)
        Me.tabpOtherInfo.Controls.Add(Me.cboPayType)
        Me.tabpOtherInfo.Controls.Add(Me.lblPayType)
        Me.tabpOtherInfo.Controls.Add(Me.objbtnAddPayType)
        Me.tabpOtherInfo.Controls.Add(Me.lblShiftType)
        Me.tabpOtherInfo.Controls.Add(Me.nudPosition)
        Me.tabpOtherInfo.Controls.Add(Me.objbtnAddShiftType)
        Me.tabpOtherInfo.Controls.Add(Me.cboShiftType)
        Me.tabpOtherInfo.Controls.Add(Me.txtPayRangeFrom)
        Me.tabpOtherInfo.Controls.Add(Me.lblPayRangeFrom)
        Me.tabpOtherInfo.Controls.Add(Me.txtPayRangeTo)
        Me.tabpOtherInfo.Controls.Add(Me.lblPayRangeTo)
        Me.tabpOtherInfo.Controls.Add(Me.lblNoofPosition)
        Me.tabpOtherInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherInfo.Name = "tabpOtherInfo"
        Me.tabpOtherInfo.Size = New System.Drawing.Size(851, 114)
        Me.tabpOtherInfo.TabIndex = 2
        Me.tabpOtherInfo.Text = "Other Information"
        Me.tabpOtherInfo.UseVisualStyleBackColor = True
        '
        'cboLevel
        '
        Me.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLevel.DropDownWidth = 200
        Me.cboLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLevel.FormattingEnabled = True
        Me.cboLevel.Location = New System.Drawing.Point(662, 15)
        Me.cboLevel.Name = "cboLevel"
        Me.cboLevel.Size = New System.Drawing.Size(121, 21)
        Me.cboLevel.TabIndex = 439
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(557, 15)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(102, 17)
        Me.lblGradeLevel.TabIndex = 440
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSeachGrade
        '
        Me.objbtnSeachGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSeachGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSeachGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSeachGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSeachGrade.BorderSelected = False
        Me.objbtnSeachGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSeachGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSeachGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSeachGrade.Location = New System.Drawing.Point(518, 16)
        Me.objbtnSeachGrade.Name = "objbtnSeachGrade"
        Me.objbtnSeachGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSeachGrade.TabIndex = 438
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(4, 15)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(107, 16)
        Me.lblGradeGroup.TabIndex = 435
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.DropDownWidth = 200
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(391, 16)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(121, 21)
        Me.cboGrade.TabIndex = 434
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(290, 16)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(97, 17)
        Me.lblGrade.TabIndex = 436
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSeachGradeGroup
        '
        Me.objbtnSeachGradeGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSeachGradeGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSeachGradeGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSeachGradeGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSeachGradeGroup.BorderSelected = False
        Me.objbtnSeachGradeGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSeachGradeGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSeachGradeGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSeachGradeGroup.Location = New System.Drawing.Point(244, 15)
        Me.objbtnSeachGradeGroup.Name = "objbtnSeachGradeGroup"
        Me.objbtnSeachGradeGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSeachGradeGroup.TabIndex = 437
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.DropDownWidth = 200
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(117, 15)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboGradeGroup.TabIndex = 433
        '
        'cboPayType
        '
        Me.cboPayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayType.DropDownWidth = 200
        Me.cboPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.Location = New System.Drawing.Point(117, 43)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(121, 21)
        Me.cboPayType.TabIndex = 13
        '
        'lblPayType
        '
        Me.lblPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayType.Location = New System.Drawing.Point(4, 43)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.Size = New System.Drawing.Size(107, 16)
        Me.lblPayType.TabIndex = 113
        Me.lblPayType.Text = "Pay Type"
        Me.lblPayType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddPayType
        '
        Me.objbtnAddPayType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddPayType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddPayType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddPayType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddPayType.BorderSelected = False
        Me.objbtnAddPayType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddPayType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddPayType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddPayType.Location = New System.Drawing.Point(244, 43)
        Me.objbtnAddPayType.Name = "objbtnAddPayType"
        Me.objbtnAddPayType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddPayType.TabIndex = 131
        '
        'lblShiftType
        '
        Me.lblShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftType.Location = New System.Drawing.Point(290, 44)
        Me.lblShiftType.Name = "lblShiftType"
        Me.lblShiftType.Size = New System.Drawing.Size(97, 17)
        Me.lblShiftType.TabIndex = 121
        Me.lblShiftType.Text = "Shift Type"
        Me.lblShiftType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudPosition
        '
        Me.nudPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPosition.Location = New System.Drawing.Point(662, 43)
        Me.nudPosition.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nudPosition.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudPosition.Name = "nudPosition"
        Me.nudPosition.Size = New System.Drawing.Size(121, 21)
        Me.nudPosition.TabIndex = 17
        Me.nudPosition.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'objbtnAddShiftType
        '
        Me.objbtnAddShiftType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddShiftType.BorderSelected = False
        Me.objbtnAddShiftType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddShiftType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddShiftType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddShiftType.Location = New System.Drawing.Point(518, 44)
        Me.objbtnAddShiftType.Name = "objbtnAddShiftType"
        Me.objbtnAddShiftType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddShiftType.TabIndex = 130
        '
        'cboShiftType
        '
        Me.cboShiftType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftType.DropDownWidth = 200
        Me.cboShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShiftType.FormattingEnabled = True
        Me.cboShiftType.Location = New System.Drawing.Point(391, 44)
        Me.cboShiftType.Name = "cboShiftType"
        Me.cboShiftType.Size = New System.Drawing.Size(121, 21)
        Me.cboShiftType.TabIndex = 14
        '
        'txtPayRangeFrom
        '
        Me.txtPayRangeFrom.AllowNegative = True
        Me.txtPayRangeFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPayRangeFrom.DigitsInGroup = 0
        Me.txtPayRangeFrom.Flags = 0
        Me.txtPayRangeFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayRangeFrom.Location = New System.Drawing.Point(117, 70)
        Me.txtPayRangeFrom.MaxDecimalPlaces = 6
        Me.txtPayRangeFrom.MaxWholeDigits = 21
        Me.txtPayRangeFrom.Name = "txtPayRangeFrom"
        Me.txtPayRangeFrom.Prefix = ""
        Me.txtPayRangeFrom.RangeMax = 1.7976931348623157E+308
        Me.txtPayRangeFrom.RangeMin = -1.7976931348623157E+308
        Me.txtPayRangeFrom.Size = New System.Drawing.Size(121, 21)
        Me.txtPayRangeFrom.TabIndex = 15
        Me.txtPayRangeFrom.Text = "0"
        Me.txtPayRangeFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPayRangeFrom
        '
        Me.lblPayRangeFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayRangeFrom.Location = New System.Drawing.Point(4, 70)
        Me.lblPayRangeFrom.Name = "lblPayRangeFrom"
        Me.lblPayRangeFrom.Size = New System.Drawing.Size(107, 16)
        Me.lblPayRangeFrom.TabIndex = 117
        Me.lblPayRangeFrom.Text = "Pay Range"
        Me.lblPayRangeFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPayRangeTo
        '
        Me.txtPayRangeTo.AllowNegative = True
        Me.txtPayRangeTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPayRangeTo.DigitsInGroup = 0
        Me.txtPayRangeTo.Flags = 0
        Me.txtPayRangeTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayRangeTo.Location = New System.Drawing.Point(390, 71)
        Me.txtPayRangeTo.MaxDecimalPlaces = 6
        Me.txtPayRangeTo.MaxWholeDigits = 21
        Me.txtPayRangeTo.Name = "txtPayRangeTo"
        Me.txtPayRangeTo.Prefix = ""
        Me.txtPayRangeTo.RangeMax = 1.7976931348623157E+308
        Me.txtPayRangeTo.RangeMin = -1.7976931348623157E+308
        Me.txtPayRangeTo.Size = New System.Drawing.Size(121, 21)
        Me.txtPayRangeTo.TabIndex = 16
        Me.txtPayRangeTo.Text = "0"
        Me.txtPayRangeTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPayRangeTo
        '
        Me.lblPayRangeTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayRangeTo.Location = New System.Drawing.Point(290, 71)
        Me.lblPayRangeTo.Name = "lblPayRangeTo"
        Me.lblPayRangeTo.Size = New System.Drawing.Size(97, 17)
        Me.lblPayRangeTo.TabIndex = 119
        Me.lblPayRangeTo.Text = "To"
        Me.lblPayRangeTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNoofPosition
        '
        Me.lblNoofPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoofPosition.Location = New System.Drawing.Point(557, 43)
        Me.lblNoofPosition.Name = "lblNoofPosition"
        Me.lblNoofPosition.Size = New System.Drawing.Size(102, 16)
        Me.lblNoofPosition.TabIndex = 67
        Me.lblNoofPosition.Text = "No. of Position"
        Me.lblNoofPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpDuties
        '
        Me.tabpDuties.Controls.Add(Me.txtResponsibilitiesDuties)
        Me.tabpDuties.Location = New System.Drawing.Point(4, 22)
        Me.tabpDuties.Name = "tabpDuties"
        Me.tabpDuties.Size = New System.Drawing.Size(851, 114)
        Me.tabpDuties.TabIndex = 0
        Me.tabpDuties.Text = "Resposibilities/Duties"
        Me.tabpDuties.UseVisualStyleBackColor = True
        '
        'txtResponsibilitiesDuties
        '
        Me.txtResponsibilitiesDuties.BackColor = System.Drawing.SystemColors.Window
        Me.txtResponsibilitiesDuties.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtResponsibilitiesDuties.Flags = 0
        Me.txtResponsibilitiesDuties.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResponsibilitiesDuties.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtResponsibilitiesDuties.Location = New System.Drawing.Point(0, 0)
        Me.txtResponsibilitiesDuties.Multiline = True
        Me.txtResponsibilitiesDuties.Name = "txtResponsibilitiesDuties"
        Me.txtResponsibilitiesDuties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtResponsibilitiesDuties.Size = New System.Drawing.Size(851, 114)
        Me.txtResponsibilitiesDuties.TabIndex = 89
        '
        'tabpExperience
        '
        Me.tabpExperience.Controls.Add(Me.txtOtherWorkingExperience)
        Me.tabpExperience.Controls.Add(Me.lblOtherWorkingExperience)
        Me.tabpExperience.Controls.Add(Me.tlsExperience)
        Me.tabpExperience.Controls.Add(Me.txtWorkingExp)
        Me.tabpExperience.Controls.Add(Me.lblMonths)
        Me.tabpExperience.Controls.Add(Me.lblWorkExp)
        Me.tabpExperience.Location = New System.Drawing.Point(4, 22)
        Me.tabpExperience.Name = "tabpExperience"
        Me.tabpExperience.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpExperience.Size = New System.Drawing.Size(851, 114)
        Me.tabpExperience.TabIndex = 3
        Me.tabpExperience.Text = "Experience"
        Me.tabpExperience.UseVisualStyleBackColor = True
        '
        'txtOtherWorkingExperience
        '
        Me.txtOtherWorkingExperience.BackColor = System.Drawing.SystemColors.Window
        Me.txtOtherWorkingExperience.Flags = 0
        Me.txtOtherWorkingExperience.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherWorkingExperience.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherWorkingExperience.Location = New System.Drawing.Point(114, 34)
        Me.txtOtherWorkingExperience.Multiline = True
        Me.txtOtherWorkingExperience.Name = "txtOtherWorkingExperience"
        Me.txtOtherWorkingExperience.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherWorkingExperience.Size = New System.Drawing.Size(730, 55)
        Me.txtOtherWorkingExperience.TabIndex = 448
        '
        'lblOtherWorkingExperience
        '
        Me.lblOtherWorkingExperience.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherWorkingExperience.Location = New System.Drawing.Point(9, 34)
        Me.lblOtherWorkingExperience.Name = "lblOtherWorkingExperience"
        Me.lblOtherWorkingExperience.Size = New System.Drawing.Size(102, 34)
        Me.lblOtherWorkingExperience.TabIndex = 447
        Me.lblOtherWorkingExperience.Text = "Other Working Experience"
        Me.lblOtherWorkingExperience.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tlsExperience
        '
        Me.tlsExperience.BackColor = System.Drawing.Color.Transparent
        Me.tlsExperience.Dock = System.Windows.Forms.DockStyle.None
        Me.tlsExperience.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsExperience.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtlbbtnExperienceBold, Me.objtlbbtnExperienceItalic})
        Me.tlsExperience.Location = New System.Drawing.Point(194, 4)
        Me.tlsExperience.Name = "tlsExperience"
        Me.tlsExperience.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsExperience.Size = New System.Drawing.Size(49, 25)
        Me.tlsExperience.TabIndex = 445
        '
        'objtlbbtnExperienceBold
        '
        Me.objtlbbtnExperienceBold.CheckOnClick = True
        Me.objtlbbtnExperienceBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnExperienceBold.Image = CType(resources.GetObject("objtlbbtnExperienceBold.Image"), System.Drawing.Image)
        Me.objtlbbtnExperienceBold.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnExperienceBold.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnExperienceBold.Name = "objtlbbtnExperienceBold"
        Me.objtlbbtnExperienceBold.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnExperienceBold.ToolTipText = "Set Experience Bold on Recruitment Portal"
        '
        'objtlbbtnExperienceItalic
        '
        Me.objtlbbtnExperienceItalic.CheckOnClick = True
        Me.objtlbbtnExperienceItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnExperienceItalic.Image = CType(resources.GetObject("objtlbbtnExperienceItalic.Image"), System.Drawing.Image)
        Me.objtlbbtnExperienceItalic.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnExperienceItalic.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnExperienceItalic.Name = "objtlbbtnExperienceItalic"
        Me.objtlbbtnExperienceItalic.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnExperienceItalic.ToolTipText = "Set Experience Italic on Recruitment Portal"
        '
        'txtWorkingExp
        '
        Me.txtWorkingExp.AllowNegative = False
        Me.txtWorkingExp.BackColor = System.Drawing.SystemColors.Window
        Me.txtWorkingExp.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWorkingExp.DigitsInGroup = 0
        Me.txtWorkingExp.Flags = 65536
        Me.txtWorkingExp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkingExp.Location = New System.Drawing.Point(114, 5)
        Me.txtWorkingExp.MaxDecimalPlaces = 0
        Me.txtWorkingExp.MaxWholeDigits = 3
        Me.txtWorkingExp.Name = "txtWorkingExp"
        Me.txtWorkingExp.Prefix = ""
        Me.txtWorkingExp.RangeMax = 1.7976931348623157E+308
        Me.txtWorkingExp.RangeMin = -1.7976931348623157E+308
        Me.txtWorkingExp.Size = New System.Drawing.Size(33, 21)
        Me.txtWorkingExp.TabIndex = 442
        Me.txtWorkingExp.Text = "0"
        '
        'lblMonths
        '
        Me.lblMonths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonths.Location = New System.Drawing.Point(148, 9)
        Me.lblMonths.Name = "lblMonths"
        Me.lblMonths.Size = New System.Drawing.Size(45, 13)
        Me.lblMonths.TabIndex = 444
        Me.lblMonths.Text = "Months"
        '
        'lblWorkExp
        '
        Me.lblWorkExp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkExp.Location = New System.Drawing.Point(9, 5)
        Me.lblWorkExp.Name = "lblWorkExp"
        Me.lblWorkExp.Size = New System.Drawing.Size(102, 17)
        Me.lblWorkExp.TabIndex = 443
        Me.lblWorkExp.Text = "Working Experience"
        Me.lblWorkExp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpRemark
        '
        Me.tabpRemark.Controls.Add(Me.txtRemark)
        Me.tabpRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpRemark.Name = "tabpRemark"
        Me.tabpRemark.Size = New System.Drawing.Size(851, 114)
        Me.tabpRemark.TabIndex = 1
        Me.tabpRemark.Text = "Remark"
        Me.tabpRemark.UseVisualStyleBackColor = True
        '
        'txtRemark
        '
        Me.txtRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(851, 114)
        Me.txtRemark.TabIndex = 90
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(557, 27)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 431
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(124, 27)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(396, 21)
        Me.cboVacancy.TabIndex = 430
        '
        'objbtnSearchJobs
        '
        Me.objbtnSearchJobs.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobs.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobs.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobs.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobs.BorderSelected = False
        Me.objbtnSearchJobs.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobs.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobs.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobs.Location = New System.Drawing.Point(793, 83)
        Me.objbtnSearchJobs.Name = "objbtnSearchJobs"
        Me.objbtnSearchJobs.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobs.TabIndex = 429
        '
        'chkExportToWeb
        '
        Me.chkExportToWeb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExportToWeb.Location = New System.Drawing.Point(492, 5)
        Me.chkExportToWeb.Name = "chkExportToWeb"
        Me.chkExportToWeb.Size = New System.Drawing.Size(118, 16)
        Me.chkExportToWeb.TabIndex = 134
        Me.chkExportToWeb.Text = "Export To Web"
        Me.chkExportToWeb.UseVisualStyleBackColor = True
        '
        'chkIsExternalVacancy
        '
        Me.chkIsExternalVacancy.Checked = True
        Me.chkIsExternalVacancy.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIsExternalVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsExternalVacancy.Location = New System.Drawing.Point(124, 5)
        Me.chkIsExternalVacancy.Name = "chkIsExternalVacancy"
        Me.chkIsExternalVacancy.Size = New System.Drawing.Size(148, 16)
        Me.chkIsExternalVacancy.TabIndex = 134
        Me.chkIsExternalVacancy.Text = "Is External Vacancy"
        Me.chkIsExternalVacancy.UseVisualStyleBackColor = True
        '
        'objbtnAddJobDept
        '
        Me.objbtnAddJobDept.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddJobDept.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddJobDept.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddJobDept.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddJobDept.BorderSelected = False
        Me.objbtnAddJobDept.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddJobDept.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddJobDept.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddJobDept.Location = New System.Drawing.Point(251, 110)
        Me.objbtnAddJobDept.Name = "objbtnAddJobDept"
        Me.objbtnAddJobDept.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddJobDept.TabIndex = 132
        '
        'objbtnAddEmployment
        '
        Me.objbtnAddEmployment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddEmployment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddEmployment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddEmployment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddEmployment.BorderSelected = False
        Me.objbtnAddEmployment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddEmployment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddEmployment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddEmployment.Location = New System.Drawing.Point(793, 110)
        Me.objbtnAddEmployment.Name = "objbtnAddEmployment"
        Me.objbtnAddEmployment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddEmployment.TabIndex = 129
        '
        'objbtnAddJob
        '
        Me.objbtnAddJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddJob.BorderSelected = False
        Me.objbtnAddJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddJob.Location = New System.Drawing.Point(793, 56)
        Me.objbtnAddJob.Name = "objbtnAddJob"
        Me.objbtnAddJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddJob.TabIndex = 128
        Me.objbtnAddJob.Visible = False
        '
        'lblVacancyTitle
        '
        Me.lblVacancyTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyTitle.Location = New System.Drawing.Point(11, 30)
        Me.lblVacancyTitle.Name = "lblVacancyTitle"
        Me.lblVacancyTitle.Size = New System.Drawing.Size(107, 16)
        Me.lblVacancyTitle.TabIndex = 126
        Me.lblVacancyTitle.Text = "Vacancy Title"
        Me.lblVacancyTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(11, 254)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(840, 2)
        Me.EZeeLine1.TabIndex = 124
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 192)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(840, 2)
        Me.objelLine1.TabIndex = 123
        '
        'lblEmploymentType
        '
        Me.lblEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmploymentType.Location = New System.Drawing.Point(563, 110)
        Me.lblEmploymentType.Name = "lblEmploymentType"
        Me.lblEmploymentType.Size = New System.Drawing.Size(97, 16)
        Me.lblEmploymentType.TabIndex = 101
        Me.lblEmploymentType.Text = "Employment Type"
        Me.lblEmploymentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmploymentType
        '
        Me.cboEmploymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmploymentType.DropDownWidth = 270
        Me.cboEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmploymentType.FormattingEnabled = True
        Me.cboEmploymentType.Location = New System.Drawing.Point(666, 110)
        Me.cboEmploymentType.Name = "cboEmploymentType"
        Me.cboEmploymentType.Size = New System.Drawing.Size(121, 21)
        Me.cboEmploymentType.TabIndex = 8
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.DropDownWidth = 270
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(666, 56)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboJobGroup.TabIndex = 6
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(563, 56)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(97, 16)
        Me.lblJobGroup.TabIndex = 95
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(11, 163)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(107, 16)
        Me.lblSection.TabIndex = 93
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSections
        '
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.DropDownWidth = 270
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(124, 163)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(121, 21)
        Me.cboSections.TabIndex = 4
        '
        'dtInterviewClosingDate
        '
        Me.dtInterviewClosingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtInterviewClosingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtInterviewClosingDate.Location = New System.Drawing.Point(399, 228)
        Me.dtInterviewClosingDate.Name = "dtInterviewClosingDate"
        Me.dtInterviewClosingDate.Size = New System.Drawing.Size(121, 21)
        Me.dtInterviewClosingDate.TabIndex = 12
        '
        'lblInterviewCloseDate
        '
        Me.lblInterviewCloseDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewCloseDate.Location = New System.Drawing.Point(296, 230)
        Me.lblInterviewCloseDate.Name = "lblInterviewCloseDate"
        Me.lblInterviewCloseDate.Size = New System.Drawing.Size(97, 17)
        Me.lblInterviewCloseDate.TabIndex = 111
        Me.lblInterviewCloseDate.Text = "Interview Closing Date"
        '
        'dtInterviewStartDate
        '
        Me.dtInterviewStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtInterviewStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtInterviewStartDate.Location = New System.Drawing.Point(124, 226)
        Me.dtInterviewStartDate.Name = "dtInterviewStartDate"
        Me.dtInterviewStartDate.Size = New System.Drawing.Size(121, 21)
        Me.dtInterviewStartDate.TabIndex = 11
        '
        'lblInterviewStartDate
        '
        Me.lblInterviewStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewStartDate.Location = New System.Drawing.Point(11, 228)
        Me.lblInterviewStartDate.Name = "lblInterviewStartDate"
        Me.lblInterviewStartDate.Size = New System.Drawing.Size(107, 17)
        Me.lblInterviewStartDate.TabIndex = 109
        Me.lblInterviewStartDate.Text = "Interview Start Date"
        '
        'dtClosingDate
        '
        Me.dtClosingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtClosingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtClosingDate.Location = New System.Drawing.Point(399, 201)
        Me.dtClosingDate.Name = "dtClosingDate"
        Me.dtClosingDate.Size = New System.Drawing.Size(121, 21)
        Me.dtClosingDate.TabIndex = 10
        '
        'lblClosingDate
        '
        Me.lblClosingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClosingDate.Location = New System.Drawing.Point(296, 203)
        Me.lblClosingDate.Name = "lblClosingDate"
        Me.lblClosingDate.Size = New System.Drawing.Size(97, 17)
        Me.lblClosingDate.TabIndex = 107
        Me.lblClosingDate.Text = "Closing Date"
        '
        'dtOpeningDate
        '
        Me.dtOpeningDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtOpeningDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtOpeningDate.Location = New System.Drawing.Point(124, 199)
        Me.dtOpeningDate.Name = "dtOpeningDate"
        Me.dtOpeningDate.Size = New System.Drawing.Size(121, 21)
        Me.dtOpeningDate.TabIndex = 9
        '
        'lblJobOpenDate
        '
        Me.lblJobOpenDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobOpenDate.Location = New System.Drawing.Point(11, 201)
        Me.lblJobOpenDate.Name = "lblJobOpenDate"
        Me.lblJobOpenDate.Size = New System.Drawing.Size(107, 17)
        Me.lblJobOpenDate.TabIndex = 105
        Me.lblJobOpenDate.Text = "Opening Date"
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(11, 56)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(107, 16)
        Me.lblBranch.TabIndex = 91
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGrp.DropDownWidth = 270
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(124, 83)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(121, 21)
        Me.cboDepartmentGrp.TabIndex = 2
        '
        'cboStation
        '
        Me.cboStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStation.DropDownWidth = 270
        Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStation.FormattingEnabled = True
        Me.cboStation.Location = New System.Drawing.Point(124, 56)
        Me.cboStation.Name = "cboStation"
        Me.cboStation.Size = New System.Drawing.Size(121, 21)
        Me.cboStation.TabIndex = 1
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(11, 83)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(107, 16)
        Me.lblDepartmentGroup.TabIndex = 92
        Me.lblDepartmentGroup.Text = "Department Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnits
        '
        Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnits.DropDownWidth = 270
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.Location = New System.Drawing.Point(399, 83)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(121, 21)
        Me.cboUnits.TabIndex = 5
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(297, 83)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(97, 16)
        Me.lblUnits.TabIndex = 94
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJobDepartment
        '
        Me.cboJobDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobDepartment.DropDownWidth = 270
        Me.cboJobDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobDepartment.FormattingEnabled = True
        Me.cboJobDepartment.Location = New System.Drawing.Point(124, 110)
        Me.cboJobDepartment.Name = "cboJobDepartment"
        Me.cboJobDepartment.Size = New System.Drawing.Size(121, 21)
        Me.cboJobDepartment.TabIndex = 3
        '
        'lblJobDept
        '
        Me.lblJobDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobDept.Location = New System.Drawing.Point(11, 110)
        Me.lblJobDept.Name = "lblJobDept"
        Me.lblJobDept.Size = New System.Drawing.Size(107, 16)
        Me.lblJobDept.TabIndex = 53
        Me.lblJobDept.Text = "Job Department"
        Me.lblJobDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 270
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(666, 83)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(121, 21)
        Me.cboJob.TabIndex = 7
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(563, 83)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(97, 16)
        Me.lblJob.TabIndex = 51
        Me.lblJob.Text = "Job "
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpInterview
        '
        Me.tabpInterview.Controls.Add(Me.btnAdd)
        Me.tabpInterview.Controls.Add(Me.btnEdit)
        Me.tabpInterview.Controls.Add(Me.btnDelete)
        Me.tabpInterview.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.tabpInterview.Controls.Add(Me.gbInterviewer)
        Me.tabpInterview.Location = New System.Drawing.Point(4, 22)
        Me.tabpInterview.Name = "tabpInterview"
        Me.tabpInterview.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpInterview.Size = New System.Drawing.Size(897, 437)
        Me.tabpInterview.TabIndex = 1
        Me.tabpInterview.Text = "Interview"
        Me.tabpInterview.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(307, 401)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(410, 401)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(900, 384)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lvInterviewer)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(304, 3)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 308
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(590, 392)
        Me.EZeeCollapsibleContainer1.TabIndex = 0
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Interviewers Detail"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvInterviewer
        '
        Me.lvInterviewer.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLevel, Me.colhEmpName, Me.colhCompany, Me.colhDepartment, Me.colhContactNo, Me.colhInterviewType, Me.objcolhInterviewerunkid, Me.objcolhIntGUID, Me.objcolhInterviewtypeunkid})
        Me.lvInterviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvInterviewer.FullRowSelect = True
        Me.lvInterviewer.GridLines = True
        Me.lvInterviewer.Location = New System.Drawing.Point(2, 26)
        Me.lvInterviewer.Name = "lvInterviewer"
        Me.lvInterviewer.Size = New System.Drawing.Size(585, 363)
        Me.lvInterviewer.TabIndex = 0
        Me.lvInterviewer.UseCompatibleStateImageBehavior = False
        Me.lvInterviewer.View = System.Windows.Forms.View.Details
        '
        'colhLevel
        '
        Me.colhLevel.Text = "Level"
        Me.colhLevel.Width = 45
        '
        'colhEmpName
        '
        Me.colhEmpName.Text = "Emp. Name"
        Me.colhEmpName.Width = 200
        '
        'colhCompany
        '
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 120
        '
        'colhDepartment
        '
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 110
        '
        'colhContactNo
        '
        Me.colhContactNo.Text = "Contact No."
        Me.colhContactNo.Width = 90
        '
        'colhInterviewType
        '
        Me.colhInterviewType.Text = "Interview Type"
        Me.colhInterviewType.Width = 110
        '
        'objcolhInterviewerunkid
        '
        Me.objcolhInterviewerunkid.Text = "Interviewerunkid"
        Me.objcolhInterviewerunkid.Width = 0
        '
        'objcolhIntGUID
        '
        Me.objcolhIntGUID.Text = "objcolhIntGUID"
        Me.objcolhIntGUID.Width = 0
        '
        'objcolhInterviewtypeunkid
        '
        Me.objcolhInterviewtypeunkid.Text = "objcolhInterviewtypeunkid"
        Me.objcolhInterviewtypeunkid.Width = 0
        '
        'gbInterviewer
        '
        Me.gbInterviewer.BorderColor = System.Drawing.Color.Black
        Me.gbInterviewer.Checked = False
        Me.gbInterviewer.CollapseAllExceptThis = False
        Me.gbInterviewer.CollapsedHoverImage = Nothing
        Me.gbInterviewer.CollapsedNormalImage = Nothing
        Me.gbInterviewer.CollapsedPressedImage = Nothing
        Me.gbInterviewer.CollapseOnLoad = False
        Me.gbInterviewer.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbInterviewer.Controls.Add(Me.lblLevelInfo)
        Me.gbInterviewer.Controls.Add(Me.nudLevel)
        Me.gbInterviewer.Controls.Add(Me.lblLevel)
        Me.gbInterviewer.Controls.Add(Me.EZeeLine3)
        Me.gbInterviewer.Controls.Add(Me.objbtnAddInterviewType)
        Me.gbInterviewer.Controls.Add(Me.lblInterViewType)
        Me.gbInterviewer.Controls.Add(Me.cboInterviewType)
        Me.gbInterviewer.Controls.Add(Me.pnlOthers)
        Me.gbInterviewer.Controls.Add(Me.cboEmployee)
        Me.gbInterviewer.Controls.Add(Me.radEmployee)
        Me.gbInterviewer.Controls.Add(Me.objColon3)
        Me.gbInterviewer.Controls.Add(Me.lblEmployeeDepartment)
        Me.gbInterviewer.Controls.Add(Me.radOthers)
        Me.gbInterviewer.Controls.Add(Me.objColon2)
        Me.gbInterviewer.Controls.Add(Me.objColon1)
        Me.gbInterviewer.Controls.Add(Me.lblEmpContact)
        Me.gbInterviewer.Controls.Add(Me.lblEmpCompany)
        Me.gbInterviewer.Controls.Add(Me.lblDepartment)
        Me.gbInterviewer.Controls.Add(Me.lblEmployeeContactNo)
        Me.gbInterviewer.Controls.Add(Me.lblEmployeeCompany)
        Me.gbInterviewer.ExpandedHoverImage = Nothing
        Me.gbInterviewer.ExpandedNormalImage = Nothing
        Me.gbInterviewer.ExpandedPressedImage = Nothing
        Me.gbInterviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInterviewer.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInterviewer.HeaderHeight = 25
        Me.gbInterviewer.HeaderMessage = ""
        Me.gbInterviewer.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInterviewer.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInterviewer.HeightOnCollapse = 0
        Me.gbInterviewer.LeftTextSpace = 0
        Me.gbInterviewer.Location = New System.Drawing.Point(3, 3)
        Me.gbInterviewer.Name = "gbInterviewer"
        Me.gbInterviewer.OpenHeight = 308
        Me.gbInterviewer.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInterviewer.ShowBorder = True
        Me.gbInterviewer.ShowCheckBox = False
        Me.gbInterviewer.ShowCollapseButton = False
        Me.gbInterviewer.ShowDefaultBorderColor = True
        Me.gbInterviewer.ShowDownButton = False
        Me.gbInterviewer.ShowHeader = True
        Me.gbInterviewer.Size = New System.Drawing.Size(295, 430)
        Me.gbInterviewer.TabIndex = 0
        Me.gbInterviewer.Temp = 0
        Me.gbInterviewer.Text = "Interview Info"
        Me.gbInterviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(269, 103)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 432
        '
        'lblLevelInfo
        '
        Me.lblLevelInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelInfo.Location = New System.Drawing.Point(162, 65)
        Me.lblLevelInfo.Name = "lblLevelInfo"
        Me.lblLevelInfo.Size = New System.Drawing.Size(120, 13)
        Me.lblLevelInfo.TabIndex = 214
        '
        'nudLevel
        '
        Me.nudLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLevel.Location = New System.Drawing.Point(115, 61)
        Me.nudLevel.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nudLevel.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudLevel.Name = "nudLevel"
        Me.nudLevel.Size = New System.Drawing.Size(40, 21)
        Me.nudLevel.TabIndex = 1
        Me.nudLevel.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(11, 65)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(92, 13)
        Me.lblLevel.TabIndex = 212
        Me.lblLevel.Text = "Level"
        '
        'EZeeLine3
        '
        Me.EZeeLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine3.Location = New System.Drawing.Point(8, 92)
        Me.EZeeLine3.Name = "EZeeLine3"
        Me.EZeeLine3.Size = New System.Drawing.Size(274, 6)
        Me.EZeeLine3.TabIndex = 211
        '
        'objbtnAddInterviewType
        '
        Me.objbtnAddInterviewType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddInterviewType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddInterviewType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddInterviewType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddInterviewType.BorderSelected = False
        Me.objbtnAddInterviewType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddInterviewType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddInterviewType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddInterviewType.Location = New System.Drawing.Point(269, 33)
        Me.objbtnAddInterviewType.Name = "objbtnAddInterviewType"
        Me.objbtnAddInterviewType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddInterviewType.TabIndex = 208
        '
        'lblInterViewType
        '
        Me.lblInterViewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterViewType.Location = New System.Drawing.Point(8, 36)
        Me.lblInterViewType.Name = "lblInterViewType"
        Me.lblInterViewType.Size = New System.Drawing.Size(95, 15)
        Me.lblInterViewType.TabIndex = 206
        Me.lblInterViewType.Text = "Interview Type"
        Me.lblInterViewType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInterviewType
        '
        Me.cboInterviewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewType.DropDownWidth = 200
        Me.cboInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewType.FormattingEnabled = True
        Me.cboInterviewType.Location = New System.Drawing.Point(115, 33)
        Me.cboInterviewType.Name = "cboInterviewType"
        Me.cboInterviewType.Size = New System.Drawing.Size(148, 21)
        Me.cboInterviewType.TabIndex = 0
        '
        'pnlOthers
        '
        Me.pnlOthers.Controls.Add(Me.lblTrainerContactNo)
        Me.pnlOthers.Controls.Add(Me.txtOtherContactNo)
        Me.pnlOthers.Controls.Add(Me.lblCompany)
        Me.pnlOthers.Controls.Add(Me.lblOtherDepartment)
        Me.pnlOthers.Controls.Add(Me.txtOtherCompany)
        Me.pnlOthers.Controls.Add(Me.txtOtherDepartment)
        Me.pnlOthers.Controls.Add(Me.lblName)
        Me.pnlOthers.Controls.Add(Me.txtOtherInterviewerName)
        Me.pnlOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOthers.Location = New System.Drawing.Point(11, 222)
        Me.pnlOthers.Name = "pnlOthers"
        Me.pnlOthers.Size = New System.Drawing.Size(277, 109)
        Me.pnlOthers.TabIndex = 199
        '
        'lblTrainerContactNo
        '
        Me.lblTrainerContactNo.Location = New System.Drawing.Point(15, 90)
        Me.lblTrainerContactNo.Name = "lblTrainerContactNo"
        Me.lblTrainerContactNo.Size = New System.Drawing.Size(85, 15)
        Me.lblTrainerContactNo.TabIndex = 160
        Me.lblTrainerContactNo.Text = "Contact No"
        Me.lblTrainerContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherContactNo
        '
        Me.txtOtherContactNo.Flags = 0
        Me.txtOtherContactNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherContactNo.Location = New System.Drawing.Point(105, 84)
        Me.txtOtherContactNo.Name = "txtOtherContactNo"
        Me.txtOtherContactNo.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherContactNo.TabIndex = 3
        '
        'lblCompany
        '
        Me.lblCompany.Location = New System.Drawing.Point(15, 63)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(85, 15)
        Me.lblCompany.TabIndex = 158
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherDepartment
        '
        Me.lblOtherDepartment.Location = New System.Drawing.Point(15, 36)
        Me.lblOtherDepartment.Name = "lblOtherDepartment"
        Me.lblOtherDepartment.Size = New System.Drawing.Size(85, 15)
        Me.lblOtherDepartment.TabIndex = 157
        Me.lblOtherDepartment.Text = "Department"
        Me.lblOtherDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherCompany
        '
        Me.txtOtherCompany.Flags = 0
        Me.txtOtherCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherCompany.Location = New System.Drawing.Point(105, 57)
        Me.txtOtherCompany.Name = "txtOtherCompany"
        Me.txtOtherCompany.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherCompany.TabIndex = 2
        '
        'txtOtherDepartment
        '
        Me.txtOtherDepartment.Flags = 0
        Me.txtOtherDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherDepartment.Location = New System.Drawing.Point(105, 30)
        Me.txtOtherDepartment.Name = "txtOtherDepartment"
        Me.txtOtherDepartment.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherDepartment.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(15, 6)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(85, 15)
        Me.lblName.TabIndex = 154
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherInterviewerName
        '
        Me.txtOtherInterviewerName.Flags = 0
        Me.txtOtherInterviewerName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherInterviewerName.Location = New System.Drawing.Point(105, 3)
        Me.txtOtherInterviewerName.Name = "txtOtherInterviewerName"
        Me.txtOtherInterviewerName.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherInterviewerName.TabIndex = 0
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 270
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(115, 103)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(148, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(11, 105)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(95, 17)
        Me.radEmployee.TabIndex = 181
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'objColon3
        '
        Me.objColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon3.Location = New System.Drawing.Point(107, 174)
        Me.objColon3.Name = "objColon3"
        Me.objColon3.Size = New System.Drawing.Size(8, 15)
        Me.objColon3.TabIndex = 192
        Me.objColon3.Text = ":"
        Me.objColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeDepartment
        '
        Me.lblEmployeeDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeDepartment.Location = New System.Drawing.Point(121, 132)
        Me.lblEmployeeDepartment.Name = "lblEmployeeDepartment"
        Me.lblEmployeeDepartment.Size = New System.Drawing.Size(136, 15)
        Me.lblEmployeeDepartment.TabIndex = 184
        Me.lblEmployeeDepartment.Text = "#DepartmentName"
        Me.lblEmployeeDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radOthers
        '
        Me.radOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOthers.Location = New System.Drawing.Point(11, 199)
        Me.radOthers.Name = "radOthers"
        Me.radOthers.Size = New System.Drawing.Size(76, 17)
        Me.radOthers.TabIndex = 198
        Me.radOthers.TabStop = True
        Me.radOthers.Text = "Other"
        Me.radOthers.UseVisualStyleBackColor = True
        '
        'objColon2
        '
        Me.objColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon2.Location = New System.Drawing.Point(107, 152)
        Me.objColon2.Name = "objColon2"
        Me.objColon2.Size = New System.Drawing.Size(8, 15)
        Me.objColon2.TabIndex = 191
        Me.objColon2.Text = ":"
        Me.objColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon1
        '
        Me.objColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon1.Location = New System.Drawing.Point(107, 132)
        Me.objColon1.Name = "objColon1"
        Me.objColon1.Size = New System.Drawing.Size(8, 15)
        Me.objColon1.TabIndex = 190
        Me.objColon1.Text = ":"
        Me.objColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpContact
        '
        Me.lblEmpContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContact.Location = New System.Drawing.Point(34, 174)
        Me.lblEmpContact.Name = "lblEmpContact"
        Me.lblEmpContact.Size = New System.Drawing.Size(72, 15)
        Me.lblEmpContact.TabIndex = 189
        Me.lblEmpContact.Text = "Contact No"
        Me.lblEmpContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpCompany
        '
        Me.lblEmpCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCompany.Location = New System.Drawing.Point(34, 152)
        Me.lblEmpCompany.Name = "lblEmpCompany"
        Me.lblEmpCompany.Size = New System.Drawing.Size(72, 15)
        Me.lblEmpCompany.TabIndex = 188
        Me.lblEmpCompany.Text = "Company"
        Me.lblEmpCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(34, 132)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(72, 15)
        Me.lblDepartment.TabIndex = 187
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeContactNo
        '
        Me.lblEmployeeContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeContactNo.Location = New System.Drawing.Point(121, 174)
        Me.lblEmployeeContactNo.Name = "lblEmployeeContactNo"
        Me.lblEmployeeContactNo.Size = New System.Drawing.Size(136, 15)
        Me.lblEmployeeContactNo.TabIndex = 186
        Me.lblEmployeeContactNo.Text = "#Contact No"
        Me.lblEmployeeContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeCompany
        '
        Me.lblEmployeeCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCompany.Location = New System.Drawing.Point(121, 152)
        Me.lblEmployeeCompany.Name = "lblEmployeeCompany"
        Me.lblEmployeeCompany.Size = New System.Drawing.Size(136, 15)
        Me.lblEmployeeCompany.TabIndex = 185
        Me.lblEmployeeCompany.Text = "#Company"
        Me.lblEmployeeCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpAdvertise
        '
        Me.tabpAdvertise.Controls.Add(Me.gbAdvertisementAnalysis)
        Me.tabpAdvertise.Location = New System.Drawing.Point(4, 22)
        Me.tabpAdvertise.Name = "tabpAdvertise"
        Me.tabpAdvertise.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpAdvertise.Size = New System.Drawing.Size(897, 437)
        Me.tabpAdvertise.TabIndex = 2
        Me.tabpAdvertise.Text = "Recruitment Cost "
        Me.tabpAdvertise.UseVisualStyleBackColor = True
        '
        'gbAdvertisementAnalysis
        '
        Me.gbAdvertisementAnalysis.BorderColor = System.Drawing.Color.Black
        Me.gbAdvertisementAnalysis.Checked = False
        Me.gbAdvertisementAnalysis.CollapseAllExceptThis = False
        Me.gbAdvertisementAnalysis.CollapsedHoverImage = Nothing
        Me.gbAdvertisementAnalysis.CollapsedNormalImage = Nothing
        Me.gbAdvertisementAnalysis.CollapsedPressedImage = Nothing
        Me.gbAdvertisementAnalysis.CollapseOnLoad = False
        Me.gbAdvertisementAnalysis.Controls.Add(Me.objbtnAdvertiser)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.objbtnAdvertiseCategory)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.btnAdDelete)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.btnAdEdit)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.btnAdAdd)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.txtNotes)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.txtDescription)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.lblNotes)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.lvAdvertisement)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.cboAdvertiser)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.lblAgency)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.txtBudget)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.lblBudget)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.txtCosting)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.lblCosting)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.lblDescription)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.cboAdvertiseCategory)
        Me.gbAdvertisementAnalysis.Controls.Add(Me.lblAdvertiser)
        Me.gbAdvertisementAnalysis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAdvertisementAnalysis.ExpandedHoverImage = Nothing
        Me.gbAdvertisementAnalysis.ExpandedNormalImage = Nothing
        Me.gbAdvertisementAnalysis.ExpandedPressedImage = Nothing
        Me.gbAdvertisementAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAdvertisementAnalysis.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAdvertisementAnalysis.HeaderHeight = 25
        Me.gbAdvertisementAnalysis.HeaderMessage = ""
        Me.gbAdvertisementAnalysis.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAdvertisementAnalysis.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAdvertisementAnalysis.HeightOnCollapse = 0
        Me.gbAdvertisementAnalysis.LeftTextSpace = 0
        Me.gbAdvertisementAnalysis.Location = New System.Drawing.Point(3, 3)
        Me.gbAdvertisementAnalysis.Name = "gbAdvertisementAnalysis"
        Me.gbAdvertisementAnalysis.OpenHeight = 308
        Me.gbAdvertisementAnalysis.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAdvertisementAnalysis.ShowBorder = True
        Me.gbAdvertisementAnalysis.ShowCheckBox = False
        Me.gbAdvertisementAnalysis.ShowCollapseButton = False
        Me.gbAdvertisementAnalysis.ShowDefaultBorderColor = True
        Me.gbAdvertisementAnalysis.ShowDownButton = False
        Me.gbAdvertisementAnalysis.ShowHeader = True
        Me.gbAdvertisementAnalysis.Size = New System.Drawing.Size(891, 431)
        Me.gbAdvertisementAnalysis.TabIndex = 0
        Me.gbAdvertisementAnalysis.Temp = 0
        Me.gbAdvertisementAnalysis.Text = "Cost Analysis"
        Me.gbAdvertisementAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAdvertiser
        '
        Me.objbtnAdvertiser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvertiser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAdvertiser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAdvertiser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAdvertiser.BorderSelected = False
        Me.objbtnAdvertiser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAdvertiser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAdvertiser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAdvertiser.Location = New System.Drawing.Point(244, 60)
        Me.objbtnAdvertiser.Name = "objbtnAdvertiser"
        Me.objbtnAdvertiser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAdvertiser.TabIndex = 210
        '
        'objbtnAdvertiseCategory
        '
        Me.objbtnAdvertiseCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvertiseCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAdvertiseCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAdvertiseCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAdvertiseCategory.BorderSelected = False
        Me.objbtnAdvertiseCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAdvertiseCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAdvertiseCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAdvertiseCategory.Location = New System.Drawing.Point(244, 34)
        Me.objbtnAdvertiseCategory.Name = "objbtnAdvertiseCategory"
        Me.objbtnAdvertiseCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAdvertiseCategory.TabIndex = 209
        '
        'btnAdDelete
        '
        Me.btnAdDelete.BackColor = System.Drawing.Color.White
        Me.btnAdDelete.BackgroundImage = CType(resources.GetObject("btnAdDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnAdDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnAdDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdDelete.FlatAppearance.BorderSize = 0
        Me.btnAdDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdDelete.ForeColor = System.Drawing.Color.Black
        Me.btnAdDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdDelete.Location = New System.Drawing.Point(900, 372)
        Me.btnAdDelete.Name = "btnAdDelete"
        Me.btnAdDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnAdDelete.TabIndex = 9
        Me.btnAdDelete.Text = "&Delete"
        Me.btnAdDelete.UseVisualStyleBackColor = True
        '
        'btnAdEdit
        '
        Me.btnAdEdit.BackColor = System.Drawing.Color.White
        Me.btnAdEdit.BackgroundImage = CType(resources.GetObject("btnAdEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnAdEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnAdEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdEdit.FlatAppearance.BorderSize = 0
        Me.btnAdEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdEdit.ForeColor = System.Drawing.Color.Black
        Me.btnAdEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdEdit.Location = New System.Drawing.Point(792, 393)
        Me.btnAdEdit.Name = "btnAdEdit"
        Me.btnAdEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnAdEdit.TabIndex = 8
        Me.btnAdEdit.Text = "&Edit"
        Me.btnAdEdit.UseVisualStyleBackColor = True
        '
        'btnAdAdd
        '
        Me.btnAdAdd.BackColor = System.Drawing.Color.White
        Me.btnAdAdd.BackgroundImage = CType(resources.GetObject("btnAdAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdAdd.FlatAppearance.BorderSize = 0
        Me.btnAdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdAdd.Location = New System.Drawing.Point(696, 393)
        Me.btnAdAdd.Name = "btnAdAdd"
        Me.btnAdAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnAdAdd.TabIndex = 7
        Me.btnAdAdd.Text = "&Add"
        Me.btnAdAdd.UseVisualStyleBackColor = True
        '
        'txtNotes
        '
        Me.txtNotes.Flags = 0
        Me.txtNotes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNotes.Location = New System.Drawing.Point(117, 88)
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.Size = New System.Drawing.Size(354, 22)
        Me.txtNotes.TabIndex = 4
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(480, 60)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(404, 50)
        Me.txtDescription.TabIndex = 5
        '
        'lblNotes
        '
        Me.lblNotes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotes.Location = New System.Drawing.Point(8, 91)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(107, 16)
        Me.lblNotes.TabIndex = 61
        Me.lblNotes.Text = "Notes"
        Me.lblNotes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvAdvertisement
        '
        Me.lvAdvertisement.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAdvertiserName, Me.colhCosting, Me.colhBudget, Me.colhDescription, Me.colhNotes, Me.objcolhAdvertiseunkid, Me.objcolhCategoryunkid, Me.objcolhADGUID})
        Me.lvAdvertisement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAdvertisement.FullRowSelect = True
        Me.lvAdvertisement.GridLines = True
        Me.lvAdvertisement.Location = New System.Drawing.Point(8, 116)
        Me.lvAdvertisement.Name = "lvAdvertisement"
        Me.lvAdvertisement.Size = New System.Drawing.Size(876, 271)
        Me.lvAdvertisement.TabIndex = 6
        Me.lvAdvertisement.UseCompatibleStateImageBehavior = False
        Me.lvAdvertisement.View = System.Windows.Forms.View.Details
        '
        'colhAdvertiserName
        '
        Me.colhAdvertiserName.Text = "Cost"
        Me.colhAdvertiserName.Width = 200
        '
        'colhCosting
        '
        Me.colhCosting.Text = "Costing"
        Me.colhCosting.Width = 110
        '
        'colhBudget
        '
        Me.colhBudget.Text = "Budget"
        Me.colhBudget.Width = 100
        '
        'colhDescription
        '
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 270
        '
        'colhNotes
        '
        Me.colhNotes.Text = "Notes"
        Me.colhNotes.Width = 270
        '
        'objcolhAdvertiseunkid
        '
        Me.objcolhAdvertiseunkid.Text = "objAdvertiseunkid"
        Me.objcolhAdvertiseunkid.Width = 0
        '
        'objcolhCategoryunkid
        '
        Me.objcolhCategoryunkid.Text = "objcolhCategoryunkid"
        Me.objcolhCategoryunkid.Width = 0
        '
        'objcolhADGUID
        '
        Me.objcolhADGUID.Text = "objcolhADGUID"
        Me.objcolhADGUID.Width = 0
        '
        'cboAdvertiser
        '
        Me.cboAdvertiser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdvertiser.DropDownWidth = 200
        Me.cboAdvertiser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAdvertiser.FormattingEnabled = True
        Me.cboAdvertiser.Location = New System.Drawing.Point(117, 60)
        Me.cboAdvertiser.Name = "cboAdvertiser"
        Me.cboAdvertiser.Size = New System.Drawing.Size(121, 21)
        Me.cboAdvertiser.TabIndex = 1
        '
        'lblAgency
        '
        Me.lblAgency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgency.Location = New System.Drawing.Point(8, 61)
        Me.lblAgency.Name = "lblAgency"
        Me.lblAgency.Size = New System.Drawing.Size(103, 16)
        Me.lblAgency.TabIndex = 52
        Me.lblAgency.Text = "Agency"
        Me.lblAgency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBudget
        '
        Me.txtBudget.AllowNegative = True
        Me.txtBudget.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBudget.DigitsInGroup = 0
        Me.txtBudget.Flags = 0
        Me.txtBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBudget.Location = New System.Drawing.Point(350, 61)
        Me.txtBudget.MaxDecimalPlaces = 6
        Me.txtBudget.MaxWholeDigits = 21
        Me.txtBudget.Name = "txtBudget"
        Me.txtBudget.Prefix = ""
        Me.txtBudget.RangeMax = 1.7976931348623157E+308
        Me.txtBudget.RangeMin = -1.7976931348623157E+308
        Me.txtBudget.Size = New System.Drawing.Size(121, 21)
        Me.txtBudget.TabIndex = 3
        Me.txtBudget.Text = "0"
        '
        'lblBudget
        '
        Me.lblBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudget.Location = New System.Drawing.Point(277, 63)
        Me.lblBudget.Name = "lblBudget"
        Me.lblBudget.Size = New System.Drawing.Size(68, 16)
        Me.lblBudget.TabIndex = 50
        Me.lblBudget.Text = "Budget"
        Me.lblBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCosting
        '
        Me.txtCosting.AllowNegative = True
        Me.txtCosting.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCosting.DigitsInGroup = 0
        Me.txtCosting.Flags = 0
        Me.txtCosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCosting.Location = New System.Drawing.Point(350, 31)
        Me.txtCosting.MaxDecimalPlaces = 6
        Me.txtCosting.MaxWholeDigits = 21
        Me.txtCosting.Name = "txtCosting"
        Me.txtCosting.Prefix = ""
        Me.txtCosting.RangeMax = 1.7976931348623157E+308
        Me.txtCosting.RangeMin = -1.7976931348623157E+308
        Me.txtCosting.Size = New System.Drawing.Size(121, 21)
        Me.txtCosting.TabIndex = 2
        Me.txtCosting.Text = "0"
        '
        'lblCosting
        '
        Me.lblCosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCosting.Location = New System.Drawing.Point(277, 34)
        Me.lblCosting.Name = "lblCosting"
        Me.lblCosting.Size = New System.Drawing.Size(68, 16)
        Me.lblCosting.TabIndex = 48
        Me.lblCosting.Text = "Costing"
        Me.lblCosting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(477, 41)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(107, 16)
        Me.lblDescription.TabIndex = 46
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAdvertiseCategory
        '
        Me.cboAdvertiseCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdvertiseCategory.DropDownWidth = 200
        Me.cboAdvertiseCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAdvertiseCategory.FormattingEnabled = True
        Me.cboAdvertiseCategory.Location = New System.Drawing.Point(117, 34)
        Me.cboAdvertiseCategory.Name = "cboAdvertiseCategory"
        Me.cboAdvertiseCategory.Size = New System.Drawing.Size(121, 21)
        Me.cboAdvertiseCategory.TabIndex = 0
        '
        'lblAdvertiser
        '
        Me.lblAdvertiser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvertiser.Location = New System.Drawing.Point(8, 36)
        Me.lblAdvertiser.Name = "lblAdvertiser"
        Me.lblAdvertiser.Size = New System.Drawing.Size(103, 16)
        Me.lblAdvertiser.TabIndex = 33
        Me.lblAdvertiser.Text = "Cost Category"
        Me.lblAdvertiser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpOtherParameters
        '
        Me.tabpOtherParameters.Controls.Add(Me.gbParametersInfo)
        Me.tabpOtherParameters.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherParameters.Name = "tabpOtherParameters"
        Me.tabpOtherParameters.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOtherParameters.Size = New System.Drawing.Size(897, 437)
        Me.tabpOtherParameters.TabIndex = 3
        Me.tabpOtherParameters.Text = "Other Parameters"
        Me.tabpOtherParameters.UseVisualStyleBackColor = True
        '
        'gbParametersInfo
        '
        Me.gbParametersInfo.BorderColor = System.Drawing.Color.Black
        Me.gbParametersInfo.Checked = False
        Me.gbParametersInfo.CollapseAllExceptThis = False
        Me.gbParametersInfo.CollapsedHoverImage = Nothing
        Me.gbParametersInfo.CollapsedNormalImage = Nothing
        Me.gbParametersInfo.CollapsedPressedImage = Nothing
        Me.gbParametersInfo.CollapseOnLoad = False
        Me.gbParametersInfo.Controls.Add(Me.tabcOtherInfo)
        Me.gbParametersInfo.Controls.Add(Me.gbLanguage)
        Me.gbParametersInfo.Controls.Add(Me.gbJobQualification)
        Me.gbParametersInfo.Controls.Add(Me.gbSkill)
        Me.gbParametersInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbParametersInfo.ExpandedHoverImage = Nothing
        Me.gbParametersInfo.ExpandedNormalImage = Nothing
        Me.gbParametersInfo.ExpandedPressedImage = Nothing
        Me.gbParametersInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbParametersInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbParametersInfo.HeaderHeight = 25
        Me.gbParametersInfo.HeaderMessage = ""
        Me.gbParametersInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbParametersInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbParametersInfo.HeightOnCollapse = 0
        Me.gbParametersInfo.LeftTextSpace = 0
        Me.gbParametersInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbParametersInfo.Name = "gbParametersInfo"
        Me.gbParametersInfo.OpenHeight = 308
        Me.gbParametersInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbParametersInfo.ShowBorder = True
        Me.gbParametersInfo.ShowCheckBox = False
        Me.gbParametersInfo.ShowCollapseButton = False
        Me.gbParametersInfo.ShowDefaultBorderColor = True
        Me.gbParametersInfo.ShowDownButton = False
        Me.gbParametersInfo.ShowHeader = True
        Me.gbParametersInfo.Size = New System.Drawing.Size(891, 431)
        Me.gbParametersInfo.TabIndex = 1
        Me.gbParametersInfo.Temp = 0
        Me.gbParametersInfo.Text = "Parameters Info"
        Me.gbParametersInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcOtherInfo
        '
        Me.tabcOtherInfo.Controls.Add(Me.tabpOtherJobQualification)
        Me.tabcOtherInfo.Controls.Add(Me.tabpOtherJobSkills)
        Me.tabcOtherInfo.Controls.Add(Me.tabpOtherJobLanguage)
        Me.tabcOtherInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcOtherInfo.Location = New System.Drawing.Point(312, 27)
        Me.tabcOtherInfo.Multiline = True
        Me.tabcOtherInfo.Name = "tabcOtherInfo"
        Me.tabcOtherInfo.SelectedIndex = 0
        Me.tabcOtherInfo.Size = New System.Drawing.Size(576, 401)
        Me.tabcOtherInfo.TabIndex = 433
        '
        'tabpOtherJobQualification
        '
        Me.tabpOtherJobQualification.Controls.Add(Me.txtOtherJobQualificationDesc)
        Me.tabpOtherJobQualification.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherJobQualification.Name = "tabpOtherJobQualification"
        Me.tabpOtherJobQualification.Size = New System.Drawing.Size(568, 375)
        Me.tabpOtherJobQualification.TabIndex = 2
        Me.tabpOtherJobQualification.Text = "Other Job Qualification"
        Me.tabpOtherJobQualification.UseVisualStyleBackColor = True
        '
        'txtOtherJobQualificationDesc
        '
        Me.txtOtherJobQualificationDesc.BackColor = System.Drawing.SystemColors.Window
        Me.txtOtherJobQualificationDesc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtOtherJobQualificationDesc.Flags = 0
        Me.txtOtherJobQualificationDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherJobQualificationDesc.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherJobQualificationDesc.Location = New System.Drawing.Point(0, 0)
        Me.txtOtherJobQualificationDesc.Multiline = True
        Me.txtOtherJobQualificationDesc.Name = "txtOtherJobQualificationDesc"
        Me.txtOtherJobQualificationDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherJobQualificationDesc.Size = New System.Drawing.Size(568, 375)
        Me.txtOtherJobQualificationDesc.TabIndex = 91
        '
        'tabpOtherJobSkills
        '
        Me.tabpOtherJobSkills.Controls.Add(Me.txtOtherJobSkillsDesc)
        Me.tabpOtherJobSkills.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherJobSkills.Name = "tabpOtherJobSkills"
        Me.tabpOtherJobSkills.Size = New System.Drawing.Size(568, 375)
        Me.tabpOtherJobSkills.TabIndex = 0
        Me.tabpOtherJobSkills.Text = "Other Job Skills"
        Me.tabpOtherJobSkills.UseVisualStyleBackColor = True
        '
        'txtOtherJobSkillsDesc
        '
        Me.txtOtherJobSkillsDesc.BackColor = System.Drawing.SystemColors.Window
        Me.txtOtherJobSkillsDesc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtOtherJobSkillsDesc.Flags = 0
        Me.txtOtherJobSkillsDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherJobSkillsDesc.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherJobSkillsDesc.Location = New System.Drawing.Point(0, 0)
        Me.txtOtherJobSkillsDesc.Multiline = True
        Me.txtOtherJobSkillsDesc.Name = "txtOtherJobSkillsDesc"
        Me.txtOtherJobSkillsDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherJobSkillsDesc.Size = New System.Drawing.Size(568, 375)
        Me.txtOtherJobSkillsDesc.TabIndex = 91
        '
        'tabpOtherJobLanguage
        '
        Me.tabpOtherJobLanguage.Controls.Add(Me.txtOtherJobLanguageDesc)
        Me.tabpOtherJobLanguage.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherJobLanguage.Name = "tabpOtherJobLanguage"
        Me.tabpOtherJobLanguage.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOtherJobLanguage.Size = New System.Drawing.Size(568, 375)
        Me.tabpOtherJobLanguage.TabIndex = 3
        Me.tabpOtherJobLanguage.Text = "Other Job Language"
        Me.tabpOtherJobLanguage.UseVisualStyleBackColor = True
        '
        'txtOtherJobLanguageDesc
        '
        Me.txtOtherJobLanguageDesc.BackColor = System.Drawing.SystemColors.Window
        Me.txtOtherJobLanguageDesc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtOtherJobLanguageDesc.Flags = 0
        Me.txtOtherJobLanguageDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherJobLanguageDesc.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherJobLanguageDesc.Location = New System.Drawing.Point(3, 3)
        Me.txtOtherJobLanguageDesc.Multiline = True
        Me.txtOtherJobLanguageDesc.Name = "txtOtherJobLanguageDesc"
        Me.txtOtherJobLanguageDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherJobLanguageDesc.Size = New System.Drawing.Size(562, 369)
        Me.txtOtherJobLanguageDesc.TabIndex = 91
        '
        'gbLanguage
        '
        Me.gbLanguage.BorderColor = System.Drawing.Color.Black
        Me.gbLanguage.Checked = False
        Me.gbLanguage.CollapseAllExceptThis = False
        Me.gbLanguage.CollapsedHoverImage = Nothing
        Me.gbLanguage.CollapsedNormalImage = Nothing
        Me.gbLanguage.CollapsedPressedImage = Nothing
        Me.gbLanguage.CollapseOnLoad = False
        Me.gbLanguage.Controls.Add(Me.objbtnAddLanguage)
        Me.gbLanguage.Controls.Add(Me.objtlsLanguage)
        Me.gbLanguage.Controls.Add(Me.pnlJobLanguage)
        Me.gbLanguage.ExpandedHoverImage = Nothing
        Me.gbLanguage.ExpandedNormalImage = Nothing
        Me.gbLanguage.ExpandedPressedImage = Nothing
        Me.gbLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLanguage.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLanguage.HeaderHeight = 25
        Me.gbLanguage.HeaderMessage = ""
        Me.gbLanguage.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLanguage.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLanguage.HeightOnCollapse = 0
        Me.gbLanguage.LeftTextSpace = 0
        Me.gbLanguage.Location = New System.Drawing.Point(6, 308)
        Me.gbLanguage.Name = "gbLanguage"
        Me.gbLanguage.OpenHeight = 308
        Me.gbLanguage.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLanguage.ShowBorder = True
        Me.gbLanguage.ShowCheckBox = False
        Me.gbLanguage.ShowCollapseButton = False
        Me.gbLanguage.ShowDefaultBorderColor = True
        Me.gbLanguage.ShowDownButton = False
        Me.gbLanguage.ShowHeader = True
        Me.gbLanguage.Size = New System.Drawing.Size(300, 116)
        Me.gbLanguage.TabIndex = 222
        Me.gbLanguage.Temp = 0
        Me.gbLanguage.Text = "Job Language"
        Me.gbLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddLanguage
        '
        Me.objbtnAddLanguage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAddLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLanguage.BorderSelected = False
        Me.objbtnAddLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLanguage.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLanguage.Location = New System.Drawing.Point(275, 3)
        Me.objbtnAddLanguage.Name = "objbtnAddLanguage"
        Me.objbtnAddLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLanguage.TabIndex = 88
        '
        'objtlsLanguage
        '
        Me.objtlsLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objtlsLanguage.Dock = System.Windows.Forms.DockStyle.None
        Me.objtlsLanguage.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.objtlsLanguage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtlbbtnBoldLanguage, Me.objtlbbtnItalicLanguage})
        Me.objtlsLanguage.Location = New System.Drawing.Point(228, -1)
        Me.objtlsLanguage.Name = "objtlsLanguage"
        Me.objtlsLanguage.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.objtlsLanguage.Size = New System.Drawing.Size(102, 25)
        Me.objtlsLanguage.TabIndex = 153
        '
        'objtlbbtnBoldLanguage
        '
        Me.objtlbbtnBoldLanguage.CheckOnClick = True
        Me.objtlbbtnBoldLanguage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnBoldLanguage.Image = CType(resources.GetObject("objtlbbtnBoldLanguage.Image"), System.Drawing.Image)
        Me.objtlbbtnBoldLanguage.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnBoldLanguage.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnBoldLanguage.Name = "objtlbbtnBoldLanguage"
        Me.objtlbbtnBoldLanguage.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnBoldLanguage.ToolTipText = "Set Skill Bold on Recruitment Portal"
        Me.objtlbbtnBoldLanguage.Visible = False
        '
        'objtlbbtnItalicLanguage
        '
        Me.objtlbbtnItalicLanguage.CheckOnClick = True
        Me.objtlbbtnItalicLanguage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnItalicLanguage.Image = CType(resources.GetObject("objtlbbtnItalicLanguage.Image"), System.Drawing.Image)
        Me.objtlbbtnItalicLanguage.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnItalicLanguage.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnItalicLanguage.Name = "objtlbbtnItalicLanguage"
        Me.objtlbbtnItalicLanguage.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnItalicLanguage.ToolTipText = "Set Skill Italic on Recruitment Portal"
        Me.objtlbbtnItalicLanguage.Visible = False
        '
        'pnlJobLanguage
        '
        Me.pnlJobLanguage.Controls.Add(Me.lvJobLanguage)
        Me.pnlJobLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlJobLanguage.Location = New System.Drawing.Point(2, 26)
        Me.pnlJobLanguage.Name = "pnlJobLanguage"
        Me.pnlJobLanguage.Size = New System.Drawing.Size(295, 87)
        Me.pnlJobLanguage.TabIndex = 125
        '
        'lvJobLanguage
        '
        Me.lvJobLanguage.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhJobLanguage})
        Me.lvJobLanguage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvJobLanguage.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvJobLanguage.Location = New System.Drawing.Point(0, 0)
        Me.lvJobLanguage.Name = "lvJobLanguage"
        Me.lvJobLanguage.Size = New System.Drawing.Size(295, 87)
        Me.lvJobLanguage.TabIndex = 0
        Me.lvJobLanguage.UseCompatibleStateImageBehavior = False
        Me.lvJobLanguage.View = System.Windows.Forms.View.Details
        '
        'objColhJobLanguage
        '
        Me.objColhJobLanguage.Tag = "objColhJobLanguage"
        Me.objColhJobLanguage.Text = ""
        Me.objColhJobLanguage.Width = 195
        '
        'gbJobQualification
        '
        Me.gbJobQualification.BorderColor = System.Drawing.Color.Black
        Me.gbJobQualification.Checked = False
        Me.gbJobQualification.CollapseAllExceptThis = False
        Me.gbJobQualification.CollapsedHoverImage = Nothing
        Me.gbJobQualification.CollapsedNormalImage = Nothing
        Me.gbJobQualification.CollapsedPressedImage = Nothing
        Me.gbJobQualification.CollapseOnLoad = False
        Me.gbJobQualification.Controls.Add(Me.objbtnAddQualification)
        Me.gbJobQualification.Controls.Add(Me.objtlsQuali)
        Me.gbJobQualification.Controls.Add(Me.pnlJobQualification)
        Me.gbJobQualification.ExpandedHoverImage = Nothing
        Me.gbJobQualification.ExpandedNormalImage = Nothing
        Me.gbJobQualification.ExpandedPressedImage = Nothing
        Me.gbJobQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJobQualification.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJobQualification.HeaderHeight = 25
        Me.gbJobQualification.HeaderMessage = ""
        Me.gbJobQualification.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJobQualification.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJobQualification.HeightOnCollapse = 0
        Me.gbJobQualification.LeftTextSpace = 0
        Me.gbJobQualification.Location = New System.Drawing.Point(6, 29)
        Me.gbJobQualification.Name = "gbJobQualification"
        Me.gbJobQualification.OpenHeight = 308
        Me.gbJobQualification.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJobQualification.ShowBorder = True
        Me.gbJobQualification.ShowCheckBox = False
        Me.gbJobQualification.ShowCollapseButton = False
        Me.gbJobQualification.ShowDefaultBorderColor = True
        Me.gbJobQualification.ShowDownButton = False
        Me.gbJobQualification.ShowHeader = True
        Me.gbJobQualification.Size = New System.Drawing.Size(300, 130)
        Me.gbJobQualification.TabIndex = 221
        Me.gbJobQualification.Temp = 0
        Me.gbJobQualification.Text = "Job Qualification"
        Me.gbJobQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddQualification
        '
        Me.objbtnAddQualification.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAddQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddQualification.BorderSelected = False
        Me.objbtnAddQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddQualification.Location = New System.Drawing.Point(275, 3)
        Me.objbtnAddQualification.Name = "objbtnAddQualification"
        Me.objbtnAddQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddQualification.TabIndex = 88
        '
        'objtlsQuali
        '
        Me.objtlsQuali.BackColor = System.Drawing.Color.Transparent
        Me.objtlsQuali.Dock = System.Windows.Forms.DockStyle.None
        Me.objtlsQuali.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.objtlsQuali.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtlbbtnQualiBold, Me.objtlbbtnQualiItalic})
        Me.objtlsQuali.Location = New System.Drawing.Point(228, -2)
        Me.objtlsQuali.Name = "objtlsQuali"
        Me.objtlsQuali.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.objtlsQuali.Size = New System.Drawing.Size(49, 25)
        Me.objtlsQuali.TabIndex = 152
        '
        'objtlbbtnQualiBold
        '
        Me.objtlbbtnQualiBold.CheckOnClick = True
        Me.objtlbbtnQualiBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnQualiBold.Image = CType(resources.GetObject("objtlbbtnQualiBold.Image"), System.Drawing.Image)
        Me.objtlbbtnQualiBold.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnQualiBold.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnQualiBold.Name = "objtlbbtnQualiBold"
        Me.objtlbbtnQualiBold.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnQualiBold.ToolTipText = "Set Qualification Bold on Recruitment Portal"
        '
        'objtlbbtnQualiItalic
        '
        Me.objtlbbtnQualiItalic.CheckOnClick = True
        Me.objtlbbtnQualiItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnQualiItalic.Image = CType(resources.GetObject("objtlbbtnQualiItalic.Image"), System.Drawing.Image)
        Me.objtlbbtnQualiItalic.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnQualiItalic.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnQualiItalic.Name = "objtlbbtnQualiItalic"
        Me.objtlbbtnQualiItalic.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnQualiItalic.ToolTipText = "Set Qualification Italic on Recruitment Portal"
        '
        'pnlJobQualification
        '
        Me.pnlJobQualification.Controls.Add(Me.lvJobQualification)
        Me.pnlJobQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlJobQualification.Location = New System.Drawing.Point(2, 26)
        Me.pnlJobQualification.Name = "pnlJobQualification"
        Me.pnlJobQualification.Size = New System.Drawing.Size(295, 100)
        Me.pnlJobQualification.TabIndex = 125
        '
        'lvJobQualification
        '
        Me.lvJobQualification.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhJobQualification})
        Me.lvJobQualification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvJobQualification.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvJobQualification.Location = New System.Drawing.Point(0, 0)
        Me.lvJobQualification.Name = "lvJobQualification"
        Me.lvJobQualification.Size = New System.Drawing.Size(295, 100)
        Me.lvJobQualification.TabIndex = 0
        Me.lvJobQualification.UseCompatibleStateImageBehavior = False
        Me.lvJobQualification.View = System.Windows.Forms.View.Details
        '
        'objColhJobQualification
        '
        Me.objColhJobQualification.Tag = "objColhJobQualification"
        Me.objColhJobQualification.Text = ""
        Me.objColhJobQualification.Width = 200
        '
        'gbSkill
        '
        Me.gbSkill.BorderColor = System.Drawing.Color.Black
        Me.gbSkill.Checked = False
        Me.gbSkill.CollapseAllExceptThis = False
        Me.gbSkill.CollapsedHoverImage = Nothing
        Me.gbSkill.CollapsedNormalImage = Nothing
        Me.gbSkill.CollapsedPressedImage = Nothing
        Me.gbSkill.CollapseOnLoad = False
        Me.gbSkill.Controls.Add(Me.objbtnAddSkills)
        Me.gbSkill.Controls.Add(Me.objtlsSkill)
        Me.gbSkill.Controls.Add(Me.pnlJobSkill)
        Me.gbSkill.ExpandedHoverImage = Nothing
        Me.gbSkill.ExpandedNormalImage = Nothing
        Me.gbSkill.ExpandedPressedImage = Nothing
        Me.gbSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSkill.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSkill.HeaderHeight = 25
        Me.gbSkill.HeaderMessage = ""
        Me.gbSkill.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSkill.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSkill.HeightOnCollapse = 0
        Me.gbSkill.LeftTextSpace = 0
        Me.gbSkill.Location = New System.Drawing.Point(6, 169)
        Me.gbSkill.Name = "gbSkill"
        Me.gbSkill.OpenHeight = 308
        Me.gbSkill.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSkill.ShowBorder = True
        Me.gbSkill.ShowCheckBox = False
        Me.gbSkill.ShowCollapseButton = False
        Me.gbSkill.ShowDefaultBorderColor = True
        Me.gbSkill.ShowDownButton = False
        Me.gbSkill.ShowHeader = True
        Me.gbSkill.Size = New System.Drawing.Size(300, 130)
        Me.gbSkill.TabIndex = 220
        Me.gbSkill.Temp = 0
        Me.gbSkill.Text = "Job Skills"
        Me.gbSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddSkills
        '
        Me.objbtnAddSkills.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAddSkills.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSkills.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSkills.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSkills.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSkills.BorderSelected = False
        Me.objbtnAddSkills.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSkills.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSkills.Location = New System.Drawing.Point(275, 3)
        Me.objbtnAddSkills.Name = "objbtnAddSkills"
        Me.objbtnAddSkills.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSkills.TabIndex = 88
        '
        'objtlsSkill
        '
        Me.objtlsSkill.BackColor = System.Drawing.Color.Transparent
        Me.objtlsSkill.Dock = System.Windows.Forms.DockStyle.None
        Me.objtlsSkill.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.objtlsSkill.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtlbbtnSkillBold, Me.objtlbbtnSkillItalic})
        Me.objtlsSkill.Location = New System.Drawing.Point(229, -1)
        Me.objtlsSkill.Name = "objtlsSkill"
        Me.objtlsSkill.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.objtlsSkill.Size = New System.Drawing.Size(49, 25)
        Me.objtlsSkill.TabIndex = 153
        '
        'objtlbbtnSkillBold
        '
        Me.objtlbbtnSkillBold.CheckOnClick = True
        Me.objtlbbtnSkillBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnSkillBold.Image = CType(resources.GetObject("objtlbbtnSkillBold.Image"), System.Drawing.Image)
        Me.objtlbbtnSkillBold.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnSkillBold.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnSkillBold.Name = "objtlbbtnSkillBold"
        Me.objtlbbtnSkillBold.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnSkillBold.ToolTipText = "Set Skill Bold on Recruitment Portal"
        '
        'objtlbbtnSkillItalic
        '
        Me.objtlbbtnSkillItalic.CheckOnClick = True
        Me.objtlbbtnSkillItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnSkillItalic.Image = CType(resources.GetObject("objtlbbtnSkillItalic.Image"), System.Drawing.Image)
        Me.objtlbbtnSkillItalic.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objtlbbtnSkillItalic.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnSkillItalic.Name = "objtlbbtnSkillItalic"
        Me.objtlbbtnSkillItalic.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnSkillItalic.ToolTipText = "Set Skill Italic on Recruitment Portal"
        '
        'pnlJobSkill
        '
        Me.pnlJobSkill.Controls.Add(Me.lvJobSkill)
        Me.pnlJobSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlJobSkill.Location = New System.Drawing.Point(2, 26)
        Me.pnlJobSkill.Name = "pnlJobSkill"
        Me.pnlJobSkill.Size = New System.Drawing.Size(295, 100)
        Me.pnlJobSkill.TabIndex = 125
        '
        'lvJobSkill
        '
        Me.lvJobSkill.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhJobSkills})
        Me.lvJobSkill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvJobSkill.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvJobSkill.Location = New System.Drawing.Point(0, 0)
        Me.lvJobSkill.Name = "lvJobSkill"
        Me.lvJobSkill.Size = New System.Drawing.Size(295, 100)
        Me.lvJobSkill.TabIndex = 0
        Me.lvJobSkill.UseCompatibleStateImageBehavior = False
        Me.lvJobSkill.View = System.Windows.Forms.View.Details
        '
        'objColhJobSkills
        '
        Me.objColhJobSkills.Tag = "objColhJobSkills"
        Me.objColhJobSkills.Text = ""
        Me.objColhJobSkills.Width = 200
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnApprove)
        Me.EZeeFooter1.Controls.Add(Me.objlblRemarkItalic)
        Me.EZeeFooter1.Controls.Add(Me.objlblRemarkBold)
        Me.EZeeFooter1.Controls.Add(Me.lblRemarkItalic)
        Me.EZeeFooter1.Controls.Add(Me.lblRemarkBold)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 469)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(910, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(684, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(111, 30)
        Me.btnApprove.TabIndex = 98
        Me.btnApprove.Text = "Save && &Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        Me.btnApprove.Visible = False
        '
        'objlblRemarkItalic
        '
        Me.objlblRemarkItalic.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblRemarkItalic.Location = New System.Drawing.Point(9, 27)
        Me.objlblRemarkItalic.Name = "objlblRemarkItalic"
        Me.objlblRemarkItalic.Size = New System.Drawing.Size(21, 16)
        Me.objlblRemarkItalic.TabIndex = 97
        Me.objlblRemarkItalic.Text = "**"
        Me.objlblRemarkItalic.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblRemarkBold
        '
        Me.objlblRemarkBold.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblRemarkBold.Location = New System.Drawing.Point(9, 7)
        Me.objlblRemarkBold.Name = "objlblRemarkBold"
        Me.objlblRemarkBold.Size = New System.Drawing.Size(21, 16)
        Me.objlblRemarkBold.TabIndex = 96
        Me.objlblRemarkBold.Text = "*"
        Me.objlblRemarkBold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemarkItalic
        '
        Me.lblRemarkItalic.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarkItalic.Location = New System.Drawing.Point(28, 27)
        Me.lblRemarkItalic.Name = "lblRemarkItalic"
        Me.lblRemarkItalic.Size = New System.Drawing.Size(571, 16)
        Me.lblRemarkItalic.TabIndex = 95
        Me.lblRemarkItalic.Text = "Surround the group of words with underscore (_) to set Responsibilities / Remark " & _
            "Italic on recruitment portal"
        Me.lblRemarkItalic.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemarkBold
        '
        Me.lblRemarkBold.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarkBold.Location = New System.Drawing.Point(28, 7)
        Me.lblRemarkBold.Name = "lblRemarkBold"
        Me.lblRemarkBold.Size = New System.Drawing.Size(571, 16)
        Me.lblRemarkBold.TabIndex = 94
        Me.lblRemarkBold.Text = "Surround the group of words with asterisk (*) to set Responsibilities / Remark Bo" & _
            "ld on recruitment portal"
        Me.lblRemarkBold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(684, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(111, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(801, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmVacancy_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(910, 524)
        Me.Controls.Add(Me.pnlVacancy)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVacancy_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Vacancy Add/Edit"
        Me.pnlVacancy.ResumeLayout(False)
        Me.tabcVacancy.ResumeLayout(False)
        Me.tabpJobInfo.ResumeLayout(False)
        Me.gbJob.ResumeLayout(False)
        Me.pnlJob.ResumeLayout(False)
        Me.tabcRemarks.ResumeLayout(False)
        Me.tabpOtherInfo.ResumeLayout(False)
        Me.tabpOtherInfo.PerformLayout()
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpDuties.ResumeLayout(False)
        Me.tabpDuties.PerformLayout()
        Me.tabpExperience.ResumeLayout(False)
        Me.tabpExperience.PerformLayout()
        Me.tlsExperience.ResumeLayout(False)
        Me.tlsExperience.PerformLayout()
        Me.tabpRemark.ResumeLayout(False)
        Me.tabpRemark.PerformLayout()
        Me.tabpInterview.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.gbInterviewer.ResumeLayout(False)
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOthers.ResumeLayout(False)
        Me.pnlOthers.PerformLayout()
        Me.tabpAdvertise.ResumeLayout(False)
        Me.gbAdvertisementAnalysis.ResumeLayout(False)
        Me.gbAdvertisementAnalysis.PerformLayout()
        Me.tabpOtherParameters.ResumeLayout(False)
        Me.gbParametersInfo.ResumeLayout(False)
        Me.tabcOtherInfo.ResumeLayout(False)
        Me.tabpOtherJobQualification.ResumeLayout(False)
        Me.tabpOtherJobQualification.PerformLayout()
        Me.tabpOtherJobSkills.ResumeLayout(False)
        Me.tabpOtherJobSkills.PerformLayout()
        Me.tabpOtherJobLanguage.ResumeLayout(False)
        Me.tabpOtherJobLanguage.PerformLayout()
        Me.gbLanguage.ResumeLayout(False)
        Me.gbLanguage.PerformLayout()
        Me.objtlsLanguage.ResumeLayout(False)
        Me.objtlsLanguage.PerformLayout()
        Me.pnlJobLanguage.ResumeLayout(False)
        Me.gbJobQualification.ResumeLayout(False)
        Me.gbJobQualification.PerformLayout()
        Me.objtlsQuali.ResumeLayout(False)
        Me.objtlsQuali.PerformLayout()
        Me.pnlJobQualification.ResumeLayout(False)
        Me.gbSkill.ResumeLayout(False)
        Me.gbSkill.PerformLayout()
        Me.objtlsSkill.ResumeLayout(False)
        Me.objtlsSkill.PerformLayout()
        Me.pnlJobSkill.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlVacancy As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tabcVacancy As System.Windows.Forms.TabControl
    Friend WithEvents tabpJobInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbJob As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlJob As System.Windows.Forms.Panel
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents cboShiftType As System.Windows.Forms.ComboBox
    Friend WithEvents lblShiftType As System.Windows.Forms.Label
    Friend WithEvents txtPayRangeTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEmploymentType As System.Windows.Forms.Label
    Friend WithEvents cboEmploymentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayRangeTo As System.Windows.Forms.Label
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents txtPayRangeFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPayRangeFrom As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayType As System.Windows.Forms.Label
    Friend WithEvents dtInterviewClosingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblInterviewCloseDate As System.Windows.Forms.Label
    Friend WithEvents dtInterviewStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblInterviewStartDate As System.Windows.Forms.Label
    Friend WithEvents dtClosingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblClosingDate As System.Windows.Forms.Label
    Friend WithEvents dtOpeningDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblJobOpenDate As System.Windows.Forms.Label
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtResponsibilitiesDuties As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNoofPosition As System.Windows.Forms.Label
    Friend WithEvents cboJobDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobDept As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents tabpInterview As System.Windows.Forms.TabPage
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvInterviewer As System.Windows.Forms.ListView
    Friend WithEvents colhEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterviewType As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbInterviewer As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblInterViewType As System.Windows.Forms.Label
    Friend WithEvents cboInterviewType As System.Windows.Forms.ComboBox
    Friend WithEvents pnlOthers As System.Windows.Forms.Panel
    Friend WithEvents lblTrainerContactNo As System.Windows.Forms.Label
    Friend WithEvents txtOtherContactNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblOtherDepartment As System.Windows.Forms.Label
    Friend WithEvents txtOtherCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtOtherInterviewerName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents objColon3 As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeDepartment As System.Windows.Forms.Label
    Friend WithEvents radOthers As System.Windows.Forms.RadioButton
    Friend WithEvents objColon2 As System.Windows.Forms.Label
    Friend WithEvents objColon1 As System.Windows.Forms.Label
    Friend WithEvents lblEmpContact As System.Windows.Forms.Label
    Friend WithEvents lblEmpCompany As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeContactNo As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeCompany As System.Windows.Forms.Label
    Friend WithEvents tabpAdvertise As System.Windows.Forms.TabPage
    Friend WithEvents gbAdvertisementAnalysis As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtNotes As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents lvAdvertisement As System.Windows.Forms.ListView
    Friend WithEvents colhAdvertiserName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCosting As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBudget As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNotes As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboAdvertiser As System.Windows.Forms.ComboBox
    Friend WithEvents lblAgency As System.Windows.Forms.Label
    Friend WithEvents txtBudget As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBudget As System.Windows.Forms.Label
    Friend WithEvents txtCosting As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCosting As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents cboAdvertiseCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblAdvertiser As System.Windows.Forms.Label
    Friend WithEvents objbtnAddInterviewType As eZee.Common.eZeeGradientButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdAdd As eZee.Common.eZeeLightButton
    Friend WithEvents lblVacancyTitle As System.Windows.Forms.Label
    Friend WithEvents nudPosition As System.Windows.Forms.NumericUpDown
    Friend WithEvents objcolhInterviewerunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIntGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhInterviewtypeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAdvertiseunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhADGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAdvertiser As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAdvertiseCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents objcolhCategoryunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeLine3 As eZee.Common.eZeeLine
    Friend WithEvents nudLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents colhLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblLevelInfo As System.Windows.Forms.Label
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents objbtnAddEmployment As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddPayType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddShiftType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddJobDept As eZee.Common.eZeeGradientButton
    Friend WithEvents chkIsExternalVacancy As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchJobs As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents chkExportToWeb As System.Windows.Forms.CheckBox
    Friend WithEvents tabcRemarks As System.Windows.Forms.TabControl
    Friend WithEvents tabpDuties As System.Windows.Forms.TabPage
    Friend WithEvents tabpRemark As System.Windows.Forms.TabPage
    Friend WithEvents objbtnSeachGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSeachGradeGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents tabpOtherInfo As System.Windows.Forms.TabPage
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents lblRemarkBold As System.Windows.Forms.Label
    Friend WithEvents lblRemarkItalic As System.Windows.Forms.Label
    Friend WithEvents objlblRemarkItalic As System.Windows.Forms.Label
    Friend WithEvents objlblRemarkBold As System.Windows.Forms.Label
    Friend WithEvents chkIsBothInternalExternal As System.Windows.Forms.CheckBox
    Friend WithEvents tabpOtherParameters As System.Windows.Forms.TabPage
    Friend WithEvents gbParametersInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tabpExperience As System.Windows.Forms.TabPage
    Friend WithEvents tlsExperience As System.Windows.Forms.ToolStrip
    Friend WithEvents objtlbbtnExperienceBold As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnExperienceItalic As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtWorkingExp As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMonths As System.Windows.Forms.Label
    Friend WithEvents lblWorkExp As System.Windows.Forms.Label
    Friend WithEvents lblOtherWorkingExperience As System.Windows.Forms.Label
    Friend WithEvents gbLanguage As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnAddLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objtlsLanguage As System.Windows.Forms.ToolStrip
    Friend WithEvents objtlbbtnBoldLanguage As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnItalicLanguage As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnlJobLanguage As System.Windows.Forms.Panel
    Friend WithEvents lvJobLanguage As System.Windows.Forms.ListView
    Friend WithEvents objColhJobLanguage As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbJobQualification As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnAddQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objtlsQuali As System.Windows.Forms.ToolStrip
    Friend WithEvents objtlbbtnQualiBold As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnQualiItalic As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnlJobQualification As System.Windows.Forms.Panel
    Friend WithEvents lvJobQualification As System.Windows.Forms.ListView
    Friend WithEvents objColhJobQualification As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbSkill As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnAddSkills As eZee.Common.eZeeGradientButton
    Friend WithEvents objtlsSkill As System.Windows.Forms.ToolStrip
    Friend WithEvents objtlbbtnSkillBold As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnSkillItalic As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnlJobSkill As System.Windows.Forms.Panel
    Friend WithEvents lvJobSkill As System.Windows.Forms.ListView
    Friend WithEvents objColhJobSkills As System.Windows.Forms.ColumnHeader
    Friend WithEvents tabcOtherInfo As System.Windows.Forms.TabControl
    Friend WithEvents tabpOtherJobQualification As System.Windows.Forms.TabPage
    Friend WithEvents tabpOtherJobSkills As System.Windows.Forms.TabPage
    Friend WithEvents tabpOtherJobLanguage As System.Windows.Forms.TabPage
    Friend WithEvents txtOtherJobQualificationDesc As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherJobSkillsDesc As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherJobLanguageDesc As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherWorkingExperience As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
End Class
