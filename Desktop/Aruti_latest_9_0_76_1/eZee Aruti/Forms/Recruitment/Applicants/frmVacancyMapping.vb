﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmVacancyMapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmVacancyMapping"
    Private mblnCancel As Boolean = True
    Private mintApplicantunkid As Integer = 0
    Private mstrApplicantName As String = String.Empty
    Private objVacancyMapping As clsApplicant_Vacancy_Mapping

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUnkId As Integer, ByVal StrName As String) As Boolean

        Try
            mintApplicantunkid = intUnkId
            mstrApplicantName = StrName

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboVacancyType.BackColor = GUI.ColorComp
            cboVacancy.BackColor = GUI.ColorComp
            txtApplicant.BackColor = GUI.ColorComp
            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            txtVacancyFoudOutFrom.BackColor = GUI.ColorOptional
            txtComments.BackColor = GUI.ColorOptional
            'Sohail (27 Apr 2017) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            cboVacancyFoudOutFrom.BackColor = GUI.ColorOptional
            'Sohail (27 Sep 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objVacancyMapping._Applicantunkid = mintApplicantunkid
            objVacancyMapping._Isactive = True
            objVacancyMapping._Userunkid = User._Object._Userunkid
            objVacancyMapping._Vacancyunkid = CInt(cboVacancy.SelectedValue)
            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            objVacancyMapping._VacancyFoundOutFrom = txtVacancyFoudOutFrom.Text.Trim
            If dtpPossibleStartDate.Checked = True Then
                objVacancyMapping._EarliestPossibleStartDate = dtpPossibleStartDate.Value
            Else
                objVacancyMapping._EarliestPossibleStartDate = CDate("01/Jan/1900")
            End If
            objVacancyMapping._Comments = txtComments.Text.Trim
            'Sohail (27 Apr 2017) -- End

            'Sohail (09 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
            objVacancyMapping._IsFromOnline = False
            'Sohail (09 Oct 2018) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            objVacancyMapping._VacancyFoundOutFromUnkId = CInt(cboVacancyFoudOutFrom.SelectedValue)
            'Sohail (27 Sep 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try
            dsList = objVacancyMapping.GetList("List", mintApplicantunkid)

            lvOldVacancy.Items.Clear()
            For Each dRow As DataRow In dsList.Tables("List").Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("vacancy").ToString
                lvItem.SubItems.Add(dRow.Item("appname").ToString)

                'Sohail (27 Apr 2017) -- Start
                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                lvItem.SubItems.Add(dRow.Item("vacancy_found_out_from").ToString)
                If dRow.Item("earliest_possible_startdate").ToString = "19000101" Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("earliest_possible_startdate").ToString).ToShortDateString)
                End If
                lvItem.SubItems.Add(dRow.Item("comments").ToString)
                'Sohail (27 Apr 2017) -- End

                lvOldVacancy.Items.Add(lvItem)
            Next

            If lvOldVacancy.Items.Count > 2 Then
                colhVacancy.Width = 415 - 20
            Else
                colhVacancy.Width = 415
            End If

            lvOldVacancy.GroupingColumn = objcolhApplicant
            lvOldVacancy.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objVacancy As New clsVacancy
        'Sohail (27 Sep 2019) -- Start
        'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
        Dim objCommon As New clsCommon_Master
        'Sohail (27 Sep 2019) -- End
        Dim dsCombos As New DataSet
        Try
            dsCombos = objVacancy.getVacancyType()
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.VACANCY_SOURCE, True, "List")
            With cboVacancyFoudOutFrom
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (27 Sep 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            objVacancy = Nothing
            objCommon = Nothing
            'Sohail (27 Sep 2019) -- End
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim dsVacList As New DataSet
            Dim objVac As New clsVacancy
            Dim strIds As String = String.Empty
            strIds = clsApplicant_Vacancy_Mapping.GetAssingedVacancyIds(mintApplicantunkid)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue), False, True)
            dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue), False, True)
            'Shani(24-Aug-2015) -- End

            Dim dView As DataView = dsVacList.Tables(0).DefaultView
            If strIds.Trim.Length > 0 Then
                dView.RowFilter = "id NOT IN(" & strIds & ")"
            End If
            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dView.ToTable
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (27 Sep 2019) -- Start
    'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
    Private Sub chkOtherVFF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOtherVFF.CheckedChanged
        Try
            If chkOtherVFF.Checked = True Then
                cboVacancyFoudOutFrom.SelectedValue = 0
                cboVacancyFoudOutFrom.Enabled = False
                txtVacancyFoudOutFrom.Enabled = True
            Else
                txtVacancyFoudOutFrom.Text = ""
                txtVacancyFoudOutFrom.Enabled = False
                cboVacancyFoudOutFrom.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkOtherVFF_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Sep 2019) -- End

#End Region

#Region " Form's Event "

    Private Sub frmVacancyMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objVacancyMapping = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyMapping_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmVacancyMapping_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyMapping_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmVacancyMapping_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyMapping_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmVacancyMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objVacancyMapping = New clsApplicant_Vacancy_Mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Call SetColor()
            Call FillCombo()
            txtApplicant.Text = mstrApplicantName
            Call FillList()
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            Call chkOtherVFF_CheckedChanged(chkOtherVFF, New System.EventArgs)
            'Sohail (27 Sep 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyMapping_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm._Other_FormNames = "frmSearchJob" 'Sohail (25 Sep 2020)
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)

            End If

            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Vacancy Type is mandatory information. Please select Vacancy Type to continue."), enMsgBoxStyle.Information)
                cboVacancyType.Focus()
                Exit Sub
            End If

            If CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Vacancy is mandatory information. Please select Vacancy to continue."), enMsgBoxStyle.Information)
                cboVacancy.Focus()
                Exit Sub
            End If

            Call SetValue()

            blnFlag = objVacancyMapping.Insert

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
			Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
			Me.colhVacancy.Text = Language._Object.getCaption(CStr(Me.colhVacancy.Tag), Me.colhVacancy.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lblVacancyFoudOutFrom.Text = Language._Object.getCaption(Me.lblVacancyFoudOutFrom.Name, Me.lblVacancyFoudOutFrom.Text)
			Me.lblPossibleStartDate.Text = Language._Object.getCaption(Me.lblPossibleStartDate.Name, Me.lblPossibleStartDate.Text)
			Me.lblComments.Text = Language._Object.getCaption(Me.lblComments.Name, Me.lblComments.Text)
			Me.colhVacancyFoundOutFrom.Text = Language._Object.getCaption(CStr(Me.colhVacancyFoundOutFrom.Tag), Me.colhVacancyFoundOutFrom.Text)
			Me.colhPossibleStartDate.Text = Language._Object.getCaption(CStr(Me.colhPossibleStartDate.Tag), Me.colhPossibleStartDate.Text)
			Me.colhComments.Text = Language._Object.getCaption(CStr(Me.colhComments.Tag), Me.colhComments.Text)
			Me.chkOtherVFF.Text = Language._Object.getCaption(Me.chkOtherVFF.Name, Me.chkOtherVFF.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Vacancy Type is mandatory information. Please select Vacancy Type to continue.")
			Language.setMessage(mstrModuleName, 2, "Vacancy is mandatory information. Please select Vacancy to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class