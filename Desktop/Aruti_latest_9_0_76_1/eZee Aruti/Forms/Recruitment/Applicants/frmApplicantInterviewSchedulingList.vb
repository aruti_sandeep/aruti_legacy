﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmApplicantInterviewSchedulingList
    Private ReadOnly mstrModuleName As String = "frmApplicantInterviewSchedulingList"
    Private objApplicantBatchInteview As clsApplicant_Batchschedule_Tran
    Private objApplicant As clsApplicant_master
    Private objVacancy As clsVacancy
    Private objCommonMaster As clsCommon_Master
    Private objBatchMaster As clsBatchSchedule
    Private objAppInterviewAnalysis As clsInterviewAnalysis_master
    Private mintAnalysisunkid As Integer = -1

#Region " Private  Methods "
    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strSearching As String = ""
        Dim dtTable As DataTable

        Try

            If User._Object.Privilege._AllowToViewInterviewSchedulingList = True Then                'Pinkal (02-Jul-2012) -- Start



                'S.SANDEEP [20-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
                'dsList = objApplicantBatchInteview.GetList("List")

                'If CInt(cboApplicant.SelectedValue) > 0 Then
                '    strSearching &= "AND applicantunkid = " & CInt(cboApplicant.SelectedValue) & " "
                'End If

                'If CInt(cboBatchName.SelectedValue) > 0 Then
                '    strSearching &= "AND batchscheduleunkid = " & CInt(cboBatchName.SelectedValue) & " "
                'End If

                'If CInt(cboInterviewType.SelectedValue) > 0 Then
                '    strSearching &= "AND interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " "
                'End If

                'If CInt(cboVacancy.SelectedValue) > 0 Then
                '    strSearching &= "AND vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " "
                'End If

                'If CInt(cboResultGroup.SelectedValue) > 0 Then
                '    strSearching &= "AND resultgroupunkid = " & CInt(cboResultGroup.SelectedValue) & " "
                'End If

                'If txtBatchCode.Text.Length > 0 Then
                '    strSearching &= "AND BatchCode LIKE  '%" & txtBatchCode.Text & "%'" & " "
                'End If

                'If txtLocation.Text.Length > 0 Then
                '    strSearching &= "AND location LIKE '%" & txtLocation.Text & "%'" & " "
                'End If

                'If dtpInterviewDateFrom.Checked = True And dtpInterviewTo.Checked = True Then
                '    strSearching &= "AND Interviewdate >= " & eZeeDate.convertDate(dtpInterviewDateFrom.Value) & " AND Interviewdate <= " & eZeeDate.convertDate(dtpInterviewTo.Value) & " "
                'End If

                'If radCancelled.Checked = True Then
                '    strSearching &= "AND ISNULL(iscancel,0) = 1 " & " "
                'End If

                'If radScheduled.Checked = True Then
                '    strSearching &= "AND ISNULL(isvoid,0)=0 AND ISNULL(iscancel,0)=0 " & " "
                'End If

                'If strSearching.Length > 0 Then
                '    strSearching = strSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = dsList.Tables(0)
                'End If

                'lvAppInterviewScheduling.Items.Clear()
                'Dim lvItem As ListViewItem
                'For Each dtRow As DataRow In dtTable.Rows
                '    lvItem = New ListViewItem
                '    lvItem.Text = dtRow.Item("BatchName").ToString
                '    lvItem.SubItems.Add(dtRow.Item("ApplicantName").ToString)
                '    If dtRow.Item("Odate").ToString.Trim.Length > 0 AndAlso dtRow.Item("Cdate").ToString.Trim.Length > 0 Then
                '        lvItem.SubItems.Add(dtRow.Item("vacancy").ToString & " ( " & eZeeDate.convertDate(dtRow.Item("Odate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dtRow.Item("Cdate").ToString).ToShortDateString & " ) ")
                '    Else
                '        lvItem.SubItems.Add(dtRow.Item("vacancy").ToString)
                '    End If
                '    lvItem.SubItems.Add(dtRow.Item("InterviewType").ToString)
                '    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Interviewdate").ToString).ToShortDateString & " " & dtRow.Item("Interviewtime").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("location").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("description").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("isvoid").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("iscancel").ToString)
                '    lvItem.SubItems(colhApplicant.Index).Tag = dtRow.Item("applicantunkid")
                '    lvItem.Tag = dtRow.Item("appbatchscheduletranunkid")
                '    If CBool(dtRow.Item("isvoid").ToString) = True Then
                '        lvItem.ForeColor = Color.Red
                '    End If
                '    If CBool(dtRow.Item("iscancel").ToString) = True Then
                '        lvItem.ForeColor = Color.Purple
                '    End If

                '    'S.SANDEEP [ 09 JULY 2013 ] -- START
                '    'ENHANCEMENT : OTHER CHANGES
                '    lvItem.SubItems.Add(dtRow.Item("GName").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("reviewer").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("score").ToString)
                '    'S.SANDEEP [ 09 JULY 2013 ] -- END

                '    lvAppInterviewScheduling.Items.Add(lvItem)

                'Next


                ''S.SANDEEP [ 09 JULY 2013 ] -- START
                ''ENHANCEMENT : OTHER CHANGES
                ''If lvAppInterviewScheduling.Items.Count > 15 Then
                ''    colhApplicant.Width = 115 - 18
                ''Else
                ''    colhApplicant.Width = 115
                ''End If

                'If lvAppInterviewScheduling.Items.Count > 15 Then
                '    colhVacancy.Width = 155 - 18
                'Else
                '    colhVacancy.Width = 155
                'End If

                'lvAppInterviewScheduling.GroupingColumn = objcolhGrp
                'lvAppInterviewScheduling.DisplayGroups(True)
                ''S.SANDEEP [ 09 JULY 2013 ] -- END


                'lvAppInterviewScheduling.EndUpdate()

                If CInt(cboApplicant.SelectedValue) > 0 Then
                    strSearching &= "AND rcapplicant_batchschedule_tran.applicantunkid = " & CInt(cboApplicant.SelectedValue) & " "
                End If

                If CInt(cboBatchName.SelectedValue) > 0 Then
                    strSearching &= "AND rcapplicant_batchschedule_tran.batchscheduleunkid = " & CInt(cboBatchName.SelectedValue) & " "
                End If

                If CInt(cboInterviewType.SelectedValue) > 0 Then
                    strSearching &= "AND rcbatchschedule_master.interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " "
                End If

                If CInt(cboVacancy.SelectedValue) > 0 Then
                    strSearching &= "AND rcbatchschedule_master.vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " "
                End If

                If CInt(cboResultGroup.SelectedValue) > 0 Then
                    strSearching &= "AND rcbatchschedule_master.resultgroupunkid = " & CInt(cboResultGroup.SelectedValue) & " "
                End If

                If txtBatchCode.Text.Length > 0 Then
                    strSearching &= "AND rcbatchschedule_master.batchcode LIKE  '%" & txtBatchCode.Text & "%'" & " "
                End If

                If txtLocation.Text.Length > 0 Then
                    strSearching &= "AND rcbatchschedule_master.location LIKE '%" & txtLocation.Text & "%'" & " "
                End If

                If dtpInterviewDateFrom.Checked = True And dtpInterviewTo.Checked = True Then
                    strSearching &= "AND CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) >= " & eZeeDate.convertDate(dtpInterviewDateFrom.Value) & " AND CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) <= " & eZeeDate.convertDate(dtpInterviewTo.Value) & " "
                End If

                If radCancelled.Checked = True Then
                    strSearching &= "AND ISNULL(rcapplicant_batchschedule_tran.iscancel,0) = 1 " & " "
                End If

                If radScheduled.Checked = True Then
                    strSearching &= "AND ISNULL(rcapplicant_batchschedule_tran.isvoid,0) = 0 AND ISNULL(rcapplicant_batchschedule_tran.iscancel,0) = 0 " & " "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                End If

                dsList = objApplicantBatchInteview.GetList("List", , , strSearching)

                dtTable = New DataView(dsList.Tables(0), "", "BatchName", DataViewRowState.CurrentRows).ToTable

                lvAppInterviewScheduling.BeginUpdate()
                lvAppInterviewScheduling.Items.Clear()
                Dim lvArray As New List(Of ListViewItem)

                Dim lvItem As ListViewItem
                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dtRow.Item("BatchName").ToString
                    lvItem.SubItems.Add(dtRow.Item("ApplicantName").ToString)
                        lvItem.SubItems.Add(dtRow.Item("vacancy").ToString)
                    lvItem.SubItems.Add(dtRow.Item("InterviewType").ToString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Interviewdate").ToString).ToShortDateString & " " & dtRow.Item("Interviewtime").ToString)
                    lvItem.SubItems.Add(dtRow.Item("location").ToString)
                    lvItem.SubItems.Add(dtRow.Item("description").ToString)
                    lvItem.SubItems.Add(dtRow.Item("isvoid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("iscancel").ToString)
                    lvItem.SubItems(colhApplicant.Index).Tag = dtRow.Item("applicantunkid")
                    lvItem.Tag = dtRow.Item("appbatchscheduletranunkid")
                    If CBool(dtRow.Item("isvoid").ToString) = True Then
                        lvItem.ForeColor = Color.Red
                    End If
                    If CBool(dtRow.Item("iscancel").ToString) = True Then
                        lvItem.ForeColor = Color.Purple
                    End If

                    lvItem.SubItems.Add(dtRow.Item("GName").ToString)
                    lvItem.SubItems.Add(dtRow.Item("reviewer").ToString)
                    lvItem.SubItems.Add(dtRow.Item("score").ToString)

                    lvArray.Add(lvItem)
                Next

                lvAppInterviewScheduling.Items.AddRange(lvArray.ToArray)

                If lvAppInterviewScheduling.Items.Count > 15 Then
                    colhVacancy.Width = 155 - 18
                Else
                    colhVacancy.Width = 155
                End If

                lvAppInterviewScheduling.GroupingColumn = objcolhGrp
                lvAppInterviewScheduling.DisplayGroups(True)

                lvAppInterviewScheduling.EndUpdate()
                'S.SANDEEP [20-APR-2017] -- END
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        objApplicant = New clsApplicant_master
        objVacancy = New clsVacancy
        objCommonMaster = New clsCommon_Master
        objBatchMaster = New clsBatchSchedule
        Try
            dsList = objApplicant.GetApplicantList("List", True)
            With cboApplicant
                .DisplayMember = "applicantname"
                .ValueMember = "applicantunkid"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objVacancy.getComboList(True, "List")
            dsList = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List")
            'Shani(24-Aug-2015) -- End

            With cboVacancy
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType")
            With cboInterviewType
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "ResultGroup")
            With cboResultGroup
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objBatchMaster.getListForCombo("List", True)
            dsList = objBatchMaster.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True)
            'Shani(24-Aug-2015) -- End

            With cboBatchName
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 25 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddInterviewSchedule
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteInterviewSchedule
            btnCancel.Enabled = User._Object.Privilege._AllowtoCancelInterviewSchedule
            mnuAnalyse.Enabled = User._Object.Privilege._AllowtoAddInterviewAnalysis
            mnuChangeBatch.Enabled = User._Object.Privilege._AllowtoChangeBatch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#End Region

#Region " Form's Events "
    Private Sub frmApplicantInterviewSchedulingList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete Then
            btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmApplicantInterviewSchedulingList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmApplicantInterviewSchedulingList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objApplicantBatchInteview = Nothing
    End Sub

    Private Sub frmApplicantInterviewSchedulingList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Call Set_Logo(Me, gApplicationType)
            objApplicantBatchInteview = New clsApplicant_Batchschedule_Tran
            objAppInterviewAnalysis = New clsInterviewAnalysis_master
            lvAppInterviewScheduling.FullRowSelect = True
            Call FillCombo()

            'S.SANDEEP [20-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
            'radScheduled.Checked = True
            RemoveHandler radScheduled.CheckedChanged, AddressOf radScheduled_CheckedChanged
            radScheduled.Checked = True
            AddHandler radScheduled.CheckedChanged, AddressOf radScheduled_CheckedChanged
            'S.SANDEEP [20-APR-2017] -- END


            'Call FillList()


            If lvAppInterviewScheduling.Items.Count > 0 Then lvAppInterviewScheduling.Items(0).Selected = True
            lvAppInterviewScheduling.Select()


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 25 DEC 2011 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantInterviewSchedulingList_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsApplicant_Batchschedule_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicant_Batchschedule_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button Events "
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrm As New frmApplicantInterviewSchedule
            objfrm.displayDialog(-1, enAction.ADD_ONE)
            'Sandeep [ 21 Aug 2010 ] -- Start
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
            If ConfigParameter._Object._InterviewSchedulingTemplateId <= 0 Then
                'Hemant (07 Oct 2019) -- End
            Call FillList()
            End If 'Hemant (07 Oct 2019)
            'Sandeep [ 21 Aug 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub EZeeLightButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim objfrm As New frmInterviewAnalysis_AddEdit
    '        objfrm.ShowDialog()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "EZeeLightButton1_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub mnuAnalyse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAnalyse.Click
        If lvAppInterviewScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            'lvAppInterviewScheduling.Select()
            Exit Sub
        End If

        If lvAppInterviewScheduling.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, " Sorry, You cannot Analyze this transaction.Reason : This transaction is voided."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If lvAppInterviewScheduling.SelectedItems(0).ForeColor = Color.Purple Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, " Sorry, You cannot Analyze this transaction.Reason : This transaction is cancelled."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        mintAnalysisunkid = objAppInterviewAnalysis.GetAnalysisID(CInt(lvAppInterviewScheduling.SelectedItems(0).Tag))
        If mintAnalysisunkid > 0 Then
            objAppInterviewAnalysis._Analysisunkid = mintAnalysisunkid
            If objAppInterviewAnalysis._Iscomplete = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, " Sorry, You cannot Analyze this transaction.Reason : Analysis for this transaction is completed."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        Try
            Dim ObjFrm As New frmInterviewAnalysis_AddEdit
            ObjFrm.displayDialog(-1, enAction.ADD_ONE, CInt(lvAppInterviewScheduling.SelectedItems(0).Tag))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAnalyse_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnnFinalAnalysis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim ObjFrm As New frmInterviewFinalEvaluation
            ObjFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnnFinalAnalysis_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAppInterviewScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Applicant from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAppInterviewScheduling.Select()
            Exit Sub
        End If

        If CBool(lvAppInterviewScheduling.SelectedItems(0).SubItems(objcolhIsVoid.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this transaction. Reason : This transaction is already deleted."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CBool(lvAppInterviewScheduling.SelectedItems(0).SubItems(objcolhIsCancel.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this transaction.Reason : This transaction is cancelled.", enMsgBoxStyle.Information))
            Exit Sub
        End If


        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAppInterviewScheduling.SelectedItems(0).Index

            objApplicantBatchInteview._Appbatchscheduletranunkid = CInt(lvAppInterviewScheduling.SelectedItems(0).Tag)
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Applicant Scheduled Interview?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objApplicantBatchInteview._Isvoid = True

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objApplicantBatchInteview._Voidreason = "Testing"
                'objApplicantBatchInteview._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.INTERVIEW, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objApplicantBatchInteview._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objApplicantBatchInteview._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 
                objApplicantBatchInteview._Voiduserunkid = User._Object._Userunkid

                objApplicantBatchInteview.Delete(CInt(lvAppInterviewScheduling.SelectedItems(0).Tag))
                'Sandeep [ 09 Oct 2010 ] -- Start
                'If radCancelled.Checked = True Or radScheduled.Checked = True Then
                '    lvAppInterviewScheduling.SelectedItems(0).Remove()
                'End If

                If objApplicantBatchInteview._Message <> "" Then
                    eZeeMsgBox.Show(objApplicantBatchInteview._Message, enMsgBoxStyle.Information)
                Else
                    If radCancelled.Checked = True Or radScheduled.Checked = True Then
                        lvAppInterviewScheduling.SelectedItems(0).Remove()
                    End If
                End If
                'Sandeep [ 09 Oct 2010 ] -- End 

                If lvAppInterviewScheduling.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAppInterviewScheduling.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAppInterviewScheduling.Items.Count - 1
                    lvAppInterviewScheduling.Items(intSelectedIndex).Selected = True
                    lvAppInterviewScheduling.EnsureVisible(intSelectedIndex)
                ElseIf lvAppInterviewScheduling.Items.Count <> 0 Then
                    lvAppInterviewScheduling.Items(intSelectedIndex).Selected = True
                    lvAppInterviewScheduling.EnsureVisible(intSelectedIndex)
                End If
            End If

            lvAppInterviewScheduling.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If lvAppInterviewScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Applicant from the list to perform further operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CBool(lvAppInterviewScheduling.SelectedItems(0).SubItems(objcolhIsCancel.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot Cancel this transaction.Reason : This transaction is already cancelled.", enMsgBoxStyle.Information))
            Exit Sub
        End If

        If CBool(lvAppInterviewScheduling.SelectedItems(0).SubItems(objcolhIsVoid.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot Cancel this transaction.Reason : This transaction is already Deleted.", enMsgBoxStyle.Information))
            Exit Sub
        End If

        mintAnalysisunkid = objAppInterviewAnalysis.GetAnalysisID(CInt(lvAppInterviewScheduling.SelectedItems(0).Tag))

        If CBool(objAppInterviewAnalysis.isUsed(mintAnalysisunkid)) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot Cancel this transaction.Reason : The Analysis for this transaction is in progress or analysis has been done.", enMsgBoxStyle.Information))
            Exit Sub
        End If


        Try
            Dim objfrm As New frmCancelRemark
            objfrm.displayDialog(CInt(lvAppInterviewScheduling.SelectedItems(0).Tag), enAction.ADD_ONE, lvAppInterviewScheduling.SelectedItems(0).SubItems(colhBatchName.Index).Text, lvAppInterviewScheduling.SelectedItems(0).SubItems(colhApplicant.Index).Text)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try

    End Sub
#End Region

    Private Sub dtpInterviewDateFrom_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpInterviewDateFrom.ValueChanged

        Try
            If dtpInterviewDateFrom.Checked = True Then
                dtpInterviewTo.Checked = True
                dtpInterviewTo.Value = dtpInterviewDateFrom.Value
            Else
                dtpInterviewTo.Checked = False
                dtpInterviewTo.Value = dtpInterviewDateFrom.Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpInterviewDateFrom_ValueChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub dtpInterviewTo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpInterviewTo.ValueChanged

        Try
            If dtpInterviewTo.Checked = True Then
                dtpInterviewDateFrom.Checked = True
                'dtpInterviewTo.Value = dtpInterviewDateFrom.Value
            Else
                dtpInterviewDateFrom.Checked = False
                'dtpInterviewTo.Value = dtpInterviewDateFrom.Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpInterviewTo_ValueChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
            'S.SANDEEP [20-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
            objbtnSearch.ShowResult(lvAppInterviewScheduling.Items.Count.ToString)
            'S.SANDEEP [20-APR-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click

        Try
            cboApplicant.SelectedValue = 0
            cboBatchName.SelectedValue = 0
            cboInterviewType.SelectedValue = 0
            cboResultGroup.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            txtBatchCode.Text = ""
            txtLocation.Text = ""
            radCancelled.Checked = False
            radShowAll.Checked = False
            radScheduled.Checked = True
            dtpInterviewDateFrom.Value = Today.Date
            dtpInterviewTo.Value = Today.Date
            dtpInterviewDateFrom.Checked = False
            dtpInterviewTo.Checked = False
            Call FillList()
            'S.SANDEEP [20-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
            objbtnReset.ShowResult(lvAppInterviewScheduling.Items.Count.ToString)
            'S.SANDEEP [20-APR-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub radCancelled_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radCancelled.CheckedChanged

        Try
            If radCancelled.Checked = True Then
                'S.SANDEEP [20-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
                'Call FillList()
                lvAppInterviewScheduling.BeginUpdate()
                lvAppInterviewScheduling.Items.Clear()
                objbtnSearch.ShowResult(lvAppInterviewScheduling.Items.Count.ToString)
                lvAppInterviewScheduling.EndUpdate()
                'S.SANDEEP [20-APR-2017] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radCancelled_CheckedChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub radScheduled_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radScheduled.CheckedChanged

        Try
            If radScheduled.Checked = True Then
                'S.SANDEEP [20-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
                'Call FillList()
                lvAppInterviewScheduling.BeginUpdate()
                lvAppInterviewScheduling.Items.Clear()
                objbtnSearch.ShowResult(lvAppInterviewScheduling.Items.Count.ToString)
                lvAppInterviewScheduling.EndUpdate()
                'S.SANDEEP [20-APR-2017] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radScheduled_CheckedChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub radShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radShowAll.CheckedChanged

        Try
            If radShowAll.Checked = True Then
                'S.SANDEEP [20-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
                'Call FillList()
                lvAppInterviewScheduling.BeginUpdate()
                lvAppInterviewScheduling.Items.Clear()
                objbtnSearch.ShowResult(lvAppInterviewScheduling.Items.Count.ToString)
                lvAppInterviewScheduling.EndUpdate()
                'S.SANDEEP [20-APR-2017] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radShowAll_CheckedChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub btnAnalyze_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If CBool(lvAppInterviewScheduling.SelectedItems(0).SubItems(objcolhIsVoid.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You cannot analyze this transaction.Reason: This transaction is voided."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            Dim ObjFrm As New frmInterviewAnalysis_AddEdit
            ObjFrm.displayDialog(-1, enAction.ADD_ONE, CInt(lvAppInterviewScheduling.SelectedItems(0).Tag))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAnalyze_Click", mstrModuleName)
        End Try

    End Sub


    Private Sub ChangeBatchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuChangeBatch.Click
        If lvAppInterviewScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            'lvAppInterviewScheduling.Select()
            Exit Sub
        End If
        Try
            Dim objfrm As New frmApplicantChangeBatch
            objfrm.displayDialog(CInt(lvAppInterviewScheduling.SelectedItems(0).Tag), enAction.EDIT_ONE)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboApplicant.ValueMember
                .DisplayMember = cboApplicant.DisplayMember
                .DataSource = CType(cboApplicant.DataSource, DataTable)
                .CodeMember = "applicant_code"
            End With

            If objFrm.DisplayDialog Then
                cboApplicant.SelectedValue = objFrm.SelectedValue
                cboApplicant.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                Dim dsVacList As New DataSet
                Dim objVac As New clsVacancy

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
                'Shani(24-Aug-2015) -- End

                With cboVacancy
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsVacList.Tables("List")
                    .SelectedValue = 0
                End With

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    Private Sub EZeeGradientButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
                'Hemant (30 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : On batch scheduling form, provide smart search on the job/vacancy.
                '.CodeMember = "applicant_code"
                .CodeMember = ""
                'Hemant (30 Mar 2022) -- End
            End With

            If objFrm.DisplayDialog Then
                cboVacancy.SelectedValue = objFrm.SelectedValue
                cboVacancy.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.rbtnSplitButton.GradientBackColor = GUI._ButttonBackColor
            Me.rbtnSplitButton.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEnrollmentDateTo.Text = Language._Object.getCaption(Me.lblEnrollmentDateTo.Name, Me.lblEnrollmentDateTo.Text)
            Me.lblInterviewFrom.Text = Language._Object.getCaption(Me.lblInterviewFrom.Name, Me.lblInterviewFrom.Text)
            Me.lblInterviewType.Text = Language._Object.getCaption(Me.lblInterviewType.Name, Me.lblInterviewType.Text)
            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
            Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.radCancelled.Text = Language._Object.getCaption(Me.radCancelled.Name, Me.radCancelled.Text)
            Me.radScheduled.Text = Language._Object.getCaption(Me.radScheduled.Name, Me.radScheduled.Text)
            Me.lblLocation.Text = Language._Object.getCaption(Me.lblLocation.Name, Me.lblLocation.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.radShowAll.Text = Language._Object.getCaption(Me.radShowAll.Name, Me.radShowAll.Text)
            Me.mnuAnalyse.Text = Language._Object.getCaption(Me.mnuAnalyse.Name, Me.mnuAnalyse.Text)
            Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
            Me.colhApplicant.Text = Language._Object.getCaption(CStr(Me.colhApplicant.Tag), Me.colhApplicant.Text)
            Me.colhVacancy.Text = Language._Object.getCaption(CStr(Me.colhVacancy.Tag), Me.colhVacancy.Text)
            Me.colhInterviewType.Text = Language._Object.getCaption(CStr(Me.colhInterviewType.Tag), Me.colhInterviewType.Text)
            Me.colhInterviewDate.Text = Language._Object.getCaption(CStr(Me.colhInterviewDate.Tag), Me.colhInterviewDate.Text)
            Me.colhLocation.Text = Language._Object.getCaption(CStr(Me.colhLocation.Tag), Me.colhLocation.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
            Me.lblBatchCode.Text = Language._Object.getCaption(Me.lblBatchCode.Name, Me.lblBatchCode.Text)
            Me.colhBatchName.Text = Language._Object.getCaption(CStr(Me.colhBatchName.Tag), Me.colhBatchName.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.rbtnSplitButton.Text = Language._Object.getCaption(Me.rbtnSplitButton.Name, Me.rbtnSplitButton.Text)
            Me.mnuChangeBatch.Text = Language._Object.getCaption(Me.mnuChangeBatch.Name, Me.mnuChangeBatch.Text)
            Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
            Me.colhInterviewer.Text = Language._Object.getCaption(CStr(Me.colhInterviewer.Tag), Me.colhInterviewer.Text)
            Me.colhScore.Text = Language._Object.getCaption(CStr(Me.colhScore.Tag), Me.colhScore.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot delete this transaction. Reason : This transaction is already deleted.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this transaction.Reason : This transaction is cancelled.", enMsgBoxStyle.Information)
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Applicant Scheduled Interview?")
            Language.setMessage(mstrModuleName, 5, "Please select Applicant from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot Cancel this transaction.Reason : This transaction is already cancelled.", enMsgBoxStyle.Information)
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot Cancel this transaction.Reason : This transaction is already Deleted.", enMsgBoxStyle.Information)
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot Cancel this transaction.Reason : The Analysis for this transaction is in progress or analysis has been done.", enMsgBoxStyle.Information)
            Language.setMessage(mstrModuleName, 9, " Sorry, You cannot Analyze this transaction.Reason : This transaction is voided.")
            Language.setMessage(mstrModuleName, 10, " Sorry, You cannot Analyze this transaction.Reason : This transaction is cancelled.")
            Language.setMessage(mstrModuleName, 11, " Sorry, You cannot Analyze this transaction.Reason : Analysis for this transaction is completed.")
            Language.setMessage(mstrModuleName, 12, "You cannot analyze this transaction.Reason: This transaction is voided.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class