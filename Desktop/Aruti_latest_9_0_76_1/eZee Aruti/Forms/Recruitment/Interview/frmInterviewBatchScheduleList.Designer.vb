﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInterviewBatchScheduleList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInterviewBatchScheduleList))
        Me.pnlInterviewBatch = New System.Windows.Forms.Panel
        Me.lvBatchList = New eZee.Common.eZeeListView(Me.components)
        Me.colhBatchNo = New System.Windows.Forms.ColumnHeader
        Me.colhBatchName = New System.Windows.Forms.ColumnHeader
        Me.colhVacancy = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewDate = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewTime = New System.Windows.Forms.ColumnHeader
        Me.colhResultCode = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsCancel = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsClose = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsVoid = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.LblVacancy = New System.Windows.Forms.Label
        Me.cboBatchName = New System.Windows.Forms.ComboBox
        Me.txtCancelRemark = New eZee.TextBox.AlphanumericTextBox
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblCancelRemark = New System.Windows.Forms.Label
        Me.dtpCancelDate = New System.Windows.Forms.DateTimePicker
        Me.LblVacancyType = New System.Windows.Forms.Label
        Me.lblCancelDate = New System.Windows.Forms.Label
        Me.lblLocation = New System.Windows.Forms.Label
        Me.txtLocation = New eZee.TextBox.AlphanumericTextBox
        Me.objStLine = New eZee.Common.eZeeStraightLine
        Me.radShowAll = New System.Windows.Forms.RadioButton
        Me.radCancelled = New System.Windows.Forms.RadioButton
        Me.radEnrolled = New System.Windows.Forms.RadioButton
        Me.lblInterviewType = New System.Windows.Forms.Label
        Me.cboInterviewType = New System.Windows.Forms.ComboBox
        Me.lblInterviewDate = New System.Windows.Forms.Label
        Me.dtpInterviewDate = New System.Windows.Forms.DateTimePicker
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.lblBatchNo = New System.Windows.Forms.Label
        Me.txtBatchNo = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlInterviewBatch.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlInterviewBatch
        '
        Me.pnlInterviewBatch.Controls.Add(Me.lvBatchList)
        Me.pnlInterviewBatch.Controls.Add(Me.gbFilterCriteria)
        Me.pnlInterviewBatch.Controls.Add(Me.eZeeHeader)
        Me.pnlInterviewBatch.Controls.Add(Me.objFooter)
        Me.pnlInterviewBatch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlInterviewBatch.Location = New System.Drawing.Point(0, 0)
        Me.pnlInterviewBatch.Name = "pnlInterviewBatch"
        Me.pnlInterviewBatch.Size = New System.Drawing.Size(844, 523)
        Me.pnlInterviewBatch.TabIndex = 0
        '
        'lvBatchList
        '
        Me.lvBatchList.BackColorOnChecked = True
        Me.lvBatchList.ColumnHeaders = Nothing
        Me.lvBatchList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhBatchNo, Me.colhBatchName, Me.colhVacancy, Me.colhInterviewDate, Me.colhInterviewTime, Me.colhResultCode, Me.colhDescription, Me.objcolhIsCancel, Me.objcolhIsClose, Me.objcolhIsVoid})
        Me.lvBatchList.CompulsoryColumns = ""
        Me.lvBatchList.FullRowSelect = True
        Me.lvBatchList.GridLines = True
        Me.lvBatchList.GroupingColumn = Nothing
        Me.lvBatchList.HideSelection = False
        Me.lvBatchList.Location = New System.Drawing.Point(12, 218)
        Me.lvBatchList.MinColumnWidth = 50
        Me.lvBatchList.MultiSelect = False
        Me.lvBatchList.Name = "lvBatchList"
        Me.lvBatchList.OptionalColumns = ""
        Me.lvBatchList.ShowMoreItem = False
        Me.lvBatchList.ShowSaveItem = False
        Me.lvBatchList.ShowSelectAll = True
        Me.lvBatchList.ShowSizeAllColumnsToFit = True
        Me.lvBatchList.Size = New System.Drawing.Size(820, 241)
        Me.lvBatchList.Sortable = True
        Me.lvBatchList.TabIndex = 5
        Me.lvBatchList.UseCompatibleStateImageBehavior = False
        Me.lvBatchList.View = System.Windows.Forms.View.Details
        '
        'colhBatchNo
        '
        Me.colhBatchNo.Tag = "colhBatchNo"
        Me.colhBatchNo.Text = "Batch Code"
        Me.colhBatchNo.Width = 100
        '
        'colhBatchName
        '
        Me.colhBatchName.Tag = "colhBatchName"
        Me.colhBatchName.Text = "Batch Name"
        Me.colhBatchName.Width = 130
        '
        'colhVacancy
        '
        Me.colhVacancy.Text = "Vacancy"
        Me.colhVacancy.Width = 130
        '
        'colhInterviewDate
        '
        Me.colhInterviewDate.Text = "Interview Date"
        Me.colhInterviewDate.Width = 100
        '
        'colhInterviewTime
        '
        Me.colhInterviewTime.Text = "Time"
        '
        'colhResultCode
        '
        Me.colhResultCode.Text = "Result Code"
        Me.colhResultCode.Width = 100
        '
        'colhDescription
        '
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 195
        '
        'objcolhIsCancel
        '
        Me.objcolhIsCancel.DisplayIndex = 8
        Me.objcolhIsCancel.Text = "IsCancel"
        Me.objcolhIsCancel.Width = 0
        '
        'objcolhIsClose
        '
        Me.objcolhIsClose.DisplayIndex = 7
        Me.objcolhIsClose.Text = "IsClose"
        Me.objcolhIsClose.Width = 0
        '
        'objcolhIsVoid
        '
        Me.objcolhIsVoid.Text = "IsVoid"
        Me.objcolhIsVoid.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.LblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboBatchName)
        Me.gbFilterCriteria.Controls.Add(Me.txtCancelRemark)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblCancelRemark)
        Me.gbFilterCriteria.Controls.Add(Me.dtpCancelDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblCancelDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblLocation)
        Me.gbFilterCriteria.Controls.Add(Me.txtLocation)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine)
        Me.gbFilterCriteria.Controls.Add(Me.radShowAll)
        Me.gbFilterCriteria.Controls.Add(Me.radCancelled)
        Me.gbFilterCriteria.Controls.Add(Me.radEnrolled)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.cboInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpInterviewDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblRemark)
        Me.gbFilterCriteria.Controls.Add(Me.txtDescription)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatchName)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatchNo)
        Me.gbFilterCriteria.Controls.Add(Me.txtBatchNo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 67
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(820, 145)
        Me.gbFilterCriteria.TabIndex = 4
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(681, 32)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 441
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 600
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(332, 32)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(343, 21)
        Me.cboVacancy.TabIndex = 229
        '
        'LblVacancy
        '
        Me.LblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVacancy.Location = New System.Drawing.Point(240, 35)
        Me.LblVacancy.Name = "LblVacancy"
        Me.LblVacancy.Size = New System.Drawing.Size(90, 15)
        Me.LblVacancy.TabIndex = 228
        Me.LblVacancy.Text = "Vacancy"
        Me.LblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBatchName
        '
        Me.cboBatchName.DropDownWidth = 300
        Me.cboBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatchName.FormattingEnabled = True
        Me.cboBatchName.Location = New System.Drawing.Point(94, 87)
        Me.cboBatchName.Name = "cboBatchName"
        Me.cboBatchName.Size = New System.Drawing.Size(130, 21)
        Me.cboBatchName.TabIndex = 224
        '
        'txtCancelRemark
        '
        Me.txtCancelRemark.Flags = 0
        Me.txtCancelRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCancelRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCancelRemark.Location = New System.Drawing.Point(332, 115)
        Me.txtCancelRemark.Name = "txtCancelRemark"
        Me.txtCancelRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCancelRemark.Size = New System.Drawing.Size(373, 21)
        Me.txtCancelRemark.TabIndex = 221
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 300
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(94, 32)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(130, 21)
        Me.cboVacancyType.TabIndex = 227
        '
        'lblCancelRemark
        '
        Me.lblCancelRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancelRemark.Location = New System.Drawing.Point(240, 116)
        Me.lblCancelRemark.Name = "lblCancelRemark"
        Me.lblCancelRemark.Size = New System.Drawing.Size(90, 15)
        Me.lblCancelRemark.TabIndex = 222
        Me.lblCancelRemark.Text = "Remark"
        Me.lblCancelRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpCancelDate
        '
        Me.dtpCancelDate.Checked = False
        Me.dtpCancelDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCancelDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCancelDate.Location = New System.Drawing.Point(94, 114)
        Me.dtpCancelDate.Name = "dtpCancelDate"
        Me.dtpCancelDate.ShowCheckBox = True
        Me.dtpCancelDate.Size = New System.Drawing.Size(130, 21)
        Me.dtpCancelDate.TabIndex = 220
        '
        'LblVacancyType
        '
        Me.LblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVacancyType.Location = New System.Drawing.Point(8, 35)
        Me.LblVacancyType.Name = "LblVacancyType"
        Me.LblVacancyType.Size = New System.Drawing.Size(80, 15)
        Me.LblVacancyType.TabIndex = 226
        Me.LblVacancyType.Text = "Vacancy Type"
        Me.LblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCancelDate
        '
        Me.lblCancelDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancelDate.Location = New System.Drawing.Point(8, 117)
        Me.lblCancelDate.Name = "lblCancelDate"
        Me.lblCancelDate.Size = New System.Drawing.Size(80, 15)
        Me.lblCancelDate.TabIndex = 219
        Me.lblCancelDate.Text = "Cancel Date"
        Me.lblCancelDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLocation
        '
        Me.lblLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.Location = New System.Drawing.Point(462, 63)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(75, 15)
        Me.lblLocation.TabIndex = 216
        Me.lblLocation.Text = "Location"
        Me.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLocation
        '
        Me.txtLocation.Flags = 0
        Me.txtLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLocation.Location = New System.Drawing.Point(543, 60)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(162, 21)
        Me.txtLocation.TabIndex = 215
        '
        'objStLine
        '
        Me.objStLine.BackColor = System.Drawing.Color.Transparent
        Me.objStLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine.Location = New System.Drawing.Point(711, 31)
        Me.objStLine.Name = "objStLine"
        Me.objStLine.Size = New System.Drawing.Size(10, 103)
        Me.objStLine.TabIndex = 214
        '
        'radShowAll
        '
        Me.radShowAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowAll.Location = New System.Drawing.Point(727, 88)
        Me.radShowAll.Name = "radShowAll"
        Me.radShowAll.Size = New System.Drawing.Size(85, 17)
        Me.radShowAll.TabIndex = 213
        Me.radShowAll.TabStop = True
        Me.radShowAll.Text = "Show All"
        Me.radShowAll.UseVisualStyleBackColor = True
        '
        'radCancelled
        '
        Me.radCancelled.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCancelled.Location = New System.Drawing.Point(727, 61)
        Me.radCancelled.Name = "radCancelled"
        Me.radCancelled.Size = New System.Drawing.Size(85, 17)
        Me.radCancelled.TabIndex = 212
        Me.radCancelled.TabStop = True
        Me.radCancelled.Text = "Cancelled"
        Me.radCancelled.UseVisualStyleBackColor = True
        '
        'radEnrolled
        '
        Me.radEnrolled.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEnrolled.Location = New System.Drawing.Point(727, 34)
        Me.radEnrolled.Name = "radEnrolled"
        Me.radEnrolled.Size = New System.Drawing.Size(85, 17)
        Me.radEnrolled.TabIndex = 211
        Me.radEnrolled.TabStop = True
        Me.radEnrolled.Text = "Scheduled"
        Me.radEnrolled.UseVisualStyleBackColor = True
        '
        'lblInterviewType
        '
        Me.lblInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewType.Location = New System.Drawing.Point(240, 64)
        Me.lblInterviewType.Name = "lblInterviewType"
        Me.lblInterviewType.Size = New System.Drawing.Size(90, 15)
        Me.lblInterviewType.TabIndex = 210
        Me.lblInterviewType.Text = "Interview Type"
        Me.lblInterviewType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInterviewType
        '
        Me.cboInterviewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewType.DropDownWidth = 300
        Me.cboInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewType.FormattingEnabled = True
        Me.cboInterviewType.Location = New System.Drawing.Point(332, 61)
        Me.cboInterviewType.Name = "cboInterviewType"
        Me.cboInterviewType.Size = New System.Drawing.Size(124, 21)
        Me.cboInterviewType.TabIndex = 209
        '
        'lblInterviewDate
        '
        Me.lblInterviewDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewDate.Location = New System.Drawing.Point(240, 91)
        Me.lblInterviewDate.Name = "lblInterviewDate"
        Me.lblInterviewDate.Size = New System.Drawing.Size(90, 15)
        Me.lblInterviewDate.TabIndex = 208
        Me.lblInterviewDate.Text = "Interview Date"
        Me.lblInterviewDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpInterviewDate
        '
        Me.dtpInterviewDate.Checked = False
        Me.dtpInterviewDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInterviewDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInterviewDate.Location = New System.Drawing.Point(332, 88)
        Me.dtpInterviewDate.Name = "dtpInterviewDate"
        Me.dtpInterviewDate.ShowCheckBox = True
        Me.dtpInterviewDate.Size = New System.Drawing.Size(124, 21)
        Me.dtpInterviewDate.TabIndex = 207
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(462, 90)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(75, 15)
        Me.lblRemark.TabIndex = 205
        Me.lblRemark.Text = "Description"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(543, 87)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(162, 21)
        Me.txtDescription.TabIndex = 204
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(8, 90)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(80, 15)
        Me.lblBatchName.TabIndex = 203
        Me.lblBatchName.Text = "Batch Name"
        Me.lblBatchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchNo
        '
        Me.lblBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchNo.Location = New System.Drawing.Point(8, 63)
        Me.lblBatchNo.Name = "lblBatchNo"
        Me.lblBatchNo.Size = New System.Drawing.Size(80, 15)
        Me.lblBatchNo.TabIndex = 201
        Me.lblBatchNo.Text = "Batch Code"
        Me.lblBatchNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBatchNo
        '
        Me.txtBatchNo.Flags = 0
        Me.txtBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchNo.Location = New System.Drawing.Point(94, 60)
        Me.txtBatchNo.Name = "txtBatchNo"
        Me.txtBatchNo.Size = New System.Drawing.Size(130, 21)
        Me.txtBatchNo.TabIndex = 200
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(793, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(770, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(844, 60)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Batch Scheduling List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 468)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(844, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(643, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 76
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(547, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 75
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(451, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(739, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 73
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmInterviewBatchScheduleList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 523)
        Me.Controls.Add(Me.pnlInterviewBatch)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInterviewBatchScheduleList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Batch Scheduling List"
        Me.pnlInterviewBatch.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlInterviewBatch As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents lblBatchNo As System.Windows.Forms.Label
    Friend WithEvents txtBatchNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvBatchList As eZee.Common.eZeeListView
    Friend WithEvents colhBatchNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBatchName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVacancy As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblInterviewDate As System.Windows.Forms.Label
    Friend WithEvents dtpInterviewDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboInterviewType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterviewType As System.Windows.Forms.Label
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents txtLocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objStLine As eZee.Common.eZeeStraightLine
    Friend WithEvents radShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents radCancelled As System.Windows.Forms.RadioButton
    Friend WithEvents radEnrolled As System.Windows.Forms.RadioButton
    Friend WithEvents dtpCancelDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCancelDate As System.Windows.Forms.Label
    Friend WithEvents txtCancelRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCancelRemark As System.Windows.Forms.Label
    Friend WithEvents colhInterviewDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterviewTime As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResultCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsClose As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsCancel As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsVoid As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboBatchName As System.Windows.Forms.ComboBox
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents LblVacancy As System.Windows.Forms.Label
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents LblVacancyType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
End Class
