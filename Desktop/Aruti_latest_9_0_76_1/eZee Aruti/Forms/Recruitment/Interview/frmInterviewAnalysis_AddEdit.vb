﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmInterviewAnalysis_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrmodulename As String = "frmInterviewAnalysis_AddEdit"
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
    'Private objInterviewer_tran As clsinterviewer_tran
    Private objBatchScheduleInterviewer_tran As clsBatchSchedule_interviewer_tran
    Private mintBatchScheduleUnkid As Integer = 0
    'Hemant (07 Oct 2019) -- End
    Private objResult As clsresult_master
    Private mintAnalysisMasterUnkid As Integer = -1
    Private mintResultGroup As Integer = -1
    Private mintAppBatchScheduleTranId As Integer = -1
    Private mintAnalysisTranId As Integer = -1
    Private objBatchDetails As clsApplicant_Batchschedule_Tran
    Private mdtTran As DataTable
    Private intAnalysisSeletedIdx As Integer = -1
    Private objAnalysisMaster As clsInterviewAnalysis_master
    Private objEmployee As clsEmployee_Master
    Private objInterviewerAnalysis As clsInteviewAnalysis_tran
    Private mintVacancyunkid As Integer = -1
    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private mintApplicantUnkid As Integer = 0
    'S.SANDEEP [ 14 May 2013 ] -- END

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intAppBatchScheduletranId As Integer) As Boolean

        Try
            mintAnalysisTranId = intUnkId
            menAction = eAction
            mintAppBatchScheduleTranId = intAppBatchScheduletranId

            Me.ShowDialog()

            intUnkId = mintAnalysisTranId
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrmodulename)
        End Try

    End Function
#End Region

#Region " Form's Events "
    Private Sub frmInterviewerAnalysis_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAnalysisMaster = Nothing
    End Sub

    Private Sub frmInterviewerAnalysis_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmInterviewerAnalysis_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmInterviewAnalysis_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        radEmployee.Checked = True
        'Hemant (07 Oct 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
        'objInterviewer_tran = New clsinterviewer_tran
        objBatchScheduleInterviewer_tran = New clsBatchSchedule_interviewer_tran
        'Hemant (07 Oct 2019) -- End
        objResult = New clsresult_master
        objAnalysisMaster = New clsInterviewAnalysis_master
        objEmployee = New clsEmployee_Master
        objInterviewerAnalysis = New clsInteviewAnalysis_tran
        lblOtherDeptName.Text = ""
        lblOtherCompany.Text = ""
        lblOtherEmpContactNo.Text = ""



        Try
            Call Set_Logo(Me, gApplicationType)
            Call GetBatchScheduleDetails()
            FillCombo()

            radEmployee.Checked = True
            If menAction = enAction.EDIT_ONE Then
                objAnalysisMaster._Analysisunkid = mintAnalysisTranId
                objBatchDetails._Appbatchscheduletranunkid = objAnalysisMaster._Appbatchscheduletranunkid
                ' objEmployee._Employeeunkid = objAnalysisMaster.

            Else
                objBatchDetails._Appbatchscheduletranunkid = mintAppBatchScheduleTranId

            End If
            objInterviewerAnalysis._AnalysisUnkid = mintAnalysisTranId
            mdtTran = objInterviewerAnalysis._DataTable
            Call FillAnalysisTran()

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 25 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmInterviewAnalysis_AddEdit_Load", mstrmodulename)
        End Try

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsInterviewAnalysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsInterviewAnalysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Try

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
            'dsList = objInterviewer_tran.getComboList(mintVacancyunkid, True)
            dsList = objBatchScheduleInterviewer_tran.getComboList(mintBatchScheduleUnkid, True)
            'Hemant (07 Oct 2019) -- End
            dtTable = New DataView(dsList.Tables(0), "id >= 0 ", "", DataViewRowState.CurrentRows).ToTable

            With cboEmployee
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                '.ValueMember = "interviewertranunkid"
                .ValueMember = "batchscheduleinterviewertranunkid"
                'Hemant (07 Oct 2019) -- End
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
            dtTable = Nothing

            dtTable = New DataView(dsList.Tables(0), "id <=0 ", "", DataViewRowState.CurrentRows).ToTable

            With cboOtherEmployee
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                '.ValueMember = "interviewertranunkid"
                .ValueMember = "batchscheduleinterviewertranunkid"
                'Hemant (07 Oct 2019) -- End
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
            dtTable = Nothing

            dsList = objResult.getComboList("List", True, mintResultGroup)
            With cboResult
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrmodulename)
        End Try

    End Sub

    Private Sub GetBatchScheduleDetails()
        Dim dsList As New DataSet
        objBatchDetails = New clsApplicant_Batchschedule_Tran
        Try
            dsList = objBatchDetails.GetList("List", mintAppBatchScheduleTranId)
            txtApplicantName.Text = dsList.Tables("List").Rows(0).Item("ApplicantName").ToString
            txtBatchCode.Text = dsList.Tables(0).Rows(0).Item("BatchCode").ToString
            txtBatchName.Text = dsList.Tables(0).Rows(0).Item("BatchName").ToString
            txtInterviewType.Text = dsList.Tables(0).Rows(0).Item("InterviewType").ToString
            mintVacancyunkid = CInt(dsList.Tables(0).Rows(0).Item("vacancyunkid").ToString)
            mintResultGroup = CInt(dsList.Tables(0).Rows(0).Item("resultgroupunkid").ToString)
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            mintApplicantUnkid = CInt(dsList.Tables(0).Rows(0).Item("applicantunkid").ToString)
            'S.SANDEEP [ 14 May 2013 ] -- END
            mintBatchScheduleUnkid = CInt(dsList.Tables(0).Rows(0).Item("batchscheduleunkid").ToString)   'Hemant (07 Oct 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetBatchScheduleDetails", mstrmodulename)
        End Try

    End Sub

    Private Sub SetValue()

        Try
            objAnalysisMaster._Appbatchscheduletranunkid = mintAppBatchScheduleTranId

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'objAnalysisMaster._Userunkid = 1
            objAnalysisMaster._Userunkid = User._Object._Userunkid
            objAnalysisMaster._Statustypid = enShortListing_Status.SC_PENDING
            objAnalysisMaster._Vacancyunkid = mintVacancyunkid
            objAnalysisMaster._Applicantunkid = mintApplicantUnkid
            'S.SANDEEP [ 14 May 2013 ] -- END


            If menAction = enAction.EDIT_ONE Then
                objAnalysisMaster._Appbatchscheduletranunkid = objAnalysisMaster._Appbatchscheduletranunkid
            Else
                objAnalysisMaster._Appbatchscheduletranunkid = mintAppBatchScheduleTranId
            End If
            If mintAnalysisMasterUnkid = -1 Then
                objAnalysisMaster._Isvoid = False
                objAnalysisMaster._Voidatetime = Nothing
                objAnalysisMaster._Voidreason = ""
                objAnalysisMaster._Voiduserunkid = -1
                'Anjan (09 Sep 2010)-Start
                'To be uncommented when user merged.
                'objAnalysisMaster._Userinkid = User._Object._Userunkid
                objAnalysisMaster._Userunkid = 1
                'Anjan (09 Sep 2010)-End

            Else
                objAnalysisMaster._Userunkid = objAnalysisMaster._Userunkid
                objAnalysisMaster._Isvoid = objAnalysisMaster._Isvoid
                objAnalysisMaster._Voidatetime = objAnalysisMaster._Voidatetime
                objAnalysisMaster._Voidreason = objAnalysisMaster._Voidreason
                objAnalysisMaster._Voiduserunkid = objAnalysisMaster._Voiduserunkid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrmodulename)
        End Try

    End Sub

    Private Sub GetValue()

        Try
            mintAppBatchScheduleTranId = objAnalysisMaster._Appbatchscheduletranunkid
            objAnalysisMaster._Isvoid = objAnalysisMaster._Isvoid
            objAnalysisMaster._Voidatetime = objAnalysisMaster._Voidatetime
            objAnalysisMaster._Voidreason = objAnalysisMaster._Voidreason
            objAnalysisMaster._Voiduserunkid = objAnalysisMaster._Voiduserunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrmodulename)
        End Try

    End Sub

    Private Sub FillAnalysisTran()
        Dim dsList As New DataSet
        Try
            lvAnalysis.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                    'dsList = objInterviewer_tran.GetInterviewerData(CInt(dtRow.Item("interviewertranunkid")))
                    dsList = objBatchScheduleInterviewer_tran.GetInterviewerData(CInt(dtRow.Item("interviewertranunkid")))
                    'Hemant (07 Oct 2019) -- End

                    'Pinkal (18-Jan-2013) -- Start
                    'Enhancement : TRA Changes
                    'Hemant (03 Jun 2020) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 3 : A Interview Score report showing interview candidate(s) and the scores of each interviewer to be developed)
                    'If dsList Is Nothing Then Exit Sub
                    If dsList Is Nothing Then Continue For
                    'Hemant (03 Jun 2020) -- End
                    'Pinkal (18-Jan-2013) -- End

                    If CInt(dsList.Tables(0).Rows(0).Item("interviewerunkid")) <> -1 Then
                        Dim objEmp As New clsEmployee_Master
                        Dim objDept As New clsDepartment

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = CInt(dsList.Tables(0).Rows(0).Item("interviewerunkid"))
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dsList.Tables(0).Rows(0).Item("interviewerunkid"))
                        'S.SANDEEP [04 JUN 2015] -- END

                        objDept._Departmentunkid = objEmp._Departmentunkid
                        lvItem.Text = objEmp._Firstname & " " & objEmp._Surname & " " & objEmp._Othername
                        lvItem.SubItems.Add(objDept._Name.ToString)
                        'Anjan (09 Sep 2010)-Start
                        'To be uncommented when company merged.
                        'lvItem.SubItems.Add(Company._Object._Name.ToString)
                        lvItem.SubItems.Add("")
                        'Anjan (09 Sep 2010)-End

                        lvItem.SubItems.Add(objEmp._Present_Tel_No.ToString)
                        objEmp = Nothing
                        objDept = Nothing
                    ElseIf CInt(dsList.Tables(0).Rows(0).Item("interviewerunkid")) = -1 Then
                        lvItem.Text = CStr(dsList.Tables(0).Rows(0).Item("otherinterviewer_name"))
                        lvItem.SubItems.Add(dsList.Tables(0).Rows(0).Item("otherdepartment").ToString)
                        lvItem.SubItems.Add(dsList.Tables(0).Rows(0).Item("othercompany").ToString)
                        lvItem.SubItems.Add(dsList.Tables(0).Rows(0).Item("othercontact_no").ToString)
                    End If

                    Dim objResult As New clsresult_master
                    objResult._Resultunkid = CInt(dtRow.Item("resultcodeunkid"))
                    lvItem.SubItems.Add(objResult._Resultname.ToString)
                    objResult = Nothing
                    lvItem.SubItems.Add(dtRow.Item("remark").ToString)
                    lvItem.SubItems.Add(dtRow.Item("resultcodeunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.SubItems.Add(dtRow.Item("interviewertranunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("analysis_date").ToString)

                    lvItem.Tag = dtRow.Item("analysistranunkid")

                    lvAnalysis.Items.Add(lvItem)
                    lvItem = Nothing
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAnalysisTran", mstrmodulename)
        End Try


    End Sub

    Private Sub ResetItems()
        Try
            If radEmployee.Checked = True Then
                cboEmployee.SelectedValue = 0
            ElseIf radOthers.Checked = True Then
                cboOtherEmployee.SelectedValue = 0
            End If
            cboResult.SelectedValue = 0
            txtRemark.Text = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetItems", mstrmodulename)
        End Try
    End Sub

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            objbtnAddResult.Enabled = User._Object.Privilege._AddResultCode
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrmodulename)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#End Region

#Region " Button Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrmodulename)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim objVacancyData As New clsVacancy
        Try
            If lvAnalysis.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 3, "There is nothing to save in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'objVacancyData._Vacancyunkid = mintVacancyunkid
            'If dtpAnalysisDate.Value > objVacancyData._Closingdate Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 111, " Sorry, You cannot Analyze this interview type.Reason : Vacancy is already closed."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Anjan (02 Mar 2012)-End 



            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAnalysisMaster.Update(mdtTran, False)
            Else
                blnFlag = objAnalysisMaster.Insert(mdtTran, False)
            End If

            If blnFlag = False And objAnalysisMaster._Message <> "" Then
                eZeeMsgBox.Show(objAnalysisMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAnalysisMaster = Nothing
                    objAnalysisMaster = New clsInterviewAnalysis_master
                    Call GetValue()
                    txtApplicantName.Focus()
                Else
                    mintAnalysisMasterUnkid = objAnalysisMaster._Analysisunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrmodulename)
        End Try
    End Sub

    Private Sub objbtnOtherEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboOtherEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboOtherEmployee.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboOtherEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherEmployee_Click", mstrmodulename)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrmodulename)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If radEmployee.Checked = True Then
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 1, "Employee is compulsory information, it cannot be blank. Please select a Employee to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow() = mdtTran.Select("interviewertranunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")
            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 2, "Selected Employee is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        ElseIf radOthers.Checked = True Then
            If CInt(cboOtherEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 1, "Employee is compulsory information, it cannot be blank. Please select a Employee to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow() = mdtTran.Select("interviewertranunkid = " & CInt(cboOtherEmployee.SelectedValue) & " AND AUD <> 'D' ")
            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 2, "Selected Employee is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        If CInt(cboResult.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 4, "Result is compulsory information, it cannot be blank.Please select Result."), enMsgBoxStyle.Information)
            cboResult.Focus()
            Exit Sub
        End If

        Try
            Dim dtARow As DataRow
            dtARow = mdtTran.NewRow

            dtARow.Item("analysistranunkid") = -1
            dtARow.Item("analysisunkid") = mintAnalysisTranId
            If radEmployee.Checked = True Then
                dtARow.Item("interviewertranunkid") = CInt(cboEmployee.SelectedValue)
            ElseIf radOthers.Checked = True Then
                dtARow.Item("interviewertranunkid") = CInt(cboOtherEmployee.SelectedValue)
            End If
            dtARow.Item("resultcodeunkid") = CInt(cboResult.SelectedValue)
            dtARow.Item("remark") = txtRemark.Text
            dtARow.Item("analysis_date") = dtpAnalysisDate.Value
            dtARow.Item("AUD") = "A"
            dtARow.Item("GUID") = Guid.NewGuid.ToString

            mdtTran.Rows.Add(dtARow)

            Call FillAnalysisTran()
            Call ResetItems()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrmodulename)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        Try
            If lvAnalysis.SelectedItems.Count > 0 Then
                If intAnalysisSeletedIdx > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvAnalysis.Items(intAnalysisSeletedIdx).SubItems(objcolhGUID.Index).Text & "'")
                    Else
                        drTemp = mdtTran.Select("analysistranunkid = " & CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag))
                    End If
                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("analysistranunkid") = lvAnalysis.Items(intAnalysisSeletedIdx).Tag
                            .Item("analysisunkid") = mintAnalysisMasterUnkid
                            If radEmployee.Checked = True Then
                                .Item("interviewertranunkid") = CInt(cboEmployee.SelectedValue)
                            ElseIf radOthers.Checked = True Then
                                .Item("interviewertranunkid") = CInt(cboOtherEmployee.SelectedValue)
                            End If
                            .Item("resultcodeunkid") = CInt(cboResult.SelectedValue)
                            .Item("remark") = txtRemark.Text
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .Item("GUID") = Guid.NewGuid.ToString
                        End With
                        Call FillAnalysisTran()
                    End If
                End If
                Call ResetItems()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrmodulename)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try
            If lvAnalysis.SelectedItems.Count > 0 Then
                If intAnalysisSeletedIdx > -1 Then
                    Dim drTemp As DataRow()

                    If CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvAnalysis.Items(intAnalysisSeletedIdx).SubItems(objcolhGUID.Index).Text & "'")
                    Else
                        drTemp = mdtTran.Select("analysistranunkid = " & CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag))
                    End If

                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        Call FillAnalysisTran()
                    End If
                End If
            End If
            Call ResetItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrmodulename)
        End Try

    End Sub

#End Region

#Region " Controls "
    Private Sub lvAnalysis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAnalysis.Click
        Dim dsInterviewerData As New DataSet
        Try
            If lvAnalysis.SelectedItems.Count > 0 Then
                intAnalysisSeletedIdx = lvAnalysis.SelectedItems(0).Index
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                'objInterviewer_tran._Interviewernkid = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhInterviewerId.Index).Text)
                'dsInterviewerData = objInterviewer_tran.GetInterviewerData(CInt(objInterviewer_tran._Interviewernkid))
                objBatchScheduleInterviewer_tran._Interviewernkid = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhInterviewerId.Index).Text)
                dsInterviewerData = objBatchScheduleInterviewer_tran.GetInterviewerData(CInt(objBatchScheduleInterviewer_tran._Interviewernkid))
                'Hemant (07 Oct 2019) -- End

                If CInt(dsInterviewerData.Tables(0).Rows(0).Item("interviewerunkid")) <> -1 Then
                    radEmployee.Checked = True
                    cboEmployee.SelectedValue = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhInterviewerId.Index).Text)
                Else
                    radOthers.Checked = True
                    cboOtherEmployee.SelectedValue = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhInterviewerId.Index).Text)
                End If
                cboResult.SelectedValue = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhResultId.Index).Text)
                txtRemark.Text = lvAnalysis.SelectedItems(0).SubItems(colhRemark.Index).Text
                dtpAnalysisDate.Value = CDate(lvAnalysis.SelectedItems(0).SubItems(objcolhAnalysisDate.Index).Text)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAnalysis_Click", mstrmodulename)
        End Try
    End Sub

    Private Sub radEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged, radOthers.CheckedChanged
        If radEmployee.Checked = True Then
            pnlEmployeeInfo.Visible = True
            pnlOtherInfo.Visible = False
        ElseIf radOthers.Checked = True Then
            pnlOtherInfo.Visible = True
            pnlEmployeeInfo.Visible = False
        End If
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim dslist As New DataSet
        Dim objDepartment As New clsDepartment
        Dim objEmployeeData As New clsEmployee_Master
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                'dslist = objInterviewer_tran.GetInterviewerData(CInt(cboEmployee.SelectedValue))
                dslist = objBatchScheduleInterviewer_tran.GetInterviewerData(CInt(cboEmployee.SelectedValue))
                'Hemant (07 Oct 2019) -- End
                lblCompanyValue.Text = Company._Object._Name

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployeeData._Employeeunkid = CInt(dslist.Tables(0).Rows(0).Item("interviewerunkid").ToString)
                objEmployeeData._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dslist.Tables(0).Rows(0).Item("interviewerunkid").ToString)
                'S.SANDEEP [04 JUN 2015] -- END

                objDepartment._Departmentunkid = objEmployeeData._Departmentunkid

                lblDepartmentValue.Text = objDepartment._Name
                lblEmployeeContactNo.Text = objEmployeeData._Present_Tel_No
            Else
                lblCompanyValue.Text = ""
                lblDepartmentValue.Text = ""
                lblEmployeeContactNo.Text = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrmodulename)
        End Try

    End Sub

    Private Sub cboOtherEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOtherEmployee.SelectedIndexChanged
        Dim dsList As New DataSet
        Try
            If CInt(cboOtherEmployee.SelectedValue) > 0 Then
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                'dsList = objInterviewer_tran.GetInterviewerData(CInt(cboOtherEmployee.SelectedValue))
                dsList = objBatchScheduleInterviewer_tran.GetInterviewerData(CInt(cboOtherEmployee.SelectedValue))
                'Hemant (07 Oct 2019) -- End
                lblOtherCompany.Text = dsList.Tables(0).Rows(0).Item("othercompany").ToString
                lblOtherDeptName.Text = dsList.Tables(0).Rows(0).Item("otherdepartment").ToString
                lblOtherEmpContactNo.Text = dsList.Tables(0).Rows(0).Item("othercontact_no").ToString
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOtherEmployee_SelectedIndexChanged", mstrmodulename)
        End Try

    End Sub

#End Region

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub objbtnAddResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResult.Click
        Dim frm As New frmResultCode_AddEdit
        Dim intRefId As Integer = 0
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objResult As New clsresult_master
                dsList = objResult.getComboList("Result", True, mintResultGroup)
                With cboResult
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Result")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddResult_Click", mstrmodulename)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbTrainingAnalysis.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTrainingAnalysis.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbTrainingAnalysis.Text = Language._Object.getCaption(Me.gbTrainingAnalysis.Name, Me.gbTrainingAnalysis.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
			Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
			Me.lnReviewerInfo.Text = Language._Object.getCaption(Me.lnReviewerInfo.Name, Me.lnReviewerInfo.Text)
			Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
			Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblEmployeeContactNo.Text = Language._Object.getCaption(Me.lblEmployeeContactNo.Name, Me.lblEmployeeContactNo.Text)
			Me.lblCompanyValue.Text = Language._Object.getCaption(Me.lblCompanyValue.Name, Me.lblCompanyValue.Text)
			Me.lblDepartmentValue.Text = Language._Object.getCaption(Me.lblDepartmentValue.Name, Me.lblDepartmentValue.Text)
			Me.lblScore.Text = Language._Object.getCaption(Me.lblScore.Name, Me.lblScore.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblAnalysisDate.Text = Language._Object.getCaption(Me.lblAnalysisDate.Name, Me.lblAnalysisDate.Text)
			Me.lnTraineeInfo.Text = Language._Object.getCaption(Me.lnTraineeInfo.Name, Me.lnTraineeInfo.Text)
			Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.lblBatchCode.Text = Language._Object.getCaption(Me.lblBatchCode.Name, Me.lblBatchCode.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.lblOtherEmpContactNo.Text = Language._Object.getCaption(Me.lblOtherEmpContactNo.Name, Me.lblOtherEmpContactNo.Text)
			Me.lblOtherCompany.Text = Language._Object.getCaption(Me.lblOtherCompany.Name, Me.lblOtherCompany.Text)
			Me.lblOtherDeptName.Text = Language._Object.getCaption(Me.lblOtherDeptName.Name, Me.lblOtherDeptName.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhPosition.Text = Language._Object.getCaption(CStr(Me.colhPosition.Tag), Me.colhPosition.Text)
			Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
			Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
			Me.colhScore.Text = Language._Object.getCaption(CStr(Me.colhScore.Tag), Me.colhScore.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
			Me.Label3.Text = Language._Object.getCaption(Me.Label3.Name, Me.Label3.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrmodulename, 1, "Employee is compulsory information, it cannot be blank. Please select a Employee to continue.")
			Language.setMessage(mstrmodulename, 2, "Selected Employee is already added to the list.")
			Language.setMessage(mstrmodulename, 3, "There is nothing to save in the list.")
			Language.setMessage(mstrmodulename, 4, "Result is compulsory information, it cannot be blank.Please select Result.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class