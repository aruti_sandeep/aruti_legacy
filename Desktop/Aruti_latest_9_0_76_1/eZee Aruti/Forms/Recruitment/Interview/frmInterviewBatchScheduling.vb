﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO

Public Class frmInterviewBatchScheduling

#Region "Private Variables "
    Private ReadOnly mstrmodulename As String = "frmInterviewBatchScheduling"
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mintBatchScheduleunkid As Integer = 0
    Dim objBatchSchedule As clsBatchSchedule
    Dim objVacancyData As clsVacancy
    Dim objCommonMaster As clsCommon_Master

    'Hemant (27 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
    Private mintVacancyunkid As Integer = 0
    'Interviewer Declaration
    Private mdtInterviewerTran As DataTable
    Private mintInterviewerIndex As Integer
    Private objInterviewerTran As clsBatchSchedule_interviewer_tran
    Private objEmployeeData As clsEmployee_Master
    Private objDepartmentData As clsDepartment
    Private objInterviewType As clsCommon_Master
    'Hemant (27 Sep 2019) -- End

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Private trd As Thread
    Private objEmailList As New List(Of clsEmailCollection)
    Dim objOldBatchSchedule As New clsBatchSchedule
    Dim strLetterContent As String = ""
    Dim rtfTemp As RichTextBox
    Dim dtRow() As DataRow = Nothing
    'Hemant (07 Oct 2019) -- End

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean

        Try

            mintBatchScheduleunkid = intUnkId

            menAction = eAction

            Me.ShowDialog()
            intUnkId = mintBatchScheduleunkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtBatchCode.BackColor = GUI.ColorComp
            txtBatchName.BackColor = GUI.ColorComp
            txtCancelRemark.BackColor = GUI.ColorComp
            txtLocation.BackColor = GUI.ColorOptional
            cboInterviewType.BackColor = GUI.ColorComp
            cboResultGroup.BackColor = GUI.ColorComp
            cboVacancy.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtCancelRemark.BackColor = GUI.ColorComp
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboVacancyType.BackColor = GUI.ColorComp
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            cboiInterViewType.BackColor = GUI.ColorComp     'Hemant (27 Sep 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrmodulename)
        End Try
    End Sub

    Private Sub GetValue()

        Try
            txtBatchCode.Text = objBatchSchedule._Batchcode
            txtBatchName.Text = objBatchSchedule._Batchname
            If objBatchSchedule._Interviewdate = Nothing Then
                dtInterviewDate.Value = Now.Date
            Else
                dtInterviewDate.Value = objBatchSchedule._Interviewdate
            End If

            If objBatchSchedule._Interviewtime <> Nothing Then
                dtInterviewTime.Value = objBatchSchedule._Interviewtime
            End If

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboVacancy.SelectedValue = objBatchSchedule._Vacancyunkid
            If objBatchSchedule._Vacancyunkid > 0 Then
                Dim objVac As New clsVacancy
                objVac._Vacancyunkid = objBatchSchedule._Vacancyunkid
                Select Case CBool(objVac._Is_External_Vacancy)
                    Case True
                        cboVacancyType.SelectedValue = enVacancyType.EXTERNAL_VACANCY
                    Case False
                        'Sohail (12 Nov 2020) -- Start
                        'Internal Issue # : - External applicants from External Internal vacancies are not coming on Import Eligible Applicant screen.
                        'cboVacancyType.SelectedValue = enVacancyType.INTERNAL_VACANCY
                        If objVac._Isbothintext = True Then
                            cboVacancyType.SelectedValue = enVacancyType.INTERNAL_AND_EXTERNAL
                        Else
                        cboVacancyType.SelectedValue = enVacancyType.INTERNAL_VACANCY
                        End If
                        'Sohail (12 Nov 2020) -- End
                End Select
                objVac = Nothing
            Else
                cboVacancyType.SelectedValue = 0
            End If
            'S.SANDEEP [ 25 DEC 2011 ] -- END


            cboInterviewType.SelectedValue = objBatchSchedule._Interviewtypeunkid
            cboResultGroup.SelectedValue = objBatchSchedule._Resultgroupunkid
            txtLocation.Text = objBatchSchedule._Location
            txtDescription.Text = objBatchSchedule._Description

            If objBatchSchedule._Isclosed = True Then
                chkIsClosed.Checked = True
            Else
                chkIsClosed.Checked = False
            End If


            txtCancelRemark.Text = objBatchSchedule._Cancel_Remark

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrmodulename)
        End Try
    End Sub

    Private Sub SetValue()

        Try
            objBatchSchedule._Batchcode = txtBatchCode.Text
            objBatchSchedule._Batchname = txtBatchName.Text
            objBatchSchedule._Interviewdate = dtInterviewDate.Value
            objBatchSchedule._Interviewtime = dtInterviewTime.Value
            objBatchSchedule._Description = txtDescription.Text
            objBatchSchedule._Resultgroupunkid = CInt(cboResultGroup.SelectedValue)
            objBatchSchedule._Isclosed = CBool(chkIsClosed.CheckState)
            objBatchSchedule._Interviewtypeunkid = CInt(cboInterviewType.SelectedValue)
            objBatchSchedule._Vacancyunkid = CInt(cboVacancy.SelectedValue)
            objBatchSchedule._Location = txtLocation.Text



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrmodulename)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        objVacancyData = New clsVacancy
        objCommonMaster = New clsCommon_Master

        Try

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objVacancyData.getComboList(True, "List")
            'With cboVacancy
            '    .ValueMember = "id"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            dsList = objVacancyData.getVacancyType()
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sandeep [ 17 DEC 2010 ] -- Start
            'dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType")
            'With cboInterviewType
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("InterviewType")
            '    .SelectedValue = 0
            'End With
            'Sandeep [ 17 DEC 2010 ] -- End` 

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "ResultGroup")
            With cboResultGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("ResultGroup")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrmodulename)
        End Try
    End Sub

    'S.SANDEEP [ 06 SEP 2011 ] -- START
    Private Function IsValid() As Boolean
        Try
            If txtBatchCode.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 4, "Batch Code cannot be blank. Batch Code is mandatory information."), enMsgBoxStyle.Information)
                txtBatchCode.Focus()
                Return False
            End If

            If txtBatchName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 5, "Batch Name cannot be blank. Batch Name is mandatory information."), enMsgBoxStyle.Information)
                txtBatchName.Focus()
                Return False
            End If
            'Anjan (04 Jan 2012)-Start
            'Issue : Interview date can be greater then vacancy dates - Mr.Andrew Suggesstion.
            'If CInt(cboVacancy.SelectedValue) > 0 Then
            '    If dtInterviewDate.Value > objVacancyData._Interview_Closedate Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 1, "Batch Interview date cannot be greater than Vacancy Interview closing date."), enMsgBoxStyle.Information)
            '        dtInterviewDate.Focus()
            '        Return False
            '    ElseIf dtInterviewDate.Value < objVacancyData._Interview_Startdate Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 2, "Batch Interview date cannot be less than Vacancy Interview start date."), enMsgBoxStyle.Information)
            '        dtInterviewDate.Focus()
            '        Return False
            '    End If
            'End If
            'Anjan (04 Jan 2012)-End


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 9, "Vacancy Type cannot be blank. Vacancy Type is compulsory information."), enMsgBoxStyle.Information)
                cboVacancyType.Focus()
                Return False
            End If
            'S.SANDEEP [ 25 DEC 2011 ] -- END


            If CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 1, "Vacancy is mandatory information. Please select Vacancy to continue."), enMsgBoxStyle.Information)
                cboVacancy.Focus()
                Return False
            End If

            If CInt(cboResultGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 2, "Result Code cannot be blank. Result Code is mandatory information."), enMsgBoxStyle.Information)
                cboResultGroup.Focus()
                Return False
            End If

            If CInt(cboInterviewType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 3, "Interview Type is mandatory information. Please select Interview Type to continue."), enMsgBoxStyle.Information)
                cboInterviewType.Focus()
                Return False
            End If

            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
            If lvInterviewer.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 21, "Please add atleast one Interview Detail."), enMsgBoxStyle.Information)
                If tabcBatchSchdule.SelectedIndex <> 1 Then tabcBatchSchdule.SelectedIndex = 1 'Interview Tab
                cboiInterViewType.Focus()
                Exit Function
            End If
            'Hemant (27 Sep 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrmodulename)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 06 SEP 2011 ] -- END 


    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Setvisibility()
        Try
            btnCancel.Enabled = User._Object.Privilege._AllowtoCancelBatch
            btnActivate.Enabled = User._Object.Privilege._AllowtoMakeBatchActive
            objbtnAddRGroup.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrmodulename)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    'Hemant (27 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
    Private Sub FillInterviewersList()
        objEmployeeData = New clsEmployee_Master
        objDepartmentData = New clsDepartment
        Try
            lvInterviewer.Items.Clear()

            Dim lvInterviewerList As ListViewItem
            For Each dtRow As DataRow In mdtInterviewerTran.Rows
                objEmployeeData._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dtRow.Item("interviewerunkid"))
                objDepartmentData._Departmentunkid = objEmployeeData._Departmentunkid
                objInterviewType._Masterunkid = CInt(dtRow.Item("interviewtypeunkid"))
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvInterviewerList = New ListViewItem
                    lvInterviewerList.Text = dtRow.Item("interviewer_level").ToString
                    If CInt(dtRow.Item("interviewerunkid")) > 0 Then
                        lvInterviewerList.SubItems.Add(objEmployeeData._Firstname + " " + objEmployeeData._Surname)
                        lvInterviewerList.SubItems.Add(Company._Object._Name)
                        lvInterviewerList.SubItems.Add(objDepartmentData._Name)
                        lvInterviewerList.SubItems.Add(objEmployeeData._Present_Tel_No)
                    Else
                        lvInterviewerList.SubItems.Add(dtRow.Item("otherinterviewer_name").ToString)
                        lvInterviewerList.SubItems.Add(dtRow.Item("othercompany").ToString)
                        lvInterviewerList.SubItems.Add(dtRow.Item("otherdepartment").ToString)
                        lvInterviewerList.SubItems.Add(dtRow.Item("othercontact_no").ToString)
                    End If

                    lvInterviewerList.SubItems.Add(objInterviewType._Name)
                    lvInterviewerList.SubItems.Add(dtRow.Item("interviewerunkid").ToString)
                    lvInterviewerList.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvInterviewerList.SubItems.Add(dtRow.Item("interviewtypeunkid").ToString)
                    lvInterviewerList.Tag = dtRow.Item("batchscheduleinterviewertranunkid")
                    lvInterviewer.Items.Add(lvInterviewerList)

                    lvInterviewerList = Nothing
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInterviewersList", mstrModuleName)
        End Try

    End Sub

    Private Sub InterviewerFillCombo()
        Dim dsCombos As New DataSet
        objInterviewType = New clsCommon_Master
        objEmployeeData = New clsEmployee_Master
        Try
            dsCombos = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType")
            With cboiInterViewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("InterviewType")
            End With

            dsCombos = objEmployeeData.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                       True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InterviewerFillCombo", mstrModuleName)
        End Try

    End Sub

    Private Sub ResetInterviewer()
        Try
            cboiInterViewType.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            lblEmployeeCompany.Text = ""
            lblEmployeeContactNo.Text = ""
            lblEmployeeDepartment.Text = ""
            nudLevel.Value = 1
            txtOtherCompany.Text = ""
            txtOtherContactNo.Text = ""
            txtOtherDepartment.Text = ""
            txtOtherInterviewerName.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrmodulename)
        End Try
    End Sub
    'Hemant (27 Sep 2019) -- End

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Private Sub Send_Notification()
        Try
            If objEmailList.Count > 0 Then
                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Stop()
                Dim objSendMail As New clsSendMail
                For Each obj In objEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = obj._ModuleRefId

                    Try
                        objSendMail.SendMail(Company._Object._Companyunkid)
                    Catch ex As Exception

                    End Try
                Next
                objEmailList.Clear()
                AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Start()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrmodulename)
        Finally
            If objEmailList.Count > 0 Then
                objEmailList.Clear()
            End If
        End Try
    End Sub

    Private Sub ViewMergeData()
        Try
            Dim StrCol As String = ""
            Dim strDataName As String = ""
            For j As Integer = 0 To dtRow(0).Table.Columns.Count - 1
                StrCol = dtRow(0).Table.Columns(j).ColumnName
                If rtfTemp.Rtf.Contains("#" & StrCol & "#") Then
                    rtfTemp.Focus()
                    strDataName = rtfTemp.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                    rtfTemp.Rtf = strDataName
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 Oct 2019) -- End


#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        Try

            If IsValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBatchSchedule.Update(ConfigParameter._Object._CurrentDateAndTime, mdtInterviewerTran)
                'Hemant (27 Sep 2019) -- [ConfigParameter._Object._CurrentDateAndTime, mdtInterviewerTran]
            Else
                blnFlag = objBatchSchedule.Insert(ConfigParameter._Object._CurrentDateAndTime, mdtInterviewerTran)
                'Hemant (27 Sep 2019) -- [ConfigParameter._Object._CurrentDateAndTime, mdtInterviewerTran]
            End If

            If blnFlag = False And objBatchSchedule._Message <> "" Then
                eZeeMsgBox.Show(objBatchSchedule._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
                Dim dsList As New DataSet
                Dim strInterviewerIds As String = String.Empty
                Dim strIntViewId() As String = Nothing
                Dim objLetterFields As New clsLetterFields
                Dim objLetterType As New clsLetterType
                rtfTemp = New RichTextBox

                If objOldBatchSchedule._Vacancyunkid <> objBatchSchedule._Vacancyunkid OrElse _
                   objOldBatchSchedule._Batchcode <> objBatchSchedule._Batchcode OrElse _
                   objOldBatchSchedule._Batchname <> objBatchSchedule._Batchname OrElse _
                   objOldBatchSchedule._Location <> objBatchSchedule._Location OrElse _
                   objOldBatchSchedule._Interviewdate <> objBatchSchedule._Interviewdate OrElse _
                   objOldBatchSchedule._Interviewtime <> objBatchSchedule._Interviewtime OrElse _
                   objOldBatchSchedule._Interviewtypeunkid <> objBatchSchedule._Interviewtypeunkid OrElse _
                   objOldBatchSchedule._Resultgroupunkid <> objBatchSchedule._Resultgroupunkid OrElse _
                   objOldBatchSchedule._Description <> objBatchSchedule._Description Then

                    strInterviewerIds = String.Join(",", mdtInterviewerTran.AsEnumerable().Select(Function(x) x.Field(Of Integer)("interviewerunkid").ToString).ToArray())
                Else
                    strInterviewerIds = String.Join(",", mdtInterviewerTran.Select("AUD = 'A'").AsEnumerable().Select(Function(x) x.Field(Of Integer)("interviewerunkid").ToString).ToArray())
                End If

                objLetterFields._BatchscheduleUnkid = objBatchSchedule._Batchscheduleunkid

                If CInt(ConfigParameter._Object._BatchSchedulingTemplateId) > 0 Then
                    objLetterType._LettertypeUnkId = CInt(ConfigParameter._Object._BatchSchedulingTemplateId)
                    strLetterContent = objLetterType._Lettercontent
                End If

                objEmailList = New List(Of clsEmailCollection)

                If CInt(ConfigParameter._Object._BatchSchedulingTemplateId) > 0 AndAlso strLetterContent.Trim.Length > 0 Then
                    If strInterviewerIds.Trim.Length > 0 Then

                        strIntViewId = strInterviewerIds.Split(CChar(","))
                        dsList = objLetterFields.GetEmployeeData(strInterviewerIds, enImg_Email_RefId.Batch_Scheduling, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)

                        For i As Integer = 0 To strIntViewId.Length - 1


                            Dim oEmail As New List(Of clsEmailCollection)

                            If dsList.Tables(0).Rows.Count > 0 Then
                                dtRow = dsList.Tables(0).Select("InterviewerUnkId = '" & CInt(strIntViewId(i)) & "'")
                            End If
                            If dtRow.Length > 0 Then
                                rtfTemp.Rtf = strLetterContent
                                Call ViewMergeData()
                            End If

                            Dim htmlOutput = "Document.html"
                            Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                            Dim htmlResult = RtfToHtmlConverter.RtfToHtml(rtfTemp.Rtf, contentUriPrefix)
                            htmlResult.WriteToFile(htmlOutput)
                            Dim strHtmlEmailContent As String = htmlResult._HTML

                            objBatchSchedule.Set_Notification_Interviewers(FinancialYear._Object._DatabaseName, _
                                                                           User._Object._Userunkid, _
                                                                           FinancialYear._Object._YearUnkid, _
                                                                           Company._Object._Companyunkid, _
                                                                           CInt(strIntViewId(i)), _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                           strHtmlEmailContent, _
                                                                           dtRow, _
                                                                           enLogin_Mode.DESKTOP, 0, 0, False, _
                                                                           oEmail _
                                                                           )

                            objEmailList.AddRange(oEmail)
                        Next
                    End If
                End If
                'Hemant (07 Oct 2019) -- End

                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBatchSchedule = Nothing
                    objBatchSchedule = New clsBatchSchedule
                    Call GetValue()
                    'Hemant (30 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members. When adding the interview detail, the level should not be mandatory. User should be able to add the employee without selecting the level.)
                    objInterviewerTran._BatchScheduleUnkid = objBatchSchedule._Batchscheduleunkid
                    mdtInterviewerTran = objInterviewerTran._DataTable
                    Call FillInterviewersList()
                    'Hemant (30 Oct 2019) -- End
                Else
                    mintBatchScheduleunkid = objBatchSchedule._Batchscheduleunkid
                    Me.Close()
                End If
            End If

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
            trd = New Thread(AddressOf Send_Notification)
            trd.IsBackground = True
            trd.Start()
            'Hemant (07 Oct 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrmodulename)
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If txtCancelRemark.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 6, "Please give Cancel Remark. Cancel Remark is compulsory information."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try

            objBatchSchedule._Cancel_Date = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
            objBatchSchedule._Cancel_Remark = txtCancelRemark.Text
            objBatchSchedule._Cancel_Userunkid = 1
            objBatchSchedule._Iscancel = True


            objBatchSchedule.Active_Cancel(mintBatchScheduleunkid)
            mblnCancel = False
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrmodulename)
        End Try

    End Sub

    Private Sub btnActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActivate.Click

        Try
            If objBatchSchedule._Iscancel = True Then
                objBatchSchedule._Cancel_Date = Nothing
                objBatchSchedule._Cancel_Remark = ""
                objBatchSchedule._Cancel_Userunkid = -1
                objBatchSchedule._Iscancel = False

                objBatchSchedule.Active_Cancel(mintBatchScheduleunkid)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrmodulename)
        End Try

    End Sub

#End Region

#Region " Form Events "

    Private Sub frmBatchScheduling_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBatchSchedule = Nothing
        objCommonMaster = Nothing
        objVacancyData = Nothing
    End Sub

    Private Sub frmBatchScheduling_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmBatchScheduling_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmBatchScheduling_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBatchSchedule = New clsBatchSchedule
        'Hemant (27 Sep 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
        radEmployee.Checked = True
        objInterviewerTran = New clsBatchSchedule_interviewer_tran
        'Hemant (27 Sep 2019) -- End
        Try

            Call SetColor()

            Call Set_Logo(Me, gApplicationType)
            'Sandeep [ 02 Oct 2010 ] -- Start
            If mintBatchScheduleunkid = -1 Then
                chkIsClosed.Visible = False
            End If
            'Sandeep [ 02 Oct 2010 ] -- End

            If menAction = enAction.EDIT_ONE Then
                objBatchSchedule._Batchscheduleunkid = mintBatchScheduleunkid
                objOldBatchSchedule._Batchscheduleunkid = mintBatchScheduleunkid  'Hemant (07 Oct 2019)
            Else
                tabcRemarks.TabPages.Remove(tabpCancelRemark)
            End If

            If objBatchSchedule._Iscancel = True Then
                btnActivate.Enabled = True
            Else
                btnActivate.Enabled = False
            End If

            Call FillCombo()
            Call InterviewerFillCombo() 'Hemant (27 Sep 2019)
            Call GetValue()

            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
            objInterviewerTran._BatchScheduleUnkid = mintBatchScheduleunkid
            mdtInterviewerTran = objInterviewerTran._DataTable
            Call FillInterviewersList()
            'Hemant (27 Sep 2019) -- End

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call Setvisibility()
            'S.SANDEEP [ 25 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchScheduling_Load", mstrmodulename)
        End Try

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsBatchSchedule.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchSchedule"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Tab Buttons "

#Region " Interview "
    'Hemant (27 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Try
            If radEmployee.Checked = True Then
                Dim dtRow As DataRow() = mdtInterviewerTran.Select("interviewerunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")

                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 10, "Selected Interviewer is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            ElseIf radOthers.Checked = True Then
                Dim dtRow As DataRow() = mdtInterviewerTran.Select("otherinterviewer_name LIKE '" & txtOtherInterviewerName.Text & "%' AND AUD <> 'D' ")

                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 10, "Selected Interviewer is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If

            If txtOtherCompany.Text = Company._Object._Name Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 11, "Company cannot be same. Please give another company name."), enMsgBoxStyle.Information)
                txtOtherCompany.Focus()
                Exit Sub
            End If

            If CInt(cboiInterViewType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 12, "Please select Interview Type."), enMsgBoxStyle.Information)
                cboiInterViewType.Focus()
                Exit Sub
            ElseIf radEmployee.Checked = True AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 13, "Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub
            ElseIf radOthers.Checked = True AndAlso txtOtherInterviewerName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 14, "Please Enter Interviewer Name."), enMsgBoxStyle.Information)
                txtOtherInterviewerName.Focus()
                Exit Sub
            End If

            'Hemant (30 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members. When adding the interview detail, the level should not be mandatory. User should be able to add the employee without selecting the level.)
            'Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = " & CDec(nudLevel.Value) & " AND AUD <> 'D' ")
            'If dtLevelRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 15, "Selected Level is already assigned to Interviewer. Please assign new level."), enMsgBoxStyle.Information)
            '    nudLevel.Focus()
            '    Exit Sub
            'End If
            'Hemant (30 Oct 2019) -- End

            Dim dtInterviewerRow As DataRow
            dtInterviewerRow = mdtInterviewerTran.NewRow

            dtInterviewerRow.Item("batchscheduleinterviewertranunkid") = -1
            dtInterviewerRow.Item("batchscheduleunkid") = mintBatchScheduleunkid
            dtInterviewerRow.Item("vacancyunkid") = CInt(cboVacancy.SelectedValue)
            dtInterviewerRow.Item("interviewer_level") = nudLevel.Value
            If radEmployee.Checked = True Then
                dtInterviewerRow.Item("interviewerunkid") = CInt(cboEmployee.SelectedValue)
                dtInterviewerRow.Item("otherinterviewer_name") = ""
                dtInterviewerRow.Item("othercompany") = ""
                dtInterviewerRow.Item("otherdepartment") = ""
                dtInterviewerRow.Item("othercontact_no") = ""
            Else
                dtInterviewerRow.Item("interviewerunkid") = CInt(-1)
                dtInterviewerRow.Item("otherinterviewer_name") = txtOtherInterviewerName.Text
                dtInterviewerRow.Item("othercompany") = txtOtherCompany.Text
                dtInterviewerRow.Item("otherdepartment") = txtOtherDepartment.Text
                dtInterviewerRow.Item("othercontact_no") = txtOtherContactNo.Text
            End If
            dtInterviewerRow.Item("interviewtypeunkid") = CInt(cboiInterViewType.SelectedValue)
            dtInterviewerRow.Item("userunkid") = User._Object._Userunkid
            dtInterviewerRow.Item("isvoid") = False
            dtInterviewerRow.Item("voiduserunkid") = -1
            dtInterviewerRow.Item("voidreason") = ""
            dtInterviewerRow.Item("GUID") = Guid.NewGuid().ToString
            dtInterviewerRow.Item("AUD") = "A"

            mdtInterviewerTran.Rows.Add(dtInterviewerRow)

            Call FillInterviewersList()
            Call ResetInterviewer()
            cboiInterViewType.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvInterviewer.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 16, "Please select Interviewer from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvInterviewer.Select()
            Exit Sub
        End If
        If CInt(cboiInterViewType.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 12, "Please select Interview Type."), enMsgBoxStyle.Information)
            cboiInterViewType.Focus()
            Exit Sub
        ElseIf radEmployee.Checked = True AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 13, "Please select Employee."), enMsgBoxStyle.Information)
            cboEmployee.Focus()
            Exit Sub
        ElseIf radOthers.Checked = True AndAlso txtOtherInterviewerName.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 14, "Please Enter Interviewer Name."), enMsgBoxStyle.Information)
            txtOtherInterviewerName.Focus()
            Exit Sub
        End If
        Try


            If lvInterviewer.SelectedItems.Count > 0 Then
                If mintInterviewerIndex > -1 Then
                    Dim drtemp As DataRow()
                    If CInt(lvInterviewer.Items(mintInterviewerIndex).Tag) = -1 Then
                        drtemp = mdtInterviewerTran.Select("GUID ='" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "'")
                    Else
                        drtemp = mdtInterviewerTran.Select("batchscheduleinterviewertranunkid = " & CInt(lvInterviewer.SelectedItems(0).Tag) & "")
                    End If

                    'Hemant (30 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members. When adding the interview detail, the level should not be mandatory. User should be able to add the employee without selecting the level.)
                    'Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = " & CInt(nudLevel.Value) & " AND batchscheduleinterviewertranunkid <> '" & CInt(lvInterviewer.SelectedItems(0).Tag) & "' AND AUD <> 'D'")

                    'If dtLevelRow.Length > 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 15, "Selected Level is already assigned to Interviewer. Please assign new level."), enMsgBoxStyle.Information)
                    '    nudLevel.Focus()
                    '    Exit Sub
                    'End If
                    'Hemant (30 Oct 2019) -- End


                    If drtemp.Length > 0 Then
                        With drtemp(0)
                            .Item("batchscheduleinterviewertranunkid") = lvInterviewer.Items(mintInterviewerIndex).Tag
                            .Item("BatchScheduleunkid") = mintBatchScheduleunkid
                            .Item("vacancyunkid") = CInt(cboVacancy.SelectedValue)
                            .Item("interviewer_level") = nudLevel.Value
                            .Item("interviewtypeunkid") = CInt(cboiInterViewType.SelectedValue)
                            If radEmployee.Checked = True Then
                                .Item("interviewerunkid") = CInt(cboEmployee.SelectedValue)
                            Else
                                .Item("interviewerunkid") = CInt(-1)
                                .Item("otherinterviewer_name") = txtOtherInterviewerName.Text
                                .Item("othercompany") = txtOtherCompany.Text
                                .Item("otherdepartment") = txtOtherDepartment.Text
                                .Item("othercontact_no") = txtOtherContactNo.Text
                            End If
                            .Item("userunkid") = 1
                            .Item("GUID") = Guid.NewGuid().ToString
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillInterviewersList()
                    End If

                End If
                Call ResetInterviewer()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrmodulename)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvInterviewer.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 16, "Please select Interviewer from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvInterviewer.Select()
            Exit Sub
        End If
        Try

            If mintInterviewerIndex > -1 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 17, "Are you sure you want to delete this Interviewer?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If

                'Hemant (03 Jun 2020) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 3 : A Interview Score report showing interview candidate(s) and the scores of each interviewer to be developed)
                Dim mstrMessage As String = ""
                If objInterviewerTran.isUsed(CInt(lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewerunkid.Index).Text), mintBatchScheduleunkid, lvInterviewer.SelectedItems(0).SubItems(colhEmpName.Index).Text) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrmodulename, 18, "Sorry, You cannot delete this Interviewer. Reason : This Interviewer is already linked with some of the Transactions.", ))
                    Exit Sub
                End If
                'Hemant (03 Jun 2020) -- End          


                Dim drTemp As DataRow()
                If CInt(lvInterviewer.Items(mintInterviewerIndex).Tag) = -1 Then
                    drTemp = mdtInterviewerTran.Select("GUID = '" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "'")
                Else
                    drTemp = mdtInterviewerTran.Select("batchscheduleinterviewertranunkid = " & CInt(lvInterviewer.SelectedItems(0).Tag) & "")
                End If

                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    Call FillInterviewersList()
                    Call ResetInterviewer()
                End If
            End If
            If lvInterviewer.Items.Count > 11 Then
                colhEmpName.Width = 110 - 18
            Else
                colhEmpName.Width = 110
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrmodulename)
        End Try

    End Sub

    Private Sub objbtnAddInterviewType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInterviewType.Click
        Dim objfrm As New frmCommonMaster
        Dim objCommonMaster As New clsCommon_Master
        Dim dsList As DataSet
        Dim intRefId As Integer = -1
        Try

            objfrm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, enAction.ADD_ONE)

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType", -1)
            With cboiInterViewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("InterviewType")
                .SelectedValue = intRefId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddInterviewType_Click", mstrModuleName)
        End Try

    End Sub
    'Hemant (27 Sep 2019) -- End
#End Region

#End Region

#Region " Other Events "
    'Hemant (27 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
    Private Sub lvtInterviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInterviewer.Click

        Try
            If lvInterviewer.SelectedItems.Count > 0 Then
                mintInterviewerIndex = lvInterviewer.SelectedItems(0).Index

                nudLevel.Value = CDec(lvInterviewer.SelectedItems(0).SubItems(colhLevel.Index).Text)

                If CDec(lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewerunkid.Index).Text) <> -1 Then
                    radEmployee.Checked = True
                    radOthers.Checked = False

                    cboEmployee.SelectedValue = lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewerunkid.Index).Text
                    lblEmployeeCompany.Text = lvInterviewer.SelectedItems(0).SubItems(colhCompany.Index).Text
                    lblEmployeeDepartment.Text = lvInterviewer.SelectedItems(0).SubItems(colhDepartment.Index).Text
                    lblEmployeeContactNo.Text = lvInterviewer.SelectedItems(0).SubItems(colhContactNo.Index).Text
                Else
                    radEmployee.Checked = False
                    radOthers.Checked = True
                    txtOtherCompany.Text = lvInterviewer.SelectedItems(0).SubItems(colhCompany.Index).Text
                    txtOtherContactNo.Text = lvInterviewer.SelectedItems(0).SubItems(colhContactNo.Index).Text
                    txtOtherDepartment.Text = lvInterviewer.SelectedItems(0).SubItems(colhDepartment.Index).Text
                    txtOtherInterviewerName.Text = lvInterviewer.SelectedItems(0).SubItems(colhEmpName.Index).Text

                End If
                cboiInterViewType.SelectedValue = lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewtypeunkid.Index).Text
            Else
                mintInterviewerIndex = -1
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvtInterviewer_Click", mstrmodulename)
        End Try

    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet
        Try

            objEmployeeData = New clsEmployee_Master
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            dsList = objEmployeeData.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)


            With cboEmployee
                objfrm.DataSource = dsList.Tables("List")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployeeData = Nothing

        End Try
    End Sub
    'Hemant (27 Sep 2019) -- End

    'Hemant (30 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : On batch scheduling form, provide smart search on the job/vacancy.
    Private Sub EZeeGradientButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboVacancy.SelectedValue = objFrm.SelectedValue
                cboVacancy.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrmodulename)
        End Try
    End Sub
    'Hemant (30 Mar 2022) -- End


#End Region

#Region " ComboBox's Events "
    'Hemant (27 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
        Dim objEmp As New clsEmployee_Master
        Dim objDept As New clsDepartment
        Try
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            objDept._Departmentunkid = objEmp._Departmentunkid
            lblEmployeeDepartment.Text = objDept._Name
            lblEmployeeCompany.Text = Company._Object._Name
            lblEmployeeContactNo.Text = objEmp._Present_Tel_No
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrmodulename)
        Finally
            objEmp = Nothing
            objDept = Nothing
        End Try
    End Sub
    'Hemant (27 Sep 2019) -- End

    
#End Region

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            If CInt(cboVacancy.SelectedValue) > 0 Then
                'S.SANDEEP [ 06 SEP 2011 ] -- START
                objVacancyData._Vacancyunkid = CInt(cboVacancy.SelectedValue)
                objlblCaption.Text = Language.getMessage(mstrmodulename, 7, "Vacancy Date From : ") & objVacancyData._Interview_Startdate.ToShortDateString & " " & _
                                     Language.getMessage(mstrmodulename, 8, "To") & " " & objVacancyData._Interview_Closedate.ToShortDateString
                'S.SANDEEP [ 06 SEP 2011 ] -- END 

                Dim objIntViewTrn As New clsinterviewer_tran
                Dim dsList As New DataSet
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
                'dsList = objIntViewTrn.getInterviewTypeCombo(CInt(cboVacancy.SelectedValue), "IntvType", True)
                'With cboInterviewType
                '    .ValueMember = "Id"
                '    .DisplayMember = "Name"
                '    .DataSource = dsList.Tables("IntvType")
                '    If menAction = enAction.EDIT_ONE Then
                '        .SelectedValue = objBatchSchedule._Interviewtypeunkid
                '    Else
                '        .SelectedValue = 0
                '    End If
                'End With
                Dim objInterviewType As New clsCommon_Master
                dsList = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "IntvType")
                With cboInterviewType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("IntvType")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objBatchSchedule._Interviewtypeunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                'Hemant (07 Oct 2019) -- End
                'S.SANDEEP [ 06 SEP 2011 ] -- START
            Else
                objlblCaption.Text = ""
                'S.SANDEEP [ 06 SEP 2011 ] -- END 
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrmodulename)
        End Try
    End Sub

    Private Sub objbtnAddRGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddRGroup.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.RESULT_GROUP, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "RGRP")
                With cboResultGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("RGRP")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddRGroup_Click", mstrmodulename)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 


    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                Dim dsVacList As New DataSet
                Dim objVac As New clsVacancy

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
                'Shani(24-Aug-2015) -- End

                With cboVacancy
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsVacList.Tables("List")
                    If menAction = enAction.EDIT_ONE Then
                        cboVacancy.SelectedValue = objBatchSchedule._Vacancyunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrmodulename)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbScheduleInterview.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbScheduleInterview.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnActivate.GradientBackColor = GUI._ButttonBackColor 
			Me.btnActivate.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.chkIsClosed.Text = Language._Object.getCaption(Me.chkIsClosed.Name, Me.chkIsClosed.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnActivate.Text = Language._Object.getCaption(Me.btnActivate.Name, Me.btnActivate.Text)
			Me.tabpBatchScheduleInfo.Text = Language._Object.getCaption(Me.tabpBatchScheduleInfo.Name, Me.tabpBatchScheduleInfo.Text)
			Me.tabpInterview.Text = Language._Object.getCaption(Me.tabpInterview.Name, Me.tabpInterview.Text)
			Me.gbInterviewer.Text = Language._Object.getCaption(Me.gbInterviewer.Name, Me.gbInterviewer.Text)
			Me.lblLevelInfo.Text = Language._Object.getCaption(Me.lblLevelInfo.Name, Me.lblLevelInfo.Text)
			Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
			Me.EZeeLine3.Text = Language._Object.getCaption(Me.EZeeLine3.Name, Me.EZeeLine3.Text)
			Me.lblInterViewType.Text = Language._Object.getCaption(Me.lblInterViewType.Name, Me.lblInterViewType.Text)
			Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblOtherDepartment.Text = Language._Object.getCaption(Me.lblOtherDepartment.Name, Me.lblOtherDepartment.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
			Me.lblEmployeeDepartment.Text = Language._Object.getCaption(Me.lblEmployeeDepartment.Name, Me.lblEmployeeDepartment.Text)
			Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
			Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
			Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblEmployeeContactNo.Text = Language._Object.getCaption(Me.lblEmployeeContactNo.Name, Me.lblEmployeeContactNo.Text)
			Me.lblEmployeeCompany.Text = Language._Object.getCaption(Me.lblEmployeeCompany.Name, Me.lblEmployeeCompany.Text)
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
			Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
			Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
			Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
			Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
			Me.colhInterviewType.Text = Language._Object.getCaption(CStr(Me.colhInterviewType.Tag), Me.colhInterviewType.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.gbScheduleInterview.Text = Language._Object.getCaption(Me.gbScheduleInterview.Name, Me.gbScheduleInterview.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.elBatchInformation.Text = Language._Object.getCaption(Me.elBatchInformation.Name, Me.elBatchInformation.Text)
			Me.elVacancyInfo.Text = Language._Object.getCaption(Me.elVacancyInfo.Name, Me.elVacancyInfo.Text)
			Me.tabpDescription.Text = Language._Object.getCaption(Me.tabpDescription.Name, Me.tabpDescription.Text)
			Me.tabpCancelRemark.Text = Language._Object.getCaption(Me.tabpCancelRemark.Name, Me.tabpCancelRemark.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.lblBatchCode.Text = Language._Object.getCaption(Me.lblBatchCode.Name, Me.lblBatchCode.Text)
			Me.lblLocation.Text = Language._Object.getCaption(Me.lblLocation.Name, Me.lblLocation.Text)
			Me.lblInterviewDateFrom.Text = Language._Object.getCaption(Me.lblInterviewDateFrom.Name, Me.lblInterviewDateFrom.Text)
			Me.lblInterview.Text = Language._Object.getCaption(Me.lblInterview.Name, Me.lblInterview.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrmodulename, 1, "Vacancy is mandatory information. Please select Vacancy to continue.")
			Language.setMessage(mstrmodulename, 2, "Result Code cannot be blank. Result Code is mandatory information.")
			Language.setMessage(mstrmodulename, 3, "Interview Type is mandatory information. Please select Interview Type to continue.")
			Language.setMessage(mstrmodulename, 4, "Batch Code cannot be blank. Batch Code is mandatory information.")
			Language.setMessage(mstrmodulename, 5, "Batch Name cannot be blank. Batch Name is mandatory information.")
			Language.setMessage(mstrmodulename, 6, "Please give Cancel Remark. Cancel Remark is compulsory information.")
			Language.setMessage(mstrmodulename, 7, "Vacancy Date From :")
			Language.setMessage(mstrmodulename, 8, "To")
			Language.setMessage(mstrmodulename, 9, "Vacancy Type cannot be blank. Vacancy Type is compulsory information.")
			Language.setMessage(mstrmodulename, 10, "Selected Interviewer is already added to the list.")
			Language.setMessage(mstrmodulename, 11, "Company cannot be same. Please give another company name.")
			Language.setMessage(mstrmodulename, 12, "Please select Interview Type.")
			Language.setMessage(mstrmodulename, 13, "Please select Employee.")
			Language.setMessage(mstrmodulename, 14, "Please Enter Interviewer Name.")
			Language.setMessage(mstrmodulename, 16, "Please select Interviewer from the list to perform further operation.")
			Language.setMessage(mstrmodulename, 17, "Are you sure you want to delete this Interviewer?")
			Language.setMessage(mstrmodulename, 18, "Sorry, You cannot delete this Interviewer. Reason : This Interviewer is already linked with some of the Transactions.")
			Language.setMessage(mstrmodulename, 21, "Please add atleast one Interview Detail.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class