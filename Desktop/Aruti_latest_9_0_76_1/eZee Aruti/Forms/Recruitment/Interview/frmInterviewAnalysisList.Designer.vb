﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInterviewAnalysisList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInterviewAnalysisList))
        Me.pnlTrainingAnalysisList = New System.Windows.Forms.Panel
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkNotEligible = New System.Windows.Forms.CheckBox
        Me.chkEligible = New System.Windows.Forms.CheckBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.chkIncomplete = New System.Windows.Forms.CheckBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.dtpAnalysisDateTo = New System.Windows.Forms.DateTimePicker
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.cboInterviewType = New System.Windows.Forms.ComboBox
        Me.lblBatch = New System.Windows.Forms.Label
        Me.dtpAnalysisDateFrom = New System.Windows.Forms.DateTimePicker
        Me.lbAnalysisDateTo = New System.Windows.Forms.Label
        Me.lblInterviewType = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.lblAnalysisDateFrom = New System.Windows.Forms.Label
        Me.objbtnSearchApplicant = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboBatch = New System.Windows.Forms.ComboBox
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacanyType = New System.Windows.Forms.Label
        Me.lblApplicant = New System.Windows.Forms.Label
        Me.cboApplicant = New System.Windows.Forms.ComboBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.lvAnalysisList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhIscheck = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewType = New System.Windows.Forms.ColumnHeader
        Me.colhAnalysisDate = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhReviewer = New System.Windows.Forms.ColumnHeader
        Me.colhScore = New System.Windows.Forms.ColumnHeader
        Me.objcolhAppbatchscheduletranunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhanalysistranunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhiseligible = New System.Windows.Forms.ColumnHeader
        Me.objcolhVacancyunkid = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.colhApplicant = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuMarkAs = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSubmitForApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuApproveDisapprove = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEligible = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNotEligible = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuViewApplicants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAllEligibleApplicants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEligibleApplicantsPending = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEligibleApplicantsApproved = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEligibleApplicantsNotApproved = New System.Windows.Forms.ToolStripMenuItem
        Me.lblColor = New System.Windows.Forms.Label
        Me.btnFinalEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEligible = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhInterviewType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAnalysisDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReviewer = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScore = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAppBatchTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAnalysisId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhVacancyId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApplicantId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSortId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlTrainingAnalysisList.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTrainingAnalysisList
        '
        Me.pnlTrainingAnalysisList.Controls.Add(Me.picStayView)
        Me.pnlTrainingAnalysisList.Controls.Add(Me.objchkAll)
        Me.pnlTrainingAnalysisList.Controls.Add(Me.dgvData)
        Me.pnlTrainingAnalysisList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlTrainingAnalysisList.Controls.Add(Me.eZeeHeader)
        Me.pnlTrainingAnalysisList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTrainingAnalysisList.Location = New System.Drawing.Point(0, 0)
        Me.pnlTrainingAnalysisList.Name = "pnlTrainingAnalysisList"
        Me.pnlTrainingAnalysisList.Size = New System.Drawing.Size(796, 459)
        Me.pnlTrainingAnalysisList.TabIndex = 0
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(743, 364)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 42
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(41, 186)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 40
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhCheck, Me.dgcolhInterviewType, Me.dgcolhAnalysisDate, Me.dgcolhReviewer, Me.dgcolhScore, Me.dgcolhRemark, Me.objdgcolhAppBatchTranId, Me.objdgcolhAnalysisId, Me.objdgcolhVacancyId, Me.objdgcolhApplicantId, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhSortId})
        Me.dgvData.Location = New System.Drawing.Point(9, 180)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(778, 220)
        Me.dgvData.TabIndex = 41
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkNotEligible)
        Me.gbFilterCriteria.Controls.Add(Me.chkEligible)
        Me.gbFilterCriteria.Controls.Add(Me.lblResult)
        Me.gbFilterCriteria.Controls.Add(Me.cboResult)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncomplete)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.chkComplete)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAnalysisDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatch)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAnalysisDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lbAnalysisDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.lblAnalysisDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBatch)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacanyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.cboApplicant)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 63)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 92
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(778, 114)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNotEligible
        '
        Me.chkNotEligible.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNotEligible.Location = New System.Drawing.Point(614, 89)
        Me.chkNotEligible.Name = "chkNotEligible"
        Me.chkNotEligible.Size = New System.Drawing.Size(143, 17)
        Me.chkNotEligible.TabIndex = 54
        Me.chkNotEligible.Text = "Not Eligible"
        Me.chkNotEligible.UseVisualStyleBackColor = True
        '
        'chkEligible
        '
        Me.chkEligible.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEligible.Location = New System.Drawing.Point(502, 89)
        Me.chkEligible.Name = "chkEligible"
        Me.chkEligible.Size = New System.Drawing.Size(106, 17)
        Me.chkEligible.TabIndex = 53
        Me.chkEligible.Text = "Eligible"
        Me.chkEligible.UseVisualStyleBackColor = True
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(8, 62)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(78, 16)
        Me.lblResult.TabIndex = 50
        Me.lblResult.Text = "Result"
        Me.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.DropDownWidth = 300
        Me.cboResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(92, 60)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(107, 21)
        Me.cboResult.TabIndex = 51
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(500, 33)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 49
        '
        'chkIncomplete
        '
        Me.chkIncomplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncomplete.Location = New System.Drawing.Point(3, 313)
        Me.chkIncomplete.Name = "chkIncomplete"
        Me.chkIncomplete.Size = New System.Drawing.Size(15, 17)
        Me.chkIncomplete.TabIndex = 42
        Me.chkIncomplete.Text = "Incomplete"
        Me.chkIncomplete.UseVisualStyleBackColor = True
        Me.chkIncomplete.Visible = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(752, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 32
        Me.objbtnReset.TabStop = False
        '
        'chkComplete
        '
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.Location = New System.Drawing.Point(3, 312)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(15, 17)
        Me.chkComplete.TabIndex = 41
        Me.chkComplete.Text = "Complete"
        Me.chkComplete.UseVisualStyleBackColor = True
        Me.chkComplete.Visible = False
        '
        'dtpAnalysisDateTo
        '
        Me.dtpAnalysisDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDateTo.Checked = False
        Me.dtpAnalysisDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAnalysisDateTo.Location = New System.Drawing.Point(292, 87)
        Me.dtpAnalysisDateTo.Name = "dtpAnalysisDateTo"
        Me.dtpAnalysisDateTo.ShowCheckBox = True
        Me.dtpAnalysisDateTo.Size = New System.Drawing.Size(107, 21)
        Me.dtpAnalysisDateTo.TabIndex = 29
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 360
        Me.cboVacancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(292, 33)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(202, 21)
        Me.cboVacancy.TabIndex = 44
        '
        'cboInterviewType
        '
        Me.cboInterviewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewType.FormattingEnabled = True
        Me.cboInterviewType.Location = New System.Drawing.Point(614, 60)
        Me.cboInterviewType.Name = "cboInterviewType"
        Me.cboInterviewType.Size = New System.Drawing.Size(150, 21)
        Me.cboInterviewType.TabIndex = 18
        '
        'lblBatch
        '
        Me.lblBatch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatch.Location = New System.Drawing.Point(527, 35)
        Me.lblBatch.Name = "lblBatch"
        Me.lblBatch.Size = New System.Drawing.Size(81, 16)
        Me.lblBatch.TabIndex = 47
        Me.lblBatch.Text = "Batch"
        Me.lblBatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAnalysisDateFrom
        '
        Me.dtpAnalysisDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDateFrom.Checked = False
        Me.dtpAnalysisDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAnalysisDateFrom.Location = New System.Drawing.Point(92, 87)
        Me.dtpAnalysisDateFrom.Name = "dtpAnalysisDateFrom"
        Me.dtpAnalysisDateFrom.ShowCheckBox = True
        Me.dtpAnalysisDateFrom.Size = New System.Drawing.Size(107, 21)
        Me.dtpAnalysisDateFrom.TabIndex = 28
        '
        'lbAnalysisDateTo
        '
        Me.lbAnalysisDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbAnalysisDateTo.Location = New System.Drawing.Point(205, 89)
        Me.lbAnalysisDateTo.Name = "lbAnalysisDateTo"
        Me.lbAnalysisDateTo.Size = New System.Drawing.Size(81, 16)
        Me.lbAnalysisDateTo.TabIndex = 27
        Me.lbAnalysisDateTo.Text = "To"
        '
        'lblInterviewType
        '
        Me.lblInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewType.Location = New System.Drawing.Point(527, 62)
        Me.lblInterviewType.Name = "lblInterviewType"
        Me.lblInterviewType.Size = New System.Drawing.Size(81, 16)
        Me.lblInterviewType.TabIndex = 17
        Me.lblInterviewType.Text = "Interivew Type"
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(205, 35)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(81, 16)
        Me.lblVacancy.TabIndex = 43
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAnalysisDateFrom
        '
        Me.lblAnalysisDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnalysisDateFrom.Location = New System.Drawing.Point(8, 89)
        Me.lblAnalysisDateFrom.Name = "lblAnalysisDateFrom"
        Me.lblAnalysisDateFrom.Size = New System.Drawing.Size(81, 16)
        Me.lblAnalysisDateFrom.TabIndex = 26
        Me.lblAnalysisDateFrom.Text = "Analysis Date"
        '
        'objbtnSearchApplicant
        '
        Me.objbtnSearchApplicant.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApplicant.BorderSelected = False
        Me.objbtnSearchApplicant.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApplicant.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApplicant.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApplicant.Location = New System.Drawing.Point(500, 60)
        Me.objbtnSearchApplicant.Name = "objbtnSearchApplicant"
        Me.objbtnSearchApplicant.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApplicant.TabIndex = 38
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(729, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 31
        Me.objbtnSearch.TabStop = False
        '
        'cboBatch
        '
        Me.cboBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatch.DropDownWidth = 300
        Me.cboBatch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatch.FormattingEnabled = True
        Me.cboBatch.Location = New System.Drawing.Point(614, 33)
        Me.cboBatch.Name = "cboBatch"
        Me.cboBatch.Size = New System.Drawing.Size(150, 21)
        Me.cboBatch.TabIndex = 48
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 300
        Me.cboVacancyType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(92, 33)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(107, 21)
        Me.cboVacancyType.TabIndex = 46
        '
        'lblVacanyType
        '
        Me.lblVacanyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacanyType.Location = New System.Drawing.Point(8, 35)
        Me.lblVacanyType.Name = "lblVacanyType"
        Me.lblVacanyType.Size = New System.Drawing.Size(78, 16)
        Me.lblVacanyType.TabIndex = 45
        Me.lblVacanyType.Text = "Vacancy Type"
        Me.lblVacanyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApplicant
        '
        Me.lblApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicant.Location = New System.Drawing.Point(205, 62)
        Me.lblApplicant.Name = "lblApplicant"
        Me.lblApplicant.Size = New System.Drawing.Size(81, 16)
        Me.lblApplicant.TabIndex = 14
        Me.lblApplicant.Text = "Applicant"
        '
        'cboApplicant
        '
        Me.cboApplicant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicant.FormattingEnabled = True
        Me.cboApplicant.Location = New System.Drawing.Point(292, 60)
        Me.cboApplicant.Name = "cboApplicant"
        Me.cboApplicant.Size = New System.Drawing.Size(202, 21)
        Me.cboApplicant.TabIndex = 16
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(796, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Interview Analysis List"
        '
        'lvAnalysisList
        '
        Me.lvAnalysisList.BackColorOnChecked = True
        Me.lvAnalysisList.CheckBoxes = True
        Me.lvAnalysisList.ColumnHeaders = Nothing
        Me.lvAnalysisList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhIscheck, Me.colhInterviewType, Me.colhAnalysisDate, Me.colhStatus, Me.colhReviewer, Me.colhScore, Me.objcolhAppbatchscheduletranunkid, Me.objcolhanalysistranunkid, Me.objcolhiseligible, Me.objcolhVacancyunkid, Me.colhRemark, Me.colhApplicant})
        Me.lvAnalysisList.CompulsoryColumns = ""
        Me.lvAnalysisList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAnalysisList.FullRowSelect = True
        Me.lvAnalysisList.GridLines = True
        Me.lvAnalysisList.GroupingColumn = Nothing
        Me.lvAnalysisList.HideSelection = False
        Me.lvAnalysisList.Location = New System.Drawing.Point(120, 13)
        Me.lvAnalysisList.MinColumnWidth = 50
        Me.lvAnalysisList.MultiSelect = False
        Me.lvAnalysisList.Name = "lvAnalysisList"
        Me.lvAnalysisList.OptionalColumns = ""
        Me.lvAnalysisList.ShowMoreItem = False
        Me.lvAnalysisList.ShowSaveItem = False
        Me.lvAnalysisList.ShowSelectAll = True
        Me.lvAnalysisList.ShowSizeAllColumnsToFit = True
        Me.lvAnalysisList.Size = New System.Drawing.Size(175, 30)
        Me.lvAnalysisList.Sortable = True
        Me.lvAnalysisList.TabIndex = 4
        Me.lvAnalysisList.UseCompatibleStateImageBehavior = False
        Me.lvAnalysisList.View = System.Windows.Forms.View.Details
        Me.lvAnalysisList.Visible = False
        '
        'objcolhIscheck
        '
        Me.objcolhIscheck.Tag = "objcolhIscheck"
        Me.objcolhIscheck.Text = ""
        Me.objcolhIscheck.Width = 25
        '
        'colhInterviewType
        '
        Me.colhInterviewType.Tag = "colhInterviewType"
        Me.colhInterviewType.Text = "Interview Type"
        Me.colhInterviewType.Width = 150
        '
        'colhAnalysisDate
        '
        Me.colhAnalysisDate.Tag = "colhAnalysisDate"
        Me.colhAnalysisDate.Text = "Analysis Date"
        Me.colhAnalysisDate.Width = 100
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 120
        '
        'colhReviewer
        '
        Me.colhReviewer.Tag = "colhReviewer"
        Me.colhReviewer.Text = "Reviewer"
        Me.colhReviewer.Width = 230
        '
        'colhScore
        '
        Me.colhScore.Tag = "colhScore"
        Me.colhScore.Text = "Score"
        Me.colhScore.Width = 100
        '
        'objcolhAppbatchscheduletranunkid
        '
        Me.objcolhAppbatchscheduletranunkid.Tag = "objcolhAppbatchscheduletranunkid"
        Me.objcolhAppbatchscheduletranunkid.Text = "Appbatchscheduletranunkid"
        Me.objcolhAppbatchscheduletranunkid.Width = 0
        '
        'objcolhanalysistranunkid
        '
        Me.objcolhanalysistranunkid.Tag = "objcolhanalysistranunkid"
        Me.objcolhanalysistranunkid.Text = "objcolhanalysistranunkid"
        Me.objcolhanalysistranunkid.Width = 0
        '
        'objcolhiseligible
        '
        Me.objcolhiseligible.Text = "objcolhiseligible"
        Me.objcolhiseligible.Width = 0
        '
        'objcolhVacancyunkid
        '
        Me.objcolhVacancyunkid.Tag = "objcolhVacancyunkid"
        Me.objcolhVacancyunkid.Text = "Vacancyunkid"
        Me.objcolhVacancyunkid.Width = 0
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 200
        '
        'colhApplicant
        '
        Me.colhApplicant.Tag = "colhApplicant"
        Me.colhApplicant.Text = "Applicant"
        Me.colhApplicant.Width = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.lblColor)
        Me.objFooter.Controls.Add(Me.btnFinalEvaluation)
        Me.objFooter.Controls.Add(Me.lvAnalysisList)
        Me.objFooter.Controls.Add(Me.btnEligible)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 404)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(796, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(102, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 161
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMarkAs, Me.mnuSubmitForApproval, Me.objSep1, Me.mnuApproveDisapprove, Me.objSep2, Me.mnuViewApplicants})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(204, 104)
        '
        'mnuMarkAs
        '
        Me.mnuMarkAs.Name = "mnuMarkAs"
        Me.mnuMarkAs.Size = New System.Drawing.Size(203, 22)
        Me.mnuMarkAs.Tag = "mnuMarkAs"
        Me.mnuMarkAs.Text = "Set E&ligible / Not Eligible"
        '
        'mnuSubmitForApproval
        '
        Me.mnuSubmitForApproval.Name = "mnuSubmitForApproval"
        Me.mnuSubmitForApproval.Size = New System.Drawing.Size(203, 22)
        Me.mnuSubmitForApproval.Tag = "mnuSubmitForApproval"
        Me.mnuSubmitForApproval.Text = "&Submit For Approval"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(200, 6)
        Me.objSep1.Tag = "objSep1"
        '
        'mnuApproveDisapprove
        '
        Me.mnuApproveDisapprove.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEligible, Me.mnuNotEligible})
        Me.mnuApproveDisapprove.Name = "mnuApproveDisapprove"
        Me.mnuApproveDisapprove.Size = New System.Drawing.Size(203, 22)
        Me.mnuApproveDisapprove.Tag = "mnuApproveDisapprove"
        Me.mnuApproveDisapprove.Text = "&Approve / Disapprove"
        '
        'mnuEligible
        '
        Me.mnuEligible.Name = "mnuEligible"
        Me.mnuEligible.Size = New System.Drawing.Size(135, 22)
        Me.mnuEligible.Tag = "mnuEligible"
        Me.mnuEligible.Text = "Eligible"
        '
        'mnuNotEligible
        '
        Me.mnuNotEligible.Name = "mnuNotEligible"
        Me.mnuNotEligible.Size = New System.Drawing.Size(135, 22)
        Me.mnuNotEligible.Tag = "mnuNotEligible"
        Me.mnuNotEligible.Text = "Not Eligible"
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(200, 6)
        Me.objSep2.Tag = "objSep2"
        '
        'mnuViewApplicants
        '
        Me.mnuViewApplicants.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAllEligibleApplicants, Me.mnuEligibleApplicantsPending, Me.mnuEligibleApplicantsApproved, Me.mnuEligibleApplicantsNotApproved})
        Me.mnuViewApplicants.Name = "mnuViewApplicants"
        Me.mnuViewApplicants.Size = New System.Drawing.Size(203, 22)
        Me.mnuViewApplicants.Tag = "mnuViewApplicants"
        Me.mnuViewApplicants.Text = "&View Applicants"
        '
        'mnuAllEligibleApplicants
        '
        Me.mnuAllEligibleApplicants.Name = "mnuAllEligibleApplicants"
        Me.mnuAllEligibleApplicants.Size = New System.Drawing.Size(257, 22)
        Me.mnuAllEligibleApplicants.Tag = "mnuAllEligibleApplicants"
        Me.mnuAllEligibleApplicants.Text = "All Eligible Applicants"
        '
        'mnuEligibleApplicantsPending
        '
        Me.mnuEligibleApplicantsPending.Name = "mnuEligibleApplicantsPending"
        Me.mnuEligibleApplicantsPending.Size = New System.Drawing.Size(257, 22)
        Me.mnuEligibleApplicantsPending.Text = "Eligible Applicants - Pending"
        '
        'mnuEligibleApplicantsApproved
        '
        Me.mnuEligibleApplicantsApproved.Name = "mnuEligibleApplicantsApproved"
        Me.mnuEligibleApplicantsApproved.Size = New System.Drawing.Size(257, 22)
        Me.mnuEligibleApplicantsApproved.Tag = "mnuEligibleApplicantsApproved"
        Me.mnuEligibleApplicantsApproved.Text = "Eligible Applicants - Approved"
        '
        'mnuEligibleApplicantsNotApproved
        '
        Me.mnuEligibleApplicantsNotApproved.Name = "mnuEligibleApplicantsNotApproved"
        Me.mnuEligibleApplicantsNotApproved.Size = New System.Drawing.Size(257, 22)
        Me.mnuEligibleApplicantsNotApproved.Tag = "mnuEligibleApplicantsNotApproved"
        Me.mnuEligibleApplicantsNotApproved.Text = "Eligible Applicants - Not Approved"
        '
        'lblColor
        '
        Me.lblColor.BackColor = System.Drawing.Color.Magenta
        Me.lblColor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblColor.Location = New System.Drawing.Point(301, 13)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(56, 30)
        Me.lblColor.TabIndex = 160
        Me.lblColor.Text = "Final Analysis"
        Me.lblColor.Visible = False
        '
        'btnFinalEvaluation
        '
        Me.btnFinalEvaluation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinalEvaluation.BackColor = System.Drawing.Color.White
        Me.btnFinalEvaluation.BackgroundImage = CType(resources.GetObject("btnFinalEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalEvaluation.FlatAppearance.BorderSize = 0
        Me.btnFinalEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.Location = New System.Drawing.Point(363, 13)
        Me.btnFinalEvaluation.Name = "btnFinalEvaluation"
        Me.btnFinalEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.Size = New System.Drawing.Size(112, 30)
        Me.btnFinalEvaluation.TabIndex = 159
        Me.btnFinalEvaluation.Text = "&Final Evaluation"
        Me.btnFinalEvaluation.UseVisualStyleBackColor = True
        Me.btnFinalEvaluation.Visible = False
        '
        'btnEligible
        '
        Me.btnEligible.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEligible.BackColor = System.Drawing.Color.White
        Me.btnEligible.BackgroundImage = CType(resources.GetObject("btnEligible.BackgroundImage"), System.Drawing.Image)
        Me.btnEligible.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEligible.BorderColor = System.Drawing.Color.Empty
        Me.btnEligible.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEligible.FlatAppearance.BorderSize = 0
        Me.btnEligible.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEligible.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEligible.ForeColor = System.Drawing.Color.Black
        Me.btnEligible.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEligible.GradientForeColor = System.Drawing.Color.Black
        Me.btnEligible.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEligible.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEligible.Location = New System.Drawing.Point(363, 13)
        Me.btnEligible.Name = "btnEligible"
        Me.btnEligible.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEligible.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEligible.Size = New System.Drawing.Size(112, 30)
        Me.btnEligible.TabIndex = 158
        Me.btnEligible.Text = "&Mark As Eligible"
        Me.btnEligible.UseVisualStyleBackColor = True
        Me.btnEligible.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(687, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 157
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(584, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 156
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(481, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 155
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Interview Type"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "AnalysisDate"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Reviewer"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Score"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhAppBatchTranId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhAnalysisId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhVacancyId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhApplicantId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhSortId"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhInterviewType
        '
        Me.dgcolhInterviewType.HeaderText = "Interview Type"
        Me.dgcolhInterviewType.Name = "dgcolhInterviewType"
        Me.dgcolhInterviewType.ReadOnly = True
        Me.dgcolhInterviewType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInterviewType.Width = 246
        '
        'dgcolhAnalysisDate
        '
        Me.dgcolhAnalysisDate.HeaderText = "Analysis Date"
        Me.dgcolhAnalysisDate.Name = "dgcolhAnalysisDate"
        Me.dgcolhAnalysisDate.ReadOnly = True
        Me.dgcolhAnalysisDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhReviewer
        '
        Me.dgcolhReviewer.HeaderText = "Reviewer"
        Me.dgcolhReviewer.Name = "dgcolhReviewer"
        Me.dgcolhReviewer.ReadOnly = True
        Me.dgcolhReviewer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhReviewer.Width = 150
        '
        'dgcolhScore
        '
        Me.dgcolhScore.HeaderText = "Score"
        Me.dgcolhScore.Name = "dgcolhScore"
        Me.dgcolhScore.ReadOnly = True
        Me.dgcolhScore.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhScore.Width = 80
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRemark.Width = 150
        '
        'objdgcolhAppBatchTranId
        '
        Me.objdgcolhAppBatchTranId.HeaderText = "objdgcolhAppBatchTranId"
        Me.objdgcolhAppBatchTranId.Name = "objdgcolhAppBatchTranId"
        Me.objdgcolhAppBatchTranId.Visible = False
        '
        'objdgcolhAnalysisId
        '
        Me.objdgcolhAnalysisId.HeaderText = "objdgcolhAnalysisId"
        Me.objdgcolhAnalysisId.Name = "objdgcolhAnalysisId"
        Me.objdgcolhAnalysisId.Visible = False
        '
        'objdgcolhVacancyId
        '
        Me.objdgcolhVacancyId.HeaderText = "objdgcolhVacancyId"
        Me.objdgcolhVacancyId.Name = "objdgcolhVacancyId"
        Me.objdgcolhVacancyId.Visible = False
        '
        'objdgcolhApplicantId
        '
        Me.objdgcolhApplicantId.HeaderText = "objdgcolhApplicantId"
        Me.objdgcolhApplicantId.Name = "objdgcolhApplicantId"
        Me.objdgcolhApplicantId.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhSortId
        '
        Me.objdgcolhSortId.HeaderText = "objdgcolhSortId"
        Me.objdgcolhSortId.Name = "objdgcolhSortId"
        Me.objdgcolhSortId.Visible = False
        '
        'frmInterviewAnalysisList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(796, 459)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlTrainingAnalysisList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInterviewAnalysisList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Interview Analysis"
        Me.pnlTrainingAnalysisList.ResumeLayout(False)
        Me.pnlTrainingAnalysisList.PerformLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTrainingAnalysisList As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboApplicant As System.Windows.Forms.ComboBox
    Friend WithEvents lblApplicant As System.Windows.Forms.Label
    Friend WithEvents cboInterviewType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterviewType As System.Windows.Forms.Label
    Friend WithEvents dtpAnalysisDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpAnalysisDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbAnalysisDateTo As System.Windows.Forms.Label
    Friend WithEvents lblAnalysisDateFrom As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnFinalEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnEligible As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchApplicant As eZee.Common.eZeeGradientButton
    Friend WithEvents chkIncomplete As System.Windows.Forms.CheckBox
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents lvAnalysisList As eZee.Common.eZeeListView
    Friend WithEvents colhApplicant As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterviewType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAnalysisDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhReviewer As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScore As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAppbatchscheduletranunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhanalysistranunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhiseligible As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents objcolhVacancyunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblVacanyType As System.Windows.Forms.Label
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents lblBatch As System.Windows.Forms.Label
    Friend WithEvents cboBatch As System.Windows.Forms.ComboBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkNotEligible As System.Windows.Forms.CheckBox
    Friend WithEvents chkEligible As System.Windows.Forms.CheckBox
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSubmitForApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMarkAs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuApproveDisapprove As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuViewApplicants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAllEligibleApplicants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEligibleApplicantsApproved As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEligibleApplicantsNotApproved As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhIscheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents mnuEligibleApplicantsPending As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEligible As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNotEligible As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhInterviewType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAnalysisDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReviewer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhScore As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAppBatchTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAnalysisId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhVacancyId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApplicantId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSortId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
