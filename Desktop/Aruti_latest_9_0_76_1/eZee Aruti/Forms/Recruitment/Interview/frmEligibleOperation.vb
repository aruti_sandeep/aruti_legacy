﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEligibleOperation

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEligibleOperation"
    Private mblnCancel As Boolean = True
    Private objAnalysisMaster As clsInterviewAnalysis_master
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
    'Private objInteviewer As clsinterviewer_tran
    Private objBatchScheduleInteviewer As clsBatchSchedule_interviewer_tran
    'Hemant (07 Oct 2019) -- End
    Private objAnalysisTran As clsInteviewAnalysis_tran
    Private mdtAnalysisTran As DataTable
    Private mdtTab As DataTable
    Private mintMaxInterviewerId As Integer = 0
    Private dRow() As DataRow
    Private iCnt As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dtRow() As DataRow) As Boolean
        Try
            dRow = dtRow
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEligibleOperation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEligibleOperation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAnalysisMaster = New clsInterviewAnalysis_master
        'Hemant (07 Oct 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
        'objInteviewer = New clsinterviewer_tran
        objBatchScheduleInteviewer = New clsBatchSchedule_interviewer_tran
        'Hemant (07 Oct 2019) -- End
        objAnalysisTran = New clsInteviewAnalysis_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings() 'Gajanan (27 Jun 2020)
            Call SetVisibility()
            objlblCaption.Text = Language.getMessage(mstrModuleName, 4, "Total Applicant Selected : ") & dRow.Length.ToString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEligibleOperation_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEligibleOperation_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAnalysisMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEligibleOperation_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsInterviewAnalysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsInterviewAnalysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmEligibleOperation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            radEligible.Enabled = User._Object.Privilege._AllowtoMarkApplicantEligible
            radNotEligible.Enabled = User._Object.Privilege._AllowtoMarkApplicantEligible
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            'S.SANDEEP [ 09 OCT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If radEligible.Checked = True Then
            '    Dim iTotal_Position, iTotal_Eligible As Integer
            '    iTotal_Position = 0 : iTotal_Eligible = 0
            '    objAnalysisMaster.Get_Eligible_Count(iTotal_Position, iTotal_Eligible, CInt(dRow(Me.iCnt).Item("vacancyunkid")))
            '    If iTotal_Eligible >= iTotal_Position Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot set this applicant as eligible. Reason : Position for this vacancy is exceeding the limit.") & vbCrLf & _
            '                                                                Language.getMessage(mstrModuleName, 3, "Please increase the no of position from vacancy."), enMsgBoxStyle.Information)
            '        Return False
            '    End If
            'End If
            'S.SANDEEP [ 09 OCT 2013 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Button Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'Gajanan [15-June-2020] -- Start
        'Enhancement NMB recruitment Changes Auto Approve Eligible Applicant.
        Dim sAnalysisIds As String = ""
        'Gajanan [15-June-2020] -- End

        Try
            If radEligible.Checked = False AndAlso radNotEligible.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Eligibility is mandatory information. Please mark Applicant as Eligible or Not Eligible."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You are about to perform applicant eligibility operation on selected applicant(s).") & vbCrLf & _
                                                                      Language.getMessage(mstrModuleName, 6, "This will set the common operation for all selected applicant(s).") & vbCrLf & _
                                                                      Language.getMessage(mstrModuleName, 7, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            If txtRemark.Text.Trim.Length <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You have not entered any remark. Are you sure you want to perform selected operation without remark?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            For Me.iCnt = 0 To dRow.Length - 1
                If IsValid() = False Then Exit Sub

                For Each sKeyId As String In dRow(Me.iCnt).Item("a_ids").ToString.Replace("'", "").Split(CChar(","))
                    objAnalysisMaster._Analysisunkid = CInt(sKeyId)
                    objAnalysisMaster._IsEligible = radEligible.Checked
                    objAnalysisMaster._Appbatchscheduletranunkid = objAnalysisMaster._Appbatchscheduletranunkid
                    objAnalysisMaster._Iscomplete = True

                    objAnalysisTran._AnalysisUnkid = CInt(sKeyId)
                    mdtAnalysisTran = objAnalysisTran._DataTable

                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                    'objInteviewer._VacancyUnkid = CInt(dRow(Me.iCnt).Item("vacancyunkid"))
                    'mdtTab = objInteviewer._DataTable
                    objBatchScheduleInteviewer._BatchScheduleUnkid = CInt(dRow(Me.iCnt).Item("batchscheduleunkid"))
                    mdtTab = objBatchScheduleInteviewer._DataTable
                    'Hemant (07 Oct 2019) -- End

                    Dim dTemp() As DataRow = mdtTab.Select("interviewer_level = '" & CInt(mdtTab.Compute("MAX(interviewer_level)", "")) & "'")
                    If dTemp.Length > 0 Then
                        'Hemant (07 Oct 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                        'mintMaxInterviewerId = CInt(dTemp(0).Item("interviewertranunkid"))
                        mintMaxInterviewerId = CInt(dTemp(0).Item("batchscheduleinterviewertranunkid"))
                        'Hemant (07 Oct 2019) -- End
                    End If

                    Dim dtRow As DataRow
                    If CBool(dRow(Me.iCnt).Item("iscomplete")) = False Then
                        dtRow = mdtAnalysisTran.NewRow
                        dtRow.Item("analysistranunkid") = -1
                        dtRow.Item("analysisunkid") = CInt(sKeyId)
                        dtRow.Item("interviewertranunkid") = mintMaxInterviewerId
                        dtRow.Item("analysis_date") = dtpFinalDate.Value
                        dtRow.Item("resultcodeunkid") = 0
                        dtRow.Item("remark") = txtRemark.Text
                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtAnalysisTran.Rows.Add(dtRow)
                    ElseIf CBool(dRow(Me.iCnt).Item("iscomplete")) = True Then
                        Dim dfRow() As DataRow = mdtAnalysisTran.Select("resultcodeunkid <= 0")
                        If dfRow.Length > 0 Then
                            With dfRow(0)
                                .Item("analysistranunkid") = .Item("analysistranunkid")
                                .Item("analysisunkid") = .Item("analysisunkid")
                                .Item("interviewertranunkid") = .Item("interviewertranunkid")
                                .Item("analysis_date") = dtpFinalDate.Value
                                .Item("resultcodeunkid") = 0
                                .Item("remark") = txtRemark.Text
                                .Item("AUD") = "U"
                                .Item("GUID") = Guid.NewGuid.ToString
                            End With
                            dfRow(0).AcceptChanges()
                            mdtAnalysisTran.AcceptChanges()
                        End If
                    End If


                    'Gajanan [15-June-2020] -- Start
                    'Enhancement NMB recruitment Changes Auto Approve Eligible Applicant.
                    If radEligible.Checked AndAlso User._Object.Privilege._AllowToPerformEligibleOperation Then
                        objAnalysisMaster._Issent = True
                    ElseIf radNotEligible.Checked AndAlso User._Object.Privilege._AllowToPerformNotEligibleOperation Then
                        objAnalysisMaster._Issent = True
                    End If

                    If dRow.Length > 0 Then
                        If objAnalysisMaster._Issent = False Then
                            Continue For
                        ElseIf CInt(dRow(Me.iCnt)("E_StatusId").ToString()) >= 0 AndAlso sAnalysisIds.Contains(dRow(Me.iCnt)("a_ids").ToString) = False Then
                            sAnalysisIds &= "," & dRow(Me.iCnt)("a_ids").ToString
                        End If
                    End If
                    'Gajanan [15-June-2020] -- End
                    blnFlag = objAnalysisMaster.Update(mdtAnalysisTran, True)
                Next
            Next

            If blnFlag = True Then
                'Gajanan [15-June-2020] -- Start
                'Enhancement NMB recruitment Changes Auto Approve Eligible Applicant.
                If (radEligible.Checked AndAlso User._Object.Privilege._AllowToPerformEligibleOperation) OrElse (radNotEligible.Checked AndAlso User._Object.Privilege._AllowToPerformNotEligibleOperation) Then
                    If sAnalysisIds.Length > 0 Then

                        Dim dtList As New DataTable
                        sAnalysisIds = Mid(sAnalysisIds, 2)

                        dtList = objAnalysisMaster.Get_DataTable(CInt(dRow(0)("vacancyunkid").ToString()), "List", sAnalysisIds)
                        Dim mdtTable As DataTable = New DataView(dtList, "", "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable

                        Dim mblnFlag As Boolean = False
                        If sAnalysisIds.Trim.Length > 0 Then

                            Dim dtmp() As DataRow = mdtTable.Select("is_grp = True")
                            If dtmp.Length > 0 Then
                                Dim iTotal_Position, iTotal_Eligible As Integer
                                iTotal_Position = 0 : iTotal_Eligible = 0
                                objAnalysisMaster.Get_Eligible_Count(iTotal_Position, iTotal_Eligible, CInt(dRow(0)("vacancyunkid").ToString()))
                                If iTotal_Eligible + dtmp.Length > iTotal_Position Then
                                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Total position set for the selected vacancy is/are [ ") & iTotal_Position & _
                                                       Language.getMessage(mstrModuleName, 10, " ] and total eligible application approved for this vacancy is/are [ ") & iTotal_Eligible & _
                                                       Language.getMessage(mstrModuleName, 11, " ] and total remaining positions are [ ") & (iTotal_Position - iTotal_Eligible) & _
                                                       Language.getMessage(mstrModuleName, 12, " ].") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 13, "You are trying to approve applicant more than the position set for this vacancy.") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 14, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                        Exit Sub
                                    End If
                                End If
                            End If

                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You are about to approve all eligible applicant. Due to this all approved applicant will be available for import applicant as an employee.") & vbCrLf & _
                                                                   Language.getMessage(mstrModuleName, 14, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                For Each sKeyId As String In sAnalysisIds.ToString.Replace("'", "").Split(CChar(","))
                                    objAnalysisMaster._Analysisunkid = CInt(sKeyId)
                                    objAnalysisMaster._Appr_Date = dtpFinalDate.Value
                                    objAnalysisMaster._Appr_Remark = Language.getMessage(mstrModuleName, 16, "Auto Approve")
                                    objAnalysisMaster._Approveuserunkid = User._Object._Userunkid
                                    objAnalysisMaster._Statustypid = enShortListing_Status.SC_APPROVED
                                    If objAnalysisMaster.Update() = True Then
                                        mblnFlag = True
                                    Else
                                        mblnFlag = False
                                    End If
                                Next
                            End If
                            If mblnFlag = False Then
                                eZeeMsgBox.Show(objAnalysisMaster._Message.ToString(), enMsgBoxStyle.Information)
                                mblnCancel = False
                                Me.Close()
                            End If
                        End If
                    End If
                    'Gajanan [15-June-2020] -- Start
                    'Enhancement NMB recruitment Changes Auto Approve Eligible Applicant.
                End If

                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEvaluationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEvaluationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbEvaluationInfo.Text = Language._Object.getCaption(Me.gbEvaluationInfo.Name, Me.gbEvaluationInfo.Text)
			Me.radNotEligible.Text = Language._Object.getCaption(Me.radNotEligible.Name, Me.radNotEligible.Text)
			Me.radEligible.Text = Language._Object.getCaption(Me.radEligible.Name, Me.radEligible.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Eligibility is mandatory information. Please mark Applicant as Eligible or Not Eligible.")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot set this applicant as eligible. Reason : Position for this vacancy is exceeding the limit.")
			Language.setMessage(mstrModuleName, 3, "Please increase the no of position from vacancy.")
			Language.setMessage(mstrModuleName, 4, "Total Applicant Selected :")
			Language.setMessage(mstrModuleName, 5, "You are about to perform applicant eligibility operation on selected applicant(s).")
			Language.setMessage(mstrModuleName, 6, "This will set the common operation for all selected applicant(s).")
			Language.setMessage(mstrModuleName, 7, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 8, "You have not entered any remark. Are you sure you want to perform selected operation without remark?")
			Language.setMessage(mstrModuleName, 9, "Total position set for the selected vacancy is/are [")
			Language.setMessage(mstrModuleName, 10, " ] and total eligible application approved for this vacancy is/are [")
			Language.setMessage(mstrModuleName, 11, " ] and total remaining positions are [")
			Language.setMessage(mstrModuleName, 12, " ].")
			Language.setMessage(mstrModuleName, 13, "You are trying to approve applicant more than the position set for this vacancy.")
			Language.setMessage(mstrModuleName, 14, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 15, "You are about to approve all eligible applicant. Due to this all approved applicant will be available for import applicant as an employee.")
			Language.setMessage(mstrModuleName, 16, "Auto Approve")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class