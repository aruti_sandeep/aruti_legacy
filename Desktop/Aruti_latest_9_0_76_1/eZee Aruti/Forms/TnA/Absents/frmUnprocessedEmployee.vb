﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUnprocessedEmployee

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmUnprocessedEmployee"
    Private mblnCancel As Boolean = True
    Private objabsent As clsemployee_absent
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
    Private mintPeriodID As Integer = 0
    Private mdtEmployee As DataTable
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intPeriodId As Integer, ByVal dtEmployee As DataTable, ByVal mstrPeriod As String, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date) As Boolean
        Try
            mdtPeriodStartDate = mdtStartDate
            mdtPeriodEndDate = mdtEndDate
            mdtEmployee = dtEmployee
            objLblPeriodName.Text = mstrPeriod
            objlblPeriodduration.Text = mdtStartDate.ToShortDateString & " To " & mdtEndDate.ToShortDateString
            mintPeriodID = intPeriodId
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmUnprocessedEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objabsent = New clsemployee_absent
            dgUnprocessedEmployee.AutoGenerateColumns = False
            dgcolhEmpCode.DataPropertyName = "Employeecode"
            dgcolhEmployee.DataPropertyName = "Employee"
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            dgUnprocessedEmployee.DataSource = mdtEmployee
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUnprocessedEmployee_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_absent.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_absent"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            If dgUnprocessedEmployee.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to Unprocessed Employee List."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            Dim dtTable As DataTable = CType(dgUnprocessedEmployee.DataSource, DataTable)
            If objabsent.ProcessForUnprocessedEmp(mdtEmployee, mintPeriodID, mdtPeriodEndDate) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Absent Process is successfully done."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(objabsent._Message, enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region


 
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnProcess.GradientBackColor = GUI._ButttonBackColor 
			Me.btnProcess.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
			Me.lblPeriodDuration.Text = Language._Object.getCaption(Me.lblPeriodDuration.Name, Me.lblPeriodDuration.Text)
			Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "There is no data to Unprocessed Employee List.")
			Language.setMessage(mstrModuleName, 2, "Employee Absent Process is successfully done.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class