﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmRecalculateTimings

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmRecalculateTimings"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtEmployee As DataTable
    Private dgView As DataView
    Private mstrAllocationFilter As String = String.Empty
    Private mblnRecaculate As Boolean = True
    'S.SANDEEP [ 17 JAN 2014 ] -- START
    Private mdtRange As DataTable
    'S.SANDEEP [ 17 JAN 2014 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal blnRecalculate As Boolean, ByVal mstrFormName As String) As Boolean
        Try
            mblnRecaculate = blnRecalculate
            Me.Text = mstrFormName
            menAction = eAction
            'S.SANDEEP [ 17 JAN 2014 ] -- START
            Call Set_Form_Size()
            'S.SANDEEP [ 17 JAN 2014 ] -- END
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub Fill_Employee_List()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", False, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , mstrAllocationFilter)
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", False, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , mstrAllocationFilter)
            'End If
            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", False, , , , , , , , , , , , , , , mstrAllocationFilter)

            dsList.Tables("Emp").Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False


            'Pinkal (25-AUG-2014) -- Start
            'Enhancement - VOLTMAP MANUAL ROUNDING ISSUE FOR VTO ON (23-AUG-2014)
            'mdtEmployee = dsList.Tables("Emp").Copy
            mdtEmployee = New DataView(dsList.Tables("Emp").Copy, "", "employeecode", DataViewRowState.CurrentRows).ToTable
            'Pinkal (25-AUG-2014) -- End


            dgView = mdtEmployee.DefaultView
            dgvData.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvData.DataSource = dgView
            objchkEmployee.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee_List", mstrModuleName)
        Finally
            ObjEmp = Nothing : dsList.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 17 JAN 2014 ] -- START
    Private Sub Set_Form_Size()
        Try
            If mblnRecaculate = True Then   'RECALCULATE TIMINGS
                gbFilter.Size = CType(New Point(404, 66), Drawing.Size)
                tblpAssessorEmployee.Size = CType(New Point(403, 339), Drawing.Size)
                tblpAssessorEmployee.Location = New Point(0, 68)
            ElseIf mblnRecaculate = True Then 'ROUND OFF TIMINGS
                gbFilter.Size = CType(New Point(404, 127), Drawing.Size)
                tblpAssessorEmployee.Size = CType(New Point(403, 277), Drawing.Size)
                tblpAssessorEmployee.Location = New Point(0, 129)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Size", mstrModuleName)
        End Try
    End Sub

    Private Sub Generate_Range()
        Try
            mdtRange = New DataTable("List")
            mdtRange.Columns.Add("lbound", System.Type.GetType("System.Int32"))
            mdtRange.Columns.Add("ubound", System.Type.GetType("System.Int32"))

            Dim i As Integer = 0
            While i < 60
                Dim dtRow As DataRow = mdtRange.NewRow
                dtRow.Item("lbound") = i
                If i > 0 Then
                    dtRow.Item("ubound") = IIf((nudMinutes.Value + i) >= 60, 59, ((nudMinutes.Value + i) - 1))
                Else
                    dtRow.Item("ubound") = (nudMinutes.Value + i)
                End If
                mdtRange.Rows.Add(dtRow)
                If i <= 0 Then
                    i = CInt(nudMinutes.Value + 1)
                Else
                    i = CInt(i + nudMinutes.Value)
                End If
            End While
        Catch ex As Exception
            MsgBox("Generate_Range : " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
    'S.SANDEEP [ 17 JAN 2014 ] -- END

#End Region

#Region " Form's Event "

    Private Sub frmRecalculateTimings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'dtpFromDate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpToDate.MinDate = FinancialYear._Object._Database_Start_Date
            Call Fill_Employee_List()
            'S.SANDEEP [ 17 JAN 2014 ] -- START
            If mblnRecaculate = False Then
                radGraceRoundOff.Checked = True
            End If
            'S.SANDEEP [ 17 JAN 2014 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRecalculateTimings_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            clslogin_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clslogin_Tran"
            objfrm.displayDialog(Me)
        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime
            txtSearch.Text = ""
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_Employee_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If dgView Is Nothing AndAlso dgView.ToTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no employee(s) to recalculate timings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim drRow() As DataRow = dgView.ToTable.Select("ischeck=true")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select employee(s) in order to recalculate timings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim lstEmpoyeeId As List(Of String) = (From p In drRow Select (p.Item("employeeunkid").ToString)).ToList
            Dim mstrEmployeeIDs As String = String.Join(",", lstEmpoyeeId.ToArray)

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                Dim objMstdata As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran

                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, dtpFromDate.Value.Date, 0, 0, True)
                Dim intPeriod As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpFromDate.Value.Date)
                'Pinkal (20-Jan-2014) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intPeriod
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriod
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    If mblnRecaculate Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "you can't recalculate employee(s) timings .Reason:Period is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    ElseIf mblnRecaculate = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "you can't round off employee(s) timings .Reason:Period is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                End If

                If intPeriod > 0 Then
                    Dim objLeaveTran As New clsTnALeaveTran

                    'Pinkal (03-Jan-2014) -- Start
                    'Enhancement : Oman Changes

                    'If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid, mstrEmployeeIDs, objPeriod._End_Date.Date) Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "you can't edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    '    Exit Sub
                    'End If
                    Dim mdtTnADate As DateTime = Nothing
                    If objPeriod._TnA_EndDate.Date < dtpFromDate.Value.Date Then
                        mdtTnADate = dtpFromDate.Value.Date
                    Else
                        mdtTnADate = objPeriod._TnA_EndDate.Date
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid, mstrEmployeeIDs, mdtTnADate.Date, enModuleReference.TnA) Then
                    If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), mstrEmployeeIDs, mdtTnADate.Date, enModuleReference.TnA) Then
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "you can't edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "you can't edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 11, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Exit Sub
                    End If

                    'Pinkal (03-Jan-2014) -- End

                End If

            End If

            Dim dTtable As DataTable = drRow.CopyToDataTable()

            Dim objTimeSheet As New clslogin_Tran


            'Pinkal (09-Nov-2013) -- Start
            'Enhancement : Oman Changes

            Me.Cursor = Cursors.WaitCursor

            If mblnRecaculate Then


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                'If objTimeSheet.RecalculateTimings(dTtable, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                '                                   , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                   , dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
                '                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                '                                   , ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, True, -1, Nothing, "", "") Then

                If objTimeSheet.RecalculateTimings(dTtable, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                   , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                   , dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                     , ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                     , ConfigParameter._Object._IsHolidayConsiderOnWeekend, ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                     , ConfigParameter._Object._IsHolidayConsiderOnDayoff, True, -1, Nothing, "", "") Then

                    'Pinkal (11-AUG-2017) -- End



                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Recalculate Timing done successfully for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    Fill_Employee_List()
                Else
                    eZeeMsgBox.Show(objTimeSheet._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

            ElseIf mblnRecaculate = False Then    'Auto RoundOff

                'S.SANDEEP [ 17 JAN 2014 ] -- START
                'If objTimeSheet.AutoRoundOff(dtpFromDate.Value.Date, dtpToDate.Value.Date, dTtable) Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Auto Round Off done successfully for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                '    dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                '    Fill_Employee_List()
                'Else
                '    eZeeMsgBox.Show(objTimeSheet._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    Exit Sub
                'End If
                If radGraceRoundOff.Checked = True Then


                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'If objTimeSheet.AutoRoundOff(dTtable, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                    '                               , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                    '                               , dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
                    '                               , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                    '                             ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, True, -1, Nothing, "", "") Then

                    If objTimeSheet.AutoRoundOff(dTtable, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                   , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                   , dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                 , ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                 , ConfigParameter._Object._IsHolidayConsiderOnWeekend, ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                 , ConfigParameter._Object._IsHolidayConsiderOnDayoff, True, -1, Nothing, "", "") Then

                        'Pinkal (11-AUG-2017) -- End

                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Auto Round Off done successfully for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        Fill_Employee_List()
                    Else
                        eZeeMsgBox.Show(objTimeSheet._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                ElseIf radManualRoundOff.Checked = True Then
                    If chkInTime.Checked = False AndAlso chkOutTime.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Please tick atleast one roundoff type checkbox, [In Time or Out Time or both] to cotinue."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    If nudMinutes.Value <= 0 OrElse nudLimit.Value <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you have not set any minute to round and its limit for rounding."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    Dim eType As enManualRoundOffType
                    If chkInTime.Checked = True AndAlso chkOutTime.Checked = True Then
                        eType = enManualRoundOffType.ROUND_BOTH
                    ElseIf chkInTime.Checked = True Then
                        eType = enManualRoundOffType.ROUND_IN_TIME
                    ElseIf chkOutTime.Checked = True Then
                        eType = enManualRoundOffType.ROUND_OUT_TIME
                    End If


                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'If objTimeSheet.ManualRoundOff(dtpFromDate.Value.Date, dtpToDate.Value.Date, dTtable, mdtRange _
                    '                              , CInt(nudMinutes.Value), CInt(nudLimit.Value), eType _
                    '                              , FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                              , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True _
                    '                              , ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                    '                              , ConfigParameter._Object._DonotAttendanceinSeconds _
                    '                              , ConfigParameter._Object._FirstCheckInLastCheckOut, True, -1, Nothing, "", "") Then

                    If objTimeSheet.ManualRoundOff(dtpFromDate.Value.Date, dtpToDate.Value.Date, dTtable, mdtRange _
                                                  , CInt(nudMinutes.Value), CInt(nudLimit.Value), eType _
                                                  , FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                  , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True _
                                                  , ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                  , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                    , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                    , True, -1, Nothing, "", "") Then

                        'Pinkal (11-AUG-2017) -- End

                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Auto Round Off done successfully for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        Fill_Employee_List()
                    Else
                        eZeeMsgBox.Show(objTimeSheet._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                End If

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridview Event"

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                mdtEmployee.AcceptChanges()
                Dim drRow As DataRow() = mdtEmployee.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If mdtEmployee.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%'"
            End If
            dgView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = dgvData.Rows(dgvData.RowCount - 1).Index Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In dgView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAllocationFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 17 JAN 2014 ] -- START
#Region " Other Event(s) "

    Private Sub radGraceRoundOff_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radGraceRoundOff.CheckedChanged
        Try
            If radGraceRoundOff.Checked = True Then
                pnlChkBox.Enabled = False : pnlMinutes.Enabled = False
                chkInTime.Checked = False : chkOutTime.Checked = False
                nudLimit.Value = 0 : nudMinutes.Value = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radGraceRoundOff_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radManualRoundOff_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radManualRoundOff.CheckedChanged
        Try
            If radManualRoundOff.Checked = True Then
                pnlChkBox.Enabled = True : pnlMinutes.Enabled = True
                chkInTime.Checked = False : chkOutTime.Checked = False
                nudLimit.Value = 0 : nudMinutes.Value = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radManualRoundOff_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub nudLimit_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudLimit.ValueChanged
        Try
            If nudLimit.Value > 0 Then
                If nudLimit.Value >= nudMinutes.Value Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Round Off limit should be less than the minute interval set."), enMsgBoxStyle.Information)
                    nudLimit.Value = 0
                    nudLimit.Focus()
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudLimit_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub nudMinutes_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudMinutes.ValueChanged
        Try
            nudLimit.Value = 0
            If nudMinutes.Value > 0 Then
                Call Generate_Range()
            Else
                mdtRange = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudMinutes_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 17 JAN 2014 ] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFdate.Text = Language._Object.getCaption(Me.lblFdate.Name, Me.lblFdate.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.radManualRoundOff.Text = Language._Object.getCaption(Me.radManualRoundOff.Name, Me.radManualRoundOff.Text)
			Me.radGraceRoundOff.Text = Language._Object.getCaption(Me.radGraceRoundOff.Name, Me.radGraceRoundOff.Text)
			Me.chkOutTime.Text = Language._Object.getCaption(Me.chkOutTime.Name, Me.chkOutTime.Text)
			Me.chkInTime.Text = Language._Object.getCaption(Me.chkInTime.Name, Me.chkInTime.Text)
			Me.lblRoundLimit.Text = Language._Object.getCaption(Me.lblRoundLimit.Name, Me.lblRoundLimit.Text)
			Me.lblMinutes.Text = Language._Object.getCaption(Me.lblMinutes.Name, Me.lblMinutes.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no employee(s) to recalculate timings.")
            Language.setMessage(mstrModuleName, 2, "Please select employee(s) in order to recalculate timings.")
            Language.setMessage(mstrModuleName, 3, "you can't recalculate employee(s) timings .Reason:Period is already over.")
            Language.setMessage(mstrModuleName, 4, "you can't round off employee(s) timings .Reason:Period is already over.")
            Language.setMessage(mstrModuleName, 5, "you can't edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.")
            Language.setMessage(mstrModuleName, 6, "Recalculate Timing done successfully for selected employee(s).")
            Language.setMessage(mstrModuleName, 7, "Auto Round Off done successfully for selected employee(s).")
			Language.setMessage(mstrModuleName, 8, "Sorry, Round Off limit should be less than the minute interval set.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Please tick atleast one roundoff type checkbox, [In Time or Out Time or both] to cotinue.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you have not set any minute to round and its limit for rounding.")
			Language.setMessage(mstrModuleName, 11, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class