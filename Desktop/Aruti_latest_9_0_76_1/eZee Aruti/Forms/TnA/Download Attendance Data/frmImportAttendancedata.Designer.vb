﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportAttendancedata
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportAttendancedata))
        Me.dtpfromdate = New System.Windows.Forms.DateTimePicker
        Me.lblfromdate = New System.Windows.Forms.Label
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker
        Me.lblToDate = New System.Windows.Forms.Label
        Me.chkUseMappedDeviceUser = New System.Windows.Forms.CheckBox
        Me.btnDownloaddata = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgDownloaddata = New System.Windows.Forms.DataGridView
        Me.ColhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.LblSuccessInfo = New System.Windows.Forms.Label
        Me.objLblSuccess = New System.Windows.Forms.Label
        Me.LblErrorInfo = New System.Windows.Forms.Label
        Me.objLblError = New System.Windows.Forms.Label
        Me.objbtnImport = New eZee.Common.eZeeSplitButton
        Me.cmnuAttendanceData = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.btnExportData = New System.Windows.Forms.ToolStripMenuItem
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.gbDeviceList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeSuspendedEmpolyee = New System.Windows.Forms.CheckBox
        Me.lnkGetDeviceList = New System.Windows.Forms.LinkLabel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lnkFillData = New System.Windows.Forms.LinkLabel
        Me.lvDeviceList = New System.Windows.Forms.ListView
        Me.objcolhchkAll = New System.Windows.Forms.ColumnHeader
        Me.colhDeviceList = New System.Windows.Forms.ColumnHeader
        Me.colhIPAddress = New System.Windows.Forms.ColumnHeader
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSrno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhDevice = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhShift = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLoginTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgDownloaddata, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.cmnuAttendanceData.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.gbDeviceList.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpfromdate
        '
        Me.dtpfromdate.Checked = False
        Me.dtpfromdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpfromdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpfromdate.Location = New System.Drawing.Point(72, 32)
        Me.dtpfromdate.Name = "dtpfromdate"
        Me.dtpfromdate.Size = New System.Drawing.Size(106, 21)
        Me.dtpfromdate.TabIndex = 1
        '
        'lblfromdate
        '
        Me.lblfromdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfromdate.Location = New System.Drawing.Point(7, 34)
        Me.lblfromdate.Name = "lblfromdate"
        Me.lblfromdate.Size = New System.Drawing.Size(60, 17)
        Me.lblfromdate.TabIndex = 30
        Me.lblfromdate.Text = "From Date"
        Me.lblfromdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpTodate
        '
        Me.dtpTodate.Checked = False
        Me.dtpTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(213, 32)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.Size = New System.Drawing.Size(103, 21)
        Me.dtpTodate.TabIndex = 2
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(185, 34)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(23, 17)
        Me.lblToDate.TabIndex = 32
        Me.lblToDate.Text = "To"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkUseMappedDeviceUser
        '
        Me.chkUseMappedDeviceUser.BackColor = System.Drawing.Color.Transparent
        Me.chkUseMappedDeviceUser.Checked = True
        Me.chkUseMappedDeviceUser.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUseMappedDeviceUser.Location = New System.Drawing.Point(7, 79)
        Me.chkUseMappedDeviceUser.Name = "chkUseMappedDeviceUser"
        Me.chkUseMappedDeviceUser.Size = New System.Drawing.Size(180, 17)
        Me.chkUseMappedDeviceUser.TabIndex = 38
        Me.chkUseMappedDeviceUser.Text = "Use Mapped Device User"
        Me.chkUseMappedDeviceUser.UseVisualStyleBackColor = False
        '
        'btnDownloaddata
        '
        Me.btnDownloaddata.BackColor = System.Drawing.Color.White
        Me.btnDownloaddata.BackgroundImage = CType(resources.GetObject("btnDownloaddata.BackgroundImage"), System.Drawing.Image)
        Me.btnDownloaddata.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDownloaddata.BorderColor = System.Drawing.Color.Empty
        Me.btnDownloaddata.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDownloaddata.FlatAppearance.BorderSize = 0
        Me.btnDownloaddata.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDownloaddata.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownloaddata.ForeColor = System.Drawing.Color.Black
        Me.btnDownloaddata.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDownloaddata.GradientForeColor = System.Drawing.Color.Black
        Me.btnDownloaddata.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDownloaddata.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDownloaddata.Location = New System.Drawing.Point(685, 12)
        Me.btnDownloaddata.Name = "btnDownloaddata"
        Me.btnDownloaddata.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDownloaddata.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDownloaddata.Size = New System.Drawing.Size(96, 32)
        Me.btnDownloaddata.TabIndex = 3
        Me.btnDownloaddata.Text = "&Fill"
        Me.btnDownloaddata.UseVisualStyleBackColor = True
        Me.btnDownloaddata.Visible = False
        '
        'dgDownloaddata
        '
        Me.dgDownloaddata.AllowUserToAddRows = False
        Me.dgDownloaddata.AllowUserToDeleteRows = False
        Me.dgDownloaddata.AllowUserToResizeColumns = False
        Me.dgDownloaddata.AllowUserToResizeRows = False
        Me.dgDownloaddata.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgDownloaddata.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgDownloaddata.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgDownloaddata.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColhImage, Me.colhSrno, Me.colhDevice, Me.colhEmployeeName, Me.colhLogindate, Me.colhShift, Me.colhLoginTime, Me.colhMessage})
        Me.dgDownloaddata.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgDownloaddata.Location = New System.Drawing.Point(0, 0)
        Me.dgDownloaddata.Name = "dgDownloaddata"
        Me.dgDownloaddata.ReadOnly = True
        Me.dgDownloaddata.RowHeadersVisible = False
        Me.dgDownloaddata.Size = New System.Drawing.Size(669, 521)
        Me.dgDownloaddata.TabIndex = 34
        Me.dgDownloaddata.TabStop = False
        '
        'ColhImage
        '
        Me.ColhImage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ColhImage.Frozen = True
        Me.ColhImage.HeaderText = ""
        Me.ColhImage.Name = "ColhImage"
        Me.ColhImage.ReadOnly = True
        Me.ColhImage.Width = 25
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.LblSuccessInfo)
        Me.EZeeFooter1.Controls.Add(Me.objLblSuccess)
        Me.EZeeFooter1.Controls.Add(Me.LblErrorInfo)
        Me.EZeeFooter1.Controls.Add(Me.objLblError)
        Me.EZeeFooter1.Controls.Add(Me.objbtnImport)
        Me.EZeeFooter1.Controls.Add(Me.btnDownloaddata)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 521)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(994, 50)
        Me.EZeeFooter1.TabIndex = 35
        '
        'LblSuccessInfo
        '
        Me.LblSuccessInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSuccessInfo.Location = New System.Drawing.Point(168, 28)
        Me.LblSuccessInfo.Name = "LblSuccessInfo"
        Me.LblSuccessInfo.Size = New System.Drawing.Size(406, 13)
        Me.LblSuccessInfo.TabIndex = 45
        Me.LblSuccessInfo.Text = "Data fetched from device(s) successfully."
        '
        'objLblSuccess
        '
        Me.objLblSuccess.BackColor = System.Drawing.Color.Green
        Me.objLblSuccess.Location = New System.Drawing.Point(143, 28)
        Me.objLblSuccess.Name = "objLblSuccess"
        Me.objLblSuccess.Size = New System.Drawing.Size(12, 13)
        Me.objLblSuccess.TabIndex = 44
        '
        'LblErrorInfo
        '
        Me.LblErrorInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblErrorInfo.Location = New System.Drawing.Point(168, 9)
        Me.LblErrorInfo.Name = "LblErrorInfo"
        Me.LblErrorInfo.Size = New System.Drawing.Size(406, 13)
        Me.LblErrorInfo.TabIndex = 43
        Me.LblErrorInfo.Text = "Error while obtaining data from device(s), Click 'Error' for more info."
        '
        'objLblError
        '
        Me.objLblError.BackColor = System.Drawing.Color.Red
        Me.objLblError.Location = New System.Drawing.Point(143, 9)
        Me.objLblError.Name = "objLblError"
        Me.objLblError.Size = New System.Drawing.Size(12, 13)
        Me.objLblError.TabIndex = 42
        '
        'objbtnImport
        '
        Me.objbtnImport.BorderColor = System.Drawing.Color.Black
        Me.objbtnImport.ContextMenuStrip = Me.cmnuAttendanceData
        Me.objbtnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnImport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnImport.Location = New System.Drawing.Point(8, 12)
        Me.objbtnImport.Name = "objbtnImport"
        Me.objbtnImport.ShowDefaultBorderColor = True
        Me.objbtnImport.Size = New System.Drawing.Size(110, 30)
        Me.objbtnImport.SplitButtonMenu = Me.cmnuAttendanceData
        Me.objbtnImport.TabIndex = 41
        Me.objbtnImport.Text = "Opearation"
        '
        'cmnuAttendanceData
        '
        Me.cmnuAttendanceData.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnExportError, Me.btnExportData})
        Me.cmnuAttendanceData.Name = "cmImportAccrueLeave"
        Me.cmnuAttendanceData.Size = New System.Drawing.Size(137, 48)
        '
        'btnExportError
        '
        Me.btnExportError.Name = "btnExportError"
        Me.btnExportError.Size = New System.Drawing.Size(136, 22)
        Me.btnExportError.Text = "Export Error"
        '
        'btnExportData
        '
        Me.btnExportData.Name = "btnExportData"
        Me.btnExportData.Size = New System.Drawing.Size(136, 22)
        Me.btnExportData.Text = "Export Data"
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(799, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(895, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.SplitContainer1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(994, 521)
        Me.pnlMain.TabIndex = 37
        '
        'SplitContainer1
        '
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.gbDeviceList)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgDownloaddata)
        Me.SplitContainer1.Size = New System.Drawing.Size(994, 521)
        Me.SplitContainer1.SplitterDistance = 323
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 35
        '
        'gbDeviceList
        '
        Me.gbDeviceList.BorderColor = System.Drawing.Color.Black
        Me.gbDeviceList.Checked = False
        Me.gbDeviceList.CollapseAllExceptThis = False
        Me.gbDeviceList.CollapsedHoverImage = Nothing
        Me.gbDeviceList.CollapsedNormalImage = Nothing
        Me.gbDeviceList.CollapsedPressedImage = Nothing
        Me.gbDeviceList.CollapseOnLoad = False
        Me.gbDeviceList.Controls.Add(Me.chkIncludeSuspendedEmpolyee)
        Me.gbDeviceList.Controls.Add(Me.lnkGetDeviceList)
        Me.gbDeviceList.Controls.Add(Me.objchkSelectAll)
        Me.gbDeviceList.Controls.Add(Me.lnkFillData)
        Me.gbDeviceList.Controls.Add(Me.chkUseMappedDeviceUser)
        Me.gbDeviceList.Controls.Add(Me.lvDeviceList)
        Me.gbDeviceList.Controls.Add(Me.dtpfromdate)
        Me.gbDeviceList.Controls.Add(Me.dtpTodate)
        Me.gbDeviceList.Controls.Add(Me.lblToDate)
        Me.gbDeviceList.Controls.Add(Me.lblfromdate)
        Me.gbDeviceList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbDeviceList.ExpandedHoverImage = Nothing
        Me.gbDeviceList.ExpandedNormalImage = Nothing
        Me.gbDeviceList.ExpandedPressedImage = Nothing
        Me.gbDeviceList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDeviceList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDeviceList.HeaderHeight = 25
        Me.gbDeviceList.HeaderMessage = ""
        Me.gbDeviceList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDeviceList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDeviceList.HeightOnCollapse = 0
        Me.gbDeviceList.LeftTextSpace = 0
        Me.gbDeviceList.Location = New System.Drawing.Point(0, 0)
        Me.gbDeviceList.Name = "gbDeviceList"
        Me.gbDeviceList.OpenHeight = 300
        Me.gbDeviceList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDeviceList.ShowBorder = True
        Me.gbDeviceList.ShowCheckBox = False
        Me.gbDeviceList.ShowCollapseButton = False
        Me.gbDeviceList.ShowDefaultBorderColor = True
        Me.gbDeviceList.ShowDownButton = False
        Me.gbDeviceList.ShowHeader = True
        Me.gbDeviceList.Size = New System.Drawing.Size(323, 521)
        Me.gbDeviceList.TabIndex = 0
        Me.gbDeviceList.Temp = 0
        Me.gbDeviceList.Text = "Device List"
        Me.gbDeviceList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeSuspendedEmpolyee
        '
        Me.chkIncludeSuspendedEmpolyee.BackColor = System.Drawing.Color.Transparent
        Me.chkIncludeSuspendedEmpolyee.Checked = True
        Me.chkIncludeSuspendedEmpolyee.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeSuspendedEmpolyee.Location = New System.Drawing.Point(7, 59)
        Me.chkIncludeSuspendedEmpolyee.Name = "chkIncludeSuspendedEmpolyee"
        Me.chkIncludeSuspendedEmpolyee.Size = New System.Drawing.Size(198, 17)
        Me.chkIncludeSuspendedEmpolyee.TabIndex = 46
        Me.chkIncludeSuspendedEmpolyee.Text = "Include Suspended Employee"
        Me.chkIncludeSuspendedEmpolyee.UseVisualStyleBackColor = False
        '
        'lnkGetDeviceList
        '
        Me.lnkGetDeviceList.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkGetDeviceList.Location = New System.Drawing.Point(212, 58)
        Me.lnkGetDeviceList.Name = "lnkGetDeviceList"
        Me.lnkGetDeviceList.Size = New System.Drawing.Size(105, 19)
        Me.lnkGetDeviceList.TabIndex = 44
        Me.lnkGetDeviceList.TabStop = True
        Me.lnkGetDeviceList.Text = "Get Device List"
        Me.lnkGetDeviceList.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkSelectAll.Location = New System.Drawing.Point(7, 104)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(13, 13)
        Me.objchkSelectAll.TabIndex = 42
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lnkFillData
        '
        Me.lnkFillData.BackColor = System.Drawing.Color.Transparent
        Me.lnkFillData.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkFillData.Location = New System.Drawing.Point(220, 3)
        Me.lnkFillData.Name = "lnkFillData"
        Me.lnkFillData.Size = New System.Drawing.Size(95, 19)
        Me.lnkFillData.TabIndex = 40
        Me.lnkFillData.TabStop = True
        Me.lnkFillData.Text = "[ Fill Data ]"
        Me.lnkFillData.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lvDeviceList
        '
        Me.lvDeviceList.CheckBoxes = True
        Me.lvDeviceList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhchkAll, Me.colhDeviceList, Me.colhIPAddress})
        Me.lvDeviceList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDeviceList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDeviceList.Location = New System.Drawing.Point(1, 99)
        Me.lvDeviceList.MultiSelect = False
        Me.lvDeviceList.Name = "lvDeviceList"
        Me.lvDeviceList.Size = New System.Drawing.Size(319, 419)
        Me.lvDeviceList.TabIndex = 1
        Me.lvDeviceList.UseCompatibleStateImageBehavior = False
        Me.lvDeviceList.View = System.Windows.Forms.View.Details
        '
        'objcolhchkAll
        '
        Me.objcolhchkAll.Tag = "objcolhchkAll"
        Me.objcolhchkAll.Text = ""
        '
        'colhDeviceList
        '
        Me.colhDeviceList.Tag = "colhDeviceList"
        Me.colhDeviceList.Text = "Device"
        Me.colhDeviceList.Width = 130
        '
        'colhIPAddress
        '
        Me.colhIPAddress.Tag = "colhIPAddress"
        Me.colhIPAddress.Text = "IP Address"
        Me.colhIPAddress.Width = 100
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Sr.No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 40
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Device"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 75
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn5.HeaderText = "Shift"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn6.HeaderText = "Time"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn7.HeaderText = "Message"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 250
        '
        'colhSrno
        '
        Me.colhSrno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhSrno.Frozen = True
        Me.colhSrno.HeaderText = "Sr.No"
        Me.colhSrno.Name = "colhSrno"
        Me.colhSrno.ReadOnly = True
        Me.colhSrno.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhSrno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhSrno.Width = 40
        '
        'colhDevice
        '
        Me.colhDevice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhDevice.Frozen = True
        Me.colhDevice.HeaderText = "Device"
        Me.colhDevice.Name = "colhDevice"
        Me.colhDevice.ReadOnly = True
        Me.colhDevice.Width = 75
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhEmployeeName.Frozen = True
        Me.colhEmployeeName.HeaderText = "Employee Name"
        Me.colhEmployeeName.Name = "colhEmployeeName"
        Me.colhEmployeeName.ReadOnly = True
        Me.colhEmployeeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhEmployeeName.Width = 200
        '
        'colhLogindate
        '
        Me.colhLogindate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhLogindate.Frozen = True
        Me.colhLogindate.HeaderText = "Date"
        Me.colhLogindate.Name = "colhLogindate"
        Me.colhLogindate.ReadOnly = True
        Me.colhLogindate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhShift
        '
        Me.colhShift.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhShift.HeaderText = "Shift"
        Me.colhShift.Name = "colhShift"
        Me.colhShift.ReadOnly = True
        Me.colhShift.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhShift.Width = 150
        '
        'colhLoginTime
        '
        Me.colhLoginTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhLoginTime.HeaderText = "Time"
        Me.colhLoginTime.Name = "colhLoginTime"
        Me.colhLoginTime.ReadOnly = True
        Me.colhLoginTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhMessage
        '
        Me.colhMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhMessage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhMessage.Width = 250
        '
        'frmImportAttendancedata
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 571)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportAttendancedata"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Attendance Data"
        CType(Me.dgDownloaddata, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.cmnuAttendanceData.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.gbDeviceList.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dtpfromdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblfromdate As System.Windows.Forms.Label
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dgDownloaddata As System.Windows.Forms.DataGridView
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents btnDownloaddata As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents chkUseMappedDeviceUser As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnImport As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuAttendanceData As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbDeviceList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvDeviceList As System.Windows.Forms.ListView
    Friend WithEvents objcolhchkAll As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDeviceList As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkFillData As System.Windows.Forms.LinkLabel
    Friend WithEvents colhIPAddress As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lnkGetDeviceList As System.Windows.Forms.LinkLabel
    Friend WithEvents ColhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhSrno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhDevice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhShift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLoginTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objLblError As System.Windows.Forms.Label
    Friend WithEvents LblErrorInfo As System.Windows.Forms.Label
    Friend WithEvents LblSuccessInfo As System.Windows.Forms.Label
    Friend WithEvents objLblSuccess As System.Windows.Forms.Label
    Friend WithEvents chkIncludeSuspendedEmpolyee As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
