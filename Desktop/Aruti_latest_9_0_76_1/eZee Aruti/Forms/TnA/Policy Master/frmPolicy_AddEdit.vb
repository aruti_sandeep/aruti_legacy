﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization


Public Class frmPolicy_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmPolicy_AddEdit"
    Private mblnCancel As Boolean = True
    Private objPolicyMst As clspolicy_master
    Private objPolicyTran As clsPolicy_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintPolicyMasterUnkid As Integer = -1
    Dim wkMins As Double
    Dim mdtDays As DataTable = Nothing
    Dim mintTotalmins As Double

    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
    Dim mdtOTPenalty As DataTable = Nothing
    'Pinkal (06-May-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintPolicyMasterUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintPolicyMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmPolicy_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPolicyMst = New clspolicy_master
        objPolicyTran = New clsPolicy_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            setColor()
            FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objPolicyMst._Policyunkid = mintPolicyMasterUnkid
                objPolicyTran.GetPolicyTran(objPolicyMst._Policyunkid)
                mdtDays = objPolicyTran._DataList
                GetValue()

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
            Else
                objPolicyMst._Policyunkid = -1
                objPolicyTran.GetPolicyTran(objPolicyMst._Policyunkid)
                mdtDays = objPolicyTran._DataList
                'S.SANDEEP [14-JUN-2018] -- END
            End If

            'Pinkal (06-Aug-2015) -- Start
            'Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
            dgcolhbreaktime.Visible = ConfigParameter._Object._FirstCheckInLastCheckOut
            dgcolhCntBreakTimeAfter.Visible = ConfigParameter._Object._FirstCheckInLastCheckOut
            dgcolhteatime.Visible = ConfigParameter._Object._FirstCheckInLastCheckOut
            dgcolhCntTeaTimeAfter.Visible = ConfigParameter._Object._FirstCheckInLastCheckOut
            'Pinkal (06-Aug-2015) -- End


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'lnkOTPenalty.Visible = ConfigParameter._Object._PolicyManagementTNA
            'Pinkal (16-Apr-2016) -- End

            GeneratePolicyDays()
            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPolicy_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPolicy_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPolicy_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPolicy_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                'btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPolicy_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPolicy_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPolicyMst = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            clspolicy_master.SetMessages()
            objfrm._Other_ModuleNames = "clspolicy_master"
            objfrm.displayDialog(Me)
        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objPolicyMst._Policycode
            txtName.Text = objPolicyMst._Policyname
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objPolicyMst._Policycode = txtCode.Text
            objPolicyMst._Policyname = txtName.Text
            objPolicyMst._Userunkid = User._Object._Userunkid

            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            objPolicyMst._dtOTPenalty = mdtOTPenalty
            'Pinkal (06-May-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    '    Private Sub GeneratePolicyDays()
    '        Try
    '            Dim days() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames

    '            If mdtDays Is Nothing Then
    '                mdtDays = New DataTable("ShiftDays")
    '                mdtDays.Columns.Add("policytranunkid", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("policyunkid", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("dayId", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("days", Type.GetType("System.String"))
    '                mdtDays.Columns.Add("isweekend", Type.GetType("System.Boolean"))
    '                mdtDays.Columns.Add("basehours", Type.GetType("System.Decimal"))
    '                mdtDays.Columns.Add("ot1", Type.GetType("System.Decimal"))
    '                mdtDays.Columns.Add("ot2", Type.GetType("System.Decimal"))
    '                mdtDays.Columns.Add("ot3", Type.GetType("System.Decimal"))
    '                mdtDays.Columns.Add("ot4", Type.GetType("System.Decimal"))
    '                mdtDays.Columns.Add("elrcoming_grace", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("ltcoming_grace", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("elrgoing_grace", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("ltgoing_grace", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("basehoursinsec", Type.GetType("System.Decimal"))
    '                mdtDays.Columns.Add("ot1insec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("ot2insec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("ot3insec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("ot4insec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("elrcome_graceinsec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("ltcome_graceinsec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("elrgoing_graceinsec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("ltgoing_graceinsec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("isvoid", Type.GetType("System.Boolean"))
    '                mdtDays.Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
    '                mdtDays.Columns.Add("voidreason", Type.GetType("System.String"))
    '                mdtDays.Columns.Add("AUD", Type.GetType("System.String"))
    '                'S.SANDEEP [ 07 JAN 2014 ] -- START
    '                mdtDays.Columns.Add("breaktime", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("breaktimeinsec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("countbreaktimeafter", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("countbreaktimeaftersec", Type.GetType("System.Int32"))
    '                'S.SANDEEP [ 07 JAN 2014 ] -- END


    '                'Pinkal (28-Jan-2014) -- Start
    '                'Enhancement : Oman Changes
    '                mdtDays.Columns.Add("teatime", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("teatimeinsec", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("countteatimeafter", Type.GetType("System.Int32"))
    '                mdtDays.Columns.Add("countteatimeaftersec", Type.GetType("System.Int32"))
    '                'Pinkal (28-Jan-2014) -- End


    'GenerateDays:
    '                For i As Integer = 0 To 6
    '                    Dim dr As DataRow = mdtDays.NewRow
    '                    dr("policytranunkid") = -1
    '                    dr("policyunkid") = mintPolicyMasterUnkid
    '                    dr("dayId") = i
    '                    dr("Days") = days(i).ToString()
    '                    dr("isweekend") = False
    '                    dr("basehours") = 0
    '                    dr("ot1") = 0.0
    '                    dr("ot2") = 0.0
    '                    dr("ot3") = 0.0
    '                    dr("ot4") = 0.0
    '                    dr("elrcoming_grace") = 0
    '                    dr("ltcoming_grace") = 0
    '                    dr("elrgoing_grace") = 0
    '                    dr("ltgoing_grace") = 0
    '                    dr("basehoursinsec") = 0
    '                    dr("ot1insec") = 0.0
    '                    dr("ot2insec") = 0.0
    '                    dr("ot3insec") = 0.0
    '                    dr("ot4insec") = 0.0
    '                    dr("elrcome_graceinsec") = 0
    '                    dr("ltcome_graceinsec") = 0
    '                    dr("elrgoing_graceinsec") = 0
    '                    dr("ltgoing_graceinsec") = 0
    '                    dr("isvoid") = False
    '                    dr("voiduserunkid") = -1
    '                    dr("voiddatetime") = DBNull.Value
    '                    dr("voidreason") = ""
    '                    dr("AUD") = "A"
    '                    'S.SANDEEP [ 07 JAN 2014 ] -- START
    '                    dr("breaktime") = 0
    '                    dr("breaktimeinsec") = 0
    '                    dr("countbreaktimeafter") = 0
    '                    dr("countbreaktimeaftersec") = 0
    '                    'S.SANDEEP [ 07 JAN 2014 ] -- END


    '                    'Pinkal (28-Jan-2014) -- Start
    '                    'Enhancement : Oman Changes
    '                    dr("teatime") = 0
    '                    dr("teatimeinsec") = 0
    '                    dr("countteatimeafter") = 0
    '                    dr("countteatimeaftersec") = 0
    '                    'Pinkal (28-Jan-2014) -- End


    '                    mdtDays.Rows.Add(dr)
    '                Next

    '            Else

    '                If mdtDays.Rows.Count > 0 Then
    '                    For i As Integer = 0 To mdtDays.Rows.Count - 1
    '                        mdtDays.Rows(i)("Days") = days(i).ToString()
    '                        mdtDays.Rows(i)("basehours") = CalculateTime(True, CInt(mdtDays.Rows(i)("basehoursinsec")))
    '                        mdtDays.Rows(i)("ot1") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot1insec")))
    '                        mdtDays.Rows(i)("ot2") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot2insec")))
    '                        mdtDays.Rows(i)("ot3") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot3insec")))
    '                        mdtDays.Rows(i)("ot4") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot4insec")))
    '                        mdtDays.Rows(i)("elrcoming_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("elrcome_graceinsec")))
    '                        mdtDays.Rows(i)("ltcoming_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("ltcome_graceinsec")))
    '                        mdtDays.Rows(i)("elrgoing_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("elrgoing_graceinsec")))
    '                        mdtDays.Rows(i)("ltgoing_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("ltgoing_graceinsec")))
    '                        'S.SANDEEP [ 07 JAN 2014 ] -- START
    '                        mdtDays.Rows(i)("breaktime") = CalculateTime(False, CInt(mdtDays.Rows(i)("breaktimeinsec")))
    '                        mdtDays.Rows(i)("countbreaktimeafter") = CalculateTime(False, CInt(mdtDays.Rows(i)("countbreaktimeaftersec")))
    '                        'S.SANDEEP [ 07 JAN 2014 ] -- END


    '                        'Pinkal (28-Jan-2014) -- Start
    '                        'Enhancement : Oman Changes
    '                        mdtDays.Rows(i)("teatime") = CalculateTime(False, CInt(mdtDays.Rows(i)("teatimeinsec")))
    '                        mdtDays.Rows(i)("countteatimeafter") = CalculateTime(False, CInt(mdtDays.Rows(i)("countteatimeaftersec")))
    '                        'Pinkal (28-Jan-2014) -- End


    '                    Next
    '                Else
    '                    GoTo GenerateDays
    '                End If

    '            End If

    '            dgPolicy.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
    '            dgPolicy.AutoGenerateColumns = False
    '            dgcolhDay.DataPropertyName = "days"
    '            dgcolhWeekend.DataPropertyName = "isweekend"
    '            dgcolhBaseHrs.DataPropertyName = "basehours"
    '            dgColhOT1.DataPropertyName = "ot1"
    '            dgcolhOT2.DataPropertyName = "ot2"
    '            dgcolhOT3.DataPropertyName = "ot3"
    '            dgcolhOT4.DataPropertyName = "ot4"
    '            dgcolhEarlycoming.DataPropertyName = "elrcoming_grace"
    '            dgcolhLatecoming.DataPropertyName = "ltcoming_grace"
    '            dgcolhEarlygoing.DataPropertyName = "elrgoing_grace"
    '            dgcolhLateGoing.DataPropertyName = "ltgoing_grace"
    '            'S.SANDEEP [ 07 JAN 2014 ] -- START
    '            dgcolhbreaktime.DataPropertyName = "breaktime"
    '            dgcolhCntBreakTimeAfter.DataPropertyName = "countbreaktimeafter"
    '            'S.SANDEEP [ 07 JAN 2014 ] -- END


    '            'Pinkal (28-Jan-2014) -- Start
    '            'Enhancement : Oman Changes
    '            dgcolhteatime.DataPropertyName = "teatime"
    '            dgcolhCntTeaTimeAfter.DataPropertyName = "countteatimeafter"
    '            'Pinkal (28-Jan-2014) -- End


    '            dgPolicy.DataSource = mdtDays


    '            ' START USED FOR WHEN WEEKEND THEN READONLY IS TRUE

    '            'For i As Integer = 0 To mdtDays.Rows.Count - 1
    '            '    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhWeekend.Index)
    '            '    dgPolicy_CellValidated(New Object, New DataGridViewCellEventArgs(dgcolhWeekend.Index, i))
    '            'Next

    '            ' ELSE USED FOR WHEN WEEKEND THEN READONLY IS TRUE


    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "GeneratePolicyDays", mstrModuleName)
    '        End Try
    '    End Sub

    Private Sub GeneratePolicyDays()
        Try
            Dim days() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames

                If mdtDays.Rows.Count > 0 Then
                    For i As Integer = 0 To mdtDays.Rows.Count - 1
                        mdtDays.Rows(i)("Days") = days(i).ToString()
                        mdtDays.Rows(i)("basehours") = CalculateTime(True, CInt(mdtDays.Rows(i)("basehoursinsec")))
                        mdtDays.Rows(i)("ot1") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot1insec")))
                        mdtDays.Rows(i)("ot2") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot2insec")))
                        mdtDays.Rows(i)("ot3") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot3insec")))
                        mdtDays.Rows(i)("ot4") = CalculateTime(True, CInt(mdtDays.Rows(i)("ot4insec")))
                        mdtDays.Rows(i)("elrcoming_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("elrcome_graceinsec")))
                        mdtDays.Rows(i)("ltcoming_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("ltcome_graceinsec")))
                        mdtDays.Rows(i)("elrgoing_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("elrgoing_graceinsec")))
                        mdtDays.Rows(i)("ltgoing_grace") = CalculateTime(False, CInt(mdtDays.Rows(i)("ltgoing_graceinsec")))
                        'S.SANDEEP [ 07 JAN 2014 ] -- START
                        mdtDays.Rows(i)("breaktime") = CalculateTime(False, CInt(mdtDays.Rows(i)("breaktimeinsec")))
                        mdtDays.Rows(i)("countbreaktimeafter") = CalculateTime(False, CInt(mdtDays.Rows(i)("countbreaktimeaftersec")))
                        'S.SANDEEP [ 07 JAN 2014 ] -- END


                        'Pinkal (28-Jan-2014) -- Start
                        'Enhancement : Oman Changes
                        mdtDays.Rows(i)("teatime") = CalculateTime(False, CInt(mdtDays.Rows(i)("teatimeinsec")))
                        mdtDays.Rows(i)("countteatimeafter") = CalculateTime(False, CInt(mdtDays.Rows(i)("countteatimeaftersec")))
                        'Pinkal (28-Jan-2014) -- End


                    Next
                Else
                objPolicyTran.GenerateDefaultDays(mdtDays, mintPolicyMasterUnkid)
            End If

            dgPolicy.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
            dgPolicy.AutoGenerateColumns = False
            dgcolhDay.DataPropertyName = "days"
            dgcolhWeekend.DataPropertyName = "isweekend"
            dgcolhBaseHrs.DataPropertyName = "basehours"
            dgColhOT1.DataPropertyName = "ot1"
            dgcolhOT2.DataPropertyName = "ot2"
            dgcolhOT3.DataPropertyName = "ot3"
            dgcolhOT4.DataPropertyName = "ot4"
            dgcolhEarlycoming.DataPropertyName = "elrcoming_grace"
            dgcolhLatecoming.DataPropertyName = "ltcoming_grace"
            dgcolhEarlygoing.DataPropertyName = "elrgoing_grace"
            dgcolhLateGoing.DataPropertyName = "ltgoing_grace"
            'S.SANDEEP [ 07 JAN 2014 ] -- START
            dgcolhbreaktime.DataPropertyName = "breaktime"
            dgcolhCntBreakTimeAfter.DataPropertyName = "countbreaktimeafter"
            'S.SANDEEP [ 07 JAN 2014 ] -- END


            'Pinkal (28-Jan-2014) -- Start
            'Enhancement : Oman Changes
            dgcolhteatime.DataPropertyName = "teatime"
            dgcolhCntTeaTimeAfter.DataPropertyName = "countteatimeafter"
            'Pinkal (28-Jan-2014) -- End


            dgPolicy.DataSource = mdtDays


            ' START USED FOR WHEN WEEKEND THEN READONLY IS TRUE

            'For i As Integer = 0 To mdtDays.Rows.Count - 1
            '    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhWeekend.Index)
            '    dgPolicy_CellValidated(New Object, New DataGridViewCellEventArgs(dgcolhWeekend.Index, i))
            'Next

            ' ELSE USED FOR WHEN WEEKEND THEN READONLY IS TRUE


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GeneratePolicyDays", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

    

    Private Sub SetHours(ByVal intColumnIndex As Integer, ByVal intRowIndex As Integer, ByVal sValue As String)
        Try
            If sValue.ToString().Trim = "" Then
                dgPolicy.Rows(intColumnIndex).Cells(intRowIndex).Value = "0"

            ElseIf sValue.ToString.Trim.Contains(".") Then

                If sValue.ToString.Trim.Contains(".60") Then
calFromHr:          dgPolicy.Rows(intRowIndex).Cells(intColumnIndex).Value = CDec(CDec(sValue.ToString.Trim) + 0.4).ToString("#0.00")
                    dgPolicy.UpdateCellValue(intColumnIndex, intRowIndex)

                ElseIf sValue.ToString <> "" And sValue.ToString <> "." Then

                    If sValue.ToString.Trim.IndexOf(".") = 0 And sValue.ToString.Trim.Contains(".60") Then
                        GoTo calFromHr
                    ElseIf sValue.ToString.Substring(sValue.ToString.Trim.IndexOf("."), sValue.ToString.Trim.Length - sValue.ToString.Trim.IndexOf(".")) = "." Then
                        dgPolicy.Rows(intRowIndex).Cells(intColumnIndex).Value = sValue.ToString & "00"
                        dgPolicy.UpdateCellValue(intColumnIndex, intRowIndex)
                    ElseIf CDec(sValue.ToString.Substring(sValue.ToString.Trim.IndexOf("."), sValue.ToString.Trim.Length - sValue.ToString.Trim.IndexOf("."))) > 0.6 Then
                        GoTo calFromHr
                    Else
                        dgPolicy.Rows(intRowIndex).Cells(intColumnIndex).Value = CDec(sValue.ToString).ToString("#0.00")
                        dgPolicy.UpdateCellValue(intColumnIndex, intRowIndex)
                    End If

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetHours", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Policy Code cannot be blank. Policy Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Policy Name cannot be blank. Policy Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            'S.SANDEEP [ 07 JAN 2014 ] -- START
            Dim dErr() As DataRow = mdtDays.Select("breaktimeinsec > 0")
            Dim blnFound As Boolean = False
            If dErr.Length > 0 Then
                For iEr As Integer = 0 To dErr.Length - 1
                    If CInt(dErr(iEr).Item("countbreaktimeaftersec")) <= CInt(dErr(iEr).Item("breaktimeinsec")) Then
                        dgPolicy.Rows(mdtDays.Rows.IndexOf(dErr(iEr))).Cells(dgcolhbreaktime.Index).Style.ForeColor = Color.Red
                        blnFound = True
                    Else
                        dgPolicy.Rows(mdtDays.Rows.IndexOf(dErr(iEr))).Cells(dgcolhbreaktime.Index).Style.ForeColor = Color.Black
                    End If
                Next
                If blnFound = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Breaktime value should be less than the Count Breaktime After value."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'S.SANDEEP [ 07 JAN 2014 ] -- END



            'Pinkal (28-Jan-2014) -- Start
            'Enhancement : Oman Changes
            dErr = Nothing
            blnFound = False

            dErr = mdtDays.Select("teatimeinsec > 0")
            If dErr.Length > 0 Then
                For iEr As Integer = 0 To dErr.Length - 1
                    If CInt(dErr(iEr).Item("countteatimeaftersec")) <= CInt(dErr(iEr).Item("teatimeinsec")) Then
                        dgPolicy.Rows(mdtDays.Rows.IndexOf(dErr(iEr))).Cells(dgcolhteatime.Index).Style.ForeColor = Color.Red
                        blnFound = True
                    Else
                        dgPolicy.Rows(mdtDays.Rows.IndexOf(dErr(iEr))).Cells(dgcolhteatime.Index).Style.ForeColor = Color.Black
                    End If
                Next
                If blnFound = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Teatime value should be less than the Count Teatime After value."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            'Pinkal (28-Jan-2014) -- End


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objPolicyMst.Update(mdtDays)
            Else
                blnFlag = objPolicyMst.Insert(mdtDays)
            End If

            If blnFlag = False And objPolicyMst._Message <> "" Then
                eZeeMsgBox.Show(objPolicyMst._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objPolicyMst = Nothing
                    objPolicyMst = New clspolicy_master
                    objPolicyTran = New clsPolicy_tran
                    mdtDays = Nothing
                    Call GetValue()
                    GeneratePolicyDays()
                    txtCode.Focus()
                Else
                    mintPolicyMasterUnkid = objPolicyMst._Policyunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtName.Text, objPolicyMst._Policyname1, objPolicyMst._Policyname2)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgPolicy_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgPolicy.CellValidated
        Try
            If dgPolicy.CurrentCell Is Nothing Then Exit Sub

            If dgPolicy.CurrentCell.Value.ToString() = "" Then
                dgPolicy.CurrentCell.Value = "0"
            End If


            If e.ColumnIndex = dgcolhWeekend.Index Then

                If CBool(dgPolicy.Rows(e.RowIndex).Cells(dgcolhWeekend.Index).Value) = True Then
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).ReadOnly = True
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).Value = 0
                Else
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).ReadOnly = False
                End If
            End If

            If mintPolicyMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("policytranunkid")) > 0 Then
                mdtDays.Rows(e.RowIndex)("AUD") = "U"
            Else
                mdtDays.Rows(e.RowIndex)("AUD") = "A"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPolicy_CellValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPolicy_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgPolicy.CellValidating
        Try
            If dgPolicy.CurrentCell Is Nothing Then Exit Sub

            If dgPolicy.CurrentCell.Value.ToString() = "" Then
                dgPolicy.CurrentCell.Value = "0"
            End If

            Dim mintBaseHr As Integer = 0
            Dim mintOT1 As Integer = 0
            Dim mintOT2 As Integer = 0
            Dim mintOT3 As Integer = 0
            Dim mintOT4 As Integer = 0
            Dim mstrFormatedValue As String = e.FormattedValue.ToString()

            If mstrFormatedValue.ToString().Trim = "" Then mstrFormatedValue = "0"

            If e.ColumnIndex = dgcolhWeekend.Index Then

                If CBool(e.FormattedValue) = True Then
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).ReadOnly = True
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).Value = 0
                Else
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).ReadOnly = False
                End If

            ElseIf e.ColumnIndex = dgcolhBaseHrs.Index Then
                If CDec(mstrFormatedValue) > 24 Then
                    eZeeMsgBox.Show(dgcolhBaseHrs.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgColhOT1.Index Then
                If CDec(mstrFormatedValue) > 24 Then
                    eZeeMsgBox.Show(dgColhOT1.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgColhOT1.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgcolhOT2.Index Then
                If CDec(mstrFormatedValue) > 24 Then
                    eZeeMsgBox.Show(dgcolhOT2.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhOT2.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgcolhOT3.Index Then
                If CDec(mstrFormatedValue) > 24 Then
                    eZeeMsgBox.Show(dgcolhOT3.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhOT3.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgcolhOT4.Index Then
                If CDec(mstrFormatedValue) > 24 Then
                    eZeeMsgBox.Show(dgcolhOT4.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhOT4.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgcolhEarlycoming.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhEarlycoming.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhEarlycoming.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgcolhLatecoming.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhLatecoming.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhLatecoming.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgcolhEarlygoing.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhEarlygoing.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhEarlygoing.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

            ElseIf e.ColumnIndex = dgcolhLateGoing.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhLateGoing.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhLateGoing.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If
                'S.SANDEEP [ 07 JAN 2014 ] -- START
            ElseIf e.ColumnIndex = dgcolhbreaktime.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhbreaktime.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhbreaktime.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If
            ElseIf e.ColumnIndex = dgcolhCntBreakTimeAfter.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhCntBreakTimeAfter.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhCntBreakTimeAfter.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If
                'S.SANDEEP [ 07 JAN 2014 ] -- END


                'Pinkal (28-Jan-2014) -- Start
                'Enhancement : Oman Changes

            ElseIf e.ColumnIndex = dgcolhteatime.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhteatime.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhteatime.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If
            ElseIf e.ColumnIndex = dgcolhCntTeaTimeAfter.Index Then
                If CDec(mstrFormatedValue) > 1440 Then
                    eZeeMsgBox.Show(dgcolhCntTeaTimeAfter.HeaderText & Language.getMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhCntTeaTimeAfter.Index).Selected = True
                    e.Cancel = True
                    Exit Sub
                End If

                'Pinkal (28-Jan-2014) -- End

            End If


            '--- START FOR UPDATE PROPER HOURS ENTER BY USER 

            If e.ColumnIndex = dgcolhBaseHrs.Index OrElse e.ColumnIndex = dgColhOT1.Index OrElse e.ColumnIndex = dgcolhOT2.Index OrElse e.ColumnIndex = dgcolhOT3.Index OrElse e.ColumnIndex = dgcolhOT4.Index Then
                SetHours(e.ColumnIndex, e.RowIndex, mstrFormatedValue.ToString())
            End If

            '--- END FOR UPDATE PROPER HOURS ENTER BY USER 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPolicy_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPolicy_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgPolicy.CellValueChanged
        Try

            If dgPolicy.CurrentCell Is Nothing Then Exit Sub

            If dgPolicy.CurrentCell.Value.ToString() = "" Then
                dgPolicy.CurrentCell.Value = "0"
            End If

            SetHours(e.ColumnIndex, e.RowIndex, dgPolicy.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString())

            Dim inthrsec As Integer = 0
            Dim intminsec As Integer = 0
            If e.ColumnIndex <> dgcolhDay.Index AndAlso (dgPolicy.CurrentCell.ColumnIndex = dgcolhBaseHrs.Index OrElse dgPolicy.CurrentCell.ColumnIndex = dgColhOT1.Index OrElse dgPolicy.CurrentCell.ColumnIndex = dgcolhOT2.Index OrElse dgPolicy.CurrentCell.ColumnIndex = dgcolhOT3.Index OrElse dgPolicy.CurrentCell.ColumnIndex = dgcolhOT4.Index) Then
                inthrsec = CInt(CDec(dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value).ToString("#0.00").Substring(0, CDec(dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value).ToString("#0.00").IndexOf("."))) * 3600
                intminsec = CInt(CDec(dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value).ToString("#0.00").Substring(CDec(dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value).ToString("#0.00").IndexOf(".") + 1, CDec(dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value).ToString("#0.00").Length - CDec(dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value).ToString("#0.00").IndexOf(".") - 1)) * 60

            ElseIf e.ColumnIndex <> dgcolhDay.Index AndAlso (dgPolicy.CurrentCell.ColumnIndex = dgcolhEarlycoming.Index OrElse dgPolicy.CurrentCell.ColumnIndex = dgcolhLatecoming.Index OrElse dgPolicy.CurrentCell.ColumnIndex = dgcolhEarlygoing.Index OrElse dgPolicy.CurrentCell.ColumnIndex = dgcolhLateGoing.Index) Then
                intminsec = CInt(dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value) * 60
            End If

            If dgPolicy.CurrentCell.ColumnIndex = dgcolhBaseHrs.Index Then
                mdtDays.Rows(e.RowIndex)("basehoursinsec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("basehours") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgColhOT1.Index Then
                mdtDays.Rows(e.RowIndex)("ot1insec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("ot1") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhOT2.Index Then
                mdtDays.Rows(e.RowIndex)("ot2insec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("ot2") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhOT3.Index Then
                mdtDays.Rows(e.RowIndex)("ot3insec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("ot3") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhOT4.Index Then
                mdtDays.Rows(e.RowIndex)("ot4insec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("ot4") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhEarlycoming.Index Then
                mdtDays.Rows(e.RowIndex)("elrcome_graceinsec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("elrcoming_grace") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhLatecoming.Index Then
                mdtDays.Rows(e.RowIndex)("ltcome_graceinsec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("ltcoming_grace") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhEarlygoing.Index Then
                mdtDays.Rows(e.RowIndex)("elrgoing_graceinsec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("elrgoing_grace") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhLateGoing.Index Then
                mdtDays.Rows(e.RowIndex)("ltgoing_graceinsec") = inthrsec + intminsec
                mdtDays.Rows(e.RowIndex)("ltgoing_grace") = dgPolicy.CurrentRow.Cells(e.ColumnIndex).Value.ToString()
                'S.SANDEEP [ 07 JAN 2014 ] -- START
            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhbreaktime.Index Then
                mdtDays.Rows(e.RowIndex)("breaktimeinsec") = Val(dgPolicy.CurrentRow.Cells(dgcolhbreaktime.Index).Value) * 60

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhCntBreakTimeAfter.Index Then
                mdtDays.Rows(e.RowIndex)("countbreaktimeaftersec") = Val(dgPolicy.CurrentRow.Cells(dgcolhCntBreakTimeAfter.Index).Value) * 60

                'S.SANDEEP [ 07 JAN 2014 ] -- END


                'Pinkal (28-Jan-2014) -- Start
                'Enhancement : Oman Changes

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhteatime.Index Then
                mdtDays.Rows(e.RowIndex)("teatimeinsec") = Val(dgPolicy.CurrentRow.Cells(dgcolhteatime.Index).Value) * 60

            ElseIf dgPolicy.CurrentCell.ColumnIndex = dgcolhCntTeaTimeAfter.Index Then
                mdtDays.Rows(e.RowIndex)("countteatimeaftersec") = Val(dgPolicy.CurrentRow.Cells(dgcolhCntTeaTimeAfter.Index).Value) * 60

                'Pinkal (28-Jan-2014) -- End


            End If

            If mintPolicyMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("policytranunkid")) > 0 Then
                mdtDays.Rows(e.RowIndex)("AUD") = "U"
            Else
                mdtDays.Rows(e.RowIndex)("AUD") = "A"
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPolicy_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPolicy_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgPolicy.RowValidating
        Try
            If e.RowIndex < 0 OrElse e.ColumnIndex < 0 Then Exit Sub

            SetHours(e.ColumnIndex, e.RowIndex, dgPolicy.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString())

            If (CDec(mdtDays.Rows(e.RowIndex)("basehoursinsec")) + CDec(mdtDays.Rows(e.RowIndex)("ot1insec")) + CDec(mdtDays.Rows(e.RowIndex)("ot2insec")) + _
               CDec(mdtDays.Rows(e.RowIndex)("ot3insec")) + CDec(mdtDays.Rows(e.RowIndex)("ot4insec"))) > 86400 AndAlso dgPolicy.Focused Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't be enter greater than 24 hrs per day."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))

                dgPolicy.CurrentCell.Selected = False
                If e.RowIndex > 0 Then
                    dgPolicy.Rows(e.RowIndex).Cells(dgcolhBaseHrs.Index).Selected = True
                Else
                    dgPolicy.Rows(0).Cells(dgcolhBaseHrs.Index).Selected = True
                End If
                dgPolicy.FirstDisplayedScrollingColumnIndex = dgcolhDay.Index
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPolicy_RowValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPolicy_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgPolicy.DataError

    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkCopyToAll_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyToAll.LinkClicked
        Try

            If dgPolicy.SelectedCells.Count < 0 Then Exit Sub
            Dim mintCurrentRow As Integer = dgPolicy.CurrentRow.Index

            Dim drRow() As DataRow = mdtDays.Select("1=1", "dayid asc")
            If drRow.Length > 0 Then
                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes

                For i As Integer = mintCurrentRow To mdtDays.Rows.Count - 1

                    If CBool(mdtDays.Rows(i)("isweekend")) = True Then Continue For
                    dgPolicy.Rows(i).Selected = True

                    'mdtDays.Rows(i)("basehours") = drRow(0)("basehours")
                    'dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhBaseHrs.Index)
                    'dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhBaseHrs.Index, i))

                    mdtDays.Rows(i)("ot1") = drRow(mintCurrentRow)("ot1")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgColhOT1.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgColhOT1.Index, i))

                    mdtDays.Rows(i)("ot2") = drRow(mintCurrentRow)("ot2")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhOT2.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhOT2.Index, i))

                    mdtDays.Rows(i)("ot3") = drRow(mintCurrentRow)("ot3")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhOT3.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhOT3.Index, i))

                    mdtDays.Rows(i)("ot4") = drRow(mintCurrentRow)("ot4")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhOT4.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhOT4.Index, i))

                    mdtDays.Rows(i)("elrcoming_grace") = drRow(mintCurrentRow)("elrcoming_grace")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhEarlycoming.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhEarlycoming.Index, i))

                    mdtDays.Rows(i)("ltcoming_grace") = drRow(mintCurrentRow)("ltcoming_grace")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhLatecoming.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhLatecoming.Index, i))

                    mdtDays.Rows(i)("elrgoing_grace") = drRow(mintCurrentRow)("elrgoing_grace")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhEarlygoing.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhEarlygoing.Index, i))

                    mdtDays.Rows(i)("ltgoing_grace") = drRow(mintCurrentRow)("ltgoing_grace")
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhLateGoing.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhLateGoing.Index, i))

                    'S.SANDEEP [ 07 JAN 2014 ] -- START
                    mdtDays.Rows(i)("breaktime") = drRow(mintCurrentRow)("breaktime")

                    'Pinkal (06-Aug-2015) -- Start
                    'Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
                    If dgcolhbreaktime.Visible Then
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhbreaktime.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhbreaktime.Index, i))
                    End If
                    'Pinkal (06-Aug-2015) -- End

                    mdtDays.Rows(i)("countbreaktimeafter") = drRow(mintCurrentRow)("countbreaktimeafter")

                    'Pinkal (06-Aug-2015) -- Start
                    'Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
                    If dgcolhCntBreakTimeAfter.Visible Then
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhCntBreakTimeAfter.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhCntBreakTimeAfter.Index, i))
                    End If
                    'Pinkal (06-Aug-2015) -- End

                    
                    'S.SANDEEP [ 07 JAN 2014 ] -- END



                    'Pinkal (28-Jan-2014) -- Start
                    'Enhancement : Oman Changes

                    mdtDays.Rows(i)("teatime") = drRow(mintCurrentRow)("teatime")

                    'Pinkal (06-Aug-2015) -- Start
                    'Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
                    If dgcolhteatime.Visible Then
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhteatime.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhteatime.Index, i))
                    End If
                    'Pinkal (06-Aug-2015) -- End



                    mdtDays.Rows(i)("countteatimeafter") = drRow(mintCurrentRow)("countteatimeafter")

                    'Pinkal (06-Aug-2015) -- Start
                    'Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
                    If dgcolhCntTeaTimeAfter.Visible Then
                    dgPolicy.CurrentCell = dgPolicy.Rows(i).Cells(dgcolhCntTeaTimeAfter.Index)
                    dgPolicy_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhCntTeaTimeAfter.Index, i))
                    End If
                    'Pinkal (06-Aug-2015) -- End

                    'Pinkal (28-Jan-2014) -- End


                Next

                dgPolicy.FirstDisplayedScrollingColumnIndex = dgColhOT1.Index

                'Pinkal (15-Oct-2013) -- End

                mdtDays.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyToAll_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.

    Private Sub lnkOTPenalty_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOTPenalty.LinkClicked
        Try
            Dim dtOTPenalty As DataTable = mdtOTPenalty
            If txtName.Text.Trim.Length > 0 Then
                Dim frm As New frmOT_Penalty
                frm.displayDialog(mintPolicyMasterUnkid, txtName.Text.Trim, menAction, mdtDays, dtOTPenalty)
                frm = Nothing
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Policy Code cannot be blank. Policy Code is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If
            mdtOTPenalty = dtOTPenalty
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LblOTPenalty_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-May-2016) -- End

#End Region

    


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
			Me.lblcode.Text = Language._Object.getCaption(Me.lblcode.Name, Me.lblcode.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.EZeeHeading1.Text = Language._Object.getCaption(Me.EZeeHeading1.Name, Me.EZeeHeading1.Text)
			Me.lnkCopyToAll.Text = Language._Object.getCaption(Me.lnkCopyToAll.Name, Me.lnkCopyToAll.Text)
			Me.dgcolhDay.HeaderText = Language._Object.getCaption(Me.dgcolhDay.Name, Me.dgcolhDay.HeaderText)
			Me.dgcolhWeekend.HeaderText = Language._Object.getCaption(Me.dgcolhWeekend.Name, Me.dgcolhWeekend.HeaderText)
			Me.dgcolhBaseHrs.HeaderText = Language._Object.getCaption(Me.dgcolhBaseHrs.Name, Me.dgcolhBaseHrs.HeaderText)
			Me.dgColhOT1.HeaderText = Language._Object.getCaption(Me.dgColhOT1.Name, Me.dgColhOT1.HeaderText)
			Me.dgcolhOT2.HeaderText = Language._Object.getCaption(Me.dgcolhOT2.Name, Me.dgcolhOT2.HeaderText)
			Me.dgcolhOT3.HeaderText = Language._Object.getCaption(Me.dgcolhOT3.Name, Me.dgcolhOT3.HeaderText)
			Me.dgcolhOT4.HeaderText = Language._Object.getCaption(Me.dgcolhOT4.Name, Me.dgcolhOT4.HeaderText)
			Me.dgcolhEarlycoming.HeaderText = Language._Object.getCaption(Me.dgcolhEarlycoming.Name, Me.dgcolhEarlycoming.HeaderText)
			Me.dgcolhLatecoming.HeaderText = Language._Object.getCaption(Me.dgcolhLatecoming.Name, Me.dgcolhLatecoming.HeaderText)
			Me.dgcolhEarlygoing.HeaderText = Language._Object.getCaption(Me.dgcolhEarlygoing.Name, Me.dgcolhEarlygoing.HeaderText)
			Me.dgcolhLateGoing.HeaderText = Language._Object.getCaption(Me.dgcolhLateGoing.Name, Me.dgcolhLateGoing.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Policy Code cannot be blank. Policy Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Policy Name cannot be blank. Policy Name is required information.")
			Language.setMessage(mstrModuleName, 3, " can not be greater than 24 hrs per day.")
			Language.setMessage(mstrModuleName, 4, "You can't be enter greater than 24 hrs per day.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class