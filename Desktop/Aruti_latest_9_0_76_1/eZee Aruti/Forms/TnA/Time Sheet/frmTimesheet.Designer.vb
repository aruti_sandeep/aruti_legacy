﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimesheet
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimesheet))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlTimesheet = New System.Windows.Forms.Panel
        Me.objTeaBreak = New System.Windows.Forms.Label
        Me.LblTeaBreak = New System.Windows.Forms.Label
        Me.lblTotalHours = New System.Windows.Forms.Label
        Me.objTotalHours = New System.Windows.Forms.Label
        Me.objBreak = New System.Windows.Forms.Label
        Me.lblBreak = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnLogin = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnVoid = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvTimeSheet = New System.Windows.Forms.DataGridView
        Me.colhCheckIntime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhHoldunkid = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhLogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCheckOut = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhWorkingHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhLoginunkid = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.gbShiftinfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.lnkUnhold = New System.Windows.Forms.LinkLabel
        Me.lnkHoldemployee = New System.Windows.Forms.LinkLabel
        Me.objStatus = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.dtpdate = New System.Windows.Forms.DateTimePicker
        Me.lbldate = New System.Windows.Forms.Label
        Me.objEmpLeave = New System.Windows.Forms.Label
        Me.lblleave = New System.Windows.Forms.Label
        Me.ObjHoursDay = New System.Windows.Forms.Label
        Me.objWorkedDays = New System.Windows.Forms.Label
        Me.lblHoursDay = New System.Windows.Forms.Label
        Me.lblDayWorked = New System.Windows.Forms.Label
        Me.objWorkingDays = New System.Windows.Forms.Label
        Me.objHoliday = New System.Windows.Forms.Label
        Me.lblWorkingDays = New System.Windows.Forms.Label
        Me.lblDayInMonth = New System.Windows.Forms.Label
        Me.lblHoliday = New System.Windows.Forms.Label
        Me.objDayInMonth = New System.Windows.Forms.Label
        Me.gbWorkDetail = New eZee.Common.eZeeLine
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboshift = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlTimesheet.SuspendLayout()
        Me.objFooter.SuspendLayout()
        CType(Me.dgvTimeSheet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbShiftinfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTimesheet
        '
        Me.pnlTimesheet.Controls.Add(Me.objTeaBreak)
        Me.pnlTimesheet.Controls.Add(Me.LblTeaBreak)
        Me.pnlTimesheet.Controls.Add(Me.lblTotalHours)
        Me.pnlTimesheet.Controls.Add(Me.objTotalHours)
        Me.pnlTimesheet.Controls.Add(Me.objBreak)
        Me.pnlTimesheet.Controls.Add(Me.lblBreak)
        Me.pnlTimesheet.Controls.Add(Me.objFooter)
        Me.pnlTimesheet.Controls.Add(Me.dgvTimeSheet)
        Me.pnlTimesheet.Controls.Add(Me.gbShiftinfo)
        Me.pnlTimesheet.Controls.Add(Me.EZeeHeader1)
        Me.pnlTimesheet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTimesheet.Location = New System.Drawing.Point(0, 0)
        Me.pnlTimesheet.Name = "pnlTimesheet"
        Me.pnlTimesheet.Size = New System.Drawing.Size(532, 503)
        Me.pnlTimesheet.TabIndex = 0
        '
        'objTeaBreak
        '
        Me.objTeaBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objTeaBreak.Location = New System.Drawing.Point(199, 434)
        Me.objTeaBreak.Name = "objTeaBreak"
        Me.objTeaBreak.Size = New System.Drawing.Size(64, 13)
        Me.objTeaBreak.TabIndex = 11
        Me.objTeaBreak.Text = "0"
        Me.objTeaBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblTeaBreak
        '
        Me.LblTeaBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.LblTeaBreak.Location = New System.Drawing.Point(125, 434)
        Me.LblTeaBreak.Name = "LblTeaBreak"
        Me.LblTeaBreak.Size = New System.Drawing.Size(69, 13)
        Me.LblTeaBreak.TabIndex = 10
        Me.LblTeaBreak.Text = "Tea Break"
        Me.LblTeaBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalHours
        '
        Me.lblTotalHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblTotalHours.Location = New System.Drawing.Point(389, 434)
        Me.lblTotalHours.Name = "lblTotalHours"
        Me.lblTotalHours.Size = New System.Drawing.Size(49, 13)
        Me.lblTotalHours.TabIndex = 8
        Me.lblTotalHours.Text = "Hours"
        Me.lblTotalHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotalHours
        '
        Me.objTotalHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objTotalHours.Location = New System.Drawing.Point(446, 434)
        Me.objTotalHours.Name = "objTotalHours"
        Me.objTotalHours.Size = New System.Drawing.Size(38, 13)
        Me.objTotalHours.TabIndex = 9
        Me.objTotalHours.Text = "0"
        Me.objTotalHours.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objBreak
        '
        Me.objBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objBreak.Location = New System.Drawing.Point(321, 434)
        Me.objBreak.Name = "objBreak"
        Me.objBreak.Size = New System.Drawing.Size(64, 13)
        Me.objBreak.TabIndex = 7
        Me.objBreak.Text = "0"
        Me.objBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBreak
        '
        Me.lblBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblBreak.Location = New System.Drawing.Point(267, 434)
        Me.lblBreak.Name = "lblBreak"
        Me.lblBreak.Size = New System.Drawing.Size(46, 13)
        Me.lblBreak.TabIndex = 6
        Me.lblBreak.Text = "Break"
        Me.lblBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnLogin)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnAdd)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnVoid)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 453)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(532, 50)
        Me.objFooter.TabIndex = 3
        '
        'btnLogin
        '
        Me.btnLogin.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogin.BackColor = System.Drawing.Color.White
        Me.btnLogin.BackgroundImage = CType(resources.GetObject("btnLogin.BackgroundImage"), System.Drawing.Image)
        Me.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLogin.BorderColor = System.Drawing.Color.Empty
        Me.btnLogin.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnLogin.FlatAppearance.BorderSize = 0
        Me.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogin.ForeColor = System.Drawing.Color.Black
        Me.btnLogin.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLogin.GradientForeColor = System.Drawing.Color.Black
        Me.btnLogin.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogin.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLogin.Location = New System.Drawing.Point(12, 8)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogin.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLogin.Size = New System.Drawing.Size(90, 30)
        Me.btnLogin.TabIndex = 8
        Me.btnLogin.Text = "&Login "
        Me.btnLogin.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(431, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(143, 8)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnAdd.TabIndex = 9
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(239, 8)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 10
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoid.FlatAppearance.BorderSize = 0
        Me.btnVoid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Location = New System.Drawing.Point(335, 8)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Size = New System.Drawing.Size(90, 30)
        Me.btnVoid.TabIndex = 11
        Me.btnVoid.Text = "&Void"
        Me.btnVoid.UseVisualStyleBackColor = False
        '
        'dgvTimeSheet
        '
        Me.dgvTimeSheet.AllowUserToAddRows = False
        Me.dgvTimeSheet.AllowUserToDeleteRows = False
        Me.dgvTimeSheet.AllowUserToResizeColumns = False
        Me.dgvTimeSheet.AllowUserToResizeRows = False
        Me.dgvTimeSheet.BackgroundColor = System.Drawing.Color.White
        Me.dgvTimeSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTimeSheet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhCheckIntime, Me.objcolhHoldunkid, Me.colhLogindate, Me.colhCheckOut, Me.colhWorkingHours, Me.objcolhLoginunkid})
        Me.dgvTimeSheet.Location = New System.Drawing.Point(12, 250)
        Me.dgvTimeSheet.Name = "dgvTimeSheet"
        Me.dgvTimeSheet.ReadOnly = True
        Me.dgvTimeSheet.RowHeadersVisible = False
        Me.dgvTimeSheet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTimeSheet.Size = New System.Drawing.Size(509, 177)
        Me.dgvTimeSheet.TabIndex = 7
        '
        'colhCheckIntime
        '
        Me.colhCheckIntime.HeaderText = "Check In"
        Me.colhCheckIntime.Name = "colhCheckIntime"
        Me.colhCheckIntime.ReadOnly = True
        Me.colhCheckIntime.Width = 175
        '
        'objcolhHoldunkid
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.objcolhHoldunkid.DefaultCellStyle = DataGridViewCellStyle1
        Me.objcolhHoldunkid.HeaderText = "Holdunkid"
        Me.objcolhHoldunkid.Name = "objcolhHoldunkid"
        Me.objcolhHoldunkid.ReadOnly = True
        Me.objcolhHoldunkid.Visible = False
        '
        'colhLogindate
        '
        Me.colhLogindate.HeaderText = "Logindate"
        Me.colhLogindate.Name = "colhLogindate"
        Me.colhLogindate.ReadOnly = True
        Me.colhLogindate.Visible = False
        '
        'colhCheckOut
        '
        Me.colhCheckOut.HeaderText = "Check Out"
        Me.colhCheckOut.Name = "colhCheckOut"
        Me.colhCheckOut.ReadOnly = True
        Me.colhCheckOut.Width = 175
        '
        'colhWorkingHours
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhWorkingHours.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhWorkingHours.HeaderText = "Hours"
        Me.colhWorkingHours.Name = "colhWorkingHours"
        Me.colhWorkingHours.ReadOnly = True
        Me.colhWorkingHours.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhWorkingHours.Width = 155
        '
        'objcolhLoginunkid
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F0"
        Me.objcolhLoginunkid.DefaultCellStyle = DataGridViewCellStyle3
        Me.objcolhLoginunkid.HeaderText = "Loginunkid"
        Me.objcolhLoginunkid.Name = "objcolhLoginunkid"
        Me.objcolhLoginunkid.ReadOnly = True
        Me.objcolhLoginunkid.Visible = False
        '
        'gbShiftinfo
        '
        Me.gbShiftinfo.BorderColor = System.Drawing.Color.Black
        Me.gbShiftinfo.Checked = False
        Me.gbShiftinfo.CollapseAllExceptThis = False
        Me.gbShiftinfo.CollapsedHoverImage = Nothing
        Me.gbShiftinfo.CollapsedNormalImage = Nothing
        Me.gbShiftinfo.CollapsedPressedImage = Nothing
        Me.gbShiftinfo.CollapseOnLoad = False
        Me.gbShiftinfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbShiftinfo.Controls.Add(Me.lnkAllocation)
        Me.gbShiftinfo.Controls.Add(Me.lnkUnhold)
        Me.gbShiftinfo.Controls.Add(Me.lnkHoldemployee)
        Me.gbShiftinfo.Controls.Add(Me.objStatus)
        Me.gbShiftinfo.Controls.Add(Me.lblStatus)
        Me.gbShiftinfo.Controls.Add(Me.dtpdate)
        Me.gbShiftinfo.Controls.Add(Me.lbldate)
        Me.gbShiftinfo.Controls.Add(Me.objEmpLeave)
        Me.gbShiftinfo.Controls.Add(Me.lblleave)
        Me.gbShiftinfo.Controls.Add(Me.ObjHoursDay)
        Me.gbShiftinfo.Controls.Add(Me.objWorkedDays)
        Me.gbShiftinfo.Controls.Add(Me.lblHoursDay)
        Me.gbShiftinfo.Controls.Add(Me.lblDayWorked)
        Me.gbShiftinfo.Controls.Add(Me.objWorkingDays)
        Me.gbShiftinfo.Controls.Add(Me.objHoliday)
        Me.gbShiftinfo.Controls.Add(Me.lblWorkingDays)
        Me.gbShiftinfo.Controls.Add(Me.lblDayInMonth)
        Me.gbShiftinfo.Controls.Add(Me.lblHoliday)
        Me.gbShiftinfo.Controls.Add(Me.objDayInMonth)
        Me.gbShiftinfo.Controls.Add(Me.gbWorkDetail)
        Me.gbShiftinfo.Controls.Add(Me.cboEmployee)
        Me.gbShiftinfo.Controls.Add(Me.lblEmployee)
        Me.gbShiftinfo.Controls.Add(Me.cboshift)
        Me.gbShiftinfo.Controls.Add(Me.Label1)
        Me.gbShiftinfo.ExpandedHoverImage = Nothing
        Me.gbShiftinfo.ExpandedNormalImage = Nothing
        Me.gbShiftinfo.ExpandedPressedImage = Nothing
        Me.gbShiftinfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShiftinfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShiftinfo.HeaderHeight = 25
        Me.gbShiftinfo.HeaderMessage = ""
        Me.gbShiftinfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbShiftinfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShiftinfo.HeightOnCollapse = 0
        Me.gbShiftinfo.LeftTextSpace = 0
        Me.gbShiftinfo.Location = New System.Drawing.Point(12, 66)
        Me.gbShiftinfo.Name = "gbShiftinfo"
        Me.gbShiftinfo.OpenHeight = 300
        Me.gbShiftinfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShiftinfo.ShowBorder = True
        Me.gbShiftinfo.ShowCheckBox = False
        Me.gbShiftinfo.ShowCollapseButton = False
        Me.gbShiftinfo.ShowDefaultBorderColor = True
        Me.gbShiftinfo.ShowDownButton = False
        Me.gbShiftinfo.ShowHeader = True
        Me.gbShiftinfo.Size = New System.Drawing.Size(509, 177)
        Me.gbShiftinfo.TabIndex = 1
        Me.gbShiftinfo.Temp = 0
        Me.gbShiftinfo.Text = "Shift Information"
        Me.gbShiftinfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(257, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 106
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(421, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 14)
        Me.lnkAllocation.TabIndex = 104
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'lnkUnhold
        '
        Me.lnkUnhold.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkUnhold.Location = New System.Drawing.Point(379, 155)
        Me.lnkUnhold.Name = "lnkUnhold"
        Me.lnkUnhold.Size = New System.Drawing.Size(119, 15)
        Me.lnkUnhold.TabIndex = 6
        Me.lnkUnhold.TabStop = True
        Me.lnkUnhold.Text = "UnHold Employee"
        Me.lnkUnhold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkUnhold.Visible = False
        '
        'lnkHoldemployee
        '
        Me.lnkHoldemployee.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkHoldemployee.Location = New System.Drawing.Point(209, 156)
        Me.lnkHoldemployee.Name = "lnkHoldemployee"
        Me.lnkHoldemployee.Size = New System.Drawing.Size(120, 14)
        Me.lnkHoldemployee.TabIndex = 5
        Me.lnkHoldemployee.TabStop = True
        Me.lnkHoldemployee.Text = "Hold Employee"
        Me.lnkHoldemployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkHoldemployee.Visible = False
        '
        'objStatus
        '
        Me.objStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objStatus.Location = New System.Drawing.Point(134, 157)
        Me.objStatus.Name = "objStatus"
        Me.objStatus.Size = New System.Drawing.Size(75, 13)
        Me.objStatus.TabIndex = 36
        Me.objStatus.Text = "On Hold"
        Me.objStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.objStatus.Visible = False
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(26, 157)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(93, 13)
        Me.lblStatus.TabIndex = 35
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblStatus.Visible = False
        '
        'dtpdate
        '
        Me.dtpdate.Checked = False
        Me.dtpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpdate.Location = New System.Drawing.Point(81, 30)
        Me.dtpdate.Name = "dtpdate"
        Me.dtpdate.Size = New System.Drawing.Size(110, 21)
        Me.dtpdate.TabIndex = 1
        '
        'lbldate
        '
        Me.lbldate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldate.Location = New System.Drawing.Point(8, 31)
        Me.lbldate.Name = "lbldate"
        Me.lbldate.Size = New System.Drawing.Size(58, 17)
        Me.lbldate.TabIndex = 30
        Me.lbldate.Text = "Date"
        Me.lbldate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objEmpLeave
        '
        Me.objEmpLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objEmpLeave.Location = New System.Drawing.Point(298, 133)
        Me.objEmpLeave.Name = "objEmpLeave"
        Me.objEmpLeave.Size = New System.Drawing.Size(31, 13)
        Me.objEmpLeave.TabIndex = 28
        Me.objEmpLeave.Text = "0"
        Me.objEmpLeave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblleave
        '
        Me.lblleave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblleave.Location = New System.Drawing.Point(209, 133)
        Me.lblleave.Name = "lblleave"
        Me.lblleave.Size = New System.Drawing.Size(79, 13)
        Me.lblleave.TabIndex = 27
        Me.lblleave.Text = "Leave"
        Me.lblleave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ObjHoursDay
        '
        Me.ObjHoursDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.ObjHoursDay.Location = New System.Drawing.Point(458, 109)
        Me.ObjHoursDay.Name = "ObjHoursDay"
        Me.ObjHoursDay.Size = New System.Drawing.Size(40, 12)
        Me.ObjHoursDay.TabIndex = 25
        Me.ObjHoursDay.Text = "0"
        Me.ObjHoursDay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWorkedDays
        '
        Me.objWorkedDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objWorkedDays.Location = New System.Drawing.Point(458, 132)
        Me.objWorkedDays.Name = "objWorkedDays"
        Me.objWorkedDays.Size = New System.Drawing.Size(40, 12)
        Me.objWorkedDays.TabIndex = 24
        Me.objWorkedDays.Text = "0"
        Me.objWorkedDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHoursDay
        '
        Me.lblHoursDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblHoursDay.Location = New System.Drawing.Point(379, 109)
        Me.lblHoursDay.Name = "lblHoursDay"
        Me.lblHoursDay.Size = New System.Drawing.Size(81, 12)
        Me.lblHoursDay.TabIndex = 19
        Me.lblHoursDay.Text = "Hours/Day"
        Me.lblHoursDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDayWorked
        '
        Me.lblDayWorked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblDayWorked.Location = New System.Drawing.Point(379, 132)
        Me.lblDayWorked.Name = "lblDayWorked"
        Me.lblDayWorked.Size = New System.Drawing.Size(81, 12)
        Me.lblDayWorked.TabIndex = 23
        Me.lblDayWorked.Text = "Days Worked"
        Me.lblDayWorked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objWorkingDays
        '
        Me.objWorkingDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objWorkingDays.Location = New System.Drawing.Point(298, 110)
        Me.objWorkingDays.Name = "objWorkingDays"
        Me.objWorkingDays.Size = New System.Drawing.Size(31, 13)
        Me.objWorkingDays.TabIndex = 18
        Me.objWorkingDays.Text = "0"
        Me.objWorkingDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objHoliday
        '
        Me.objHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objHoliday.Location = New System.Drawing.Point(134, 132)
        Me.objHoliday.Name = "objHoliday"
        Me.objHoliday.Size = New System.Drawing.Size(31, 13)
        Me.objHoliday.TabIndex = 22
        Me.objHoliday.Text = "0"
        Me.objHoliday.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWorkingDays
        '
        Me.lblWorkingDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblWorkingDays.Location = New System.Drawing.Point(209, 110)
        Me.lblWorkingDays.Name = "lblWorkingDays"
        Me.lblWorkingDays.Size = New System.Drawing.Size(79, 13)
        Me.lblWorkingDays.TabIndex = 17
        Me.lblWorkingDays.Text = "Working Days"
        Me.lblWorkingDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDayInMonth
        '
        Me.lblDayInMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblDayInMonth.Location = New System.Drawing.Point(26, 110)
        Me.lblDayInMonth.Name = "lblDayInMonth"
        Me.lblDayInMonth.Size = New System.Drawing.Size(93, 13)
        Me.lblDayInMonth.TabIndex = 15
        Me.lblDayInMonth.Text = "Days in Month"
        Me.lblDayInMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHoliday
        '
        Me.lblHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblHoliday.Location = New System.Drawing.Point(26, 133)
        Me.lblHoliday.Name = "lblHoliday"
        Me.lblHoliday.Size = New System.Drawing.Size(93, 13)
        Me.lblHoliday.TabIndex = 21
        Me.lblHoliday.Text = "Holiday"
        Me.lblHoliday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objDayInMonth
        '
        Me.objDayInMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objDayInMonth.Location = New System.Drawing.Point(134, 109)
        Me.objDayInMonth.Name = "objDayInMonth"
        Me.objDayInMonth.Size = New System.Drawing.Size(31, 13)
        Me.objDayInMonth.TabIndex = 16
        Me.objDayInMonth.Text = "0"
        Me.objDayInMonth.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gbWorkDetail
        '
        Me.gbWorkDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbWorkDetail.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.gbWorkDetail.Location = New System.Drawing.Point(8, 89)
        Me.gbWorkDetail.Name = "gbWorkDetail"
        Me.gbWorkDetail.Size = New System.Drawing.Size(490, 14)
        Me.gbWorkDetail.TabIndex = 7
        Me.gbWorkDetail.Text = "Work Detail"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(81, 59)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(170, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 61)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(58, 17)
        Me.lblEmployee.TabIndex = 5
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboshift
        '
        Me.cboshift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboshift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboshift.FormattingEnabled = True
        Me.cboshift.Location = New System.Drawing.Point(341, 59)
        Me.cboshift.Name = "cboshift"
        Me.cboshift.Size = New System.Drawing.Size(157, 21)
        Me.cboshift.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(287, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Shift"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(532, 60)
        Me.EZeeHeader1.TabIndex = 0
        Me.EZeeHeader1.Title = "Timesheet Information"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Check In"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Break In"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.Visible = False
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn3.HeaderText = "Break Out"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn4.HeaderText = "Check Out"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.Width = 155
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Hours"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'frmTimesheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(532, 503)
        Me.Controls.Add(Me.pnlTimesheet)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTimesheet"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Timesheet"
        Me.pnlTimesheet.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvTimeSheet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbShiftinfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTimesheet As System.Windows.Forms.Panel
    Friend WithEvents gbShiftinfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gbWorkDetail As eZee.Common.eZeeLine
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboshift As System.Windows.Forms.ComboBox
    Friend WithEvents objWorkedDays As System.Windows.Forms.Label
    Friend WithEvents lblHoursDay As System.Windows.Forms.Label
    Friend WithEvents lblDayWorked As System.Windows.Forms.Label
    Friend WithEvents objWorkingDays As System.Windows.Forms.Label
    Friend WithEvents objHoliday As System.Windows.Forms.Label
    Friend WithEvents lblWorkingDays As System.Windows.Forms.Label
    Friend WithEvents lblDayInMonth As System.Windows.Forms.Label
    Friend WithEvents lblHoliday As System.Windows.Forms.Label
    Friend WithEvents objDayInMonth As System.Windows.Forms.Label
    Friend WithEvents ObjHoursDay As System.Windows.Forms.Label
    Friend WithEvents objEmpLeave As System.Windows.Forms.Label
    Friend WithEvents lblleave As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnVoid As eZee.Common.eZeeLightButton
    Friend WithEvents lblTotalHours As System.Windows.Forms.Label
    Friend WithEvents objTotalHours As System.Windows.Forms.Label
    Friend WithEvents objBreak As System.Windows.Forms.Label
    Friend WithEvents lblBreak As System.Windows.Forms.Label
    Friend WithEvents dtpdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbldate As System.Windows.Forms.Label
    Friend WithEvents btnLogin As eZee.Common.eZeeLightButton
    Friend WithEvents objStatus As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lnkHoldemployee As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkUnhold As System.Windows.Forms.LinkLabel
    Friend WithEvents dgvTimeSheet As System.Windows.Forms.DataGridView
    Friend WithEvents colhCheckIntime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhHoldunkid As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhLogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCheckOut As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhWorkingHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhLoginunkid As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objTeaBreak As System.Windows.Forms.Label
    Friend WithEvents LblTeaBreak As System.Windows.Forms.Label
End Class
