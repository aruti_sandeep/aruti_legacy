﻿Option Strict Off

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmProcessPendingLoanList

#Region " Private Varaibles "
    Private objLoanAdvanceunkid As clsLoan_Advance
    Private objProcesspendingloan As clsProcess_pending_loan
    Private ReadOnly mstrModuleName As String = "frmProcessPendingLoanList"
    'Sandeep [ 23 Oct 2010 ] -- Start
    'Private mintSeletedValue As Integer = 0
    Private mintSeletedValue As Integer = 1
    'Sandeep [ 23 Oct 2010 ] -- End 
#End Region

#Region " Private Function "

    Private Sub FillList()
        Dim dsProcessPendingLoan As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Dim objExchangeRate As New clsExchangeRate
        Try

            If User._Object.Privilege._AllowToViewProcessLoanAdvanceList = True Then    'Pinkal (02-Jul-2012) -- Start

            dsProcessPendingLoan = objProcesspendingloan.GetList("Loan")

            If txtApplicationNo.Text.Trim <> "" Then
                StrSearching &= "AND Application_No like '" & txtApplicationNo.Text.Trim & "%' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                StrSearching &= "AND Loan_AdvanceUnkid = " & CInt(cboLoanAdvance.SelectedIndex) & " "
            End If

            If txtAmount.Text.Trim <> "" Then
                If txtAmount.Decimal > 0 Then
                    StrSearching &= "AND Amount = " & txtAmount.Decimal & " "
                End If
            End If

            If txtApprovedAmount.Text.Trim <> "" Then
                If txtApprovedAmount.Decimal > 0 Then 'Sohail (11 May 2011)
                    StrSearching &= "AND Approved_Amount = " & txtApprovedAmount.Decimal & " "
                End If
            End If

            'If txtRemark.Text.Trim <> "" Then
            '    StrSearching &= "AND Remark like '" & txtRemark.Text.Trim & "%' "
            'End If

            If dtpAppDate.Checked Then
                StrSearching &= "AND application_date='" & eZeeDate.convertDate(dtpAppDate.Value) & "' "
            End If

            'If txtRemark.Text.Trim.Length > 0 Then
            '    StrSearching &= "AND remark like '" & txtRemark.Text.Trim & "%' "
            'End If


            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsProcessPendingLoan.Tables("Loan"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsProcessPendingLoan.Tables("Loan")
            End If


            lvPendingLoanAdvance.Items.Clear()
            objExchangeRate._ExchangeRateunkid = 1
            Dim lvItem As New ListViewItem

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("Application_No").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("application_date").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                lvItem.SubItems.Add(dtRow.Item("LoanScheme").ToString)
                lvItem.SubItems.Add(dtRow.Item("Loan_Advance").ToString)
                lvItem.SubItems.Add(Format(CDec(dtRow.Item("Amount").ToString), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)
                lvItem.SubItems.Add(Format(CDec(dtRow.Item("Approved_Amount").ToString), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)
                RemoveHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                cboStatus.SelectedValue = CInt(dtRow.Item("loan_statusunkid").ToString)
                AddHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                lvItem.SubItems.Add(cboStatus.Text.ToString)
                'lvItem.SubItems.Add(dtRow.Item("Remark").ToString)
                lvItem.SubItems.Add(dtRow.Item("isloan").ToString)
                lvItem.SubItems.Add(dtRow.Item("loan_statusunkid").ToString)
                lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                lvItem.SubItems.Add(dtRow.Item("loanschemeunkid").ToString)
                lvItem.SubItems.Add(dtRow.Item("approverunkid").ToString)
                'Sandeep [ 21 Aug 2010 ] -- Start
                lvItem.SubItems.Add(dtRow.Item("isexternal_entity").ToString)
                'Sandeep [ 21 Aug 2010 ] -- End 

                'lvItem.Text = dtRow.Item("EmpName").ToString
                'lvItem.SubItems.Add(dtRow.Item("LoanScheme").ToString)
                'lvItem.SubItems.Add(dtRow.Item("Loan_Advance").ToString)
                'lvItem.SubItems.Add(dtRow.Item("Amount").ToString)
                'RemoveHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                'cboStatus.SelectedValue = CInt(dtRow.Item("loan_statusunkid").ToString)
                'AddHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                'lvItem.SubItems.Add(cboStatus.Text.ToString)
                'lvItem.SubItems.Add(dtRow.Item("Remark").ToString)
                'lvItem.SubItems.Add(dtRow.Item("isloan").ToString)
                'lvItem.SubItems.Add(dtRow.Item("loan_statusunkid").ToString)
                'lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                'lvItem.SubItems.Add(dtRow.Item("loanschemeunkid").ToString)
                'lvItem.SubItems.Add(dtRow.Item("approverunkid").ToString)
                'lvItem.SubItems.Add(dtRow.Item("Approved_Amount").ToString)
                'lvItem.SubItems.Add(dtRow.Item("Application_No").ToString)
                'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow("application_date").ToString).ToShortDateString)
                '' lvItem.SubItems.Add(dtRow.Item("approved_amount").ToString)

                lvItem.Tag = dtRow.Item("processpendingloanunkid")

                lvPendingLoanAdvance.Items.Add(lvItem)
            Next

            cboStatus.SelectedValue = mintSeletedValue

            If lvPendingLoanAdvance.Items.Count > 16 Then
                colhEmployee.Width = 150 - 20
            Else
                colhEmployee.Width = 150
            End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        ' Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Employee", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True)
            'End If
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With

            dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With


            'Sandeep [ 21 Aug 2010 ] -- Start
            'dsCombos = objProcesspendingloan.GetLoan_Status("Status")
            'Sandeep [ 23 Oct 2010 ] -- Start
            'dsCombos = objProcesspendingloan.GetLoan_Status("Status", True)
            dsCombos = objProcesspendingloan.GetLoan_Status("Status", , True)
            'Sandeep [ 23 Oct 2010 ] -- End 
            'Sandeep [ 21 Aug 2010 ] -- End 
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Status")
                'Sandeep [ 23 Oct 2010 ] -- Start
                '.SelectedValue = 0
                .SelectedValue = 1
                'Sandeep [ 23 Oct 2010 ] -- End 
            End With

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 2, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 3, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub Generate_MenuItems()
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objProcesspendingloan.GetLoan_Status("Status", False, False)
            cmnuStatus.Items.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                Dim objbtnpopupFCStatus As New ToolStripMenuItem
                objbtnpopupFCStatus.Name = "objbtnpopupFCStatus" & i
                objbtnpopupFCStatus.Text = dsList.Tables(0).Rows(i)("Name").ToString
                objbtnpopupFCStatus.Tag = dsList.Tables(0).Rows(i)("Id")
                AddHandler objbtnpopupFCStatus.Click, AddressOf FCStatus_Click
                cmnuStatus.Items.Add(objbtnpopupFCStatus)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
        End Try
    End Sub

    Private Sub FCStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmProcessPendingLoan_AddEdit
        Try

            If lvPendingLoanAdvance.SelectedItems.Count <= 0 Then Exit Sub

            If objProcesspendingloan.Get_Id_Used_In_Paymet(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance is already paid to Employee."), enMsgBoxStyle.Information)
                lvPendingLoanAdvance.Select()
                Exit Sub
            End If

            If objProcesspendingloan.Get_Id_Used_In_LoanAdvance(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance amount is assigned to particular employee."), enMsgBoxStyle.Information)
                lvPendingLoanAdvance.Select()
                Exit Sub
            End If

            Select Case CInt(lvPendingLoanAdvance.SelectedItems(0).SubItems(objcolhStatusUnkid.Index).Text)
                Case 3
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance status is Rejected."), enMsgBoxStyle.Information)
                    lvPendingLoanAdvance.Select()
                    Exit Sub
                Case Else
                    If frm.displayDialog(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag), enAction.EDIT_ONE, sender.tag) Then
                        Call FillList()
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FCStatus_Click", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddPendingLoan
            btnEdit.Enabled = User._Object.Privilege._EditPendingLoan
            btnDelete.Enabled = User._Object.Privilege._DeletePendingLoan
            'S.SANDEEP [ 06 SEP 2011 ] -- START
            'ENHANCEMENT : MAKING COMMON FUNCTION
            'btnAssignLoan.Enabled = User._Object.Privilege._AllowAssignPendingLoan
            mnuAssign.Enabled = User._Object.Privilege._AllowAssignPendingLoan
            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            btnChangeStatus.Enabled = User._Object.Privilege._AllowChangePendingLoanAdvanceStatus


            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuGlobalApprove.Enabled = User._Object.Privilege._AllowtoApproveLoan
            mnuGlobalAssign.Enabled = User._Object.Privilege._AllowAssignPendingLoan
            'Anjan (25 Oct 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " From's Events "

    Private Sub frmProcessPendingLoanList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProcesspendingloan = New clsProcess_pending_loan
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()

            Call FillCombo()
            'Call FillList()
            'Sandeep [ 21 Aug 2010 ] -- Start
            Call Generate_MenuItems()
            'Sandeep [ 21 Aug 2010 ] -- End 

            If lvPendingLoanAdvance.Items.Count > 0 Then lvPendingLoanAdvance.Items(0).Selected = True
            lvPendingLoanAdvance.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessPendingLoanList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProcessPendingLoanList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Delete And lvPendingLoanAdvance.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmProcessPendingLoanList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmProcessPendingLoanList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objProcesspendingloan = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboLoanAdvance.SelectedIndex = 0
            cboLoanScheme.SelectedValue = 0
            'Sandeep [ 23 Oct 2010 ] -- Start
            'cboStatus.SelectedValue = 0
            cboStatus.SelectedValue = 1
            'Sandeep [ 23 Oct 2010 ] -- End 
            txtAmount.Text = ""
            txtApprovedAmount.Text = ""
            txtApplicationNo.Text = ""
            dtpAppDate.Checked = False

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvPendingLoanAdvance.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Loan/Advance Application from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvPendingLoanAdvance.Select()
                Exit Sub
            End If

            objProcesspendingloan._Processpendingloanunkid = CInt(lvPendingLoanAdvance.SelectedItems(0).Tag)


            Select Case objProcesspendingloan._Loan_Statusunkid
                Case 2, 3
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance status is Approved or Rejected."), enMsgBoxStyle.Information)
                    Exit Sub
                    'Anjan (04 Apr 2011)-Start
                Case 4
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance status is Assigned."), enMsgBoxStyle.Information)
                    Exit Sub
                    'Anjan (04 Apr 2011)-End
            End Select

            'Anjan (28 Mar 2011)-Start
            If objProcesspendingloan._Application_Date.Date < FinancialYear._Object._Database_Start_Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance has been brought forwarded."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Anjan (28 Mar 2011)-End

            'If objLoan_Advance._Isloan = True Then
            '    If objLoan_Advance._Balance_Amount <= objLoan_Advance._Net_Amount Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'Else
            '    If objLoan_Advance._Balance_Amount <= objLoan_Advance._Advance_Amount Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If


            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPendingLoanAdvance.SelectedItems(0).Index

            'If objLoan_Advance.isUsed(CInt(strTag(0))) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Loan. Reason: This Loan is in use."), enMsgBoxStyle.Information) '?2
            '    lvLoanAdvance.Select()
            '    Exit Sub
            'End If

            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvLoanAdvance.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete this Loan/Advance Application?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objProcesspendingloan._Isvoid = True

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objProcesspendingloan._Voidreason = "Testing"
                'objProcesspendingloan._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LOAN, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objProcesspendingloan._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objProcesspendingloan._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 
                objProcesspendingloan._Voiduserunkid = User._Object._Userunkid

                objProcesspendingloan.Delete(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag))
                lvPendingLoanAdvance.SelectedItems(0).Remove()

                If lvPendingLoanAdvance.Items.Count <= 0 Then
                    Exit Try
                End If

                'If lvPendingLoanAdvance.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvPendingLoanAdvance.Items.Count - 1
                '    lvPendingLoanAdvance.Items(intSelectedIndex).Selected = True
                '    lvPendingLoanAdvance.EnsureVisible(intSelectedIndex)
                'ElseIf lvPendingLoanAdvance.Items.Count <> 0 Then
                '    lvPendingLoanAdvance.Items(intSelectedIndex).Selected = True
                '    lvPendingLoanAdvance.EnsureVisible(intSelectedIndex)
                'End If
            End If
            lvPendingLoanAdvance.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmProcessPendingLoan_AddEdit
        Try
            If lvPendingLoanAdvance.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Loan/Advance Application from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvPendingLoanAdvance.Select()
                Exit Sub
            End If


            If objProcesspendingloan.Get_Id_Used_In_Paymet(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance is already paid to Employee."), enMsgBoxStyle.Information)
                lvPendingLoanAdvance.Select()
                Exit Sub
            End If

            If objProcesspendingloan.Get_Id_Used_In_LoanAdvance(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance amount is assigned to particular employee."), enMsgBoxStyle.Information)
                lvPendingLoanAdvance.Select()
                Exit Sub
            End If

           
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPendingLoanAdvance.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Select Case CInt(lvPendingLoanAdvance.SelectedItems(0).SubItems(objcolhStatusUnkid.Index).Text)
                Case 3
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance status is Rejected."), enMsgBoxStyle.Information)
                    lvPendingLoanAdvance.Select()
                    Exit Sub
            End Select

            If frm.displayDialog(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            frm = Nothing

            'lvPendingLoanAdvance.Items(intSelectedIndex).Selected = True
            'lvPendingLoanAdvance.EnsureVisible(intSelectedIndex)
            'lvPendingLoanAdvance.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)

        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmProcessPendingLoan_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 06 SEP 2011 ] -- START
    'ENHANCEMENT : MAKING COMMON FUNCTION
    Private Sub mnuAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssign.Click
        Dim frm As New frmLoanAdvance_AddEdit
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If lvPendingLoanAdvance.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "please select loan/advance application from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvPendingLoanAdvance.Select()
                Exit Sub
            End If

            If objProcesspendingloan.Get_Id_Used_In_LoanAdvance(CInt(lvPendingLoanAdvance.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "this loan/advance amount is already assigned to particular employee."), enMsgBoxStyle.Information)
            Else

                frm._ApprovedAmount = CDec(lvPendingLoanAdvance.SelectedItems(0).SubItems(colhApp_Amount.Index).Text) 'sohail (11 may 2011)
                frm._ApproverId = CInt(lvPendingLoanAdvance.SelectedItems(0).SubItems(objcolhApprover.Index).Text)
                frm._Employeeid = CInt(lvPendingLoanAdvance.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
                frm._IsItLoan = CBool(lvPendingLoanAdvance.SelectedItems(0).SubItems(colhIsLoan.Index).Text)
                frm._LoanSchemeId = CInt(lvPendingLoanAdvance.SelectedItems(0).SubItems(objcolhSchemeId.Index).Text)
                frm._IsFromAssign = True
                frm._IsExternalEntity = CBool(lvPendingLoanAdvance.SelectedItems(0).SubItems(objcolhIsExternal.Index).Text)

                If frm.displayDialog(-1, enAction.ADD_ONE, CInt(lvPendingLoanAdvance.SelectedItems(0).Tag)) Then
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Call FillList() : Me.Close()
                    Call FillList()
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                End If
                End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGlobalApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalApprove.Click
        Try
            Dim frm As New frmGlobalApproveLoan

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

            Call FillList()

            frm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalApprove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
        Try
            Dim frm As New frmGlobalAssignLoanAdvance
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 06 SEP 2011 ] -- END 


#End Region

#Region " Controls "

    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboStatus_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectionChangeCommitted
        Try
            mintSeletedValue = CInt(cboStatus.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub lvLoanAdvance_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPendingLoanAdvance.SelectedIndexChanged
        Try
            If lvPendingLoanAdvance.SelectedItems.Count > 0 Then
                Select Case CInt(lvPendingLoanAdvance.SelectedItems(0).SubItems(objcolhStatusUnkid.Index).Text)
                    Case 1 'Pending
                        'S.SANDEEP [ 06 SEP 2011 ] -- START
                        'ENHANCEMENT : MAKING COMMON FUNCTION
                        'btnAssignLoan.Enabled = False
                        mnuAssign.Enabled = False
                        'S.SANDEEP [ 06 SEP 2011 ] -- END 
                    Case 2 'Approved
                        'S.SANDEEP [ 06 SEP 2011 ] -- START
                        'ENHANCEMENT : MAKING COMMON FUNCTION
                        'btnAssignLoan.Enabled = True
                        mnuAssign.Enabled = True
                        'S.SANDEEP [ 06 SEP 2011 ] -- END 
                    Case 3 'Rejected
                        'S.SANDEEP [ 06 SEP 2011 ] -- START
                        'ENHANCEMENT : MAKING COMMON FUNCTION
                        'btnAssignLoan.Enabled = False
                        mnuAssign.Enabled = False
                        'S.SANDEEP [ 06 SEP 2011 ] -- END 
                        'Anjan (04 Apr 2011)-Start
                    Case 4
                        'S.SANDEEP [ 06 SEP 2011 ] -- START
                        'ENHANCEMENT : MAKING COMMON FUNCTION
                        'btnAssignLoan.Enabled = False
                        mnuAssign.Enabled = False
                        'S.SANDEEP [ 06 SEP 2011 ] -- END 
                        'Anjan (04 Apr 2011)-End
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLoanAdvance_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor 
			Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhLoanScheme.Text = Language._Object.getCaption(CStr(Me.colhLoanScheme.Tag), Me.colhLoanScheme.Text)
			Me.colhLoan_Advance.Text = Language._Object.getCaption(CStr(Me.colhLoan_Advance.Tag), Me.colhLoan_Advance.Text)
			Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.colhIsLoan.Text = Language._Object.getCaption(CStr(Me.colhIsLoan.Tag), Me.colhIsLoan.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhApp_Amount.Text = Language._Object.getCaption(CStr(Me.colhApp_Amount.Tag), Me.colhApp_Amount.Text)
			Me.lblApprovedAmount.Text = Language._Object.getCaption(Me.lblApprovedAmount.Name, Me.lblApprovedAmount.Text)
			Me.colhApplicationNo.Text = Language._Object.getCaption(CStr(Me.colhApplicationNo.Tag), Me.colhApplicationNo.Text)
			Me.lblAppNo.Text = Language._Object.getCaption(Me.lblAppNo.Name, Me.lblAppNo.Text)
			Me.lblAppDate.Text = Language._Object.getCaption(Me.lblAppDate.Name, Me.lblAppDate.Text)
			Me.colhAppDate.Text = Language._Object.getCaption(CStr(Me.colhAppDate.Tag), Me.colhAppDate.Text)
			Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuAssign.Text = Language._Object.getCaption(Me.mnuAssign.Name, Me.mnuAssign.Text)
			Me.mnuGlobalApprove.Text = Language._Object.getCaption(Me.mnuGlobalApprove.Name, Me.mnuGlobalApprove.Text)
			Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Loan")
			Language.setMessage(mstrModuleName, 3, "Advance")
			Language.setMessage(mstrModuleName, 4, "Please select Loan/Advance Application from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance status is Approved or Rejected.")
			Language.setMessage(mstrModuleName, 6, "Are you sure you want to delete this Loan/Advance Application?")
			Language.setMessage(mstrModuleName, 7, "Please select Loan/Advance Application from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 8, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance is already paid to Employee.")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance amount is assigned to particular employee.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot edit Loan/Advance Application. Reason : Loan/Advance status is Rejected.")
			Language.setMessage(mstrModuleName, 11, "please select loan/advance application from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 12, "this loan/advance amount is already assigned to particular employee.")
			Language.setMessage(mstrModuleName, 13, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance status is Assigned.")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance has been brought forwarded.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class