﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalApproveLoan

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalApproveLoan"
    Private objPendingLoan As clsProcess_pending_loan
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private dtTab As DataTable

    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objDMaster As New clsDepartment
        Dim objBMaster As New clsStation
        Dim objSMaster As New clsSections
        Dim objJMaster As New clsJobs
        Dim objAMaster As New clsLoan_Approver
        Try
            dsCombo = objDMaster.getComboList("List", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objBMaster.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objSMaster.getComboList("List", True)
            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objJMaster.getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objAMaster.GetList("List", True, True, True)
            With cboApprover
                .ValueMember = "approverunkid"
                .DisplayMember = "approvername"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objPendingLoan.GetLoan_Status("List", True)
            Dim dtTemp() As DataRow = dsCombo.Tables("List").Select("Id NOT IN(2)")
            For i As Integer = 0 To dtTemp.Length - 1
                dsCombo.Tables("List").Rows.Remove(dtTemp(i))
            Next
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 2
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            dsCombo.Dispose() : objAMaster = Nothing : objBMaster = Nothing : objDMaster = Nothing _
            : objJMaster = Nothing : objSMaster = Nothing
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim intMode As Integer

            If radAdvance.Checked Then
                intMode = 0
            ElseIf radLoan.Checked Then
                intMode = 1
            ElseIf radAll.Checked Then
                intMode = -1
            End If

            dtTab = objPendingLoan.GetDataList(1, _
                                               CInt(cboDepartment.SelectedValue), _
                                               CInt(cboBranch.SelectedValue), _
                                               CInt(cboSection.SelectedValue), _
                                               CInt(cboJob.SelectedValue), _
                                               intMode)
            dgvData.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhAAmount.DataPropertyName = "AppAmount"
            dgcolhAppNo.DataPropertyName = "AppNo"
            dgcolhEcode.DataPropertyName = "ECode"
            dgcolhEName.DataPropertyName = "EName"
            dgcolhRAmount.DataPropertyName = "Amount"
            objcolhPendingUnkid.DataPropertyName = "PId"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhGrpId.DataPropertyName = "GrpId"

            dgvData.DataSource = dtTab

            dgcolhAAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhRAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhAAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhAAmount.DefaultCellStyle.ForeColor = Color.Blue
            dgcolhAAmount.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
            dgcolhAAmount.DefaultCellStyle.SelectionBackColor = Color.White
            dgcolhAAmount.DefaultCellStyle.SelectionForeColor = Color.Red

            Call SetGridColor()
            Call SetCollapseValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                    dgvData.Rows(i).Cells(dgcolhAAmount.Index).ReadOnly = True
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        'dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = "▬"
                        dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmGlobalApproveLoan_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPendingLoan = New clsProcess_pending_loan
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            radAll.Checked = True
            Call FillCombo()
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalApproveLoan_Load", mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(cboApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
                cboApprover.Focus()
                Exit Sub
            End If

            Dim dtTemp() As DataRow = Nothing
            dtTemp = dtTab.Select("IsChecked = 1 And IsGrp = false")
            If dtTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one information to Approve."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            For i As Integer = 0 To dtTemp.Length - 1
                objPendingLoan._Processpendingloanunkid = CInt(dtTemp(i).Item("PId"))
                objPendingLoan._Approverunkid = CInt(cboApprover.SelectedValue)
                objPendingLoan._Loan_Statusunkid = CInt(cboStatus.SelectedValue)
                objPendingLoan._Approved_Amount = CDec(dtTemp(i).Item("AppAmount"))
                objPendingLoan.Update()
            Next

            Call FillGrid()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged, _
                                                                                                                   cboDepartment.SelectedIndexChanged, _
                                                                                                                   cboJob.SelectedIndexChanged, _
                                                                                                                   cboSection.SelectedIndexChanged
        Try
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 1
                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            Dim blnFlg As Boolean = CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex = -1 Then Exit Sub

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then Exit Sub

            If IsDBNull(dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value) Then
                dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value = dgvData.Rows(e.RowIndex).Cells(dgcolhRAmount.Index).Value
            ElseIf CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value) <= 0 Or (CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value) > CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhRAmount.Index).Value)) Then
                dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value = dgvData.Rows(e.RowIndex).Cells(dgcolhRAmount.Index).Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvData.EditingControlShowing
        Try
            If (Me.dgvData.CurrentCell.ColumnIndex = 6) And Not e.Control Is Nothing Then
                Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                AddHandler txt.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpCashDenomination_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub radAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAll.CheckedChanged
        Try
            If radAll.Checked Then Call FillGrid()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radAdvance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAdvance.CheckedChanged
        Try
            If radAdvance.Checked Then Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAdvance_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        Try
            If radLoan.Checked Then Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radLoan_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbPending.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPending.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbPending.Text = Language._Object.getCaption(Me.gbPending.Name, Me.gbPending.Text)
			Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
			Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
			Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
			Me.radAll.Text = Language._Object.getCaption(Me.radAll.Name, Me.radAll.Text)
			Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
			Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
			Me.dgcolhAppNo.HeaderText = Language._Object.getCaption(Me.dgcolhAppNo.Name, Me.dgcolhAppNo.HeaderText)
			Me.dgcolhRAmount.HeaderText = Language._Object.getCaption(Me.dgcolhRAmount.Name, Me.dgcolhRAmount.HeaderText)
			Me.dgcolhAAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAAmount.Name, Me.dgcolhAAmount.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select atleast one information to Approve.")
			Language.setMessage(mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class