﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmLoanScheme_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLoanScheme_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLoanScheme As clsLoan_Scheme
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLoanSchemeunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintLoanSchemeunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintLoanSchemeunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtMinSalary.BackColor = GUI.ColorOptional


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objExchangeRate As New clsExchangeRate
        objExchangeRate._ExchangeRateunkid = 1

        txtCode.Text = objLoanScheme._Code
        txtName.Text = objLoanScheme._Name
        txtDescription.Text = objLoanScheme._Description
        txtMinSalary.Text = Format(objLoanScheme._Minnetsalary, objExchangeRate._fmtCurrency)

        'If objLoanScheme._Isinclude_Parttime = True Then
        '    chkIncludePartTimeEmp.Checked = True
        'Else
        '    chkIncludePartTimeEmp.Checked = False
        'End If
        objExchangeRate = Nothing
    End Sub

    Private Sub SetValue()
        objLoanScheme._code = txtCode.Text
        objLoanScheme._name = txtName.Text
        objLoanScheme._Description = txtDescription.Text
        objLoanScheme._Minnetsalary = txtMinSalary.Decimal
        'If chkIncludePartTimeEmp.Checked Then
        '    objLoanScheme._Isinclude_Parttime = True
        'Else
        '    objLoanScheme._Isinclude_Parttime = False
        'End If
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmLoanScheme_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoanScheme = Nothing
    End Sub

    Private Sub frmLoanScheme_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmLoanScheme_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanScheme_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoanScheme = New clsLoan_Scheme
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objLoanScheme._Loanschemeunkid = mintLoanSchemeunkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanScheme_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Scheme.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Scheme"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try


            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Scheme cannot be blank. Loan Scheme is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            '--Vimal 28-Aug-2010 Start

            If Trim(txtMinSalary.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Minimum Salary cannot be blank. Please insert Minimum Salary."), enMsgBoxStyle.Information)
                txtMinSalary.Focus()
                Exit Sub
            End If

            '--Vimal 28-Aug-2010 End


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objLoanScheme.Update()
            Else
                blnFlag = objLoanScheme.Insert()
            End If

            If blnFlag = False And objLoanScheme._Message <> "" Then
                eZeeMsgBox.Show(objLoanScheme._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLoanScheme = Nothing
                    objLoanScheme = New clsLoan_Scheme
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintLoanSchemeunkid = objLoanScheme._Loanschemeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbLoanInfo.Text = Language._Object.getCaption(Me.gbLoanInfo.Name, Me.gbLoanInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lnEligibility.Text = Language._Object.getCaption(Me.lnEligibility.Name, Me.lnEligibility.Text)
			Me.lblMinSalary.Text = Language._Object.getCaption(Me.lblMinSalary.Name, Me.lblMinSalary.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Loan Scheme cannot be blank. Loan Scheme is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Minimum Salary cannot be blank. Please insert Minimum Salary.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class