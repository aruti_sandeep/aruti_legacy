﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalChangeStatus

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalChangeStatus"
    Private objLoanAdvance As clsLoan_Advance
    Private objStatusTran As New clsLoan_Status_tran
    Private objPaymentTran As New clsPayment_tran
    Private mstrSearchText As String = String.Empty
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mintBaseCountryId As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty
    Private mintLoanAdvanceUnkid As Integer = 0
    Private mintEmployeeUnkid As Integer = 0
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    Private mdecBaseExRate As Decimal = 0.0
    Private mdecPaidExRate As Decimal = 0.0
    Private mdecBaseCurrBalanceAmt As Decimal = 0.0
    Private mdecPaidCurrBalanceAmt As Decimal = 0.0
    'Private mdecPaidCurrBalFormatedAmt As Decimal = 0.0
    Private mintPaidCurrId As Integer = 0
    'Private mblnIsFromApplyButton As Boolean = False
    Private mblnIsWrittenOff As Boolean = False
    Private mdtList As New DataTable
    Private mdtExcel As New DataTable

#End Region

#Region " Form's Events "

    Private Sub frmGlobalChangeStatus_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalChangeStatus_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmGlobalChangeStatus_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLoanAdvance = New clsLoan_Advance
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()

            If ConfigParameter._Object._PaymentVocNoType = 1 Then
                txtVoucherNo.Enabled = False
            Else
                txtVoucherNo.Enabled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalChangeStatus_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim dtTable As New DataTable
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim objExRate As New clsExchangeRate
            Dim objCompanyBank As New clsCompany_Bank_tran

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, False, "Employee", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboEmployee)

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboLoanScheme)

            dsList = objMaster.GetLoan_Saving_Status("Status", False)
            dtTable = dsList.Tables("Status").Select("Id IN(" & enLoanStatus.IN_PROGRESS & "," & enLoanStatus.ON_HOLD & ")").CopyToDataTable
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dtTable
                .SelectedValue = enLoanStatus.IN_PROGRESS
            End With
            dtTable = Nothing

            Call cboStatus_SelectedIndexChanged(cboStatus, New EventArgs())

            dsList = objLoanAdvance.GetLoanCalculationTypeList("List", True)
            With cboCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objLoanAdvance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)

            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)

            If intFirstPeriodId > 0 Then
                dtTable = New DataView(dsList.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("Period"), "1=2", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = intFirstPeriodId
            End With
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstPeriodId
            dtpPaymentDate.Value = CDate(objPeriod._Start_Date).Date

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, "Period", True)

            With cboAssignedPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Period")
                .SelectedValue = 0
            End With

            With cboDeductionPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Period").Copy
                .SelectedValue = 0
            End With

            dsList = objMaster.GetLoanAdvance("List")
            With cboMode
                .ValueMember = "ID"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("List")
            End With

            dsList = objMaster.GetPaymentMode("PayMode")
            With cboPaymentMode
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("PayMode")
                .SelectedValue = 0
            End With

            dsList = objExRate.getComboList("ExRate", False)
            If dsList.Tables("ExRate").Rows.Count > 0 Then
                dtTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                Else
                    mintBaseCountryId = 0
                End If

                With cboCurrency
                    .ValueMember = "countryunkid"
                    .DisplayMember = "currency_sign"
                    .DataSource = dsList.Tables("ExRate")
                    If mintBaseCountryId > 0 Then
                        .SelectedValue = mintBaseCountryId
                    Else
                        .SelectedIndex = 0
                    End If
                End With
            End If

            dsList = objCompanyBank.GetComboList(Company._Object._Companyunkid, 1, "BankGrp")
            With cboBankGroup
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("BankGrp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim StrSearching As String = String.Empty
            Dim dsList As New DataSet

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            objlblCount.Text = "( 0 / 0 )"
            'Sohail (31 Dec 2019) -- End

            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                If CInt(cboCalcType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 45, "Interest calculation type is mandatory information."), enMsgBoxStyle.Information)
                    cboInterestCalcType.Focus()
                    Exit Sub
                ElseIf CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    StrSearching &= "AND (lnloan_advance_tran.interest_calctype_id = " & CInt(cboInterestCalcType.SelectedValue) & " " & _
                                    " OR lnloan_advance_tran.interest_calctype_id = 0) "
                End If
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_scheme_master.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue)
            End If

            If CInt(cboAssignedPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.periodunkid = " & CInt(cboAssignedPeriod.SelectedValue)
            End If

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue)
            End If

            If CInt(cboCalcType.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.calctype_id = " & CInt(cboCalcType.SelectedValue)
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND ST.statusunkid = " & CInt(cboStatus.SelectedValue)
            End If

            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                StrSearching &= "AND lnloan_advance_tran.isloan=1 "
            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                StrSearching &= "AND lnloan_advance_tran.isloan=0 "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, _
                                                                                       enStatusType.Open, , True)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstOpenPeriodID

            dsList = objLoanAdvance.GetListForGlobalChangeStatus(FinancialYear._Object._DatabaseName, _
                                                                 User._Object._Userunkid, _
                                                                 FinancialYear._Object._YearUnkid, _
                                                                 Company._Object._Companyunkid, _
                                                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                 objPeriod._Start_Date, _
                                                                 objPeriod._End_Date, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 "Loan", StrSearching)
            mdtList = dsList.Tables("Loan")

            dgvLoanList.AutoGenerateColumns = False

            objdgcolhSelect.DataPropertyName = "IsChecked"
            dgcolhVocNo.DataPropertyName = "VoucherNo"
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhLoanScheme.DataPropertyName = "LoanScheme"
            dgcolhLoanAdvance.DataPropertyName = "Mode"
            dgcolhCalcType.DataPropertyName = "LoanCalcType"
            dgcolhInterestCalcType.DataPropertyName = "LoanInterestCalcType"

            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                dgcolhLoanAdvanceAmount.HeaderText = Language.getMessage(mstrModuleName, 2, "Loan Amount")
                dgvLoanList.Columns(dgcolhCalcType.Index).Visible = True
                dgvLoanList.Columns(dgcolhInterestCalcType.Index).Visible = True
            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                dgcolhLoanAdvanceAmount.HeaderText = Language.getMessage(mstrModuleName, 3, "Advance Amount")
                dgvLoanList.Columns(dgcolhCalcType.Index).Visible = False
                dgvLoanList.Columns(dgcolhInterestCalcType.Index).Visible = False
            End If

            dgcolhLoanAdvanceAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhLoanAdvanceAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhLoanAdvanceAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhLoanAdvanceAmount.DataPropertyName = "LoanAdvanceAmount"

            dgcolhBalanceAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhBalanceAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhBalanceAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhBalanceAmount.DataPropertyName = "balance_amount"

            dgcolhSettelmentAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhSettelmentAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhSettelmentAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhSettelmentAmount.DataPropertyName = "settelment_amount"

            dgcolhStatus.DataPropertyName = "loan_status"
            dgcolhChangedStatus.DataPropertyName = "ChangedStatus"
            objdgcolhLoanAdvanceUnkid.DataPropertyName = "loanadvancetranunkid"
            objdgcolhEmployeeUnkid.DataPropertyName = "employeeunkid"
            objdgcolhLoanSchemeUnkid.DataPropertyName = "loanschemeunkid"
            objdgcolhAssignedPeriodUnkid.DataPropertyName = "periodunkid"
            objdgcolhDeductionPeriodUnkid.DataPropertyName = "deductionperiodunkid"
            objdgcolhCalcTypeId.DataPropertyName = "calctype_id"
            objdgcolhInterestCalcTypeId.DataPropertyName = "interest_calctype_id"
            objdgcolhStatusUnkid.DataPropertyName = "statusunkid"
            objdgcolhChangeStatusUnkid.DataPropertyName = "changestatusunkid"

            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                Dim dRow As DataRow() = mdtList.Select("calctype_id=" & enLoanCalcId.No_Interest)
                If CInt(cboCalcType.SelectedValue) = enLoanCalcId.No_Interest OrElse dRow.Length = mdtList.Rows.Count Then
                    dgvLoanList.Columns(dgcolhInterestCalcType.Index).Visible = False
                Else
                    dgvLoanList.Columns(dgcolhInterestCalcType.Index).Visible = True
                End If
            End If

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            'dgvLoanList.DataSource = mdtList
            objlblCount.Text = "( 0 / " & mdtList.Rows.Count.ToString & " )"
            dgvLoanList.DataSource = mdtList.DefaultView
            mdtList.DefaultView.Sort = "IsChecked DESC"
            'Sohail (31 Dec 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            Dim blnFlag As Boolean
            Dim dsList As New DataSet
            Dim strEmployeeIDs As String = String.Empty
            Dim strLoanAdvanceUnkIDs As String = String.Empty
            Dim objEmpBank As New clsEmployeeBanks
            Dim objExRate As New clsExchangeRate

            If mdtList.Rows.Count > 0 Then
                strEmployeeIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToArray())
                strLoanAdvanceUnkIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray())
            End If

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Pay Period."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            End If

            If ConfigParameter._Object._PaymentVocNoType = 0 Then
                If txtVoucherNo.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Voucher No. cannot be blank. Voucher No. is compulsory information."), enMsgBoxStyle.Information)
                    txtVoucherNo.Focus()
                    Return False
                End If
            End If

            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.Value, True)
            If dsList Is Nothing OrElse dsList.Tables("ExRate").Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 44, "There is no exchange rate defined for current period."), enMsgBoxStyle.Information)
                Return False
            End If

            If mblnIsWrittenOff = True Then

                If radValue.Checked = False AndAlso radPercentage.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If CDate(dtpPaymentDate.Value).Date < mdtPeriodStartDate OrElse CDate(dtpPaymentDate.Value).Date > mdtPeriodEndDate Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Payment Date should be in between selected period start date and end date."), enMsgBoxStyle.Information)
                    dtpPaymentDate.Focus()
                    Return False
                End If

                If CInt(cboPaymentMode.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Payment Mode is compulsory information. Please select Payment Mode to continue."), enMsgBoxStyle.Information)
                    cboPaymentMode.Focus()
                    Return False
                End If

                Select Case CInt(cboPaymentMode.SelectedValue)
                    Case enPaymentMode.CHEQUE, enPaymentMode.TRANSFER
                        If CInt(cboBankGroup.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue."), enMsgBoxStyle.Information)
                            cboBankGroup.Focus()
                            Return False
                        End If
                        If CInt(cboBranch.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue."), enMsgBoxStyle.Information)
                            cboBranch.Focus()
                            Return False
                        ElseIf CInt(cboAccountNo.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please select Bank Account. Bank Account is mandatory information."), enMsgBoxStyle.Information)
                            cboAccountNo.Focus()
                            Return False
                        End If
                        If txtChequeNo.Text = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Cheque No. cannot be blank. Please enter the cheque information."), enMsgBoxStyle.Information)
                            txtChequeNo.Focus()
                            Return False
                        End If
                End Select

                If radPercentage.Checked = True Then
                    If txtPercentage.Decimal <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Percent cannot be 0 or blank. Percent is compulsory information."), enMsgBoxStyle.Information)
                        txtPercentage.Focus()
                        Return False

                    ElseIf CDec(txtPercentage.Decimal) <= 0.0 AndAlso CDec(txtPercentage.Decimal) > 100.0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Invalid Percentage. Must be in between 0 To 100."), enMsgBoxStyle.Information)
                        txtPercentage.Focus()
                        Return False
                    End If
                End If

                If txtRemarks.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Remark cannot be blank. Remark is compulsory information."), enMsgBoxStyle.Information)
                    txtRemarks.Focus()
                    Return False
                End If
            End If

            Dim objLoan_Advance As New clsLoan_Advance
            Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
            Dim objTnA As New clsTnALeaveTran
            Dim dREx As DataRow

            If mdtExcel.Columns.Contains("LoanVocNo") = False Then mdtExcel.Columns.Add("LoanVocNo", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("Mode") = False Then mdtExcel.Columns.Add("Mode", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("Employee") = False Then mdtExcel.Columns.Add("Employee", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("LoanScheme") = False Then mdtExcel.Columns.Add("LoanScheme", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("ReferScreen") = False Then mdtExcel.Columns.Add("ReferScreen", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("Remark") = False Then mdtExcel.Columns.Add("Remark", GetType(System.String)).DefaultValue = ""

            For Each dRow As DataRow In mdtList.Select("IsChecked=True")

                blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriod.SelectedValue), dRow("employeeunkid").ToString, dtpPaymentDate.Value)
                If blnFlag = False Then
                    dREx = mdtExcel.NewRow
                    dREx("LoanVocNo") = dRow("VoucherNo")
                    dREx("Mode") = dRow("Mode")
                    dREx("Employee") = dRow("Employee")
                    dREx("LoanScheme") = dRow("LoanScheme")
                    dREx("ReferScreen") = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                    dREx("Remark") = Language.getMessage(mstrModuleName, 19, "Some of Operation(s) either Interest Rate/EMI Tenure/Topup amount are still pending for approval process in current period. In order to change status, Please either Approve or Reject pending operations for current period.")
                    mdtExcel.Rows.Add(dREx)
                    dRow("IsError") = True
                End If

                If objTnA.IsPayrollProcessDone(CInt(cboPayPeriod.SelectedValue), dRow("employeeunkid").ToString, mdtPeriodEndDate, enModuleReference.Payroll) = True Then
                    dREx = mdtExcel.NewRow
                    dREx("LoanVocNo") = dRow("VoucherNo")
                    dREx("Mode") = dRow("Mode")
                    dREx("Employee") = dRow("Employee")
                    dREx("LoanScheme") = dRow("LoanScheme")
                    dREx("ReferScreen") = Language.getMessage(mstrModuleName, 37, "Process Payroll")
                    dREx("Remark") = Language.getMessage(mstrModuleName, 20, "You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for this employee.")
                    mdtExcel.Rows.Add(dREx)
                    dRow("IsError") = True
                End If

                dsList = objLoan_Advance.GetLastLoanBalance("List", CInt(dRow("loanadvancetranunkid")))
                Dim strMsg As String = ""
                Dim strScreen As String = ""
                If dsList.Tables("List").Rows.Count > 0 Then
                    If dsList.Tables("List").Rows.Count > 0 AndAlso _
                       dsList.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(mdtPeriodStartDate) Then
                        If CInt(dsList.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 21, " Process Payroll.")
                            strScreen = Language.getMessage(mstrModuleName, 38, "Payroll Transaction => Process Payroll")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(dsList.Tables("List").Rows(0).Item("isreceipt")) = False Then
                            strMsg = Language.getMessage(mstrModuleName, 22, " Loan Payment List.")
                            strScreen = Language.getMessage(mstrModuleName, 39, "Loan/Advance & Savings => Loan/Advance => Operation => Payment")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(dsList.Tables("List").Rows(0).Item("isreceipt")) = True Then
                            strMsg = Language.getMessage(mstrModuleName, 23, " Receipt Payment List.")
                            strScreen = Language.getMessage(mstrModuleName, 40, "Loan/Advance & Savings => Loan/Advance => Operation => Receipt")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 24, " Loan Installment Change.")
                            strScreen = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 25, " Loan Topup Added.")
                            strScreen = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 26, " Loan Rate Change.")
                            strScreen = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                        End If

                        dREx = mdtExcel.NewRow
                        dREx("LoanVocNo") = dRow("VoucherNo")
                        dREx("Mode") = dRow("Mode")
                        dREx("Employee") = dRow("Employee")
                        dREx("LoanScheme") = dRow("LoanScheme")
                        dREx("ReferScreen") = strScreen
                        dREx("Remark") = Language.getMessage(mstrModuleName, 27, "You cannot change loan status. Please delete last transaction from the screen of") & strMsg
                        mdtExcel.Rows.Add(dREx)
                        dRow("IsError") = True
                    End If
                End If

                If mblnIsWrittenOff = True Then

                    If IsDBNull(dRow("settelment_amount")) = True OrElse CDec(dRow("settelment_amount")) <= 0 Then
                        dREx = mdtExcel.NewRow
                        dREx("LoanVocNo") = dRow("VoucherNo")
                        dREx("Mode") = dRow("Mode")
                        dREx("Employee") = dRow("Employee")
                        dREx("LoanScheme") = dRow("LoanScheme")
                        dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                        dREx("Remark") = Language.getMessage(mstrModuleName, 48, "Settlement amount cannot be blank. Settlement amount is compulsory information.")
                        mdtExcel.Rows.Add(dREx)
                        dRow("IsError") = True
                    Else
                        If CDec(dRow("settelment_amount")) > CDec(dRow("balance_amount")) Then
                            dREx = mdtExcel.NewRow
                            dREx("LoanVocNo") = dRow("VoucherNo")
                            dREx("Mode") = dRow("Mode")
                            dREx("Employee") = dRow("Employee")
                            dREx("LoanScheme") = dRow("LoanScheme")
                            dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                            dREx("Remark") = Language.getMessage(mstrModuleName, 49, "Settlement amount cannot be greater than Balance amount.")
                            mdtExcel.Rows.Add(dREx)
                            dRow("IsError") = True
                        End If
                    End If

                    If CStr(dRow("ChangedStatus")) = "" OrElse CInt(dRow("changestatusunkid")) <= 0 Then
                        dREx = mdtExcel.NewRow
                        dREx("LoanVocNo") = dRow("VoucherNo")
                        dREx("Mode") = dRow("Mode")
                        dREx("Employee") = dRow("Employee")
                        dREx("LoanScheme") = dRow("LoanScheme")
                        dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                        dREx("Remark") = Language.getMessage(mstrModuleName, 50, "Change Status cannot be balnk. Change Status is mandatory information.")
                        mdtExcel.Rows.Add(dREx)
                        dRow("IsError") = True
                    End If

                    Select CInt(cboPaymentMode.SelectedValue)
                        Case enPaymentMode.CHEQUE, enPaymentMode.TRANSFER
                            If objEmpBank.isEmployeeBankExist(CInt(dRow("employeeunkid"))) = False Then
                                dREx = mdtExcel.NewRow
                                dREx("LoanVocNo") = dRow("VoucherNo")
                                dREx("Mode") = dRow("Mode")
                                dREx("Employee") = dRow("Employee")
                                dREx("LoanScheme") = dRow("LoanScheme")
                                dREx("ReferScreen") = Language.getMessage(mstrModuleName, 41, "Payroll Transaction => Employee Banks")
                                dREx("Remark") = Language.getMessage(mstrModuleName, 28, "This employees do not have any bank account. Please add bank account from Employee Bank.")
                                mdtExcel.Rows.Add(dREx)
                                dRow("IsError") = True
                            End If

                            dsList = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       ConfigParameter._Object._UserAccessModeSetting, True, True, False, _
                                                                       "", "List", dRow("employeeunkid").ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                dREx = mdtExcel.NewRow
                                dREx("LoanVocNo") = dRow("VoucherNo")
                                dREx("Mode") = dRow("Mode")
                                dREx("Employee") = dRow("Employee")
                                dREx("LoanScheme") = dRow("LoanScheme")
                                dREx("ReferScreen") = Language.getMessage(mstrModuleName, 43, "Payroll Transaction => Employee Banks")
                                dREx("Remark") = Language.getMessage(mstrModuleName, 30, "This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank.")
                                mdtExcel.Rows.Add(dREx)
                                dRow("IsError") = True
                            End If
                    End Select
                    

                    dsList = objLoan_Advance.GetLastLoanBalance("List", CInt(dRow("employeeunkid")))
                    If dsList.Tables("List").Rows.Count > 0 Then
                        If dsList.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(dtpPaymentDate.Value.Date) Then
                            dREx = mdtExcel.NewRow
                            dREx("LoanVocNo") = dRow("VoucherNo")
                            dREx("Mode") = dRow("Mode")
                            dREx("Employee") = dRow("Employee")
                            dREx("LoanScheme") = dRow("LoanScheme")
                            dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                            dREx("Remark") = Language.getMessage(mstrModuleName, 29, "Payment date should not less than last transactoin date") & _
                                                  " " & Format(eZeeDate.convertDate(dsList.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & "."
                            mdtExcel.Rows.Add(dREx)
                            dRow("IsError") = True
                        End If
                    End If
                End If
            Next
            mdtList.AcceptChanges()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub Calculate_LoanBalanceInfo()
        Dim strLoanAdvanceTranIDs As String = String.Empty
        Try
            If mdtList.Rows.Count > 0 Then
                strLoanAdvanceTranIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray())

                Dim objLoan As New clsLoan_Advance
                Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                                                                          User._Object._Userunkid, _
                                                                          FinancialYear._Object._YearUnkid, _
                                                                          Company._Object._Companyunkid, _
                                                                          mdtPeriodStartDate, _
                                                                          mdtPeriodEndDate, _
                                                                          ConfigParameter._Object._UserAccessModeSetting, _
                                                                          True, "List", dtpPaymentDate.Value.Date, "", , , , , , , , , , , , , , , , _
                                                                          strLoanAdvanceTranIDs)

                Dim dTemp As DataRow()

                dTemp = mdtList.Select("IsChecked=True")

                If dTemp.Length > 0 Then

                    For Each dtRow As DataRow In dTemp

                        'Sohail (11 Feb 2020) -- Start
                        'NMB Enhancement # : index out of bound error.
                        'If radValue.Checked = True Then
                        '    dgvLoanList.Rows(mdtList.Rows.IndexOf(dtRow)).Cells(dgcolhSettelmentAmount.Index).ReadOnly = False
                        'ElseIf radPercentage.Checked = True Then
                        '    dgvLoanList.Rows(mdtList.Rows.IndexOf(dtRow)).Cells(dgcolhSettelmentAmount.Index).ReadOnly = True
                        'Else
                        '    dgvLoanList.Rows(mdtList.Rows.IndexOf(dtRow)).Cells(dgcolhSettelmentAmount.Index).ReadOnly = True
                        'End If
                        'Sohail (11 Feb 2020) -- End

                        Dim dR As DataRow() = dsLoan.Tables("List").Select("loanadvancetranunkid=" & CInt(dtRow.Item("loanadvancetranunkid")))
                        If dR.Length > 0 Then
                            
                            If CInt(dtRow("modeid")) = enLoanAdvance.LOAN Then
                                mdecBaseCurrBalanceAmt = CDec(dR(0).Item("LastProjectedBalance"))
                            ElseIf CInt(dtRow("modeid")) = enLoanAdvance.ADVANCE Then
                                mdecBaseCurrBalanceAmt = CDec(dR(0).Item("balance_amount"))
                            End If

                            If mdecBaseExRate <> 0 Then
                                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
                                dtRow.Item("balance_amount") = mdecPaidCurrBalanceAmt
                            Else
                                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
                                dtRow.Item("balance_amount") = mdecPaidCurrBalanceAmt
                            End If

                            If radPercentage.Checked = True Then
                                dtRow.Item("settelment_amount") = mdecPaidCurrBalanceAmt * txtPercentage.Decimal / 100
                            Else
                                dtRow.Item("settelment_amount") = mdecPaidCurrBalanceAmt
                            End If

                            dtRow.Item("settelment_amount") = CDec(Format(dtRow.Item("settelment_amount"), GUI.fmtCurrency))
                            dtRow.Item("changestatusunkid") = CInt(IIf(CBool(dtRow.Item("IsChecked")) = True, CInt(cboChangeStatus.SelectedValue), 0))
                            Select Case CInt(dtRow.Item("changestatusunkid"))
                                Case 0 'Select
                                    dtRow.Item("ChangedStatus") = ""
                                Case enLoanStatus.IN_PROGRESS
                                    dtRow.Item("ChangedStatus") = Language.getMessage(mstrModuleName, 32, "In Progress")
                                Case enLoanStatus.ON_HOLD
                                    dtRow.Item("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "On Hold")
                                Case enLoanStatus.WRITTEN_OFF
                                    dtRow.Item("ChangedStatus") = Language.getMessage(mstrModuleName, 34, "Written Off")
                            End Select
                        End If
                        dtRow.AcceptChanges()
                    Next
                End If
            End If
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            'dgvLoanList.DataSource = mdtList
            dgvLoanList.DataSource = mdtList.DefaultView
            If mdtList IsNot Nothing AndAlso mdtList.Columns.Count > 0 Then mdtList.DefaultView.Sort = "IsChecked DESC"
            'Sohail (11 Feb 2020) -- Start
            'NMB Enhancement # : index out of bound error.
            If radValue.Checked = True AndAlso CInt(cboChangeStatus.SelectedValue) = enLoanStatus.WRITTEN_OFF Then
                dgvLoanList.Columns(dgcolhSettelmentAmount.Index).ReadOnly = False
            ElseIf radPercentage.Checked = True Then
                dgvLoanList.Columns(dgcolhSettelmentAmount.Index).ReadOnly = True
            Else
                dgvLoanList.Columns(dgcolhSettelmentAmount.Index).ReadOnly = True
            End If
            'Sohail (11 Feb 2020) -- End
            'Sohail (31 Dec 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_LoanBalanceInfo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrAdvanceFilter = ""
            cboEmployee.SelectedValue = 0
            Call SetDefaultSearchText(cboEmployee)
            cboLoanScheme.SelectedValue = 0
            Call SetDefaultSearchText(cboLoanScheme)
            cboAssignedPeriod.SelectedValue = 0
            cboDeductionPeriod.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            cboCalcType.SelectedValue = 0
            cboInterestCalcType.SelectedValue = 0
            cboMode.SelectedValue = enLoanAdvance.LOAN
            radValue.Checked = False : radPercentage.Checked = False
            cboPaymentMode.SelectedValue = 0
            cboCurrency.SelectedValue = mintBaseCountryId
            cboBankGroup.SelectedValue = 0
            cboBranch.SelectedValue = 0
            cboAccountNo.SelectedValue = 0
            txtChequeNo.Text = ""
            cboChangeStatus.SelectedValue = 0
            txtRemarks.Text = ""
            txtPercentage.Text = "0"
            txtVoucherNo.Text = ""
            chkSelectAll.Checked = False

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            dtpPaymentDate.Value = CDate(objPeriod._Start_Date).Date

            dgvLoanList.DataSource = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue(ByVal dRow As DataRow)
        Dim strStatusData As String = String.Empty
        Try
            objStatusTran._Isvoid = False
            objStatusTran._Loanadvancetranunkid = CInt(dRow("loanadvancetranunkid"))
            objStatusTran._Remark = txtRemarks.Text
            objStatusTran._Settle_Amount = 0
            objStatusTran._Userunkid = User._Object._Userunkid
            objStatusTran._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objStatusTran._Staus_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voiduserunkid = -1
            objStatusTran._Statusunkid = CInt(cboChangeStatus.SelectedValue)
            If CInt(dRow("calctype_id")) <> enLoanCalcId.No_Interest Then
                objStatusTran._IsCalculateInterest = chkCalculateInterest.Checked
            Else
                objStatusTran._IsCalculateInterest = False
            End If

            If mblnIsWrittenOff = True Then

                strStatusData = objStatusTran._Isvoid & "|" & _
                                 objStatusTran._Loanadvancetranunkid & "|" & _
                                 objStatusTran._Remark & "|" & _
                                 objStatusTran._Settle_Amount & "|" & _
                                 objStatusTran._Staus_Date & "|" & _
                                 objStatusTran._Voiddatetime & "|" & _
                                 objStatusTran._Voiduserunkid & "|" & _
                                 objStatusTran._Statusunkid & "|" & _
                                 objStatusTran._Periodunkid & "|"

                objPaymentTran._strData = strStatusData
                objPaymentTran._IsFromStatus = True

                objPaymentTran._Voucherno = txtVoucherNo.Text
                objPaymentTran._Periodunkid = CInt(dRow("periodunkid"))
                objPaymentTran._Paymentdate = dtpPaymentDate.Value
                objPaymentTran._PaymentDate_Periodunkid = CInt(cboPayPeriod.SelectedValue)
                objPaymentTran._Employeeunkid = CInt(dRow("employeeunkid"))
                If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                    objPaymentTran._Paymentrefid = enLoanAdvance.LOAN
                ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                    objPaymentTran._Paymentrefid = enLoanAdvance.ADVANCE
                End If
                objPaymentTran._Paymentmodeid = CInt(cboPaymentMode.SelectedValue)
                objPaymentTran._Branchunkid = CInt(cboBranch.SelectedValue)
                objPaymentTran._Chequeno = txtChequeNo.Text
                If radValue.Checked = True Then
                    objPaymentTran._Paymentbyid = enPaymentBy.Value
                ElseIf radPercentage.Checked = True Then
                    objPaymentTran._Paymentbyid = enPaymentBy.Percentage
                End If
                objPaymentTran._Percentage = CDec(IIf(radPercentage.Checked, txtPercentage.Text, 0))
                objPaymentTran._Voucherref = CStr(dRow("VoucherNo"))
                objPaymentTran._Payreftranunkid = CInt(dRow("loanadvancetranunkid"))
                objPaymentTran._PaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED
                objPaymentTran._Accountno = CStr(IIf(CInt(cboAccountNo.SelectedValue) > 0, cboAccountNo.Text, "0"))
                objPaymentTran._Is_Receipt = True
                objPaymentTran._Isglobalpayment = True
                objPaymentTran._Isapproved = False
                objPaymentTran._Userunkid = User._Object._Userunkid
                objPaymentTran._Isvoid = False
                objPaymentTran._Voiddatetime = Nothing
                objPaymentTran._Voiduserunkid = -1
                objPaymentTran._Voidreason = ""

                objPaymentTran._Basecurrencyid = mintBaseCurrId
                objPaymentTran._Baseexchangerate = mdecBaseExRate
                objPaymentTran._Paidcurrencyid = mintPaidCurrId
                objPaymentTran._Expaidrate = mdecPaidExRate
                objPaymentTran._Countryunkid = CInt(cboCurrency.SelectedValue)
                objPaymentTran._Roundingtypeid = 0
                objPaymentTran._Roundingmultipleid = 0
                objPaymentTran._RoundingAdjustment = CDec(0.0)
                objPaymentTran._Remarks = txtRemarks.Text.Trim

                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = CDec(dRow("settelment_amount")) * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = CDec(dRow("settelment_amount")) * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = CDec(dRow("settelment_amount"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility(ByVal IsEnable As Boolean)
        Try
            radValue.Enabled = IsEnable
            'radValue.Checked = False
            radPercentage.Enabled = IsEnable
            'radPercentage.Checked = False
            cboPaymentMode.Enabled = IsEnable
            If CInt(cboPaymentMode.SelectedValue) <= 0 Then
                cboBankGroup.Enabled = IsEnable
                cboBranch.Enabled = IsEnable
                cboAccountNo.Enabled = IsEnable
                txtChequeNo.Enabled = IsEnable
            End If
            txtChequeNo.Text = ""
            txtPercentage.Decimal = CDec(0.0)
            If radValue.Checked = True Then
                txtPercentage.Enabled = False
            Else
                txtPercentage.Enabled = IsEnable
            End If
            txtRemarks.Text = ""
            dtpPaymentDate.Enabled = IsEnable
            btnApply.Enabled = IsEnable
            chkCalculateInterest.Checked = False

            dgvLoanList.Columns(dgcolhSettelmentAmount.Index).Visible = IsEnable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboEmployee)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Try
            Dim objCompanyBank As New clsCompany_Bank_tran
            Dim dsList As New DataSet

            dsList = objCompanyBank.GetComboList(Company._Object._Companyunkid, 2, "Branch", " cfbankbranch_master.bankgroupunkid = " & CInt(cboBankGroup.SelectedValue) & " ")
            With cboBranch
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Branch")
                .SelectedValue = 0
                'If mintPaymentTranId = -1 Then
                '    .SelectedValue = 0
                'Else
                '    .SelectedValue = objPaymentTran._Branchunkid
                'End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsList As New DataSet
        Try
            dsList = objCompanyBank.GetComboListBankAccount("Account", Company._Object._Companyunkid, True, _
                                                            CInt(cboBankGroup.SelectedValue), CInt(cboBranch.SelectedValue))
            With cboAccountNo
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Account")
                .SelectedValue = 0
                'If mintPaymentTranId = -1 Then
                '    .SelectedValue = 0
                'Else
                '    .SelectedValue = mintPaymentTranId
                'End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBranch_SelectedIndexChanged", mstrModuleName)
        Finally
            objCompanyBank = Nothing
        End Try
    End Sub

    Private Sub cboPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMode.SelectedIndexChanged
        Try
            If CInt(cboPaymentMode.SelectedValue) > 0 AndAlso CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH Then
                cboBankGroup.Enabled = False
                cboBankGroup.SelectedValue = 0
                cboBranch.Enabled = False
                cboBranch.SelectedValue = 0
                cboAccountNo.Enabled = False
                cboAccountNo.SelectedValue = 0
                txtChequeNo.Text = ""
                txtChequeNo.Enabled = False
            Else
                cboBankGroup.Enabled = True
                cboBankGroup.SelectedValue = 0
                cboBranch.Enabled = True
                cboBranch.SelectedValue = 0
                cboAccountNo.Enabled = True
                cboAccountNo.SelectedValue = 0
                txtChequeNo.Text = ""
                txtChequeNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPaymentMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objMaster As New clsMasterData

            If CInt(cboStatus.SelectedValue) > 0 Then
                dsList = objMaster.GetLoan_Saving_Status("Status", False)
                Dim dtTable As DataTable = dsList.Tables("Status").Select("Id NOT IN(" & CInt(cboStatus.SelectedValue) & "," & enLoanStatus.COMPLETED & ")").CopyToDataTable
                With cboChangeStatus
                    .ValueMember = "Id"
                    .DisplayMember = "NAME"
                    .DataSource = dtTable
                    .SelectedValue = 0
                End With
                chkSelectAll.Checked = False
                If dgvLoanList.RowCount > 0 Then Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                If intFirstOpenPeriod > 0 Then
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstOpenPeriod
                    dtpPaymentDate.Value = CDate(objPeriod._Start_Date).Date
                    mdtPeriodStartDate = CDate(objPeriod._Start_Date).Date
                    mdtPeriodEndDate = CDate(objPeriod._End_Date).Date
                End If
            Else
                dtpPaymentDate.Value = CDate(ConfigParameter._Object._CurrentDateAndTime).Date
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExchangeRate.Text = ""

            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.Value, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable

            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExchangeRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "There is no exchange rate defined for current period."), enMsgBoxStyle.Information)
                'Exit Sub
            End If

            If mdtList.Rows.Count > 0 Then
                Dim dTemp As DataRow() = mdtList.Select("IsChecked=True")
                If dTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
                    txtPercentage.Text = ""
                    dgvLoanList.Focus()
                    Exit Sub
                End If

                Call Calculate_LoanBalanceInfo()
            End If

            'Dim dRow As DataRow() = mdtList.Select("IsChecked=True")
            'If dRow.Length > 0 Then
            '    Call Calculate_LoanBalanceInfo()
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboChangeStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChangeStatus.SelectedIndexChanged
        Try
            Select Case CInt(cboChangeStatus.SelectedValue)
                Case 0 'Select
                    chkCalculateInterest.Enabled = True
                    cboPaymentMode.SelectedValue = 0
                    cboBankGroup.SelectedValue = 0
                    Call SetVisibility(True)

                Case enLoanStatus.IN_PROGRESS
                    chkCalculateInterest.Enabled = False
                    cboPaymentMode.SelectedValue = 0
                    cboBankGroup.SelectedValue = 0
                    Call SetVisibility(False)

                Case enLoanStatus.ON_HOLD
                    cboPaymentMode.SelectedValue = 0
                    cboBankGroup.SelectedValue = 0
                    chkCalculateInterest.Enabled = True
                    Call SetVisibility(False)

                Case enLoanStatus.WRITTEN_OFF
                    chkCalculateInterest.Enabled = False
                    Call SetVisibility(True)
            End Select

            If CInt(cboChangeStatus.SelectedValue) > 0 Then
                For Each dR As DataRow In mdtList.Rows
                    If CBool(dR.Item("IsChecked")) = True Then
                        dR("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
                        dR("ChangedStatus") = IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.Text, "")
                    End If
                Next
            Else
                For Each row As DataRow In mdtList.Rows
                    row("changestatusunkid") = 0
                    row("ChangedStatus") = ""
                Next
            End If
            mdtList.AcceptChanges()

            Call Calculate_LoanBalanceInfo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChangeStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboInterestCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboInterestCalcType.SelectedIndexChanged
        Try
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                chkSelectAll.Checked = False
                If CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.DAILY Then
                    dtpPaymentDate.Value = mdtPeriodStartDate
                    dtpPaymentDate.Enabled = True
                    If dgvLoanList.RowCount > 0 Then Call FillList()

                    'Sohail (14 Mar 2017) -- Start
                    'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                    'ElseIf CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.MONTHLY Then
                    'Sohail (23 May 2017) -- Start
                    'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                    'ElseIf CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.MONTHLY OrElse CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.BY_TENURE Then
                ElseIf CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.MONTHLY OrElse CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.BY_TENURE OrElse CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.SIMPLE_INTEREST Then
                    'Sohail (23 May 2017) -- End
                    'Sohail (14 Mar 2017) -- End
                    dtpPaymentDate.Value = mdtPeriodStartDate
                    dtpPaymentDate.Enabled = False
                    If dgvLoanList.RowCount > 0 Then Call FillList()
                End If
                'Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboInterestCalcType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            If CInt(cboMode.SelectedValue) > 0 Then
                If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                    cboCalcType.SelectedValue = 0
                    cboCalcType.Enabled = True
                    cboInterestCalcType.SelectedValue = 0
                    cboInterestCalcType.Enabled = True
                    chkCalculateInterest.Checked = False
                    chkCalculateInterest.Enabled = True

                ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                    cboCalcType.SelectedValue = 0
                    cboCalcType.Enabled = False
                    cboInterestCalcType.SelectedValue = 0
                    cboInterestCalcType.Enabled = False
                    chkCalculateInterest.Checked = False
                    chkCalculateInterest.Enabled = False
                End If
                dgvLoanList.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Button's Events "

    Private Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            Dim frm As New frmAdvanceSearch

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
    '    Dim objEmpBank As New clsEmployeeBanks
    '    Dim objLoan_Advance As New clsLoan_Advance
    '    Dim dsList As New DataSet
    '    Dim strLoanAdvanceTranIDs As String = String.Empty
    '    Try
    '        mblnIsFromApplyButton = True

    '        If dgvLoanList.RowCount <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        If CInt(cboChangeStatus.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select change status. Change status is mandatory information."), enMsgBoxStyle.Information)
    '            cboChangeStatus.Focus()
    '            Exit Sub
    '        End If

    '        Select Case CInt(cboChangeStatus.SelectedValue)

    '            Case enLoanStatus.IN_PROGRESS, enLoanStatus.ON_HOLD

    '                If mdtList.Rows.Count > 0 Then
    '                    strLoanAdvanceTranIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray())
    '                End If

    '                Dim dTemp As DataRow()
    '                dTemp = mdtList.Select("IsChecked=True")
    '                If dTemp.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
    '                    dgvLoanList.Focus()
    '                    Exit Sub
    '                End If

    '                'For Each dRow As DataRow In dTemp
    '                '    mintLoanAdvanceUnkid = CInt(dRow("loanadvancetranunkid"))
    '                '    dRow("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
    '                '    Select Case CInt(dRow("changestatusunkid"))
    '                '        Case 0 'Select
    '                '            dRow("ChangedStatus") = ""
    '                '        Case enLoanStatus.IN_PROGRESS
    '                '            dRow("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "In Progress")
    '                '        Case enLoanStatus.ON_HOLD
    '                '            dRow("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "On Hold")
    '                '        Case enLoanStatus.WRITTEN_OFF
    '                '            dRow("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "Written Off")
    '                '    End Select

    '                '    dRow.AcceptChanges()
    '                'Next
    '                'dgvLoanList.DataSource = mdtList

    '            Case enLoanStatus.WRITTEN_OFF

    '                If radValue.Checked = False AndAlso radPercentage.Checked = False Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), enMsgBoxStyle.Information)
    '                    lblPaymentBy.Focus()
    '                    Exit Sub
    '                End If

    '                If radPercentage.Checked = True Then
    '                    If CDec(txtPercentage.Decimal) < 0.0 AndAlso CDec(txtPercentage.Decimal) > 100.0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Invalid Percentage. Must be in between 0 To 100."), enMsgBoxStyle.Information)
    '                        txtPercentage.Focus()
    '                        Exit Sub
    '                    End If
    '                End If

    '                Call Calculate_LoanBalanceInfo()
    '        End Select

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnApply_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim objEmpBank As New clsEmployeeBanks
        Dim objLoan_Advance As New clsLoan_Advance
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Try
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            'If dgvLoanList.RowCount <= 0 Then
            If mdtList Is Nothing OrElse mdtList.Rows.Count <= 0 Then
                'Sohail (31 Dec 2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dTemp As DataRow() = mdtList.Select("IsChecked=True")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
                dgvLoanList.Focus()
                Exit Sub
            End If

            If CInt(cboChangeStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Please select change status. Change status is mandatory information."), enMsgBoxStyle.Information)
                cboChangeStatus.Focus()
                Exit Sub
            End If

            Select Case CInt(cboChangeStatus.SelectedValue)

                Case enLoanStatus.IN_PROGRESS, enLoanStatus.ON_HOLD

                    mblnIsWrittenOff = False
                    If IsValidate() = False Then Exit Sub

                    For i As Integer = 0 To dTemp.Length - 1

                        If CBool(dTemp(i)("IsError")) = False Then
                            Call SetValue(dTemp(i))
                            blnFlag = objStatusTran.Insert(FinancialYear._Object._DatabaseName, _
                                                           User._Object._Userunkid, _
                                                           FinancialYear._Object._YearUnkid, _
                                                           Company._Object._Companyunkid, _
                                                           mdtPeriodStartDate, _
                                                           mdtPeriodEndDate, _
                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                           True, , True)

                            If blnFlag = False And objPaymentTran._Message <> "" Then
                                eZeeMsgBox.Show(objStatusTran._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            ElseIf blnFlag = True Then
                                objLoan_Advance._Loanadvancetranunkid = CInt(dTemp(i)("loanadvancetranunkid"))
                                objLoan_Advance._LoanStatus = CInt(cboChangeStatus.SelectedValue)
                                If objLoan_Advance.Update() = False Then
                                    eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next

                Case enLoanStatus.WRITTEN_OFF

                    mblnIsWrittenOff = True
                    If IsValidate() = False Then Exit Sub

                    For i As Integer = 0 To dTemp.Length - 1

                        If CBool(dTemp(i)("IsError")) = False Then
                            Call SetValue(dTemp(i))
                            'Sohail (23 May 2017) -- Start
                            'Enhancement - 67.1 - Link budget with Payroll.
                            'blnFlag = objPaymentTran.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                            '                                Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, _
                            '                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                            '                                ConfigParameter._Object._PaymentVocNoType, ConfigParameter._Object._PaymentVocPrefix, _
                            '                                ConfigParameter._Object._CurrentDateAndTime, True, "")
                            blnFlag = objPaymentTran.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                            Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, _
                                                            ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                            ConfigParameter._Object._PaymentVocNoType, ConfigParameter._Object._PaymentVocPrefix, _
                                                            ConfigParameter._Object._CurrentDateAndTime, True, "", Nothing)
                            'Sohail (23 May 2017) -- End

                            If blnFlag = False And objPaymentTran._Message <> "" Then
                                eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    Next
            End Select

            If mdtExcel.Rows.Count > 0 Then
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim IExcel As New ExcelData
                'S.SANDEEP [12-Jan-2018] -- END
                Dim strFilePath As String = String.Empty
                Dim strFileName As String = Language._Object.getCaption(mstrModuleName, Me.Text).Replace(" ", "_")
                strFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\" & strFileName & "_" & Now.ToString("yyyyMMddhhmmss") & ".xlsx"
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "There are some operations that need to be done before change status. Please click on OK button to generate excel sheet for more detail log. Refer the exported path: ") & strFilePath, CType(enMsgBoxStyle.Question + enMsgBoxStyle.OkOnly, enMsgBoxStyle)) = Windows.Forms.DialogResult.OK Then
                    'S.SANDEEP [12-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 0001843
                    'IExcel.Export(strFilePath, mdtExcel)
                    'Sohail (11 Feb 2020) -- Start
                    'NMB Enhancement # : not able to generate data in exported file.
                    'OpenXML_Export(strFilePath, mdtExcel)
                    Dim ds As New DataSet
                    ds.Tables.Clear()
                    ds.Tables.Add(mdtExcel.Copy)
                    OpenXML_Export(strFilePath, ds)
                    'Sohail (11 Feb 2020) -- End
                    'S.SANDEEP [12-Jan-2018] -- END
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Excel file exported successfully."), enMsgBoxStyle.Information)
                    Process.Start(strFilePath)
                End If
            Else
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Process completed successfully."), enMsgBoxStyle.Information)
                End If
            End If
            chkSelectAll.Checked = False
            txtRemarks.Text = ""
            FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            If mdtExcel IsNot Nothing AndAlso mdtExcel.Rows.Count > 0 Then mdtExcel.Rows.Clear()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            chkSelectAll.Checked = False
            Call FillList()
            If dgvLoanList.RowCount > 0 Then
                Call objbtnSearch.ShowResult(CStr(dgvLoanList.RowCount - 1))
            Else
                Call objbtnSearch.ShowResult(CStr(dgvLoanList.RowCount))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If mdtList.Rows.Count > 0 Then
                'For Each dR As DataRow In mdtList.Rows
                For Each R As DataRow In mdtList.DefaultView.ToTable.Rows
                    Dim dR As DataRow = mdtList.Select("loanadvancetranunkid = " & CInt(R.Item("loanadvancetranunkid")) & " ").FirstOrDefault
                    If dR Is Nothing Then Continue For
                    'Sohail (31 Dec 2019) -- End
                    RemoveHandler dgvLoanList.CellContentClick, AddressOf dgvLoanList_CellContentClick
                    dR("IsChecked") = chkSelectAll.Checked
                    AddHandler dgvLoanList.CellContentClick, AddressOf dgvLoanList_CellContentClick

                    If chkSelectAll.Checked = True Then
                        dR("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
                        dR("ChangedStatus") = IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.Text, "")
                    ElseIf chkSelectAll.Checked = False Then
                        dR("changestatusunkid") = 0
                        dR("ChangedStatus") = ""
                    End If
                Next
                If radValue.Checked = True Then
                    'Sohail (11 Feb 2020) -- Start
                    'NMB Enhancement # : index out of bound error.
                    'dgvLoanList.Columns(dgcolhSettelmentAmount.Index).ReadOnly = Not (chkSelectAll.Checked)
                    dgvLoanList.Columns(dgcolhSettelmentAmount.Index).ReadOnly = False
                    'Sohail (11 Feb 2020) -- End
                ElseIf radPercentage.Checked = True Then
                    dgvLoanList.Columns(dgcolhSettelmentAmount.Index).ReadOnly = True
                Else
                    dgvLoanList.Columns(dgcolhSettelmentAmount.Index).ReadOnly = True
                End If
            End If
            mdtList.AcceptChanges()

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            objlblCount.Text = "( " & mdtList.Select("IsChecked = 1 ").Length.ToString & " / " & mdtList.Rows.Count.ToString & " )"
            'Sohail (31 Dec 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Private Sub dgvLoanList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanList.CellContentClick, _
                                                                                                                                        dgvLoanList.CellContentDoubleClick
        Try
            RemoveHandler dgvLoanList.CellContentClick, AddressOf dgvLoanList_CellContentClick

            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgvLoanList.IsCurrentCellDirty Then
                    dgvLoanList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                'mdtList.AcceptChanges() 'Sohail (11 Feb 2020)
                Dim dR As DataRow() = mdtList.Select("IsChecked=True")

                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                If dR.Length <= 0 Then
                    chkSelectAll.CheckState = CheckState.Unchecked
                ElseIf dR.Length < mdtList.Rows.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                ElseIf dR.Length = mdtList.Rows.Count Then
                    chkSelectAll.CheckState = CheckState.Checked
                End If

                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Search option on Global Loan Change Status screen.
                'Sohail (11 Feb 2020) -- Start
                'NMB Enhancement # : index out of bound error.
                'objlblCount.Text = "( " & dR.Length.ToString & " / " & mdtList.Rows.Count.ToString & " )"
                Dim intUnkID As Integer = CInt(dgvLoanList.Rows(e.RowIndex).Cells(objdgcolhLoanAdvanceUnkid.Index).Value)
                Dim r() As DataRow = mdtList.Select("loanadvancetranunkid = " & intUnkID & " ")
                If CBool(dgvLoanList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = False Then
                    If r.Length > 0 Then
                        r(0).Item("changestatusunkid") = 0
                        r(0).Item("ChangedStatus") = ""
                    End If
                ElseIf CBool(dgvLoanList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = True Then
                    If r.Length > 0 Then
                        r(0).Item("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
                        r(0).Item("ChangedStatus") = IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.Text, "")
                    End If
                End If

                mdtList.AcceptChanges()
                objlblCount.Text = "( " & mdtList.Select("IsChecked=True").Length.ToString & " / " & mdtList.Rows.Count.ToString & " )"
                'Sohail (11 Feb 2020) -- End
                'Sohail (31 Dec 2019) -- End

                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If

            'Sohail (11 Feb 2020) -- Start
            'NMB Enhancement # : index out of bound error.
            'If radValue.Checked = True Then
            '    dgvLoanList.Rows(e.RowIndex).Cells(dgcolhSettelmentAmount.Index).ReadOnly = Not CBool(dgvLoanList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value)
            'Else
            '    dgvLoanList.Rows(e.RowIndex).Cells(dgcolhSettelmentAmount.Index).ReadOnly = True
            'End If

            'If CBool(dgvLoanList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = False Then
            '    mdtList.Rows(e.RowIndex).Item("changestatusunkid") = 0
            '    mdtList.Rows(e.RowIndex).Item("ChangedStatus") = ""
            'ElseIf CBool(dgvLoanList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = True Then
            '    mdtList.Rows(e.RowIndex).Item("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
            '    mdtList.Rows(e.RowIndex).Item("ChangedStatus") = IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.Text, "")
            'End If
            'Sohail (11 Feb 2020) -- End

            'Call Calculate_LoanBalanceInfo()

            AddHandler dgvLoanList.CellContentClick, AddressOf dgvLoanList_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanList_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvLoanList_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanList.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            'RemoveHandler dgvLoanList.CellValueChanged, AddressOf dgvLoanList_CellValueChanged

            Select Case e.ColumnIndex
                Case dgcolhSettelmentAmount.Index
                    If dgvLoanList.Rows(e.RowIndex).Cells(dgcolhSettelmentAmount.Index).Value.ToString <> "" Then
                        If CDec(dgvLoanList.Rows(e.RowIndex).Cells(dgcolhSettelmentAmount.Index).Value) > CDec(dgvLoanList.Rows(e.RowIndex).Cells(dgcolhBalanceAmount.Index).Value) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Settelment amount cannot be greater than Balance amout."), enMsgBoxStyle.Information)
                            dgvLoanList.Rows(e.RowIndex).Cells(dgcolhSettelmentAmount.Index).Value = dgvLoanList.Rows(e.RowIndex).Cells(dgcolhBalanceAmount.Index).Value
                        End If
                    Else
                        dgvLoanList.Rows(e.RowIndex).Cells(dgcolhSettelmentAmount.Index).Value = dgvLoanList.Rows(e.RowIndex).Cells(dgcolhBalanceAmount.Index).Value
                    End If
            End Select

            'AddHandler dgvLoanList.CellValueChanged, AddressOf dgvLoanList_CellValueChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanList_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvLoanList_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanList.CellEnter
        Try
            If e.RowIndex < 0 Then Exit Sub

            Select Case e.ColumnIndex
                Case dgcolhSettelmentAmount.Index
                    'If CBool(dgvLoanList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = True Then 'Sohail (11 Feb 2020)
                        SendKeys.Send("{F2}")
                    'End If 'Sohail (11 Feb 2020)
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanList_CellEnter", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " RadioButton's Events "

    Private Sub radValue_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radValue.CheckedChanged
        Try
            If radValue.Checked = True Then
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Search option on Global Loan Change Status screen.
                'If dgvLoanList.RowCount <= 0 Then
                If mdtList Is Nothing OrElse mdtList.Rows.Count <= 0 Then
                    'Sohail (31 Dec 2019) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), enMsgBoxStyle.Information)
                    radValue.Checked = False
                    Exit Sub
                End If
                dtpPaymentDate.Value = mdtPeriodStartDate
                txtPercentage.Text = "0"
                txtPercentage.Enabled = False
                'mblnIsFromApplyButton = False
                'chkSelectAll.Checked = False
                Call Calculate_LoanBalanceInfo()
            Else
                txtPercentage.Enabled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radValue_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radPercentage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPercentage.CheckedChanged
        Try
            If radPercentage.Checked = True Then
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Search option on Global Loan Change Status screen.
                'If dgvLoanList.RowCount <= 0 Then
                If mdtList Is Nothing OrElse mdtList.Rows.Count <= 0 Then
                    'Sohail (31 Dec 2019) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), enMsgBoxStyle.Information)
                    radPercentage.Checked = False
                    Exit Sub
                End If
                dtpPaymentDate.Value = mdtPeriodStartDate
                txtPercentage.Text = "0"
                txtPercentage.Enabled = True
                'mblnIsFromApplyButton = False
                'chkSelectAll.Checked = False
                Call Calculate_LoanBalanceInfo()
            Else
                txtPercentage.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radPercentage_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DatePicker's Events "

    Private Sub dtpPaymentDate_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPaymentDate.Validated
        Try


            If radValue.Checked = False AndAlso radPercentage.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), enMsgBoxStyle.Information)
                dtpPaymentDate.Value = CDate(mdtPeriodStartDate)
                lblPaymentBy.Focus()
                Exit Sub
            End If

            'mblnIsFromApplyButton = False
            Dim dTemp As DataRow() = mdtList.Select("IsChecked=True")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
                dtpPaymentDate.Value = CDate(mdtPeriodStartDate)
                dgvLoanList.Focus()
                Exit Sub
            End If

            Call Calculate_LoanBalanceInfo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpPaymentDate_Validated", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " TextBox's Events "

    Private Sub txtPercentage_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentage.Validated
        Try
            If txtPercentage.Text <> "" Then
                'mblnIsFromApplyButton = True
                If radValue.Checked = False AndAlso radPercentage.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), enMsgBoxStyle.Information)
                    lblPaymentBy.Focus()
                    Exit Sub
                End If

                If radPercentage.Checked = True Then
                    If CDec(txtPercentage.Decimal) < 0.0 AndAlso CDec(txtPercentage.Decimal) > 100.0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Invalid Percentage. Must be in between 0 To 100."), enMsgBoxStyle.Information)
                        txtPercentage.Focus()
                        Exit Sub
                    End If
                End If

                If CInt(cboChangeStatus.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Change status. Change status is mandatory information."), enMsgBoxStyle.Information)
                    cboChangeStatus.Focus()
                    Exit Sub
                End If

                Dim dTemp As DataRow() = mdtList.Select("IsChecked=True")
                If dTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
                    txtPercentage.Text = "0"
                    dgvLoanList.Focus()
                    Exit Sub
                End If

                Call Calculate_LoanBalanceInfo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPercentage_Validated:- ", mstrModuleName)
        End Try
    End Sub

    'Sohail (31 Dec 2019) -- Start
    'NMB Enhancement # : Search option on Global Loan Change Status screen.
    Private Sub txtSearch_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.GotFocus
        Try
            With txtSearch
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.Leave
        Try
            If txtSearch.Text.Trim = "" Then
                mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to Search")
                With txtSearch
                    .ForeColor = Color.Gray
                    .Text = mstrSearchText
                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If txtSearch.Text.Trim = mstrSearchText Then Exit Sub
            If mdtList IsNot Nothing AndAlso mdtList.Rows.Count > 0 Then
                'Sohail (29 Jan 2020) -- Start
                'NMB Enhancement # : Comma separated employee code search option on global loan change status screen.
                'mdtList.DefaultView.RowFilter = "employee LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'  OR loanscheme LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR loancalctype LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
                If txtSearch.Text.Contains(",") = True Then
                    'Sohail (11 Feb 2020) -- Start
                    'NMB Enhancement # : allow to search with space after comma.
                    'Dim arr As String = "'" & txtSearch.Text.Replace("'", "''").Replace(",", "','") & "'"
                    Dim arr As String = "'" & txtSearch.Text.Replace("  ", "").Replace(" ", "").Replace("'", "''").Replace(",", "','") & "'"
                    'Sohail (11 Feb 2020) -- End

                    mdtList.DefaultView.RowFilter = "employeecode IN (" & arr & ") "
                Else
                    mdtList.DefaultView.RowFilter = "employee LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'  OR loanscheme LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR loancalctype LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR VoucherNo LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
                End If
                'Sohail (29 Jan 2020) -- End
                dgvLoanList.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (31 Dec 2019) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApply.GradientBackColor = GUI._ButttonBackColor
            Me.btnApply.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.Name, Me.btnApply.Text)
            Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.Name, Me.lblPaymentDate.Text)
            Me.lblChangeStatus.Text = Language._Object.getCaption(Me.lblChangeStatus.Name, Me.lblChangeStatus.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
            Me.lblChequeNo.Text = Language._Object.getCaption(Me.lblChequeNo.Name, Me.lblChequeNo.Text)
            Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.Name, Me.lblAccountNo.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
            Me.lblPaymentMode.Text = Language._Object.getCaption(Me.lblPaymentMode.Name, Me.lblPaymentMode.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblPaymentBy.Text = Language._Object.getCaption(Me.lblPaymentBy.Name, Me.lblPaymentBy.Text)
            Me.radPercentage.Text = Language._Object.getCaption(Me.radPercentage.Name, Me.radPercentage.Text)
            Me.radValue.Text = Language._Object.getCaption(Me.radValue.Name, Me.radValue.Text)
            Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.Name, Me.lblDeductionPeriod.Text)
            Me.lblAssignedPeriod.Text = Language._Object.getCaption(Me.lblAssignedPeriod.Name, Me.lblAssignedPeriod.Text)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.dgcolhVocNo.HeaderText = Language._Object.getCaption(Me.dgcolhVocNo.Name, Me.dgcolhVocNo.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhLoanScheme.HeaderText = Language._Object.getCaption(Me.dgcolhLoanScheme.Name, Me.dgcolhLoanScheme.HeaderText)
            Me.dgcolhLoanAdvance.HeaderText = Language._Object.getCaption(Me.dgcolhLoanAdvance.Name, Me.dgcolhLoanAdvance.HeaderText)
            Me.dgcolhCalcType.HeaderText = Language._Object.getCaption(Me.dgcolhCalcType.Name, Me.dgcolhCalcType.HeaderText)
            Me.dgcolhInterestCalcType.HeaderText = Language._Object.getCaption(Me.dgcolhInterestCalcType.Name, Me.dgcolhInterestCalcType.HeaderText)
            Me.dgcolhLoanAdvanceAmount.HeaderText = Language._Object.getCaption(Me.dgcolhLoanAdvanceAmount.Name, Me.dgcolhLoanAdvanceAmount.HeaderText)
            Me.dgcolhBalanceAmount.HeaderText = Language._Object.getCaption(Me.dgcolhBalanceAmount.Name, Me.dgcolhBalanceAmount.HeaderText)
            Me.dgcolhSettelmentAmount.HeaderText = Language._Object.getCaption(Me.dgcolhSettelmentAmount.Name, Me.dgcolhSettelmentAmount.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhChangedStatus.HeaderText = Language._Object.getCaption(Me.dgcolhChangedStatus.Name, Me.dgcolhChangedStatus.HeaderText)
            Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.Name, Me.lblVoucherNo.Text)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.chkCalculateInterest.Text = Language._Object.getCaption(Me.chkCalculateInterest.Name, Me.chkCalculateInterest.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Type to Search")
            Language.setMessage(mstrModuleName, 2, "Loan Amount")
            Language.setMessage(mstrModuleName, 3, "Advance Amount")
            Language.setMessage(mstrModuleName, 4, "There is no record found for further process.")
            Language.setMessage(mstrModuleName, 5, "Please tick atleast one record for further process.")
            Language.setMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue.")
            Language.setMessage(mstrModuleName, 7, "Invalid Percentage. Must be in between 0 To 100.")
            Language.setMessage(mstrModuleName, 8, "Please select Pay Period.")
            Language.setMessage(mstrModuleName, 9, "Voucher No. cannot be blank. Voucher No. is compulsory information.")
            Language.setMessage(mstrModuleName, 10, "Please select Change status. Change status is mandatory information.")
            Language.setMessage(mstrModuleName, 11, "Payment Date should be in between selected period start date and end date.")
            Language.setMessage(mstrModuleName, 12, "Payment Mode is compulsory information. Please select Payment Mode to continue.")
            Language.setMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue.")
            Language.setMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue.")
            Language.setMessage(mstrModuleName, 15, "Please select Bank Account. Bank Account is mandatory information.")
            Language.setMessage(mstrModuleName, 16, "Cheque No. cannot be blank. Please enter the cheque information.")
            Language.setMessage(mstrModuleName, 17, "Percent cannot be 0 or blank. Percent is compulsory information.")
            Language.setMessage(mstrModuleName, 18, "Remark cannot be blank. Remark is compulsory information.")
            Language.setMessage(mstrModuleName, 19, "Some of Operation(s) either Interest Rate/EMI Tenure/Topup amount are still pending for approval process in current period. In order to change status, Please either Approve or Reject pending operations for current period.")
            Language.setMessage(mstrModuleName, 20, "You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for this employee.")
            Language.setMessage(mstrModuleName, 21, " Process Payroll.")
            Language.setMessage(mstrModuleName, 22, " Loan Payment List.")
            Language.setMessage(mstrModuleName, 23, " Receipt Payment List.")
            Language.setMessage(mstrModuleName, 24, " Loan Installment Change.")
            Language.setMessage(mstrModuleName, 25, " Loan Topup Added.")
            Language.setMessage(mstrModuleName, 26, " Loan Rate Change.")
            Language.setMessage(mstrModuleName, 27, "You cannot change loan status. Please delete last transaction from the screen of")
            Language.setMessage(mstrModuleName, 28, "This employees do not have any bank account. Please add bank account from Employee Bank.")
            Language.setMessage(mstrModuleName, 29, "Payment date should not less than last transactoin date")
            Language.setMessage(mstrModuleName, 30, "This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank.")
            Language.setMessage(mstrModuleName, 31, "Settelment amount cannot be greater than Balance amout.")
            Language.setMessage(mstrModuleName, 32, "In Progress")
            Language.setMessage(mstrModuleName, 33, "On Hold")
            Language.setMessage(mstrModuleName, 34, "Written Off")
            Language.setMessage(mstrModuleName, 35, "Process completed successfully.")
            Language.setMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
            Language.setMessage(mstrModuleName, 37, "Process Payroll")
            Language.setMessage(mstrModuleName, 38, "Payroll Transaction => Process Payroll")
            Language.setMessage(mstrModuleName, 39, "Loan/Advance & Savings => Loan/Advance => Operation => Payment")
            Language.setMessage(mstrModuleName, 40, "Loan/Advance & Savings => Loan/Advance => Operation => Receipt")
            Language.setMessage(mstrModuleName, 41, "Payroll Transaction => Employee Banks")
            Language.setMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
            Language.setMessage(mstrModuleName, 43, "Payroll Transaction => Employee Banks")
            Language.setMessage(mstrModuleName, 44, "There is no exchange rate defined for current period.")
            Language.setMessage(mstrModuleName, 45, "Interest calculation type is mandatory information.")
            Language.setMessage(mstrModuleName, 46, "There are some operations that need to be done before change status. Please click on OK button to generate excel sheet for more detail log. Refer the exported path:")
            Language.setMessage(mstrModuleName, 47, "Excel file exported successfully.")
            Language.setMessage(mstrModuleName, 48, "Settlement amount cannot be blank. Settlement amount is compulsory information.")
            Language.setMessage(mstrModuleName, 49, "Settlement amount cannot be greater than Balance amount.")
            Language.setMessage(mstrModuleName, 50, "Change Status cannot be balnk. Change Status is mandatory information.")
            Language.setMessage(mstrModuleName, 51, "Please select change status. Change status is mandatory information.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class