﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanStatus_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanStatus_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvLoanAdvanceStatushistory = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbLoanStatusInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkCalInterest = New System.Windows.Forms.CheckBox
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lnkAddSettlement = New System.Windows.Forms.LinkLabel
        Me.lblBalance = New System.Windows.Forms.Label
        Me.txtBalance = New eZee.TextBox.NumericTextBox
        Me.txtSettlementAmt = New eZee.TextBox.NumericTextBox
        Me.lblSettlementAmt = New System.Windows.Forms.Label
        Me.txtLoanScheme = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmployeeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.objColDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhUser = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTransactionDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCalculateInterest = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objLoanAdvancestatustranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvLoanAdvanceStatushistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbLoanStatusInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvLoanAdvanceStatushistory)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbLoanStatusInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(634, 499)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgvLoanAdvanceStatushistory
        '
        Me.dgvLoanAdvanceStatushistory.AllowUserToAddRows = False
        Me.dgvLoanAdvanceStatushistory.AllowUserToDeleteRows = False
        Me.dgvLoanAdvanceStatushistory.AllowUserToResizeRows = False
        Me.dgvLoanAdvanceStatushistory.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanAdvanceStatushistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLoanAdvanceStatushistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objColDelete, Me.dgcolhUser, Me.dgcolhTransactionDate, Me.dgcolhStatus, Me.dgcolhCalculateInterest, Me.objdgcolhIsGroup, Me.objLoanAdvancestatustranunkid, Me.objdgcolhPeriodStatusId})
        Me.dgvLoanAdvanceStatushistory.Location = New System.Drawing.Point(11, 206)
        Me.dgvLoanAdvanceStatushistory.Name = "dgvLoanAdvanceStatushistory"
        Me.dgvLoanAdvanceStatushistory.ReadOnly = True
        Me.dgvLoanAdvanceStatushistory.RowHeadersVisible = False
        Me.dgvLoanAdvanceStatushistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanAdvanceStatushistory.Size = New System.Drawing.Size(611, 226)
        Me.dgvLoanAdvanceStatushistory.TabIndex = 3
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 444)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(634, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(422, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(525, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbLoanStatusInfo
        '
        Me.gbLoanStatusInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanStatusInfo.Checked = False
        Me.gbLoanStatusInfo.CollapseAllExceptThis = False
        Me.gbLoanStatusInfo.CollapsedHoverImage = Nothing
        Me.gbLoanStatusInfo.CollapsedNormalImage = Nothing
        Me.gbLoanStatusInfo.CollapsedPressedImage = Nothing
        Me.gbLoanStatusInfo.CollapseOnLoad = False
        Me.gbLoanStatusInfo.Controls.Add(Me.chkCalInterest)
        Me.gbLoanStatusInfo.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbLoanStatusInfo.Controls.Add(Me.cboPeriod)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblPeriod)
        Me.gbLoanStatusInfo.Controls.Add(Me.lnkAddSettlement)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblBalance)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtBalance)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtSettlementAmt)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblSettlementAmt)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtLoanScheme)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtEmployeeName)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblRemarks)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtRemarks)
        Me.gbLoanStatusInfo.Controls.Add(Me.cboStatus)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblStatus)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblEmpName)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblLoanScheme)
        Me.gbLoanStatusInfo.ExpandedHoverImage = Nothing
        Me.gbLoanStatusInfo.ExpandedNormalImage = Nothing
        Me.gbLoanStatusInfo.ExpandedPressedImage = Nothing
        Me.gbLoanStatusInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanStatusInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanStatusInfo.HeaderHeight = 25
        Me.gbLoanStatusInfo.HeaderMessage = ""
        Me.gbLoanStatusInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoanStatusInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanStatusInfo.HeightOnCollapse = 0
        Me.gbLoanStatusInfo.LeftTextSpace = 0
        Me.gbLoanStatusInfo.Location = New System.Drawing.Point(11, 3)
        Me.gbLoanStatusInfo.Name = "gbLoanStatusInfo"
        Me.gbLoanStatusInfo.OpenHeight = 300
        Me.gbLoanStatusInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanStatusInfo.ShowBorder = True
        Me.gbLoanStatusInfo.ShowCheckBox = False
        Me.gbLoanStatusInfo.ShowCollapseButton = False
        Me.gbLoanStatusInfo.ShowDefaultBorderColor = True
        Me.gbLoanStatusInfo.ShowDownButton = False
        Me.gbLoanStatusInfo.ShowHeader = True
        Me.gbLoanStatusInfo.Size = New System.Drawing.Size(611, 197)
        Me.gbLoanStatusInfo.TabIndex = 0
        Me.gbLoanStatusInfo.Temp = 0
        Me.gbLoanStatusInfo.Text = "Loan Status Info"
        Me.gbLoanStatusInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCalInterest
        '
        Me.chkCalInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCalInterest.Location = New System.Drawing.Point(462, 35)
        Me.chkCalInterest.Name = "chkCalInterest"
        Me.chkCalInterest.Size = New System.Drawing.Size(141, 17)
        Me.chkCalInterest.TabIndex = 258
        Me.chkCalInterest.Text = "Calculate Interest"
        Me.chkCalInterest.UseVisualStyleBackColor = True
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(386, 33)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 256
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(113, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(267, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(99, 15)
        Me.lblPeriod.TabIndex = 255
        Me.lblPeriod.Text = "Period"
        '
        'lnkAddSettlement
        '
        Me.lnkAddSettlement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAddSettlement.Location = New System.Drawing.Point(392, 117)
        Me.lnkAddSettlement.Name = "lnkAddSettlement"
        Me.lnkAddSettlement.Size = New System.Drawing.Size(212, 15)
        Me.lnkAddSettlement.TabIndex = 252
        Me.lnkAddSettlement.TabStop = True
        Me.lnkAddSettlement.Text = "Add Settlement"
        '
        'lblBalance
        '
        Me.lblBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(392, 87)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(99, 15)
        Me.lblBalance.TabIndex = 250
        Me.lblBalance.Text = "Balance"
        '
        'txtBalance
        '
        Me.txtBalance.AllowNegative = True
        Me.txtBalance.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBalance.DigitsInGroup = 0
        Me.txtBalance.Flags = 0
        Me.txtBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.Location = New System.Drawing.Point(497, 84)
        Me.txtBalance.MaxDecimalPlaces = 6
        Me.txtBalance.MaxWholeDigits = 21
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Prefix = ""
        Me.txtBalance.RangeMax = 1.7976931348623157E+308
        Me.txtBalance.RangeMin = -1.7976931348623157E+308
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(107, 21)
        Me.txtBalance.TabIndex = 7
        Me.txtBalance.Text = "0"
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSettlementAmt
        '
        Me.txtSettlementAmt.AllowNegative = True
        Me.txtSettlementAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSettlementAmt.DigitsInGroup = 0
        Me.txtSettlementAmt.Flags = 0
        Me.txtSettlementAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettlementAmt.Location = New System.Drawing.Point(497, 60)
        Me.txtSettlementAmt.MaxDecimalPlaces = 6
        Me.txtSettlementAmt.MaxWholeDigits = 21
        Me.txtSettlementAmt.Name = "txtSettlementAmt"
        Me.txtSettlementAmt.Prefix = ""
        Me.txtSettlementAmt.RangeMax = 1.7976931348623157E+308
        Me.txtSettlementAmt.RangeMin = -1.7976931348623157E+308
        Me.txtSettlementAmt.ReadOnly = True
        Me.txtSettlementAmt.Size = New System.Drawing.Size(107, 21)
        Me.txtSettlementAmt.TabIndex = 6
        Me.txtSettlementAmt.Text = "0"
        Me.txtSettlementAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSettlementAmt
        '
        Me.lblSettlementAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSettlementAmt.Location = New System.Drawing.Point(392, 63)
        Me.lblSettlementAmt.Name = "lblSettlementAmt"
        Me.lblSettlementAmt.Size = New System.Drawing.Size(99, 15)
        Me.lblSettlementAmt.TabIndex = 247
        Me.lblSettlementAmt.Text = "Settlement Amount"
        '
        'txtLoanScheme
        '
        Me.txtLoanScheme.Flags = 0
        Me.txtLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanScheme.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLoanScheme.Location = New System.Drawing.Point(113, 60)
        Me.txtLoanScheme.Name = "txtLoanScheme"
        Me.txtLoanScheme.ReadOnly = True
        Me.txtLoanScheme.Size = New System.Drawing.Size(267, 21)
        Me.txtLoanScheme.TabIndex = 2
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.Flags = 0
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeName.Location = New System.Drawing.Point(113, 87)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.Size = New System.Drawing.Size(267, 21)
        Me.txtEmployeeName.TabIndex = 3
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(8, 141)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(99, 15)
        Me.lblRemarks.TabIndex = 238
        Me.lblRemarks.Text = "Remarks"
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemarks.Location = New System.Drawing.Point(113, 141)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemarks.Size = New System.Drawing.Size(490, 49)
        Me.txtRemarks.TabIndex = 5
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(113, 114)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(267, 21)
        Me.cboStatus.TabIndex = 4
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 117)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(99, 15)
        Me.lblStatus.TabIndex = 233
        Me.lblStatus.Text = "Loan Status"
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 90)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(99, 15)
        Me.lblEmpName.TabIndex = 232
        Me.lblEmpName.Text = "Employee Name"
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(99, 15)
        Me.lblLoanScheme.TabIndex = 5
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'objColDelete
        '
        Me.objColDelete.HeaderText = ""
        Me.objColDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objColDelete.Name = "objColDelete"
        Me.objColDelete.ReadOnly = True
        Me.objColDelete.Width = 25
        '
        'dgcolhUser
        '
        Me.dgcolhUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhUser.HeaderText = "User"
        Me.dgcolhUser.Name = "dgcolhUser"
        Me.dgcolhUser.ReadOnly = True
        Me.dgcolhUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTransactionDate
        '
        Me.dgcolhTransactionDate.HeaderText = "Transaction Date"
        Me.dgcolhTransactionDate.Name = "dgcolhTransactionDate"
        Me.dgcolhTransactionDate.ReadOnly = True
        Me.dgcolhTransactionDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTransactionDate.Width = 150
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCalculateInterest
        '
        Me.dgcolhCalculateInterest.HeaderText = "Calculate Interest"
        Me.dgcolhCalculateInterest.Name = "dgcolhCalculateInterest"
        Me.dgcolhCalculateInterest.ReadOnly = True
        Me.dgcolhCalculateInterest.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "IsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Visible = False
        '
        'objLoanAdvancestatustranunkid
        '
        Me.objLoanAdvancestatustranunkid.HeaderText = "LoanAdvancestatustranunkid"
        Me.objLoanAdvancestatustranunkid.Name = "objLoanAdvancestatustranunkid"
        Me.objLoanAdvancestatustranunkid.ReadOnly = True
        Me.objLoanAdvancestatustranunkid.Visible = False
        '
        'objdgcolhPeriodStatusId
        '
        Me.objdgcolhPeriodStatusId.HeaderText = "objcolhPeriodStatusId"
        Me.objdgcolhPeriodStatusId.Name = "objdgcolhPeriodStatusId"
        Me.objdgcolhPeriodStatusId.ReadOnly = True
        Me.objdgcolhPeriodStatusId.Visible = False
        '
        'frmLoanStatus_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(634, 499)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanStatus_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Change Loan Status"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvLoanAdvanceStatushistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbLoanStatusInfo.ResumeLayout(False)
        Me.gbLoanStatusInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbLoanStatusInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtLoanScheme As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployeeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSettlementAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblSettlementAmt As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents txtBalance As eZee.TextBox.NumericTextBox
    Friend WithEvents lnkAddSettlement As System.Windows.Forms.LinkLabel
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents dgvLoanAdvanceStatushistory As System.Windows.Forms.DataGridView
    Friend WithEvents chkCalInterest As System.Windows.Forms.CheckBox
    Friend WithEvents objColDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTransactionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCalculateInterest As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objLoanAdvancestatustranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
